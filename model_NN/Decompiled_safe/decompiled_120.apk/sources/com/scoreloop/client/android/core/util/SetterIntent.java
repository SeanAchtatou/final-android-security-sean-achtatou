package com.scoreloop.client.android.core.util;

import java.text.ParseException;
import java.util.Date;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SetterIntent {
    private boolean a;
    private boolean b;
    private Object c;
    private Boolean d;

    public enum KeyMode {
        COERCE_NULL_WHEN_NO_KEY,
        THROWS_WHEN_NO_KEY,
        USE_NULL_WHEN_NO_KEY
    }

    public enum ValueMode {
        ALLOWS_AND_COERCES_NULL_VALUE,
        ALLOWS_NULL_VALUE,
        REQUIRES_NON_NULL_VALUE
    }

    private Object a(KeyMode keyMode, Object obj) {
        if (keyMode == KeyMode.USE_NULL_WHEN_NO_KEY) {
            return null;
        }
        if (keyMode == KeyMode.COERCE_NULL_WHEN_NO_KEY) {
            return obj;
        }
        throw new JSONException("invalid value");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.client.android.core.util.SetterIntent.a(boolean, boolean, java.lang.Boolean, java.lang.Object):void
     arg types: [int, int, int, java.lang.Object]
     candidates:
      com.scoreloop.client.android.core.util.SetterIntent.a(org.json.JSONObject, java.lang.String, com.scoreloop.client.android.core.util.SetterIntent$KeyMode, com.scoreloop.client.android.core.util.SetterIntent$ValueMode):java.lang.Integer
      com.scoreloop.client.android.core.util.SetterIntent.a(boolean, boolean, java.lang.Boolean, java.lang.Object):void */
    private void a(Object obj) {
        a(true, true, (Boolean) false, obj);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.client.android.core.util.SetterIntent.a(boolean, boolean, java.lang.Boolean, java.lang.Object):void
     arg types: [int, int, int, ?[OBJECT, ARRAY]]
     candidates:
      com.scoreloop.client.android.core.util.SetterIntent.a(org.json.JSONObject, java.lang.String, com.scoreloop.client.android.core.util.SetterIntent$KeyMode, com.scoreloop.client.android.core.util.SetterIntent$ValueMode):java.lang.Integer
      com.scoreloop.client.android.core.util.SetterIntent.a(boolean, boolean, java.lang.Boolean, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.client.android.core.util.SetterIntent.a(boolean, boolean, java.lang.Boolean, java.lang.Object):void
     arg types: [int, int, int, java.lang.Object]
     candidates:
      com.scoreloop.client.android.core.util.SetterIntent.a(org.json.JSONObject, java.lang.String, com.scoreloop.client.android.core.util.SetterIntent$KeyMode, com.scoreloop.client.android.core.util.SetterIntent$ValueMode):java.lang.Integer
      com.scoreloop.client.android.core.util.SetterIntent.a(boolean, boolean, java.lang.Boolean, java.lang.Object):void */
    private void a(Object obj, ValueMode valueMode) {
        if (valueMode == ValueMode.REQUIRES_NON_NULL_VALUE) {
            a(false, true, (Boolean) true, (Object) null);
        } else if (valueMode == ValueMode.ALLOWS_AND_COERCES_NULL_VALUE) {
            a(true, true, (Boolean) true, obj);
        } else {
            a(true, true, (Boolean) true, (Object) null);
        }
    }

    private void a(boolean z, boolean z2, Boolean bool, Object obj) {
        this.b = z;
        this.a = z2;
        this.d = bool;
        this.c = obj;
    }

    private boolean a(JSONObject jSONObject, String str) {
        return jSONObject.has(str);
    }

    private boolean b(JSONObject jSONObject, String str) {
        Object opt = jSONObject.opt(str);
        return opt != null && !JSONObject.NULL.equals(opt);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.client.android.core.util.SetterIntent.a(boolean, boolean, java.lang.Boolean, java.lang.Object):void
     arg types: [int, int, ?[OBJECT, ARRAY], ?[OBJECT, ARRAY]]
     candidates:
      com.scoreloop.client.android.core.util.SetterIntent.a(org.json.JSONObject, java.lang.String, com.scoreloop.client.android.core.util.SetterIntent$KeyMode, com.scoreloop.client.android.core.util.SetterIntent$ValueMode):java.lang.Integer
      com.scoreloop.client.android.core.util.SetterIntent.a(boolean, boolean, java.lang.Boolean, java.lang.Object):void */
    private void c() {
        a(false, false, (Boolean) null, (Object) null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.client.android.core.util.SetterIntent.a(com.scoreloop.client.android.core.util.SetterIntent$KeyMode, java.lang.Object):java.lang.Object
     arg types: [com.scoreloop.client.android.core.util.SetterIntent$KeyMode, int]
     candidates:
      com.scoreloop.client.android.core.util.SetterIntent.a(java.lang.Object, com.scoreloop.client.android.core.util.SetterIntent$ValueMode):void
      com.scoreloop.client.android.core.util.SetterIntent.a(org.json.JSONObject, java.lang.String):boolean
      com.scoreloop.client.android.core.util.SetterIntent.a(com.scoreloop.client.android.core.util.SetterIntent$KeyMode, java.lang.Object):java.lang.Object */
    public Integer a(JSONObject jSONObject, String str, KeyMode keyMode, ValueMode valueMode) {
        return d(jSONObject, str, valueMode) ? (Integer) a() : (Integer) a(keyMode, (Object) 0);
    }

    public Object a() {
        return this.c;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.client.android.core.util.SetterIntent.a(java.lang.Object, com.scoreloop.client.android.core.util.SetterIntent$ValueMode):void
     arg types: [boolean, com.scoreloop.client.android.core.util.SetterIntent$ValueMode]
     candidates:
      com.scoreloop.client.android.core.util.SetterIntent.a(com.scoreloop.client.android.core.util.SetterIntent$KeyMode, java.lang.Object):java.lang.Object
      com.scoreloop.client.android.core.util.SetterIntent.a(org.json.JSONObject, java.lang.String):boolean
      com.scoreloop.client.android.core.util.SetterIntent.a(java.lang.Object, com.scoreloop.client.android.core.util.SetterIntent$ValueMode):void */
    public boolean a(JSONObject jSONObject, String str, ValueMode valueMode) {
        if (!a(jSONObject, str)) {
            c();
        } else if (b(jSONObject, str)) {
            a(Boolean.valueOf(jSONObject.getBoolean(str)));
        } else {
            a((Object) false, valueMode);
        }
        return this.b;
    }

    public Boolean b() {
        return this.d;
    }

    public JSONObject b(JSONObject jSONObject, String str, KeyMode keyMode, ValueMode valueMode) {
        return f(jSONObject, str, valueMode) ? (JSONObject) a() : (JSONObject) a(keyMode, new JSONObject());
    }

    public boolean b(JSONObject jSONObject, String str, ValueMode valueMode) {
        if (!a(jSONObject, str)) {
            c();
        } else if (b(jSONObject, str)) {
            String string = jSONObject.getString(str);
            try {
                a(Formats.a.parse(string));
            } catch (ParseException e) {
                try {
                    a(Formats.b.parse(string));
                } catch (ParseException e2) {
                    throw new JSONException(String.format("Invalid format of the %s date: '%s'", str, string));
                }
            }
        } else {
            a(new Date(), valueMode);
        }
        return this.b;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.client.android.core.util.SetterIntent.a(com.scoreloop.client.android.core.util.SetterIntent$KeyMode, java.lang.Object):java.lang.Object
     arg types: [com.scoreloop.client.android.core.util.SetterIntent$KeyMode, long]
     candidates:
      com.scoreloop.client.android.core.util.SetterIntent.a(java.lang.Object, com.scoreloop.client.android.core.util.SetterIntent$ValueMode):void
      com.scoreloop.client.android.core.util.SetterIntent.a(org.json.JSONObject, java.lang.String):boolean
      com.scoreloop.client.android.core.util.SetterIntent.a(com.scoreloop.client.android.core.util.SetterIntent$KeyMode, java.lang.Object):java.lang.Object */
    public Long c(JSONObject jSONObject, String str, KeyMode keyMode, ValueMode valueMode) {
        return g(jSONObject, str, valueMode) ? (Long) a() : (Long) a(keyMode, (Object) 0L);
    }

    public boolean c(JSONObject jSONObject, String str, ValueMode valueMode) {
        if (!a(jSONObject, str)) {
            c();
        } else if (b(jSONObject, str)) {
            a(Double.valueOf(jSONObject.getDouble(str)));
        } else {
            a(Double.valueOf(0.0d), valueMode);
        }
        return this.b;
    }

    public String d(JSONObject jSONObject, String str, KeyMode keyMode, ValueMode valueMode) {
        return h(jSONObject, str, valueMode) ? (String) a() : (String) a(keyMode, "");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.client.android.core.util.SetterIntent.a(java.lang.Object, com.scoreloop.client.android.core.util.SetterIntent$ValueMode):void
     arg types: [int, com.scoreloop.client.android.core.util.SetterIntent$ValueMode]
     candidates:
      com.scoreloop.client.android.core.util.SetterIntent.a(com.scoreloop.client.android.core.util.SetterIntent$KeyMode, java.lang.Object):java.lang.Object
      com.scoreloop.client.android.core.util.SetterIntent.a(org.json.JSONObject, java.lang.String):boolean
      com.scoreloop.client.android.core.util.SetterIntent.a(java.lang.Object, com.scoreloop.client.android.core.util.SetterIntent$ValueMode):void */
    public boolean d(JSONObject jSONObject, String str, ValueMode valueMode) {
        if (!a(jSONObject, str)) {
            c();
        } else if (b(jSONObject, str)) {
            a(Integer.valueOf(jSONObject.getInt(str)));
        } else {
            a((Object) 0, valueMode);
        }
        return this.b;
    }

    public boolean e(JSONObject jSONObject, String str, ValueMode valueMode) {
        if (!a(jSONObject, str)) {
            c();
        } else if (b(jSONObject, str)) {
            a(jSONObject.getJSONArray(str));
        } else {
            a(new JSONArray(), valueMode);
        }
        return this.b;
    }

    public boolean f(JSONObject jSONObject, String str, ValueMode valueMode) {
        if (!a(jSONObject, str)) {
            c();
        } else if (b(jSONObject, str)) {
            a(jSONObject.getJSONObject(str));
        } else {
            a(new JSONObject(), valueMode);
        }
        return this.b;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.client.android.core.util.SetterIntent.a(java.lang.Object, com.scoreloop.client.android.core.util.SetterIntent$ValueMode):void
     arg types: [long, com.scoreloop.client.android.core.util.SetterIntent$ValueMode]
     candidates:
      com.scoreloop.client.android.core.util.SetterIntent.a(com.scoreloop.client.android.core.util.SetterIntent$KeyMode, java.lang.Object):java.lang.Object
      com.scoreloop.client.android.core.util.SetterIntent.a(org.json.JSONObject, java.lang.String):boolean
      com.scoreloop.client.android.core.util.SetterIntent.a(java.lang.Object, com.scoreloop.client.android.core.util.SetterIntent$ValueMode):void */
    public boolean g(JSONObject jSONObject, String str, ValueMode valueMode) {
        if (!a(jSONObject, str)) {
            c();
        } else if (b(jSONObject, str)) {
            a(Long.valueOf(jSONObject.getLong(str)));
        } else {
            a((Object) 0L, valueMode);
        }
        return this.b;
    }

    public boolean h(JSONObject jSONObject, String str, ValueMode valueMode) {
        if (!a(jSONObject, str)) {
            c();
        } else if (b(jSONObject, str)) {
            String string = jSONObject.getString(str);
            if ("".equals(string)) {
                a("", valueMode);
            } else {
                a(string);
            }
        } else {
            a("", valueMode);
        }
        return this.b;
    }
}
