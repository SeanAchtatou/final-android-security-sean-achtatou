package com.scoreloop.client.android.ui.component.entry;

import android.graphics.drawable.Drawable;
import com.scoreloop.client.android.ui.component.base.ComponentActivity;
import com.scoreloop.client.android.ui.component.base.k;
import org.anddev.andengine.R;

public final class b extends k {
    public b(ComponentActivity componentActivity, Drawable drawable, String str) {
        super(componentActivity, drawable, str, null, null);
    }

    /* access modifiers changed from: protected */
    public final int j() {
        return R.id.sl_list_item_main_icon;
    }

    /* access modifiers changed from: protected */
    public final int h() {
        return R.layout.sl_list_item_main;
    }

    /* access modifiers changed from: protected */
    public final int g() {
        return R.id.sl_list_item_main_subtitle;
    }

    /* access modifiers changed from: protected */
    public final int k() {
        return R.id.sl_list_item_main_title;
    }

    public final int a() {
        return 10;
    }
}
