package com.scoreloop.client.android.ui.framework;

public final class ae {
    private final int a;
    private final int b;
    private final int c;

    public ae(int i, int i2, int i3) {
        this.c = i;
        this.b = i2;
        this.a = i3;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ae aeVar = (ae) obj;
        if (this.a != aeVar.a) {
            return false;
        }
        if (this.b != aeVar.b) {
            return false;
        }
        if (this.c != aeVar.c) {
            return false;
        }
        return true;
    }

    public final int a() {
        return this.a;
    }

    public final int b() {
        return this.b;
    }

    public final int c() {
        return this.c;
    }

    public final int hashCode() {
        return ((((this.a + 31) * 31) + this.b) * 31) + this.c;
    }
}
