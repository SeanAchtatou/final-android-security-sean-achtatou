package com.scoreloop.client.android.core.model;

import android.graphics.Bitmap;
import com.scoreloop.client.android.core.util.Formats;
import com.scoreloop.client.android.core.util.SetterIntent;
import java.util.Date;
import java.util.UUID;
import org.anddev.andengine.entity.layer.tiled.tmx.util.constants.TMXConstants;
import org.json.JSONException;
import org.json.JSONObject;

public final class Achievement extends BaseEntity implements MessageTargetInterface {
    public static String a = "achievement";
    private final Award c;
    private Date d;
    private boolean e;
    private AchievementsStore f;
    private String g;
    private int h;
    private String i;

    public Achievement(Award award) {
        this.c = award;
        this.h = this.c.getInitialValue();
    }

    public Achievement(Award award, JSONObject jSONObject, AchievementsStore achievementsStore) {
        this.f = achievementsStore;
        this.c = award;
        if (jSONObject != null) {
            a(jSONObject);
        } else {
            this.h = this.c.getInitialValue();
        }
        if (this.b == null && this.i == null) {
            this.i = UUID.randomUUID().toString();
        }
    }

    public Achievement(AwardList awardList, JSONObject jSONObject) {
        this.c = awardList.getAwardWithIdentifier(jSONObject.getString("achievable_identifier"));
        a(jSONObject);
    }

    private void f() {
        this.f.a(this);
    }

    public final String a() {
        return a;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
    public final JSONObject a(boolean z) {
        JSONObject d2 = super.d();
        d2.put("achievable_list_id", getAward().b().a());
        d2.put("achievable_identifier", getAward().getIdentifier());
        d2.put("achievable_type", getAward().e());
        d2.put("achieved_count", getValue());
        d2.put("user_id", c());
        if (getDate() != null) {
            String format = Formats.a.format(getDate());
            if (format.length() < 24) {
                format = format + "+0000";
            }
            d2.put("achieved_at", format);
        }
        if (z) {
            if (this.e) {
                d2.put("needs_submit", true);
            }
            d2.put("local_id", this.i);
        }
        return d2;
    }

    public final void a(Achievement achievement, boolean z) {
        boolean z2;
        if (z) {
            this.e = false;
            z2 = true;
        } else {
            z2 = false;
        }
        String identifier = achievement.getIdentifier();
        if (!(identifier == null || identifier == getIdentifier())) {
            b(identifier);
            z2 = true;
        }
        int value = achievement.getValue();
        if (value > this.h) {
            this.h = value;
            this.e = false;
            z2 = true;
        }
        Date date = achievement.getDate();
        if (!(date == null || date == this.d)) {
            this.d = date;
            z2 = true;
        }
        if (z2) {
            f();
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Object obj) {
        a(obj, "award", getAward());
        a(obj, TMXConstants.TAG_PROPERTY_ATTRIBUTE_VALUE, Integer.valueOf(getValue()));
        a(obj, "isAchieved", Boolean.valueOf(isAchieved()));
    }

    public final void a(String str) {
        this.g = str;
    }

    public final void a(JSONObject jSONObject) {
        super.a(jSONObject);
        SetterIntent setterIntent = new SetterIntent();
        String d2 = setterIntent.d(jSONObject, "achievable_identifier", SetterIntent.KeyMode.THROWS_WHEN_NO_KEY, SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE);
        if (!d2.equals(this.c.getIdentifier())) {
            throw new JSONException("invalid achievable_identifier " + d2);
        }
        String d3 = setterIntent.d(jSONObject, "achievable_type", SetterIntent.KeyMode.THROWS_WHEN_NO_KEY, SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE);
        if (!d3.equals(this.c.e())) {
            throw new JSONException("invalid achievable_type " + d3);
        }
        this.h = setterIntent.a(jSONObject, "achieved_count", SetterIntent.KeyMode.THROWS_WHEN_NO_KEY, SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE).intValue();
        if (!this.c.isValidCounterValue(this.h)) {
            throw new JSONException("invalid achieved_count " + this.h);
        }
        if (setterIntent.h(jSONObject, "user_id", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.g = (String) setterIntent.a();
        }
        if (setterIntent.b(jSONObject, "achieved_at", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.d = (Date) setterIntent.a();
        }
        if (setterIntent.a(jSONObject, "needs_submit", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.e = ((Boolean) setterIntent.a()).booleanValue();
        }
        if (setterIntent.h(jSONObject, "local_id", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.i = (String) setterIntent.a();
        }
    }

    public final String c() {
        return this.g;
    }

    public final JSONObject d() {
        return a(false);
    }

    public final String e() {
        return this.i;
    }

    public final Award getAward() {
        return this.c;
    }

    public final Date getDate() {
        return this.d;
    }

    public final Bitmap getImage() {
        return isAchieved() ? getAward().getAchievedImage() : getAward().getUnachievedImage();
    }

    public final int getValue() {
        return this.h;
    }

    public final boolean isAchieved() {
        return getAward().isAchievedByValue(getValue());
    }

    public final boolean needsSubmit() {
        return this.e;
    }

    public final void setAchieved() {
        setValue(this.c.getAchievingValue());
    }

    public final void setValue(int i2) {
        if (!this.c.isValidCounterValue(i2)) {
            throw new IllegalArgumentException("invalid value, should be in: " + this.c.getCounterRange());
        } else if (i2 < this.h) {
            throw new IllegalArgumentException("the value to set must not be less than the old value: " + this.h);
        } else if (this.f == null) {
            throw new IllegalStateException("setValue() on achievement only allowed for session user");
        } else {
            boolean isAchieved = isAchieved();
            this.h = i2;
            if (isAchieved() != isAchieved) {
                this.e = true;
                this.d = new Date();
            }
            f();
        }
    }
}
