package com.scoreloop.client.android.ui;

import android.content.Context;

public final class f {
    private static e a;

    public static e a() {
        if (a != null) {
            return a;
        }
        throw new IllegalStateException("ScoreloopManagerSingleton.init() must be called first");
    }

    public static e a(Context context, String str) {
        if (a != null) {
            throw new IllegalStateException("ScoreloopManagerSingleton.init() can be called only once");
        }
        c cVar = new c(context, str);
        a = cVar;
        return cVar;
    }
}
