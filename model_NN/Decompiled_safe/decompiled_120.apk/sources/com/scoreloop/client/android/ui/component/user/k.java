package com.scoreloop.client.android.ui.component.user;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import org.anddev.andengine.R;

final class k extends Dialog implements View.OnClickListener {
    private EditText a;
    private Button b;
    private /* synthetic */ UserAddBuddyListActivity c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    k(UserAddBuddyListActivity userAddBuddyListActivity, Context context) {
        super(context, R.style.sl_dialog);
        this.c = userAddBuddyListActivity;
    }

    public final void onClick(View view) {
        if (view.getId() == R.id.button_ok) {
            dismiss();
            UserAddBuddyListActivity.a(this.c, this.a.getText().toString().trim());
        }
    }

    /* access modifiers changed from: protected */
    public final void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.sl_dialog_login);
        setTitle(this.c.getResources().getString(R.string.sl_scoreloop_username));
        this.a = (EditText) findViewById(R.id.edit_login);
        this.b = (Button) findViewById(R.id.button_ok);
        this.b.setOnClickListener(this);
    }
}
