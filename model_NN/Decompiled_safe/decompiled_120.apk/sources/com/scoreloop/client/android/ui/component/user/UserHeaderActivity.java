package com.scoreloop.client.android.ui.component.user;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.scoreloop.client.android.core.controller.MessageController;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.ui.a.k;
import com.scoreloop.client.android.ui.component.a.e;
import com.scoreloop.client.android.ui.component.base.ComponentHeaderActivity;
import com.scoreloop.client.android.ui.framework.h;
import com.scoreloop.client.android.ui.framework.s;
import java.util.List;
import org.anddev.andengine.R;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.opengl.texture.compressed.pvr.PVRTexture;

public class UserHeaderActivity extends ComponentHeaderActivity implements View.OnClickListener {
    private static /* synthetic */ int[] d;
    private boolean a;
    private MessageController b;
    private j c;

    private static /* synthetic */ int[] e() {
        int[] iArr = d;
        if (iArr == null) {
            iArr = new int[j.values().length];
            try {
                iArr[j.BLANK.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[j.BUDDY.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[j.PROFILE.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            d = iArr;
        }
        return iArr;
    }

    private void b() {
        c();
        this.a = false;
    }

    private static String b(s sVar, String str) {
        Integer num = (Integer) sVar.a(str);
        return num != null ? num.toString() : "";
    }

    private ImageView c() {
        ImageView imageView = (ImageView) findViewById(R.id.sl_control_icon);
        imageView.setImageDrawable(null);
        imageView.setOnClickListener(null);
        imageView.setEnabled(false);
        View findViewById = findViewById(R.id.sl_header_layout);
        findViewById.setEnabled(true);
        findViewById.setOnClickListener(null);
        return imageView;
    }

    public void onClick(View view) {
        User F = F();
        if (this.c == j.PROFILE) {
            B();
            a(A().e(F));
        } else if (this.c == j.BUDDY) {
            b();
            e.a(this, F, z(), new e(this, F));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.client.android.ui.component.user.UserHeaderActivity.b(int, boolean):void
     arg types: [?, int]
     candidates:
      com.scoreloop.client.android.ui.component.user.UserHeaderActivity.b(com.scoreloop.client.android.ui.framework.s, java.lang.String):java.lang.String
      com.scoreloop.client.android.ui.component.user.UserHeaderActivity.b(int, boolean):void */
    public void onCreate(Bundle bundle) {
        super.a(bundle, (int) R.layout.sl_header_user);
        a(s.a("sessionUserValues", "userBuddies"), s.a("userValues", "userName"), s.a("userValues", "userImageUrl"), s.a("userValues", "numberGames"), s.a("userValues", "numberBuddies"), s.a("userValues", "numberGlobalAchievements"));
        this.c = (j) k().a("mode", j.BLANK);
        switch (e()[this.c.ordinal()]) {
            case TouchEvent.ACTION_CANCEL /*3*/:
                b((int) R.drawable.sl_button_account_settings, true);
                break;
            default:
                b();
                break;
        }
        d();
    }

    public final boolean a(Menu menu) {
        super.a(menu);
        menu.add(0, (int) PVRTexture.FLAG_MIPMAP, 0, (int) R.string.sl_remove_friend).setIcon((int) R.drawable.sl_icon_remove_friend);
        if (G()) {
            return true;
        }
        menu.add(0, 296, 0, (int) R.string.sl_abuse_report_title).setIcon((int) R.drawable.sl_icon_flag_inappropriate);
        return true;
    }

    public final boolean a(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case PVRTexture.FLAG_MIPMAP /*256*/:
                b();
                User F = F();
                e.b(this, F, z(), new f(this, F));
                B();
                return true;
            case 296:
                this.b = new MessageController(E());
                this.b.setTarget(F());
                this.b.setMessageType(MessageController.TYPE_ABUSE_REPORT);
                this.b.setText("Inappropriate user in ScoreloopUI");
                this.b.addReceiverWithUsers(MessageController.RECEIVER_SYSTEM, new Object[0]);
                if (this.b.isSubmitAllowed()) {
                    this.b.submitMessage();
                }
                B();
                return true;
            default:
                return super.a(menuItem);
        }
    }

    public final boolean b(Menu menu) {
        MenuItem findItem = menu.findItem(PVRTexture.FLAG_MIPMAP);
        if (findItem != null) {
            findItem.setVisible(this.a);
        }
        return super.b(menu);
    }

    public final void a(s sVar, String str, Object obj, Object obj2) {
        d();
    }

    public final void a(s sVar, String str) {
        if ("userName".equals(str)) {
            y().a(str, h.NOT_DIRTY, (Object) null);
        } else if ("userImageUrl".equals(str)) {
            y().a(str, h.NOT_DIRTY, (Object) null);
        } else if ("numberGames".equals(str)) {
            y().a(str, h.NOT_DIRTY, (Object) null);
        } else if ("numberBuddies".equals(str)) {
            y().a(str, h.NOT_DIRTY, (Object) null);
        } else if ("numberGlobalAchievements".equals(str)) {
            y().a(str, h.NOT_DIRTY, (Object) null);
        } else if (this.c == j.BUDDY && "userBuddies".equals(str)) {
            z().a(str, h.NOT_DIRTY, (Object) null);
        }
    }

    public final void a(RequestController requestController) {
        if (requestController == this.b) {
            d(getString(R.string.sl_abuse_report_sent));
        }
    }

    private void b(int i, boolean z) {
        ImageView c2 = c();
        c2.setImageResource(i);
        c2.setEnabled(true);
        c2.setOnClickListener(this);
        if (z) {
            View findViewById = findViewById(R.id.sl_header_layout);
            findViewById.setEnabled(true);
            findViewById.setOnClickListener(this);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.client.android.ui.component.user.UserHeaderActivity.b(int, boolean):void
     arg types: [?, int]
     candidates:
      com.scoreloop.client.android.ui.component.user.UserHeaderActivity.b(com.scoreloop.client.android.ui.framework.s, java.lang.String):java.lang.String
      com.scoreloop.client.android.ui.component.user.UserHeaderActivity.b(int, boolean):void */
    private void d() {
        List list;
        s y = y();
        String str = (String) y.a("userImageUrl");
        if (str == null) {
            str = ((User) y.a("user")).getImageUrl();
        }
        if (str != null) {
            k.a(str, getResources().getDrawable(R.drawable.sl_icon_games_loading), a());
        } else if (F().getIdentifier() == null) {
            a().setImageDrawable(null);
        } else {
            a().setImageResource(R.drawable.sl_header_icon_user);
        }
        c((String) y.a("userName"));
        ((TextView) findViewById(R.id.sl_header_number_games)).setText(b(y, "numberGames"));
        ((TextView) findViewById(R.id.sl_header_number_friends)).setText(b(y, "numberBuddies"));
        ((TextView) findViewById(R.id.sl_header_number_achievements)).setText(b(y, "numberGlobalAchievements"));
        if (this.c == j.BUDDY && (list = (List) z().a("userBuddies")) != null) {
            if (F() == Session.getCurrentSession().getUser() || list.contains(F())) {
                c();
                this.a = true;
                return;
            }
            b((int) R.drawable.sl_button_add_friend, false);
            this.a = false;
        }
    }
}
