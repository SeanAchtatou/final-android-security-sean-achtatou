package com.scoreloop.client.android.core.model;

import com.scoreloop.client.android.core.util.Logger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ScoreFormatter {
    private static final HashMap a;
    private Pattern b = Pattern.compile("[ \r\n\t]*[\"]?([A-Za-z0-9]+(\\:[0-9]+)?)[\"]?[ ]*=[ ]*((\"([^\"\\\\]|\\\\\"|\\\\)*\")|[^;]*)[ \r\n\t]*");
    private Pattern c = Pattern.compile("[0-9]+");
    private Pattern d = Pattern.compile("%[rRtTqQlLmMsSh]|%0[hmsq]");
    private HashMap e = new HashMap();
    private HashMap f = new HashMap();
    private HashMap g = new HashMap();

    public enum ScoreFormatKey {
        DefaultFormat(a.DefaultFormat),
        ScoresOnlyFormat(a.ScoresOnlyFormat),
        LevelAndModeFormat(a.LevelAndModeFormat),
        LevelOnlyFormat(a.LevelOnlyFormat),
        ModeOnlyFormat(a.ModeOnlyFormat),
        NoLevelFormat(a.NoLevelFormat),
        ScoresAndLevelFormat(a.ScoresAndLevelFormat);
        
        private a a;

        private ScoreFormatKey(a aVar) {
            this.a = aVar;
        }

        public static ScoreFormatKey parse(String str) {
            try {
                return (ScoreFormatKey) Enum.valueOf(ScoreFormatKey.class, str);
            } catch (IllegalArgumentException e) {
                return null;
            }
        }

        /* access modifiers changed from: package-private */
        public final a a() {
            return this.a;
        }
    }

    enum a {
        DefaultFormat,
        LevelAndModeFormat,
        LevelOnlyFormat,
        ModeOnlyFormat,
        NoLevelFormat,
        ScoresAndLevelFormat,
        ScoresOnlyFormat,
        ResultTimeConversion,
        LevelSymbol,
        ModeSymbol,
        ResultSymbol,
        MinorResultSymbol,
        Separator;

        public static a parse(String str) {
            try {
                return (a) Enum.valueOf(a.class, str);
            } catch (IllegalArgumentException e) {
                return null;
            }
        }
    }

    static {
        HashMap hashMap = new HashMap();
        a = hashMap;
        hashMap.put(a.DefaultFormat, "%r%R %S %L %l");
        a.put(a.ResultTimeConversion, "%h:%0m:%0s");
        a.put(a.ScoresOnlyFormat, "%r%R");
        a.put(a.LevelOnlyFormat, "%L %l");
        a.put(a.ModeOnlyFormat, "%M %m");
        a.put(a.LevelAndModeFormat, "%L %l");
        a.put(a.ScoresAndLevelFormat, "%r%R %S %L %l");
        a.put(a.LevelSymbol, "Level");
        a.put(a.ModeSymbol, "Mode");
        a.put(a.ResultSymbol, "");
        a.put(a.MinorResultSymbol, "");
        a.put(a.Separator, "for");
    }

    public ScoreFormatter(String str) {
        this.e.putAll(a);
        b(str);
    }

    private String a(String str, double d2) {
        boolean z;
        char charAt;
        String l;
        int i = (int) (d2 / 3600.0d);
        int i2 = (int) ((d2 % 3600.0d) / 60.0d);
        int i3 = (int) (d2 % 60.0d);
        int i4 = (int) ((100.0d * d2) % 100.0d);
        if (str.charAt(1) == '0') {
            z = true;
            charAt = str.charAt(2);
        } else {
            z = false;
            charAt = str.charAt(1);
        }
        switch (charAt) {
            case 'h':
                l = Long.toString((long) i);
                break;
            case 'm':
                l = Long.toString((long) i2);
                break;
            case 'q':
                l = Long.toString((long) i4);
                break;
            case 's':
                l = Long.toString((long) i3);
                break;
            default:
                Logger.b("Formatter", "Unsupported time token: " + str);
                l = str;
                break;
        }
        return (!z || l.length() >= 2) ? l : "0" + l;
    }

    private String a(String str, Score score) {
        String str2;
        switch (str.charAt(1) == '0' ? str.charAt(2) : str.charAt(1)) {
            case 'L':
                return (String) this.e.get(a.LevelSymbol);
            case 'M':
                return (String) this.e.get(a.ModeSymbol);
            case 'Q':
                return (String) this.e.get(a.MinorResultSymbol);
            case 'R':
                return (String) this.e.get(a.ResultSymbol);
            case 'S':
                return (String) this.e.get(a.Separator);
            case 'T':
                return a(score.getResult() == null ? 0.0d : score.getResult().doubleValue());
            case 'l':
                Integer level = score.getLevel();
                if (level == null) {
                    return "";
                }
                str2 = (String) this.g.get(level);
                if (str2 == null) {
                    return Integer.toString(level.intValue());
                }
                break;
            case 'm':
                Integer mode = score.getMode();
                if (mode == null) {
                    return "";
                }
                str2 = (String) this.f.get(mode);
                if (str2 == null) {
                    return Integer.toString(mode.intValue());
                }
                break;
            case 'q':
                return String.format("%.0f", score.getMinorResult());
            case 'r':
                return String.format("%.0f", score.getResult());
            case 't':
                return a(score.getMinorResult() == null ? 0.0d : score.getMinorResult().doubleValue());
            default:
                Logger.b("Formatter", "Unsupported token: " + str);
                return str;
        }
        return str2;
    }

    private List a(String str) {
        String str2;
        ArrayList arrayList = new ArrayList();
        int i = 0;
        do {
            Matcher matcher = this.d.matcher(str);
            if (matcher.find(i)) {
                String substring = str.substring(matcher.start(), matcher.end());
                arrayList.add(substring);
                int end = matcher.end();
                str2 = substring;
                i = end;
                continue;
            } else {
                str2 = null;
                continue;
            }
        } while (str2 != null);
        return arrayList;
    }

    private void b(String str) {
        if (str != null) {
            int i = 0;
            Matcher matcher = this.b.matcher(str);
            while (true) {
                if (!matcher.find()) {
                    break;
                }
                String trim = matcher.group(1).trim();
                String trim2 = matcher.group(3).trim();
                int end = matcher.end();
                if (trim2.startsWith("\"")) {
                    trim2 = trim2.substring(1, trim2.length() - 1);
                }
                String replace = trim2.replace("\\\"", "\"");
                if (trim.startsWith("mode:")) {
                    try {
                        this.f.put(Integer.valueOf(Integer.parseInt(trim.substring("mode:".length()))), replace);
                    } catch (NumberFormatException e2) {
                        Logger.b("Formatter", "Invalid mode key: " + trim);
                    }
                } else {
                    a parse = a.parse(trim);
                    if (parse != null) {
                        this.e.put(parse, replace);
                    } else if (this.c.matcher(trim).matches()) {
                        try {
                            this.g.put(Integer.valueOf(Integer.parseInt(trim)), replace);
                        } catch (NumberFormatException e3) {
                            Logger.b("Formatter", "Invalid level key: " + trim);
                        }
                    } else {
                        Logger.b("Formatter", "Unknown format key: " + trim);
                    }
                }
                if (end < str.length()) {
                    if (str.charAt(end) != ';') {
                        Logger.b("Formatter", "Missing format separator in string: " + str.substring(end));
                        i = end;
                        break;
                    }
                    i = end + 1;
                } else {
                    i = end;
                }
            }
            if (i < str.length()) {
                Logger.b("Formatter", "Couldn't parse string (incorrect format): " + str.substring(i));
            }
        }
    }

    /* access modifiers changed from: protected */
    public String a(double d2) {
        String str = (String) this.e.get(a.ResultTimeConversion);
        Iterator it = a(str).iterator();
        while (true) {
            String str2 = str;
            if (!it.hasNext()) {
                return str2;
            }
            String str3 = (String) it.next();
            str = str2.replace(str3, a(str3, d2));
        }
    }

    public String formatScore(Score score) {
        return formatScore(score, ScoreFormatKey.DefaultFormat);
    }

    public String formatScore(Score score, ScoreFormatKey scoreFormatKey) {
        String str = (String) this.e.get(scoreFormatKey.a());
        Iterator it = a(str).iterator();
        while (true) {
            String str2 = str;
            if (!it.hasNext()) {
                return str2;
            }
            String str3 = (String) it.next();
            str = str2.replace(str3, a(str3, score));
        }
    }

    public String[] getDefinedModesNames(int i, int i2) {
        String[] strArr = new String[i2];
        int i3 = 0;
        while (true) {
            int i4 = i3;
            if (i4 >= strArr.length) {
                return strArr;
            }
            strArr[i4] = (String) this.f.get(Integer.valueOf(i4 + i));
            i3 = i4 + 1;
        }
    }
}
