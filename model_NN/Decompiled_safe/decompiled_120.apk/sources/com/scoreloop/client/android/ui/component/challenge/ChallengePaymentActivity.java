package com.scoreloop.client.android.ui.component.challenge;

import android.app.Dialog;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.ui.component.base.ComponentActivity;
import com.scoreloop.client.android.ui.framework.ag;
import com.scoreloop.client.android.ui.framework.ah;
import com.scoreloop.client.android.ui.framework.ai;
import com.scoreloop.client.android.ui.framework.w;
import org.anddev.andengine.R;
import org.anddev.andengine.util.constants.TimeConstants;

public class ChallengePaymentActivity extends ComponentActivity implements ag {
    private ah a;
    private WebView b;

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i) {
        switch (i) {
            case TimeConstants.DAYSPERWEEK /*7*/:
                ai aiVar = new ai(this);
                aiVar.b(getResources().getString(R.string.sl_leave_payment));
                aiVar.a((ag) this);
                aiVar.setOnDismissListener(this);
                return aiVar;
            default:
                return super.onCreateDialog(i);
        }
    }

    /* access modifiers changed from: protected */
    public void onPrepareDialog(int i, Dialog dialog) {
        switch (i) {
            case TimeConstants.DAYSPERWEEK /*7*/:
                ((ai) dialog).a(this.a);
                break;
        }
        super.onPrepareDialog(i, dialog);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.client.android.ui.component.base.ComponentActivity.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.scoreloop.client.android.ui.component.challenge.ChallengePaymentActivity.a(com.scoreloop.client.android.ui.framework.w, int):void
      com.scoreloop.client.android.ui.component.base.ComponentActivity.a(com.scoreloop.client.android.core.controller.RequestController, java.lang.Exception):void
      com.scoreloop.client.android.ui.framework.BaseActivity.a(android.content.Context, java.lang.String):void
      com.scoreloop.client.android.ui.framework.BaseActivity.a(int, com.scoreloop.client.android.ui.framework.y):void
      com.scoreloop.client.android.ui.framework.BaseActivity.a(com.scoreloop.client.android.ui.framework.s, java.lang.String):void
      com.scoreloop.client.android.ui.framework.aj.a(com.scoreloop.client.android.ui.framework.s, java.lang.String):void
      com.scoreloop.client.android.ui.framework.ag.a(com.scoreloop.client.android.ui.framework.w, int):void
      com.scoreloop.client.android.ui.component.base.ComponentActivity.a(int, boolean):void */
    /* access modifiers changed from: protected */
    public final boolean a(ah ahVar) {
        this.a = ahVar;
        a(7, true);
        return false;
    }

    public final void a(w wVar, int i) {
        if (i == 0) {
            wVar.dismiss();
            ((ah) wVar.d()).a();
            return;
        }
        wVar.dismiss();
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        k().b("navigationIntent", this.a);
    }

    public void onCreate(Bundle bundle) {
        this.a = (ah) k().a("navigationIntent");
        super.onCreate(bundle);
        b((int) R.layout.sl_challenge_payment);
        this.b = (WebView) findViewById(R.id.sl_webview);
        this.b.setWebViewClient(new m(this));
        WebSettings settings = this.b.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setDefaultTextEncodingName("UTF-8");
        settings.setBuiltInZoomControls(true);
        this.b.loadUrl(Session.getCurrentSession().getPaymentUrl());
        this.b.requestFocus();
    }
}
