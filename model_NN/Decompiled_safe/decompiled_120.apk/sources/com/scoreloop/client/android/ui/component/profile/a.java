package com.scoreloop.client.android.ui.component.profile;

import com.scoreloop.client.android.core.model.User;

final class a implements Runnable {
    private /* synthetic */ ProfileSettingsListActivity a;
    private final /* synthetic */ User b;

    a(ProfileSettingsListActivity profileSettingsListActivity, User user) {
        this.a = profileSettingsListActivity;
        this.b = user;
    }

    public final void run() {
        this.a.b(this.a.e);
        this.a.e.setUser(this.b);
        this.a.e.submitUser();
    }
}
