package com.scoreloop.client.android.ui.a;

import android.os.Handler;
import java.lang.ref.SoftReference;
import java.util.HashMap;
import java.util.HashSet;
import java.util.concurrent.ConcurrentHashMap;

public final class m {
    /* access modifiers changed from: private */
    public int a;
    private long b;
    private HashMap c;
    /* access modifiers changed from: private */
    public ConcurrentHashMap d;
    private Handler e;
    private final Runnable f;

    public m() {
        this(100);
    }

    public m(int i) {
        this.f = new c(this);
        this.a = i;
        this.b = 0;
        this.e = new Handler();
        this.c = new b(this, this.a / 2);
        this.d = new ConcurrentHashMap(this.a / 2);
    }

    public final void a() {
        long currentTimeMillis = System.currentTimeMillis();
        HashSet hashSet = new HashSet(this.c.keySet());
        this.d.clear();
        for (Object next : hashSet) {
            f fVar = (f) this.c.get(next);
            if (fVar.a() + fVar.b() < currentTimeMillis) {
                synchronized (this.c) {
                    this.d.put(next, new SoftReference(fVar));
                    this.c.remove(next);
                }
            }
        }
        a(this.b);
    }

    private void a(long j) {
        if (j > 0) {
            if (j < this.b) {
                this.b = j;
            } else if (this.b == 0) {
                this.b = j;
            }
        }
        this.e.removeCallbacks(this.f);
        if (this.b > 0) {
            this.e.postDelayed(this.f, this.b);
        }
    }

    public final void a(Object obj, Object obj2) {
        long currentTimeMillis = System.currentTimeMillis();
        synchronized (this.c) {
            this.c.put(obj, new f(this, obj2, currentTimeMillis));
        }
        a(180000);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x002b, code lost:
        r1 = (com.scoreloop.client.android.ui.a.f) r0.get();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0031, code lost:
        if (r1 == null) goto L_0x004c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0033, code lost:
        r0 = r4.c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0035, code lost:
        monitor-enter(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        r1.a(r2);
        r4.c.put(r5, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x003e, code lost:
        monitor-exit(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x003f, code lost:
        r4.d.remove(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x004c, code lost:
        r4.d.remove(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0051, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:?, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0021, code lost:
        r0 = (java.lang.ref.SoftReference) r4.d.get(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0029, code lost:
        if (r0 == null) goto L_0x0051;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.scoreloop.client.android.ui.a.f a(java.lang.Object r5) {
        /*
            r4 = this;
            long r2 = java.lang.System.currentTimeMillis()
            java.util.HashMap r1 = r4.c
            monitor-enter(r1)
            java.util.HashMap r0 = r4.c     // Catch:{ all -> 0x0046 }
            java.lang.Object r0 = r0.get(r5)     // Catch:{ all -> 0x0046 }
            com.scoreloop.client.android.ui.a.f r0 = (com.scoreloop.client.android.ui.a.f) r0     // Catch:{ all -> 0x0046 }
            if (r0 == 0) goto L_0x0020
            r0.a(r2)     // Catch:{ all -> 0x0046 }
            java.util.HashMap r2 = r4.c     // Catch:{ all -> 0x0046 }
            r2.remove(r5)     // Catch:{ all -> 0x0046 }
            java.util.HashMap r2 = r4.c     // Catch:{ all -> 0x0046 }
            r2.put(r5, r0)     // Catch:{ all -> 0x0046 }
            monitor-exit(r1)     // Catch:{ all -> 0x0046 }
        L_0x001f:
            return r0
        L_0x0020:
            monitor-exit(r1)     // Catch:{ all -> 0x0046 }
            java.util.concurrent.ConcurrentHashMap r0 = r4.d
            java.lang.Object r0 = r0.get(r5)
            java.lang.ref.SoftReference r0 = (java.lang.ref.SoftReference) r0
            if (r0 == 0) goto L_0x0051
            java.lang.Object r1 = r0.get()
            com.scoreloop.client.android.ui.a.f r1 = (com.scoreloop.client.android.ui.a.f) r1
            if (r1 == 0) goto L_0x004c
            java.util.HashMap r0 = r4.c
            monitor-enter(r0)
            r1.a(r2)     // Catch:{ all -> 0x0049 }
            java.util.HashMap r2 = r4.c     // Catch:{ all -> 0x0049 }
            r2.put(r5, r1)     // Catch:{ all -> 0x0049 }
            monitor-exit(r0)     // Catch:{ all -> 0x0049 }
            java.util.concurrent.ConcurrentHashMap r0 = r4.d
            r0.remove(r1)
            r0 = r1
            goto L_0x001f
        L_0x0046:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0046 }
            throw r0
        L_0x0049:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0049 }
            throw r1
        L_0x004c:
            java.util.concurrent.ConcurrentHashMap r1 = r4.d
            r1.remove(r0)
        L_0x0051:
            r0 = 0
            goto L_0x001f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.scoreloop.client.android.ui.a.m.a(java.lang.Object):com.scoreloop.client.android.ui.a.f");
    }
}
