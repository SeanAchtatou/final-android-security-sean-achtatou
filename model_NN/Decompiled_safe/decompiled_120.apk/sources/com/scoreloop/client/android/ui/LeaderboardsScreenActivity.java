package com.scoreloop.client.android.ui;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.ui.framework.ScreenActivity;

public class LeaderboardsScreenActivity extends ScreenActivity {
    public void onCreate(Bundle bundle) {
        Integer num;
        Integer num2;
        super.onCreate(bundle);
        c cVar = (c) f.a();
        Intent intent = getIntent();
        if (intent.hasExtra("mode")) {
            num = Integer.valueOf(intent.getIntExtra("mode", 0));
            Game game = cVar.m().getGame();
            Integer minMode = game.getMinMode();
            Integer maxMode = game.getMaxMode();
            if (!(minMode == null || maxMode == null || (num.intValue() >= minMode.intValue() && num.intValue() < maxMode.intValue()))) {
                Log.e("ScoreloopUI", "mode extra parameter on LeaderboardsScreenActivity is out of range [" + minMode + "," + maxMode + "[");
                finish();
                return;
            }
        } else {
            num = null;
        }
        if (intent.hasExtra("leaderboard")) {
            num2 = Integer.valueOf(intent.getIntExtra("leaderboard", 0));
            if (num2.intValue() < 0 || num2.intValue() > 3) {
                Log.e("ScoreloopUI", "leaderboard extra parameter on LeaderboardsScreenActivity is invalid");
                finish();
                return;
            }
        } else {
            num2 = null;
        }
        a(cVar.a(null, num, num2), bundle);
    }
}
