package com.scoreloop.client.android.ui.component.post;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import com.scoreloop.client.android.core.controller.MessageController;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.controller.RequestControllerException;
import com.scoreloop.client.android.core.controller.RequestControllerObserver;
import com.scoreloop.client.android.core.controller.SocialProviderController;
import com.scoreloop.client.android.core.controller.SocialProviderControllerObserver;
import com.scoreloop.client.android.core.model.Entity;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.SocialProvider;
import com.scoreloop.client.android.ui.framework.BaseActivity;
import java.util.HashMap;
import java.util.Map;
import org.anddev.andengine.R;

public class PostOverlayActivity extends Activity implements CompoundButton.OnCheckedChangeListener, RequestControllerObserver, SocialProviderControllerObserver {
    private static Entity a = null;
    /* access modifiers changed from: private */
    public final Map b = new HashMap();
    /* access modifiers changed from: private */
    public MessageController c;
    /* access modifiers changed from: private */
    public EditText d;
    private Button e;
    private Button f;
    private final Map g = new HashMap();
    private Handler h = new Handler();

    public static void a(Entity entity) {
        a = entity;
    }

    private void a(String str, int i) {
        SocialProvider socialProviderForIdentifier = SocialProvider.getSocialProviderForIdentifier(str);
        CheckBox checkBox = (CheckBox) findViewById(i);
        this.b.put(checkBox, socialProviderForIdentifier);
        this.g.put(socialProviderForIdentifier, checkBox);
        checkBox.setOnCheckedChangeListener(this);
    }

    /* access modifiers changed from: private */
    public void a(boolean z) {
        if (z) {
            showDialog(102);
        } else {
            dismissDialog(102);
        }
        boolean z2 = !z;
        this.f.setEnabled(z2);
        this.e.setEnabled(z2);
        this.d.setEnabled(z2);
        for (CheckBox enabled : this.b.keySet()) {
            enabled.setEnabled(z2);
        }
    }

    private Dialog a(int i) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(i));
        AlertDialog create = builder.create();
        create.getWindow().requestFeature(1);
        create.setCanceledOnTouchOutside(true);
        return create;
    }

    /* access modifiers changed from: protected */
    public Entity a() {
        return a;
    }

    /* access modifiers changed from: protected */
    public String b() {
        return "Achievement";
    }

    public void onBackPressed() {
    }

    public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
        if (z) {
            SocialProvider socialProvider = (SocialProvider) this.b.get((CheckBox) compoundButton);
            if (!socialProvider.isUserConnected(Session.getCurrentSession().getUser())) {
                a(true);
                this.h.post(new a(this, socialProvider));
            }
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.sl_post);
        Entity a2 = a();
        if (a2 == null || a2.getIdentifier() == null) {
            finish();
            return;
        }
        this.c = new MessageController(this);
        ((TextView) findViewById(R.id.sl_post_text)).setText(String.format(getString(R.string.sl_format_post), b()));
        this.e = (Button) findViewById(R.id.cancel_button);
        this.e.setOnClickListener(new c(this));
        this.d = (EditText) findViewById(R.id.message_edittext);
        this.f = (Button) findViewById(R.id.ok_button);
        this.f.setOnClickListener(new b(this));
        a(SocialProvider.FACEBOOK_IDENTIFIER, R.id.facebook_checkbox);
        a(SocialProvider.MYSPACE_IDENTIFIER, R.id.myspace_checkbox);
        a(SocialProvider.TWITTER_IDENTIFIER, R.id.twitter_checkbox);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i) {
        switch (i) {
            case 100:
                return a((int) R.string.sl_error_message_network);
            case 101:
                return a((int) R.string.sl_error_message_connect_failed);
            case 102:
                ProgressDialog progressDialog = new ProgressDialog(this);
                progressDialog.setCancelable(false);
                progressDialog.setMessage("progress");
                return progressDialog;
            default:
                return null;
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        return false;
    }

    public void requestControllerDidFail(RequestController requestController, Exception exc) {
        if (requestController == this.c) {
            a(false);
            for (SocialProvider a2 : SocialProvider.getSupportedProviders()) {
                a(a2);
            }
            if (!(exc instanceof RequestControllerException) || ((RequestControllerException) exc).getErrorCode() != 110) {
                showDialog(100);
            } else {
                showDialog(101);
            }
        }
    }

    public void requestControllerDidReceiveResponse(RequestController requestController) {
        if (requestController == this.c) {
            dismissDialog(102);
            BaseActivity.a(this, String.format(getResources().getString(R.string.sl_format_posted), b()));
            getApplicationContext();
            a();
            finish();
        }
    }

    public void socialProviderControllerDidCancel(SocialProviderController socialProviderController) {
        a(false);
        a(socialProviderController.getSocialProvider());
    }

    public void socialProviderControllerDidEnterInvalidCredentials(SocialProviderController socialProviderController) {
        a(false);
        a(socialProviderController.getSocialProvider());
        showDialog(101);
    }

    public void socialProviderControllerDidFail(SocialProviderController socialProviderController, Throwable th) {
        a(false);
        a(socialProviderController.getSocialProvider());
        showDialog(100);
    }

    public void socialProviderControllerDidSucceed(SocialProviderController socialProviderController) {
        a(false);
        a(socialProviderController.getSocialProvider());
    }

    private void a(SocialProvider socialProvider) {
        if (!socialProvider.isUserConnected(Session.getCurrentSession().getUser())) {
            ((CheckBox) this.g.get(socialProvider)).setChecked(false);
        }
    }
}
