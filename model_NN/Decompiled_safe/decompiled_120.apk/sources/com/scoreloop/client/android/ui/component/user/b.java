package com.scoreloop.client.android.ui.component.user;

import com.scoreloop.client.android.ui.component.a.c;
import com.scoreloop.client.android.ui.framework.y;
import org.anddev.andengine.R;

final class b implements c {
    private /* synthetic */ UserListActivity a;

    b(UserListActivity userListActivity) {
        this.a = userListActivity;
    }

    public final void a(int i) {
        this.a.n();
        this.a.a(h.LOAD_BUDDIES.ordinal(), y.SET);
        if (!this.a.o()) {
            this.a.d(this.a.getResources().getString(R.string.sl_format_one_friend_added));
        }
    }
}
