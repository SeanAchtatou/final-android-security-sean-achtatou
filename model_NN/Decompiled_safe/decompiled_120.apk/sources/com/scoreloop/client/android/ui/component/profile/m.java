package com.scoreloop.client.android.ui.component.profile;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.scoreloop.client.android.ui.framework.w;
import org.anddev.andengine.R;

public final class m extends w {
    private String b;
    private EditText c;
    private EditText d;
    private TextView e;

    public m(Context context, String str) {
        super(context);
        setCancelable(true);
        this.b = str;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        return R.layout.sl_dialog_profile_edit_initial;
    }

    public final void onClick(View view) {
        if (this.a != null) {
            switch (view.getId()) {
                case R.id.sl_button_ok /*2131361802*/:
                    this.a.a(this, 0);
                    return;
                case R.id.sl_button_cancel /*2131361806*/:
                    this.a.a(this, 1);
                    return;
                default:
                    return;
            }
        }
    }

    public final String b() {
        return this.c.getText().toString();
    }

    public final String c() {
        return this.d.getText().toString();
    }

    public final void a(String str) {
        this.e.setText(str);
    }

    /* access modifiers changed from: protected */
    public final void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        ((Button) findViewById(R.id.sl_button_ok)).setOnClickListener(this);
        ((Button) findViewById(R.id.sl_button_cancel)).setOnClickListener(this);
        ((TextView) findViewById(R.id.sl_user_profile_edit_initial_current_text)).setText(this.b);
        this.c = (EditText) findViewById(R.id.sl_user_profile_edit_initial_username_text);
        this.d = (EditText) findViewById(R.id.sl_user_profile_edit_initial_email_text);
        this.e = (TextView) findViewById(R.id.sl_dialog_hint);
    }
}
