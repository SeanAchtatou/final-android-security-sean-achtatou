package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.controller.AddressBook;
import com.scoreloop.client.android.core.controller.e;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.SocialProvider;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.RequestCompletionCallback;
import com.scoreloop.client.android.core.server.RequestMethod;
import com.scoreloop.client.android.core.server.Response;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class UsersController extends RequestController {
    private int c;
    private boolean d;
    private LoginSearchOperator e;
    private List f;
    private Integer g;

    public enum LoginSearchOperator {
        EXACT_MATCH,
        LIKE,
        PREFIX
    }

    class a extends Request {
        static final Object a = a.class;
        private User b;
        private Game c;

        public a(RequestCompletionCallback requestCompletionCallback, User user, Game game) {
            super(requestCompletionCallback);
            this.b = user;
            this.c = game;
            a(a);
        }

        public String a() {
            return String.format("/service/users/%s/games/%s/buddies", this.b.getIdentifier(), this.c.getIdentifier());
        }

        public JSONObject b() {
            JSONObject jSONObject = new JSONObject();
            try {
                JSONObject jSONObject2 = new JSONObject();
                SearchSpec searchSpec = new SearchSpec(new e("login", e.a.ASCENDING));
                jSONObject2.put("name", "UserGameBuddiesSearch");
                jSONObject2.put("definition", searchSpec.a());
                jSONObject.put("search_list", jSONObject2);
                return jSONObject;
            } catch (JSONException e) {
                throw new IllegalStateException("Invalid data", e);
            }
        }

        public RequestMethod c() {
            return RequestMethod.GET;
        }
    }

    class b extends d {
        public b(RequestCompletionCallback requestCompletionCallback, Game game, boolean z, int i, User user) {
            super(requestCompletionCallback, game, z, i, "#recommended_buddies", null);
            a(user);
        }

        public JSONObject b() {
            JSONObject jSONObject = new JSONObject();
            JSONObject jSONObject2 = new JSONObject();
            try {
                jSONObject2.put("user_id", this.d.getIdentifier());
                jSONObject.put("user", jSONObject2);
                JSONObject a = this.e.a();
                a.put("per_page", this.b);
                jSONObject.put("search_list", a);
                return jSONObject;
            } catch (JSONException e) {
                throw new IllegalStateException("Invalid data", e);
            }
        }
    }

    final class c {
        private String a;
        private SearchSpec b;

        c(String str, SearchSpec searchSpec) {
            this.a = str;
            this.b = searchSpec;
        }

        /* access modifiers changed from: package-private */
        public final JSONObject a() {
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put("name", this.a);
                if (this.b != null) {
                    jSONObject.put("definition", this.b.a());
                }
                return jSONObject;
            } catch (JSONException e) {
                throw new IllegalStateException("Invalid search spec data");
            }
        }

        /* access modifiers changed from: package-private */
        public final void a(String str) {
            if (this.b != null) {
                this.b.a(new c("reference_user_id", d.EXACT, str));
            }
        }
    }

    class d extends Request {
        protected final Game a;
        protected final int b;
        protected final boolean c;
        protected User d;
        protected c e;

        public d(RequestCompletionCallback requestCompletionCallback, Game game, boolean z, int i, String str, SearchSpec searchSpec) {
            super(requestCompletionCallback);
            this.a = game;
            this.c = z;
            this.b = i;
            this.e = new c(str, searchSpec);
        }

        public String a() {
            if (this.c) {
                return "/service/users";
            }
            return String.format("/service/games/%s/users", this.a.getIdentifier());
        }

        public void a(User user) {
            this.d = user;
        }

        public JSONObject b() {
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put("limit", this.b);
                if (this.d != null) {
                    this.e.a(this.d.getIdentifier());
                }
                jSONObject.put("search_list", this.e.a());
                return jSONObject;
            } catch (JSONException e2) {
                throw new IllegalStateException("Invalid data", e2);
            }
        }

        public RequestMethod c() {
            return RequestMethod.GET;
        }
    }

    public UsersController(RequestControllerObserver requestControllerObserver) {
        this(null, requestControllerObserver);
    }

    public UsersController(Session session, RequestControllerObserver requestControllerObserver) {
        super(session, requestControllerObserver);
        this.c = 25;
        this.d = true;
        this.e = LoginSearchOperator.PREFIX;
        this.f = Collections.emptyList();
        this.g = null;
    }

    private d a(String str, SearchSpec searchSpec) {
        if (str != null && searchSpec != null) {
            return new d(g(), getGame(), this.d, this.c, str, searchSpec);
        }
        throw new IllegalArgumentException("arguments must not be null");
    }

    private void a(List list, Integer num) {
        this.f = Collections.unmodifiableList(list);
        this.g = num;
    }

    private SearchSpec b() {
        SearchSpec searchSpec = new SearchSpec(new e("login", e.a.ASCENDING));
        if (!this.d) {
            searchSpec.a(new c("skills_game_id", d.IS, getGame().getIdentifier()));
        }
        return searchSpec;
    }

    /* access modifiers changed from: package-private */
    public boolean a(Request request, Response response) {
        int i = 0;
        if (response.f() != 200) {
            throw RequestControllerException.a(response);
        }
        ArrayList arrayList = new ArrayList();
        Integer num = null;
        if (request.l() == a.a) {
            JSONArray jSONArray = response.e().getJSONObject(User.a).getJSONArray("buddies");
            int length = jSONArray.length();
            while (i < length) {
                arrayList.add(new User(jSONArray.getJSONObject(i)));
                i++;
            }
        } else if (response.c()) {
            JSONArray d2 = response.d();
            int length2 = d2.length();
            while (i < length2) {
                arrayList.add(new User(d2.getJSONObject(i).getJSONObject(User.a)));
                i++;
            }
        } else {
            num = Integer.valueOf(response.e().getInt("users_count"));
        }
        a(arrayList, num);
        return true;
    }

    public int getCountOfUsers() {
        return this.g != null ? this.g.intValue() : getUsers().size();
    }

    public int getSearchLimit() {
        return this.c;
    }

    public LoginSearchOperator getSearchOperator() {
        return this.e;
    }

    public boolean getSearchesGlobal() {
        return this.d;
    }

    public List getUsers() {
        return this.f;
    }

    public boolean isMaxUserCount() {
        return getCountOfUsers() >= 999;
    }

    public boolean isOverLimit() {
        return this.g != null;
    }

    public void loadBuddies(User user, Game game) {
        if (user == null) {
            throw new IllegalArgumentException("The user parameter must not be null");
        } else if (game == null) {
            throw new IllegalArgumentException("The game paramter must not be null");
        } else {
            a aVar = new a(g(), user, game);
            a_();
            aVar.a(120000);
            b(aVar);
        }
    }

    public void loadRecommendedBuddies(int i) {
        b bVar = new b(g(), getGame(), this.d, i, i());
        a_();
        bVar.a(120000);
        b(bVar);
    }

    public void searchByEmail(String str) {
        if (str == null) {
            throw new IllegalArgumentException("Parameter anEmail cannot be null");
        }
        SearchSpec b2 = b();
        b2.a(new c("email", d.EXACT, str));
        d a2 = a("UserEMailSearch", b2);
        a_();
        b(a2);
    }

    public void searchByLocalAddressBook() {
        SearchSpec b2 = b();
        AddressBook a2 = AddressBook.a();
        a2.a(AddressBook.HashAlgorithm.MD5);
        a2.a("shwu2831j78s");
        List a3 = a2.a(h().d());
        if (a3.size() == 0) {
            a(Collections.emptyList(), (Integer) null);
            j();
            return;
        }
        if (a3.size() == 1) {
            b2.a(new c("email_digest", d.EQUALS_ANY, (String) a3.get(0)));
        } else {
            b2.a(new c("email_digest", d.EQUALS_ANY, a3));
        }
        d a4 = a("UserAddressBookSearch", b2);
        a_();
        b(a4);
    }

    public void searchByLogin(String str) {
        d dVar;
        if (str == null) {
            throw new IllegalArgumentException("Parameter aLogin cannot be null");
        }
        SearchSpec b2 = b();
        if (this.e != null) {
            switch (this.e) {
                case LIKE:
                    dVar = d.LIKE;
                    break;
                case EXACT_MATCH:
                    dVar = d.EXACT;
                    break;
                default:
                    dVar = d.BEGINS_WITH;
                    break;
            }
        } else {
            dVar = d.BEGINS_WITH;
        }
        b2.a(new c("login", dVar, str));
        d a2 = a("UserLoginSearch", b2);
        a_();
        b(a2);
    }

    public void searchBySocialProvider(SocialProvider socialProvider) {
        if (socialProvider == null) {
            throw new IllegalArgumentException("Parameter aSocialProvider cannot be null");
        }
        SearchSpec b2 = b();
        b2.a(new c("social_provider_id", d.EXACT, socialProvider.getIdentifier()));
        d a2 = a("#user_social_provider_search", b2);
        a2.a(i());
        a_();
        b(a2);
    }

    public void setSearchLimit(int i) {
        this.c = i;
    }

    public void setSearchOperator(LoginSearchOperator loginSearchOperator) {
        if (loginSearchOperator == null) {
            throw new IllegalArgumentException("Parameter aSearchOperator cannot be null");
        }
        this.e = loginSearchOperator;
    }

    public void setSearchesGlobal(boolean z) {
        if (getGame() != null || z) {
            this.d = z;
            return;
        }
        throw new IllegalArgumentException("cannot search not globally without game being set");
    }
}
