package com.scoreloop.client.android.core.model;

import android.content.Context;
import com.scoreloop.client.android.core.server.Server;
import com.scoreloop.client.android.core.util.SetterIntent;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;

public class Session {
    private static Session a;
    private final Device b;
    private Game c;
    private final SessionObserver d;
    private final Server e;
    private String f;
    private Challenge g;
    private List h;
    private State i;
    private final User j;
    private Context k = null;
    private String l;
    private long m;
    private long n;

    public enum State {
        AUTHENTICATED,
        AUTHENTICATING,
        TIMEOUT,
        DEVICE_KNOWN,
        DEVICE_UNKNOWN,
        DEVICE_VERIFIED,
        FAILED,
        INITIAL,
        READY
    }

    public Session(SessionObserver sessionObserver, Server server) {
        this.e = server;
        this.d = sessionObserver;
        this.h = new ArrayList();
        this.j = new User();
        this.b = new Device();
        this.i = State.INITIAL;
        this.m = 0;
        this.n = System.currentTimeMillis();
    }

    static void a(Session session) {
        a = session;
    }

    private boolean f() {
        if (this.m == 0) {
            return false;
        }
        return System.currentTimeMillis() - this.n > this.m;
    }

    public static Session getCurrentSession() {
        return a;
    }

    public Device a() {
        return this.b;
    }

    public void a(Context context) {
        this.k = context;
    }

    public void a(Game game) {
        this.c = game;
    }

    public void a(State state) {
        this.i = state;
    }

    public void a(List list) {
        this.h = list;
    }

    public void a(JSONObject jSONObject) {
        SetterIntent setterIntent = new SetterIntent();
        if (setterIntent.h(jSONObject, "direct_pay_url", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.f = (String) setterIntent.a();
        }
        if (setterIntent.h(jSONObject, "slapp_download_url", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.l = (String) setterIntent.a();
        }
        if (setterIntent.h(jSONObject, "characteristic", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            String str = (String) setterIntent.a();
            if (this.c != null) {
                this.c.a(str);
            }
        }
        if (setterIntent.h(jSONObject, "timeout", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            long parseLong = Long.parseLong((String) setterIntent.a());
            if (parseLong - 10 > 0) {
                this.m = (parseLong - 10) * 1000;
            }
        }
    }

    public Server b() {
        return this.e;
    }

    public State c() {
        if (f() && this.i != State.AUTHENTICATING) {
            this.i = State.TIMEOUT;
        }
        return this.i;
    }

    public Context d() {
        return this.k;
    }

    public void e() {
        this.n = System.currentTimeMillis();
    }

    public Money getBalance() {
        return getUser().c();
    }

    public Challenge getChallenge() {
        return this.g;
    }

    public List getChallengeStakes() {
        return this.h;
    }

    public Game getGame() {
        return this.c;
    }

    public String getPaymentUrl() {
        return this.f;
    }

    public List getScoreSearchLists() {
        return getUser().f();
    }

    public String getScoreloopAppDownloadUrl() {
        return this.l;
    }

    public User getUser() {
        return this.j;
    }

    public boolean isAuthenticated() {
        return this.i == State.AUTHENTICATED;
    }

    public boolean isOwnedByUser(User user) {
        if (user != null) {
            return getUser().equals(user);
        }
        throw new IllegalArgumentException("user argument must not be null");
    }

    public void setChallenge(Challenge challenge) {
        this.g = challenge;
    }
}
