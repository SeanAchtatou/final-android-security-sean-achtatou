package com.scoreloop.client.android.ui;

import android.util.Log;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.controller.RequestControllerObserver;
import com.scoreloop.client.android.core.controller.ScoreController;
import com.scoreloop.client.android.core.model.Score;
import java.util.ListIterator;

final class b implements RequestControllerObserver {
    private /* synthetic */ c a;
    private final /* synthetic */ ListIterator b;

    b(c cVar, ListIterator listIterator) {
        this.a = cVar;
        this.b = listIterator;
    }

    public final void requestControllerDidFail(RequestController requestController, Exception exc) {
        Log.w("ScoreloopUI", "failed to submit localScore: " + exc);
        a((ScoreController) requestController);
    }

    public final void requestControllerDidReceiveResponse(RequestController requestController) {
        a((ScoreController) requestController);
    }

    private void a(ScoreController scoreController) {
        if (this.b.hasNext()) {
            scoreController.submitScore((Score) this.b.next());
        } else {
            this.a.t();
        }
    }
}
