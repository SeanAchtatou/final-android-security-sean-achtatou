package com.scoreloop.client.android.ui.component.achievement;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ListAdapter;
import com.scoreloop.client.android.core.controller.AchievementsController;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.model.Achievement;
import com.scoreloop.client.android.ui.component.base.ComponentListActivity;
import com.scoreloop.client.android.ui.component.post.PostOverlayActivity;
import com.scoreloop.client.android.ui.framework.a;
import com.scoreloop.client.android.ui.framework.i;

public class AchievementListActivity extends ComponentListActivity {
    private AchievementsController a;

    public final /* bridge */ /* synthetic */ void a(a aVar) {
        d dVar = (d) aVar;
        Achievement g = dVar.g();
        if (dVar.b()) {
            getApplicationContext();
            Intent intent = new Intent(this, PostOverlayActivity.class);
            PostOverlayActivity.a(g);
            startActivity(intent);
        }
    }

    /* access modifiers changed from: private */
    public void a() {
        i t = t();
        t.clear();
        boolean G = G();
        for (Achievement dVar : this.a.getAchievements()) {
            t.add(new d(this, dVar, G));
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        a((ListAdapter) new i(this));
        if (G()) {
            this.a = ((a) k().a("achievementsEngine")).a();
        } else {
            this.a = new AchievementsController(E());
        }
    }

    public final void a(int i) {
        if (G()) {
            r();
            ((a) k().a("achievementsEngine")).a(true, new b(this));
            return;
        }
        b(this.a);
        this.a.setUser(F());
        this.a.loadAchievements();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        q();
    }

    public final void a(RequestController requestController) {
        a();
    }
}
