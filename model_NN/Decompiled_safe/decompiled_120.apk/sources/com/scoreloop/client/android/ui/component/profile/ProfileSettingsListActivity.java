package com.scoreloop.client.android.ui.component.profile;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.ListAdapter;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.controller.RequestControllerException;
import com.scoreloop.client.android.core.controller.RequestControllerObserver;
import com.scoreloop.client.android.core.controller.UserController;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.ui.component.base.ComponentListActivity;
import com.scoreloop.client.android.ui.framework.a;
import com.scoreloop.client.android.ui.framework.ag;
import com.scoreloop.client.android.ui.framework.ai;
import com.scoreloop.client.android.ui.framework.s;
import org.anddev.andengine.R;
import org.anddev.andengine.util.constants.TimeConstants;

public class ProfileSettingsListActivity extends ComponentListActivity implements DialogInterface.OnDismissListener, RequestControllerObserver {
    /* access modifiers changed from: private */
    public f a;
    /* access modifiers changed from: private */
    public f b;
    /* access modifiers changed from: private */
    public f c;
    /* access modifiers changed from: private */
    public f d;
    /* access modifiers changed from: private */
    public UserController e;
    private String f;
    private String g;
    private String h;
    /* access modifiers changed from: private */
    public String i;
    private n j;
    private boolean k = true;

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putString("restoreEmail", this.f);
        bundle.putString("errorTitle", this.g);
        bundle.putString("errorMessage", this.h);
        bundle.putString("hint", this.i);
        if (this.j != null) {
            bundle.putString("lastRequestType", this.j.toString());
        }
        bundle.putBoolean("lastUpdateError", this.k);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i2) {
        switch (i2) {
            case TimeConstants.MONTHSPERYEAR /*12*/:
                p pVar = new p(this, getString(R.string.sl_change_username), getString(R.string.sl_current), getString(R.string.sl_new), null);
                pVar.a((ag) new c(this));
                pVar.setOnDismissListener(this);
                return pVar;
            case 13:
                p pVar2 = new p(this, getString(R.string.sl_change_email), getString(R.string.sl_current), getString(R.string.sl_new), null);
                pVar2.a((ag) new e(this));
                pVar2.setOnDismissListener(this);
                return pVar2;
            case 14:
                m mVar = new m(this, Session.getCurrentSession().getUser().getLogin());
                mVar.a((ag) new b(this));
                mVar.setOnDismissListener(this);
                return mVar;
            case 15:
                q qVar = new q(this);
                qVar.setOnDismissListener(this);
                return qVar;
            case 16:
            default:
                return super.onCreateDialog(i2);
            case 17:
                p pVar3 = new p(this, getString(R.string.sl_merge_account_title), null, null, getString(R.string.sl_merge_account_description));
                pVar3.a((ag) new d(this));
                pVar3.setOnDismissListener(this);
                return pVar3;
        }
    }

    /* access modifiers changed from: protected */
    public void onPrepareDialog(int i2, Dialog dialog) {
        switch (i2) {
            case TimeConstants.MONTHSPERYEAR /*12*/:
                p pVar = (p) dialog;
                pVar.a(Session.getCurrentSession().getUser().getLogin());
                pVar.d(this.i);
                if (!this.k || !n.USERNAME.equals(this.j)) {
                    pVar.b();
                    break;
                }
            case 13:
                p pVar2 = (p) dialog;
                pVar2.d(this.i);
                pVar2.a(Session.getCurrentSession().getUser().getEmailAddress());
                if (!this.k || !n.EMAIL.equals(this.j)) {
                    pVar2.b();
                    break;
                }
            case 14:
                ((m) dialog).a(this.i);
                break;
            case 15:
            case 16:
            default:
                super.onPrepareDialog(i2, dialog);
                break;
            case 17:
                p pVar3 = (p) dialog;
                pVar3.d(this.i);
                if (!this.k || !n.MERGE_ACCOUNTS.equals(this.j)) {
                    pVar3.b();
                    break;
                }
        }
        if (dialog instanceof q) {
            q qVar = (q) dialog;
            qVar.b(this.h);
            qVar.a(this.g);
        } else if (dialog instanceof ai) {
            ai aiVar = (ai) dialog;
            aiVar.b(this.h);
            aiVar.a(this.g);
        }
    }

    public void onCreate(Bundle bundle) {
        if (bundle != null) {
            this.f = bundle.getString("restoreEmail");
            this.g = bundle.getString("errorTitle");
            this.h = bundle.getString("errorMessage");
            this.i = bundle.getString("hint");
            if (bundle.containsKey("lastRequestType")) {
                this.j = n.valueOf(bundle.getString("lastRequestType"));
            }
            if (bundle.containsKey("lastUpdateError")) {
                this.k = bundle.getBoolean("lastUpdateError");
            }
        }
        super.onCreate(bundle);
        User user = Session.getCurrentSession().getUser();
        this.e = new UserController(this);
        this.c = new f(this, getResources().getDrawable(R.drawable.sl_icon_change_picture), getString(R.string.sl_change_picture), getString(R.string.sl_change_picture_details));
        this.d = new f(this, getResources().getDrawable(R.drawable.sl_icon_change_username), getString(R.string.sl_change_username), user.getLogin());
        this.a = new f(this, getResources().getDrawable(R.drawable.sl_icon_change_email), getString(R.string.sl_change_email), user.getEmailAddress());
        this.b = new f(this, getResources().getDrawable(R.drawable.sl_icon_merge_account), getString(R.string.sl_merge_account_title), getString(R.string.sl_merge_account_subtitle));
        if (user.getLogin() == null || user.getEmailAddress() == null) {
            b(this.e);
            this.e.loadUser();
        } else {
            a((ListAdapter) new g(this, this));
        }
        s();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.client.android.ui.component.base.ComponentActivity.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.scoreloop.client.android.ui.component.profile.ProfileSettingsListActivity.a(com.scoreloop.client.android.ui.component.profile.ProfileSettingsListActivity, java.lang.Object):void
      com.scoreloop.client.android.ui.component.profile.ProfileSettingsListActivity.a(com.scoreloop.client.android.ui.component.profile.ProfileSettingsListActivity, java.lang.String):void
      com.scoreloop.client.android.ui.component.profile.ProfileSettingsListActivity.a(com.scoreloop.client.android.core.controller.RequestController, java.lang.Exception):void
      com.scoreloop.client.android.ui.component.base.ComponentActivity.a(com.scoreloop.client.android.core.controller.RequestController, java.lang.Exception):void
      com.scoreloop.client.android.ui.framework.BaseActivity.a(android.content.Context, java.lang.String):void
      com.scoreloop.client.android.ui.framework.BaseActivity.a(int, com.scoreloop.client.android.ui.framework.y):void
      com.scoreloop.client.android.ui.framework.BaseActivity.a(com.scoreloop.client.android.ui.framework.s, java.lang.String):void
      com.scoreloop.client.android.ui.framework.aj.a(com.scoreloop.client.android.ui.framework.s, java.lang.String):void
      com.scoreloop.client.android.ui.component.base.ComponentActivity.a(int, boolean):void */
    public final void a(a aVar) {
        this.i = null;
        if (aVar == this.d) {
            if (Session.getCurrentSession().getUser().getEmailAddress() == null) {
                a(14, true);
            } else {
                a(12, true);
            }
        } else if (aVar == this.c) {
            a(A().d(Session.getCurrentSession().getUser()));
        } else if (aVar == this.a) {
            a(13, true);
        } else if (aVar == this.b) {
            a(17, true);
        }
    }

    public final void a(int i2) {
        this.a.c(Session.getCurrentSession().getUser().getEmailAddress());
        this.d.c(Session.getCurrentSession().getUser().getLogin());
        if (t() != null) {
            t().notifyDataSetChanged();
        }
        D().c();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.client.android.ui.component.base.ComponentActivity.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.scoreloop.client.android.ui.component.profile.ProfileSettingsListActivity.a(com.scoreloop.client.android.ui.component.profile.ProfileSettingsListActivity, java.lang.Object):void
      com.scoreloop.client.android.ui.component.profile.ProfileSettingsListActivity.a(com.scoreloop.client.android.ui.component.profile.ProfileSettingsListActivity, java.lang.String):void
      com.scoreloop.client.android.ui.component.profile.ProfileSettingsListActivity.a(com.scoreloop.client.android.core.controller.RequestController, java.lang.Exception):void
      com.scoreloop.client.android.ui.component.base.ComponentActivity.a(com.scoreloop.client.android.core.controller.RequestController, java.lang.Exception):void
      com.scoreloop.client.android.ui.framework.BaseActivity.a(android.content.Context, java.lang.String):void
      com.scoreloop.client.android.ui.framework.BaseActivity.a(int, com.scoreloop.client.android.ui.framework.y):void
      com.scoreloop.client.android.ui.framework.BaseActivity.a(com.scoreloop.client.android.ui.framework.s, java.lang.String):void
      com.scoreloop.client.android.ui.framework.aj.a(com.scoreloop.client.android.ui.framework.s, java.lang.String):void
      com.scoreloop.client.android.ui.component.base.ComponentActivity.a(int, boolean):void */
    /* access modifiers changed from: protected */
    public final void a(RequestController requestController, Exception exc) {
        a((Object) requestController);
        if (!(exc instanceof RequestControllerException) || !((RequestControllerException) exc).hasDetail(16) || !n.MERGE_ACCOUNTS.equals(this.j)) {
            this.k = true;
            if (exc instanceof RequestControllerException) {
                RequestControllerException requestControllerException = (RequestControllerException) exc;
                if (requestControllerException.hasDetail(16)) {
                    this.g = getString(R.string.sl_error_message_email_already_taken_title);
                    this.h = getString(R.string.sl_error_message_email_already_taken);
                    a(15, true);
                } else if (requestControllerException.hasDetail(8)) {
                    this.i = getString(R.string.sl_error_message_invalid_email);
                    a(this.j.a(), true);
                } else {
                    if (requestControllerException.hasDetail(2) || (requestControllerException.hasDetail(1) | requestControllerException.hasDetail(4))) {
                        this.i = getString(R.string.sl_error_message_username_already_taken);
                        a(this.j.a(), true);
                    } else {
                        super.a(requestController, exc);
                    }
                }
            } else {
                super.a(requestController, exc);
            }
            B();
            a(this.j);
        } else {
            this.g = getString(R.string.sl_merge_account_success_title);
            this.h = getString(R.string.sl_merge_account_success);
            a(15, true);
            this.k = false;
            B();
            a(this.j);
        }
        Session.getCurrentSession().getUser().setEmailAddress(this.f);
    }

    private static String a(n nVar) {
        if (n.EMAIL.equals(nVar)) {
            return "result.change-email";
        }
        if (n.USERNAME.equals(nVar)) {
            return "result.change-username";
        }
        if (n.USERNAME_EMAIL.equals(nVar)) {
            return "result.change-username-firsttime";
        }
        if (n.MERGE_ACCOUNTS.equals(nVar)) {
            return "result.merge-account";
        }
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.client.android.ui.component.base.ComponentActivity.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.scoreloop.client.android.ui.component.profile.ProfileSettingsListActivity.a(com.scoreloop.client.android.ui.component.profile.ProfileSettingsListActivity, java.lang.Object):void
      com.scoreloop.client.android.ui.component.profile.ProfileSettingsListActivity.a(com.scoreloop.client.android.ui.component.profile.ProfileSettingsListActivity, java.lang.String):void
      com.scoreloop.client.android.ui.component.profile.ProfileSettingsListActivity.a(com.scoreloop.client.android.core.controller.RequestController, java.lang.Exception):void
      com.scoreloop.client.android.ui.component.base.ComponentActivity.a(com.scoreloop.client.android.core.controller.RequestController, java.lang.Exception):void
      com.scoreloop.client.android.ui.framework.BaseActivity.a(android.content.Context, java.lang.String):void
      com.scoreloop.client.android.ui.framework.BaseActivity.a(int, com.scoreloop.client.android.ui.framework.y):void
      com.scoreloop.client.android.ui.framework.BaseActivity.a(com.scoreloop.client.android.ui.framework.s, java.lang.String):void
      com.scoreloop.client.android.ui.framework.aj.a(com.scoreloop.client.android.ui.framework.s, java.lang.String):void
      com.scoreloop.client.android.ui.component.base.ComponentActivity.a(int, boolean):void */
    public final void a(RequestController requestController) {
        s y = y();
        y.b("userName", Session.getCurrentSession().getUser().getDisplayName());
        y.b("userImageUrl", Session.getCurrentSession().getUser().getImageUrl());
        a((ListAdapter) new g(this, this));
        a((Object) requestController);
        q();
        if (n.MERGE_ACCOUNTS.equals(this.j)) {
            this.i = getString(R.string.sl_merge_account_not_found);
            this.k = true;
            a(17, true);
            B();
            a(this.j);
        } else if (this.j != null) {
            this.k = false;
            B();
            a(this.j);
        }
    }

    static /* synthetic */ void a(ProfileSettingsListActivity profileSettingsListActivity, String str, String str2, n nVar) {
        profileSettingsListActivity.f = Session.getCurrentSession().getUser().getEmailAddress();
        User user = Session.getCurrentSession().getUser();
        if (str != null) {
            user.setEmailAddress(str);
        }
        if (str2 != null) {
            user.setLogin(str2);
        }
        profileSettingsListActivity.j = nVar;
        profileSettingsListActivity.l().post(new a(profileSettingsListActivity, user));
    }
}
