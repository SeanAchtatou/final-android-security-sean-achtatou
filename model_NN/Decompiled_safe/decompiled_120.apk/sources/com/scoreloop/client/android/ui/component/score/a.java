package com.scoreloop.client.android.ui.component.score;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import org.anddev.andengine.R;

public final class a extends com.scoreloop.client.android.ui.framework.a {
    public a(Context context) {
        super(context, null, null);
    }

    public final int a() {
        return 22;
    }

    public final View a(View view) {
        View view2;
        if (view == null) {
            view2 = e().inflate((int) R.layout.sl_list_item_score_submit_local, (ViewGroup) null);
        } else {
            view2 = view;
        }
        view2.setEnabled(false);
        ((Button) view2.findViewById(R.id.sl_submit_local_score_button)).setOnClickListener(new b(this, view2));
        return view2;
    }

    public final boolean b() {
        return true;
    }
}
