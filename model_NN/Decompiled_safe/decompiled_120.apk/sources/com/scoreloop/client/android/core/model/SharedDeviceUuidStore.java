package com.scoreloop.client.android.core.model;

import android.content.Context;
import com.scoreloop.client.android.core.util.FileStore;
import org.json.JSONObject;

public class SharedDeviceUuidStore extends FileStore {
    public SharedDeviceUuidStore() {
    }

    public SharedDeviceUuidStore(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public SharedDeviceUuid b(JSONObject jSONObject) {
        return new SharedDeviceUuid(jSONObject);
    }

    /* access modifiers changed from: protected */
    public String a() {
        return "edce0eda-3cc1-4144-b11f-c4bbe62da4a6";
    }

    /* access modifiers changed from: protected */
    public JSONObject a(SharedDeviceUuid sharedDeviceUuid) {
        return sharedDeviceUuid.b();
    }
}
