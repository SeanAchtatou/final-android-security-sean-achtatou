package com.scoreloop.client.android.ui.component.profile;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.scoreloop.client.android.ui.framework.w;
import org.anddev.andengine.R;

public final class p extends w {
    private final String b;
    private final String c;
    private final String d;
    private String e;
    private final String f;
    private String g;
    private String h;
    private EditText i;
    private TextView j;
    private TextView k;

    public p(Context context, String str, String str2, String str3, String str4) {
        super(context);
        setCancelable(true);
        this.b = str;
        this.d = str2;
        this.f = str3;
        this.c = str4;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        return R.layout.sl_dialog_profile_edit;
    }

    public final void onClick(View view) {
        if (this.a != null) {
            switch (view.getId()) {
                case R.id.sl_button_ok /*2131361802*/:
                    this.a.a(this, 0);
                    return;
                case R.id.sl_button_cancel /*2131361806*/:
                    this.a.a(this, 1);
                    return;
                default:
                    return;
            }
        }
    }

    public final void a(String str) {
        this.e = str;
        e();
    }

    public final void b() {
        this.g = null;
        e();
    }

    public final void d(String str) {
        this.h = str;
        e();
    }

    public final String c() {
        this.g = this.i.getText().toString();
        return this.i.getText().toString();
    }

    private void e() {
        if (this.i != null) {
            this.i.setText(this.g);
        }
        if (this.k != null) {
            this.k.setText(this.e);
        }
        if (this.j != null) {
            this.j.setVisibility(0);
            this.j.setText(this.h);
            return;
        }
        this.j.setVisibility(4);
    }

    /* access modifiers changed from: protected */
    public final void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        ((Button) findViewById(R.id.sl_button_ok)).setOnClickListener(this);
        ((Button) findViewById(R.id.sl_button_cancel)).setOnClickListener(this);
        ((TextView) findViewById(R.id.sl_title)).setText(this.b);
        TextView textView = (TextView) findViewById(R.id.sl_description);
        if (this.c != null) {
            textView.setText(this.c);
        } else {
            textView.setVisibility(4);
        }
        TextView textView2 = (TextView) findViewById(R.id.sl_user_profile_edit_current_label);
        this.k = (TextView) findViewById(R.id.sl_user_profile_edit_current_text);
        if (this.d != null) {
            textView2.setText(this.d);
        } else {
            textView2.setVisibility(4);
            this.k.setVisibility(4);
        }
        TextView textView3 = (TextView) findViewById(R.id.sl_user_profile_edit_new_label);
        if (this.f != null) {
            textView3.setText(this.f);
        } else {
            textView3.setVisibility(4);
        }
        this.i = (EditText) findViewById(R.id.sl_user_profile_edit_new_text);
        this.j = (TextView) findViewById(R.id.sl_dialog_hint);
        e();
    }
}
