package com.scoreloop.client.android.ui.component.profile;

import com.scoreloop.client.android.ui.framework.ag;
import com.scoreloop.client.android.ui.framework.w;
import java.util.regex.Pattern;
import org.anddev.andengine.R;

final class b implements ag {
    private /* synthetic */ ProfileSettingsListActivity a;

    b(ProfileSettingsListActivity profileSettingsListActivity) {
        this.a = profileSettingsListActivity;
    }

    public final void a(w wVar, int i) {
        m mVar = (m) wVar;
        if (i == 0) {
            String trim = mVar.c().trim();
            String trim2 = mVar.b().trim();
            if (!Pattern.compile(".+@.+\\.[a-z]+").matcher(trim).matches()) {
                mVar.a(this.a.getString(R.string.sl_please_email_address));
                return;
            }
            ProfileSettingsListActivity.a(this.a, trim, trim2, n.USERNAME_EMAIL);
        }
        wVar.dismiss();
    }
}
