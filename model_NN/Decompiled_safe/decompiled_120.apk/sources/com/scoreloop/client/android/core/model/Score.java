package com.scoreloop.client.android.core.model;

import com.scoreloop.client.android.core.util.JSONUtils;
import com.scoreloop.client.android.core.util.SetterIntent;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import org.anddev.andengine.level.util.constants.LevelConstants;
import org.json.JSONObject;

public class Score extends BaseEntity implements MessageTargetInterface {
    public static String a = "score";
    private String c;
    private Integer d;
    private Double e;
    private Integer f;
    private Double g;
    private Integer h;
    private String i = UUID.randomUUID().toString();
    private User j;
    private Map k;

    public Score(Double d2, Map map) {
        this.g = d2;
        if (map != null) {
            this.k = new HashMap(map);
            this.e = (Double) this.k.get(Game.CONTEXT_KEY_MINOR_RESULT);
            this.d = (Integer) this.k.get(Game.CONTEXT_KEY_LEVEL);
            this.f = (Integer) this.k.get(Game.CONTEXT_KEY_MODE);
            this.k.remove(Game.CONTEXT_KEY_MINOR_RESULT);
            this.k.remove(Game.CONTEXT_KEY_LEVEL);
            this.k.remove(Game.CONTEXT_KEY_MODE);
        }
    }

    public Score(JSONObject jSONObject) {
        a(jSONObject);
    }

    public static boolean areModesEqual(Score score, Score score2) {
        if (score != null && score2 != null) {
            return areModesEqual(score.getMode(), score2.getMode());
        }
        throw new IllegalArgumentException();
    }

    public static boolean areModesEqual(Integer num, Score score) {
        if (score != null) {
            return areModesEqual(num, score.getMode());
        }
        throw new IllegalArgumentException();
    }

    public static boolean areModesEqual(Integer num, Integer num2) {
        if (num != null) {
            return num.equals(num2);
        }
        throw new IllegalArgumentException();
    }

    public String a() {
        return a;
    }

    public void a(User user) {
        this.j = user;
    }

    public void a(Integer num) {
        this.h = num;
    }

    public void a(String str) {
        this.i = str;
    }

    public void a(JSONObject jSONObject) {
        super.a(jSONObject);
        SetterIntent setterIntent = new SetterIntent();
        if (setterIntent.h(jSONObject, "device_id", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.c = (String) setterIntent.a();
        }
        if (setterIntent.c(jSONObject, "result", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.g = (Double) setterIntent.a();
        }
        if (setterIntent.c(jSONObject, "minor_result", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.e = (Double) setterIntent.a();
        }
        if (setterIntent.d(jSONObject, LevelConstants.TAG_LEVEL, SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.d = (Integer) setterIntent.a();
        }
        if (setterIntent.d(jSONObject, "mode", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.f = (Integer) setterIntent.a();
        }
        if (setterIntent.f(jSONObject, User.a, SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.j = new User((JSONObject) setterIntent.a());
        }
        if (setterIntent.f(jSONObject, "context", SetterIntent.ValueMode.ALLOWS_NULL_VALUE)) {
            this.k = setterIntent.b().booleanValue() ? null : JSONUtils.a((JSONObject) setterIntent.a());
        }
    }

    public String c() {
        return this.i;
    }

    public JSONObject d() {
        JSONObject d2 = super.d();
        if (this.c == null && this.j != null) {
            this.c = this.j.e();
        }
        d2.put("device_id", this.c);
        d2.put("result", JSONUtils.a(this.g));
        d2.put(LevelConstants.TAG_LEVEL, this.d);
        if (this.j != null) {
            d2.put("user_id", this.j.getIdentifier());
        }
        d2.put("mode", this.f);
        d2.put("minor_result", JSONUtils.a(this.e));
        if (this.k != null) {
            d2.put("context", JSONUtils.a(this.k));
        }
        return d2;
    }

    public Map getContext() {
        return this.k;
    }

    public Integer getLevel() {
        return this.d;
    }

    public Double getMinorResult() {
        return this.e;
    }

    public Integer getMode() {
        return this.f;
    }

    public Integer getRank() {
        return this.h;
    }

    public Double getResult() {
        return this.g;
    }

    public User getUser() {
        return this.j;
    }

    public void setContext(Map map) {
        this.k = map;
    }

    public void setLevel(Integer num) {
        this.d = num;
    }

    public void setMinorResult(Double d2) {
        this.e = d2;
    }

    public void setMode(Integer num) {
        this.f = num;
    }

    public void setResult(Double d2) {
        this.g = d2;
    }
}
