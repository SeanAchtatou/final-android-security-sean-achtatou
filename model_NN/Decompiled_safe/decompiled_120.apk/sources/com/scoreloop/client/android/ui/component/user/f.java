package com.scoreloop.client.android.ui.component.user;

import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.ui.component.a.c;
import org.anddev.andengine.R;

final class f implements c {
    private /* synthetic */ UserHeaderActivity a;
    private final /* synthetic */ User b;

    f(UserHeaderActivity userHeaderActivity, User user) {
        this.a = userHeaderActivity;
        this.b = user;
    }

    public final void a(int i) {
        if (!this.a.o()) {
            this.a.d(String.format(this.a.getString(R.string.sl_format_removed_as_friend), this.b.getDisplayName()));
        }
    }
}
