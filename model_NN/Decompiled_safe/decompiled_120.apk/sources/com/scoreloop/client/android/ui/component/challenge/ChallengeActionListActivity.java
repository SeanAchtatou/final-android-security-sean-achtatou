package com.scoreloop.client.android.ui.component.challenge;

import android.app.Dialog;
import android.os.Bundle;
import android.widget.ListAdapter;
import com.scoreloop.client.android.ui.component.base.ComponentListActivity;
import com.scoreloop.client.android.ui.component.base.n;
import com.scoreloop.client.android.ui.framework.ag;
import com.scoreloop.client.android.ui.framework.i;
import com.scoreloop.client.android.ui.framework.v;
import org.anddev.andengine.R;

public abstract class ChallengeActionListActivity extends ComponentListActivity implements ag {
    /* access modifiers changed from: package-private */
    public abstract n a();

    /* access modifiers changed from: package-private */
    public abstract n b();

    /* access modifiers changed from: package-private */
    public abstract i c();

    /* access modifiers changed from: package-private */
    public abstract q d();

    /* access modifiers changed from: package-private */
    public final void g() {
        i iVar = new i(this);
        iVar.add(a());
        iVar.add(c());
        iVar.add(d());
        iVar.add(b());
        a((ListAdapter) iVar);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i) {
        switch (i) {
            case 5:
                v vVar = new v(this);
                vVar.b(getResources().getString(R.string.sl_error_message_challenge_ongoing));
                vVar.a((ag) this);
                return vVar;
            case 6:
                v vVar2 = new v(this);
                vVar2.b(getResources().getString(R.string.sl_error_message_challenge_game_not_ready));
                vVar2.a((ag) this);
                return vVar2;
            default:
                return super.onCreateDialog(i);
        }
    }

    /* access modifiers changed from: protected */
    public final boolean h() {
        if (D().b()) {
            c(5);
            return false;
        } else if (D().a()) {
            return true;
        } else {
            c(6);
            return false;
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }
}
