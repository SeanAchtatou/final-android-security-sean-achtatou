package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.model.Device;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.RequestCompletionCallback;
import com.scoreloop.client.android.core.server.RequestMethod;
import com.scoreloop.client.android.core.server.Response;
import com.scoreloop.client.android.core.util.Logger;
import java.nio.channels.IllegalSelectorException;
import org.anddev.andengine.entity.layer.tiled.tmx.util.constants.TMXConstants;
import org.anddev.andengine.input.touch.TouchEvent;
import org.json.JSONException;
import org.json.JSONObject;

class b extends RequestController {
    private Request c;
    private Device d;
    private Request e;

    /* renamed from: com.scoreloop.client.android.core.controller.b$1  reason: invalid class name */
    /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] a = new int[C0000b.values().length];

        static {
            try {
                a[C0000b.VERIFY.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                a[C0000b.CREATE.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                a[C0000b.RESET.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                a[C0000b.UPDATE.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
        }
    }

    class a extends Request {
        private final Device a;
        private final C0000b b;

        public a(RequestCompletionCallback requestCompletionCallback, Device device, C0000b bVar) {
            super(requestCompletionCallback);
            this.a = device;
            this.b = bVar;
        }

        public String a() {
            return "/service/device";
        }

        public JSONObject b() {
            JSONObject jSONObject = new JSONObject();
            try {
                switch (AnonymousClass1.a[this.b.ordinal()]) {
                    case 1:
                        jSONObject.put("uuid", this.a.h());
                        jSONObject.put("system", this.a.c());
                        break;
                    case 2:
                        jSONObject.put(Device.a, this.a.d());
                        break;
                    case TouchEvent.ACTION_CANCEL /*3*/:
                        JSONObject jSONObject2 = new JSONObject();
                        jSONObject2.put("uuid", this.a.h());
                        jSONObject2.put(TMXConstants.TAG_TILE_ATTRIBUTE_ID, this.a.getIdentifier());
                        jSONObject2.put("system", this.a.c());
                        jSONObject2.put("state", "freed");
                        jSONObject.put(Device.a, jSONObject2);
                        break;
                    case 4:
                        jSONObject.put(Device.a, this.a.d());
                        break;
                    default:
                        throw new IllegalSelectorException();
                }
                return jSONObject;
            } catch (JSONException e) {
                throw new IllegalStateException("Invalid device data", e);
            }
        }

        public RequestMethod c() {
            switch (AnonymousClass1.a[this.b.ordinal()]) {
                case 1:
                    return RequestMethod.GET;
                case 2:
                    return RequestMethod.POST;
                case TouchEvent.ACTION_CANCEL /*3*/:
                case 4:
                    return RequestMethod.PUT;
                default:
                    throw new IllegalSelectorException();
            }
        }

        public C0000b d() {
            return this.b;
        }
    }

    /* renamed from: com.scoreloop.client.android.core.controller.b$b  reason: collision with other inner class name */
    enum C0000b {
        CREATE(21),
        RESET(22),
        UPDATE(23),
        VERIFY(20);
        
        private final int a;

        private C0000b(int i) {
            this.a = i;
        }
    }

    b(Session session, RequestControllerObserver requestControllerObserver) {
        super(session, requestControllerObserver);
        this.d = session.a();
    }

    /* access modifiers changed from: package-private */
    public boolean a(Request request, Response response) {
        int f = response.f();
        JSONObject optJSONObject = response.e().optJSONObject(Device.a);
        if (((a) request).d() == C0000b.VERIFY) {
            if (f == 404) {
                return false;
            }
            if (this.c != null) {
                this.c.n();
            }
            this.c = null;
        }
        if ((f == 200 || f == 201) && optJSONObject != null) {
            this.d.b(optJSONObject.getString(TMXConstants.TAG_TILE_ATTRIBUTE_ID));
            if ("freed".equalsIgnoreCase(optJSONObject.optString("state"))) {
                this.d.a(Device.State.FREED);
            } else {
                this.d.a(f == 200 ? Device.State.VERIFIED : Device.State.CREATED);
            }
            return true;
        }
        throw new Exception("Request failed with status: " + f);
    }

    /* access modifiers changed from: protected */
    public void a_() {
        Logger.a("DeviceController", "reset()");
        super.a_();
        if (this.e != null) {
            if (!this.e.m()) {
                Logger.a("DeviceController", "reset() - canceling verify request");
                h().b().b(this.e);
            }
            this.e = null;
        }
    }

    /* access modifiers changed from: package-private */
    public Device b() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public void c() {
        a_();
        this.e = new a(g(), b(), C0000b.VERIFY);
        a(this.e);
        this.c = new a(g(), b(), C0000b.CREATE);
        a(this.c);
    }
}
