package com.scoreloop.client.android.ui.component.user;

import android.graphics.drawable.Drawable;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.ui.component.base.ComponentActivity;
import com.scoreloop.client.android.ui.component.base.k;
import org.anddev.andengine.R;

public final class g extends k {
    private final boolean a;

    public g(ComponentActivity componentActivity, Drawable drawable, User user, boolean z) {
        super(componentActivity, drawable, user.getDisplayName(), null, user);
        this.a = z;
    }

    /* access modifiers changed from: protected */
    public final String i() {
        return ((User) p()).getImageUrl();
    }

    /* access modifiers changed from: protected */
    public final int h() {
        return R.layout.sl_list_item_user;
    }

    public final int a() {
        return 24;
    }

    public final Drawable d() {
        return c().getResources().getDrawable(R.drawable.sl_icon_user);
    }

    public final boolean m() {
        return this.a;
    }
}
