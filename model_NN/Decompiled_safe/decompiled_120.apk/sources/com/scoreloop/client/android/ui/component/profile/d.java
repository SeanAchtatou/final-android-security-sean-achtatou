package com.scoreloop.client.android.ui.component.profile;

import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.ui.framework.ag;
import com.scoreloop.client.android.ui.framework.w;
import java.util.regex.Pattern;
import org.anddev.andengine.R;

final class d implements ag {
    private /* synthetic */ ProfileSettingsListActivity a;

    d(ProfileSettingsListActivity profileSettingsListActivity) {
        this.a = profileSettingsListActivity;
    }

    public final void a(w wVar, int i) {
        if (i == 0) {
            p pVar = (p) wVar;
            String trim = pVar.c().trim();
            if (!Pattern.compile(".+@.+\\.[a-z]+").matcher(trim).matches()) {
                this.a.i = this.a.getString(R.string.sl_please_email_valid);
                pVar.d(this.a.i);
                return;
            } else if (Session.getCurrentSession().getUser().getEmailAddress() == null || !Session.getCurrentSession().getUser().getEmailAddress().equalsIgnoreCase(trim)) {
                ProfileSettingsListActivity.a(this.a, trim, (String) null, n.MERGE_ACCOUNTS);
            } else {
                this.a.i = this.a.getString(R.string.sl_merge_account_email_current);
                pVar.d(this.a.i);
                return;
            }
        }
        wVar.dismiss();
    }
}
