package com.scoreloop.client.android.ui.framework;

public abstract class am {
    private static al a;

    public static al a() {
        if (a != null) {
            return a;
        }
        throw new IllegalStateException("you have to init the screen-manager-singleton first");
    }

    public static void a(al alVar) {
        if (a != null) {
            throw new IllegalStateException("ScreenManagerSingleton.init() can be called only once");
        }
        a = alVar;
    }
}
