package com.scoreloop.client.android.ui.framework;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import org.anddev.andengine.R;

public abstract class w extends Dialog implements View.OnClickListener {
    protected ag a;
    private String b;
    private String c;
    private TextView d;
    private Button e;
    private Object f;

    /* access modifiers changed from: protected */
    public abstract int a();

    protected w(Context context) {
        super(context, R.style.sl_dialog);
        setCancelable(false);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(a());
        this.d = (TextView) findViewById(R.id.sl_text);
        this.e = (Button) findViewById(R.id.sl_button_ok);
        b();
    }

    public final void a(ag agVar) {
        this.a = agVar;
    }

    public void b(String str) {
        this.b = str;
        b();
    }

    public final void c(String str) {
        this.c = str;
        b();
    }

    private void b() {
        if (this.d != null) {
            this.d.setText(this.b);
        }
        if (this.d != null && this.c != null) {
            this.e.setText(this.c);
        }
    }

    public final void a(Object obj) {
        this.f = obj;
    }

    public final Object d() {
        return this.f;
    }
}
