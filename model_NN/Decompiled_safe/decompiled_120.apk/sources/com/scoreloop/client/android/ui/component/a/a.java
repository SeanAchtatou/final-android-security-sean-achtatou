package com.scoreloop.client.android.ui.component.a;

import com.scoreloop.client.android.core.controller.AchievementsController;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.ui.framework.s;

public final class a extends b {
    public static final String[] a = {"numberAchievements", "numberAwards"};
    private AchievementsController b;

    public a() {
        super(a);
    }

    /* access modifiers changed from: protected */
    public final void b(s sVar) {
        a("numberAwards", Integer.valueOf(this.b.getAwardList().getAwards().size()));
        a("numberAchievements", Integer.valueOf(this.b.countAchievedAwards()));
    }

    /* access modifiers changed from: protected */
    public final void c(s sVar) {
        if (this.b == null) {
            this.b = new AchievementsController(this);
        }
        this.b.setUser((User) sVar.a("user"));
        this.b.loadAchievements();
    }
}
