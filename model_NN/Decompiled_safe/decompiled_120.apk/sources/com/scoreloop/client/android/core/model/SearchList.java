package com.scoreloop.client.android.core.model;

import com.scoreloop.client.android.core.util.SetterIntent;
import java.util.List;
import org.json.JSONObject;

public class SearchList extends BaseEntity {
    static SearchList a;
    public static String c = "search_list";
    private String d;

    public SearchList(String str) {
        super(str);
    }

    SearchList(String str, String str2) {
        this(str);
        this.d = str2;
    }

    public SearchList(JSONObject jSONObject) {
        a(jSONObject);
    }

    static SearchList a(String str) {
        List<SearchList> f;
        if (str == null) {
            throw new IllegalArgumentException();
        }
        Session currentSession = Session.getCurrentSession();
        if (!(currentSession == null || (f = currentSession.getUser().f()) == null)) {
            for (SearchList searchList : f) {
                if (str.equalsIgnoreCase(searchList.getIdentifier())) {
                    return searchList;
                }
            }
        }
        return new SearchList(str, "");
    }

    public static SearchList getBuddiesScoreSearchList() {
        return a("701bb990-80d8-11de-8a39-0800200c9a66");
    }

    public static SearchList getDefaultScoreSearchList() {
        List f;
        Session currentSession = Session.getCurrentSession();
        return (currentSession == null || (f = currentSession.getUser().f()) == null || f.size() <= 0) ? getGlobalScoreSearchList() : (SearchList) f.get(0);
    }

    public static SearchList getGlobalScoreSearchList() {
        return a("428a66d4-e6ca-4ff0-b7ea-f482ba4541a1");
    }

    public static SearchList getLocalScoreSearchList() {
        if (a == null) {
            a = new SearchList("#local");
        }
        return a;
    }

    public static SearchList getTwentyFourHourScoreSearchList() {
        return a("428a66d4-e6ca-4ff0-b7ea-f482ba4541a2");
    }

    public static SearchList getUserCountryLocationScoreSearchList() {
        return a("428a66d4-e6ca-4ff0-b7ea-f482ba4541a3");
    }

    public static SearchList getUserNationalityScoreSearchList() {
        return a("428a66d4-e6ca-4ff0-b7ea-f482ba4541a4");
    }

    public String a() {
        return c;
    }

    public void a(JSONObject jSONObject) {
        super.a(jSONObject);
        SetterIntent setterIntent = new SetterIntent();
        if (setterIntent.h(jSONObject, "name", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.d = (String) setterIntent.a();
        }
    }

    public JSONObject d() {
        JSONObject d2 = super.d();
        d2.put("name", getName());
        return d2;
    }

    public String getName() {
        return this.d;
    }

    public void setName(String str) {
        this.d = str;
    }

    public String toString() {
        return getName();
    }
}
