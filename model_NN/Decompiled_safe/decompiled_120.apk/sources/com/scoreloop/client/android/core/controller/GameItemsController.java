package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.model.GameItem;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.Response;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.json.JSONArray;

public class GameItemsController extends RequestController {
    private List c = Collections.emptyList();
    private List d = Collections.emptyList();
    private boolean e = false;

    public GameItemsController(Session session, RequestControllerObserver requestControllerObserver) {
        super(session, requestControllerObserver);
    }

    /* access modifiers changed from: package-private */
    public boolean a(Request request, Response response) {
        if (response.f() != 200 || response.e() == null) {
            throw new Exception("Request failed");
        }
        ArrayList arrayList = new ArrayList();
        JSONArray jSONArray = response.e().getJSONArray("items");
        int length = jSONArray.length();
        for (int i = 0; i < length; i++) {
            arrayList.add(new GameItem(jSONArray.getJSONObject(i).getJSONObject(GameItem.a)));
        }
        this.d = Collections.unmodifiableList(arrayList);
        return true;
    }
}
