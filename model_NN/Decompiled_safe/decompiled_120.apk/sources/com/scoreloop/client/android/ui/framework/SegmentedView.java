package com.scoreloop.client.android.ui.framework;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

public abstract class SegmentedView extends LinearLayout implements View.OnClickListener {
    protected int a = -1;
    public int b = -1;
    private View.OnClickListener c;

    /* access modifiers changed from: protected */
    public abstract void a(int i, boolean z);

    public SegmentedView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public final int b() {
        return this.a;
    }

    public void onClick(View view) {
        int childCount = getChildCount();
        int i = 0;
        while (i < childCount) {
            if (getChildAt(i) != view) {
                i++;
            } else if (i != this.a) {
                a(i);
                if (this.c != null) {
                    this.c.onClick(this);
                    return;
                }
                return;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
    }

    public final void c() {
        requestLayout();
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            getChildAt(i).setOnClickListener(this);
        }
        a();
    }

    public final void a(View.OnClickListener onClickListener) {
        this.c = onClickListener;
    }

    /* access modifiers changed from: protected */
    public final void b(int i) {
        if (this.a != -1) {
            a(this.a, false);
        }
        this.b = this.a;
        this.a = i;
        if (this.a != -1) {
            a(this.a, true);
        }
    }

    public void a(int i) {
        b(i);
    }
}
