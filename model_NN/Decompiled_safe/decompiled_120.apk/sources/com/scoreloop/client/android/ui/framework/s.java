package com.scoreloop.client.android.ui.framework;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public final class s {
    private final Map a = new HashMap();
    private an b;
    private Map c;
    private final Map d = new HashMap();
    private List e;

    public static String a(String... strArr) {
        if (strArr.length == 0) {
            return null;
        }
        if (strArr.length == 1) {
            return strArr[0];
        }
        StringBuilder sb = new StringBuilder();
        sb.append(strArr[0]);
        for (int i = 1; i < strArr.length; i++) {
            sb.append("/");
            sb.append(strArr[i]);
        }
        return sb.toString();
    }

    private static boolean c(String str) {
        return str.indexOf("/") == -1;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.client.android.ui.framework.s.a(java.lang.String, boolean):com.scoreloop.client.android.ui.framework.g
     arg types: [java.lang.String, int]
     candidates:
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, com.scoreloop.client.android.ui.framework.j):void
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, java.lang.Object):java.lang.Object
      com.scoreloop.client.android.ui.framework.s.a(com.scoreloop.client.android.ui.framework.s, java.util.Set):void
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, com.scoreloop.client.android.ui.framework.aj):void
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, boolean):com.scoreloop.client.android.ui.framework.g */
    public final void a(String str, aj ajVar) {
        ArrayList arrayList;
        aj ajVar2 = ajVar;
        String str2 = str;
        s sVar = this;
        while (!c(str2)) {
            g a2 = sVar.a(str2, true);
            sVar = a2.c;
            str2 = a2.b;
        }
        List list = (List) sVar.b().get(str2);
        if (list == null) {
            ArrayList arrayList2 = new ArrayList();
            sVar.c.put(str2, arrayList2);
            arrayList = arrayList2;
        } else {
            arrayList = list;
        }
        arrayList.add(new WeakReference(ajVar2));
    }

    public final void a(c... cVarArr) {
        Collections.addAll(c(), cVarArr);
    }

    /* access modifiers changed from: package-private */
    public final void a(s sVar, Set set) {
        Iterator it = set.iterator();
        while (it.hasNext()) {
            String str = (String) it.next();
            b(str, sVar.a(str, (Object) null));
        }
    }

    private void a(String str, j jVar) {
        List list = (List) b().get(str);
        if (list != null) {
            for (WeakReference weakReference : new ArrayList(list)) {
                aj ajVar = (aj) weakReference.get();
                if (ajVar != null) {
                    jVar.a(ajVar);
                } else {
                    list.remove(weakReference);
                }
            }
        }
    }

    private g a(String str, boolean z) {
        g gVar = new g();
        String[] split = str.split("/", 2);
        gVar.a = split[0];
        gVar.b = split[1];
        gVar.c = (s) this.d.get(gVar.a);
        if (gVar.c == null && z) {
            gVar.c = new s();
            this.d.put(gVar.a, gVar.c);
        }
        return gVar;
    }

    private Map b() {
        if (this.c == null) {
            this.c = new HashMap();
        }
        return this.c;
    }

    public final Object a(String str) {
        return a(str, (Object) null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.client.android.ui.framework.s.a(java.lang.String, boolean):com.scoreloop.client.android.ui.framework.g
     arg types: [java.lang.String, int]
     candidates:
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, com.scoreloop.client.android.ui.framework.j):void
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, java.lang.Object):java.lang.Object
      com.scoreloop.client.android.ui.framework.s.a(com.scoreloop.client.android.ui.framework.s, java.util.Set):void
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, com.scoreloop.client.android.ui.framework.aj):void
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, boolean):com.scoreloop.client.android.ui.framework.g */
    public final Object a(String str, Object obj) {
        Object obj2 = obj;
        String str2 = str;
        s sVar = this;
        while (!c(str2)) {
            g a2 = sVar.a(str2, false);
            if (a2.c == null) {
                return obj2;
            }
            sVar = a2.c;
            str2 = a2.b;
        }
        if (sVar.d.containsKey(str2)) {
            return sVar.d.get(str2);
        }
        return obj2;
    }

    private c d(String str) {
        HashSet hashSet = new HashSet();
        for (c cVar : c()) {
            cVar.a(hashSet);
            if (hashSet.contains(str)) {
                return cVar;
            }
        }
        if (this.b == null) {
            return null;
        }
        c a2 = this.b.a(str);
        if (a2 == null) {
            return a2;
        }
        a(a2);
        return a2;
    }

    private List c() {
        if (this.e == null) {
            this.e = new ArrayList();
        }
        return this.e;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.client.android.ui.framework.s.a(java.lang.String, com.scoreloop.client.android.ui.framework.j):void
     arg types: [java.lang.String, com.scoreloop.client.android.ui.framework.n]
     candidates:
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, boolean):com.scoreloop.client.android.ui.framework.g
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, java.lang.Object):java.lang.Object
      com.scoreloop.client.android.ui.framework.s.a(com.scoreloop.client.android.ui.framework.s, java.util.Set):void
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, com.scoreloop.client.android.ui.framework.aj):void
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, com.scoreloop.client.android.ui.framework.j):void */
    private void a(String str, Object obj, Object obj2) {
        a(str, (j) new n(this, str, obj, obj2));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.client.android.ui.framework.s.a(java.lang.String, com.scoreloop.client.android.ui.framework.j):void
     arg types: [java.lang.String, com.scoreloop.client.android.ui.framework.o]
     candidates:
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, boolean):com.scoreloop.client.android.ui.framework.g
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, java.lang.Object):java.lang.Object
      com.scoreloop.client.android.ui.framework.s.a(com.scoreloop.client.android.ui.framework.s, java.util.Set):void
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, com.scoreloop.client.android.ui.framework.aj):void
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, com.scoreloop.client.android.ui.framework.j):void */
    private void e(String str) {
        a(str, (j) new o(this, str));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.client.android.ui.framework.s.a(java.lang.String, boolean):com.scoreloop.client.android.ui.framework.g
     arg types: [java.lang.String, int]
     candidates:
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, com.scoreloop.client.android.ui.framework.j):void
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, java.lang.Object):java.lang.Object
      com.scoreloop.client.android.ui.framework.s.a(com.scoreloop.client.android.ui.framework.s, java.util.Set):void
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, com.scoreloop.client.android.ui.framework.aj):void
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, boolean):com.scoreloop.client.android.ui.framework.g */
    public final void b(String str, Object obj) {
        Object obj2 = obj;
        String str2 = str;
        s sVar = this;
        while (!c(str2)) {
            g a2 = sVar.a(str2, true);
            sVar = a2.c;
            str2 = a2.b;
        }
        Object obj3 = sVar.d.get(str2);
        sVar.d.put(str2, obj2);
        sVar.a.put(str2, new Date());
        if (obj3 != obj2) {
            sVar.a(str2, obj3, obj2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.client.android.ui.framework.s.a(java.lang.String, boolean):com.scoreloop.client.android.ui.framework.g
     arg types: [java.lang.String, int]
     candidates:
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, com.scoreloop.client.android.ui.framework.j):void
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, java.lang.Object):java.lang.Object
      com.scoreloop.client.android.ui.framework.s.a(com.scoreloop.client.android.ui.framework.s, java.util.Set):void
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, com.scoreloop.client.android.ui.framework.aj):void
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, boolean):com.scoreloop.client.android.ui.framework.g */
    public final void b(String str, aj ajVar) {
        aj ajVar2 = ajVar;
        String str2 = str;
        s sVar = this;
        while (!c(str2)) {
            g a2 = sVar.a(str2, false);
            if (a2.c != null) {
                sVar = a2.c;
                str2 = a2.b;
            } else {
                return;
            }
        }
        List list = (List) sVar.b().get(str2);
        if (list != null) {
            for (WeakReference weakReference : new ArrayList(list)) {
                aj ajVar3 = (aj) weakReference.get();
                if (ajVar3 == null || ajVar3 == ajVar2) {
                    list.remove(weakReference);
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.client.android.ui.framework.s.a(java.lang.String, boolean):com.scoreloop.client.android.ui.framework.g
     arg types: [java.lang.String, int]
     candidates:
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, com.scoreloop.client.android.ui.framework.j):void
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, java.lang.Object):java.lang.Object
      com.scoreloop.client.android.ui.framework.s.a(com.scoreloop.client.android.ui.framework.s, java.util.Set):void
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, com.scoreloop.client.android.ui.framework.aj):void
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, boolean):com.scoreloop.client.android.ui.framework.g */
    public final boolean a(String str, h hVar, Object obj) {
        boolean z;
        Object obj2 = obj;
        h hVar2 = hVar;
        String str2 = str;
        s sVar = this;
        while (!c(str2)) {
            g a2 = sVar.a(str2, false);
            if (a2.c == null) {
                return false;
            }
            sVar = a2.c;
            str2 = a2.b;
        }
        Date date = (Date) sVar.a.get(str2);
        if (hVar2 == h.NOT_DIRTY) {
            if (date == null) {
                z = false;
            }
            z = true;
        } else {
            if (hVar2 == h.NOT_OLDER_THAN) {
                if (date == null) {
                    z = false;
                } else {
                    Date date2 = new Date();
                    date2.setTime(date2.getTime() - ((Long) obj2).longValue());
                    z = !date.before(date2);
                }
            }
            z = true;
        }
        if (z) {
            return true;
        }
        c d2 = sVar.d(str2);
        if (d2 != null && !d2.a()) {
            d2.a(sVar);
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.client.android.ui.framework.s.a(java.lang.String, boolean):com.scoreloop.client.android.ui.framework.g
     arg types: [java.lang.String, int]
     candidates:
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, com.scoreloop.client.android.ui.framework.j):void
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, java.lang.Object):java.lang.Object
      com.scoreloop.client.android.ui.framework.s.a(com.scoreloop.client.android.ui.framework.s, java.util.Set):void
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, com.scoreloop.client.android.ui.framework.aj):void
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, boolean):com.scoreloop.client.android.ui.framework.g */
    /* access modifiers changed from: package-private */
    public final void a(s sVar, Set set, aj ajVar) {
        s sVar2;
        Object obj;
        Iterator it = set.iterator();
        while (it.hasNext()) {
            String str = (String) it.next();
            if (c(str)) {
                ajVar.a(this, str);
                if (sVar != null) {
                    obj = sVar.a(str, (Object) null);
                } else {
                    obj = null;
                }
                Object a2 = a(str, (Object) null);
                if (obj != a2) {
                    ajVar.a(this, str, obj, a2);
                }
            } else {
                g a3 = a(str, false);
                if (a3.c != null) {
                    if (sVar != null) {
                        sVar2 = (s) sVar.a(a3.a, (Object) null);
                    } else {
                        sVar2 = null;
                    }
                    a3.c.a(sVar2, Collections.singleton(a3.b), ajVar);
                }
            }
        }
    }

    public final void a() {
        this.a.clear();
        for (String str : this.d.keySet()) {
            Object obj = this.d.get(str);
            if (obj instanceof s) {
                ((s) obj).a();
            } else {
                e(str);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.client.android.ui.framework.s.a(java.lang.String, boolean):com.scoreloop.client.android.ui.framework.g
     arg types: [java.lang.String, int]
     candidates:
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, com.scoreloop.client.android.ui.framework.j):void
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, java.lang.Object):java.lang.Object
      com.scoreloop.client.android.ui.framework.s.a(com.scoreloop.client.android.ui.framework.s, java.util.Set):void
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, com.scoreloop.client.android.ui.framework.aj):void
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, boolean):com.scoreloop.client.android.ui.framework.g */
    public final void b(String str) {
        String str2 = str;
        s sVar = this;
        while (!c(str2)) {
            g a2 = sVar.a(str2, false);
            if (a2.c != null) {
                sVar = a2.c;
                str2 = a2.b;
            } else {
                return;
            }
        }
        sVar.a.put(str2, null);
        sVar.e(str2);
    }

    public final void a(an anVar) {
        this.b = anVar;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        sb.append(" [");
        boolean z = true;
        Iterator it = this.d.keySet().iterator();
        while (true) {
            boolean z2 = z;
            if (!it.hasNext()) {
                sb.append("]");
                return sb.toString();
            }
            String str = (String) it.next();
            if (!z2) {
                sb.append(", ");
            }
            sb.append(str);
            sb.append("=");
            Object obj = this.d.get(str);
            sb.append(obj != null ? obj.toString() : "NULL");
            if (this.a.get(str) == null) {
                sb.append("(*)");
            }
            z = false;
        }
    }
}
