package com.scoreloop.client.android.ui.framework;

public enum t {
    PAGE_TO_NEXT(2),
    PAGE_TO_OWN(3),
    PAGE_TO_PREV(1),
    PAGE_TO_RECENT(4),
    PAGE_TO_TOP(0);
    
    private int f;

    private t(int i) {
        this.f = 1 << i;
    }

    /* access modifiers changed from: package-private */
    public final int a(int i) {
        return this.f | i;
    }

    /* access modifiers changed from: package-private */
    public final boolean b(int i) {
        return (this.f & i) != 0;
    }
}
