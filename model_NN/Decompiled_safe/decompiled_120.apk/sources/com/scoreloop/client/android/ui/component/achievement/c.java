package com.scoreloop.client.android.ui.component.achievement;

final class c implements Runnable {
    private /* synthetic */ a a;
    private final /* synthetic */ boolean b;
    private final /* synthetic */ Runnable c;

    c(a aVar, boolean z, Runnable runnable) {
        this.a = aVar;
        this.b = z;
        this.c = runnable;
    }

    public final void run() {
        if (this.a.b()) {
            this.a.a(this.b, this.c);
            return;
        }
        if (this.c != null) {
            this.a.f.add(this.c);
        }
        this.a.d();
    }
}
