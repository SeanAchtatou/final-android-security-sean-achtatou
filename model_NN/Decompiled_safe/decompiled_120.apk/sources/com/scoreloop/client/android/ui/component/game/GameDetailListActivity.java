package com.scoreloop.client.android.ui.component.game;

import android.os.Bundle;
import android.widget.ListAdapter;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.controller.RequestControllerObserver;
import com.scoreloop.client.android.core.controller.UsersController;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.ui.component.base.ComponentListActivity;
import com.scoreloop.client.android.ui.component.base.h;
import com.scoreloop.client.android.ui.component.base.n;
import com.scoreloop.client.android.ui.framework.a;
import com.scoreloop.client.android.ui.framework.i;
import java.util.List;
import org.anddev.andengine.R;

public class GameDetailListActivity extends ComponentListActivity implements RequestControllerObserver {
    private UsersController a;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        a((ListAdapter) new a(this, this));
        this.a = new UsersController(this);
        this.a.loadBuddies(F(), C());
    }

    public final void a(int i) {
        super.a(i);
        t().notifyDataSetChanged();
    }

    public final void a(a aVar) {
        if (aVar.a() == 14) {
            a(A().a((User) ((g) aVar).p(), (Boolean) null));
        }
    }

    public final void a(RequestController requestController) {
        if (requestController == this.a) {
            List<User> users = this.a.getUsers();
            i t = t();
            t.add(new n(this, String.format(getString(R.string.sl_format_friends_playing), C().getName())));
            if (users.size() == 0) {
                t.add(new h(this, getString(R.string.sl_no_friends_playing)));
                return;
            }
            for (User gVar : users) {
                t.add(new g(this, gVar));
            }
        }
    }
}
