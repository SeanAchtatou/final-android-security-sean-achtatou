package com.scoreloop.client.android.ui.component.profile;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.scoreloop.client.android.ui.framework.w;
import org.anddev.andengine.R;

public final class q extends w {
    private String b;
    private String c;

    public q(Context context) {
        super(context);
        setCancelable(true);
    }

    /* access modifiers changed from: protected */
    public final int a() {
        return R.layout.sl_dialog_error;
    }

    public final void onClick(View view) {
        if (view.getId() == R.id.sl_button_ok) {
            dismiss();
        }
    }

    public final void a(String str) {
        this.b = str;
        b();
    }

    public final void b(String str) {
        this.c = str;
        b();
    }

    /* access modifiers changed from: protected */
    public final void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        ((Button) findViewById(R.id.sl_button_ok)).setOnClickListener(this);
        b();
    }

    private void b() {
        ((TextView) findViewById(R.id.sl_title)).setText(this.b);
        ((TextView) findViewById(R.id.sl_error_message)).setText(this.c);
    }
}
