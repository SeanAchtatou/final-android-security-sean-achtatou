package com.scoreloop.client.android.ui.component.user;

import android.app.Dialog;
import android.content.res.Resources;
import android.os.Bundle;
import android.widget.ListAdapter;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.controller.SocialProviderController;
import com.scoreloop.client.android.core.controller.SocialProviderControllerObserver;
import com.scoreloop.client.android.core.controller.UsersController;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.SocialProvider;
import com.scoreloop.client.android.ui.component.a.e;
import com.scoreloop.client.android.ui.component.base.ComponentListActivity;
import com.scoreloop.client.android.ui.component.base.a;
import com.scoreloop.client.android.ui.component.base.n;
import com.scoreloop.client.android.ui.component.base.o;
import com.scoreloop.client.android.ui.framework.i;
import java.util.List;
import org.anddev.andengine.R;
import org.anddev.andengine.util.Base64;

public class UserAddBuddyListActivity extends ComponentListActivity implements SocialProviderControllerObserver {
    private final Object a = new Object();
    private final Object b = new Object();
    private UsersController c;

    static /* synthetic */ void a(UserAddBuddyListActivity userAddBuddyListActivity, String str) {
        userAddBuddyListActivity.b(userAddBuddyListActivity.c);
        userAddBuddyListActivity.c.setSearchOperator(UsersController.LoginSearchOperator.EXACT_MATCH);
        userAddBuddyListActivity.c.searchByLogin(str);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        a((ListAdapter) new i(this));
        this.c = new UsersController(E());
        this.c.setSearchesGlobal(true);
        i t = t();
        Resources resources = getResources();
        x();
        t.clear();
        t.add(new n(this, getString(R.string.sl_add_friends)));
        t.add(new m(this, resources.getDrawable(R.drawable.sl_icon_facebook), getString(R.string.sl_facebook), SocialProvider.getSocialProviderForIdentifier(SocialProvider.FACEBOOK_IDENTIFIER)));
        t.add(new m(this, resources.getDrawable(R.drawable.sl_icon_twitter), getString(R.string.sl_twitter), SocialProvider.getSocialProviderForIdentifier(SocialProvider.TWITTER_IDENTIFIER)));
        t.add(new m(this, resources.getDrawable(R.drawable.sl_icon_myspace), getString(R.string.sl_myspace), SocialProvider.getSocialProviderForIdentifier(SocialProvider.MYSPACE_IDENTIFIER)));
        if (a.a(o.ADDRESS_BOOK)) {
            t.add(new m(this, resources.getDrawable(R.drawable.sl_icon_addressbook), getString(R.string.sl_addressbook), this.a));
        }
        t.add(new m(this, resources.getDrawable(R.drawable.sl_icon_scoreloop), getString(R.string.sl_scoreloop_username), this.b));
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i) {
        switch (i) {
            case Base64.Encoder.LINE_GROUPS /*19*/:
                k kVar = new k(this, this);
                kVar.setOnDismissListener(this);
                return kVar;
            default:
                return super.onCreateDialog(i);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.client.android.ui.component.base.ComponentActivity.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.scoreloop.client.android.ui.component.user.UserAddBuddyListActivity.a(com.scoreloop.client.android.ui.component.user.UserAddBuddyListActivity, java.lang.String):void
      com.scoreloop.client.android.ui.component.base.ComponentActivity.a(com.scoreloop.client.android.core.controller.RequestController, java.lang.Exception):void
      com.scoreloop.client.android.ui.framework.BaseActivity.a(android.content.Context, java.lang.String):void
      com.scoreloop.client.android.ui.framework.BaseActivity.a(int, com.scoreloop.client.android.ui.framework.y):void
      com.scoreloop.client.android.ui.framework.BaseActivity.a(com.scoreloop.client.android.ui.framework.s, java.lang.String):void
      com.scoreloop.client.android.ui.framework.aj.a(com.scoreloop.client.android.ui.framework.s, java.lang.String):void
      com.scoreloop.client.android.ui.component.base.ComponentActivity.a(int, boolean):void */
    public final void a(com.scoreloop.client.android.ui.framework.a aVar) {
        Object p = ((m) aVar).p();
        if (p == this.a) {
            b(this.c);
            this.c.searchByLocalAddressBook();
        } else if (p == this.b) {
            a(19, true);
        } else {
            SocialProvider socialProvider = (SocialProvider) p;
            if (socialProvider.isUserConnected(Session.getCurrentSession().getUser())) {
                b(this.c);
                this.c.searchBySocialProvider(socialProvider);
                return;
            }
            SocialProviderController.getSocialProviderController(null, this, socialProvider).connect(this);
        }
    }

    public final void a(RequestController requestController) {
        if (requestController != this.c) {
            return;
        }
        if (!this.c.isOverLimit()) {
            List users = this.c.getUsers();
            if (users.isEmpty()) {
                d(getResources().getString(R.string.sl_found_no_user));
                return;
            }
            r();
            e.a(this, users, z(), new a(this));
        } else if (this.c.isMaxUserCount()) {
            d(getResources().getString(R.string.sl_found_too_many_users));
        } else {
            d(String.format(getResources().getString(R.string.sl_format_found_many_users), Integer.valueOf(this.c.getCountOfUsers())));
        }
    }

    public void socialProviderControllerDidCancel(SocialProviderController socialProviderController) {
    }

    public void socialProviderControllerDidEnterInvalidCredentials(SocialProviderController socialProviderController) {
        c(0);
    }

    public void socialProviderControllerDidFail(SocialProviderController socialProviderController, Throwable th) {
        c(0);
    }

    public void socialProviderControllerDidSucceed(SocialProviderController socialProviderController) {
        SocialProvider socialProvider = socialProviderController.getSocialProvider();
        if (socialProvider.isUserConnected(Session.getCurrentSession().getUser())) {
            b(this.c);
            this.c.searchBySocialProvider(socialProvider);
        }
    }
}
