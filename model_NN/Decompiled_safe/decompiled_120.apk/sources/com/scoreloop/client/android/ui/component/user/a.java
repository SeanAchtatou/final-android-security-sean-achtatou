package com.scoreloop.client.android.ui.component.user;

import com.scoreloop.client.android.ui.component.a.c;
import org.anddev.andengine.R;

final class a implements c {
    private /* synthetic */ UserAddBuddyListActivity a;

    a(UserAddBuddyListActivity userAddBuddyListActivity) {
        this.a = userAddBuddyListActivity;
    }

    public final void a(int i) {
        this.a.n();
        if (!this.a.o()) {
            switch (i) {
                case 0:
                    this.a.d(this.a.getResources().getString(R.string.sl_found_no_user));
                    return;
                case 1:
                    this.a.d(this.a.getResources().getString(R.string.sl_format_one_friend_added));
                    return;
                default:
                    this.a.d(String.format(this.a.getResources().getString(R.string.sl_format_friends_added), Integer.valueOf(i)));
                    return;
            }
        }
    }
}
