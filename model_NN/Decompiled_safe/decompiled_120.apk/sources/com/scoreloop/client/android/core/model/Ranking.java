package com.scoreloop.client.android.core.model;

import com.scoreloop.client.android.core.util.SetterIntent;
import org.json.JSONObject;

public class Ranking {
    private Integer a;
    private Score b;
    private Integer c;

    public void a(JSONObject jSONObject) {
        SetterIntent setterIntent = new SetterIntent();
        this.a = setterIntent.a(jSONObject, "rank", SetterIntent.KeyMode.THROWS_WHEN_NO_KEY, SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE);
        this.c = setterIntent.a(jSONObject, "total", SetterIntent.KeyMode.THROWS_WHEN_NO_KEY, SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE);
        if (this.a.intValue() == 0) {
            this.a = null;
        }
        if (setterIntent.f(jSONObject, "score", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.b = new Score((JSONObject) setterIntent.a());
            this.b.a(this.a);
        }
    }

    public Integer getRank() {
        return this.a;
    }

    public Score getScore() {
        return this.b;
    }

    public Integer getTotal() {
        return this.c;
    }
}
