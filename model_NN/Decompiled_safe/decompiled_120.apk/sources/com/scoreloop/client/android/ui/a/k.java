package com.scoreloop.client.android.ui.a;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import java.io.IOException;
import java.io.InputStream;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public final class k {
    private static k a = null;
    private static m b = null;

    public static void a(String str, Drawable drawable, ImageView imageView) {
        if (str != null) {
            if (a == null) {
                a = new k();
            }
            if (b == null) {
                b = new m(150);
            }
            f a2 = b.a(str);
            if (a2 == null) {
                k kVar = a;
                if (a(str, imageView)) {
                    j jVar = new j(kVar, imageView);
                    imageView.setImageDrawable(new i(drawable, jVar));
                    jVar.execute(str);
                    return;
                }
                return;
            }
            a(str, imageView);
            imageView.setImageBitmap((Bitmap) a2.c());
        }
    }

    private static boolean a(String str, ImageView imageView) {
        j b2 = b(imageView);
        if (b2 != null) {
            String a2 = b2.a;
            if (a2 != null && a2.equals(str)) {
                return false;
            }
            b2.cancel(true);
        }
        return true;
    }

    /* access modifiers changed from: private */
    public static j b(ImageView imageView) {
        if (imageView != null) {
            Drawable drawable = imageView.getDrawable();
            if (drawable instanceof i) {
                return ((i) drawable).a();
            }
        }
        return null;
    }

    private static g a(String str) {
        InputStream inputStream;
        Throwable th;
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(str);
        try {
            HttpResponse execute = defaultHttpClient.execute(httpGet);
            int statusCode = execute.getStatusLine().getStatusCode();
            if (statusCode == 404) {
                return g.a();
            }
            if (statusCode != 200) {
                return g.b();
            }
            HttpEntity entity = execute.getEntity();
            if (entity != null) {
                try {
                    InputStream content = entity.getContent();
                    try {
                        g gVar = new g(BitmapFactory.decodeStream(new l(content)));
                        if (content != null) {
                            content.close();
                        }
                        entity.consumeContent();
                        return gVar;
                    } catch (Throwable th2) {
                        Throwable th3 = th2;
                        inputStream = content;
                        th = th3;
                        if (inputStream != null) {
                            inputStream.close();
                        }
                        entity.consumeContent();
                        throw th;
                    }
                } catch (Throwable th4) {
                    Throwable th5 = th4;
                    inputStream = null;
                    th = th5;
                }
            }
            return g.b();
        } catch (IOException e) {
            httpGet.abort();
        } catch (IllegalStateException e2) {
            httpGet.abort();
        } catch (Exception e3) {
            httpGet.abort();
        }
    }

    static g a(Context context, String str) {
        g a2 = a.a(context, str);
        if (a2 == null) {
            a2 = a(str);
            if (a2.d()) {
                a.a(context, str, a2);
            }
        }
        return a2;
    }
}
