package com.scoreloop.client.android.ui.a;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import java.io.File;
import java.io.FileOutputStream;

public final class a {
    static boolean a() {
        return Environment.getExternalStorageState().equals("mounted");
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0049  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.io.File b(android.content.Context r5, java.lang.String r6) {
        /*
            r4 = 0
            java.io.File r0 = android.os.Environment.getExternalStorageDirectory()
            if (r0 == 0) goto L_0x004b
            java.io.File r1 = new java.io.File
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "/Android/data/"
            r2.<init>(r3)
            java.lang.String r3 = r5.getPackageName()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = "/cache/"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r1.<init>(r0, r2)
            boolean r0 = r1.exists()
            if (r0 == 0) goto L_0x0031
            boolean r0 = r1.isDirectory()
            if (r0 != 0) goto L_0x0037
        L_0x0031:
            boolean r0 = r1.mkdirs()
            if (r0 == 0) goto L_0x004b
        L_0x0037:
            r0 = r1
        L_0x0038:
            if (r0 == 0) goto L_0x0049
            byte[] r1 = r6.getBytes()
            java.lang.String r1 = com.scoreloop.client.android.ui.a.e.a(r1)
            java.io.File r2 = new java.io.File
            r2.<init>(r0, r1)
            r0 = r2
        L_0x0048:
            return r0
        L_0x0049:
            r0 = r4
            goto L_0x0048
        L_0x004b:
            r0 = r4
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: com.scoreloop.client.android.ui.a.a.b(android.content.Context, java.lang.String):java.io.File");
    }

    public static g a(Context context, String str) {
        File b;
        if (!(a() || Environment.getExternalStorageState().equals("mounted_ro")) || (b = b(context, str)) == null || !b.exists() || !b.canRead()) {
            return null;
        }
        if (b.length() == 0) {
            return g.a();
        }
        return new g(BitmapFactory.decodeFile(b.getAbsolutePath()));
    }

    public static boolean a(Context context, String str, Bitmap bitmap) {
        return a(context, str, new g(bitmap));
    }

    public static boolean a(Context context, String str, g gVar) {
        if (a()) {
            File b = b(context, str);
            try {
                if (gVar.e()) {
                    b.createNewFile();
                    return true;
                }
                FileOutputStream fileOutputStream = new FileOutputStream(b);
                gVar.c().compress(Bitmap.CompressFormat.PNG, 90, fileOutputStream);
                fileOutputStream.close();
                return true;
            } catch (Exception e) {
            }
        }
        return false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x0031 A[SYNTHETIC, Splitter:B:25:0x0031] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.io.File a(android.content.Context r6, java.lang.String r7, java.io.InputStream r8) {
        /*
            r5 = 0
            boolean r0 = a()
            if (r0 == 0) goto L_0x002b
            java.io.File r0 = b(r6, r7)
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x003d, all -> 0x002d }
            r1.<init>(r0)     // Catch:{ Exception -> 0x003d, all -> 0x002d }
            r2 = 1024(0x400, float:1.435E-42)
            byte[] r2 = new byte[r2]     // Catch:{ Exception -> 0x0024, all -> 0x003b }
        L_0x0014:
            int r3 = r8.read(r2)     // Catch:{ Exception -> 0x0024, all -> 0x003b }
            r4 = -1
            if (r3 != r4) goto L_0x001f
            r1.close()     // Catch:{ IOException -> 0x0035 }
        L_0x001e:
            return r0
        L_0x001f:
            r4 = 0
            r1.write(r2, r4, r3)     // Catch:{ Exception -> 0x0024, all -> 0x003b }
            goto L_0x0014
        L_0x0024:
            r0 = move-exception
            r0 = r1
        L_0x0026:
            if (r0 == 0) goto L_0x002b
            r0.close()     // Catch:{ IOException -> 0x0037 }
        L_0x002b:
            r0 = r5
            goto L_0x001e
        L_0x002d:
            r0 = move-exception
            r1 = r5
        L_0x002f:
            if (r1 == 0) goto L_0x0034
            r1.close()     // Catch:{ IOException -> 0x0039 }
        L_0x0034:
            throw r0
        L_0x0035:
            r1 = move-exception
            goto L_0x001e
        L_0x0037:
            r0 = move-exception
            goto L_0x002b
        L_0x0039:
            r1 = move-exception
            goto L_0x0034
        L_0x003b:
            r0 = move-exception
            goto L_0x002f
        L_0x003d:
            r0 = move-exception
            r0 = r5
            goto L_0x0026
        */
        throw new UnsupportedOperationException("Method not decompiled: com.scoreloop.client.android.ui.a.a.a(android.content.Context, java.lang.String, java.io.InputStream):java.io.File");
    }
}
