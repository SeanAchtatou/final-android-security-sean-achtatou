package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.model.Achievement;
import com.scoreloop.client.android.core.model.AchievementsStore;
import com.scoreloop.client.android.core.model.Award;
import com.scoreloop.client.android.core.model.AwardList;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.core.persistence.DbAchievementsStore;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.RequestCompletionCallback;
import com.scoreloop.client.android.core.server.RequestMethod;
import com.scoreloop.client.android.core.server.Response;
import com.scoreloop.client.android.core.util.Logger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AchievementsController extends RequestController {
    private static String c = null;
    private List d;
    private AchievementsStore e;
    private AwardList f;
    private boolean g;
    private User h;

    final class a extends Request {
        private final AwardList a;
        private final Game b;
        private final User c;

        public a(RequestCompletionCallback requestCompletionCallback, User user, Game game, AwardList awardList) {
            super(requestCompletionCallback);
            this.c = user;
            this.b = game;
            this.a = awardList;
        }

        public final String a() {
            return String.format("/service/games/%s/achievements", this.b.getIdentifier());
        }

        public final JSONObject b() {
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put("user_id", this.c.getIdentifier());
                jSONObject.put("achievable_list_id", this.a != null ? this.a.a() : null);
                return jSONObject;
            } catch (JSONException e) {
                throw new IllegalStateException("Invalid achievement request", e);
            }
        }

        public final RequestMethod c() {
            return RequestMethod.GET;
        }
    }

    public AchievementsController(RequestControllerObserver requestControllerObserver) {
        this(null, requestControllerObserver);
    }

    public AchievementsController(Session session, RequestControllerObserver requestControllerObserver) {
        super(session, requestControllerObserver);
        this.d = Collections.emptyList();
        this.e = null;
        this.f = null;
        this.g = true;
        this.h = null;
        this.h = i();
    }

    private Achievement a(String str, String str2) {
        b();
        Award awardWithIdentifier = getAwardList().getAwardWithIdentifier(str);
        for (Achievement achievement : getAchievements()) {
            boolean equals = str2 == null ? achievement.getIdentifier() == null : str2.equals(achievement.getIdentifier());
            if (awardWithIdentifier.equals(achievement.getAward()) && equals) {
                return achievement;
            }
        }
        return null;
    }

    private void a(List list, List list2) {
        HashSet hashSet = new HashSet();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            hashSet.add(((Achievement) it.next()).getAward());
        }
        Iterator it2 = list2.iterator();
        while (it2.hasNext()) {
            Award award = (Award) it2.next();
            if (!hashSet.contains(award)) {
                list.add(new Achievement(award));
            }
        }
    }

    private void b() {
        if (getAwardList() == null) {
            throw new IllegalStateException("no SLAwards.bundle found in the assets");
        }
    }

    private void b(List list) {
        Achievement achievement;
        Iterator it = list.iterator();
        while (it.hasNext()) {
            Achievement achievement2 = (Achievement) it.next();
            Award award = achievement2.getAward();
            String identifier = award.getIdentifier();
            Achievement a2 = a(identifier, achievement2.getIdentifier());
            if (a2 != null || (achievement = a(identifier, (String) null)) == null || achievement.needsSubmit()) {
                achievement = a2;
            }
            if (achievement == null) {
                achievement = new Achievement(award, null, a());
            }
            achievement.a(achievement2, false);
        }
        a().b();
    }

    private void c() {
        ArrayList arrayList = new ArrayList();
        AwardList awardList = getAwardList();
        AchievementsStore a2 = a();
        for (Award award : awardList.getAwards()) {
            try {
                List a3 = a2.a(award);
                if (a3.size() > 0) {
                    arrayList.addAll(a3);
                } else {
                    arrayList.add(new Achievement(award, null, a2));
                }
            } catch (Exception e2) {
                e2.printStackTrace();
                Logger.b("can't create achievement from local store for award: " + award);
            }
        }
        a(arrayList);
    }

    private void k() {
        a aVar = new a(g(), getUser(), getGame(), getAwardList());
        aVar.a(120000);
        b(aVar);
    }

    /* access modifiers changed from: package-private */
    public AchievementsStore a() {
        if (this.e == null) {
            this.e = new DbAchievementsStore(h().d(), getAwardList(), getGame().getIdentifier());
        }
        return this.e;
    }

    /* access modifiers changed from: protected */
    public void a(List list) {
        this.d = Collections.unmodifiableList(list);
    }

    /* access modifiers changed from: package-private */
    public boolean a(Request request, Response response) {
        try {
            if (response.f() != 200) {
                throw new IllegalStateException("invalid response status: " + response.f());
            }
            JSONArray jSONArray = response.e().getJSONArray("achievements");
            AwardList awardList = getAwardList();
            List awards = awardList.getAwards();
            ArrayList arrayList = new ArrayList();
            int length = jSONArray.length();
            for (int i = 0; i < length; i++) {
                Achievement achievement = new Achievement(awardList, jSONArray.getJSONObject(i).getJSONObject(Achievement.a));
                if (awards.contains(achievement.getAward())) {
                    arrayList.add(achievement);
                }
            }
            if (h().isOwnedByUser(getUser())) {
                c();
                b(arrayList);
                return true;
            }
            a(arrayList, awards);
            a(arrayList);
            return true;
        } catch (Exception e2) {
            throw new IllegalStateException(e2);
        }
    }

    public int countAchievedAwards() {
        HashSet hashSet = new HashSet();
        for (Achievement achievement : getAchievements()) {
            if (achievement.isAchieved()) {
                hashSet.add(achievement.getAward());
            }
        }
        return hashSet.size();
    }

    public Achievement getAchievementForAwardIdentifier(String str) {
        b();
        Award awardWithIdentifier = getAwardList().getAwardWithIdentifier(str);
        for (Achievement achievement : getAchievements()) {
            if (awardWithIdentifier.equals(achievement.getAward())) {
                return achievement;
            }
        }
        return null;
    }

    public List getAchievements() {
        return this.d;
    }

    public AwardList getAwardList() {
        if (this.f != null) {
            return this.f;
        }
        if (this.f == null && c == null) {
            this.f = getGame().e();
        }
        if (this.f == null) {
            this.f = AwardList.a(h().d(), getGame().getIdentifier(), c);
            if (c == null) {
                getGame().a(this.f);
            }
        }
        return this.f;
    }

    public boolean getForceInitialSync() {
        return this.g;
    }

    public User getUser() {
        return this.h;
    }

    public boolean hadInitialSync() {
        return a().a();
    }

    public void loadAchievements() {
        b();
        a_();
        if (!h().isOwnedByUser(getUser()) || (this.g && !a().a())) {
            k();
            return;
        }
        c();
        j();
    }

    public void setForceInitialSync(boolean z) {
        this.g = z;
    }

    public void setUser(User user) {
        if (user == null) {
            throw new IllegalArgumentException("user must not be null");
        }
        this.h = user;
    }
}
