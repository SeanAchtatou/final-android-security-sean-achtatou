package com.scoreloop.client.android.ui;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import com.scoreloop.client.android.core.model.Challenge;
import com.scoreloop.client.android.core.model.Session;
import org.anddev.andengine.R;
import org.anddev.andengine.input.touch.TouchEvent;

public class ShowResultOverlayActivity extends Activity {
    public void onCreate(Bundle bundle) {
        String string;
        super.onCreate(bundle);
        setContentView((int) R.layout.sl_result);
        c cVar = (c) f.a();
        switch (cVar.l()) {
            case 1:
                string = getResources().getString(R.string.sl_status_success_score);
                break;
            case 2:
                string = getResources().getString(R.string.sl_status_success_local_score);
                break;
            case TouchEvent.ACTION_CANCEL /*3*/:
                Challenge j = cVar.j();
                if (!j.isOpen() && !j.isAssigned()) {
                    if (j.isComplete()) {
                        if (!Session.getCurrentSession().getUser().equals(j.getWinner())) {
                            string = getResources().getString(R.string.sl_status_success_challenge_lost);
                            break;
                        } else {
                            string = getResources().getString(R.string.sl_status_success_challenge_won);
                            break;
                        }
                    } else {
                        string = "";
                        break;
                    }
                } else {
                    string = getResources().getString(R.string.sl_status_success_challenge_created);
                    break;
                }
            case 4:
                string = getResources().getString(R.string.sl_status_error_network);
                break;
            case 5:
                string = getResources().getString(R.string.sl_status_error_balance);
                break;
            default:
                throw new IllegalStateException("this should not happen - make sure to start ShowResultOverlayActivity only after onScoreSubmit() was called");
        }
        ((TextView) findViewById(R.id.sl_text)).setText(string);
        ((Button) findViewById(R.id.sl_button)).setOnClickListener(new d(this));
    }
}
