package com.scoreloop.client.android.ui.component.score;

import android.os.Bundle;
import android.view.View;
import android.widget.ListAdapter;
import com.scoreloop.client.android.core.controller.RankingController;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.controller.ScoresController;
import com.scoreloop.client.android.core.model.Ranking;
import com.scoreloop.client.android.core.model.Score;
import com.scoreloop.client.android.core.model.SearchList;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.ui.component.base.ComponentListActivity;
import com.scoreloop.client.android.ui.component.base.e;
import com.scoreloop.client.android.ui.component.base.h;
import com.scoreloop.client.android.ui.component.base.j;
import com.scoreloop.client.android.ui.framework.a;
import com.scoreloop.client.android.ui.framework.ad;
import com.scoreloop.client.android.ui.framework.aj;
import com.scoreloop.client.android.ui.framework.ak;
import com.scoreloop.client.android.ui.framework.s;
import com.scoreloop.client.android.ui.framework.t;
import java.util.List;
import org.anddev.andengine.R;
import org.anddev.andengine.input.touch.TouchEvent;

public class ScoreListActivity extends ComponentListActivity implements ad, aj {
    private static /* synthetic */ int[] i;
    private int a = -1;
    /* access modifiers changed from: private */
    public int b;
    /* access modifiers changed from: private */
    public t c;
    private Ranking d;
    private RankingController e;
    private ScoresController f;
    private SearchList g;
    private a h;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.client.android.ui.component.base.e.a(com.scoreloop.client.android.core.model.User, java.lang.Boolean):com.scoreloop.client.android.ui.framework.u
     arg types: [com.scoreloop.client.android.core.model.User, int]
     candidates:
      com.scoreloop.client.android.ui.component.base.e.a(com.scoreloop.client.android.core.model.User, int):com.scoreloop.client.android.ui.framework.u
      com.scoreloop.client.android.ui.component.base.e.a(com.scoreloop.client.android.core.model.User, java.lang.Boolean):com.scoreloop.client.android.ui.framework.u */
    public final /* bridge */ /* synthetic */ void a(a aVar) {
        e A = A();
        User user = ((Score) ((h) aVar).p()).getUser();
        if (Session.getCurrentSession().isOwnedByUser(user)) {
            a(A.e(user));
        } else {
            a(A.a(user, (Boolean) true));
        }
    }

    private static /* synthetic */ int[] b() {
        int[] iArr = i;
        if (iArr == null) {
            iArr = new int[t.values().length];
            try {
                iArr[t.PAGE_TO_NEXT.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[t.PAGE_TO_OWN.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[t.PAGE_TO_PREV.ordinal()] = 3;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[t.PAGE_TO_RECENT.ordinal()] = 4;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[t.PAGE_TO_TOP.ordinal()] = 5;
            } catch (NoSuchFieldError e6) {
            }
            i = iArr;
        }
        return iArr;
    }

    static /* synthetic */ int c(ScoreListActivity scoreListActivity) {
        if (scoreListActivity.a == -1) {
            View a2 = ((h) ((ak) scoreListActivity.t()).b(scoreListActivity.b)).a((View) null);
            a2.measure(0, 0);
            scoreListActivity.a = (scoreListActivity.u().getHeight() - a2.getMeasuredHeight()) / 2;
        }
        return scoreListActivity.a;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        a((ListAdapter) new ak(this, (byte) 0));
        this.g = (SearchList) k().a("searchList", SearchList.getDefaultScoreSearchList());
        a("mode");
        if (this.g.equals(SearchList.getBuddiesScoreSearchList())) {
            a("numberBuddies");
        }
        this.f = new ScoresController(E());
        this.f.setRangeLength(j.a(u(), new h(this, new Score(Double.valueOf(0.0d), null), false)));
        this.f.setSearchList(this.g);
        if (a()) {
            this.h = new a(this);
            return;
        }
        this.e = new RankingController(E());
        this.e.setSearchList(this.g);
    }

    /* access modifiers changed from: protected */
    public final void b(a aVar) {
        if (aVar == this.h) {
            r();
            D().a(new g(this));
        } else if (aVar.a() == 21) {
            b(t.PAGE_TO_OWN);
        }
    }

    public final void a(t tVar) {
        b(tVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.client.android.ui.framework.s.a(java.lang.String, java.lang.Object):java.lang.Object
     arg types: [java.lang.String, int]
     candidates:
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, boolean):com.scoreloop.client.android.ui.framework.g
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, com.scoreloop.client.android.ui.framework.j):void
      com.scoreloop.client.android.ui.framework.s.a(com.scoreloop.client.android.ui.framework.s, java.util.Set):void
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, com.scoreloop.client.android.ui.framework.aj):void
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, java.lang.Object):java.lang.Object */
    public final void a(int i2) {
        b(this.f);
        this.f.setMode((Integer) m().a("mode"));
        switch (b()[this.c.ordinal()]) {
            case 1:
                this.f.loadNextRange();
                return;
            case 2:
                this.f.loadRangeForUser(Session.getCurrentSession().getUser());
                return;
            case TouchEvent.ACTION_CANCEL /*3*/:
                this.f.loadPreviousRange();
                return;
            case 4:
                this.f.loadRangeAtRank(((Integer) k().a("recentTopRank", (Object) 1)).intValue());
                return;
            case 5:
                this.f.loadRangeAtRank(1);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        b(t.PAGE_TO_RECENT);
    }

    public final void a(s sVar, String str, Object obj, Object obj2) {
        if (a(str, "mode", obj, obj2)) {
            if (obj == null) {
                b(t.PAGE_TO_RECENT);
            } else {
                b(t.PAGE_TO_TOP);
            }
        } else if (a(str, "numberBuddies", obj, obj2)) {
            b(t.PAGE_TO_TOP);
        }
    }

    public final void a(RequestController requestController) {
        ScoreListActivity scoreListActivity;
        boolean z;
        int rank;
        if (requestController == this.f) {
            ak akVar = (ak) t();
            akVar.clear();
            List scores = this.f.getScores();
            int size = scores.size();
            if (a()) {
                z = false;
            } else {
                z = true;
            }
            for (int i2 = 0; i2 < size; i2++) {
                Score score = (Score) scores.get(i2);
                if (this.g == SearchList.getLocalScoreSearchList() ? false : Session.getCurrentSession().isOwnedByUser(score.getUser())) {
                    this.b = i2;
                    akVar.add(new e(this, score, null));
                } else {
                    akVar.add(new h(this, score, z));
                }
            }
            if (size == 0) {
                akVar.add(new h(this, getString(R.string.sl_no_scores)));
                rank = 1;
            } else {
                rank = ((Score) scores.get(0)).getRank();
            }
            k().b("recentTopRank", rank);
            boolean hasPreviousRange = this.f.hasPreviousRange();
            akVar.a(hasPreviousRange, hasPreviousRange, this.f.hasNextRange());
            if (a()) {
                v();
                scoreListActivity = this;
            } else {
                this.e.loadRankingForUserInGameMode(F(), (Integer) m().a("mode"));
                return;
            }
        } else if (requestController == this.e) {
            ak akVar2 = (ak) t();
            this.d = this.e.getRanking();
            if (this.d.getRank() == null) {
                c(new d(this));
            } else if (this.b != -1) {
                ((e) akVar2.b(this.b)).a(this.d);
                akVar2.notifyDataSetChanged();
                scoreListActivity = this;
            } else {
                Score score2 = this.d.getScore();
                if (score2 != null) {
                    c(new e(this, score2, this.d));
                    scoreListActivity = this;
                }
            }
            scoreListActivity = this;
        } else {
            return;
        }
        scoreListActivity.u().post(new f(scoreListActivity));
    }

    /* access modifiers changed from: private */
    public void b(t tVar) {
        this.b = -1;
        this.c = tVar;
        v();
        q();
    }

    private boolean a() {
        return this.g == SearchList.getLocalScoreSearchList();
    }
}
