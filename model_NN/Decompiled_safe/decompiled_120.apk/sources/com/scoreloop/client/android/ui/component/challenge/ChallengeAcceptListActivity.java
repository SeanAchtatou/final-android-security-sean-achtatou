package com.scoreloop.client.android.ui.component.challenge;

import android.app.Dialog;
import android.os.Bundle;
import com.scoreloop.client.android.core.controller.ChallengeController;
import com.scoreloop.client.android.core.controller.ChallengeControllerObserver;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.model.Challenge;
import com.scoreloop.client.android.ui.component.a.d;
import com.scoreloop.client.android.ui.component.base.b;
import com.scoreloop.client.android.ui.component.base.m;
import com.scoreloop.client.android.ui.component.base.n;
import com.scoreloop.client.android.ui.framework.ag;
import com.scoreloop.client.android.ui.framework.ah;
import com.scoreloop.client.android.ui.framework.ai;
import com.scoreloop.client.android.ui.framework.aj;
import com.scoreloop.client.android.ui.framework.h;
import com.scoreloop.client.android.ui.framework.s;
import com.scoreloop.client.android.ui.framework.v;
import com.scoreloop.client.android.ui.framework.w;
import org.anddev.andengine.R;
import org.anddev.andengine.input.touch.TouchEvent;

public class ChallengeAcceptListActivity extends ChallengeActionListActivity implements ChallengeControllerObserver, j, ag {
    private Challenge a;
    private i b;
    private boolean c;
    private ai d;
    private ah e;
    private Runnable f;
    private s g;

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i) {
        switch (i) {
            case 1:
                v vVar = new v(this);
                vVar.b(getResources().getString(R.string.sl_error_message_challenge_balance));
                vVar.a((ag) this);
                vVar.setOnDismissListener(this);
                return vVar;
            case 2:
                v vVar2 = new v(this);
                vVar2.b(getResources().getString(R.string.sl_error_message_challenge_accept));
                vVar2.a((ag) this);
                vVar2.setOnDismissListener(this);
                return vVar2;
            case TouchEvent.ACTION_CANCEL /*3*/:
                v vVar3 = new v(this);
                vVar3.b(getResources().getString(R.string.sl_error_message_challenge_reject));
                vVar3.a((ag) this);
                vVar3.setOnDismissListener(this);
                return vVar3;
            case 4:
                ai aiVar = new ai(this);
                aiVar.b(getResources().getString(R.string.sl_leave_accept_challenge));
                aiVar.a((ag) this);
                aiVar.setOnDismissListener(this);
                return aiVar;
            default:
                return super.onCreateDialog(i);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.client.android.ui.component.base.ComponentActivity.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.scoreloop.client.android.ui.component.challenge.ChallengeAcceptListActivity.a(com.scoreloop.client.android.core.controller.RequestController, java.lang.Exception):void
      com.scoreloop.client.android.ui.component.challenge.ChallengeAcceptListActivity.a(com.scoreloop.client.android.ui.framework.s, java.lang.String):void
      com.scoreloop.client.android.ui.component.challenge.ChallengeAcceptListActivity.a(com.scoreloop.client.android.ui.framework.w, int):void
      com.scoreloop.client.android.ui.component.base.ComponentActivity.a(com.scoreloop.client.android.core.controller.RequestController, java.lang.Exception):void
      com.scoreloop.client.android.ui.framework.BaseActivity.a(android.content.Context, java.lang.String):void
      com.scoreloop.client.android.ui.framework.BaseActivity.a(int, com.scoreloop.client.android.ui.framework.y):void
      com.scoreloop.client.android.ui.framework.BaseActivity.a(com.scoreloop.client.android.ui.framework.s, java.lang.String):void
      com.scoreloop.client.android.ui.framework.aj.a(com.scoreloop.client.android.ui.framework.s, java.lang.String):void
      com.scoreloop.client.android.ui.framework.ag.a(com.scoreloop.client.android.ui.framework.w, int):void
      com.scoreloop.client.android.ui.framework.ag.a(com.scoreloop.client.android.ui.framework.w, int):void
      com.scoreloop.client.android.ui.component.base.ComponentActivity.a(int, boolean):void */
    public void challengeControllerDidFailOnInsufficientBalance(ChallengeController challengeController) {
        this.c = true;
        a(1, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.client.android.ui.component.base.ComponentActivity.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.scoreloop.client.android.ui.component.challenge.ChallengeAcceptListActivity.a(com.scoreloop.client.android.core.controller.RequestController, java.lang.Exception):void
      com.scoreloop.client.android.ui.component.challenge.ChallengeAcceptListActivity.a(com.scoreloop.client.android.ui.framework.s, java.lang.String):void
      com.scoreloop.client.android.ui.component.challenge.ChallengeAcceptListActivity.a(com.scoreloop.client.android.ui.framework.w, int):void
      com.scoreloop.client.android.ui.component.base.ComponentActivity.a(com.scoreloop.client.android.core.controller.RequestController, java.lang.Exception):void
      com.scoreloop.client.android.ui.framework.BaseActivity.a(android.content.Context, java.lang.String):void
      com.scoreloop.client.android.ui.framework.BaseActivity.a(int, com.scoreloop.client.android.ui.framework.y):void
      com.scoreloop.client.android.ui.framework.BaseActivity.a(com.scoreloop.client.android.ui.framework.s, java.lang.String):void
      com.scoreloop.client.android.ui.framework.aj.a(com.scoreloop.client.android.ui.framework.s, java.lang.String):void
      com.scoreloop.client.android.ui.framework.ag.a(com.scoreloop.client.android.ui.framework.w, int):void
      com.scoreloop.client.android.ui.framework.ag.a(com.scoreloop.client.android.ui.framework.w, int):void
      com.scoreloop.client.android.ui.component.base.ComponentActivity.a(int, boolean):void */
    public void challengeControllerDidFailToAcceptChallenge(ChallengeController challengeController) {
        this.c = true;
        a(2, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.client.android.ui.component.base.ComponentActivity.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.scoreloop.client.android.ui.component.challenge.ChallengeAcceptListActivity.a(com.scoreloop.client.android.core.controller.RequestController, java.lang.Exception):void
      com.scoreloop.client.android.ui.component.challenge.ChallengeAcceptListActivity.a(com.scoreloop.client.android.ui.framework.s, java.lang.String):void
      com.scoreloop.client.android.ui.component.challenge.ChallengeAcceptListActivity.a(com.scoreloop.client.android.ui.framework.w, int):void
      com.scoreloop.client.android.ui.component.base.ComponentActivity.a(com.scoreloop.client.android.core.controller.RequestController, java.lang.Exception):void
      com.scoreloop.client.android.ui.framework.BaseActivity.a(android.content.Context, java.lang.String):void
      com.scoreloop.client.android.ui.framework.BaseActivity.a(int, com.scoreloop.client.android.ui.framework.y):void
      com.scoreloop.client.android.ui.framework.BaseActivity.a(com.scoreloop.client.android.ui.framework.s, java.lang.String):void
      com.scoreloop.client.android.ui.framework.aj.a(com.scoreloop.client.android.ui.framework.s, java.lang.String):void
      com.scoreloop.client.android.ui.framework.ag.a(com.scoreloop.client.android.ui.framework.w, int):void
      com.scoreloop.client.android.ui.framework.ag.a(com.scoreloop.client.android.ui.framework.w, int):void
      com.scoreloop.client.android.ui.component.base.ComponentActivity.a(int, boolean):void */
    public void challengeControllerDidFailToRejectChallenge(ChallengeController challengeController) {
        this.c = true;
        a(3, true);
    }

    private void a(Runnable runnable) {
        if (this.d == null) {
            runnable.run();
        } else {
            this.f = runnable;
        }
    }

    /* access modifiers changed from: package-private */
    public final n a() {
        return new n(this, getString(R.string.sl_accept_challenge));
    }

    /* access modifiers changed from: package-private */
    public final n b() {
        return new n(this, this.a, this);
    }

    /* access modifiers changed from: package-private */
    public final i c() {
        if (this.b == null) {
            this.b = new i(this, this.a.getContender(), F());
        }
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public final q d() {
        return new q(this, this.a);
    }

    /* access modifiers changed from: protected */
    public void onPrepareDialog(int i, Dialog dialog) {
        switch (i) {
            case 4:
                ai aiVar = (ai) dialog;
                aiVar.a(this.e);
                this.d = aiVar;
                break;
        }
        super.onPrepareDialog(i, dialog);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.client.android.ui.component.base.ComponentActivity.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.scoreloop.client.android.ui.component.challenge.ChallengeAcceptListActivity.a(com.scoreloop.client.android.core.controller.RequestController, java.lang.Exception):void
      com.scoreloop.client.android.ui.component.challenge.ChallengeAcceptListActivity.a(com.scoreloop.client.android.ui.framework.s, java.lang.String):void
      com.scoreloop.client.android.ui.component.challenge.ChallengeAcceptListActivity.a(com.scoreloop.client.android.ui.framework.w, int):void
      com.scoreloop.client.android.ui.component.base.ComponentActivity.a(com.scoreloop.client.android.core.controller.RequestController, java.lang.Exception):void
      com.scoreloop.client.android.ui.framework.BaseActivity.a(android.content.Context, java.lang.String):void
      com.scoreloop.client.android.ui.framework.BaseActivity.a(int, com.scoreloop.client.android.ui.framework.y):void
      com.scoreloop.client.android.ui.framework.BaseActivity.a(com.scoreloop.client.android.ui.framework.s, java.lang.String):void
      com.scoreloop.client.android.ui.framework.aj.a(com.scoreloop.client.android.ui.framework.s, java.lang.String):void
      com.scoreloop.client.android.ui.framework.ag.a(com.scoreloop.client.android.ui.framework.w, int):void
      com.scoreloop.client.android.ui.framework.ag.a(com.scoreloop.client.android.ui.framework.w, int):void
      com.scoreloop.client.android.ui.component.base.ComponentActivity.a(int, boolean):void */
    /* access modifiers changed from: protected */
    public final boolean a(ah ahVar) {
        if (this.c) {
            return true;
        }
        this.e = ahVar;
        a(4, true);
        return false;
    }

    public final void a(w wVar, int i) {
        if (wVar == this.d) {
            this.d = null;
            if (i == 0) {
                wVar.dismiss();
                ((ah) wVar.d()).a();
                return;
            }
            wVar.dismiss();
            if (this.f != null) {
                Runnable runnable = this.f;
                this.f = null;
                runnable.run();
                return;
            }
            return;
        }
        wVar.dismiss();
        a((Runnable) new g(this));
    }

    public final void e() {
        if (h()) {
            this.c = false;
            this.a.setContestant(F());
            ChallengeController challengeController = new ChallengeController(this);
            b(challengeController);
            challengeController.setChallenge(this.a);
            challengeController.acceptChallenge();
        }
    }

    public final void f() {
        this.a.setContestant(F());
        ChallengeController challengeController = new ChallengeController(this);
        b(challengeController);
        challengeController.setChallenge(this.a);
        challengeController.rejectChallenge();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.client.android.ui.framework.s.a(java.lang.String, com.scoreloop.client.android.ui.framework.aj):void
     arg types: [java.lang.String, com.scoreloop.client.android.ui.component.challenge.ChallengeAcceptListActivity]
     candidates:
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, boolean):com.scoreloop.client.android.ui.framework.g
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, com.scoreloop.client.android.ui.framework.j):void
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, java.lang.Object):java.lang.Object
      com.scoreloop.client.android.ui.framework.s.a(com.scoreloop.client.android.ui.framework.s, java.util.Set):void
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, com.scoreloop.client.android.ui.framework.aj):void */
    public void onCreate(Bundle bundle) {
        this.e = (ah) k().a("navigationIntent", this.e);
        this.f = (Runnable) k().a("navigationDialogContinuation");
        Boolean bool = (Boolean) k().a("navigationAllowed", Boolean.TRUE);
        if (bool != null) {
            this.c = bool.booleanValue();
        }
        super.onCreate(bundle);
        a(s.a("userValues", "numberChallengesWon"));
        this.a = (Challenge) k().a("challenge", (Object) null);
        this.g = new s();
        this.g.b("user", this.a.getContender());
        this.g.a("numberChallengesWon", (aj) this);
        this.g.a(new d());
        q();
        g();
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        k().b("navigationIntent", this.e);
        k().b("navigationDialogContinuation", this.f);
        k().b("navigationAllowed", Boolean.valueOf(this.c));
    }

    public final void a(int i) {
        this.g.a("numberChallengesWon", h.NOT_DIRTY, (Object) null);
    }

    public void onResume() {
        super.onResume();
        this.c = true;
    }

    public final void a(s sVar, String str, Object obj, Object obj2) {
        if (sVar == this.g) {
            if (a(str, "numberChallengesWon", obj, obj2)) {
                c().b(m.b(this, this.g));
                t().notifyDataSetChanged();
            }
        } else if (a(str, "numberChallengesWon", obj, obj2)) {
            c().c(m.b(this, sVar));
            t().notifyDataSetChanged();
        }
    }

    public final void a(s sVar, String str) {
        if (sVar == this.g) {
            this.g.a("numberChallengesWon", h.NOT_DIRTY, (Object) null);
        } else if ("numberChallengesWon".equals(str)) {
            y().a(str, h.NOT_DIRTY, (Object) null);
        }
    }

    /* access modifiers changed from: protected */
    public final void a(RequestController requestController, Exception exc) {
        this.c = true;
        w();
    }

    public final void a(RequestController requestController) {
        this.c = true;
        Challenge challenge = ((ChallengeController) requestController).getChallenge();
        if (challenge.isAccepted()) {
            a((Runnable) new h(this));
        } else if (challenge.isRejected()) {
            i();
        } else {
            throw new IllegalStateException("this should not happen - illegal state of the accepted/rejected challenge");
        }
    }

    static /* synthetic */ void b(ChallengeAcceptListActivity challengeAcceptListActivity) {
        j();
        b D = challengeAcceptListActivity.D();
        challengeAcceptListActivity.a.getMode();
        D.a(challengeAcceptListActivity.a);
    }
}
