package com.scoreloop.client.android.core.persistence;

import android.content.Context;
import android.content.SharedPreferences;
import com.scoreloop.client.android.core.model.Achievement;
import com.scoreloop.client.android.core.model.AchievementsStore;
import com.scoreloop.client.android.core.model.Award;
import com.scoreloop.client.android.core.util.CryptoUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

class a implements AchievementsStore {
    private final Context a;
    private final String b;
    private final CryptoUtil c;

    a(Context context, String str) {
        this.a = context;
        this.b = str;
        this.c = new CryptoUtil(str, "com.scoreloop.achievements.store_");
    }

    private Achievement a(String str, Award award) {
        try {
            return new Achievement(award, new JSONObject(str), this);
        } catch (JSONException e) {
            return null;
        }
    }

    private String a(String str) {
        return str + "_";
    }

    private String a(String str, String str2) {
        return a(str) + str2;
    }

    private String b(Achievement achievement) {
        try {
            return achievement.a(true).toString();
        } catch (JSONException e) {
            throw new IllegalStateException();
        }
    }

    private String d() {
        return f() + "_data";
    }

    private String e() {
        return this.b;
    }

    private String f() {
        return "com.scoreloop.achievements.store_" + e();
    }

    public List a(Award award) {
        String identifier = award.getIdentifier();
        SharedPreferences sharedPreferences = this.a.getSharedPreferences(d(), 0);
        ArrayList arrayList = new ArrayList();
        Set<String> keySet = sharedPreferences.getAll().keySet();
        for (String next : keySet) {
            if (next.startsWith(a(identifier))) {
                arrayList.add(a(this.c.c(sharedPreferences.getString(next, null)), award));
            }
        }
        if (arrayList.size() == 0 && keySet.contains(identifier)) {
            arrayList.add(a(sharedPreferences.getString(identifier, null), award));
        }
        return arrayList;
    }

    public void a(Achievement achievement) {
        SharedPreferences.Editor edit = this.a.getSharedPreferences(d(), 0).edit();
        String a2 = this.c.a(b(achievement));
        String identifier = achievement.getAward().getIdentifier();
        if (!(achievement.e() == null || achievement.getIdentifier() == null)) {
            edit.remove(a(identifier, achievement.e()));
        }
        edit.putString(a(identifier, achievement.getIdentifier() != null ? achievement.getIdentifier() : achievement.e()), a2);
        edit.commit();
    }

    public boolean a() {
        return this.a.getSharedPreferences(f(), 0).getBoolean("did_query_server", false);
    }

    public void b() {
        SharedPreferences.Editor edit = this.a.getSharedPreferences(f(), 0).edit();
        edit.putBoolean("did_query_server", true);
        edit.commit();
    }

    /* access modifiers changed from: package-private */
    public void c() {
        for (String sharedPreferences : new String[]{f(), d()}) {
            SharedPreferences.Editor edit = this.a.getSharedPreferences(sharedPreferences, 0).edit();
            edit.clear();
            edit.commit();
        }
    }
}
