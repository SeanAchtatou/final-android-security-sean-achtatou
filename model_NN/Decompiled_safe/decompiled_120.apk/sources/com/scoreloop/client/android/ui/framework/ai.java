package com.scoreloop.client.android.ui.framework;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import org.anddev.andengine.R;

public final class ai extends w {
    private String b;

    public ai(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public final int a() {
        return R.layout.sl_dialog_ok_cancel;
    }

    public final void a(String str) {
        this.b = str;
        b();
    }

    public final void onClick(View view) {
        if (this.a != null) {
            if (view.getId() == R.id.sl_button_ok) {
                this.a.a(this, 0);
            } else if (view.getId() == R.id.sl_button_cancel) {
                this.a.a(this, 1);
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        ((Button) findViewById(R.id.sl_button_ok)).setOnClickListener(this);
        ((Button) findViewById(R.id.sl_button_cancel)).setOnClickListener(this);
        b();
    }

    private void b() {
        TextView textView = (TextView) findViewById(R.id.sl_title);
        if (this.b != null) {
            textView.setText(this.b);
            textView.setVisibility(0);
            return;
        }
        textView.setVisibility(4);
    }
}
