package com.scoreloop.client.android.ui.component.news;

import android.os.Bundle;
import com.scoreloop.client.android.ui.component.base.ComponentHeaderActivity;
import com.scoreloop.client.android.ui.component.base.m;
import com.scoreloop.client.android.ui.framework.h;
import com.scoreloop.client.android.ui.framework.s;
import org.anddev.andengine.R;

public class NewsHeaderActivity extends ComponentHeaderActivity {
    public void onCreate(Bundle bundle) {
        super.a(bundle, (int) R.layout.sl_header_default);
        a(C().getName());
        c(getString(R.string.sl_news));
        a().setImageDrawable(m.b(this, y(), true));
        a(s.a("userValues", "newsNumberUnreadItems"));
    }

    public final void a(s sVar, String str, Object obj, Object obj2) {
        b(m.d(this, y()));
        a().setImageDrawable(m.b(this, y(), true));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.client.android.ui.framework.s.a(java.lang.String, com.scoreloop.client.android.ui.framework.h, java.lang.Object):boolean
     arg types: [java.lang.String, com.scoreloop.client.android.ui.framework.h, long]
     candidates:
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, java.lang.Object, java.lang.Object):void
      com.scoreloop.client.android.ui.framework.s.a(com.scoreloop.client.android.ui.framework.s, java.util.Set, com.scoreloop.client.android.ui.framework.aj):void
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, com.scoreloop.client.android.ui.framework.h, java.lang.Object):boolean */
    public final void a(s sVar, String str) {
        if (str.equals("newsNumberUnreadItems")) {
            y().a(str, h.NOT_OLDER_THAN, (Object) 30000L);
        }
    }
}
