package com.scoreloop.client.android.ui.component.game;

import android.os.Bundle;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.scoreloop.client.android.core.controller.GamesController;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.controller.RequestControllerObserver;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.ui.component.base.ComponentListActivity;
import com.scoreloop.client.android.ui.component.base.h;
import com.scoreloop.client.android.ui.component.base.j;
import com.scoreloop.client.android.ui.component.base.k;
import com.scoreloop.client.android.ui.component.base.n;
import com.scoreloop.client.android.ui.framework.a;
import com.scoreloop.client.android.ui.framework.ad;
import com.scoreloop.client.android.ui.framework.ak;
import com.scoreloop.client.android.ui.framework.t;
import java.util.List;
import org.anddev.andengine.R;
import org.anddev.andengine.input.touch.TouchEvent;

public class GameListActivity extends ComponentListActivity implements RequestControllerObserver, ad {
    private static /* synthetic */ int[] d;
    private GamesController a;
    private int b;
    /* access modifiers changed from: private */
    public t c;

    public final /* bridge */ /* synthetic */ void a(a aVar) {
        a(A().a((Game) ((e) aVar).p()));
    }

    private static /* synthetic */ int[] a() {
        int[] iArr = d;
        if (iArr == null) {
            iArr = new int[t.values().length];
            try {
                iArr[t.PAGE_TO_NEXT.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[t.PAGE_TO_OWN.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[t.PAGE_TO_PREV.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[t.PAGE_TO_RECENT.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[t.PAGE_TO_TOP.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            d = iArr;
        }
        return iArr;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        a((ListAdapter) new ak(this));
        this.b = ((Integer) k().a("mode")).intValue();
        int a2 = j.a(u(), new k(this, null, "title", "subtitle", null));
        this.a = new GamesController(this);
        this.a.setRangeLength(a2);
        if (this.b == 3) {
            this.a.setLoadsDevicesPlatformOnly(false);
        }
        b(t.PAGE_TO_TOP);
    }

    private void b(t tVar) {
        this.c = tVar;
        q();
    }

    public final void a(int i) {
        b(this.a);
        switch (a()[this.c.ordinal()]) {
            case 1:
                this.a.loadNextRange();
                return;
            case 2:
            case 4:
            default:
                return;
            case TouchEvent.ACTION_CANCEL /*3*/:
                this.a.loadPreviousRange();
                return;
            case 5:
                switch (this.b) {
                    case 0:
                        b(this.a);
                        this.a.loadRangeForUser(F());
                        return;
                    case 1:
                        b(this.a);
                        this.a.loadRangeForPopular();
                        return;
                    case 2:
                        b(this.a);
                        this.a.loadRangeForNew();
                        return;
                    case TouchEvent.ACTION_CANCEL /*3*/:
                        b(this.a);
                        this.a.loadRangeForBuddies();
                        return;
                    default:
                        return;
                }
        }
    }

    public final void a(RequestController requestController) {
        int i;
        if (requestController == this.a) {
            List<Game> games = this.a.getGames();
            ak akVar = (ak) t();
            akVar.clear();
            switch (this.b) {
                case 0:
                    if (!G()) {
                        i = R.string.sl_games;
                        break;
                    } else {
                        i = R.string.sl_my_games;
                        break;
                    }
                case 1:
                    i = R.string.sl_popular_games;
                    break;
                case 2:
                    i = R.string.sl_new_games;
                    break;
                case TouchEvent.ACTION_CANCEL /*3*/:
                    i = R.string.sl_friends_games;
                    break;
                default:
                    i = R.string.sl_games;
                    break;
            }
            akVar.add(new n(this, getString(i)));
            for (Game eVar : games) {
                akVar.add(new e(this, getResources().getDrawable(R.drawable.sl_icon_games_loading), eVar));
            }
            if (games.size() == 0) {
                akVar.add(new h(this, getString(R.string.sl_no_games)));
                return;
            }
            boolean hasPreviousRange = this.a.hasPreviousRange();
            akVar.a(hasPreviousRange, hasPreviousRange, this.a.hasNextRange());
            ListView u = u();
            u.post(new f(this, u, akVar));
        }
    }

    public final void a(t tVar) {
        b(tVar);
    }
}
