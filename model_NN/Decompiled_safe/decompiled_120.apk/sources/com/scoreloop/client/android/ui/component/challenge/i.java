package com.scoreloop.client.android.ui.component.challenge;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.ui.a.k;
import com.scoreloop.client.android.ui.framework.a;
import org.anddev.andengine.R;

final class i extends a {
    private final User a;
    private String b;
    private final User c;
    private String d;

    public i(Context context, User user, User user2) {
        super(context, null, null);
        this.a = user;
        this.c = user2;
    }

    public final int a() {
        return 7;
    }

    public final View a(View view) {
        View view2;
        if (view == null) {
            view2 = e().inflate((int) R.layout.sl_list_item_challenge_participants, (ViewGroup) null);
        } else {
            view2 = view;
        }
        ImageView imageView = (ImageView) view2.findViewById(R.id.contender_icon);
        String imageUrl = this.a.getImageUrl();
        if (imageUrl != null) {
            k.a(imageUrl, g(), imageView);
        }
        ((TextView) view2.findViewById(R.id.contender_name)).setText(this.a.getDisplayName());
        ((TextView) view2.findViewById(R.id.contender_stats)).setText(this.b);
        ImageView imageView2 = (ImageView) view2.findViewById(R.id.contestant_icon);
        if (this.c != null) {
            String imageUrl2 = this.c.getImageUrl();
            if (imageUrl2 != null) {
                k.a(imageUrl2, g(), imageView2);
            }
        } else {
            imageView2.setImageDrawable(c().getResources().getDrawable(R.drawable.sl_icon_challenge_anyone));
        }
        ((TextView) view2.findViewById(R.id.contestant_name)).setText(this.c != null ? this.c.getDisplayName() : c().getResources().getString(R.string.sl_anyone));
        ((TextView) view2.findViewById(R.id.contestant_stats)).setText(this.d);
        return view2;
    }

    public final boolean b() {
        return false;
    }

    private Drawable g() {
        return c().getResources().getDrawable(R.drawable.sl_icon_games_loading);
    }

    public final void b(String str) {
        this.b = str;
    }

    public final void c(String str) {
        this.d = str;
    }
}
