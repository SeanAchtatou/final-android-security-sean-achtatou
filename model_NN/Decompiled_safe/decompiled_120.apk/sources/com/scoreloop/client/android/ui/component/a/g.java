package com.scoreloop.client.android.ui.component.a;

import com.scoreloop.client.android.core.controller.UserController;
import com.scoreloop.client.android.core.model.Entity;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.ui.framework.s;

public final class g extends b {
    public static final String[] a = {"userName", "userImageUrl", "userBalance", "numberGames", "numberBuddies", "numberGlobalAchievements"};
    private UserController b;

    public g() {
        super(a);
    }

    /* access modifiers changed from: protected */
    public final void b(s sVar) {
        User user = this.b.getUser();
        a("userName", user.getDisplayName());
        a("userImageUrl", user.getImageUrl());
        a("userBalance", Session.getCurrentSession().getBalance());
        a("numberGames", user.getGamesCounter());
        a("numberBuddies", user.getBuddiesCounter());
        a("numberGlobalAchievements", user.getGlobalAchievementsCounter());
    }

    /* access modifiers changed from: protected */
    public final void c(s sVar) {
        this.b = new UserController(this);
        this.b.setCachedResponseUsed(false);
        this.b.setUser((Entity) sVar.a("user"));
        this.b.loadUser();
    }

    public final void a(s sVar) {
        super.a(sVar);
        a("userName", this.b.getUser().getDisplayName());
    }
}
