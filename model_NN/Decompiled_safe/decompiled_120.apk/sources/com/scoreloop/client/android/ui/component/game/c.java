package com.scoreloop.client.android.ui.component.game;

import android.view.View;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.ui.component.base.d;

final class c implements View.OnClickListener {
    private /* synthetic */ GameDetailHeaderActivity a;
    private final /* synthetic */ Game b;

    c(GameDetailHeaderActivity gameDetailHeaderActivity, Game game) {
        this.a = gameDetailHeaderActivity;
        this.b = game;
    }

    public final void onClick(View view) {
        this.a.B();
        this.b.getName();
        d.b(this.a, this.b);
    }
}
