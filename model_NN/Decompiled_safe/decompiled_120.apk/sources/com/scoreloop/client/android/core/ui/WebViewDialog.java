package com.scoreloop.client.android.core.ui;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

public class WebViewDialog extends Dialog {
    private WebView a;
    private WebViewClient b;

    class a extends WebViewClient {
        private a() {
        }

        public void onLoadResource(WebView webView, String str) {
            WebViewDialog.this.a(webView, str);
        }
    }

    public WebViewDialog(Context context) {
        super(context);
        c();
    }

    public WebViewDialog(Context context, int i) {
        super(context, i);
        c();
    }

    private void c() {
        this.a = new WebView(getContext());
    }

    /* access modifiers changed from: protected */
    public View a(Context context, View view) {
        FrameLayout frameLayout = new FrameLayout(context);
        frameLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -1, 1.0f));
        Display defaultDisplay = getWindow().getWindowManager().getDefaultDisplay();
        frameLayout.setMinimumHeight(defaultDisplay.getHeight() - 90);
        frameLayout.setMinimumWidth(defaultDisplay.getWidth() - 20);
        frameLayout.addView(view);
        View zoomControls = this.a.getZoomControls();
        frameLayout.addView(zoomControls, new FrameLayout.LayoutParams(-1, -2, 80));
        zoomControls.setVisibility(8);
        return frameLayout;
    }

    public void a() {
        this.a.stopLoading();
    }

    /* access modifiers changed from: protected */
    public void a(WebSettings webSettings) {
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDefaultTextEncodingName("UTF-8");
    }

    /* access modifiers changed from: protected */
    public void a(WebView webView, String str) {
    }

    public void a(String str) {
        this.a.loadUrl(str);
    }

    public void b() {
        CookieSyncManager createInstance = CookieSyncManager.createInstance(getContext());
        CookieManager.getInstance().removeAllCookie();
        createInstance.sync();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onStart();
        Context context = getContext();
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(1);
        linearLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        setContentView(linearLayout);
        this.a.setLayoutParams(new LinearLayout.LayoutParams(-1, -1, 1.0f));
        this.b = new a();
        this.a.setWebViewClient(this.b);
        linearLayout.addView(a(context, this.a));
        a(this.a.getSettings());
    }
}
