package com.scoreloop.client.android.ui.framework;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;

public abstract class a {
    private final Context a;
    private Drawable b;
    private final LayoutInflater c = ((LayoutInflater) this.a.getSystemService("layout_inflater"));
    private String d;

    public abstract int a();

    public abstract View a(View view);

    public abstract boolean b();

    public a(Context context, Drawable drawable, String str) {
        this.a = context;
        this.b = drawable;
        this.d = str;
    }

    /* access modifiers changed from: protected */
    public final Context c() {
        return this.a;
    }

    public Drawable d() {
        return this.b;
    }

    /* access modifiers changed from: protected */
    public final LayoutInflater e() {
        return this.c;
    }

    public final void a(String str) {
        this.d = str;
    }

    public final String f() {
        return this.d;
    }

    public final void a(Drawable drawable) {
        this.b = drawable;
    }
}
