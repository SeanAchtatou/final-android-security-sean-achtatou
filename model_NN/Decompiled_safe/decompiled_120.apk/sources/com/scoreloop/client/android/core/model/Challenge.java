package com.scoreloop.client.android.core.model;

import com.scoreloop.client.android.core.util.JSONUtils;
import com.scoreloop.client.android.core.util.SetterIntent;
import java.util.Date;
import java.util.Map;
import org.anddev.andengine.entity.layer.tiled.tmx.util.constants.TMXConstants;
import org.anddev.andengine.level.util.constants.LevelConstants;
import org.json.JSONException;
import org.json.JSONObject;

public class Challenge extends BaseEntity implements MessageTargetInterface {
    public static String a = "challenge";
    private Date c;
    private User d;
    private Score e;
    private Integer f;
    private User g;
    private Score h;
    private Integer i;
    private Map j;
    private Date k;
    private String l;
    private Integer m;
    private User n;
    private Integer o;
    private Money p;
    private Money q;
    private Money r;
    private Money s;
    private Money t;
    private String u;
    private User v;

    public Challenge(Money money) {
        this.r = money;
        this.u = "created";
    }

    public Challenge(JSONObject jSONObject) {
        a(jSONObject);
    }

    private void a(Money money) {
        this.r = money;
    }

    private String c() {
        return this.u;
    }

    public String a() {
        return a;
    }

    public void a(Score score) {
        if (this.d == null || this.d.equals(score.getUser())) {
            if (isOpen()) {
                throw new IllegalStateException("Can not modify a already open challenge");
            } else if (isComplete()) {
                throw new IllegalStateException("Can not modify a already completed challenge");
            } else {
                this.d = score.getUser();
                this.e = score;
                this.u = "open";
            }
        } else if (this.g != null && !this.g.equals(score.getUser())) {
            throw new IllegalStateException("Can not change already assigned contender or contestant");
        } else if (isCreated() || isOpen() || isAccepted()) {
            this.g = score.getUser();
            this.h = score;
            this.u = "complete";
        } else {
            throw new IllegalStateException("Can not submit a score for a non-open challenge");
        }
    }

    public void a(User user, boolean z) {
        if (!isOpen() && !isAssigned()) {
            if (isAccepted() && (!isAccepted() || !z)) {
                throw new IllegalStateException("Can not accept a rejected challenge");
            } else if (isRejected() && (!isRejected() || z)) {
                throw new IllegalStateException("Can not reject a accepted challenge");
            }
        }
        setContestant(user);
        a(z ? "accepted" : "rejected");
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        this.u = str;
    }

    public void a(JSONObject jSONObject) {
        super.a(jSONObject);
        SetterIntent setterIntent = new SetterIntent();
        if (setterIntent.h(jSONObject, "state", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.u = (String) setterIntent.a();
        }
        if (setterIntent.d(jSONObject, LevelConstants.TAG_LEVEL, SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.m = (Integer) setterIntent.a();
        }
        if (setterIntent.d(jSONObject, "mode", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.o = (Integer) setterIntent.a();
        }
        if (setterIntent.h(jSONObject, "game_id", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.l = (String) setterIntent.a();
        }
        if (setterIntent.h(jSONObject, "contender_id", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.d = new User();
            this.d.b((String) setterIntent.a());
        }
        if (setterIntent.f(jSONObject, "contender", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.d = new User((JSONObject) setterIntent.a());
        }
        if (setterIntent.h(jSONObject, "contestant_id", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.g = new User();
            this.g.b((String) setterIntent.a());
        }
        if (setterIntent.f(jSONObject, "contestant", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.g = new User((JSONObject) setterIntent.a());
        }
        if (setterIntent.f(jSONObject, "winner", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            if (this.d == null || this.g == null) {
                throw new JSONException("winner present but missing contender or contestant");
            } else if (setterIntent.d((JSONObject) setterIntent.a(), TMXConstants.TAG_TILE_ATTRIBUTE_ID, SetterIntent.KeyMode.COERCE_NULL_WHEN_NO_KEY, SetterIntent.ValueMode.ALLOWS_AND_COERCES_NULL_VALUE).equals(this.d.getIdentifier())) {
                this.v = this.d;
                this.n = this.g;
            } else {
                this.v = this.g;
                this.n = this.d;
            }
        }
        if (setterIntent.f(jSONObject, "contender_score", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.e = new Score((JSONObject) setterIntent.a());
        }
        if (setterIntent.f(jSONObject, "contestant_score", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.h = new Score((JSONObject) setterIntent.a());
        }
        if (setterIntent.d(jSONObject, "contender_skill_value", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.f = (Integer) setterIntent.a();
        }
        if (setterIntent.d(jSONObject, "contestant_skill_value", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.i = (Integer) setterIntent.a();
        }
        if (setterIntent.f(jSONObject, "stake", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.r = new Money((JSONObject) setterIntent.a());
        }
        if (setterIntent.f(jSONObject, "price", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.p = new Money((JSONObject) setterIntent.a());
        }
        if (setterIntent.f(jSONObject, "stake_in_local_currency", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.t = new Money((JSONObject) setterIntent.a());
        } else if (this.r != null) {
            this.t = this.r.clone();
        } else {
            this.r = null;
        }
        if (setterIntent.f(jSONObject, "stake_in_contestant_currency", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.s = new Money((JSONObject) setterIntent.a());
        }
        if (setterIntent.f(jSONObject, "price_in_contestant_currency", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.q = new Money((JSONObject) setterIntent.a());
        }
        if (setterIntent.b(jSONObject, "created_at", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.k = (Date) setterIntent.a();
        }
        if (setterIntent.b(jSONObject, "completed_at", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.c = (Date) setterIntent.a();
        }
        if (setterIntent.f(jSONObject, "context", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.j = JSONUtils.a((JSONObject) setterIntent.a());
        }
    }

    public JSONObject d() {
        JSONObject d2 = super.d();
        d2.put("state", this.u);
        d2.put(LevelConstants.TAG_LEVEL, this.m);
        d2.put("mode", this.o);
        d2.put("game_id", this.l);
        if (this.d != null) {
            d2.put("contender_id", this.d.getIdentifier());
        }
        if (this.v != null) {
            d2.put("winner_id", this.v.getIdentifier());
        }
        if (this.n != null) {
            d2.put("looser_id", this.n.getIdentifier());
        }
        if (this.r != null) {
            d2.put("stake", this.r.c());
        }
        if (this.g != null) {
            String identifier = this.g.getIdentifier();
            if (identifier != null) {
                d2.put("contestant_id", identifier);
            } else {
                d2.put("contestant", this.g.d());
            }
        }
        if (this.h != null) {
            String identifier2 = this.h.getIdentifier();
            if (identifier2 != null) {
                d2.put("contestant_score_id", identifier2);
            } else {
                d2.put("contestant_score", this.h.d());
            }
        }
        if (this.e != null) {
            String identifier3 = this.e.getIdentifier();
            if (identifier3 != null) {
                d2.put("contender_score_id", identifier3);
            } else {
                d2.put("contender_score", this.e.d());
            }
        }
        if (this.j != null) {
            d2.put("context", JSONUtils.a(this.j));
        }
        return d2;
    }

    public Date getCompletedAt() {
        return this.c;
    }

    public User getContender() {
        return this.d;
    }

    public Score getContenderScore() {
        return this.e;
    }

    public Integer getContenderSkill() {
        return this.f;
    }

    public User getContestant() {
        return this.g;
    }

    public Score getContestantScore() {
        return this.h;
    }

    public Integer getContestantSkill() {
        return this.i;
    }

    public Map getContext() {
        return this.j;
    }

    public Date getCreatedAt() {
        return this.k;
    }

    public Integer getLevel() {
        return this.m;
    }

    public User getLoser() {
        return this.n;
    }

    public Integer getMode() {
        return this.o;
    }

    public Money getPrize() {
        return this.p;
    }

    public Money getStake() {
        return this.r;
    }

    public User getWinner() {
        return this.v;
    }

    public boolean isAccepted() {
        return "accepted".equalsIgnoreCase(c());
    }

    public boolean isAssigned() {
        return "assigned".equalsIgnoreCase(c());
    }

    public boolean isCancelled() {
        return "cancelled".equalsIgnoreCase(c());
    }

    public boolean isComplete() {
        return "complete".equalsIgnoreCase(c());
    }

    public boolean isCreated() {
        return "created".equalsIgnoreCase(c());
    }

    public boolean isDone() {
        return "done".equalsIgnoreCase(c());
    }

    public boolean isInvalid() {
        return "invalid".equalsIgnoreCase(c());
    }

    public boolean isInvited() {
        return "invited".equalsIgnoreCase(c());
    }

    public boolean isOpen() {
        return "open".equalsIgnoreCase(c());
    }

    public boolean isPlayableForUser(User user) {
        return getIdentifier() == null || ((isOpen() || isAssigned()) && !getContender().equals(user));
    }

    public boolean isRejected() {
        return "rejected".equalsIgnoreCase(c());
    }

    public boolean isWinner(User user) {
        if (user != null) {
            return user.equals(getWinner());
        }
        throw new IllegalArgumentException();
    }

    public void setContender(User user) {
        this.d = user;
    }

    public void setContenderScore(Score score) {
        this.e = score;
    }

    public void setContestant(User user) {
        this.g = user;
    }

    public void setContestantScore(Score score) {
        this.h = score;
    }

    public void setContext(Map map) {
        this.j = map;
    }

    public void setLevel(Integer num) {
        this.m = num;
    }

    public void setMode(Integer num) {
        this.o = num;
    }
}
