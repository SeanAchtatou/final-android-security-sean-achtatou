package com.scoreloop.client.android.ui.component.entry;

import android.content.Context;
import android.os.Bundle;
import android.widget.ListAdapter;
import com.scoreloop.client.android.ui.component.base.ComponentListActivity;
import com.scoreloop.client.android.ui.component.base.d;
import com.scoreloop.client.android.ui.component.base.e;
import com.scoreloop.client.android.ui.component.base.k;
import com.scoreloop.client.android.ui.component.base.m;
import com.scoreloop.client.android.ui.component.base.o;
import com.scoreloop.client.android.ui.framework.a;
import com.scoreloop.client.android.ui.framework.h;
import com.scoreloop.client.android.ui.framework.s;
import org.anddev.andengine.R;

public class EntryListActivity extends ComponentListActivity {
    /* access modifiers changed from: private */
    public b a;
    /* access modifiers changed from: private */
    public b b;
    /* access modifiers changed from: private */
    public b c;
    /* access modifiers changed from: private */
    public b d;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        a((ListAdapter) new a(this, this));
        a(s.a("userValues", "numberAchievements"), s.a("userValues", "numberChallengesWon"), s.a("userValues", "newsNumberUnreadItems"));
    }

    public final void a(a aVar) {
        e A = A();
        if (aVar == this.c) {
            a(A.a(C(), null, null));
        } else if (aVar == this.b) {
            a(A.c(F()));
        } else if (aVar == this.a) {
            a(A.a(F()));
        } else if (aVar == this.d) {
            a(A.g());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.client.android.ui.component.base.m.a(android.content.Context, com.scoreloop.client.android.ui.framework.s, boolean):java.lang.String
     arg types: [com.scoreloop.client.android.ui.component.entry.EntryListActivity, com.scoreloop.client.android.ui.framework.s, int]
     candidates:
      com.scoreloop.client.android.ui.component.base.m.a(android.content.Context, com.scoreloop.client.android.ui.framework.s, com.scoreloop.client.android.ui.component.base.a):java.lang.String
      com.scoreloop.client.android.ui.component.base.m.a(android.content.Context, com.scoreloop.client.android.ui.framework.s, boolean):java.lang.String */
    public final void a(s sVar, String str, Object obj, Object obj2) {
        if (a(str, "numberAchievements", obj, obj2)) {
            this.a.c(m.a((Context) this, y(), false));
            t().notifyDataSetChanged();
        } else if (a(str, "numberChallengesWon", obj, obj2)) {
            this.b.c(m.b(this, y()));
            t().notifyDataSetChanged();
        } else if (a(str, "newsNumberUnreadItems", obj, obj2)) {
            this.d.c(m.d(this, y()));
            this.d.a(m.b(this, y(), false));
            t().notifyDataSetChanged();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.client.android.ui.framework.s.a(java.lang.String, com.scoreloop.client.android.ui.framework.h, java.lang.Object):boolean
     arg types: [java.lang.String, com.scoreloop.client.android.ui.framework.h, long]
     candidates:
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, java.lang.Object, java.lang.Object):void
      com.scoreloop.client.android.ui.framework.s.a(com.scoreloop.client.android.ui.framework.s, java.util.Set, com.scoreloop.client.android.ui.framework.aj):void
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, com.scoreloop.client.android.ui.framework.h, java.lang.Object):boolean */
    public final void a(s sVar, String str) {
        x();
        if (com.scoreloop.client.android.ui.component.base.a.a(o.ACHIEVEMENT) && str.equals("numberAchievements")) {
            y().a(str, h.NOT_DIRTY, (Object) null);
        }
        if (com.scoreloop.client.android.ui.component.base.a.a(o.CHALLENGE) && str.equals("numberChallengesWon")) {
            y().a(str, h.NOT_DIRTY, (Object) null);
        }
        if (com.scoreloop.client.android.ui.component.base.a.a(o.NEWS) && str.equals("newsNumberUnreadItems")) {
            y().a(str, h.NOT_OLDER_THAN, (Object) 30000L);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        v();
        if (!d.b(this)) {
            c(new k(this, getResources().getDrawable(R.drawable.sl_icon_scoreloop), getString(R.string.sl_slapp_title), getString(R.string.sl_slapp_subtitle), null));
        }
    }

    /* access modifiers changed from: protected */
    public final void b(a aVar) {
        v();
        d.a(this);
    }
}
