package com.scoreloop.client.android.core.model;

import android.graphics.Bitmap;

public final class Award {
    private Bitmap a = null;
    private final AwardList b;
    private final Range c;
    private final String d;
    private final int e;
    private String f = "";
    private String g = "";
    private final Money h;
    private Bitmap i = null;

    Award(AwardList awardList, String str, Range range, int i2, Money money) {
        if (awardList == null || str == null || range == null || i2 < 0 || money == null) {
            throw new IllegalArgumentException();
        }
        this.b = awardList;
        this.d = str;
        this.c = range;
        this.e = i2;
        this.h = money;
    }

    /* access modifiers changed from: package-private */
    public final String a() {
        return "Award." + getIdentifier() + ".achieved.png";
    }

    /* access modifiers changed from: package-private */
    public final void a(Bitmap bitmap) {
        this.a = bitmap;
    }

    /* access modifiers changed from: package-private */
    public final void a(String str) {
        this.f = str;
    }

    public final AwardList b() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public final void b(Bitmap bitmap) {
        this.i = bitmap;
    }

    /* access modifiers changed from: package-private */
    public final void b(String str) {
        this.g = str;
    }

    /* access modifiers changed from: package-private */
    public final String c() {
        return "Award." + getIdentifier() + ".description";
    }

    /* access modifiers changed from: package-private */
    public final String d() {
        return "Award." + getIdentifier() + ".title";
    }

    /* access modifiers changed from: package-private */
    public final String e() {
        return "Award";
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        return this.d.equals(((Award) obj).d);
    }

    /* access modifiers changed from: package-private */
    public final String f() {
        return "Award." + getIdentifier() + ".unachieved.png";
    }

    public final Bitmap getAchievedImage() {
        return this.a;
    }

    public final int getAchievingValue() {
        return this.c.b();
    }

    public final Range getCounterRange() {
        return this.c;
    }

    public final String getIdentifier() {
        return this.d;
    }

    public final int getInitialValue() {
        return this.c.getLocation();
    }

    public final String getLocalizedDescription() {
        return this.f;
    }

    public final String getLocalizedTitle() {
        return this.g;
    }

    public final Money getRewardMoney() {
        return this.h;
    }

    public final Bitmap getUnachievedImage() {
        return this.i;
    }

    public final int hashCode() {
        return ((this.b.a().hashCode() + 31) * 31) + this.d.hashCode();
    }

    public final boolean isAchievedByValue(int i2) {
        return i2 >= getAchievingValue();
    }

    public final boolean isValidCounterValue(int i2) {
        return getInitialValue() <= i2 && i2 <= getAchievingValue();
    }

    public final String toString() {
        return "Award [id=" + getIdentifier() + "]";
    }
}
