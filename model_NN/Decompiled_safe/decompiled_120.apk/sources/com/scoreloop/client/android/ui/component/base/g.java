package com.scoreloop.client.android.ui.component.base;

import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.controller.RequestControllerObserver;

final class g implements RequestControllerObserver {
    private /* synthetic */ ComponentActivity a;

    /* synthetic */ g(ComponentActivity componentActivity) {
        this(componentActivity, (byte) 0);
    }

    private g(ComponentActivity componentActivity, byte b) {
        this.a = componentActivity;
    }

    public final void requestControllerDidFail(RequestController requestController, Exception exc) {
        this.a.requestControllerDidFail(requestController, exc);
    }

    public final void requestControllerDidReceiveResponse(RequestController requestController) {
        this.a.requestControllerDidReceiveResponse(requestController);
    }
}
