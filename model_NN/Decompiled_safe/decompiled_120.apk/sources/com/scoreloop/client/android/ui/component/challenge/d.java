package com.scoreloop.client.android.ui.component.challenge;

import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.ui.component.base.ComponentActivity;
import com.scoreloop.client.android.ui.component.base.k;
import org.anddev.andengine.R;

public final class d extends k {
    public d(ComponentActivity componentActivity, User user) {
        super(componentActivity, null, null, null, user);
        if (user == null) {
            a(componentActivity.getResources().getDrawable(R.drawable.sl_icon_challenge_anyone));
            a(componentActivity.getResources().getString(R.string.sl_against_anyone));
            return;
        }
        a(componentActivity.getResources().getDrawable(R.drawable.sl_icon_user));
        a(user.getDisplayName());
    }

    /* access modifiers changed from: protected */
    public final int g() {
        return 0;
    }

    public final int a() {
        return 5;
    }

    /* access modifiers changed from: protected */
    public final int h() {
        return R.layout.sl_list_item_user;
    }

    /* access modifiers changed from: protected */
    public final String i() {
        if (p() != null) {
            return ((User) p()).getImageUrl();
        }
        return null;
    }

    public final boolean b() {
        return true;
    }
}
