package com.scoreloop.client.android.core.model;

import java.util.Date;
import java.util.UUID;
import org.json.JSONObject;

public class SharedDeviceUuid {
    private final String a;
    private final Date b;

    public SharedDeviceUuid() {
        this.a = UUID.randomUUID().toString() + "-generated";
        this.b = new Date();
    }

    public SharedDeviceUuid(JSONObject jSONObject) {
        this.a = jSONObject.getString("uuid");
        this.b = new Date((long) jSONObject.getInt("date"));
    }

    public String a() {
        return this.a;
    }

    public JSONObject b() {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("uuid", this.a);
        jSONObject.put("date", this.b.getTime());
        return jSONObject;
    }
}
