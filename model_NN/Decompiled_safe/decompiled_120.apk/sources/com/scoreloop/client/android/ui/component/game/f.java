package com.scoreloop.client.android.ui.component.game;

import android.widget.ListView;
import com.scoreloop.client.android.ui.framework.ak;
import com.scoreloop.client.android.ui.framework.t;

final class f implements Runnable {
    private /* synthetic */ GameListActivity a;
    private final /* synthetic */ ListView b;
    private final /* synthetic */ ak c;

    f(GameListActivity gameListActivity, ListView listView, ak akVar) {
        this.a = gameListActivity;
        this.b = listView;
        this.c = akVar;
    }

    public final void run() {
        if (this.a.c == t.PAGE_TO_TOP) {
            this.b.setSelection(0);
        } else if (this.a.c == t.PAGE_TO_NEXT) {
            this.b.setSelection(this.c.b());
        } else if (this.a.c == t.PAGE_TO_PREV) {
            this.b.setSelectionFromTop(this.c.c() + 1, this.b.getHeight());
        }
    }
}
