package com.scoreloop.client.android.ui.a;

import android.graphics.Bitmap;

final class g {
    private final Bitmap a;
    private final h b;

    static g a() {
        return new g(h.NOT_FOUND);
    }

    static g b() {
        return new g(h.ERROR);
    }

    g(Bitmap bitmap) {
        this.a = bitmap;
        this.b = h.OK;
    }

    private g(h hVar) {
        this.a = null;
        this.b = hVar;
    }

    /* access modifiers changed from: package-private */
    public final Bitmap c() {
        return this.a;
    }

    /* access modifiers changed from: package-private */
    public final boolean d() {
        return this.b != h.ERROR;
    }

    /* access modifiers changed from: package-private */
    public final boolean e() {
        return this.b == h.NOT_FOUND;
    }
}
