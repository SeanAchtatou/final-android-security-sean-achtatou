package com.scoreloop.client.android.ui.a;

import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import java.lang.ref.WeakReference;

final class i extends BitmapDrawable {
    private final WeakReference a;

    public i(Drawable drawable, j jVar) {
        super(((BitmapDrawable) drawable).getBitmap());
        this.a = new WeakReference(jVar);
    }

    public final j a() {
        return (j) this.a.get();
    }
}
