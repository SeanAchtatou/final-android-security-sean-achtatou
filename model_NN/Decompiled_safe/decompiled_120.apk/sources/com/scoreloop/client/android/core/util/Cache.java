package com.scoreloop.client.android.core.util;

import android.os.Handler;
import java.lang.ref.SoftReference;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Cache {
    /* access modifiers changed from: private */
    public int a;
    private long b;
    private HashMap c;
    /* access modifiers changed from: private */
    public ConcurrentHashMap d;
    private Handler e;
    private final Runnable f;

    class a {
        private long b;
        private long c;
        private Object d;

        a(Object obj, long j, long j2) {
            this.d = obj;
            this.b = j;
            this.c = j2;
        }

        /* access modifiers changed from: package-private */
        public long a() {
            return this.b;
        }

        /* access modifiers changed from: package-private */
        public long b() {
            return this.c;
        }

        /* access modifiers changed from: package-private */
        public Object c() {
            return this.d;
        }
    }

    public Cache() {
        this(100);
    }

    public Cache(int i) {
        this.f = new a(this);
        this.a = i;
        this.b = 0;
        this.e = new Handler();
        c();
        d();
    }

    private void a(long j) {
        if (j > 0) {
            if (j < this.b) {
                this.b = j;
            } else if (this.b == 0) {
                this.b = j;
            }
        }
        this.e.removeCallbacks(this.f);
        if (this.b > 0) {
            this.e.postDelayed(this.f, this.b);
        }
    }

    private void c() {
        this.c = new LinkedHashMap(this.a / 2, 0.75f, true) {
            private static final long serialVersionUID = 1;

            /* access modifiers changed from: protected */
            public boolean removeEldestEntry(Map.Entry entry) {
                if (size() <= Cache.this.a) {
                    return false;
                }
                Cache.this.d.put(entry.getKey(), new SoftReference(entry.getValue()));
                return true;
            }
        };
    }

    private void d() {
        this.d = new ConcurrentHashMap(this.a / 2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0028, code lost:
        r1 = (com.scoreloop.client.android.core.util.Cache.a) r0.get();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x002e, code lost:
        if (r1 == null) goto L_0x0038;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0038, code lost:
        r3.d.remove(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x003d, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        return r1.c();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001e, code lost:
        r0 = (java.lang.ref.SoftReference) r3.d.get(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0026, code lost:
        if (r0 == null) goto L_0x003d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object a(java.lang.Object r4) {
        /*
            r3 = this;
            java.util.HashMap r1 = r3.c
            monitor-enter(r1)
            java.util.HashMap r0 = r3.c     // Catch:{ all -> 0x0035 }
            java.lang.Object r0 = r0.get(r4)     // Catch:{ all -> 0x0035 }
            com.scoreloop.client.android.core.util.Cache$a r0 = (com.scoreloop.client.android.core.util.Cache.a) r0     // Catch:{ all -> 0x0035 }
            if (r0 == 0) goto L_0x001d
            java.util.HashMap r2 = r3.c     // Catch:{ all -> 0x0035 }
            r2.remove(r4)     // Catch:{ all -> 0x0035 }
            java.util.HashMap r2 = r3.c     // Catch:{ all -> 0x0035 }
            r2.put(r4, r0)     // Catch:{ all -> 0x0035 }
            java.lang.Object r0 = r0.c()     // Catch:{ all -> 0x0035 }
            monitor-exit(r1)     // Catch:{ all -> 0x0035 }
        L_0x001c:
            return r0
        L_0x001d:
            monitor-exit(r1)     // Catch:{ all -> 0x0035 }
            java.util.concurrent.ConcurrentHashMap r0 = r3.d
            java.lang.Object r0 = r0.get(r4)
            java.lang.ref.SoftReference r0 = (java.lang.ref.SoftReference) r0
            if (r0 == 0) goto L_0x003d
            java.lang.Object r1 = r0.get()
            com.scoreloop.client.android.core.util.Cache$a r1 = (com.scoreloop.client.android.core.util.Cache.a) r1
            if (r1 == 0) goto L_0x0038
            java.lang.Object r0 = r1.c()
            goto L_0x001c
        L_0x0035:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0035 }
            throw r0
        L_0x0038:
            java.util.concurrent.ConcurrentHashMap r1 = r3.d
            r1.remove(r0)
        L_0x003d:
            r0 = 0
            goto L_0x001c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.scoreloop.client.android.core.util.Cache.a(java.lang.Object):java.lang.Object");
    }

    public void a() {
        long currentTimeMillis = System.currentTimeMillis();
        HashSet hashSet = new HashSet(this.c.keySet());
        this.d.clear();
        for (Object next : hashSet) {
            a aVar = (a) this.c.get(next);
            if (aVar.a() + aVar.b() < currentTimeMillis) {
                synchronized (this.c) {
                    this.d.put(next, new SoftReference(aVar));
                    this.c.remove(next);
                }
            }
        }
        a(this.b);
    }

    public void a(Object obj, Object obj2, long j) {
        long currentTimeMillis = System.currentTimeMillis();
        if (obj2 != null) {
            synchronized (this.c) {
                this.c.put(obj, new a(obj2, currentTimeMillis, j));
            }
        }
        a(j);
    }

    public void b() {
        synchronized (this.c) {
            this.c.clear();
        }
        synchronized (this.d) {
            this.d.clear();
        }
    }
}
