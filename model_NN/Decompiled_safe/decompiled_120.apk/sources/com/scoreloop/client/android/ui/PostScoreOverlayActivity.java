package com.scoreloop.client.android.ui;

import com.scoreloop.client.android.core.model.Challenge;
import com.scoreloop.client.android.core.model.Entity;
import com.scoreloop.client.android.core.model.Score;
import com.scoreloop.client.android.ui.component.base.m;
import com.scoreloop.client.android.ui.component.post.PostOverlayActivity;

public class PostScoreOverlayActivity extends PostOverlayActivity {
    /* access modifiers changed from: protected */
    public final Entity a() {
        c cVar = (c) f.a();
        Challenge j = cVar.j();
        if (j == null || j.getIdentifier() == null) {
            return cVar.k();
        }
        return j;
    }

    /* access modifiers changed from: protected */
    public final String b() {
        c cVar = (c) f.a();
        Entity a = a();
        if (a instanceof Score) {
            return "Score: " + m.c((Score) a, cVar.i());
        }
        return "Challenge";
    }
}
