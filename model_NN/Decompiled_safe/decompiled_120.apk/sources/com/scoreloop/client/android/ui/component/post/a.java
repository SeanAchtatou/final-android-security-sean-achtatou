package com.scoreloop.client.android.ui.component.post;

import com.scoreloop.client.android.core.controller.SocialProviderController;
import com.scoreloop.client.android.core.model.SocialProvider;

final class a implements Runnable {
    private /* synthetic */ PostOverlayActivity a;
    private final /* synthetic */ SocialProvider b;

    a(PostOverlayActivity postOverlayActivity, SocialProvider socialProvider) {
        this.a = postOverlayActivity;
        this.b = socialProvider;
    }

    public final void run() {
        SocialProviderController.getSocialProviderController(null, this.a, this.b).connect(this.a);
    }
}
