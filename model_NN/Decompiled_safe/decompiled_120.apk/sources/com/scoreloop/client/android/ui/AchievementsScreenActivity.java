package com.scoreloop.client.android.ui;

import android.os.Bundle;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.ui.component.base.a;
import com.scoreloop.client.android.ui.component.base.o;
import com.scoreloop.client.android.ui.framework.ScreenActivity;

public class AchievementsScreenActivity extends ScreenActivity {
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        c cVar = (c) f.a();
        if (!a.a(o.ACHIEVEMENT)) {
            finish();
        } else {
            a(cVar.a((User) null), bundle);
        }
    }
}
