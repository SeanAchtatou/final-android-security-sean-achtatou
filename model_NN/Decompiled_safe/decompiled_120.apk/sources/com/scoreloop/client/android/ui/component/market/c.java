package com.scoreloop.client.android.ui.component.market;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.TextView;
import com.scoreloop.client.android.ui.component.base.ComponentActivity;
import com.scoreloop.client.android.ui.component.base.f;
import com.scoreloop.client.android.ui.component.base.k;
import org.anddev.andengine.R;

public final class c extends k {
    private String a;
    private Integer b;

    public c(ComponentActivity componentActivity, Drawable drawable, String str, String str2) {
        super(componentActivity, drawable, str, str2, null);
    }

    public final void a(Integer num) {
        this.b = num;
    }

    /* access modifiers changed from: protected */
    public final f l() {
        return new b();
    }

    /* access modifiers changed from: protected */
    public final void a(View view, f fVar) {
        super.a(view, fVar);
        ((b) fVar).e = (TextView) view.findViewById(R.id.sl_number);
    }

    /* access modifiers changed from: protected */
    public final void a(f fVar) {
        super.a(fVar);
        b bVar = (b) fVar;
        if (bVar.e == null) {
            return;
        }
        if (this.b != null) {
            bVar.e.setText(this.b.toString());
        } else {
            bVar.e.setText((CharSequence) null);
        }
    }

    /* access modifiers changed from: protected */
    public final String i() {
        return this.a;
    }

    /* access modifiers changed from: protected */
    public final int h() {
        return R.layout.sl_list_item_market;
    }
}
