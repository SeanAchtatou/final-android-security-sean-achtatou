package com.scoreloop.client.android.ui.component.user;

import android.graphics.drawable.Drawable;
import com.scoreloop.client.android.ui.component.base.ComponentActivity;
import com.scoreloop.client.android.ui.component.base.k;
import org.anddev.andengine.R;

public final class m extends k {
    public m(ComponentActivity componentActivity, Drawable drawable, String str, Object obj) {
        super(componentActivity, drawable, str, null, obj);
    }

    /* access modifiers changed from: protected */
    public final int h() {
        return R.layout.sl_list_item_user;
    }

    public final int a() {
        return 26;
    }
}
