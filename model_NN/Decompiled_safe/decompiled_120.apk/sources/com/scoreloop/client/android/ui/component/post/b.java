package com.scoreloop.client.android.ui.component.post;

import android.view.View;
import android.widget.CheckBox;
import com.scoreloop.client.android.core.model.SocialProvider;

final class b implements View.OnClickListener {
    private /* synthetic */ PostOverlayActivity a;

    b(PostOverlayActivity postOverlayActivity) {
        this.a = postOverlayActivity;
    }

    public final void onClick(View view) {
        this.a.c.setTarget(this.a.a());
        for (CheckBox checkBox : this.a.b.keySet()) {
            SocialProvider socialProvider = (SocialProvider) this.a.b.get(checkBox);
            if (checkBox.isChecked()) {
                this.a.c.addReceiverWithUsers(socialProvider, new Object[0]);
            }
        }
        this.a.c.setText(this.a.d.getText().toString());
        if (this.a.c.isSubmitAllowed()) {
            this.a.a(true);
            this.a.c.submitMessage();
        }
    }
}
