package com.scoreloop.client.android.ui.component.a;

import com.scoreloop.client.android.core.addon.RSSFeed;
import com.scoreloop.client.android.ui.framework.c;
import com.scoreloop.client.android.ui.framework.s;
import java.util.Collections;
import java.util.Set;

public final class j implements c {
    public static final String[] a = {"newsNumberUnreadItems", "newsFeed"};
    private RSSFeed b;

    public final boolean a() {
        if (this.b != null) {
            return this.b.getState() == RSSFeed.State.PENDING;
        }
        return false;
    }

    public final void a(s sVar) {
        if (this.b == null) {
            this.b = new RSSFeed(null);
        }
        this.b.reloadOnNextRequest();
        this.b.requestAllItems(new f(this, sVar), false, null);
    }

    public final void a(Set set) {
        Collections.addAll(set, a);
    }
}
