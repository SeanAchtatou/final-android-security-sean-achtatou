package com.scoreloop.client.android.core.model;

import java.util.Comparator;

public class ScoreComparator implements Comparator {
    private boolean a;
    private boolean b;

    public ScoreComparator(boolean z, boolean z2) {
        this.a = z;
        this.b = z2;
    }

    /* renamed from: a */
    public int compare(Score score, Score score2) {
        double doubleValue = score.getResult().doubleValue() - score2.getResult().doubleValue();
        double doubleValue2 = (score.getMinorResult() == null || score2.getMinorResult() == null) ? 0.0d : score.getMinorResult().doubleValue() - score2.getMinorResult().doubleValue();
        if (doubleValue != 0.0d) {
            if (!this.a) {
                doubleValue = -doubleValue;
            }
            return (int) doubleValue;
        } else if (doubleValue2 == 0.0d) {
            return 0;
        } else {
            return (int) (this.b ? doubleValue2 : -doubleValue2);
        }
    }
}
