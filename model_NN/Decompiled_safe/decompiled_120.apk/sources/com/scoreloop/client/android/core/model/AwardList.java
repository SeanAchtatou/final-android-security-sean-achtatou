package com.scoreloop.client.android.core.model;

import android.content.Context;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class AwardList {
    private final List a = new ArrayList();
    private final Map b = new HashMap();
    private String c;

    public static AwardList a(Context context, String str, String str2) {
        if (context != null && str != null) {
            return new a(context).a(str, str2);
        }
        throw new IllegalArgumentException("you have to provide context and a game");
    }

    public final String a() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public final void a(Award award) {
        String identifier = award.getIdentifier();
        if (this.b.containsKey(identifier)) {
            throw new IllegalStateException("award with same identifier already added");
        }
        this.a.add(award);
        this.b.put(identifier, award);
    }

    /* access modifiers changed from: package-private */
    public final void a(String str) {
        this.c = str;
    }

    public final List getAwardIdentifiers() {
        ArrayList arrayList = new ArrayList();
        for (Award identifier : this.a) {
            arrayList.add(identifier.getIdentifier());
        }
        return arrayList;
    }

    public final Award getAwardWithIdentifier(String str) {
        if (str == null) {
            throw new IllegalArgumentException("identifier argument must not be null");
        }
        Award award = (Award) this.b.get(str);
        if (award != null) {
            return award;
        }
        throw new IllegalArgumentException("invalid identifier: " + str);
    }

    public final List getAwards() {
        return this.a;
    }
}
