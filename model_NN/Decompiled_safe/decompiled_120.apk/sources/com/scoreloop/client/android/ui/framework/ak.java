package com.scoreloop.client.android.ui.framework;

import android.content.Context;

public final class ak extends i {
    private final int b = 1;
    private af c;
    private int d = 0;
    private af e;
    private af f;

    public ak(Context context, byte b2) {
        super(context);
    }

    public ak(Context context) {
        super(context);
    }

    public final void a(boolean z, boolean z2, boolean z3) {
        this.d = 0;
        if (z) {
            if (this.f == null) {
                this.f = new af(getContext(), t.PAGE_TO_TOP);
            }
            insert(this.f, this.b);
            this.d = t.PAGE_TO_TOP.a(this.d);
        }
        if (z2) {
            if (this.e == null) {
                this.e = new af(getContext(), t.PAGE_TO_PREV);
            }
            insert(this.e, z ? this.b + 1 : this.b);
            this.d = t.PAGE_TO_PREV.a(this.d);
        }
        if (z3) {
            if (this.c == null) {
                this.c = new af(getContext(), t.PAGE_TO_NEXT);
            }
            add(this.c);
            this.d = t.PAGE_TO_NEXT.a(this.d);
        }
    }

    public final a b(int i) {
        return (a) getItem(b() + i);
    }

    public final int b() {
        int i = this.b;
        if (t.PAGE_TO_TOP.b(this.d)) {
            i++;
        }
        if (t.PAGE_TO_PREV.b(this.d)) {
            return i + 1;
        }
        return i;
    }

    public final int c() {
        int count = getCount() - 1;
        if (t.PAGE_TO_NEXT.b(this.d)) {
            count--;
        }
        return Math.max(0, count);
    }

    public final void a(int i) {
        if (this.a != null) {
            a aVar = (a) getItem(i);
            if (aVar.a() == 0) {
                ((ad) this.a).a(((af) aVar).g());
            } else {
                this.a.a(aVar);
            }
        }
    }
}
