package com.scoreloop.client.android.ui.component.profile;

import android.content.Intent;
import android.net.Uri;
import com.scoreloop.client.android.ui.a.d;

final class l implements Runnable {
    private /* synthetic */ ProfileSettingsPictureListActivity a;
    private final /* synthetic */ Intent b;

    l(ProfileSettingsPictureListActivity profileSettingsPictureListActivity, Intent intent) {
        this.a = profileSettingsPictureListActivity;
        this.b = intent;
    }

    public final void run() {
        Uri data = this.b.getData();
        ProfileSettingsPictureListActivity.a(this.a, d.a(data, this.a.getContentResolver(), d.a(data, this.a.getContentResolver(), this.a.getApplicationContext())), data);
    }
}
