package com.scoreloop.client.android.ui.component.market;

import android.content.res.Resources;
import android.os.Bundle;
import android.widget.ListAdapter;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.ui.component.base.ComponentListActivity;
import com.scoreloop.client.android.ui.component.base.e;
import com.scoreloop.client.android.ui.framework.a;
import com.scoreloop.client.android.ui.framework.aj;
import org.anddev.andengine.R;

public class MarketListActivity extends ComponentListActivity implements aj {
    /* access modifiers changed from: private */
    public c a;
    /* access modifiers changed from: private */
    public c b;
    /* access modifiers changed from: private */
    public c c;
    /* access modifiers changed from: private */
    public c d;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Resources resources = getResources();
        this.b = new c(this, resources.getDrawable(R.drawable.sl_icon_games), getString(R.string.sl_my_games), getString(R.string.sl_games_subtitle));
        this.b.a(Session.getCurrentSession().getUser().getGamesCounter());
        this.d = new c(this, resources.getDrawable(R.drawable.sl_icon_market), getString(R.string.sl_popular_games), getString(R.string.sl_popular_games_subtitle));
        this.c = new c(this, resources.getDrawable(R.drawable.sl_icon_market), getString(R.string.sl_new_games), getString(R.string.sl_new_games_subtitle));
        this.a = new c(this, resources.getDrawable(R.drawable.sl_icon_market), getString(R.string.sl_friends_games), getString(R.string.sl_friends_games_subtitle));
        a((ListAdapter) new a(this, this));
    }

    public final void a(a aVar) {
        e A = A();
        User F = F();
        if (aVar == this.b) {
            a(A.a(F, 0));
        } else if (aVar == this.d) {
            a(A.a(F, 1));
        } else if (aVar == this.c) {
            a(A.a(F, 2));
        } else if (aVar == this.a) {
            a(A.a(F, 3));
        }
    }
}
