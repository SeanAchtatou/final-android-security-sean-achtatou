package com.scoreloop.client.android.core.controller;

class e {
    private final String a;
    private final a b;

    enum a {
        ASCENDING("ASC"),
        DESCENDING("DESC");
        
        private final String a;

        private a(String str) {
            this.a = str;
        }

        public final String toString() {
            return this.a;
        }
    }

    public e(String str, a aVar) {
        this.a = str;
        this.b = aVar;
    }

    public String a() {
        return this.a;
    }

    public String b() {
        return this.b.toString();
    }
}
