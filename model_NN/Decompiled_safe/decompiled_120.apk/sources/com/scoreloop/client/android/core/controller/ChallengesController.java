package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.model.Challenge;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.RequestCompletionCallback;
import com.scoreloop.client.android.core.server.RequestMethod;
import com.scoreloop.client.android.core.server.Response;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ChallengesController extends RequestController {
    private List c;
    private User d;

    class a extends Request {
        private final Game a;
        private final String b;
        private final User c;

        public a(RequestCompletionCallback requestCompletionCallback, Game game, User user, String str) {
            super(requestCompletionCallback);
            if (game == null) {
                throw new IllegalStateException("internal error: aGame should be set");
            }
            this.a = game;
            this.c = user;
            this.b = str;
        }

        public String a() {
            return String.format("/service/games/%s/challenges", this.a.getIdentifier());
        }

        public JSONObject b() {
            JSONObject jSONObject = new JSONObject();
            try {
                if (this.b != null) {
                    jSONObject.put("search_list_id", this.b);
                }
                if (this.c != null) {
                    jSONObject.put("user_id", this.c.getIdentifier());
                }
                return jSONObject;
            } catch (JSONException e) {
                throw new IllegalStateException("Invalid challenge data", e);
            }
        }

        public RequestMethod c() {
            return RequestMethod.GET;
        }
    }

    public ChallengesController(RequestControllerObserver requestControllerObserver) {
        this(null, requestControllerObserver);
    }

    public ChallengesController(Session session, RequestControllerObserver requestControllerObserver) {
        super(session, requestControllerObserver);
        this.c = Collections.emptyList();
        this.d = h().getUser();
    }

    private void a(String str) {
        a aVar = new a(g(), getGame(), getUser(), str);
        a_();
        aVar.a(30000);
        b(aVar);
    }

    private void a(List list) {
        this.c = Collections.unmodifiableList(list);
    }

    /* access modifiers changed from: package-private */
    public boolean a(Request request, Response response) {
        int f = response.f();
        if (f != 200) {
            throw new Exception("Request failed with status:" + f);
        }
        JSONArray jSONArray = ((JSONObject) response.a()).getJSONArray("challenges");
        ArrayList arrayList = new ArrayList();
        int length = jSONArray.length();
        for (int i = 0; i < length; i++) {
            arrayList.add(new Challenge(jSONArray.getJSONObject(i).getJSONObject(Challenge.a)));
        }
        a(arrayList);
        return true;
    }

    public List getChallenges() {
        return this.c;
    }

    public User getUser() {
        return this.d;
    }

    public void loadChallengeHistory() {
        a("#history");
    }

    public void loadOpenChallenges() {
        a("#open");
    }

    public void setUser(User user) {
        if (user == null) {
            throw new IllegalArgumentException("user must not be null");
        }
        this.d = user;
    }
}
