package com.scoreloop.client.android.ui.component.a;

import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.controller.RequestControllerException;
import com.scoreloop.client.android.core.controller.RequestControllerObserver;
import com.scoreloop.client.android.core.controller.UserController;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.ui.framework.BaseActivity;
import com.scoreloop.client.android.ui.framework.s;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.anddev.andengine.R;

public final class e implements RequestControllerObserver {
    private static /* synthetic */ int[] h;
    private final BaseActivity a;
    private final c b;
    private final UserController c;
    private int d;
    private final i e;
    private final s f;
    private final List g = new ArrayList();

    private static /* synthetic */ int[] b() {
        int[] iArr = h;
        if (iArr == null) {
            iArr = new int[i.values().length];
            try {
                iArr[i.ADD.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[i.REMOVE.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            h = iArr;
        }
        return iArr;
    }

    public static void a(BaseActivity baseActivity, List list, s sVar, c cVar) {
        new e(baseActivity, i.ADD, list, sVar, cVar);
    }

    public static void a(BaseActivity baseActivity, User user, s sVar, c cVar) {
        new e(baseActivity, i.ADD, Collections.singletonList(user), sVar, cVar);
    }

    public static void b(BaseActivity baseActivity, User user, s sVar, c cVar) {
        new e(baseActivity, i.REMOVE, Collections.singletonList(user), sVar, cVar);
    }

    private e(BaseActivity baseActivity, i iVar, List list, s sVar, c cVar) {
        this.a = baseActivity;
        this.e = iVar;
        this.g.addAll(list);
        this.f = sVar;
        this.b = cVar;
        this.c = new UserController(this);
        a();
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x006b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a() {
        /*
            r6 = this;
            r5 = 0
            com.scoreloop.client.android.core.model.Session r0 = com.scoreloop.client.android.core.model.Session.getCurrentSession()
            com.scoreloop.client.android.core.model.User r1 = r0.getUser()
            java.util.List r2 = r1.getBuddyUsers()
        L_0x000d:
            java.util.List r0 = r6.g
            if (r0 != 0) goto L_0x004b
            r0 = r5
        L_0x0012:
            if (r0 == 0) goto L_0x0034
            boolean r3 = r1.equals(r0)
            if (r3 != 0) goto L_0x000d
            if (r2 == 0) goto L_0x0034
            com.scoreloop.client.android.ui.component.a.i r3 = r6.e
            com.scoreloop.client.android.ui.component.a.i r4 = com.scoreloop.client.android.ui.component.a.i.ADD
            if (r3 != r4) goto L_0x0028
            boolean r3 = r2.contains(r0)
            if (r3 != 0) goto L_0x000d
        L_0x0028:
            com.scoreloop.client.android.ui.component.a.i r3 = r6.e
            com.scoreloop.client.android.ui.component.a.i r4 = com.scoreloop.client.android.ui.component.a.i.REMOVE
            if (r3 != r4) goto L_0x0034
            boolean r3 = r2.contains(r0)
            if (r3 == 0) goto L_0x000d
        L_0x0034:
            if (r0 == 0) goto L_0x006b
            com.scoreloop.client.android.core.controller.UserController r1 = r6.c
            r1.setUser(r0)
            int[] r0 = b()
            com.scoreloop.client.android.ui.component.a.i r1 = r6.e
            int r1 = r1.ordinal()
            r0 = r0[r1]
            switch(r0) {
                case 1: goto L_0x005f;
                case 2: goto L_0x0065;
                default: goto L_0x004a;
            }
        L_0x004a:
            return
        L_0x004b:
            java.util.List r0 = r6.g
            boolean r0 = r0.isEmpty()
            if (r0 == 0) goto L_0x0055
            r0 = r5
            goto L_0x0012
        L_0x0055:
            java.util.List r0 = r6.g
            r3 = 0
            java.lang.Object r0 = r0.remove(r3)
            com.scoreloop.client.android.core.model.User r0 = (com.scoreloop.client.android.core.model.User) r0
            goto L_0x0012
        L_0x005f:
            com.scoreloop.client.android.core.controller.UserController r0 = r6.c
            r0.addAsBuddy()
            goto L_0x004a
        L_0x0065:
            com.scoreloop.client.android.core.controller.UserController r0 = r6.c
            r0.removeAsBuddy()
            goto L_0x004a
        L_0x006b:
            com.scoreloop.client.android.ui.framework.s r0 = r6.f
            java.lang.String r1 = "numberBuddies"
            java.lang.Object r0 = r0.a(r1)
            java.lang.Integer r0 = (java.lang.Integer) r0
            if (r0 == 0) goto L_0x008f
            com.scoreloop.client.android.ui.component.a.i r1 = r6.e
            com.scoreloop.client.android.ui.component.a.i r2 = com.scoreloop.client.android.ui.component.a.i.ADD
            if (r1 != r2) goto L_0x00a0
            int r0 = r0.intValue()
            int r1 = r6.d
            int r0 = r0 + r1
        L_0x0084:
            com.scoreloop.client.android.ui.framework.s r1 = r6.f
            java.lang.String r2 = "numberBuddies"
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r1.b(r2, r0)
        L_0x008f:
            com.scoreloop.client.android.ui.framework.s r0 = r6.f
            r0.a()
            com.scoreloop.client.android.ui.component.a.c r0 = r6.b
            if (r0 == 0) goto L_0x004a
            com.scoreloop.client.android.ui.component.a.c r0 = r6.b
            int r1 = r6.d
            r0.a(r1)
            goto L_0x004a
        L_0x00a0:
            int r0 = r0.intValue()
            int r1 = r6.d
            int r0 = r0 - r1
            goto L_0x0084
        */
        throw new UnsupportedOperationException("Method not decompiled: com.scoreloop.client.android.ui.component.a.e.a():void");
    }

    public final void requestControllerDidFail(RequestController requestController, Exception exc) {
        if (exc instanceof RequestControllerException) {
            int errorCode = ((RequestControllerException) exc).getErrorCode();
            switch (b()[this.e.ordinal()]) {
                case 1:
                    if (errorCode == 40) {
                        this.a.d(String.format(this.a.getString(R.string.sl_format_friend_already_added), this.c.getUser().getDisplayName()));
                        break;
                    }
                    break;
                case 2:
                    if (errorCode == 41) {
                        this.a.d(String.format(this.a.getString(R.string.sl_format_friend_already_removed), this.c.getUser().getDisplayName()));
                        break;
                    }
                    break;
            }
        }
        a();
    }

    public final void requestControllerDidReceiveResponse(RequestController requestController) {
        this.d++;
        a();
    }
}
