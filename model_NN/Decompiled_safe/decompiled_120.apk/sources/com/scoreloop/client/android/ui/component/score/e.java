package com.scoreloop.client.android.ui.component.score;

import android.content.Context;
import android.view.View;
import android.widget.TextView;
import com.scoreloop.client.android.core.model.Ranking;
import com.scoreloop.client.android.core.model.Score;
import com.scoreloop.client.android.ui.component.base.ComponentActivity;
import com.scoreloop.client.android.ui.component.base.f;
import com.scoreloop.client.android.ui.component.base.m;
import org.anddev.andengine.R;

public final class e extends h {
    private Ranking a;

    public e(ComponentActivity componentActivity, Score score, Ranking ranking) {
        super(componentActivity, score, true);
        this.a = ranking;
    }

    /* access modifiers changed from: protected */
    public final f l() {
        return new c();
    }

    /* access modifiers changed from: protected */
    public final void a(View view, f fVar) {
        super.a(view, fVar);
        ((c) fVar).e = (TextView) view.findViewById(R.id.sl_list_item_score_percent);
    }

    /* access modifiers changed from: protected */
    public final int h() {
        return R.layout.sl_list_item_score_highlighted;
    }

    public final int a() {
        return 21;
    }

    public final void a(Ranking ranking) {
        this.a = ranking;
    }

    /* access modifiers changed from: protected */
    public final void a(f fVar) {
        super.a(fVar);
        Context c = c();
        Ranking ranking = this.a;
        n().x();
        ((c) fVar).e.setText(m.a(c, ranking));
    }
}
