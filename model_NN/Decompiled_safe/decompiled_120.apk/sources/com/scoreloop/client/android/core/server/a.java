package com.scoreloop.client.android.core.server;

import com.scoreloop.client.android.core.util.Logger;
import com.scoreloop.client.android.core.util.SetterIntent;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import org.anddev.andengine.entity.layer.tiled.tmx.util.constants.TMXConstants;
import org.apache.http.client.methods.HttpPost;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class a {
    private String a = null;
    private Boolean b = false;
    /* access modifiers changed from: private */
    public e c;
    private final BayeuxConnectionObserver d;
    private String e;
    private URI f;

    /* renamed from: com.scoreloop.client.android.core.server.a$a  reason: collision with other inner class name */
    class C0001a {
        protected int a;
        protected String b;
        protected JSONObject c;
        protected JSONObject d;
        private HttpPost f;

        private C0001a(e eVar) {
            this.f = null;
        }

        /* access modifiers changed from: private */
        public HttpPost b() {
            HttpPost httpPost;
            synchronized (this) {
                this.f = a.this.c.a();
                httpPost = this.f;
            }
            return httpPost;
        }

        public void a() {
            synchronized (this) {
                if (this.f != null) {
                    this.f.abort();
                }
            }
        }

        public void a(int i, String str, JSONObject jSONObject, JSONObject jSONObject2) {
            this.a = i;
            this.b = str;
            this.c = jSONObject;
            this.d = jSONObject2;
        }
    }

    a(URL url, BayeuxConnectionObserver bayeuxConnectionObserver, byte[] bArr) {
        if (bayeuxConnectionObserver == null) {
            throw new IllegalArgumentException("Observer can't be null");
        }
        try {
            this.f = url.toURI();
            this.d = bayeuxConnectionObserver;
            if (bArr != null) {
                this.c = new f(this.f, bArr);
            } else {
                this.c = new e(this.f);
            }
        } catch (URISyntaxException e2) {
            throw new IllegalArgumentException("Malformed URL");
        }
    }

    private void a(C0001a aVar, Integer num, JSONObject jSONObject, String str, JSONObject jSONObject2) {
        JSONObject jSONObject3;
        if (str == null) {
            throw new IllegalArgumentException("Request's channel can't be null !");
        }
        if (this.a == null) {
            Logger.a("BayeuxConnection", "executeRequest(): about to handshake");
            b(aVar);
            Logger.a("ServerCommThread", "executeRequest(): handshake completed");
        }
        JSONObject jSONObject4 = new JSONObject();
        try {
            jSONObject4.put("clientId", b());
            jSONObject4.put("channel", str);
            jSONObject4.put(TMXConstants.TAG_DATA, jSONObject);
            jSONObject4.put(TMXConstants.TAG_TILE_ATTRIBUTE_ID, num);
            if (this.e != null) {
                jSONObject3 = jSONObject2 == null ? new JSONObject() : jSONObject2;
                jSONObject3.put("token", this.e);
            } else {
                jSONObject3 = jSONObject2;
            }
            jSONObject4.putOpt("ext", jSONObject3);
            a(aVar, jSONObject4);
        } catch (JSONException e2) {
            throw new b(e2);
        }
    }

    private void a(C0001a aVar, JSONObject jSONObject) {
        JSONArray jSONArray = new JSONArray();
        jSONArray.put(jSONObject);
        JSONArray a2 = this.c.a(aVar.b(), jSONArray);
        int length = a2.length();
        SetterIntent setterIntent = new SetterIntent();
        int i = 0;
        int i2 = 0;
        while (i < length) {
            try {
                JSONObject jSONObject2 = a2.getJSONObject(i);
                String d2 = setterIntent.d(jSONObject2, "channel", SetterIntent.KeyMode.THROWS_WHEN_NO_KEY, SetterIntent.ValueMode.ALLOWS_NULL_VALUE);
                Object opt = jSONObject2.opt(TMXConstants.TAG_DATA);
                Integer a3 = setterIntent.a(jSONObject2, TMXConstants.TAG_TILE_ATTRIBUTE_ID, SetterIntent.KeyMode.USE_NULL_WHEN_NO_KEY, SetterIntent.ValueMode.ALLOWS_NULL_VALUE);
                if (setterIntent.f(jSONObject2, "ext", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
                    JSONObject jSONObject3 = (JSONObject) setterIntent.a();
                    if (setterIntent.d(jSONObject3, "status", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
                        i2 = ((Integer) setterIntent.a()).intValue();
                    }
                    if (setterIntent.h(jSONObject3, "token", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
                        this.e = (String) setterIntent.a();
                    }
                }
                int i3 = i2;
                if ("/meta/handshake".equalsIgnoreCase(d2)) {
                    c(jSONObject2);
                } else if ("/meta/connect".equalsIgnoreCase(d2)) {
                    a(jSONObject2);
                } else if ("/meta/disconnect".equalsIgnoreCase(d2)) {
                    b(jSONObject2);
                } else {
                    this.d.a(this, a3, opt, d2, i3);
                }
                i++;
                i2 = i3;
            } catch (JSONException e2) {
                throw new d();
            }
        }
    }

    private void a(JSONObject jSONObject) {
    }

    private void b(C0001a aVar) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("channel", "/meta/handshake");
            jSONObject.put("version", "1");
            JSONArray jSONArray = new JSONArray();
            jSONArray.put("request-response");
            jSONObject.put("supportedConnectionTypes", jSONArray);
            JSONObject jSONObject2 = new JSONObject();
            JSONObject jSONObject3 = new JSONObject();
            jSONObject3.put("version", "3");
            jSONObject2.put("api", jSONObject3);
            jSONObject.put("ext", jSONObject2);
            a(aVar, this.d.b(this, jSONObject));
        } catch (JSONException e2) {
            throw new IllegalStateException();
        }
    }

    private void b(JSONObject jSONObject) {
    }

    private void c(JSONObject jSONObject) {
        try {
            this.a = jSONObject.getString("clientId");
            this.b = true;
            this.d.a(this, jSONObject);
        } catch (JSONException e2) {
            throw new d(e2);
        }
    }

    /* access modifiers changed from: package-private */
    public C0001a a() {
        return new C0001a(this.c);
    }

    /* access modifiers changed from: package-private */
    public void a(C0001a aVar) {
        a(aVar, Integer.valueOf(aVar.a), aVar.c, aVar.b, aVar.d);
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        this.c.a(str);
    }

    /* access modifiers changed from: package-private */
    public String b() {
        return this.a;
    }

    /* access modifiers changed from: package-private */
    public void b(String str) {
        this.c.b(str);
    }

    public void c(String str) {
        this.c.c(str);
    }
}
