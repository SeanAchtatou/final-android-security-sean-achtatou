package com.scoreloop.client.android.ui.component.a;

import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.controller.RequestControllerObserver;
import com.scoreloop.client.android.ui.framework.c;
import com.scoreloop.client.android.ui.framework.s;
import java.util.Collections;
import java.util.Set;

public abstract class b implements RequestControllerObserver, c {
    private boolean a;
    private final String[] b;
    private s c;

    /* access modifiers changed from: protected */
    public abstract void b(s sVar);

    /* access modifiers changed from: protected */
    public abstract void c(s sVar);

    protected b(String... strArr) {
        this.b = strArr;
    }

    public final boolean a() {
        return this.a;
    }

    /* access modifiers changed from: protected */
    public final void a(String str, Object obj) {
        this.c.b(str, obj);
    }

    public void requestControllerDidFail(RequestController requestController, Exception exc) {
        this.a = false;
    }

    public void requestControllerDidReceiveResponse(RequestController requestController) {
        this.a = false;
        b(this.c);
    }

    public void a(s sVar) {
        this.a = true;
        this.c = sVar;
        c(sVar);
    }

    public final void a(Set set) {
        Collections.addAll(set, this.b);
    }
}
