package com.scoreloop.client.android.ui.component.base;

import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import org.anddev.andengine.R;

public abstract class ComponentHeaderActivity extends ComponentActivity implements View.OnClickListener {
    private TextView a;

    /* access modifiers changed from: protected */
    public final ImageView a() {
        return (ImageView) findViewById(R.id.sl_header_image);
    }

    public void onClick(View view) {
    }

    public final void a(Bundle bundle, int i) {
        super.onCreate(bundle);
        setContentView(i);
    }

    /* access modifiers changed from: protected */
    public final void a(boolean z) {
    }

    /* access modifiers changed from: protected */
    public final void a(String str) {
        if (this.a == null) {
            Display defaultDisplay = getWindowManager().getDefaultDisplay();
            int orientation = defaultDisplay.getOrientation();
            DisplayMetrics displayMetrics = new DisplayMetrics();
            defaultDisplay.getMetrics(displayMetrics);
            if (displayMetrics.widthPixels > displayMetrics.heightPixels || orientation == 1 || orientation == 3) {
                this.a = (TextView) findViewById(R.id.sl_header_caption_land);
            } else {
                this.a = (TextView) findViewById(R.id.sl_header_caption);
            }
        }
        if (this.a != null) {
            this.a.setText(str);
        }
    }

    /* access modifiers changed from: protected */
    public final void b(String str) {
        TextView textView = (TextView) findViewById(R.id.sl_header_subtitle);
        if (textView != null) {
            textView.setText(str);
        }
    }

    /* access modifiers changed from: protected */
    public final void c(String str) {
        ((TextView) findViewById(R.id.sl_header_title)).setText(str);
    }
}
