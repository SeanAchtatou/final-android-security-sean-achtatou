package com.scoreloop.client.android.ui.component.game;

import android.os.Bundle;
import android.widget.Button;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.ui.a.k;
import com.scoreloop.client.android.ui.component.base.ComponentHeaderActivity;
import com.scoreloop.client.android.ui.component.base.d;
import org.anddev.andengine.R;

public class GameDetailHeaderActivity extends ComponentHeaderActivity {
    public void onCreate(Bundle bundle) {
        super.a(bundle, (int) R.layout.sl_header_game);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        q();
    }

    public final void a(int i) {
        Button button = (Button) findViewById(R.id.sl_control_button);
        Game C = C();
        k.a(C.getImageUrl(), getResources().getDrawable(R.drawable.sl_icon_games_loading), a());
        c(C.getName());
        b(C.getPublisherName());
        if (C.getPackageNames() != null) {
            if (d.c(this, C)) {
                button.setText(getString(R.string.sl_launch));
                button.setOnClickListener(new c(this, C));
            } else {
                button.setText(getString(R.string.sl_get));
                button.setOnClickListener(new d(this, C));
            }
            button.setVisibility(0);
            return;
        }
        button.setVisibility(8);
    }
}
