package com.scoreloop.client.android.ui.component.score;

import android.widget.ListView;
import com.scoreloop.client.android.ui.framework.ak;
import com.scoreloop.client.android.ui.framework.t;

final class f implements Runnable {
    private /* synthetic */ ScoreListActivity a;

    f(ScoreListActivity scoreListActivity) {
        this.a = scoreListActivity;
    }

    public final void run() {
        ak akVar = (ak) this.a.t();
        ListView u = this.a.u();
        if (this.a.b != -1) {
            u.setSelectionFromTop(akVar.b() + this.a.b, ScoreListActivity.c(this.a));
        } else if (this.a.c == t.PAGE_TO_TOP) {
            u.setSelection(0);
        } else if (this.a.c == t.PAGE_TO_NEXT) {
            u.setSelection(akVar.b());
        } else if (this.a.c == t.PAGE_TO_PREV) {
            u.setSelectionFromTop(akVar.c() + 1, u.getHeight());
        }
    }
}
