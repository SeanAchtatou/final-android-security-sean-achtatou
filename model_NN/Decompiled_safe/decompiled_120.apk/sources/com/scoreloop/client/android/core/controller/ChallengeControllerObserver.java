package com.scoreloop.client.android.core.controller;

public interface ChallengeControllerObserver extends RequestControllerObserver {
    void challengeControllerDidFailOnInsufficientBalance(ChallengeController challengeController);

    void challengeControllerDidFailToAcceptChallenge(ChallengeController challengeController);

    void challengeControllerDidFailToRejectChallenge(ChallengeController challengeController);
}
