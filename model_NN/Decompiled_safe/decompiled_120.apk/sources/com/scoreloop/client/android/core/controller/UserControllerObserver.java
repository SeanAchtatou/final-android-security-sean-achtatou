package com.scoreloop.client.android.core.controller;

public interface UserControllerObserver extends RequestControllerObserver {
    void userControllerDidFailOnEmailAlreadyTaken(UserController userController);

    void userControllerDidFailOnInvalidEmailFormat(UserController userController);

    void userControllerDidFailOnUsernameAlreadyTaken(UserController userController);
}
