package com.scoreloop.client.android.ui.component.challenge;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.scoreloop.client.android.core.model.Challenge;
import com.scoreloop.client.android.core.model.Money;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.ui.component.base.ComponentActivity;
import com.scoreloop.client.android.ui.component.base.m;
import com.scoreloop.client.android.ui.framework.a;
import java.math.BigDecimal;
import org.anddev.andengine.R;

public final class c extends a {
    private final Challenge a;
    private boolean b;

    public c(ComponentActivity componentActivity, Challenge challenge, boolean z) {
        super(componentActivity, null, null);
        this.a = challenge;
        this.b = z;
    }

    public final int a() {
        return 4;
    }

    public final View a(View view) {
        p pVar;
        View view2;
        Object obj;
        BigDecimal bigDecimal;
        Drawable drawable;
        if (view == null) {
            View inflate = e().inflate((int) R.layout.sl_list_item_challenge_history, (ViewGroup) null);
            p pVar2 = new p();
            pVar2.e = (ImageView) inflate.findViewById(R.id.sl_icon);
            pVar2.a = (TextView) inflate.findViewById(R.id.sl_contender_name);
            pVar2.b = (TextView) inflate.findViewById(R.id.sl_contender_score);
            pVar2.c = (TextView) inflate.findViewById(R.id.sl_contestant_name);
            pVar2.d = (TextView) inflate.findViewById(R.id.sl_contestant_score);
            pVar2.g = (LinearLayout) inflate.findViewById(R.id.sl_scores);
            pVar2.f = (TextView) inflate.findViewById(R.id.sl_prize);
            inflate.setTag(pVar2);
            view2 = inflate;
            pVar = pVar2;
        } else {
            pVar = (p) view.getTag();
            view2 = view;
        }
        if (this.a.isComplete()) {
            BigDecimal subtract = BigDecimal.ZERO.subtract(this.a.getStake().getAmount());
            if (Session.getCurrentSession().isOwnedByUser(this.a.getWinner())) {
                drawable = c().getResources().getDrawable(R.drawable.sl_icon_challenge_won);
                obj = "+";
                bigDecimal = subtract.add(this.a.getPrize().getAmount());
            } else {
                obj = "";
                bigDecimal = subtract;
                drawable = c().getResources().getDrawable(R.drawable.sl_icon_challenge_lost);
            }
            a(pVar, drawable, null, m.a(this.a.getContestantScore(), ((ComponentActivity) c()).x()), String.valueOf(obj) + m.a(new Money(bigDecimal), ((ComponentActivity) c()).x()));
        } else if (this.a.isOpen()) {
            a(pVar, null, c().getResources().getString(R.string.sl_anyone), c().getResources().getString(R.string.sl_pending), null);
        } else if (this.a.isAssigned()) {
            a(pVar, null, null, c().getResources().getString(R.string.sl_pending), null);
        } else if (this.a.isRejected()) {
            a(pVar, null, null, c().getResources().getString(R.string.sl_rejected), m.a(new Money(BigDecimal.ZERO), ((ComponentActivity) c()).x()));
        } else if (this.a.isAccepted()) {
            a(pVar, null, null, c().getResources().getString(R.string.sl_pending), null);
        }
        return view2;
    }

    public final boolean b() {
        return true;
    }

    private void a(p pVar, Drawable drawable, String str, String str2, String str3) {
        String str4;
        pVar.e.setImageDrawable(drawable != null ? drawable : c().getResources().getDrawable(R.drawable.sl_icon_challenges));
        pVar.a.setText(this.a.getContender().getDisplayName());
        pVar.b.setText(m.a(this.a.getContenderScore(), ((ComponentActivity) c()).x()));
        pVar.c.setText(str != null ? str : this.a.getContestant().getDisplayName());
        pVar.d.setText(str2 != null ? str2 : c().getResources().getString(R.string.sl_pending));
        TextView textView = pVar.f;
        if (str3 != null) {
            str4 = str3;
        } else {
            str4 = "-" + m.a(this.a.getStake(), ((ComponentActivity) c()).x());
        }
        textView.setText(str4);
        if (this.b) {
            pVar.f.setVisibility(0);
            pVar.g.setVisibility(8);
            return;
        }
        pVar.f.setVisibility(8);
        pVar.g.setVisibility(0);
    }

    /* access modifiers changed from: package-private */
    public final void a(boolean z) {
        this.b = z;
    }
}
