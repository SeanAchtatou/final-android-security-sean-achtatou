package com.scoreloop.client.android.ui.component.challenge;

import android.graphics.Bitmap;
import android.webkit.WebView;
import android.webkit.WebViewClient;

final class m extends WebViewClient {
    private boolean a;
    private /* synthetic */ ChallengePaymentActivity b;

    /* synthetic */ m(ChallengePaymentActivity challengePaymentActivity) {
        this(challengePaymentActivity, (byte) 0);
    }

    private m(ChallengePaymentActivity challengePaymentActivity, byte b2) {
        this.b = challengePaymentActivity;
    }

    public final void onPageFinished(WebView webView, String str) {
        this.b.y().b("userBalance");
        if (this.a) {
            this.b.n();
            webView.requestFocus();
            this.a = false;
        }
    }

    public final void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        if (!this.a) {
            this.b.r();
            this.a = true;
        }
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        webView.loadUrl(str);
        return true;
    }
}
