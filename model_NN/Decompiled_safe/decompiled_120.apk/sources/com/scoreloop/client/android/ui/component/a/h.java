package com.scoreloop.client.android.ui.component.a;

import com.scoreloop.client.android.core.controller.UserController;
import com.scoreloop.client.android.core.model.Entity;
import com.scoreloop.client.android.ui.framework.s;

public final class h extends b {
    public static final String[] a = {"userBuddies"};
    private UserController b;

    public h() {
        super(a);
    }

    /* access modifiers changed from: protected */
    public final void b(s sVar) {
        a("userBuddies", this.b.getUser().getBuddyUsers());
    }

    /* access modifiers changed from: protected */
    public final void c(s sVar) {
        this.b = new UserController(this);
        this.b.setUser((Entity) sVar.a("user"));
        this.b.loadBuddies();
    }
}
