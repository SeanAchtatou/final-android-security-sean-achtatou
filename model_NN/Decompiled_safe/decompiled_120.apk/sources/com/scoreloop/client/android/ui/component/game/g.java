package com.scoreloop.client.android.ui.component.game;

import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.ui.component.base.ComponentActivity;
import com.scoreloop.client.android.ui.component.base.k;
import org.anddev.andengine.R;

public final class g extends k {
    public g(ComponentActivity componentActivity, User user) {
        super(componentActivity, componentActivity.getResources().getDrawable(R.drawable.sl_icon_user), user.getDisplayName(), null, user);
    }

    /* access modifiers changed from: protected */
    public final String i() {
        return ((User) p()).getImageUrl();
    }

    /* access modifiers changed from: protected */
    public final int h() {
        return R.layout.sl_list_item_user;
    }

    public final int a() {
        return 14;
    }
}
