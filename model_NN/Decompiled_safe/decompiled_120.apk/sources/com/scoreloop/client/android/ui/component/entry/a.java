package com.scoreloop.client.android.ui.component.entry;

import android.content.Context;
import android.content.res.Resources;
import com.scoreloop.client.android.ui.component.base.n;
import com.scoreloop.client.android.ui.component.base.o;
import com.scoreloop.client.android.ui.framework.i;
import org.anddev.andengine.R;

final class a extends i {
    private /* synthetic */ EntryListActivity b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public a(EntryListActivity entryListActivity, Context context) {
        super(context);
        this.b = entryListActivity;
        Resources resources = context.getResources();
        entryListActivity.x();
        add(new n(context, entryListActivity.C().getName()));
        entryListActivity.c = new b(entryListActivity, resources.getDrawable(R.drawable.sl_icon_leaderboards), context.getString(R.string.sl_leaderboards));
        add(entryListActivity.c);
        if (com.scoreloop.client.android.ui.component.base.a.a(o.ACHIEVEMENT)) {
            entryListActivity.a = new b(entryListActivity, resources.getDrawable(R.drawable.sl_icon_achievements), context.getString(R.string.sl_achievements));
            add(entryListActivity.a);
        }
        if (com.scoreloop.client.android.ui.component.base.a.a(o.CHALLENGE)) {
            entryListActivity.b = new b(entryListActivity, resources.getDrawable(R.drawable.sl_icon_challenges), context.getString(R.string.sl_challenges));
            add(entryListActivity.b);
        }
        if (com.scoreloop.client.android.ui.component.base.a.a(o.NEWS)) {
            entryListActivity.d = new b(entryListActivity, resources.getDrawable(R.drawable.sl_icon_news_closed), context.getString(R.string.sl_news));
            add(entryListActivity.d);
        }
    }
}
