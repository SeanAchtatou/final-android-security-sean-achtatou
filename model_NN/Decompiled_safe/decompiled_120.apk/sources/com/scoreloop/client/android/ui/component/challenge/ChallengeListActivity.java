package com.scoreloop.client.android.ui.component.challenge;

import android.os.Bundle;
import android.widget.ListAdapter;
import com.scoreloop.client.android.core.controller.ChallengesController;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.model.Challenge;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.ui.component.base.ComponentListActivity;
import com.scoreloop.client.android.ui.component.base.e;
import com.scoreloop.client.android.ui.component.base.l;
import com.scoreloop.client.android.ui.component.base.n;
import com.scoreloop.client.android.ui.framework.a;
import com.scoreloop.client.android.ui.framework.h;
import com.scoreloop.client.android.ui.framework.i;
import com.scoreloop.client.android.ui.framework.s;
import java.util.Iterator;
import java.util.List;
import org.anddev.andengine.R;

public class ChallengeListActivity extends ComponentListActivity {
    private List a;
    private l b;
    private l c;
    private List d;
    private ChallengesController e;
    private List f;
    private ChallengesController g;
    private boolean h;
    private boolean i = true;
    private boolean j = true;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        a((ListAdapter) new i(this));
        this.g = new ChallengesController(E());
        this.e = new ChallengesController(E());
        a(s.a("sessionUserValues", "userBuddies"));
    }

    public final void a(a aVar) {
        e A = A();
        if (aVar == this.b) {
            this.i = false;
            a();
        } else if (aVar == this.c) {
            this.j = false;
            a();
        } else if (aVar.a() == 6) {
            a(A.b((Challenge) ((o) aVar).p()));
        } else if (aVar.a() == 5) {
            a(A.b((User) ((d) aVar).p()));
        } else if (aVar.a() == 4) {
            this.h = !this.h;
            i t = t();
            for (int i2 = 0; i2 < t.getCount(); i2++) {
                a aVar2 = (a) t.getItem(i2);
                if (aVar2.a() == 4) {
                    ((c) aVar2).a(this.h);
                }
            }
            t.notifyDataSetChanged();
        }
    }

    public final void a(int i2) {
        b(this.g);
        this.g.loadOpenChallenges();
        b(this.e);
        this.e.loadChallengeHistory();
    }

    public void onStart() {
        super.onStart();
        q();
    }

    public final void a(s sVar, String str, Object obj, Object obj2) {
        if (obj2 instanceof List) {
            this.a = (List) obj2;
            a();
        }
    }

    public final void a(s sVar, String str) {
        z().a("userBuddies", h.NOT_DIRTY, (Object) null);
    }

    public final void a(RequestController requestController) {
        if (requestController == this.e) {
            this.d = this.e.getChallenges();
        } else if (requestController == this.g) {
            this.f = this.g.getChallenges();
        }
        a();
    }

    private void a() {
        boolean z;
        if (this.f != null && this.d != null && this.a != null) {
            i t = t();
            t.clear();
            t.add(new n(this, getString(R.string.sl_open_challenges)));
            if (this.f.size() > 0) {
                Iterator it = this.f.iterator();
                int i2 = 0;
                while (true) {
                    if (it.hasNext()) {
                        Challenge challenge = (Challenge) it.next();
                        if (this.j && (i2 = i2 + 1) > 2) {
                            this.c = new l(this);
                            t.add(this.c);
                            break;
                        }
                        t.add(new o(this, challenge));
                    } else {
                        break;
                    }
                }
            } else {
                t.add(new com.scoreloop.client.android.ui.component.base.h(this, getResources().getString(R.string.sl_no_open_challenges)));
            }
            t.add(new n(this, getString(R.string.sl_challenges_history)));
            if (this.d.size() > 0) {
                Iterator it2 = this.d.iterator();
                boolean z2 = false;
                int i3 = 0;
                while (true) {
                    if (!it2.hasNext()) {
                        z = z2;
                        break;
                    }
                    Challenge challenge2 = (Challenge) it2.next();
                    if (challenge2.isComplete() || challenge2.isOpen() || challenge2.isAssigned() || challenge2.isRejected() || challenge2.isAccepted()) {
                        z2 = true;
                        if (this.i && (i3 = i3 + 1) > 2) {
                            this.b = new l(this);
                            t.add(this.b);
                            z = true;
                            break;
                        }
                        t.add(new c(this, challenge2, this.h));
                    }
                }
                if (!z) {
                    t.add(new com.scoreloop.client.android.ui.component.base.h(this, getResources().getString(R.string.sl_no_history_challenges)));
                }
            } else {
                t.add(new com.scoreloop.client.android.ui.component.base.h(this, getResources().getString(R.string.sl_no_history_challenges)));
            }
            t.add(new n(this, getString(R.string.sl_new_challenge)));
            t.add(new d(this, null));
            for (User dVar : this.a) {
                t.add(new d(this, dVar));
            }
        }
    }
}
