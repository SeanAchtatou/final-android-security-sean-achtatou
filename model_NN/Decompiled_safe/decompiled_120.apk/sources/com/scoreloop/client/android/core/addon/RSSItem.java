package com.scoreloop.client.android.core.addon;

import android.content.Context;
import android.content.SharedPreferences;
import java.util.Iterator;
import java.util.List;

public class RSSItem {
    private final Context a;
    private String b;
    private String c;
    private String d;
    private boolean e;
    private String f;
    private String g;
    private Boolean h;

    RSSItem(Context context) {
        if (context == null) {
            throw new IllegalArgumentException("context must not be null");
        }
        this.a = context;
    }

    public static void resetPersistentReadFlags(Context context, List list) {
        SharedPreferences.Editor edit = context.getSharedPreferences("com.scoreloop.feed", 0).edit();
        edit.clear();
        edit.commit();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            ((RSSItem) it.next()).h = null;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        this.b = str;
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z) {
        this.e = z;
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        try {
            return (getTitle() == null || getIdentifier() == null) ? false : true;
        } catch (Exception e2) {
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public void b(String str) {
        if (str == null) {
            throw new IllegalArgumentException("identifier must not be null");
        }
        this.c = str;
    }

    /* access modifiers changed from: package-private */
    public void c(String str) {
        this.d = str;
    }

    /* access modifiers changed from: package-private */
    public void d(String str) {
        this.f = str;
    }

    /* access modifiers changed from: package-private */
    public void e(String str) {
        this.g = str;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        return getIdentifier().equals(((RSSItem) obj).getIdentifier());
    }

    public String getDescription() {
        return this.b;
    }

    public String getIdentifier() {
        if (this.c != null) {
            return this.c;
        }
        throw new IllegalStateException("identifier of RSSItem must not be null");
    }

    public String getImageUrlString() {
        return this.d;
    }

    public String getLinkUrlString() {
        return this.f;
    }

    public String getTitle() {
        return this.g;
    }

    public boolean hasPersistentReadFlag() {
        if (this.h != null) {
            return this.h.booleanValue();
        }
        this.h = Boolean.valueOf(this.a.getSharedPreferences("com.scoreloop.feed", 0).getBoolean(getIdentifier(), false));
        return this.h.booleanValue();
    }

    public int hashCode() {
        return getIdentifier().hashCode();
    }

    public boolean isSticky() {
        return this.e;
    }

    public void setHasPersistentReadFlag(boolean z) {
        if (hasPersistentReadFlag() != z) {
            SharedPreferences.Editor edit = this.a.getSharedPreferences("com.scoreloop.feed", 0).edit();
            edit.putBoolean(getIdentifier(), z);
            edit.commit();
            this.h = Boolean.valueOf(z);
        }
    }
}
