package com.scoreloop.client.android.ui.component.base;

import android.util.Log;

final class c extends IllegalStateException {
    c(String str) {
        super(str);
        Log.e("ScoreloopUI", "=====================================================================================");
        Log.e("ScoreloopUI", "scoreloop.properties file verification error. Please resolve any issues first!");
        Log.e("ScoreloopUI", str);
    }
}
