package com.scoreloop.client.android.core.model;

import java.util.ArrayList;
import java.util.List;
import org.anddev.andengine.entity.layer.tiled.tmx.util.constants.TMXConstants;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Message {
    private final List a = new ArrayList();
    private MessageTargetInterface b;
    private String c;
    private Type d = Type.TARGET_INFERRED;

    public enum Type {
        ABUSE_REPORT("abuse_report"),
        RECOMMENDATION("recommendation"),
        TARGET_INFERRED(null);
        
        private final String a;

        private Type(String str) {
            this.a = str;
        }

        public final String getTypeString() {
            return this.a;
        }
    }

    private JSONArray g() {
        JSONArray jSONArray = new JSONArray();
        for (MessageReceiver b2 : b()) {
            try {
                jSONArray.put(b2.b());
            } catch (JSONException e) {
                throw new IllegalStateException(e);
            }
        }
        return jSONArray;
    }

    private JSONObject h() {
        if (c() == null) {
            throw new IllegalStateException();
        }
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put(TMXConstants.TAG_TILE_ATTRIBUTE_ID, c().getIdentifier());
            jSONObject.put("target_type", c().a());
            return jSONObject;
        } catch (JSONException e) {
            throw new IllegalStateException(e);
        }
    }

    public Message a() {
        Message message = new Message();
        message.a(c());
        message.a(e());
        message.a(d());
        for (MessageReceiver a2 : b()) {
            message.a(a2);
        }
        return message;
    }

    public void a(Type type) {
        this.d = type;
    }

    public void a(MessageReceiver messageReceiver) {
        if (!this.a.contains(messageReceiver)) {
            this.a.add(messageReceiver);
        }
    }

    public void a(MessageTargetInterface messageTargetInterface) {
        if (messageTargetInterface == null) {
            throw new IllegalArgumentException();
        }
        this.b = messageTargetInterface;
    }

    public void a(String str) {
        this.c = str;
    }

    public List b() {
        return this.a;
    }

    public MessageTargetInterface c() {
        return this.b;
    }

    public String d() {
        return this.c;
    }

    public Type e() {
        return this.d;
    }

    public JSONObject f() {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("target", h());
        jSONObject.put("receivers", g());
        if (d() != null) {
            jSONObject.put("text", d());
        }
        String typeString = e().getTypeString();
        if (typeString != null) {
            jSONObject.put("message_type", typeString);
        }
        return jSONObject;
    }
}
