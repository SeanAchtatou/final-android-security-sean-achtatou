package com.scoreloop.client.android.core.model;

import com.scoreloop.client.android.core.util.Formats;
import com.scoreloop.client.android.core.util.SetterIntent;
import java.util.Date;
import org.anddev.andengine.entity.layer.tiled.tmx.util.constants.TMXConstants;
import org.json.JSONObject;

public class Activity extends BaseEntity {
    public static String a = "activity";
    private final Session c;
    private Date d;
    private String e;
    private String f;
    private String g;
    private String h;
    private String i;
    private Entity j;
    private Entity k;

    public enum ImageSize {
        SMALL("small"),
        MEDIUM("medium"),
        LARGE("large");
        
        private final String a;

        private ImageSize(String str) {
            this.a = str;
        }

        private String a() {
            return this.a;
        }
    }

    public Activity(Session session, JSONObject jSONObject) {
        this.c = session;
        a(jSONObject);
    }

    private String c() {
        return this.f;
    }

    public String a() {
        return a;
    }

    /* access modifiers changed from: protected */
    public void a(Object obj) {
        super.a(obj);
        a(obj, "date", getDate());
        a(obj, "message", getMessage());
        a(obj, TMXConstants.TAG_OBJECT_ATTRIBUTE_TYPE, c());
    }

    public void a(JSONObject jSONObject) {
        super.a(jSONObject);
        SetterIntent setterIntent = new SetterIntent();
        if (setterIntent.h(jSONObject, "message", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.e = (String) setterIntent.a();
        }
        if (setterIntent.h(jSONObject, "time_ago", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.i = (String) setterIntent.a();
        }
        if (setterIntent.b(jSONObject, "updated_at", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.d = (Date) setterIntent.a();
        }
        this.f = setterIntent.d(jSONObject, "target_type", SetterIntent.KeyMode.USE_NULL_WHEN_NO_KEY, SetterIntent.ValueMode.ALLOWS_NULL_VALUE);
        if (setterIntent.h(jSONObject, "user_id", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.h = (String) setterIntent.a();
        }
        if (setterIntent.h(jSONObject, "game_id", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.g = (String) setterIntent.a();
        }
    }

    public JSONObject d() {
        JSONObject d2 = super.d();
        d2.put("message", getMessage());
        d2.put("updated_at", Formats.a.format(getDate()));
        d2.put("target_type", c());
        return d2;
    }

    public Date getDate() {
        return this.d;
    }

    public Entity getGame() {
        if (this.j == null) {
            this.j = Game.a(this.c, this.g);
        }
        return this.j;
    }

    public String getImageUrl(ImageSize imageSize) {
        return "http://p.scoreloop.com" + String.format("/activities/%s/image?size=%s", getIdentifier(), ImageSize.a(imageSize));
    }

    public String getMessage() {
        return this.e;
    }

    public String getTimeAgo() {
        return this.i;
    }

    public Entity getUser() {
        if (this.k == null) {
            this.k = User.a(this.c, this.h);
        }
        return this.k;
    }
}
