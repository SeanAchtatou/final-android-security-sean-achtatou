package com.scoreloop.client.android.ui.component.achievement;

import com.scoreloop.client.android.core.controller.AchievementController;
import com.scoreloop.client.android.core.controller.AchievementsController;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.controller.RequestControllerObserver;
import com.scoreloop.client.android.core.model.Achievement;
import java.util.ArrayList;
import java.util.List;

public final class a implements RequestControllerObserver {
    private AchievementController a;
    private AchievementsController b;
    private boolean c;
    private boolean d;
    private List e = new ArrayList();
    /* access modifiers changed from: private */
    public List f = new ArrayList();

    public final AchievementsController a() {
        if (this.b == null) {
            this.b = new AchievementsController(this);
        }
        return this.b;
    }

    public final boolean b() {
        return a().getAchievements().size() > 0;
    }

    private void c() {
        List<Runnable> list = this.e;
        this.e = new ArrayList();
        for (Runnable run : list) {
            run.run();
        }
    }

    /* access modifiers changed from: private */
    public void d() {
        List<Runnable> list = this.f;
        this.f = new ArrayList();
        for (Runnable run : list) {
            run.run();
        }
    }

    public final void requestControllerDidFail(RequestController requestController, Exception exc) {
        if (requestController == this.b) {
            this.c = false;
            c();
        } else if (requestController == this.a) {
            this.d = false;
            d();
        }
    }

    public final void requestControllerDidReceiveResponse(RequestController requestController) {
        if (requestController == this.b) {
            this.c = false;
            c();
        } else if (requestController == this.a) {
            this.d = false;
            e();
        }
    }

    public final void a(boolean z, Runnable runnable) {
        if (!b() || (!a().hadInitialSync() && z)) {
            c cVar = new c(this, z, runnable);
            if (!b() || (z && !a().hadInitialSync())) {
                this.e.add(cVar);
                if (!this.c) {
                    this.c = true;
                    a().setForceInitialSync(z);
                    a().loadAchievements();
                    return;
                }
                return;
            }
            cVar.run();
            return;
        }
        if (runnable != null) {
            this.f.add(runnable);
        }
        if (!this.d) {
            e();
        }
    }

    private void e() {
        for (Achievement achievement : a().getAchievements()) {
            if (achievement.needsSubmit()) {
                if (this.a == null) {
                    this.a = new AchievementController(this);
                }
                this.a.setAchievement(achievement);
                this.d = true;
                this.a.submitAchievement();
                return;
            }
        }
        d();
    }
}
