package com.scoreloop.client.android.ui.component.challenge;

import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.scoreloop.client.android.core.model.Challenge;
import com.scoreloop.client.android.ui.component.base.ComponentActivity;
import com.scoreloop.client.android.ui.framework.a;
import org.anddev.andengine.R;

final class n extends a {
    private final Challenge a;
    /* access modifiers changed from: private */
    public boolean b = true;
    /* access modifiers changed from: private */
    public j c;

    public n(ComponentActivity componentActivity, Challenge challenge, j jVar) {
        super(componentActivity, null, null);
        this.a = challenge;
        this.c = jVar;
    }

    public final int a() {
        return 3;
    }

    public final View a(View view) {
        View view2;
        if (view == null) {
            view2 = e().inflate((int) R.layout.sl_list_item_challenge_controls, (ViewGroup) null);
        } else {
            view2 = view;
        }
        Button button = (Button) view2.findViewById(R.id.control1);
        button.setOnClickListener(new a(this));
        Button button2 = (Button) view2.findViewById(R.id.control2);
        button2.setOnClickListener(new b(this));
        if (this.a == null) {
            button.setText(c().getResources().getString(R.string.sl_create_challenge));
        } else {
            button.setText(c().getResources().getString(R.string.sl_accept_start_challenge));
            if (this.a.isAssigned()) {
                button2.setText(c().getResources().getString(R.string.sl_reject_challenge));
                button2.setVisibility(0);
                return view2;
            }
        }
        button2.setVisibility(8);
        return view2;
    }

    public final boolean b() {
        return false;
    }
}
