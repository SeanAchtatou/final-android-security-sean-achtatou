package com.scoreloop.client.android.ui.component.challenge;

import android.widget.SeekBar;

final class k implements SeekBar.OnSeekBarChangeListener {
    private /* synthetic */ e a;

    k(e eVar) {
        this.a = eVar;
    }

    public final void onProgressChanged(SeekBar seekBar, int i, boolean z) {
        this.a.c = i;
        this.a.a(((f) seekBar.getTag()).a);
    }

    public final void onStartTrackingTouch(SeekBar seekBar) {
    }

    public final void onStopTrackingTouch(SeekBar seekBar) {
    }
}
