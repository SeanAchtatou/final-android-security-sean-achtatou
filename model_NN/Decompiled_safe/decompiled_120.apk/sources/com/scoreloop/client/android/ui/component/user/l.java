package com.scoreloop.client.android.ui.component.user;

import com.scoreloop.client.android.ui.component.base.ComponentActivity;
import com.scoreloop.client.android.ui.component.base.k;
import org.anddev.andengine.R;

public final class l extends k {
    public l(ComponentActivity componentActivity) {
        super(componentActivity, null, componentActivity.getString(R.string.sl_add_friends), null, null);
    }

    /* access modifiers changed from: protected */
    public final int h() {
        return R.layout.sl_list_item_user_add_buddies;
    }

    public final int a() {
        return 25;
    }
}
