package com.scoreloop.client.android.ui.a;

import java.lang.ref.SoftReference;
import java.util.LinkedHashMap;
import java.util.Map;

final class b extends LinkedHashMap {
    private /* synthetic */ m a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    b(m mVar, int i) {
        super(i, 0.75f, true);
        this.a = mVar;
    }

    /* access modifiers changed from: protected */
    public final boolean removeEldestEntry(Map.Entry entry) {
        if (size() <= this.a.a) {
            return false;
        }
        this.a.d.put(entry.getKey(), new SoftReference((f) entry.getValue()));
        return true;
    }
}
