package com.scoreloop.client.android.ui.component.profile;

import android.content.Context;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.ui.component.base.n;
import com.scoreloop.client.android.ui.framework.i;
import org.anddev.andengine.R;

final class g extends i {
    private /* synthetic */ ProfileSettingsListActivity b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public g(ProfileSettingsListActivity profileSettingsListActivity, Context context) {
        super(context);
        this.b = profileSettingsListActivity;
        add(new n(context, profileSettingsListActivity.getString(R.string.sl_account_settings)));
        add(profileSettingsListActivity.c);
        add(profileSettingsListActivity.d);
        if (Session.getCurrentSession().getUser().getEmailAddress() != null) {
            add(profileSettingsListActivity.a);
        }
        add(profileSettingsListActivity.b);
    }
}
