package com.scoreloop.client.android.ui.component.base;

import android.content.Context;
import android.util.Log;
import com.scoreloop.client.android.core.controller.AchievementsController;
import com.scoreloop.client.android.core.model.Client;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.Score;
import com.scoreloop.client.android.core.model.ScoreFormatter;
import com.scoreloop.client.android.core.model.Session;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.IllegalFormatException;
import java.util.Properties;

public final class a {
    private int a;
    private String b = "%.2f %s";
    private String c;
    private ScoreFormatter.ScoreFormatKey d;
    private ScoreFormatter.ScoreFormatKey e;
    private ScoreFormatter.ScoreFormatKey f;
    private ScoreFormatter g;
    private String[] h;
    private boolean i = true;

    public a(Context context, Session session) {
        Properties properties = Client.getProperties(context);
        o[] values = o.values();
        for (o oVar : values) {
            String a2 = oVar.a();
            String property = properties.getProperty(a2);
            if (property != null) {
                oVar.a(a(property.trim(), a2));
            }
        }
        String property2 = properties.getProperty("ui.feature.achievement.forceSync");
        if (property2 != null) {
            this.i = a(property2.trim(), "ui.feature.achievement.forceSync");
        }
        this.c = properties.getProperty("ui.format.score.result");
        if (this.c != null) {
            this.c = this.c.trim();
        }
        this.g = new ScoreFormatter(properties.getProperty("ui.format.score"));
        this.d = a(properties, "ui.format.score.leaderboard");
        this.e = a(properties, "ui.format.score.challenges");
        this.f = a(properties, "ui.format.score.socialnetworkpost");
        Game game = session.getGame();
        if (game == null || !game.hasModes()) {
            this.h = new String[0];
        } else {
            int intValue = game.getMinMode().intValue();
            int intValue2 = game.getModeCount().intValue();
            this.h = new String[intValue2];
            for (int i2 = intValue; i2 < intValue + intValue2; i2++) {
                this.h[i2] = this.g.formatScore(new Score(null, Collections.singletonMap(Game.CONTEXT_KEY_MODE, Integer.valueOf(i2))), ScoreFormatter.ScoreFormatKey.ModeOnlyFormat);
            }
        }
        this.b = properties.getProperty("ui.format.money", this.b).trim();
        String property3 = properties.getProperty("ui.res.modes.name");
        if (property3 != null) {
            this.a = context.getResources().getIdentifier(property3.trim(), "array", context.getPackageName());
            Log.i("test", "Type: " + context.getResources().getResourceTypeName(this.a));
        }
        a(context, session);
    }

    public final int a() {
        return this.a;
    }

    public final String[] b() {
        return this.h;
    }

    public final String c() {
        return this.b;
    }

    public final ScoreFormatter.ScoreFormatKey d() {
        return this.d;
    }

    public final ScoreFormatter.ScoreFormatKey e() {
        return this.e;
    }

    public final ScoreFormatter.ScoreFormatKey f() {
        return this.f;
    }

    public final String g() {
        return this.c;
    }

    public final ScoreFormatter h() {
        return this.g;
    }

    public static boolean a(o oVar) {
        return oVar.b();
    }

    private static ScoreFormatter.ScoreFormatKey a(Properties properties, String str) {
        String property = properties.getProperty(str);
        if (property == null) {
            return null;
        }
        ScoreFormatter.ScoreFormatKey parse = ScoreFormatter.ScoreFormatKey.parse(property);
        if (parse != null) {
            return parse;
        }
        throw new c("invalid " + str + " value (unrecognized format key): " + property);
    }

    private static boolean a(String str, String str2) {
        if (str.equalsIgnoreCase("false")) {
            return false;
        }
        if (str.equalsIgnoreCase("true")) {
            return true;
        }
        throw new c("property " + str2 + " must be either 'true' or 'false'");
    }

    private void a(Context context, Session session) {
        if (!o.a(o.ACHIEVEMENT) || new AchievementsController(new i(this)).getAwardList() != null) {
            Game game = session.getGame();
            if (game != null && game.hasModes()) {
                int intValue = game.getModeCount().intValue();
                if (this.a == 0) {
                    int intValue2 = session.getGame().getMinMode().intValue();
                    String[] definedModesNames = this.g.getDefinedModesNames(intValue2, intValue);
                    for (int i2 = 0; i2 < definedModesNames.length; i2++) {
                        if (definedModesNames[i2] == null) {
                            throw new c("no name configured for mode " + (intValue2 + i2) + " - check " + "ui.format.score");
                        }
                    }
                } else {
                    String[] stringArray = context.getResources().getStringArray(this.a);
                    if (stringArray == null || stringArray.length != intValue) {
                        throw new c("your modes string array must have exactily " + intValue + " entries!");
                    }
                }
            }
            try {
                String.format(this.b, BigDecimal.ONE, "$");
            } catch (IllegalFormatException e2) {
                throw new c("invalid ui.format.money value: must contain valid %f and %s specifiers in that order. " + e2.getLocalizedMessage());
            }
        } else {
            throw new c("when you enable the achievement feature you also have to provide an SLAwards.bundle in the assets folder");
        }
    }
}
