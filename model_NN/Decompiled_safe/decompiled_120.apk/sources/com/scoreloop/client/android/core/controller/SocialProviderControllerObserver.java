package com.scoreloop.client.android.core.controller;

public interface SocialProviderControllerObserver {
    void socialProviderControllerDidCancel(SocialProviderController socialProviderController);

    void socialProviderControllerDidEnterInvalidCredentials(SocialProviderController socialProviderController);

    void socialProviderControllerDidFail(SocialProviderController socialProviderController, Throwable th);

    void socialProviderControllerDidSucceed(SocialProviderController socialProviderController);
}
