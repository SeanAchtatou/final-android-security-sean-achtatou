package com.scoreloop.client.android.ui.a;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.widget.ImageView;
import java.lang.ref.WeakReference;

final class j extends AsyncTask {
    /* access modifiers changed from: private */
    public String a;
    private final WeakReference b;
    private final Drawable c = null;
    private /* synthetic */ k d;

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object doInBackground(Object... objArr) {
        ImageView imageView = (ImageView) this.b.get();
        this.a = ((String[]) objArr)[0];
        if (imageView != null) {
            return k.a(imageView.getContext(), this.a);
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        g gVar = (g) obj;
        Bitmap c2 = (isCancelled() || gVar == null) ? null : gVar.c();
        if (gVar != null && gVar.d()) {
            k.b.a(this.a, c2);
        }
        if (this.b != null) {
            ImageView imageView = (ImageView) this.b.get();
            if (this != k.b(imageView)) {
                return;
            }
            if (c2 != null || this.c == null) {
                imageView.setImageBitmap(c2);
            } else {
                imageView.setImageDrawable(this.c);
            }
        }
    }

    public j(k kVar, ImageView imageView) {
        this.d = kVar;
        this.b = new WeakReference(imageView);
    }
}
