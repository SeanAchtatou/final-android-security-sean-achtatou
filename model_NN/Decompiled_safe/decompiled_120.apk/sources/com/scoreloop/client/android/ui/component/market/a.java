package com.scoreloop.client.android.ui.component.market;

import android.content.Context;
import com.scoreloop.client.android.ui.component.base.n;
import com.scoreloop.client.android.ui.framework.i;
import org.anddev.andengine.R;

final class a extends i {
    private /* synthetic */ MarketListActivity b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public a(MarketListActivity marketListActivity, Context context) {
        super(context);
        this.b = marketListActivity;
        add(new n(context, context.getString(R.string.sl_market)));
        add(marketListActivity.c);
        add(marketListActivity.d);
        add(marketListActivity.a);
        add(new n(context, context.getString(R.string.sl_playing)));
        add(marketListActivity.b);
    }
}
