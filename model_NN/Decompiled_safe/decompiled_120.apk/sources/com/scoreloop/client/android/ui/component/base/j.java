package com.scoreloop.client.android.ui.component.base;

import android.view.View;
import android.view.WindowManager;
import com.scoreloop.client.android.ui.framework.a;

public abstract class j {
    public static int a(View view, a aVar) {
        View a = aVar.a((View) null);
        a.measure(0, 0);
        int height = view.getHeight();
        if (height == 0) {
            height = ((WindowManager) view.getContext().getSystemService("window")).getDefaultDisplay().getHeight();
        }
        return Math.max(10, (height / a.getMeasuredHeight()) + 1);
    }
}
