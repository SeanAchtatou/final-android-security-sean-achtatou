package com.scoreloop.client.android.ui.component.achievement;

import android.graphics.drawable.BitmapDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.scoreloop.client.android.core.model.Achievement;
import com.scoreloop.client.android.core.model.Award;
import com.scoreloop.client.android.core.model.Money;
import com.scoreloop.client.android.ui.component.base.ComponentActivity;
import com.scoreloop.client.android.ui.component.base.m;
import com.scoreloop.client.android.ui.framework.a;
import org.anddev.andengine.R;

public final class d extends a {
    private final Achievement a;
    private final boolean b;
    private final String c;

    public d(ComponentActivity componentActivity, Achievement achievement, boolean z) {
        super(componentActivity, new BitmapDrawable(achievement.getImage()), achievement.getAward().getLocalizedTitle());
        String str;
        com.scoreloop.client.android.ui.component.base.a x = componentActivity.x();
        Award award = achievement.getAward();
        String localizedDescription = award.getLocalizedDescription();
        Money rewardMoney = award.getRewardMoney();
        if (rewardMoney == null || !rewardMoney.hasAmount()) {
            str = localizedDescription;
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append(m.a(rewardMoney, x)).append("\n").append(localizedDescription);
            str = sb.toString();
        }
        this.c = str;
        this.a = achievement;
        this.b = z;
    }

    public final Achievement g() {
        return this.a;
    }

    public final int a() {
        return 1;
    }

    public final View a(View view) {
        View view2;
        if (view == null) {
            view2 = e().inflate((int) R.layout.sl_list_item_achievement, (ViewGroup) null);
        } else {
            view2 = view;
        }
        ((ImageView) view2.findViewById(R.id.sl_list_item_achievement_icon)).setImageDrawable(d());
        ((TextView) view2.findViewById(R.id.sl_list_item_achievement_title)).setText(f());
        ((TextView) view2.findViewById(R.id.sl_list_item_achievement_description)).setText(this.c);
        view2.findViewById(R.id.sl_list_item_achievement_accessory).setVisibility(b() ? 0 : 4);
        return view2;
    }

    public final boolean b() {
        return this.b && this.a.isAchieved() && this.a.getIdentifier() != null;
    }
}
