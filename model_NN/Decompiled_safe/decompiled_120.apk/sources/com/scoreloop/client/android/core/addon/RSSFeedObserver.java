package com.scoreloop.client.android.core.addon;

public interface RSSFeedObserver {
    void feedDidFailToReceiveNextItem(RSSFeed rSSFeed, Exception exc);

    void feedDidReceiveNextItem(RSSFeed rSSFeed, RSSItem rSSItem);

    void feedDidRequestNextItem(RSSFeed rSSFeed);
}
