package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.model.Device;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.Money;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.SocialProvider;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.RequestCompletionCallback;
import com.scoreloop.client.android.core.server.RequestMethod;
import com.scoreloop.client.android.core.server.Response;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.anddev.andengine.util.constants.TimeConstants;
import org.json.JSONException;
import org.json.JSONObject;

class f extends RequestController {
    /* access modifiers changed from: private */
    public b c;
    private final RequestControllerObserver d = new a();

    final class a implements RequestControllerObserver {
        static final /* synthetic */ boolean a = (!f.class.desiredAssertionStatus());

        private a() {
        }

        public final void requestControllerDidFail(RequestController requestController, Exception exc) {
            Session h = f.this.h();
            if (a || h.c() == Session.State.AUTHENTICATING) {
                h.a(Session.State.FAILED);
                f.this.a(exc);
                b unused = f.this.c = (b) null;
                return;
            }
            throw new AssertionError();
        }

        public final void requestControllerDidReceiveResponse(RequestController requestController) {
            Session h = f.this.h();
            if (!a && h.c() != Session.State.AUTHENTICATING) {
                throw new AssertionError();
            } else if (h.a().getIdentifier() != null) {
                h.getUser().a(h.a().getIdentifier());
                b unused = f.this.c = (b) null;
            } else {
                throw new IllegalStateException();
            }
        }
    }

    class b extends Request {
        private final Device a;
        private final Game b;

        public b(RequestCompletionCallback requestCompletionCallback, Game game, Device device) {
            super(requestCompletionCallback);
            this.b = game;
            this.a = device;
        }

        public String a() {
            if (this.b == null) {
                return "/service/session";
            }
            return String.format("/service/games/%s/session", this.b.getIdentifier());
        }

        public JSONObject b() {
            JSONObject jSONObject = new JSONObject();
            JSONObject jSONObject2 = new JSONObject();
            try {
                jSONObject2.put("device_id", this.a.getIdentifier());
                jSONObject.put("user", jSONObject2);
                return jSONObject;
            } catch (JSONException e) {
                throw new IllegalStateException("Invalid device data");
            }
        }

        public RequestMethod c() {
            return RequestMethod.POST;
        }
    }

    f(Session session, RequestControllerObserver requestControllerObserver) {
        super(session, requestControllerObserver);
    }

    private List a(Money money) {
        String d2 = money.d();
        return money.compareTo(new Money(d2, new BigDecimal(10000))) < 0 ? a(d2) : money.compareTo(new Money(d2, new BigDecimal(100000))) < 0 ? c(d2) : b(d2);
    }

    private List a(String str) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new Money(str, new BigDecimal(100)));
        arrayList.add(new Money(str, new BigDecimal(200)));
        arrayList.add(new Money(str, new BigDecimal(500)));
        arrayList.add(new Money(str, new BigDecimal((int) TimeConstants.MILLISECONDSPERSECOND)));
        arrayList.add(new Money(str, new BigDecimal(2000)));
        arrayList.add(new Money(str, new BigDecimal(5000)));
        return arrayList;
    }

    private List b(String str) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new Money(str, new BigDecimal((int) TimeConstants.MILLISECONDSPERSECOND)));
        arrayList.add(new Money(str, new BigDecimal(2500)));
        arrayList.add(new Money(str, new BigDecimal(5000)));
        arrayList.add(new Money(str, new BigDecimal(10000)));
        arrayList.add(new Money(str, new BigDecimal(50000)));
        arrayList.add(new Money(str, new BigDecimal(100000)));
        return arrayList;
    }

    private List c(String str) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new Money(str, new BigDecimal(500)));
        arrayList.add(new Money(str, new BigDecimal((int) TimeConstants.MILLISECONDSPERSECOND)));
        arrayList.add(new Money(str, new BigDecimal(2500)));
        arrayList.add(new Money(str, new BigDecimal(5000)));
        arrayList.add(new Money(str, new BigDecimal(10000)));
        arrayList.add(new Money(str, new BigDecimal(20000)));
        return arrayList;
    }

    /* access modifiers changed from: package-private */
    public boolean a(Request request, Response response) {
        Session h = h();
        int f = response.f();
        JSONObject e = response.e();
        JSONObject optJSONObject = e.optJSONObject(User.a);
        if ((f == 200 || f == 201) && optJSONObject != null) {
            User user = h.getUser();
            user.a(optJSONObject);
            user.a(true);
            SocialProvider.a(user, optJSONObject);
            h.a(a(user.c()));
            h.a(e);
            h.a(Session.State.AUTHENTICATED);
            return true;
        }
        h.a(Session.State.FAILED);
        throw new Exception("Session authentication request failed with status: " + f);
    }

    /* access modifiers changed from: package-private */
    public void b() {
        Session h = h();
        Device a2 = h.a();
        if (h.c() == Session.State.FAILED) {
            a2.b(null);
        }
        b bVar = new b(g(), getGame(), a2);
        if (a2.getIdentifier() == null && this.c == null) {
            this.c = new b(h(), this.d);
        }
        a_();
        h.a(Session.State.AUTHENTICATING);
        if (a2.getIdentifier() == null) {
            this.c.c();
        }
        a(bVar);
    }
}
