package com.scoreloop.client.android.ui.framework;

import android.app.Activity;
import android.app.ActivityGroup;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.List;
import org.anddev.andengine.R;

public class TabsActivity extends ActivityGroup implements View.OnClickListener, f, z {
    private u a;

    public final boolean a(Menu menu) {
        Activity activity = getLocalActivityManager().getActivity(b(this.a.d()));
        if (activity == null || !(activity instanceof f)) {
            return true;
        }
        return ((f) activity).a(menu);
    }

    public final boolean b(Menu menu) {
        Activity activity = getLocalActivityManager().getActivity(b(this.a.d()));
        if (activity == null || !(activity instanceof f)) {
            return true;
        }
        return ((f) activity).b(menu);
    }

    public final boolean a(MenuItem menuItem) {
        Activity activity = getLocalActivityManager().getActivity(b(this.a.d()));
        if (activity == null || !(activity instanceof f)) {
            return false;
        }
        return ((f) activity).a(menuItem);
    }

    public final boolean a(ah ahVar) {
        Activity currentActivity = getLocalActivityManager().getCurrentActivity();
        if (currentActivity instanceof BaseActivity) {
            return ((BaseActivity) currentActivity).a(ahVar);
        }
        return true;
    }

    public void onClick(View view) {
        a(((TabView) view).b());
        am.a();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.sl_tabs);
        am.a().a(this);
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        ((TabView) findViewById(R.id.sl_tabs_segments)).removeAllViews();
        am.a().a(this);
    }

    public final void a(u uVar) {
        this.a = uVar;
        for (k a2 : uVar.a()) {
            a2.a(true);
        }
        TabView tabView = (TabView) findViewById(R.id.sl_tabs_segments);
        tabView.a(this);
        List a3 = uVar.a();
        for (int i = 0; i < a3.size(); i++) {
            TextView textView = (TextView) getLayoutInflater().inflate((int) R.layout.sl_tab_caption, (ViewGroup) null);
            textView.setLayoutParams(new LinearLayout.LayoutParams(0, (int) getResources().getDimension(R.dimen.sl_clickable_height), 1.0f));
            textView.setText(((k) a3.get(i)).c());
            tabView.addView(textView);
        }
        tabView.c();
        a(uVar.d());
    }

    private void a(int i) {
        ((TabView) findViewById(R.id.sl_tabs_segments)).a(i);
        this.a.a(i);
        k kVar = (k) this.a.a().get(i);
        m.a(this, kVar.b(), b(i), R.id.sl_tabs_body, 0);
        kVar.a(false);
    }

    private static String b(int i) {
        return "tab-" + i;
    }
}
