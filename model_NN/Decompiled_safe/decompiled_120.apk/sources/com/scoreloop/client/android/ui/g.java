package com.scoreloop.client.android.ui;

import android.util.Log;
import com.scoreloop.client.android.ui.component.base.a;
import com.scoreloop.client.android.ui.component.base.o;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

final class g {
    private int a;
    private final Map b;
    private final String c;
    private final List d = new ArrayList();
    private boolean e;
    private /* synthetic */ c f;

    g(c cVar, String str, Map map) {
        this.f = cVar;
        this.c = str;
        this.b = map;
    }

    /* access modifiers changed from: package-private */
    public final void a(String str, o oVar) {
        if (a.a(oVar)) {
            a(str, new Object[0]);
        }
    }

    public final void a(String str, Object... objArr) {
        Object obj;
        this.a++;
        if (!this.b.containsKey(str)) {
            this.d.add(b(str, objArr));
        }
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < objArr.length - 1) {
                String str2 = (String) objArr[i2];
                Object obj2 = objArr[i2 + 1];
                Map map = (Map) this.b.get(str);
                if (map == null || (obj = map.get(str2)) == null || !obj.equals(obj2)) {
                    this.d.add(b(str, objArr));
                } else {
                    i = i2 + 2;
                }
            } else {
                return;
            }
        }
        this.d.add(b(str, objArr));
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        if (this.e) {
            throw new a();
        }
    }

    private String b(String str, Object[] objArr) {
        StringBuilder sb = new StringBuilder();
        sb.append("android:name=\"");
        sb.append(str);
        sb.append("\"");
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= objArr.length - 1) {
                return sb.toString();
            }
            String str2 = (String) objArr[i2];
            Object obj = objArr[i2 + 1];
            sb.append(" android:");
            sb.append(str2);
            sb.append("=\"");
            if (str2.equals("theme")) {
                sb.append(this.f.g.getResources().getResourceName(((Integer) obj).intValue()));
            } else {
                sb.append(obj.toString());
            }
            sb.append("\"");
            i = i2 + 2;
        }
    }

    private void a(String str) {
        Log.e("ScoreloopUI", "=====================================================================================");
        Log.e("ScoreloopUI", "Manifest file verification error. Please resolve any issues first!");
        Log.e("ScoreloopUI", str);
        for (String str2 : this.d) {
            Log.e("ScoreloopUI", "<" + this.c + " " + str2 + "/>");
        }
    }

    /* access modifiers changed from: package-private */
    public final void b() {
        if (this.a == this.d.size()) {
            a("At least one of following entries is mssing in your AndroidManifest.xml file:");
            this.e = true;
        }
    }

    /* access modifiers changed from: package-private */
    public final void c() {
        if (this.d.size() > 0) {
            a("All the following entries are missing in your AndroidManifest.xml file:");
            this.e = true;
        }
    }
}
