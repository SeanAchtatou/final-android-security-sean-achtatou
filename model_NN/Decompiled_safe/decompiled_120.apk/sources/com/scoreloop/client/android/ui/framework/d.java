package com.scoreloop.client.android.ui.framework;

import java.util.ArrayList;
import java.util.List;

public final class d implements al {
    private e a;
    private boolean b = true;
    /* access modifiers changed from: private */
    public b c = null;
    /* access modifiers changed from: private */
    public u d = null;

    /* access modifiers changed from: protected */
    public final void a(u uVar, int i) {
        u d2 = d();
        r c2 = c();
        k b2 = d2.b();
        if (b2 == null) {
            throw new IllegalStateException("we don't currently support screens without a header");
        }
        if (uVar != null) {
            b2.d();
        }
        c2.b(b2, i);
        List a2 = d2.a();
        int size = a2.size();
        if (uVar != null) {
            List a3 = uVar.a();
            int min = Math.min(size, a3.size());
            for (int i2 = 0; i2 < min; i2++) {
                a3.get(i2);
                ((k) a2.get(i2)).d();
            }
        }
        if (size == 0) {
            c2.c();
        } else if (size == 1) {
            c2.a((k) a2.get(0), i);
        } else if (size > 1) {
            this.d = d2;
            c2.a(i);
        }
        c2.a(d2);
    }

    public final void a(u uVar) {
        if (uVar != null) {
            a(p.FORWARD, new ab(this, uVar));
        }
    }

    public final void a(u uVar, r rVar, boolean z) {
        b b2;
        if (z) {
            f();
        }
        a(uVar, rVar);
        a((this.c == null || (b2 = this.c.a) == null) ? null : b2.b(), 1);
    }

    public final void a() {
        a(p.BACK, new aa(this));
    }

    public final void a(r rVar) {
        r a2 = this.c.a();
        for (b bVar = this.c; bVar != null; bVar = bVar.a) {
            if (bVar.a() == a2) {
                bVar.b = rVar;
            }
        }
        a((u) null, 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.client.android.ui.framework.d.a(com.scoreloop.client.android.ui.framework.u, com.scoreloop.client.android.ui.framework.r, boolean):void
     arg types: [com.scoreloop.client.android.ui.framework.u, com.scoreloop.client.android.ui.framework.r, int]
     candidates:
      com.scoreloop.client.android.ui.framework.d.a(com.scoreloop.client.android.ui.framework.d, com.scoreloop.client.android.ui.framework.u, com.scoreloop.client.android.ui.framework.r):void
      com.scoreloop.client.android.ui.framework.d.a(com.scoreloop.client.android.ui.framework.d, com.scoreloop.client.android.ui.framework.u, com.scoreloop.client.android.ui.framework.u):boolean
      com.scoreloop.client.android.ui.framework.d.a(com.scoreloop.client.android.ui.framework.u, com.scoreloop.client.android.ui.framework.r, boolean):void */
    public final void b(r rVar) {
        if (this.d != null) {
            u uVar = this.d;
            this.d = null;
            a(uVar, rVar, false);
        }
    }

    public final void a(z zVar) {
        if (this.d != null) {
            u uVar = this.d;
            this.d = null;
            zVar.a(uVar);
        }
    }

    public final void b(u uVar) {
        u d2 = d();
        List g = g();
        int size = g.size() - 1;
        for (int i = 0; i < size; i++) {
            ((r) g.get(i)).b().finish();
        }
        this.b = false;
        a((b) null);
        r rVar = (r) g.get(size);
        rVar.a();
        a(uVar, rVar);
        a(d2, 0);
        this.b = true;
    }

    /* access modifiers changed from: private */
    public void f() {
        for (r b2 : g()) {
            b2.b().finish();
        }
        a((b) null);
        this.d = null;
    }

    public final void b() {
        a(p.EXIT, new ac(this));
    }

    public final k a(String str) {
        return this.c.a(str);
    }

    /* access modifiers changed from: protected */
    public final r c() {
        if (this.c != null) {
            return this.c.a();
        }
        return null;
    }

    public final u d() {
        if (this.c != null) {
            return this.c.b();
        }
        return null;
    }

    public final int e() {
        return b.a(this.c);
    }

    private List g() {
        ArrayList arrayList = new ArrayList();
        r rVar = null;
        for (b bVar = this.c; bVar != null; bVar = bVar.a) {
            r a2 = bVar.a();
            if (rVar == null || a2 != rVar) {
                arrayList.add(a2);
                rVar = a2;
            }
        }
        return arrayList;
    }

    static /* synthetic */ b b(d dVar) {
        if (dVar.c == null) {
            return null;
        }
        b bVar = dVar.c;
        dVar.a(dVar.c.a);
        return bVar;
    }

    /* access modifiers changed from: private */
    public void a(u uVar, r rVar) {
        a(new b(this.c, uVar, rVar));
    }

    public final void a(e eVar) {
        this.a = eVar;
    }

    private void a(b bVar) {
        b bVar2 = this.c;
        this.c = bVar;
        if (this.b) {
            if (bVar2 != null || bVar == null) {
                if (bVar2 != null && bVar == null && this.a != null) {
                    this.a.n();
                }
            } else if (this.a != null) {
                this.a.o();
            }
        }
    }

    static /* synthetic */ boolean a(d dVar, u uVar, u uVar2) {
        if (dVar.a != null) {
            return dVar.a.a(uVar, uVar2);
        }
        return false;
    }

    private void a(p pVar, Runnable runnable) {
        r c2 = c();
        if (c2 == null) {
            runnable.run();
            return;
        }
        ah ahVar = new ah(pVar, runnable);
        if (c2.a(ahVar)) {
            ahVar.a();
        }
    }
}
