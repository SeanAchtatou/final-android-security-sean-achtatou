package com.scoreloop.client.android.core.model;

public interface ImageSource {
    public static final ImageSource IMAGE_SOURCE_DEFAULT = new d();
    public static final ImageSource IMAGE_SOURCE_SCORELOOP = new c();

    String getName();
}
