package com.scoreloop.client.android.ui.framework;

import android.content.Intent;
import java.util.ArrayList;
import java.util.List;

public final class u {
    private final List a = new ArrayList();
    private k b;
    private final s c = new s();
    private int d = 0;
    private final List e = new ArrayList();
    private x f;
    private int g = 0;

    public final k a(int i, Intent intent) {
        k kVar = new k(i, intent);
        this.a.add(kVar);
        return kVar;
    }

    public final void a(int i, int i2, int i3) {
        this.e.add(new ae(i, i2, i3));
    }

    public final void a(x xVar) {
        this.f = xVar;
    }

    /* access modifiers changed from: package-private */
    public final k a(String str) {
        if (this.b != null && this.b.a(str)) {
            return this.b;
        }
        for (k kVar : this.a) {
            if (kVar.a(str)) {
                return kVar;
            }
        }
        return null;
    }

    public final List a() {
        return this.a;
    }

    public final k b() {
        return this.b;
    }

    public final s c() {
        return this.c;
    }

    public final int d() {
        return this.d;
    }

    public final List e() {
        return this.e;
    }

    public final x f() {
        return this.f;
    }

    public final int g() {
        if (this.g != 0) {
            int size = this.e.size();
            for (int i = 0; i < size; i++) {
                if (this.g == ((ae) this.e.get(i)).c()) {
                    return i;
                }
            }
        }
        return -1;
    }

    public final k a(Intent intent) {
        return a(0, intent);
    }

    public final k b(Intent intent) {
        this.b = new k(0, intent);
        return this.b;
    }

    public final void a(int i) {
        this.d = i;
    }

    public final void b(int i) {
        this.g = i;
    }
}
