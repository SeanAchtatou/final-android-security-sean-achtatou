package com.scoreloop.client.android.ui.framework;

import android.app.Activity;
import android.app.ActivityGroup;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import org.anddev.andengine.R;

public class ScreenActivity extends ActivityGroup implements View.OnClickListener, r {
    public ScreenActivity() {
        super(false);
    }

    public final void a() {
        getLocalActivityManager().removeAllActivities();
    }

    /* access modifiers changed from: protected */
    public final void a(u uVar, Bundle bundle) {
        if (bundle == null) {
            am.a().a(uVar, this, true);
        }
    }

    public final Activity b() {
        return this;
    }

    public final boolean a(ah ahVar) {
        boolean z;
        Activity activity = getLocalActivityManager().getActivity("header");
        if (activity == null || !(activity instanceof BaseActivity)) {
            z = true;
        } else {
            z = ((BaseActivity) activity).a(ahVar) & true;
        }
        Activity activity2 = getLocalActivityManager().getActivity("body");
        if (activity2 instanceof BaseActivity) {
            return z & ((BaseActivity) activity2).a(ahVar);
        }
        if (activity2 instanceof TabsActivity) {
            return z & ((TabsActivity) activity2).a(ahVar);
        }
        return z;
    }

    public void onClick(View view) {
        if (view == findViewById(R.id.sl_status_close_button)) {
            am.a().b();
        } else if (view == findViewById(R.id.sl_shortcuts)) {
            ShortcutView shortcutView = (ShortcutView) view;
            ah ahVar = new ah(p.SHORTCUT, new l(this, shortcutView.b()));
            if (!a(ahVar)) {
                shortcutView.a(shortcutView.b);
            } else {
                ahVar.a();
            }
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.sl_screen);
        ImageView imageView = (ImageView) findViewById(R.id.sl_status_close_button);
        if (imageView != null) {
            imageView.setEnabled(true);
            imageView.setOnClickListener(this);
        }
        ((ShortcutView) findViewById(R.id.sl_shortcuts)).a(this);
        if (bundle == null) {
            am.a().b(this);
            return;
        }
        int i = bundle.getInt("stackEntryReference");
        int e = am.a().e();
        if (i != e) {
            Log.w("ScoreloopUI", String.format("onCreate with savedInstanceState: contains wrong stackEntryReference %s and current stack depth is %s", Integer.valueOf(i), Integer.valueOf(e)));
            am.a().b();
            finish();
            return;
        }
        am.a().a(this);
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4 || keyEvent.getRepeatCount() != 0) {
            return super.onKeyDown(i, keyEvent);
        }
        am.a().a();
        return true;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        boolean z;
        boolean onCreateOptionsMenu = super.onCreateOptionsMenu(menu);
        Activity activity = getLocalActivityManager().getActivity("body");
        if (activity == null || !(activity instanceof f)) {
            z = onCreateOptionsMenu;
        } else {
            z = ((f) activity).a(menu) | onCreateOptionsMenu;
        }
        Activity activity2 = getLocalActivityManager().getActivity("header");
        if (activity2 == null || !(activity2 instanceof f)) {
            return z;
        }
        return z | ((f) activity2).a(menu);
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean z;
        boolean onPrepareOptionsMenu = super.onPrepareOptionsMenu(menu);
        Activity activity = getLocalActivityManager().getActivity("header");
        if (activity == null || !(activity instanceof f)) {
            z = onPrepareOptionsMenu;
        } else {
            z = ((f) activity).b(menu) | onPrepareOptionsMenu;
        }
        Activity activity2 = getLocalActivityManager().getActivity("body");
        if (activity2 != null && (activity2 instanceof f)) {
            z |= ((f) activity2).b(menu);
        }
        am.a();
        return z;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        boolean z;
        Activity activity;
        Activity activity2;
        boolean onOptionsItemSelected = super.onOptionsItemSelected(menuItem);
        if (onOptionsItemSelected || (activity2 = getLocalActivityManager().getActivity("header")) == null || !(activity2 instanceof f)) {
            z = onOptionsItemSelected;
        } else {
            z = ((f) activity2).a(menuItem);
        }
        if (z || (activity = getLocalActivityManager().getActivity("body")) == null || !(activity instanceof f)) {
            return z;
        }
        return ((f) activity).a(menuItem);
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putInt("stackEntryReference", am.a().e());
    }

    public final void a(u uVar) {
        ShortcutView shortcutView = (ShortcutView) findViewById(R.id.sl_shortcuts);
        shortcutView.a(this, uVar.e());
        shortcutView.a(uVar.g());
    }

    public final void a(k kVar, int i) {
        m.a(this, kVar.b(), "body", R.id.sl_body, i);
    }

    public final void c() {
        ((ViewGroup) findViewById(R.id.sl_body)).removeAllViews();
    }

    public final void b(k kVar, int i) {
        m.a(this, kVar.b(), "header", R.id.sl_header, i);
    }

    public final void d() {
        startActivity(new Intent(this, ScreenActivity.class));
    }

    public final void a(int i) {
        Intent intent = new Intent(this, TabsActivity.class);
        intent.addFlags(536870912);
        m.a(this, intent, "body", R.id.sl_body, i);
    }

    public void onPause() {
        finish();
        super.onPause();
    }
}
