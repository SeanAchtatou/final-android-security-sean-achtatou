package com.scoreloop.client.android.ui.component.score;

import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.ui.component.base.ComponentActivity;
import com.scoreloop.client.android.ui.component.base.k;
import org.anddev.andengine.R;

public final class d extends k {
    public d(ComponentActivity componentActivity) {
        super(componentActivity, null, Session.getCurrentSession().getUser().getDisplayName(), componentActivity.getResources().getString(R.string.sl_not_on_highscore_list), null);
    }

    /* access modifiers changed from: protected */
    public final String i() {
        return Session.getCurrentSession().getUser().getImageUrl();
    }

    /* access modifiers changed from: protected */
    public final int h() {
        return R.layout.sl_list_item_score_excluded;
    }

    public final int a() {
        return 20;
    }

    public final boolean b() {
        return false;
    }
}
