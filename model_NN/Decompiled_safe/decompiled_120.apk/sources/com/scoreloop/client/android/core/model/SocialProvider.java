package com.scoreloop.client.android.core.model;

import com.scoreloop.client.android.core.util.Logger;
import com.scoreloop.client.android.core.util.SetterIntent;
import java.util.ArrayList;
import java.util.List;
import org.anddev.andengine.level.util.constants.LevelConstants;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class SocialProvider implements ImageSource, MessageReceiverInterface {
    public static final String FACEBOOK_IDENTIFIER = "com.facebook.v1";
    public static final String MYSPACE_IDENTIFIER = "com.myspace.v1";
    public static final String TWITTER_IDENTIFIER = "com.twitter.v1";
    private static List a = null;
    private static final String[] b = {"com.scoreloop.client.android.core.spi.oauthfacebook.OAuthFacebookSocialProvider", "com.scoreloop.client.android.core.spi.myspace.MySpaceSocialProvider", "com.scoreloop.client.android.core.spi.twitter.TwitterSocialProvider"};

    public static void a(User user, JSONObject jSONObject) {
        for (SocialProvider d : getSupportedProviders()) {
            d.d(user, jSONObject);
        }
    }

    static void b(User user, JSONObject jSONObject) {
        for (SocialProvider e : getSupportedProviders()) {
            e.e(user, jSONObject);
        }
    }

    static void c(User user, JSONObject jSONObject) {
        for (SocialProvider f : getSupportedProviders()) {
            f.f(user, jSONObject);
        }
    }

    public static SocialProvider getSocialProviderForIdentifier(String str) {
        for (SocialProvider socialProvider : getSupportedProviders()) {
            if (socialProvider.getIdentifier().equalsIgnoreCase(str)) {
                return socialProvider;
            }
        }
        return null;
    }

    public static List getSupportedProviders() {
        if (a == null) {
            a = new ArrayList();
            for (int i = 0; i < b.length; i++) {
                try {
                    a.add(Class.forName(b[i]).newInstance());
                } catch (Exception e) {
                    Logger.b("error instantiating social provider: " + e.getLocalizedMessage());
                }
            }
        }
        return a;
    }

    public String a() {
        return getIdentifier();
    }

    public void a(User user) {
        d(user, new JSONObject());
    }

    public void a(User user, String str, String str2, String str3) {
        if (str == null) {
            throw new IllegalArgumentException();
        }
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("token", str);
            jSONObject.put("token_secret", str2);
            jSONObject.put(LevelConstants.TAG_LEVEL_ATTRIBUTE_UID, str3);
            user.a(jSONObject, c());
        } catch (JSONException e) {
            throw new IllegalStateException(e);
        }
    }

    public boolean a(Session session) {
        return isUserConnected(session.getUser());
    }

    public String[] a(Object... objArr) {
        String[] strArr = new String[objArr.length];
        for (int i = 0; i < objArr.length; i++) {
            Object obj = objArr[i];
            if (obj instanceof User) {
                strArr[i] = ((User) obj).getIdentifier();
            } else if (!(obj instanceof String)) {
                return null;
            } else {
                strArr[i] = (String) obj;
            }
        }
        return strArr;
    }

    public abstract Class b();

    /* access modifiers changed from: protected */
    public String c() {
        return getIdentifier();
    }

    /* access modifiers changed from: protected */
    public String d() {
        return getName();
    }

    /* access modifiers changed from: protected */
    public void d(User user, JSONObject jSONObject) {
        if (user == null || jSONObject == null) {
            throw new IllegalArgumentException();
        }
        try {
            user.a(new SetterIntent().b(jSONObject, d(), SetterIntent.KeyMode.USE_NULL_WHEN_NO_KEY, SetterIntent.ValueMode.ALLOWS_NULL_VALUE), c());
        } catch (JSONException e) {
            throw new IllegalStateException(e);
        }
    }

    /* access modifiers changed from: protected */
    public void e(User user, JSONObject jSONObject) {
        if (user == null || jSONObject == null) {
            throw new IllegalArgumentException();
        }
        try {
            SetterIntent setterIntent = new SetterIntent();
            if (setterIntent.f(jSONObject, d(), SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
                user.a((JSONObject) setterIntent.a(), c());
            }
        } catch (JSONException e) {
            throw new IllegalStateException(e);
        }
    }

    /* access modifiers changed from: protected */
    public void f(User user, JSONObject jSONObject) {
        JSONObject d = user.d(c());
        if (d != null) {
            try {
                jSONObject.put(d(), d);
            } catch (JSONException e) {
                throw new IllegalStateException(e);
            }
        }
    }

    public abstract String getIdentifier();

    public final String getName() {
        return getIdentifier().split("\\.")[1];
    }

    public final Integer getVersion() {
        return new Integer(getIdentifier().split("\\.")[2].substring(1));
    }

    public boolean isUserConnected(User user) {
        return user.c(c());
    }
}
