package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.Ranking;
import com.scoreloop.client.android.core.model.Score;
import com.scoreloop.client.android.core.model.ScoreComparator;
import com.scoreloop.client.android.core.model.SearchList;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.core.persistence.LocalScoreStore;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.RequestCompletionCallback;
import com.scoreloop.client.android.core.server.RequestMethod;
import com.scoreloop.client.android.core.server.Response;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ScoresController extends RequestController {
    private final a c;
    private Integer d;
    private b e;
    private RankingController f;
    /* access modifiers changed from: private */
    public Score g;
    private Comparator h;
    private SearchList i;
    private final LocalScoreStore j;

    class a implements RequestControllerObserver {
        private a() {
        }

        public void requestControllerDidFail(RequestController requestController, Exception exc) {
            ScoresController.this.f().requestControllerDidFail(ScoresController.this, exc);
        }

        public void requestControllerDidReceiveResponse(RequestController requestController) {
            Ranking ranking = ((RankingController) requestController).getRanking();
            if (ScoresController.this.g != null && ScoresController.this.g.getRank() == null) {
                ScoresController.this.g.a(ranking.getRank());
            }
            ScoresController.this.a(ranking.getRank());
        }
    }

    class b extends Request {
        private final Game a;
        private final Integer b;
        private int c;
        private final int d;
        private final SearchList e;
        private final User f;

        public b(RequestCompletionCallback requestCompletionCallback, Game game, SearchList searchList, User user, Integer num, int i, int i2) {
            super(requestCompletionCallback);
            if (game == null) {
                throw new IllegalStateException("internal error: null game");
            }
            this.a = game;
            this.e = searchList;
            this.f = user;
            this.b = num;
            this.d = i;
            this.c = i2;
        }

        public String a() {
            return String.format("/service/games/%s/scores", this.a.getIdentifier());
        }

        public void a(int i) {
            this.c = i;
        }

        public JSONObject b() {
            JSONObject jSONObject = new JSONObject();
            try {
                if (this.e != null) {
                    jSONObject.putOpt("search_list_id", this.e.getIdentifier());
                }
                jSONObject.put("user_id", this.f.getIdentifier());
                jSONObject.put("offset", this.c);
                jSONObject.put("per_page", this.d);
                if (this.b != null) {
                    jSONObject.put("mode", this.b);
                }
                return jSONObject;
            } catch (JSONException e2) {
                throw new IllegalStateException("Invalid challenge data", e2);
            }
        }

        public RequestMethod c() {
            return RequestMethod.GET;
        }
    }

    public ScoresController(RequestControllerObserver requestControllerObserver) {
        this(null, requestControllerObserver);
    }

    public ScoresController(Session session, RequestControllerObserver requestControllerObserver) {
        super(session, requestControllerObserver);
        this.c = new a();
        this.d = null;
        this.g = null;
        this.i = null;
        this.i = SearchList.getDefaultScoreSearchList();
        this.h = new ScoreComparator(getGame().f(), getGame().g());
        this.j = new LocalScoreStore(h().d(), h().getGame().getIdentifier(), h().a().h(), h().getUser());
    }

    private void a(int i2) {
        a_();
        if (this.i != SearchList.getLocalScoreSearchList()) {
            this.c.a(i2);
            b bVar = new b(g(), getGame(), getSearchList(), i(), getMode(), this.c.a(), i2);
            bVar.a(60000L);
            b(bVar);
            return;
        }
        b(i2);
    }

    /* access modifiers changed from: private */
    public void a(Integer num) {
        int i2;
        if (num != null) {
            i2 = Math.max(1, num.intValue() - (getRangeLength() / 2));
        } else {
            i2 = 1;
        }
        c(i2 - 1);
        b bVar = this.e;
        this.e = null;
        a_();
        bVar.a(60000L);
        b(bVar);
    }

    private RankingController b() {
        if (this.f == null) {
            this.f = new RankingController(h(), new a());
        }
        return this.f;
    }

    private void b(int i2) {
        List a2 = this.j.a(this.d);
        Collections.sort(a2, this.h);
        int size = a2.size();
        if (i2 < size) {
            List subList = a2.subList(i2, size);
            int size2 = subList.size();
            for (int i3 = 0; i3 < size2; i3++) {
                ((Score) subList.get(i3)).a(Integer.valueOf(i3 + i2 + 1));
            }
            a2 = subList;
        } else {
            a2.clear();
        }
        this.c.a(i2);
        this.c.a(a2);
        j();
    }

    private void c() {
        this.c.a(0);
        this.e = new b(g(), getGame(), getSearchList(), i(), getMode(), this.c.a(), 0);
    }

    private void c(int i2) {
        if (this.e == null) {
            throw new IllegalStateException("_nextRequest must not be null");
        }
        this.c.c(i2);
        this.e.a(i2);
    }

    /* access modifiers changed from: package-private */
    public boolean a(Request request, Response response) {
        int i2;
        if (response.f() != 200) {
            a_();
            throw new Exception("Request failed, returned status: " + response.f());
        }
        JSONArray jSONArray = response.e().getJSONArray("scores");
        Integer rank = this.g == null ? null : this.g.getRank();
        ArrayList arrayList = new ArrayList();
        int b2 = this.c.b() + 1;
        int i3 = 0;
        while (i3 < jSONArray.length()) {
            int i4 = i3 + 1;
            Score score = new Score(jSONArray.getJSONObject(i3).getJSONObject(Score.a));
            if (rank == null || b2 != rank.intValue() || (this.g.getIdentifier() != null && score.getIdentifier().equals(this.g.getIdentifier()))) {
                i2 = b2;
            } else {
                i2 = b2 + 1;
                score.a(Integer.valueOf(b2));
                arrayList.add(this.g);
            }
            b2 = i2 + 1;
            score.a(Integer.valueOf(i2));
            arrayList.add(score);
            i3 = i4;
        }
        this.c.a(arrayList);
        return true;
    }

    /* access modifiers changed from: protected */
    public void a_() {
        if (this.f != null) {
            this.f.a_();
        }
        super.a_();
    }

    public Comparator getLocalScoreComparator() {
        return this.h;
    }

    public Score getLocalScoreToSubmit() {
        List a2 = this.j.a(this.d);
        if (a2.size() == 0) {
            return null;
        }
        Collections.sort(a2, this.h);
        Score score = (Score) a2.get(0);
        if (score.getIdentifier() == null) {
            return score;
        }
        return null;
    }

    public Integer getMode() {
        return this.d;
    }

    public int getRangeLength() {
        return this.c.f();
    }

    public List getScores() {
        return this.c.d();
    }

    public SearchList getSearchList() {
        return this.i;
    }

    public boolean hasNextRange() {
        return this.c.h();
    }

    public boolean hasPreviousRange() {
        return this.c.i();
    }

    public void loadNextRange() {
        if (!hasNextRange()) {
            throw new IllegalStateException("There's no next range");
        } else if (this.c.g()) {
            a(this.c.c());
        } else {
            a(0);
        }
    }

    public void loadPreviousRange() {
        if (!hasPreviousRange()) {
            throw new IllegalStateException("There's no previous range");
        } else if (this.c.g()) {
            a(this.c.e());
        } else {
            a(0);
        }
    }

    public void loadRangeAtRank(int i2) {
        if (i2 <= 0) {
            throw new IllegalArgumentException("rank must be a positive integer");
        }
        a(i2 - 1);
    }

    public void loadRangeForScore(Score score) {
        if (this.i == SearchList.getLocalScoreSearchList()) {
            throw new IllegalStateException("loadRangeForScore is not available when the search list is set to the local search list");
        }
        a_();
        c();
        RankingController b2 = b();
        b2.setSearchList(getSearchList());
        b2.a(getLocalScoreComparator());
        this.g = score;
        this.g.a((Integer) null);
        b2.loadRankingForScore(score);
    }

    public void loadRangeForScoreResult(Double d2, Map map) {
        loadRangeForScore(new Score(d2, map));
    }

    public void loadRangeForUser(User user) {
        if (this.i == SearchList.getLocalScoreSearchList()) {
            throw new IllegalStateException("loadRangeForUser is not available when the search list is set to the local search list");
        }
        a_();
        c();
        this.g = null;
        RankingController b2 = b();
        b2.setSearchList(getSearchList());
        b2.loadRankingForUserInGameMode(user, getMode());
    }

    public void setLocalScoreComparator(Comparator comparator) {
        if (comparator == null) {
            throw new IllegalArgumentException("the comparator must not be null");
        }
        this.h = comparator;
    }

    public void setMode(Integer num) {
        this.d = num;
    }

    public void setRangeLength(int i2) {
        this.c.b(i2);
    }

    public void setSearchList(SearchList searchList) {
        if (this.i != searchList) {
            this.i = searchList;
            if (this.g != null) {
                this.g.a((Integer) null);
            }
        }
    }
}
