package com.scoreloop.client.android.ui.component.profile;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.widget.ListAdapter;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.controller.RequestControllerObserver;
import com.scoreloop.client.android.core.controller.SocialProviderController;
import com.scoreloop.client.android.core.controller.SocialProviderControllerObserver;
import com.scoreloop.client.android.core.controller.UserController;
import com.scoreloop.client.android.core.model.ImageSource;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.SocialProvider;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.ui.a.a;
import com.scoreloop.client.android.ui.a.e;
import com.scoreloop.client.android.ui.component.base.ComponentListActivity;
import java.io.ByteArrayOutputStream;
import org.anddev.andengine.R;
import org.anddev.andengine.util.constants.MIMETypes;

public class ProfileSettingsPictureListActivity extends ComponentListActivity implements RequestControllerObserver, SocialProviderControllerObserver {
    private Runnable a;
    /* access modifiers changed from: private */
    public h b;
    /* access modifiers changed from: private */
    public h c;
    /* access modifiers changed from: private */
    public h d;
    /* access modifiers changed from: private */
    public h e;
    /* access modifiers changed from: private */
    public h f;
    private User g;
    private UserController h;

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        if (intent != null && intent.getData() != null && !intent.getData().toString().trim().equals("")) {
            l().post(new l(this, intent));
        }
    }

    static /* synthetic */ void a(ProfileSettingsPictureListActivity profileSettingsPictureListActivity, Bitmap bitmap, Uri uri) {
        profileSettingsPictureListActivity.b(profileSettingsPictureListActivity.h);
        String uri2 = uri.toString();
        if (a.a(profileSettingsPictureListActivity, uri2, bitmap)) {
            profileSettingsPictureListActivity.y().b("userImageUrl", uri2);
        }
        profileSettingsPictureListActivity.g.setImageSource(ImageSource.IMAGE_SOURCE_SCORELOOP);
        profileSettingsPictureListActivity.g.setImageMimeType(MIMETypes.PNG);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 90, byteArrayOutputStream);
        profileSettingsPictureListActivity.g.setImageData(e.a(byteArrayOutputStream.toByteArray()));
        profileSettingsPictureListActivity.h.submitUser();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Resources resources = getResources();
        this.b = new h(this, resources.getDrawable(R.drawable.sl_icon_device), getString(R.string.sl_device_library));
        this.c = new h(this, resources.getDrawable(R.drawable.sl_icon_facebook), getString(R.string.sl_facebook));
        this.f = new h(this, resources.getDrawable(R.drawable.sl_icon_twitter), getString(R.string.sl_twitter));
        this.d = new h(this, resources.getDrawable(R.drawable.sl_icon_myspace), getString(R.string.sl_myspace));
        this.e = new h(this, resources.getDrawable(R.drawable.sl_icon_user), getString(R.string.sl_set_default));
        a((ListAdapter) new o(this, this));
        this.g = Session.getCurrentSession().getUser();
        this.h = new UserController(this);
        this.h.setUser(this.g);
    }

    public final void a(com.scoreloop.client.android.ui.framework.a aVar) {
        if (aVar == this.b) {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction("android.intent.action.GET_CONTENT");
            intent.putExtra("windowTitle", getString(R.string.sl_choose_photo));
            try {
                startActivityForResult(intent, 1);
            } catch (Exception e2) {
            }
        } else if (aVar == this.c) {
            a(SocialProvider.FACEBOOK_IDENTIFIER, new k(this));
        } else if (aVar == this.f) {
            a(SocialProvider.TWITTER_IDENTIFIER, new j(this));
        } else if (aVar == this.d) {
            a(SocialProvider.MYSPACE_IDENTIFIER, new i(this));
        } else if (aVar == this.e) {
            this.g.setImageSource(ImageSource.IMAGE_SOURCE_DEFAULT);
            this.g.setImageMimeType(null);
            this.g.setImageData(null);
            b(this.h);
            this.h.submitUser();
        }
    }

    static /* synthetic */ void f(ProfileSettingsPictureListActivity profileSettingsPictureListActivity) {
        profileSettingsPictureListActivity.g.setImageSource(SocialProvider.getSocialProviderForIdentifier(SocialProvider.FACEBOOK_IDENTIFIER));
        profileSettingsPictureListActivity.g.setImageMimeType(null);
        profileSettingsPictureListActivity.g.setImageData(null);
        profileSettingsPictureListActivity.b(profileSettingsPictureListActivity.h);
        profileSettingsPictureListActivity.h.submitUser();
    }

    static /* synthetic */ void h(ProfileSettingsPictureListActivity profileSettingsPictureListActivity) {
        profileSettingsPictureListActivity.g.setImageSource(SocialProvider.getSocialProviderForIdentifier(SocialProvider.MYSPACE_IDENTIFIER));
        profileSettingsPictureListActivity.g.setImageMimeType(null);
        profileSettingsPictureListActivity.g.setImageData(null);
        profileSettingsPictureListActivity.b(profileSettingsPictureListActivity.h);
        profileSettingsPictureListActivity.h.submitUser();
    }

    static /* synthetic */ void g(ProfileSettingsPictureListActivity profileSettingsPictureListActivity) {
        profileSettingsPictureListActivity.g.setImageSource(SocialProvider.getSocialProviderForIdentifier(SocialProvider.TWITTER_IDENTIFIER));
        profileSettingsPictureListActivity.g.setImageMimeType(null);
        profileSettingsPictureListActivity.g.setImageData(null);
        profileSettingsPictureListActivity.b(profileSettingsPictureListActivity.h);
        profileSettingsPictureListActivity.h.submitUser();
    }

    /* access modifiers changed from: protected */
    public final void a(RequestController requestController, Exception exc) {
        super.a(requestController, exc);
        y().b("userImageUrl", this.g.getImageUrl());
    }

    public final void a(RequestController requestController) {
        y().b("userImageUrl", this.g.getImageUrl());
        a((Object) requestController);
    }

    public void socialProviderControllerDidCancel(SocialProviderController socialProviderController) {
        a(socialProviderController);
    }

    public void socialProviderControllerDidEnterInvalidCredentials(SocialProviderController socialProviderController) {
        socialProviderControllerDidFail(socialProviderController, new RuntimeException("Invalid Credentials"));
    }

    public void socialProviderControllerDidFail(SocialProviderController socialProviderController, Throwable th) {
        a(socialProviderController);
        d(String.format(getString(R.string.sl_format_connect_failed), socialProviderController.getSocialProvider().getName()));
    }

    public void socialProviderControllerDidSucceed(SocialProviderController socialProviderController) {
        a(socialProviderController);
        if (!o() && this.a != null) {
            this.a.run();
        }
    }

    private void a(String str, Runnable runnable) {
        SocialProvider socialProviderForIdentifier = SocialProvider.getSocialProviderForIdentifier(str);
        if (socialProviderForIdentifier.isUserConnected(Session.getCurrentSession().getUser())) {
            runnable.run();
            return;
        }
        SocialProviderController socialProviderController = SocialProviderController.getSocialProviderController(Session.getCurrentSession(), this, socialProviderForIdentifier);
        this.a = runnable;
        b(socialProviderController);
        socialProviderController.connect(this);
    }
}
