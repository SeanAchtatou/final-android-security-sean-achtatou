package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.Ranking;
import com.scoreloop.client.android.core.model.Score;
import com.scoreloop.client.android.core.model.ScoreComparator;
import com.scoreloop.client.android.core.model.SearchList;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.core.persistence.LocalScoreStore;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.RequestCompletionCallback;
import com.scoreloop.client.android.core.server.RequestMethod;
import com.scoreloop.client.android.core.server.Response;
import java.util.Comparator;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class RankingController extends RequestController {
    private Ranking c;
    private Comparator d;
    private SearchList e;
    private final LocalScoreStore f;

    class a extends Request {
        private final Game a;
        private final Integer b;
        private final Score c;
        private final SearchList d;
        private final User e;

        public a(RequestCompletionCallback requestCompletionCallback, Game game, SearchList searchList, User user, Score score, Integer num) {
            super(requestCompletionCallback);
            if (game == null) {
                throw new IllegalStateException("internal error: aGame not being set");
            }
            this.a = game;
            this.d = searchList;
            this.e = user;
            this.c = score;
            this.b = num;
        }

        public String a() {
            return String.format("/service/games/%s/scores/rankings", this.a.getIdentifier());
        }

        public JSONObject b() {
            JSONObject jSONObject = new JSONObject();
            try {
                if (this.d != null) {
                    jSONObject.putOpt("search_list_id", this.d.getIdentifier());
                }
                if (this.c != null) {
                    jSONObject.put(Score.a, this.c.d());
                } else {
                    jSONObject.put("user_id", this.e.getIdentifier());
                    if (this.b != null) {
                        jSONObject.put("mode", this.b);
                    }
                }
                return jSONObject;
            } catch (JSONException e2) {
                throw new IllegalStateException("Invalid ranking data", e2);
            }
        }

        public RequestMethod c() {
            return RequestMethod.GET;
        }
    }

    public RankingController(RequestControllerObserver requestControllerObserver) {
        this(null, requestControllerObserver);
    }

    public RankingController(Session session, RequestControllerObserver requestControllerObserver) {
        super(session, requestControllerObserver);
        this.c = new Ranking();
        this.e = null;
        if (getGame() == null) {
            throw new IllegalStateException("I think there's no point in getting a rank of a score in a null game..");
        }
        this.e = SearchList.getDefaultScoreSearchList();
        this.d = new ScoreComparator(getGame().f(), getGame().g());
        this.f = new LocalScoreStore(h().d(), h().getGame().getIdentifier(), h().a().h(), h().getUser());
    }

    public void a(Comparator comparator) {
        if (comparator == null) {
            throw new IllegalArgumentException("the comparator must not be null");
        }
        this.d = comparator;
    }

    /* access modifiers changed from: package-private */
    public boolean a(Request request, Response response) {
        if (response.f() != 200) {
            throw new Exception("Request failed");
        }
        JSONArray d2 = response.d();
        this.c = new Ranking();
        this.c.a(d2.getJSONObject(0).getJSONObject("ranking"));
        return true;
    }

    public Ranking getRanking() {
        return this.c;
    }

    public SearchList getSearchList() {
        return this.e;
    }

    public void loadRankingForScore(Score score) {
        if (score == null) {
            throw new IllegalArgumentException("score parameter cannot be null");
        } else if (this.e == null) {
            throw new IllegalArgumentException("Search list or user is required for score ranking");
        } else {
            if (score.getUser() == null) {
                score.a(i());
            }
            if (this.e != SearchList.getLocalScoreSearchList()) {
                a aVar = new a(g(), getGame(), this.e, null, score, score.getMode());
                a_();
                aVar.a(30000);
                b(aVar);
                return;
            }
            throw new IllegalStateException("loadRankingForScore is not supported in this release when the search list is set to the local search list");
        }
    }

    public void loadRankingForScoreResult(Double d2, Map map) {
        loadRankingForScore(new Score(d2, map));
    }

    public void loadRankingForUserInGameMode(User user, Integer num) {
        if (user == null) {
            throw new IllegalArgumentException("user paramter cannot be null");
        } else if (this.e == SearchList.getLocalScoreSearchList()) {
            throw new IllegalStateException("loadRankingForUserInGameMode is not available when the search list is set to the local search list");
        } else {
            a aVar = new a(g(), getGame(), this.e, user, null, num);
            a_();
            aVar.a(30000);
            b(aVar);
        }
    }

    public void setSearchList(SearchList searchList) {
        this.e = searchList;
    }
}
