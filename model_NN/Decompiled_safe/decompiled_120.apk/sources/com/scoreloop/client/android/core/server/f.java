package com.scoreloop.client.android.core.server;

import com.scoreloop.client.android.core.settings.Settings;
import java.net.URI;
import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.apache.http.client.methods.HttpPost;

class f extends e {
    private final Cipher a;
    private final byte[] b;

    f(URI uri, byte[] bArr) {
        super(uri);
        try {
            if (Settings.a != null) {
                this.a = Cipher.getInstance("AES/CBC/PKCS7Padding", Settings.a);
            } else {
                this.a = Cipher.getInstance("AES/CBC/PKCS7Padding");
            }
            this.b = bArr;
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException();
        } catch (NoSuchPaddingException e2) {
            throw new IllegalStateException();
        } catch (NoSuchProviderException e3) {
            throw new IllegalStateException();
        }
    }

    /* access modifiers changed from: package-private */
    public String a(HttpPost httpPost, String str) {
        SecretKeySpec secretKeySpec = new SecretKeySpec(this.b, "AES");
        try {
            this.a.init(1, secretKeySpec, new IvParameterSpec(this.b));
            byte[] a2 = super.a(httpPost, this.a.doFinal(str.getBytes("UTF8")));
            try {
                this.a.init(2, secretKeySpec, new IvParameterSpec(this.b));
                return new String(this.a.doFinal(a2), "UTF8");
            } catch (GeneralSecurityException e) {
                throw new IllegalStateException(e);
            }
        } catch (GeneralSecurityException e2) {
            throw new IllegalStateException(e2);
        }
    }

    /* access modifiers changed from: protected */
    public String b() {
        return "x-application/sjson";
    }
}
