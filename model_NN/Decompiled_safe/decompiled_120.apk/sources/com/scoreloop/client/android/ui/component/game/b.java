package com.scoreloop.client.android.ui.component.game;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.ui.framework.a;
import org.anddev.andengine.R;

public final class b extends a {
    private final Game a;

    public b(Context context, Game game) {
        super(context, null, game.getDescription());
        this.a = game;
    }

    public final int a() {
        return 13;
    }

    public final View a(View view) {
        View view2;
        if (view == null) {
            view2 = e().inflate((int) R.layout.sl_list_item_game_detail, (ViewGroup) null);
        } else {
            view2 = view;
        }
        TextView textView = (TextView) view2.findViewById(R.id.sl_list_item_game_detail_text);
        String description = this.a.getDescription();
        if (description == null) {
            description = this.a.getPublisherName();
        }
        if (description != null) {
            textView.setText(description.replace("\r", ""));
        }
        return view2;
    }

    public final boolean b() {
        return false;
    }
}
