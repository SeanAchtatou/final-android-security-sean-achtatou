package com.scoreloop.client.android.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Handler;
import com.scoreloop.client.android.core.controller.ChallengeController;
import com.scoreloop.client.android.core.controller.ChallengeControllerObserver;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.controller.ScoreController;
import com.scoreloop.client.android.core.controller.ScoresController;
import com.scoreloop.client.android.core.model.Challenge;
import com.scoreloop.client.android.core.model.Client;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.Score;
import com.scoreloop.client.android.core.model.ScoreSubmitException;
import com.scoreloop.client.android.core.model.SearchList;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.ui.component.a.g;
import com.scoreloop.client.android.ui.component.a.h;
import com.scoreloop.client.android.ui.component.achievement.AchievementHeaderActivity;
import com.scoreloop.client.android.ui.component.achievement.AchievementListActivity;
import com.scoreloop.client.android.ui.component.achievement.a;
import com.scoreloop.client.android.ui.component.base.b;
import com.scoreloop.client.android.ui.component.base.e;
import com.scoreloop.client.android.ui.component.base.o;
import com.scoreloop.client.android.ui.component.base.p;
import com.scoreloop.client.android.ui.component.challenge.ChallengeAcceptListActivity;
import com.scoreloop.client.android.ui.component.challenge.ChallengeCreateListActivity;
import com.scoreloop.client.android.ui.component.challenge.ChallengeHeaderActivity;
import com.scoreloop.client.android.ui.component.challenge.ChallengeListActivity;
import com.scoreloop.client.android.ui.component.challenge.ChallengePaymentActivity;
import com.scoreloop.client.android.ui.component.entry.EntryListActivity;
import com.scoreloop.client.android.ui.component.game.GameDetailHeaderActivity;
import com.scoreloop.client.android.ui.component.game.GameDetailListActivity;
import com.scoreloop.client.android.ui.component.game.GameListActivity;
import com.scoreloop.client.android.ui.component.market.MarketHeaderActivity;
import com.scoreloop.client.android.ui.component.market.MarketListActivity;
import com.scoreloop.client.android.ui.component.news.NewsHeaderActivity;
import com.scoreloop.client.android.ui.component.news.NewsListActivity;
import com.scoreloop.client.android.ui.component.profile.ProfileSettingsListActivity;
import com.scoreloop.client.android.ui.component.profile.ProfileSettingsPictureListActivity;
import com.scoreloop.client.android.ui.component.score.ScoreHeaderActivity;
import com.scoreloop.client.android.ui.component.score.ScoreListActivity;
import com.scoreloop.client.android.ui.component.user.UserAddBuddyListActivity;
import com.scoreloop.client.android.ui.component.user.UserDetailListActivity;
import com.scoreloop.client.android.ui.component.user.UserHeaderActivity;
import com.scoreloop.client.android.ui.component.user.UserListActivity;
import com.scoreloop.client.android.ui.component.user.j;
import com.scoreloop.client.android.ui.framework.BaseActivity;
import com.scoreloop.client.android.ui.framework.am;
import com.scoreloop.client.android.ui.framework.an;
import com.scoreloop.client.android.ui.framework.d;
import com.scoreloop.client.android.ui.framework.i;
import com.scoreloop.client.android.ui.framework.s;
import com.scoreloop.client.android.ui.framework.u;
import com.scoreloop.client.android.ui.framework.x;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import org.anddev.andengine.R;

final class c implements ChallengeControllerObserver, b, e, p, e, an, com.scoreloop.client.android.ui.framework.e, x, Runnable {
    private a a;
    private s b;
    private ChallengeController c;
    private boolean d;
    private final Client e;
    private final com.scoreloop.client.android.ui.component.base.a f;
    /* access modifiers changed from: private */
    public final Context g;
    private final Handler h = new Handler();
    private Challenge i;
    private Score j;
    private int k;
    private i l;
    private h m;
    private ScoreController n;
    private s o;
    private s p;
    private List q = null;

    private static boolean a(String[] strArr, String str) {
        for (String equalsIgnoreCase : strArr) {
            if (equalsIgnoreCase.equalsIgnoreCase(str)) {
                return true;
            }
        }
        return false;
    }

    c(Context context, String str) {
        this.g = context;
        this.e = new Client(context, "cb9b6303-d03a-40f8-b1b5-c6a8c193b2df", str, null);
        this.f = new com.scoreloop.client.android.ui.component.base.a(this.g, this.e.getSession());
        try {
            PackageInfo packageInfo = this.g.getPackageManager().getPackageInfo(this.g.getPackageName(), 4097);
            Map a2 = a(packageInfo);
            g gVar = new g(this, "activity", a2);
            gVar.a("com.scoreloop.client.android.ui.framework.ScreenActivity", new Object[0]);
            gVar.a("com.scoreloop.client.android.ui.framework.TabsActivity", new Object[0]);
            gVar.a("com.scoreloop.client.android.ui.component.market.MarketHeaderActivity", new Object[0]);
            gVar.a("com.scoreloop.client.android.ui.component.market.MarketListActivity", new Object[0]);
            gVar.a("com.scoreloop.client.android.ui.component.entry.EntryListActivity", new Object[0]);
            gVar.a("com.scoreloop.client.android.ui.component.post.PostOverlayActivity", "theme", Integer.valueOf((int) R.style.sl_dialog));
            gVar.a("com.scoreloop.client.android.ui.component.score.ScoreHeaderActivity", new Object[0]);
            gVar.a("com.scoreloop.client.android.ui.component.score.ScoreListActivity", new Object[0]);
            gVar.a("com.scoreloop.client.android.ui.component.user.UserAddBuddyListActivity", new Object[0]);
            gVar.a("com.scoreloop.client.android.ui.component.user.UserHeaderActivity", new Object[0]);
            gVar.a("com.scoreloop.client.android.ui.component.user.UserDetailListActivity", new Object[0]);
            gVar.a("com.scoreloop.client.android.ui.component.user.UserListActivity", new Object[0]);
            gVar.a("com.scoreloop.client.android.ui.component.game.GameDetailHeaderActivity", new Object[0]);
            gVar.a("com.scoreloop.client.android.ui.component.game.GameDetailListActivity", new Object[0]);
            gVar.a("com.scoreloop.client.android.ui.component.game.GameListActivity", new Object[0]);
            gVar.a("com.scoreloop.client.android.ui.component.achievement.AchievementHeaderActivity", o.ACHIEVEMENT);
            gVar.a("com.scoreloop.client.android.ui.component.achievement.AchievementListActivity", o.ACHIEVEMENT);
            gVar.a("com.scoreloop.client.android.ui.component.news.NewsHeaderActivity", o.NEWS);
            gVar.a("com.scoreloop.client.android.ui.component.news.NewsListActivity", o.NEWS);
            gVar.a("com.scoreloop.client.android.ui.component.challenge.ChallengeHeaderActivity", o.CHALLENGE);
            gVar.a("com.scoreloop.client.android.ui.component.challenge.ChallengeListActivity", o.CHALLENGE);
            gVar.a("com.scoreloop.client.android.ui.component.challenge.ChallengeAcceptListActivity", o.CHALLENGE);
            gVar.a("com.scoreloop.client.android.ui.component.challenge.ChallengeCreateListActivity", o.CHALLENGE);
            gVar.a("com.scoreloop.client.android.ui.component.challenge.ChallengePaymentActivity", o.CHALLENGE);
            gVar.a("com.scoreloop.client.android.ui.component.profile.ProfileSettingsListActivity", new Object[0]);
            gVar.a("com.scoreloop.client.android.ui.component.profile.ProfileSettingsPictureListActivity", new Object[0]);
            gVar.c();
            g gVar2 = new g(this, "activity", a2);
            gVar2.a("com.scoreloop.client.android.ui.EntryScreenActivity", new Object[0]);
            gVar2.a("com.scoreloop.client.android.ui.BuddiesScreenActivity", new Object[0]);
            gVar2.a("com.scoreloop.client.android.ui.LeaderboardsScreenActivity", new Object[0]);
            gVar2.a("com.scoreloop.client.android.ui.ChallengesScreenActivity", o.CHALLENGE);
            gVar2.a("com.scoreloop.client.android.ui.AchievementsScreenActivity", o.ACHIEVEMENT);
            gVar2.a("com.scoreloop.client.android.ui.SocialMarketScreenActivity", new Object[0]);
            gVar2.a("com.scoreloop.client.android.ui.ProfileScreenActivity", new Object[0]);
            gVar2.b();
            g gVar3 = new g(this, "uses-permission", b(packageInfo));
            gVar3.a("android.permission.INTERNET", new Object[0]);
            gVar3.a("android.permission.READ_PHONE_STATE", new Object[0]);
            gVar3.a("android.permission.READ_CONTACTS", o.ADDRESS_BOOK);
            gVar3.c();
            gVar.a();
            gVar2.a();
            gVar3.a();
            d dVar = new d();
            dVar.a(this);
            am.a(dVar);
            i.a();
        } catch (PackageManager.NameNotFoundException e2) {
            throw new a();
        }
    }

    public final boolean a() {
        if (this.l != null) {
            return this.l.a();
        }
        throw new IllegalStateException("trying to check if gameplay can be started, but the callback is not set - did you call ScoreloopManagerSingleton.get().setOnCanStartGamePlayObserver(...)?");
    }

    public final void challengeControllerDidFailOnInsufficientBalance(ChallengeController challengeController) {
        if (challengeController == this.c) {
            this.k = 5;
            this.i = null;
            this.d = false;
        }
    }

    public final void challengeControllerDidFailToAcceptChallenge(ChallengeController challengeController) {
        requestControllerDidFail(challengeController, new RuntimeException("challengeControllerDidFailToAcceptChallenge"));
    }

    public final void challengeControllerDidFailToRejectChallenge(ChallengeController challengeController) {
        requestControllerDidFail(challengeController, new RuntimeException("challengeControllerDidFailToRejectChallenge"));
    }

    public final u a(User user) {
        u a2 = a(user, (Game) null);
        a2.b(new Intent(this.g, AchievementHeaderActivity.class));
        s a3 = a2.a(new Intent(this.g, AchievementListActivity.class)).a();
        if (this.a == null) {
            this.a = new a();
        }
        a3.b("achievementsEngine", this.a);
        return a2;
    }

    public final u b(Challenge challenge) {
        u a2 = a((User) null, (Game) null);
        a2.b(new Intent(this.g, ChallengeHeaderActivity.class));
        a2.a(new Intent(this.g, ChallengeAcceptListActivity.class)).a().b("challenge", challenge);
        return a2;
    }

    public final u b(User user) {
        u a2 = a((User) null, (Game) null);
        a2.b(new Intent(this.g, ChallengeHeaderActivity.class));
        a2.a(new Intent(this.g, ChallengeCreateListActivity.class)).a().b("contestant", user);
        return a2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.client.android.ui.framework.s.b(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, int]
     candidates:
      com.scoreloop.client.android.ui.framework.s.b(java.lang.String, com.scoreloop.client.android.ui.framework.aj):void
      com.scoreloop.client.android.ui.framework.s.b(java.lang.String, java.lang.Object):void */
    public final u d() {
        u a2 = a((User) null, (Game) null);
        a2.b(new Intent(this.g, ChallengeHeaderActivity.class)).a().b("challengeHeaderMode", (Object) 1);
        a2.a(new Intent(this.g, ChallengePaymentActivity.class));
        return a2;
    }

    public final u c(User user) {
        u a2 = a(user, (Game) null);
        a2.b(new Intent(this.g, ChallengeHeaderActivity.class));
        a2.a(new Intent(this.g, ChallengeListActivity.class));
        return a2;
    }

    public final u e() {
        u a2 = a((User) null, (Game) null);
        a2.b(new Intent(this.g, UserHeaderActivity.class)).a().b("mode", j.PROFILE);
        a2.a(new Intent(this.g, EntryListActivity.class));
        a2.b((int) R.string.sl_home);
        return a2;
    }

    public final u a(Game game) {
        u a2 = a((User) null, game);
        a2.b(new Intent(this.g, GameDetailHeaderActivity.class));
        a2.a(new Intent(this.g, GameDetailListActivity.class));
        return a2;
    }

    public final u a(User user, int i2) {
        u a2 = a(user, (Game) null);
        if (i2 == 0) {
            a2.b(new Intent(this.g, UserHeaderActivity.class)).a().b("mode", j.BUDDY);
        } else {
            a2.b(new Intent(this.g, MarketHeaderActivity.class));
        }
        a2.a(new Intent(this.g, GameListActivity.class)).a().b("mode", Integer.valueOf(i2));
        return a2;
    }

    public final u f() {
        u a2 = a((User) null, (Game) null);
        a2.b(new Intent(this.g, MarketHeaderActivity.class));
        a2.a(new Intent(this.g, MarketListActivity.class));
        a2.b((int) R.string.sl_market);
        return a2;
    }

    public final u g() {
        u a2 = a((User) null, (Game) null);
        a2.b(new Intent(this.g, NewsHeaderActivity.class));
        a2.a(new Intent(this.g, NewsListActivity.class));
        return a2;
    }

    public final u d(User user) {
        u a2 = a(user, (Game) null);
        a2.b(new Intent(this.g, UserHeaderActivity.class));
        a2.a(new Intent(this.g, ProfileSettingsPictureListActivity.class));
        return a2;
    }

    public final u e(User user) {
        u a2 = a(user, (Game) null);
        a2.b(new Intent(this.g, UserHeaderActivity.class));
        a2.a(new Intent(this.g, ProfileSettingsListActivity.class));
        return a2;
    }

    public final u a(Game game, Integer num, Integer num2) {
        u a2 = a((User) null, game);
        a2.c().b("mode", num != null ? num : b(game).getMinMode());
        Boolean valueOf = Boolean.valueOf(num2 != null && num2.intValue() == 3);
        a2.b(new Intent(this.g, ScoreHeaderActivity.class)).a().b("isLocalLeadearboard", valueOf);
        if (valueOf.booleanValue()) {
            a2.a(new Intent(this.g, ScoreListActivity.class)).a().b("searchList", SearchList.getLocalScoreSearchList());
        } else {
            a2.a(R.string.sl_global, new Intent(this.g, ScoreListActivity.class)).a().b("searchList", SearchList.getGlobalScoreSearchList());
            a2.a(R.string.sl_friends, new Intent(this.g, ScoreListActivity.class)).a().b("searchList", SearchList.getBuddiesScoreSearchList());
            a2.a(R.string.sl_twentyfour, new Intent(this.g, ScoreListActivity.class)).a().b("searchList", SearchList.getTwentyFourHourScoreSearchList());
            if (num2 != null) {
                a2.a(num2.intValue());
            }
        }
        return a2;
    }

    private u a(User user, Game game) {
        s sVar;
        s sVar2;
        u uVar = new u();
        s c2 = uVar.c();
        User g2 = g(user);
        if (this.e.getSession().isOwnedByUser(g2)) {
            sVar = s();
        } else if (this.b == null || !this.b.a("user").equals(g2)) {
            s sVar3 = new s();
            sVar3.a(this);
            sVar3.b("user", g2);
            this.b = sVar3;
            sVar = sVar3;
        } else {
            sVar = this.b;
        }
        c2.b("userValues", sVar);
        Game b2 = b(game);
        if (b2.equals(this.e.getSession().getGame())) {
            sVar2 = r();
        } else {
            s sVar4 = new s();
            sVar4.a(this);
            sVar4.b("game", b2);
            sVar2 = sVar4;
        }
        c2.b("gameValues", sVar2);
        c2.b("sessionUserValues", s());
        c2.b("sessionGameValues", r());
        c2.b("manager", this);
        c2.b("factory", this);
        c2.b("tracker", this);
        c2.b("configuration", this.f);
        uVar.a(this);
        uVar.a(R.string.sl_home, R.drawable.sl_shortcut_home_default, R.drawable.sl_shortcut_home_active);
        uVar.a(R.string.sl_friends, R.drawable.sl_shortcut_friends_default, R.drawable.sl_shortcut_friends_active);
        uVar.a(R.string.sl_market, R.drawable.sl_shortcut_market_default, R.drawable.sl_shortcut_market_active);
        return uVar;
    }

    public final u h() {
        u a2 = a((User) null, (Game) null);
        a2.b(new Intent(this.g, UserHeaderActivity.class)).a().b("mode", j.BLANK);
        a2.a(new Intent(this.g, UserAddBuddyListActivity.class));
        return a2;
    }

    public final u a(User user, Boolean bool) {
        u a2 = a(user, (Game) null);
        a2.b(new Intent(this.g, UserHeaderActivity.class)).a().b("mode", j.BUDDY);
        a2.a(new Intent(this.g, UserDetailListActivity.class)).a().b("userPlaysSessionGame", bool);
        return a2;
    }

    public final u f(User user) {
        u a2 = a(user, (Game) null);
        a2.b(new Intent(this.g, UserHeaderActivity.class)).a().b("mode", this.e.getSession().isOwnedByUser(g(user)) ? j.BLANK : j.BUDDY);
        a2.a(new Intent(this.g, UserListActivity.class));
        if (this.e.getSession().isOwnedByUser(g(user))) {
            a2.b((int) R.string.sl_friends);
        }
        return a2;
    }

    private static void a(u uVar) {
        am.a().b(uVar);
    }

    private Game b(Game game) {
        return game != null ? game : this.e.getSession().getGame();
    }

    private User g(User user) {
        return user != null ? user : this.e.getSession().getUser();
    }

    private static Map a(PackageInfo packageInfo) {
        HashMap hashMap = new HashMap();
        for (ActivityInfo activityInfo : packageInfo.activities) {
            HashMap hashMap2 = null;
            if (activityInfo.theme != 0) {
                hashMap2 = new HashMap();
                hashMap2.put("theme", Integer.valueOf(activityInfo.theme));
            }
            hashMap.put(activityInfo.name, hashMap2);
        }
        return hashMap;
    }

    private static Map b(PackageInfo packageInfo) {
        HashMap hashMap = new HashMap();
        String[] strArr = packageInfo.requestedPermissions;
        if (strArr != null) {
            for (String put : strArr) {
                hashMap.put(put, null);
            }
        }
        return hashMap;
    }

    /* access modifiers changed from: package-private */
    public final com.scoreloop.client.android.ui.component.base.a i() {
        return this.f;
    }

    /* access modifiers changed from: package-private */
    public final Challenge j() {
        return this.i;
    }

    /* access modifiers changed from: package-private */
    public final Score k() {
        return this.j;
    }

    /* access modifiers changed from: package-private */
    public final int l() {
        return this.k;
    }

    private String p() {
        return this.g.getSharedPreferences("com.scoreloop.ui.login", 0).getString("userName", null);
    }

    private ScoreController q() {
        if (this.n == null) {
            this.n = new ScoreController(this);
        }
        return this.n;
    }

    /* access modifiers changed from: protected */
    public final Session m() {
        return this.e.getSession();
    }

    private s r() {
        if (this.o == null) {
            this.o = new s();
            this.o.a(this);
            this.o.b("game", this.e.getSession().getGame());
        }
        return this.o;
    }

    private s s() {
        if (this.p == null) {
            this.p = new s();
            this.p.a(this);
            this.p.b("user", this.e.getSession().getUser());
        }
        return this.p;
    }

    public final com.scoreloop.client.android.ui.framework.c a(String str) {
        if (a(g.a, str)) {
            return new g();
        }
        if (a(com.scoreloop.client.android.ui.component.a.d.a, str)) {
            return new com.scoreloop.client.android.ui.component.a.d();
        }
        if (a(com.scoreloop.client.android.ui.component.a.a.a, str)) {
            return new com.scoreloop.client.android.ui.component.a.a();
        }
        if (a(com.scoreloop.client.android.ui.component.a.j.a, str)) {
            return new com.scoreloop.client.android.ui.component.a.j();
        }
        if (a(h.a, str)) {
            return new h();
        }
        return null;
    }

    public final boolean b() {
        return this.d;
    }

    public final void a(Score score, Boolean bool) {
        boolean z;
        Game game = this.e.getSession().getGame();
        Integer mode = score.getMode();
        if (game.hasModes()) {
            if (mode == null) {
                throw new IllegalArgumentException("the game has modes, but no mode was passed");
            }
            int intValue = game.getMinMode().intValue();
            int intValue2 = game.getMaxMode().intValue();
            if (mode.intValue() < intValue || mode.intValue() >= intValue2) {
                throw new IllegalArgumentException("mode out of range [" + intValue + "," + intValue2 + "[");
            }
        }
        if (game.hasModes() || mode == null) {
            this.k = 0;
            this.j = score;
            if (!this.d) {
                this.i = null;
                ScoreController q2 = q();
                if (bool == null || !bool.booleanValue()) {
                    z = false;
                } else {
                    z = true;
                }
                q2.setShouldSubmitScoreLocally(z);
                q().submitScore(this.j);
            } else if (!com.scoreloop.client.android.ui.component.base.a.a(o.CHALLENGE)) {
                throw new IllegalStateException("we're in challenge mode, but the challenge feature is not enabled in the scoreloop.properties");
            } else {
                if (this.i.isCreated()) {
                    this.i.setContenderScore(this.j);
                }
                if (this.i.isAccepted()) {
                    this.i.setContestantScore(this.j);
                }
                if (this.c == null) {
                    this.c = new ChallengeController(this);
                }
                this.c.setChallenge(this.i);
                this.c.submitChallenge();
            }
        } else {
            throw new IllegalArgumentException("the game has no modes, but a mode was passed");
        }
    }

    public final void a(int i2) {
        if (i2 == R.string.sl_home) {
            a(e());
        } else if (i2 == R.string.sl_friends) {
            a(f(null));
        } else if (i2 == R.string.sl_market) {
            a(f());
        }
    }

    public final void c() {
        User user = this.e.getSession().getUser();
        if (user.isAuthenticated()) {
            SharedPreferences.Editor edit = this.g.getSharedPreferences("com.scoreloop.ui.login", 0).edit();
            edit.putString("userName", user.getDisplayName());
            edit.putString("userImageUrl", user.getImageUrl());
            edit.commit();
        }
    }

    public final void requestControllerDidFail(RequestController requestController, Exception exc) {
        if (requestController == this.n || requestController == this.c) {
            this.j = null;
            this.i = null;
            if (exc instanceof ScoreSubmitException) {
                this.k = 2;
            } else {
                this.k = 4;
            }
            this.d = false;
        }
    }

    public final void requestControllerDidReceiveResponse(RequestController requestController) {
        if (requestController != this.n && requestController != this.c) {
            return;
        }
        if (this.d) {
            this.k = 3;
            this.d = false;
        } else if (q().shouldSubmitScoreLocally()) {
            this.k = 2;
        } else {
            this.k = 1;
        }
    }

    public final void run() {
        String p2 = p();
        if (p2 != null) {
            BaseActivity.a(this.g, String.format(this.g.getString(R.string.sl_format_welcome_back), p2), 1);
        }
    }

    /* access modifiers changed from: private */
    public void t() {
        List<Runnable> list = this.q;
        this.q = null;
        for (Runnable run : list) {
            run.run();
        }
    }

    public final void n() {
        this.p = null;
        this.o = null;
        this.b = null;
        c();
    }

    public final boolean a(u uVar, u uVar2) {
        s c2 = uVar.c();
        s c3 = uVar2.c();
        String a2 = s.a("userValues", "user");
        if (!((User) c2.a(a2)).equals((User) c3.a(a2))) {
            return true;
        }
        String a3 = s.a("gameValues", "game");
        return !((Game) c2.a(a3)).equals((Game) c3.a(a3));
    }

    public final void o() {
        String p2 = p();
        if (p2 != null) {
            this.e.getSession().getUser().setLogin(p2);
        }
        String string = this.g.getSharedPreferences("com.scoreloop.ui.login", 0).getString("userImageUrl", null);
        if (string != null && s().a("userImageUrl") == null) {
            s().b("userImageUrl", string);
        }
    }

    public final void a(Challenge challenge) {
        if (this.m == null) {
            throw new IllegalStateException("trying to start gameplay, but the callback is not set - did you call ScoreloopManagerSingleton.get().setOnStartGamePlayRequestObserver(...)?");
        }
        this.d = true;
        this.i = challenge;
    }

    public final void a(Runnable runnable) {
        if (this.q == null) {
            ArrayList arrayList = new ArrayList();
            ScoresController scoresController = new ScoresController(this.e.getSession(), this);
            scoresController.setSearchList(SearchList.getLocalScoreSearchList());
            Game game = this.e.getSession().getGame();
            int intValue = game.getMinMode() != null ? game.getMinMode().intValue() : 0;
            int intValue2 = game.getModeCount().intValue() + intValue;
            while (intValue < intValue2) {
                scoresController.setMode(Integer.valueOf(intValue));
                Score localScoreToSubmit = scoresController.getLocalScoreToSubmit();
                if (localScoreToSubmit != null) {
                    arrayList.add(localScoreToSubmit);
                }
                intValue++;
            }
            this.q = new ArrayList();
            if (runnable != null) {
                this.q.add(runnable);
            }
            if (arrayList.size() == 0) {
                t();
                return;
            }
            ListIterator listIterator = arrayList.listIterator();
            new ScoreController(this.e.getSession(), new b(this, listIterator)).submitScore((Score) listIterator.next());
        } else if (runnable != null) {
            this.q.add(runnable);
        }
    }
}
