package com.scoreloop.client.android.ui.component.score;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import com.scoreloop.client.android.ui.component.base.ComponentHeaderActivity;
import com.scoreloop.client.android.ui.framework.s;
import org.anddev.andengine.R;

public class ScoreHeaderActivity extends ComponentHeaderActivity implements DialogInterface.OnClickListener {
    public void onClick(DialogInterface dialogInterface, int i) {
        m().b("mode", Integer.valueOf(d(i)));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.client.android.ui.component.base.ComponentActivity.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.scoreloop.client.android.ui.component.base.ComponentHeaderActivity.a(android.os.Bundle, int):void
      com.scoreloop.client.android.ui.component.base.ComponentActivity.a(com.scoreloop.client.android.core.controller.RequestController, java.lang.Exception):void
      com.scoreloop.client.android.ui.framework.BaseActivity.a(android.content.Context, java.lang.String):void
      com.scoreloop.client.android.ui.framework.BaseActivity.a(int, com.scoreloop.client.android.ui.framework.y):void
      com.scoreloop.client.android.ui.framework.BaseActivity.a(com.scoreloop.client.android.ui.framework.s, java.lang.String):void
      com.scoreloop.client.android.ui.framework.aj.a(com.scoreloop.client.android.ui.framework.s, java.lang.String):void
      com.scoreloop.client.android.ui.component.base.ComponentActivity.a(int, boolean):void */
    public void onClick(View view) {
        a(18, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.client.android.ui.framework.s.a(java.lang.String, java.lang.Object):java.lang.Object
     arg types: [java.lang.String, boolean]
     candidates:
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, boolean):com.scoreloop.client.android.ui.framework.g
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, com.scoreloop.client.android.ui.framework.j):void
      com.scoreloop.client.android.ui.framework.s.a(com.scoreloop.client.android.ui.framework.s, java.util.Set):void
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, com.scoreloop.client.android.ui.framework.aj):void
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, java.lang.Object):java.lang.Object */
    public void onCreate(Bundle bundle) {
        super.a(bundle, (int) R.layout.sl_header_default);
        a(C().getName());
        a().setImageDrawable(getResources().getDrawable(R.drawable.sl_header_icon_leaderboards));
        if (((Boolean) k().a("isLocalLeadearboard", (Object) false)).booleanValue()) {
            c(getString(R.string.sl_local_leaderboard));
        } else {
            c(getString(R.string.sl_leaderboards));
        }
        if (C().hasModes()) {
            ImageView imageView = (ImageView) findViewById(R.id.sl_control_icon);
            imageView.setImageResource(R.drawable.sl_button_more);
            imageView.setEnabled(true);
            imageView.setOnClickListener(this);
            b();
            a("mode");
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i) {
        AlertDialog create;
        switch (i) {
            case 18:
                if (x().a() != 0) {
                    create = new AlertDialog.Builder(this).setItems(x().a(), this).create();
                } else {
                    create = new AlertDialog.Builder(this).setItems(x().b(), this).create();
                }
                create.setOnDismissListener(this);
                return create;
            default:
                return super.onCreateDialog(i);
        }
    }

    private void b() {
        b(e(((Integer) m().a("mode")).intValue()));
    }

    public final void a(s sVar, String str, Object obj, Object obj2) {
        if (str.equals("mode") && obj2 != null && !obj2.equals(obj)) {
            b();
        }
    }
}
