package com.scoreloop.client.android.ui.component.base;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.scoreloop.client.android.ui.framework.a;
import org.anddev.andengine.R;

public final class n extends a {
    public n(Context context, String str) {
        super(context, null, str);
    }

    public final int a() {
        return 2;
    }

    public final View a(View view) {
        View view2;
        if (view == null) {
            view2 = e().inflate((int) R.layout.sl_list_item_caption, (ViewGroup) null);
        } else {
            view2 = view;
        }
        ((TextView) view2.findViewById(R.id.sl_list_item_caption_title)).setText(f());
        return view2;
    }

    public final boolean b() {
        return false;
    }
}
