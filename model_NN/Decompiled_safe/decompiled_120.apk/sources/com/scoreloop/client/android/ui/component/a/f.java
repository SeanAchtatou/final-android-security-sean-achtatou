package com.scoreloop.client.android.ui.component.a;

import com.scoreloop.client.android.core.addon.RSSFeed;
import com.scoreloop.client.android.core.addon.RSSItem;
import com.scoreloop.client.android.ui.framework.s;
import java.util.Iterator;
import java.util.List;

final class f implements RSSFeed.Continuation {
    private /* synthetic */ j a;
    private final /* synthetic */ s b;

    f(j jVar, s sVar) {
        this.a = jVar;
        this.b = sVar;
    }

    public final void withLoadedFeed(List list, Exception exc) {
        if (list != null) {
            this.b.b("newsFeed", list);
            Iterator it = list.iterator();
            int i = 0;
            while (it.hasNext()) {
                if (!((RSSItem) it.next()).hasPersistentReadFlag()) {
                    i++;
                }
            }
            this.b.b("newsNumberUnreadItems", Integer.valueOf(i));
        }
    }
}
