package com.scoreloop.client.android.ui.component.base;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.ui.framework.a;
import com.scoreloop.client.android.ui.framework.i;
import com.scoreloop.client.android.ui.framework.q;
import org.anddev.andengine.R;

public abstract class ComponentListActivity extends ComponentActivity implements View.OnClickListener, AdapterView.OnItemClickListener, q {
    private a a;
    private boolean b = true;

    public final void s() {
        this.b = false;
    }

    public final i t() {
        return (i) u().getAdapter();
    }

    public final ListView u() {
        return (ListView) findViewById(R.id.sl_list);
    }

    public final void v() {
        ViewGroup viewGroup = (ViewGroup) findViewById(R.id.sl_footer);
        if (viewGroup != null) {
            this.a = null;
            viewGroup.removeAllViews();
        }
    }

    public void onClick(View view) {
        if (this.a != null) {
            b(this.a);
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        b((int) R.layout.sl_list_view);
        u().setFocusable(true);
        u().setOnItemClickListener(this);
    }

    /* access modifiers changed from: protected */
    public void b(a aVar) {
    }

    public void onItemClick(AdapterView adapterView, View view, int i, long j) {
        i t = t();
        if (((a) t.getItem(i)).b()) {
            t.a(i);
        }
    }

    public void a(a aVar) {
    }

    public final void a(ListAdapter listAdapter) {
        u().setAdapter(listAdapter);
        t().a(this);
    }

    public final void c(a aVar) {
        View a2;
        ViewGroup viewGroup = (ViewGroup) findViewById(R.id.sl_footer);
        if (viewGroup != null && (a2 = aVar.a((View) null)) != null) {
            this.a = aVar;
            viewGroup.addView(a2);
            if (aVar.b()) {
                a2.setOnClickListener(this);
            }
        }
    }

    public final boolean a(Menu menu) {
        getMenuInflater().inflate(R.menu.sl_options_menu, menu);
        return super.a(menu);
    }

    public final boolean b(Menu menu) {
        MenuItem findItem = menu.findItem(R.id.sl_item_account_settings);
        if (findItem != null) {
            findItem.setVisible(this.b);
        }
        return super.b(menu);
    }

    public final boolean a(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.sl_item_account_settings /*2131361904*/:
                B();
                a(A().e(Session.getCurrentSession().getUser()));
                return true;
            default:
                return super.a(menuItem);
        }
    }
}
