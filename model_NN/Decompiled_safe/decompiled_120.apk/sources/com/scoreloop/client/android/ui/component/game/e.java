package com.scoreloop.client.android.ui.component.game;

import android.graphics.drawable.Drawable;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.ui.component.base.ComponentActivity;
import com.scoreloop.client.android.ui.component.base.k;
import org.anddev.andengine.R;

public final class e extends k {
    public e(ComponentActivity componentActivity, Drawable drawable, Game game) {
        super(componentActivity, drawable, game.getName(), game.getPublisherName(), game);
    }

    /* access modifiers changed from: protected */
    public final String i() {
        return ((Game) p()).getImageUrl();
    }

    public final int a() {
        return 12;
    }

    public final Drawable d() {
        return c().getResources().getDrawable(R.drawable.sl_icon_games);
    }
}
