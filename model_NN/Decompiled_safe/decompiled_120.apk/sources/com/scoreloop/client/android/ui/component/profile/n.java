package com.scoreloop.client.android.ui.component.profile;

enum n {
    USERNAME(12),
    EMAIL(13),
    USERNAME_EMAIL(14),
    MERGE_ACCOUNTS(17);
    
    private final int e;

    private n(int i) {
        this.e = i;
    }

    public final int a() {
        return this.e;
    }
}
