package com.scoreloop.client.android.ui;

import android.os.Bundle;
import com.scoreloop.client.android.ui.component.base.a;
import com.scoreloop.client.android.ui.component.base.o;
import com.scoreloop.client.android.ui.framework.ScreenActivity;

public class ChallengesScreenActivity extends ScreenActivity {
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        c cVar = (c) f.a();
        if (!a.a(o.CHALLENGE)) {
            finish();
        } else {
            a(cVar.c(null), bundle);
        }
    }
}
