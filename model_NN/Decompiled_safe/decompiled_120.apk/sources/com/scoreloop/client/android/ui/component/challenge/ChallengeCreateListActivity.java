package com.scoreloop.client.android.ui.component.challenge;

import android.os.Bundle;
import com.scoreloop.client.android.core.controller.ChallengeController;
import com.scoreloop.client.android.core.controller.ChallengeControllerObserver;
import com.scoreloop.client.android.core.model.Money;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.ui.component.a.d;
import com.scoreloop.client.android.ui.component.base.m;
import com.scoreloop.client.android.ui.component.base.n;
import com.scoreloop.client.android.ui.framework.ag;
import com.scoreloop.client.android.ui.framework.aj;
import com.scoreloop.client.android.ui.framework.h;
import com.scoreloop.client.android.ui.framework.s;
import com.scoreloop.client.android.ui.framework.w;
import org.anddev.andengine.R;

public class ChallengeCreateListActivity extends ChallengeActionListActivity implements ChallengeControllerObserver, j, ag {
    private n a;
    private i b;
    private e c;
    private User d;
    private s e;

    public void challengeControllerDidFailOnInsufficientBalance(ChallengeController challengeController) {
        throw new IllegalStateException("this should not happen - an insufficient balance error occured while creating the challenge");
    }

    public void challengeControllerDidFailToAcceptChallenge(ChallengeController challengeController) {
        throw new IllegalStateException("this should not happen - an accept error occured while creating the challenge");
    }

    public void challengeControllerDidFailToRejectChallenge(ChallengeController challengeController) {
        throw new IllegalStateException("this should not happen - a reject error occured while creating the challenge");
    }

    /* access modifiers changed from: package-private */
    public final n a() {
        return new n(this, getString(R.string.sl_new_challenge));
    }

    /* access modifiers changed from: package-private */
    public final n b() {
        this.a = new n(this, null, this);
        return this.a;
    }

    /* access modifiers changed from: package-private */
    public final i c() {
        if (this.b == null) {
            this.b = new i(this, F(), this.d);
        }
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public final q d() {
        this.c = new e(this);
        return this.c;
    }

    public final void a(w wVar, int i) {
        wVar.dismiss();
        i();
    }

    public final void e() {
        Money h;
        if (h() && (h = this.c.h()) != null) {
            ChallengeController challengeController = new ChallengeController(this);
            challengeController.createChallenge(h, this.d);
            Integer g = this.c.g();
            if (g != null) {
                challengeController.getChallenge().setMode(g);
            }
            j();
            D().a(challengeController.getChallenge());
        }
    }

    public final void f() {
        throw new IllegalStateException("this should not happen - a button has been clicked that isn't there");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.client.android.ui.framework.s.a(java.lang.String, com.scoreloop.client.android.ui.framework.aj):void
     arg types: [java.lang.String, com.scoreloop.client.android.ui.component.challenge.ChallengeCreateListActivity]
     candidates:
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, boolean):com.scoreloop.client.android.ui.framework.g
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, com.scoreloop.client.android.ui.framework.j):void
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, java.lang.Object):java.lang.Object
      com.scoreloop.client.android.ui.framework.s.a(com.scoreloop.client.android.ui.framework.s, java.util.Set):void
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, com.scoreloop.client.android.ui.framework.aj):void */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        a(s.a("userValues", "numberChallengesWon"));
        this.d = (User) k().a("contestant", (Object) null);
        if (this.d != null) {
            this.e = new s();
            this.e.b("user", this.d);
            this.e.a("numberChallengesWon", (aj) this);
            this.e.a(new d());
            q();
        }
        g();
    }

    public final void a(int i) {
        if (this.d != null) {
            this.e.a("numberChallengesWon", h.NOT_DIRTY, (Object) null);
        }
    }

    public final void a(s sVar, String str, Object obj, Object obj2) {
        if (sVar == this.e) {
            if (a(str, "numberChallengesWon", obj, obj2)) {
                c().c(m.b(this, this.e));
                t().notifyDataSetChanged();
            }
        } else if (a(str, "numberChallengesWon", obj, obj2)) {
            c().b(m.b(this, sVar));
            t().notifyDataSetChanged();
        }
    }

    public final void a(s sVar, String str) {
        if (sVar == this.e) {
            this.e.a("numberChallengesWon", h.NOT_DIRTY, (Object) null);
        } else if ("numberChallengesWon".equals(str)) {
            y().a(str, h.NOT_DIRTY, (Object) null);
        }
    }
}
