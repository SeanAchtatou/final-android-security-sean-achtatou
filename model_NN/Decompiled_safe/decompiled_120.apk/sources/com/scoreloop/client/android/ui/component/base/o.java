package com.scoreloop.client.android.ui.component.base;

public enum o {
    ACHIEVEMENT("ui.feature.achievement", false),
    ADDRESS_BOOK("ui.feature.address_book", true),
    CHALLENGE("ui.feature.challenge", false),
    NEWS("ui.feature.news", false);
    
    private boolean e = true;
    private String f;

    private o(String str, boolean z) {
        this.f = str;
        this.e = z;
    }

    /* access modifiers changed from: package-private */
    public final String a() {
        return this.f;
    }

    /* access modifiers changed from: package-private */
    public final boolean b() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public final void a(boolean z) {
        this.e = z;
    }
}
