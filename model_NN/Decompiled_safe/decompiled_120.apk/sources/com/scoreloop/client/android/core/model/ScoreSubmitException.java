package com.scoreloop.client.android.core.model;

public class ScoreSubmitException extends Exception {
    private static final long serialVersionUID = 1;

    public ScoreSubmitException(String str, Throwable th) {
        super(str, th);
    }
}
