package com.scoreloop.client.android.ui.framework;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import java.util.Iterator;
import java.util.List;
import org.anddev.andengine.R;

public class ShortcutView extends SegmentedView {
    private List c = null;

    public ShortcutView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public final void a(Activity activity, List list) {
        LinearLayout.LayoutParams layoutParams;
        if (this.c == null || !this.c.equals(list)) {
            removeAllViews();
            this.c = list;
            Display defaultDisplay = activity.getWindowManager().getDefaultDisplay();
            Iterator it = list.iterator();
            while (it.hasNext()) {
                ae aeVar = (ae) it.next();
                ViewGroup viewGroup = (ViewGroup) activity.getLayoutInflater().inflate((int) R.layout.sl_tab_shortcut, (ViewGroup) null);
                int dimension = (int) getResources().getDimension(R.dimen.sl_margin_shortcut);
                DisplayMetrics displayMetrics = new DisplayMetrics();
                defaultDisplay.getMetrics(displayMetrics);
                int orientation = defaultDisplay.getOrientation();
                if (displayMetrics.widthPixels > displayMetrics.heightPixels || orientation == 3 || orientation == 1) {
                    layoutParams = new LinearLayout.LayoutParams(-1, 0, 1.0f);
                    layoutParams.gravity = 17;
                    layoutParams.leftMargin = dimension;
                    layoutParams.rightMargin = dimension;
                } else {
                    layoutParams = new LinearLayout.LayoutParams(0, -2, 1.0f);
                    layoutParams.gravity = 17;
                    layoutParams.bottomMargin = dimension;
                    layoutParams.topMargin = dimension;
                }
                viewGroup.setLayoutParams(layoutParams);
                viewGroup.setId(aeVar.c());
                ((ImageView) viewGroup.findViewById(R.id.sl_image_tab_view)).setImageResource(aeVar.b());
                addView(viewGroup);
            }
            c();
            return;
        }
        this.c = list;
    }

    /* access modifiers changed from: protected */
    public final void a(int i, boolean z) {
        View childAt = getChildAt(i);
        ae aeVar = (ae) this.c.get(i);
        if (z) {
            ((ImageView) childAt.findViewById(R.id.sl_image_tab_view)).setImageResource(aeVar.a());
            childAt.setBackgroundResource(R.drawable.sl_shortcut_highlight);
            return;
        }
        ((ImageView) childAt.findViewById(R.id.sl_image_tab_view)).setImageResource(aeVar.b());
        childAt.setBackgroundDrawable(null);
    }
}
