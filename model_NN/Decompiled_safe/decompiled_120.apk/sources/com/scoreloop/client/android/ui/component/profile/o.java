package com.scoreloop.client.android.ui.component.profile;

import android.content.Context;
import com.scoreloop.client.android.ui.component.base.n;
import com.scoreloop.client.android.ui.framework.i;
import org.anddev.andengine.R;

final class o extends i {
    private /* synthetic */ ProfileSettingsPictureListActivity b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public o(ProfileSettingsPictureListActivity profileSettingsPictureListActivity, Context context) {
        super(context);
        this.b = profileSettingsPictureListActivity;
        add(new n(context, profileSettingsPictureListActivity.getString(R.string.sl_change_picture)));
        add(profileSettingsPictureListActivity.b);
        add(profileSettingsPictureListActivity.c);
        add(profileSettingsPictureListActivity.f);
        add(profileSettingsPictureListActivity.d);
        add(profileSettingsPictureListActivity.e);
    }
}
