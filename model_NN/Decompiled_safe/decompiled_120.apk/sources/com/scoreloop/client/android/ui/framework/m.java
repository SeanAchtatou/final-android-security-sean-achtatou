package com.scoreloop.client.android.ui.framework;

import android.app.ActivityGroup;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.ViewSwitcher;
import org.anddev.andengine.R;

public abstract class m {
    public static void a(ActivityGroup activityGroup, Intent intent, String str, int i, int i2) {
        View decorView = activityGroup.getLocalActivityManager().startActivity(str, intent).getDecorView();
        ViewParent parent = decorView.getParent();
        if (parent == null || !(parent instanceof ViewGroup)) {
            ViewGroup viewGroup = (ViewGroup) activityGroup.findViewById(i);
            if (i2 == 0 || !(viewGroup instanceof ViewSwitcher)) {
                viewGroup.removeAllViews();
                viewGroup.addView(decorView, new ViewGroup.LayoutParams(-1, -1));
                return;
            }
            ViewSwitcher viewSwitcher = (ViewSwitcher) viewGroup;
            if (i2 == 1) {
                viewSwitcher.setInAnimation(activityGroup, R.anim.sl_next_in);
                viewSwitcher.setOutAnimation(activityGroup, R.anim.sl_next_out);
            } else {
                viewSwitcher.setInAnimation(activityGroup, R.anim.sl_previous_in);
                viewSwitcher.setOutAnimation(activityGroup, R.anim.sl_previous_out);
            }
            int childCount = viewSwitcher.getChildCount();
            if (childCount == 0) {
                viewSwitcher.addView(decorView, 0, new ViewGroup.LayoutParams(-1, -1));
            } else if (childCount == 1) {
                viewSwitcher.addView(decorView, 1, new ViewGroup.LayoutParams(-1, -1));
                viewSwitcher.showNext();
            } else {
                viewSwitcher.removeViewAt(0);
                viewSwitcher.addView(decorView, 1, new ViewGroup.LayoutParams(-1, -1));
                viewSwitcher.showNext();
            }
        } else {
            throw new IllegalStateException("should not happen - currently we don't recycle activities");
        }
    }
}
