package com.scoreloop.client.android.ui.framework;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import org.anddev.andengine.R;

public final class af extends a {
    private static /* synthetic */ int[] b;
    private final t a;

    private static /* synthetic */ int[] h() {
        int[] iArr = b;
        if (iArr == null) {
            iArr = new int[t.values().length];
            try {
                iArr[t.PAGE_TO_NEXT.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[t.PAGE_TO_OWN.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[t.PAGE_TO_PREV.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[t.PAGE_TO_RECENT.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[t.PAGE_TO_TOP.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            b = iArr;
        }
        return iArr;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public af(android.content.Context r5, com.scoreloop.client.android.ui.framework.t r6) {
        /*
            r4 = this;
            r3 = 0
            android.content.res.Resources r0 = r5.getResources()
            int[] r1 = h()
            int r2 = r6.ordinal()
            r1 = r1[r2]
            switch(r1) {
                case 1: goto L_0x0027;
                case 2: goto L_0x0012;
                case 3: goto L_0x002f;
                case 4: goto L_0x0012;
                case 5: goto L_0x0037;
                default: goto L_0x0012;
            }
        L_0x0012:
            r0 = r3
        L_0x0013:
            int[] r1 = h()
            int r2 = r6.ordinal()
            r1 = r1[r2]
            switch(r1) {
                case 1: goto L_0x003f;
                case 2: goto L_0x0020;
                case 3: goto L_0x0047;
                case 4: goto L_0x0020;
                case 5: goto L_0x004f;
                default: goto L_0x0020;
            }
        L_0x0020:
            r1 = r3
        L_0x0021:
            r4.<init>(r5, r0, r1)
            r4.a = r6
            return
        L_0x0027:
            r1 = 2130837569(0x7f020041, float:1.7280096E38)
            android.graphics.drawable.Drawable r0 = r0.getDrawable(r1)
            goto L_0x0013
        L_0x002f:
            r1 = 2130837571(0x7f020043, float:1.72801E38)
            android.graphics.drawable.Drawable r0 = r0.getDrawable(r1)
            goto L_0x0013
        L_0x0037:
            r1 = 2130837577(0x7f020049, float:1.7280112E38)
            android.graphics.drawable.Drawable r0 = r0.getDrawable(r1)
            goto L_0x0013
        L_0x003f:
            r1 = 2131099761(0x7f060071, float:1.7811884E38)
            java.lang.String r1 = r5.getString(r1)
            goto L_0x0021
        L_0x0047:
            r1 = 2131099782(0x7f060086, float:1.7811927E38)
            java.lang.String r1 = r5.getString(r1)
            goto L_0x0021
        L_0x004f:
            r1 = 2131099803(0x7f06009b, float:1.781197E38)
            java.lang.String r1 = r5.getString(r1)
            goto L_0x0021
        */
        throw new UnsupportedOperationException("Method not decompiled: com.scoreloop.client.android.ui.framework.af.<init>(android.content.Context, com.scoreloop.client.android.ui.framework.t):void");
    }

    public final t g() {
        return this.a;
    }

    public final int a() {
        return 0;
    }

    public final View a(View view) {
        View view2;
        if (view == null) {
            view2 = e().inflate((int) R.layout.sl_list_item_icon_title_small, (ViewGroup) null);
        } else {
            view2 = view;
        }
        ((ImageView) view2.findViewById(R.id.sl_icon)).setImageDrawable(d());
        ((TextView) view2.findViewById(R.id.sl_title)).setText(f());
        return view2;
    }

    public final boolean b() {
        return true;
    }
}
