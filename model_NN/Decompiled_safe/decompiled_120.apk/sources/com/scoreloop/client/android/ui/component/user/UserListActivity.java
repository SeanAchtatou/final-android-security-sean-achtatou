package com.scoreloop.client.android.ui.component.user;

import android.app.Dialog;
import android.os.Bundle;
import android.widget.ListAdapter;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.controller.RequestControllerObserver;
import com.scoreloop.client.android.core.controller.UserController;
import com.scoreloop.client.android.core.controller.UsersController;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.ui.component.a.e;
import com.scoreloop.client.android.ui.component.base.ComponentListActivity;
import com.scoreloop.client.android.ui.component.base.l;
import com.scoreloop.client.android.ui.component.base.n;
import com.scoreloop.client.android.ui.framework.a;
import com.scoreloop.client.android.ui.framework.ag;
import com.scoreloop.client.android.ui.framework.ai;
import com.scoreloop.client.android.ui.framework.h;
import com.scoreloop.client.android.ui.framework.i;
import com.scoreloop.client.android.ui.framework.s;
import com.scoreloop.client.android.ui.framework.w;
import com.scoreloop.client.android.ui.framework.y;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.anddev.andengine.R;

public class UserListActivity extends ComponentListActivity implements RequestControllerObserver, ag {
    private static /* synthetic */ int[] j;
    private a a;
    private List b;
    private List c;
    private a d;
    private h e = h.NONE;
    private a f;
    private boolean g = true;
    private UserController h;
    private UsersController i;

    private static /* synthetic */ int[] b() {
        int[] iArr = j;
        if (iArr == null) {
            iArr = new int[h.values().length];
            try {
                iArr[h.LOAD_BUDDIES.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[h.LOAD_RECOMMENDATIONS.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[h.NONE.ordinal()] = 3;
            } catch (NoSuchFieldError e4) {
            }
            j = iArr;
        }
        return iArr;
    }

    private void a(i iVar, List list, Boolean bool, boolean z) {
        Iterator it = list.iterator();
        int i2 = 0;
        while (it.hasNext()) {
            User user = (User) it.next();
            if (!z || (i2 = i2 + 1) <= 2) {
                iVar.add(new g(this, getResources().getDrawable(R.drawable.sl_icon_user), user, bool.booleanValue()));
            } else {
                if (this.f == null) {
                    this.f = new l(this);
                }
                iVar.add(this.f);
                return;
            }
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        a((ListAdapter) new i(this));
        this.h = new UserController(this);
        this.i = new UsersController(this);
        a(s.a("userValues", "numberBuddies"));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.client.android.ui.component.base.ComponentActivity.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.scoreloop.client.android.ui.component.user.UserListActivity.a(com.scoreloop.client.android.ui.framework.s, java.lang.String):void
      com.scoreloop.client.android.ui.component.user.UserListActivity.a(com.scoreloop.client.android.ui.framework.w, int):void
      com.scoreloop.client.android.ui.component.base.ComponentActivity.a(com.scoreloop.client.android.core.controller.RequestController, java.lang.Exception):void
      com.scoreloop.client.android.ui.framework.BaseActivity.a(android.content.Context, java.lang.String):void
      com.scoreloop.client.android.ui.framework.BaseActivity.a(int, com.scoreloop.client.android.ui.framework.y):void
      com.scoreloop.client.android.ui.framework.BaseActivity.a(com.scoreloop.client.android.ui.framework.s, java.lang.String):void
      com.scoreloop.client.android.ui.framework.aj.a(com.scoreloop.client.android.ui.framework.s, java.lang.String):void
      com.scoreloop.client.android.ui.framework.ag.a(com.scoreloop.client.android.ui.framework.w, int):void
      com.scoreloop.client.android.ui.component.base.ComponentActivity.a(int, boolean):void */
    public final void a(a aVar) {
        if (aVar == this.a) {
            a(A().h());
        } else if (aVar == this.d) {
            a(10, true);
        } else if (aVar == this.f) {
            this.g = false;
            a();
        } else if (aVar instanceof g) {
            g gVar = (g) aVar;
            a(A().a((User) gVar.p(), Boolean.valueOf(gVar.m())));
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i2) {
        switch (i2) {
            case 10:
                ai aiVar = new ai(this);
                aiVar.b(getResources().getString(R.string.sl_leave_accept_match_buddies));
                aiVar.c(getResources().getString(R.string.sl_leave_accept_match_buddies_ok));
                aiVar.a((ag) this);
                aiVar.setCancelable(true);
                aiVar.setOnDismissListener(this);
                return aiVar;
            default:
                return super.onCreateDialog(i2);
        }
    }

    public final void a(w wVar, int i2) {
        wVar.dismiss();
        if (i2 == 0) {
            a(h.LOAD_RECOMMENDATIONS.ordinal(), y.SET);
            p();
        }
    }

    public final void a(int i2) {
        this.e = h.values()[i2];
        User F = F();
        Game C = C();
        switch (b()[this.e.ordinal()]) {
            case 1:
                this.b = null;
                this.c = null;
                this.h.setUser(F);
                b(this.h);
                this.h.loadBuddies();
                if (C != null) {
                    b(this.i);
                    this.i.loadBuddies(F, C);
                    return;
                }
                return;
            case 2:
                b(this.i);
                this.i.loadRecommendedBuddies(1);
                return;
            default:
                return;
        }
    }

    public final void a(s sVar, String str, Object obj, Object obj2) {
        if (a(str, "numberBuddies", obj, obj2)) {
            a(h.LOAD_BUDDIES.ordinal(), y.SET);
        }
    }

    public final void a(s sVar, String str) {
        if ("numberBuddies".equals(str)) {
            y().a(str, h.NOT_DIRTY, (Object) null);
        }
    }

    public final void a(RequestController requestController) {
        h hVar = this.e;
        this.e = h.NONE;
        switch (b()[hVar.ordinal()]) {
            case 1:
                if (requestController == this.i) {
                    this.c = this.i.getUsers();
                } else if (requestController == this.h) {
                    this.b = this.h.getUser().getBuddyUsers();
                    if (this.b != null) {
                        this.b = new ArrayList(this.b);
                    }
                }
                if ((C() == null || this.c != null) && this.b != null) {
                    if (C() != null) {
                        for (User user : this.c) {
                            int i2 = 0;
                            while (true) {
                                if (i2 < this.b.size()) {
                                    if (((User) this.b.get(i2)).getIdentifier().equals(user.getIdentifier())) {
                                        this.b.remove(i2);
                                    } else {
                                        i2++;
                                    }
                                }
                            }
                        }
                    }
                    a();
                    return;
                }
                this.e = hVar;
                return;
            case 2:
                if (requestController == this.i) {
                    List users = this.i.getUsers();
                    if (users.size() > 0) {
                        r();
                        e.a(this, (User) users.get(0), z(), new b(this));
                        return;
                    }
                    return;
                }
                return;
            default:
                return;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.client.android.ui.component.user.UserListActivity.a(com.scoreloop.client.android.ui.framework.i, java.util.List, java.lang.Boolean, boolean):void
     arg types: [com.scoreloop.client.android.ui.framework.i, java.util.List, int, boolean]
     candidates:
      com.scoreloop.client.android.ui.component.user.UserListActivity.a(com.scoreloop.client.android.ui.framework.s, java.lang.String, java.lang.Object, java.lang.Object):void
      com.scoreloop.client.android.ui.component.base.ComponentActivity.a(java.lang.String, java.lang.String, java.lang.Object, java.lang.Object):boolean
      com.scoreloop.client.android.ui.framework.BaseActivity.a(com.scoreloop.client.android.ui.framework.s, java.lang.String, java.lang.Object, java.lang.Object):void
      com.scoreloop.client.android.ui.framework.aj.a(com.scoreloop.client.android.ui.framework.s, java.lang.String, java.lang.Object, java.lang.Object):void
      com.scoreloop.client.android.ui.component.user.UserListActivity.a(com.scoreloop.client.android.ui.framework.i, java.util.List, java.lang.Boolean, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.client.android.ui.component.user.UserListActivity.a(com.scoreloop.client.android.ui.framework.i, java.util.List, java.lang.Boolean, boolean):void
     arg types: [com.scoreloop.client.android.ui.framework.i, java.util.List, int, int]
     candidates:
      com.scoreloop.client.android.ui.component.user.UserListActivity.a(com.scoreloop.client.android.ui.framework.s, java.lang.String, java.lang.Object, java.lang.Object):void
      com.scoreloop.client.android.ui.component.base.ComponentActivity.a(java.lang.String, java.lang.String, java.lang.Object, java.lang.Object):boolean
      com.scoreloop.client.android.ui.framework.BaseActivity.a(com.scoreloop.client.android.ui.framework.s, java.lang.String, java.lang.Object, java.lang.Object):void
      com.scoreloop.client.android.ui.framework.aj.a(com.scoreloop.client.android.ui.framework.s, java.lang.String, java.lang.Object, java.lang.Object):void
      com.scoreloop.client.android.ui.component.user.UserListActivity.a(com.scoreloop.client.android.ui.framework.i, java.util.List, java.lang.Boolean, boolean):void */
    private void a() {
        boolean z;
        boolean z2;
        i t = t();
        t.clear();
        boolean G = G();
        if (G) {
            if (this.a == null) {
                this.a = new l(this);
            }
            t.add(this.a);
        }
        int size = this.b.size();
        if (C() == null || this.c.size() <= 0) {
            z = false;
        } else {
            t.add(new n(this, String.format(getString(R.string.sl_format_friends_playing), C().getName())));
            a(t, this.c, (Boolean) true, this.g && size > 0);
            z = true;
        }
        if (size > 0) {
            t.add(new n(this, getString(R.string.sl_friends)));
            a(t, this.b, (Boolean) false, false);
            z2 = true;
        } else {
            z2 = z;
        }
        if (z2) {
            return;
        }
        if (G) {
            if (this.d == null) {
                this.d = new i(this);
            }
            t.add(this.d);
            return;
        }
        t.add(new n(this, getString(R.string.sl_friends)));
        t.add(new com.scoreloop.client.android.ui.component.base.h(this, getString(R.string.sl_no_friends)));
    }
}
