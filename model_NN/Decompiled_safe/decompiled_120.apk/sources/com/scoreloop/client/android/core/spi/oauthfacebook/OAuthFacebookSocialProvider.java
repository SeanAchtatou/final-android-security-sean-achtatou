package com.scoreloop.client.android.core.spi.oauthfacebook;

import com.scoreloop.client.android.core.model.SocialProvider;

public final class OAuthFacebookSocialProvider extends SocialProvider {
    public final Class b() {
        return OAuthFacebookSocialProviderController.class;
    }

    public final String getIdentifier() {
        return SocialProvider.FACEBOOK_IDENTIFIER;
    }
}
