package com.scoreloop.client.android.ui.framework;

public final class b {
    /* access modifiers changed from: private */
    public final b a;
    /* access modifiers changed from: private */
    public r b;
    private final u c;

    public static int a(b bVar) {
        if (bVar == null) {
            return 0;
        }
        return a(bVar.a) + 1;
    }

    /* synthetic */ b(b bVar, u uVar, r rVar) {
        this(bVar, uVar, rVar, (byte) 0);
    }

    private b(b bVar, u uVar, r rVar, byte b2) {
        this.a = bVar;
        this.c = uVar;
        this.b = rVar;
    }

    /* access modifiers changed from: package-private */
    public final k a(String str) {
        k a2 = this.c.a(str);
        if (a2 != null || this.a == null) {
            return a2;
        }
        return this.a.a(str);
    }

    /* access modifiers changed from: package-private */
    public final r a() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public final u b() {
        return this.c;
    }
}
