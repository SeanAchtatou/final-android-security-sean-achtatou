package com.scoreloop.client.android.ui.component.base;

import android.content.Context;
import android.graphics.drawable.Drawable;
import com.scoreloop.client.android.core.model.Money;
import com.scoreloop.client.android.core.model.Ranking;
import com.scoreloop.client.android.core.model.Score;
import com.scoreloop.client.android.core.model.ScoreFormatter;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.ui.framework.s;
import java.math.BigDecimal;
import java.util.List;
import org.anddev.andengine.R;

public final class m {
    private static final BigDecimal a = new BigDecimal(100);

    public static String a(Score score, a aVar) {
        ScoreFormatter.ScoreFormatKey e = aVar.e();
        ScoreFormatter h = aVar.h();
        if (e == null) {
            return d(score, aVar);
        }
        return h.formatScore(score, e);
    }

    public static String b(Score score, a aVar) {
        ScoreFormatter.ScoreFormatKey d = aVar.d();
        ScoreFormatter h = aVar.h();
        if (d == null) {
            return d(score, aVar);
        }
        return h.formatScore(score, d);
    }

    public static String a(Money money, a aVar) {
        BigDecimal divide = money.getAmount().divide(a);
        String applicationCurrencyNamePlural = Money.getApplicationCurrencyNamePlural();
        if (divide.equals(BigDecimal.ONE)) {
            applicationCurrencyNamePlural = Money.getApplicationCurrencyNameSingular();
        }
        return String.format(aVar.c(), divide, applicationCurrencyNamePlural);
    }

    public static String a(Context context, Ranking ranking) {
        int intValue;
        int i;
        if (ranking == null || (intValue = ranking.getTotal().intValue()) == 0) {
            return "";
        }
        int intValue2 = (ranking.getRank().intValue() * 100) / intValue;
        if (intValue2 == 0) {
            i = 1;
        } else if (intValue2 == 100) {
            i = 100;
        } else {
            i = ((intValue2 + 5) / 5) * 5;
        }
        return String.format(context.getString(R.string.sl_format_leaderboards_percent), Integer.valueOf(i));
    }

    private static String d(Score score, a aVar) {
        String g = aVar.g();
        if (g == null) {
            return aVar.h().formatScore(score, ScoreFormatter.ScoreFormatKey.ScoresOnlyFormat);
        }
        return String.format(g, score.getResult());
    }

    public static String c(Score score, a aVar) {
        ScoreFormatter.ScoreFormatKey f = aVar.f();
        ScoreFormatter h = aVar.h();
        if (f == null) {
            return d(score, aVar);
        }
        return h.formatScore(score, f);
    }

    public static String a(Context context, s sVar, boolean z) {
        Integer num = (Integer) sVar.a("numberAchievements");
        Integer num2 = (Integer) sVar.a("numberAwards");
        if (num == null || num2 == null) {
            return "";
        }
        return String.format(context.getString(z ? R.string.sl_format_achievements_extended : R.string.sl_format_achievements), num, num2);
    }

    public static String a(Context context, s sVar, a aVar) {
        Money money = (Money) sVar.a("userBalance");
        if (money == null) {
            return "";
        }
        return String.format(context.getString(R.string.sl_format_balance), a(money, aVar));
    }

    public static String a(Context context, s sVar) {
        Integer num = (Integer) sVar.a("numberBuddies");
        if (num == null) {
            return "";
        }
        return String.format(context.getString(R.string.sl_format_number_friends), num);
    }

    public static String b(Context context, s sVar) {
        Integer num = (Integer) sVar.a("numberChallengesWon");
        Integer num2 = (Integer) sVar.a("numberChallengesPlayed");
        if (num == null || num2 == null) {
            return "";
        }
        return String.format(context.getString(R.string.sl_format_challenge_stats), num, num2);
    }

    public static String c(Context context, s sVar) {
        Integer num = (Integer) sVar.a("numberGames");
        if (num == null) {
            return "";
        }
        return String.format(context.getString(R.string.sl_format_number_games), num);
    }

    public static Drawable b(Context context, s sVar, boolean z) {
        int i;
        List list = (List) sVar.a("newsFeed");
        Integer num = (Integer) sVar.a("newsNumberUnreadItems");
        if (list == null || num == null || list.size() == 0 || num.intValue() > 0) {
            i = z ? R.drawable.sl_header_icon_news_closed : R.drawable.sl_icon_news_closed;
        } else {
            i = z ? R.drawable.sl_header_icon_news_opened : R.drawable.sl_icon_news_opened;
        }
        return context.getResources().getDrawable(i);
    }

    public static String d(Context context, s sVar) {
        Integer num = (Integer) sVar.a("newsNumberUnreadItems");
        if (num == null) {
            return "";
        }
        if (num.intValue() == 0) {
            List list = (List) sVar.a("newsFeed");
            if (list == null || list.size() == 0) {
                return context.getString(R.string.sl_no_news);
            }
            return context.getString(R.string.sl_no_news_items);
        } else if (num.intValue() == 1) {
            return context.getString(R.string.sl_one_news_item);
        } else {
            return String.format(context.getString(R.string.sl_format_new_news_items), num);
        }
    }

    public static String a(Context context, Score score) {
        Integer rank = score.getRank();
        User user = score.getUser();
        if (user == null) {
            user = Session.getCurrentSession().getUser();
        }
        return String.format(context.getString(R.string.sl_format_score_title), rank, user.getDisplayName());
    }
}
