package com.scoreloop.client.android.ui.framework;

import android.content.Context;
import android.util.AttributeSet;
import org.anddev.andengine.R;
import org.anddev.andengine.input.touch.TouchEvent;

public class TabView extends SegmentedView {
    private final int[][] c = {new int[]{R.drawable.sl_tab_active, R.drawable.sl_tab_default_left_border}, new int[]{R.drawable.sl_tab_default_right_border, R.drawable.sl_tab_active}};
    private final int[][] d = {new int[]{R.drawable.sl_tab_active, R.drawable.sl_tab_default_left_border, R.drawable.sl_tab_default_left_border}, new int[]{R.drawable.sl_tab_default_right_border, R.drawable.sl_tab_active, R.drawable.sl_tab_default_left_border}, new int[]{R.drawable.sl_tab_default_right_border, R.drawable.sl_tab_default_right_border, R.drawable.sl_tab_active}};
    private final int[][] e = {new int[]{R.drawable.sl_tab_active, R.drawable.sl_tab_default_left_border, R.drawable.sl_tab_default_left_border, R.drawable.sl_tab_default_left_border}, new int[]{R.drawable.sl_tab_default_right_border, R.drawable.sl_tab_active, R.drawable.sl_tab_default_left_border, R.drawable.sl_tab_default_left_border}, new int[]{R.drawable.sl_tab_default_right_border, R.drawable.sl_tab_default_right_border, R.drawable.sl_tab_active, R.drawable.sl_tab_default_left_border}, new int[]{R.drawable.sl_tab_default_right_border, R.drawable.sl_tab_default_right_border, R.drawable.sl_tab_default_right_border, R.drawable.sl_tab_active}};

    public TabView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        if (getChildCount() != 0) {
            a(0);
        }
    }

    /* access modifiers changed from: protected */
    public final void a(int i, boolean z) {
        getChildAt(i).setEnabled(true);
    }

    public final void a(int i) {
        int[][] iArr;
        if (i != this.a) {
            int childCount = getChildCount();
            switch (childCount) {
                case 2:
                    iArr = this.c;
                    break;
                case TouchEvent.ACTION_CANCEL /*3*/:
                    iArr = this.d;
                    break;
                case 4:
                    iArr = this.e;
                    break;
                default:
                    throw new IllegalStateException("unsupported number of tabs");
            }
            for (int i2 = 0; i2 < childCount; i2++) {
                getChildAt(i2).setBackgroundResource(iArr[i][i2]);
            }
            b(i);
        }
    }
}
