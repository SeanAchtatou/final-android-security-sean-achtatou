package com.scoreloop.client.android.core.model;

import com.scoreloop.client.android.core.util.SetterIntent;
import org.json.JSONArray;
import org.json.JSONObject;

public class Game extends BaseEntity implements MessageTargetInterface {
    public static final String CONTEXT_KEY_LEVEL = "SLContextKeyLevel";
    public static final String CONTEXT_KEY_MINOR_RESULT = "SLContextKeyMinorResult";
    public static final String CONTEXT_KEY_MODE = "SLContextKeyMode";
    public static String a = "game";
    private String c;
    private String d;
    private String e;
    private String f;
    private Integer g;
    private Integer h;
    private Integer i;
    private Integer j;
    private String k;
    private String l;
    private String m;
    private String n;
    private String o;
    private String[] p;
    private AwardList q;
    private boolean r;
    private boolean s;

    private Game(String str) {
        super(str);
    }

    Game(String str, String str2) {
        if (str == null || str2 == null) {
            throw new IllegalArgumentException("id and secret must be passed");
        }
        b(str);
        this.l = str2;
    }

    public Game(JSONObject jSONObject) {
        a(jSONObject);
    }

    public static Entity a(Session session, String str) {
        if (session != null) {
            Game game = session.getGame();
            if (!(str == null || game == null || !str.equals(game.getIdentifier()))) {
                return game;
            }
        }
        if (str != null) {
            return new Game(str);
        }
        return null;
    }

    public String a() {
        return a;
    }

    public void a(AwardList awardList) {
        this.q = awardList;
    }

    /* access modifiers changed from: package-private */
    public void a(Integer num) {
        this.g = num;
    }

    public void a(String str) {
        this.c = str;
    }

    public void a(JSONObject jSONObject) {
        super.a(jSONObject);
        SetterIntent setterIntent = new SetterIntent();
        if (setterIntent.h(jSONObject, "name", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.k = (String) setterIntent.a();
        }
        if (setterIntent.d(jSONObject, "min_level", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.i = (Integer) setterIntent.a();
        }
        if (setterIntent.d(jSONObject, "max_level", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.g = (Integer) setterIntent.a();
        }
        if (setterIntent.d(jSONObject, "mode_count", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            if (this.j == null) {
                this.j = 0;
            }
            if (this.h == null) {
                this.h = (Integer) setterIntent.a();
            }
        }
        if (setterIntent.h(jSONObject, "characteristic", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.c = (String) setterIntent.a();
        }
        if (setterIntent.h(jSONObject, "description", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.d = (String) setterIntent.a();
        }
        if (setterIntent.h(jSONObject, "version", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.n = (String) setterIntent.a();
        }
        if (setterIntent.h(jSONObject, "image_url", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.f = (String) setterIntent.a();
        }
        if (setterIntent.h(jSONObject, "download_url", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.e = (String) setterIntent.a();
        }
        if (setterIntent.h(jSONObject, "state", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.m = (String) setterIntent.a();
        }
        if (setterIntent.h(jSONObject, "publisher_name", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.o = (String) setterIntent.a();
        }
        if (setterIntent.e(jSONObject, "android_package_names", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            JSONArray jSONArray = (JSONArray) setterIntent.a();
            this.p = new String[jSONArray.length()];
            for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                this.p[i2] = jSONArray.getString(i2);
            }
        }
    }

    public void a(boolean z) {
        this.r = z;
    }

    /* access modifiers changed from: package-private */
    public void b(Integer num) {
        this.h = num;
    }

    public void b(boolean z) {
        this.s = z;
    }

    public String c() {
        return this.l;
    }

    /* access modifiers changed from: package-private */
    public void c(Integer num) {
        this.i = num;
    }

    /* access modifiers changed from: package-private */
    public void c(String str) {
        this.k = str;
    }

    public JSONObject d() {
        JSONObject d2 = super.d();
        d2.put("secret", this.l);
        d2.put("name", this.k);
        d2.put("min_level", this.i);
        d2.put("max_level", this.g);
        d2.put("characteristic", this.c);
        d2.put("description", this.d);
        d2.put("version", this.n);
        d2.put("image_url", this.f);
        d2.put("download_url", this.e);
        d2.put("state", this.m);
        d2.put("publisher_name", this.o);
        return d2;
    }

    /* access modifiers changed from: package-private */
    public void d(Integer num) {
        this.j = num;
    }

    /* access modifiers changed from: package-private */
    public void d(String str) {
        this.n = str;
    }

    public AwardList e() {
        return this.q;
    }

    public boolean f() {
        return this.r;
    }

    public boolean g() {
        return this.s;
    }

    public String getDescription() {
        return this.d;
    }

    public String getDownloadUrl() {
        return this.e;
    }

    public String getImageUrl() {
        return this.f;
    }

    public Integer getLevelCount() {
        if (!hasLevels()) {
            return 1;
        }
        return Integer.valueOf(getMaxLevel().intValue() - getMinLevel().intValue());
    }

    public Integer getMaxLevel() {
        return this.g;
    }

    public Integer getMaxMode() {
        return this.h;
    }

    public Integer getMinLevel() {
        return this.i;
    }

    public Integer getMinMode() {
        return this.j;
    }

    public Integer getModeCount() {
        if (!hasModes()) {
            return 1;
        }
        return Integer.valueOf(getMaxMode().intValue() - getMinMode().intValue());
    }

    public String getName() {
        return this.k;
    }

    public String[] getPackageNames() {
        return this.p;
    }

    public String getPublisherName() {
        return this.o;
    }

    public String getVersion() {
        return this.n;
    }

    public boolean hasLevels() {
        return (getMinLevel() == null || getMaxLevel() == null) ? false : true;
    }

    public boolean hasModes() {
        return (getMinMode() == null || getMaxMode() == null || getMaxMode().intValue() - getMinMode().intValue() <= 1) ? false : true;
    }
}
