package com.scoreloop.client.android.ui.component.challenge;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.scoreloop.client.android.core.model.Challenge;
import com.scoreloop.client.android.ui.component.base.ComponentActivity;
import com.scoreloop.client.android.ui.component.base.m;
import com.scoreloop.client.android.ui.framework.a;
import org.anddev.andengine.R;

class q extends a {
    private final Challenge a;

    public q(ComponentActivity componentActivity, Challenge challenge) {
        super(componentActivity, null, null);
        this.a = challenge;
    }

    /* access modifiers changed from: protected */
    public final ComponentActivity i() {
        return (ComponentActivity) c();
    }

    public int a() {
        return 9;
    }

    public View a(View view) {
        View view2;
        if (view == null) {
            view2 = e().inflate((int) R.layout.sl_list_item_challenge_settings, (ViewGroup) null);
        } else {
            view2 = view;
        }
        b(view2);
        return view2;
    }

    public boolean b() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void b(View view) {
        ((TextView) view.findViewById(R.id.stake)).setText(String.format(c().getResources().getString(R.string.sl_format_stake), m.a(this.a.getStake(), ((ComponentActivity) c()).x())));
        TextView textView = (TextView) view.findViewById(R.id.mode);
        if (((ComponentActivity) c()).C().hasModes()) {
            textView.setVisibility(0);
            textView.setText(((ComponentActivity) c()).e(this.a.getMode().intValue()));
            return;
        }
        textView.setVisibility(8);
    }
}
