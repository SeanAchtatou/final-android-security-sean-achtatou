package com.scoreloop.client.android.ui.framework;

import android.content.Intent;

public final class k {
    private static int a = 0;
    private final s b = new s();
    private boolean c = true;
    private final String d;
    private final Intent e;
    private final int f;
    private boolean g = false;

    k(int i, Intent intent) {
        StringBuilder sb = new StringBuilder("pane-");
        int i2 = a + 1;
        a = i2;
        this.d = sb.append(i2).toString();
        this.f = i;
        this.e = intent;
    }

    public final s a() {
        return this.b;
    }

    public final Intent b() {
        this.e.putExtra("activityIdentifier", this.d);
        if (!this.c || !this.g) {
            this.e.setFlags(this.e.getFlags() & -67108865);
        } else {
            this.e.addFlags(67108864);
        }
        return this.e;
    }

    public final int c() {
        return this.f;
    }

    /* access modifiers changed from: package-private */
    public final boolean a(String str) {
        return this.d.equals(str);
    }

    /* access modifiers changed from: package-private */
    public final void a(boolean z) {
        this.c = z;
    }

    /* access modifiers changed from: package-private */
    public final void d() {
        this.g = true;
    }
}
