package com.scoreloop.client.android.ui.component.news;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.scoreloop.client.android.core.addon.RSSItem;
import com.scoreloop.client.android.ui.a.k;
import org.anddev.andengine.R;

public final class a extends com.scoreloop.client.android.ui.framework.a {
    private final RSSItem a;

    public a(Context context, RSSItem rSSItem) {
        super(context, null, rSSItem.getTitle());
        this.a = rSSItem;
    }

    public final RSSItem g() {
        return this.a;
    }

    public final int a() {
        return 15;
    }

    public final View a(View view) {
        View view2;
        if (view == null) {
            view2 = e().inflate((int) R.layout.sl_list_item_news, (ViewGroup) null);
        } else {
            view2 = view;
        }
        ImageView imageView = (ImageView) view2.findViewById(R.id.sl_list_item_news_icon);
        int i = this.a.hasPersistentReadFlag() ? R.drawable.sl_icon_news_opened : R.drawable.sl_icon_news_closed;
        imageView.setImageResource(i);
        String imageUrlString = this.a.getImageUrlString();
        if (imageUrlString != null) {
            k.a(imageUrlString, c().getResources().getDrawable(i), imageView);
        }
        ((TextView) view2.findViewById(R.id.sl_list_item_news_title)).setText(f());
        ((TextView) view2.findViewById(R.id.sl_list_item_news_description)).setText(this.a.getDescription());
        view2.findViewById(R.id.sl_list_item_news_accessory).setVisibility(b() ? 0 : 4);
        return view2;
    }

    public final boolean b() {
        return this.a.getLinkUrlString() != null;
    }
}
