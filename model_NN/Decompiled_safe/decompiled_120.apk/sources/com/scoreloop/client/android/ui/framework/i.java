package com.scoreloop.client.android.ui.framework;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

public class i extends ArrayAdapter {
    private static int b = 1;
    protected q a;

    public static void a() {
        b = 29;
    }

    public i(Context context) {
        super(context, 0);
    }

    public int getItemViewType(int i) {
        return ((a) getItem(i)).a();
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        return ((a) getItem(i)).a(view);
    }

    public int getViewTypeCount() {
        return b;
    }

    public boolean isEnabled(int i) {
        return ((a) getItem(i)).b();
    }

    public void a(int i) {
        if (this.a != null) {
            this.a.a((a) getItem(i));
        }
    }

    public final void a(q qVar) {
        this.a = qVar;
    }
}
