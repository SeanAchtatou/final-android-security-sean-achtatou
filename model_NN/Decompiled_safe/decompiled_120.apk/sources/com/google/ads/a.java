package com.google.ads;

public enum a {
    MALE("m"),
    FEMALE("f");
    
    private String c;

    private a(String str) {
        this.c = str;
    }

    public final String toString() {
        return this.c;
    }
}
