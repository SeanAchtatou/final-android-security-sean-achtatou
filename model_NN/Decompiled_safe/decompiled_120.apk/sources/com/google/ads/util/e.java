package com.google.ads.util;

import java.io.UnsupportedEncodingException;

public class e {
    private static /* synthetic */ boolean a = (!e.class.desiredAssertionStatus());

    private e() {
    }

    public static String a(byte[] bArr) {
        try {
            int length = bArr.length;
            b bVar = new b();
            int i = (length / 3) * 4;
            if (!bVar.a) {
                switch (length % 3) {
                    case 1:
                        i += 2;
                        break;
                    case 2:
                        i += 3;
                        break;
                }
            } else if (length % 3 > 0) {
                i += 4;
            }
            if (bVar.b && length > 0) {
                i += (((length - 1) / 57) + 1) * (bVar.c ? 2 : 1);
            }
            bVar.d = new byte[i];
            bVar.a(bArr, length);
            if (a || bVar.e == i) {
                return new String(bVar.d, "US-ASCII");
            }
            throw new AssertionError();
        } catch (UnsupportedEncodingException e) {
            throw new AssertionError(e);
        }
    }
}
