package com.google.ads.util;

public final class b extends c {
    private static final byte[] f = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 43, 47};
    private static final byte[] g = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 45, 95};
    private static /* synthetic */ boolean l = (!e.class.desiredAssertionStatus());
    public final boolean a = false;
    public final boolean b = false;
    public final boolean c = false;
    private final byte[] h = new byte[2];
    private int i = 0;
    private int j;
    private final byte[] k = g;

    public b() {
        this.d = null;
        this.j = this.b ? 19 : -1;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x005e  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0109  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0174  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(byte[] r12, int r13) {
        /*
            r11 = this;
            byte[] r0 = r11.k
            byte[] r1 = r11.d
            r2 = 0
            int r3 = r11.j
            r4 = 0
            int r5 = r13 + 0
            r6 = -1
            int r7 = r11.i
            switch(r7) {
                case 0: goto L_0x00b3;
                case 1: goto L_0x00b8;
                case 2: goto L_0x00de;
                default: goto L_0x0010;
            }
        L_0x0010:
            r10 = r6
            r6 = r4
            r4 = r10
        L_0x0013:
            r7 = -1
            if (r4 == r7) goto L_0x0234
            r7 = 0
            int r2 = r2 + 1
            int r8 = r4 >> 18
            r8 = r8 & 63
            byte r8 = r0[r8]
            r1[r7] = r8
            r7 = 1
            int r2 = r2 + 1
            int r8 = r4 >> 12
            r8 = r8 & 63
            byte r8 = r0[r8]
            r1[r7] = r8
            r7 = 2
            int r2 = r2 + 1
            int r8 = r4 >> 6
            r8 = r8 & 63
            byte r8 = r0[r8]
            r1[r7] = r8
            r7 = 3
            int r2 = r2 + 1
            r4 = r4 & 63
            byte r4 = r0[r4]
            r1[r7] = r4
            int r3 = r3 + -1
            if (r3 != 0) goto L_0x0234
            boolean r3 = r11.c
            if (r3 == 0) goto L_0x004f
            r3 = 4
            int r2 = r2 + 1
            r4 = 13
            r1[r3] = r4
        L_0x004f:
            int r3 = r2 + 1
            r4 = 10
            r1[r2] = r4
            r2 = 19
            r4 = r3
            r3 = r2
            r2 = r6
        L_0x005a:
            int r6 = r2 + 3
            if (r6 > r5) goto L_0x0100
            byte r6 = r12[r2]
            r6 = r6 & 255(0xff, float:3.57E-43)
            int r6 = r6 << 16
            int r7 = r2 + 1
            byte r7 = r12[r7]
            r7 = r7 & 255(0xff, float:3.57E-43)
            int r7 = r7 << 8
            r6 = r6 | r7
            int r7 = r2 + 2
            byte r7 = r12[r7]
            r7 = r7 & 255(0xff, float:3.57E-43)
            r6 = r6 | r7
            int r7 = r6 >> 18
            r7 = r7 & 63
            byte r7 = r0[r7]
            r1[r4] = r7
            int r7 = r4 + 1
            int r8 = r6 >> 12
            r8 = r8 & 63
            byte r8 = r0[r8]
            r1[r7] = r8
            int r7 = r4 + 2
            int r8 = r6 >> 6
            r8 = r8 & 63
            byte r8 = r0[r8]
            r1[r7] = r8
            int r7 = r4 + 3
            r6 = r6 & 63
            byte r6 = r0[r6]
            r1[r7] = r6
            int r2 = r2 + 3
            int r4 = r4 + 4
            int r3 = r3 + -1
            if (r3 != 0) goto L_0x005a
            boolean r3 = r11.c
            if (r3 == 0) goto L_0x0231
            int r3 = r4 + 1
            r6 = 13
            r1[r4] = r6
        L_0x00aa:
            int r4 = r3 + 1
            r6 = 10
            r1[r3] = r6
            r3 = 19
            goto L_0x005a
        L_0x00b3:
            r10 = r6
            r6 = r4
            r4 = r10
            goto L_0x0013
        L_0x00b8:
            r7 = 2
            if (r7 > r5) goto L_0x0010
            byte[] r6 = r11.h
            r7 = 0
            byte r6 = r6[r7]
            r6 = r6 & 255(0xff, float:3.57E-43)
            int r6 = r6 << 16
            r7 = 0
            int r4 = r4 + 1
            byte r7 = r12[r7]
            r7 = r7 & 255(0xff, float:3.57E-43)
            int r7 = r7 << 8
            r6 = r6 | r7
            r7 = 1
            int r4 = r4 + 1
            byte r7 = r12[r7]
            r7 = r7 & 255(0xff, float:3.57E-43)
            r6 = r6 | r7
            r7 = 0
            r11.i = r7
            r10 = r6
            r6 = r4
            r4 = r10
            goto L_0x0013
        L_0x00de:
            if (r5 <= 0) goto L_0x0010
            byte[] r6 = r11.h
            r7 = 0
            byte r6 = r6[r7]
            r6 = r6 & 255(0xff, float:3.57E-43)
            int r6 = r6 << 16
            byte[] r7 = r11.h
            r8 = 1
            byte r7 = r7[r8]
            r7 = r7 & 255(0xff, float:3.57E-43)
            int r7 = r7 << 8
            r6 = r6 | r7
            r7 = 0
            int r4 = r4 + 1
            byte r7 = r12[r7]
            r7 = r7 & 255(0xff, float:3.57E-43)
            r6 = r6 | r7
            r7 = 0
            r11.i = r7
            goto L_0x0010
        L_0x0100:
            int r6 = r11.i
            int r6 = r2 - r6
            r7 = 1
            int r7 = r5 - r7
            if (r6 != r7) goto L_0x0174
            r6 = 0
            int r7 = r11.i
            if (r7 <= 0) goto L_0x016c
            byte[] r7 = r11.h
            r8 = 0
            int r6 = r6 + 1
            byte r7 = r7[r8]
            r10 = r7
            r7 = r6
            r6 = r2
            r2 = r10
        L_0x0119:
            r2 = r2 & 255(0xff, float:3.57E-43)
            int r2 = r2 << 4
            int r8 = r11.i
            int r7 = r8 - r7
            r11.i = r7
            int r7 = r4 + 1
            int r8 = r2 >> 6
            r8 = r8 & 63
            byte r8 = r0[r8]
            r1[r4] = r8
            int r4 = r7 + 1
            r2 = r2 & 63
            byte r0 = r0[r2]
            r1[r7] = r0
            boolean r0 = r11.a
            if (r0 == 0) goto L_0x022e
            int r0 = r4 + 1
            r2 = 61
            r1[r4] = r2
            int r2 = r0 + 1
            r4 = 61
            r1[r0] = r4
            r0 = r2
        L_0x0146:
            boolean r2 = r11.b
            if (r2 == 0) goto L_0x015c
            boolean r2 = r11.c
            if (r2 == 0) goto L_0x0155
            int r2 = r0 + 1
            r4 = 13
            r1[r0] = r4
            r0 = r2
        L_0x0155:
            int r2 = r0 + 1
            r4 = 10
            r1[r0] = r4
            r0 = r2
        L_0x015c:
            r1 = r0
            r0 = r6
        L_0x015e:
            boolean r2 = com.google.ads.util.b.l
            if (r2 != 0) goto L_0x0218
            int r2 = r11.i
            if (r2 == 0) goto L_0x0218
            java.lang.AssertionError r0 = new java.lang.AssertionError
            r0.<init>()
            throw r0
        L_0x016c:
            int r7 = r2 + 1
            byte r2 = r12[r2]
            r10 = r7
            r7 = r6
            r6 = r10
            goto L_0x0119
        L_0x0174:
            int r6 = r11.i
            int r6 = r2 - r6
            r7 = 2
            int r7 = r5 - r7
            if (r6 != r7) goto L_0x01fa
            r6 = 0
            int r7 = r11.i
            r8 = 1
            if (r7 <= r8) goto L_0x01ea
            byte[] r7 = r11.h
            r8 = 0
            int r6 = r6 + 1
            byte r7 = r7[r8]
            r10 = r7
            r7 = r6
            r6 = r2
            r2 = r10
        L_0x018e:
            r2 = r2 & 255(0xff, float:3.57E-43)
            int r2 = r2 << 10
            int r8 = r11.i
            if (r8 <= 0) goto L_0x01f2
            byte[] r8 = r11.h
            int r9 = r7 + 1
            byte r7 = r8[r7]
            r8 = r9
            r10 = r6
            r6 = r7
            r7 = r10
        L_0x01a0:
            r6 = r6 & 255(0xff, float:3.57E-43)
            int r6 = r6 << 2
            r2 = r2 | r6
            int r6 = r11.i
            int r6 = r6 - r8
            r11.i = r6
            int r6 = r4 + 1
            int r8 = r2 >> 12
            r8 = r8 & 63
            byte r8 = r0[r8]
            r1[r4] = r8
            int r4 = r6 + 1
            int r8 = r2 >> 6
            r8 = r8 & 63
            byte r8 = r0[r8]
            r1[r6] = r8
            int r6 = r4 + 1
            r2 = r2 & 63
            byte r0 = r0[r2]
            r1[r4] = r0
            boolean r0 = r11.a
            if (r0 == 0) goto L_0x022c
            int r0 = r6 + 1
            r2 = 61
            r1[r6] = r2
        L_0x01d0:
            boolean r2 = r11.b
            if (r2 == 0) goto L_0x01e6
            boolean r2 = r11.c
            if (r2 == 0) goto L_0x01df
            int r2 = r0 + 1
            r4 = 13
            r1[r0] = r4
            r0 = r2
        L_0x01df:
            int r2 = r0 + 1
            r4 = 10
            r1[r0] = r4
            r0 = r2
        L_0x01e6:
            r1 = r0
            r0 = r7
            goto L_0x015e
        L_0x01ea:
            int r7 = r2 + 1
            byte r2 = r12[r2]
            r10 = r7
            r7 = r6
            r6 = r10
            goto L_0x018e
        L_0x01f2:
            int r8 = r6 + 1
            byte r6 = r12[r6]
            r10 = r8
            r8 = r7
            r7 = r10
            goto L_0x01a0
        L_0x01fa:
            boolean r0 = r11.b
            if (r0 == 0) goto L_0x0214
            if (r4 <= 0) goto L_0x0214
            r0 = 19
            if (r3 == r0) goto L_0x0214
            boolean r0 = r11.c
            if (r0 == 0) goto L_0x022a
            int r0 = r4 + 1
            r6 = 13
            r1[r4] = r6
        L_0x020e:
            int r4 = r0 + 1
            r6 = 10
            r1[r0] = r6
        L_0x0214:
            r0 = r2
            r1 = r4
            goto L_0x015e
        L_0x0218:
            boolean r2 = com.google.ads.util.b.l
            if (r2 != 0) goto L_0x0224
            if (r0 == r5) goto L_0x0224
            java.lang.AssertionError r0 = new java.lang.AssertionError
            r0.<init>()
            throw r0
        L_0x0224:
            r11.e = r1
            r11.j = r3
            r0 = 1
            return r0
        L_0x022a:
            r0 = r4
            goto L_0x020e
        L_0x022c:
            r0 = r6
            goto L_0x01d0
        L_0x022e:
            r0 = r4
            goto L_0x0146
        L_0x0231:
            r3 = r4
            goto L_0x00aa
        L_0x0234:
            r4 = r2
            r2 = r6
            goto L_0x005a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.ads.util.b.a(byte[], int):boolean");
    }
}
