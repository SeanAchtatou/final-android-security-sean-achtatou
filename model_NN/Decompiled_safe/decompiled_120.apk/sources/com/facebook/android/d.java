package com.facebook.android;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.facebook.android.b;

public class d extends Dialog {
    static final float[] a = {460.0f, 260.0f};
    static final float[] b = {280.0f, 420.0f};
    static final FrameLayout.LayoutParams c = new FrameLayout.LayoutParams(-1, -1);
    private String d;
    /* access modifiers changed from: private */
    public b.a e;
    /* access modifiers changed from: private */
    public ProgressDialog f;
    /* access modifiers changed from: private */
    public WebView g;
    private LinearLayout h;
    /* access modifiers changed from: private */
    public TextView i;

    class a extends WebViewClient {
        private a() {
        }

        public void onPageFinished(WebView webView, String str) {
            super.onPageFinished(webView, str);
            String title = d.this.g.getTitle();
            if (title != null && title.length() > 0) {
                d.this.i.setText(title);
            }
            d.this.f.hide();
        }

        public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
            Log.d("Facebook-WebView", "Webview loading URL: " + str);
            super.onPageStarted(webView, str, bitmap);
            d.this.f.show();
        }

        public void onReceivedError(WebView webView, int i, String str, String str2) {
            super.onReceivedError(webView, i, str, str2);
            d.this.e.a(new a(str, i, str2));
            d.this.dismiss();
        }

        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            Log.d("Facebook-WebView", "Redirect URL: " + str);
            if (str.startsWith("fbconnect://success")) {
                Bundle b = e.b(str);
                String string = b.getString("error");
                if (string == null) {
                    string = b.getString("error_type");
                }
                if (string == null) {
                    d.this.e.a(b);
                } else if (string.equals("access_denied") || string.equals("OAuthAccessDeniedException")) {
                    d.this.e.a();
                } else {
                    d.this.e.a(new c(string));
                }
                d.this.dismiss();
                return true;
            } else if (str.startsWith("fbconnect://cancel")) {
                d.this.e.a();
                d.this.dismiss();
                return true;
            } else if (str.contains("touch")) {
                return false;
            } else {
                d.this.getContext().startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
                return true;
            }
        }
    }

    public d(Context context, String str, b.a aVar) {
        super(context);
        this.d = str;
        this.e = aVar;
    }

    private void a() {
        requestWindowFeature(1);
        Drawable b2 = b();
        this.i = new TextView(getContext());
        this.i.setText("Facebook");
        this.i.setTextColor(-1);
        this.i.setTypeface(Typeface.DEFAULT_BOLD);
        this.i.setBackgroundColor(-9599820);
        this.i.setPadding(6, 4, 4, 4);
        this.i.setCompoundDrawablePadding(6);
        this.i.setCompoundDrawablesWithIntrinsicBounds(b2, (Drawable) null, (Drawable) null, (Drawable) null);
        this.h.addView(this.i);
    }

    private Drawable b() {
        return Drawable.createFromStream(getClass().getClassLoader().getResourceAsStream("com/facebook/android/facebook_icon.png"), "com/facebook/android/facebook_icon.png");
    }

    private void c() {
        this.g = new WebView(getContext());
        this.g.setVerticalScrollBarEnabled(false);
        this.g.setHorizontalScrollBarEnabled(false);
        this.g.setWebViewClient(new a());
        this.g.getSettings().setJavaScriptEnabled(true);
        this.g.loadUrl(this.d);
        this.g.setLayoutParams(c);
        this.h.addView(this.g);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f = new ProgressDialog(getContext());
        this.f.requestWindowFeature(1);
        this.f.setMessage("Loading...");
        this.h = new LinearLayout(getContext());
        this.h.setOrientation(1);
        a();
        c();
        Display defaultDisplay = getWindow().getWindowManager().getDefaultDisplay();
        float f2 = getContext().getResources().getDisplayMetrics().density;
        float[] fArr = defaultDisplay.getWidth() < defaultDisplay.getHeight() ? b : a;
        addContentView(this.h, new FrameLayout.LayoutParams((int) ((fArr[0] * f2) + 0.5f), (int) ((fArr[1] * f2) + 0.5f)));
    }
}
