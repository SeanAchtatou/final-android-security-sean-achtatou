package com.eddiehsu.mathgame.library;

import org.anddev.andengine.entity.Entity;
import org.anddev.andengine.entity.text.Text;
import org.anddev.andengine.opengl.font.Font;

public final class n extends Entity {
    public n(Font font) {
        Text text = new Text(138.0f, 110.0f, font, "Toddler");
        Text text2 = new Text(119.0f, 240.0f, font, "1st Grade");
        Text text3 = new Text(110.0f, 370.0f, font, "2nd Grade");
        Text text4 = new Text(110.0f, 500.0f, font, "3rd Grade");
        Text text5 = new Text(108.0f, 630.0f, font, "4th Grade");
        text.setColor(1.0f, 1.0f, 0.66f);
        text2.setColor(1.0f, 1.0f, 0.66f);
        text3.setColor(1.0f, 1.0f, 0.66f);
        text4.setColor(1.0f, 1.0f, 0.66f);
        text5.setColor(1.0f, 1.0f, 0.66f);
        attachChild(text);
        attachChild(text2);
        attachChild(text3);
        attachChild(text4);
        attachChild(text5);
    }
}
