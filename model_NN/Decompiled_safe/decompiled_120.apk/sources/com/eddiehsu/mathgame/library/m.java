package com.eddiehsu.mathgame.library;

public final class m {
    private static final m a = new m();
    private e b = null;
    private e c = null;
    private e d = null;
    private boolean e = false;
    private boolean f = false;
    private boolean g = false;

    private m() {
    }

    public final void a() {
        this.d = null;
        this.c = null;
        this.b = null;
        this.g = false;
        this.f = false;
        this.e = false;
    }

    public final int b() {
        return this.b.c();
    }

    public final int c() {
        return this.b.d();
    }

    public final int d() {
        return this.c.c();
    }

    public final int e() {
        return this.c.d();
    }

    public final int f() {
        return this.d.c();
    }

    public final int g() {
        return this.d.d();
    }

    public final void h() {
        this.e = false;
    }

    public final void i() {
        this.f = false;
    }

    public final void j() {
        this.g = false;
    }

    public final boolean k() {
        return this.e;
    }

    public final boolean l() {
        return this.f;
    }

    public final boolean m() {
        return this.g;
    }

    public final int a(e eVar) {
        if (!this.e && eVar.b() <= 81) {
            this.b = eVar;
            this.e = true;
            return 3;
        } else if (!this.e || this.f) {
            if (!this.e || !this.f || this.g) {
                if (!this.e || !this.f || !this.g) {
                    return 0;
                }
                boolean z = false;
                if (!eVar.equals(this.b) && !eVar.equals(this.c) && !eVar.equals(this.d) && eVar.b() <= 81) {
                    if (this.c.b() == 96) {
                        if (this.b.b() + this.d.b() == eVar.b()) {
                            z = true;
                        }
                    } else if (this.c.b() == 97) {
                        if (this.b.b() - this.d.b() == eVar.b()) {
                            z = true;
                        }
                    } else if (this.c.b() == 98) {
                        if (this.b.b() * this.d.b() == eVar.b()) {
                            z = true;
                        }
                    } else if (this.c.b() == 99 && this.b.b() / this.d.b() == eVar.b() && this.b.b() % this.d.b() == 0) {
                        z = true;
                    }
                }
                if (z) {
                    return 2;
                }
                if (eVar.equals(this.b) || eVar.equals(this.c) || eVar.equals(this.d) || eVar.b() == 94) {
                    return 4;
                }
                return 0;
            } else if (!eVar.equals(this.b) && !eVar.equals(this.c) && eVar.b() <= 81) {
                this.d = eVar;
                this.g = true;
                return 3;
            } else if (eVar.equals(this.b) || eVar.equals(this.c) || eVar.b() == 94) {
                return 4;
            } else {
                return 0;
            }
        } else if (eVar.b() > 81 && eVar.b() != 94) {
            this.c = eVar;
            this.f = true;
            return 3;
        } else if (eVar.equals(this.b) || eVar.b() == 94) {
            return 4;
        } else {
            return 0;
        }
    }

    public static m n() {
        return a;
    }
}
