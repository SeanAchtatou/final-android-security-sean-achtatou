package com.eddiehsu.mathgame.library;

import android.graphics.Color;

public final class c {
    public static final int a = Color.rgb(255, 255, 255);
    public static final int b = Color.rgb(255, 225, 10);
    public static final int[] c = {-65536, Color.rgb(255, 165, 0), Color.rgb(255, 255, 0), Color.rgb(0, 128, 0), -16776961, Color.rgb(75, 0, 130), Color.rgb(238, 130, 238)};

    private c() {
    }
}
