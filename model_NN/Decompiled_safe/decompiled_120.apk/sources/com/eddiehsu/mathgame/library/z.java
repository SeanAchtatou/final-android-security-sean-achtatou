package com.eddiehsu.mathgame.library;

public final class z {
    private static final z a = new z();
    private int b = 0;
    private int c = 0;
    private int d = 0;
    private int e = 0;
    private int f = 0;
    private int g = 0;
    private int h = 0;
    private int i = 0;
    private boolean j = false;

    private z() {
    }

    public final int a() {
        return this.b;
    }

    public final int b() {
        return this.c;
    }

    public final int c() {
        return this.d;
    }

    public final int d() {
        return this.e;
    }

    public final int e() {
        return this.f;
    }

    public final int f() {
        return this.g;
    }

    public final int g() {
        return this.h;
    }

    public final int h() {
        return this.i;
    }

    public final void i() {
        this.j = false;
    }

    public final boolean a(e[][] eVarArr) {
        if (!this.j) {
            boolean z = false;
            int i2 = 0;
            boolean z2 = false;
            int i3 = 0;
            boolean z3 = false;
            int i4 = 0;
            int i5 = 0;
            boolean z4 = false;
            int i6 = 0;
            int i7 = 0;
            int i8 = 0;
            int i9 = 0;
            int i10 = 0;
            while (i5 < 5) {
                boolean z5 = z4;
                boolean z6 = z;
                boolean z7 = z2;
                boolean z8 = z3;
                for (int i11 = 0; i11 < 5; i11++) {
                    int b2 = eVarArr[i5][i11].b();
                    if (b2 == 96) {
                        i7 = i5;
                        z5 = true;
                        i10 = i11;
                    } else if (b2 == 97) {
                        i9 = i5;
                        z6 = true;
                        i8 = i11;
                    } else if (b2 == 98) {
                        i4 = i5;
                        z7 = true;
                        i3 = i11;
                    } else if (b2 == 99) {
                        i2 = i5;
                        z8 = true;
                        i6 = i11;
                    }
                }
                i5++;
                z3 = z8;
                z2 = z7;
                z = z6;
                z4 = z5;
            }
            if (z4 || z || z2 || z3) {
                this.b = 0;
                while (this.b < 5) {
                    this.c = 0;
                    while (this.c < 5) {
                        int b3 = eVarArr[this.b][this.c].b();
                        if (b3 <= 81) {
                            this.f = 0;
                            while (this.f < 5) {
                                this.g = 0;
                                while (this.g < 5) {
                                    int b4 = eVarArr[this.f][this.g].b();
                                    if (b4 <= 81 && !(this.f == this.b && this.g == this.c)) {
                                        this.h = 0;
                                        while (this.h < 5) {
                                            this.i = 0;
                                            while (this.i < 5) {
                                                int b5 = eVarArr[this.h][this.i].b();
                                                if (b5 <= 81 && !((this.h == this.b && this.i == this.c) || (this.h == this.f && this.i == this.g))) {
                                                    if (z4 && b3 + b4 == b5) {
                                                        this.d = i7;
                                                        this.e = i10;
                                                        this.j = true;
                                                    }
                                                    if (z && b3 - b4 == b5) {
                                                        this.d = i9;
                                                        this.e = i8;
                                                        this.j = true;
                                                    }
                                                    if (z2 && b3 * b4 == b5) {
                                                        this.d = i4;
                                                        this.e = i3;
                                                        this.j = true;
                                                    }
                                                    if (z3 && b3 / b4 == b5 && b3 % b4 == 0) {
                                                        this.d = i2;
                                                        this.e = i6;
                                                        this.j = true;
                                                    }
                                                }
                                                if (this.j) {
                                                    break;
                                                }
                                                this.i = this.i + 1;
                                            }
                                            if (this.j) {
                                                break;
                                            }
                                            this.h = this.h + 1;
                                        }
                                    }
                                    if (this.j) {
                                        break;
                                    }
                                    this.g = this.g + 1;
                                }
                                if (this.j) {
                                    break;
                                }
                                this.f = this.f + 1;
                            }
                        }
                        if (this.j) {
                            break;
                        }
                        this.c = this.c + 1;
                    }
                    if (this.j) {
                        break;
                    }
                    this.b = this.b + 1;
                }
            }
        }
        if (this.b >= 5) {
            this.b = 0;
        }
        if (this.d >= 5) {
            this.d = 0;
        }
        if (this.f >= 5) {
            this.f = 0;
        }
        if (this.h >= 5) {
            this.h = 0;
        }
        if (this.c >= 5) {
            this.c = 0;
        }
        if (this.e >= 5) {
            this.e = 0;
        }
        if (this.g >= 5) {
            this.g = 0;
        }
        if (this.i >= 5) {
            this.i = 0;
        }
        return this.j;
    }

    public final void b(e[][] eVarArr) {
        if (this.j) {
            eVarArr[this.b][this.c].g();
            eVarArr[this.d][this.e].h();
            eVarArr[this.f][this.g].g();
            eVarArr[this.h][this.i].h();
        }
    }

    public static z j() {
        return a;
    }
}
