package com.eddiehsu.mathgame.library;

import android.util.Log;
import com.google.ads.AdView;
import com.google.ads.b;
import com.google.ads.e;
import com.google.ads.f;
import com.google.ads.g;

final class k implements b {
    private /* synthetic */ w a;
    private final /* synthetic */ AdView b;

    k(w wVar, AdView adView) {
        this.a = wVar;
        this.b = adView;
    }

    public final void a(e eVar, f fVar) {
        Log.i("Ads Failed", String.valueOf(eVar.toString()) + fVar.toString());
        if (this.a.a < 5 && fVar.equals(f.NO_FILL)) {
            this.a.a++;
            this.b.a(new g());
        }
    }

    public final void a() {
        this.b.setVisibility(0);
        this.b.setEnabled(true);
    }
}
