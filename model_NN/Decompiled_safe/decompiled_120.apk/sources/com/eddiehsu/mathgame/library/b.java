package com.eddiehsu.mathgame.library;

import android.content.Intent;
import android.net.Uri;
import android.view.View;

final class b implements View.OnClickListener {
    private /* synthetic */ Instruction a;

    b(Instruction instruction) {
        this.a = instruction;
    }

    public final void onClick(View view) {
        this.a.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://www.youtube.com/watch?v=6dkLwpW8Rgc")));
    }
}
