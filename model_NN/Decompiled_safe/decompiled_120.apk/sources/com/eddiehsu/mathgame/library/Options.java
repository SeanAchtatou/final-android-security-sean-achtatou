package com.eddiehsu.mathgame.library;

import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceGroup;
import android.util.Log;
import android.view.KeyEvent;
import com.scoreloop.client.android.ui.f;
import org.anddev.andengine.R;

public class Options extends PreferenceActivity {
    private AudioManager a;
    private int b = 0;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        addPreferencesFromResource(R.layout.options);
        this.a = (AudioManager) getSystemService("audio");
        Intent intent = getIntent();
        if (intent.hasExtra("game_count")) {
            this.b = intent.getIntExtra("game_count", 0);
        }
        if (this.b < 3) {
            ((PreferenceGroup) findPreference("category_game_options")).removePreference(findPreference("rate"));
        }
    }

    public void onResume() {
        try {
            f.a(this, "oRYOJDGwf6d0oh+Bm7vpOkSsL8axB64yud2G1/YWpVvUuc8vDsu4CQ==");
        } catch (IllegalStateException e) {
            Log.v("Init scoreloop", e.getMessage());
        }
        super.onResume();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 24) {
            this.a.adjustStreamVolume(3, 1, 1);
        } else if (i == 25) {
            this.a.adjustStreamVolume(3, -1, 1);
        } else if (i == 82) {
            finish();
        } else {
            super.onKeyDown(i, keyEvent);
        }
        return true;
    }
}
