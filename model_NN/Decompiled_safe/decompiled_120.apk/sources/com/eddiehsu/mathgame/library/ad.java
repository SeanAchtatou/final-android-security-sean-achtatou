package com.eddiehsu.mathgame.library;

import org.anddev.andengine.entity.modifier.IEntityModifier;
import org.anddev.andengine.util.modifier.IModifier;

final class ad implements IEntityModifier.IEntityModifierListener {
    private /* synthetic */ MathGame a;

    ad(MathGame mathGame) {
        this.a = mathGame;
    }

    public final /* bridge */ /* synthetic */ void onModifierFinished(IModifier iModifier, Object obj) {
        this.a.E.reset();
        this.a.B.registerEntityModifier(this.a.E);
        this.a.a(this.a.ao);
    }

    public final /* bridge */ /* synthetic */ void onModifierStarted(IModifier iModifier, Object obj) {
    }
}
