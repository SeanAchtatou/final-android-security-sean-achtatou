package com.eddiehsu.mathgame.library;

import org.anddev.andengine.entity.sprite.Sprite;

final class q implements Runnable {
    private /* synthetic */ MathGame a;

    q(MathGame mathGame) {
        this.a = mathGame;
    }

    public final void run() {
        this.a.ag.detachChild(this.a.f);
        this.a.ag.attachChild(new Sprite(0.0f, 0.0f, this.a.v));
        this.a.ag.setOnSceneTouchListener(this.a);
        this.a.Z = 1;
        this.a.a(this.a.aq);
        if (this.a.g.booleanValue()) {
            this.a.au.a();
        }
    }
}
