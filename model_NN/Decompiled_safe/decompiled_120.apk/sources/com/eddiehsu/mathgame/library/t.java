package com.eddiehsu.mathgame.library;

import android.content.Intent;
import android.util.Log;
import com.scoreloop.client.android.ui.PostScoreOverlayActivity;
import org.anddev.andengine.entity.modifier.IEntityModifier;
import org.anddev.andengine.util.modifier.IModifier;

final class t implements IEntityModifier.IEntityModifierListener {
    final /* synthetic */ MathGame a;

    t(MathGame mathGame) {
        this.a = mathGame;
    }

    public final /* bridge */ /* synthetic */ void onModifierFinished(IModifier iModifier, Object obj) {
        if (this.a.ab) {
            this.a.runOnUiThread(new f(this));
            return;
        }
        try {
            this.a.startActivity(new Intent(this.a, PostScoreOverlayActivity.class));
        } catch (IllegalStateException e) {
            Log.v("share score", e.getMessage());
        }
    }

    public final /* bridge */ /* synthetic */ void onModifierStarted(IModifier iModifier, Object obj) {
    }
}
