package com.eddiehsu.mathgame.library;

import android.app.Activity;
import android.media.AudioManager;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.Button;
import android.widget.ImageView;
import org.anddev.andengine.R;

public class Instruction extends Activity {
    private AudioManager a;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.instruction);
        this.a = (AudioManager) getSystemService("audio");
        ((ImageView) findViewById(R.id.youtube_button)).setOnClickListener(new b(this));
        ((Button) findViewById(R.id.close_button)).setOnClickListener(new a(this));
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 24) {
            this.a.adjustStreamVolume(3, 1, 1);
        } else if (i == 25) {
            this.a.adjustStreamVolume(3, -1, 1);
        } else if (i == 82) {
            finish();
        } else {
            super.onKeyDown(i, keyEvent);
        }
        return true;
    }
}
