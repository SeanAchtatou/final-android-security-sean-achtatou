package com.eddiehsu.mathgame.library;

import org.anddev.andengine.entity.modifier.IEntityModifier;
import org.anddev.andengine.util.modifier.IModifier;

final class g implements IEntityModifier.IEntityModifierListener {
    private /* synthetic */ e a;

    g(e eVar) {
        this.a = eVar;
    }

    public final /* bridge */ /* synthetic */ void onModifierFinished(IModifier iModifier, Object obj) {
        this.a.setAlpha(1.0f);
    }

    public final /* bridge */ /* synthetic */ void onModifierStarted(IModifier iModifier, Object obj) {
    }
}
