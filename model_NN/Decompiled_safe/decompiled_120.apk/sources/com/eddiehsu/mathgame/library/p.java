package com.eddiehsu.mathgame.library;

import org.anddev.andengine.engine.handler.IUpdateHandler;
import org.anddev.andengine.entity.sprite.Sprite;

final class p implements IUpdateHandler {
    private /* synthetic */ MathGame a;

    p(MathGame mathGame) {
        this.a = mathGame;
    }

    public final void onUpdate(float f) {
        if (this.a.d.isLoadedToHardware()) {
            this.a.ag.unregisterUpdateHandler(this);
            this.a.f = new Sprite((float) (240 - (this.a.e.getWidth() / 2)), (float) (400 - (this.a.e.getHeight() / 2)), this.a.e);
            this.a.ag.attachChild(this.a.f);
            this.a.a();
        }
    }

    public final void reset() {
    }
}
