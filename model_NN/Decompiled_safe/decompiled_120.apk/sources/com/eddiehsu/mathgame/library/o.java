package com.eddiehsu.mathgame.library;

import org.anddev.andengine.engine.handler.timer.ITimerCallback;
import org.anddev.andengine.engine.handler.timer.TimerHandler;

final class o implements ITimerCallback {
    private /* synthetic */ MathGame a;

    o(MathGame mathGame) {
        this.a = mathGame;
    }

    public final void onTimePassed(TimerHandler timerHandler) {
        if (this.a.T && this.a.aa == 1) {
            this.a.V = this.a.S - (this.a.mEngine.getSecondsElapsedTotal() - this.a.U);
            if (this.a.V < 0.0f) {
                this.a.c();
            } else if (this.a.V < 60.0f) {
                if (this.a.V >= 10.0f) {
                    this.a.y.setText("0:" + Integer.toString((int) Math.floor((double) this.a.V)));
                } else {
                    this.a.y.setText("0:0" + Integer.toString((int) Math.floor((double) this.a.V)));
                }
                this.a.W.setPosition(-20.0f - (((this.a.S - this.a.V) / this.a.S) * 480.0f), 133.0f);
            }
        }
        if (this.a.M < this.a.L) {
            if (this.a.L - this.a.M > 3) {
                MathGame mathGame = this.a;
                mathGame.M = mathGame.M + ((this.a.L - this.a.M) / 3);
            } else {
                MathGame mathGame2 = this.a;
                mathGame2.M = mathGame2.M + 1;
            }
            this.a.w.setText("Score:  " + Long.toString(this.a.M));
        }
    }
}
