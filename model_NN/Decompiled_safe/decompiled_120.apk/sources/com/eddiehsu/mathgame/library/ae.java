package com.eddiehsu.mathgame.library;

import org.anddev.andengine.engine.handler.timer.TimerHandler;

final class ae implements Runnable {
    final /* synthetic */ MathGame a;

    ae(MathGame mathGame) {
        this.a = mathGame;
    }

    public final void run() {
        if (this.a.Z == 6) {
            this.a.af.getChild(3).detachChild(this.a.aj);
        } else if (this.a.g.booleanValue()) {
            if (this.a.av) {
                this.a.av = false;
                this.a.ag.registerUpdateHandler(new TimerHandler(1.0f, new d(this)));
            } else {
                this.a.au.a();
            }
        }
        this.a.Z = 1;
        if (this.a.aa == 1) {
            this.a.b(this.a.ar);
        } else {
            this.a.b(this.a.as);
        }
        this.a.a(this.a.aq);
        this.a.mEngine.setScene(this.a.ag);
    }
}
