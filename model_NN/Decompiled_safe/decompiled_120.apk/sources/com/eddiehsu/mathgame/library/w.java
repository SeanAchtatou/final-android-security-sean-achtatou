package com.eddiehsu.mathgame.library;

import android.app.Activity;
import android.os.Handler;
import com.google.ads.AdView;
import com.google.ads.g;
import org.anddev.andengine.R;

public final class w {
    public int a;
    final Handler b = new Handler();
    final Runnable c = new j(this);
    private Activity d;
    private int e;
    private Runnable f = new i(this);

    public w(Activity activity) {
        this.d = activity;
        this.e = R.id.game_ad;
        this.a = 0;
    }

    static /* synthetic */ void b(w wVar) {
        wVar.a = 0;
        AdView adView = (AdView) wVar.d.findViewById(wVar.e);
        adView.setVerticalGravity(80);
        adView.a(new k(wVar, adView));
        adView.a(new g());
    }

    static /* synthetic */ void a(w wVar) {
        AdView adView = (AdView) wVar.d.findViewById(wVar.e);
        adView.setVisibility(4);
        adView.setEnabled(false);
    }

    public final void a() {
        this.b.post(this.f);
    }
}
