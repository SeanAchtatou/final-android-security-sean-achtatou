package com.eddiehsu.mathgame.library;

import android.app.AlertDialog;

final class f implements Runnable {
    private /* synthetic */ t a;

    f(t tVar) {
        this.a = tVar;
    }

    public final void run() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.a.a);
        builder.setMessage("Sharing score only works with global scores, please turn off local scores under:\nMain Menu -> Options").setNeutralButton("Cancel", new y(this));
        builder.create().show();
    }
}
