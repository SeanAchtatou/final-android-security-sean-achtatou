package com.eddiehsu.mathgame.library;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import com.scoreloop.client.android.ui.LeaderboardsScreenActivity;
import com.scoreloop.client.android.ui.f;
import java.io.IOException;
import java.lang.reflect.Array;
import org.anddev.andengine.R;
import org.anddev.andengine.audio.music.Music;
import org.anddev.andengine.audio.music.MusicFactory;
import org.anddev.andengine.audio.sound.Sound;
import org.anddev.andengine.audio.sound.SoundFactory;
import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.engine.handler.timer.TimerHandler;
import org.anddev.andengine.engine.options.EngineOptions;
import org.anddev.andengine.engine.options.WakeLockOptions;
import org.anddev.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.anddev.andengine.entity.Entity;
import org.anddev.andengine.entity.layer.tiled.tmx.util.constants.TMXConstants;
import org.anddev.andengine.entity.modifier.AlphaModifier;
import org.anddev.andengine.entity.modifier.DelayModifier;
import org.anddev.andengine.entity.modifier.MoveYModifier;
import org.anddev.andengine.entity.modifier.ParallelEntityModifier;
import org.anddev.andengine.entity.modifier.ScaleModifier;
import org.anddev.andengine.entity.modifier.SequenceEntityModifier;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.scene.background.ColorBackground;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.entity.text.ChangeableText;
import org.anddev.andengine.entity.text.Text;
import org.anddev.andengine.entity.util.FPSLogger;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.opengl.font.Font;
import org.anddev.andengine.opengl.font.FontFactory;
import org.anddev.andengine.opengl.font.StrokeFont;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.anddev.andengine.opengl.texture.bitmap.BitmapTexture;
import org.anddev.andengine.opengl.texture.compressed.pvr.PVRTexture;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.texture.region.TiledTextureRegion;
import org.anddev.andengine.ui.activity.LayoutGameActivity;
import org.anddev.andengine.util.Debug;
import org.anddev.andengine.util.HorizontalAlign;
import org.anddev.andengine.util.MathUtils;
import org.anddev.andengine.util.modifier.ease.EaseBackOut;
import org.anddev.andengine.util.modifier.ease.EaseStrongIn;

public class MathGame extends LayoutGameActivity implements Scene.IOnSceneTouchListener {
    private Text A;
    /* access modifiers changed from: private */
    public Text B;
    /* access modifiers changed from: private */
    public ChangeableText C;
    private SequenceEntityModifier D;
    /* access modifiers changed from: private */
    public SequenceEntityModifier E;
    /* access modifiers changed from: private */
    public SequenceEntityModifier F;
    private AlphaModifier G;
    private Sprite H;
    private Sprite I;
    private e[][] J = ((e[][]) Array.newInstance(e.class, 5, 5));
    private e[] K = new e[5];
    /* access modifiers changed from: private */
    public long L = 0;
    /* access modifiers changed from: private */
    public long M = 0;
    /* access modifiers changed from: private */
    public int N = 1;
    /* access modifiers changed from: private */
    public int O = 1500;
    private int P = 0;
    private int Q = 2;
    private int R = 2;
    /* access modifiers changed from: private */
    public float S = 60.0f;
    /* access modifiers changed from: private */
    public boolean T = false;
    /* access modifiers changed from: private */
    public float U = 0.0f;
    /* access modifiers changed from: private */
    public float V = 60.0f;
    /* access modifiers changed from: private */
    public Sprite W;
    private boolean X = false;
    private boolean Y = false;
    /* access modifiers changed from: private */
    public int Z = 0;
    Double a = Double.valueOf(3.5d);
    /* access modifiers changed from: private */
    public int aa = 0;
    /* access modifiers changed from: private */
    public boolean ab = false;
    private boolean ac = true;
    private boolean ad = true;
    private int ae = 0;
    /* access modifiers changed from: private */
    public Scene af;
    /* access modifiers changed from: private */
    public Scene ag;
    private Scene ah;
    private n ai;
    /* access modifiers changed from: private */
    public h aj;
    private Sound ak;
    private Sound al;
    private Sound am;
    private Sound an;
    /* access modifiers changed from: private */
    public Sound ao;
    /* access modifiers changed from: private */
    public Sound ap;
    /* access modifiers changed from: private */
    public Music aq;
    /* access modifiers changed from: private */
    public Music ar;
    /* access modifiers changed from: private */
    public Music as;
    private AudioManager at;
    /* access modifiers changed from: private */
    public w au;
    /* access modifiers changed from: private */
    public boolean av = false;
    protected BitmapTextureAtlas[] b;
    protected int c;
    protected BitmapTextureAtlas d;
    protected TextureRegion e;
    protected Sprite f;
    /* access modifiers changed from: private */
    public Boolean g = true;
    private Camera h;
    private BitmapTextureAtlas i;
    private Font j;
    private BitmapTextureAtlas k;
    private Font l;
    private BitmapTextureAtlas m;
    private Font n;
    private BitmapTextureAtlas o;
    private TiledTextureRegion p;
    private TextureRegion q;
    private TextureRegion r;
    private BitmapTextureAtlas s;
    private TextureRegion t;
    private BitmapTextureAtlas u;
    /* access modifiers changed from: private */
    public TextureRegion v;
    /* access modifiers changed from: private */
    public ChangeableText w;
    private ChangeableText x;
    /* access modifiers changed from: private */
    public ChangeableText y;
    /* access modifiers changed from: private */
    public ChangeableText z;

    /* access modifiers changed from: protected */
    public int getLayoutID() {
        return R.layout.main;
    }

    /* access modifiers changed from: protected */
    public int getRenderSurfaceViewID() {
        return R.id.xmllayoutexample_rendersurfaceview;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.at = (AudioManager) getSystemService("audio");
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        this.ab = defaultSharedPreferences.getBoolean("local_score_checkbox_preference", false);
        this.ac = defaultSharedPreferences.getBoolean("music_checkbox_preference", true);
        this.ad = defaultSharedPreferences.getBoolean("sound_checkbox_preference", true);
        this.ae = defaultSharedPreferences.getInt("counter", 0);
        SharedPreferences.Editor edit = defaultSharedPreferences.edit();
        int i2 = this.ae + 1;
        this.ae = i2;
        edit.putInt("counter", i2);
        edit.commit();
        Log.v("Game count", Integer.toString(this.ae));
        this.au = new w(this);
    }

    public Engine onLoadEngine() {
        this.h = new Camera(0.0f, 0.0f, 480.0f, 800.0f);
        return new Engine(new EngineOptions(true, EngineOptions.ScreenOrientation.PORTRAIT, new RatioResolutionPolicy(480.0f, 800.0f), this.h).setWakeLockOptions(WakeLockOptions.SCREEN_ON).setNeedsMusic(true).setNeedsSound(true));
    }

    public void onLoadResources() {
        FontFactory.setAssetBasePath("font/");
        this.i = new BitmapTextureAtlas((int) PVRTexture.FLAG_MIPMAP, (int) PVRTexture.FLAG_MIPMAP, TextureOptions.BILINEAR);
        this.j = new Font(this.i, Typeface.create(Typeface.DEFAULT, 0), 32.0f, true, -1);
        this.k = new BitmapTextureAtlas((int) PVRTexture.FLAG_TWIDDLE, (int) PVRTexture.FLAG_TWIDDLE, TextureOptions.BILINEAR);
        this.l = new StrokeFont(this.k, Typeface.create(Typeface.DEFAULT, 1), 80.0f, true, -1, 4.0f, -1, true);
        this.m = new BitmapTextureAtlas((int) PVRTexture.FLAG_MIPMAP, (int) PVRTexture.FLAG_MIPMAP, TextureOptions.BILINEAR);
        this.n = FontFactory.createFromAsset(this.m, this, "EraserRegular.ttf", 46.0f, true, -1);
        this.mEngine.getFontManager().loadFonts(this.j, this.l, this.n);
        BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");
        this.o = new BitmapTextureAtlas((int) PVRTexture.FLAG_MIPMAP, (int) PVRTexture.FLAG_MIPMAP, TextureOptions.BILINEAR);
        this.p = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.o, this, "munkie.png", 0, 0, 3, 1);
        this.q = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.o, this, "hint_button.png", 0, 128);
        this.r = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.o, this, "unselect_button.png", 0, 192);
        this.s = new BitmapTextureAtlas((int) PVRTexture.FLAG_TWIDDLE, 32, TextureOptions.BILINEAR);
        this.t = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.s, this, "timer_bar.png", 0, 0);
        this.u = new BitmapTextureAtlas((int) PVRTexture.FLAG_TWIDDLE, (int) PVRTexture.FLAG_BUMPMAP, BitmapTexture.BitmapTextureFormat.RGB_565, TextureOptions.BILINEAR);
        this.v = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.u, this, "main_screen.png", 0, 0);
        this.b = new BitmapTextureAtlas[]{this.i, this.k, this.m, this.o, this.s, this.u};
        this.d = new BitmapTextureAtlas((int) PVRTexture.FLAG_MIPMAP, (int) PVRTexture.FLAG_MIPMAP, BitmapTexture.BitmapTextureFormat.RGB_565, TextureOptions.BILINEAR);
        this.e = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.d, this, "splash.png", 0, 0);
        this.mEngine.getTextureManager().loadTexture(this.d);
        SoundFactory.setAssetBasePath("sfx/");
        MusicFactory.setAssetBasePath("sfx/");
        try {
            this.ak = SoundFactory.createSoundFromAsset(this.mEngine.getSoundManager(), this, "select.ogg");
            this.al = SoundFactory.createSoundFromAsset(this.mEngine.getSoundManager(), this, "jump.ogg");
            this.am = SoundFactory.createSoundFromAsset(this.mEngine.getSoundManager(), this, "score.ogg");
            this.an = SoundFactory.createSoundFromAsset(this.mEngine.getSoundManager(), this, "ready.ogg");
            this.ao = SoundFactory.createSoundFromAsset(this.mEngine.getSoundManager(), this, "go.ogg");
            this.ap = SoundFactory.createSoundFromAsset(this.mEngine.getSoundManager(), this, "gameover.ogg");
            this.ar = MusicFactory.createMusicFromAsset(this.mEngine.getMusicManager(), this, "quick.ogg");
            this.as = MusicFactory.createMusicFromAsset(this.mEngine.getMusicManager(), this, "timeless.ogg");
            this.as.setLooping(true);
            this.aq = MusicFactory.createMusicFromAsset(this.mEngine.getMusicManager(), this, "menu.ogg");
            this.aq.setLooping(true);
        } catch (IOException e2) {
            Debug.e(e2);
        }
    }

    public final void a() {
        this.c = 0;
        Log.d("Echo", "Begin Load - T: " + this.c + " - pResume: " + false);
        this.Z = 0;
        this.ag.registerUpdateHandler(new r(this));
    }

    public Scene onLoadScene() {
        this.mEngine.registerUpdateHandler(new FPSLogger());
        this.ag = new Scene();
        this.ag.setBackground(new ColorBackground(1.0f, 1.0f, 1.0f));
        this.ag.registerUpdateHandler(new p(this));
        this.ah = new Scene();
        this.ah.setBackground(new ColorBackground(0.05f, 0.25f, 0.15f));
        this.ai = new n(this.n);
        this.ah.attachChild(this.ai);
        this.ah.setOnSceneTouchListener(this);
        this.af = new Scene();
        for (int i2 = 0; i2 < 4; i2++) {
            this.af.attachChild(new Entity());
        }
        this.af.setBackground(new ColorBackground(0.35f, 0.7f, 0.95f));
        this.W = new Sprite(-20.0f, 133.0f, this.t);
        this.af.getChild(1).attachChild(this.W);
        this.w = new ChangeableText(20.0f, 171.0f, this.j, "Score:  0", "Score:  000111000111000".length());
        this.x = new ChangeableText(290.0f, 171.0f, this.j, " ", "Bonus: 999x".length());
        this.y = new ChangeableText(210.0f, 124.0f, this.j, " ", "1:00".length());
        this.y.setScale(0.75f);
        this.w.setColor(0.0f, 0.0f, 0.0f);
        this.y.setColor(0.0f, 0.0f, 0.0f);
        this.af.getChild(1).attachChild(this.w);
        this.af.getChild(1).attachChild(this.x);
        this.af.getChild(1).attachChild(this.y);
        this.af.registerUpdateHandler(new TimerHandler(0.05f, true, new o(this)));
        this.A = new Text(110.0f, 440.0f, this.l, "READY");
        this.B = new Text(160.0f, 440.0f, this.l, "GO!");
        this.A.setColor(0.0f, 0.0f, 0.0f, 0.0f);
        this.B.setColor(0.0f, 0.0f, 0.0f, 0.0f);
        this.af.getChild(3).attachChild(this.A);
        this.af.getChild(3).attachChild(this.B);
        this.C = new ChangeableText(0.0f, 440.0f, this.l, " ", HorizontalAlign.CENTER, "TIME'S\nUP!".length());
        this.C.setColor(0.0f, 0.0f, 0.0f, 0.0f);
        this.af.getChild(3).attachChild(this.C);
        int i3 = 0;
        while (true) {
            int i4 = i3;
            if (i4 >= 5) {
                break;
            }
            int i5 = 0;
            while (true) {
                int i6 = i5;
                if (i6 >= 5) {
                    break;
                }
                this.J[i4][i6] = new e(i4, i6, 2, this.p, this.j);
                this.af.getChild(2).attachChild(this.J[i4][i6]);
                i5 = i6 + 1;
            }
            i3 = i4 + 1;
        }
        int i7 = 0;
        while (true) {
            int i8 = i7;
            if (i8 >= 5) {
                this.I = new Sprite(35.0f, 718.0f, this.r);
                this.af.getChild(0).attachChild(this.I);
                this.H = new Sprite(265.0f, 718.0f, this.q);
                this.af.getChild(0).attachChild(this.H);
                this.z = new ChangeableText(0.0f, 0.0f, this.j, " ", "+99999".length());
                this.z.setScale(0.75f);
                this.z.setColor(0.0f, 0.0f, 0.0f, 0.0f);
                this.af.getChild(3).attachChild(this.z);
                this.G = new AlphaModifier(0.3f, 0.0f, 1.0f);
                this.F = new SequenceEntityModifier(new ab(this), new DelayModifier(0.4f), new ParallelEntityModifier(new AlphaModifier(0.2f, 0.0f, 1.0f), new ScaleModifier(1.4f, 0.75f, 1.75f, EaseBackOut.getInstance()), new MoveYModifier(0.9f, 440.0f, 390.0f, EaseBackOut.getInstance())), new AlphaModifier(0.5f, 1.0f, 0.2f));
                this.E = new SequenceEntityModifier(new aa(this), new DelayModifier(0.3f), new ParallelEntityModifier(new AlphaModifier(0.2f, 0.0f, 1.0f), new ScaleModifier(0.7f, 0.5f, 2.0f, EaseBackOut.getInstance()), new MoveYModifier(0.3f, 440.0f, 390.0f, EaseBackOut.getInstance())), new AlphaModifier(0.3f, 1.0f, 0.0f));
                this.D = new SequenceEntityModifier(new ad(this), new DelayModifier(0.3f), new ParallelEntityModifier(new AlphaModifier(0.2f, 0.0f, 1.0f), new ScaleModifier(0.7f, 0.5f, 2.0f, EaseBackOut.getInstance()), new MoveYModifier(0.3f, 440.0f, 390.0f, EaseBackOut.getInstance())), new AlphaModifier(0.3f, 1.0f, 0.0f));
                this.aj = new h(this.n);
                this.af.setOnSceneTouchListener(this);
                return this.ag;
            }
            this.K[i8] = new e(-2, i8, 95, this.p, this.j);
            this.af.getChild(2).attachChild(this.K[i8]);
            i7 = i8 + 1;
        }
    }

    public void onLoadComplete() {
    }

    public boolean onSceneTouchEvent(Scene scene, TouchEvent touchEvent) {
        runOnUpdateThread(new v(this, touchEvent));
        return false;
    }

    public final void a(TouchEvent touchEvent) {
        float x2 = touchEvent.getX();
        float y2 = touchEvent.getY();
        if (x2 < 240.0f) {
            if (y2 > 94.0f && y2 <= 184.0f) {
                a(this.ak);
                Intent intent = new Intent(this, Options.class);
                intent.putExtra("game_count", this.ae);
                startActivity(intent);
            } else if (y2 > 184.0f && y2 <= 274.0f) {
                a(this.ak);
                e();
            }
        } else if (y2 > 150.0f && y2 <= 240.0f) {
            a(this.ak);
            this.Z = 2;
            this.mEngine.setScene(this.ah);
        } else if (y2 > 240.0f && y2 <= 330.0f) {
            a(this.ak);
            l.i().f();
            this.aa = 0;
            this.mEngine.setScene(this.af);
            b();
        } else if (y2 > 330.0f && y2 <= 420.0f) {
            a(this.ak);
            l.i().f();
            this.aa = 1;
            this.mEngine.setScene(this.af);
            b();
        }
    }

    public final void b(TouchEvent touchEvent) {
        float y2 = touchEvent.getY();
        AlphaModifier alphaModifier = new AlphaModifier(0.3f, 0.0f, 1.0f, new u(this));
        if (((double) y2) > 45.0d && ((double) y2) <= 175.0d) {
            a(this.ak);
            l.i().a();
            this.ai.getChild(0).clearEntityModifiers();
            this.ai.getChild(0).registerEntityModifier(alphaModifier);
        } else if (((double) y2) > 175.0d && ((double) y2) <= 305.0d) {
            a(this.ak);
            l.i().b();
            this.ai.getChild(1).clearEntityModifiers();
            this.ai.getChild(1).registerEntityModifier(alphaModifier);
        } else if (((double) y2) > 305.0d && ((double) y2) <= 435.0d) {
            a(this.ak);
            l.i().c();
            this.ai.getChild(2).clearEntityModifiers();
            this.ai.getChild(2).registerEntityModifier(alphaModifier);
        } else if (((double) y2) > 435.0d && ((double) y2) <= 565.0d) {
            a(this.ak);
            l.i().d();
            this.ai.getChild(3).clearEntityModifiers();
            this.ai.getChild(3).registerEntityModifier(alphaModifier);
        } else if (((double) y2) > 565.0d && y2 < 720.0f) {
            a(this.ak);
            l.i().e();
            this.ai.getChild(4).clearEntityModifiers();
            this.ai.getChild(4).registerEntityModifier(alphaModifier);
        }
    }

    public final void c(TouchEvent touchEvent) {
        int i2;
        float x2 = touchEvent.getX();
        float y2 = touchEvent.getY() + 6.0f;
        if (y2 > 224.0f && y2 < 704.0f && this.T) {
            int i3 = (int) (x2 / 96.0f);
            int i4 = (int) ((y2 - 224.0f) / 96.0f);
            int i5 = (this.N - 1) % 7;
            int a2 = m.n().a(this.J[i4][i3]);
            if (a2 == 2) {
                int b2 = this.J[i4][i3].b();
                float x3 = this.J[i4][i3].getX();
                float y3 = this.J[i4][i3].getY();
                h();
                a(this.am);
                this.J[m.n().b()][m.n().c()].a(m.n().b(), m.n().c(), l.i().h(), -16777216);
                this.J[m.n().d()][m.n().e()].a(m.n().d(), m.n().e(), l.i().h(), -16777216);
                this.J[m.n().f()][m.n().g()].a(m.n().f(), m.n().g(), l.i().h(), -16777216);
                this.J[i4][i3].a(i4, i3, l.i().h(), -16777216);
                if (this.N < 999 && ((i4 == 2 && i3 == 2) || ((m.n().b() == 2 && m.n().c() == 2) || ((m.n().d() == 2 && m.n().e() == 2) || (m.n().f() == 2 && m.n().g() == 2))))) {
                    this.N++;
                    i5 = (this.N - 1) % 7;
                }
                this.J[2][2].a(2, 2, b2, c.c[i5]);
                this.J[2][2].setAlpha(1.0f);
                this.J[2][2].b(i4, i3, 2, 2);
                this.J[m.n().b()][m.n().c()].a(0.25f);
                this.J[m.n().d()][m.n().e()].a(0.5f);
                this.J[m.n().f()][m.n().g()].a(0.75f);
                this.J[i4][i3].a(1.0f);
                if (!(this.Q == 2 && this.R == 2)) {
                    if ((i4 == this.Q && i3 == this.R) || ((m.n().b() == this.Q && m.n().c() == this.R) || ((m.n().d() == this.Q && m.n().e() == this.R) || (m.n().f() == this.Q && m.n().g() == this.R)))) {
                        if (this.aa == 1) {
                            this.O += this.P * 33;
                        } else {
                            this.O += this.P;
                        }
                        if (this.O > 99999) {
                            this.O = 99999;
                        }
                    }
                    this.J[this.Q][this.R].a(-1);
                    this.J[this.Q][this.R].b(0);
                }
                this.Q = MathUtils.random(0, 4);
                this.R = MathUtils.random(0, 4);
                if (!(this.Q == 2 && this.R == 2)) {
                    int random = MathUtils.random(1, 100);
                    if (random > 85) {
                        this.Q = 2;
                        this.R = 2;
                    } else {
                        if (random > 70) {
                            i2 = c.b;
                            this.P = 48;
                        } else {
                            i2 = c.a;
                            this.P = 25;
                        }
                        this.J[this.Q][this.R].a(i2);
                        this.J[this.Q][this.R].b(2);
                    }
                }
                this.L += (long) (this.N * this.O);
                this.x.setColor(((float) Color.red(c.c[i5])) / 255.0f, ((float) Color.green(c.c[i5])) / 255.0f, ((float) Color.blue(c.c[i5])) / 255.0f);
                if (this.N > 99) {
                    this.x.setText("Bonus: " + Integer.toString(this.N) + TMXConstants.TAG_OBJECT_ATTRIBUTE_X);
                } else if (this.N > 9) {
                    this.x.setText("Bonus:  " + Integer.toString(this.N) + TMXConstants.TAG_OBJECT_ATTRIBUTE_X);
                } else {
                    this.x.setText("Bonus:   " + Integer.toString(this.N) + TMXConstants.TAG_OBJECT_ATTRIBUTE_X);
                }
                this.z.setText("+" + Integer.toString(this.O));
                this.z.setPosition(x3, y3);
                this.z.registerEntityModifier(new ParallelEntityModifier(new AlphaModifier(1.4f, 1.0f, 0.0f, EaseStrongIn.getInstance()), new MoveYModifier(1.2f, y3 - 42.0f, y3 - 118.0f)));
                z.j().i();
                if (!z.j().a(this.J)) {
                    c();
                }
            } else if (a2 == 3 && m.n().m()) {
                if (this.X && this.K[2].b() != this.J[i4][i3].b()) {
                    j();
                }
                this.K[2].setAlpha(0.0f);
                this.K[3].setAlpha(1.0f);
                this.J[i4][i3].a();
                this.J[i4][i3].setAlpha(1.0f);
                this.J[i4][i3].b(i4, i3, -2, 2);
                a(this.al);
            } else if (a2 == 3 && m.n().l()) {
                if (this.X && this.K[1].b() != this.J[i4][i3].b()) {
                    j();
                }
                this.K[1].setAlpha(0.0f);
                this.J[i4][i3].a();
                this.J[i4][i3].setAlpha(1.0f);
                this.J[i4][i3].b(i4, i3, -2, 1);
                a(this.al);
            } else if (a2 == 3 && m.n().k()) {
                if (this.X && this.K[0].b() != this.J[i4][i3].b()) {
                    j();
                }
                this.K[0].setAlpha(0.0f);
                this.J[i4][i3].a();
                this.J[i4][i3].setAlpha(1.0f);
                this.J[i4][i3].b(i4, i3, -2, 0);
                a(this.al);
            } else if (a2 == 4) {
                a(this.ak);
                h();
            }
        }
        if (y2 > 704.0f && x2 < 240.0f && this.T) {
            a(this.ak);
            h();
            this.G.reset();
            this.I.registerEntityModifier(this.G);
        } else if (y2 > 704.0f && x2 >= 240.0f && this.T) {
            a(this.ak);
            i();
        }
    }

    public final void d(TouchEvent touchEvent) {
        float y2 = touchEvent.getY();
        if (this.aa == 99 && y2 < 720.0f) {
            g();
        } else if (y2 > 158.0f && y2 <= 350.0f) {
            a(this.ak);
            this.aj.getChild(1).clearEntityModifiers();
            this.aj.getChild(1).registerEntityModifier(new AlphaModifier(0.3f, 0.0f, 1.0f, new t(this)));
        } else if (y2 > 350.0f && y2 <= 542.0f) {
            a(this.ak);
            this.aj.getChild(2).clearEntityModifiers();
            this.aj.getChild(2).registerEntityModifier(new AlphaModifier(0.3f, 0.0f, 1.0f, new s(this)));
        } else if (y2 > 542.0f && y2 < 720.0f) {
            g();
        }
    }

    private void g() {
        a(this.ak);
        this.aj.getChild(3).clearEntityModifiers();
        this.aj.getChild(3).registerEntityModifier(new AlphaModifier(0.3f, 0.0f, 1.0f, new x(this)));
    }

    private void h() {
        if (m.n().k()) {
            m.n().h();
            this.J[m.n().b()][m.n().c()].e();
        }
        if (m.n().l()) {
            m.n().i();
            this.J[m.n().d()][m.n().e()].e();
        }
        if (m.n().m()) {
            m.n().j();
            this.J[m.n().f()][m.n().g()].e();
        }
        this.K[3].setAlpha(0.0f);
        if (this.X) {
            j();
        }
    }

    private void i() {
        h();
        z.j().b(this.J);
        this.K[0].a(-2, 0, this.J[z.j().a()][z.j().b()].b(), -16777216);
        this.K[1].a(-2, 1, this.J[z.j().c()][z.j().d()].b(), -16777216);
        this.K[2].a(-2, 2, this.J[z.j().e()][z.j().f()].b(), -16777216);
        this.K[4].a(-2, 4, this.J[z.j().g()][z.j().h()].b(), -16777216);
        for (int i2 = 0; i2 < 5; i2++) {
            this.K[i2].setAlpha(0.7f);
        }
        this.X = true;
        this.H.setAlpha(0.5f);
    }

    private void j() {
        this.J[z.j().a()][z.j().b()].f();
        this.J[z.j().c()][z.j().d()].f();
        this.J[z.j().e()][z.j().f()].f();
        this.J[z.j().g()][z.j().h()].f();
        for (int i2 = 0; i2 < 5; i2++) {
            this.K[i2].setAlpha(0.0f);
        }
        this.X = false;
        this.H.setAlpha(1.0f);
    }

    public final void b() {
        this.V = 60.0f;
        this.W.setPosition(-20.0f, 133.0f);
        m.n().a();
        l.i().g();
        this.L = 0;
        this.M = 0;
        this.N = 1;
        this.O = 1500;
        this.P = 0;
        this.Q = 2;
        this.R = 2;
        this.w.setText("Score:  0");
        this.x.setText("Bonus:   1x");
        this.x.setColor(((float) Color.red(c.c[0])) / 255.0f, ((float) Color.green(c.c[0])) / 255.0f, ((float) Color.blue(c.c[0])) / 255.0f);
        z.j().i();
        this.T = false;
        this.Y = false;
        this.Z = 3;
        this.H.setAlpha(1.0f);
        this.I.setAlpha(1.0f);
        if (this.g.booleanValue()) {
            w wVar = this.au;
            wVar.b.post(wVar.c);
        }
        do {
            for (int i2 = 0; i2 < 5; i2++) {
                for (int i3 = 0; i3 < 5; i3++) {
                    this.J[i2][i3].a(i2, i3, l.i().h(), -16777216);
                    if (i2 != 2 || i3 != 2) {
                        this.J[i2][i3].setAlpha(1.0f);
                        this.J[i2][i3].b(0);
                    }
                }
            }
        } while (!z.j().a(this.J));
        for (int i4 = 0; i4 < 5; i4++) {
            this.K[i4].a(-2, i4, 95, -16777216);
        }
        this.C.unregisterEntityModifier(this.F);
        this.A.unregisterEntityModifier(this.D);
        this.B.unregisterEntityModifier(this.E);
        this.C.setColor(0.9f, 0.0f, 0.0f, 0.0f);
        this.A.setColor(1.0f, 0.0f, 0.0f, 0.0f);
        this.B.setColor(0.0f, 1.0f, 0.0f, 0.0f);
        this.z.clearEntityModifiers();
        this.z.setColor(0.0f, 0.0f, 0.0f, 0.0f);
        b(this.aq);
        if (this.aa == 1) {
            this.y.setText("1:00");
            this.D.reset();
            this.A.registerEntityModifier(this.D);
            a(this.an);
            a(this.ar);
        } else {
            this.y.setText(" ");
            this.E.reset();
            this.B.registerEntityModifier(this.E);
            a(this.ao);
            a(this.as);
        }
        this.af.setVisible(true);
    }

    public final void c() {
        if (!this.Y) {
            this.T = false;
            this.Y = true;
            this.Z = 5;
            if (this.V <= 0.0f) {
                this.C.setText("TIME'S\nUP!");
                this.C.setPosition(113.0f, 440.0f);
                this.F.reset();
                this.C.registerEntityModifier(this.F);
                a(this.ap);
                i();
            } else {
                this.C.setText("STUCK!");
                this.C.setPosition(102.0f, 440.0f);
                this.F.reset();
                this.C.registerEntityModifier(new DelayModifier(1.0f, new ac(this)));
            }
            d();
            if (this.g.booleanValue()) {
                this.I.setAlpha(0.0f);
                this.H.setAlpha(0.0f);
                this.au.a();
            }
        }
    }

    public final void d() {
        if (this.M <= 0) {
            return;
        }
        if (this.aa == 1 || this.aa == 0 || this.aa == 2) {
            runOnUiThread(new af(this));
        }
    }

    public final void e() {
        Intent intent = new Intent(this, LeaderboardsScreenActivity.class);
        int i2 = this.aa;
        if (this.aa == 99) {
            i2 = 0;
        }
        intent.putExtra("mode", i2);
        if (this.ab) {
            intent.putExtra("leaderboard", 3);
        } else {
            intent.putExtra("leaderboard", 2);
        }
        startActivity(intent);
    }

    public final void f() {
        runOnUpdateThread(new ae(this));
    }

    public final void a(Sound sound) {
        if (this.ad) {
            sound.play();
        }
    }

    public final void a(Music music) {
        if (this.ac && !music.isPlaying()) {
            music.play();
        }
    }

    private void c(Music music) {
        if (this.ac && music.isPlaying()) {
            music.pause();
        }
    }

    public final void b(Music music) {
        if (this.ac && music.isPlaying()) {
            music.pause();
            music.seekTo(0);
        }
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 24) {
            this.at.adjustStreamVolume(3, 1, 1);
            if (this.Z != 0) {
                a(this.al);
            }
        } else if (i2 == 25) {
            this.at.adjustStreamVolume(3, -1, 1);
            if (this.Z != 0) {
                a(this.al);
            }
        } else if (this.Z == 2) {
            if (i2 == 4 || i2 == 82) {
                f();
            }
        } else if (this.Z == 3 || this.Z == 4 || this.Z == 5) {
            if (i2 == 4 || i2 == 82) {
                if (this.M <= 0 || this.aa != 0) {
                    if (i2 == 4) {
                        this.av = true;
                    }
                    d();
                    f();
                } else {
                    View inflate = View.inflate(this, R.layout.end_this_game_text, null);
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setView(inflate).setCancelable(false).setPositiveButton("Yes", new ah(this)).setNegativeButton("No", new ag(this));
                    AlertDialog create = builder.create();
                    create.setOnDismissListener(new al(this));
                    create.show();
                }
            } else if (i2 != 84 || !this.T) {
                super.onKeyDown(i2, keyEvent);
            } else {
                i();
            }
        } else if (this.Z == 6) {
            if (i2 == 4) {
                runOnUpdateThread(new ak(this));
            } else if (i2 == 82) {
                f();
            } else {
                super.onKeyDown(i2, keyEvent);
            }
        } else if (this.Z == 1) {
            super.onKeyDown(i2, keyEvent);
        }
        return true;
    }

    public void onPauseGame() {
        if (this.Z == 3 || this.Z == 4 || this.Z == 5) {
            if (this.aa == 1) {
                b(this.ar);
            } else {
                c(this.as);
            }
        } else if (this.Z == 1) {
            c(this.aq);
        }
        super.onPauseGame();
    }

    public void onResumeGame() {
        try {
            f.a(this, "oRYOJDGwf6d0oh+Bm7vpOkSsL8axB64yud2G1/YWpVvUuc8vDsu4CQ==");
        } catch (IllegalStateException e2) {
            Log.v("Init scoreloop", e2.getMessage());
        }
        if (this.Z == 3 || this.Z == 4 || this.Z == 5) {
            if (this.aa == 1) {
                runOnUpdateThread(new ai(this));
            } else {
                a(this.as);
            }
            if (this.aa == 1 && this.T) {
                runOnUiThread(new aj(this));
            }
        } else if (this.Z == 1) {
            SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
            this.ab = defaultSharedPreferences.getBoolean("local_score_checkbox_preference", false);
            this.ac = defaultSharedPreferences.getBoolean("music_checkbox_preference", true);
            this.ad = defaultSharedPreferences.getBoolean("sound_checkbox_preference", true);
            a(this.aq);
        }
        if ((this.Z == 1 || this.Z == 6) && this.g.booleanValue()) {
            this.au.a();
        }
        super.onResumeGame();
    }
}
