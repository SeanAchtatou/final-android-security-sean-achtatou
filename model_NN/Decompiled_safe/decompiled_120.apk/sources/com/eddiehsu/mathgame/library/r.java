package com.eddiehsu.mathgame.library;

import android.util.Log;
import org.anddev.andengine.engine.handler.IUpdateHandler;

final class r implements IUpdateHandler {
    private /* synthetic */ MathGame a;
    private final /* synthetic */ boolean b = false;

    r(MathGame mathGame) {
        this.a = mathGame;
    }

    public final void onUpdate(float f) {
        if (this.a.c < this.a.b.length) {
            Log.d("Echo", "Loading texture: " + (this.a.c + 1) + " of " + this.a.b.length);
            this.a.mEngine.getTextureManager().loadTexture(this.a.b[this.a.c]);
            if (!this.a.b[this.a.c].isLoadedToHardware()) {
                return;
            }
            if (this.a.c == this.a.b.length - 1) {
                this.a.ag.unregisterUpdateHandler(this);
                this.a.mEngine.getFontManager().reloadFonts();
                if (this.b) {
                    Log.d("Echo", "Textures Loaded, Resuming...");
                    return;
                }
                Log.d("Echo", "Textures Loaded, proceeding to MainMenu");
                MathGame mathGame = this.a;
                mathGame.runOnUpdateThread(new q(mathGame));
                return;
            }
            this.a.c++;
            Log.d("Echo", "Loading Next texture: " + (this.a.c + 1));
        }
    }

    public final void reset() {
    }
}
