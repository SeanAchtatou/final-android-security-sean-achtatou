package com.eddiehsu.mathgame.library;

import org.anddev.andengine.opengl.texture.compressed.pvr.PVRTexture;
import org.anddev.andengine.util.MathUtils;

public final class l {
    private static final l a = new l();
    private int[] b = new int[PVRTexture.FLAG_TWIDDLE];
    private int[] c = new int[PVRTexture.FLAG_TWIDDLE];
    private int[] d = {2};
    private int[] e = {1};
    private int[] f = {96};
    private int[] g = {4, 5, 6};
    private int[] h = {1, 2, 3};
    private int[] i = {96, 97};
    private int[] j = {11, 12, 13, 14, 15, 16, 17, 18, 19};
    private int[] k = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    private int[] l = {96, 97};
    private int[] m = {1, 4, 6, 8, 9};
    private int[] n = {2, 3};
    private int[] o = {98, 99};
    private int[] p = {1, 10, 12, 14, 15, 16, 18, 20, 21, 24, 27, 28, 32, 36};
    private int[] q = {2, 3, 4, 5, 6, 7, 8, 9};
    private int[] r = {98, 99};
    private int[] s = {25, 49, 64, 81, 11, 13, 17, 19, 50, 60, 70, 80, 55, 60, 65, 70, 75, 12, 14, 15, 16, 18, 20, 21, 24, 27, 28, 30, 32, 35, 36, 40, 42, 45, 48, 54, 56, 63, 72};
    private int[] t = {2, 3, 4, 5, 6, 7, 8, 9, 10};
    private int[] u = {96, 97, 98, 99};
    private int v;
    private int w = 0;

    private l() {
        f();
    }

    public final void a() {
        a(this.d, this.e, this.f, 1, 5, Math.round(((((float) ((this.d.length * 1) + (this.e.length * 5))) * 0.30208334f) / 0.6979166f) / ((float) this.f.length)));
    }

    public final void b() {
        a(this.g, this.h, this.i, 1, 2, Math.round(((((float) ((this.g.length * 1) + (this.h.length * 2))) * 0.30208334f) / 0.6979166f) / ((float) this.i.length)));
    }

    public final void c() {
        a(this.j, this.k, this.l, 1, 3, Math.round(((((float) ((this.j.length * 1) + (this.k.length * 3))) * 0.30208334f) / 0.6979166f) / ((float) this.l.length)));
    }

    public final void d() {
        a(this.m, this.n, this.o, 1, 4, Math.round(((((float) ((this.m.length * 1) + (this.n.length * 4))) * 0.30208334f) / 0.6979166f) / ((float) this.o.length)));
    }

    public final void e() {
        a(this.p, this.q, this.r, 1, 3, Math.round(((((float) ((this.p.length * 1) + (this.q.length * 3))) * 0.30208334f) / 0.6979166f) / ((float) this.r.length)));
    }

    public final void f() {
        int round = Math.round((((float) (this.s.length * 2)) * 2.5f) / ((float) this.t.length));
        a(this.s, this.t, this.u, 2, round, Math.round(((((float) ((this.s.length * 2) + (this.t.length * round))) * 0.30208334f) / 0.6979166f) / ((float) this.u.length)));
    }

    private void a(int[] iArr, int[] iArr2, int[] iArr3, int i2, int i3, int i4) {
        this.w = 0;
        for (int i5 = 0; i5 < iArr.length; i5++) {
            for (int i6 = 0; i6 < i2; i6++) {
                this.c[this.w] = iArr[i5];
                this.w++;
            }
        }
        for (int i7 = 0; i7 < iArr2.length; i7++) {
            for (int i8 = 0; i8 < i3; i8++) {
                this.c[this.w] = iArr2[i7];
                this.w++;
            }
        }
        for (int i9 = 0; i9 < iArr3.length; i9++) {
            for (int i10 = 0; i10 < i4; i10++) {
                this.c[this.w] = iArr3[i9];
                this.w++;
            }
        }
        g();
    }

    public final void g() {
        for (int i2 = 0; i2 < this.b.length; i2++) {
            this.b[i2] = this.c[MathUtils.random(0, this.w - 1)];
        }
        this.v = 0;
    }

    public final int h() {
        if (this.v >= this.b.length) {
            g();
        }
        int[] iArr = this.b;
        int i2 = this.v;
        this.v = i2 + 1;
        return iArr[i2];
    }

    public static l i() {
        return a;
    }
}
