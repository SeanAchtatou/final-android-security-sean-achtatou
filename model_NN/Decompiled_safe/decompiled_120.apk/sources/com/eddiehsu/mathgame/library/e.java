package com.eddiehsu.mathgame.library;

import android.graphics.Color;
import org.anddev.andengine.entity.Entity;
import org.anddev.andengine.entity.modifier.DelayModifier;
import org.anddev.andengine.entity.modifier.FadeInModifier;
import org.anddev.andengine.entity.modifier.LoopEntityModifier;
import org.anddev.andengine.entity.modifier.MoveXModifier;
import org.anddev.andengine.entity.modifier.MoveYModifier;
import org.anddev.andengine.entity.modifier.ParallelEntityModifier;
import org.anddev.andengine.entity.modifier.SequenceEntityModifier;
import org.anddev.andengine.entity.sprite.TiledSprite;
import org.anddev.andengine.entity.text.ChangeableText;
import org.anddev.andengine.opengl.font.Font;
import org.anddev.andengine.opengl.texture.region.TiledTextureRegion;
import org.anddev.andengine.util.modifier.ease.EaseBackOut;

public final class e extends Entity {
    private TiledSprite a;
    private ChangeableText b;
    private Font c;
    private int d;
    private int e;
    private String f;
    private int g;
    private int h;
    private float i;
    private float j;
    private int k = 6;
    private int l = this.k;
    private int m = 0;
    private int n = -33;
    private ParallelEntityModifier o;
    private ParallelEntityModifier p;
    private SequenceEntityModifier q;
    private SequenceEntityModifier r;
    private SequenceEntityModifier s;
    private SequenceEntityModifier t;

    public e(int i2, int i3, int i4, TiledTextureRegion tiledTextureRegion, Font font) {
        this.g = i2;
        this.h = i3;
        this.e = i4;
        this.c = font;
        this.d = -16777216;
        this.i = (float) (this.h * 96);
        this.j = (float) ((this.g * 96) + 224);
        j();
        this.a = new TiledSprite(this.i + ((float) this.k), this.j + ((float) this.l), tiledTextureRegion.clone());
        this.b = new ChangeableText(this.i + 42.0f + ((float) this.m), this.j + 42.0f + ((float) this.n), this.c, " ", "XX".length());
        setAlpha(0.0f);
        attachChild(this.a);
        attachChild(this.b);
        this.q = new SequenceEntityModifier(new MoveYModifier(0.125f, this.j + ((float) this.l), this.j + ((float) this.l) + 4.0f), new LoopEntityModifier(new SequenceEntityModifier(new MoveYModifier(0.25f, this.j + ((float) this.l) + 4.0f, (this.j + ((float) this.l)) - 4.0f), new MoveYModifier(0.25f, (this.j + ((float) this.l)) - 4.0f, this.j + ((float) this.l) + 4.0f))));
        this.r = new SequenceEntityModifier(new MoveYModifier(0.125f, this.j + 42.0f + ((float) this.n), this.j + 42.0f + ((float) this.n) + 4.0f), new LoopEntityModifier(new SequenceEntityModifier(new MoveYModifier(0.25f, this.j + 42.0f + ((float) this.n) + 4.0f, ((this.j + 42.0f) + ((float) this.n)) - 4.0f), new MoveYModifier(0.25f, ((this.j + 42.0f) + ((float) this.n)) - 4.0f, this.j + 42.0f + ((float) this.n) + 4.0f))));
        this.s = new SequenceEntityModifier(new MoveYModifier(0.125f, this.j + ((float) this.l), (this.j + ((float) this.l)) - 4.0f), new LoopEntityModifier(new SequenceEntityModifier(new MoveYModifier(0.25f, (this.j + ((float) this.l)) - 4.0f, this.j + ((float) this.l) + 4.0f), new MoveYModifier(0.25f, this.j + ((float) this.l) + 4.0f, (this.j + ((float) this.l)) - 4.0f))));
        this.t = new SequenceEntityModifier(new MoveYModifier(0.125f, this.j + 42.0f + ((float) this.n), ((this.j + 42.0f) + ((float) this.n)) - 4.0f), new LoopEntityModifier(new SequenceEntityModifier(new MoveYModifier(0.25f, ((this.j + 42.0f) + ((float) this.n)) - 4.0f, this.j + 42.0f + ((float) this.n) + 4.0f), new MoveYModifier(0.25f, this.j + 42.0f + ((float) this.n) + 4.0f, ((this.j + 42.0f) + ((float) this.n)) - 4.0f))));
    }

    public final void a(int i2, int i3, int i4, int i5) {
        this.g = i2;
        this.h = i3;
        this.e = i4;
        this.d = i5;
        if (this.g == 2 && this.h == 2 && this.d == -16777216) {
            this.e = 94;
        }
        this.i = (float) (this.h * 96);
        this.j = (float) ((this.g * 96) + 224);
        if (i2 == -2) {
            this.j = (float) ((int) ((96.0f * (((float) this.g) - 0.1f)) + 224.0f));
        }
        j();
        a();
        i();
        this.b.setText(this.f);
        setAlpha(0.0f);
    }

    private void i() {
        this.a.reset();
        this.b.reset();
        this.a.setPosition(this.i + ((float) this.k), this.j + ((float) this.l));
        this.b.setPosition(this.i + 42.0f + ((float) this.m), this.j + 42.0f + ((float) this.n));
        if (this.d != -16777216) {
            this.a.setColor(((float) Color.red(this.d)) / 255.0f, ((float) Color.green(this.d)) / 255.0f, ((float) Color.blue(this.d)) / 255.0f);
        }
        this.b.setColor(0.0f, 0.0f, 0.0f);
    }

    public final void a() {
        this.a.clearEntityModifiers();
        this.b.clearEntityModifiers();
    }

    public final void setAlpha(float f2) {
        this.a.setAlpha(f2);
        this.b.setAlpha(f2);
    }

    public final void a(int i2) {
        this.d = i2;
        this.a.setColor(((float) Color.red(this.d)) / 255.0f, ((float) Color.green(this.d)) / 255.0f, ((float) Color.blue(this.d)) / 255.0f);
    }

    public final void b(int i2) {
        this.a.setCurrentTileIndex(i2);
    }

    public final int b() {
        return this.e;
    }

    public final int c() {
        return this.g;
    }

    public final int d() {
        return this.h;
    }

    public final float getX() {
        return this.i;
    }

    public final float getY() {
        return this.j;
    }

    public final void e() {
        this.a.unregisterEntityModifier(this.o);
        this.b.unregisterEntityModifier(this.p);
        i();
    }

    public final void f() {
        this.a.unregisterEntityModifier(this.q);
        this.b.unregisterEntityModifier(this.r);
        this.a.unregisterEntityModifier(this.s);
        this.b.unregisterEntityModifier(this.t);
    }

    public final void a(float f2) {
        if (this.g != 2 || this.h != 2) {
            SequenceEntityModifier sequenceEntityModifier = new SequenceEntityModifier(new g(this), new DelayModifier(f2), new FadeInModifier(0.5f));
            this.a.registerEntityModifier(sequenceEntityModifier);
            this.b.registerEntityModifier(sequenceEntityModifier);
        }
    }

    public final void g() {
        this.a.unregisterEntityModifier(this.q);
        this.b.unregisterEntityModifier(this.r);
        this.q.reset();
        this.r.reset();
        this.a.registerEntityModifier(this.q);
        this.b.registerEntityModifier(this.r);
    }

    public final void h() {
        this.a.unregisterEntityModifier(this.s);
        this.b.unregisterEntityModifier(this.t);
        this.s.reset();
        this.t.reset();
        this.a.registerEntityModifier(this.s);
        this.b.registerEntityModifier(this.t);
    }

    public final void b(int i2, int i3, int i4, int i5) {
        float f2 = (float) i3;
        float f3 = (float) i4;
        if (i2 == i4 && i3 == i5) {
            f2 -= 0.2f;
        }
        if (i4 == -2) {
            f3 -= 0.1f;
        }
        if (i4 == 2 && i5 == 2) {
            this.a.setColor(((float) Color.red(this.d)) / 255.0f, ((float) Color.green(this.d)) / 255.0f, ((float) Color.blue(this.d)) / 255.0f);
        }
        this.o = new ParallelEntityModifier(new MoveXModifier(0.5f, (96.0f * f2) + ((float) this.k), (float) ((i5 * 96) + this.k), EaseBackOut.getInstance()), new MoveYModifier(0.6f, (float) ((i2 * 96) + 224 + this.l), (96.0f * f3) + 224.0f + ((float) this.l), EaseBackOut.getInstance()));
        this.p = new ParallelEntityModifier(new MoveXModifier(0.5f, (f2 * 96.0f) + 42.0f + ((float) this.m), ((float) (i5 * 96)) + 42.0f + ((float) this.m), EaseBackOut.getInstance()), new MoveYModifier(0.6f, ((float) ((i2 * 96) + 224)) + 42.0f + ((float) this.n), (f3 * 96.0f) + 224.0f + 42.0f + ((float) this.n), EaseBackOut.getInstance()));
        this.a.registerEntityModifier(this.o);
        this.b.registerEntityModifier(this.p);
    }

    private void j() {
        if (this.e < 10 || this.e > 81) {
            this.m = -2;
        } else {
            this.m = -11;
        }
        if (this.e == 96) {
            this.f = "+";
        } else if (this.e == 97) {
            this.f = "−";
        } else if (this.e == 98) {
            this.f = "×";
        } else if (this.e == 99) {
            this.f = "÷";
        } else if (this.e == 94) {
            this.f = " ";
        } else if (this.e == 95) {
            this.f = "=";
        } else {
            this.f = Integer.toString(this.e);
        }
    }
}
