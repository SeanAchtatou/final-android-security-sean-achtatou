package com.eddiehsu.mathgame.library;

import android.util.Log;
import com.scoreloop.client.android.core.model.Score;
import com.scoreloop.client.android.ui.f;
import org.anddev.andengine.util.constants.TimeConstants;

final class af implements Runnable {
    private /* synthetic */ MathGame a;

    af(MathGame mathGame) {
        this.a = mathGame;
    }

    public final void run() {
        Score score = new Score(new Double(12.0d), null);
        score.setResult(Double.valueOf((double) this.a.L));
        score.setMinorResult(Double.valueOf(((double) ((this.a.O + this.a.N) * 100)) + this.a.a.doubleValue()));
        score.setLevel(Integer.valueOf((this.a.N * TimeConstants.MICROSECONDSPERSECOND) + this.a.O));
        score.setMode(Integer.valueOf(this.a.aa));
        if (this.a.ab) {
            score.setMinorResult(Double.valueOf(9.99999E7d + this.a.a.doubleValue()));
        }
        try {
            f.a().a(score, Boolean.valueOf(this.a.ab));
        } catch (IllegalStateException e) {
            Log.v("submit scoreloop", e.getMessage());
        }
    }
}
