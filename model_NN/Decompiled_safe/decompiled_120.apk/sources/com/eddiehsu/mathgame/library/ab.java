package com.eddiehsu.mathgame.library;

import org.anddev.andengine.entity.modifier.IEntityModifier;
import org.anddev.andengine.util.modifier.IModifier;

final class ab implements IEntityModifier.IEntityModifierListener {
    private /* synthetic */ MathGame a;

    ab(MathGame mathGame) {
        this.a = mathGame;
    }

    public final /* bridge */ /* synthetic */ void onModifierFinished(IModifier iModifier, Object obj) {
        if (this.a.aa == 99) {
            this.a.aj.getChild(1).setAlpha(0.0f);
            this.a.aj.getChild(2).setAlpha(0.0f);
            this.a.aj.getChild(3).setPosition(98.0f, 446.0f);
        } else {
            this.a.aj.getChild(1).setAlpha(1.0f);
            this.a.aj.getChild(2).setAlpha(1.0f);
            this.a.aj.getChild(3).setPosition(98.0f, 638.0f);
        }
        this.a.Z = 6;
        this.a.af.getChild(3).attachChild(this.a.aj);
        this.a.z.clearEntityModifiers();
        this.a.z.setAlpha(1.0f);
    }

    public final /* bridge */ /* synthetic */ void onModifierStarted(IModifier iModifier, Object obj) {
    }
}
