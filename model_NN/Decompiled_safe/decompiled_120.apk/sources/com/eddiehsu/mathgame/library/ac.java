package com.eddiehsu.mathgame.library;

import org.anddev.andengine.entity.modifier.IEntityModifier;
import org.anddev.andengine.util.modifier.IModifier;

final class ac implements IEntityModifier.IEntityModifierListener {
    private /* synthetic */ MathGame a;

    ac(MathGame mathGame) {
        this.a = mathGame;
    }

    public final /* bridge */ /* synthetic */ void onModifierFinished(IModifier iModifier, Object obj) {
        this.a.C.registerEntityModifier(this.a.F);
        this.a.a(this.a.ap);
        if (this.a.aa == 1) {
            this.a.b(this.a.ar);
        } else {
            this.a.b(this.a.as);
        }
    }

    public final /* bridge */ /* synthetic */ void onModifierStarted(IModifier iModifier, Object obj) {
    }
}
