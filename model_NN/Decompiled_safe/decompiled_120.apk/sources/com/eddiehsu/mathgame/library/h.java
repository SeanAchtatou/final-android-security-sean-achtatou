package com.eddiehsu.mathgame.library;

import org.anddev.andengine.entity.Entity;
import org.anddev.andengine.entity.primitive.Rectangle;
import org.anddev.andengine.entity.text.Text;
import org.anddev.andengine.opengl.font.Font;

public final class h extends Entity {
    public h(Font font) {
        Rectangle rectangle = new Rectangle(0.0f, 0.0f, 480.0f, 800.0f);
        Text text = new Text(76.0f, 254.0f, font, "Share Score");
        Text text2 = new Text(82.0f, 446.0f, font, "High Scores");
        Text text3 = new Text(98.0f, 638.0f, font, "Play Again");
        rectangle.setColor(0.0f, 0.0f, 0.0f, 0.4f);
        text.setColor(1.0f, 0.6f, 1.0f);
        text2.setColor(1.0f, 0.6f, 1.0f);
        text3.setColor(1.0f, 0.6f, 1.0f);
        attachChild(rectangle);
        attachChild(text);
        attachChild(text2);
        attachChild(text3);
    }
}
