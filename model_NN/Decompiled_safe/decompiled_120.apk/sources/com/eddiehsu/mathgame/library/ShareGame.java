package com.eddiehsu.mathgame.library;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class ShareGame extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Intent intent = new Intent("android.intent.action.SEND");
        intent.putExtra("android.intent.extra.TEXT", "stimulate your brain with #Android app #EinsteinMathAcademy by @androidphd https://market.android.com/details?id=com.eddiehsu.mathgame");
        intent.putExtra("android.intent.extra.SUBJECT", "Cool math game for Android: Einstein Math Academy");
        intent.setType("text/plain");
        startActivity(Intent.createChooser(intent, "Share this game"));
        finish();
    }
}
