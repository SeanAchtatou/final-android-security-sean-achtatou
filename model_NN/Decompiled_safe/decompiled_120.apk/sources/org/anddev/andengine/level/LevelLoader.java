package org.anddev.andengine.level;

import android.content.Context;
import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.HashMap;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import org.anddev.andengine.level.util.constants.LevelConstants;
import org.anddev.andengine.util.Debug;
import org.anddev.andengine.util.StreamUtils;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

public class LevelLoader implements LevelConstants {
    private String mAssetBasePath;
    private IEntityLoader mDefaultEntityLoader;
    private final HashMap mEntityLoaders;

    public interface IEntityLoader {
        void onLoadEntity(String str, Attributes attributes);
    }

    public LevelLoader() {
        this("");
    }

    public LevelLoader(String str) {
        this.mEntityLoaders = new HashMap();
        setAssetBasePath(str);
    }

    public IEntityLoader getDefaultEntityLoader() {
        return this.mDefaultEntityLoader;
    }

    public void setDefaultEntityLoader(IEntityLoader iEntityLoader) {
        this.mDefaultEntityLoader = iEntityLoader;
    }

    public void setAssetBasePath(String str) {
        if (str.endsWith("/") || str.length() == 0) {
            this.mAssetBasePath = str;
            return;
        }
        throw new IllegalStateException("pAssetBasePath must end with '/' or be lenght zero.");
    }

    /* access modifiers changed from: protected */
    public void onAfterLoadLevel() {
    }

    /* access modifiers changed from: protected */
    public void onBeforeLoadLevel() {
    }

    public void registerEntityLoader(String str, IEntityLoader iEntityLoader) {
        this.mEntityLoaders.put(str, iEntityLoader);
    }

    public void registerEntityLoader(String[] strArr, IEntityLoader iEntityLoader) {
        HashMap hashMap = this.mEntityLoaders;
        for (int length = strArr.length - 1; length >= 0; length--) {
            hashMap.put(strArr[length], iEntityLoader);
        }
    }

    public void loadLevelFromAsset(Context context, String str) {
        loadLevelFromStream(context.getAssets().open(String.valueOf(this.mAssetBasePath) + str));
    }

    public void loadLevelFromResource(Context context, int i) {
        loadLevelFromStream(context.getResources().openRawResource(i));
    }

    public void loadLevelFromStream(InputStream inputStream) {
        try {
            XMLReader xMLReader = SAXParserFactory.newInstance().newSAXParser().getXMLReader();
            onBeforeLoadLevel();
            xMLReader.setContentHandler(new LevelParser(this.mDefaultEntityLoader, this.mEntityLoaders));
            xMLReader.parse(new InputSource(new BufferedInputStream(inputStream)));
            onAfterLoadLevel();
        } catch (SAXException e) {
            Debug.e(e);
        } catch (ParserConfigurationException e2) {
            Debug.e(e2);
        } finally {
            StreamUtils.close(inputStream);
        }
    }
}
