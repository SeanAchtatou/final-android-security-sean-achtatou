package org.anddev.andengine.level;

import java.util.HashMap;
import org.anddev.andengine.level.LevelLoader;
import org.anddev.andengine.level.util.constants.LevelConstants;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

public class LevelParser extends DefaultHandler implements LevelConstants {
    private final LevelLoader.IEntityLoader mDefaultEntityLoader;
    private final HashMap mEntityLoaders;

    public LevelParser(LevelLoader.IEntityLoader iEntityLoader, HashMap hashMap) {
        this.mDefaultEntityLoader = iEntityLoader;
        this.mEntityLoaders = hashMap;
    }

    public void startElement(String str, String str2, String str3, Attributes attributes) {
        LevelLoader.IEntityLoader iEntityLoader = (LevelLoader.IEntityLoader) this.mEntityLoaders.get(str2);
        if (iEntityLoader != null) {
            iEntityLoader.onLoadEntity(str2, attributes);
        } else if (this.mDefaultEntityLoader != null) {
            this.mDefaultEntityLoader.onLoadEntity(str2, attributes);
        } else {
            throw new IllegalArgumentException("Unexpected tag: '" + str2 + "'.");
        }
    }
}
