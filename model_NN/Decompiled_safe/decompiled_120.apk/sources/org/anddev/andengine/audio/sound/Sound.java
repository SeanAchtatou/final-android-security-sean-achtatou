package org.anddev.andengine.audio.sound;

import org.anddev.andengine.audio.BaseAudioEntity;

public class Sound extends BaseAudioEntity {
    private int mLoopCount = 0;
    private float mRate = 1.0f;
    private final int mSoundID;
    private int mStreamID = 0;

    Sound(SoundManager soundManager, int i) {
        super(soundManager);
        this.mSoundID = i;
    }

    public void setLoopCount(int i) {
        this.mLoopCount = i;
        if (this.mStreamID != 0) {
            getAudioManager().getSoundPool().setLoop(this.mStreamID, i);
        }
    }

    public void setRate(float f) {
        this.mRate = f;
        if (this.mStreamID != 0) {
            getAudioManager().getSoundPool().setRate(this.mStreamID, f);
        }
    }

    /* access modifiers changed from: protected */
    public SoundManager getAudioManager() {
        return (SoundManager) super.getAudioManager();
    }

    public void play() {
        float masterVolume = getMasterVolume();
        this.mStreamID = getAudioManager().getSoundPool().play(this.mSoundID, this.mLeftVolume * masterVolume, this.mRightVolume * masterVolume, 1, this.mLoopCount, this.mRate);
    }

    public void stop() {
        if (this.mStreamID != 0) {
            getAudioManager().getSoundPool().stop(this.mStreamID);
        }
    }

    public void resume() {
        if (this.mStreamID != 0) {
            getAudioManager().getSoundPool().resume(this.mStreamID);
        }
    }

    public void pause() {
        if (this.mStreamID != 0) {
            getAudioManager().getSoundPool().pause(this.mStreamID);
        }
    }

    public void release() {
    }

    public void setLooping(boolean z) {
        setLoopCount(z ? -1 : 0);
    }

    public void setVolume(float f, float f2) {
        super.setVolume(f, f2);
        if (this.mStreamID != 0) {
            float masterVolume = getMasterVolume();
            getAudioManager().getSoundPool().setVolume(this.mStreamID, this.mLeftVolume * masterVolume, masterVolume * this.mRightVolume);
        }
    }

    public void onMasterVolumeChanged(float f) {
        setVolume(this.mLeftVolume, this.mRightVolume);
    }
}
