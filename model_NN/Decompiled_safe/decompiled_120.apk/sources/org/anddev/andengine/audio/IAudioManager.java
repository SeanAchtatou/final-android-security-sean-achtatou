package org.anddev.andengine.audio;

public interface IAudioManager {
    void add(IAudioEntity iAudioEntity);

    float getMasterVolume();

    void releaseAll();

    void setMasterVolume(float f);
}
