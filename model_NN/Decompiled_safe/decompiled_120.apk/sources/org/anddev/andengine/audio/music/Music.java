package org.anddev.andengine.audio.music;

import android.media.MediaPlayer;
import org.anddev.andengine.audio.BaseAudioEntity;

public class Music extends BaseAudioEntity {
    private final MediaPlayer mMediaPlayer;

    Music(MusicManager musicManager, MediaPlayer mediaPlayer) {
        super(musicManager);
        this.mMediaPlayer = mediaPlayer;
    }

    public boolean isPlaying() {
        return this.mMediaPlayer.isPlaying();
    }

    public MediaPlayer getMediaPlayer() {
        return this.mMediaPlayer;
    }

    /* access modifiers changed from: protected */
    public MusicManager getAudioManager() {
        return (MusicManager) super.getAudioManager();
    }

    public void play() {
        this.mMediaPlayer.start();
    }

    public void stop() {
        this.mMediaPlayer.stop();
    }

    public void resume() {
        this.mMediaPlayer.start();
    }

    public void pause() {
        this.mMediaPlayer.pause();
    }

    public void release() {
        this.mMediaPlayer.release();
    }

    public void setLooping(boolean z) {
        this.mMediaPlayer.setLooping(z);
    }

    public void setVolume(float f, float f2) {
        super.setVolume(f, f2);
        float masterVolume = getAudioManager().getMasterVolume();
        this.mMediaPlayer.setVolume(f * masterVolume, masterVolume * f2);
    }

    public void onMasterVolumeChanged(float f) {
        setVolume(this.mLeftVolume, this.mRightVolume);
    }

    public void seekTo(int i) {
        this.mMediaPlayer.seekTo(i);
    }

    public void setOnCompletionListener(MediaPlayer.OnCompletionListener onCompletionListener) {
        this.mMediaPlayer.setOnCompletionListener(onCompletionListener);
    }
}
