package org.anddev.andengine.audio.sound;

import android.content.Context;
import java.io.FileDescriptor;

public class SoundFactory {
    private static String sAssetBasePath = "";

    public static void setAssetBasePath(String str) {
        if (str.endsWith("/") || str.length() == 0) {
            sAssetBasePath = str;
            return;
        }
        throw new IllegalStateException("pAssetBasePath must end with '/' or be lenght zero.");
    }

    public static void reset() {
        setAssetBasePath("");
    }

    public static Sound createSoundFromPath(SoundManager soundManager, String str) {
        Sound sound = new Sound(soundManager, soundManager.getSoundPool().load(str, 1));
        soundManager.add(sound);
        return sound;
    }

    public static Sound createSoundFromAsset(SoundManager soundManager, Context context, String str) {
        Sound sound = new Sound(soundManager, soundManager.getSoundPool().load(context.getAssets().openFd(String.valueOf(sAssetBasePath) + str), 1));
        soundManager.add(sound);
        return sound;
    }

    public static Sound createSoundFromResource(SoundManager soundManager, Context context, int i) {
        Sound sound = new Sound(soundManager, soundManager.getSoundPool().load(context, i, 1));
        soundManager.add(sound);
        return sound;
    }

    public static Sound createSoundFromFileDescriptor(SoundManager soundManager, FileDescriptor fileDescriptor, long j, long j2) {
        Sound sound = new Sound(soundManager, soundManager.getSoundPool().load(fileDescriptor, j, j2, 1));
        soundManager.add(sound);
        return sound;
    }
}
