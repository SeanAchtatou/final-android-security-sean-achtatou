package org.anddev.andengine.audio;

public abstract class BaseAudioEntity implements IAudioEntity {
    private final IAudioManager mAudioManager;
    protected float mLeftVolume = 1.0f;
    protected float mRightVolume = 1.0f;

    public BaseAudioEntity(IAudioManager iAudioManager) {
        this.mAudioManager = iAudioManager;
    }

    /* access modifiers changed from: protected */
    public IAudioManager getAudioManager() {
        return this.mAudioManager;
    }

    public float getActualLeftVolume() {
        return this.mLeftVolume * getMasterVolume();
    }

    public float getActualRightVolume() {
        return this.mRightVolume * getMasterVolume();
    }

    /* access modifiers changed from: protected */
    public float getMasterVolume() {
        return this.mAudioManager.getMasterVolume();
    }

    public float getVolume() {
        return (this.mLeftVolume + this.mRightVolume) * 0.5f;
    }

    public float getLeftVolume() {
        return this.mLeftVolume;
    }

    public float getRightVolume() {
        return this.mRightVolume;
    }

    public final void setVolume(float f) {
        setVolume(f, f);
    }

    public void setVolume(float f, float f2) {
        this.mLeftVolume = f;
        this.mRightVolume = f2;
    }
}
