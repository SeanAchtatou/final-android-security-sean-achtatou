package org.anddev.andengine.audio;

import java.util.ArrayList;

public abstract class BaseAudioManager implements IAudioManager {
    protected final ArrayList mAudioEntities = new ArrayList();
    protected float mMasterVolume = 1.0f;

    public float getMasterVolume() {
        return this.mMasterVolume;
    }

    public void setMasterVolume(float f) {
        this.mMasterVolume = f;
        ArrayList arrayList = this.mAudioEntities;
        for (int size = arrayList.size() - 1; size >= 0; size--) {
            ((IAudioEntity) arrayList.get(size)).onMasterVolumeChanged(f);
        }
    }

    public void add(IAudioEntity iAudioEntity) {
        this.mAudioEntities.add(iAudioEntity);
    }

    public void releaseAll() {
        ArrayList arrayList = this.mAudioEntities;
        for (int size = arrayList.size() - 1; size >= 0; size--) {
            IAudioEntity iAudioEntity = (IAudioEntity) arrayList.get(size);
            iAudioEntity.stop();
            iAudioEntity.release();
        }
    }
}
