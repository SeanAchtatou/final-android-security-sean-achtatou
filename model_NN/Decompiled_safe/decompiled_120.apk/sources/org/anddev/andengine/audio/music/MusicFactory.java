package org.anddev.andengine.audio.music;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import java.io.File;
import java.io.FileInputStream;

public class MusicFactory {
    private static String sAssetBasePath = "";

    public static void setAssetBasePath(String str) {
        if (str.endsWith("/") || str.length() == 0) {
            sAssetBasePath = str;
            return;
        }
        throw new IllegalStateException("pAssetBasePath must end with '/' or be lenght zero.");
    }

    public static void reset() {
        setAssetBasePath("");
    }

    public static Music createMusicFromFile(MusicManager musicManager, File file) {
        MediaPlayer mediaPlayer = new MediaPlayer();
        mediaPlayer.setDataSource(new FileInputStream(file).getFD());
        mediaPlayer.prepare();
        Music music = new Music(musicManager, mediaPlayer);
        musicManager.add(music);
        return music;
    }

    public static Music createMusicFromAsset(MusicManager musicManager, Context context, String str) {
        MediaPlayer mediaPlayer = new MediaPlayer();
        AssetFileDescriptor openFd = context.getAssets().openFd(String.valueOf(sAssetBasePath) + str);
        mediaPlayer.setDataSource(openFd.getFileDescriptor(), openFd.getStartOffset(), openFd.getLength());
        mediaPlayer.prepare();
        Music music = new Music(musicManager, mediaPlayer);
        musicManager.add(music);
        return music;
    }

    public static Music createMusicFromResource(MusicManager musicManager, Context context, int i) {
        MediaPlayer create = MediaPlayer.create(context, i);
        create.prepare();
        Music music = new Music(musicManager, create);
        musicManager.add(music);
        return music;
    }
}
