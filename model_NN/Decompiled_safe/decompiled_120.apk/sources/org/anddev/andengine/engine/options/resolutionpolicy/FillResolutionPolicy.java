package org.anddev.andengine.engine.options.resolutionpolicy;

import android.view.View;
import org.anddev.andengine.opengl.view.RenderSurfaceView;

public class FillResolutionPolicy extends BaseResolutionPolicy {
    public void onMeasure(RenderSurfaceView renderSurfaceView, int i, int i2) {
        BaseResolutionPolicy.throwOnNotMeasureSpecEXACTLY(i, i2);
        renderSurfaceView.setMeasuredDimensionProxy(View.MeasureSpec.getSize(i), View.MeasureSpec.getSize(i2));
    }
}
