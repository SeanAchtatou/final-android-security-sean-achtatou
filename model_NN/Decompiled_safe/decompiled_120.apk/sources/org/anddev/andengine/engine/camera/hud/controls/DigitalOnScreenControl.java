package org.anddev.andengine.engine.camera.hud.controls;

import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.engine.camera.hud.controls.BaseOnScreenControl;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.util.MathUtils;

public class DigitalOnScreenControl extends BaseOnScreenControl {
    private static final float ANGLE_DELTA = 22.5f;
    private static final float EXTENT_DIAGONAL = 0.354f;
    private static final float EXTENT_SIDE = 0.5f;
    private boolean mAllowDiagonal;

    public DigitalOnScreenControl(float f, float f2, Camera camera, TextureRegion textureRegion, TextureRegion textureRegion2, float f3, BaseOnScreenControl.IOnScreenControlListener iOnScreenControlListener) {
        this(f, f2, camera, textureRegion, textureRegion2, f3, false, iOnScreenControlListener);
    }

    public DigitalOnScreenControl(float f, float f2, Camera camera, TextureRegion textureRegion, TextureRegion textureRegion2, float f3, boolean z, BaseOnScreenControl.IOnScreenControlListener iOnScreenControlListener) {
        super(f, f2, camera, textureRegion, textureRegion2, f3, iOnScreenControlListener);
        this.mAllowDiagonal = z;
    }

    public boolean isAllowDiagonal() {
        return this.mAllowDiagonal;
    }

    public void setAllowDiagonal(boolean z) {
        this.mAllowDiagonal = z;
    }

    /* access modifiers changed from: protected */
    public void onUpdateControlKnob(float f, float f2) {
        if (f == 0.0f && f2 == 0.0f) {
            super.onUpdateControlKnob(0.0f, 0.0f);
        } else if (this.mAllowDiagonal) {
            float radToDeg = MathUtils.radToDeg(MathUtils.atan2(f2, f)) + 180.0f;
            if (testDiagonalAngle(0.0f, radToDeg) || testDiagonalAngle(360.0f, radToDeg)) {
                super.onUpdateControlKnob(-0.5f, 0.0f);
            } else if (testDiagonalAngle(45.0f, radToDeg)) {
                super.onUpdateControlKnob(-0.354f, -0.354f);
            } else if (testDiagonalAngle(90.0f, radToDeg)) {
                super.onUpdateControlKnob(0.0f, -0.5f);
            } else if (testDiagonalAngle(135.0f, radToDeg)) {
                super.onUpdateControlKnob(EXTENT_DIAGONAL, -0.354f);
            } else if (testDiagonalAngle(180.0f, radToDeg)) {
                super.onUpdateControlKnob(EXTENT_SIDE, 0.0f);
            } else if (testDiagonalAngle(225.0f, radToDeg)) {
                super.onUpdateControlKnob(EXTENT_DIAGONAL, EXTENT_DIAGONAL);
            } else if (testDiagonalAngle(270.0f, radToDeg)) {
                super.onUpdateControlKnob(0.0f, EXTENT_SIDE);
            } else if (testDiagonalAngle(315.0f, radToDeg)) {
                super.onUpdateControlKnob(-0.354f, EXTENT_DIAGONAL);
            } else {
                super.onUpdateControlKnob(0.0f, 0.0f);
            }
        } else if (Math.abs(f) > Math.abs(f2)) {
            if (f > 0.0f) {
                super.onUpdateControlKnob(EXTENT_SIDE, 0.0f);
            } else if (f < 0.0f) {
                super.onUpdateControlKnob(-0.5f, 0.0f);
            } else if (f == 0.0f) {
                super.onUpdateControlKnob(0.0f, 0.0f);
            }
        } else if (f2 > 0.0f) {
            super.onUpdateControlKnob(0.0f, EXTENT_SIDE);
        } else if (f2 < 0.0f) {
            super.onUpdateControlKnob(0.0f, -0.5f);
        } else if (f2 == 0.0f) {
            super.onUpdateControlKnob(0.0f, 0.0f);
        }
    }

    private boolean testDiagonalAngle(float f, float f2) {
        return f2 > f - ANGLE_DELTA && f2 < f + ANGLE_DELTA;
    }
}
