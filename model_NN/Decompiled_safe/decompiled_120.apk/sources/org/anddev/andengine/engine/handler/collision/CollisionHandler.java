package org.anddev.andengine.engine.handler.collision;

import java.util.ArrayList;
import org.anddev.andengine.engine.handler.IUpdateHandler;
import org.anddev.andengine.entity.shape.IShape;
import org.anddev.andengine.util.ListUtils;

public class CollisionHandler implements IUpdateHandler {
    private final IShape mCheckShape;
    private final ICollisionCallback mCollisionCallback;
    private final ArrayList mTargetStaticEntities;

    public CollisionHandler(ICollisionCallback iCollisionCallback, IShape iShape, IShape iShape2) {
        this(iCollisionCallback, iShape, ListUtils.toList(iShape2));
    }

    public CollisionHandler(ICollisionCallback iCollisionCallback, IShape iShape, ArrayList arrayList) {
        if (iCollisionCallback == null) {
            throw new IllegalArgumentException("pCollisionCallback must not be null!");
        } else if (iShape == null) {
            throw new IllegalArgumentException("pCheckShape must not be null!");
        } else if (arrayList == null) {
            throw new IllegalArgumentException("pTargetStaticEntities must not be null!");
        } else {
            this.mCollisionCallback = iCollisionCallback;
            this.mCheckShape = iShape;
            this.mTargetStaticEntities = arrayList;
        }
    }

    public void onUpdate(float f) {
        IShape iShape = this.mCheckShape;
        ArrayList arrayList = this.mTargetStaticEntities;
        int size = arrayList.size();
        int i = 0;
        while (i < size) {
            if (!iShape.collidesWith((IShape) arrayList.get(i)) || this.mCollisionCallback.onCollision(iShape, (IShape) arrayList.get(i))) {
                i++;
            } else {
                return;
            }
        }
    }

    public void reset() {
    }
}
