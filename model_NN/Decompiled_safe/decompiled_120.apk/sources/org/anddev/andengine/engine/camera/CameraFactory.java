package org.anddev.andengine.engine.camera;

import android.content.Context;
import android.util.DisplayMetrics;

public class CameraFactory {
    public static Camera createPixelPerfectCamera(Context context, float f, float f2) {
        DisplayMetrics displayMetrics = getDisplayMetrics(context);
        float f3 = (float) displayMetrics.widthPixels;
        float f4 = (float) displayMetrics.heightPixels;
        return new Camera(f - (f3 * 0.5f), f2 - (0.5f * f4), f3, f4);
    }

    private static DisplayMetrics getDisplayMetrics(Context context) {
        return context.getResources().getDisplayMetrics();
    }
}
