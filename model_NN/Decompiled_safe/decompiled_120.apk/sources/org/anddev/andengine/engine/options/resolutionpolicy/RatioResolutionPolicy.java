package org.anddev.andengine.engine.options.resolutionpolicy;

import android.view.View;
import org.anddev.andengine.opengl.view.RenderSurfaceView;

public class RatioResolutionPolicy extends BaseResolutionPolicy {
    private final float mRatio;

    public RatioResolutionPolicy(float f) {
        this.mRatio = f;
    }

    public RatioResolutionPolicy(float f, float f2) {
        this.mRatio = f / f2;
    }

    public void onMeasure(RenderSurfaceView renderSurfaceView, int i, int i2) {
        BaseResolutionPolicy.throwOnNotMeasureSpecEXACTLY(i, i2);
        int size = View.MeasureSpec.getSize(i);
        int size2 = View.MeasureSpec.getSize(i2);
        float f = this.mRatio;
        if (((float) size) / ((float) size2) < f) {
            size2 = Math.round(((float) size) / f);
        } else {
            size = Math.round(((float) size2) * f);
        }
        renderSurfaceView.setMeasuredDimensionProxy(size, size2);
    }
}
