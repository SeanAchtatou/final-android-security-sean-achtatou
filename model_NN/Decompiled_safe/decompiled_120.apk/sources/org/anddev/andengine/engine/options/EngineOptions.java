package org.anddev.andengine.engine.options;

import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.engine.options.resolutionpolicy.IResolutionPolicy;

public class EngineOptions {
    private final Camera mCamera;
    private final boolean mFullscreen;
    private boolean mNeedsMusic;
    private boolean mNeedsSound;
    private final RenderOptions mRenderOptions = new RenderOptions();
    private final IResolutionPolicy mResolutionPolicy;
    private final ScreenOrientation mScreenOrientation;
    private final TouchOptions mTouchOptions = new TouchOptions();
    private int mUpdateThreadPriority = 0;
    private WakeLockOptions mWakeLockOptions = WakeLockOptions.SCREEN_BRIGHT;

    public enum ScreenOrientation {
        LANDSCAPE,
        PORTRAIT
    }

    public EngineOptions(boolean z, ScreenOrientation screenOrientation, IResolutionPolicy iResolutionPolicy, Camera camera) {
        this.mFullscreen = z;
        this.mScreenOrientation = screenOrientation;
        this.mResolutionPolicy = iResolutionPolicy;
        this.mCamera = camera;
    }

    public TouchOptions getTouchOptions() {
        return this.mTouchOptions;
    }

    public RenderOptions getRenderOptions() {
        return this.mRenderOptions;
    }

    public boolean isFullscreen() {
        return this.mFullscreen;
    }

    public ScreenOrientation getScreenOrientation() {
        return this.mScreenOrientation;
    }

    public IResolutionPolicy getResolutionPolicy() {
        return this.mResolutionPolicy;
    }

    public Camera getCamera() {
        return this.mCamera;
    }

    public int getUpdateThreadPriority() {
        return this.mUpdateThreadPriority;
    }

    public void setUpdateThreadPriority(int i) {
        this.mUpdateThreadPriority = i;
    }

    public boolean needsSound() {
        return this.mNeedsSound;
    }

    public EngineOptions setNeedsSound(boolean z) {
        this.mNeedsSound = z;
        return this;
    }

    public boolean needsMusic() {
        return this.mNeedsMusic;
    }

    public EngineOptions setNeedsMusic(boolean z) {
        this.mNeedsMusic = z;
        return this;
    }

    public WakeLockOptions getWakeLockOptions() {
        return this.mWakeLockOptions;
    }

    public EngineOptions setWakeLockOptions(WakeLockOptions wakeLockOptions) {
        this.mWakeLockOptions = wakeLockOptions;
        return this;
    }
}
