package org.anddev.andengine.engine.camera;

import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.util.MathUtils;

public class ZoomCamera extends BoundCamera {
    protected float mZoomFactor = 1.0f;

    public ZoomCamera(float f, float f2, float f3, float f4) {
        super(f, f2, f3, f4);
    }

    public float getZoomFactor() {
        return this.mZoomFactor;
    }

    public void setZoomFactor(float f) {
        this.mZoomFactor = f;
        if (this.mBoundsEnabled) {
            ensureInBounds();
        }
    }

    public float getMinX() {
        if (this.mZoomFactor == 1.0f) {
            return super.getMinX();
        }
        float centerX = getCenterX();
        return centerX - ((centerX - super.getMinX()) / this.mZoomFactor);
    }

    public float getMaxX() {
        if (this.mZoomFactor == 1.0f) {
            return super.getMaxX();
        }
        float centerX = getCenterX();
        return centerX + ((super.getMaxX() - centerX) / this.mZoomFactor);
    }

    public float getMinY() {
        if (this.mZoomFactor == 1.0f) {
            return super.getMinY();
        }
        float centerY = getCenterY();
        return centerY - ((centerY - super.getMinY()) / this.mZoomFactor);
    }

    public float getMaxY() {
        if (this.mZoomFactor == 1.0f) {
            return super.getMaxY();
        }
        float centerY = getCenterY();
        return centerY + ((super.getMaxY() - centerY) / this.mZoomFactor);
    }

    public float getWidth() {
        return super.getWidth() / this.mZoomFactor;
    }

    public float getHeight() {
        return super.getHeight() / this.mZoomFactor;
    }

    /* access modifiers changed from: protected */
    public void applySceneToCameraSceneOffset(TouchEvent touchEvent) {
        float f = this.mZoomFactor;
        if (f != 1.0f) {
            float centerX = getCenterX();
            float centerY = getCenterY();
            VERTICES_TOUCH_TMP[0] = touchEvent.getX();
            VERTICES_TOUCH_TMP[1] = touchEvent.getY();
            MathUtils.scaleAroundCenter(VERTICES_TOUCH_TMP, f, f, centerX, centerY);
            touchEvent.set(VERTICES_TOUCH_TMP[0], VERTICES_TOUCH_TMP[1]);
        }
        super.applySceneToCameraSceneOffset(touchEvent);
    }

    /* access modifiers changed from: protected */
    public void unapplySceneToCameraSceneOffset(TouchEvent touchEvent) {
        super.unapplySceneToCameraSceneOffset(touchEvent);
        float f = this.mZoomFactor;
        if (f != 1.0f) {
            float centerX = getCenterX();
            float centerY = getCenterY();
            VERTICES_TOUCH_TMP[0] = touchEvent.getX();
            VERTICES_TOUCH_TMP[1] = touchEvent.getY();
            MathUtils.revertScaleAroundCenter(VERTICES_TOUCH_TMP, f, f, centerX, centerY);
            touchEvent.set(VERTICES_TOUCH_TMP[0], VERTICES_TOUCH_TMP[1]);
        }
    }
}
