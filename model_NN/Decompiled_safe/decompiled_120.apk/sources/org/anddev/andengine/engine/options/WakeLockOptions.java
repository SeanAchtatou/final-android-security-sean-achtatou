package org.anddev.andengine.engine.options;

public enum WakeLockOptions {
    BRIGHT(26),
    SCREEN_BRIGHT(10),
    SCREEN_DIM(6),
    SCREEN_ON(-1);
    
    private final int mFlag;

    private WakeLockOptions(int i) {
        this.mFlag = i;
    }

    public final int getFlag() {
        return this.mFlag;
    }
}
