package org.anddev.andengine.engine.camera;

public class BoundCamera extends Camera {
    private float mBoundsCenterX;
    private float mBoundsCenterY;
    protected boolean mBoundsEnabled;
    private float mBoundsHeight;
    private float mBoundsMaxX;
    private float mBoundsMaxY;
    private float mBoundsMinX;
    private float mBoundsMinY;
    private float mBoundsWidth;

    public BoundCamera(float f, float f2, float f3, float f4) {
        super(f, f2, f3, f4);
    }

    public BoundCamera(float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8) {
        super(f, f2, f3, f4);
        setBounds(f5, f6, f7, f8);
        this.mBoundsEnabled = true;
    }

    public boolean isBoundsEnabled() {
        return this.mBoundsEnabled;
    }

    public void setBoundsEnabled(boolean z) {
        this.mBoundsEnabled = z;
    }

    public void setBounds(float f, float f2, float f3, float f4) {
        this.mBoundsMinX = f;
        this.mBoundsMaxX = f2;
        this.mBoundsMinY = f3;
        this.mBoundsMaxY = f4;
        this.mBoundsWidth = this.mBoundsMaxX - this.mBoundsMinX;
        this.mBoundsHeight = this.mBoundsMaxY - this.mBoundsMinY;
        this.mBoundsCenterX = this.mBoundsMinX + (this.mBoundsWidth * 0.5f);
        this.mBoundsCenterY = this.mBoundsMinY + (this.mBoundsHeight * 0.5f);
    }

    public float getBoundsWidth() {
        return this.mBoundsWidth;
    }

    public float getBoundsHeight() {
        return this.mBoundsHeight;
    }

    public void setCenter(float f, float f2) {
        super.setCenter(f, f2);
        if (this.mBoundsEnabled) {
            ensureInBounds();
        }
    }

    /* access modifiers changed from: protected */
    public void ensureInBounds() {
        super.setCenter(determineBoundedX(), determineBoundedY());
    }

    private float determineBoundedX() {
        boolean z;
        if (this.mBoundsWidth < getWidth()) {
            return this.mBoundsCenterX;
        }
        float centerX = getCenterX();
        float minX = this.mBoundsMinX - getMinX();
        boolean z2 = minX > 0.0f;
        float maxX = getMaxX() - this.mBoundsMaxX;
        if (maxX > 0.0f) {
            z = true;
        } else {
            z = false;
        }
        if (z2) {
            if (z) {
                return (centerX - maxX) + minX;
            }
            return centerX + minX;
        } else if (z) {
            return centerX - maxX;
        } else {
            return centerX;
        }
    }

    private float determineBoundedY() {
        boolean z;
        if (this.mBoundsHeight < getHeight()) {
            return this.mBoundsCenterY;
        }
        float centerY = getCenterY();
        float minY = this.mBoundsMinY - getMinY();
        boolean z2 = minY > 0.0f;
        float maxY = getMaxY() - this.mBoundsMaxY;
        if (maxY > 0.0f) {
            z = true;
        } else {
            z = false;
        }
        if (z2) {
            if (z) {
                return (centerY - maxY) + minY;
            }
            return centerY + minY;
        } else if (z) {
            return centerY - maxY;
        } else {
            return centerY;
        }
    }
}
