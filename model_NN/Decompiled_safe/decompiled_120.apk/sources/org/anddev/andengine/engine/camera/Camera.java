package org.anddev.andengine.engine.camera;

import javax.microedition.khronos.opengles.GL10;
import org.anddev.andengine.collision.RectangularShapeCollisionChecker;
import org.anddev.andengine.engine.camera.hud.HUD;
import org.anddev.andengine.engine.handler.IUpdateHandler;
import org.anddev.andengine.entity.IEntity;
import org.anddev.andengine.entity.primitive.Line;
import org.anddev.andengine.entity.shape.RectangularShape;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.opengl.util.GLHelper;
import org.anddev.andengine.util.MathUtils;

public class Camera implements IUpdateHandler {
    protected static final float[] VERTICES_TOUCH_TMP = new float[2];
    protected float mCameraSceneRotation = 0.0f;
    private IEntity mChaseEntity;
    private float mFarZ = 1.0f;
    private HUD mHUD;
    private float mMaxX;
    private float mMaxY;
    private float mMinX;
    private float mMinY;
    private float mNearZ = -1.0f;
    protected float mRotation = 0.0f;
    protected int mSurfaceHeight;
    protected int mSurfaceWidth;
    protected int mSurfaceX;
    protected int mSurfaceY;

    public Camera(float f, float f2, float f3, float f4) {
        this.mMinX = f;
        this.mMaxX = f + f3;
        this.mMinY = f2;
        this.mMaxY = f2 + f4;
    }

    public float getMinX() {
        return this.mMinX;
    }

    public float getMaxX() {
        return this.mMaxX;
    }

    public float getMinY() {
        return this.mMinY;
    }

    public float getMaxY() {
        return this.mMaxY;
    }

    public float getNearZClippingPlane() {
        return this.mNearZ;
    }

    public float getFarZClippingPlane() {
        return this.mFarZ;
    }

    public void setNearZClippingPlane(float f) {
        this.mNearZ = f;
    }

    public void setFarZClippingPlane(float f) {
        this.mFarZ = f;
    }

    public void setZClippingPlanes(float f, float f2) {
        this.mNearZ = f;
        this.mFarZ = f2;
    }

    public float getWidth() {
        return this.mMaxX - this.mMinX;
    }

    public float getHeight() {
        return this.mMaxY - this.mMinY;
    }

    public float getWidthRaw() {
        return this.mMaxX - this.mMinX;
    }

    public float getHeightRaw() {
        return this.mMaxY - this.mMinY;
    }

    public float getCenterX() {
        float f = this.mMinX;
        return f + ((this.mMaxX - f) * 0.5f);
    }

    public float getCenterY() {
        float f = this.mMinY;
        return f + ((this.mMaxY - f) * 0.5f);
    }

    public void setCenter(float f, float f2) {
        float centerX = f - getCenterX();
        float centerY = f2 - getCenterY();
        this.mMinX += centerX;
        this.mMaxX = centerX + this.mMaxX;
        this.mMinY += centerY;
        this.mMaxY += centerY;
    }

    public void offsetCenter(float f, float f2) {
        setCenter(getCenterX() + f, getCenterY() + f2);
    }

    public HUD getHUD() {
        return this.mHUD;
    }

    public void setHUD(HUD hud) {
        this.mHUD = hud;
        hud.setCamera(this);
    }

    public boolean hasHUD() {
        return this.mHUD != null;
    }

    public void setChaseEntity(IEntity iEntity) {
        this.mChaseEntity = iEntity;
    }

    public float getRotation() {
        return this.mRotation;
    }

    public void setRotation(float f) {
        this.mRotation = f;
    }

    public float getCameraSceneRotation() {
        return this.mCameraSceneRotation;
    }

    public void setCameraSceneRotation(float f) {
        this.mCameraSceneRotation = f;
    }

    public int getSurfaceX() {
        return this.mSurfaceX;
    }

    public int getSurfaceY() {
        return this.mSurfaceY;
    }

    public int getSurfaceWidth() {
        return this.mSurfaceWidth;
    }

    public int getSurfaceHeight() {
        return this.mSurfaceHeight;
    }

    public void setSurfaceSize(int i, int i2, int i3, int i4) {
        this.mSurfaceX = i;
        this.mSurfaceY = i2;
        this.mSurfaceWidth = i3;
        this.mSurfaceHeight = i4;
    }

    public boolean isRotated() {
        return this.mRotation != 0.0f;
    }

    public void onUpdate(float f) {
        if (this.mHUD != null) {
            this.mHUD.onUpdate(f);
        }
        updateChaseEntity();
    }

    public void reset() {
    }

    public void onDrawHUD(GL10 gl10) {
        if (this.mHUD != null) {
            this.mHUD.onDraw(gl10, this);
        }
    }

    public void updateChaseEntity() {
        if (this.mChaseEntity != null) {
            float[] sceneCenterCoordinates = this.mChaseEntity.getSceneCenterCoordinates();
            setCenter(sceneCenterCoordinates[0], sceneCenterCoordinates[1]);
        }
    }

    public boolean isLineVisible(Line line) {
        return RectangularShapeCollisionChecker.isVisible(this, line);
    }

    public boolean isRectangularShapeVisible(RectangularShape rectangularShape) {
        return RectangularShapeCollisionChecker.isVisible(this, rectangularShape);
    }

    public void onApplySceneMatrix(GL10 gl10) {
        GLHelper.setProjectionIdentityMatrix(gl10);
        gl10.glOrthof(getMinX(), getMaxX(), getMaxY(), getMinY(), this.mNearZ, this.mFarZ);
        float f = this.mRotation;
        if (f != 0.0f) {
            applyRotation(gl10, getCenterX(), getCenterY(), f);
        }
    }

    public void onApplySceneBackgroundMatrix(GL10 gl10) {
        GLHelper.setProjectionIdentityMatrix(gl10);
        float widthRaw = getWidthRaw();
        float heightRaw = getHeightRaw();
        gl10.glOrthof(0.0f, widthRaw, heightRaw, 0.0f, this.mNearZ, this.mFarZ);
        float f = this.mRotation;
        if (f != 0.0f) {
            applyRotation(gl10, widthRaw * 0.5f, heightRaw * 0.5f, f);
        }
    }

    public void onApplyCameraSceneMatrix(GL10 gl10) {
        GLHelper.setProjectionIdentityMatrix(gl10);
        float widthRaw = getWidthRaw();
        float heightRaw = getHeightRaw();
        gl10.glOrthof(0.0f, widthRaw, heightRaw, 0.0f, this.mNearZ, this.mFarZ);
        float f = this.mCameraSceneRotation;
        if (f != 0.0f) {
            applyRotation(gl10, widthRaw * 0.5f, heightRaw * 0.5f, f);
        }
    }

    private void applyRotation(GL10 gl10, float f, float f2, float f3) {
        gl10.glTranslatef(f, f2, 0.0f);
        gl10.glRotatef(f3, 0.0f, 0.0f, 1.0f);
        gl10.glTranslatef(-f, -f2, 0.0f);
    }

    public void convertSceneToCameraSceneTouchEvent(TouchEvent touchEvent) {
        unapplySceneRotation(touchEvent);
        applySceneToCameraSceneOffset(touchEvent);
        applyCameraSceneRotation(touchEvent);
    }

    public void convertCameraSceneToSceneTouchEvent(TouchEvent touchEvent) {
        unapplyCameraSceneRotation(touchEvent);
        unapplySceneToCameraSceneOffset(touchEvent);
        applySceneRotation(touchEvent);
    }

    /* access modifiers changed from: protected */
    public void applySceneToCameraSceneOffset(TouchEvent touchEvent) {
        touchEvent.offset(-this.mMinX, -this.mMinY);
    }

    /* access modifiers changed from: protected */
    public void unapplySceneToCameraSceneOffset(TouchEvent touchEvent) {
        touchEvent.offset(this.mMinX, this.mMinY);
    }

    private void applySceneRotation(TouchEvent touchEvent) {
        float f = -this.mRotation;
        if (f != 0.0f) {
            VERTICES_TOUCH_TMP[0] = touchEvent.getX();
            VERTICES_TOUCH_TMP[1] = touchEvent.getY();
            MathUtils.rotateAroundCenter(VERTICES_TOUCH_TMP, f, getCenterX(), getCenterY());
            touchEvent.set(VERTICES_TOUCH_TMP[0], VERTICES_TOUCH_TMP[1]);
        }
    }

    private void unapplySceneRotation(TouchEvent touchEvent) {
        float f = this.mRotation;
        if (f != 0.0f) {
            VERTICES_TOUCH_TMP[0] = touchEvent.getX();
            VERTICES_TOUCH_TMP[1] = touchEvent.getY();
            MathUtils.revertRotateAroundCenter(VERTICES_TOUCH_TMP, f, getCenterX(), getCenterY());
            touchEvent.set(VERTICES_TOUCH_TMP[0], VERTICES_TOUCH_TMP[1]);
        }
    }

    private void applyCameraSceneRotation(TouchEvent touchEvent) {
        float f = -this.mCameraSceneRotation;
        if (f != 0.0f) {
            VERTICES_TOUCH_TMP[0] = touchEvent.getX();
            VERTICES_TOUCH_TMP[1] = touchEvent.getY();
            MathUtils.rotateAroundCenter(VERTICES_TOUCH_TMP, f, (this.mMaxX - this.mMinX) * 0.5f, (this.mMaxY - this.mMinY) * 0.5f);
            touchEvent.set(VERTICES_TOUCH_TMP[0], VERTICES_TOUCH_TMP[1]);
        }
    }

    private void unapplyCameraSceneRotation(TouchEvent touchEvent) {
        float f = -this.mCameraSceneRotation;
        if (f != 0.0f) {
            VERTICES_TOUCH_TMP[0] = touchEvent.getX();
            VERTICES_TOUCH_TMP[1] = touchEvent.getY();
            MathUtils.revertRotateAroundCenter(VERTICES_TOUCH_TMP, f, (this.mMaxX - this.mMinX) * 0.5f, (this.mMaxY - this.mMinY) * 0.5f);
            touchEvent.set(VERTICES_TOUCH_TMP[0], VERTICES_TOUCH_TMP[1]);
        }
    }

    public void convertSurfaceToSceneTouchEvent(TouchEvent touchEvent, int i, int i2) {
        float f;
        float f2;
        float f3 = this.mRotation;
        if (f3 == 0.0f) {
            float x = touchEvent.getX() / ((float) i);
            f = x;
            f2 = touchEvent.getY() / ((float) i2);
        } else if (f3 == 180.0f) {
            float y = 1.0f - (touchEvent.getY() / ((float) i2));
            f = 1.0f - (touchEvent.getX() / ((float) i));
            f2 = y;
        } else {
            VERTICES_TOUCH_TMP[0] = touchEvent.getX();
            VERTICES_TOUCH_TMP[1] = touchEvent.getY();
            MathUtils.rotateAroundCenter(VERTICES_TOUCH_TMP, f3, (float) (i / 2), (float) (i2 / 2));
            float f4 = VERTICES_TOUCH_TMP[0] / ((float) i);
            f = f4;
            f2 = VERTICES_TOUCH_TMP[1] / ((float) i2);
        }
        convertAxisAlignedSurfaceToSceneTouchEvent(touchEvent, f, f2);
    }

    private void convertAxisAlignedSurfaceToSceneTouchEvent(TouchEvent touchEvent, float f, float f2) {
        float minX = getMinX();
        float maxX = getMaxX();
        float minY = getMinY();
        touchEvent.set(minX + ((maxX - minX) * f), ((getMaxY() - minY) * f2) + minY);
    }
}
