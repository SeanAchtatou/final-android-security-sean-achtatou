package org.anddev.andengine.engine.camera.hud.controls;

import android.util.FloatMath;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.engine.camera.hud.controls.BaseOnScreenControl;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.input.touch.detector.ClickDetector;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.util.MathUtils;
import org.anddev.andengine.util.constants.TimeConstants;

public class AnalogOnScreenControl extends BaseOnScreenControl implements ClickDetector.IClickDetectorListener, TimeConstants {
    private final ClickDetector mClickDetector = new ClickDetector(this);

    public interface IAnalogOnScreenControlListener extends BaseOnScreenControl.IOnScreenControlListener {
        void onControlClick(AnalogOnScreenControl analogOnScreenControl);
    }

    public AnalogOnScreenControl(float f, float f2, Camera camera, TextureRegion textureRegion, TextureRegion textureRegion2, float f3, IAnalogOnScreenControlListener iAnalogOnScreenControlListener) {
        super(f, f2, camera, textureRegion, textureRegion2, f3, iAnalogOnScreenControlListener);
        this.mClickDetector.setEnabled(false);
    }

    public AnalogOnScreenControl(float f, float f2, Camera camera, TextureRegion textureRegion, TextureRegion textureRegion2, float f3, long j, IAnalogOnScreenControlListener iAnalogOnScreenControlListener) {
        super(f, f2, camera, textureRegion, textureRegion2, f3, iAnalogOnScreenControlListener);
        this.mClickDetector.setTriggerClickMaximumMilliseconds(j);
    }

    public IAnalogOnScreenControlListener getOnScreenControlListener() {
        return (IAnalogOnScreenControlListener) super.getOnScreenControlListener();
    }

    public void setOnControlClickEnabled(boolean z) {
        this.mClickDetector.setEnabled(z);
    }

    public void setOnControlClickMaximumMilliseconds(long j) {
        this.mClickDetector.setTriggerClickMaximumMilliseconds(j);
    }

    public void onClick(ClickDetector clickDetector, TouchEvent touchEvent) {
        getOnScreenControlListener().onControlClick(this);
    }

    /* access modifiers changed from: protected */
    public boolean onHandleControlBaseTouched(TouchEvent touchEvent, float f, float f2) {
        this.mClickDetector.onSceneTouchEvent(null, touchEvent);
        return super.onHandleControlBaseTouched(touchEvent, f, f2);
    }

    /* access modifiers changed from: protected */
    public void onUpdateControlKnob(float f, float f2) {
        if ((f * f) + (f2 * f2) <= 0.25f) {
            super.onUpdateControlKnob(f, f2);
            return;
        }
        float atan2 = MathUtils.atan2(f2, f);
        super.onUpdateControlKnob(FloatMath.cos(atan2) * 0.5f, FloatMath.sin(atan2) * 0.5f);
    }
}
