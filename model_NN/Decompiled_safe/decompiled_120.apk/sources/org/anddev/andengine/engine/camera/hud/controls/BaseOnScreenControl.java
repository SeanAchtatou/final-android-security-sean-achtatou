package org.anddev.andengine.engine.camera.hud.controls;

import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.engine.camera.hud.HUD;
import org.anddev.andengine.engine.handler.timer.ITimerCallback;
import org.anddev.andengine.engine.handler.timer.TimerHandler;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.util.MathUtils;

public abstract class BaseOnScreenControl extends HUD implements Scene.IOnSceneTouchListener {
    private static final int INVALID_POINTER_ID = -1;
    private int mActivePointerID = -1;
    private final Sprite mControlBase;
    private final Sprite mControlKnob;
    /* access modifiers changed from: private */
    public float mControlValueX;
    /* access modifiers changed from: private */
    public float mControlValueY;
    /* access modifiers changed from: private */
    public final IOnScreenControlListener mOnScreenControlListener;

    public interface IOnScreenControlListener {
        void onControlChange(BaseOnScreenControl baseOnScreenControl, float f, float f2);
    }

    public BaseOnScreenControl(float f, float f2, Camera camera, TextureRegion textureRegion, TextureRegion textureRegion2, float f3, IOnScreenControlListener iOnScreenControlListener) {
        setCamera(camera);
        this.mOnScreenControlListener = iOnScreenControlListener;
        this.mControlBase = new Sprite(f, f2, textureRegion) {
            public boolean onAreaTouched(TouchEvent touchEvent, float f, float f2) {
                return BaseOnScreenControl.this.onHandleControlBaseTouched(touchEvent, f, f2);
            }
        };
        this.mControlKnob = new Sprite(0.0f, 0.0f, textureRegion2);
        onHandleControlKnobReleased();
        setOnSceneTouchListener(this);
        registerTouchArea(this.mControlBase);
        registerUpdateHandler(new TimerHandler(f3, true, new ITimerCallback() {
            public void onTimePassed(TimerHandler timerHandler) {
                BaseOnScreenControl.this.mOnScreenControlListener.onControlChange(BaseOnScreenControl.this, BaseOnScreenControl.this.mControlValueX, BaseOnScreenControl.this.mControlValueY);
            }
        }));
        attachChild(this.mControlBase);
        attachChild(this.mControlKnob);
        setTouchAreaBindingEnabled(true);
    }

    public Sprite getControlBase() {
        return this.mControlBase;
    }

    public Sprite getControlKnob() {
        return this.mControlKnob;
    }

    public IOnScreenControlListener getOnScreenControlListener() {
        return this.mOnScreenControlListener;
    }

    public boolean onSceneTouchEvent(Scene scene, TouchEvent touchEvent) {
        if (touchEvent.getPointerID() != this.mActivePointerID) {
            return false;
        }
        onHandleControlBaseLeft();
        switch (touchEvent.getAction()) {
            case 1:
            case TouchEvent.ACTION_CANCEL /*3*/:
                this.mActivePointerID = -1;
                return false;
            case 2:
            default:
                return false;
        }
    }

    public void refreshControlKnobPosition() {
        onUpdateControlKnob(this.mControlValueX * 0.5f, this.mControlValueY * 0.5f);
    }

    /* access modifiers changed from: protected */
    public void onHandleControlBaseLeft() {
        onUpdateControlKnob(0.0f, 0.0f);
    }

    /* access modifiers changed from: protected */
    public void onHandleControlKnobReleased() {
        onUpdateControlKnob(0.0f, 0.0f);
    }

    /* access modifiers changed from: protected */
    public boolean onHandleControlBaseTouched(TouchEvent touchEvent, float f, float f2) {
        int pointerID = touchEvent.getPointerID();
        switch (touchEvent.getAction()) {
            case 0:
                if (this.mActivePointerID == -1) {
                    this.mActivePointerID = pointerID;
                    updateControlKnob(f, f2);
                    return true;
                }
                break;
            case 1:
            case TouchEvent.ACTION_CANCEL /*3*/:
                if (this.mActivePointerID == pointerID) {
                    this.mActivePointerID = -1;
                    onHandleControlKnobReleased();
                    return true;
                }
                break;
            case 2:
            default:
                if (this.mActivePointerID == pointerID) {
                    updateControlKnob(f, f2);
                    return true;
                }
                break;
        }
        return true;
    }

    private void updateControlKnob(float f, float f2) {
        Sprite sprite = this.mControlBase;
        onUpdateControlKnob((MathUtils.bringToBounds(0.0f, sprite.getWidth(), f) / sprite.getWidth()) - 0.5f, (MathUtils.bringToBounds(0.0f, sprite.getHeight(), f2) / sprite.getHeight()) - 0.5f);
    }

    /* access modifiers changed from: protected */
    public void onUpdateControlKnob(float f, float f2) {
        Sprite sprite = this.mControlBase;
        Sprite sprite2 = this.mControlKnob;
        this.mControlValueX = 2.0f * f;
        this.mControlValueY = 2.0f * f2;
        float[] sceneCenterCoordinates = sprite.getSceneCenterCoordinates();
        sprite2.setPosition((sceneCenterCoordinates[0] - (sprite2.getWidth() * 0.5f)) + (sprite.getWidthScaled() * f), (sprite.getHeightScaled() * f2) + (sceneCenterCoordinates[1] - (sprite2.getHeight() * 0.5f)));
    }
}
