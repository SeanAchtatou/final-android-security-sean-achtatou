package org.anddev.andengine.engine.handler;

import org.anddev.andengine.entity.IEntity;

public abstract class BaseEntityUpdateHandler implements IUpdateHandler {
    private final IEntity mEntity;

    /* access modifiers changed from: protected */
    public abstract void onUpdate(float f, IEntity iEntity);

    public BaseEntityUpdateHandler(IEntity iEntity) {
        this.mEntity = iEntity;
    }

    public final void onUpdate(float f) {
        onUpdate(f, this.mEntity);
    }

    public void reset() {
    }
}
