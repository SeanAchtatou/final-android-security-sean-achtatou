package org.anddev.andengine.engine.handler.physics;

import org.anddev.andengine.engine.handler.BaseEntityUpdateHandler;
import org.anddev.andengine.entity.IEntity;

public class PhysicsHandler extends BaseEntityUpdateHandler {
    protected float mAccelerationX = 0.0f;
    protected float mAccelerationY = 0.0f;
    protected float mAngularVelocity = 0.0f;
    private boolean mEnabled = true;
    protected float mVelocityX = 0.0f;
    protected float mVelocityY = 0.0f;

    public PhysicsHandler(IEntity iEntity) {
        super(iEntity);
    }

    public boolean isEnabled() {
        return this.mEnabled;
    }

    public void setEnabled(boolean z) {
        this.mEnabled = z;
    }

    public float getVelocityX() {
        return this.mVelocityX;
    }

    public float getVelocityY() {
        return this.mVelocityY;
    }

    public void setVelocityX(float f) {
        this.mVelocityX = f;
    }

    public void setVelocityY(float f) {
        this.mVelocityY = f;
    }

    public void setVelocity(float f) {
        this.mVelocityX = f;
        this.mVelocityY = f;
    }

    public void setVelocity(float f, float f2) {
        this.mVelocityX = f;
        this.mVelocityY = f2;
    }

    public float getAccelerationX() {
        return this.mAccelerationX;
    }

    public float getAccelerationY() {
        return this.mAccelerationY;
    }

    public void setAccelerationX(float f) {
        this.mAccelerationX = f;
    }

    public void setAccelerationY(float f) {
        this.mAccelerationY = f;
    }

    public void setAcceleration(float f, float f2) {
        this.mAccelerationX = f;
        this.mAccelerationY = f2;
    }

    public void setAcceleration(float f) {
        this.mAccelerationX = f;
        this.mAccelerationY = f;
    }

    public void accelerate(float f, float f2) {
        this.mAccelerationX += f;
        this.mAccelerationY += f2;
    }

    public float getAngularVelocity() {
        return this.mAngularVelocity;
    }

    public void setAngularVelocity(float f) {
        this.mAngularVelocity = f;
    }

    /* access modifiers changed from: protected */
    public void onUpdate(float f, IEntity iEntity) {
        if (this.mEnabled) {
            float f2 = this.mAccelerationX;
            float f3 = this.mAccelerationY;
            if (!(f2 == 0.0f && f3 == 0.0f)) {
                this.mVelocityX = (f2 * f) + this.mVelocityX;
                this.mVelocityY += f3 * f;
            }
            float f4 = this.mAngularVelocity;
            if (f4 != 0.0f) {
                iEntity.setRotation((f4 * f) + iEntity.getRotation());
            }
            float f5 = this.mVelocityX;
            float f6 = this.mVelocityY;
            if (f5 != 0.0f || f6 != 0.0f) {
                iEntity.setPosition((f5 * f) + iEntity.getX(), (f6 * f) + iEntity.getY());
            }
        }
    }

    public void reset() {
        this.mAccelerationX = 0.0f;
        this.mAccelerationY = 0.0f;
        this.mVelocityX = 0.0f;
        this.mVelocityY = 0.0f;
        this.mAngularVelocity = 0.0f;
    }
}
