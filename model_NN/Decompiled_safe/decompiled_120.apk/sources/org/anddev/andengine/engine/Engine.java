package org.anddev.andengine.engine;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Debug;
import android.os.Process;
import android.os.Vibrator;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;
import org.anddev.andengine.audio.music.MusicFactory;
import org.anddev.andengine.audio.music.MusicManager;
import org.anddev.andengine.audio.sound.SoundFactory;
import org.anddev.andengine.audio.sound.SoundManager;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.engine.handler.IUpdateHandler;
import org.anddev.andengine.engine.handler.UpdateHandlerList;
import org.anddev.andengine.engine.handler.runnable.RunnableHandler;
import org.anddev.andengine.engine.options.EngineOptions;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.input.touch.controller.ITouchController;
import org.anddev.andengine.input.touch.controller.SingleTouchControler;
import org.anddev.andengine.opengl.buffer.BufferObjectManager;
import org.anddev.andengine.opengl.font.FontFactory;
import org.anddev.andengine.opengl.font.FontManager;
import org.anddev.andengine.opengl.texture.TextureManager;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.anddev.andengine.opengl.util.GLHelper;
import org.anddev.andengine.sensor.SensorDelay;
import org.anddev.andengine.sensor.accelerometer.AccelerometerData;
import org.anddev.andengine.sensor.accelerometer.AccelerometerSensorOptions;
import org.anddev.andengine.sensor.accelerometer.IAccelerometerListener;
import org.anddev.andengine.sensor.location.ILocationListener;
import org.anddev.andengine.sensor.location.LocationProviderStatus;
import org.anddev.andengine.sensor.location.LocationSensorOptions;
import org.anddev.andengine.sensor.orientation.IOrientationListener;
import org.anddev.andengine.sensor.orientation.OrientationData;
import org.anddev.andengine.sensor.orientation.OrientationSensorOptions;
import org.anddev.andengine.util.constants.TimeConstants;

public class Engine implements SensorEventListener, LocationListener, View.OnTouchListener, ITouchController.ITouchEventCallback, TimeConstants {
    private static final SensorDelay SENSORDELAY_DEFAULT = SensorDelay.GAME;
    private static final int UPDATEHANDLERS_CAPACITY_DEFAULT = 32;
    private AccelerometerData mAccelerometerData;
    private IAccelerometerListener mAccelerometerListener;
    private final BufferObjectManager mBufferObjectManager = new BufferObjectManager();
    protected final Camera mCamera;
    /* access modifiers changed from: private */
    public final EngineOptions mEngineOptions;
    private final FontManager mFontManager = new FontManager();
    private boolean mIsMethodTracing;
    private long mLastTick = -1;
    private Location mLocation;
    private ILocationListener mLocationListener;
    private MusicManager mMusicManager;
    private OrientationData mOrientationData;
    private IOrientationListener mOrientationListener;
    private boolean mRunning = false;
    protected Scene mScene;
    private float mSecondsElapsedTotal = 0.0f;
    private SoundManager mSoundManager;
    protected int mSurfaceHeight = 1;
    protected int mSurfaceWidth = 1;
    private final TextureManager mTextureManager = new TextureManager();
    private final State mThreadLocker = new State(null);
    private ITouchController mTouchController;
    private final UpdateHandlerList mUpdateHandlers = new UpdateHandlerList(32);
    private final UpdateThread mUpdateThread = new UpdateThread();
    private final RunnableHandler mUpdateThreadRunnableHandler = new RunnableHandler();
    private Vibrator mVibrator;

    public Engine(EngineOptions engineOptions) {
        BitmapTextureAtlasTextureRegionFactory.reset();
        SoundFactory.reset();
        MusicFactory.reset();
        FontFactory.reset();
        BufferObjectManager.setActiveInstance(this.mBufferObjectManager);
        this.mEngineOptions = engineOptions;
        setTouchController(new SingleTouchControler());
        this.mCamera = engineOptions.getCamera();
        if (this.mEngineOptions.needsSound()) {
            this.mSoundManager = new SoundManager();
        }
        if (this.mEngineOptions.needsMusic()) {
            this.mMusicManager = new MusicManager();
        }
        this.mUpdateThread.start();
    }

    public boolean isRunning() {
        return this.mRunning;
    }

    public synchronized void start() {
        if (!this.mRunning) {
            this.mLastTick = System.nanoTime();
            this.mRunning = true;
        }
    }

    public synchronized void stop() {
        if (this.mRunning) {
            this.mRunning = false;
        }
    }

    public Scene getScene() {
        return this.mScene;
    }

    public void setScene(Scene scene) {
        this.mScene = scene;
    }

    public EngineOptions getEngineOptions() {
        return this.mEngineOptions;
    }

    public Camera getCamera() {
        return this.mCamera;
    }

    public float getSecondsElapsedTotal() {
        return this.mSecondsElapsedTotal;
    }

    public void setSurfaceSize(int i, int i2) {
        this.mSurfaceWidth = i;
        this.mSurfaceHeight = i2;
        onUpdateCameraSurface();
    }

    /* access modifiers changed from: protected */
    public void onUpdateCameraSurface() {
        this.mCamera.setSurfaceSize(0, 0, this.mSurfaceWidth, this.mSurfaceHeight);
    }

    public int getSurfaceWidth() {
        return this.mSurfaceWidth;
    }

    public int getSurfaceHeight() {
        return this.mSurfaceHeight;
    }

    public ITouchController getTouchController() {
        return this.mTouchController;
    }

    public void setTouchController(ITouchController iTouchController) {
        this.mTouchController = iTouchController;
        this.mTouchController.applyTouchOptions(this.mEngineOptions.getTouchOptions());
        this.mTouchController.setTouchEventCallback(this);
    }

    public AccelerometerData getAccelerometerData() {
        return this.mAccelerometerData;
    }

    public OrientationData getOrientationData() {
        return this.mOrientationData;
    }

    public SoundManager getSoundManager() {
        if (this.mSoundManager != null) {
            return this.mSoundManager;
        }
        throw new IllegalStateException("To enable the SoundManager, check the EngineOptions!");
    }

    public MusicManager getMusicManager() {
        if (this.mMusicManager != null) {
            return this.mMusicManager;
        }
        throw new IllegalStateException("To enable the MusicManager, check the EngineOptions!");
    }

    public TextureManager getTextureManager() {
        return this.mTextureManager;
    }

    public FontManager getFontManager() {
        return this.mFontManager;
    }

    public void clearUpdateHandlers() {
        this.mUpdateHandlers.clear();
    }

    public void registerUpdateHandler(IUpdateHandler iUpdateHandler) {
        this.mUpdateHandlers.add(iUpdateHandler);
    }

    public void unregisterUpdateHandler(IUpdateHandler iUpdateHandler) {
        this.mUpdateHandlers.remove(iUpdateHandler);
    }

    public boolean isMethodTracing() {
        return this.mIsMethodTracing;
    }

    public void startMethodTracing(String str) {
        if (!this.mIsMethodTracing) {
            this.mIsMethodTracing = true;
            Debug.startMethodTracing(str);
        }
    }

    public void stopMethodTracing() {
        if (this.mIsMethodTracing) {
            Debug.stopMethodTracing();
            this.mIsMethodTracing = false;
        }
    }

    public void onAccuracyChanged(Sensor sensor, int i) {
        if (this.mRunning) {
            switch (sensor.getType()) {
                case 1:
                    if (this.mAccelerometerData != null) {
                        this.mAccelerometerData.setAccuracy(i);
                        this.mAccelerometerListener.onAccelerometerChanged(this.mAccelerometerData);
                        return;
                    } else if (this.mOrientationData != null) {
                        this.mOrientationData.setAccelerometerAccuracy(i);
                        this.mOrientationListener.onOrientationChanged(this.mOrientationData);
                        return;
                    } else {
                        return;
                    }
                case 2:
                    this.mOrientationData.setMagneticFieldAccuracy(i);
                    this.mOrientationListener.onOrientationChanged(this.mOrientationData);
                    return;
                default:
                    return;
            }
        }
    }

    public void onSensorChanged(SensorEvent sensorEvent) {
        if (this.mRunning) {
            switch (sensorEvent.sensor.getType()) {
                case 1:
                    if (this.mAccelerometerData != null) {
                        this.mAccelerometerData.setValues(sensorEvent.values);
                        this.mAccelerometerListener.onAccelerometerChanged(this.mAccelerometerData);
                        return;
                    } else if (this.mOrientationData != null) {
                        this.mOrientationData.setAccelerometerValues(sensorEvent.values);
                        this.mOrientationListener.onOrientationChanged(this.mOrientationData);
                        return;
                    } else {
                        return;
                    }
                case 2:
                    this.mOrientationData.setMagneticFieldValues(sensorEvent.values);
                    this.mOrientationListener.onOrientationChanged(this.mOrientationData);
                    return;
                default:
                    return;
            }
        }
    }

    public void onLocationChanged(Location location) {
        if (this.mLocation == null) {
            this.mLocation = location;
        } else if (location == null) {
            this.mLocationListener.onLocationLost();
        } else {
            this.mLocation = location;
            this.mLocationListener.onLocationChanged(location);
        }
    }

    public void onProviderDisabled(String str) {
        this.mLocationListener.onLocationProviderDisabled();
    }

    public void onProviderEnabled(String str) {
        this.mLocationListener.onLocationProviderEnabled();
    }

    public void onStatusChanged(String str, int i, Bundle bundle) {
        switch (i) {
            case 0:
                this.mLocationListener.onLocationProviderStatusChanged(LocationProviderStatus.OUT_OF_SERVICE, bundle);
                return;
            case 1:
                this.mLocationListener.onLocationProviderStatusChanged(LocationProviderStatus.TEMPORARILY_UNAVAILABLE, bundle);
                return;
            case 2:
                this.mLocationListener.onLocationProviderStatusChanged(LocationProviderStatus.AVAILABLE, bundle);
                return;
            default:
                return;
        }
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (!this.mRunning) {
            return false;
        }
        boolean onHandleMotionEvent = this.mTouchController.onHandleMotionEvent(motionEvent);
        try {
            Thread.sleep(20);
            return onHandleMotionEvent;
        } catch (InterruptedException e) {
            org.anddev.andengine.util.Debug.e(e);
            return onHandleMotionEvent;
        }
    }

    public boolean onTouchEvent(TouchEvent touchEvent) {
        Scene sceneFromSurfaceTouchEvent = getSceneFromSurfaceTouchEvent(touchEvent);
        Camera cameraFromSurfaceTouchEvent = getCameraFromSurfaceTouchEvent(touchEvent);
        convertSurfaceToSceneTouchEvent(cameraFromSurfaceTouchEvent, touchEvent);
        if (onTouchHUD(cameraFromSurfaceTouchEvent, touchEvent)) {
            return true;
        }
        return onTouchScene(sceneFromSurfaceTouchEvent, touchEvent);
    }

    /* access modifiers changed from: protected */
    public boolean onTouchHUD(Camera camera, TouchEvent touchEvent) {
        if (camera.hasHUD()) {
            return camera.getHUD().onSceneTouchEvent(touchEvent);
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean onTouchScene(Scene scene, TouchEvent touchEvent) {
        if (scene != null) {
            return scene.onSceneTouchEvent(touchEvent);
        }
        return false;
    }

    public void runOnUpdateThread(Runnable runnable) {
        this.mUpdateThreadRunnableHandler.postRunnable(runnable);
    }

    public void interruptUpdateThread() {
        this.mUpdateThread.interrupt();
    }

    public void onResume() {
        this.mTextureManager.reloadTextures();
        this.mFontManager.reloadFonts();
        BufferObjectManager.setActiveInstance(this.mBufferObjectManager);
        this.mBufferObjectManager.reloadBufferObjects();
    }

    public void onPause() {
    }

    /* access modifiers changed from: protected */
    public Camera getCameraFromSurfaceTouchEvent(TouchEvent touchEvent) {
        return getCamera();
    }

    /* access modifiers changed from: protected */
    public Scene getSceneFromSurfaceTouchEvent(TouchEvent touchEvent) {
        return this.mScene;
    }

    /* access modifiers changed from: protected */
    public void convertSurfaceToSceneTouchEvent(Camera camera, TouchEvent touchEvent) {
        camera.convertSurfaceToSceneTouchEvent(touchEvent, this.mSurfaceWidth, this.mSurfaceHeight);
    }

    public void onLoadComplete(Scene scene) {
        setScene(scene);
    }

    /* access modifiers changed from: package-private */
    public void onTickUpdate() {
        if (this.mRunning) {
            onUpdate(getNanosecondsElapsed());
            yieldDraw();
            return;
        }
        yieldDraw();
        Thread.sleep(16);
    }

    private void yieldDraw() {
        State state = this.mThreadLocker;
        state.notifyCanDraw();
        state.waitUntilCanUpdate();
    }

    /* access modifiers changed from: protected */
    public void onUpdate(long j) {
        float f = ((float) j) / 1.0E9f;
        this.mSecondsElapsedTotal += f;
        this.mLastTick += j;
        this.mTouchController.onUpdate(f);
        updateUpdateHandlers(f);
        onUpdateScene(f);
    }

    /* access modifiers changed from: protected */
    public void onUpdateScene(float f) {
        if (this.mScene != null) {
            this.mScene.onUpdate(f);
        }
    }

    /* access modifiers changed from: protected */
    public void updateUpdateHandlers(float f) {
        this.mUpdateThreadRunnableHandler.onUpdate(f);
        this.mUpdateHandlers.onUpdate(f);
        getCamera().onUpdate(f);
    }

    public void onDrawFrame(GL10 gl10) {
        State state = this.mThreadLocker;
        state.waitUntilCanDraw();
        this.mTextureManager.updateTextures(gl10);
        this.mFontManager.updateFonts(gl10);
        if (GLHelper.EXTENSIONS_VERTEXBUFFEROBJECTS) {
            this.mBufferObjectManager.updateBufferObjects((GL11) gl10);
        }
        onDrawScene(gl10);
        state.notifyCanUpdate();
    }

    /* access modifiers changed from: protected */
    public void onDrawScene(GL10 gl10) {
        Camera camera = getCamera();
        this.mScene.onDraw(gl10, camera);
        camera.onDrawHUD(gl10);
    }

    private long getNanosecondsElapsed() {
        return calculateNanosecondsElapsed(System.nanoTime(), this.mLastTick);
    }

    /* access modifiers changed from: protected */
    public long calculateNanosecondsElapsed(long j, long j2) {
        return j - j2;
    }

    public boolean enableVibrator(Context context) {
        this.mVibrator = (Vibrator) context.getSystemService("vibrator");
        return this.mVibrator != null;
    }

    public void vibrate(long j) {
        if (this.mVibrator != null) {
            this.mVibrator.vibrate(j);
            return;
        }
        throw new IllegalStateException("You need to enable the Vibrator before you can use it!");
    }

    public void vibrate(long[] jArr, int i) {
        if (this.mVibrator != null) {
            this.mVibrator.vibrate(jArr, i);
            return;
        }
        throw new IllegalStateException("You need to enable the Vibrator before you can use it!");
    }

    public void enableLocationSensor(Context context, ILocationListener iLocationListener, LocationSensorOptions locationSensorOptions) {
        this.mLocationListener = iLocationListener;
        LocationManager locationManager = (LocationManager) context.getSystemService("location");
        String bestProvider = locationManager.getBestProvider(locationSensorOptions, locationSensorOptions.isEnabledOnly());
        locationManager.requestLocationUpdates(bestProvider, locationSensorOptions.getMinimumTriggerTime(), (float) locationSensorOptions.getMinimumTriggerDistance(), this);
        onLocationChanged(locationManager.getLastKnownLocation(bestProvider));
    }

    public void disableLocationSensor(Context context) {
        ((LocationManager) context.getSystemService("location")).removeUpdates(this);
    }

    public boolean enableAccelerometerSensor(Context context, IAccelerometerListener iAccelerometerListener) {
        return enableAccelerometerSensor(context, iAccelerometerListener, new AccelerometerSensorOptions(SENSORDELAY_DEFAULT));
    }

    public boolean enableAccelerometerSensor(Context context, IAccelerometerListener iAccelerometerListener, AccelerometerSensorOptions accelerometerSensorOptions) {
        SensorManager sensorManager = (SensorManager) context.getSystemService("sensor");
        if (!isSensorSupported(sensorManager, 1)) {
            return false;
        }
        this.mAccelerometerListener = iAccelerometerListener;
        if (this.mAccelerometerData == null) {
            this.mAccelerometerData = new AccelerometerData(((WindowManager) context.getSystemService("window")).getDefaultDisplay().getOrientation());
        }
        registerSelfAsSensorListener(sensorManager, 1, accelerometerSensorOptions.getSensorDelay());
        return true;
    }

    public boolean disableAccelerometerSensor(Context context) {
        SensorManager sensorManager = (SensorManager) context.getSystemService("sensor");
        if (!isSensorSupported(sensorManager, 1)) {
            return false;
        }
        unregisterSelfAsSensorListener(sensorManager, 1);
        return true;
    }

    public boolean enableOrientationSensor(Context context, IOrientationListener iOrientationListener) {
        return enableOrientationSensor(context, iOrientationListener, new OrientationSensorOptions(SENSORDELAY_DEFAULT));
    }

    public boolean enableOrientationSensor(Context context, IOrientationListener iOrientationListener, OrientationSensorOptions orientationSensorOptions) {
        SensorManager sensorManager = (SensorManager) context.getSystemService("sensor");
        if (!isSensorSupported(sensorManager, 1) || !isSensorSupported(sensorManager, 2)) {
            return false;
        }
        this.mOrientationListener = iOrientationListener;
        if (this.mOrientationData == null) {
            this.mOrientationData = new OrientationData(((WindowManager) context.getSystemService("window")).getDefaultDisplay().getOrientation());
        }
        registerSelfAsSensorListener(sensorManager, 1, orientationSensorOptions.getSensorDelay());
        registerSelfAsSensorListener(sensorManager, 2, orientationSensorOptions.getSensorDelay());
        return true;
    }

    public boolean disableOrientationSensor(Context context) {
        SensorManager sensorManager = (SensorManager) context.getSystemService("sensor");
        if (!isSensorSupported(sensorManager, 1) || !isSensorSupported(sensorManager, 2)) {
            return false;
        }
        unregisterSelfAsSensorListener(sensorManager, 1);
        unregisterSelfAsSensorListener(sensorManager, 2);
        return true;
    }

    private boolean isSensorSupported(SensorManager sensorManager, int i) {
        return sensorManager.getSensorList(i).size() > 0;
    }

    private void registerSelfAsSensorListener(SensorManager sensorManager, int i, SensorDelay sensorDelay) {
        sensorManager.registerListener(this, sensorManager.getSensorList(i).get(0), sensorDelay.getDelay());
    }

    private void unregisterSelfAsSensorListener(SensorManager sensorManager, int i) {
        sensorManager.unregisterListener(this, sensorManager.getSensorList(i).get(0));
    }

    class UpdateThread extends Thread {
        public UpdateThread() {
            super("UpdateThread");
        }

        public void run() {
            Process.setThreadPriority(Engine.this.mEngineOptions.getUpdateThreadPriority());
            while (true) {
                try {
                    Engine.this.onTickUpdate();
                } catch (InterruptedException e) {
                    org.anddev.andengine.util.Debug.d("UpdateThread interrupted. Don't worry - this Exception is most likely expected!", e);
                    interrupt();
                    return;
                }
            }
        }
    }

    class State {
        boolean mDrawing;

        private State() {
            this.mDrawing = false;
        }

        /* synthetic */ State(State state) {
            this();
        }

        public synchronized void notifyCanDraw() {
            this.mDrawing = true;
            notifyAll();
        }

        public synchronized void notifyCanUpdate() {
            this.mDrawing = false;
            notifyAll();
        }

        public synchronized void waitUntilCanDraw() {
            while (!this.mDrawing) {
                wait();
            }
        }

        public synchronized void waitUntilCanUpdate() {
            while (this.mDrawing) {
                wait();
            }
        }
    }
}
