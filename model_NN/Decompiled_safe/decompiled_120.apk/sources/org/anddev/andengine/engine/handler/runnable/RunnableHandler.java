package org.anddev.andengine.engine.handler.runnable;

import java.util.ArrayList;
import org.anddev.andengine.engine.handler.IUpdateHandler;

public class RunnableHandler implements IUpdateHandler {
    private final ArrayList mRunnables = new ArrayList();

    public synchronized void onUpdate(float f) {
        ArrayList arrayList = this.mRunnables;
        for (int size = arrayList.size() - 1; size >= 0; size--) {
            ((Runnable) arrayList.get(size)).run();
        }
        arrayList.clear();
    }

    public void reset() {
        this.mRunnables.clear();
    }

    public synchronized void postRunnable(Runnable runnable) {
        this.mRunnables.add(runnable);
    }
}
