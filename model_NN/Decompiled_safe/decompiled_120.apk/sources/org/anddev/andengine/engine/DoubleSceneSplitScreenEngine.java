package org.anddev.andengine.engine;

import javax.microedition.khronos.opengles.GL10;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.engine.options.EngineOptions;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.opengl.util.GLHelper;

public class DoubleSceneSplitScreenEngine extends Engine {
    private final Camera mSecondCamera;
    private Scene mSecondScene;

    public DoubleSceneSplitScreenEngine(EngineOptions engineOptions, Camera camera) {
        super(engineOptions);
        this.mSecondCamera = camera;
    }

    public Camera getCamera() {
        return this.mCamera;
    }

    public Camera getFirstCamera() {
        return this.mCamera;
    }

    public Camera getSecondCamera() {
        return this.mSecondCamera;
    }

    public Scene getScene() {
        return super.getScene();
    }

    public Scene getFirstScene() {
        return super.getScene();
    }

    public Scene getSecondScene() {
        return this.mSecondScene;
    }

    public void setScene(Scene scene) {
        setFirstScene(scene);
    }

    public void setFirstScene(Scene scene) {
        super.setScene(scene);
    }

    public void setSecondScene(Scene scene) {
        this.mSecondScene = scene;
    }

    /* access modifiers changed from: protected */
    public void onDrawScene(GL10 gl10) {
        Camera firstCamera = getFirstCamera();
        Camera secondCamera = getSecondCamera();
        int i = this.mSurfaceWidth >> 1;
        int i2 = this.mSurfaceHeight;
        GLHelper.enableScissorTest(gl10);
        gl10.glScissor(0, 0, i, i2);
        gl10.glViewport(0, 0, i, i2);
        this.mScene.onDraw(gl10, firstCamera);
        firstCamera.onDrawHUD(gl10);
        gl10.glScissor(i, 0, i, i2);
        gl10.glViewport(i, 0, i, i2);
        this.mSecondScene.onDraw(gl10, secondCamera);
        secondCamera.onDrawHUD(gl10);
        GLHelper.disableScissorTest(gl10);
    }

    /* access modifiers changed from: protected */
    public Camera getCameraFromSurfaceTouchEvent(TouchEvent touchEvent) {
        if (touchEvent.getX() <= ((float) (this.mSurfaceWidth >> 1))) {
            return getFirstCamera();
        }
        return getSecondCamera();
    }

    /* access modifiers changed from: protected */
    public Scene getSceneFromSurfaceTouchEvent(TouchEvent touchEvent) {
        if (touchEvent.getX() <= ((float) (this.mSurfaceWidth >> 1))) {
            return getFirstScene();
        }
        return getSecondScene();
    }

    /* access modifiers changed from: protected */
    public void onUpdateScene(float f) {
        super.onUpdateScene(f);
        if (this.mSecondScene != null) {
            this.mSecondScene.onUpdate(f);
        }
    }

    /* access modifiers changed from: protected */
    public void convertSurfaceToSceneTouchEvent(Camera camera, TouchEvent touchEvent) {
        int i = this.mSurfaceWidth >> 1;
        if (camera == getFirstCamera()) {
            camera.convertSurfaceToSceneTouchEvent(touchEvent, i, this.mSurfaceHeight);
            return;
        }
        touchEvent.offset((float) (-i), 0.0f);
        camera.convertSurfaceToSceneTouchEvent(touchEvent, i, this.mSurfaceHeight);
    }

    /* access modifiers changed from: protected */
    public void updateUpdateHandlers(float f) {
        super.updateUpdateHandlers(f);
        getSecondCamera().onUpdate(f);
    }

    /* access modifiers changed from: protected */
    public void onUpdateCameraSurface() {
        int i = this.mSurfaceWidth >> 1;
        getFirstCamera().setSurfaceSize(0, 0, i, this.mSurfaceHeight);
        getSecondCamera().setSurfaceSize(i, 0, i, this.mSurfaceHeight);
    }
}
