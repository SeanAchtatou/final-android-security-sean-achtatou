package org.anddev.andengine.engine;

import org.anddev.andengine.engine.options.EngineOptions;
import org.anddev.andengine.util.constants.TimeConstants;

public class LimitedFPSEngine extends Engine {
    private final long mPreferredFrameLengthNanoseconds;

    public LimitedFPSEngine(EngineOptions engineOptions, int i) {
        super(engineOptions);
        this.mPreferredFrameLengthNanoseconds = TimeConstants.NANOSECONDSPERSECOND / ((long) i);
    }

    public void onUpdate(long j) {
        long j2 = this.mPreferredFrameLengthNanoseconds - j;
        if (j2 <= 0) {
            super.onUpdate(j);
            return;
        }
        Thread.sleep((long) ((int) (j2 / TimeConstants.NANOSECONDSPERMILLISECOND)));
        super.onUpdate(j2 + j);
    }
}
