package org.anddev.andengine.engine.handler.timer;

import org.anddev.andengine.engine.handler.IUpdateHandler;

public class TimerHandler implements IUpdateHandler {
    private boolean mAutoReset;
    protected final ITimerCallback mTimerCallback;
    private boolean mTimerCallbackTriggered;
    private float mTimerSeconds;
    private float mTimerSecondsElapsed;

    public TimerHandler(float f, ITimerCallback iTimerCallback) {
        this(f, false, iTimerCallback);
    }

    public TimerHandler(float f, boolean z, ITimerCallback iTimerCallback) {
        this.mTimerSeconds = f;
        this.mAutoReset = z;
        this.mTimerCallback = iTimerCallback;
    }

    public boolean isAutoReset() {
        return this.mAutoReset;
    }

    public void setAutoReset(boolean z) {
        this.mAutoReset = z;
    }

    public void setTimerSeconds(float f) {
        this.mTimerSeconds = f;
    }

    public float getTimerSeconds() {
        return this.mTimerSeconds;
    }

    public float getTimerSecondsElapsed() {
        return this.mTimerSecondsElapsed;
    }

    public boolean isTimerCallbackTriggered() {
        return this.mTimerCallbackTriggered;
    }

    public void setTimerCallbackTriggered(boolean z) {
        this.mTimerCallbackTriggered = z;
    }

    public void onUpdate(float f) {
        if (this.mAutoReset) {
            this.mTimerSecondsElapsed += f;
            while (this.mTimerSecondsElapsed >= this.mTimerSeconds) {
                this.mTimerSecondsElapsed -= this.mTimerSeconds;
                this.mTimerCallback.onTimePassed(this);
            }
        } else if (!this.mTimerCallbackTriggered) {
            this.mTimerSecondsElapsed += f;
            if (this.mTimerSecondsElapsed >= this.mTimerSeconds) {
                this.mTimerCallbackTriggered = true;
                this.mTimerCallback.onTimePassed(this);
            }
        }
    }

    public void reset() {
        this.mTimerCallbackTriggered = false;
        this.mTimerSecondsElapsed = 0.0f;
    }
}
