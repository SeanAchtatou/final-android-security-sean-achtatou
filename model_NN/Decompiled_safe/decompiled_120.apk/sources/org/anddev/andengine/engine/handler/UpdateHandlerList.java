package org.anddev.andengine.engine.handler;

import org.anddev.andengine.util.SmartList;

public class UpdateHandlerList extends SmartList implements IUpdateHandler {
    private static final long serialVersionUID = -8842562717687229277L;

    public UpdateHandlerList() {
    }

    public UpdateHandlerList(int i) {
        super(i);
    }

    public void onUpdate(float f) {
        for (int size = size() - 1; size >= 0; size--) {
            ((IUpdateHandler) get(size)).onUpdate(f);
        }
    }

    public void reset() {
        for (int size = size() - 1; size >= 0; size--) {
            ((IUpdateHandler) get(size)).reset();
        }
    }
}
