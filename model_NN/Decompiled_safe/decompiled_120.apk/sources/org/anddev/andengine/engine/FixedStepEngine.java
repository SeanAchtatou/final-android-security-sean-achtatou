package org.anddev.andengine.engine;

import org.anddev.andengine.engine.options.EngineOptions;
import org.anddev.andengine.util.constants.TimeConstants;

public class FixedStepEngine extends Engine {
    private long mSecondsElapsedAccumulator;
    private final long mStepLength;

    public FixedStepEngine(EngineOptions engineOptions, int i) {
        super(engineOptions);
        this.mStepLength = TimeConstants.NANOSECONDSPERSECOND / ((long) i);
    }

    public void onUpdate(long j) {
        this.mSecondsElapsedAccumulator += j;
        long j2 = this.mStepLength;
        while (this.mSecondsElapsedAccumulator >= j2) {
            super.onUpdate(j2);
            this.mSecondsElapsedAccumulator -= j2;
        }
    }
}
