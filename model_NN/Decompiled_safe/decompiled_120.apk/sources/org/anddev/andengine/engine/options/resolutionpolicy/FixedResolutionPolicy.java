package org.anddev.andengine.engine.options.resolutionpolicy;

import org.anddev.andengine.opengl.view.RenderSurfaceView;

public class FixedResolutionPolicy extends BaseResolutionPolicy {
    private final int mHeight;
    private final int mWidth;

    public FixedResolutionPolicy(int i, int i2) {
        this.mWidth = i;
        this.mHeight = i2;
    }

    public void onMeasure(RenderSurfaceView renderSurfaceView, int i, int i2) {
        renderSurfaceView.setMeasuredDimensionProxy(this.mWidth, this.mHeight);
    }
}
