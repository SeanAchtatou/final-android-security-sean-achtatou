package org.anddev.andengine.engine.camera;

public class SmoothCamera extends ZoomCamera {
    private float mMaxVelocityX;
    private float mMaxVelocityY;
    private float mMaxZoomFactorChange;
    private float mTargetCenterX = getCenterX();
    private float mTargetCenterY = getCenterY();
    private float mTargetZoomFactor = 1.0f;

    public SmoothCamera(float f, float f2, float f3, float f4, float f5, float f6, float f7) {
        super(f, f2, f3, f4);
        this.mMaxVelocityX = f5;
        this.mMaxVelocityY = f6;
        this.mMaxZoomFactorChange = f7;
    }

    public void setCenter(float f, float f2) {
        this.mTargetCenterX = f;
        this.mTargetCenterY = f2;
    }

    public void setCenterDirect(float f, float f2) {
        super.setCenter(f, f2);
        this.mTargetCenterX = f;
        this.mTargetCenterY = f2;
    }

    public void setZoomFactor(float f) {
        if (this.mTargetZoomFactor == f) {
            return;
        }
        if (this.mTargetZoomFactor == this.mZoomFactor) {
            this.mTargetZoomFactor = f;
            onSmoothZoomStarted();
            return;
        }
        this.mTargetZoomFactor = f;
    }

    public void setZoomFactorDirect(float f) {
        if (this.mTargetZoomFactor != this.mZoomFactor) {
            this.mTargetZoomFactor = f;
            super.setZoomFactor(f);
            onSmoothZoomFinished();
            return;
        }
        this.mTargetZoomFactor = f;
        super.setZoomFactor(f);
    }

    public void setMaxVelocityX(float f) {
        this.mMaxVelocityX = f;
    }

    public void setMaxVelocityY(float f) {
        this.mMaxVelocityY = f;
    }

    public void setMaxVelocity(float f, float f2) {
        this.mMaxVelocityX = f;
        this.mMaxVelocityY = f2;
    }

    public void setMaxZoomFactorChange(float f) {
        this.mMaxZoomFactorChange = f;
    }

    /* access modifiers changed from: protected */
    public void onSmoothZoomStarted() {
    }

    /* access modifiers changed from: protected */
    public void onSmoothZoomFinished() {
    }

    public void onUpdate(float f) {
        super.onUpdate(f);
        float centerX = getCenterX();
        float centerY = getCenterY();
        float f2 = this.mTargetCenterX;
        float f3 = this.mTargetCenterY;
        if (!(centerX == f2 && centerY == f3)) {
            super.setCenter(centerX + limitToMaxVelocityX(f2 - centerX, f), centerY + limitToMaxVelocityY(f3 - centerY, f));
        }
        float zoomFactor = getZoomFactor();
        float f4 = this.mTargetZoomFactor;
        if (zoomFactor != f4) {
            super.setZoomFactor(zoomFactor + limitToMaxZoomFactorChange(f4 - zoomFactor, f));
            if (this.mZoomFactor == this.mTargetZoomFactor) {
                onSmoothZoomFinished();
            }
        }
    }

    private float limitToMaxVelocityX(float f, float f2) {
        if (f > 0.0f) {
            return Math.min(f, this.mMaxVelocityX * f2);
        }
        return Math.max(f, (-this.mMaxVelocityX) * f2);
    }

    private float limitToMaxVelocityY(float f, float f2) {
        if (f > 0.0f) {
            return Math.min(f, this.mMaxVelocityY * f2);
        }
        return Math.max(f, (-this.mMaxVelocityY) * f2);
    }

    private float limitToMaxZoomFactorChange(float f, float f2) {
        if (f > 0.0f) {
            return Math.min(f, this.mMaxZoomFactorChange * f2);
        }
        return Math.max(f, (-this.mMaxZoomFactorChange) * f2);
    }
}
