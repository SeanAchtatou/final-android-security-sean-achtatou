package org.anddev.andengine.engine.options.resolutionpolicy;

import android.view.View;
import org.anddev.andengine.opengl.view.RenderSurfaceView;

public class RelativeResolutionPolicy extends BaseResolutionPolicy {
    private final float mHeightScale;
    private final float mWidthScale;

    public RelativeResolutionPolicy(float f) {
        this(f, f);
    }

    public RelativeResolutionPolicy(float f, float f2) {
        this.mWidthScale = f;
        this.mHeightScale = f2;
    }

    public void onMeasure(RenderSurfaceView renderSurfaceView, int i, int i2) {
        BaseResolutionPolicy.throwOnNotMeasureSpecEXACTLY(i, i2);
        renderSurfaceView.setMeasuredDimensionProxy((int) (((float) View.MeasureSpec.getSize(i)) * this.mWidthScale), (int) (((float) View.MeasureSpec.getSize(i2)) * this.mHeightScale));
    }
}
