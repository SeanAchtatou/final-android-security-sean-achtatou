package org.anddev.andengine.ui.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.EditText;
import android.widget.Toast;
import org.anddev.andengine.util.Callback;
import org.anddev.andengine.util.Debug;

public abstract class GenericInputDialogBuilder {
    protected final Context mContext;
    private final String mDefaultText;
    /* access modifiers changed from: private */
    public final int mErrorResID;
    protected final int mIconResID;
    protected final int mMessageResID;
    protected final DialogInterface.OnCancelListener mOnCancelListener;
    protected final Callback mSuccessCallback;
    protected final int mTitleResID;

    /* access modifiers changed from: protected */
    public abstract Object generateResult(String str);

    public GenericInputDialogBuilder(Context context, int i, int i2, int i3, int i4, Callback callback, DialogInterface.OnCancelListener onCancelListener) {
        this(context, i, i2, i3, i4, "", callback, onCancelListener);
    }

    public GenericInputDialogBuilder(Context context, int i, int i2, int i3, int i4, String str, Callback callback, DialogInterface.OnCancelListener onCancelListener) {
        this.mContext = context;
        this.mTitleResID = i;
        this.mMessageResID = i2;
        this.mErrorResID = i3;
        this.mIconResID = i4;
        this.mDefaultText = str;
        this.mSuccessCallback = callback;
        this.mOnCancelListener = onCancelListener;
    }

    public Dialog create() {
        final EditText editText = new EditText(this.mContext);
        editText.setText(this.mDefaultText);
        AlertDialog.Builder builder = new AlertDialog.Builder(this.mContext);
        if (this.mTitleResID != 0) {
            builder.setTitle(this.mTitleResID);
        }
        if (this.mMessageResID != 0) {
            builder.setMessage(this.mMessageResID);
        }
        if (this.mIconResID != 0) {
            builder.setIcon(this.mIconResID);
        }
        setView(builder, editText);
        builder.setOnCancelListener(this.mOnCancelListener).setPositiveButton(17039370, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                try {
                    GenericInputDialogBuilder.this.mSuccessCallback.onCallback(GenericInputDialogBuilder.this.generateResult(editText.getText().toString()));
                    dialogInterface.dismiss();
                } catch (IllegalArgumentException e) {
                    Debug.e("Error in GenericInputDialogBuilder.generateResult()", e);
                    Toast.makeText(GenericInputDialogBuilder.this.mContext, GenericInputDialogBuilder.this.mErrorResID, 0).show();
                }
            }
        }).setNegativeButton(17039360, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                GenericInputDialogBuilder.this.mOnCancelListener.onCancel(dialogInterface);
                dialogInterface.dismiss();
            }
        });
        return builder.create();
    }

    /* access modifiers changed from: protected */
    public void setView(AlertDialog.Builder builder, EditText editText) {
        builder.setView(editText);
    }
}
