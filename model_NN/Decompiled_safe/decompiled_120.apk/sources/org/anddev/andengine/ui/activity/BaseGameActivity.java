package org.anddev.andengine.ui.activity;

import android.os.Bundle;
import android.os.PowerManager;
import android.widget.FrameLayout;
import org.anddev.andengine.audio.music.MusicManager;
import org.anddev.andengine.audio.sound.SoundManager;
import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.engine.options.EngineOptions;
import org.anddev.andengine.engine.options.WakeLockOptions;
import org.anddev.andengine.opengl.font.FontManager;
import org.anddev.andengine.opengl.texture.TextureManager;
import org.anddev.andengine.opengl.view.RenderSurfaceView;
import org.anddev.andengine.sensor.accelerometer.AccelerometerSensorOptions;
import org.anddev.andengine.sensor.accelerometer.IAccelerometerListener;
import org.anddev.andengine.sensor.location.ILocationListener;
import org.anddev.andengine.sensor.location.LocationSensorOptions;
import org.anddev.andengine.sensor.orientation.IOrientationListener;
import org.anddev.andengine.sensor.orientation.OrientationSensorOptions;
import org.anddev.andengine.ui.IGameInterface;
import org.anddev.andengine.util.ActivityUtils;
import org.anddev.andengine.util.Debug;
import org.anddev.andengine.util.constants.Constants;

public abstract class BaseGameActivity extends BaseActivity implements IGameInterface {
    private static /* synthetic */ int[] $SWITCH_TABLE$org$anddev$andengine$engine$options$EngineOptions$ScreenOrientation;
    /* access modifiers changed from: protected */
    public Engine mEngine;
    private boolean mGameLoaded;
    protected boolean mHasWindowFocused;
    private boolean mPaused;
    protected RenderSurfaceView mRenderSurfaceView;
    private PowerManager.WakeLock mWakeLock;

    static /* synthetic */ int[] $SWITCH_TABLE$org$anddev$andengine$engine$options$EngineOptions$ScreenOrientation() {
        int[] iArr = $SWITCH_TABLE$org$anddev$andengine$engine$options$EngineOptions$ScreenOrientation;
        if (iArr == null) {
            iArr = new int[EngineOptions.ScreenOrientation.values().length];
            try {
                iArr[EngineOptions.ScreenOrientation.LANDSCAPE.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[EngineOptions.ScreenOrientation.PORTRAIT.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            $SWITCH_TABLE$org$anddev$andengine$engine$options$EngineOptions$ScreenOrientation = iArr;
        }
        return iArr;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.mPaused = true;
        this.mEngine = onLoadEngine();
        applyEngineOptions(this.mEngine.getEngineOptions());
        onSetContentView();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.mPaused && this.mHasWindowFocused) {
            doResume();
        }
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        if (z) {
            if (this.mPaused) {
                doResume();
            }
            this.mHasWindowFocused = true;
            return;
        }
        if (!this.mPaused) {
            doPause();
        }
        this.mHasWindowFocused = false;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (!this.mPaused) {
            doPause();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.mEngine.interruptUpdateThread();
        onUnloadResources();
    }

    public void onUnloadResources() {
        if (this.mEngine.getEngineOptions().needsMusic()) {
            getMusicManager().releaseAll();
        }
        if (this.mEngine.getEngineOptions().needsSound()) {
            getSoundManager().releaseAll();
        }
    }

    public Engine getEngine() {
        return this.mEngine;
    }

    public TextureManager getTextureManager() {
        return this.mEngine.getTextureManager();
    }

    public FontManager getFontManager() {
        return this.mEngine.getFontManager();
    }

    public SoundManager getSoundManager() {
        return this.mEngine.getSoundManager();
    }

    public MusicManager getMusicManager() {
        return this.mEngine.getMusicManager();
    }

    public void onResumeGame() {
    }

    public void onPauseGame() {
    }

    private void doResume() {
        if (!this.mGameLoaded) {
            onLoadResources();
            this.mEngine.onLoadComplete(onLoadScene());
            onLoadComplete();
            this.mGameLoaded = true;
        }
        this.mPaused = false;
        acquireWakeLock(this.mEngine.getEngineOptions().getWakeLockOptions());
        this.mEngine.onResume();
        this.mRenderSurfaceView.onResume();
        this.mEngine.start();
        onResumeGame();
    }

    private void doPause() {
        this.mPaused = true;
        releaseWakeLock();
        this.mEngine.onPause();
        this.mEngine.stop();
        this.mRenderSurfaceView.onPause();
        onPauseGame();
    }

    public void runOnUpdateThread(Runnable runnable) {
        this.mEngine.runOnUpdateThread(runnable);
    }

    /* access modifiers changed from: protected */
    public void onSetContentView() {
        this.mRenderSurfaceView = new RenderSurfaceView(this);
        this.mRenderSurfaceView.setEGLConfigChooser(false);
        this.mRenderSurfaceView.setRenderer(this.mEngine);
        setContentView(this.mRenderSurfaceView, createSurfaceViewLayoutParams());
    }

    private void acquireWakeLock(WakeLockOptions wakeLockOptions) {
        if (wakeLockOptions == WakeLockOptions.SCREEN_ON) {
            ActivityUtils.keepScreenOn(this);
            return;
        }
        this.mWakeLock = ((PowerManager) getSystemService("power")).newWakeLock(wakeLockOptions.getFlag() | 536870912, Constants.DEBUGTAG);
        try {
            this.mWakeLock.acquire();
        } catch (SecurityException e) {
            Debug.e("You have to add\n\t<uses-permission android:name=\"android.permission.WAKE_LOCK\"/>\nto your AndroidManifest.xml !", e);
        }
    }

    private void releaseWakeLock() {
        if (this.mWakeLock != null && this.mWakeLock.isHeld()) {
            this.mWakeLock.release();
        }
    }

    private void applyEngineOptions(EngineOptions engineOptions) {
        if (engineOptions.isFullscreen()) {
            ActivityUtils.requestFullscreen(this);
        }
        if (engineOptions.needsMusic() || engineOptions.needsSound()) {
            setVolumeControlStream(3);
        }
        switch ($SWITCH_TABLE$org$anddev$andengine$engine$options$EngineOptions$ScreenOrientation()[engineOptions.getScreenOrientation().ordinal()]) {
            case 1:
                setRequestedOrientation(0);
                return;
            case 2:
                setRequestedOrientation(1);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public FrameLayout.LayoutParams createSurfaceViewLayoutParams() {
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, -1);
        layoutParams.gravity = 17;
        return layoutParams;
    }

    /* access modifiers changed from: protected */
    public void enableVibrator() {
        this.mEngine.enableVibrator(this);
    }

    /* access modifiers changed from: protected */
    public void enableLocationSensor(ILocationListener iLocationListener, LocationSensorOptions locationSensorOptions) {
        this.mEngine.enableLocationSensor(this, iLocationListener, locationSensorOptions);
    }

    /* access modifiers changed from: protected */
    public void disableLocationSensor() {
        this.mEngine.disableLocationSensor(this);
    }

    /* access modifiers changed from: protected */
    public boolean enableAccelerometerSensor(IAccelerometerListener iAccelerometerListener) {
        return this.mEngine.enableAccelerometerSensor(this, iAccelerometerListener);
    }

    /* access modifiers changed from: protected */
    public boolean enableAccelerometerSensor(IAccelerometerListener iAccelerometerListener, AccelerometerSensorOptions accelerometerSensorOptions) {
        return this.mEngine.enableAccelerometerSensor(this, iAccelerometerListener, accelerometerSensorOptions);
    }

    /* access modifiers changed from: protected */
    public boolean disableAccelerometerSensor() {
        return this.mEngine.disableAccelerometerSensor(this);
    }

    /* access modifiers changed from: protected */
    public boolean enableOrientationSensor(IOrientationListener iOrientationListener) {
        return this.mEngine.enableOrientationSensor(this, iOrientationListener);
    }

    /* access modifiers changed from: protected */
    public boolean enableOrientationSensor(IOrientationListener iOrientationListener, OrientationSensorOptions orientationSensorOptions) {
        return this.mEngine.enableOrientationSensor(this, iOrientationListener, orientationSensorOptions);
    }

    /* access modifiers changed from: protected */
    public boolean disableOrientationSensor() {
        return this.mEngine.disableOrientationSensor(this);
    }
}
