package org.anddev.andengine.ui.dialog;

import android.content.Context;
import android.content.DialogInterface;
import org.anddev.andengine.util.Callback;

public class StringInputDialogBuilder extends GenericInputDialogBuilder {
    public StringInputDialogBuilder(Context context, int i, int i2, int i3, int i4, Callback callback, DialogInterface.OnCancelListener onCancelListener) {
        super(context, i, i2, i3, i4, callback, onCancelListener);
    }

    public StringInputDialogBuilder(Context context, int i, int i2, int i3, int i4, String str, Callback callback, DialogInterface.OnCancelListener onCancelListener) {
        super(context, i, i2, i3, i4, str, callback, onCancelListener);
    }

    /* access modifiers changed from: protected */
    public String generateResult(String str) {
        return str;
    }
}
