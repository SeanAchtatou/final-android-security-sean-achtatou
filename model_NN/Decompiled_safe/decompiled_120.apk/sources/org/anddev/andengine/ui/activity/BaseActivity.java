package org.anddev.andengine.ui.activity;

import android.app.Activity;
import java.util.concurrent.Callable;
import org.anddev.andengine.util.ActivityUtils;
import org.anddev.andengine.util.AsyncCallable;
import org.anddev.andengine.util.Callback;
import org.anddev.andengine.util.progress.ProgressCallable;

public abstract class BaseActivity extends Activity {

    public class CancelledException extends Exception {
        private static final long serialVersionUID = -78123211381435596L;
    }

    /* access modifiers changed from: protected */
    public void doAsync(int i, int i2, Callable callable, Callback callback) {
        doAsync(i, i2, callable, callback, (Callback) null);
    }

    /* access modifiers changed from: protected */
    public void doAsync(int i, int i2, Callable callable, Callback callback, Callback callback2) {
        ActivityUtils.doAsync(this, i, i2, callable, callback, callback2);
    }

    /* access modifiers changed from: protected */
    public void doProgressAsync(int i, ProgressCallable progressCallable, Callback callback) {
        doProgressAsync(i, progressCallable, callback, null);
    }

    /* access modifiers changed from: protected */
    public void doProgressAsync(int i, ProgressCallable progressCallable, Callback callback, Callback callback2) {
        ActivityUtils.doProgressAsync(this, i, progressCallable, callback, callback2);
    }

    /* access modifiers changed from: protected */
    public void doAsync(int i, int i2, AsyncCallable asyncCallable, Callback callback, Callback callback2) {
        ActivityUtils.doAsync(this, i, i2, asyncCallable, callback, callback2);
    }
}
