package org.anddev.andengine.ui.activity;

import android.content.Intent;
import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.engine.handler.timer.ITimerCallback;
import org.anddev.andengine.engine.handler.timer.TimerHandler;
import org.anddev.andengine.engine.options.EngineOptions;
import org.anddev.andengine.engine.options.resolutionpolicy.IResolutionPolicy;
import org.anddev.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.scene.SplashScene;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasFactory;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.anddev.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource;
import org.anddev.andengine.opengl.texture.bitmap.BitmapTexture;
import org.anddev.andengine.opengl.texture.region.TextureRegion;

public abstract class BaseSplashActivity extends BaseGameActivity {
    private Camera mCamera;
    private TextureRegion mLoadingScreenTextureRegion;
    private IBitmapTextureAtlasSource mSplashTextureAtlasSource;

    /* access modifiers changed from: protected */
    public abstract Class getFollowUpActivity();

    /* access modifiers changed from: protected */
    public abstract EngineOptions.ScreenOrientation getScreenOrientation();

    /* access modifiers changed from: protected */
    public abstract float getSplashDuration();

    /* access modifiers changed from: protected */
    public abstract IBitmapTextureAtlasSource onGetSplashTextureAtlasSource();

    /* access modifiers changed from: protected */
    public float getSplashScaleFrom() {
        return 1.0f;
    }

    /* access modifiers changed from: protected */
    public float getSplashScaleTo() {
        return 1.0f;
    }

    public void onLoadComplete() {
    }

    public Engine onLoadEngine() {
        this.mSplashTextureAtlasSource = onGetSplashTextureAtlasSource();
        int width = this.mSplashTextureAtlasSource.getWidth();
        int height = this.mSplashTextureAtlasSource.getHeight();
        this.mCamera = getSplashCamera(width, height);
        return new Engine(new EngineOptions(true, getScreenOrientation(), getSplashResolutionPolicy(width, height), this.mCamera));
    }

    public void onLoadResources() {
        BitmapTextureAtlas createForTextureAtlasSourceSize = BitmapTextureAtlasFactory.createForTextureAtlasSourceSize(BitmapTexture.BitmapTextureFormat.RGBA_8888, this.mSplashTextureAtlasSource, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mLoadingScreenTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromSource(createForTextureAtlasSourceSize, this.mSplashTextureAtlasSource, 0, 0);
        getEngine().getTextureManager().loadTexture(createForTextureAtlasSourceSize);
    }

    public Scene onLoadScene() {
        float splashDuration = getSplashDuration();
        SplashScene splashScene = new SplashScene(this.mCamera, this.mLoadingScreenTextureRegion, splashDuration, getSplashScaleFrom(), getSplashScaleTo());
        splashScene.registerUpdateHandler(new TimerHandler(splashDuration, new ITimerCallback() {
            public void onTimePassed(TimerHandler timerHandler) {
                BaseSplashActivity.this.startActivity(new Intent(BaseSplashActivity.this, BaseSplashActivity.this.getFollowUpActivity()));
                BaseSplashActivity.this.finish();
            }
        }));
        return splashScene;
    }

    /* access modifiers changed from: protected */
    public Camera getSplashCamera(int i, int i2) {
        return new Camera(0.0f, 0.0f, (float) i, (float) i2);
    }

    /* access modifiers changed from: protected */
    public IResolutionPolicy getSplashResolutionPolicy(int i, int i2) {
        return new RatioResolutionPolicy((float) i, (float) i2);
    }
}
