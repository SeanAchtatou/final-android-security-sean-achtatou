package org.anddev.andengine.opengl.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import java.util.ArrayList;
import java.util.concurrent.Semaphore;
import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.egl.EGLSurface;
import javax.microedition.khronos.opengles.GL;
import javax.microedition.khronos.opengles.GL10;

public class GLSurfaceView extends SurfaceView implements SurfaceHolder.Callback {
    public static final int DEBUG_CHECK_GL_ERROR = 1;
    public static final int DEBUG_LOG_GL_CALLS = 2;
    public static final int RENDERMODE_CONTINUOUSLY = 1;
    public static final int RENDERMODE_WHEN_DIRTY = 0;
    /* access modifiers changed from: private */
    public static final Semaphore sEglSemaphore = new Semaphore(1);
    private int mDebugFlags;
    /* access modifiers changed from: private */
    public EGLConfigChooser mEGLConfigChooser;
    private GLThread mGLThread;
    /* access modifiers changed from: private */
    public GLWrapper mGLWrapper;
    private boolean mHasSurface;
    private int mRenderMode;
    private Renderer mRenderer;
    private int mSurfaceHeight;
    private int mSurfaceWidth;

    public interface Renderer {
        void onDrawFrame(GL10 gl10);

        void onSurfaceChanged(GL10 gl10, int i, int i2);

        void onSurfaceCreated(GL10 gl10, EGLConfig eGLConfig);
    }

    public GLSurfaceView(Context context) {
        super(context);
        init();
    }

    public GLSurfaceView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init();
    }

    private void init() {
        SurfaceHolder holder = getHolder();
        holder.addCallback(this);
        holder.setType(2);
        this.mRenderMode = 1;
    }

    public void setGLWrapper(GLWrapper gLWrapper) {
        this.mGLWrapper = gLWrapper;
    }

    public void setDebugFlags(int i) {
        this.mDebugFlags = i;
    }

    public int getDebugFlags() {
        return this.mDebugFlags;
    }

    public void setRenderer(Renderer renderer) {
        if (this.mRenderer != null) {
            throw new IllegalStateException("setRenderer has already been called for this instance.");
        }
        this.mRenderer = renderer;
    }

    public void setEGLConfigChooser(EGLConfigChooser eGLConfigChooser) {
        if (this.mRenderer != null) {
            throw new IllegalStateException("setRenderer has already been called for this instance.");
        }
        this.mEGLConfigChooser = eGLConfigChooser;
    }

    public void setEGLConfigChooser(boolean z) {
        setEGLConfigChooser(new SimpleEGLConfigChooser(z));
    }

    public void setEGLConfigChooser(int i, int i2, int i3, int i4, int i5, int i6) {
        setEGLConfigChooser(new ComponentSizeChooser(i, i2, i3, i4, i5, i6));
    }

    public void setRenderMode(int i) {
        this.mRenderMode = i;
        if (this.mGLThread != null) {
            this.mGLThread.setRenderMode(i);
        }
    }

    public int getRenderMode() {
        return this.mRenderMode;
    }

    public void requestRender() {
        this.mGLThread.requestRender();
    }

    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        if (this.mGLThread != null) {
            this.mGLThread.surfaceCreated();
        }
        this.mHasSurface = true;
    }

    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        if (this.mGLThread != null) {
            this.mGLThread.surfaceDestroyed();
        }
        this.mHasSurface = false;
    }

    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
        if (this.mGLThread != null) {
            this.mGLThread.onWindowResize(i2, i3);
        }
        this.mSurfaceWidth = i2;
        this.mSurfaceHeight = i3;
    }

    public void onPause() {
        this.mGLThread.onPause();
        this.mGLThread.requestExitAndWait();
        this.mGLThread = null;
    }

    public void onResume() {
        if (this.mEGLConfigChooser == null) {
            this.mEGLConfigChooser = new SimpleEGLConfigChooser(true);
        }
        this.mGLThread = new GLThread(this.mRenderer);
        this.mGLThread.start();
        this.mGLThread.setRenderMode(this.mRenderMode);
        if (this.mHasSurface) {
            this.mGLThread.surfaceCreated();
        }
        if (this.mSurfaceWidth > 0 && this.mSurfaceHeight > 0) {
            this.mGLThread.onWindowResize(this.mSurfaceWidth, this.mSurfaceHeight);
        }
        this.mGLThread.onResume();
    }

    public void queueEvent(Runnable runnable) {
        if (this.mGLThread != null) {
            this.mGLThread.queueEvent(runnable);
        }
    }

    class GLThread extends Thread {
        private boolean mDone = false;
        private EglHelper mEglHelper;
        private final ArrayList mEventQueue = new ArrayList();
        private boolean mHasSurface;
        private int mHeight = 0;
        private boolean mPaused;
        private int mRenderMode = 1;
        private final Renderer mRenderer;
        private boolean mRequestRender = true;
        private boolean mSizeChanged;
        private int mWidth = 0;

        GLThread(Renderer renderer) {
            this.mRenderer = renderer;
            this.mSizeChanged = true;
            setName("GLThread");
        }

        /* JADX WARNING: Code restructure failed: missing block: B:10:0x0025, code lost:
            org.anddev.andengine.opengl.view.GLSurfaceView.access$0().release();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:11:0x002c, code lost:
            throw r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:8:0x001c, code lost:
            org.anddev.andengine.opengl.view.GLSurfaceView.access$0().release();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:9:0x0024, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Removed duplicated region for block: B:9:0x0024 A[ExcHandler:  FINALLY, Splitter:B:0:0x0000] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r2 = this;
                java.util.concurrent.Semaphore r0 = org.anddev.andengine.opengl.view.GLSurfaceView.sEglSemaphore     // Catch:{ InterruptedException -> 0x0012, all -> 0x0024 }
                r0.acquire()     // Catch:{ InterruptedException -> 0x0012, all -> 0x0024 }
                r2.guardedRun()     // Catch:{ InterruptedException -> 0x001b, all -> 0x0024 }
                java.util.concurrent.Semaphore r0 = org.anddev.andengine.opengl.view.GLSurfaceView.sEglSemaphore
                r0.release()
            L_0x0011:
                return
            L_0x0012:
                r0 = move-exception
                java.util.concurrent.Semaphore r0 = org.anddev.andengine.opengl.view.GLSurfaceView.sEglSemaphore
                r0.release()
                goto L_0x0011
            L_0x001b:
                r0 = move-exception
                java.util.concurrent.Semaphore r0 = org.anddev.andengine.opengl.view.GLSurfaceView.sEglSemaphore
                r0.release()
                goto L_0x0011
            L_0x0024:
                r0 = move-exception
                java.util.concurrent.Semaphore r1 = org.anddev.andengine.opengl.view.GLSurfaceView.sEglSemaphore
                r1.release()
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: org.anddev.andengine.opengl.view.GLSurfaceView.GLThread.run():void");
        }

        /* JADX WARNING: Code restructure failed: missing block: B:10:0x0027, code lost:
            if (r11.mPaused == false) goto L_0x0099;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:11:0x0029, code lost:
            r11.mEglHelper.finish();
            r3 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:13:0x0033, code lost:
            if (needToWait() != false) goto L_0x0042;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:15:0x0037, code lost:
            if (r11.mDone == false) goto L_0x0046;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:23:0x0042, code lost:
            wait();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:24:0x0046, code lost:
            r4 = r11.mSizeChanged;
            r5 = r11.mWidth;
            r6 = r11.mHeight;
            r11.mSizeChanged = false;
            r11.mRequestRender = false;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:26:0x0053, code lost:
            if (r3 == false) goto L_0x0096;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:27:0x0055, code lost:
            r11.mEglHelper.start();
            r1 = true;
            r3 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:28:0x005c, code lost:
            if (r1 == false) goto L_0x0094;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:29:0x005e, code lost:
            r1 = (javax.microedition.khronos.opengles.GL10) r11.mEglHelper.createSurface(r11.this$0.getHolder());
            r0 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:30:0x006e, code lost:
            if (r3 == false) goto L_0x009b;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:31:0x0070, code lost:
            r11.mRenderer.onSurfaceCreated(r1, r11.mEglHelper.mEglConfig);
            r2 = false;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:32:0x007a, code lost:
            if (r0 == false) goto L_0x0082;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:33:0x007c, code lost:
            r11.mRenderer.onSurfaceChanged(r1, r5, r6);
            r0 = false;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:34:0x0082, code lost:
            if (r5 <= 0) goto L_0x0090;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:35:0x0084, code lost:
            if (r6 <= 0) goto L_0x0090;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:36:0x0086, code lost:
            r11.mRenderer.onDrawFrame(r1);
            r11.mEglHelper.swap();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:38:0x0094, code lost:
            r1 = r2;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:39:0x0096, code lost:
            r3 = r1;
            r1 = r4;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:40:0x0099, code lost:
            r3 = false;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:41:0x009b, code lost:
            r2 = r3;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private void guardedRun() {
            /*
                r11 = this;
                r9 = 0
                r8 = 1
                org.anddev.andengine.opengl.view.GLSurfaceView$EglHelper r0 = new org.anddev.andengine.opengl.view.GLSurfaceView$EglHelper
                org.anddev.andengine.opengl.view.GLSurfaceView r1 = org.anddev.andengine.opengl.view.GLSurfaceView.this
                r0.<init>()
                r11.mEglHelper = r0
                org.anddev.andengine.opengl.view.GLSurfaceView$EglHelper r0 = r11.mEglHelper
                r0.start()
                r0 = 0
                r1 = r8
                r2 = r0
                r0 = r8
            L_0x0014:
                boolean r3 = r11.mDone
                if (r3 == 0) goto L_0x001e
            L_0x0018:
                org.anddev.andengine.opengl.view.GLSurfaceView$EglHelper r0 = r11.mEglHelper
                r0.finish()
                return
            L_0x001e:
                monitor-enter(r11)
            L_0x001f:
                java.lang.Runnable r3 = r11.getEvent()     // Catch:{ all -> 0x003b }
                if (r3 != 0) goto L_0x003e
                boolean r3 = r11.mPaused     // Catch:{ all -> 0x003b }
                if (r3 == 0) goto L_0x0099
                org.anddev.andengine.opengl.view.GLSurfaceView$EglHelper r3 = r11.mEglHelper     // Catch:{ all -> 0x003b }
                r3.finish()     // Catch:{ all -> 0x003b }
                r3 = r8
            L_0x002f:
                boolean r4 = r11.needToWait()     // Catch:{ all -> 0x003b }
                if (r4 != 0) goto L_0x0042
                boolean r4 = r11.mDone     // Catch:{ all -> 0x003b }
                if (r4 == 0) goto L_0x0046
                monitor-exit(r11)     // Catch:{ all -> 0x003b }
                goto L_0x0018
            L_0x003b:
                r0 = move-exception
                monitor-exit(r11)     // Catch:{ all -> 0x003b }
                throw r0
            L_0x003e:
                r3.run()     // Catch:{ all -> 0x003b }
                goto L_0x001f
            L_0x0042:
                r11.wait()     // Catch:{ all -> 0x003b }
                goto L_0x002f
            L_0x0046:
                boolean r4 = r11.mSizeChanged     // Catch:{ all -> 0x003b }
                int r5 = r11.mWidth     // Catch:{ all -> 0x003b }
                int r6 = r11.mHeight     // Catch:{ all -> 0x003b }
                r7 = 0
                r11.mSizeChanged = r7     // Catch:{ all -> 0x003b }
                r7 = 0
                r11.mRequestRender = r7     // Catch:{ all -> 0x003b }
                monitor-exit(r11)     // Catch:{ all -> 0x003b }
                if (r3 == 0) goto L_0x0096
                org.anddev.andengine.opengl.view.GLSurfaceView$EglHelper r1 = r11.mEglHelper
                r1.start()
                r1 = r8
                r3 = r8
            L_0x005c:
                if (r1 == 0) goto L_0x0094
                org.anddev.andengine.opengl.view.GLSurfaceView$EglHelper r0 = r11.mEglHelper
                org.anddev.andengine.opengl.view.GLSurfaceView r1 = org.anddev.andengine.opengl.view.GLSurfaceView.this
                android.view.SurfaceHolder r1 = r1.getHolder()
                javax.microedition.khronos.opengles.GL r0 = r0.createSurface(r1)
                javax.microedition.khronos.opengles.GL10 r0 = (javax.microedition.khronos.opengles.GL10) r0
                r1 = r0
                r0 = r8
            L_0x006e:
                if (r3 == 0) goto L_0x009b
                org.anddev.andengine.opengl.view.GLSurfaceView$Renderer r2 = r11.mRenderer
                org.anddev.andengine.opengl.view.GLSurfaceView$EglHelper r3 = r11.mEglHelper
                javax.microedition.khronos.egl.EGLConfig r3 = r3.mEglConfig
                r2.onSurfaceCreated(r1, r3)
                r2 = r9
            L_0x007a:
                if (r0 == 0) goto L_0x0082
                org.anddev.andengine.opengl.view.GLSurfaceView$Renderer r0 = r11.mRenderer
                r0.onSurfaceChanged(r1, r5, r6)
                r0 = r9
            L_0x0082:
                if (r5 <= 0) goto L_0x0090
                if (r6 <= 0) goto L_0x0090
                org.anddev.andengine.opengl.view.GLSurfaceView$Renderer r3 = r11.mRenderer
                r3.onDrawFrame(r1)
                org.anddev.andengine.opengl.view.GLSurfaceView$EglHelper r3 = r11.mEglHelper
                r3.swap()
            L_0x0090:
                r10 = r2
                r2 = r1
                r1 = r10
                goto L_0x0014
            L_0x0094:
                r1 = r2
                goto L_0x006e
            L_0x0096:
                r3 = r1
                r1 = r4
                goto L_0x005c
            L_0x0099:
                r3 = r9
                goto L_0x002f
            L_0x009b:
                r2 = r3
                goto L_0x007a
            */
            throw new UnsupportedOperationException("Method not decompiled: org.anddev.andengine.opengl.view.GLSurfaceView.GLThread.guardedRun():void");
        }

        private boolean needToWait() {
            if (this.mDone) {
                return false;
            }
            if (this.mPaused || !this.mHasSurface) {
                return true;
            }
            if (this.mWidth <= 0 || this.mHeight <= 0 || (!this.mRequestRender && this.mRenderMode != 1)) {
                return true;
            }
            return false;
        }

        public void setRenderMode(int i) {
            if (i < 0 || i > 1) {
                throw new IllegalArgumentException("renderMode");
            }
            synchronized (this) {
                this.mRenderMode = i;
                if (i == 1) {
                    notify();
                }
            }
        }

        public int getRenderMode() {
            int i;
            synchronized (this) {
                i = this.mRenderMode;
            }
            return i;
        }

        public void requestRender() {
            synchronized (this) {
                this.mRequestRender = true;
                notify();
            }
        }

        public void surfaceCreated() {
            synchronized (this) {
                this.mHasSurface = true;
                notify();
            }
        }

        public void surfaceDestroyed() {
            synchronized (this) {
                this.mHasSurface = false;
                notify();
            }
        }

        public void onPause() {
            synchronized (this) {
                this.mPaused = true;
            }
        }

        public void onResume() {
            synchronized (this) {
                this.mPaused = false;
                notify();
            }
        }

        public void onWindowResize(int i, int i2) {
            synchronized (this) {
                this.mWidth = i;
                this.mHeight = i2;
                this.mSizeChanged = true;
                notify();
            }
        }

        public void requestExitAndWait() {
            synchronized (this) {
                this.mDone = true;
                notify();
            }
            try {
                join();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }

        public void queueEvent(Runnable runnable) {
            synchronized (this) {
                this.mEventQueue.add(runnable);
            }
        }

        private Runnable getEvent() {
            synchronized (this) {
                if (this.mEventQueue.size() <= 0) {
                    return null;
                }
                Runnable runnable = (Runnable) this.mEventQueue.remove(0);
                return runnable;
            }
        }
    }

    class EglHelper {
        EGL10 mEgl;
        EGLConfig mEglConfig;
        EGLContext mEglContext;
        EGLDisplay mEglDisplay;
        EGLSurface mEglSurface;

        public EglHelper() {
        }

        public void start() {
            this.mEgl = (EGL10) EGLContext.getEGL();
            this.mEglDisplay = this.mEgl.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);
            this.mEgl.eglInitialize(this.mEglDisplay, new int[2]);
            this.mEglConfig = GLSurfaceView.this.mEGLConfigChooser.chooseConfig(this.mEgl, this.mEglDisplay);
            this.mEglContext = this.mEgl.eglCreateContext(this.mEglDisplay, this.mEglConfig, EGL10.EGL_NO_CONTEXT, null);
            this.mEglSurface = null;
        }

        public GL createSurface(SurfaceHolder surfaceHolder) {
            if (this.mEglSurface != null) {
                this.mEgl.eglMakeCurrent(this.mEglDisplay, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_CONTEXT);
                this.mEgl.eglDestroySurface(this.mEglDisplay, this.mEglSurface);
            }
            this.mEglSurface = this.mEgl.eglCreateWindowSurface(this.mEglDisplay, this.mEglConfig, surfaceHolder, null);
            this.mEgl.eglMakeCurrent(this.mEglDisplay, this.mEglSurface, this.mEglSurface, this.mEglContext);
            GL gl = this.mEglContext.getGL();
            return GLSurfaceView.this.mGLWrapper != null ? GLSurfaceView.this.mGLWrapper.wrap(gl) : gl;
        }

        public boolean swap() {
            this.mEgl.eglSwapBuffers(this.mEglDisplay, this.mEglSurface);
            return this.mEgl.eglGetError() != 12302;
        }

        public void finish() {
            if (this.mEglSurface != null) {
                this.mEgl.eglMakeCurrent(this.mEglDisplay, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_CONTEXT);
                this.mEgl.eglDestroySurface(this.mEglDisplay, this.mEglSurface);
                this.mEglSurface = null;
            }
            if (this.mEglContext != null) {
                this.mEgl.eglDestroyContext(this.mEglDisplay, this.mEglContext);
                this.mEglContext = null;
            }
            if (this.mEglDisplay != null) {
                this.mEgl.eglTerminate(this.mEglDisplay);
                this.mEglDisplay = null;
            }
        }
    }
}
