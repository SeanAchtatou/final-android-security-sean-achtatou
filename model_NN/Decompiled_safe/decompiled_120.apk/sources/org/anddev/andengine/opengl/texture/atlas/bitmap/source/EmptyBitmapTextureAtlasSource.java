package org.anddev.andengine.opengl.texture.atlas.bitmap.source;

import android.graphics.Bitmap;
import org.anddev.andengine.opengl.texture.source.BaseTextureAtlasSource;

public class EmptyBitmapTextureAtlasSource extends BaseTextureAtlasSource implements IBitmapTextureAtlasSource {
    private final int mHeight;
    private final int mWidth;

    public EmptyBitmapTextureAtlasSource(int i, int i2) {
        this(0, 0, i, i2);
    }

    public EmptyBitmapTextureAtlasSource(int i, int i2, int i3, int i4) {
        super(i, i2);
        this.mWidth = i3;
        this.mHeight = i4;
    }

    public EmptyBitmapTextureAtlasSource clone() {
        return new EmptyBitmapTextureAtlasSource(this.mTexturePositionX, this.mTexturePositionY, this.mWidth, this.mHeight);
    }

    public int getWidth() {
        return this.mWidth;
    }

    public int getHeight() {
        return this.mHeight;
    }

    public Bitmap onLoadBitmap(Bitmap.Config config) {
        return Bitmap.createBitmap(this.mWidth, this.mHeight, config);
    }

    public String toString() {
        return String.valueOf(getClass().getSimpleName()) + "(" + this.mWidth + " x " + this.mHeight + ")";
    }
}
