package org.anddev.andengine.opengl.texture.region;

import org.anddev.andengine.opengl.texture.ITexture;
import org.anddev.andengine.opengl.texture.atlas.ITextureAtlas;
import org.anddev.andengine.opengl.texture.source.ITextureAtlasSource;

public class TextureRegionFactory {
    public static TextureRegion extractFromTexture(ITexture iTexture, int i, int i2, int i3, int i4, boolean z) {
        TextureRegion textureRegion = new TextureRegion(iTexture, i, i2, i3, i4);
        textureRegion.setTextureRegionBufferManaged(z);
        return textureRegion;
    }

    public static TextureRegion createFromSource(ITextureAtlas iTextureAtlas, ITextureAtlasSource iTextureAtlasSource, int i, int i2, boolean z) {
        TextureRegion textureRegion = new TextureRegion(iTextureAtlas, i, i2, iTextureAtlasSource.getWidth(), iTextureAtlasSource.getHeight());
        iTextureAtlas.addTextureAtlasSource(iTextureAtlasSource, textureRegion.getTexturePositionX(), textureRegion.getTexturePositionY());
        textureRegion.setTextureRegionBufferManaged(z);
        return textureRegion;
    }

    public static TiledTextureRegion createTiledFromSource(ITextureAtlas iTextureAtlas, ITextureAtlasSource iTextureAtlasSource, int i, int i2, int i3, int i4, boolean z) {
        TiledTextureRegion tiledTextureRegion = new TiledTextureRegion(iTextureAtlas, i, i2, iTextureAtlasSource.getWidth(), iTextureAtlasSource.getHeight(), i3, i4);
        iTextureAtlas.addTextureAtlasSource(iTextureAtlasSource, tiledTextureRegion.getTexturePositionX(), tiledTextureRegion.getTexturePositionY());
        tiledTextureRegion.setTextureRegionBufferManaged(z);
        return tiledTextureRegion;
    }
}
