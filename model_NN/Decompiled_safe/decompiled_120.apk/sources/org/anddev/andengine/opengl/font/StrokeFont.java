package org.anddev.andengine.opengl.font;

import android.graphics.Paint;
import android.graphics.Typeface;
import org.anddev.andengine.opengl.texture.ITexture;

public class StrokeFont extends Font {
    private final boolean mStrokeOnly;
    private final Paint mStrokePaint;

    public StrokeFont(ITexture iTexture, Typeface typeface, float f, boolean z, int i, float f2, int i2) {
        this(iTexture, typeface, f, z, i, f2, i2, false);
    }

    public StrokeFont(ITexture iTexture, Typeface typeface, float f, boolean z, int i, float f2, int i2, boolean z2) {
        super(iTexture, typeface, f, z, i);
        this.mStrokePaint = new Paint();
        this.mStrokePaint.setTypeface(typeface);
        this.mStrokePaint.setStyle(Paint.Style.STROKE);
        this.mStrokePaint.setStrokeWidth(f2);
        this.mStrokePaint.setColor(i2);
        this.mStrokePaint.setTextSize(f);
        this.mStrokePaint.setAntiAlias(z);
        this.mStrokeOnly = z2;
    }

    /* access modifiers changed from: protected */
    public void drawCharacterString(String str) {
        if (!this.mStrokeOnly) {
            super.drawCharacterString(str);
        }
        this.mCanvas.drawText(str, 0.0f, -this.mFontMetrics.ascent, this.mStrokePaint);
    }
}
