package org.anddev.andengine.opengl.texture.atlas.bitmap.source;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Picture;
import org.anddev.andengine.opengl.texture.source.BaseTextureAtlasSource;
import org.anddev.andengine.util.Debug;

public abstract class PictureBitmapTextureAtlasSource extends BaseTextureAtlasSource implements IBitmapTextureAtlasSource {
    protected final int mHeight;
    protected final Picture mPicture;
    protected final int mWidth;

    public abstract PictureBitmapTextureAtlasSource clone();

    public PictureBitmapTextureAtlasSource(Picture picture) {
        this(picture, 0, 0);
    }

    public PictureBitmapTextureAtlasSource(Picture picture, int i, int i2) {
        this(picture, i, i2, picture.getWidth(), picture.getHeight());
    }

    public PictureBitmapTextureAtlasSource(Picture picture, int i, int i2, float f) {
        this(picture, i, i2, Math.round(((float) picture.getWidth()) * f), Math.round(((float) picture.getHeight()) * f));
    }

    public PictureBitmapTextureAtlasSource(Picture picture, int i, int i2, int i3, int i4) {
        super(i, i2);
        this.mPicture = picture;
        this.mWidth = i3;
        this.mHeight = i4;
    }

    public int getWidth() {
        return this.mWidth;
    }

    public int getHeight() {
        return this.mHeight;
    }

    public Bitmap onLoadBitmap(Bitmap.Config config) {
        Picture picture = this.mPicture;
        if (picture == null) {
            Debug.e("Failed loading Bitmap in PictureBitmapTextureAtlasSource.");
            return null;
        }
        Bitmap createBitmap = Bitmap.createBitmap(this.mWidth, this.mHeight, config);
        Canvas canvas = new Canvas(createBitmap);
        canvas.scale(((float) this.mWidth) / ((float) this.mPicture.getWidth()), ((float) this.mHeight) / ((float) this.mPicture.getHeight()), 0.0f, 0.0f);
        picture.draw(canvas);
        return createBitmap;
    }
}
