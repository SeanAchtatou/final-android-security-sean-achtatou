package org.anddev.andengine.opengl.vertex;

import org.anddev.andengine.opengl.buffer.BufferObject;

public abstract class VertexBuffer extends BufferObject {
    public VertexBuffer(int i, int i2, boolean z) {
        super(i, i2, z);
    }
}
