package org.anddev.andengine.opengl.texture.atlas.buildable;

import java.util.ArrayList;
import javax.microedition.khronos.opengles.GL10;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.atlas.ITextureAtlas;
import org.anddev.andengine.opengl.texture.atlas.buildable.builder.ITextureBuilder;
import org.anddev.andengine.opengl.texture.source.ITextureAtlasSource;
import org.anddev.andengine.util.Callback;

public class BuildableTextureAtlas implements ITextureAtlas {
    private final ITextureAtlas mTextureAtlas;
    private final ArrayList mTextureAtlasSourcesToPlace = new ArrayList();

    public BuildableTextureAtlas(ITextureAtlas iTextureAtlas) {
        this.mTextureAtlas = iTextureAtlas;
    }

    public int getWidth() {
        return this.mTextureAtlas.getWidth();
    }

    public int getHeight() {
        return this.mTextureAtlas.getHeight();
    }

    public int getHardwareTextureID() {
        return this.mTextureAtlas.getHardwareTextureID();
    }

    public boolean isLoadedToHardware() {
        return this.mTextureAtlas.isLoadedToHardware();
    }

    public void setLoadedToHardware(boolean z) {
        this.mTextureAtlas.setLoadedToHardware(z);
    }

    public boolean isUpdateOnHardwareNeeded() {
        return this.mTextureAtlas.isUpdateOnHardwareNeeded();
    }

    public void setUpdateOnHardwareNeeded(boolean z) {
        this.mTextureAtlas.setUpdateOnHardwareNeeded(z);
    }

    public void loadToHardware(GL10 gl10) {
        this.mTextureAtlas.loadToHardware(gl10);
    }

    public void unloadFromHardware(GL10 gl10) {
        this.mTextureAtlas.unloadFromHardware(gl10);
    }

    public void reloadToHardware(GL10 gl10) {
        this.mTextureAtlas.reloadToHardware(gl10);
    }

    public void bind(GL10 gl10) {
        this.mTextureAtlas.bind(gl10);
    }

    public TextureOptions getTextureOptions() {
        return this.mTextureAtlas.getTextureOptions();
    }

    public void addTextureAtlasSource(ITextureAtlasSource iTextureAtlasSource, int i, int i2) {
        this.mTextureAtlas.addTextureAtlasSource(iTextureAtlasSource, i, i2);
    }

    public void removeTextureAtlasSource(ITextureAtlasSource iTextureAtlasSource, int i, int i2) {
        this.mTextureAtlas.removeTextureAtlasSource(iTextureAtlasSource, i, i2);
    }

    public void clearTextureAtlasSources() {
        this.mTextureAtlas.clearTextureAtlasSources();
        this.mTextureAtlasSourcesToPlace.clear();
    }

    public boolean hasTextureStateListener() {
        return this.mTextureAtlas.hasTextureStateListener();
    }

    public ITextureAtlas.ITextureAtlasStateListener getTextureStateListener() {
        return this.mTextureAtlas.getTextureStateListener();
    }

    public void addTextureAtlasSource(ITextureAtlasSource iTextureAtlasSource, Callback callback) {
        this.mTextureAtlasSourcesToPlace.add(new TextureAtlasSourceWithWithLocationCallback(iTextureAtlasSource, callback));
    }

    public void removeTextureAtlasSource(ITextureAtlasSource iTextureAtlasSource) {
        ArrayList arrayList = this.mTextureAtlasSourcesToPlace;
        for (int size = arrayList.size() - 1; size >= 0; size--) {
            if (((TextureAtlasSourceWithWithLocationCallback) arrayList.get(size)).mTextureAtlasSource == iTextureAtlasSource) {
                arrayList.remove(size);
                this.mTextureAtlas.setUpdateOnHardwareNeeded(true);
                return;
            }
        }
    }

    public void build(ITextureBuilder iTextureBuilder) {
        iTextureBuilder.pack(this.mTextureAtlas, this.mTextureAtlasSourcesToPlace);
        this.mTextureAtlasSourcesToPlace.clear();
        this.mTextureAtlas.setUpdateOnHardwareNeeded(true);
    }

    public class TextureAtlasSourceWithWithLocationCallback {
        private final Callback mCallback;
        /* access modifiers changed from: private */
        public final ITextureAtlasSource mTextureAtlasSource;

        public TextureAtlasSourceWithWithLocationCallback(ITextureAtlasSource iTextureAtlasSource, Callback callback) {
            this.mTextureAtlasSource = iTextureAtlasSource;
            this.mCallback = callback;
        }

        public Callback getCallback() {
            return this.mCallback;
        }

        public ITextureAtlasSource getTextureAtlasSource() {
            return this.mTextureAtlasSource;
        }
    }
}
