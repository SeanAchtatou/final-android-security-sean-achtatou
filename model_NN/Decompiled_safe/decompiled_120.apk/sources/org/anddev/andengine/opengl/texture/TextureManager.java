package org.anddev.andengine.opengl.texture;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import javax.microedition.khronos.opengles.GL10;
import org.anddev.andengine.util.Debug;

public class TextureManager {
    private final ArrayList mTexturesLoaded = new ArrayList();
    private final HashSet mTexturesManaged = new HashSet();
    private final ArrayList mTexturesToBeLoaded = new ArrayList();
    private final ArrayList mTexturesToBeUnloaded = new ArrayList();

    /* access modifiers changed from: protected */
    public void clear() {
        this.mTexturesToBeLoaded.clear();
        this.mTexturesLoaded.clear();
        this.mTexturesManaged.clear();
    }

    public boolean loadTexture(ITexture iTexture) {
        if (this.mTexturesManaged.contains(iTexture)) {
            this.mTexturesToBeUnloaded.remove(iTexture);
            return false;
        }
        this.mTexturesManaged.add(iTexture);
        this.mTexturesToBeLoaded.add(iTexture);
        return true;
    }

    public boolean unloadTexture(ITexture iTexture) {
        if (!this.mTexturesManaged.contains(iTexture)) {
            return false;
        }
        if (this.mTexturesLoaded.contains(iTexture)) {
            this.mTexturesToBeUnloaded.add(iTexture);
        } else if (this.mTexturesToBeLoaded.remove(iTexture)) {
            this.mTexturesManaged.remove(iTexture);
        }
        return true;
    }

    public void loadTextures(ITexture... iTextureArr) {
        for (int length = iTextureArr.length - 1; length >= 0; length--) {
            loadTexture(iTextureArr[length]);
        }
    }

    public void unloadTextures(ITexture... iTextureArr) {
        for (int length = iTextureArr.length - 1; length >= 0; length--) {
            unloadTexture(iTextureArr[length]);
        }
    }

    public void reloadTextures() {
        Iterator it = this.mTexturesManaged.iterator();
        while (it.hasNext()) {
            ((ITexture) it.next()).setLoadedToHardware(false);
        }
        this.mTexturesToBeLoaded.addAll(this.mTexturesLoaded);
        this.mTexturesLoaded.clear();
        this.mTexturesManaged.removeAll(this.mTexturesToBeUnloaded);
        this.mTexturesToBeUnloaded.clear();
    }

    public void updateTextures(GL10 gl10) {
        HashSet hashSet = this.mTexturesManaged;
        ArrayList arrayList = this.mTexturesLoaded;
        ArrayList arrayList2 = this.mTexturesToBeLoaded;
        ArrayList arrayList3 = this.mTexturesToBeUnloaded;
        int size = arrayList.size();
        if (size > 0) {
            for (int i = size - 1; i >= 0; i--) {
                ITexture iTexture = (ITexture) arrayList.get(i);
                if (iTexture.isUpdateOnHardwareNeeded()) {
                    try {
                        iTexture.reloadToHardware(gl10);
                    } catch (IOException e) {
                        Debug.e(e);
                    }
                }
            }
        }
        int size2 = arrayList2.size();
        if (size2 > 0) {
            for (int i2 = size2 - 1; i2 >= 0; i2--) {
                ITexture iTexture2 = (ITexture) arrayList2.remove(i2);
                if (!iTexture2.isLoadedToHardware()) {
                    try {
                        iTexture2.loadToHardware(gl10);
                    } catch (IOException e2) {
                        Debug.e(e2);
                    }
                }
                arrayList.add(iTexture2);
            }
        }
        int size3 = arrayList3.size();
        if (size3 > 0) {
            for (int i3 = size3 - 1; i3 >= 0; i3--) {
                ITexture iTexture3 = (ITexture) arrayList3.remove(i3);
                if (iTexture3.isLoadedToHardware()) {
                    iTexture3.unloadFromHardware(gl10);
                }
                arrayList.remove(iTexture3);
                hashSet.remove(iTexture3);
            }
        }
        if (size2 > 0 || size3 > 0) {
            System.gc();
        }
    }
}
