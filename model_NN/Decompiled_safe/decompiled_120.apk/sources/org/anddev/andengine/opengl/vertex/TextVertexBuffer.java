package org.anddev.andengine.opengl.vertex;

import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.opengl.font.Font;
import org.anddev.andengine.opengl.font.Letter;
import org.anddev.andengine.opengl.util.FastFloatBuffer;
import org.anddev.andengine.util.HorizontalAlign;

public class TextVertexBuffer extends VertexBuffer {
    private static /* synthetic */ int[] $SWITCH_TABLE$org$anddev$andengine$util$HorizontalAlign = null;
    public static final int VERTICES_PER_CHARACTER = 6;
    private final HorizontalAlign mHorizontalAlign;

    static /* synthetic */ int[] $SWITCH_TABLE$org$anddev$andengine$util$HorizontalAlign() {
        int[] iArr = $SWITCH_TABLE$org$anddev$andengine$util$HorizontalAlign;
        if (iArr == null) {
            iArr = new int[HorizontalAlign.values().length];
            try {
                iArr[HorizontalAlign.CENTER.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[HorizontalAlign.LEFT.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[HorizontalAlign.RIGHT.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$org$anddev$andengine$util$HorizontalAlign = iArr;
        }
        return iArr;
    }

    public TextVertexBuffer(int i, HorizontalAlign horizontalAlign, int i2, boolean z) {
        super(i * 12, i2, z);
        this.mHorizontalAlign = horizontalAlign;
    }

    public synchronized void update(Font font, int i, int[] iArr, String[] strArr) {
        int i2;
        int[] iArr2 = this.mBufferData;
        int lineHeight = font.getLineHeight();
        int length = strArr.length;
        int i3 = 0;
        int i4 = 0;
        while (i4 < length) {
            String str = strArr[i4];
            switch ($SWITCH_TABLE$org$anddev$andengine$util$HorizontalAlign()[this.mHorizontalAlign.ordinal()]) {
                case 2:
                    i2 = (i - iArr[i4]) >> 1;
                    break;
                case TouchEvent.ACTION_CANCEL:
                    i2 = i - iArr[i4];
                    break;
                default:
                    i2 = 0;
                    break;
            }
            int lineHeight2 = (font.getLineHeight() + font.getLineGap()) * i4;
            int floatToRawIntBits = Float.floatToRawIntBits((float) lineHeight2);
            int length2 = str.length();
            int i5 = i3;
            for (int i6 = 0; i6 < length2; i6++) {
                Letter letter = font.getLetter(str.charAt(i6));
                int floatToRawIntBits2 = Float.floatToRawIntBits((float) i2);
                int floatToRawIntBits3 = Float.floatToRawIntBits((float) (letter.mWidth + i2));
                int floatToRawIntBits4 = Float.floatToRawIntBits((float) (lineHeight2 + lineHeight));
                int i7 = i5 + 1;
                iArr2[i5] = floatToRawIntBits2;
                int i8 = i7 + 1;
                iArr2[i7] = floatToRawIntBits;
                int i9 = i8 + 1;
                iArr2[i8] = floatToRawIntBits2;
                int i10 = i9 + 1;
                iArr2[i9] = floatToRawIntBits4;
                int i11 = i10 + 1;
                iArr2[i10] = floatToRawIntBits3;
                int i12 = i11 + 1;
                iArr2[i11] = floatToRawIntBits4;
                int i13 = i12 + 1;
                iArr2[i12] = floatToRawIntBits3;
                int i14 = i13 + 1;
                iArr2[i13] = floatToRawIntBits4;
                int i15 = i14 + 1;
                iArr2[i14] = floatToRawIntBits3;
                int i16 = i15 + 1;
                iArr2[i15] = floatToRawIntBits;
                int i17 = i16 + 1;
                iArr2[i16] = floatToRawIntBits2;
                i5 = i17 + 1;
                iArr2[i17] = floatToRawIntBits;
                i2 += letter.mAdvance;
            }
            i4++;
            i3 = i5;
        }
        FastFloatBuffer fastFloatBuffer = this.mFloatBuffer;
        fastFloatBuffer.position(0);
        fastFloatBuffer.put(iArr2);
        fastFloatBuffer.position(0);
        super.setHardwareBufferNeedsUpdate();
    }
}
