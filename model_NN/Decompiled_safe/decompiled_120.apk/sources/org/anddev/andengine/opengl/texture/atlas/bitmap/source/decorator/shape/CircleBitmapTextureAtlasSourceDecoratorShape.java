package org.anddev.andengine.opengl.texture.atlas.bitmap.source.decorator.shape;

import android.graphics.Canvas;
import android.graphics.Paint;
import org.anddev.andengine.opengl.texture.atlas.bitmap.source.decorator.BaseBitmapTextureAtlasSourceDecorator;

public class CircleBitmapTextureAtlasSourceDecoratorShape implements IBitmapTextureAtlasSourceDecoratorShape {
    private static CircleBitmapTextureAtlasSourceDecoratorShape sDefaultInstance;

    public static CircleBitmapTextureAtlasSourceDecoratorShape getDefaultInstance() {
        if (sDefaultInstance == null) {
            sDefaultInstance = new CircleBitmapTextureAtlasSourceDecoratorShape();
        }
        return sDefaultInstance;
    }

    public void onDecorateBitmap(Canvas canvas, Paint paint, BaseBitmapTextureAtlasSourceDecorator.TextureAtlasSourceDecoratorOptions textureAtlasSourceDecoratorOptions) {
        canvas.drawCircle(((((float) canvas.getWidth()) + textureAtlasSourceDecoratorOptions.getInsetLeft()) - textureAtlasSourceDecoratorOptions.getInsetRight()) * 0.5f, ((((float) canvas.getHeight()) + textureAtlasSourceDecoratorOptions.getInsetTop()) - textureAtlasSourceDecoratorOptions.getInsetBottom()) * 0.5f, Math.min(((((float) canvas.getWidth()) - textureAtlasSourceDecoratorOptions.getInsetLeft()) - textureAtlasSourceDecoratorOptions.getInsetRight()) * 0.5f, ((((float) canvas.getHeight()) - textureAtlasSourceDecoratorOptions.getInsetTop()) - textureAtlasSourceDecoratorOptions.getInsetBottom()) * 0.5f), paint);
    }
}
