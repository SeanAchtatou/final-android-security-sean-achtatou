package org.anddev.andengine.opengl.texture.region;

import org.anddev.andengine.opengl.texture.ITexture;

public class TextureRegion extends BaseTextureRegion {
    public TextureRegion(ITexture iTexture, int i, int i2, int i3, int i4) {
        super(iTexture, i, i2, i3, i4);
    }

    public TextureRegion clone() {
        return new TextureRegion(this.mTexture, this.mTexturePositionX, this.mTexturePositionY, this.mWidth, this.mHeight);
    }

    public float getTextureCoordinateX1() {
        return ((float) this.mTexturePositionX) / ((float) this.mTexture.getWidth());
    }

    public float getTextureCoordinateY1() {
        return ((float) this.mTexturePositionY) / ((float) this.mTexture.getHeight());
    }

    public float getTextureCoordinateX2() {
        return ((float) (this.mTexturePositionX + this.mWidth)) / ((float) this.mTexture.getWidth());
    }

    public float getTextureCoordinateY2() {
        return ((float) (this.mTexturePositionY + this.mHeight)) / ((float) this.mTexture.getHeight());
    }

    public int getTextureCropLeft() {
        return getTexturePositionX();
    }

    public int getTextureCropTop() {
        return getTexturePositionY();
    }

    public int getTextureCropWidth() {
        return getWidth();
    }

    public int getTextureCropHeight() {
        return getHeight();
    }
}
