package org.anddev.andengine.opengl.texture.bitmap;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLUtils;
import java.io.InputStream;
import javax.microedition.khronos.opengles.GL10;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.opengl.texture.ITexture;
import org.anddev.andengine.opengl.texture.Texture;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.util.GLHelper;
import org.anddev.andengine.util.MathUtils;
import org.anddev.andengine.util.StreamUtils;

public abstract class BitmapTexture extends Texture {
    private final BitmapTextureFormat mBitmapTextureFormat;
    private final int mHeight;
    private final int mWidth;

    /* access modifiers changed from: protected */
    public abstract InputStream onGetInputStream();

    public BitmapTexture() {
        this(BitmapTextureFormat.RGBA_8888, TextureOptions.DEFAULT, null);
    }

    public BitmapTexture(BitmapTextureFormat bitmapTextureFormat) {
        this(bitmapTextureFormat, TextureOptions.DEFAULT, null);
    }

    public BitmapTexture(TextureOptions textureOptions) {
        this(BitmapTextureFormat.RGBA_8888, textureOptions, null);
    }

    public BitmapTexture(BitmapTextureFormat bitmapTextureFormat, TextureOptions textureOptions) {
        this(bitmapTextureFormat, textureOptions, null);
    }

    /* JADX INFO: finally extract failed */
    public BitmapTexture(BitmapTextureFormat bitmapTextureFormat, TextureOptions textureOptions, ITexture.ITextureStateListener iTextureStateListener) {
        super(bitmapTextureFormat.getPixelFormat(), textureOptions, iTextureStateListener);
        this.mBitmapTextureFormat = bitmapTextureFormat;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        try {
            BitmapFactory.decodeStream(onGetInputStream(), null, options);
            StreamUtils.close(null);
            this.mWidth = options.outWidth;
            this.mHeight = options.outHeight;
            if (!MathUtils.isPowerOfTwo(this.mWidth) || !MathUtils.isPowerOfTwo(this.mHeight)) {
                throw new IllegalArgumentException("pWidth and pHeight must be a power of 2!");
            }
        } catch (Throwable th) {
            StreamUtils.close(null);
            throw th;
        }
    }

    public int getWidth() {
        return this.mWidth;
    }

    public int getHeight() {
        return this.mHeight;
    }

    /* access modifiers changed from: protected */
    public void writeTextureToHardware(GL10 gl10) {
        Bitmap.Config bitmapConfig = this.mBitmapTextureFormat.getBitmapConfig();
        boolean z = this.mTextureOptions.mPreMultipyAlpha;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = bitmapConfig;
        Bitmap decodeStream = BitmapFactory.decodeStream(onGetInputStream(), null, options);
        if (z) {
            GLUtils.texImage2D(3553, 0, decodeStream, 0);
        } else {
            GLHelper.glTexImage2D(gl10, 3553, 0, decodeStream, 0, this.mPixelFormat);
        }
        decodeStream.recycle();
    }

    public enum BitmapTextureFormat {
        RGBA_8888(Bitmap.Config.ARGB_8888, Texture.PixelFormat.RGBA_8888),
        RGB_565(Bitmap.Config.RGB_565, Texture.PixelFormat.RGB_565),
        RGBA_4444(Bitmap.Config.ARGB_4444, Texture.PixelFormat.RGBA_4444),
        A_8(Bitmap.Config.ALPHA_8, Texture.PixelFormat.A_8);
        
        private static /* synthetic */ int[] $SWITCH_TABLE$org$anddev$andengine$opengl$texture$Texture$PixelFormat;
        private final Bitmap.Config mBitmapConfig;
        private final Texture.PixelFormat mPixelFormat;

        private BitmapTextureFormat(Bitmap.Config config, Texture.PixelFormat pixelFormat) {
            this.mBitmapConfig = config;
            this.mPixelFormat = pixelFormat;
        }

        public static BitmapTextureFormat fromPixelFormat(Texture.PixelFormat pixelFormat) {
            switch ($SWITCH_TABLE$org$anddev$andengine$opengl$texture$Texture$PixelFormat()[pixelFormat.ordinal()]) {
                case 2:
                    return RGBA_4444;
                case TouchEvent.ACTION_CANCEL:
                default:
                    throw new IllegalArgumentException("Unsupported " + Texture.PixelFormat.class.getName() + ": '" + pixelFormat + "'.");
                case 4:
                    return RGBA_8888;
                case 5:
                    return RGB_565;
                case 6:
                    return A_8;
            }
        }

        public final Bitmap.Config getBitmapConfig() {
            return this.mBitmapConfig;
        }

        public final Texture.PixelFormat getPixelFormat() {
            return this.mPixelFormat;
        }
    }
}
