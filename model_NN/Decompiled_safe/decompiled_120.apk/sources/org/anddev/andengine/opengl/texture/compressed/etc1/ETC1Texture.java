package org.anddev.andengine.opengl.texture.compressed.etc1;

import android.opengl.ETC1;
import android.opengl.ETC1Util;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import javax.microedition.khronos.opengles.GL10;
import org.anddev.andengine.opengl.texture.ITexture;
import org.anddev.andengine.opengl.texture.Texture;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.util.StreamUtils;

public abstract class ETC1Texture extends Texture {
    private ETC1TextureHeader mETC1TextureHeader;

    /* access modifiers changed from: protected */
    public abstract InputStream getInputStream();

    public ETC1Texture() {
        this(TextureOptions.DEFAULT, null);
    }

    public ETC1Texture(ITexture.ITextureStateListener iTextureStateListener) {
        this(TextureOptions.DEFAULT, iTextureStateListener);
    }

    public ETC1Texture(TextureOptions textureOptions) {
        this(textureOptions, null);
    }

    public ETC1Texture(TextureOptions textureOptions, ITexture.ITextureStateListener iTextureStateListener) {
        super(Texture.PixelFormat.RGB_565, textureOptions, iTextureStateListener);
        InputStream inputStream;
        Throwable th;
        try {
            InputStream inputStream2 = getInputStream();
            try {
                this.mETC1TextureHeader = new ETC1TextureHeader(StreamUtils.streamToBytes(inputStream2, 16));
                StreamUtils.close(inputStream2);
            } catch (Throwable th2) {
                Throwable th3 = th2;
                inputStream = inputStream2;
                th = th3;
                StreamUtils.close(inputStream);
                throw th;
            }
        } catch (Throwable th4) {
            Throwable th5 = th4;
            inputStream = null;
            th = th5;
        }
    }

    public int getWidth() {
        return this.mETC1TextureHeader.getWidth();
    }

    public int getHeight() {
        return this.mETC1TextureHeader.getHeight();
    }

    /* access modifiers changed from: protected */
    public void writeTextureToHardware(GL10 gl10) {
        ETC1Util.loadTexture(3553, 0, 0, this.mPixelFormat.getGLFormat(), this.mPixelFormat.getGLType(), getInputStream());
    }

    public class ETC1TextureHeader {
        private final ByteBuffer mDataByteBuffer;
        private final int mHeight;
        private final int mWidth;

        public ETC1TextureHeader(byte[] bArr) {
            if (bArr.length != 16) {
                throw new IllegalArgumentException("Invalid " + getClass().getSimpleName() + "!");
            }
            this.mDataByteBuffer = ByteBuffer.allocateDirect(16).order(ByteOrder.nativeOrder());
            this.mDataByteBuffer.put(bArr, 0, 16);
            this.mDataByteBuffer.position(0);
            if (!ETC1.isValid(this.mDataByteBuffer)) {
                throw new IllegalArgumentException("Invalid " + getClass().getSimpleName() + "!");
            }
            this.mWidth = ETC1.getWidth(this.mDataByteBuffer);
            this.mHeight = ETC1.getHeight(this.mDataByteBuffer);
        }

        public int getWidth() {
            return this.mWidth;
        }

        public int getHeight() {
            return this.mHeight;
        }
    }
}
