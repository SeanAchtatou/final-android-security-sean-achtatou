package org.anddev.andengine.opengl.view;

import android.content.Context;
import android.util.AttributeSet;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;
import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.opengl.util.GLHelper;
import org.anddev.andengine.opengl.view.GLSurfaceView;
import org.anddev.andengine.util.Debug;

public class RenderSurfaceView extends GLSurfaceView {
    private Renderer mRenderer;

    public RenderSurfaceView(Context context) {
        super(context);
    }

    public RenderSurfaceView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void setRenderer(Engine engine) {
        setOnTouchListener(engine);
        this.mRenderer = new Renderer(engine);
        setRenderer(this.mRenderer);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        this.mRenderer.mEngine.getEngineOptions().getResolutionPolicy().onMeasure(this, i, i2);
    }

    public void setMeasuredDimensionProxy(int i, int i2) {
        setMeasuredDimension(i, i2);
    }

    public class Renderer implements GLSurfaceView.Renderer {
        /* access modifiers changed from: private */
        public final Engine mEngine;

        public Renderer(Engine engine) {
            this.mEngine = engine;
        }

        public void onSurfaceChanged(GL10 gl10, int i, int i2) {
            Debug.d("onSurfaceChanged: pWidth=" + i + "  pHeight=" + i2);
            this.mEngine.setSurfaceSize(i, i2);
            gl10.glViewport(0, 0, i, i2);
            gl10.glLoadIdentity();
        }

        public void onSurfaceCreated(GL10 gl10, EGLConfig eGLConfig) {
            Debug.d("onSurfaceCreated");
            GLHelper.reset(gl10);
            GLHelper.setPerspectiveCorrectionHintFastest(gl10);
            GLHelper.setShadeModelFlat(gl10);
            GLHelper.disableLightning(gl10);
            GLHelper.disableDither(gl10);
            GLHelper.disableDepthTest(gl10);
            GLHelper.disableMultisample(gl10);
            GLHelper.enableBlend(gl10);
            GLHelper.enableTextures(gl10);
            GLHelper.enableTexCoordArray(gl10);
            GLHelper.enableVertexArray(gl10);
            GLHelper.enableCulling(gl10);
            gl10.glFrontFace(2305);
            gl10.glCullFace(1029);
            GLHelper.enableExtensions(gl10, this.mEngine.getEngineOptions().getRenderOptions());
        }

        public void onDrawFrame(GL10 gl10) {
            try {
                this.mEngine.onDrawFrame(gl10);
            } catch (InterruptedException e) {
                Debug.e("GLThread interrupted!", e);
            }
        }
    }
}
