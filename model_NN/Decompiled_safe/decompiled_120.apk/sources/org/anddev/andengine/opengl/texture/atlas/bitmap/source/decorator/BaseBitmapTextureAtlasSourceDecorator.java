package org.anddev.andengine.opengl.texture.atlas.bitmap.source.decorator;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import org.anddev.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource;
import org.anddev.andengine.opengl.texture.source.BaseTextureAtlasSource;

public abstract class BaseBitmapTextureAtlasSourceDecorator extends BaseTextureAtlasSource implements IBitmapTextureAtlasSource {
    protected final IBitmapTextureAtlasSource mBitmapTextureAtlasSource;
    protected Paint mPaint;
    protected TextureAtlasSourceDecoratorOptions mTextureAtlasSourceDecoratorOptions;

    public abstract BaseBitmapTextureAtlasSourceDecorator clone();

    /* access modifiers changed from: protected */
    public abstract void onDecorateBitmap(Canvas canvas);

    public BaseBitmapTextureAtlasSourceDecorator(IBitmapTextureAtlasSource iBitmapTextureAtlasSource) {
        this(iBitmapTextureAtlasSource, new TextureAtlasSourceDecoratorOptions());
    }

    public BaseBitmapTextureAtlasSourceDecorator(IBitmapTextureAtlasSource iBitmapTextureAtlasSource, TextureAtlasSourceDecoratorOptions textureAtlasSourceDecoratorOptions) {
        super(iBitmapTextureAtlasSource.getTexturePositionX(), iBitmapTextureAtlasSource.getTexturePositionY());
        TextureAtlasSourceDecoratorOptions textureAtlasSourceDecoratorOptions2;
        this.mPaint = new Paint();
        this.mBitmapTextureAtlasSource = iBitmapTextureAtlasSource;
        if (textureAtlasSourceDecoratorOptions == null) {
            textureAtlasSourceDecoratorOptions2 = new TextureAtlasSourceDecoratorOptions();
        } else {
            textureAtlasSourceDecoratorOptions2 = textureAtlasSourceDecoratorOptions;
        }
        this.mTextureAtlasSourceDecoratorOptions = textureAtlasSourceDecoratorOptions2;
        this.mPaint.setAntiAlias(this.mTextureAtlasSourceDecoratorOptions.getAntiAliasing());
    }

    public Paint getPaint() {
        return this.mPaint;
    }

    public void setPaint(Paint paint) {
        this.mPaint = paint;
    }

    public TextureAtlasSourceDecoratorOptions getTextureAtlasSourceDecoratorOptions() {
        return this.mTextureAtlasSourceDecoratorOptions;
    }

    public void setTextureAtlasSourceDecoratorOptions(TextureAtlasSourceDecoratorOptions textureAtlasSourceDecoratorOptions) {
        this.mTextureAtlasSourceDecoratorOptions = textureAtlasSourceDecoratorOptions;
    }

    public int getWidth() {
        return this.mBitmapTextureAtlasSource.getWidth();
    }

    public int getHeight() {
        return this.mBitmapTextureAtlasSource.getHeight();
    }

    public Bitmap onLoadBitmap(Bitmap.Config config) {
        Bitmap ensureLoadedBitmapIsMutable = ensureLoadedBitmapIsMutable(this.mBitmapTextureAtlasSource.onLoadBitmap(config));
        onDecorateBitmap(new Canvas(ensureLoadedBitmapIsMutable));
        return ensureLoadedBitmapIsMutable;
    }

    private Bitmap ensureLoadedBitmapIsMutable(Bitmap bitmap) {
        if (bitmap.isMutable()) {
            return bitmap;
        }
        Bitmap copy = bitmap.copy(bitmap.getConfig(), true);
        bitmap.recycle();
        return copy;
    }

    public class TextureAtlasSourceDecoratorOptions {
        public static final TextureAtlasSourceDecoratorOptions DEFAULT = new TextureAtlasSourceDecoratorOptions();
        private boolean mAntiAliasing;
        private float mInsetBottom = 0.25f;
        private float mInsetLeft = 0.25f;
        private float mInsetRight = 0.25f;
        private float mInsetTop = 0.25f;

        /* access modifiers changed from: protected */
        public TextureAtlasSourceDecoratorOptions clone() {
            TextureAtlasSourceDecoratorOptions textureAtlasSourceDecoratorOptions = new TextureAtlasSourceDecoratorOptions();
            textureAtlasSourceDecoratorOptions.setInsets(this.mInsetLeft, this.mInsetTop, this.mInsetRight, this.mInsetBottom);
            textureAtlasSourceDecoratorOptions.setAntiAliasing(this.mAntiAliasing);
            return textureAtlasSourceDecoratorOptions;
        }

        public boolean getAntiAliasing() {
            return this.mAntiAliasing;
        }

        public float getInsetLeft() {
            return this.mInsetLeft;
        }

        public float getInsetRight() {
            return this.mInsetRight;
        }

        public float getInsetTop() {
            return this.mInsetTop;
        }

        public float getInsetBottom() {
            return this.mInsetBottom;
        }

        public TextureAtlasSourceDecoratorOptions setAntiAliasing(boolean z) {
            this.mAntiAliasing = z;
            return this;
        }

        public TextureAtlasSourceDecoratorOptions setInsetLeft(float f) {
            this.mInsetLeft = f;
            return this;
        }

        public TextureAtlasSourceDecoratorOptions setInsetRight(float f) {
            this.mInsetRight = f;
            return this;
        }

        public TextureAtlasSourceDecoratorOptions setInsetTop(float f) {
            this.mInsetTop = f;
            return this;
        }

        public TextureAtlasSourceDecoratorOptions setInsetBottom(float f) {
            this.mInsetBottom = f;
            return this;
        }

        public TextureAtlasSourceDecoratorOptions setInsets(float f) {
            return setInsets(f, f, f, f);
        }

        public TextureAtlasSourceDecoratorOptions setInsets(float f, float f2, float f3, float f4) {
            this.mInsetLeft = f;
            this.mInsetTop = f2;
            this.mInsetRight = f3;
            this.mInsetBottom = f4;
            return this;
        }
    }
}
