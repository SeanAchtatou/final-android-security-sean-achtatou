package org.anddev.andengine.opengl.font;

class Size {
    private float mHeight;
    private float mWidth;

    public Size() {
        this(0.0f, 0.0f);
    }

    public Size(float f, float f2) {
        setWidth(f);
        setHeight(f2);
    }

    public void setWidth(float f) {
        this.mWidth = f;
    }

    public float getWidth() {
        return this.mWidth;
    }

    public void setHeight(float f) {
        this.mHeight = f;
    }

    public float getHeight() {
        return this.mHeight;
    }

    public void set(int i, int i2) {
        setWidth((float) i);
        setHeight((float) i2);
    }
}
