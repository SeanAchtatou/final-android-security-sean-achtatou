package org.anddev.andengine.opengl.texture.atlas.bitmap.source;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import java.io.IOException;
import java.io.InputStream;
import org.anddev.andengine.opengl.texture.source.BaseTextureAtlasSource;
import org.anddev.andengine.util.Debug;
import org.anddev.andengine.util.StreamUtils;

public class AssetBitmapTextureAtlasSource extends BaseTextureAtlasSource implements IBitmapTextureAtlasSource {
    private final String mAssetPath;
    private final Context mContext;
    private final int mHeight;
    private final int mWidth;

    public AssetBitmapTextureAtlasSource(Context context, String str) {
        this(context, str, 0, 0);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AssetBitmapTextureAtlasSource(Context context, String str, int i, int i2) {
        super(i, i2);
        InputStream inputStream;
        InputStream inputStream2 = null;
        this.mContext = context;
        this.mAssetPath = str;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        try {
            inputStream = context.getAssets().open(str);
            try {
                BitmapFactory.decodeStream(inputStream, null, options);
                StreamUtils.close(inputStream);
            } catch (IOException e) {
                IOException iOException = e;
                inputStream2 = inputStream;
                e = iOException;
                try {
                    Debug.e("Failed loading Bitmap in AssetBitmapTextureAtlasSource. AssetPath: " + str, e);
                    StreamUtils.close(inputStream2);
                    this.mWidth = options.outWidth;
                    this.mHeight = options.outHeight;
                } catch (Throwable th) {
                    th = th;
                    inputStream = inputStream2;
                    StreamUtils.close(inputStream);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                StreamUtils.close(inputStream);
                throw th;
            }
        } catch (IOException e2) {
            e = e2;
            Debug.e("Failed loading Bitmap in AssetBitmapTextureAtlasSource. AssetPath: " + str, e);
            StreamUtils.close(inputStream2);
            this.mWidth = options.outWidth;
            this.mHeight = options.outHeight;
        } catch (Throwable th3) {
            th = th3;
            inputStream = null;
            StreamUtils.close(inputStream);
            throw th;
        }
        this.mWidth = options.outWidth;
        this.mHeight = options.outHeight;
    }

    AssetBitmapTextureAtlasSource(Context context, String str, int i, int i2, int i3, int i4) {
        super(i, i2);
        this.mContext = context;
        this.mAssetPath = str;
        this.mWidth = i3;
        this.mHeight = i4;
    }

    public AssetBitmapTextureAtlasSource clone() {
        return new AssetBitmapTextureAtlasSource(this.mContext, this.mAssetPath, this.mTexturePositionX, this.mTexturePositionY, this.mWidth, this.mHeight);
    }

    public int getWidth() {
        return this.mWidth;
    }

    public int getHeight() {
        return this.mHeight;
    }

    public Bitmap onLoadBitmap(Bitmap.Config config) {
        InputStream inputStream;
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = config;
            inputStream = this.mContext.getAssets().open(this.mAssetPath);
            try {
                Bitmap decodeStream = BitmapFactory.decodeStream(inputStream, null, options);
                StreamUtils.close(inputStream);
                return decodeStream;
            } catch (IOException e) {
                e = e;
                try {
                    Debug.e("Failed loading Bitmap in " + getClass().getSimpleName() + ". AssetPath: " + this.mAssetPath, e);
                    StreamUtils.close(inputStream);
                    return null;
                } catch (Throwable th) {
                    th = th;
                    StreamUtils.close(inputStream);
                    throw th;
                }
            }
        } catch (IOException e2) {
            e = e2;
            inputStream = null;
            Debug.e("Failed loading Bitmap in " + getClass().getSimpleName() + ". AssetPath: " + this.mAssetPath, e);
            StreamUtils.close(inputStream);
            return null;
        } catch (Throwable th2) {
            th = th2;
            inputStream = null;
            StreamUtils.close(inputStream);
            throw th;
        }
    }

    public String toString() {
        return String.valueOf(getClass().getSimpleName()) + "(" + this.mAssetPath + ")";
    }
}
