package org.anddev.andengine.opengl.texture.atlas.bitmap.source.decorator;

import android.graphics.Canvas;
import org.anddev.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource;
import org.anddev.andengine.opengl.texture.atlas.bitmap.source.decorator.BaseBitmapTextureAtlasSourceDecorator;
import org.anddev.andengine.opengl.texture.atlas.bitmap.source.decorator.shape.IBitmapTextureAtlasSourceDecoratorShape;

public abstract class BaseShapeBitmapTextureAtlasSourceDecorator extends BaseBitmapTextureAtlasSourceDecorator {
    protected final IBitmapTextureAtlasSourceDecoratorShape mBitmapTextureAtlasSourceDecoratorShape;

    public abstract BaseShapeBitmapTextureAtlasSourceDecorator clone();

    public BaseShapeBitmapTextureAtlasSourceDecorator(IBitmapTextureAtlasSource iBitmapTextureAtlasSource, IBitmapTextureAtlasSourceDecoratorShape iBitmapTextureAtlasSourceDecoratorShape, BaseBitmapTextureAtlasSourceDecorator.TextureAtlasSourceDecoratorOptions textureAtlasSourceDecoratorOptions) {
        super(iBitmapTextureAtlasSource, textureAtlasSourceDecoratorOptions);
        this.mBitmapTextureAtlasSourceDecoratorShape = iBitmapTextureAtlasSourceDecoratorShape;
    }

    /* access modifiers changed from: protected */
    public void onDecorateBitmap(Canvas canvas) {
        this.mBitmapTextureAtlasSourceDecoratorShape.onDecorateBitmap(canvas, this.mPaint, this.mTextureAtlasSourceDecoratorOptions == null ? BaseBitmapTextureAtlasSourceDecorator.TextureAtlasSourceDecoratorOptions.DEFAULT : this.mTextureAtlasSourceDecoratorOptions);
    }
}
