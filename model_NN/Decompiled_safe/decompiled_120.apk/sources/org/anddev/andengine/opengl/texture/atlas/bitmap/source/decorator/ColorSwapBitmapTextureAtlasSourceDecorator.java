package org.anddev.andengine.opengl.texture.atlas.bitmap.source.decorator;

import android.graphics.AvoidXfermode;
import org.anddev.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource;
import org.anddev.andengine.opengl.texture.atlas.bitmap.source.decorator.BaseBitmapTextureAtlasSourceDecorator;
import org.anddev.andengine.opengl.texture.atlas.bitmap.source.decorator.shape.IBitmapTextureAtlasSourceDecoratorShape;

public class ColorSwapBitmapTextureAtlasSourceDecorator extends BaseShapeBitmapTextureAtlasSourceDecorator {
    private static final int TOLERANCE_DEFAULT = 0;
    protected final int mColorKeyColor;
    protected final int mColorSwapColor;
    protected final int mTolerance;

    public ColorSwapBitmapTextureAtlasSourceDecorator(IBitmapTextureAtlasSource iBitmapTextureAtlasSource, IBitmapTextureAtlasSourceDecoratorShape iBitmapTextureAtlasSourceDecoratorShape, int i, int i2) {
        this(iBitmapTextureAtlasSource, iBitmapTextureAtlasSourceDecoratorShape, i, 0, i2, null);
    }

    public ColorSwapBitmapTextureAtlasSourceDecorator(IBitmapTextureAtlasSource iBitmapTextureAtlasSource, IBitmapTextureAtlasSourceDecoratorShape iBitmapTextureAtlasSourceDecoratorShape, int i, int i2, BaseBitmapTextureAtlasSourceDecorator.TextureAtlasSourceDecoratorOptions textureAtlasSourceDecoratorOptions) {
        this(iBitmapTextureAtlasSource, iBitmapTextureAtlasSourceDecoratorShape, i, 0, i2, textureAtlasSourceDecoratorOptions);
    }

    public ColorSwapBitmapTextureAtlasSourceDecorator(IBitmapTextureAtlasSource iBitmapTextureAtlasSource, IBitmapTextureAtlasSourceDecoratorShape iBitmapTextureAtlasSourceDecoratorShape, int i, int i2, int i3) {
        this(iBitmapTextureAtlasSource, iBitmapTextureAtlasSourceDecoratorShape, i, i2, i3, null);
    }

    public ColorSwapBitmapTextureAtlasSourceDecorator(IBitmapTextureAtlasSource iBitmapTextureAtlasSource, IBitmapTextureAtlasSourceDecoratorShape iBitmapTextureAtlasSourceDecoratorShape, int i, int i2, int i3, BaseBitmapTextureAtlasSourceDecorator.TextureAtlasSourceDecoratorOptions textureAtlasSourceDecoratorOptions) {
        super(iBitmapTextureAtlasSource, iBitmapTextureAtlasSourceDecoratorShape, textureAtlasSourceDecoratorOptions);
        this.mColorKeyColor = i;
        this.mTolerance = i2;
        this.mColorSwapColor = i3;
        this.mPaint.setXfermode(new AvoidXfermode(i, i2, AvoidXfermode.Mode.TARGET));
        this.mPaint.setColor(i3);
    }

    public ColorSwapBitmapTextureAtlasSourceDecorator clone() {
        return new ColorSwapBitmapTextureAtlasSourceDecorator(this.mBitmapTextureAtlasSource, this.mBitmapTextureAtlasSourceDecoratorShape, this.mColorKeyColor, this.mTolerance, this.mColorSwapColor, this.mTextureAtlasSourceDecoratorOptions);
    }
}
