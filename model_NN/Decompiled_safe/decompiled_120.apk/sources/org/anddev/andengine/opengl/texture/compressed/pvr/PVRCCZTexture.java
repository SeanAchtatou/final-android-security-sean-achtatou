package org.anddev.andengine.opengl.texture.compressed.pvr;

import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.zip.GZIPInputStream;
import java.util.zip.Inflater;
import java.util.zip.InflaterInputStream;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.opengl.texture.ITexture;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.compressed.pvr.PVRTexture;
import org.anddev.andengine.util.ArrayUtils;
import org.anddev.andengine.util.StreamUtils;

public abstract class PVRCCZTexture extends PVRTexture {
    public PVRCCZTexture(PVRTexture.PVRTextureFormat pVRTextureFormat) {
        super(pVRTextureFormat);
    }

    public PVRCCZTexture(PVRTexture.PVRTextureFormat pVRTextureFormat, ITexture.ITextureStateListener iTextureStateListener) {
        super(pVRTextureFormat, iTextureStateListener);
    }

    public PVRCCZTexture(PVRTexture.PVRTextureFormat pVRTextureFormat, TextureOptions textureOptions) {
        super(pVRTextureFormat, textureOptions);
    }

    public PVRCCZTexture(PVRTexture.PVRTextureFormat pVRTextureFormat, TextureOptions textureOptions, ITexture.ITextureStateListener iTextureStateListener) {
        super(pVRTextureFormat, textureOptions, iTextureStateListener);
    }

    /* access modifiers changed from: protected */
    public final InputStream getInputStream() {
        InputStream onGetInputStream = onGetInputStream();
        CCZHeader cCZHeader = new CCZHeader(StreamUtils.streamToBytes(onGetInputStream, 16));
        return cCZHeader.getCCZCompressionFormat().wrap(onGetInputStream, cCZHeader.getUncompressedSize());
    }

    public class CCZHeader {
        public static final byte[] MAGIC_IDENTIFIER = {67, 67, 90, 33};
        public static final int SIZE = 16;
        private final CCZCompressionFormat mCCZCompressionFormat;
        private final ByteBuffer mDataByteBuffer;

        public CCZHeader(byte[] bArr) {
            this.mDataByteBuffer = ByteBuffer.wrap(bArr);
            this.mDataByteBuffer.rewind();
            this.mDataByteBuffer.order(ByteOrder.BIG_ENDIAN);
            if (!ArrayUtils.equals(bArr, 0, MAGIC_IDENTIFIER, 0, MAGIC_IDENTIFIER.length)) {
                throw new IllegalArgumentException("Invalid " + getClass().getSimpleName() + "!");
            }
            this.mCCZCompressionFormat = CCZCompressionFormat.fromID(getCCZCompressionFormatID());
        }

        private short getCCZCompressionFormatID() {
            return this.mDataByteBuffer.getShort(4);
        }

        public CCZCompressionFormat getCCZCompressionFormat() {
            return this.mCCZCompressionFormat;
        }

        public short getVersion() {
            return this.mDataByteBuffer.getShort(6);
        }

        public int getUserdata() {
            return this.mDataByteBuffer.getInt(8);
        }

        public int getUncompressedSize() {
            return this.mDataByteBuffer.getInt(12);
        }
    }

    public enum CCZCompressionFormat {
        ZLIB(0),
        BZIP2(1),
        GZIP(2),
        NONE(3);
        
        private static /* synthetic */ int[] $SWITCH_TABLE$org$anddev$andengine$opengl$texture$compressed$pvr$PVRCCZTexture$CCZCompressionFormat;
        private final short mID;

        private CCZCompressionFormat(short s) {
            this.mID = s;
        }

        public final InputStream wrap(InputStream inputStream, int i) {
            switch ($SWITCH_TABLE$org$anddev$andengine$opengl$texture$compressed$pvr$PVRCCZTexture$CCZCompressionFormat()[ordinal()]) {
                case 1:
                    return new InflaterInputStream(inputStream, new Inflater(), i);
                case 2:
                default:
                    throw new IllegalArgumentException("Unexpected " + CCZCompressionFormat.class.getSimpleName() + ": '" + this + "'.");
                case TouchEvent.ACTION_CANCEL:
                    return new GZIPInputStream(inputStream, i);
            }
        }

        public static CCZCompressionFormat fromID(short s) {
            for (CCZCompressionFormat cCZCompressionFormat : values()) {
                if (cCZCompressionFormat.mID == s) {
                    return cCZCompressionFormat;
                }
            }
            throw new IllegalArgumentException("Unexpected " + CCZCompressionFormat.class.getSimpleName() + "-ID: '" + ((int) s) + "'.");
        }
    }
}
