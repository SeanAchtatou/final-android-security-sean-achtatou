package org.anddev.andengine.opengl.vertex;

import org.anddev.andengine.opengl.util.FastFloatBuffer;

public class LineVertexBuffer extends VertexBuffer {
    public static final int VERTICES_PER_LINE = 2;

    public LineVertexBuffer(int i, boolean z) {
        super(4, i, z);
    }

    public synchronized void update(float f, float f2, float f3, float f4) {
        int[] iArr = this.mBufferData;
        iArr[0] = Float.floatToRawIntBits(f);
        iArr[1] = Float.floatToRawIntBits(f2);
        iArr[2] = Float.floatToRawIntBits(f3);
        iArr[3] = Float.floatToRawIntBits(f4);
        FastFloatBuffer fastFloatBuffer = this.mFloatBuffer;
        fastFloatBuffer.position(0);
        fastFloatBuffer.put(iArr);
        fastFloatBuffer.position(0);
        super.setHardwareBufferNeedsUpdate();
    }
}
