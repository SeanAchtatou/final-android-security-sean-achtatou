package org.anddev.andengine.opengl.texture.atlas.bitmap.source.decorator;

import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Shader;
import org.anddev.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource;
import org.anddev.andengine.opengl.texture.atlas.bitmap.source.decorator.BaseBitmapTextureAtlasSourceDecorator;
import org.anddev.andengine.opengl.texture.atlas.bitmap.source.decorator.shape.IBitmapTextureAtlasSourceDecoratorShape;

public class LinearGradientFillBitmapTextureAtlasSourceDecorator extends BaseShapeBitmapTextureAtlasSourceDecorator {
    protected final int[] mColors;
    protected final LinearGradientDirection mLinearGradientDirection;
    protected final float[] mPositions;

    public LinearGradientFillBitmapTextureAtlasSourceDecorator(IBitmapTextureAtlasSource iBitmapTextureAtlasSource, IBitmapTextureAtlasSourceDecoratorShape iBitmapTextureAtlasSourceDecoratorShape, int i, int i2, LinearGradientDirection linearGradientDirection) {
        this(iBitmapTextureAtlasSource, iBitmapTextureAtlasSourceDecoratorShape, i, i2, linearGradientDirection, (BaseBitmapTextureAtlasSourceDecorator.TextureAtlasSourceDecoratorOptions) null);
    }

    public LinearGradientFillBitmapTextureAtlasSourceDecorator(IBitmapTextureAtlasSource iBitmapTextureAtlasSource, IBitmapTextureAtlasSourceDecoratorShape iBitmapTextureAtlasSourceDecoratorShape, int i, int i2, LinearGradientDirection linearGradientDirection, BaseBitmapTextureAtlasSourceDecorator.TextureAtlasSourceDecoratorOptions textureAtlasSourceDecoratorOptions) {
        this(iBitmapTextureAtlasSource, iBitmapTextureAtlasSourceDecoratorShape, new int[]{i, i2}, (float[]) null, linearGradientDirection, textureAtlasSourceDecoratorOptions);
    }

    public LinearGradientFillBitmapTextureAtlasSourceDecorator(IBitmapTextureAtlasSource iBitmapTextureAtlasSource, IBitmapTextureAtlasSourceDecoratorShape iBitmapTextureAtlasSourceDecoratorShape, int[] iArr, float[] fArr, LinearGradientDirection linearGradientDirection) {
        this(iBitmapTextureAtlasSource, iBitmapTextureAtlasSourceDecoratorShape, iArr, fArr, linearGradientDirection, (BaseBitmapTextureAtlasSourceDecorator.TextureAtlasSourceDecoratorOptions) null);
    }

    public LinearGradientFillBitmapTextureAtlasSourceDecorator(IBitmapTextureAtlasSource iBitmapTextureAtlasSource, IBitmapTextureAtlasSourceDecoratorShape iBitmapTextureAtlasSourceDecoratorShape, int[] iArr, float[] fArr, LinearGradientDirection linearGradientDirection, BaseBitmapTextureAtlasSourceDecorator.TextureAtlasSourceDecoratorOptions textureAtlasSourceDecoratorOptions) {
        super(iBitmapTextureAtlasSource, iBitmapTextureAtlasSourceDecoratorShape, textureAtlasSourceDecoratorOptions);
        this.mColors = iArr;
        this.mPositions = fArr;
        this.mLinearGradientDirection = linearGradientDirection;
        this.mPaint.setStyle(Paint.Style.FILL);
        int width = iBitmapTextureAtlasSource.getWidth() - 1;
        int height = iBitmapTextureAtlasSource.getHeight() - 1;
        this.mPaint.setShader(new LinearGradient((float) linearGradientDirection.getFromX(width), (float) linearGradientDirection.getFromY(height), (float) linearGradientDirection.getToX(width), (float) linearGradientDirection.getToY(height), iArr, fArr, Shader.TileMode.CLAMP));
    }

    public LinearGradientFillBitmapTextureAtlasSourceDecorator clone() {
        return new LinearGradientFillBitmapTextureAtlasSourceDecorator(this.mBitmapTextureAtlasSource, this.mBitmapTextureAtlasSourceDecoratorShape, this.mColors, this.mPositions, this.mLinearGradientDirection, this.mTextureAtlasSourceDecoratorOptions);
    }

    public enum LinearGradientDirection {
        LEFT_TO_RIGHT(1, 0, 0, 0),
        RIGHT_TO_LEFT(0, 0, 1, 0),
        BOTTOM_TO_TOP(0, 1, 0, 0),
        TOP_TO_BOTTOM(0, 0, 0, 1),
        TOPLEFT_TO_BOTTOMRIGHT(0, 0, 1, 1),
        BOTTOMRIGHT_TO_TOPLEFT(1, 1, 0, 0),
        TOPRIGHT_TO_BOTTOMLEFT(1, 0, 0, 1),
        BOTTOMLEFT_TO_TOPRIGHT(0, 1, 1, 0);
        
        private final int mFromX;
        private final int mFromY;
        private final int mToX;
        private final int mToY;

        private LinearGradientDirection(int i, int i2, int i3, int i4) {
            this.mFromX = i;
            this.mFromY = i2;
            this.mToX = i3;
            this.mToY = i4;
        }

        /* access modifiers changed from: package-private */
        public final int getFromX(int i) {
            return this.mFromX * i;
        }

        /* access modifiers changed from: package-private */
        public final int getFromY(int i) {
            return this.mFromY * i;
        }

        /* access modifiers changed from: package-private */
        public final int getToX(int i) {
            return this.mToX * i;
        }

        /* access modifiers changed from: package-private */
        public final int getToY(int i) {
            return this.mToY * i;
        }
    }
}
