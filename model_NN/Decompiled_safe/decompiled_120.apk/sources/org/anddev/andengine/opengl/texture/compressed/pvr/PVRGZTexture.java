package org.anddev.andengine.opengl.texture.compressed.pvr;

import java.util.zip.GZIPInputStream;
import org.anddev.andengine.opengl.texture.ITexture;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.compressed.pvr.PVRTexture;

public abstract class PVRGZTexture extends PVRTexture {
    public PVRGZTexture(PVRTexture.PVRTextureFormat pVRTextureFormat) {
        super(pVRTextureFormat);
    }

    public PVRGZTexture(PVRTexture.PVRTextureFormat pVRTextureFormat, ITexture.ITextureStateListener iTextureStateListener) {
        super(pVRTextureFormat, iTextureStateListener);
    }

    public PVRGZTexture(PVRTexture.PVRTextureFormat pVRTextureFormat, TextureOptions textureOptions) {
        super(pVRTextureFormat, textureOptions);
    }

    public PVRGZTexture(PVRTexture.PVRTextureFormat pVRTextureFormat, TextureOptions textureOptions, ITexture.ITextureStateListener iTextureStateListener) {
        super(pVRTextureFormat, textureOptions, iTextureStateListener);
    }

    /* access modifiers changed from: protected */
    public GZIPInputStream getInputStream() {
        return new GZIPInputStream(onGetInputStream());
    }
}
