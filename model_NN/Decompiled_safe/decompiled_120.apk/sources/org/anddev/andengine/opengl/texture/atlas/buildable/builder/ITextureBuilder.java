package org.anddev.andengine.opengl.texture.atlas.buildable.builder;

import java.util.ArrayList;
import org.anddev.andengine.opengl.texture.atlas.ITextureAtlas;

public interface ITextureBuilder {
    void pack(ITextureAtlas iTextureAtlas, ArrayList arrayList);

    public class TextureAtlasSourcePackingException extends Exception {
        private static final long serialVersionUID = 4700734424214372671L;

        public TextureAtlasSourcePackingException(String str) {
            super(str);
        }
    }
}
