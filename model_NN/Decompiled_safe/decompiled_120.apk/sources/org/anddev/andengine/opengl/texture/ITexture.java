package org.anddev.andengine.opengl.texture;

import javax.microedition.khronos.opengles.GL10;
import org.anddev.andengine.util.Debug;

public interface ITexture {
    void bind(GL10 gl10);

    int getHardwareTextureID();

    int getHeight();

    TextureOptions getTextureOptions();

    ITextureStateListener getTextureStateListener();

    int getWidth();

    boolean hasTextureStateListener();

    boolean isLoadedToHardware();

    boolean isUpdateOnHardwareNeeded();

    void loadToHardware(GL10 gl10);

    void reloadToHardware(GL10 gl10);

    void setLoadedToHardware(boolean z);

    void setUpdateOnHardwareNeeded(boolean z);

    void unloadFromHardware(GL10 gl10);

    public interface ITextureStateListener {
        void onLoadedToHardware(ITexture iTexture);

        void onUnloadedFromHardware(ITexture iTexture);

        public class TextureStateAdapter implements ITextureStateListener {
            public void onLoadedToHardware(ITexture iTexture) {
            }

            public void onUnloadedFromHardware(ITexture iTexture) {
            }
        }

        public class DebugTextureStateListener implements ITextureStateListener {
            public void onLoadedToHardware(ITexture iTexture) {
                Debug.d("Texture loaded: " + iTexture.toString());
            }

            public void onUnloadedFromHardware(ITexture iTexture) {
                Debug.d("Texture unloaded: " + iTexture.toString());
            }
        }
    }
}
