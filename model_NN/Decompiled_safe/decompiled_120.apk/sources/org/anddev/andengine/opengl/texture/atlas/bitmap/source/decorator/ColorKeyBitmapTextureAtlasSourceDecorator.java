package org.anddev.andengine.opengl.texture.atlas.bitmap.source.decorator;

import org.anddev.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource;
import org.anddev.andengine.opengl.texture.atlas.bitmap.source.decorator.BaseBitmapTextureAtlasSourceDecorator;
import org.anddev.andengine.opengl.texture.atlas.bitmap.source.decorator.shape.IBitmapTextureAtlasSourceDecoratorShape;

public class ColorKeyBitmapTextureAtlasSourceDecorator extends ColorSwapBitmapTextureAtlasSourceDecorator {
    public ColorKeyBitmapTextureAtlasSourceDecorator(IBitmapTextureAtlasSource iBitmapTextureAtlasSource, IBitmapTextureAtlasSourceDecoratorShape iBitmapTextureAtlasSourceDecoratorShape, int i) {
        super(iBitmapTextureAtlasSource, iBitmapTextureAtlasSourceDecoratorShape, i, 0);
    }

    public ColorKeyBitmapTextureAtlasSourceDecorator(IBitmapTextureAtlasSource iBitmapTextureAtlasSource, IBitmapTextureAtlasSourceDecoratorShape iBitmapTextureAtlasSourceDecoratorShape, int i, BaseBitmapTextureAtlasSourceDecorator.TextureAtlasSourceDecoratorOptions textureAtlasSourceDecoratorOptions) {
        super(iBitmapTextureAtlasSource, iBitmapTextureAtlasSourceDecoratorShape, i, 0, textureAtlasSourceDecoratorOptions);
    }

    public ColorKeyBitmapTextureAtlasSourceDecorator(IBitmapTextureAtlasSource iBitmapTextureAtlasSource, IBitmapTextureAtlasSourceDecoratorShape iBitmapTextureAtlasSourceDecoratorShape, int i, int i2) {
        super(iBitmapTextureAtlasSource, iBitmapTextureAtlasSourceDecoratorShape, i, 0, i2);
    }

    public ColorKeyBitmapTextureAtlasSourceDecorator(IBitmapTextureAtlasSource iBitmapTextureAtlasSource, IBitmapTextureAtlasSourceDecoratorShape iBitmapTextureAtlasSourceDecoratorShape, int i, int i2, BaseBitmapTextureAtlasSourceDecorator.TextureAtlasSourceDecoratorOptions textureAtlasSourceDecoratorOptions) {
        super(iBitmapTextureAtlasSource, iBitmapTextureAtlasSourceDecoratorShape, i, i2, 0, textureAtlasSourceDecoratorOptions);
    }

    public ColorKeyBitmapTextureAtlasSourceDecorator clone() {
        return new ColorKeyBitmapTextureAtlasSourceDecorator(this.mBitmapTextureAtlasSource, this.mBitmapTextureAtlasSourceDecoratorShape, this.mColorKeyColor, this.mTolerance, this.mTextureAtlasSourceDecoratorOptions);
    }
}
