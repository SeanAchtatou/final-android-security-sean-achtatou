package org.anddev.andengine.opengl.texture.atlas.bitmap.source.decorator;

import android.graphics.Paint;
import org.anddev.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource;
import org.anddev.andengine.opengl.texture.atlas.bitmap.source.decorator.BaseBitmapTextureAtlasSourceDecorator;
import org.anddev.andengine.opengl.texture.atlas.bitmap.source.decorator.shape.IBitmapTextureAtlasSourceDecoratorShape;

public class FillBitmapTextureAtlasSourceDecorator extends BaseShapeBitmapTextureAtlasSourceDecorator {
    protected final int mFillColor;

    public FillBitmapTextureAtlasSourceDecorator(IBitmapTextureAtlasSource iBitmapTextureAtlasSource, IBitmapTextureAtlasSourceDecoratorShape iBitmapTextureAtlasSourceDecoratorShape, int i) {
        this(iBitmapTextureAtlasSource, iBitmapTextureAtlasSourceDecoratorShape, i, null);
    }

    public FillBitmapTextureAtlasSourceDecorator(IBitmapTextureAtlasSource iBitmapTextureAtlasSource, IBitmapTextureAtlasSourceDecoratorShape iBitmapTextureAtlasSourceDecoratorShape, int i, BaseBitmapTextureAtlasSourceDecorator.TextureAtlasSourceDecoratorOptions textureAtlasSourceDecoratorOptions) {
        super(iBitmapTextureAtlasSource, iBitmapTextureAtlasSourceDecoratorShape, textureAtlasSourceDecoratorOptions);
        this.mFillColor = i;
        this.mPaint.setStyle(Paint.Style.FILL);
        this.mPaint.setColor(i);
    }

    public FillBitmapTextureAtlasSourceDecorator clone() {
        return new FillBitmapTextureAtlasSourceDecorator(this.mBitmapTextureAtlasSource, this.mBitmapTextureAtlasSourceDecoratorShape, this.mFillColor, this.mTextureAtlasSourceDecoratorOptions);
    }
}
