package org.anddev.andengine.opengl.util;

import android.graphics.Bitmap;
import android.opengl.GLException;
import android.os.Build;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;
import org.anddev.andengine.engine.options.RenderOptions;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.opengl.texture.Texture;
import org.anddev.andengine.opengl.texture.region.crop.TextureRegionCrop;
import org.anddev.andengine.util.Debug;

public class GLHelper {
    private static /* synthetic */ int[] $SWITCH_TABLE$org$anddev$andengine$opengl$texture$Texture$PixelFormat = null;
    public static final int BYTES_PER_FLOAT = 4;
    public static final int BYTES_PER_PIXEL_RGBA = 4;
    public static boolean EXTENSIONS_DRAWTEXTURE = false;
    public static boolean EXTENSIONS_TEXTURE_NON_POWER_OF_TWO = false;
    public static boolean EXTENSIONS_VERTEXBUFFEROBJECTS = false;
    private static final int[] HARDWAREBUFFERID_CONTAINER = new int[1];
    private static final int[] HARDWARETEXTUREID_CONTAINER = new int[1];
    private static final boolean IS_LITTLE_ENDIAN;
    private static float sAlpha = -1.0f;
    private static float sBlue = -1.0f;
    private static int sCurrentDestinationBlendMode = -1;
    private static int sCurrentHardwareBufferID = -1;
    private static int sCurrentHardwareTextureID = -1;
    private static int sCurrentMatrix = -1;
    private static int sCurrentSourceBlendMode = -1;
    private static FastFloatBuffer sCurrentTextureFloatBuffer = null;
    private static TextureRegionCrop sCurrentTextureRegionCrop = null;
    private static FastFloatBuffer sCurrentVertexFloatBuffer = null;
    private static boolean sEnableBlend = false;
    private static boolean sEnableCulling = false;
    private static boolean sEnableDepthTest = true;
    private static boolean sEnableDither = true;
    private static boolean sEnableLightning = true;
    private static boolean sEnableMultisample = true;
    private static boolean sEnableScissorTest = false;
    private static boolean sEnableTexCoordArray = false;
    private static boolean sEnableTextures = false;
    private static boolean sEnableVertexArray = false;
    private static float sGreen = -1.0f;
    private static float sLineWidth = 1.0f;
    private static float sRed = -1.0f;

    static /* synthetic */ int[] $SWITCH_TABLE$org$anddev$andengine$opengl$texture$Texture$PixelFormat() {
        int[] iArr = $SWITCH_TABLE$org$anddev$andengine$opengl$texture$Texture$PixelFormat;
        if (iArr == null) {
            iArr = new int[Texture.PixelFormat.values().length];
            try {
                iArr[Texture.PixelFormat.AI_88.ordinal()] = 8;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[Texture.PixelFormat.A_8.ordinal()] = 6;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[Texture.PixelFormat.I_8.ordinal()] = 7;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[Texture.PixelFormat.RGBA_4444.ordinal()] = 2;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[Texture.PixelFormat.RGBA_5551.ordinal()] = 3;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[Texture.PixelFormat.RGBA_8888.ordinal()] = 4;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[Texture.PixelFormat.RGB_565.ordinal()] = 5;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[Texture.PixelFormat.UNDEFINED.ordinal()] = 1;
            } catch (NoSuchFieldError e8) {
            }
            $SWITCH_TABLE$org$anddev$andengine$opengl$texture$Texture$PixelFormat = iArr;
        }
        return iArr;
    }

    static {
        boolean z;
        if (ByteOrder.nativeOrder() == ByteOrder.LITTLE_ENDIAN) {
            z = true;
        } else {
            z = false;
        }
        IS_LITTLE_ENDIAN = z;
    }

    public static void reset(GL10 gl10) {
        sCurrentHardwareBufferID = -1;
        sCurrentHardwareTextureID = -1;
        sCurrentMatrix = -1;
        sCurrentSourceBlendMode = -1;
        sCurrentDestinationBlendMode = -1;
        sCurrentVertexFloatBuffer = null;
        sCurrentTextureFloatBuffer = null;
        sCurrentTextureRegionCrop = null;
        enableDither(gl10);
        enableLightning(gl10);
        enableDepthTest(gl10);
        enableMultisample(gl10);
        disableBlend(gl10);
        disableCulling(gl10);
        disableTextures(gl10);
        disableTexCoordArray(gl10);
        disableVertexArray(gl10);
        sLineWidth = 1.0f;
        sRed = -1.0f;
        sGreen = -1.0f;
        sBlue = -1.0f;
        sAlpha = -1.0f;
        EXTENSIONS_VERTEXBUFFEROBJECTS = false;
        EXTENSIONS_DRAWTEXTURE = false;
        EXTENSIONS_TEXTURE_NON_POWER_OF_TWO = false;
    }

    public static void enableExtensions(GL10 gl10, RenderOptions renderOptions) {
        boolean z;
        boolean z2;
        String glGetString = gl10.glGetString(7938);
        String glGetString2 = gl10.glGetString(7937);
        String glGetString3 = gl10.glGetString(7939);
        Debug.d("RENDERER: " + glGetString2);
        Debug.d("VERSION: " + glGetString);
        Debug.d("EXTENSIONS: " + glGetString3);
        boolean contains = glGetString.contains("1.0");
        boolean contains2 = glGetString.contains("2.");
        boolean contains3 = glGetString2.contains("PixelFlinger");
        boolean contains4 = glGetString3.contains("_vertex_buffer_object");
        boolean contains5 = glGetString3.contains("draw_texture");
        boolean contains6 = glGetString3.contains("texture_npot");
        EXTENSIONS_VERTEXBUFFEROBJECTS = !renderOptions.isDisableExtensionVertexBufferObjects() && !contains3 && (contains4 || !contains);
        if (renderOptions.isDisableExtensionVertexBufferObjects() || (!contains5 && contains)) {
            z = false;
        } else {
            z = true;
        }
        EXTENSIONS_DRAWTEXTURE = z;
        if (contains6 || contains2) {
            z2 = true;
        } else {
            z2 = false;
        }
        EXTENSIONS_TEXTURE_NON_POWER_OF_TWO = z2;
        hackBrokenDevices();
        Debug.d("EXTENSIONS_VERXTEXBUFFEROBJECTS = " + EXTENSIONS_VERTEXBUFFEROBJECTS);
        Debug.d("EXTENSIONS_DRAWTEXTURE = " + EXTENSIONS_DRAWTEXTURE);
    }

    private static void hackBrokenDevices() {
        if (Build.PRODUCT.contains("morrison")) {
            EXTENSIONS_VERTEXBUFFEROBJECTS = false;
        }
    }

    public static void setColor(GL10 gl10, float f, float f2, float f3, float f4) {
        if (f4 != sAlpha || f != sRed || f2 != sGreen || f3 != sBlue) {
            sAlpha = f4;
            sRed = f;
            sGreen = f2;
            sBlue = f3;
            gl10.glColor4f(f, f2, f3, f4);
        }
    }

    public static void enableVertexArray(GL10 gl10) {
        if (!sEnableVertexArray) {
            sEnableVertexArray = true;
            gl10.glEnableClientState(32884);
        }
    }

    public static void disableVertexArray(GL10 gl10) {
        if (sEnableVertexArray) {
            sEnableVertexArray = false;
            gl10.glDisableClientState(32884);
        }
    }

    public static void enableTexCoordArray(GL10 gl10) {
        if (!sEnableTexCoordArray) {
            sEnableTexCoordArray = true;
            gl10.glEnableClientState(32888);
        }
    }

    public static void disableTexCoordArray(GL10 gl10) {
        if (sEnableTexCoordArray) {
            sEnableTexCoordArray = false;
            gl10.glDisableClientState(32888);
        }
    }

    public static void enableScissorTest(GL10 gl10) {
        if (!sEnableScissorTest) {
            sEnableScissorTest = true;
            gl10.glEnable(3089);
        }
    }

    public static void disableScissorTest(GL10 gl10) {
        if (sEnableScissorTest) {
            sEnableScissorTest = false;
            gl10.glDisable(3089);
        }
    }

    public static void enableBlend(GL10 gl10) {
        if (!sEnableBlend) {
            sEnableBlend = true;
            gl10.glEnable(3042);
        }
    }

    public static void disableBlend(GL10 gl10) {
        if (sEnableBlend) {
            sEnableBlend = false;
            gl10.glDisable(3042);
        }
    }

    public static void enableCulling(GL10 gl10) {
        if (!sEnableCulling) {
            sEnableCulling = true;
            gl10.glEnable(2884);
        }
    }

    public static void disableCulling(GL10 gl10) {
        if (sEnableCulling) {
            sEnableCulling = false;
            gl10.glDisable(2884);
        }
    }

    public static void enableTextures(GL10 gl10) {
        if (!sEnableTextures) {
            sEnableTextures = true;
            gl10.glEnable(3553);
        }
    }

    public static void disableTextures(GL10 gl10) {
        if (sEnableTextures) {
            sEnableTextures = false;
            gl10.glDisable(3553);
        }
    }

    public static void enableLightning(GL10 gl10) {
        if (!sEnableLightning) {
            sEnableLightning = true;
            gl10.glEnable(2896);
        }
    }

    public static void disableLightning(GL10 gl10) {
        if (sEnableLightning) {
            sEnableLightning = false;
            gl10.glDisable(2896);
        }
    }

    public static void enableDither(GL10 gl10) {
        if (!sEnableDither) {
            sEnableDither = true;
            gl10.glEnable(3024);
        }
    }

    public static void disableDither(GL10 gl10) {
        if (sEnableDither) {
            sEnableDither = false;
            gl10.glDisable(3024);
        }
    }

    public static void enableDepthTest(GL10 gl10) {
        if (!sEnableDepthTest) {
            sEnableDepthTest = true;
            gl10.glEnable(2929);
        }
    }

    public static void disableDepthTest(GL10 gl10) {
        if (sEnableDepthTest) {
            sEnableDepthTest = false;
            gl10.glDisable(2929);
        }
    }

    public static void enableMultisample(GL10 gl10) {
        if (!sEnableMultisample) {
            sEnableMultisample = true;
            gl10.glEnable(32925);
        }
    }

    public static void disableMultisample(GL10 gl10) {
        if (sEnableMultisample) {
            sEnableMultisample = false;
            gl10.glDisable(32925);
        }
    }

    public static void bindBuffer(GL11 gl11, int i) {
        if (sCurrentHardwareBufferID != i) {
            sCurrentHardwareBufferID = i;
            gl11.glBindBuffer(34962, i);
        }
    }

    public static void deleteBuffer(GL11 gl11, int i) {
        HARDWAREBUFFERID_CONTAINER[0] = i;
        gl11.glDeleteBuffers(1, HARDWAREBUFFERID_CONTAINER, 0);
    }

    public static void bindTexture(GL10 gl10, int i) {
        if (sCurrentHardwareTextureID != i) {
            sCurrentHardwareTextureID = i;
            gl10.glBindTexture(3553, i);
        }
    }

    public static void forceBindTexture(GL10 gl10, int i) {
        sCurrentHardwareTextureID = i;
        gl10.glBindTexture(3553, i);
    }

    public static void deleteTexture(GL10 gl10, int i) {
        HARDWARETEXTUREID_CONTAINER[0] = i;
        gl10.glDeleteTextures(1, HARDWARETEXTUREID_CONTAINER, 0);
    }

    public static void texCoordPointer(GL10 gl10, FastFloatBuffer fastFloatBuffer) {
        if (sCurrentTextureFloatBuffer != fastFloatBuffer) {
            sCurrentTextureFloatBuffer = fastFloatBuffer;
            gl10.glTexCoordPointer(2, 5126, 0, fastFloatBuffer.mByteBuffer);
        }
    }

    public static void texCoordZeroPointer(GL11 gl11) {
        gl11.glTexCoordPointer(2, 5126, 0, 0);
    }

    public static void vertexPointer(GL10 gl10, FastFloatBuffer fastFloatBuffer) {
        if (sCurrentVertexFloatBuffer != fastFloatBuffer) {
            sCurrentVertexFloatBuffer = fastFloatBuffer;
            gl10.glVertexPointer(2, 5126, 0, fastFloatBuffer.mByteBuffer);
        }
    }

    public static void vertexZeroPointer(GL11 gl11) {
        gl11.glVertexPointer(2, 5126, 0, 0);
    }

    public static void blendFunction(GL10 gl10, int i, int i2) {
        if (sCurrentSourceBlendMode != i || sCurrentDestinationBlendMode != i2) {
            sCurrentSourceBlendMode = i;
            sCurrentDestinationBlendMode = i2;
            gl10.glBlendFunc(i, i2);
        }
    }

    public static void lineWidth(GL10 gl10, float f) {
        if (sLineWidth != f) {
            sLineWidth = f;
            gl10.glLineWidth(f);
        }
    }

    public static void switchToModelViewMatrix(GL10 gl10) {
        if (sCurrentMatrix != 5888) {
            sCurrentMatrix = 5888;
            gl10.glMatrixMode(5888);
        }
    }

    public static void switchToProjectionMatrix(GL10 gl10) {
        if (sCurrentMatrix != 5889) {
            sCurrentMatrix = 5889;
            gl10.glMatrixMode(5889);
        }
    }

    public static void setProjectionIdentityMatrix(GL10 gl10) {
        switchToProjectionMatrix(gl10);
        gl10.glLoadIdentity();
    }

    public static void setModelViewIdentityMatrix(GL10 gl10) {
        switchToModelViewMatrix(gl10);
        gl10.glLoadIdentity();
    }

    public static void setShadeModelFlat(GL10 gl10) {
        gl10.glShadeModel(7424);
    }

    public static void setPerspectiveCorrectionHintFastest(GL10 gl10) {
        gl10.glHint(3152, 4353);
    }

    public static void bufferData(GL11 gl11, ByteBuffer byteBuffer, int i) {
        gl11.glBufferData(34962, byteBuffer.capacity(), byteBuffer, i);
    }

    public static void textureCrop(GL11 gl11, TextureRegionCrop textureRegionCrop) {
        if (textureRegionCrop != sCurrentTextureRegionCrop || textureRegionCrop.isDirty()) {
            sCurrentTextureRegionCrop = textureRegionCrop;
            gl11.glTexParameteriv(3553, 35741, textureRegionCrop.getData(), 0);
        }
    }

    public static void glTexImage2D(GL10 gl10, int i, int i2, Bitmap bitmap, int i3, Texture.PixelFormat pixelFormat) {
        GL10 gl102 = gl10;
        int i4 = i;
        int i5 = i2;
        int i6 = i3;
        gl102.glTexImage2D(i4, i5, pixelFormat.getGLFormat(), bitmap.getWidth(), bitmap.getHeight(), i6, pixelFormat.getGLFormat(), pixelFormat.getGLType(), getPixels(bitmap, pixelFormat));
    }

    public static void glTexSubImage2D(GL10 gl10, int i, int i2, int i3, int i4, Bitmap bitmap, Texture.PixelFormat pixelFormat) {
        GL10 gl102 = gl10;
        int i5 = i;
        int i6 = i2;
        int i7 = i3;
        int i8 = i4;
        gl102.glTexSubImage2D(i5, i6, i7, i8, bitmap.getWidth(), bitmap.getHeight(), pixelFormat.getGLFormat(), pixelFormat.getGLType(), getPixels(bitmap, pixelFormat));
    }

    private static Buffer getPixels(Bitmap bitmap, Texture.PixelFormat pixelFormat) {
        int[] pixelsARGB_8888 = getPixelsARGB_8888(bitmap);
        switch ($SWITCH_TABLE$org$anddev$andengine$opengl$texture$Texture$PixelFormat()[pixelFormat.ordinal()]) {
            case 2:
                return ByteBuffer.wrap(convertARGB_8888toARGB_4444(pixelsARGB_8888));
            case TouchEvent.ACTION_CANCEL:
            default:
                throw new IllegalArgumentException("Unexpected " + Texture.PixelFormat.class.getSimpleName() + ": '" + pixelFormat + "'.");
            case 4:
                return IntBuffer.wrap(convertARGB_8888toRGBA_8888(pixelsARGB_8888));
            case 5:
                return ByteBuffer.wrap(convertARGB_8888toRGB_565(pixelsARGB_8888));
            case 6:
                return ByteBuffer.wrap(convertARGB_8888toA_8(pixelsARGB_8888));
        }
    }

    private static int[] convertARGB_8888toRGBA_8888(int[] iArr) {
        if (IS_LITTLE_ENDIAN) {
            for (int length = iArr.length - 1; length >= 0; length--) {
                int i = iArr[length];
                iArr[length] = ((i & 16711680) >> 16) | (-16711936 & i) | ((i & 255) << 16);
            }
        } else {
            for (int length2 = iArr.length - 1; length2 >= 0; length2--) {
                int i2 = iArr[length2];
                iArr[length2] = ((i2 & -16777216) >> 24) | ((16777215 & i2) << 8);
            }
        }
        return iArr;
    }

    private static byte[] convertARGB_8888toRGB_565(int[] iArr) {
        byte[] bArr = new byte[(iArr.length * 2)];
        if (IS_LITTLE_ENDIAN) {
            int length = bArr.length - 1;
            int i = length;
            for (int length2 = iArr.length - 1; length2 >= 0; length2--) {
                int i2 = iArr[length2];
                int i3 = (i2 >> 8) & 255;
                int i4 = i - 1;
                bArr[i] = (byte) (((i2 >> 16) & 255 & 248) | (i3 >> 5));
                i = i4 - 1;
                bArr[i4] = (byte) (((i2 & 255) >> 3) | ((i3 << 3) & 224));
            }
        } else {
            int length3 = bArr.length - 1;
            int i5 = length3;
            for (int length4 = iArr.length - 1; length4 >= 0; length4--) {
                int i6 = iArr[length4];
                int i7 = (i6 >> 8) & 255;
                int i8 = i5 - 1;
                bArr[i5] = (byte) (((i6 & 255) >> 3) | ((i7 << 3) & 224));
                i5 = i8 - 1;
                bArr[i8] = (byte) (((i6 >> 16) & 255 & 248) | (i7 >> 5));
            }
        }
        return bArr;
    }

    private static byte[] convertARGB_8888toARGB_4444(int[] iArr) {
        byte[] bArr = new byte[(iArr.length * 2)];
        if (IS_LITTLE_ENDIAN) {
            int length = bArr.length - 1;
            int i = length;
            for (int length2 = iArr.length - 1; length2 >= 0; length2--) {
                int i2 = iArr[length2];
                int i3 = i - 1;
                bArr[i] = (byte) (((i2 >> 28) & 15) | ((i2 >> 16) & 240));
                i = i3 - 1;
                bArr[i3] = (byte) ((i2 & 15) | ((i2 >> 8) & 240));
            }
        } else {
            int length3 = bArr.length - 1;
            int i4 = length3;
            for (int length4 = iArr.length - 1; length4 >= 0; length4--) {
                int i5 = iArr[length4];
                int i6 = i4 - 1;
                bArr[i4] = (byte) ((i5 & 15) | ((i5 >> 8) & 240));
                i4 = i6 - 1;
                bArr[i6] = (byte) (((i5 >> 28) & 15) | ((i5 >> 16) & 240));
            }
        }
        return bArr;
    }

    private static byte[] convertARGB_8888toA_8(int[] iArr) {
        byte[] bArr = new byte[iArr.length];
        if (IS_LITTLE_ENDIAN) {
            for (int length = iArr.length - 1; length >= 0; length--) {
                bArr[length] = (byte) (iArr[length] >> 24);
            }
        } else {
            for (int length2 = iArr.length - 1; length2 >= 0; length2--) {
                bArr[length2] = (byte) (iArr[length2] & 255);
            }
        }
        return bArr;
    }

    public static int[] getPixelsARGB_8888(Bitmap bitmap) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        int[] iArr = new int[(width * height)];
        bitmap.getPixels(iArr, 0, width, 0, 0, width, height);
        return iArr;
    }

    public static void checkGLError(GL10 gl10) {
        int glGetError = gl10.glGetError();
        if (glGetError != 0) {
            throw new GLException(glGetError);
        }
    }
}
