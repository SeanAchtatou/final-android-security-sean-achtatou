package org.anddev.andengine.opengl.texture.region.buffer;

import org.anddev.andengine.opengl.buffer.BufferObject;
import org.anddev.andengine.opengl.texture.region.BaseTextureRegion;
import org.anddev.andengine.opengl.util.FastFloatBuffer;

public class SpriteBatchTextureRegionBuffer extends BufferObject {
    protected int mIndex;

    public SpriteBatchTextureRegionBuffer(int i, int i2, boolean z) {
        super(i * 2 * 6, i2, z);
    }

    public int getIndex() {
        return this.mIndex;
    }

    public void setIndex(int i) {
        this.mIndex = i;
    }

    public void add(BaseTextureRegion baseTextureRegion) {
        if (baseTextureRegion.getTexture() != null) {
            int floatToRawIntBits = Float.floatToRawIntBits(baseTextureRegion.getTextureCoordinateX1());
            int floatToRawIntBits2 = Float.floatToRawIntBits(baseTextureRegion.getTextureCoordinateY1());
            int floatToRawIntBits3 = Float.floatToRawIntBits(baseTextureRegion.getTextureCoordinateX2());
            int floatToRawIntBits4 = Float.floatToRawIntBits(baseTextureRegion.getTextureCoordinateY2());
            int[] iArr = this.mBufferData;
            int i = this.mIndex;
            int i2 = i + 1;
            iArr[i] = floatToRawIntBits;
            int i3 = i2 + 1;
            iArr[i2] = floatToRawIntBits2;
            int i4 = i3 + 1;
            iArr[i3] = floatToRawIntBits;
            int i5 = i4 + 1;
            iArr[i4] = floatToRawIntBits4;
            int i6 = i5 + 1;
            iArr[i5] = floatToRawIntBits3;
            int i7 = i6 + 1;
            iArr[i6] = floatToRawIntBits2;
            int i8 = i7 + 1;
            iArr[i7] = floatToRawIntBits3;
            int i9 = i8 + 1;
            iArr[i8] = floatToRawIntBits2;
            int i10 = i9 + 1;
            iArr[i9] = floatToRawIntBits;
            int i11 = i10 + 1;
            iArr[i10] = floatToRawIntBits4;
            int i12 = i11 + 1;
            iArr[i11] = floatToRawIntBits3;
            iArr[i12] = floatToRawIntBits4;
            this.mIndex = i12 + 1;
        }
    }

    public void submit() {
        FastFloatBuffer fastFloatBuffer = this.mFloatBuffer;
        fastFloatBuffer.position(0);
        fastFloatBuffer.put(this.mBufferData);
        fastFloatBuffer.position(0);
        super.setHardwareBufferNeedsUpdate();
    }
}
