package org.anddev.andengine.opengl.vertex;

import org.anddev.andengine.opengl.util.FastFloatBuffer;

public class RectangleVertexBuffer extends VertexBuffer {
    private static final int FLOAT_TO_RAW_INT_BITS_ZERO = Float.floatToRawIntBits(0.0f);
    public static final int VERTICES_PER_RECTANGLE = 4;

    public RectangleVertexBuffer(int i, boolean z) {
        super(8, i, z);
    }

    public synchronized void update(float f, float f2) {
        int i = FLOAT_TO_RAW_INT_BITS_ZERO;
        int i2 = FLOAT_TO_RAW_INT_BITS_ZERO;
        int floatToRawIntBits = Float.floatToRawIntBits(f);
        int floatToRawIntBits2 = Float.floatToRawIntBits(f2);
        int[] iArr = this.mBufferData;
        iArr[0] = i;
        iArr[1] = i2;
        iArr[2] = i;
        iArr[3] = floatToRawIntBits2;
        iArr[4] = floatToRawIntBits;
        iArr[5] = i2;
        iArr[6] = floatToRawIntBits;
        iArr[7] = floatToRawIntBits2;
        FastFloatBuffer floatBuffer = getFloatBuffer();
        floatBuffer.position(0);
        floatBuffer.put(iArr);
        floatBuffer.position(0);
        super.setHardwareBufferNeedsUpdate();
    }
}
