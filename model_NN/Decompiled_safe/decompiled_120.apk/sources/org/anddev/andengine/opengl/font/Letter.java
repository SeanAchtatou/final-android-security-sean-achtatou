package org.anddev.andengine.opengl.font;

public class Letter {
    public final int mAdvance;
    public final char mCharacter;
    public final int mHeight;
    public final float mTextureHeight;
    public final float mTextureWidth;
    public final float mTextureX;
    public final float mTextureY;
    public final int mWidth;

    Letter(char c, int i, int i2, int i3, float f, float f2, float f3, float f4) {
        this.mCharacter = c;
        this.mAdvance = i;
        this.mWidth = i2;
        this.mHeight = i3;
        this.mTextureX = f;
        this.mTextureY = f2;
        this.mTextureWidth = f3;
        this.mTextureHeight = f4;
    }

    public int hashCode() {
        return this.mCharacter + 31;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        if (this.mCharacter != ((Letter) obj).mCharacter) {
            return false;
        }
        return true;
    }
}
