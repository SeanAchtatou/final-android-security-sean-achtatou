package org.anddev.andengine.opengl.font;

import android.util.SparseArray;
import org.anddev.andengine.util.Library;

public class FontLibrary extends Library {
    public FontLibrary() {
    }

    public FontLibrary(int i) {
        super(i);
    }

    /* access modifiers changed from: package-private */
    public void loadFonts(FontManager fontManager) {
        SparseArray sparseArray = this.mItems;
        for (int size = sparseArray.size() - 1; size >= 0; size--) {
            Font font = (Font) sparseArray.valueAt(size);
            if (font != null) {
                fontManager.loadFont(font);
            }
        }
    }
}
