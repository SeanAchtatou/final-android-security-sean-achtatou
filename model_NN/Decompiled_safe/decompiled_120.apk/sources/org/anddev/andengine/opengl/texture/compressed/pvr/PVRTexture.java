package org.anddev.andengine.opengl.texture.compressed.pvr;

import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import javax.microedition.khronos.opengles.GL10;
import org.anddev.andengine.opengl.texture.ITexture;
import org.anddev.andengine.opengl.texture.Texture;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.util.GLHelper;
import org.anddev.andengine.util.ArrayUtils;
import org.anddev.andengine.util.Debug;
import org.anddev.andengine.util.MathUtils;
import org.anddev.andengine.util.StreamUtils;

public abstract class PVRTexture extends Texture {
    public static final int FLAG_ALPHA = 32768;
    public static final int FLAG_BUMPMAP = 1024;
    public static final int FLAG_CUBEMAP = 4096;
    public static final int FLAG_FALSEMIPCOL = 8192;
    public static final int FLAG_MIPMAP = 256;
    public static final int FLAG_TILING = 2048;
    public static final int FLAG_TWIDDLE = 512;
    public static final int FLAG_VERTICALFLIP = 65536;
    public static final int FLAG_VOLUME = 16384;
    private final PVRTextureHeader mPVRTextureHeader;

    /* access modifiers changed from: protected */
    public abstract InputStream onGetInputStream();

    public PVRTexture(PVRTextureFormat pVRTextureFormat) {
        this(pVRTextureFormat, TextureOptions.DEFAULT, null);
    }

    public PVRTexture(PVRTextureFormat pVRTextureFormat, ITexture.ITextureStateListener iTextureStateListener) {
        this(pVRTextureFormat, TextureOptions.DEFAULT, iTextureStateListener);
    }

    public PVRTexture(PVRTextureFormat pVRTextureFormat, TextureOptions textureOptions) {
        this(pVRTextureFormat, textureOptions, null);
    }

    public PVRTexture(PVRTextureFormat pVRTextureFormat, TextureOptions textureOptions, ITexture.ITextureStateListener iTextureStateListener) {
        super(pVRTextureFormat.getPixelFormat(), textureOptions, iTextureStateListener);
        InputStream inputStream;
        Throwable th;
        try {
            InputStream inputStream2 = getInputStream();
            try {
                this.mPVRTextureHeader = new PVRTextureHeader(StreamUtils.streamToBytes(inputStream2, 52));
                StreamUtils.close(inputStream2);
                if (!MathUtils.isPowerOfTwo(getWidth()) || !MathUtils.isPowerOfTwo(getHeight())) {
                    throw new IllegalArgumentException("mWidth and mHeight must be a power of 2!");
                } else if (this.mPVRTextureHeader.getPVRTextureFormat().getPixelFormat() != pVRTextureFormat.getPixelFormat()) {
                    throw new IllegalArgumentException("Other PVRTextureFormat: '" + this.mPVRTextureHeader.getPVRTextureFormat().getPixelFormat() + "' found than expected: '" + pVRTextureFormat.getPixelFormat() + "'.");
                } else if (this.mPVRTextureHeader.getPVRTextureFormat().isCompressed()) {
                    throw new IllegalArgumentException("Invalid PVRTextureFormat: '" + this.mPVRTextureHeader.getPVRTextureFormat() + "'.");
                } else {
                    this.mUpdateOnHardwareNeeded = true;
                }
            } catch (Throwable th2) {
                Throwable th3 = th2;
                inputStream = inputStream2;
                th = th3;
                StreamUtils.close(inputStream);
                throw th;
            }
        } catch (Throwable th4) {
            Throwable th5 = th4;
            inputStream = null;
            th = th5;
        }
    }

    public int getWidth() {
        return this.mPVRTextureHeader.getWidth();
    }

    public int getHeight() {
        return this.mPVRTextureHeader.getHeight();
    }

    public PVRTextureHeader getPVRTextureHeader() {
        return this.mPVRTextureHeader;
    }

    /* access modifiers changed from: protected */
    public InputStream getInputStream() {
        return onGetInputStream();
    }

    /* access modifiers changed from: protected */
    public void generateHardwareTextureID(GL10 gl10) {
        gl10.glPixelStorei(3317, 1);
        super.generateHardwareTextureID(gl10);
    }

    /* access modifiers changed from: protected */
    public void writeTextureToHardware(GL10 gl10) {
        InputStream inputStream = getInputStream();
        try {
            byte[] streamToBytes = StreamUtils.streamToBytes(inputStream);
            ByteBuffer wrap = ByteBuffer.wrap(streamToBytes);
            wrap.rewind();
            wrap.order(ByteOrder.LITTLE_ENDIAN);
            int width = getWidth();
            int height = getHeight();
            int dataLength = this.mPVRTextureHeader.getDataLength();
            int gLFormat = this.mPixelFormat.getGLFormat();
            int gLType = this.mPixelFormat.getGLType();
            int bitsPerPixel = this.mPVRTextureHeader.getBitsPerPixel() / 8;
            int i = 0;
            int i2 = 0;
            int i3 = height;
            int i4 = width;
            while (i2 < dataLength) {
                int i5 = i4 * i3 * bitsPerPixel;
                ByteBuffer order = ByteBuffer.allocate(i5).order(ByteOrder.nativeOrder());
                order.put(streamToBytes, i2 + 52, i5);
                if (i > 0 && !(i4 == i3 && MathUtils.nextPowerOfTwo(i4) == i4)) {
                    Debug.w(String.format("Mipmap level '%u' is not squared. Width: '%u', height: '%u'. Texture won't render correctly.", Integer.valueOf(i), Integer.valueOf(i4), Integer.valueOf(i3)));
                }
                gl10.glTexImage2D(3553, i, gLFormat, i4, i3, 0, gLFormat, gLType, order);
                GLHelper.checkGLError(gl10);
                int i6 = i2 + i5;
                int max = Math.max(i4 >> 1, 1);
                i++;
                i2 = i6;
                i3 = Math.max(i3 >> 1, 1);
                i4 = max;
            }
        } finally {
            StreamUtils.close(inputStream);
        }
    }

    public class PVRTextureHeader {
        private static final int FORMAT_FLAG_MASK = 255;
        public static final byte[] MAGIC_IDENTIFIER = {80, 86, 82, 33};
        public static final int SIZE = 52;
        private final ByteBuffer mDataByteBuffer;
        private final PVRTextureFormat mPVRTextureFormat;

        public PVRTextureHeader(byte[] bArr) {
            this.mDataByteBuffer = ByteBuffer.wrap(bArr);
            this.mDataByteBuffer.rewind();
            this.mDataByteBuffer.order(ByteOrder.LITTLE_ENDIAN);
            if (!ArrayUtils.equals(bArr, 44, MAGIC_IDENTIFIER, 0, MAGIC_IDENTIFIER.length)) {
                throw new IllegalArgumentException("Invalid " + getClass().getSimpleName() + "!");
            }
            this.mPVRTextureFormat = PVRTextureFormat.fromID(getFlags() & FORMAT_FLAG_MASK);
        }

        public PVRTextureFormat getPVRTextureFormat() {
            return this.mPVRTextureFormat;
        }

        public int headerLength() {
            return this.mDataByteBuffer.getInt(0);
        }

        public int getHeight() {
            return this.mDataByteBuffer.getInt(4);
        }

        public int getWidth() {
            return this.mDataByteBuffer.getInt(8);
        }

        public int getNumMipmaps() {
            return this.mDataByteBuffer.getInt(12);
        }

        public int getFlags() {
            return this.mDataByteBuffer.getInt(16);
        }

        public int getDataLength() {
            return this.mDataByteBuffer.getInt(20);
        }

        public int getBitsPerPixel() {
            return this.mDataByteBuffer.getInt(24);
        }

        public int getBitmaskRed() {
            return this.mDataByteBuffer.getInt(28);
        }

        public int getBitmaskGreen() {
            return this.mDataByteBuffer.getInt(32);
        }

        public int getBitmaskBlue() {
            return this.mDataByteBuffer.getInt(36);
        }

        public int getBitmaskAlpha() {
            return this.mDataByteBuffer.getInt(40);
        }

        public boolean hasAlpha() {
            return getBitmaskAlpha() != 0;
        }

        public int getPVRTag() {
            return this.mDataByteBuffer.getInt(44);
        }

        public int numSurfs() {
            return this.mDataByteBuffer.getInt(48);
        }
    }

    public enum PVRTextureFormat {
        RGBA_4444(16, false, Texture.PixelFormat.RGBA_4444),
        RGBA_5551(17, false, Texture.PixelFormat.RGBA_5551),
        RGBA_8888(18, false, Texture.PixelFormat.RGBA_8888),
        RGB_565(19, false, Texture.PixelFormat.RGB_565),
        I_8(22, false, Texture.PixelFormat.I_8),
        AI_88(23, false, Texture.PixelFormat.AI_88),
        A_8(27, false, Texture.PixelFormat.A_8);
        
        private static /* synthetic */ int[] $SWITCH_TABLE$org$anddev$andengine$opengl$texture$Texture$PixelFormat;
        private final boolean mCompressed;
        private final int mID;
        private final Texture.PixelFormat mPixelFormat;

        private PVRTextureFormat(int i, boolean z, Texture.PixelFormat pixelFormat) {
            this.mID = i;
            this.mCompressed = z;
            this.mPixelFormat = pixelFormat;
        }

        public static PVRTextureFormat fromID(int i) {
            for (PVRTextureFormat pVRTextureFormat : values()) {
                if (pVRTextureFormat.mID == i) {
                    return pVRTextureFormat;
                }
            }
            throw new IllegalArgumentException("Unexpected " + PVRTextureFormat.class.getSimpleName() + "-ID: '" + i + "'.");
        }

        public static PVRTextureFormat fromPixelFormat(Texture.PixelFormat pixelFormat) {
            switch ($SWITCH_TABLE$org$anddev$andengine$opengl$texture$Texture$PixelFormat()[pixelFormat.ordinal()]) {
                case 4:
                    break;
                default:
                    throw new IllegalArgumentException("Unsupported " + Texture.PixelFormat.class.getName() + ": '" + pixelFormat + "'.");
            }
            return RGBA_8888;
        }

        public final int getID() {
            return this.mID;
        }

        public final boolean isCompressed() {
            return this.mCompressed;
        }

        public final Texture.PixelFormat getPixelFormat() {
            return this.mPixelFormat;
        }
    }
}
