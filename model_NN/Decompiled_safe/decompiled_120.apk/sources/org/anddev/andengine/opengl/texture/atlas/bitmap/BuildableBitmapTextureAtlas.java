package org.anddev.andengine.opengl.texture.atlas.bitmap;

import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.atlas.ITextureAtlas;
import org.anddev.andengine.opengl.texture.atlas.buildable.BuildableTextureAtlas;
import org.anddev.andengine.opengl.texture.bitmap.BitmapTexture;

public class BuildableBitmapTextureAtlas extends BuildableTextureAtlas {
    public BuildableBitmapTextureAtlas(int i, int i2) {
        this(i, i2, BitmapTexture.BitmapTextureFormat.RGBA_8888);
    }

    public BuildableBitmapTextureAtlas(int i, int i2, BitmapTexture.BitmapTextureFormat bitmapTextureFormat) {
        this(i, i2, bitmapTextureFormat, TextureOptions.DEFAULT, null);
    }

    public BuildableBitmapTextureAtlas(int i, int i2, ITextureAtlas.ITextureAtlasStateListener iTextureAtlasStateListener) {
        this(i, i2, BitmapTexture.BitmapTextureFormat.RGBA_8888, TextureOptions.DEFAULT, iTextureAtlasStateListener);
    }

    public BuildableBitmapTextureAtlas(int i, int i2, BitmapTexture.BitmapTextureFormat bitmapTextureFormat, ITextureAtlas.ITextureAtlasStateListener iTextureAtlasStateListener) {
        this(i, i2, bitmapTextureFormat, TextureOptions.DEFAULT, iTextureAtlasStateListener);
    }

    public BuildableBitmapTextureAtlas(int i, int i2, TextureOptions textureOptions) {
        this(i, i2, BitmapTexture.BitmapTextureFormat.RGBA_8888, textureOptions, null);
    }

    public BuildableBitmapTextureAtlas(int i, int i2, BitmapTexture.BitmapTextureFormat bitmapTextureFormat, TextureOptions textureOptions) {
        this(i, i2, bitmapTextureFormat, textureOptions, null);
    }

    public BuildableBitmapTextureAtlas(int i, int i2, TextureOptions textureOptions, ITextureAtlas.ITextureAtlasStateListener iTextureAtlasStateListener) {
        this(i, i2, BitmapTexture.BitmapTextureFormat.RGBA_8888, textureOptions, iTextureAtlasStateListener);
    }

    public BuildableBitmapTextureAtlas(int i, int i2, BitmapTexture.BitmapTextureFormat bitmapTextureFormat, TextureOptions textureOptions, ITextureAtlas.ITextureAtlasStateListener iTextureAtlasStateListener) {
        super(new BitmapTextureAtlas(i, i2, bitmapTextureFormat, textureOptions, iTextureAtlasStateListener));
    }
}
