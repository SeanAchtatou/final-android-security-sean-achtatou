package org.anddev.andengine.opengl.texture.atlas;

import java.util.ArrayList;
import org.anddev.andengine.opengl.texture.Texture;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.atlas.ITextureAtlas;
import org.anddev.andengine.opengl.texture.source.ITextureAtlasSource;
import org.anddev.andengine.util.MathUtils;

public abstract class TextureAtlas extends Texture implements ITextureAtlas {
    protected final int mHeight;
    protected final ArrayList mTextureAtlasSources = new ArrayList();
    protected final int mWidth;

    public TextureAtlas(int i, int i2, Texture.PixelFormat pixelFormat, TextureOptions textureOptions, ITextureAtlas.ITextureAtlasStateListener iTextureAtlasStateListener) {
        super(pixelFormat, textureOptions, iTextureAtlasStateListener);
        if (!MathUtils.isPowerOfTwo(i) || !MathUtils.isPowerOfTwo(i2)) {
            throw new IllegalArgumentException("pWidth and pHeight must be a power of 2!");
        }
        this.mWidth = i;
        this.mHeight = i2;
    }

    public int getWidth() {
        return this.mWidth;
    }

    public int getHeight() {
        return this.mHeight;
    }

    public ITextureAtlas.ITextureAtlasStateListener getTextureStateListener() {
        return (ITextureAtlas.ITextureAtlasStateListener) super.getTextureStateListener();
    }

    public void addTextureAtlasSource(ITextureAtlasSource iTextureAtlasSource, int i, int i2) {
        checkTextureAtlasSourcePosition(iTextureAtlasSource, i, i2);
        iTextureAtlasSource.setTexturePositionX(i);
        iTextureAtlasSource.setTexturePositionY(i2);
        this.mTextureAtlasSources.add(iTextureAtlasSource);
        this.mUpdateOnHardwareNeeded = true;
    }

    public void removeTextureAtlasSource(ITextureAtlasSource iTextureAtlasSource, int i, int i2) {
        ArrayList arrayList = this.mTextureAtlasSources;
        for (int size = arrayList.size() - 1; size >= 0; size--) {
            ITextureAtlasSource iTextureAtlasSource2 = (ITextureAtlasSource) arrayList.get(size);
            if (iTextureAtlasSource2 == iTextureAtlasSource && iTextureAtlasSource2.getTexturePositionX() == i && iTextureAtlasSource2.getTexturePositionY() == i2) {
                arrayList.remove(size);
                this.mUpdateOnHardwareNeeded = true;
                return;
            }
        }
    }

    public void clearTextureAtlasSources() {
        this.mTextureAtlasSources.clear();
        this.mUpdateOnHardwareNeeded = true;
    }

    private void checkTextureAtlasSourcePosition(ITextureAtlasSource iTextureAtlasSource, int i, int i2) {
        if (i < 0) {
            throw new IllegalArgumentException("Illegal negative pTexturePositionX supplied: '" + i + "'");
        } else if (i2 < 0) {
            throw new IllegalArgumentException("Illegal negative pTexturePositionY supplied: '" + i2 + "'");
        } else if (iTextureAtlasSource.getWidth() + i > getWidth() || iTextureAtlasSource.getHeight() + i2 > getHeight()) {
            throw new IllegalArgumentException("Supplied pTextureAtlasSource must not exceed bounds of Texture.");
        }
    }
}
