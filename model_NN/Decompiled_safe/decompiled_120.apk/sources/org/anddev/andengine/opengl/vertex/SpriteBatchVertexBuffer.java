package org.anddev.andengine.opengl.vertex;

import org.anddev.andengine.opengl.util.FastFloatBuffer;
import org.anddev.andengine.util.Transformation;

public class SpriteBatchVertexBuffer extends VertexBuffer {
    private static final Transformation TRANSFORATION_TMP = new Transformation();
    public static final int VERTICES_PER_RECTANGLE = 6;
    private static final float[] VERTICES_TMP = new float[8];
    protected int mIndex;

    public SpriteBatchVertexBuffer(int i, int i2, boolean z) {
        super(i * 2 * 6, i2, z);
    }

    public int getIndex() {
        return this.mIndex;
    }

    public void setIndex(int i) {
        this.mIndex = i;
    }

    public void add(float f, float f2, float f3, float f4, float f5) {
        float f6 = f3 * 0.5f;
        float f7 = 0.5f * f4;
        TRANSFORATION_TMP.setToIdentity();
        TRANSFORATION_TMP.postTranslate(-f6, -f7);
        TRANSFORATION_TMP.postRotate(f5);
        TRANSFORATION_TMP.postTranslate(f6, f7);
        TRANSFORATION_TMP.postTranslate(f, f2);
        add(f3, f4, TRANSFORATION_TMP);
    }

    public void add(float f, float f2, float f3, float f4, float f5, float f6) {
        float f7 = f3 * 0.5f;
        float f8 = 0.5f * f4;
        TRANSFORATION_TMP.setToIdentity();
        TRANSFORATION_TMP.postTranslate(-f7, -f8);
        TRANSFORATION_TMP.postScale(f5, f6);
        TRANSFORATION_TMP.postTranslate(f7, f8);
        TRANSFORATION_TMP.postTranslate(f, f2);
        add(f3, f4, TRANSFORATION_TMP);
    }

    public void add(float f, float f2, float f3, float f4, float f5, float f6, float f7) {
        float f8 = f3 * 0.5f;
        float f9 = 0.5f * f4;
        TRANSFORATION_TMP.setToIdentity();
        TRANSFORATION_TMP.postTranslate(-f8, -f9);
        TRANSFORATION_TMP.postScale(f6, f7);
        TRANSFORATION_TMP.postRotate(f5);
        TRANSFORATION_TMP.postTranslate(f8, f9);
        TRANSFORATION_TMP.postTranslate(f, f2);
        add(f3, f4, TRANSFORATION_TMP);
    }

    public void add(float f, float f2, Transformation transformation) {
        VERTICES_TMP[0] = 0.0f;
        VERTICES_TMP[1] = 0.0f;
        VERTICES_TMP[2] = 0.0f;
        VERTICES_TMP[3] = f2;
        VERTICES_TMP[4] = f;
        VERTICES_TMP[5] = 0.0f;
        VERTICES_TMP[6] = f;
        VERTICES_TMP[7] = f2;
        transformation.transform(VERTICES_TMP);
        addInner(VERTICES_TMP[0], VERTICES_TMP[1], VERTICES_TMP[2], VERTICES_TMP[3], VERTICES_TMP[4], VERTICES_TMP[5], VERTICES_TMP[6], VERTICES_TMP[7]);
    }

    public void add(float f, float f2, float f3, float f4) {
        addInner(f, f2, f + f3, f2 + f4);
    }

    public void addInner(float f, float f2, float f3, float f4) {
        int floatToRawIntBits = Float.floatToRawIntBits(f);
        int floatToRawIntBits2 = Float.floatToRawIntBits(f2);
        int floatToRawIntBits3 = Float.floatToRawIntBits(f3);
        int floatToRawIntBits4 = Float.floatToRawIntBits(f4);
        int[] iArr = this.mBufferData;
        int i = this.mIndex;
        int i2 = i + 1;
        iArr[i] = floatToRawIntBits;
        int i3 = i2 + 1;
        iArr[i2] = floatToRawIntBits2;
        int i4 = i3 + 1;
        iArr[i3] = floatToRawIntBits;
        int i5 = i4 + 1;
        iArr[i4] = floatToRawIntBits4;
        int i6 = i5 + 1;
        iArr[i5] = floatToRawIntBits3;
        int i7 = i6 + 1;
        iArr[i6] = floatToRawIntBits2;
        int i8 = i7 + 1;
        iArr[i7] = floatToRawIntBits3;
        int i9 = i8 + 1;
        iArr[i8] = floatToRawIntBits2;
        int i10 = i9 + 1;
        iArr[i9] = floatToRawIntBits;
        int i11 = i10 + 1;
        iArr[i10] = floatToRawIntBits4;
        int i12 = i11 + 1;
        iArr[i11] = floatToRawIntBits3;
        iArr[i12] = floatToRawIntBits4;
        this.mIndex = i12 + 1;
    }

    public void addInner(float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8) {
        int floatToRawIntBits = Float.floatToRawIntBits(f);
        int floatToRawIntBits2 = Float.floatToRawIntBits(f2);
        int floatToRawIntBits3 = Float.floatToRawIntBits(f3);
        int floatToRawIntBits4 = Float.floatToRawIntBits(f4);
        int floatToRawIntBits5 = Float.floatToRawIntBits(f5);
        int floatToRawIntBits6 = Float.floatToRawIntBits(f6);
        int floatToRawIntBits7 = Float.floatToRawIntBits(f7);
        int floatToRawIntBits8 = Float.floatToRawIntBits(f8);
        int[] iArr = this.mBufferData;
        int i = this.mIndex;
        int i2 = i + 1;
        iArr[i] = floatToRawIntBits;
        int i3 = i2 + 1;
        iArr[i2] = floatToRawIntBits2;
        int i4 = i3 + 1;
        iArr[i3] = floatToRawIntBits3;
        int i5 = i4 + 1;
        iArr[i4] = floatToRawIntBits4;
        int i6 = i5 + 1;
        iArr[i5] = floatToRawIntBits5;
        int i7 = i6 + 1;
        iArr[i6] = floatToRawIntBits6;
        int i8 = i7 + 1;
        iArr[i7] = floatToRawIntBits5;
        int i9 = i8 + 1;
        iArr[i8] = floatToRawIntBits6;
        int i10 = i9 + 1;
        iArr[i9] = floatToRawIntBits3;
        int i11 = i10 + 1;
        iArr[i10] = floatToRawIntBits4;
        int i12 = i11 + 1;
        iArr[i11] = floatToRawIntBits7;
        iArr[i12] = floatToRawIntBits8;
        this.mIndex = i12 + 1;
    }

    public void submit() {
        FastFloatBuffer fastFloatBuffer = this.mFloatBuffer;
        fastFloatBuffer.position(0);
        fastFloatBuffer.put(this.mBufferData);
        fastFloatBuffer.position(0);
        super.setHardwareBufferNeedsUpdate();
    }
}
