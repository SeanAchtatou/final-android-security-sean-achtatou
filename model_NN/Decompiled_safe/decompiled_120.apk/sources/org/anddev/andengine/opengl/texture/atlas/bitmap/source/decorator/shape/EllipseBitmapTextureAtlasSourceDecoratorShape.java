package org.anddev.andengine.opengl.texture.atlas.bitmap.source.decorator.shape;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import org.anddev.andengine.opengl.texture.atlas.bitmap.source.decorator.BaseBitmapTextureAtlasSourceDecorator;

public class EllipseBitmapTextureAtlasSourceDecoratorShape implements IBitmapTextureAtlasSourceDecoratorShape {
    private static EllipseBitmapTextureAtlasSourceDecoratorShape sDefaultInstance;
    private final RectF mRectF = new RectF();

    public static EllipseBitmapTextureAtlasSourceDecoratorShape getDefaultInstance() {
        if (sDefaultInstance == null) {
            sDefaultInstance = new EllipseBitmapTextureAtlasSourceDecoratorShape();
        }
        return sDefaultInstance;
    }

    public void onDecorateBitmap(Canvas canvas, Paint paint, BaseBitmapTextureAtlasSourceDecorator.TextureAtlasSourceDecoratorOptions textureAtlasSourceDecoratorOptions) {
        this.mRectF.set(textureAtlasSourceDecoratorOptions.getInsetLeft(), textureAtlasSourceDecoratorOptions.getInsetTop(), ((float) canvas.getWidth()) - textureAtlasSourceDecoratorOptions.getInsetRight(), ((float) canvas.getHeight()) - textureAtlasSourceDecoratorOptions.getInsetBottom());
        canvas.drawOval(this.mRectF, paint);
    }
}
