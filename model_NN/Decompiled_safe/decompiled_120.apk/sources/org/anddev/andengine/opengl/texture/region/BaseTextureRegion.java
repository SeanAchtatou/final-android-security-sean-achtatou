package org.anddev.andengine.opengl.texture.region;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;
import org.anddev.andengine.opengl.texture.ITexture;
import org.anddev.andengine.opengl.texture.region.buffer.TextureRegionBuffer;
import org.anddev.andengine.opengl.texture.region.crop.TextureRegionCrop;
import org.anddev.andengine.opengl.util.GLHelper;

public abstract class BaseTextureRegion {
    protected int mHeight;
    protected final ITexture mTexture;
    protected int mTexturePositionX;
    protected int mTexturePositionY;
    protected final TextureRegionBuffer mTextureRegionBuffer = new TextureRegionBuffer(this, 35044, true);
    protected final TextureRegionCrop mTextureRegionCrop = new TextureRegionCrop(this);
    protected int mWidth;

    public abstract float getTextureCoordinateX1();

    public abstract float getTextureCoordinateX2();

    public abstract float getTextureCoordinateY1();

    public abstract float getTextureCoordinateY2();

    public abstract int getTextureCropHeight();

    public abstract int getTextureCropLeft();

    public abstract int getTextureCropTop();

    public abstract int getTextureCropWidth();

    public BaseTextureRegion(ITexture iTexture, int i, int i2, int i3, int i4) {
        this.mTexture = iTexture;
        this.mTexturePositionX = i;
        this.mTexturePositionY = i2;
        this.mWidth = i3;
        this.mHeight = i4;
        initTextureBuffer();
    }

    /* access modifiers changed from: protected */
    public void initTextureBuffer() {
        updateTextureRegionBuffer();
    }

    public int getWidth() {
        return this.mWidth;
    }

    public int getHeight() {
        return this.mHeight;
    }

    public void setWidth(int i) {
        this.mWidth = i;
        updateTextureRegionBuffer();
    }

    public void setHeight(int i) {
        this.mHeight = i;
        updateTextureRegionBuffer();
    }

    public void setTexturePosition(int i, int i2) {
        this.mTexturePositionX = i;
        this.mTexturePositionY = i2;
        updateTextureRegionBuffer();
    }

    public int getTexturePositionX() {
        return this.mTexturePositionX;
    }

    public int getTexturePositionY() {
        return this.mTexturePositionY;
    }

    public ITexture getTexture() {
        return this.mTexture;
    }

    public TextureRegionBuffer getTextureBuffer() {
        return this.mTextureRegionBuffer;
    }

    public TextureRegionCrop getTexureRegionCrop() {
        return this.mTextureRegionCrop;
    }

    public boolean isFlippedHorizontal() {
        return this.mTextureRegionBuffer.isFlippedHorizontal();
    }

    public void setFlippedHorizontal(boolean z) {
        this.mTextureRegionBuffer.setFlippedHorizontal(z);
        this.mTextureRegionCrop.setFlippedHorizontal(z);
    }

    public boolean isFlippedVertical() {
        return this.mTextureRegionBuffer.isFlippedVertical();
    }

    public void setFlippedVertical(boolean z) {
        this.mTextureRegionBuffer.setFlippedVertical(z);
        this.mTextureRegionCrop.setFlippedVertical(z);
    }

    public boolean isTextureRegionBufferManaged() {
        return this.mTextureRegionBuffer.isManaged();
    }

    public void setTextureRegionBufferManaged(boolean z) {
        this.mTextureRegionBuffer.setManaged(z);
    }

    /* access modifiers changed from: protected */
    public void updateTextureRegionBuffer() {
        this.mTextureRegionBuffer.update();
        this.mTextureRegionCrop.update();
    }

    public void onApply(GL10 gl10) {
        this.mTexture.bind(gl10);
        if (GLHelper.EXTENSIONS_VERTEXBUFFEROBJECTS) {
            GL11 gl11 = (GL11) gl10;
            this.mTextureRegionBuffer.selectOnHardware(gl11);
            GLHelper.texCoordZeroPointer(gl11);
            return;
        }
        GLHelper.texCoordPointer(gl10, this.mTextureRegionBuffer.getFloatBuffer());
    }

    public void onApplyCrop(GL11 gl11) {
        this.mTexture.bind(gl11);
        this.mTextureRegionCrop.apply(gl11);
    }
}
