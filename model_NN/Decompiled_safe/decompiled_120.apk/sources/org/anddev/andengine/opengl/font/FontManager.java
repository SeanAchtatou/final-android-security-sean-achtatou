package org.anddev.andengine.opengl.font;

import java.util.ArrayList;
import javax.microedition.khronos.opengles.GL10;

public class FontManager {
    private final ArrayList mFontsManaged = new ArrayList();

    public void clear() {
        this.mFontsManaged.clear();
    }

    public void loadFont(Font font) {
        if (font == null) {
            throw new IllegalArgumentException("pFont must not be null!");
        }
        this.mFontsManaged.add(font);
    }

    public void loadFonts(FontLibrary fontLibrary) {
        fontLibrary.loadFonts(this);
    }

    public void loadFonts(Font... fontArr) {
        for (int length = fontArr.length - 1; length >= 0; length--) {
            loadFont(fontArr[length]);
        }
    }

    public void updateFonts(GL10 gl10) {
        ArrayList arrayList = this.mFontsManaged;
        int size = arrayList.size();
        if (size > 0) {
            for (int i = size - 1; i >= 0; i--) {
                ((Font) arrayList.get(i)).update(gl10);
            }
        }
    }

    public void reloadFonts() {
        ArrayList arrayList = this.mFontsManaged;
        for (int size = arrayList.size() - 1; size >= 0; size--) {
            ((Font) arrayList.get(size)).reload();
        }
    }
}
