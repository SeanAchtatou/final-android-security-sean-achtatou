package org.anddev.andengine.opengl.util;

import java.lang.ref.SoftReference;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

public class FastFloatBuffer {
    private static SoftReference sWeakIntArray = new SoftReference(new int[0]);
    public final ByteBuffer mByteBuffer;
    private final FloatBuffer mFloatBuffer = this.mByteBuffer.asFloatBuffer();
    private final IntBuffer mIntBuffer = this.mByteBuffer.asIntBuffer();

    public FastFloatBuffer(int i) {
        this.mByteBuffer = ByteBuffer.allocateDirect(i * 4).order(ByteOrder.nativeOrder());
    }

    public void flip() {
        this.mByteBuffer.flip();
        this.mFloatBuffer.flip();
        this.mIntBuffer.flip();
    }

    public void put(float f) {
        ByteBuffer byteBuffer = this.mByteBuffer;
        IntBuffer intBuffer = this.mIntBuffer;
        byteBuffer.position(byteBuffer.position() + 4);
        this.mFloatBuffer.put(f);
        intBuffer.position(intBuffer.position() + 1);
    }

    public void put(float[] fArr) {
        int length = fArr.length;
        int[] iArr = (int[]) sWeakIntArray.get();
        if (iArr == null || iArr.length < length) {
            iArr = new int[length];
            sWeakIntArray = new SoftReference(iArr);
        }
        for (int i = 0; i < length; i++) {
            iArr[i] = Float.floatToRawIntBits(fArr[i]);
        }
        ByteBuffer byteBuffer = this.mByteBuffer;
        byteBuffer.position(byteBuffer.position() + (length * 4));
        FloatBuffer floatBuffer = this.mFloatBuffer;
        floatBuffer.position(floatBuffer.position() + length);
        this.mIntBuffer.put(iArr, 0, length);
    }

    public void put(int[] iArr) {
        ByteBuffer byteBuffer = this.mByteBuffer;
        byteBuffer.position(byteBuffer.position() + (iArr.length * 4));
        FloatBuffer floatBuffer = this.mFloatBuffer;
        floatBuffer.position(floatBuffer.position() + iArr.length);
        this.mIntBuffer.put(iArr, 0, iArr.length);
    }

    public static int[] convert(float... fArr) {
        int length = fArr.length;
        int[] iArr = new int[length];
        for (int i = 0; i < length; i++) {
            iArr[i] = Float.floatToRawIntBits(fArr[i]);
        }
        return iArr;
    }

    public void put(FastFloatBuffer fastFloatBuffer) {
        ByteBuffer byteBuffer = this.mByteBuffer;
        byteBuffer.put(fastFloatBuffer.mByteBuffer);
        this.mFloatBuffer.position(byteBuffer.position() >> 2);
        this.mIntBuffer.position(byteBuffer.position() >> 2);
    }

    public int capacity() {
        return this.mFloatBuffer.capacity();
    }

    public int position() {
        return this.mFloatBuffer.position();
    }

    public void position(int i) {
        this.mByteBuffer.position(i * 4);
        this.mFloatBuffer.position(i);
        this.mIntBuffer.position(i);
    }

    public FloatBuffer slice() {
        return this.mFloatBuffer.slice();
    }

    public int remaining() {
        return this.mFloatBuffer.remaining();
    }

    public int limit() {
        return this.mFloatBuffer.limit();
    }

    public void clear() {
        this.mByteBuffer.clear();
        this.mFloatBuffer.clear();
        this.mIntBuffer.clear();
    }
}
