package org.anddev.andengine.opengl.texture;

import javax.microedition.khronos.opengles.GL10;
import org.anddev.andengine.opengl.texture.ITexture;
import org.anddev.andengine.opengl.util.GLHelper;

public abstract class Texture implements ITexture {
    private static final int[] HARDWARETEXTUREID_FETCHER = new int[1];
    protected int mHardwareTextureID = -1;
    protected boolean mLoadedToHardware;
    protected final PixelFormat mPixelFormat;
    protected final TextureOptions mTextureOptions;
    protected final ITexture.ITextureStateListener mTextureStateListener;
    protected boolean mUpdateOnHardwareNeeded = false;

    /* access modifiers changed from: protected */
    public abstract void writeTextureToHardware(GL10 gl10);

    public Texture(PixelFormat pixelFormat, TextureOptions textureOptions, ITexture.ITextureStateListener iTextureStateListener) {
        this.mPixelFormat = pixelFormat;
        this.mTextureOptions = textureOptions;
        this.mTextureStateListener = iTextureStateListener;
    }

    public int getHardwareTextureID() {
        return this.mHardwareTextureID;
    }

    public boolean isLoadedToHardware() {
        return this.mLoadedToHardware;
    }

    public void setLoadedToHardware(boolean z) {
        this.mLoadedToHardware = z;
    }

    public boolean isUpdateOnHardwareNeeded() {
        return this.mUpdateOnHardwareNeeded;
    }

    public void setUpdateOnHardwareNeeded(boolean z) {
        this.mUpdateOnHardwareNeeded = z;
    }

    public PixelFormat getPixelFormat() {
        return this.mPixelFormat;
    }

    public TextureOptions getTextureOptions() {
        return this.mTextureOptions;
    }

    public ITexture.ITextureStateListener getTextureStateListener() {
        return this.mTextureStateListener;
    }

    public boolean hasTextureStateListener() {
        return this.mTextureStateListener != null;
    }

    public void loadToHardware(GL10 gl10) {
        GLHelper.enableTextures(gl10);
        generateHardwareTextureID(gl10);
        bindTextureOnHardware(gl10);
        applyTextureOptions(gl10);
        writeTextureToHardware(gl10);
        this.mUpdateOnHardwareNeeded = false;
        this.mLoadedToHardware = true;
        if (this.mTextureStateListener != null) {
            this.mTextureStateListener.onLoadedToHardware(this);
        }
    }

    public void unloadFromHardware(GL10 gl10) {
        GLHelper.enableTextures(gl10);
        deleteTextureOnHardware(gl10);
        this.mHardwareTextureID = -1;
        this.mLoadedToHardware = false;
        if (this.mTextureStateListener != null) {
            this.mTextureStateListener.onUnloadedFromHardware(this);
        }
    }

    public void reloadToHardware(GL10 gl10) {
        unloadFromHardware(gl10);
        loadToHardware(gl10);
    }

    public void bind(GL10 gl10) {
        GLHelper.bindTexture(gl10, this.mHardwareTextureID);
    }

    /* access modifiers changed from: protected */
    public void applyTextureOptions(GL10 gl10) {
        this.mTextureOptions.apply(gl10);
    }

    /* access modifiers changed from: protected */
    public void bindTextureOnHardware(GL10 gl10) {
        GLHelper.forceBindTexture(gl10, this.mHardwareTextureID);
    }

    /* access modifiers changed from: protected */
    public void deleteTextureOnHardware(GL10 gl10) {
        GLHelper.deleteTexture(gl10, this.mHardwareTextureID);
    }

    /* access modifiers changed from: protected */
    public void generateHardwareTextureID(GL10 gl10) {
        gl10.glGenTextures(1, HARDWARETEXTUREID_FETCHER, 0);
        this.mHardwareTextureID = HARDWARETEXTUREID_FETCHER[0];
    }

    public enum PixelFormat {
        UNDEFINED(-1, -1, -1),
        RGBA_4444(6408, 32819, 16),
        RGBA_5551(6408, 32820, 16),
        RGBA_8888(6408, 5121, 32),
        RGB_565(6407, 33635, 16),
        A_8(6406, 5121, 8),
        I_8(6409, 5121, 8),
        AI_88(6410, 5121, 16);
        
        private final int mBitsPerPixel;
        private final int mGLFormat;
        private final int mGLType;

        private PixelFormat(int i, int i2, int i3) {
            this.mGLFormat = i;
            this.mGLType = i2;
            this.mBitsPerPixel = i3;
        }

        public final int getGLFormat() {
            return this.mGLFormat;
        }

        public final int getGLType() {
            return this.mGLType;
        }

        public final int getBitsPerPixel() {
            return this.mBitsPerPixel;
        }
    }
}
