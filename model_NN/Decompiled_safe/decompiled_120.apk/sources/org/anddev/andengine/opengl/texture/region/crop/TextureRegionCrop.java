package org.anddev.andengine.opengl.texture.region.crop;

import javax.microedition.khronos.opengles.GL11;
import org.anddev.andengine.opengl.texture.region.BaseTextureRegion;
import org.anddev.andengine.opengl.util.GLHelper;

public class TextureRegionCrop {
    private static final int BOTTOM_INDEX = 1;
    private static final int HEIGHT_INDEX = 3;
    private static final int LEFT_INDEX = 0;
    private static final int WIDTH_INDEX = 2;
    private final int[] mData = new int[4];
    private boolean mDirty = true;
    private boolean mFlippedHorizontal;
    private boolean mFlippedVertical;
    protected final BaseTextureRegion mTextureRegion;

    public TextureRegionCrop(BaseTextureRegion baseTextureRegion) {
        this.mTextureRegion = baseTextureRegion;
    }

    public boolean isDirty() {
        return this.mDirty;
    }

    public int[] getData() {
        return this.mData;
    }

    public boolean isFlippedHorizontal() {
        return this.mFlippedHorizontal;
    }

    public void setFlippedHorizontal(boolean z) {
        if (this.mFlippedHorizontal != z) {
            this.mFlippedHorizontal = z;
            update();
        }
    }

    public boolean isFlippedVertical() {
        return this.mFlippedVertical;
    }

    public void setFlippedVertical(boolean z) {
        if (this.mFlippedVertical != z) {
            this.mFlippedVertical = z;
            update();
        }
    }

    public void update() {
        BaseTextureRegion baseTextureRegion = this.mTextureRegion;
        if (baseTextureRegion.getTexture() != null) {
            int[] iArr = this.mData;
            int textureCropLeft = baseTextureRegion.getTextureCropLeft();
            int textureCropTop = baseTextureRegion.getTextureCropTop();
            int textureCropWidth = baseTextureRegion.getTextureCropWidth();
            int textureCropHeight = baseTextureRegion.getTextureCropHeight();
            if (!this.mFlippedVertical && !this.mFlippedHorizontal) {
                iArr[0] = textureCropLeft;
                iArr[1] = textureCropTop + textureCropHeight;
                iArr[2] = textureCropWidth;
                iArr[3] = -textureCropHeight;
            }
            this.mDirty = true;
        }
    }

    public void selectOnHardware(GL11 gl11) {
        if (this.mDirty) {
            this.mDirty = false;
            synchronized (this) {
                GLHelper.textureCrop(gl11, this);
            }
        }
    }

    public void apply(GL11 gl11) {
        GLHelper.textureCrop(gl11, this);
    }
}
