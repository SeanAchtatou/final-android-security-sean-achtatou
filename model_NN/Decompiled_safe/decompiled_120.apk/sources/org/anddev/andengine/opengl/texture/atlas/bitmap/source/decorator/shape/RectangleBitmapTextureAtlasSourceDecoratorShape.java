package org.anddev.andengine.opengl.texture.atlas.bitmap.source.decorator.shape;

import android.graphics.Canvas;
import android.graphics.Paint;
import org.anddev.andengine.opengl.texture.atlas.bitmap.source.decorator.BaseBitmapTextureAtlasSourceDecorator;

public class RectangleBitmapTextureAtlasSourceDecoratorShape implements IBitmapTextureAtlasSourceDecoratorShape {
    private static RectangleBitmapTextureAtlasSourceDecoratorShape sDefaultInstance;

    public static RectangleBitmapTextureAtlasSourceDecoratorShape getDefaultInstance() {
        if (sDefaultInstance == null) {
            sDefaultInstance = new RectangleBitmapTextureAtlasSourceDecoratorShape();
        }
        return sDefaultInstance;
    }

    public void onDecorateBitmap(Canvas canvas, Paint paint, BaseBitmapTextureAtlasSourceDecorator.TextureAtlasSourceDecoratorOptions textureAtlasSourceDecoratorOptions) {
        canvas.drawRect(textureAtlasSourceDecoratorOptions.getInsetLeft(), textureAtlasSourceDecoratorOptions.getInsetTop(), ((float) canvas.getWidth()) - textureAtlasSourceDecoratorOptions.getInsetRight(), ((float) canvas.getHeight()) - textureAtlasSourceDecoratorOptions.getInsetBottom(), paint);
    }
}
