package org.anddev.andengine.opengl.texture.atlas;

import org.anddev.andengine.opengl.texture.ITexture;
import org.anddev.andengine.opengl.texture.source.ITextureAtlasSource;
import org.anddev.andengine.util.Debug;

public interface ITextureAtlas extends ITexture {
    void addTextureAtlasSource(ITextureAtlasSource iTextureAtlasSource, int i, int i2);

    void clearTextureAtlasSources();

    ITextureAtlasStateListener getTextureStateListener();

    void removeTextureAtlasSource(ITextureAtlasSource iTextureAtlasSource, int i, int i2);

    public interface ITextureAtlasStateListener extends ITexture.ITextureStateListener {
        void onTextureAtlasSourceLoadExeption(ITextureAtlas iTextureAtlas, ITextureAtlasSource iTextureAtlasSource, Throwable th);

        public class TextureAtlasStateAdapter implements ITextureAtlasStateListener {
            public void onLoadedToHardware(ITexture iTexture) {
            }

            public void onTextureAtlasSourceLoadExeption(ITextureAtlas iTextureAtlas, ITextureAtlasSource iTextureAtlasSource, Throwable th) {
            }

            public void onUnloadedFromHardware(ITexture iTexture) {
            }
        }

        public class DebugTextureAtlasStateListener implements ITextureAtlasStateListener {
            public void onLoadedToHardware(ITexture iTexture) {
                Debug.d("Texture loaded: " + iTexture.toString());
            }

            public void onTextureAtlasSourceLoadExeption(ITextureAtlas iTextureAtlas, ITextureAtlasSource iTextureAtlasSource, Throwable th) {
                Debug.e("Exception loading TextureAtlasSource. TextureAtlas: " + iTextureAtlas.toString() + " TextureAtlasSource: " + iTextureAtlasSource.toString(), th);
            }

            public void onUnloadedFromHardware(ITexture iTexture) {
                Debug.d("Texture unloaded: " + iTexture.toString());
            }
        }
    }
}
