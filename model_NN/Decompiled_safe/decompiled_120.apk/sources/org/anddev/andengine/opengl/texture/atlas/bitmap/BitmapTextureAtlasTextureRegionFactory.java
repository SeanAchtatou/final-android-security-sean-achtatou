package org.anddev.andengine.opengl.texture.atlas.bitmap;

import android.content.Context;
import org.anddev.andengine.opengl.texture.atlas.bitmap.source.AssetBitmapTextureAtlasSource;
import org.anddev.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource;
import org.anddev.andengine.opengl.texture.atlas.bitmap.source.ResourceBitmapTextureAtlasSource;
import org.anddev.andengine.opengl.texture.atlas.buildable.BuildableTextureAtlasTextureRegionFactory;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.texture.region.TextureRegionFactory;
import org.anddev.andengine.opengl.texture.region.TiledTextureRegion;

public class BitmapTextureAtlasTextureRegionFactory {
    private static String sAssetBasePath = "";
    private static boolean sCreateTextureRegionBuffersManaged;

    public static void setAssetBasePath(String str) {
        if (str.endsWith("/") || str.length() == 0) {
            sAssetBasePath = str;
            return;
        }
        throw new IllegalArgumentException("pAssetBasePath must end with '/' or be lenght zero.");
    }

    public static void setCreateTextureRegionBuffersManaged(boolean z) {
        sCreateTextureRegionBuffersManaged = z;
    }

    public static void reset() {
        setAssetBasePath("");
        sCreateTextureRegionBuffersManaged = false;
    }

    public static TextureRegion createFromAsset(BitmapTextureAtlas bitmapTextureAtlas, Context context, String str, int i, int i2) {
        return createFromSource(bitmapTextureAtlas, new AssetBitmapTextureAtlasSource(context, String.valueOf(sAssetBasePath) + str), i, i2);
    }

    public static TiledTextureRegion createTiledFromAsset(BitmapTextureAtlas bitmapTextureAtlas, Context context, String str, int i, int i2, int i3, int i4) {
        return createTiledFromSource(bitmapTextureAtlas, new AssetBitmapTextureAtlasSource(context, String.valueOf(sAssetBasePath) + str), i, i2, i3, i4);
    }

    public static TextureRegion createFromResource(BitmapTextureAtlas bitmapTextureAtlas, Context context, int i, int i2, int i3) {
        return createFromSource(bitmapTextureAtlas, new ResourceBitmapTextureAtlasSource(context, i), i2, i3);
    }

    public static TiledTextureRegion createTiledFromResource(BitmapTextureAtlas bitmapTextureAtlas, Context context, int i, int i2, int i3, int i4, int i5) {
        return createTiledFromSource(bitmapTextureAtlas, new ResourceBitmapTextureAtlasSource(context, i), i2, i3, i4, i5);
    }

    public static TextureRegion createFromSource(BitmapTextureAtlas bitmapTextureAtlas, IBitmapTextureAtlasSource iBitmapTextureAtlasSource, int i, int i2) {
        return TextureRegionFactory.createFromSource(bitmapTextureAtlas, iBitmapTextureAtlasSource, i, i2, sCreateTextureRegionBuffersManaged);
    }

    public static TiledTextureRegion createTiledFromSource(BitmapTextureAtlas bitmapTextureAtlas, IBitmapTextureAtlasSource iBitmapTextureAtlasSource, int i, int i2, int i3, int i4) {
        return TextureRegionFactory.createTiledFromSource(bitmapTextureAtlas, iBitmapTextureAtlasSource, i, i2, i3, i4, sCreateTextureRegionBuffersManaged);
    }

    public static TextureRegion createFromAsset(BuildableBitmapTextureAtlas buildableBitmapTextureAtlas, Context context, String str) {
        return createFromSource(buildableBitmapTextureAtlas, new AssetBitmapTextureAtlasSource(context, String.valueOf(sAssetBasePath) + str));
    }

    public static TiledTextureRegion createTiledFromAsset(BuildableBitmapTextureAtlas buildableBitmapTextureAtlas, Context context, String str, int i, int i2) {
        return createTiledFromSource(buildableBitmapTextureAtlas, new AssetBitmapTextureAtlasSource(context, String.valueOf(sAssetBasePath) + str), i, i2);
    }

    public static TextureRegion createFromResource(BuildableBitmapTextureAtlas buildableBitmapTextureAtlas, Context context, int i) {
        return createFromSource(buildableBitmapTextureAtlas, new ResourceBitmapTextureAtlasSource(context, i));
    }

    public static TiledTextureRegion createTiledFromResource(BuildableBitmapTextureAtlas buildableBitmapTextureAtlas, Context context, int i, int i2, int i3) {
        return createTiledFromSource(buildableBitmapTextureAtlas, new ResourceBitmapTextureAtlasSource(context, i), i2, i3);
    }

    public static TextureRegion createFromSource(BuildableBitmapTextureAtlas buildableBitmapTextureAtlas, IBitmapTextureAtlasSource iBitmapTextureAtlasSource) {
        return BuildableTextureAtlasTextureRegionFactory.createFromSource(buildableBitmapTextureAtlas, iBitmapTextureAtlasSource, sCreateTextureRegionBuffersManaged);
    }

    public static TiledTextureRegion createTiledFromSource(BuildableBitmapTextureAtlas buildableBitmapTextureAtlas, IBitmapTextureAtlasSource iBitmapTextureAtlasSource, int i, int i2) {
        return BuildableTextureAtlasTextureRegionFactory.createTiledFromSource(buildableBitmapTextureAtlas, iBitmapTextureAtlasSource, i, i2, sCreateTextureRegionBuffersManaged);
    }
}
