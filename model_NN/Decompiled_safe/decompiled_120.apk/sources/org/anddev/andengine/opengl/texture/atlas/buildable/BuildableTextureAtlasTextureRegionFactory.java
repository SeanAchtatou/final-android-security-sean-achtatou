package org.anddev.andengine.opengl.texture.atlas.buildable;

import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.texture.region.TiledTextureRegion;
import org.anddev.andengine.opengl.texture.source.ITextureAtlasSource;
import org.anddev.andengine.util.Callback;

public class BuildableTextureAtlasTextureRegionFactory {
    public static TextureRegion createFromSource(BuildableTextureAtlas buildableTextureAtlas, ITextureAtlasSource iTextureAtlasSource, boolean z) {
        final TextureRegion textureRegion = new TextureRegion(buildableTextureAtlas, 0, 0, iTextureAtlasSource.getWidth(), iTextureAtlasSource.getHeight());
        buildableTextureAtlas.addTextureAtlasSource(iTextureAtlasSource, new Callback() {
            public void onCallback(ITextureAtlasSource iTextureAtlasSource) {
                TextureRegion.this.setTexturePosition(iTextureAtlasSource.getTexturePositionX(), iTextureAtlasSource.getTexturePositionY());
            }
        });
        textureRegion.setTextureRegionBufferManaged(z);
        return textureRegion;
    }

    public static TiledTextureRegion createTiledFromSource(BuildableTextureAtlas buildableTextureAtlas, ITextureAtlasSource iTextureAtlasSource, int i, int i2, boolean z) {
        final TiledTextureRegion tiledTextureRegion = new TiledTextureRegion(buildableTextureAtlas, 0, 0, iTextureAtlasSource.getWidth(), iTextureAtlasSource.getHeight(), i, i2);
        buildableTextureAtlas.addTextureAtlasSource(iTextureAtlasSource, new Callback() {
            public void onCallback(ITextureAtlasSource iTextureAtlasSource) {
                TiledTextureRegion.this.setTexturePosition(iTextureAtlasSource.getTexturePositionX(), iTextureAtlasSource.getTexturePositionY());
            }
        });
        tiledTextureRegion.setTextureRegionBufferManaged(z);
        return tiledTextureRegion;
    }
}
