package org.anddev.andengine.opengl.texture.atlas.bitmap;

import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource;
import org.anddev.andengine.opengl.texture.bitmap.BitmapTexture;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.util.MathUtils;

public class BitmapTextureAtlasFactory {
    public static BitmapTextureAtlas createForTextureAtlasSourceSize(BitmapTexture.BitmapTextureFormat bitmapTextureFormat, TextureRegion textureRegion) {
        return createForTextureRegionSize(bitmapTextureFormat, textureRegion, TextureOptions.DEFAULT);
    }

    public static BitmapTextureAtlas createForTextureRegionSize(BitmapTexture.BitmapTextureFormat bitmapTextureFormat, TextureRegion textureRegion, TextureOptions textureOptions) {
        return new BitmapTextureAtlas(MathUtils.nextPowerOfTwo(textureRegion.getWidth()), MathUtils.nextPowerOfTwo(textureRegion.getHeight()), bitmapTextureFormat, textureOptions);
    }

    public static BitmapTextureAtlas createForTextureAtlasSourceSize(BitmapTexture.BitmapTextureFormat bitmapTextureFormat, IBitmapTextureAtlasSource iBitmapTextureAtlasSource) {
        return createForTextureAtlasSourceSize(bitmapTextureFormat, iBitmapTextureAtlasSource, TextureOptions.DEFAULT);
    }

    public static BitmapTextureAtlas createForTextureAtlasSourceSize(BitmapTexture.BitmapTextureFormat bitmapTextureFormat, IBitmapTextureAtlasSource iBitmapTextureAtlasSource, TextureOptions textureOptions) {
        return new BitmapTextureAtlas(MathUtils.nextPowerOfTwo(iBitmapTextureAtlasSource.getWidth()), MathUtils.nextPowerOfTwo(iBitmapTextureAtlasSource.getHeight()), bitmapTextureFormat, textureOptions);
    }
}
