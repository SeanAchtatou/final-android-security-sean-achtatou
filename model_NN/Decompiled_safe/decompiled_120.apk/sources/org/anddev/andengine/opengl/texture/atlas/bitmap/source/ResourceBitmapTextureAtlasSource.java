package org.anddev.andengine.opengl.texture.atlas.bitmap.source;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import org.anddev.andengine.opengl.texture.source.BaseTextureAtlasSource;

public class ResourceBitmapTextureAtlasSource extends BaseTextureAtlasSource implements IBitmapTextureAtlasSource {
    private final Context mContext;
    private final int mDrawableResourceID;
    private final int mHeight;
    private final int mWidth;

    public ResourceBitmapTextureAtlasSource(Context context, int i) {
        this(context, i, 0, 0);
    }

    public ResourceBitmapTextureAtlasSource(Context context, int i, int i2, int i3) {
        super(i2, i3);
        this.mContext = context;
        this.mDrawableResourceID = i;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(context.getResources(), i, options);
        this.mWidth = options.outWidth;
        this.mHeight = options.outHeight;
    }

    protected ResourceBitmapTextureAtlasSource(Context context, int i, int i2, int i3, int i4, int i5) {
        super(i2, i3);
        this.mContext = context;
        this.mDrawableResourceID = i;
        this.mWidth = i4;
        this.mHeight = i5;
    }

    public ResourceBitmapTextureAtlasSource clone() {
        return new ResourceBitmapTextureAtlasSource(this.mContext, this.mDrawableResourceID, this.mTexturePositionX, this.mTexturePositionY, this.mWidth, this.mHeight);
    }

    public int getWidth() {
        return this.mWidth;
    }

    public int getHeight() {
        return this.mHeight;
    }

    public Bitmap onLoadBitmap(Bitmap.Config config) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = config;
        return BitmapFactory.decodeResource(this.mContext.getResources(), this.mDrawableResourceID, options);
    }

    public String toString() {
        return String.valueOf(getClass().getSimpleName()) + "(" + this.mDrawableResourceID + ")";
    }
}
