package org.anddev.andengine.opengl.view;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLDisplay;

public class ComponentSizeChooser extends BaseConfigChooser {
    protected int mAlphaSize;
    protected int mBlueSize;
    protected int mDepthSize;
    protected int mGreenSize;
    protected int mRedSize;
    protected int mStencilSize;
    private final int[] mValue = new int[1];

    public /* bridge */ /* synthetic */ EGLConfig chooseConfig(EGL10 egl10, EGLDisplay eGLDisplay) {
        return super.chooseConfig(egl10, eGLDisplay);
    }

    public ComponentSizeChooser(int i, int i2, int i3, int i4, int i5, int i6) {
        super(new int[]{12324, i, 12323, i2, 12322, i3, 12321, i4, 12325, i5, 12326, i6, 12344});
        this.mRedSize = i;
        this.mGreenSize = i2;
        this.mBlueSize = i3;
        this.mAlphaSize = i4;
        this.mDepthSize = i5;
        this.mStencilSize = i6;
    }

    public EGLConfig chooseConfig(EGL10 egl10, EGLDisplay eGLDisplay, EGLConfig[] eGLConfigArr) {
        EGLConfig eGLConfig;
        int length = eGLConfigArr.length;
        int i = 0;
        int i2 = 1000;
        EGLConfig eGLConfig2 = null;
        while (i < length) {
            EGLConfig eGLConfig3 = eGLConfigArr[i];
            int findConfigAttrib = findConfigAttrib(egl10, eGLDisplay, eGLConfig3, 12324, 0);
            int findConfigAttrib2 = findConfigAttrib(egl10, eGLDisplay, eGLConfig3, 12323, 0);
            int findConfigAttrib3 = findConfigAttrib(egl10, eGLDisplay, eGLConfig3, 12322, 0);
            int findConfigAttrib4 = findConfigAttrib(egl10, eGLDisplay, eGLConfig3, 12321, 0);
            int findConfigAttrib5 = findConfigAttrib(egl10, eGLDisplay, eGLConfig3, 12325, 0);
            int abs = Math.abs(findConfigAttrib(egl10, eGLDisplay, eGLConfig3, 12326, 0) - this.mStencilSize) + Math.abs(findConfigAttrib - this.mRedSize) + Math.abs(findConfigAttrib2 - this.mGreenSize) + Math.abs(findConfigAttrib3 - this.mBlueSize) + Math.abs(findConfigAttrib4 - this.mAlphaSize) + Math.abs(findConfigAttrib5 - this.mDepthSize);
            if (abs < i2) {
                eGLConfig = eGLConfig3;
            } else {
                abs = i2;
                eGLConfig = eGLConfig2;
            }
            i++;
            i2 = abs;
            eGLConfig2 = eGLConfig;
        }
        return eGLConfig2;
    }

    private int findConfigAttrib(EGL10 egl10, EGLDisplay eGLDisplay, EGLConfig eGLConfig, int i, int i2) {
        if (egl10.eglGetConfigAttrib(eGLDisplay, eGLConfig, i, this.mValue)) {
            return this.mValue[0];
        }
        return i2;
    }
}
