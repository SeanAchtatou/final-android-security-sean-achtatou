package org.anddev.andengine.opengl.font;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.opengl.GLUtils;
import android.util.FloatMath;
import android.util.SparseArray;
import java.util.ArrayList;
import javax.microedition.khronos.opengles.GL10;
import org.anddev.andengine.opengl.texture.ITexture;

public class Font {
    protected static final int LETTER_EXTRA_WIDTH = 10;
    protected static final float LETTER_LEFT_OFFSET = 0.0f;
    protected static final int PADDING = 1;
    private final Paint mBackgroundPaint;
    protected final Canvas mCanvas = new Canvas();
    private final Size mCreateLetterTemporarySize = new Size();
    private int mCurrentTextureX = 0;
    private int mCurrentTextureY = 0;
    protected final Paint.FontMetrics mFontMetrics;
    private final Rect mGetLetterBitmapTemporaryRect = new Rect();
    private final Rect mGetLetterBoundsTemporaryRect = new Rect();
    private final Rect mGetStringWidthTemporaryRect = new Rect();
    private final ArrayList mLettersPendingToBeDrawnToTexture = new ArrayList();
    private final int mLineGap;
    private final int mLineHeight;
    private final SparseArray mManagedCharacterToLetterMap = new SparseArray();
    protected final Paint mPaint;
    private final float[] mTemporaryTextWidthFetchers = new float[1];
    private final ITexture mTexture;
    private final float mTextureHeight;
    private final float mTextureWidth;

    public Font(ITexture iTexture, Typeface typeface, float f, boolean z, int i) {
        this.mTexture = iTexture;
        this.mTextureWidth = (float) iTexture.getWidth();
        this.mTextureHeight = (float) iTexture.getHeight();
        this.mPaint = new Paint();
        this.mPaint.setTypeface(typeface);
        this.mPaint.setColor(i);
        this.mPaint.setTextSize(f);
        this.mPaint.setAntiAlias(z);
        this.mBackgroundPaint = new Paint();
        this.mBackgroundPaint.setColor(0);
        this.mBackgroundPaint.setStyle(Paint.Style.FILL);
        this.mFontMetrics = this.mPaint.getFontMetrics();
        this.mLineHeight = ((int) FloatMath.ceil(Math.abs(this.mFontMetrics.ascent) + Math.abs(this.mFontMetrics.descent))) + 2;
        this.mLineGap = (int) FloatMath.ceil(this.mFontMetrics.leading);
    }

    public int getLineGap() {
        return this.mLineGap;
    }

    public int getLineHeight() {
        return this.mLineHeight;
    }

    public ITexture getTexture() {
        return this.mTexture;
    }

    public synchronized void reload() {
        ArrayList arrayList = this.mLettersPendingToBeDrawnToTexture;
        SparseArray sparseArray = this.mManagedCharacterToLetterMap;
        for (int size = sparseArray.size() - 1; size >= 0; size--) {
            arrayList.add((Letter) sparseArray.valueAt(size));
        }
    }

    private int getLetterAdvance(char c) {
        this.mPaint.getTextWidths(String.valueOf(c), this.mTemporaryTextWidthFetchers);
        return (int) FloatMath.ceil(this.mTemporaryTextWidthFetchers[0]);
    }

    private Bitmap getLetterBitmap(char c) {
        Rect rect = this.mGetLetterBitmapTemporaryRect;
        String valueOf = String.valueOf(c);
        this.mPaint.getTextBounds(valueOf, 0, 1, rect);
        rect.right += 2;
        Bitmap createBitmap = Bitmap.createBitmap(rect.width() == 0 ? 3 : rect.width() + LETTER_EXTRA_WIDTH, getLineHeight(), Bitmap.Config.ARGB_8888);
        this.mCanvas.setBitmap(createBitmap);
        this.mCanvas.drawRect(LETTER_LEFT_OFFSET, LETTER_LEFT_OFFSET, (float) createBitmap.getWidth(), (float) createBitmap.getHeight(), this.mBackgroundPaint);
        drawCharacterString(valueOf);
        return createBitmap;
    }

    /* access modifiers changed from: protected */
    public void drawCharacterString(String str) {
        this.mCanvas.drawText(str, 1.0f, (-this.mFontMetrics.ascent) + 1.0f, this.mPaint);
    }

    public int getStringWidth(String str) {
        this.mPaint.getTextBounds(str, 0, str.length(), this.mGetStringWidthTemporaryRect);
        return this.mGetStringWidthTemporaryRect.width();
    }

    private void getLetterBounds(char c, Size size) {
        this.mPaint.getTextBounds(String.valueOf(c), 0, 1, this.mGetLetterBoundsTemporaryRect);
        size.set(this.mGetLetterBoundsTemporaryRect.width() + LETTER_EXTRA_WIDTH + 2, getLineHeight());
    }

    public void prepareLetters(char... cArr) {
        for (char letter : cArr) {
            getLetter(letter);
        }
    }

    public synchronized Letter getLetter(char c) {
        Letter letter;
        SparseArray sparseArray = this.mManagedCharacterToLetterMap;
        letter = (Letter) sparseArray.get(c);
        if (letter == null) {
            letter = createLetter(c);
            this.mLettersPendingToBeDrawnToTexture.add(letter);
            sparseArray.put(c, letter);
        }
        return letter;
    }

    private Letter createLetter(char c) {
        float f = this.mTextureWidth;
        float f2 = this.mTextureHeight;
        Size size = this.mCreateLetterTemporarySize;
        getLetterBounds(c, size);
        float width = size.getWidth();
        float height = size.getHeight();
        if (((float) this.mCurrentTextureX) + width >= f) {
            this.mCurrentTextureX = 0;
            this.mCurrentTextureY += getLineGap() + getLineHeight();
        }
        float f3 = height / f2;
        Letter letter = new Letter(c, getLetterAdvance(c), (int) width, (int) height, ((float) this.mCurrentTextureX) / f, ((float) this.mCurrentTextureY) / f2, width / f, f3);
        this.mCurrentTextureX = (int) (((float) this.mCurrentTextureX) + width);
        return letter;
    }

    public synchronized void update(GL10 gl10) {
        ArrayList arrayList = this.mLettersPendingToBeDrawnToTexture;
        if (arrayList.size() > 0) {
            this.mTexture.bind(gl10);
            float f = this.mTextureWidth;
            float f2 = this.mTextureHeight;
            for (int size = arrayList.size() - 1; size >= 0; size--) {
                Letter letter = (Letter) arrayList.get(size);
                Bitmap letterBitmap = getLetterBitmap(letter.mCharacter);
                GLUtils.texSubImage2D(3553, 0, (int) (letter.mTextureX * f), (int) (letter.mTextureY * f2), letterBitmap);
                letterBitmap.recycle();
            }
            arrayList.clear();
            System.gc();
        }
    }
}
