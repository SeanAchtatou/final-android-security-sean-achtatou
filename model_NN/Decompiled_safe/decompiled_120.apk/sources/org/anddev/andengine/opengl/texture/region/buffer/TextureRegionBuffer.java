package org.anddev.andengine.opengl.texture.region.buffer;

import org.anddev.andengine.opengl.buffer.BufferObject;
import org.anddev.andengine.opengl.texture.region.BaseTextureRegion;
import org.anddev.andengine.opengl.util.FastFloatBuffer;

public class TextureRegionBuffer extends BufferObject {
    private boolean mFlippedHorizontal;
    private boolean mFlippedVertical;
    protected final BaseTextureRegion mTextureRegion;

    public TextureRegionBuffer(BaseTextureRegion baseTextureRegion, int i, boolean z) {
        super(8, i, z);
        this.mTextureRegion = baseTextureRegion;
    }

    public BaseTextureRegion getTextureRegion() {
        return this.mTextureRegion;
    }

    public boolean isFlippedHorizontal() {
        return this.mFlippedHorizontal;
    }

    public void setFlippedHorizontal(boolean z) {
        if (this.mFlippedHorizontal != z) {
            this.mFlippedHorizontal = z;
            update();
        }
    }

    public boolean isFlippedVertical() {
        return this.mFlippedVertical;
    }

    public void setFlippedVertical(boolean z) {
        if (this.mFlippedVertical != z) {
            this.mFlippedVertical = z;
            update();
        }
    }

    public synchronized void update() {
        BaseTextureRegion baseTextureRegion = this.mTextureRegion;
        if (baseTextureRegion.getTexture() != null) {
            int floatToRawIntBits = Float.floatToRawIntBits(baseTextureRegion.getTextureCoordinateX1());
            int floatToRawIntBits2 = Float.floatToRawIntBits(baseTextureRegion.getTextureCoordinateY1());
            int floatToRawIntBits3 = Float.floatToRawIntBits(baseTextureRegion.getTextureCoordinateX2());
            int floatToRawIntBits4 = Float.floatToRawIntBits(baseTextureRegion.getTextureCoordinateY2());
            int[] iArr = this.mBufferData;
            if (this.mFlippedVertical) {
                if (this.mFlippedHorizontal) {
                    iArr[0] = floatToRawIntBits3;
                    iArr[1] = floatToRawIntBits4;
                    iArr[2] = floatToRawIntBits3;
                    iArr[3] = floatToRawIntBits2;
                    iArr[4] = floatToRawIntBits;
                    iArr[5] = floatToRawIntBits4;
                    iArr[6] = floatToRawIntBits;
                    iArr[7] = floatToRawIntBits2;
                } else {
                    iArr[0] = floatToRawIntBits;
                    iArr[1] = floatToRawIntBits4;
                    iArr[2] = floatToRawIntBits;
                    iArr[3] = floatToRawIntBits2;
                    iArr[4] = floatToRawIntBits3;
                    iArr[5] = floatToRawIntBits4;
                    iArr[6] = floatToRawIntBits3;
                    iArr[7] = floatToRawIntBits2;
                }
            } else if (this.mFlippedHorizontal) {
                iArr[0] = floatToRawIntBits3;
                iArr[1] = floatToRawIntBits2;
                iArr[2] = floatToRawIntBits3;
                iArr[3] = floatToRawIntBits4;
                iArr[4] = floatToRawIntBits;
                iArr[5] = floatToRawIntBits2;
                iArr[6] = floatToRawIntBits;
                iArr[7] = floatToRawIntBits4;
            } else {
                iArr[0] = floatToRawIntBits;
                iArr[1] = floatToRawIntBits2;
                iArr[2] = floatToRawIntBits;
                iArr[3] = floatToRawIntBits4;
                iArr[4] = floatToRawIntBits3;
                iArr[5] = floatToRawIntBits2;
                iArr[6] = floatToRawIntBits3;
                iArr[7] = floatToRawIntBits4;
            }
            FastFloatBuffer fastFloatBuffer = this.mFloatBuffer;
            fastFloatBuffer.position(0);
            fastFloatBuffer.put(iArr);
            fastFloatBuffer.position(0);
            super.setHardwareBufferNeedsUpdate();
        }
    }
}
