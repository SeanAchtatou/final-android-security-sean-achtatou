package org.anddev.andengine.opengl.texture.atlas.buildable.builder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import org.anddev.andengine.entity.layer.tiled.tmx.util.constants.TMXConstants;
import org.anddev.andengine.opengl.texture.atlas.ITextureAtlas;
import org.anddev.andengine.opengl.texture.atlas.buildable.BuildableTextureAtlas;
import org.anddev.andengine.opengl.texture.atlas.buildable.builder.ITextureBuilder;
import org.anddev.andengine.opengl.texture.source.ITextureAtlasSource;

public class BlackPawnTextureBuilder implements ITextureBuilder {
    private static final Comparator TEXTURESOURCE_COMPARATOR = new Comparator() {
        public int compare(BuildableTextureAtlas.TextureAtlasSourceWithWithLocationCallback textureAtlasSourceWithWithLocationCallback, BuildableTextureAtlas.TextureAtlasSourceWithWithLocationCallback textureAtlasSourceWithWithLocationCallback2) {
            int width = textureAtlasSourceWithWithLocationCallback2.getTextureAtlasSource().getWidth() - textureAtlasSourceWithWithLocationCallback.getTextureAtlasSource().getWidth();
            return width != 0 ? width : textureAtlasSourceWithWithLocationCallback2.getTextureAtlasSource().getHeight() - textureAtlasSourceWithWithLocationCallback.getTextureAtlasSource().getHeight();
        }
    };
    private final int mTextureAtlasSourceSpacing;

    public BlackPawnTextureBuilder(int i) {
        this.mTextureAtlasSourceSpacing = i;
    }

    public void pack(ITextureAtlas iTextureAtlas, ArrayList arrayList) {
        Collections.sort(arrayList, TEXTURESOURCE_COMPARATOR);
        Node node = new Node(new Rect(0, 0, iTextureAtlas.getWidth(), iTextureAtlas.getHeight()));
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            BuildableTextureAtlas.TextureAtlasSourceWithWithLocationCallback textureAtlasSourceWithWithLocationCallback = (BuildableTextureAtlas.TextureAtlasSourceWithWithLocationCallback) arrayList.get(i);
            ITextureAtlasSource textureAtlasSource = textureAtlasSourceWithWithLocationCallback.getTextureAtlasSource();
            Node insert = node.insert(textureAtlasSource, iTextureAtlas.getWidth(), iTextureAtlas.getHeight(), this.mTextureAtlasSourceSpacing);
            if (insert == null) {
                throw new ITextureBuilder.TextureAtlasSourcePackingException("Could not pack: " + textureAtlasSource.toString());
            }
            iTextureAtlas.addTextureAtlasSource(textureAtlasSource, insert.mRect.mLeft, insert.mRect.mTop);
            textureAtlasSourceWithWithLocationCallback.getCallback().onCallback(textureAtlasSource);
        }
    }

    public class Rect {
        private final int mHeight;
        /* access modifiers changed from: private */
        public final int mLeft;
        /* access modifiers changed from: private */
        public final int mTop;
        private final int mWidth;

        public Rect(int i, int i2, int i3, int i4) {
            this.mLeft = i;
            this.mTop = i2;
            this.mWidth = i3;
            this.mHeight = i4;
        }

        public int getWidth() {
            return this.mWidth;
        }

        public int getHeight() {
            return this.mHeight;
        }

        public int getLeft() {
            return this.mLeft;
        }

        public int getTop() {
            return this.mTop;
        }

        public int getRight() {
            return this.mLeft + this.mWidth;
        }

        public int getBottom() {
            return this.mTop + this.mHeight;
        }

        public String toString() {
            return "@: " + this.mLeft + "/" + this.mTop + " * " + this.mWidth + TMXConstants.TAG_OBJECT_ATTRIBUTE_X + this.mHeight;
        }
    }

    public class Node {
        private Node mChildA;
        private Node mChildB;
        /* access modifiers changed from: private */
        public final Rect mRect;
        private ITextureAtlasSource mTextureAtlasSource;

        public Node(int i, int i2, int i3, int i4) {
            this(new Rect(i, i2, i3, i4));
        }

        public Node(Rect rect) {
            this.mRect = rect;
        }

        public Rect getRect() {
            return this.mRect;
        }

        public Node getChildA() {
            return this.mChildA;
        }

        public Node getChildB() {
            return this.mChildB;
        }

        public Node insert(ITextureAtlasSource iTextureAtlasSource, int i, int i2, int i3) {
            boolean z;
            if (this.mChildA != null && this.mChildB != null) {
                Node insert = this.mChildA.insert(iTextureAtlasSource, i, i2, i3);
                if (insert != null) {
                    return insert;
                }
                return this.mChildB.insert(iTextureAtlasSource, i, i2, i3);
            } else if (this.mTextureAtlasSource != null) {
                return null;
            } else {
                int width = iTextureAtlasSource.getWidth();
                int height = iTextureAtlasSource.getHeight();
                int width2 = this.mRect.getWidth();
                int height2 = this.mRect.getHeight();
                if (width > width2 || height > height2) {
                    return null;
                }
                int i4 = width + i3;
                int i5 = height + i3;
                int left = this.mRect.getLeft();
                boolean z2 = height == height2 && this.mRect.getTop() + height == i2;
                if (width == width2 && left + width == i) {
                    z = true;
                } else {
                    z = false;
                }
                if (i4 == width2) {
                    if (i5 == height2) {
                        this.mTextureAtlasSource = iTextureAtlasSource;
                        return this;
                    } else if (z2) {
                        this.mTextureAtlasSource = iTextureAtlasSource;
                        return this;
                    }
                }
                if (z) {
                    if (i5 == height2) {
                        this.mTextureAtlasSource = iTextureAtlasSource;
                        return this;
                    } else if (z2) {
                        this.mTextureAtlasSource = iTextureAtlasSource;
                        return this;
                    } else if (i5 > height2) {
                        return null;
                    } else {
                        return createChildren(iTextureAtlasSource, i, i2, i3, width2 - width, height2 - i5);
                    }
                } else if (z2) {
                    if (i4 == width2) {
                        this.mTextureAtlasSource = iTextureAtlasSource;
                        return this;
                    } else if (i4 > width2) {
                        return null;
                    } else {
                        return createChildren(iTextureAtlasSource, i, i2, i3, width2 - i4, height2 - height);
                    }
                } else if (i4 > width2 || i5 > height2) {
                    return null;
                } else {
                    return createChildren(iTextureAtlasSource, i, i2, i3, width2 - i4, height2 - i5);
                }
            }
        }

        private Node createChildren(ITextureAtlasSource iTextureAtlasSource, int i, int i2, int i3, int i4, int i5) {
            Rect rect = this.mRect;
            if (i4 >= i5) {
                this.mChildA = new Node(rect.getLeft(), rect.getTop(), iTextureAtlasSource.getWidth() + i3, rect.getHeight());
                this.mChildB = new Node(rect.getLeft() + iTextureAtlasSource.getWidth() + i3, rect.getTop(), rect.getWidth() - (iTextureAtlasSource.getWidth() + i3), rect.getHeight());
            } else {
                this.mChildA = new Node(rect.getLeft(), rect.getTop(), rect.getWidth(), iTextureAtlasSource.getHeight() + i3);
                this.mChildB = new Node(rect.getLeft(), rect.getTop() + iTextureAtlasSource.getHeight() + i3, rect.getWidth(), rect.getHeight() - (iTextureAtlasSource.getHeight() + i3));
            }
            return this.mChildA.insert(iTextureAtlasSource, i, i2, i3);
        }
    }
}
