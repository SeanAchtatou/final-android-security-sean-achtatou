package org.anddev.andengine.opengl.font;

import android.content.Context;
import android.graphics.Typeface;
import org.anddev.andengine.opengl.texture.ITexture;

public class FontFactory {
    private static String sAssetBasePath = "";

    public static void setAssetBasePath(String str) {
        if (str.endsWith("/") || str.length() == 0) {
            sAssetBasePath = str;
            return;
        }
        throw new IllegalStateException("pAssetBasePath must end with '/' or be lenght zero.");
    }

    public static void reset() {
        setAssetBasePath("");
    }

    public static Font create(ITexture iTexture, Typeface typeface, float f, boolean z, int i) {
        return new Font(iTexture, typeface, f, z, i);
    }

    public static StrokeFont createStroke(ITexture iTexture, Typeface typeface, float f, boolean z, int i, float f2, int i2) {
        return new StrokeFont(iTexture, typeface, f, z, i, f2, i2);
    }

    public static StrokeFont createStroke(ITexture iTexture, Typeface typeface, float f, boolean z, int i, float f2, int i2, boolean z2) {
        return new StrokeFont(iTexture, typeface, f, z, i, f2, i2, z2);
    }

    public static Font createFromAsset(ITexture iTexture, Context context, String str, float f, boolean z, int i) {
        return new Font(iTexture, Typeface.createFromAsset(context.getAssets(), String.valueOf(sAssetBasePath) + str), f, z, i);
    }

    public static StrokeFont createStrokeFromAsset(ITexture iTexture, Context context, String str, float f, boolean z, int i, float f2, int i2) {
        return new StrokeFont(iTexture, Typeface.createFromAsset(context.getAssets(), String.valueOf(sAssetBasePath) + str), f, z, i, f2, i2);
    }

    public static StrokeFont createStrokeFromAsset(ITexture iTexture, Context context, String str, float f, boolean z, int i, float f2, int i2, boolean z2) {
        return new StrokeFont(iTexture, Typeface.createFromAsset(context.getAssets(), String.valueOf(sAssetBasePath) + str), f, z, i, f2, i2, z2);
    }
}
