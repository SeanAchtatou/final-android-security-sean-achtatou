package org.anddev.andengine.opengl.texture.region;

import org.anddev.andengine.opengl.texture.ITexture;

public class TiledTextureRegion extends BaseTextureRegion {
    private int mCurrentTileColumn = 0;
    private int mCurrentTileRow = 0;
    private final int mTileColumns;
    private final int mTileCount = (this.mTileColumns * this.mTileRows);
    private final int mTileRows;

    public TiledTextureRegion(ITexture iTexture, int i, int i2, int i3, int i4, int i5, int i6) {
        super(iTexture, i, i2, i3, i4);
        this.mTileColumns = i5;
        this.mTileRows = i6;
        initTextureBuffer();
    }

    /* access modifiers changed from: protected */
    public void initTextureBuffer() {
        if (this.mTileRows != 0 && this.mTileColumns != 0) {
            super.initTextureBuffer();
        }
    }

    public int getTileCount() {
        return this.mTileCount;
    }

    public int getTileWidth() {
        return super.getWidth() / this.mTileColumns;
    }

    public int getTileHeight() {
        return super.getHeight() / this.mTileRows;
    }

    public int getCurrentTileColumn() {
        return this.mCurrentTileColumn;
    }

    public int getCurrentTileRow() {
        return this.mCurrentTileRow;
    }

    public int getCurrentTileIndex() {
        return (this.mCurrentTileRow * this.mTileColumns) + this.mCurrentTileColumn;
    }

    public void setCurrentTileIndex(int i, int i2) {
        if (i != this.mCurrentTileColumn || i2 != this.mCurrentTileRow) {
            this.mCurrentTileColumn = i;
            this.mCurrentTileRow = i2;
            super.updateTextureRegionBuffer();
        }
    }

    public void setCurrentTileIndex(int i) {
        if (i < this.mTileCount) {
            int i2 = this.mTileColumns;
            setCurrentTileIndex(i % i2, i / i2);
        }
    }

    public int getTexturePositionOfCurrentTileX() {
        return super.getTexturePositionX() + (this.mCurrentTileColumn * getTileWidth());
    }

    public int getTexturePositionOfCurrentTileY() {
        return super.getTexturePositionY() + (this.mCurrentTileRow * getTileHeight());
    }

    public TiledTextureRegion clone() {
        TiledTextureRegion tiledTextureRegion = new TiledTextureRegion(this.mTexture, getTexturePositionX(), getTexturePositionY(), getWidth(), getHeight(), this.mTileColumns, this.mTileRows);
        tiledTextureRegion.setCurrentTileIndex(this.mCurrentTileColumn, this.mCurrentTileRow);
        return tiledTextureRegion;
    }

    public float getTextureCoordinateX1() {
        return ((float) getTexturePositionOfCurrentTileX()) / ((float) this.mTexture.getWidth());
    }

    public float getTextureCoordinateY1() {
        return ((float) getTexturePositionOfCurrentTileY()) / ((float) this.mTexture.getHeight());
    }

    public float getTextureCoordinateX2() {
        return ((float) (getTexturePositionOfCurrentTileX() + getTileWidth())) / ((float) this.mTexture.getWidth());
    }

    public float getTextureCoordinateY2() {
        return ((float) (getTexturePositionOfCurrentTileY() + getTileHeight())) / ((float) this.mTexture.getHeight());
    }

    public int getTextureCropLeft() {
        return getTexturePositionOfCurrentTileX();
    }

    public int getTextureCropTop() {
        return getTexturePositionOfCurrentTileY();
    }

    public int getTextureCropWidth() {
        return getTileWidth();
    }

    public int getTextureCropHeight() {
        return getTileHeight();
    }

    public void nextTile() {
        setCurrentTileIndex((getCurrentTileIndex() + 1) % getTileCount());
    }
}
