package org.anddev.andengine.opengl.texture.region;

import org.anddev.andengine.util.Library;

public class TextureRegionLibrary extends Library {
    public TextureRegionLibrary(int i) {
        super(i);
    }

    public TextureRegion get(int i) {
        return (TextureRegion) super.get(i);
    }

    public TiledTextureRegion getTiled(int i) {
        return (TiledTextureRegion) this.mItems.get(i);
    }
}
