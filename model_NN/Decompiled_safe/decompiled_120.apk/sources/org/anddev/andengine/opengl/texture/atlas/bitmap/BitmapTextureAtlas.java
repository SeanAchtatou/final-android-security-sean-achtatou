package org.anddev.andengine.opengl.texture.atlas.bitmap;

import android.graphics.Bitmap;
import android.opengl.GLUtils;
import java.util.ArrayList;
import javax.microedition.khronos.opengles.GL10;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.atlas.ITextureAtlas;
import org.anddev.andengine.opengl.texture.atlas.TextureAtlas;
import org.anddev.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource;
import org.anddev.andengine.opengl.texture.bitmap.BitmapTexture;
import org.anddev.andengine.opengl.util.GLHelper;
import org.anddev.andengine.util.Debug;

public class BitmapTextureAtlas extends TextureAtlas {
    private final BitmapTexture.BitmapTextureFormat mBitmapTextureFormat;

    public BitmapTextureAtlas(int i, int i2) {
        this(i, i2, BitmapTexture.BitmapTextureFormat.RGBA_8888);
    }

    public BitmapTextureAtlas(int i, int i2, BitmapTexture.BitmapTextureFormat bitmapTextureFormat) {
        this(i, i2, bitmapTextureFormat, TextureOptions.DEFAULT, null);
    }

    public BitmapTextureAtlas(int i, int i2, ITextureAtlas.ITextureAtlasStateListener iTextureAtlasStateListener) {
        this(i, i2, BitmapTexture.BitmapTextureFormat.RGBA_8888, TextureOptions.DEFAULT, iTextureAtlasStateListener);
    }

    public BitmapTextureAtlas(int i, int i2, BitmapTexture.BitmapTextureFormat bitmapTextureFormat, ITextureAtlas.ITextureAtlasStateListener iTextureAtlasStateListener) {
        this(i, i2, bitmapTextureFormat, TextureOptions.DEFAULT, iTextureAtlasStateListener);
    }

    public BitmapTextureAtlas(int i, int i2, TextureOptions textureOptions) {
        this(i, i2, BitmapTexture.BitmapTextureFormat.RGBA_8888, textureOptions, null);
    }

    public BitmapTextureAtlas(int i, int i2, BitmapTexture.BitmapTextureFormat bitmapTextureFormat, TextureOptions textureOptions) {
        this(i, i2, bitmapTextureFormat, textureOptions, null);
    }

    public BitmapTextureAtlas(int i, int i2, TextureOptions textureOptions, ITextureAtlas.ITextureAtlasStateListener iTextureAtlasStateListener) {
        this(i, i2, BitmapTexture.BitmapTextureFormat.RGBA_8888, textureOptions, iTextureAtlasStateListener);
    }

    public BitmapTextureAtlas(int i, int i2, BitmapTexture.BitmapTextureFormat bitmapTextureFormat, TextureOptions textureOptions, ITextureAtlas.ITextureAtlasStateListener iTextureAtlasStateListener) {
        super(i, i2, bitmapTextureFormat.getPixelFormat(), textureOptions, iTextureAtlasStateListener);
        this.mBitmapTextureFormat = bitmapTextureFormat;
    }

    public BitmapTexture.BitmapTextureFormat getBitmapTextureFormat() {
        return this.mBitmapTextureFormat;
    }

    /* access modifiers changed from: protected */
    public void writeTextureToHardware(GL10 gl10) {
        Bitmap.Config bitmapConfig = this.mBitmapTextureFormat.getBitmapConfig();
        int gLFormat = this.mPixelFormat.getGLFormat();
        int gLType = this.mPixelFormat.getGLType();
        boolean z = this.mTextureOptions.mPreMultipyAlpha;
        ArrayList arrayList = this.mTextureAtlasSources;
        int size = arrayList.size();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < size) {
                IBitmapTextureAtlasSource iBitmapTextureAtlasSource = (IBitmapTextureAtlasSource) arrayList.get(i2);
                if (iBitmapTextureAtlasSource != null) {
                    Bitmap onLoadBitmap = iBitmapTextureAtlasSource.onLoadBitmap(bitmapConfig);
                    if (onLoadBitmap == null) {
                        try {
                            throw new IllegalArgumentException(String.valueOf(iBitmapTextureAtlasSource.getClass().getSimpleName()) + ": " + iBitmapTextureAtlasSource.toString() + " returned a null Bitmap.");
                        } catch (IllegalArgumentException e) {
                            Debug.e("Error loading: " + iBitmapTextureAtlasSource.toString(), e);
                            if (getTextureStateListener() != null) {
                                getTextureStateListener().onTextureAtlasSourceLoadExeption(this, iBitmapTextureAtlasSource, e);
                            } else {
                                throw e;
                            }
                        }
                    } else {
                        if (z) {
                            GLUtils.texSubImage2D(3553, 0, iBitmapTextureAtlasSource.getTexturePositionX(), iBitmapTextureAtlasSource.getTexturePositionY(), onLoadBitmap, gLFormat, gLType);
                        } else {
                            GLHelper.glTexSubImage2D(gl10, 3553, 0, iBitmapTextureAtlasSource.getTexturePositionX(), iBitmapTextureAtlasSource.getTexturePositionY(), onLoadBitmap, this.mPixelFormat);
                        }
                        onLoadBitmap.recycle();
                    }
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void bindTextureOnHardware(GL10 gl10) {
        super.bindTextureOnHardware(gl10);
        sendPlaceholderBitmapToHardware();
    }

    private void sendPlaceholderBitmapToHardware() {
        Bitmap createBitmap = Bitmap.createBitmap(this.mWidth, this.mHeight, this.mBitmapTextureFormat.getBitmapConfig());
        GLUtils.texImage2D(3553, 0, createBitmap, 0);
        createBitmap.recycle();
    }
}
