package org.anddev.andengine.opengl.texture.atlas.bitmap.source.decorator.shape;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import org.anddev.andengine.opengl.texture.atlas.bitmap.source.decorator.BaseBitmapTextureAtlasSourceDecorator;

public class RoundedRectangleBitmapTextureAtlasSourceDecoratorShape implements IBitmapTextureAtlasSourceDecoratorShape {
    private static final float CORNER_RADIUS_DEFAULT = 1.0f;
    private static RoundedRectangleBitmapTextureAtlasSourceDecoratorShape sDefaultInstance;
    private final float mCornerRadiusX;
    private final float mCornerRadiusY;
    private final RectF mRectF;

    public RoundedRectangleBitmapTextureAtlasSourceDecoratorShape() {
        this(1.0f, 1.0f);
    }

    public RoundedRectangleBitmapTextureAtlasSourceDecoratorShape(float f, float f2) {
        this.mRectF = new RectF();
        this.mCornerRadiusX = f;
        this.mCornerRadiusY = f2;
    }

    public static RoundedRectangleBitmapTextureAtlasSourceDecoratorShape getDefaultInstance() {
        if (sDefaultInstance == null) {
            sDefaultInstance = new RoundedRectangleBitmapTextureAtlasSourceDecoratorShape();
        }
        return sDefaultInstance;
    }

    public void onDecorateBitmap(Canvas canvas, Paint paint, BaseBitmapTextureAtlasSourceDecorator.TextureAtlasSourceDecoratorOptions textureAtlasSourceDecoratorOptions) {
        this.mRectF.set(textureAtlasSourceDecoratorOptions.getInsetLeft(), textureAtlasSourceDecoratorOptions.getInsetTop(), ((float) canvas.getWidth()) - textureAtlasSourceDecoratorOptions.getInsetRight(), ((float) canvas.getHeight()) - textureAtlasSourceDecoratorOptions.getInsetBottom());
        canvas.drawRoundRect(this.mRectF, this.mCornerRadiusX, this.mCornerRadiusY, paint);
    }
}
