package org.anddev.andengine.opengl.buffer;

import java.util.ArrayList;
import java.util.HashSet;
import javax.microedition.khronos.opengles.GL11;

public class BufferObjectManager {
    private static BufferObjectManager mActiveInstance = null;
    private static final ArrayList mBufferObjectsLoaded = new ArrayList();
    private static final HashSet mBufferObjectsManaged = new HashSet();
    private static final ArrayList mBufferObjectsToBeLoaded = new ArrayList();
    private static final ArrayList mBufferObjectsToBeUnloaded = new ArrayList();

    public static BufferObjectManager getActiveInstance() {
        return mActiveInstance;
    }

    public static void setActiveInstance(BufferObjectManager bufferObjectManager) {
        mActiveInstance = bufferObjectManager;
    }

    public synchronized void clear() {
        mBufferObjectsToBeLoaded.clear();
        mBufferObjectsLoaded.clear();
        mBufferObjectsManaged.clear();
    }

    public synchronized void loadBufferObject(BufferObject bufferObject) {
        if (bufferObject != null) {
            if (mBufferObjectsManaged.contains(bufferObject)) {
                mBufferObjectsToBeUnloaded.remove(bufferObject);
            } else {
                mBufferObjectsManaged.add(bufferObject);
                mBufferObjectsToBeLoaded.add(bufferObject);
            }
        }
    }

    public synchronized void unloadBufferObject(BufferObject bufferObject) {
        if (bufferObject != null) {
            if (mBufferObjectsManaged.contains(bufferObject)) {
                if (mBufferObjectsLoaded.contains(bufferObject)) {
                    mBufferObjectsToBeUnloaded.add(bufferObject);
                } else if (mBufferObjectsToBeLoaded.remove(bufferObject)) {
                    mBufferObjectsManaged.remove(bufferObject);
                }
            }
        }
    }

    public void loadBufferObjects(BufferObject... bufferObjectArr) {
        for (int length = bufferObjectArr.length - 1; length >= 0; length--) {
            loadBufferObject(bufferObjectArr[length]);
        }
    }

    public void unloadBufferObjects(BufferObject... bufferObjectArr) {
        for (int length = bufferObjectArr.length - 1; length >= 0; length--) {
            unloadBufferObject(bufferObjectArr[length]);
        }
    }

    public synchronized void reloadBufferObjects() {
        ArrayList arrayList = mBufferObjectsLoaded;
        for (int size = arrayList.size() - 1; size >= 0; size--) {
            ((BufferObject) arrayList.get(size)).setLoadedToHardware(false);
        }
        mBufferObjectsToBeLoaded.addAll(arrayList);
        arrayList.clear();
    }

    public synchronized void updateBufferObjects(GL11 gl11) {
        HashSet hashSet = mBufferObjectsManaged;
        ArrayList arrayList = mBufferObjectsLoaded;
        ArrayList arrayList2 = mBufferObjectsToBeLoaded;
        ArrayList arrayList3 = mBufferObjectsToBeUnloaded;
        int size = arrayList2.size();
        if (size > 0) {
            for (int i = size - 1; i >= 0; i--) {
                BufferObject bufferObject = (BufferObject) arrayList2.get(i);
                if (!bufferObject.isLoadedToHardware()) {
                    bufferObject.loadToHardware(gl11);
                    bufferObject.setHardwareBufferNeedsUpdate();
                }
                arrayList.add(bufferObject);
            }
            arrayList2.clear();
        }
        int size2 = arrayList3.size();
        if (size2 > 0) {
            for (int i2 = size2 - 1; i2 >= 0; i2--) {
                BufferObject bufferObject2 = (BufferObject) arrayList3.remove(i2);
                if (bufferObject2.isLoadedToHardware()) {
                    bufferObject2.unloadFromHardware(gl11);
                }
                arrayList.remove(bufferObject2);
                hashSet.remove(bufferObject2);
            }
        }
    }
}
