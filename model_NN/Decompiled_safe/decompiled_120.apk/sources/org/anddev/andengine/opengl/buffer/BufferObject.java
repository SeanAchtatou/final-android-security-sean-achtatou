package org.anddev.andengine.opengl.buffer;

import javax.microedition.khronos.opengles.GL11;
import org.anddev.andengine.opengl.util.FastFloatBuffer;
import org.anddev.andengine.opengl.util.GLHelper;

public abstract class BufferObject {
    private static final int[] HARDWAREBUFFERID_FETCHER = new int[1];
    protected final int[] mBufferData;
    private final int mDrawType;
    protected final FastFloatBuffer mFloatBuffer;
    private int mHardwareBufferID = -1;
    private boolean mHardwareBufferNeedsUpdate = true;
    private boolean mLoadedToHardware;
    private boolean mManaged;

    public BufferObject(int i, int i2, boolean z) {
        this.mDrawType = i2;
        this.mManaged = z;
        this.mBufferData = new int[i];
        this.mFloatBuffer = new FastFloatBuffer(i);
        if (z) {
            loadToActiveBufferObjectManager();
        }
    }

    public boolean isManaged() {
        return this.mManaged;
    }

    public void setManaged(boolean z) {
        this.mManaged = z;
    }

    public FastFloatBuffer getFloatBuffer() {
        return this.mFloatBuffer;
    }

    public int getHardwareBufferID() {
        return this.mHardwareBufferID;
    }

    public boolean isLoadedToHardware() {
        return this.mLoadedToHardware;
    }

    /* access modifiers changed from: package-private */
    public void setLoadedToHardware(boolean z) {
        this.mLoadedToHardware = z;
    }

    public void setHardwareBufferNeedsUpdate() {
        this.mHardwareBufferNeedsUpdate = true;
    }

    public void selectOnHardware(GL11 gl11) {
        int i = this.mHardwareBufferID;
        if (i != -1) {
            GLHelper.bindBuffer(gl11, i);
            if (this.mHardwareBufferNeedsUpdate) {
                this.mHardwareBufferNeedsUpdate = false;
                synchronized (this) {
                    GLHelper.bufferData(gl11, this.mFloatBuffer.mByteBuffer, this.mDrawType);
                }
            }
        }
    }

    public void loadToActiveBufferObjectManager() {
        BufferObjectManager.getActiveInstance().loadBufferObject(this);
    }

    public void unloadFromActiveBufferObjectManager() {
        BufferObjectManager.getActiveInstance().unloadBufferObject(this);
    }

    public void loadToHardware(GL11 gl11) {
        this.mHardwareBufferID = generateHardwareBufferID(gl11);
        this.mLoadedToHardware = true;
    }

    public void unloadFromHardware(GL11 gl11) {
        deleteBufferOnHardware(gl11);
        this.mHardwareBufferID = -1;
        this.mLoadedToHardware = false;
    }

    private void deleteBufferOnHardware(GL11 gl11) {
        GLHelper.deleteBuffer(gl11, this.mHardwareBufferID);
    }

    private int generateHardwareBufferID(GL11 gl11) {
        gl11.glGenBuffers(1, HARDWAREBUFFERID_FETCHER, 0);
        return HARDWAREBUFFERID_FETCHER[0];
    }
}
