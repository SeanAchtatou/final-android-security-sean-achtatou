package org.anddev.andengine.opengl.texture.atlas.bitmap.source.decorator.shape;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import org.anddev.andengine.opengl.texture.atlas.bitmap.source.decorator.BaseBitmapTextureAtlasSourceDecorator;

public class ArcBitmapTextureAtlasSourceDecoratorShape implements IBitmapTextureAtlasSourceDecoratorShape {
    private static final float STARTANGLE_DEFAULT = 0.0f;
    private static final float SWEEPANGLE_DEFAULT = 360.0f;
    private static final boolean USECENTER_DEFAULT = true;
    private static ArcBitmapTextureAtlasSourceDecoratorShape sDefaultInstance;
    private final RectF mRectF;
    private final float mStartAngle;
    private final float mSweepAngle;
    private final boolean mUseCenter;

    public ArcBitmapTextureAtlasSourceDecoratorShape() {
        this(STARTANGLE_DEFAULT, SWEEPANGLE_DEFAULT, USECENTER_DEFAULT);
    }

    public ArcBitmapTextureAtlasSourceDecoratorShape(float f, float f2, boolean z) {
        this.mRectF = new RectF();
        this.mStartAngle = f;
        this.mSweepAngle = f2;
        this.mUseCenter = z;
    }

    public static ArcBitmapTextureAtlasSourceDecoratorShape getDefaultInstance() {
        if (sDefaultInstance == null) {
            sDefaultInstance = new ArcBitmapTextureAtlasSourceDecoratorShape();
        }
        return sDefaultInstance;
    }

    public void onDecorateBitmap(Canvas canvas, Paint paint, BaseBitmapTextureAtlasSourceDecorator.TextureAtlasSourceDecoratorOptions textureAtlasSourceDecoratorOptions) {
        this.mRectF.set(textureAtlasSourceDecoratorOptions.getInsetLeft(), textureAtlasSourceDecoratorOptions.getInsetTop(), ((float) canvas.getWidth()) - textureAtlasSourceDecoratorOptions.getInsetRight(), ((float) canvas.getHeight()) - textureAtlasSourceDecoratorOptions.getInsetBottom());
        canvas.drawArc(this.mRectF, this.mStartAngle, this.mSweepAngle, this.mUseCenter, paint);
    }
}
