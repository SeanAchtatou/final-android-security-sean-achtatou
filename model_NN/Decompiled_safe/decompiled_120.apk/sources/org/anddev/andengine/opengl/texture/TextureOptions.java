package org.anddev.andengine.opengl.texture;

import javax.microedition.khronos.opengles.GL10;

public class TextureOptions {
    public static final TextureOptions BILINEAR = new TextureOptions(9729, 9729, 33071, 33071, 8448, false);
    public static final TextureOptions BILINEAR_PREMULTIPLYALPHA = new TextureOptions(9729, 9729, 33071, 33071, 8448, true);
    public static final TextureOptions DEFAULT = NEAREST_PREMULTIPLYALPHA;
    public static final TextureOptions NEAREST = new TextureOptions(9728, 9728, 33071, 33071, 8448, false);
    public static final TextureOptions NEAREST_PREMULTIPLYALPHA = new TextureOptions(9728, 9728, 33071, 33071, 8448, true);
    public static final TextureOptions REPEATING_BILINEAR = new TextureOptions(9729, 9729, 10497, 10497, 8448, false);
    public static final TextureOptions REPEATING_BILINEAR_PREMULTIPLYALPHA = new TextureOptions(9729, 9729, 10497, 10497, 8448, true);
    public static final TextureOptions REPEATING_NEAREST = new TextureOptions(9728, 9728, 10497, 10497, 8448, false);
    public static final TextureOptions REPEATING_NEAREST_PREMULTIPLYALPHA = new TextureOptions(9728, 9728, 10497, 10497, 8448, true);
    public final int mMagFilter;
    public final int mMinFilter;
    public final boolean mPreMultipyAlpha;
    public final int mTextureEnvironment;
    public final float mWrapS;
    public final float mWrapT;

    public TextureOptions(int i, int i2, int i3, int i4, int i5, boolean z) {
        this.mMinFilter = i;
        this.mMagFilter = i2;
        this.mWrapT = (float) i3;
        this.mWrapS = (float) i4;
        this.mTextureEnvironment = i5;
        this.mPreMultipyAlpha = z;
    }

    public void apply(GL10 gl10) {
        gl10.glTexParameterf(3553, 10241, (float) this.mMinFilter);
        gl10.glTexParameterf(3553, 10240, (float) this.mMagFilter);
        gl10.glTexParameterf(3553, 10242, this.mWrapS);
        gl10.glTexParameterf(3553, 10243, this.mWrapT);
        gl10.glTexEnvf(8960, 8704, (float) this.mTextureEnvironment);
    }
}
