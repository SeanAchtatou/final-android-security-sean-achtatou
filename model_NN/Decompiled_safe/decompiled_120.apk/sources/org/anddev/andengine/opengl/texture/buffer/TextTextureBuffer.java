package org.anddev.andengine.opengl.texture.buffer;

import org.anddev.andengine.opengl.buffer.BufferObject;
import org.anddev.andengine.opengl.font.Font;
import org.anddev.andengine.opengl.font.Letter;
import org.anddev.andengine.opengl.util.FastFloatBuffer;

public class TextTextureBuffer extends BufferObject {
    public TextTextureBuffer(int i, int i2, boolean z) {
        super(i, i2, z);
    }

    public synchronized void update(Font font, String[] strArr) {
        FastFloatBuffer floatBuffer = getFloatBuffer();
        floatBuffer.position(0);
        for (String str : strArr) {
            int length = str.length();
            for (int i = 0; i < length; i++) {
                Letter letter = font.getLetter(str.charAt(i));
                float f = letter.mTextureX;
                float f2 = letter.mTextureY;
                float f3 = letter.mTextureWidth + f;
                float f4 = letter.mTextureHeight + f2;
                floatBuffer.put(f);
                floatBuffer.put(f2);
                floatBuffer.put(f);
                floatBuffer.put(f4);
                floatBuffer.put(f3);
                floatBuffer.put(f4);
                floatBuffer.put(f3);
                floatBuffer.put(f4);
                floatBuffer.put(f3);
                floatBuffer.put(f2);
                floatBuffer.put(f);
                floatBuffer.put(f2);
            }
        }
        floatBuffer.position(0);
        setHardwareBufferNeedsUpdate();
    }
}
