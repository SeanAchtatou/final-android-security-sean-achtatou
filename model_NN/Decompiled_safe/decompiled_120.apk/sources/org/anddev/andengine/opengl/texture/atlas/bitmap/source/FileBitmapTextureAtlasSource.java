package org.anddev.andengine.opengl.texture.atlas.bitmap.source;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import org.anddev.andengine.opengl.texture.source.BaseTextureAtlasSource;
import org.anddev.andengine.util.Debug;
import org.anddev.andengine.util.StreamUtils;

public class FileBitmapTextureAtlasSource extends BaseTextureAtlasSource implements IBitmapTextureAtlasSource {
    private final File mFile;
    private final int mHeight;
    private final int mWidth;

    public FileBitmapTextureAtlasSource(File file) {
        this(file, 0, 0);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FileBitmapTextureAtlasSource(File file, int i, int i2) {
        super(i, i2);
        FileInputStream fileInputStream;
        FileInputStream fileInputStream2 = null;
        this.mFile = file;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        try {
            fileInputStream = new FileInputStream(file);
            try {
                BitmapFactory.decodeStream(fileInputStream, null, options);
                StreamUtils.close(fileInputStream);
            } catch (IOException e) {
                IOException iOException = e;
                fileInputStream2 = fileInputStream;
                e = iOException;
                try {
                    Debug.e("Failed loading Bitmap in FileBitmapTextureAtlasSource. File: " + file, e);
                    StreamUtils.close(fileInputStream2);
                    this.mWidth = options.outWidth;
                    this.mHeight = options.outHeight;
                } catch (Throwable th) {
                    th = th;
                    fileInputStream = fileInputStream2;
                    StreamUtils.close(fileInputStream);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                StreamUtils.close(fileInputStream);
                throw th;
            }
        } catch (IOException e2) {
            e = e2;
            Debug.e("Failed loading Bitmap in FileBitmapTextureAtlasSource. File: " + file, e);
            StreamUtils.close(fileInputStream2);
            this.mWidth = options.outWidth;
            this.mHeight = options.outHeight;
        } catch (Throwable th3) {
            th = th3;
            fileInputStream = null;
            StreamUtils.close(fileInputStream);
            throw th;
        }
        this.mWidth = options.outWidth;
        this.mHeight = options.outHeight;
    }

    FileBitmapTextureAtlasSource(File file, int i, int i2, int i3, int i4) {
        super(i, i2);
        this.mFile = file;
        this.mWidth = i3;
        this.mHeight = i4;
    }

    public FileBitmapTextureAtlasSource clone() {
        return new FileBitmapTextureAtlasSource(this.mFile, this.mTexturePositionX, this.mTexturePositionY, this.mWidth, this.mHeight);
    }

    public int getWidth() {
        return this.mWidth;
    }

    public int getHeight() {
        return this.mHeight;
    }

    public Bitmap onLoadBitmap(Bitmap.Config config) {
        FileInputStream fileInputStream;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = config;
        try {
            fileInputStream = new FileInputStream(this.mFile);
            try {
                Bitmap decodeStream = BitmapFactory.decodeStream(fileInputStream, null, options);
                StreamUtils.close(fileInputStream);
                return decodeStream;
            } catch (IOException e) {
                e = e;
                try {
                    Debug.e("Failed loading Bitmap in " + getClass().getSimpleName() + ". File: " + this.mFile, e);
                    StreamUtils.close(fileInputStream);
                    return null;
                } catch (Throwable th) {
                    th = th;
                    StreamUtils.close(fileInputStream);
                    throw th;
                }
            }
        } catch (IOException e2) {
            e = e2;
            fileInputStream = null;
            Debug.e("Failed loading Bitmap in " + getClass().getSimpleName() + ". File: " + this.mFile, e);
            StreamUtils.close(fileInputStream);
            return null;
        } catch (Throwable th2) {
            th = th2;
            fileInputStream = null;
            StreamUtils.close(fileInputStream);
            throw th;
        }
    }

    public String toString() {
        return String.valueOf(getClass().getSimpleName()) + "(" + this.mFile + ")";
    }
}
