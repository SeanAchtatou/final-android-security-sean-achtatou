package org.anddev.andengine.opengl.view;

class SimpleEGLConfigChooser extends ComponentSizeChooser {
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SimpleEGLConfigChooser(boolean z) {
        super(4, 4, 4, 0, z ? 16 : 0, 0);
        this.mRedSize = 5;
        this.mGreenSize = 6;
        this.mBlueSize = 5;
    }
}
