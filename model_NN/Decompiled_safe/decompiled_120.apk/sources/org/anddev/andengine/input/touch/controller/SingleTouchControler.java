package org.anddev.andengine.input.touch.controller;

import android.view.MotionEvent;

public class SingleTouchControler extends BaseTouchController {
    public boolean onHandleMotionEvent(MotionEvent motionEvent) {
        return fireTouchEvent(motionEvent.getX(), motionEvent.getY(), motionEvent.getAction(), 0, motionEvent);
    }
}
