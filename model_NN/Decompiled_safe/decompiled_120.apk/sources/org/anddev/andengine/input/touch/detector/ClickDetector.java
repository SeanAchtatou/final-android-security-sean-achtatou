package org.anddev.andengine.input.touch.detector;

import org.anddev.andengine.input.touch.TouchEvent;

public class ClickDetector extends BaseDetector {
    private static final long TRIGGER_CLICK_MAXIMUM_MILLISECONDS_DEFAULT = 200;
    private final IClickDetectorListener mClickDetectorListener;
    private long mDownTimeMilliseconds;
    private long mTriggerClickMaximumMilliseconds;

    public interface IClickDetectorListener {
        void onClick(ClickDetector clickDetector, TouchEvent touchEvent);
    }

    public ClickDetector(IClickDetectorListener iClickDetectorListener) {
        this(TRIGGER_CLICK_MAXIMUM_MILLISECONDS_DEFAULT, iClickDetectorListener);
    }

    public ClickDetector(long j, IClickDetectorListener iClickDetectorListener) {
        this.mDownTimeMilliseconds = Long.MIN_VALUE;
        this.mTriggerClickMaximumMilliseconds = j;
        this.mClickDetectorListener = iClickDetectorListener;
    }

    public long getTriggerClickMaximumMilliseconds() {
        return this.mTriggerClickMaximumMilliseconds;
    }

    public void setTriggerClickMaximumMilliseconds(long j) {
        this.mTriggerClickMaximumMilliseconds = j;
    }

    public boolean onManagedTouchEvent(TouchEvent touchEvent) {
        switch (touchEvent.getAction()) {
            case 0:
                this.mDownTimeMilliseconds = touchEvent.getMotionEvent().getDownTime();
                return true;
            case 1:
            case TouchEvent.ACTION_CANCEL:
                if (touchEvent.getMotionEvent().getEventTime() - this.mDownTimeMilliseconds <= this.mTriggerClickMaximumMilliseconds) {
                    this.mDownTimeMilliseconds = Long.MIN_VALUE;
                    this.mClickDetectorListener.onClick(this, touchEvent);
                }
                return true;
            case 2:
            default:
                return false;
        }
    }
}
