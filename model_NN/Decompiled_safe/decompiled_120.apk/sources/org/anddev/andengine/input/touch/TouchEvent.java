package org.anddev.andengine.input.touch;

import android.view.MotionEvent;
import org.anddev.andengine.util.pool.GenericPool;

public class TouchEvent {
    public static final int ACTION_CANCEL = 3;
    public static final int ACTION_DOWN = 0;
    public static final int ACTION_MOVE = 2;
    public static final int ACTION_OUTSIDE = 4;
    public static final int ACTION_UP = 1;
    private static final TouchEventPool TOUCHEVENT_POOL = new TouchEventPool(null);
    protected int mAction;
    protected MotionEvent mMotionEvent;
    protected int mPointerID;
    protected float mX;
    protected float mY;

    public static TouchEvent obtain(float f, float f2, int i, int i2, MotionEvent motionEvent) {
        TouchEvent touchEvent = (TouchEvent) TOUCHEVENT_POOL.obtainPoolItem();
        touchEvent.set(f, f2, i, i2, motionEvent);
        return touchEvent;
    }

    private void set(float f, float f2, int i, int i2, MotionEvent motionEvent) {
        this.mX = f;
        this.mY = f2;
        this.mAction = i;
        this.mPointerID = i2;
        this.mMotionEvent = motionEvent;
    }

    public void recycle() {
        TOUCHEVENT_POOL.recyclePoolItem(this);
    }

    public static void recycle(TouchEvent touchEvent) {
        TOUCHEVENT_POOL.recyclePoolItem(touchEvent);
    }

    public float getX() {
        return this.mX;
    }

    public float getY() {
        return this.mY;
    }

    public void set(float f, float f2) {
        this.mX = f;
        this.mY = f2;
    }

    public void offset(float f, float f2) {
        this.mX += f;
        this.mY += f2;
    }

    public int getPointerID() {
        return this.mPointerID;
    }

    public int getAction() {
        return this.mAction;
    }

    public boolean isActionDown() {
        return this.mAction == 0;
    }

    public boolean isActionUp() {
        return this.mAction == 1;
    }

    public boolean isActionMove() {
        return this.mAction == 2;
    }

    public boolean isActionCancel() {
        return this.mAction == 3;
    }

    public boolean isActionOutside() {
        return this.mAction == 4;
    }

    public MotionEvent getMotionEvent() {
        return this.mMotionEvent;
    }

    final class TouchEventPool extends GenericPool {
        private TouchEventPool() {
        }

        /* synthetic */ TouchEventPool(TouchEventPool touchEventPool) {
            this();
        }

        /* access modifiers changed from: protected */
        public final TouchEvent onAllocatePoolItem() {
            return new TouchEvent();
        }
    }
}
