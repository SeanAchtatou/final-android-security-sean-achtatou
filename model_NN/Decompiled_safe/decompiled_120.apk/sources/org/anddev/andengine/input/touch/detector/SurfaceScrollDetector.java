package org.anddev.andengine.input.touch.detector;

import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.input.touch.detector.ScrollDetector;

public class SurfaceScrollDetector extends ScrollDetector {
    public SurfaceScrollDetector(float f, ScrollDetector.IScrollDetectorListener iScrollDetectorListener) {
        super(f, iScrollDetectorListener);
    }

    public SurfaceScrollDetector(ScrollDetector.IScrollDetectorListener iScrollDetectorListener) {
        super(iScrollDetectorListener);
    }

    /* access modifiers changed from: protected */
    public float getX(TouchEvent touchEvent) {
        return touchEvent.getMotionEvent().getX();
    }

    /* access modifiers changed from: protected */
    public float getY(TouchEvent touchEvent) {
        return touchEvent.getMotionEvent().getY();
    }
}
