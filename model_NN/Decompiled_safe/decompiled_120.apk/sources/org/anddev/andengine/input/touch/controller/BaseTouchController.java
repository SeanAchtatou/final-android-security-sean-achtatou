package org.anddev.andengine.input.touch.controller;

import android.view.MotionEvent;
import org.anddev.andengine.engine.options.TouchOptions;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.input.touch.controller.ITouchController;
import org.anddev.andengine.util.pool.RunnablePoolItem;
import org.anddev.andengine.util.pool.RunnablePoolUpdateHandler;

public abstract class BaseTouchController implements ITouchController {
    private boolean mRunOnUpdateThread;
    /* access modifiers changed from: private */
    public ITouchController.ITouchEventCallback mTouchEventCallback;
    private final RunnablePoolUpdateHandler mTouchEventRunnablePoolUpdateHandler = new RunnablePoolUpdateHandler() {
        /* access modifiers changed from: protected */
        public TouchEventRunnablePoolItem onAllocatePoolItem() {
            return new TouchEventRunnablePoolItem();
        }
    };

    public void setTouchEventCallback(ITouchController.ITouchEventCallback iTouchEventCallback) {
        this.mTouchEventCallback = iTouchEventCallback;
    }

    public void reset() {
        if (this.mRunOnUpdateThread) {
            this.mTouchEventRunnablePoolUpdateHandler.reset();
        }
    }

    public void onUpdate(float f) {
        if (this.mRunOnUpdateThread) {
            this.mTouchEventRunnablePoolUpdateHandler.onUpdate(f);
        }
    }

    /* access modifiers changed from: protected */
    public boolean fireTouchEvent(float f, float f2, int i, int i2, MotionEvent motionEvent) {
        if (this.mRunOnUpdateThread) {
            TouchEvent obtain = TouchEvent.obtain(f, f2, i, i2, MotionEvent.obtain(motionEvent));
            TouchEventRunnablePoolItem touchEventRunnablePoolItem = (TouchEventRunnablePoolItem) this.mTouchEventRunnablePoolUpdateHandler.obtainPoolItem();
            touchEventRunnablePoolItem.set(obtain);
            this.mTouchEventRunnablePoolUpdateHandler.postPoolItem(touchEventRunnablePoolItem);
            return true;
        }
        TouchEvent obtain2 = TouchEvent.obtain(f, f2, i, i2, motionEvent);
        boolean onTouchEvent = this.mTouchEventCallback.onTouchEvent(obtain2);
        obtain2.recycle();
        return onTouchEvent;
    }

    public void applyTouchOptions(TouchOptions touchOptions) {
        this.mRunOnUpdateThread = touchOptions.isRunOnUpdateThread();
    }

    class TouchEventRunnablePoolItem extends RunnablePoolItem {
        private TouchEvent mTouchEvent;

        TouchEventRunnablePoolItem() {
        }

        public void set(TouchEvent touchEvent) {
            this.mTouchEvent = touchEvent;
        }

        public void run() {
            BaseTouchController.this.mTouchEventCallback.onTouchEvent(this.mTouchEvent);
        }

        /* access modifiers changed from: protected */
        public void onRecycle() {
            super.onRecycle();
            TouchEvent touchEvent = this.mTouchEvent;
            touchEvent.getMotionEvent().recycle();
            touchEvent.recycle();
        }
    }
}
