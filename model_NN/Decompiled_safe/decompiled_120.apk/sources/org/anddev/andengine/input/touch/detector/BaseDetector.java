package org.anddev.andengine.input.touch.detector;

import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.input.touch.TouchEvent;

public abstract class BaseDetector implements Scene.IOnSceneTouchListener {
    private boolean mEnabled = true;

    /* access modifiers changed from: protected */
    public abstract boolean onManagedTouchEvent(TouchEvent touchEvent);

    public boolean isEnabled() {
        return this.mEnabled;
    }

    public void setEnabled(boolean z) {
        this.mEnabled = z;
    }

    public boolean onSceneTouchEvent(Scene scene, TouchEvent touchEvent) {
        return onTouchEvent(touchEvent);
    }

    public final boolean onTouchEvent(TouchEvent touchEvent) {
        if (this.mEnabled) {
            return onManagedTouchEvent(touchEvent);
        }
        return false;
    }
}
