package org.anddev.andengine.input.touch.detector;

import org.anddev.andengine.input.touch.TouchEvent;

public class ScrollDetector extends BaseDetector {
    private static final float TRIGGER_SCROLL_MINIMUM_DISTANCE_DEFAULT = 10.0f;
    private float mLastX;
    private float mLastY;
    private final IScrollDetectorListener mScrollDetectorListener;
    private float mTriggerScrollMinimumDistance;
    private boolean mTriggered;

    public interface IScrollDetectorListener {
        void onScroll(ScrollDetector scrollDetector, TouchEvent touchEvent, float f, float f2);
    }

    public ScrollDetector(IScrollDetectorListener iScrollDetectorListener) {
        this(TRIGGER_SCROLL_MINIMUM_DISTANCE_DEFAULT, iScrollDetectorListener);
    }

    public ScrollDetector(float f, IScrollDetectorListener iScrollDetectorListener) {
        this.mTriggerScrollMinimumDistance = f;
        this.mScrollDetectorListener = iScrollDetectorListener;
    }

    public float getTriggerScrollMinimumDistance() {
        return this.mTriggerScrollMinimumDistance;
    }

    public void setTriggerScrollMinimumDistance(float f) {
        this.mTriggerScrollMinimumDistance = f;
    }

    public boolean onManagedTouchEvent(TouchEvent touchEvent) {
        float x = getX(touchEvent);
        float y = getY(touchEvent);
        switch (touchEvent.getAction()) {
            case 0:
                this.mLastX = x;
                this.mLastY = y;
                this.mTriggered = false;
                return true;
            case 1:
            case 2:
            case TouchEvent.ACTION_CANCEL:
                float f = x - this.mLastX;
                float f2 = y - this.mLastY;
                float f3 = this.mTriggerScrollMinimumDistance;
                if (this.mTriggered || Math.abs(f) > f3 || Math.abs(f2) > f3) {
                    this.mScrollDetectorListener.onScroll(this, touchEvent, f, f2);
                    this.mLastX = x;
                    this.mLastY = y;
                    this.mTriggered = true;
                }
                return true;
            default:
                return false;
        }
    }

    /* access modifiers changed from: protected */
    public float getX(TouchEvent touchEvent) {
        return touchEvent.getX();
    }

    /* access modifiers changed from: protected */
    public float getY(TouchEvent touchEvent) {
        return touchEvent.getY();
    }
}
