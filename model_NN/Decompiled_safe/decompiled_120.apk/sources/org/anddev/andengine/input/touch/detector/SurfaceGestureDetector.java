package org.anddev.andengine.input.touch.detector;

import android.view.GestureDetector;
import android.view.MotionEvent;
import org.anddev.andengine.input.touch.TouchEvent;

public abstract class SurfaceGestureDetector extends BaseDetector {
    private static final float SWIPE_MIN_DISTANCE_DEFAULT = 120.0f;
    private final GestureDetector mGestureDetector;

    /* access modifiers changed from: protected */
    public abstract boolean onDoubleTap();

    /* access modifiers changed from: protected */
    public abstract boolean onSingleTap();

    /* access modifiers changed from: protected */
    public abstract boolean onSwipeDown();

    /* access modifiers changed from: protected */
    public abstract boolean onSwipeLeft();

    /* access modifiers changed from: protected */
    public abstract boolean onSwipeRight();

    /* access modifiers changed from: protected */
    public abstract boolean onSwipeUp();

    public SurfaceGestureDetector() {
        this(SWIPE_MIN_DISTANCE_DEFAULT);
    }

    public SurfaceGestureDetector(float f) {
        this.mGestureDetector = new GestureDetector(new InnerOnGestureDetectorListener(f));
    }

    public boolean onManagedTouchEvent(TouchEvent touchEvent) {
        return this.mGestureDetector.onTouchEvent(touchEvent.getMotionEvent());
    }

    class InnerOnGestureDetectorListener extends GestureDetector.SimpleOnGestureListener {
        private final float mSwipeMinDistance;

        public InnerOnGestureDetectorListener(float f) {
            this.mSwipeMinDistance = f;
        }

        public boolean onSingleTapConfirmed(MotionEvent motionEvent) {
            return SurfaceGestureDetector.this.onSingleTap();
        }

        public boolean onDoubleTap(MotionEvent motionEvent) {
            return SurfaceGestureDetector.this.onDoubleTap();
        }

        public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
            float f3 = this.mSwipeMinDistance;
            if (Math.abs(f) > Math.abs(f2)) {
                if (motionEvent.getX() - motionEvent2.getX() > f3) {
                    return SurfaceGestureDetector.this.onSwipeLeft();
                }
                if (motionEvent2.getX() - motionEvent.getX() > f3) {
                    return SurfaceGestureDetector.this.onSwipeRight();
                }
            } else if (motionEvent.getY() - motionEvent2.getY() > f3) {
                return SurfaceGestureDetector.this.onSwipeUp();
            } else {
                if (motionEvent2.getY() - motionEvent.getY() > f3) {
                    return SurfaceGestureDetector.this.onSwipeDown();
                }
            }
            return false;
        }
    }

    public class SurfaceGestureDetectorAdapter extends SurfaceGestureDetector {
        /* access modifiers changed from: protected */
        public boolean onDoubleTap() {
            return false;
        }

        /* access modifiers changed from: protected */
        public boolean onSingleTap() {
            return false;
        }

        /* access modifiers changed from: protected */
        public boolean onSwipeDown() {
            return false;
        }

        /* access modifiers changed from: protected */
        public boolean onSwipeLeft() {
            return false;
        }

        /* access modifiers changed from: protected */
        public boolean onSwipeRight() {
            return false;
        }

        /* access modifiers changed from: protected */
        public boolean onSwipeUp() {
            return false;
        }
    }
}
