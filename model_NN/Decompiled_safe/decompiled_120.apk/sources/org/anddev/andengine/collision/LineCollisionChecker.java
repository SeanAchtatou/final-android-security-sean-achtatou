package org.anddev.andengine.collision;

import org.anddev.andengine.entity.primitive.Line;

public class LineCollisionChecker extends ShapeCollisionChecker {
    public static boolean checkLineCollision(float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8) {
        if (BaseCollisionChecker.relativeCCW(f, f2, f3, f4, f7, f8) * BaseCollisionChecker.relativeCCW(f, f2, f3, f4, f5, f6) <= 0) {
            if (BaseCollisionChecker.relativeCCW(f5, f6, f7, f8, f3, f4) * BaseCollisionChecker.relativeCCW(f5, f6, f7, f8, f, f2) <= 0) {
                return true;
            }
        }
        return false;
    }

    public static void fillVertices(Line line, float[] fArr) {
        fArr[0] = 0.0f;
        fArr[1] = 0.0f;
        fArr[2] = line.getX2() - line.getX1();
        fArr[3] = line.getY2() - line.getY1();
        line.getLocalToSceneTransformation().transform(fArr);
    }
}
