package org.anddev.andengine.collision;

import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.entity.primitive.Line;
import org.anddev.andengine.entity.shape.RectangularShape;
import org.anddev.andengine.util.MathUtils;

public class RectangularShapeCollisionChecker extends ShapeCollisionChecker {
    private static final int LINE_VERTEX_COUNT = 2;
    private static final int RECTANGULARSHAPE_VERTEX_COUNT = 4;
    private static final float[] VERTICES_COLLISION_TMP_A = new float[8];
    private static final float[] VERTICES_COLLISION_TMP_B = new float[8];
    private static final float[] VERTICES_CONTAINS_TMP = new float[8];

    public static boolean checkContains(RectangularShape rectangularShape, float f, float f2) {
        fillVertices(rectangularShape, VERTICES_CONTAINS_TMP);
        return ShapeCollisionChecker.checkContains(VERTICES_CONTAINS_TMP, 8, f, f2);
    }

    public static boolean isVisible(Camera camera, RectangularShape rectangularShape) {
        fillVertices(camera, VERTICES_COLLISION_TMP_A);
        fillVertices(rectangularShape, VERTICES_COLLISION_TMP_B);
        return ShapeCollisionChecker.checkCollision(8, VERTICES_COLLISION_TMP_A, 8, VERTICES_COLLISION_TMP_B);
    }

    public static boolean isVisible(Camera camera, Line line) {
        fillVertices(camera, VERTICES_COLLISION_TMP_A);
        LineCollisionChecker.fillVertices(line, VERTICES_COLLISION_TMP_B);
        return ShapeCollisionChecker.checkCollision(8, VERTICES_COLLISION_TMP_A, 4, VERTICES_COLLISION_TMP_B);
    }

    public static boolean checkCollision(RectangularShape rectangularShape, RectangularShape rectangularShape2) {
        fillVertices(rectangularShape, VERTICES_COLLISION_TMP_A);
        fillVertices(rectangularShape2, VERTICES_COLLISION_TMP_B);
        return ShapeCollisionChecker.checkCollision(8, VERTICES_COLLISION_TMP_A, 8, VERTICES_COLLISION_TMP_B);
    }

    public static boolean checkCollision(RectangularShape rectangularShape, Line line) {
        fillVertices(rectangularShape, VERTICES_COLLISION_TMP_A);
        LineCollisionChecker.fillVertices(line, VERTICES_COLLISION_TMP_B);
        return ShapeCollisionChecker.checkCollision(8, VERTICES_COLLISION_TMP_A, 4, VERTICES_COLLISION_TMP_B);
    }

    public static void fillVertices(RectangularShape rectangularShape, float[] fArr) {
        float width = rectangularShape.getWidth();
        float height = rectangularShape.getHeight();
        fArr[0] = 0.0f;
        fArr[1] = 0.0f;
        fArr[2] = width;
        fArr[3] = 0.0f;
        fArr[4] = width;
        fArr[5] = height;
        fArr[6] = 0.0f;
        fArr[7] = height;
        rectangularShape.getLocalToSceneTransformation().transform(fArr);
    }

    private static void fillVertices(Camera camera, float[] fArr) {
        fArr[0] = camera.getMinX();
        fArr[1] = camera.getMinY();
        fArr[2] = camera.getMaxX();
        fArr[3] = camera.getMinY();
        fArr[4] = camera.getMaxX();
        fArr[5] = camera.getMaxY();
        fArr[6] = camera.getMinX();
        fArr[7] = camera.getMaxY();
        MathUtils.rotateAroundCenter(fArr, camera.getRotation(), camera.getCenterX(), camera.getCenterY());
    }
}
