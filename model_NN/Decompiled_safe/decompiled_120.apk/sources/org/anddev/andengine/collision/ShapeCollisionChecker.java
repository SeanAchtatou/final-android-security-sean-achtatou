package org.anddev.andengine.collision;

public class ShapeCollisionChecker extends BaseCollisionChecker {
    public static boolean checkCollision(int i, float[] fArr, int i2, float[] fArr2) {
        for (int i3 = i - 4; i3 >= 0; i3 -= 2) {
            if (checkCollisionSub(i3, i3 + 2, fArr, fArr2, i2)) {
                return true;
            }
        }
        if (checkCollisionSub(i - 2, 0, fArr, fArr2, i2)) {
            return true;
        }
        if (checkContains(fArr, i, fArr2[0], fArr2[1])) {
            return true;
        }
        if (checkContains(fArr2, i2, fArr[0], fArr[1])) {
            return true;
        }
        return false;
    }

    private static boolean checkCollisionSub(int i, int i2, float[] fArr, float[] fArr2, int i3) {
        float f = fArr[i + 0];
        float f2 = fArr[i + 1];
        float f3 = fArr[i2 + 0];
        float f4 = fArr[i2 + 1];
        for (int i4 = i3 - 4; i4 >= 0; i4 -= 2) {
            if (LineCollisionChecker.checkLineCollision(f, f2, f3, f4, fArr2[i4 + 0], fArr2[i4 + 1], fArr2[i4 + 2 + 0], fArr2[i4 + 2 + 1])) {
                return true;
            }
        }
        if (LineCollisionChecker.checkLineCollision(f, f2, f3, f4, fArr2[i3 - 2], fArr2[i3 - 1], fArr2[0], fArr2[1])) {
            return true;
        }
        return false;
    }

    public static boolean checkContains(float[] fArr, int i, float f, float f2) {
        int i2 = i - 4;
        int i3 = 0;
        while (i2 >= 0) {
            int relativeCCW = BaseCollisionChecker.relativeCCW(fArr[i2], fArr[i2 + 1], fArr[i2 + 2], fArr[i2 + 3], f, f2);
            if (relativeCCW == 0) {
                return true;
            }
            i2 -= 2;
            i3 = relativeCCW + i3;
        }
        int relativeCCW2 = BaseCollisionChecker.relativeCCW(fArr[i - 2], fArr[i - 1], fArr[0], fArr[1], f, f2);
        if (relativeCCW2 == 0) {
            return true;
        }
        int i4 = relativeCCW2 + i3;
        int i5 = i / 2;
        return i4 == i5 || i4 == (-i5);
    }
}
