package org.anddev.andengine.util.modifier.ease;

public class EaseStrongIn implements IEaseFunction {
    private static EaseStrongIn INSTANCE;

    private EaseStrongIn() {
    }

    public static EaseStrongIn getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EaseStrongIn();
        }
        return INSTANCE;
    }

    public float getPercentage(float f, float f2) {
        return getValue(f / f2);
    }

    public static float getValue(float f) {
        return f * f * f * f * f;
    }
}
