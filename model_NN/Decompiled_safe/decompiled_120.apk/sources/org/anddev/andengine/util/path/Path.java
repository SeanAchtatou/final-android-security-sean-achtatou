package org.anddev.andengine.util.path;

import java.util.ArrayList;

public class Path {
    private final ArrayList mSteps = new ArrayList();

    public int getLength() {
        return this.mSteps.size();
    }

    public Step getStep(int i) {
        return (Step) this.mSteps.get(i);
    }

    public Direction getDirectionToPreviousStep(int i) {
        if (i == 0) {
            return null;
        }
        return Direction.fromDelta(getTileColumn(i - 1) - getTileColumn(i), getTileRow(i - 1) - getTileRow(i));
    }

    public Direction getDirectionToNextStep(int i) {
        if (i == getLength() - 1) {
            return null;
        }
        return Direction.fromDelta(getTileColumn(i + 1) - getTileColumn(i), getTileRow(i + 1) - getTileRow(i));
    }

    public int getTileColumn(int i) {
        return getStep(i).getTileColumn();
    }

    public int getTileRow(int i) {
        return getStep(i).getTileRow();
    }

    public void append(int i, int i2) {
        append(new Step(i, i2));
    }

    public void append(Step step) {
        this.mSteps.add(step);
    }

    public void prepend(int i, int i2) {
        prepend(new Step(i, i2));
    }

    public void prepend(Step step) {
        this.mSteps.add(0, step);
    }

    public boolean contains(int i, int i2) {
        ArrayList arrayList = this.mSteps;
        for (int size = arrayList.size() - 1; size >= 0; size--) {
            Step step = (Step) arrayList.get(size);
            if (step.getTileColumn() == i && step.getTileRow() == i2) {
                return true;
            }
        }
        return false;
    }

    public int getFromTileRow() {
        return getTileRow(0);
    }

    public int getFromTileColumn() {
        return getTileColumn(0);
    }

    public int getToTileRow() {
        return getTileRow(this.mSteps.size() - 1);
    }

    public int getToTileColumn() {
        return getTileColumn(this.mSteps.size() - 1);
    }

    public class Step {
        private final int mTileColumn;
        private final int mTileRow;

        public Step(int i, int i2) {
            this.mTileColumn = i;
            this.mTileRow = i2;
        }

        public int getTileColumn() {
            return this.mTileColumn;
        }

        public int getTileRow() {
            return this.mTileRow;
        }

        public int hashCode() {
            return this.mTileColumn << (this.mTileRow + 16);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            Step step = (Step) obj;
            if (this.mTileColumn != step.mTileColumn) {
                return false;
            }
            if (this.mTileRow != step.mTileRow) {
                return false;
            }
            return true;
        }
    }
}
