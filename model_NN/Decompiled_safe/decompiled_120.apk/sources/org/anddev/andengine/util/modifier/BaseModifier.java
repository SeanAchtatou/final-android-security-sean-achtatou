package org.anddev.andengine.util.modifier;

import org.anddev.andengine.util.SmartList;
import org.anddev.andengine.util.modifier.IModifier;

public abstract class BaseModifier implements IModifier {
    protected boolean mFinished;
    private final SmartList mModifierListeners = new SmartList(2);
    private boolean mRemoveWhenFinished = true;

    public abstract IModifier clone();

    public BaseModifier() {
    }

    public BaseModifier(IModifier.IModifierListener iModifierListener) {
        addModifierListener(iModifierListener);
    }

    public boolean isFinished() {
        return this.mFinished;
    }

    public final boolean isRemoveWhenFinished() {
        return this.mRemoveWhenFinished;
    }

    public final void setRemoveWhenFinished(boolean z) {
        this.mRemoveWhenFinished = z;
    }

    public void addModifierListener(IModifier.IModifierListener iModifierListener) {
        if (iModifierListener != null) {
            this.mModifierListeners.add(iModifierListener);
        }
    }

    public boolean removeModifierListener(IModifier.IModifierListener iModifierListener) {
        if (iModifierListener == null) {
            return false;
        }
        return this.mModifierListeners.remove(iModifierListener);
    }

    /* access modifiers changed from: protected */
    public void onModifierStarted(Object obj) {
        SmartList smartList = this.mModifierListeners;
        for (int size = smartList.size() - 1; size >= 0; size--) {
            ((IModifier.IModifierListener) smartList.get(size)).onModifierStarted(this, obj);
        }
    }

    /* access modifiers changed from: protected */
    public void onModifierFinished(Object obj) {
        SmartList smartList = this.mModifierListeners;
        for (int size = smartList.size() - 1; size >= 0; size--) {
            ((IModifier.IModifierListener) smartList.get(size)).onModifierFinished(this, obj);
        }
    }
}
