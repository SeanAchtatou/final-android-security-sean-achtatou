package org.anddev.andengine.util.pool;

public abstract class Pool extends GenericPool {
    public Pool() {
    }

    public Pool(int i) {
        super(i);
    }

    public Pool(int i, int i2) {
        super(i, i2);
    }

    /* access modifiers changed from: protected */
    public PoolItem onHandleAllocatePoolItem() {
        PoolItem poolItem = (PoolItem) super.onHandleAllocatePoolItem();
        poolItem.mParent = this;
        return poolItem;
    }

    /* access modifiers changed from: protected */
    public void onHandleObtainItem(PoolItem poolItem) {
        poolItem.mRecycled = false;
        poolItem.onObtain();
    }

    /* access modifiers changed from: protected */
    public void onHandleRecycleItem(PoolItem poolItem) {
        poolItem.onRecycle();
        poolItem.mRecycled = true;
    }

    public synchronized void recyclePoolItem(PoolItem poolItem) {
        if (poolItem.mParent == null) {
            throw new IllegalArgumentException("PoolItem not assigned to a pool!");
        } else if (!poolItem.isFromPool(this)) {
            throw new IllegalArgumentException("PoolItem from another pool!");
        } else if (poolItem.isRecycled()) {
            throw new IllegalArgumentException("PoolItem already recycled!");
        } else {
            super.recyclePoolItem((Object) poolItem);
        }
    }

    public synchronized boolean ownsPoolItem(PoolItem poolItem) {
        return poolItem.mParent == this;
    }

    /* access modifiers changed from: package-private */
    public void recycle(PoolItem poolItem) {
        recyclePoolItem(poolItem);
    }
}
