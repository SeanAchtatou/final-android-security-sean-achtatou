package org.anddev.andengine.util.modifier.ease;

public class EaseStrongInOut implements IEaseFunction {
    private static EaseStrongInOut INSTANCE;

    private EaseStrongInOut() {
    }

    public static EaseStrongInOut getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EaseStrongInOut();
        }
        return INSTANCE;
    }

    public float getPercentage(float f, float f2) {
        float f3 = f / f2;
        if (f3 < 0.5f) {
            return EaseStrongIn.getValue(f3 * 2.0f) * 0.5f;
        }
        return (EaseStrongOut.getValue((f3 * 2.0f) - 1.0f) * 0.5f) + 0.5f;
    }
}
