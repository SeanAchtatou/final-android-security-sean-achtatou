package org.anddev.andengine.util.modifier.ease;

public class EaseBounceOut implements IEaseFunction {
    private static EaseBounceOut INSTANCE;

    private EaseBounceOut() {
    }

    public static EaseBounceOut getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EaseBounceOut();
        }
        return INSTANCE;
    }

    public float getPercentage(float f, float f2) {
        return getValue(f / f2);
    }

    public static float getValue(float f) {
        if (f < 0.36363637f) {
            return 7.5625f * f * f;
        }
        if (f < 0.72727275f) {
            float f2 = f - 0.54545456f;
            return (f2 * 7.5625f * f2) + 0.75f;
        } else if (f < 0.90909094f) {
            float f3 = f - 0.8181818f;
            return (f3 * 7.5625f * f3) + 0.9375f;
        } else {
            float f4 = f - 0.95454544f;
            return (f4 * 7.5625f * f4) + 0.984375f;
        }
    }
}
