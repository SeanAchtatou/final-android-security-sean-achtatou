package org.anddev.andengine.util.modifier.ease;

import android.util.FloatMath;

public class EaseCircularIn implements IEaseFunction {
    private static EaseCircularIn INSTANCE;

    private EaseCircularIn() {
    }

    public static EaseCircularIn getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EaseCircularIn();
        }
        return INSTANCE;
    }

    public float getPercentage(float f, float f2) {
        return getValue(f / f2);
    }

    public static float getValue(float f) {
        return -(FloatMath.sqrt(1.0f - (f * f)) - 1.0f);
    }
}
