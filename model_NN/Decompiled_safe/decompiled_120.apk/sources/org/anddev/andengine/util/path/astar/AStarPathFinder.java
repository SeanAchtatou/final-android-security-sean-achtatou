package org.anddev.andengine.util.path.astar;

import java.lang.reflect.Array;
import java.util.ArrayList;
import org.anddev.andengine.util.path.IPathFinder;
import org.anddev.andengine.util.path.ITiledMap;

public class AStarPathFinder implements IPathFinder {
    private final IAStarHeuristic mAStarHeuristic;
    private final boolean mAllowDiagonalMovement;
    private final int mMaxSearchDepth;
    private final Node[][] mNodes;
    private final ArrayList mOpenNodes;
    private final ITiledMap mTiledMap;
    private final ArrayList mVisitedNodes;

    public AStarPathFinder(ITiledMap iTiledMap, int i, boolean z) {
        this(iTiledMap, i, z, new EuclideanHeuristic());
    }

    public AStarPathFinder(ITiledMap iTiledMap, int i, boolean z, IAStarHeuristic iAStarHeuristic) {
        this.mVisitedNodes = new ArrayList();
        this.mOpenNodes = new ArrayList();
        this.mAStarHeuristic = iAStarHeuristic;
        this.mTiledMap = iTiledMap;
        this.mMaxSearchDepth = i;
        this.mAllowDiagonalMovement = z;
        this.mNodes = (Node[][]) Array.newInstance(Node.class, iTiledMap.getTileRows(), iTiledMap.getTileColumns());
        Node[][] nodeArr = this.mNodes;
        for (int tileColumns = iTiledMap.getTileColumns() - 1; tileColumns >= 0; tileColumns--) {
            for (int tileRows = iTiledMap.getTileRows() - 1; tileRows >= 0; tileRows--) {
                nodeArr[tileRows][tileColumns] = new Node(tileColumns, tileRows);
            }
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v13, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v24, resolved type: org.anddev.andengine.util.path.astar.AStarPathFinder$Node} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.anddev.andengine.util.path.Path findPath(java.lang.Object r27, int r28, int r29, int r30, int r31, int r32) {
        /*
            r26 = this;
            r0 = r26
            org.anddev.andengine.util.path.ITiledMap r0 = r0.mTiledMap
            r13 = r0
            r0 = r13
            r1 = r27
            r2 = r31
            r3 = r32
            boolean r4 = r0.isTileBlocked(r1, r2, r3)
            if (r4 == 0) goto L_0x0014
            r4 = 0
        L_0x0013:
            return r4
        L_0x0014:
            r0 = r26
            java.util.ArrayList r0 = r0.mOpenNodes
            r14 = r0
            r0 = r26
            java.util.ArrayList r0 = r0.mVisitedNodes
            r15 = r0
            r0 = r26
            org.anddev.andengine.util.path.astar.AStarPathFinder$Node[][] r0 = r0.mNodes
            r16 = r0
            r4 = r16[r30]
            r17 = r4[r29]
            r4 = r16[r32]
            r18 = r4[r31]
            r0 = r26
            org.anddev.andengine.util.path.astar.IAStarHeuristic r0 = r0.mAStarHeuristic
            r19 = r0
            r0 = r26
            boolean r0 = r0.mAllowDiagonalMovement
            r20 = r0
            r0 = r26
            int r0 = r0.mMaxSearchDepth
            r21 = r0
            r4 = 0
            r0 = r4
            r1 = r17
            r1.mCost = r0
            r4 = 0
            r0 = r4
            r1 = r17
            r1.mDepth = r0
            r4 = 0
            r0 = r4
            r1 = r18
            r1.mParent = r0
            r15.clear()
            r14.clear()
            r0 = r14
            r1 = r17
            r0.add(r1)
            r4 = 0
            r5 = r4
        L_0x005e:
            r0 = r5
            r1 = r21
            if (r0 >= r1) goto L_0x0069
            boolean r4 = r14.isEmpty()
            if (r4 == 0) goto L_0x0072
        L_0x0069:
            r0 = r18
            org.anddev.andengine.util.path.astar.AStarPathFinder$Node r0 = r0.mParent
            r4 = r0
            if (r4 != 0) goto L_0x0152
            r4 = 0
            goto L_0x0013
        L_0x0072:
            r4 = 0
            java.lang.Object r4 = r14.remove(r4)
            r0 = r4
            org.anddev.andengine.util.path.astar.AStarPathFinder$Node r0 = (org.anddev.andengine.util.path.astar.AStarPathFinder.Node) r0
            r12 = r0
            r0 = r12
            r1 = r18
            if (r0 == r1) goto L_0x0069
            r15.add(r12)
            r4 = -1
            r22 = r4
            r4 = r5
        L_0x0087:
            r5 = 1
            r0 = r22
            r1 = r5
            if (r0 <= r1) goto L_0x008f
            r5 = r4
            goto L_0x005e
        L_0x008f:
            r5 = -1
            r23 = r5
            r24 = r4
        L_0x0094:
            r4 = 1
            r0 = r23
            r1 = r4
            if (r0 <= r1) goto L_0x00a1
            int r4 = r22 + 1
            r22 = r4
            r4 = r24
            goto L_0x0087
        L_0x00a1:
            if (r22 != 0) goto L_0x00a5
            if (r23 == 0) goto L_0x0174
        L_0x00a5:
            if (r20 != 0) goto L_0x00ab
            if (r22 == 0) goto L_0x00ab
            if (r23 != 0) goto L_0x0174
        L_0x00ab:
            int r4 = r12.mTileColumn
            int r8 = r22 + r4
            int r4 = r12.mTileRow
            int r9 = r23 + r4
            r4 = r26
            r5 = r27
            r6 = r29
            r7 = r30
            boolean r4 = r4.isTileBlocked(r5, r6, r7, r8, r9)
            if (r4 != 0) goto L_0x0174
            float r10 = r12.mCost
            int r6 = r12.mTileColumn
            int r7 = r12.mTileRow
            r4 = r13
            r5 = r27
            float r4 = r4.getStepCost(r5, r6, r7, r8, r9)
            float r4 = r4 + r10
            r5 = r16[r9]
            r25 = r5[r8]
            r13.onTileVisitedByPathFinder(r8, r9)
            r0 = r25
            float r0 = r0.mCost
            r5 = r0
            int r5 = (r4 > r5 ? 1 : (r4 == r5 ? 0 : -1))
            if (r5 >= 0) goto L_0x00fd
            r0 = r14
            r1 = r25
            boolean r5 = r0.contains(r1)
            if (r5 == 0) goto L_0x00ee
            r0 = r14
            r1 = r25
            r0.remove(r1)
        L_0x00ee:
            r0 = r15
            r1 = r25
            boolean r5 = r0.contains(r1)
            if (r5 == 0) goto L_0x00fd
            r0 = r15
            r1 = r25
            r0.remove(r1)
        L_0x00fd:
            r0 = r14
            r1 = r25
            boolean r5 = r0.contains(r1)
            if (r5 != 0) goto L_0x0174
            r0 = r15
            r1 = r25
            boolean r5 = r0.contains(r1)
            if (r5 != 0) goto L_0x0174
            r0 = r4
            r1 = r25
            r1.mCost = r0
            r0 = r25
            float r0 = r0.mCost
            r4 = r0
            r0 = r28
            float r0 = (float) r0
            r5 = r0
            int r4 = (r4 > r5 ? 1 : (r4 == r5 ? 0 : -1))
            if (r4 > 0) goto L_0x0174
            r5 = r19
            r6 = r13
            r7 = r27
            r10 = r31
            r11 = r32
            float r4 = r5.getExpectedRestCost(r6, r7, r8, r9, r10, r11)
            r0 = r4
            r1 = r25
            r1.mExpectedRestCost = r0
            r0 = r25
            r1 = r12
            int r4 = r0.setParent(r1)
            r0 = r24
            r1 = r4
            int r4 = java.lang.Math.max(r0, r1)
            r0 = r14
            r1 = r25
            r0.add(r1)
            java.util.Collections.sort(r14)
        L_0x014a:
            int r5 = r23 + 1
            r23 = r5
            r24 = r4
            goto L_0x0094
        L_0x0152:
            org.anddev.andengine.util.path.Path r4 = new org.anddev.andengine.util.path.Path
            r4.<init>()
            r5 = r16[r32]
            r5 = r5[r31]
        L_0x015b:
            r0 = r5
            r1 = r17
            if (r0 != r1) goto L_0x016a
            r0 = r4
            r1 = r29
            r2 = r30
            r0.prepend(r1, r2)
            goto L_0x0013
        L_0x016a:
            int r6 = r5.mTileColumn
            int r7 = r5.mTileRow
            r4.prepend(r6, r7)
            org.anddev.andengine.util.path.astar.AStarPathFinder$Node r5 = r5.mParent
            goto L_0x015b
        L_0x0174:
            r4 = r24
            goto L_0x014a
        */
        throw new UnsupportedOperationException("Method not decompiled: org.anddev.andengine.util.path.astar.AStarPathFinder.findPath(java.lang.Object, int, int, int, int, int):org.anddev.andengine.util.path.Path");
    }

    /* access modifiers changed from: protected */
    public boolean isTileBlocked(Object obj, int i, int i2, int i3, int i4) {
        if (i3 < 0 || i4 < 0 || i3 >= this.mTiledMap.getTileColumns() || i4 >= this.mTiledMap.getTileRows()) {
            return true;
        }
        if (i == i3 && i2 == i4) {
            return true;
        }
        return this.mTiledMap.isTileBlocked(obj, i3, i4);
    }

    class Node implements Comparable {
        float mCost;
        int mDepth;
        float mExpectedRestCost;
        Node mParent;
        final int mTileColumn;
        final int mTileRow;

        public Node(int i, int i2) {
            this.mTileColumn = i;
            this.mTileRow = i2;
        }

        public int setParent(Node node) {
            this.mDepth = node.mDepth + 1;
            this.mParent = node;
            return this.mDepth;
        }

        public int compareTo(Node node) {
            float f = this.mExpectedRestCost + this.mCost;
            float f2 = node.mExpectedRestCost + node.mCost;
            if (f < f2) {
                return -1;
            }
            if (f > f2) {
                return 1;
            }
            return 0;
        }
    }
}
