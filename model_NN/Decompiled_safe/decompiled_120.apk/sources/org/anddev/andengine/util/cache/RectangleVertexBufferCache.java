package org.anddev.andengine.util.cache;

import org.anddev.andengine.opengl.buffer.BufferObjectManager;
import org.anddev.andengine.opengl.vertex.RectangleVertexBuffer;
import org.anddev.andengine.util.MultiKey;
import org.anddev.andengine.util.MultiKeyHashMap;

public class RectangleVertexBufferCache {
    private final int mDrawType;
    private final MultiKeyHashMap mRectangleVertexBufferCache;

    public RectangleVertexBufferCache() {
        this(35044);
    }

    public RectangleVertexBufferCache(int i) {
        this.mRectangleVertexBufferCache = new MultiKeyHashMap();
        this.mDrawType = i;
    }

    public RectangleVertexBuffer get(int i, int i2) {
        RectangleVertexBuffer rectangleVertexBuffer = (RectangleVertexBuffer) this.mRectangleVertexBufferCache.get((Object[]) new Integer[]{Integer.valueOf(i), Integer.valueOf(i2)});
        return rectangleVertexBuffer != null ? rectangleVertexBuffer : put(i, i2, new RectangleVertexBuffer(this.mDrawType, false));
    }

    public RectangleVertexBuffer put(int i, int i2, RectangleVertexBuffer rectangleVertexBuffer) {
        rectangleVertexBuffer.update((float) i, (float) i2);
        BufferObjectManager.getActiveInstance().loadBufferObject(rectangleVertexBuffer);
        this.mRectangleVertexBufferCache.put(new MultiKey(Integer.valueOf(i), Integer.valueOf(i2)), rectangleVertexBuffer);
        return rectangleVertexBuffer;
    }
}
