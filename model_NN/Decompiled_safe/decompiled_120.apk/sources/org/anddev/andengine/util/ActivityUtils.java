package org.anddev.andengine.util;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.view.Window;
import android.view.WindowManager;
import java.util.concurrent.Callable;
import org.anddev.andengine.opengl.texture.compressed.pvr.PVRTexture;
import org.anddev.andengine.ui.activity.BaseActivity;
import org.anddev.andengine.util.progress.IProgressListener;
import org.anddev.andengine.util.progress.ProgressCallable;

public class ActivityUtils {
    public static void requestFullscreen(Activity activity) {
        Window window = activity.getWindow();
        window.addFlags(PVRTexture.FLAG_BUMPMAP);
        window.clearFlags(PVRTexture.FLAG_TILING);
        window.requestFeature(1);
    }

    public static void setScreenBrightness(Activity activity, float f) {
        Window window = activity.getWindow();
        WindowManager.LayoutParams attributes = window.getAttributes();
        attributes.screenBrightness = f;
        window.setAttributes(attributes);
    }

    public static void keepScreenOn(Activity activity) {
        activity.getWindow().addFlags(128);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.anddev.andengine.util.ActivityUtils.doAsync(android.content.Context, int, int, java.util.concurrent.Callable, org.anddev.andengine.util.Callback, org.anddev.andengine.util.Callback, boolean):void
     arg types: [android.content.Context, int, int, java.util.concurrent.Callable, org.anddev.andengine.util.Callback, ?[OBJECT, ARRAY], int]
     candidates:
      org.anddev.andengine.util.ActivityUtils.doAsync(android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.util.concurrent.Callable, org.anddev.andengine.util.Callback, org.anddev.andengine.util.Callback, boolean):void
      org.anddev.andengine.util.ActivityUtils.doAsync(android.content.Context, int, int, java.util.concurrent.Callable, org.anddev.andengine.util.Callback, org.anddev.andengine.util.Callback, boolean):void */
    public static void doAsync(Context context, int i, int i2, Callable callable, Callback callback) {
        doAsync(context, i, i2, callable, callback, (Callback) null, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.anddev.andengine.util.ActivityUtils.doAsync(android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.util.concurrent.Callable, org.anddev.andengine.util.Callback, org.anddev.andengine.util.Callback, boolean):void
     arg types: [android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.util.concurrent.Callable, org.anddev.andengine.util.Callback, ?[OBJECT, ARRAY], int]
     candidates:
      org.anddev.andengine.util.ActivityUtils.doAsync(android.content.Context, int, int, java.util.concurrent.Callable, org.anddev.andengine.util.Callback, org.anddev.andengine.util.Callback, boolean):void
      org.anddev.andengine.util.ActivityUtils.doAsync(android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.util.concurrent.Callable, org.anddev.andengine.util.Callback, org.anddev.andengine.util.Callback, boolean):void */
    public static void doAsync(Context context, CharSequence charSequence, CharSequence charSequence2, Callable callable, Callback callback) {
        doAsync(context, charSequence, charSequence2, callable, callback, (Callback) null, false);
    }

    public static void doAsync(Context context, int i, int i2, Callable callable, Callback callback, boolean z) {
        doAsync(context, i, i2, callable, callback, (Callback) null, z);
    }

    public static void doAsync(Context context, CharSequence charSequence, CharSequence charSequence2, Callable callable, Callback callback, boolean z) {
        doAsync(context, charSequence, charSequence2, callable, callback, (Callback) null, z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.anddev.andengine.util.ActivityUtils.doAsync(android.content.Context, int, int, java.util.concurrent.Callable, org.anddev.andengine.util.Callback, org.anddev.andengine.util.Callback, boolean):void
     arg types: [android.content.Context, int, int, java.util.concurrent.Callable, org.anddev.andengine.util.Callback, org.anddev.andengine.util.Callback, int]
     candidates:
      org.anddev.andengine.util.ActivityUtils.doAsync(android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.util.concurrent.Callable, org.anddev.andengine.util.Callback, org.anddev.andengine.util.Callback, boolean):void
      org.anddev.andengine.util.ActivityUtils.doAsync(android.content.Context, int, int, java.util.concurrent.Callable, org.anddev.andengine.util.Callback, org.anddev.andengine.util.Callback, boolean):void */
    public static void doAsync(Context context, int i, int i2, Callable callable, Callback callback, Callback callback2) {
        doAsync(context, i, i2, callable, callback, callback2, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.anddev.andengine.util.ActivityUtils.doAsync(android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.util.concurrent.Callable, org.anddev.andengine.util.Callback, org.anddev.andengine.util.Callback, boolean):void
     arg types: [android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.util.concurrent.Callable, org.anddev.andengine.util.Callback, org.anddev.andengine.util.Callback, int]
     candidates:
      org.anddev.andengine.util.ActivityUtils.doAsync(android.content.Context, int, int, java.util.concurrent.Callable, org.anddev.andengine.util.Callback, org.anddev.andengine.util.Callback, boolean):void
      org.anddev.andengine.util.ActivityUtils.doAsync(android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.util.concurrent.Callable, org.anddev.andengine.util.Callback, org.anddev.andengine.util.Callback, boolean):void */
    public static void doAsync(Context context, CharSequence charSequence, CharSequence charSequence2, Callable callable, Callback callback, Callback callback2) {
        doAsync(context, charSequence, charSequence2, callable, callback, callback2, false);
    }

    public static void doAsync(Context context, int i, int i2, Callable callable, Callback callback, Callback callback2, boolean z) {
        doAsync(context, context.getString(i), context.getString(i2), callable, callback, callback2, z);
    }

    public static void doAsync(Context context, CharSequence charSequence, CharSequence charSequence2, Callable callable, Callback callback, Callback callback2, boolean z) {
        final Context context2 = context;
        final CharSequence charSequence3 = charSequence;
        final CharSequence charSequence4 = charSequence2;
        final boolean z2 = z;
        final Callable callable2 = callable;
        final Callback callback3 = callback;
        final Callback callback4 = callback2;
        new AsyncTask() {
            private Exception mException = null;
            private ProgressDialog mPD;

            public void onPreExecute() {
                this.mPD = ProgressDialog.show(context2, charSequence3, charSequence4, true, z2);
                if (z2) {
                    ProgressDialog progressDialog = this.mPD;
                    final Callback callback = callback4;
                    progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        public void onCancel(DialogInterface dialogInterface) {
                            callback.onCallback(new BaseActivity.CancelledException());
                            dialogInterface.dismiss();
                        }
                    });
                }
                super.onPreExecute();
            }

            public Object doInBackground(Void... voidArr) {
                try {
                    return callable2.call();
                } catch (Exception e) {
                    this.mException = e;
                    return null;
                }
            }

            public void onPostExecute(Object obj) {
                try {
                    this.mPD.dismiss();
                } catch (Exception e) {
                    Debug.e("Error", e);
                }
                if (isCancelled()) {
                    this.mException = new BaseActivity.CancelledException();
                }
                if (this.mException == null) {
                    callback3.onCallback(obj);
                } else if (callback4 == null) {
                    Debug.e("Error", this.mException);
                } else {
                    callback4.onCallback(this.mException);
                }
                super.onPostExecute(obj);
            }
        }.execute((Object[]) null);
    }

    public static void doProgressAsync(Context context, int i, ProgressCallable progressCallable, Callback callback) {
        doProgressAsync(context, i, progressCallable, callback, null);
    }

    public static void doProgressAsync(Context context, int i, ProgressCallable progressCallable, Callback callback, Callback callback2) {
        final Context context2 = context;
        final int i2 = i;
        final ProgressCallable progressCallable2 = progressCallable;
        final Callback callback3 = callback;
        final Callback callback4 = callback2;
        new AsyncTask() {
            private Exception mException = null;
            private ProgressDialog mPD;

            public void onPreExecute() {
                this.mPD = new ProgressDialog(context2);
                this.mPD.setTitle(i2);
                this.mPD.setIcon(17301582);
                this.mPD.setIndeterminate(false);
                this.mPD.setProgressStyle(1);
                this.mPD.show();
                super.onPreExecute();
            }

            public Object doInBackground(Void... voidArr) {
                try {
                    return progressCallable2.call(new IProgressListener() {
                        public void onProgressChanged(int i) {
                            AnonymousClass2.this.onProgressUpdate(Integer.valueOf(i));
                        }
                    });
                } catch (Exception e) {
                    this.mException = e;
                    return null;
                }
            }

            public void onProgressUpdate(Integer... numArr) {
                this.mPD.setProgress(numArr[0].intValue());
            }

            public void onPostExecute(Object obj) {
                try {
                    this.mPD.dismiss();
                } catch (Exception e) {
                    Debug.e("Error", e);
                }
                if (isCancelled()) {
                    this.mException = new BaseActivity.CancelledException();
                }
                if (this.mException == null) {
                    callback3.onCallback(obj);
                } else if (callback4 == null) {
                    Debug.e("Error", this.mException);
                } else {
                    callback4.onCallback(this.mException);
                }
                super.onPostExecute(obj);
            }
        }.execute((Object[]) null);
    }

    public static void doAsync(Context context, int i, int i2, AsyncCallable asyncCallable, final Callback callback, Callback callback2) {
        final ProgressDialog show = ProgressDialog.show(context, context.getString(i), context.getString(i2));
        asyncCallable.call(new Callback() {
            public void onCallback(Object obj) {
                try {
                    show.dismiss();
                } catch (Exception e) {
                    Debug.e("Error", e);
                }
                callback.onCallback(obj);
            }
        }, callback2);
    }
}
