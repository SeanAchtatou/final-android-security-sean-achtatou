package org.anddev.andengine.util;

import android.util.SparseArray;

public class Library {
    protected final SparseArray mItems;

    public Library() {
        this.mItems = new SparseArray();
    }

    public Library(int i) {
        this.mItems = new SparseArray(i);
    }

    public void put(int i, Object obj) {
        Object obj2 = this.mItems.get(i);
        if (obj2 == null) {
            this.mItems.put(i, obj);
            return;
        }
        throw new IllegalArgumentException("ID: '" + i + "' is already associated with item: '" + obj2.toString() + "'.");
    }

    public void remove(int i) {
        this.mItems.remove(i);
    }

    public Object get(int i) {
        return this.mItems.get(i);
    }
}
