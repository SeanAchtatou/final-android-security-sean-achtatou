package org.anddev.andengine.util.modifier.ease;

public class EaseQuadOut implements IEaseFunction {
    private static EaseQuadOut INSTANCE;

    private EaseQuadOut() {
    }

    public static EaseQuadOut getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EaseQuadOut();
        }
        return INSTANCE;
    }

    public float getPercentage(float f, float f2) {
        return getValue(f / f2);
    }

    public static float getValue(float f) {
        return (-f) * (f - 2.0f);
    }
}
