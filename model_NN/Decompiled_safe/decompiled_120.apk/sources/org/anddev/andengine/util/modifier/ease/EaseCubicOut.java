package org.anddev.andengine.util.modifier.ease;

public class EaseCubicOut implements IEaseFunction {
    private static EaseCubicOut INSTANCE;

    private EaseCubicOut() {
    }

    public static EaseCubicOut getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EaseCubicOut();
        }
        return INSTANCE;
    }

    public float getPercentage(float f, float f2) {
        return getValue(f / f2);
    }

    public static float getValue(float f) {
        float f2 = f - 1.0f;
        return (f2 * f2 * f2) + 1.0f;
    }
}
