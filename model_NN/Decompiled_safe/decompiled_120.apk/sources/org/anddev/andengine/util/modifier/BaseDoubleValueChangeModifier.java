package org.anddev.andengine.util.modifier;

import org.anddev.andengine.util.modifier.IModifier;

public abstract class BaseDoubleValueChangeModifier extends BaseSingleValueChangeModifier {
    private float mValueChangeBPerSecond;

    /* access modifiers changed from: protected */
    public abstract void onChangeValues(float f, Object obj, float f2, float f3);

    public BaseDoubleValueChangeModifier(float f, float f2, float f3) {
        this(f, f2, f3, null);
    }

    public BaseDoubleValueChangeModifier(float f, float f2, float f3, IModifier.IModifierListener iModifierListener) {
        super(f, f2, iModifierListener);
        this.mValueChangeBPerSecond = f3 / f;
    }

    protected BaseDoubleValueChangeModifier(BaseDoubleValueChangeModifier baseDoubleValueChangeModifier) {
        super(baseDoubleValueChangeModifier);
        this.mValueChangeBPerSecond = baseDoubleValueChangeModifier.mValueChangeBPerSecond;
    }

    /* access modifiers changed from: protected */
    public void onChangeValue(float f, Object obj, float f2) {
        onChangeValues(f, obj, f2, this.mValueChangeBPerSecond * f);
    }
}
