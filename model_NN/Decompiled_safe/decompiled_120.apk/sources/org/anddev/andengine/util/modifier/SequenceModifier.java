package org.anddev.andengine.util.modifier;

import org.anddev.andengine.util.modifier.IModifier;
import org.anddev.andengine.util.modifier.util.ModifierUtils;

public class SequenceModifier extends BaseModifier implements IModifier.IModifierListener {
    private int mCurrentSubSequenceModifierIndex;
    private final float mDuration;
    private boolean mFinishedCached;
    private float mSecondsElapsed;
    private ISubSequenceModifierListener mSubSequenceModifierListener;
    private final IModifier[] mSubSequenceModifiers;

    public interface ISubSequenceModifierListener {
        void onSubSequenceFinished(IModifier iModifier, Object obj, int i);

        void onSubSequenceStarted(IModifier iModifier, Object obj, int i);
    }

    public SequenceModifier(IModifier... iModifierArr) {
        this(null, null, iModifierArr);
    }

    public SequenceModifier(ISubSequenceModifierListener iSubSequenceModifierListener, IModifier... iModifierArr) {
        this(iSubSequenceModifierListener, null, iModifierArr);
    }

    public SequenceModifier(IModifier.IModifierListener iModifierListener, IModifier... iModifierArr) {
        this(null, iModifierListener, iModifierArr);
    }

    public SequenceModifier(ISubSequenceModifierListener iSubSequenceModifierListener, IModifier.IModifierListener iModifierListener, IModifier... iModifierArr) {
        super(iModifierListener);
        if (iModifierArr.length == 0) {
            throw new IllegalArgumentException("pModifiers must not be empty!");
        }
        this.mSubSequenceModifierListener = iSubSequenceModifierListener;
        this.mSubSequenceModifiers = iModifierArr;
        this.mDuration = ModifierUtils.getSequenceDurationOfModifier(iModifierArr);
        iModifierArr[0].addModifierListener(this);
    }

    protected SequenceModifier(SequenceModifier sequenceModifier) {
        this.mDuration = sequenceModifier.mDuration;
        IModifier[] iModifierArr = sequenceModifier.mSubSequenceModifiers;
        this.mSubSequenceModifiers = new IModifier[iModifierArr.length];
        IModifier[] iModifierArr2 = this.mSubSequenceModifiers;
        for (int length = iModifierArr2.length - 1; length >= 0; length--) {
            iModifierArr2[length] = iModifierArr[length].clone();
        }
        iModifierArr2[0].addModifierListener(this);
    }

    public SequenceModifier clone() {
        return new SequenceModifier(this);
    }

    public ISubSequenceModifierListener getSubSequenceModifierListener() {
        return this.mSubSequenceModifierListener;
    }

    public void setSubSequenceModifierListener(ISubSequenceModifierListener iSubSequenceModifierListener) {
        this.mSubSequenceModifierListener = iSubSequenceModifierListener;
    }

    public float getSecondsElapsed() {
        return this.mSecondsElapsed;
    }

    public float getDuration() {
        return this.mDuration;
    }

    public float onUpdate(float f, Object obj) {
        if (this.mFinished) {
            return 0.0f;
        }
        this.mFinishedCached = false;
        float f2 = f;
        while (f2 > 0.0f && !this.mFinishedCached) {
            f2 -= this.mSubSequenceModifiers[this.mCurrentSubSequenceModifierIndex].onUpdate(f2, obj);
        }
        this.mFinishedCached = false;
        float f3 = f - f2;
        this.mSecondsElapsed += f3;
        return f3;
    }

    public void reset() {
        if (isFinished()) {
            this.mSubSequenceModifiers[this.mSubSequenceModifiers.length - 1].removeModifierListener(this);
        } else {
            this.mSubSequenceModifiers[this.mCurrentSubSequenceModifierIndex].removeModifierListener(this);
        }
        this.mCurrentSubSequenceModifierIndex = 0;
        this.mFinished = false;
        this.mSecondsElapsed = 0.0f;
        this.mSubSequenceModifiers[0].addModifierListener(this);
        IModifier[] iModifierArr = this.mSubSequenceModifiers;
        for (int length = iModifierArr.length - 1; length >= 0; length--) {
            iModifierArr[length].reset();
        }
    }

    public void onModifierStarted(IModifier iModifier, Object obj) {
        if (this.mCurrentSubSequenceModifierIndex == 0) {
            onModifierStarted(obj);
        }
        if (this.mSubSequenceModifierListener != null) {
            this.mSubSequenceModifierListener.onSubSequenceStarted(iModifier, obj, this.mCurrentSubSequenceModifierIndex);
        }
    }

    public void onModifierFinished(IModifier iModifier, Object obj) {
        if (this.mSubSequenceModifierListener != null) {
            this.mSubSequenceModifierListener.onSubSequenceFinished(iModifier, obj, this.mCurrentSubSequenceModifierIndex);
        }
        iModifier.removeModifierListener(this);
        this.mCurrentSubSequenceModifierIndex++;
        if (this.mCurrentSubSequenceModifierIndex < this.mSubSequenceModifiers.length) {
            this.mSubSequenceModifiers[this.mCurrentSubSequenceModifierIndex].addModifierListener(this);
            return;
        }
        this.mFinished = true;
        this.mFinishedCached = true;
        onModifierFinished(obj);
    }
}
