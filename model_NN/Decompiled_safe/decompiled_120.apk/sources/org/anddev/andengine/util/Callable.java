package org.anddev.andengine.util;

public interface Callable {
    Object call();
}
