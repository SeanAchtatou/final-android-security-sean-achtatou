package org.anddev.andengine.util.sort;

import java.util.Comparator;
import java.util.List;

public abstract class Sorter {
    public abstract void sort(List list, int i, int i2, Comparator comparator);

    public abstract void sort(Object[] objArr, int i, int i2, Comparator comparator);

    public final void sort(Object[] objArr, Comparator comparator) {
        sort(objArr, 0, objArr.length, comparator);
    }

    public final void sort(List list, Comparator comparator) {
        sort(list, 0, list.size(), comparator);
    }
}
