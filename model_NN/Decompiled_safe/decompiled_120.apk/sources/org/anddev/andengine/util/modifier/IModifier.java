package org.anddev.andengine.util.modifier;

import java.util.Comparator;

public interface IModifier extends Cloneable {
    public static final Comparator MODIFIER_COMPARATOR_DURATION_DESCENDING = new Comparator() {
        public int compare(IModifier iModifier, IModifier iModifier2) {
            float duration = iModifier.getDuration();
            float duration2 = iModifier2.getDuration();
            if (duration < duration2) {
                return 1;
            }
            if (duration > duration2) {
                return -1;
            }
            return 0;
        }
    };

    public class CloneNotSupportedException extends RuntimeException {
        private static final long serialVersionUID = -5838035434002587320L;
    }

    public interface IModifierListener {
        void onModifierFinished(IModifier iModifier, Object obj);

        void onModifierStarted(IModifier iModifier, Object obj);
    }

    void addModifierListener(IModifierListener iModifierListener);

    IModifier clone();

    float getDuration();

    float getSecondsElapsed();

    boolean isFinished();

    boolean isRemoveWhenFinished();

    float onUpdate(float f, Object obj);

    boolean removeModifierListener(IModifierListener iModifierListener);

    void reset();

    void setRemoveWhenFinished(boolean z);
}
