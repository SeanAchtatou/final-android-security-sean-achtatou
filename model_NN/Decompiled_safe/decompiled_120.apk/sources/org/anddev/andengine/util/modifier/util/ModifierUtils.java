package org.anddev.andengine.util.modifier.util;

import org.anddev.andengine.util.modifier.IModifier;

public class ModifierUtils {
    public static float getSequenceDurationOfModifier(IModifier[] iModifierArr) {
        float f = Float.MIN_VALUE;
        for (int length = iModifierArr.length - 1; length >= 0; length--) {
            f += iModifierArr[length].getDuration();
        }
        return f;
    }
}
