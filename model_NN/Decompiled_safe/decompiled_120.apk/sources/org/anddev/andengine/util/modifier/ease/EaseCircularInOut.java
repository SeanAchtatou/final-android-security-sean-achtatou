package org.anddev.andengine.util.modifier.ease;

public class EaseCircularInOut implements IEaseFunction {
    private static EaseCircularInOut INSTANCE;

    private EaseCircularInOut() {
    }

    public static EaseCircularInOut getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EaseCircularInOut();
        }
        return INSTANCE;
    }

    public float getPercentage(float f, float f2) {
        float f3 = f / f2;
        if (f3 < 0.5f) {
            return EaseCircularIn.getValue(f3 * 2.0f) * 0.5f;
        }
        return (EaseCircularOut.getValue((f3 * 2.0f) - 1.0f) * 0.5f) + 0.5f;
    }
}
