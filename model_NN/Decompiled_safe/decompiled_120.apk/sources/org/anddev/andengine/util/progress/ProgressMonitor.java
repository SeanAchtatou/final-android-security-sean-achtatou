package org.anddev.andengine.util.progress;

public class ProgressMonitor implements IProgressListener {
    private final IProgressListener mListener;
    private final ProgressMonitor mParentProgressMonitor;
    private int mProgress;
    private int mSubMonitorRangeFrom;
    private int mSubMonitorRangeTo;

    public ProgressMonitor(IProgressListener iProgressListener) {
        this.mSubMonitorRangeFrom = 0;
        this.mSubMonitorRangeTo = 100;
        this.mProgress = 0;
        this.mListener = iProgressListener;
        this.mParentProgressMonitor = null;
    }

    public ProgressMonitor(ProgressMonitor progressMonitor) {
        this.mSubMonitorRangeFrom = 0;
        this.mSubMonitorRangeTo = 100;
        this.mProgress = 0;
        this.mListener = null;
        this.mParentProgressMonitor = progressMonitor;
    }

    public ProgressMonitor getParentProgressMonitor() {
        return this.mParentProgressMonitor;
    }

    public int getProgress() {
        return this.mProgress;
    }

    public void setSubMonitorRange(int i, int i2) {
        this.mSubMonitorRangeFrom = i;
        this.mSubMonitorRangeTo = i2;
    }

    public void onProgressChanged(int i) {
        this.mProgress = i;
        if (this.mParentProgressMonitor != null) {
            this.mParentProgressMonitor.onSubProgressChanged(i);
        } else {
            this.mListener.onProgressChanged(i);
        }
    }

    private void onSubProgressChanged(int i) {
        int i2 = i;
        ProgressMonitor progressMonitor = this;
        while (true) {
            i2 = ((int) (((float) (i2 * (progressMonitor.mSubMonitorRangeTo - progressMonitor.mSubMonitorRangeFrom))) / 100.0f)) + progressMonitor.mSubMonitorRangeFrom;
            if (progressMonitor.mParentProgressMonitor != null) {
                progressMonitor = progressMonitor.mParentProgressMonitor;
            } else {
                progressMonitor.mListener.onProgressChanged(i2);
                return;
            }
        }
    }
}
