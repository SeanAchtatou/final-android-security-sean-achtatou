package org.anddev.andengine.util.path;

public enum Direction {
    UP(0, -1),
    DOWN(0, 1),
    LEFT(-1, 0),
    RIGHT(1, 0);
    
    private final int mDeltaX;
    private final int mDeltaY;

    private Direction(int i, int i2) {
        this.mDeltaX = i;
        this.mDeltaY = i2;
    }

    public static Direction fromDelta(int i, int i2) {
        if (i == 0) {
            if (i2 == 1) {
                return DOWN;
            }
            if (i2 == -1) {
                return UP;
            }
        } else if (i2 == 0) {
            if (i == 1) {
                return RIGHT;
            }
            if (i == -1) {
                return LEFT;
            }
        }
        throw new IllegalArgumentException();
    }

    public final int getDeltaX() {
        return this.mDeltaX;
    }

    public final int getDeltaY() {
        return this.mDeltaY;
    }
}
