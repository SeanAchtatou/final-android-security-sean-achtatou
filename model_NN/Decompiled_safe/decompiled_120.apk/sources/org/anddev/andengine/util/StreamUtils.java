package org.anddev.andengine.util;

import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Writer;
import java.util.Scanner;

public class StreamUtils {
    public static final int IO_BUFFER_SIZE = 8192;

    public static final String readFully(InputStream inputStream) {
        StringBuilder sb = new StringBuilder();
        Scanner scanner = new Scanner(inputStream);
        while (scanner.hasNextLine()) {
            sb.append(scanner.nextLine());
        }
        return sb.toString();
    }

    public static byte[] streamToBytes(InputStream inputStream) {
        return streamToBytes(inputStream, -1);
    }

    public static byte[] streamToBytes(InputStream inputStream, int i) {
        int i2;
        if (i == -1) {
            i2 = 8192;
        } else {
            i2 = i;
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(i2);
        copy(inputStream, byteArrayOutputStream, (long) i);
        return byteArrayOutputStream.toByteArray();
    }

    public static void copy(InputStream inputStream, OutputStream outputStream) {
        copy(inputStream, outputStream, -1);
    }

    public static boolean copyAndClose(InputStream inputStream, OutputStream outputStream) {
        try {
            copy(inputStream, outputStream, -1);
            close(inputStream);
            close(outputStream);
            return true;
        } catch (IOException e) {
            close(inputStream);
            close(outputStream);
            return false;
        } catch (Throwable th) {
            close(inputStream);
            close(outputStream);
            throw th;
        }
    }

    public static void copy(InputStream inputStream, OutputStream outputStream, long j) {
        if (j >= 0) {
            byte[] bArr = new byte[8192];
            int min = Math.min((int) j, 8192);
            long j2 = j;
            while (true) {
                int read = inputStream.read(bArr, 0, min);
                if (read != -1) {
                    if (j2 <= ((long) read)) {
                        outputStream.write(bArr, 0, (int) j2);
                        break;
                    } else {
                        outputStream.write(bArr, 0, read);
                        j2 -= (long) read;
                    }
                } else {
                    break;
                }
            }
        } else {
            byte[] bArr2 = new byte[8192];
            while (true) {
                int read2 = inputStream.read(bArr2);
                if (read2 == -1) {
                    break;
                }
                outputStream.write(bArr2, 0, read2);
            }
        }
        outputStream.flush();
    }

    public static void close(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void flushCloseStream(OutputStream outputStream) {
        if (outputStream != null) {
            try {
                outputStream.flush();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                close(outputStream);
            }
        }
    }

    public static void flushCloseWriter(Writer writer) {
        if (writer != null) {
            try {
                writer.flush();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                close(writer);
            }
        }
    }
}
