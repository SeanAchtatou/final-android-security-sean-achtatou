package org.anddev.andengine.util;

import java.util.HashMap;
import java.util.Map;

public class MultiKeyHashMap extends HashMap {
    private static final long serialVersionUID = -6262447639526561122L;

    public Object get(Object... objArr) {
        int hash = MultiKey.hash(objArr);
        for (Map.Entry entry : entrySet()) {
            MultiKey multiKey = (MultiKey) entry.getKey();
            if (multiKey.hashCode() == hash && isEqualKey(multiKey.getKeys(), objArr)) {
                return entry.getValue();
            }
        }
        return null;
    }

    private boolean isEqualKey(Object[] objArr, Object[] objArr2) {
        if (objArr.length != objArr2.length) {
            return false;
        }
        for (int i = 0; i < objArr.length; i++) {
            Object obj = objArr[i];
            Object obj2 = objArr2[i];
            if (obj == null) {
                if (obj2 != null) {
                    return false;
                }
            } else if (!obj.equals(obj2)) {
                return false;
            }
        }
        return true;
    }
}
