package org.anddev.andengine.util.modifier.ease;

public class EaseQuintInOut implements IEaseFunction {
    private static EaseQuintInOut INSTANCE;

    private EaseQuintInOut() {
    }

    public static EaseQuintInOut getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EaseQuintInOut();
        }
        return INSTANCE;
    }

    public float getPercentage(float f, float f2) {
        float f3 = f / f2;
        if (f3 < 0.5f) {
            return EaseQuintIn.getValue(f3 * 2.0f) * 0.5f;
        }
        return (EaseQuintOut.getValue((f3 * 2.0f) - 1.0f) * 0.5f) + 0.5f;
    }
}
