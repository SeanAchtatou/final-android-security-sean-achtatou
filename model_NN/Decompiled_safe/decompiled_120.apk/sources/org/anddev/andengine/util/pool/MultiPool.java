package org.anddev.andengine.util.pool;

import android.util.SparseArray;

public class MultiPool {
    private final SparseArray mPools = new SparseArray();

    public void registerPool(int i, GenericPool genericPool) {
        this.mPools.put(i, genericPool);
    }

    public Object obtainPoolItem(int i) {
        GenericPool genericPool = (GenericPool) this.mPools.get(i);
        if (genericPool == null) {
            return null;
        }
        return genericPool.obtainPoolItem();
    }

    public void recyclePoolItem(int i, Object obj) {
        GenericPool genericPool = (GenericPool) this.mPools.get(i);
        if (genericPool != null) {
            genericPool.recyclePoolItem(obj);
        }
    }
}
