package org.anddev.andengine.util.modifier.ease;

public class EaseCubicIn implements IEaseFunction {
    private static EaseCubicIn INSTANCE;

    private EaseCubicIn() {
    }

    public static EaseCubicIn getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EaseCubicIn();
        }
        return INSTANCE;
    }

    public float getPercentage(float f, float f2) {
        return getValue(f / f2);
    }

    public static float getValue(float f) {
        return f * f * f;
    }
}
