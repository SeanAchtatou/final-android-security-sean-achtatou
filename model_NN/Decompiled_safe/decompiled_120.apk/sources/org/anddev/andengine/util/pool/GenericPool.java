package org.anddev.andengine.util.pool;

import java.util.Collections;
import java.util.Stack;
import org.anddev.andengine.util.Debug;

public abstract class GenericPool {
    private final Stack mAvailableItems;
    private final int mGrowth;
    private int mUnrecycledCount;

    /* access modifiers changed from: protected */
    public abstract Object onAllocatePoolItem();

    public GenericPool() {
        this(0);
    }

    public GenericPool(int i) {
        this(i, 1);
    }

    public GenericPool(int i, int i2) {
        this.mAvailableItems = new Stack();
        if (i2 < 0) {
            throw new IllegalArgumentException("pGrowth must be at least 0!");
        }
        this.mGrowth = i2;
        if (i > 0) {
            batchAllocatePoolItems(i);
        }
    }

    public synchronized int getUnrecycledCount() {
        return this.mUnrecycledCount;
    }

    /* access modifiers changed from: protected */
    public void onHandleRecycleItem(Object obj) {
    }

    /* access modifiers changed from: protected */
    public Object onHandleAllocatePoolItem() {
        return onAllocatePoolItem();
    }

    /* access modifiers changed from: protected */
    public void onHandleObtainItem(Object obj) {
    }

    public synchronized void batchAllocatePoolItems(int i) {
        Stack stack = this.mAvailableItems;
        for (int i2 = i - 1; i2 >= 0; i2--) {
            stack.push(onHandleAllocatePoolItem());
        }
    }

    public synchronized Object obtainPoolItem() {
        Object pop;
        if (this.mAvailableItems.size() > 0) {
            pop = this.mAvailableItems.pop();
        } else {
            if (this.mGrowth == 1) {
                pop = onHandleAllocatePoolItem();
            } else {
                batchAllocatePoolItems(this.mGrowth);
                pop = this.mAvailableItems.pop();
            }
            Debug.i(String.valueOf(getClass().getName()) + "<" + pop.getClass().getSimpleName() + "> was exhausted, with " + this.mUnrecycledCount + " item not yet recycled. Allocated " + this.mGrowth + " more.");
        }
        onHandleObtainItem(pop);
        this.mUnrecycledCount++;
        return pop;
    }

    public synchronized void recyclePoolItem(Object obj) {
        if (obj == null) {
            throw new IllegalArgumentException("Cannot recycle null item!");
        }
        onHandleRecycleItem(obj);
        this.mAvailableItems.push(obj);
        this.mUnrecycledCount--;
        if (this.mUnrecycledCount < 0) {
            Debug.e("More items recycled than obtained!");
        }
    }

    public synchronized void shufflePoolItems() {
        Collections.shuffle(this.mAvailableItems);
    }
}
