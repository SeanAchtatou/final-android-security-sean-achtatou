package org.anddev.andengine.util.pool;

public abstract class PoolItem {
    Pool mParent;
    boolean mRecycled = true;

    public Pool getParent() {
        return this.mParent;
    }

    public boolean isRecycled() {
        return this.mRecycled;
    }

    public boolean isFromPool(Pool pool) {
        return pool == this.mParent;
    }

    /* access modifiers changed from: protected */
    public void onRecycle() {
    }

    /* access modifiers changed from: protected */
    public void onObtain() {
    }

    public void recycle() {
        if (this.mParent == null) {
            throw new IllegalStateException("Item already recycled!");
        }
        this.mParent.recycle(this);
    }
}
