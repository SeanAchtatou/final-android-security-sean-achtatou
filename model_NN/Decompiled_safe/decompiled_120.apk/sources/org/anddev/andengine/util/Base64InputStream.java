package org.anddev.andengine.util;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import org.anddev.andengine.util.Base64;

public class Base64InputStream extends FilterInputStream {
    private static final int BUFFER_SIZE = 2048;
    private static byte[] EMPTY = new byte[0];
    private final Base64.Coder coder;
    private boolean eof;
    private byte[] inputBuffer;
    private int outputEnd;
    private int outputStart;

    public Base64InputStream(InputStream inputStream, int i) {
        this(inputStream, i, false);
    }

    public Base64InputStream(InputStream inputStream, int i, boolean z) {
        super(inputStream);
        this.eof = false;
        this.inputBuffer = new byte[2048];
        if (z) {
            this.coder = new Base64.Encoder(i, null);
        } else {
            this.coder = new Base64.Decoder(i, null);
        }
        this.coder.output = new byte[this.coder.maxOutputSize(2048)];
        this.outputStart = 0;
        this.outputEnd = 0;
    }

    public boolean markSupported() {
        return false;
    }

    public void mark(int i) {
        throw new UnsupportedOperationException();
    }

    public void reset() {
        throw new UnsupportedOperationException();
    }

    public void close() {
        this.in.close();
        this.inputBuffer = null;
    }

    public int available() {
        return this.outputEnd - this.outputStart;
    }

    public long skip(long j) {
        if (this.outputStart >= this.outputEnd) {
            refill();
        }
        if (this.outputStart >= this.outputEnd) {
            return 0;
        }
        long min = Math.min(j, (long) (this.outputEnd - this.outputStart));
        this.outputStart = (int) (((long) this.outputStart) + min);
        return min;
    }

    public int read() {
        if (this.outputStart >= this.outputEnd) {
            refill();
        }
        if (this.outputStart >= this.outputEnd) {
            return -1;
        }
        byte[] bArr = this.coder.output;
        int i = this.outputStart;
        this.outputStart = i + 1;
        return bArr[i];
    }

    public int read(byte[] bArr, int i, int i2) {
        if (this.outputStart >= this.outputEnd) {
            refill();
        }
        if (this.outputStart >= this.outputEnd) {
            return -1;
        }
        int min = Math.min(i2, this.outputEnd - this.outputStart);
        System.arraycopy(this.coder.output, this.outputStart, bArr, i, min);
        this.outputStart += min;
        return min;
    }

    private void refill() {
        boolean process;
        if (!this.eof) {
            int read = this.in.read(this.inputBuffer);
            if (read == -1) {
                this.eof = true;
                process = this.coder.process(EMPTY, 0, 0, true);
            } else {
                process = this.coder.process(this.inputBuffer, 0, read, false);
            }
            if (!process) {
                throw new IOException("bad base-64");
            }
            this.outputEnd = this.coder.op;
            this.outputStart = 0;
        }
    }
}
