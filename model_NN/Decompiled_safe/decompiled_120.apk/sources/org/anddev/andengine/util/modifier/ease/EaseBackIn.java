package org.anddev.andengine.util.modifier.ease;

public class EaseBackIn implements IEaseFunction {
    private static EaseBackIn INSTANCE = null;
    private static final float OVERSHOOT_CONSTANT = 1.70158f;

    private EaseBackIn() {
    }

    public static EaseBackIn getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EaseBackIn();
        }
        return INSTANCE;
    }

    public float getPercentage(float f, float f2) {
        return getValue(f / f2);
    }

    public static float getValue(float f) {
        return f * f * ((2.70158f * f) - OVERSHOOT_CONSTANT);
    }
}
