package org.anddev.andengine.util;

import android.util.Log;
import org.anddev.andengine.util.constants.Constants;

public class Debug implements Constants {
    private static DebugLevel sDebugLevel = DebugLevel.VERBOSE;
    private static String sDebugTag = Constants.DEBUGTAG;

    public static String getDebugTag() {
        return sDebugTag;
    }

    public static void setDebugTag(String str) {
        sDebugTag = str;
    }

    public static DebugLevel getDebugLevel() {
        return sDebugLevel;
    }

    public static void setDebugLevel(DebugLevel debugLevel) {
        if (debugLevel == null) {
            throw new IllegalArgumentException("pDebugLevel must not be null!");
        }
        sDebugLevel = debugLevel;
    }

    public static void v(String str) {
        v(str, null);
    }

    public static void v(String str, Throwable th) {
        if (DebugLevel.access$2(sDebugLevel, DebugLevel.VERBOSE)) {
            Log.v(sDebugTag, str, th);
        }
    }

    public static void d(String str) {
        d(str, null);
    }

    public static void d(String str, Throwable th) {
        if (DebugLevel.access$2(sDebugLevel, DebugLevel.DEBUG)) {
            Log.d(sDebugTag, str, th);
        }
    }

    public static void i(String str) {
        i(str, null);
    }

    public static void i(String str, Throwable th) {
        if (DebugLevel.access$2(sDebugLevel, DebugLevel.INFO)) {
            Log.i(sDebugTag, str, th);
        }
    }

    public static void w(String str) {
        w(str, null);
    }

    public static void w(Throwable th) {
        w("", th);
    }

    public static void w(String str, Throwable th) {
        if (!DebugLevel.access$2(sDebugLevel, DebugLevel.WARNING)) {
            return;
        }
        if (th == null) {
            Log.w(sDebugTag, str, new Exception());
        } else {
            Log.w(sDebugTag, str, th);
        }
    }

    public static void e(String str) {
        e(str, null);
    }

    public static void e(Throwable th) {
        e(sDebugTag, th);
    }

    public static void e(String str, Throwable th) {
        if (!DebugLevel.access$2(sDebugLevel, DebugLevel.ERROR)) {
            return;
        }
        if (th == null) {
            Log.e(sDebugTag, str, new Exception());
        } else {
            Log.e(sDebugTag, str, th);
        }
    }

    public enum DebugLevel implements Comparable {
        NONE,
        ERROR,
        WARNING,
        INFO,
        DEBUG,
        VERBOSE;
        
        public static DebugLevel ALL = VERBOSE;

        private boolean isSameOrLessThan(DebugLevel debugLevel) {
            return compareTo(debugLevel) >= 0;
        }
    }
}
