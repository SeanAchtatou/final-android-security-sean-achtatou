package org.anddev.andengine.util.modifier.ease;

public class EaseQuadInOut implements IEaseFunction {
    private static EaseQuadInOut INSTANCE;

    private EaseQuadInOut() {
    }

    public static EaseQuadInOut getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EaseQuadInOut();
        }
        return INSTANCE;
    }

    public float getPercentage(float f, float f2) {
        float f3 = f / f2;
        if (f3 < 0.5f) {
            return EaseQuadIn.getValue(f3 * 2.0f) * 0.5f;
        }
        return (EaseQuadOut.getValue((f3 * 2.0f) - 1.0f) * 0.5f) + 0.5f;
    }
}
