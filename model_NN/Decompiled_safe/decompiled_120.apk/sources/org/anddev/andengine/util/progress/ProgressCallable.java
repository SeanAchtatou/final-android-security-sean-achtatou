package org.anddev.andengine.util.progress;

public interface ProgressCallable {
    Object call(IProgressListener iProgressListener);
}
