package org.anddev.andengine.util;

public interface ParameterCallable {
    void call(Object obj);
}
