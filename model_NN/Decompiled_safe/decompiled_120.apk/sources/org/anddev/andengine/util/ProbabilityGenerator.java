package org.anddev.andengine.util;

import java.util.ArrayList;

public class ProbabilityGenerator {
    private final ArrayList mEntries = new ArrayList();
    private float mProbabilitySum;

    public void add(float f, Object... objArr) {
        this.mProbabilitySum += f;
        this.mEntries.add(new Entry(f, objArr));
    }

    public Object next() {
        float random = MathUtils.random(0.0f, this.mProbabilitySum);
        ArrayList arrayList = this.mEntries;
        float f = random;
        for (int size = arrayList.size() - 1; size >= 0; size--) {
            Entry entry = (Entry) arrayList.get(size);
            f -= entry.mFactor;
            if (f <= 0.0f) {
                return entry.getReturnValue();
            }
        }
        return ((Entry) arrayList.get(arrayList.size() - 1)).getReturnValue();
    }

    public void clear() {
        this.mProbabilitySum = 0.0f;
        this.mEntries.clear();
    }

    class Entry {
        public final Object[] mData;
        public final float mFactor;

        public Entry(float f, Object[] objArr) {
            this.mFactor = f;
            this.mData = objArr;
        }

        public Object getReturnValue() {
            if (this.mData.length == 1) {
                return this.mData[0];
            }
            return ArrayUtils.random(this.mData);
        }
    }
}
