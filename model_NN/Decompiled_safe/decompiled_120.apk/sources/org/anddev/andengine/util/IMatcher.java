package org.anddev.andengine.util;

public interface IMatcher {
    boolean matches(Object obj);
}
