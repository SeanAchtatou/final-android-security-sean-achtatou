package org.anddev.andengine.util.modifier.ease;

public class EaseQuadIn implements IEaseFunction {
    private static EaseQuadIn INSTANCE;

    private EaseQuadIn() {
    }

    public static EaseQuadIn getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EaseQuadIn();
        }
        return INSTANCE;
    }

    public float getPercentage(float f, float f2) {
        return getValue(f / f2);
    }

    public static float getValue(float f) {
        return f * f;
    }
}
