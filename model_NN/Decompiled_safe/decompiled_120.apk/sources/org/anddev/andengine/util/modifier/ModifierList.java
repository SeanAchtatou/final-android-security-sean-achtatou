package org.anddev.andengine.util.modifier;

import org.anddev.andengine.engine.handler.IUpdateHandler;
import org.anddev.andengine.util.SmartList;

public class ModifierList extends SmartList implements IUpdateHandler {
    private static final long serialVersionUID = 1610345592534873475L;
    private final Object mTarget;

    public ModifierList(Object obj) {
        this.mTarget = obj;
    }

    public ModifierList(Object obj, int i) {
        super(i);
        this.mTarget = obj;
    }

    public Object getTarget() {
        return this.mTarget;
    }

    public void onUpdate(float f) {
        int size = size();
        if (size > 0) {
            for (int i = size - 1; i >= 0; i--) {
                IModifier iModifier = (IModifier) get(i);
                iModifier.onUpdate(f, this.mTarget);
                if (iModifier.isFinished() && iModifier.isRemoveWhenFinished()) {
                    remove(i);
                }
            }
        }
    }

    public void reset() {
        for (int size = size() - 1; size >= 0; size--) {
            ((IModifier) get(size)).reset();
        }
    }
}
