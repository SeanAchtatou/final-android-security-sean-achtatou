package org.anddev.andengine.util.modifier.ease;

public class EaseBackOut implements IEaseFunction {
    private static EaseBackOut INSTANCE;

    private EaseBackOut() {
    }

    public static EaseBackOut getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EaseBackOut();
        }
        return INSTANCE;
    }

    public float getPercentage(float f, float f2) {
        return getValue(f / f2);
    }

    public static float getValue(float f) {
        float f2 = f - 1.0f;
        return (((f2 * 2.70158f) + 1.70158f) * f2 * f2) + 1.0f;
    }
}
