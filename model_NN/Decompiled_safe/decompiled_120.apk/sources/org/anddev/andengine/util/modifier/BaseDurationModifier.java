package org.anddev.andengine.util.modifier;

import org.anddev.andengine.util.modifier.IModifier;

public abstract class BaseDurationModifier extends BaseModifier {
    protected final float mDuration;
    private float mSecondsElapsed;

    /* access modifiers changed from: protected */
    public abstract void onManagedInitialize(Object obj);

    /* access modifiers changed from: protected */
    public abstract void onManagedUpdate(float f, Object obj);

    public BaseDurationModifier(float f) {
        this.mDuration = f;
    }

    public BaseDurationModifier(float f, IModifier.IModifierListener iModifierListener) {
        super(iModifierListener);
        this.mDuration = f;
    }

    protected BaseDurationModifier(BaseDurationModifier baseDurationModifier) {
        this(baseDurationModifier.mDuration);
    }

    public float getSecondsElapsed() {
        return this.mSecondsElapsed;
    }

    public float getDuration() {
        return this.mDuration;
    }

    public final float onUpdate(float f, Object obj) {
        float f2;
        if (this.mFinished) {
            return 0.0f;
        }
        if (this.mSecondsElapsed == 0.0f) {
            onManagedInitialize(obj);
            onModifierStarted(obj);
        }
        if (this.mSecondsElapsed + f >= this.mDuration) {
            f2 = this.mDuration - this.mSecondsElapsed;
        } else {
            f2 = f;
        }
        this.mSecondsElapsed += f2;
        onManagedUpdate(f2, obj);
        if (this.mDuration == -1.0f || this.mSecondsElapsed < this.mDuration) {
            return f2;
        }
        this.mSecondsElapsed = this.mDuration;
        this.mFinished = true;
        onModifierFinished(obj);
        return f2;
    }

    public void reset() {
        this.mFinished = false;
        this.mSecondsElapsed = 0.0f;
    }
}
