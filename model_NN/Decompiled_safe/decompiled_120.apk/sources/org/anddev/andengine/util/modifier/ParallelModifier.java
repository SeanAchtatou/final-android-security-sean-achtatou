package org.anddev.andengine.util.modifier;

import java.util.Arrays;
import org.anddev.andengine.util.modifier.IModifier;

public class ParallelModifier extends BaseModifier implements IModifier.IModifierListener {
    private final float mDuration;
    private boolean mFinishedCached;
    private final IModifier[] mModifiers;
    private float mSecondsElapsed;

    public ParallelModifier(IModifier... iModifierArr) {
        this(null, iModifierArr);
    }

    public ParallelModifier(IModifier.IModifierListener iModifierListener, IModifier... iModifierArr) {
        super(iModifierListener);
        if (iModifierArr.length == 0) {
            throw new IllegalArgumentException("pModifiers must not be empty!");
        }
        Arrays.sort(iModifierArr, MODIFIER_COMPARATOR_DURATION_DESCENDING);
        this.mModifiers = iModifierArr;
        IModifier iModifier = iModifierArr[0];
        this.mDuration = iModifier.getDuration();
        iModifier.addModifierListener(this);
    }

    protected ParallelModifier(ParallelModifier parallelModifier) {
        IModifier[] iModifierArr = parallelModifier.mModifiers;
        this.mModifiers = new IModifier[iModifierArr.length];
        IModifier[] iModifierArr2 = this.mModifiers;
        for (int length = iModifierArr2.length - 1; length >= 0; length--) {
            iModifierArr2[length] = iModifierArr[length].clone();
        }
        IModifier iModifier = iModifierArr2[0];
        this.mDuration = iModifier.getDuration();
        iModifier.addModifierListener(this);
    }

    public ParallelModifier clone() {
        return new ParallelModifier(this);
    }

    public float getSecondsElapsed() {
        return this.mSecondsElapsed;
    }

    public float getDuration() {
        return this.mDuration;
    }

    public float onUpdate(float f, Object obj) {
        if (this.mFinished) {
            return 0.0f;
        }
        IModifier[] iModifierArr = this.mModifiers;
        this.mFinishedCached = false;
        float f2 = f;
        while (f2 > 0.0f && !this.mFinishedCached) {
            float f3 = 0.0f;
            for (int length = iModifierArr.length - 1; length >= 0; length--) {
                f3 = Math.max(f3, iModifierArr[length].onUpdate(f, obj));
            }
            f2 -= f3;
        }
        this.mFinishedCached = false;
        float f4 = f - f2;
        this.mSecondsElapsed += f4;
        return f4;
    }

    public void reset() {
        this.mFinished = false;
        this.mSecondsElapsed = 0.0f;
        IModifier[] iModifierArr = this.mModifiers;
        for (int length = iModifierArr.length - 1; length >= 0; length--) {
            iModifierArr[length].reset();
        }
    }

    public void onModifierStarted(IModifier iModifier, Object obj) {
        onModifierStarted(obj);
    }

    public void onModifierFinished(IModifier iModifier, Object obj) {
        this.mFinished = true;
        this.mFinishedCached = true;
        onModifierFinished(obj);
    }
}
