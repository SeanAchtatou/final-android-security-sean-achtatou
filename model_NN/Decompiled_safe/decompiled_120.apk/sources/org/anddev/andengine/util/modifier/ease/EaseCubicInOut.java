package org.anddev.andengine.util.modifier.ease;

public class EaseCubicInOut implements IEaseFunction {
    private static EaseCubicInOut INSTANCE;

    private EaseCubicInOut() {
    }

    public static EaseCubicInOut getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EaseCubicInOut();
        }
        return INSTANCE;
    }

    public float getPercentage(float f, float f2) {
        float f3 = f / f2;
        if (f3 < 0.5f) {
            return EaseCubicIn.getValue(f3 * 2.0f) * 0.5f;
        }
        return (EaseCubicOut.getValue((f3 * 2.0f) - 1.0f) * 0.5f) + 0.5f;
    }
}
