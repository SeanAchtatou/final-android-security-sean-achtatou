package org.anddev.andengine.util.modifier.ease;

import android.util.FloatMath;
import org.anddev.andengine.util.constants.MathConstants;

public class EaseSineOut implements MathConstants, IEaseFunction {
    private static EaseSineOut INSTANCE;

    private EaseSineOut() {
    }

    public static EaseSineOut getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EaseSineOut();
        }
        return INSTANCE;
    }

    public float getPercentage(float f, float f2) {
        return getValue(f / f2);
    }

    public static float getValue(float f) {
        return FloatMath.sin(1.5707964f * f);
    }
}
