package org.anddev.andengine.util;

import android.content.Context;
import android.os.Environment;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.InputStream;

public class FileUtils {
    public static void copyToExternalStorage(Context context, int i, String str) {
        copyToExternalStorage(context, context.getResources().openRawResource(i), str);
    }

    public static void copyToInternalStorage(Context context, int i, String str) {
        copyToInternalStorage(context, context.getResources().openRawResource(i), str);
    }

    public static void copyToExternalStorage(Context context, String str, String str2) {
        copyToExternalStorage(context, context.getAssets().open(str), str2);
    }

    public static void copyToInternalStorage(Context context, String str, String str2) {
        copyToInternalStorage(context, context.getAssets().open(str), str2);
    }

    private static void copyToInternalStorage(Context context, InputStream inputStream, String str) {
        StreamUtils.copyAndClose(inputStream, new FileOutputStream(new File(context.getFilesDir(), str)));
    }

    public static void copyToExternalStorage(Context context, InputStream inputStream, String str) {
        if (isExternalStorageWriteable()) {
            StreamUtils.copyAndClose(inputStream, new FileOutputStream(getAbsolutePathOnExternalStorage(context, str)));
            return;
        }
        throw new IllegalStateException("External Storage is not writeable.");
    }

    public static boolean isFileExistingOnExternalStorage(Context context, String str) {
        if (isExternalStorageReadable()) {
            File file = new File(getAbsolutePathOnExternalStorage(context, str));
            return file.exists() && file.isFile();
        }
        throw new IllegalStateException("External Storage is not readable.");
    }

    public static boolean isDirectoryExistingOnExternalStorage(Context context, String str) {
        if (isExternalStorageReadable()) {
            File file = new File(getAbsolutePathOnExternalStorage(context, str));
            return file.exists() && file.isDirectory();
        }
        throw new IllegalStateException("External Storage is not readable.");
    }

    public static boolean ensureDirectoriesExistOnExternalStorage(Context context, String str) {
        if (isDirectoryExistingOnExternalStorage(context, str)) {
            return true;
        }
        if (isExternalStorageWriteable()) {
            return new File(getAbsolutePathOnExternalStorage(context, str)).mkdirs();
        }
        throw new IllegalStateException("External Storage is not writeable.");
    }

    public static InputStream openOnExternalStorage(Context context, String str) {
        return new FileInputStream(getAbsolutePathOnExternalStorage(context, str));
    }

    public static String[] getDirectoryListOnExternalStorage(Context context, String str) {
        return new File(getAbsolutePathOnExternalStorage(context, str)).list();
    }

    public static String[] getDirectoryListOnExternalStorage(Context context, String str, FilenameFilter filenameFilter) {
        return new File(getAbsolutePathOnExternalStorage(context, str)).list(filenameFilter);
    }

    public static String getAbsolutePathOnInternalStorage(Context context, String str) {
        return String.valueOf(context.getFilesDir().getAbsolutePath()) + str;
    }

    public static String getAbsolutePathOnExternalStorage(Context context, String str) {
        return Environment.getExternalStorageDirectory() + "/Android/data/" + context.getApplicationInfo().packageName + "/files/" + str;
    }

    public static boolean isExternalStorageWriteable() {
        return Environment.getExternalStorageState().equals("mounted");
    }

    public static boolean isExternalStorageReadable() {
        String externalStorageState = Environment.getExternalStorageState();
        return externalStorageState.equals("mounted") || externalStorageState.equals("mounted_ro");
    }

    public static void copyFile(File file, File file2) {
        FileInputStream fileInputStream = new FileInputStream(file);
        FileOutputStream fileOutputStream = new FileOutputStream(file2);
        try {
            StreamUtils.copy(fileInputStream, fileOutputStream);
        } finally {
            StreamUtils.close(fileInputStream);
            StreamUtils.close(fileOutputStream);
        }
    }

    public static boolean deleteDirectory(File file) {
        if (file.isDirectory()) {
            for (String file2 : file.list()) {
                if (!deleteDirectory(new File(file, file2))) {
                    return false;
                }
            }
        }
        return file.delete();
    }
}
