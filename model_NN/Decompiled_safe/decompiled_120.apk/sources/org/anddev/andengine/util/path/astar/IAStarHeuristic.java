package org.anddev.andengine.util.path.astar;

import org.anddev.andengine.util.path.ITiledMap;

public interface IAStarHeuristic {
    float getExpectedRestCost(ITiledMap iTiledMap, Object obj, int i, int i2, int i3, int i4);
}
