package org.anddev.andengine.util;

import java.util.ArrayList;

public class SmartList extends ArrayList {
    private static final long serialVersionUID = -8335986399182700102L;

    public SmartList() {
    }

    public SmartList(int i) {
        super(i);
    }

    public boolean remove(Object obj, ParameterCallable parameterCallable) {
        boolean remove = remove(obj);
        if (remove) {
            parameterCallable.call(obj);
        }
        return remove;
    }

    public Object remove(IMatcher iMatcher) {
        for (int i = 0; i < size(); i++) {
            if (iMatcher.matches(get(i))) {
                return remove(i);
            }
        }
        return null;
    }

    public Object remove(IMatcher iMatcher, ParameterCallable parameterCallable) {
        for (int size = size() - 1; size >= 0; size--) {
            if (iMatcher.matches(get(size))) {
                Object remove = remove(size);
                parameterCallable.call(remove);
                return remove;
            }
        }
        return null;
    }

    public boolean removeAll(IMatcher iMatcher) {
        boolean z = false;
        for (int size = size() - 1; size >= 0; size--) {
            if (iMatcher.matches(get(size))) {
                remove(size);
                z = true;
            }
        }
        return z;
    }

    public boolean removeAll(IMatcher iMatcher, ParameterCallable parameterCallable) {
        boolean z = false;
        for (int size = size() - 1; size >= 0; size--) {
            if (iMatcher.matches(get(size))) {
                parameterCallable.call(remove(size));
                z = true;
            }
        }
        return z;
    }

    public void clear(ParameterCallable parameterCallable) {
        for (int size = size() - 1; size >= 0; size--) {
            parameterCallable.call(remove(size));
        }
    }

    public Object find(IMatcher iMatcher) {
        for (int size = size() - 1; size >= 0; size--) {
            Object obj = get(size);
            if (iMatcher.matches(obj)) {
                return obj;
            }
        }
        return null;
    }

    public void call(ParameterCallable parameterCallable) {
        for (int size = size() - 1; size >= 0; size--) {
            parameterCallable.call(get(size));
        }
    }

    public void call(IMatcher iMatcher, ParameterCallable parameterCallable) {
        for (int size = size() - 1; size >= 0; size--) {
            Object obj = get(size);
            if (iMatcher.matches(obj)) {
                parameterCallable.call(obj);
            }
        }
    }
}
