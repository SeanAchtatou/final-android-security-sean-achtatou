package org.anddev.andengine.util.modifier;

import org.anddev.andengine.util.modifier.IModifier;
import org.anddev.andengine.util.modifier.ease.IEaseFunction;

public abstract class BaseSingleValueSpanModifier extends BaseDurationModifier {
    protected final IEaseFunction mEaseFunction;
    private final float mFromValue;
    private final float mValueSpan;

    /* access modifiers changed from: protected */
    public abstract void onSetInitialValue(Object obj, float f);

    /* access modifiers changed from: protected */
    public abstract void onSetValue(Object obj, float f, float f2);

    public BaseSingleValueSpanModifier(float f, float f2, float f3) {
        this(f, f2, f3, null, IEaseFunction.DEFAULT);
    }

    public BaseSingleValueSpanModifier(float f, float f2, float f3, IEaseFunction iEaseFunction) {
        this(f, f2, f3, null, iEaseFunction);
    }

    public BaseSingleValueSpanModifier(float f, float f2, float f3, IModifier.IModifierListener iModifierListener) {
        this(f, f2, f3, iModifierListener, IEaseFunction.DEFAULT);
    }

    public BaseSingleValueSpanModifier(float f, float f2, float f3, IModifier.IModifierListener iModifierListener, IEaseFunction iEaseFunction) {
        super(f, iModifierListener);
        this.mFromValue = f2;
        this.mValueSpan = f3 - f2;
        this.mEaseFunction = iEaseFunction;
    }

    protected BaseSingleValueSpanModifier(BaseSingleValueSpanModifier baseSingleValueSpanModifier) {
        super(baseSingleValueSpanModifier);
        this.mFromValue = baseSingleValueSpanModifier.mFromValue;
        this.mValueSpan = baseSingleValueSpanModifier.mValueSpan;
        this.mEaseFunction = baseSingleValueSpanModifier.mEaseFunction;
    }

    /* access modifiers changed from: protected */
    public void onManagedInitialize(Object obj) {
        onSetInitialValue(obj, this.mFromValue);
    }

    /* access modifiers changed from: protected */
    public void onManagedUpdate(float f, Object obj) {
        float percentage = this.mEaseFunction.getPercentage(getSecondsElapsed(), this.mDuration);
        onSetValue(obj, percentage, this.mFromValue + (this.mValueSpan * percentage));
    }
}
