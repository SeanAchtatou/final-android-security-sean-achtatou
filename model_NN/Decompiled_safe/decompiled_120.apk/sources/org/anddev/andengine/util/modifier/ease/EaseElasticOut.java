package org.anddev.andengine.util.modifier.ease;

import android.util.FloatMath;
import org.anddev.andengine.util.constants.MathConstants;

public class EaseElasticOut implements MathConstants, IEaseFunction {
    private static EaseElasticOut INSTANCE;

    private EaseElasticOut() {
    }

    public static EaseElasticOut getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EaseElasticOut();
        }
        return INSTANCE;
    }

    public float getPercentage(float f, float f2) {
        return getValue(f, f2, f / f2);
    }

    public static float getValue(float f, float f2, float f3) {
        if (f == 0.0f) {
            return 0.0f;
        }
        if (f == f2) {
            return 1.0f;
        }
        float f4 = 0.3f * f2;
        return (FloatMath.sin((((f3 * f2) - (f4 / 4.0f)) * 6.2831855f) / f4) * ((float) Math.pow(2.0d, (double) (-10.0f * f3)))) + 1.0f;
    }
}
