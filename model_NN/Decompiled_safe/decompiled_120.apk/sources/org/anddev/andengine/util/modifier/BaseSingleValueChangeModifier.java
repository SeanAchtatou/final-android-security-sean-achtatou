package org.anddev.andengine.util.modifier;

import org.anddev.andengine.util.modifier.IModifier;

public abstract class BaseSingleValueChangeModifier extends BaseDurationModifier {
    private final float mValueChangePerSecond;

    /* access modifiers changed from: protected */
    public abstract void onChangeValue(float f, Object obj, float f2);

    public BaseSingleValueChangeModifier(float f, float f2) {
        this(f, f2, null);
    }

    public BaseSingleValueChangeModifier(float f, float f2, IModifier.IModifierListener iModifierListener) {
        super(f, iModifierListener);
        this.mValueChangePerSecond = f2 / f;
    }

    protected BaseSingleValueChangeModifier(BaseSingleValueChangeModifier baseSingleValueChangeModifier) {
        super(baseSingleValueChangeModifier);
        this.mValueChangePerSecond = baseSingleValueChangeModifier.mValueChangePerSecond;
    }

    /* access modifiers changed from: protected */
    public void onManagedInitialize(Object obj) {
    }

    /* access modifiers changed from: protected */
    public void onManagedUpdate(float f, Object obj) {
        onChangeValue(f, obj, this.mValueChangePerSecond * f);
    }
}
