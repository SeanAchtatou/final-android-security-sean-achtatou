package org.anddev.andengine.util.modifier.ease;

public class EaseBounceIn implements IEaseFunction {
    private static EaseBounceIn INSTANCE;

    private EaseBounceIn() {
    }

    public static EaseBounceIn getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EaseBounceIn();
        }
        return INSTANCE;
    }

    public float getPercentage(float f, float f2) {
        return getValue(f / f2);
    }

    public static float getValue(float f) {
        return 1.0f - EaseBounceOut.getValue(1.0f - f);
    }
}
