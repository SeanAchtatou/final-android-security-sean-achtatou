package org.anddev.andengine.util.modifier.ease;

public class EaseExponentialInOut implements IEaseFunction {
    private static EaseExponentialInOut INSTANCE;

    private EaseExponentialInOut() {
    }

    public static EaseExponentialInOut getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EaseExponentialInOut();
        }
        return INSTANCE;
    }

    public float getPercentage(float f, float f2) {
        float f3 = f / f2;
        if (f3 < 0.5f) {
            return EaseExponentialIn.getValue(f3 * 2.0f) * 0.5f;
        }
        return (EaseExponentialOut.getValue((f3 * 2.0f) - 1.0f) * 0.5f) + 0.5f;
    }
}
