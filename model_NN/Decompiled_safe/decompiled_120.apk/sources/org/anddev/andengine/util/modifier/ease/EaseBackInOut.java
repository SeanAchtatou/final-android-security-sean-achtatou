package org.anddev.andengine.util.modifier.ease;

public class EaseBackInOut implements IEaseFunction {
    private static EaseBackInOut INSTANCE;

    private EaseBackInOut() {
    }

    public static EaseBackInOut getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EaseBackInOut();
        }
        return INSTANCE;
    }

    public float getPercentage(float f, float f2) {
        float f3 = f / f2;
        if (f3 < 0.5f) {
            return EaseBackIn.getValue(f3 * 2.0f) * 0.5f;
        }
        return (EaseBackOut.getValue((f3 * 2.0f) - 1.0f) * 0.5f) + 0.5f;
    }
}
