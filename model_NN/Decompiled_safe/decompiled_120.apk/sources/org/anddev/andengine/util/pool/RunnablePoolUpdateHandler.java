package org.anddev.andengine.util.pool;

public abstract class RunnablePoolUpdateHandler extends PoolUpdateHandler {
    /* access modifiers changed from: protected */
    public abstract RunnablePoolItem onAllocatePoolItem();

    public RunnablePoolUpdateHandler() {
    }

    public RunnablePoolUpdateHandler(int i) {
        super(i);
    }

    /* access modifiers changed from: protected */
    public void onHandlePoolItem(RunnablePoolItem runnablePoolItem) {
        runnablePoolItem.run();
    }
}
