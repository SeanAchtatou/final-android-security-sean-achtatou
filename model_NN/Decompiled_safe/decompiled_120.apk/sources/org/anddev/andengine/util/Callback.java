package org.anddev.andengine.util;

public interface Callback {
    void onCallback(Object obj);
}
