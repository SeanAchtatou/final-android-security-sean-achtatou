package org.anddev.andengine.util.levelstats;

import android.content.Context;
import java.io.IOException;
import java.util.ArrayList;
import org.anddev.andengine.util.Callback;
import org.anddev.andengine.util.Debug;
import org.anddev.andengine.util.MathUtils;
import org.anddev.andengine.util.SimplePreferences;
import org.anddev.andengine.util.StreamUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

public class LevelStatsDBConnector {
    private static final String PREFERENCES_LEVELSTATSDBCONNECTOR_PLAYERID_ID = "preferences.levelstatsdbconnector.playerid";
    /* access modifiers changed from: private */
    public final int mPlayerID;
    /* access modifiers changed from: private */
    public final String mSecret;
    /* access modifiers changed from: private */
    public final String mSubmitURL;

    public LevelStatsDBConnector(Context context, String str, String str2) {
        this.mSecret = str;
        this.mSubmitURL = str2;
        int i = SimplePreferences.getInstance(context).getInt(PREFERENCES_LEVELSTATSDBCONNECTOR_PLAYERID_ID, -1);
        if (i != -1) {
            this.mPlayerID = i;
            return;
        }
        this.mPlayerID = MathUtils.random(1000000000, Integer.MAX_VALUE);
        SimplePreferences.getEditorInstance(context).putInt(PREFERENCES_LEVELSTATSDBCONNECTOR_PLAYERID_ID, this.mPlayerID).commit();
    }

    public void submitAsync(int i, boolean z, int i2) {
        submitAsync(i, z, i2, null);
    }

    public void submitAsync(int i, boolean z, int i2, Callback callback) {
        final int i3 = i;
        final boolean z2 = z;
        final int i4 = i2;
        final Callback callback2 = callback;
        new Thread(new Runnable() {
            public void run() {
                try {
                    DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
                    HttpPost httpPost = new HttpPost(LevelStatsDBConnector.this.mSubmitURL);
                    ArrayList arrayList = new ArrayList(5);
                    arrayList.add(new BasicNameValuePair("level_id", String.valueOf(i3)));
                    arrayList.add(new BasicNameValuePair("solved", z2 ? "1" : "0"));
                    arrayList.add(new BasicNameValuePair("secondsplayed", String.valueOf(i4)));
                    arrayList.add(new BasicNameValuePair("player_id", String.valueOf(LevelStatsDBConnector.this.mPlayerID)));
                    arrayList.add(new BasicNameValuePair("secret", String.valueOf(LevelStatsDBConnector.this.mSecret)));
                    httpPost.setEntity(new UrlEncodedFormEntity(arrayList));
                    HttpResponse execute = defaultHttpClient.execute(httpPost);
                    if (execute.getStatusLine().getStatusCode() == 200) {
                        if (StreamUtils.readFully(execute.getEntity().getContent()).equals("<success/>")) {
                            if (callback2 != null) {
                                callback2.onCallback(true);
                            }
                        } else if (callback2 != null) {
                            callback2.onCallback(false);
                        }
                    } else if (callback2 != null) {
                        callback2.onCallback(false);
                    }
                } catch (IOException e) {
                    Debug.e(e);
                    if (callback2 != null) {
                        callback2.onCallback(false);
                    }
                }
            }
        }).start();
    }
}
