package org.anddev.andengine.util.path.astar;

import android.util.FloatMath;
import org.anddev.andengine.util.path.ITiledMap;

public class EuclideanHeuristic implements IAStarHeuristic {
    public float getExpectedRestCost(ITiledMap iTiledMap, Object obj, int i, int i2, int i3, int i4) {
        float f = (float) (i3 - i);
        float f2 = (float) (i4 - i2);
        return FloatMath.sqrt((f * f) + (f2 * f2));
    }
}
