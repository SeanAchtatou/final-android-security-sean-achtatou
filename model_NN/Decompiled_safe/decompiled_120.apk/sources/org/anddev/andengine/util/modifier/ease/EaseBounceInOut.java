package org.anddev.andengine.util.modifier.ease;

public class EaseBounceInOut implements IEaseFunction {
    private static EaseBounceInOut INSTANCE;

    private EaseBounceInOut() {
    }

    public static EaseBounceInOut getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EaseBounceInOut();
        }
        return INSTANCE;
    }

    public float getPercentage(float f, float f2) {
        float f3 = f / f2;
        if (f3 < 0.5f) {
            return EaseBounceIn.getValue(f3 * 2.0f) * 0.5f;
        }
        return (EaseBounceOut.getValue((f3 * 2.0f) - 1.0f) * 0.5f) + 0.5f;
    }
}
