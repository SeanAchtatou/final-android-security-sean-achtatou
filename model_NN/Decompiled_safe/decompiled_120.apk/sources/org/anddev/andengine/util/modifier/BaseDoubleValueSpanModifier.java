package org.anddev.andengine.util.modifier;

import org.anddev.andengine.util.modifier.IModifier;
import org.anddev.andengine.util.modifier.ease.IEaseFunction;

public abstract class BaseDoubleValueSpanModifier extends BaseSingleValueSpanModifier {
    private final float mFromValueB;
    private final float mValueSpanB;

    /* access modifiers changed from: protected */
    public abstract void onSetInitialValues(Object obj, float f, float f2);

    /* access modifiers changed from: protected */
    public abstract void onSetValues(Object obj, float f, float f2, float f3);

    public BaseDoubleValueSpanModifier(float f, float f2, float f3, float f4, float f5) {
        this(f, f2, f3, f4, f5, null, IEaseFunction.DEFAULT);
    }

    public BaseDoubleValueSpanModifier(float f, float f2, float f3, float f4, float f5, IEaseFunction iEaseFunction) {
        this(f, f2, f3, f4, f5, null, iEaseFunction);
    }

    public BaseDoubleValueSpanModifier(float f, float f2, float f3, float f4, float f5, IModifier.IModifierListener iModifierListener) {
        this(f, f2, f3, f4, f5, iModifierListener, IEaseFunction.DEFAULT);
    }

    public BaseDoubleValueSpanModifier(float f, float f2, float f3, float f4, float f5, IModifier.IModifierListener iModifierListener, IEaseFunction iEaseFunction) {
        super(f, f2, f3, iModifierListener, iEaseFunction);
        this.mFromValueB = f4;
        this.mValueSpanB = f5 - f4;
    }

    protected BaseDoubleValueSpanModifier(BaseDoubleValueSpanModifier baseDoubleValueSpanModifier) {
        super(baseDoubleValueSpanModifier);
        this.mFromValueB = baseDoubleValueSpanModifier.mFromValueB;
        this.mValueSpanB = baseDoubleValueSpanModifier.mValueSpanB;
    }

    /* access modifiers changed from: protected */
    public void onSetInitialValue(Object obj, float f) {
        onSetInitialValues(obj, f, this.mFromValueB);
    }

    /* access modifiers changed from: protected */
    public void onSetValue(Object obj, float f, float f2) {
        onSetValues(obj, f, f2, this.mFromValueB + (this.mValueSpanB * f));
    }
}
