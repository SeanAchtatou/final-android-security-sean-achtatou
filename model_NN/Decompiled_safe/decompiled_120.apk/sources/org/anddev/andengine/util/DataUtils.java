package org.anddev.andengine.util;

import org.anddev.andengine.util.constants.DataConstants;

public class DataUtils implements DataConstants {
    public static int unsignedByteToInt(byte b) {
        return b & 255;
    }
}
