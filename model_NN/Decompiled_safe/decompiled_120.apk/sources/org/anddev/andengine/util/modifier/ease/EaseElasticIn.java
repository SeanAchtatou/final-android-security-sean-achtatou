package org.anddev.andengine.util.modifier.ease;

import android.util.FloatMath;
import org.anddev.andengine.util.constants.MathConstants;

public class EaseElasticIn implements MathConstants, IEaseFunction {
    private static EaseElasticIn INSTANCE;

    private EaseElasticIn() {
    }

    public static EaseElasticIn getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EaseElasticIn();
        }
        return INSTANCE;
    }

    public float getPercentage(float f, float f2) {
        return getValue(f, f2, f / f2);
    }

    public static float getValue(float f, float f2, float f3) {
        if (f == 0.0f) {
            return 0.0f;
        }
        if (f == f2) {
            return 1.0f;
        }
        float f4 = 0.3f * f2;
        float f5 = f3 - 1.0f;
        return FloatMath.sin((((f5 * f2) - (f4 / 4.0f)) * 6.2831855f) / f4) * (-((float) Math.pow(2.0d, (double) (10.0f * f5))));
    }
}
