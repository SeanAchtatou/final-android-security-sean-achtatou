package org.anddev.andengine.util.modifier;

import org.anddev.andengine.util.modifier.IModifier;

public class LoopModifier extends BaseModifier implements IModifier.IModifierListener {
    public static final int LOOP_CONTINUOUS = -1;
    private final float mDuration;
    private boolean mFinishedCached;
    private int mLoop;
    private final int mLoopCount;
    private ILoopModifierListener mLoopModifierListener;
    private final IModifier mModifier;
    private boolean mModifierStartedCalled;
    private float mSecondsElapsed;

    public interface ILoopModifierListener {
        void onLoopFinished(LoopModifier loopModifier, int i, int i2);

        void onLoopStarted(LoopModifier loopModifier, int i, int i2);
    }

    public LoopModifier(IModifier iModifier) {
        this(iModifier, -1);
    }

    public LoopModifier(IModifier iModifier, int i) {
        this(iModifier, i, null, null);
    }

    public LoopModifier(IModifier iModifier, int i, IModifier.IModifierListener iModifierListener) {
        this(iModifier, i, null, iModifierListener);
    }

    public LoopModifier(IModifier iModifier, int i, ILoopModifierListener iLoopModifierListener) {
        this(iModifier, i, iLoopModifierListener, null);
    }

    public LoopModifier(IModifier iModifier, int i, ILoopModifierListener iLoopModifierListener, IModifier.IModifierListener iModifierListener) {
        super(iModifierListener);
        this.mModifier = iModifier;
        this.mLoopCount = i;
        this.mLoopModifierListener = iLoopModifierListener;
        this.mLoop = 0;
        this.mDuration = i == -1 ? Float.POSITIVE_INFINITY : iModifier.getDuration() * ((float) i);
        this.mModifier.addModifierListener(this);
    }

    protected LoopModifier(LoopModifier loopModifier) {
        this(loopModifier.mModifier.clone(), loopModifier.mLoopCount);
    }

    public LoopModifier clone() {
        return new LoopModifier(this);
    }

    public ILoopModifierListener getLoopModifierListener() {
        return this.mLoopModifierListener;
    }

    public void setLoopModifierListener(ILoopModifierListener iLoopModifierListener) {
        this.mLoopModifierListener = iLoopModifierListener;
    }

    public float getSecondsElapsed() {
        return this.mSecondsElapsed;
    }

    public float getDuration() {
        return this.mDuration;
    }

    public float onUpdate(float f, Object obj) {
        if (this.mFinished) {
            return 0.0f;
        }
        this.mFinishedCached = false;
        float f2 = f;
        while (f2 > 0.0f && !this.mFinishedCached) {
            f2 -= this.mModifier.onUpdate(f2, obj);
        }
        this.mFinishedCached = false;
        float f3 = f - f2;
        this.mSecondsElapsed += f3;
        return f3;
    }

    public void reset() {
        this.mLoop = 0;
        this.mSecondsElapsed = 0.0f;
        this.mModifierStartedCalled = false;
        this.mModifier.reset();
    }

    public void onModifierStarted(IModifier iModifier, Object obj) {
        if (!this.mModifierStartedCalled) {
            this.mModifierStartedCalled = true;
            onModifierStarted(obj);
        }
        if (this.mLoopModifierListener != null) {
            this.mLoopModifierListener.onLoopStarted(this, this.mLoop, this.mLoopCount);
        }
    }

    public void onModifierFinished(IModifier iModifier, Object obj) {
        if (this.mLoopModifierListener != null) {
            this.mLoopModifierListener.onLoopFinished(this, this.mLoop, this.mLoopCount);
        }
        if (this.mLoopCount != -1) {
            this.mLoop++;
            if (this.mLoop >= this.mLoopCount) {
                this.mFinished = true;
                this.mFinishedCached = true;
                onModifierFinished(obj);
                return;
            }
        }
        this.mSecondsElapsed = 0.0f;
        this.mModifier.reset();
    }
}
