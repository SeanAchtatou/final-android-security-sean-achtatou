package org.anddev.andengine.util.modifier.ease;

public class EaseStrongOut implements IEaseFunction {
    private static EaseStrongOut INSTANCE;

    private EaseStrongOut() {
    }

    public static EaseStrongOut getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EaseStrongOut();
        }
        return INSTANCE;
    }

    public float getPercentage(float f, float f2) {
        return getValue(f / f2);
    }

    public static float getValue(float f) {
        float f2 = f - 1.0f;
        return (f2 * f2 * f2 * f2 * f2) + 1.0f;
    }
}
