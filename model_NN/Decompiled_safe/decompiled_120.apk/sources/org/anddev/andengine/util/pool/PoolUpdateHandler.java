package org.anddev.andengine.util.pool;

import java.util.ArrayList;
import org.anddev.andengine.engine.handler.IUpdateHandler;

public abstract class PoolUpdateHandler implements IUpdateHandler {
    private final Pool mPool;
    private final ArrayList mScheduledPoolItems;

    /* access modifiers changed from: protected */
    public abstract PoolItem onAllocatePoolItem();

    /* access modifiers changed from: protected */
    public abstract void onHandlePoolItem(PoolItem poolItem);

    public PoolUpdateHandler() {
        this.mScheduledPoolItems = new ArrayList();
        this.mPool = new Pool() {
            /* access modifiers changed from: protected */
            public PoolItem onAllocatePoolItem() {
                return PoolUpdateHandler.this.onAllocatePoolItem();
            }
        };
    }

    public PoolUpdateHandler(int i) {
        this.mScheduledPoolItems = new ArrayList();
        this.mPool = new Pool(i) {
            /* access modifiers changed from: protected */
            public PoolItem onAllocatePoolItem() {
                return PoolUpdateHandler.this.onAllocatePoolItem();
            }
        };
    }

    public PoolUpdateHandler(int i, int i2) {
        this.mScheduledPoolItems = new ArrayList();
        this.mPool = new Pool(i, i2) {
            /* access modifiers changed from: protected */
            public PoolItem onAllocatePoolItem() {
                return PoolUpdateHandler.this.onAllocatePoolItem();
            }
        };
    }

    public void onUpdate(float f) {
        ArrayList arrayList = this.mScheduledPoolItems;
        synchronized (arrayList) {
            int size = arrayList.size();
            if (size > 0) {
                Pool pool = this.mPool;
                for (int i = 0; i < size; i++) {
                    PoolItem poolItem = (PoolItem) arrayList.get(i);
                    onHandlePoolItem(poolItem);
                    pool.recyclePoolItem(poolItem);
                }
                arrayList.clear();
            }
        }
    }

    public void reset() {
        ArrayList arrayList = this.mScheduledPoolItems;
        synchronized (arrayList) {
            int size = arrayList.size();
            Pool pool = this.mPool;
            for (int i = size - 1; i >= 0; i--) {
                pool.recyclePoolItem((PoolItem) arrayList.get(i));
            }
            arrayList.clear();
        }
    }

    public PoolItem obtainPoolItem() {
        return (PoolItem) this.mPool.obtainPoolItem();
    }

    public void postPoolItem(PoolItem poolItem) {
        synchronized (this.mScheduledPoolItems) {
            if (poolItem == null) {
                throw new IllegalArgumentException("PoolItem already recycled!");
            } else if (!this.mPool.ownsPoolItem(poolItem)) {
                throw new IllegalArgumentException("PoolItem from another pool!");
            } else {
                this.mScheduledPoolItems.add(poolItem);
            }
        }
    }
}
