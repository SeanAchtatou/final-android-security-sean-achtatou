package org.anddev.andengine.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import java.util.GregorianCalendar;
import org.anddev.andengine.util.constants.Constants;

public class BetaUtils implements Constants {
    private static final String PREFERENCES_BETAUTILS_ID = "preferences.betautils.lastuse";

    public static boolean finishWhenExpired(Activity activity, GregorianCalendar gregorianCalendar, int i, int i2) {
        return finishWhenExpired(activity, gregorianCalendar, i, i2, null, null);
    }

    public static boolean finishWhenExpired(final Activity activity, GregorianCalendar gregorianCalendar, int i, int i2, final Intent intent, final Intent intent2) {
        SharedPreferences instance = SimplePreferences.getInstance(activity);
        long max = Math.max(System.currentTimeMillis(), instance.getLong(PREFERENCES_BETAUTILS_ID, -1));
        instance.edit().putLong(PREFERENCES_BETAUTILS_ID, max).commit();
        GregorianCalendar gregorianCalendar2 = new GregorianCalendar();
        gregorianCalendar2.setTimeInMillis(max);
        if (!gregorianCalendar2.after(gregorianCalendar)) {
            return false;
        }
        AlertDialog.Builder message = new AlertDialog.Builder(activity).setTitle(i).setIcon(17301543).setMessage(i2);
        message.setPositiveButton(17039370, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                if (intent != null) {
                    activity.startActivity(intent);
                }
                activity.finish();
            }
        });
        message.setNegativeButton(17039360, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                if (intent2 != null) {
                    activity.startActivity(intent2);
                }
                activity.finish();
            }
        }).create().show();
        return true;
    }
}
