package org.anddev.andengine.util.path;

public interface ITiledMap {
    float getStepCost(Object obj, int i, int i2, int i3, int i4);

    int getTileColumns();

    int getTileRows();

    boolean isTileBlocked(Object obj, int i, int i2);

    void onTileVisitedByPathFinder(int i, int i2);
}
