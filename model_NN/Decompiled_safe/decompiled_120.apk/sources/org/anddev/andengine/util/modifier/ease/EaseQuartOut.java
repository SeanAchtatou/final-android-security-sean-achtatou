package org.anddev.andengine.util.modifier.ease;

public class EaseQuartOut implements IEaseFunction {
    private static EaseQuartOut INSTANCE;

    private EaseQuartOut() {
    }

    public static EaseQuartOut getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EaseQuartOut();
        }
        return INSTANCE;
    }

    public float getPercentage(float f, float f2) {
        return getValue(f / f2);
    }

    public static float getValue(float f) {
        float f2 = f - 1.0f;
        return 1.0f - (f2 * ((f2 * f2) * f2));
    }
}
