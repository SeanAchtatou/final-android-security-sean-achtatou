package org.anddev.andengine.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import org.anddev.andengine.util.constants.Constants;

public class SimplePreferences implements Constants {
    private static SharedPreferences.Editor EDITORINSTANCE;
    private static SharedPreferences INSTANCE;

    public static SharedPreferences getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = PreferenceManager.getDefaultSharedPreferences(context);
        }
        return INSTANCE;
    }

    public static SharedPreferences.Editor getEditorInstance(Context context) {
        if (EDITORINSTANCE == null) {
            EDITORINSTANCE = getInstance(context).edit();
        }
        return EDITORINSTANCE;
    }

    public static int incrementAccessCount(Context context, String str) {
        return incrementAccessCount(context, str, 1);
    }

    public static int incrementAccessCount(Context context, String str, int i) {
        SharedPreferences instance = getInstance(context);
        int i2 = instance.getInt(str, 0) + i;
        instance.edit().putInt(str, i2).commit();
        return i2;
    }

    public static int getAccessCount(Context context, String str) {
        return getInstance(context).getInt(str, 0);
    }
}
