package org.anddev.andengine.util.modifier.ease;

public class EaseExponentialIn implements IEaseFunction {
    private static EaseExponentialIn INSTANCE;

    private EaseExponentialIn() {
    }

    public static EaseExponentialIn getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EaseExponentialIn();
        }
        return INSTANCE;
    }

    public float getPercentage(float f, float f2) {
        return getValue(f / f2);
    }

    public static float getValue(float f) {
        return (float) (f == 0.0f ? 0.0d : Math.pow(2.0d, (double) (10.0f * (f - 1.0f))) - 0.0010000000474974513d);
    }
}
