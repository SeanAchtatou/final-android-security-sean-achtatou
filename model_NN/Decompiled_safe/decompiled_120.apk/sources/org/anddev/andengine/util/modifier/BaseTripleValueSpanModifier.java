package org.anddev.andengine.util.modifier;

import org.anddev.andengine.util.modifier.IModifier;
import org.anddev.andengine.util.modifier.ease.IEaseFunction;

public abstract class BaseTripleValueSpanModifier extends BaseDoubleValueSpanModifier {
    private final float mFromValueC;
    private final float mValueSpanC;

    /* access modifiers changed from: protected */
    public abstract void onSetInitialValues(Object obj, float f, float f2, float f3);

    /* access modifiers changed from: protected */
    public abstract void onSetValues(Object obj, float f, float f2, float f3, float f4);

    public BaseTripleValueSpanModifier(float f, float f2, float f3, float f4, float f5, float f6, float f7, IEaseFunction iEaseFunction) {
        this(f, f2, f3, f4, f5, f6, f7, null, iEaseFunction);
    }

    public BaseTripleValueSpanModifier(float f, float f2, float f3, float f4, float f5, float f6, float f7, IModifier.IModifierListener iModifierListener, IEaseFunction iEaseFunction) {
        super(f, f2, f3, f4, f5, iModifierListener, iEaseFunction);
        this.mFromValueC = f6;
        this.mValueSpanC = f7 - f6;
    }

    protected BaseTripleValueSpanModifier(BaseTripleValueSpanModifier baseTripleValueSpanModifier) {
        super(baseTripleValueSpanModifier);
        this.mFromValueC = baseTripleValueSpanModifier.mFromValueC;
        this.mValueSpanC = baseTripleValueSpanModifier.mValueSpanC;
    }

    /* access modifiers changed from: protected */
    public void onSetInitialValues(Object obj, float f, float f2) {
        onSetInitialValues(obj, f, f2, this.mFromValueC);
    }

    /* access modifiers changed from: protected */
    public void onSetValues(Object obj, float f, float f2, float f3) {
        onSetValues(obj, f, f2, f3, this.mFromValueC + (this.mValueSpanC * f));
    }
}
