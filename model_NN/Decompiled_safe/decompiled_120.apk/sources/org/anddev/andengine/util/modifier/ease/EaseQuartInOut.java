package org.anddev.andengine.util.modifier.ease;

public class EaseQuartInOut implements IEaseFunction {
    private static EaseQuartInOut INSTANCE;

    private EaseQuartInOut() {
    }

    public static EaseQuartInOut getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EaseQuartInOut();
        }
        return INSTANCE;
    }

    public float getPercentage(float f, float f2) {
        float f3 = f / f2;
        if (f3 < 0.5f) {
            return EaseQuartIn.getValue(f3 * 2.0f) * 0.5f;
        }
        return (EaseQuartOut.getValue((f3 * 2.0f) - 1.0f) * 0.5f) + 0.5f;
    }
}
