package org.anddev.andengine.util.modifier.ease;

public class EaseQuartIn implements IEaseFunction {
    private static EaseQuartIn INSTANCE;

    private EaseQuartIn() {
    }

    public static EaseQuartIn getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EaseQuartIn();
        }
        return INSTANCE;
    }

    public float getPercentage(float f, float f2) {
        return getValue(f / f2);
    }

    public static float getValue(float f) {
        return f * f * f * f;
    }
}
