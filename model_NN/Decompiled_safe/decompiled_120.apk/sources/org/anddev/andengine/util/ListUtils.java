package org.anddev.andengine.util;

import java.util.ArrayList;

public class ListUtils {
    public static ArrayList toList(Object obj) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(obj);
        return arrayList;
    }

    public static ArrayList toList(Object... objArr) {
        ArrayList arrayList = new ArrayList();
        for (Object add : objArr) {
            arrayList.add(add);
        }
        return arrayList;
    }
}
