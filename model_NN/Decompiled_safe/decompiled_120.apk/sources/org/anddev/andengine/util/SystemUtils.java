package org.anddev.andengine.util;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.Scanner;
import java.util.regex.MatchResult;
import org.anddev.andengine.util.constants.TimeConstants;

public class SystemUtils {
    private static final String BOGOMIPS_PATTERN = "BogoMIPS[\\s]*:[\\s]*(\\d+\\.\\d+)[\\s]*\n";
    private static final String MEMFREE_PATTERN = "MemFree[\\s]*:[\\s]*(\\d+)[\\s]*kB\n";
    private static final String MEMTOTAL_PATTERN = "MemTotal[\\s]*:[\\s]*(\\d+)[\\s]*kB\n";

    public static int getPackageVersionCode(Context context) {
        return getPackageInfo(context).versionCode;
    }

    public static String getPackageVersionName(Context context) {
        return getPackageInfo(context).versionName;
    }

    private static PackageInfo getPackageInfo(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            Debug.e(e);
            return null;
        }
    }

    public static boolean hasSystemFeature(Context context, String str) {
        try {
            Method method = PackageManager.class.getMethod("hasSystemFeature", String.class);
            if (method == null) {
                return false;
            }
            return ((Boolean) method.invoke(context.getPackageManager(), str)).booleanValue();
        } catch (Throwable th) {
            return false;
        }
    }

    public static boolean isAndroidVersionOrHigher(int i) {
        return Integer.parseInt(Build.VERSION.SDK) >= i;
    }

    public static float getCPUBogoMips() {
        MatchResult matchSystemFile = matchSystemFile("/proc/cpuinfo", BOGOMIPS_PATTERN, TimeConstants.MILLISECONDSPERSECOND);
        try {
            if (matchSystemFile.groupCount() > 0) {
                return Float.parseFloat(matchSystemFile.group(1));
            }
            throw new SystemUtilsException();
        } catch (NumberFormatException e) {
            throw new SystemUtilsException(e);
        }
    }

    public static int getMemoryTotal() {
        MatchResult matchSystemFile = matchSystemFile("/proc/meminfo", MEMTOTAL_PATTERN, TimeConstants.MILLISECONDSPERSECOND);
        try {
            if (matchSystemFile.groupCount() > 0) {
                return Integer.parseInt(matchSystemFile.group(1));
            }
            throw new SystemUtilsException();
        } catch (NumberFormatException e) {
            throw new SystemUtilsException(e);
        }
    }

    public static int getMemoryFree() {
        MatchResult matchSystemFile = matchSystemFile("/proc/meminfo", MEMFREE_PATTERN, TimeConstants.MILLISECONDSPERSECOND);
        try {
            if (matchSystemFile.groupCount() > 0) {
                return Integer.parseInt(matchSystemFile.group(1));
            }
            throw new SystemUtilsException();
        } catch (NumberFormatException e) {
            throw new SystemUtilsException(e);
        }
    }

    public static int getCPUFrequencyCurrent() {
        return readSystemFileAsInt("/sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq");
    }

    public static int getCPUFrequencyMin() {
        return readSystemFileAsInt("/sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_min_freq");
    }

    public static int getCPUFrequencyMax() {
        return readSystemFileAsInt("/sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_max_freq");
    }

    public static int getCPUFrequencyMinScaling() {
        return readSystemFileAsInt("/sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq");
    }

    public static int getCPUFrequencyMaxScaling() {
        return readSystemFileAsInt("/sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq");
    }

    private static MatchResult matchSystemFile(String str, String str2, int i) {
        InputStream inputStream;
        IOException iOException;
        Throwable th;
        boolean z;
        try {
            InputStream inputStream2 = new ProcessBuilder("/system/bin/cat", str).start().getInputStream();
            try {
                Scanner scanner = new Scanner(inputStream2);
                if (scanner.findWithinHorizon(str2, i) != null) {
                    z = true;
                } else {
                    z = false;
                }
                if (z) {
                    MatchResult match = scanner.match();
                    StreamUtils.close(inputStream2);
                    return match;
                }
                throw new SystemUtilsException();
            } catch (IOException e) {
                IOException iOException2 = e;
                inputStream = inputStream2;
                iOException = iOException2;
                try {
                    throw new SystemUtilsException(iOException);
                } catch (Throwable th2) {
                    th = th2;
                    StreamUtils.close(inputStream);
                    throw th;
                }
            } catch (Throwable th3) {
                Throwable th4 = th3;
                inputStream = inputStream2;
                th = th4;
                StreamUtils.close(inputStream);
                throw th;
            }
        } catch (IOException e2) {
            IOException iOException3 = e2;
            inputStream = null;
            iOException = iOException3;
        } catch (Throwable th5) {
            Throwable th6 = th5;
            inputStream = null;
            th = th6;
            StreamUtils.close(inputStream);
            throw th;
        }
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:17:0x0038=Splitter:B:17:0x0038, B:9:0x0029=Splitter:B:9:0x0029} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static int readSystemFileAsInt(java.lang.String r6) {
        /*
            r0 = 0
            java.lang.ProcessBuilder r1 = new java.lang.ProcessBuilder     // Catch:{ IOException -> 0x0025, NumberFormatException -> 0x0034, all -> 0x003e }
            r2 = 2
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ IOException -> 0x0025, NumberFormatException -> 0x0034, all -> 0x003e }
            r3 = 0
            java.lang.String r4 = "/system/bin/cat"
            r2[r3] = r4     // Catch:{ IOException -> 0x0025, NumberFormatException -> 0x0034, all -> 0x003e }
            r3 = 1
            r2[r3] = r6     // Catch:{ IOException -> 0x0025, NumberFormatException -> 0x0034, all -> 0x003e }
            r1.<init>(r2)     // Catch:{ IOException -> 0x0025, NumberFormatException -> 0x0034, all -> 0x003e }
            java.lang.Process r1 = r1.start()     // Catch:{ IOException -> 0x0025, NumberFormatException -> 0x0034, all -> 0x003e }
            java.io.InputStream r0 = r1.getInputStream()     // Catch:{ IOException -> 0x0025, NumberFormatException -> 0x0034, all -> 0x003e }
            java.lang.String r1 = org.anddev.andengine.util.StreamUtils.readFully(r0)     // Catch:{ IOException -> 0x004d, NumberFormatException -> 0x0048, all -> 0x0043 }
            int r1 = java.lang.Integer.parseInt(r1)     // Catch:{ IOException -> 0x004d, NumberFormatException -> 0x0048, all -> 0x0043 }
            org.anddev.andengine.util.StreamUtils.close(r0)
            return r1
        L_0x0025:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x0029:
            org.anddev.andengine.util.SystemUtils$SystemUtilsException r2 = new org.anddev.andengine.util.SystemUtils$SystemUtilsException     // Catch:{ all -> 0x002f }
            r2.<init>(r0)     // Catch:{ all -> 0x002f }
            throw r2     // Catch:{ all -> 0x002f }
        L_0x002f:
            r0 = move-exception
        L_0x0030:
            org.anddev.andengine.util.StreamUtils.close(r1)
            throw r0
        L_0x0034:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x0038:
            org.anddev.andengine.util.SystemUtils$SystemUtilsException r2 = new org.anddev.andengine.util.SystemUtils$SystemUtilsException     // Catch:{ all -> 0x002f }
            r2.<init>(r0)     // Catch:{ all -> 0x002f }
            throw r2     // Catch:{ all -> 0x002f }
        L_0x003e:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0030
        L_0x0043:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0030
        L_0x0048:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0038
        L_0x004d:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0029
        */
        throw new UnsupportedOperationException("Method not decompiled: org.anddev.andengine.util.SystemUtils.readSystemFileAsInt(java.lang.String):int");
    }

    public class SystemUtilsException extends Exception {
        private static final long serialVersionUID = -7256483361095147596L;

        public SystemUtilsException() {
        }

        public SystemUtilsException(Throwable th) {
            super(th);
        }
    }
}
