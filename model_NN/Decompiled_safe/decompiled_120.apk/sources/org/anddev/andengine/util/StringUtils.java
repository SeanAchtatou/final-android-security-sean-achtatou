package org.anddev.andengine.util;

public class StringUtils {
    public static String padFront(String str, char c, int i) {
        int length = i - str.length();
        if (length <= 0) {
            return str;
        }
        StringBuilder sb = new StringBuilder();
        for (int i2 = length - 1; i2 >= 0; i2--) {
            sb.append(c);
        }
        sb.append(str);
        return sb.toString();
    }

    public static int countOccurrences(String str, char c) {
        int i = 0;
        int indexOf = str.indexOf(c, 0);
        while (indexOf != -1) {
            i++;
            indexOf = str.indexOf(c, indexOf + 1);
        }
        return i;
    }

    public static String[] split(String str, char c) {
        return split(str, c, null);
    }

    public static String[] split(String str, char c, String[] strArr) {
        int countOccurrences = countOccurrences(str, c) + 1;
        String[] strArr2 = strArr != null && strArr.length == countOccurrences ? strArr : new String[countOccurrences];
        if (countOccurrences == 0) {
            strArr2[0] = str;
        } else {
            int i = 0;
            for (int i2 = 0; i2 < countOccurrences - 1; i2++) {
                int indexOf = str.indexOf(c, i);
                strArr2[i2] = str.substring(i, indexOf);
                i = indexOf + 1;
            }
            strArr2[countOccurrences - 1] = str.substring(i, str.length());
        }
        return strArr2;
    }
}
