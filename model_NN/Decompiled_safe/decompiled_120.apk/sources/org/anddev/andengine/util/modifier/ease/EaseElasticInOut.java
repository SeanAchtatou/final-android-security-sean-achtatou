package org.anddev.andengine.util.modifier.ease;

import org.anddev.andengine.util.constants.MathConstants;

public class EaseElasticInOut implements MathConstants, IEaseFunction {
    private static EaseElasticInOut INSTANCE;

    private EaseElasticInOut() {
    }

    public static EaseElasticInOut getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EaseElasticInOut();
        }
        return INSTANCE;
    }

    public float getPercentage(float f, float f2) {
        float f3 = f / f2;
        if (f3 < 0.5f) {
            return EaseElasticIn.getValue(2.0f * f, f2, f3 * 2.0f) * 0.5f;
        }
        return (EaseElasticOut.getValue((f * 2.0f) - f2, f2, (f3 * 2.0f) - 1.0f) * 0.5f) + 0.5f;
    }
}
