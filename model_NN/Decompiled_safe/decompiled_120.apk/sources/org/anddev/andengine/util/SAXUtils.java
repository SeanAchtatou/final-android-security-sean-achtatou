package org.anddev.andengine.util;

import org.xml.sax.Attributes;

public class SAXUtils {
    public static String getAttribute(Attributes attributes, String str, String str2) {
        String value = attributes.getValue("", str);
        return value != null ? value : str2;
    }

    public static String getAttributeOrThrow(Attributes attributes, String str) {
        String value = attributes.getValue("", str);
        if (value != null) {
            return value;
        }
        throw new IllegalArgumentException("No value found for attribute: '" + str + "'");
    }

    public static boolean getBooleanAttribute(Attributes attributes, String str, boolean z) {
        String value = attributes.getValue("", str);
        return value != null ? Boolean.parseBoolean(value) : z;
    }

    public static boolean getBooleanAttributeOrThrow(Attributes attributes, String str) {
        return Boolean.parseBoolean(getAttributeOrThrow(attributes, str));
    }

    public static byte getByteAttribute(Attributes attributes, String str, byte b) {
        String value = attributes.getValue("", str);
        return value != null ? Byte.parseByte(value) : b;
    }

    public static byte getByteAttributeOrThrow(Attributes attributes, String str) {
        return Byte.parseByte(getAttributeOrThrow(attributes, str));
    }

    public static short getShortAttribute(Attributes attributes, String str, short s) {
        String value = attributes.getValue("", str);
        return value != null ? Short.parseShort(value) : s;
    }

    public static short getShortAttributeOrThrow(Attributes attributes, String str) {
        return Short.parseShort(getAttributeOrThrow(attributes, str));
    }

    public static int getIntAttribute(Attributes attributes, String str, int i) {
        String value = attributes.getValue("", str);
        return value != null ? Integer.parseInt(value) : i;
    }

    public static int getIntAttributeOrThrow(Attributes attributes, String str) {
        return Integer.parseInt(getAttributeOrThrow(attributes, str));
    }

    public static long getLongAttribute(Attributes attributes, String str, long j) {
        String value = attributes.getValue("", str);
        return value != null ? Long.parseLong(value) : j;
    }

    public static long getLongAttributeOrThrow(Attributes attributes, String str) {
        return Long.parseLong(getAttributeOrThrow(attributes, str));
    }

    public static float getFloatAttribute(Attributes attributes, String str, float f) {
        String value = attributes.getValue("", str);
        return value != null ? Float.parseFloat(value) : f;
    }

    public static float getFloatAttributeOrThrow(Attributes attributes, String str) {
        return Float.parseFloat(getAttributeOrThrow(attributes, str));
    }

    public static double getDoubleAttribute(Attributes attributes, String str, double d) {
        String value = attributes.getValue("", str);
        return value != null ? Double.parseDouble(value) : d;
    }

    public static double getDoubleAttributeOrThrow(Attributes attributes, String str) {
        return Double.parseDouble(getAttributeOrThrow(attributes, str));
    }

    public static void appendAttribute(StringBuilder sb, String str, boolean z) {
        appendAttribute(sb, str, String.valueOf(z));
    }

    public static void appendAttribute(StringBuilder sb, String str, byte b) {
        appendAttribute(sb, str, String.valueOf((int) b));
    }

    public static void appendAttribute(StringBuilder sb, String str, short s) {
        appendAttribute(sb, str, String.valueOf((int) s));
    }

    public static void appendAttribute(StringBuilder sb, String str, int i) {
        appendAttribute(sb, str, String.valueOf(i));
    }

    public static void appendAttribute(StringBuilder sb, String str, long j) {
        appendAttribute(sb, str, String.valueOf(j));
    }

    public static void appendAttribute(StringBuilder sb, String str, float f) {
        appendAttribute(sb, str, String.valueOf(f));
    }

    public static void appendAttribute(StringBuilder sb, String str, double d) {
        appendAttribute(sb, str, String.valueOf(d));
    }

    public static void appendAttribute(StringBuilder sb, String str, String str2) {
        sb.append(' ').append(str).append('=').append('\"').append(str2).append('\"');
    }
}
