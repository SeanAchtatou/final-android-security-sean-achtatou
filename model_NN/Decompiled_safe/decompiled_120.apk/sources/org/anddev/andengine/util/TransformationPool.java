package org.anddev.andengine.util;

import org.anddev.andengine.util.pool.GenericPool;

public class TransformationPool {
    private static final GenericPool POOL = new GenericPool() {
        /* access modifiers changed from: protected */
        public Transformation onAllocatePoolItem() {
            return new Transformation();
        }
    };

    public static Transformation obtain() {
        return (Transformation) POOL.obtainPoolItem();
    }

    public static void recycle(Transformation transformation) {
        transformation.setToIdentity();
        POOL.recyclePoolItem(transformation);
    }
}
