package org.anddev.andengine.util.modifier.ease;

import android.util.FloatMath;

public class EaseCircularOut implements IEaseFunction {
    private static EaseCircularOut INSTANCE;

    private EaseCircularOut() {
    }

    public static EaseCircularOut getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EaseCircularOut();
        }
        return INSTANCE;
    }

    public float getPercentage(float f, float f2) {
        return getValue(f / f2);
    }

    public static float getValue(float f) {
        float f2 = f - 1.0f;
        return FloatMath.sqrt(1.0f - (f2 * f2));
    }
}
