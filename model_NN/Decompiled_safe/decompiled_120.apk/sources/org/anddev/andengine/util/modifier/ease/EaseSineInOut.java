package org.anddev.andengine.util.modifier.ease;

import android.util.FloatMath;

public class EaseSineInOut implements IEaseFunction {
    private static EaseSineInOut INSTANCE;

    private EaseSineInOut() {
    }

    public static EaseSineInOut getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EaseSineInOut();
        }
        return INSTANCE;
    }

    public float getPercentage(float f, float f2) {
        return (FloatMath.cos((f / f2) * 3.1415927f) - 1.0f) * -0.5f;
    }
}
