package org.anddev.andengine.util.modifier.ease;

public class EaseExponentialOut implements IEaseFunction {
    private static EaseExponentialOut INSTANCE;

    private EaseExponentialOut() {
    }

    public static EaseExponentialOut getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EaseExponentialOut();
        }
        return INSTANCE;
    }

    public float getPercentage(float f, float f2) {
        return getValue(f / f2);
    }

    public static float getValue(float f) {
        if (f == 1.0f) {
            return 1.0f;
        }
        return (-((float) Math.pow(2.0d, (double) (-10.0f * f)))) + 1.0f;
    }
}
