package org.anddev.andengine.util.path.astar;

import org.anddev.andengine.util.path.ITiledMap;

public class NullHeuristic implements IAStarHeuristic {
    public float getExpectedRestCost(ITiledMap iTiledMap, Object obj, int i, int i2, int i3, int i4) {
        return 0.0f;
    }
}
