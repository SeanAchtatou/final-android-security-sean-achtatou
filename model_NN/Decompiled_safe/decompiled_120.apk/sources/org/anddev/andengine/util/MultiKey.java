package org.anddev.andengine.util;

import java.util.Arrays;

public class MultiKey {
    private final int mCachedHashCode;
    private final Object[] mKeys;

    public MultiKey(Object... objArr) {
        this.mKeys = objArr;
        this.mCachedHashCode = hash(objArr);
    }

    public Object[] getKeys() {
        return this.mKeys;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof MultiKey) {
            return Arrays.equals(this.mKeys, ((MultiKey) obj).mKeys);
        }
        return false;
    }

    public static int hash(Object... objArr) {
        int i = 0;
        for (Object obj : objArr) {
            if (obj != null) {
                i ^= obj.hashCode();
            }
        }
        return i;
    }

    public int hashCode() {
        return this.mCachedHashCode;
    }

    public String toString() {
        return "MultiKey" + Arrays.asList(this.mKeys).toString();
    }

    public Object getKey(int i) {
        return this.mKeys[i];
    }

    public int size() {
        return this.mKeys.length;
    }
}
