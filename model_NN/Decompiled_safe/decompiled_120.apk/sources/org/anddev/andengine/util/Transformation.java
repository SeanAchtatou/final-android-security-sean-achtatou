package org.anddev.andengine.util;

import android.util.FloatMath;

public class Transformation {
    private float a = 1.0f;
    private float b = 0.0f;
    private float c = 0.0f;
    private float d = 1.0f;
    private float tx = 0.0f;
    private float ty = 0.0f;

    public String toString() {
        return "Transformation{[" + this.a + ", " + this.c + ", " + this.tx + "][" + this.b + ", " + this.d + ", " + this.ty + "][0.0, 0.0, 1.0]}";
    }

    public void reset() {
        setToIdentity();
    }

    public void setToIdentity() {
        this.a = 1.0f;
        this.d = 1.0f;
        this.b = 0.0f;
        this.c = 0.0f;
        this.tx = 0.0f;
        this.ty = 0.0f;
    }

    public void setTo(Transformation transformation) {
        this.a = transformation.a;
        this.d = transformation.d;
        this.b = transformation.b;
        this.c = transformation.c;
        this.tx = transformation.tx;
        this.ty = transformation.ty;
    }

    public void preTranslate(float f, float f2) {
        preConcat(1.0f, 0.0f, 0.0f, 1.0f, f, f2);
    }

    public void postTranslate(float f, float f2) {
        postConcat(1.0f, 0.0f, 0.0f, 1.0f, f, f2);
    }

    public Transformation setToTranslate(float f, float f2) {
        this.a = 1.0f;
        this.b = 0.0f;
        this.c = 0.0f;
        this.d = 1.0f;
        this.tx = f;
        this.ty = f2;
        return this;
    }

    public void preScale(float f, float f2) {
        preConcat(f, 0.0f, 0.0f, f2, 0.0f, 0.0f);
    }

    public void postScale(float f, float f2) {
        postConcat(f, 0.0f, 0.0f, f2, 0.0f, 0.0f);
    }

    public Transformation setToScale(float f, float f2) {
        this.a = f;
        this.b = 0.0f;
        this.c = 0.0f;
        this.d = f2;
        this.tx = 0.0f;
        this.ty = 0.0f;
        return this;
    }

    public void preRotate(float f) {
        float degToRad = MathUtils.degToRad(f);
        float sin = FloatMath.sin(degToRad);
        float cos = FloatMath.cos(degToRad);
        preConcat(cos, sin, -sin, cos, 0.0f, 0.0f);
    }

    public void postRotate(float f) {
        float degToRad = MathUtils.degToRad(f);
        float sin = FloatMath.sin(degToRad);
        float cos = FloatMath.cos(degToRad);
        postConcat(cos, sin, -sin, cos, 0.0f, 0.0f);
    }

    public Transformation setToRotate(float f) {
        float degToRad = MathUtils.degToRad(f);
        float sin = FloatMath.sin(degToRad);
        float cos = FloatMath.cos(degToRad);
        this.a = cos;
        this.b = sin;
        this.c = -sin;
        this.d = cos;
        this.tx = 0.0f;
        this.ty = 0.0f;
        return this;
    }

    public void postConcat(Transformation transformation) {
        postConcat(transformation.a, transformation.b, transformation.c, transformation.d, transformation.tx, transformation.ty);
    }

    private void postConcat(float f, float f2, float f3, float f4, float f5, float f6) {
        float f7 = this.a;
        float f8 = this.b;
        float f9 = this.c;
        float f10 = this.d;
        float f11 = this.tx;
        float f12 = this.ty;
        this.a = (f7 * f) + (f8 * f3);
        this.b = (f7 * f2) + (f8 * f4);
        this.c = (f9 * f) + (f10 * f3);
        this.d = (f9 * f2) + (f10 * f4);
        this.tx = (f11 * f) + (f12 * f3) + f5;
        this.ty = (f11 * f2) + (f12 * f4) + f6;
    }

    public void preConcat(Transformation transformation) {
        preConcat(transformation.a, transformation.b, transformation.c, transformation.d, transformation.tx, transformation.ty);
    }

    private void preConcat(float f, float f2, float f3, float f4, float f5, float f6) {
        float f7 = this.a;
        float f8 = this.b;
        float f9 = this.c;
        float f10 = this.d;
        float f11 = this.tx;
        float f12 = this.ty;
        this.a = (f * f7) + (f2 * f9);
        this.b = (f * f8) + (f2 * f10);
        this.c = (f3 * f7) + (f4 * f9);
        this.d = (f3 * f8) + (f4 * f10);
        this.tx = (f7 * f5) + (f9 * f6) + f11;
        this.ty = (f5 * f8) + (f6 * f10) + f12;
    }

    public void transform(float[] fArr) {
        int i = 0;
        int length = fArr.length / 2;
        int i2 = 0;
        while (true) {
            length--;
            if (length >= 0) {
                int i3 = i + 1;
                float f = fArr[i];
                int i4 = i3 + 1;
                float f2 = fArr[i3];
                int i5 = i2 + 1;
                fArr[i2] = (this.a * f) + (this.c * f2) + this.tx;
                i2 = i5 + 1;
                fArr[i5] = (f * this.b) + (f2 * this.d) + this.ty;
                i = i4;
            } else {
                return;
            }
        }
    }
}
