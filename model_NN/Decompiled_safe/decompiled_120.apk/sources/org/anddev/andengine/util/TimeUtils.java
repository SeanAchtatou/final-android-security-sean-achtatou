package org.anddev.andengine.util;

import org.anddev.andengine.util.constants.TimeConstants;

public class TimeUtils implements TimeConstants {
    public static String formatSeconds(int i) {
        return formatSeconds(i, new StringBuilder());
    }

    public static String formatSeconds(int i, StringBuilder sb) {
        int i2 = i % 60;
        sb.append(i / 60);
        sb.append(':');
        if (i2 < 10) {
            sb.append('0');
        }
        sb.append(i2);
        return sb.toString();
    }
}
