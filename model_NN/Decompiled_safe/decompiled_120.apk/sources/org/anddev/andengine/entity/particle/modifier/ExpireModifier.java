package org.anddev.andengine.entity.particle.modifier;

import org.anddev.andengine.entity.particle.Particle;
import org.anddev.andengine.util.MathUtils;

public class ExpireModifier implements IParticleModifier {
    private float mMaxLifeTime;
    private float mMinLifeTime;

    public ExpireModifier(float f) {
        this(f, f);
    }

    public ExpireModifier(float f, float f2) {
        this.mMinLifeTime = f;
        this.mMaxLifeTime = f2;
    }

    public float getMinLifeTime() {
        return this.mMinLifeTime;
    }

    public float getMaxLifeTime() {
        return this.mMaxLifeTime;
    }

    public void setLifeTime(float f) {
        this.mMinLifeTime = f;
        this.mMaxLifeTime = f;
    }

    public void setLifeTime(float f, float f2) {
        this.mMinLifeTime = f;
        this.mMaxLifeTime = f2;
    }

    public void onInitializeParticle(Particle particle) {
        particle.setDeathTime((MathUtils.RANDOM.nextFloat() * (this.mMaxLifeTime - this.mMinLifeTime)) + this.mMinLifeTime);
    }

    public void onUpdateParticle(Particle particle) {
    }
}
