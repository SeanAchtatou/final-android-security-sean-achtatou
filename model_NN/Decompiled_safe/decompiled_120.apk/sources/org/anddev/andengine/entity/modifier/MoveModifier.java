package org.anddev.andengine.entity.modifier;

import org.anddev.andengine.entity.IEntity;
import org.anddev.andengine.entity.modifier.IEntityModifier;
import org.anddev.andengine.util.modifier.ease.IEaseFunction;

public class MoveModifier extends DoubleValueSpanEntityModifier {
    public MoveModifier(float f, float f2, float f3, float f4, float f5) {
        this(f, f2, f3, f4, f5, null, IEaseFunction.DEFAULT);
    }

    public MoveModifier(float f, float f2, float f3, float f4, float f5, IEaseFunction iEaseFunction) {
        this(f, f2, f3, f4, f5, null, iEaseFunction);
    }

    public MoveModifier(float f, float f2, float f3, float f4, float f5, IEntityModifier.IEntityModifierListener iEntityModifierListener) {
        super(f, f2, f3, f4, f5, iEntityModifierListener, IEaseFunction.DEFAULT);
    }

    public MoveModifier(float f, float f2, float f3, float f4, float f5, IEntityModifier.IEntityModifierListener iEntityModifierListener, IEaseFunction iEaseFunction) {
        super(f, f2, f3, f4, f5, iEntityModifierListener, iEaseFunction);
    }

    protected MoveModifier(MoveModifier moveModifier) {
        super(moveModifier);
    }

    public MoveModifier clone() {
        return new MoveModifier(this);
    }

    /* access modifiers changed from: protected */
    public void onSetInitialValues(IEntity iEntity, float f, float f2) {
        iEntity.setPosition(f, f2);
    }

    /* access modifiers changed from: protected */
    public void onSetValues(IEntity iEntity, float f, float f2, float f3) {
        iEntity.setPosition(f2, f3);
    }
}
