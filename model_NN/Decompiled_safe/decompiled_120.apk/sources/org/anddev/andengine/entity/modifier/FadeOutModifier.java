package org.anddev.andengine.entity.modifier;

import org.anddev.andengine.entity.modifier.IEntityModifier;
import org.anddev.andengine.util.modifier.ease.IEaseFunction;

public class FadeOutModifier extends AlphaModifier {
    public FadeOutModifier(float f) {
        super(f, 1.0f, 0.0f, IEaseFunction.DEFAULT);
    }

    public FadeOutModifier(float f, IEaseFunction iEaseFunction) {
        super(f, 1.0f, 0.0f, iEaseFunction);
    }

    public FadeOutModifier(float f, IEntityModifier.IEntityModifierListener iEntityModifierListener) {
        super(f, 1.0f, 0.0f, iEntityModifierListener, IEaseFunction.DEFAULT);
    }

    public FadeOutModifier(float f, IEntityModifier.IEntityModifierListener iEntityModifierListener, IEaseFunction iEaseFunction) {
        super(f, 1.0f, 0.0f, iEntityModifierListener, iEaseFunction);
    }

    protected FadeOutModifier(FadeOutModifier fadeOutModifier) {
        super(fadeOutModifier);
    }

    public FadeOutModifier clone() {
        return new FadeOutModifier(this);
    }
}
