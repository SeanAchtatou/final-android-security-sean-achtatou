package org.anddev.andengine.entity.layer.tiled.tmx;

import android.content.Context;
import java.io.IOException;
import java.util.ArrayList;
import org.anddev.andengine.entity.layer.tiled.tmx.TMXLoader;
import org.anddev.andengine.entity.layer.tiled.tmx.util.constants.TMXConstants;
import org.anddev.andengine.entity.layer.tiled.tmx.util.exception.TMXParseException;
import org.anddev.andengine.entity.layer.tiled.tmx.util.exception.TSXLoadException;
import org.anddev.andengine.opengl.texture.TextureManager;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.util.Debug;
import org.anddev.andengine.util.SAXUtils;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

public class TMXParser extends DefaultHandler implements TMXConstants {
    private final Context mContext;
    private String mDataCompression;
    private String mDataEncoding;
    private boolean mInData;
    private boolean mInImage;
    private boolean mInLayer;
    private boolean mInMap;
    private boolean mInObject;
    private boolean mInObjectGroup;
    private boolean mInProperties;
    private boolean mInProperty;
    private boolean mInTile;
    private boolean mInTileset;
    private int mLastTileSetTileID;
    private final StringBuilder mStringBuilder = new StringBuilder();
    private final TMXLoader.ITMXTilePropertiesListener mTMXTilePropertyListener;
    private TMXTiledMap mTMXTiledMap;
    private final TextureManager mTextureManager;
    private final TextureOptions mTextureOptions;

    public TMXParser(Context context, TextureManager textureManager, TextureOptions textureOptions, TMXLoader.ITMXTilePropertiesListener iTMXTilePropertiesListener) {
        this.mContext = context;
        this.mTextureManager = textureManager;
        this.mTextureOptions = textureOptions;
        this.mTMXTilePropertyListener = iTMXTilePropertiesListener;
    }

    /* access modifiers changed from: package-private */
    public TMXTiledMap getTMXTiledMap() {
        return this.mTMXTiledMap;
    }

    public void startElement(String str, String str2, String str3, Attributes attributes) {
        TMXTileSet loadFromAsset;
        if (str2.equals(TMXConstants.TAG_MAP)) {
            this.mInMap = true;
            this.mTMXTiledMap = new TMXTiledMap(attributes);
        } else if (str2.equals(TMXConstants.TAG_TILESET)) {
            this.mInTileset = true;
            String value = attributes.getValue("", "source");
            if (value == null) {
                loadFromAsset = new TMXTileSet(attributes, this.mTextureOptions);
            } else {
                try {
                    loadFromAsset = new TSXLoader(this.mContext, this.mTextureManager, this.mTextureOptions).loadFromAsset(this.mContext, SAXUtils.getIntAttribute(attributes, TMXConstants.TAG_TILESET_ATTRIBUTE_FIRSTGID, 1), value);
                } catch (TSXLoadException e) {
                    throw new TMXParseException("Failed to load TMXTileSet from source: " + value, e);
                }
            }
            this.mTMXTiledMap.addTMXTileSet(loadFromAsset);
        } else if (str2.equals(TMXConstants.TAG_IMAGE)) {
            this.mInImage = true;
            ArrayList tMXTileSets = this.mTMXTiledMap.getTMXTileSets();
            ((TMXTileSet) tMXTileSets.get(tMXTileSets.size() - 1)).setImageSource(this.mContext, this.mTextureManager, attributes);
        } else if (str2.equals(TMXConstants.TAG_TILE)) {
            this.mInTile = true;
            if (this.mInTileset) {
                this.mLastTileSetTileID = SAXUtils.getIntAttributeOrThrow(attributes, TMXConstants.TAG_TILE_ATTRIBUTE_ID);
            } else if (this.mInData) {
                ArrayList tMXLayers = this.mTMXTiledMap.getTMXLayers();
                ((TMXLayer) tMXLayers.get(tMXLayers.size() - 1)).initializeTMXTileFromXML(attributes, this.mTMXTilePropertyListener);
            }
        } else if (str2.equals(TMXConstants.TAG_PROPERTIES)) {
            this.mInProperties = true;
        } else if (this.mInProperties && str2.equals(TMXConstants.TAG_PROPERTY)) {
            this.mInProperty = true;
            if (this.mInTile) {
                ArrayList tMXTileSets2 = this.mTMXTiledMap.getTMXTileSets();
                ((TMXTileSet) tMXTileSets2.get(tMXTileSets2.size() - 1)).addTMXTileProperty(this.mLastTileSetTileID, new TMXTileProperty(attributes));
            } else if (this.mInLayer) {
                ArrayList tMXLayers2 = this.mTMXTiledMap.getTMXLayers();
                ((TMXLayer) tMXLayers2.get(tMXLayers2.size() - 1)).addTMXLayerProperty(new TMXLayerProperty(attributes));
            } else if (this.mInObject) {
                ArrayList tMXObjectGroups = this.mTMXTiledMap.getTMXObjectGroups();
                ArrayList tMXObjects = ((TMXObjectGroup) tMXObjectGroups.get(tMXObjectGroups.size() - 1)).getTMXObjects();
                ((TMXObject) tMXObjects.get(tMXObjects.size() - 1)).addTMXObjectProperty(new TMXObjectProperty(attributes));
            } else if (this.mInObjectGroup) {
                ArrayList tMXObjectGroups2 = this.mTMXTiledMap.getTMXObjectGroups();
                ((TMXObjectGroup) tMXObjectGroups2.get(tMXObjectGroups2.size() - 1)).addTMXObjectGroupProperty(new TMXObjectGroupProperty(attributes));
            } else if (this.mInMap) {
                this.mTMXTiledMap.addTMXTiledMapProperty(new TMXTiledMapProperty(attributes));
            }
        } else if (str2.equals(TMXConstants.TAG_LAYER)) {
            this.mInLayer = true;
            this.mTMXTiledMap.addTMXLayer(new TMXLayer(this.mTMXTiledMap, attributes));
        } else if (str2.equals(TMXConstants.TAG_DATA)) {
            this.mInData = true;
            this.mDataEncoding = attributes.getValue("", TMXConstants.TAG_DATA_ATTRIBUTE_ENCODING);
            this.mDataCompression = attributes.getValue("", TMXConstants.TAG_DATA_ATTRIBUTE_COMPRESSION);
        } else if (str2.equals(TMXConstants.TAG_OBJECTGROUP)) {
            this.mInObjectGroup = true;
            this.mTMXTiledMap.addTMXObjectGroup(new TMXObjectGroup(attributes));
        } else if (str2.equals(TMXConstants.TAG_OBJECT)) {
            this.mInObject = true;
            ArrayList tMXObjectGroups3 = this.mTMXTiledMap.getTMXObjectGroups();
            ((TMXObjectGroup) tMXObjectGroups3.get(tMXObjectGroups3.size() - 1)).addTMXObject(new TMXObject(attributes));
        } else {
            throw new TMXParseException("Unexpected start tag: '" + str2 + "'.");
        }
    }

    public void characters(char[] cArr, int i, int i2) {
        this.mStringBuilder.append(cArr, i, i2);
    }

    public void endElement(String str, String str2, String str3) {
        if (str2.equals(TMXConstants.TAG_MAP)) {
            this.mInMap = false;
        } else if (str2.equals(TMXConstants.TAG_TILESET)) {
            this.mInTileset = false;
        } else if (str2.equals(TMXConstants.TAG_IMAGE)) {
            this.mInImage = false;
        } else if (str2.equals(TMXConstants.TAG_TILE)) {
            this.mInTile = false;
        } else if (str2.equals(TMXConstants.TAG_PROPERTIES)) {
            this.mInProperties = false;
        } else if (str2.equals(TMXConstants.TAG_PROPERTY)) {
            this.mInProperty = false;
        } else if (str2.equals(TMXConstants.TAG_LAYER)) {
            this.mInLayer = false;
        } else if (str2.equals(TMXConstants.TAG_DATA)) {
            if ((this.mDataCompression == null || this.mDataEncoding == null) ? false : true) {
                ArrayList tMXLayers = this.mTMXTiledMap.getTMXLayers();
                try {
                    ((TMXLayer) tMXLayers.get(tMXLayers.size() - 1)).initializeTMXTilesFromDataString(this.mStringBuilder.toString().trim(), this.mDataEncoding, this.mDataCompression, this.mTMXTilePropertyListener);
                } catch (IOException e) {
                    Debug.e(e);
                }
                this.mDataCompression = null;
                this.mDataEncoding = null;
            }
            this.mInData = false;
        } else if (str2.equals(TMXConstants.TAG_OBJECTGROUP)) {
            this.mInObjectGroup = false;
        } else if (str2.equals(TMXConstants.TAG_OBJECT)) {
            this.mInObject = false;
        } else {
            throw new TMXParseException("Unexpected end tag: '" + str2 + "'.");
        }
        this.mStringBuilder.setLength(0);
    }
}
