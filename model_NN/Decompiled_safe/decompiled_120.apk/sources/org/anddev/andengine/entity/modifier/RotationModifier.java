package org.anddev.andengine.entity.modifier;

import org.anddev.andengine.entity.IEntity;
import org.anddev.andengine.entity.modifier.IEntityModifier;
import org.anddev.andengine.util.modifier.ease.IEaseFunction;

public class RotationModifier extends SingleValueSpanEntityModifier {
    public RotationModifier(float f, float f2, float f3) {
        this(f, f2, f3, null, IEaseFunction.DEFAULT);
    }

    public RotationModifier(float f, float f2, float f3, IEaseFunction iEaseFunction) {
        this(f, f2, f3, null, iEaseFunction);
    }

    public RotationModifier(float f, float f2, float f3, IEntityModifier.IEntityModifierListener iEntityModifierListener) {
        super(f, f2, f3, iEntityModifierListener, IEaseFunction.DEFAULT);
    }

    public RotationModifier(float f, float f2, float f3, IEntityModifier.IEntityModifierListener iEntityModifierListener, IEaseFunction iEaseFunction) {
        super(f, f2, f3, iEntityModifierListener, iEaseFunction);
    }

    protected RotationModifier(RotationModifier rotationModifier) {
        super(rotationModifier);
    }

    public RotationModifier clone() {
        return new RotationModifier(this);
    }

    /* access modifiers changed from: protected */
    public void onSetInitialValue(IEntity iEntity, float f) {
        iEntity.setRotation(f);
    }

    /* access modifiers changed from: protected */
    public void onSetValue(IEntity iEntity, float f, float f2) {
        iEntity.setRotation(f2);
    }
}
