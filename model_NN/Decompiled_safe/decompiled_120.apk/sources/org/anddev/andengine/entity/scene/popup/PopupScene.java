package org.anddev.andengine.entity.scene.popup;

import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.engine.handler.timer.ITimerCallback;
import org.anddev.andengine.engine.handler.timer.TimerHandler;
import org.anddev.andengine.entity.scene.CameraScene;
import org.anddev.andengine.entity.scene.Scene;

public class PopupScene extends CameraScene {
    public PopupScene(Camera camera, Scene scene, float f) {
        this(camera, scene, f, null);
    }

    public PopupScene(Camera camera, final Scene scene, float f, final Runnable runnable) {
        super(camera);
        setBackgroundEnabled(false);
        scene.setChildScene(this, false, true, true);
        registerUpdateHandler(new TimerHandler(f, new ITimerCallback() {
            public void onTimePassed(TimerHandler timerHandler) {
                PopupScene.this.unregisterUpdateHandler(timerHandler);
                scene.clearChildScene();
                if (runnable != null) {
                    runnable.run();
                }
            }
        }));
    }
}
