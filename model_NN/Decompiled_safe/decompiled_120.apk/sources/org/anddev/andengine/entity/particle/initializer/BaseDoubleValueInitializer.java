package org.anddev.andengine.entity.particle.initializer;

import org.anddev.andengine.entity.particle.Particle;
import org.anddev.andengine.util.MathUtils;

public abstract class BaseDoubleValueInitializer extends BaseSingleValueInitializer {
    protected float mMaxValueB;
    protected float mMinValueB;

    /* access modifiers changed from: protected */
    public abstract void onInitializeParticle(Particle particle, float f, float f2);

    public BaseDoubleValueInitializer(float f, float f2, float f3, float f4) {
        super(f, f2);
        this.mMinValueB = f3;
        this.mMaxValueB = f4;
    }

    /* access modifiers changed from: protected */
    public final void onInitializeParticle(Particle particle, float f) {
        onInitializeParticle(particle, f, getRandomValueB());
    }

    private final float getRandomValueB() {
        if (this.mMinValueB == this.mMaxValueB) {
            return this.mMaxValueB;
        }
        return (MathUtils.RANDOM.nextFloat() * (this.mMaxValueB - this.mMinValueB)) + this.mMinValueB;
    }
}
