package org.anddev.andengine.entity.sprite.batch;

import java.util.ArrayList;
import org.anddev.andengine.entity.IEntity;
import org.anddev.andengine.entity.sprite.BaseSprite;
import org.anddev.andengine.opengl.texture.ITexture;
import org.anddev.andengine.opengl.texture.region.buffer.SpriteBatchTextureRegionBuffer;
import org.anddev.andengine.opengl.vertex.SpriteBatchVertexBuffer;
import org.anddev.andengine.util.SmartList;

public class SpriteGroup extends DynamicSpriteBatch {
    public SpriteGroup(ITexture iTexture, int i) {
        super(iTexture, i);
        setChildrenVisible(false);
    }

    public SpriteGroup(ITexture iTexture, int i, SpriteBatchVertexBuffer spriteBatchVertexBuffer, SpriteBatchTextureRegionBuffer spriteBatchTextureRegionBuffer) {
        super(iTexture, i, spriteBatchVertexBuffer, spriteBatchTextureRegionBuffer);
        setChildrenVisible(false);
    }

    public void attachChild(IEntity iEntity) {
        if (iEntity instanceof BaseSprite) {
            attachChild((BaseSprite) iEntity);
            return;
        }
        throw new IllegalArgumentException("A SpriteGroup can only handle children of type BaseSprite or subclasses of BaseSprite, like Sprite, TiledSprite or AnimatedSprite.");
    }

    public void attachChild(BaseSprite baseSprite) {
        assertCapacity();
        assertTexture(baseSprite.getTextureRegion());
        super.attachChild((IEntity) baseSprite);
    }

    public void attachChildren(ArrayList arrayList) {
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            attachChild((BaseSprite) arrayList.get(i));
        }
    }

    /* access modifiers changed from: protected */
    public boolean onUpdateSpriteBatch() {
        SmartList smartList = this.mChildren;
        if (smartList == null) {
            return false;
        }
        int size = smartList.size();
        for (int i = 0; i < size; i++) {
            super.drawWithoutChecks((BaseSprite) smartList.get(i));
        }
        return true;
    }

    private void assertCapacity() {
        if (getChildCount() >= this.mCapacity) {
            throw new IllegalStateException("This SpriteGroup has already reached its capacity (" + this.mCapacity + ") !");
        }
    }
}
