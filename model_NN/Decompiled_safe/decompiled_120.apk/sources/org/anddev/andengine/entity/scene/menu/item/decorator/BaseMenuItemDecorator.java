package org.anddev.andengine.entity.scene.menu.item.decorator;

import java.util.Comparator;
import javax.microedition.khronos.opengles.GL10;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.engine.handler.IUpdateHandler;
import org.anddev.andengine.entity.IEntity;
import org.anddev.andengine.entity.modifier.IEntityModifier;
import org.anddev.andengine.entity.scene.menu.item.IMenuItem;
import org.anddev.andengine.entity.shape.IShape;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.util.Transformation;

public abstract class BaseMenuItemDecorator implements IMenuItem {
    private final IMenuItem mMenuItem;

    /* access modifiers changed from: protected */
    public abstract void onMenuItemReset(IMenuItem iMenuItem);

    /* access modifiers changed from: protected */
    public abstract void onMenuItemSelected(IMenuItem iMenuItem);

    /* access modifiers changed from: protected */
    public abstract void onMenuItemUnselected(IMenuItem iMenuItem);

    public BaseMenuItemDecorator(IMenuItem iMenuItem) {
        this.mMenuItem = iMenuItem;
    }

    public int getID() {
        return this.mMenuItem.getID();
    }

    public final void onSelected() {
        this.mMenuItem.onSelected();
        onMenuItemSelected(this.mMenuItem);
    }

    public final void onUnselected() {
        this.mMenuItem.onUnselected();
        onMenuItemUnselected(this.mMenuItem);
    }

    public float getX() {
        return this.mMenuItem.getX();
    }

    public float getY() {
        return this.mMenuItem.getY();
    }

    public void setPosition(IEntity iEntity) {
        this.mMenuItem.setPosition(iEntity);
    }

    public void setPosition(float f, float f2) {
        this.mMenuItem.setPosition(f, f2);
    }

    public float getBaseWidth() {
        return this.mMenuItem.getBaseWidth();
    }

    public float getBaseHeight() {
        return this.mMenuItem.getBaseHeight();
    }

    public float getWidth() {
        return this.mMenuItem.getWidth();
    }

    public float getWidthScaled() {
        return this.mMenuItem.getWidthScaled();
    }

    public float getHeight() {
        return this.mMenuItem.getHeight();
    }

    public float getHeightScaled() {
        return this.mMenuItem.getHeightScaled();
    }

    public float getInitialX() {
        return this.mMenuItem.getInitialX();
    }

    public float getInitialY() {
        return this.mMenuItem.getInitialY();
    }

    public float getRed() {
        return this.mMenuItem.getRed();
    }

    public float getGreen() {
        return this.mMenuItem.getGreen();
    }

    public float getBlue() {
        return this.mMenuItem.getBlue();
    }

    public float getAlpha() {
        return this.mMenuItem.getAlpha();
    }

    public void setAlpha(float f) {
        this.mMenuItem.setAlpha(f);
    }

    public void setColor(float f, float f2, float f3) {
        this.mMenuItem.setColor(f, f2, f3);
    }

    public void setColor(float f, float f2, float f3, float f4) {
        this.mMenuItem.setColor(f, f2, f3, f4);
    }

    public boolean isRotated() {
        return this.mMenuItem.isRotated();
    }

    public float getRotation() {
        return this.mMenuItem.getRotation();
    }

    public void setRotation(float f) {
        this.mMenuItem.setRotation(f);
    }

    public float getRotationCenterX() {
        return this.mMenuItem.getRotationCenterX();
    }

    public float getRotationCenterY() {
        return this.mMenuItem.getRotationCenterY();
    }

    public void setRotationCenterX(float f) {
        this.mMenuItem.setRotationCenterX(f);
    }

    public void setRotationCenterY(float f) {
        this.mMenuItem.setRotationCenterY(f);
    }

    public void setRotationCenter(float f, float f2) {
        this.mMenuItem.setRotationCenter(f, f2);
    }

    public boolean isScaled() {
        return this.mMenuItem.isScaled();
    }

    public float getScaleX() {
        return this.mMenuItem.getScaleX();
    }

    public float getScaleY() {
        return this.mMenuItem.getScaleY();
    }

    public void setScale(float f) {
        this.mMenuItem.setScale(f);
    }

    public void setScale(float f, float f2) {
        this.mMenuItem.setScale(f, f2);
    }

    public void setScaleX(float f) {
        this.mMenuItem.setScaleX(f);
    }

    public void setScaleY(float f) {
        this.mMenuItem.setScaleY(f);
    }

    public float getScaleCenterX() {
        return this.mMenuItem.getScaleCenterX();
    }

    public float getScaleCenterY() {
        return this.mMenuItem.getScaleCenterY();
    }

    public void setScaleCenterX(float f) {
        this.mMenuItem.setScaleCenterX(f);
    }

    public void setScaleCenterY(float f) {
        this.mMenuItem.setScaleCenterY(f);
    }

    public void setScaleCenter(float f, float f2) {
        this.mMenuItem.setScaleCenter(f, f2);
    }

    public boolean collidesWith(IShape iShape) {
        return this.mMenuItem.collidesWith(iShape);
    }

    public float[] getSceneCenterCoordinates() {
        return this.mMenuItem.getSceneCenterCoordinates();
    }

    public boolean isCullingEnabled() {
        return this.mMenuItem.isCullingEnabled();
    }

    public void registerEntityModifier(IEntityModifier iEntityModifier) {
        this.mMenuItem.registerEntityModifier(iEntityModifier);
    }

    public boolean unregisterEntityModifier(IEntityModifier iEntityModifier) {
        return this.mMenuItem.unregisterEntityModifier(iEntityModifier);
    }

    public boolean unregisterEntityModifiers(IEntityModifier.IEntityModifierMatcher iEntityModifierMatcher) {
        return this.mMenuItem.unregisterEntityModifiers(iEntityModifierMatcher);
    }

    public void clearEntityModifiers() {
        this.mMenuItem.clearEntityModifiers();
    }

    public void setInitialPosition() {
        this.mMenuItem.setInitialPosition();
    }

    public void setBlendFunction(int i, int i2) {
        this.mMenuItem.setBlendFunction(i, i2);
    }

    public void setCullingEnabled(boolean z) {
        this.mMenuItem.setCullingEnabled(z);
    }

    public int getZIndex() {
        return this.mMenuItem.getZIndex();
    }

    public void setZIndex(int i) {
        this.mMenuItem.setZIndex(i);
    }

    public void onDraw(GL10 gl10, Camera camera) {
        this.mMenuItem.onDraw(gl10, camera);
    }

    public void onUpdate(float f) {
        this.mMenuItem.onUpdate(f);
    }

    public void reset() {
        this.mMenuItem.reset();
        onMenuItemReset(this.mMenuItem);
    }

    public boolean contains(float f, float f2) {
        return this.mMenuItem.contains(f, f2);
    }

    public float[] convertLocalToSceneCoordinates(float f, float f2) {
        return this.mMenuItem.convertLocalToSceneCoordinates(f, f2);
    }

    public float[] convertLocalToSceneCoordinates(float f, float f2, float[] fArr) {
        return this.mMenuItem.convertLocalToSceneCoordinates(f, f2, fArr);
    }

    public float[] convertLocalToSceneCoordinates(float[] fArr) {
        return this.mMenuItem.convertLocalToSceneCoordinates(fArr);
    }

    public float[] convertLocalToSceneCoordinates(float[] fArr, float[] fArr2) {
        return this.mMenuItem.convertLocalToSceneCoordinates(fArr, fArr2);
    }

    public float[] convertSceneToLocalCoordinates(float f, float f2) {
        return this.mMenuItem.convertSceneToLocalCoordinates(f, f2);
    }

    public float[] convertSceneToLocalCoordinates(float f, float f2, float[] fArr) {
        return this.mMenuItem.convertSceneToLocalCoordinates(f, f2, fArr);
    }

    public float[] convertSceneToLocalCoordinates(float[] fArr) {
        return this.mMenuItem.convertSceneToLocalCoordinates(fArr);
    }

    public float[] convertSceneToLocalCoordinates(float[] fArr, float[] fArr2) {
        return this.mMenuItem.convertSceneToLocalCoordinates(fArr, fArr2);
    }

    public boolean onAreaTouched(TouchEvent touchEvent, float f, float f2) {
        return this.mMenuItem.onAreaTouched(touchEvent, f, f2);
    }

    public int getChildCount() {
        return this.mMenuItem.getChildCount();
    }

    public void attachChild(IEntity iEntity) {
        this.mMenuItem.attachChild(iEntity);
    }

    public boolean attachChild(IEntity iEntity, int i) {
        return this.mMenuItem.attachChild(iEntity, i);
    }

    public IEntity getFirstChild() {
        return this.mMenuItem.getFirstChild();
    }

    public IEntity getLastChild() {
        return this.mMenuItem.getLastChild();
    }

    public IEntity getChild(int i) {
        return this.mMenuItem.getChild(i);
    }

    public int getChildIndex(IEntity iEntity) {
        return this.mMenuItem.getChildIndex(iEntity);
    }

    public boolean setChildIndex(IEntity iEntity, int i) {
        return this.mMenuItem.setChildIndex(iEntity, i);
    }

    public IEntity findChild(IEntity.IEntityMatcher iEntityMatcher) {
        return this.mMenuItem.findChild(iEntityMatcher);
    }

    public boolean swapChildren(IEntity iEntity, IEntity iEntity2) {
        return this.mMenuItem.swapChildren(iEntity, iEntity2);
    }

    public boolean swapChildren(int i, int i2) {
        return this.mMenuItem.swapChildren(i, i2);
    }

    public void sortChildren() {
        this.mMenuItem.sortChildren();
    }

    public void sortChildren(Comparator comparator) {
        this.mMenuItem.sortChildren(comparator);
    }

    public boolean detachSelf() {
        return this.mMenuItem.detachSelf();
    }

    public boolean detachChild(IEntity iEntity) {
        return this.mMenuItem.detachChild(iEntity);
    }

    public IEntity detachChild(IEntity.IEntityMatcher iEntityMatcher) {
        return this.mMenuItem.detachChild(iEntityMatcher);
    }

    public boolean detachChildren(IEntity.IEntityMatcher iEntityMatcher) {
        return this.mMenuItem.detachChildren(iEntityMatcher);
    }

    public void detachChildren() {
        this.mMenuItem.detachChildren();
    }

    public void callOnChildren(IEntity.IEntityCallable iEntityCallable) {
        callOnChildren(iEntityCallable);
    }

    public void callOnChildren(IEntity.IEntityMatcher iEntityMatcher, IEntity.IEntityCallable iEntityCallable) {
        this.mMenuItem.callOnChildren(iEntityMatcher, iEntityCallable);
    }

    public Transformation getLocalToSceneTransformation() {
        return this.mMenuItem.getLocalToSceneTransformation();
    }

    public Transformation getSceneToLocalTransformation() {
        return this.mMenuItem.getSceneToLocalTransformation();
    }

    public boolean hasParent() {
        return this.mMenuItem.hasParent();
    }

    public IEntity getParent() {
        return this.mMenuItem.getParent();
    }

    public void setParent(IEntity iEntity) {
        this.mMenuItem.setParent(iEntity);
    }

    public boolean isVisible() {
        return this.mMenuItem.isVisible();
    }

    public void setVisible(boolean z) {
        this.mMenuItem.setVisible(z);
    }

    public boolean isChildrenVisible() {
        return this.mMenuItem.isChildrenVisible();
    }

    public void setChildrenVisible(boolean z) {
        this.mMenuItem.setChildrenVisible(z);
    }

    public boolean isIgnoreUpdate() {
        return this.mMenuItem.isIgnoreUpdate();
    }

    public void setIgnoreUpdate(boolean z) {
        this.mMenuItem.setIgnoreUpdate(z);
    }

    public boolean isChildrenIgnoreUpdate() {
        return this.mMenuItem.isChildrenIgnoreUpdate();
    }

    public void setChildrenIgnoreUpdate(boolean z) {
        this.mMenuItem.setChildrenIgnoreUpdate(z);
    }

    public void setUserData(Object obj) {
        this.mMenuItem.setUserData(obj);
    }

    public Object getUserData() {
        return this.mMenuItem.getUserData();
    }

    public void onAttached() {
        this.mMenuItem.onAttached();
    }

    public void onDetached() {
        this.mMenuItem.onDetached();
    }

    public void registerUpdateHandler(IUpdateHandler iUpdateHandler) {
        this.mMenuItem.registerUpdateHandler(iUpdateHandler);
    }

    public boolean unregisterUpdateHandler(IUpdateHandler iUpdateHandler) {
        return this.mMenuItem.unregisterUpdateHandler(iUpdateHandler);
    }

    public void clearUpdateHandlers() {
        this.mMenuItem.clearUpdateHandlers();
    }

    public boolean unregisterUpdateHandlers(IUpdateHandler.IUpdateHandlerMatcher iUpdateHandlerMatcher) {
        return this.mMenuItem.unregisterUpdateHandlers(iUpdateHandlerMatcher);
    }
}
