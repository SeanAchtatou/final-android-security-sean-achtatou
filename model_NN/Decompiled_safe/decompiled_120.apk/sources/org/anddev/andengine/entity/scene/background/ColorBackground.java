package org.anddev.andengine.entity.scene.background;

import javax.microedition.khronos.opengles.GL10;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.opengl.texture.compressed.pvr.PVRTexture;

public class ColorBackground extends BaseBackground {
    private float mAlpha = 1.0f;
    private float mBlue = 0.0f;
    private boolean mColorEnabled = true;
    private float mGreen = 0.0f;
    private float mRed = 0.0f;

    protected ColorBackground() {
    }

    public ColorBackground(float f, float f2, float f3) {
        this.mRed = f;
        this.mGreen = f2;
        this.mBlue = f3;
    }

    public ColorBackground(float f, float f2, float f3, float f4) {
        this.mRed = f;
        this.mGreen = f2;
        this.mBlue = f3;
        this.mAlpha = f4;
    }

    public void setColor(float f, float f2, float f3) {
        this.mRed = f;
        this.mGreen = f2;
        this.mBlue = f3;
    }

    public void setColor(float f, float f2, float f3, float f4) {
        setColor(f, f2, f3);
        this.mAlpha = f4;
    }

    public void setColor(int i, int i2, int i3) {
        setColor(((float) i) / 255.0f, ((float) i2) / 255.0f, ((float) i3) / 255.0f);
    }

    public void setColor(int i, int i2, int i3, int i4) {
        setColor(((float) i) / 255.0f, ((float) i2) / 255.0f, ((float) i3) / 255.0f, ((float) i4) / 255.0f);
    }

    public void setColorEnabled(boolean z) {
        this.mColorEnabled = z;
    }

    public boolean isColorEnabled() {
        return this.mColorEnabled;
    }

    public void onDraw(GL10 gl10, Camera camera) {
        if (this.mColorEnabled) {
            gl10.glClearColor(this.mRed, this.mGreen, this.mBlue, this.mAlpha);
            gl10.glClear(PVRTexture.FLAG_VOLUME);
        }
    }
}
