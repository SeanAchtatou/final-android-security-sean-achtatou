package org.anddev.andengine.entity.scene.menu.animator;

import java.util.ArrayList;
import org.anddev.andengine.entity.modifier.AlphaModifier;
import org.anddev.andengine.entity.scene.menu.item.IMenuItem;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.util.HorizontalAlign;
import org.anddev.andengine.util.modifier.ease.IEaseFunction;

public class AlphaMenuAnimator extends BaseMenuAnimator {
    private static /* synthetic */ int[] $SWITCH_TABLE$org$anddev$andengine$util$HorizontalAlign = null;
    private static final float ALPHA_FROM = 0.0f;
    private static final float ALPHA_TO = 1.0f;

    static /* synthetic */ int[] $SWITCH_TABLE$org$anddev$andengine$util$HorizontalAlign() {
        int[] iArr = $SWITCH_TABLE$org$anddev$andengine$util$HorizontalAlign;
        if (iArr == null) {
            iArr = new int[HorizontalAlign.values().length];
            try {
                iArr[HorizontalAlign.CENTER.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[HorizontalAlign.LEFT.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[HorizontalAlign.RIGHT.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$org$anddev$andengine$util$HorizontalAlign = iArr;
        }
        return iArr;
    }

    public AlphaMenuAnimator() {
    }

    public AlphaMenuAnimator(IEaseFunction iEaseFunction) {
        super(iEaseFunction);
    }

    public AlphaMenuAnimator(HorizontalAlign horizontalAlign) {
        super(horizontalAlign);
    }

    public AlphaMenuAnimator(HorizontalAlign horizontalAlign, IEaseFunction iEaseFunction) {
        super(horizontalAlign, iEaseFunction);
    }

    public AlphaMenuAnimator(float f) {
        super(f);
    }

    public AlphaMenuAnimator(float f, IEaseFunction iEaseFunction) {
        super(f, iEaseFunction);
    }

    public AlphaMenuAnimator(HorizontalAlign horizontalAlign, float f) {
        super(horizontalAlign, f);
    }

    public AlphaMenuAnimator(HorizontalAlign horizontalAlign, float f, IEaseFunction iEaseFunction) {
        super(horizontalAlign, f, iEaseFunction);
    }

    public void buildAnimations(ArrayList arrayList, float f, float f2) {
        IEaseFunction iEaseFunction = this.mEaseFunction;
        for (int size = arrayList.size() - 1; size >= 0; size--) {
            AlphaModifier alphaModifier = new AlphaModifier(1.0f, (float) ALPHA_FROM, 1.0f, iEaseFunction);
            alphaModifier.setRemoveWhenFinished(false);
            ((IMenuItem) arrayList.get(size)).registerEntityModifier(alphaModifier);
        }
    }

    public void prepareAnimations(ArrayList arrayList, float f, float f2) {
        float widthScaled;
        float maximumWidth = getMaximumWidth(arrayList);
        float f3 = (f - maximumWidth) * 0.5f;
        float overallHeight = (f2 - getOverallHeight(arrayList)) * 0.5f;
        float f4 = this.mMenuItemSpacing;
        int size = arrayList.size();
        int i = 0;
        float f5 = 0.0f;
        while (i < size) {
            IMenuItem iMenuItem = (IMenuItem) arrayList.get(i);
            switch ($SWITCH_TABLE$org$anddev$andengine$util$HorizontalAlign()[this.mHorizontalAlign.ordinal()]) {
                case 1:
                    widthScaled = 0.0f;
                    break;
                case 2:
                default:
                    widthScaled = (maximumWidth - iMenuItem.getWidthScaled()) * 0.5f;
                    break;
                case TouchEvent.ACTION_CANCEL /*3*/:
                    widthScaled = maximumWidth - iMenuItem.getWidthScaled();
                    break;
            }
            iMenuItem.setPosition(widthScaled + f3, overallHeight + f5);
            iMenuItem.setAlpha(ALPHA_FROM);
            i++;
            f5 = iMenuItem.getHeight() + f4 + f5;
        }
    }
}
