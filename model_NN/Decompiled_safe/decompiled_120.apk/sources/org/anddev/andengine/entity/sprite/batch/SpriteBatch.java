package org.anddev.andengine.entity.sprite.batch;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.entity.Entity;
import org.anddev.andengine.entity.sprite.BaseSprite;
import org.anddev.andengine.opengl.texture.ITexture;
import org.anddev.andengine.opengl.texture.region.BaseTextureRegion;
import org.anddev.andengine.opengl.texture.region.buffer.SpriteBatchTextureRegionBuffer;
import org.anddev.andengine.opengl.util.GLHelper;
import org.anddev.andengine.opengl.vertex.SpriteBatchVertexBuffer;

public class SpriteBatch extends Entity {
    protected final int mCapacity;
    private int mDestinationBlendFunction;
    protected int mIndex;
    private int mSourceBlendFunction;
    private final SpriteBatchTextureRegionBuffer mSpriteBatchTextureRegionBuffer;
    private final SpriteBatchVertexBuffer mSpriteBatchVertexBuffer;
    protected final ITexture mTexture;
    private int mVertices;

    public SpriteBatch(ITexture iTexture, int i) {
        this(iTexture, i, new SpriteBatchVertexBuffer(i, 35044, true), new SpriteBatchTextureRegionBuffer(i, 35044, true));
    }

    public SpriteBatch(ITexture iTexture, int i, SpriteBatchVertexBuffer spriteBatchVertexBuffer, SpriteBatchTextureRegionBuffer spriteBatchTextureRegionBuffer) {
        this.mTexture = iTexture;
        this.mCapacity = i;
        this.mSpriteBatchVertexBuffer = spriteBatchVertexBuffer;
        this.mSpriteBatchTextureRegionBuffer = spriteBatchTextureRegionBuffer;
        initBlendFunction();
    }

    public void setBlendFunction(int i, int i2) {
        this.mSourceBlendFunction = i;
        this.mDestinationBlendFunction = i2;
    }

    public int getIndex() {
        return this.mIndex;
    }

    public void setIndex(int i) {
        assertCapacity(i);
        this.mIndex = i;
        int i2 = i * 2 * 6;
        this.mSpriteBatchVertexBuffer.setIndex(i2);
        this.mSpriteBatchTextureRegionBuffer.setIndex(i2);
    }

    /* access modifiers changed from: protected */
    public void doDraw(GL10 gl10, Camera camera) {
        onInitDraw(gl10);
        begin(gl10);
        onApplyVertices(gl10);
        onApplyTextureRegion(gl10);
        drawVertices(gl10, camera);
        end(gl10);
    }

    public void reset() {
        super.reset();
        initBlendFunction();
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        super.finalize();
        if (this.mSpriteBatchVertexBuffer.isManaged()) {
            this.mSpriteBatchVertexBuffer.unloadFromActiveBufferObjectManager();
        }
        if (this.mSpriteBatchTextureRegionBuffer.isManaged()) {
            this.mSpriteBatchTextureRegionBuffer.unloadFromActiveBufferObjectManager();
        }
    }

    /* access modifiers changed from: protected */
    public void begin(GL10 gl10) {
    }

    /* access modifiers changed from: protected */
    public void end(GL10 gl10) {
    }

    public void draw(BaseTextureRegion baseTextureRegion, float f, float f2, float f3, float f4) {
        assertCapacity();
        assertTexture(baseTextureRegion);
        this.mSpriteBatchVertexBuffer.add(f, f2, f3, f4);
        this.mSpriteBatchTextureRegionBuffer.add(baseTextureRegion);
        this.mIndex++;
    }

    public void drawWithoutChecks(BaseTextureRegion baseTextureRegion, float f, float f2, float f3, float f4) {
        this.mSpriteBatchVertexBuffer.add(f, f2, f3, f4);
        this.mSpriteBatchTextureRegionBuffer.add(baseTextureRegion);
        this.mIndex++;
    }

    public void draw(BaseTextureRegion baseTextureRegion, float f, float f2, float f3, float f4, float f5) {
        assertCapacity();
        assertTexture(baseTextureRegion);
        this.mSpriteBatchVertexBuffer.add(f, f2, f3, f4, f5);
        this.mSpriteBatchTextureRegionBuffer.add(baseTextureRegion);
        this.mIndex++;
    }

    public void drawWithoutChecks(BaseTextureRegion baseTextureRegion, float f, float f2, float f3, float f4, float f5) {
        this.mSpriteBatchVertexBuffer.add(f, f2, f3, f4, f5);
        this.mSpriteBatchTextureRegionBuffer.add(baseTextureRegion);
        this.mIndex++;
    }

    public void draw(BaseTextureRegion baseTextureRegion, float f, float f2, float f3, float f4, float f5, float f6) {
        assertCapacity();
        assertTexture(baseTextureRegion);
        this.mSpriteBatchVertexBuffer.add(f, f2, f3, f4, f5, f6);
        this.mSpriteBatchTextureRegionBuffer.add(baseTextureRegion);
        this.mIndex++;
    }

    public void drawWithoutChecks(BaseTextureRegion baseTextureRegion, float f, float f2, float f3, float f4, float f5, float f6) {
        this.mSpriteBatchVertexBuffer.add(f, f2, f3, f4, f5, f6);
        this.mSpriteBatchTextureRegionBuffer.add(baseTextureRegion);
        this.mIndex++;
    }

    public void draw(BaseTextureRegion baseTextureRegion, float f, float f2, float f3, float f4, float f5, float f6, float f7) {
        assertCapacity();
        assertTexture(baseTextureRegion);
        this.mSpriteBatchVertexBuffer.add(f, f2, f3, f4, f5, f6, f7);
        this.mSpriteBatchTextureRegionBuffer.add(baseTextureRegion);
        this.mIndex++;
    }

    public void drawWithoutChecks(BaseTextureRegion baseTextureRegion, float f, float f2, float f3, float f4, float f5, float f6, float f7) {
        this.mSpriteBatchVertexBuffer.add(f, f2, f3, f4, f5, f6, f7);
        this.mSpriteBatchTextureRegionBuffer.add(baseTextureRegion);
        this.mIndex++;
    }

    public void draw(BaseTextureRegion baseTextureRegion, float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8) {
        assertCapacity();
        assertTexture(baseTextureRegion);
        this.mSpriteBatchVertexBuffer.addInner(f, f2, f3, f4, f5, f6, f7, f8);
        this.mSpriteBatchTextureRegionBuffer.add(baseTextureRegion);
        this.mIndex++;
    }

    public void drawWithoutChecks(BaseTextureRegion baseTextureRegion, float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8) {
        this.mSpriteBatchVertexBuffer.addInner(f, f2, f3, f4, f5, f6, f7, f8);
        this.mSpriteBatchTextureRegionBuffer.add(baseTextureRegion);
        this.mIndex++;
    }

    public void draw(BaseSprite baseSprite) {
        if (baseSprite.isVisible()) {
            assertCapacity();
            BaseTextureRegion textureRegion = baseSprite.getTextureRegion();
            assertTexture(textureRegion);
            if (baseSprite.getRotation() != 0.0f || baseSprite.isScaled()) {
                this.mSpriteBatchVertexBuffer.add(baseSprite.getWidth(), baseSprite.getHeight(), baseSprite.getLocalToParentTransformation());
            } else {
                this.mSpriteBatchVertexBuffer.add(baseSprite.getX(), baseSprite.getY(), baseSprite.getWidth(), baseSprite.getHeight());
            }
            this.mSpriteBatchTextureRegionBuffer.add(textureRegion);
            this.mIndex++;
        }
    }

    public void drawWithoutChecks(BaseSprite baseSprite) {
        if (baseSprite.isVisible()) {
            BaseTextureRegion textureRegion = baseSprite.getTextureRegion();
            if (baseSprite.getRotation() != 0.0f || baseSprite.isScaled()) {
                this.mSpriteBatchVertexBuffer.add(baseSprite.getWidth(), baseSprite.getHeight(), baseSprite.getLocalToParentTransformation());
            } else {
                this.mSpriteBatchVertexBuffer.add(baseSprite.getX(), baseSprite.getY(), baseSprite.getWidth(), baseSprite.getHeight());
            }
            this.mSpriteBatchTextureRegionBuffer.add(textureRegion);
            this.mIndex++;
        }
    }

    public void submit() {
        onSubmit();
    }

    private void onSubmit() {
        this.mVertices = this.mIndex * 6;
        this.mSpriteBatchVertexBuffer.submit();
        this.mSpriteBatchTextureRegionBuffer.submit();
        this.mIndex = 0;
        this.mSpriteBatchVertexBuffer.setIndex(0);
        this.mSpriteBatchTextureRegionBuffer.setIndex(0);
    }

    private void initBlendFunction() {
        if (this.mTexture.getTextureOptions().mPreMultipyAlpha) {
            setBlendFunction(1, 771);
        }
    }

    /* access modifiers changed from: protected */
    public void onInitDraw(GL10 gl10) {
        GLHelper.setColor(gl10, this.mRed, this.mGreen, this.mBlue, this.mAlpha);
        GLHelper.enableVertexArray(gl10);
        GLHelper.blendFunction(gl10, this.mSourceBlendFunction, this.mDestinationBlendFunction);
        GLHelper.enableTextures(gl10);
        GLHelper.enableTexCoordArray(gl10);
    }

    /* access modifiers changed from: protected */
    public void onApplyVertices(GL10 gl10) {
        if (GLHelper.EXTENSIONS_VERTEXBUFFEROBJECTS) {
            GL11 gl11 = (GL11) gl10;
            this.mSpriteBatchVertexBuffer.selectOnHardware(gl11);
            GLHelper.vertexZeroPointer(gl11);
            return;
        }
        GLHelper.vertexPointer(gl10, this.mSpriteBatchVertexBuffer.getFloatBuffer());
    }

    private void onApplyTextureRegion(GL10 gl10) {
        if (GLHelper.EXTENSIONS_VERTEXBUFFEROBJECTS) {
            GL11 gl11 = (GL11) gl10;
            this.mSpriteBatchTextureRegionBuffer.selectOnHardware(gl11);
            this.mTexture.bind(gl10);
            GLHelper.texCoordZeroPointer(gl11);
            return;
        }
        this.mTexture.bind(gl10);
        GLHelper.texCoordPointer(gl10, this.mSpriteBatchTextureRegionBuffer.getFloatBuffer());
    }

    private void drawVertices(GL10 gl10, Camera camera) {
        gl10.glDrawArrays(4, 0, this.mVertices);
    }

    private void assertCapacity(int i) {
        if (i >= this.mCapacity) {
            throw new IllegalStateException("This supplied pIndex: '" + i + "' is exceeding the capacity: '" + this.mCapacity + "' of this SpriteBatch!");
        }
    }

    private void assertCapacity() {
        if (this.mIndex == this.mCapacity) {
            throw new IllegalStateException("This SpriteBatch has already reached its capacity (" + this.mCapacity + ") !");
        }
    }

    /* access modifiers changed from: protected */
    public void assertTexture(BaseTextureRegion baseTextureRegion) {
        if (baseTextureRegion.getTexture() != this.mTexture) {
            throw new IllegalArgumentException("The supplied Texture does match the Texture of this SpriteBatch!");
        }
    }
}
