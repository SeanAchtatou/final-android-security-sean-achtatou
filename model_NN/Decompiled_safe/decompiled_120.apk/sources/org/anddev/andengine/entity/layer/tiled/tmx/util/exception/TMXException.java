package org.anddev.andengine.entity.layer.tiled.tmx.util.exception;

public abstract class TMXException extends Exception {
    private static final long serialVersionUID = 337819550394833109L;

    public TMXException() {
    }

    public TMXException(String str, Throwable th) {
        super(str, th);
    }

    public TMXException(String str) {
        super(str);
    }

    public TMXException(Throwable th) {
        super(th);
    }
}
