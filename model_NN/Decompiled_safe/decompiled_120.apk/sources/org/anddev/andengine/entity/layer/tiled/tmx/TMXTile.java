package org.anddev.andengine.entity.layer.tiled.tmx;

import org.anddev.andengine.opengl.texture.region.TextureRegion;

public class TMXTile {
    int mGlobalTileID;
    TextureRegion mTextureRegion;
    private final int mTileColumn;
    private final int mTileHeight;
    private final int mTileRow;
    private final int mTileWidth;

    public TMXTile(int i, int i2, int i3, int i4, int i5, TextureRegion textureRegion) {
        this.mGlobalTileID = i;
        this.mTileRow = i3;
        this.mTileColumn = i2;
        this.mTileWidth = i4;
        this.mTileHeight = i5;
        this.mTextureRegion = textureRegion;
    }

    public int getGlobalTileID() {
        return this.mGlobalTileID;
    }

    public int getTileRow() {
        return this.mTileRow;
    }

    public int getTileColumn() {
        return this.mTileColumn;
    }

    public int getTileX() {
        return this.mTileColumn * this.mTileWidth;
    }

    public int getTileY() {
        return this.mTileRow * this.mTileHeight;
    }

    public int getTileWidth() {
        return this.mTileWidth;
    }

    public int getTileHeight() {
        return this.mTileHeight;
    }

    public TextureRegion getTextureRegion() {
        return this.mTextureRegion;
    }

    public void setGlobalTileID(TMXTiledMap tMXTiledMap, int i) {
        this.mGlobalTileID = i;
        this.mTextureRegion = tMXTiledMap.getTextureRegionFromGlobalTileID(i);
    }

    public void setTextureRegion(TextureRegion textureRegion) {
        this.mTextureRegion = textureRegion;
    }

    public TMXProperties getTMXTileProperties(TMXTiledMap tMXTiledMap) {
        return tMXTiledMap.getTMXTileProperties(this.mGlobalTileID);
    }
}
