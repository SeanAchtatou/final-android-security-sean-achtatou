package org.anddev.andengine.entity.modifier;

import org.anddev.andengine.entity.IEntity;
import org.anddev.andengine.entity.modifier.IEntityModifier;
import org.anddev.andengine.util.modifier.ease.IEaseFunction;

public class ColorModifier extends TripleValueSpanEntityModifier {
    public ColorModifier(float f, float f2, float f3, float f4, float f5, float f6, float f7) {
        this(f, f2, f3, f4, f5, f6, f7, null, IEaseFunction.DEFAULT);
    }

    public ColorModifier(float f, float f2, float f3, float f4, float f5, float f6, float f7, IEaseFunction iEaseFunction) {
        this(f, f2, f3, f4, f5, f6, f7, null, iEaseFunction);
    }

    public ColorModifier(float f, float f2, float f3, float f4, float f5, float f6, float f7, IEntityModifier.IEntityModifierListener iEntityModifierListener) {
        super(f, f2, f3, f4, f5, f6, f7, iEntityModifierListener, IEaseFunction.DEFAULT);
    }

    public ColorModifier(float f, float f2, float f3, float f4, float f5, float f6, float f7, IEntityModifier.IEntityModifierListener iEntityModifierListener, IEaseFunction iEaseFunction) {
        super(f, f2, f3, f4, f5, f6, f7, iEntityModifierListener, iEaseFunction);
    }

    protected ColorModifier(ColorModifier colorModifier) {
        super(colorModifier);
    }

    public ColorModifier clone() {
        return new ColorModifier(this);
    }

    /* access modifiers changed from: protected */
    public void onSetInitialValues(IEntity iEntity, float f, float f2, float f3) {
        iEntity.setColor(f, f2, f3);
    }

    /* access modifiers changed from: protected */
    public void onSetValues(IEntity iEntity, float f, float f2, float f3, float f4) {
        iEntity.setColor(f2, f3, f4);
    }
}
