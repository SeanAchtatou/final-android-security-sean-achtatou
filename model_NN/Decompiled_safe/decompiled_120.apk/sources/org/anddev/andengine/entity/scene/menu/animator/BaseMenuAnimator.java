package org.anddev.andengine.entity.scene.menu.animator;

import java.util.ArrayList;
import org.anddev.andengine.entity.scene.menu.item.IMenuItem;
import org.anddev.andengine.util.HorizontalAlign;
import org.anddev.andengine.util.modifier.ease.IEaseFunction;

public abstract class BaseMenuAnimator implements IMenuAnimator {
    protected static final float DURATION = 1.0f;
    private static final HorizontalAlign HORIZONTALALIGN_DEFAULT = HorizontalAlign.CENTER;
    private static final float MENUITEMSPACING_DEFAULT = 1.0f;
    protected final IEaseFunction mEaseFunction;
    protected final HorizontalAlign mHorizontalAlign;
    protected final float mMenuItemSpacing;

    public BaseMenuAnimator() {
        this(1.0f);
    }

    public BaseMenuAnimator(IEaseFunction iEaseFunction) {
        this(1.0f, iEaseFunction);
    }

    public BaseMenuAnimator(float f) {
        this(HORIZONTALALIGN_DEFAULT, f);
    }

    public BaseMenuAnimator(float f, IEaseFunction iEaseFunction) {
        this(HORIZONTALALIGN_DEFAULT, f, iEaseFunction);
    }

    public BaseMenuAnimator(HorizontalAlign horizontalAlign) {
        this(horizontalAlign, 1.0f);
    }

    public BaseMenuAnimator(HorizontalAlign horizontalAlign, IEaseFunction iEaseFunction) {
        this(horizontalAlign, 1.0f, iEaseFunction);
    }

    public BaseMenuAnimator(HorizontalAlign horizontalAlign, float f) {
        this(horizontalAlign, f, IEaseFunction.DEFAULT);
    }

    public BaseMenuAnimator(HorizontalAlign horizontalAlign, float f, IEaseFunction iEaseFunction) {
        this.mHorizontalAlign = horizontalAlign;
        this.mMenuItemSpacing = f;
        this.mEaseFunction = iEaseFunction;
    }

    /* access modifiers changed from: protected */
    public float getMaximumWidth(ArrayList arrayList) {
        float f = Float.MIN_VALUE;
        for (int size = arrayList.size() - 1; size >= 0; size--) {
            f = Math.max(f, ((IMenuItem) arrayList.get(size)).getWidthScaled());
        }
        return f;
    }

    /* access modifiers changed from: protected */
    public float getOverallHeight(ArrayList arrayList) {
        int size = arrayList.size() - 1;
        float f = 0.0f;
        while (size >= 0) {
            size--;
            f = ((IMenuItem) arrayList.get(size)).getHeight() + f;
        }
        return (((float) (arrayList.size() - 1)) * this.mMenuItemSpacing) + f;
    }
}
