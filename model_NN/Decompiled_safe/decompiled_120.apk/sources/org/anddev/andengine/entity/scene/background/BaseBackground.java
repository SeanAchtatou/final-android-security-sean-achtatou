package org.anddev.andengine.entity.scene.background;

import org.anddev.andengine.util.modifier.IModifier;
import org.anddev.andengine.util.modifier.ModifierList;

public abstract class BaseBackground implements IBackground {
    private static final int BACKGROUNDMODIFIERS_CAPACITY_DEFAULT = 4;
    private final ModifierList mBackgroundModifiers = new ModifierList(this, 4);

    public void addBackgroundModifier(IModifier iModifier) {
        this.mBackgroundModifiers.add(iModifier);
    }

    public boolean removeBackgroundModifier(IModifier iModifier) {
        return this.mBackgroundModifiers.remove(iModifier);
    }

    public void clearBackgroundModifiers() {
        this.mBackgroundModifiers.clear();
    }

    public void onUpdate(float f) {
        this.mBackgroundModifiers.onUpdate(f);
    }

    public void reset() {
        this.mBackgroundModifiers.reset();
    }
}
