package org.anddev.andengine.entity.particle.initializer;

import org.anddev.andengine.entity.particle.Particle;
import org.anddev.andengine.util.MathUtils;

public abstract class BaseTripleValueInitializer extends BaseDoubleValueInitializer {
    protected float mMaxValueC;
    protected float mMinValueC;

    /* access modifiers changed from: protected */
    public abstract void onInitializeParticle(Particle particle, float f, float f2, float f3);

    public BaseTripleValueInitializer(float f, float f2, float f3, float f4, float f5, float f6) {
        super(f, f2, f3, f4);
        this.mMinValueC = f5;
        this.mMaxValueC = f6;
    }

    /* access modifiers changed from: protected */
    public final void onInitializeParticle(Particle particle, float f, float f2) {
        onInitializeParticle(particle, f, f2, getRandomValueC());
    }

    private final float getRandomValueC() {
        if (this.mMinValueC == this.mMaxValueC) {
            return this.mMaxValueC;
        }
        return (MathUtils.RANDOM.nextFloat() * (this.mMaxValueC - this.mMinValueC)) + this.mMinValueC;
    }
}
