package org.anddev.andengine.entity.layer.tiled.tmx.util.exception;

public class TSXLoadException extends TMXException {
    private static final long serialVersionUID = 10055223972707703L;

    public TSXLoadException() {
    }

    public TSXLoadException(String str, Throwable th) {
        super(str, th);
    }

    public TSXLoadException(String str) {
        super(str);
    }

    public TSXLoadException(Throwable th) {
        super(th);
    }
}
