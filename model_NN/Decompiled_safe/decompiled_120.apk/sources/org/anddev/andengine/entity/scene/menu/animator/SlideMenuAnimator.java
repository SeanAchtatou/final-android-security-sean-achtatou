package org.anddev.andengine.entity.scene.menu.animator;

import java.util.ArrayList;
import org.anddev.andengine.entity.modifier.MoveModifier;
import org.anddev.andengine.entity.scene.menu.item.IMenuItem;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.util.HorizontalAlign;
import org.anddev.andengine.util.modifier.ease.IEaseFunction;

public class SlideMenuAnimator extends BaseMenuAnimator {
    private static /* synthetic */ int[] $SWITCH_TABLE$org$anddev$andengine$util$HorizontalAlign;

    static /* synthetic */ int[] $SWITCH_TABLE$org$anddev$andengine$util$HorizontalAlign() {
        int[] iArr = $SWITCH_TABLE$org$anddev$andengine$util$HorizontalAlign;
        if (iArr == null) {
            iArr = new int[HorizontalAlign.values().length];
            try {
                iArr[HorizontalAlign.CENTER.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[HorizontalAlign.LEFT.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[HorizontalAlign.RIGHT.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$org$anddev$andengine$util$HorizontalAlign = iArr;
        }
        return iArr;
    }

    public SlideMenuAnimator() {
    }

    public SlideMenuAnimator(IEaseFunction iEaseFunction) {
        super(iEaseFunction);
    }

    public SlideMenuAnimator(HorizontalAlign horizontalAlign) {
        super(horizontalAlign);
    }

    public SlideMenuAnimator(HorizontalAlign horizontalAlign, IEaseFunction iEaseFunction) {
        super(horizontalAlign, iEaseFunction);
    }

    public SlideMenuAnimator(float f) {
        super(f);
    }

    public SlideMenuAnimator(float f, IEaseFunction iEaseFunction) {
        super(f, iEaseFunction);
    }

    public SlideMenuAnimator(HorizontalAlign horizontalAlign, float f) {
        super(horizontalAlign, f);
    }

    public SlideMenuAnimator(HorizontalAlign horizontalAlign, float f, IEaseFunction iEaseFunction) {
        super(horizontalAlign, f, iEaseFunction);
    }

    public void buildAnimations(ArrayList arrayList, float f, float f2) {
        float widthScaled;
        IEaseFunction iEaseFunction = this.mEaseFunction;
        float maximumWidth = getMaximumWidth(arrayList);
        float f3 = (f - maximumWidth) * 0.5f;
        float overallHeight = (f2 - getOverallHeight(arrayList)) * 0.5f;
        int size = arrayList.size();
        int i = 0;
        float f4 = 0.0f;
        while (i < size) {
            IMenuItem iMenuItem = (IMenuItem) arrayList.get(i);
            switch ($SWITCH_TABLE$org$anddev$andengine$util$HorizontalAlign()[this.mHorizontalAlign.ordinal()]) {
                case 1:
                    widthScaled = 0.0f;
                    break;
                case 2:
                default:
                    widthScaled = (maximumWidth - iMenuItem.getWidthScaled()) * 0.5f;
                    break;
                case TouchEvent.ACTION_CANCEL /*3*/:
                    widthScaled = maximumWidth - iMenuItem.getWidthScaled();
                    break;
            }
            MoveModifier moveModifier = new MoveModifier(1.0f, -maximumWidth, widthScaled + f3, overallHeight + f4, overallHeight + f4, iEaseFunction);
            moveModifier.setRemoveWhenFinished(false);
            iMenuItem.registerEntityModifier(moveModifier);
            i++;
            f4 = iMenuItem.getHeight() + this.mMenuItemSpacing + f4;
        }
    }

    public void prepareAnimations(ArrayList arrayList, float f, float f2) {
        float maximumWidth = getMaximumWidth(arrayList);
        float overallHeight = (f2 - getOverallHeight(arrayList)) * 0.5f;
        float f3 = this.mMenuItemSpacing;
        int size = arrayList.size();
        float f4 = 0.0f;
        for (int i = 0; i < size; i++) {
            IMenuItem iMenuItem = (IMenuItem) arrayList.get(i);
            iMenuItem.setPosition(-maximumWidth, overallHeight + f4);
            f4 += iMenuItem.getHeight() + f3;
        }
    }
}
