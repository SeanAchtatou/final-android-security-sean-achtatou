package org.anddev.andengine.entity.scene.menu.item.decorator;

import org.anddev.andengine.entity.scene.menu.item.IMenuItem;

public class ScaleMenuItemDecorator extends BaseMenuItemDecorator {
    private final float mSelectedScale;
    private final float mUnselectedScale;

    public ScaleMenuItemDecorator(IMenuItem iMenuItem, float f, float f2) {
        super(iMenuItem);
        this.mSelectedScale = f;
        this.mUnselectedScale = f2;
        iMenuItem.setScale(f2);
    }

    public void onMenuItemSelected(IMenuItem iMenuItem) {
        setScale(this.mSelectedScale);
    }

    public void onMenuItemUnselected(IMenuItem iMenuItem) {
        setScale(this.mUnselectedScale);
    }

    public void onMenuItemReset(IMenuItem iMenuItem) {
        setScale(this.mUnselectedScale);
    }
}
