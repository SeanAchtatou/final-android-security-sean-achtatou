package org.anddev.andengine.entity.layer.tiled.tmx;

import android.util.SparseArray;
import java.util.ArrayList;
import org.anddev.andengine.entity.layer.tiled.tmx.util.constants.TMXConstants;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.vertex.RectangleVertexBuffer;
import org.anddev.andengine.util.SAXUtils;
import org.xml.sax.Attributes;

public class TMXTiledMap implements TMXConstants {
    private final SparseArray mGlobalTileIDToTMXTilePropertiesCache = new SparseArray();
    private final SparseArray mGlobalTileIDToTextureRegionCache = new SparseArray();
    private final String mOrientation;
    private final RectangleVertexBuffer mSharedVertexBuffer;
    private final ArrayList mTMXLayers = new ArrayList();
    private final ArrayList mTMXObjectGroups = new ArrayList();
    private final ArrayList mTMXTileSets = new ArrayList();
    private final TMXProperties mTMXTiledMapProperties = new TMXProperties();
    private final int mTileColumns;
    private final int mTileHeight;
    private final int mTileWidth;
    private final int mTilesRows;

    TMXTiledMap(Attributes attributes) {
        this.mOrientation = attributes.getValue("", TMXConstants.TAG_MAP_ATTRIBUTE_ORIENTATION);
        if (!this.mOrientation.equals(TMXConstants.TAG_MAP_ATTRIBUTE_ORIENTATION_VALUE_ORTHOGONAL)) {
            throw new IllegalArgumentException("orientation: '" + this.mOrientation + "' is not supported.");
        }
        this.mTileColumns = SAXUtils.getIntAttributeOrThrow(attributes, "width");
        this.mTilesRows = SAXUtils.getIntAttributeOrThrow(attributes, "height");
        this.mTileWidth = SAXUtils.getIntAttributeOrThrow(attributes, "tilewidth");
        this.mTileHeight = SAXUtils.getIntAttributeOrThrow(attributes, "tileheight");
        this.mSharedVertexBuffer = new RectangleVertexBuffer(35044, true);
        this.mSharedVertexBuffer.update((float) this.mTileWidth, (float) this.mTileHeight);
    }

    public final String getOrientation() {
        return this.mOrientation;
    }

    public final int getWidth() {
        return this.mTileColumns;
    }

    public final int getTileColumns() {
        return this.mTileColumns;
    }

    public final int getHeight() {
        return this.mTilesRows;
    }

    public final int getTileRows() {
        return this.mTilesRows;
    }

    public final int getTileWidth() {
        return this.mTileWidth;
    }

    public final int getTileHeight() {
        return this.mTileHeight;
    }

    public RectangleVertexBuffer getSharedVertexBuffer() {
        return this.mSharedVertexBuffer;
    }

    /* access modifiers changed from: package-private */
    public void addTMXTileSet(TMXTileSet tMXTileSet) {
        this.mTMXTileSets.add(tMXTileSet);
    }

    public ArrayList getTMXTileSets() {
        return this.mTMXTileSets;
    }

    /* access modifiers changed from: package-private */
    public void addTMXLayer(TMXLayer tMXLayer) {
        this.mTMXLayers.add(tMXLayer);
    }

    public ArrayList getTMXLayers() {
        return this.mTMXLayers;
    }

    /* access modifiers changed from: package-private */
    public void addTMXObjectGroup(TMXObjectGroup tMXObjectGroup) {
        this.mTMXObjectGroups.add(tMXObjectGroup);
    }

    public ArrayList getTMXObjectGroups() {
        return this.mTMXObjectGroups;
    }

    public TMXProperties getTMXTilePropertiesByGlobalTileID(int i) {
        return (TMXProperties) this.mGlobalTileIDToTMXTilePropertiesCache.get(i);
    }

    public void addTMXTiledMapProperty(TMXTiledMapProperty tMXTiledMapProperty) {
        this.mTMXTiledMapProperties.add(tMXTiledMapProperty);
    }

    public TMXProperties getTMXTiledMapProperties() {
        return this.mTMXTiledMapProperties;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        if (this.mSharedVertexBuffer.isManaged()) {
            this.mSharedVertexBuffer.unloadFromActiveBufferObjectManager();
        }
    }

    public TMXProperties getTMXTileProperties(int i) {
        TMXProperties tMXProperties = (TMXProperties) this.mGlobalTileIDToTMXTilePropertiesCache.get(i);
        if (tMXProperties != null) {
            return tMXProperties;
        }
        ArrayList arrayList = this.mTMXTileSets;
        for (int size = arrayList.size() - 1; size >= 0; size--) {
            TMXTileSet tMXTileSet = (TMXTileSet) arrayList.get(size);
            if (i >= tMXTileSet.getFirstGlobalTileID()) {
                return tMXTileSet.getTMXTilePropertiesFromGlobalTileID(i);
            }
        }
        throw new IllegalArgumentException("No TMXTileProperties found for pGlobalTileID=" + i);
    }

    public TextureRegion getTextureRegionFromGlobalTileID(int i) {
        SparseArray sparseArray = this.mGlobalTileIDToTextureRegionCache;
        TextureRegion textureRegion = (TextureRegion) sparseArray.get(i);
        if (textureRegion != null) {
            return textureRegion;
        }
        ArrayList arrayList = this.mTMXTileSets;
        for (int size = arrayList.size() - 1; size >= 0; size--) {
            TMXTileSet tMXTileSet = (TMXTileSet) arrayList.get(size);
            if (i >= tMXTileSet.getFirstGlobalTileID()) {
                TextureRegion textureRegionFromGlobalTileID = tMXTileSet.getTextureRegionFromGlobalTileID(i);
                sparseArray.put(i, textureRegionFromGlobalTileID);
                return textureRegionFromGlobalTileID;
            }
        }
        throw new IllegalArgumentException("No TextureRegion found for pGlobalTileID=" + i);
    }
}
