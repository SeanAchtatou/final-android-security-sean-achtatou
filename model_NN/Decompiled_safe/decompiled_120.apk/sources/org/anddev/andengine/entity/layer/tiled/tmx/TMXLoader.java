package org.anddev.andengine.entity.layer.tiled.tmx;

import android.content.Context;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import org.anddev.andengine.entity.layer.tiled.tmx.util.exception.TMXLoadException;
import org.anddev.andengine.opengl.texture.TextureManager;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

public class TMXLoader {
    private final Context mContext;
    private final ITMXTilePropertiesListener mTMXTilePropertyListener;
    private final TextureManager mTextureManager;
    private final TextureOptions mTextureOptions;

    public interface ITMXTilePropertiesListener {
        void onTMXTileWithPropertiesCreated(TMXTiledMap tMXTiledMap, TMXLayer tMXLayer, TMXTile tMXTile, TMXProperties tMXProperties);
    }

    public TMXLoader(Context context, TextureManager textureManager) {
        this(context, textureManager, TextureOptions.DEFAULT);
    }

    public TMXLoader(Context context, TextureManager textureManager, TextureOptions textureOptions) {
        this(context, textureManager, textureOptions, null);
    }

    public TMXLoader(Context context, TextureManager textureManager, ITMXTilePropertiesListener iTMXTilePropertiesListener) {
        this(context, textureManager, TextureOptions.DEFAULT, iTMXTilePropertiesListener);
    }

    public TMXLoader(Context context, TextureManager textureManager, TextureOptions textureOptions, ITMXTilePropertiesListener iTMXTilePropertiesListener) {
        this.mContext = context;
        this.mTextureManager = textureManager;
        this.mTextureOptions = textureOptions;
        this.mTMXTilePropertyListener = iTMXTilePropertiesListener;
    }

    public TMXTiledMap loadFromAsset(Context context, String str) {
        try {
            return load(context.getAssets().open(str));
        } catch (IOException e) {
            throw new TMXLoadException("Could not load TMXTiledMap from asset: " + str, e);
        }
    }

    public TMXTiledMap load(InputStream inputStream) {
        try {
            XMLReader xMLReader = SAXParserFactory.newInstance().newSAXParser().getXMLReader();
            TMXParser tMXParser = new TMXParser(this.mContext, this.mTextureManager, this.mTextureOptions, this.mTMXTilePropertyListener);
            xMLReader.setContentHandler(tMXParser);
            xMLReader.parse(new InputSource(new BufferedInputStream(inputStream)));
            return tMXParser.getTMXTiledMap();
        } catch (SAXException e) {
            throw new TMXLoadException(e);
        } catch (ParserConfigurationException e2) {
            return null;
        } catch (IOException e3) {
            throw new TMXLoadException(e3);
        }
    }
}
