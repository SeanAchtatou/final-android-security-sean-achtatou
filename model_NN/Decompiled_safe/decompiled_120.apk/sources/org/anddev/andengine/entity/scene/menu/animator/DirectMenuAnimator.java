package org.anddev.andengine.entity.scene.menu.animator;

import java.util.ArrayList;
import org.anddev.andengine.entity.scene.menu.item.IMenuItem;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.util.HorizontalAlign;

public class DirectMenuAnimator extends BaseMenuAnimator {
    private static /* synthetic */ int[] $SWITCH_TABLE$org$anddev$andengine$util$HorizontalAlign;

    static /* synthetic */ int[] $SWITCH_TABLE$org$anddev$andengine$util$HorizontalAlign() {
        int[] iArr = $SWITCH_TABLE$org$anddev$andengine$util$HorizontalAlign;
        if (iArr == null) {
            iArr = new int[HorizontalAlign.values().length];
            try {
                iArr[HorizontalAlign.CENTER.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[HorizontalAlign.LEFT.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[HorizontalAlign.RIGHT.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$org$anddev$andengine$util$HorizontalAlign = iArr;
        }
        return iArr;
    }

    public DirectMenuAnimator() {
    }

    public DirectMenuAnimator(HorizontalAlign horizontalAlign) {
        super(horizontalAlign);
    }

    public DirectMenuAnimator(float f) {
        super(f);
    }

    public DirectMenuAnimator(HorizontalAlign horizontalAlign, float f) {
        super(horizontalAlign, f);
    }

    public void buildAnimations(ArrayList arrayList, float f, float f2) {
    }

    public void prepareAnimations(ArrayList arrayList, float f, float f2) {
        float widthScaled;
        float maximumWidth = getMaximumWidth(arrayList);
        float f3 = (f - maximumWidth) * 0.5f;
        float overallHeight = (f2 - getOverallHeight(arrayList)) * 0.5f;
        float f4 = this.mMenuItemSpacing;
        int size = arrayList.size();
        int i = 0;
        float f5 = 0.0f;
        while (i < size) {
            IMenuItem iMenuItem = (IMenuItem) arrayList.get(i);
            switch ($SWITCH_TABLE$org$anddev$andengine$util$HorizontalAlign()[this.mHorizontalAlign.ordinal()]) {
                case 1:
                    widthScaled = 0.0f;
                    break;
                case 2:
                default:
                    widthScaled = (maximumWidth - iMenuItem.getWidthScaled()) * 0.5f;
                    break;
                case TouchEvent.ACTION_CANCEL /*3*/:
                    widthScaled = maximumWidth - iMenuItem.getWidthScaled();
                    break;
            }
            iMenuItem.setPosition(widthScaled + f3, overallHeight + f5);
            i++;
            f5 = iMenuItem.getHeight() + f4 + f5;
        }
    }
}
