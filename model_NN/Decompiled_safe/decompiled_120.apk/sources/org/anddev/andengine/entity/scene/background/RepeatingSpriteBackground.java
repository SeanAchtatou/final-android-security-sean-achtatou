package org.anddev.andengine.entity.scene.background;

import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.opengl.texture.TextureManager;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.anddev.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource;
import org.anddev.andengine.opengl.texture.bitmap.BitmapTexture;
import org.anddev.andengine.opengl.texture.region.TextureRegion;

public class RepeatingSpriteBackground extends SpriteBackground {
    private BitmapTextureAtlas mBitmapTextureAtlas;
    private final float mScale;

    public RepeatingSpriteBackground(float f, float f2, TextureManager textureManager, IBitmapTextureAtlasSource iBitmapTextureAtlasSource) {
        this(f, f2, textureManager, iBitmapTextureAtlasSource, 1.0f);
    }

    public RepeatingSpriteBackground(float f, float f2, TextureManager textureManager, IBitmapTextureAtlasSource iBitmapTextureAtlasSource, float f3) {
        super(null);
        this.mScale = f3;
        this.mEntity = loadSprite(f, f2, textureManager, iBitmapTextureAtlasSource);
    }

    public BitmapTextureAtlas getBitmapTextureAtlas() {
        return this.mBitmapTextureAtlas;
    }

    private Sprite loadSprite(float f, float f2, TextureManager textureManager, IBitmapTextureAtlasSource iBitmapTextureAtlasSource) {
        this.mBitmapTextureAtlas = new BitmapTextureAtlas(iBitmapTextureAtlasSource.getWidth(), iBitmapTextureAtlasSource.getHeight(), BitmapTexture.BitmapTextureFormat.RGBA_8888, TextureOptions.REPEATING_NEAREST_PREMULTIPLYALPHA);
        TextureRegion createFromSource = BitmapTextureAtlasTextureRegionFactory.createFromSource(this.mBitmapTextureAtlas, iBitmapTextureAtlasSource, 0, 0);
        int round = Math.round(f / this.mScale);
        int round2 = Math.round(f2 / this.mScale);
        createFromSource.setWidth(round);
        createFromSource.setHeight(round2);
        textureManager.loadTexture(this.mBitmapTextureAtlas);
        Sprite sprite = new Sprite(0.0f, 0.0f, (float) round, (float) round2, createFromSource);
        sprite.setScaleCenter(0.0f, 0.0f);
        sprite.setScale(this.mScale);
        return sprite;
    }
}
