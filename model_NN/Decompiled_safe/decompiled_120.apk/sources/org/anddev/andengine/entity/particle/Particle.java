package org.anddev.andengine.entity.particle;

import org.anddev.andengine.engine.handler.physics.PhysicsHandler;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.vertex.RectangleVertexBuffer;

public class Particle extends Sprite {
    boolean mDead = false;
    private float mDeathTime = -1.0f;
    private float mLifeTime = 0.0f;
    private final PhysicsHandler mPhysicsHandler = new PhysicsHandler(this);

    public Particle(float f, float f2, TextureRegion textureRegion) {
        super(f, f2, textureRegion);
    }

    public Particle(float f, float f2, TextureRegion textureRegion, RectangleVertexBuffer rectangleVertexBuffer) {
        super(f, f2, textureRegion, rectangleVertexBuffer);
    }

    public float getLifeTime() {
        return this.mLifeTime;
    }

    public float getDeathTime() {
        return this.mDeathTime;
    }

    public void setDeathTime(float f) {
        this.mDeathTime = f;
    }

    public boolean isDead() {
        return this.mDead;
    }

    public void setDead(boolean z) {
        this.mDead = z;
    }

    public PhysicsHandler getPhysicsHandler() {
        return this.mPhysicsHandler;
    }

    /* access modifiers changed from: protected */
    public void onManagedUpdate(float f) {
        if (!this.mDead) {
            this.mLifeTime += f;
            this.mPhysicsHandler.onUpdate(f);
            super.onManagedUpdate(f);
            float f2 = this.mDeathTime;
            if (f2 != -1.0f && this.mLifeTime > f2) {
                setDead(true);
            }
        }
    }

    public void reset() {
        super.reset();
        this.mPhysicsHandler.reset();
        this.mDead = false;
        this.mDeathTime = -1.0f;
        this.mLifeTime = 0.0f;
    }
}
