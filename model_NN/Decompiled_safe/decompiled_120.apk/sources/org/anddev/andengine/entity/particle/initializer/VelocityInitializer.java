package org.anddev.andengine.entity.particle.initializer;

import org.anddev.andengine.entity.particle.Particle;

public class VelocityInitializer extends BaseDoubleValueInitializer {
    public VelocityInitializer(float f) {
        this(f, f, f, f);
    }

    public VelocityInitializer(float f, float f2) {
        this(f, f, f2, f2);
    }

    public VelocityInitializer(float f, float f2, float f3, float f4) {
        super(f, f2, f3, f4);
    }

    public float getMinVelocityX() {
        return this.mMinValue;
    }

    public float getMaxVelocityX() {
        return this.mMaxValue;
    }

    public float getMinVelocityY() {
        return this.mMinValueB;
    }

    public float getMaxVelocityY() {
        return this.mMaxValueB;
    }

    public void setVelocityX(float f) {
        this.mMinValue = f;
        this.mMaxValue = f;
    }

    public void setVelocityY(float f) {
        this.mMinValueB = f;
        this.mMaxValueB = f;
    }

    public void setVelocityX(float f, float f2) {
        this.mMinValue = f;
        this.mMaxValue = f2;
    }

    public void setVelocityY(float f, float f2) {
        this.mMinValueB = f;
        this.mMaxValueB = f2;
    }

    public void setVelocity(float f, float f2, float f3, float f4) {
        this.mMinValue = f;
        this.mMaxValue = f2;
        this.mMinValueB = f3;
        this.mMaxValueB = f4;
    }

    public void onInitializeParticle(Particle particle, float f, float f2) {
        particle.getPhysicsHandler().setVelocity(f, f2);
    }
}
