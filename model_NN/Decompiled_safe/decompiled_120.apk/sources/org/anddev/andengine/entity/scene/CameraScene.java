package org.anddev.andengine.entity.scene;

import javax.microedition.khronos.opengles.GL10;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.entity.shape.Shape;
import org.anddev.andengine.input.touch.TouchEvent;

public class CameraScene extends Scene {
    protected Camera mCamera;

    public CameraScene() {
        this(null);
    }

    public CameraScene(Camera camera) {
        this.mCamera = camera;
    }

    public Camera getCamera() {
        return this.mCamera;
    }

    public void setCamera(Camera camera) {
        this.mCamera = camera;
    }

    public boolean onSceneTouchEvent(TouchEvent touchEvent) {
        if (this.mCamera == null) {
            return false;
        }
        this.mCamera.convertSceneToCameraSceneTouchEvent(touchEvent);
        if (super.onSceneTouchEvent(touchEvent)) {
            return true;
        }
        this.mCamera.convertCameraSceneToSceneTouchEvent(touchEvent);
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean onChildSceneTouchEvent(TouchEvent touchEvent) {
        if (!(this.mChildScene instanceof CameraScene)) {
            return super.onChildSceneTouchEvent(touchEvent);
        }
        this.mCamera.convertCameraSceneToSceneTouchEvent(touchEvent);
        boolean onChildSceneTouchEvent = super.onChildSceneTouchEvent(touchEvent);
        this.mCamera.convertSceneToCameraSceneTouchEvent(touchEvent);
        return onChildSceneTouchEvent;
    }

    /* access modifiers changed from: protected */
    public void onManagedDraw(GL10 gl10, Camera camera) {
        if (this.mCamera != null) {
            gl10.glMatrixMode(5889);
            this.mCamera.onApplyCameraSceneMatrix(gl10);
            gl10.glMatrixMode(5888);
            gl10.glPushMatrix();
            gl10.glLoadIdentity();
            super.onManagedDraw(gl10, camera);
            gl10.glPopMatrix();
            gl10.glMatrixMode(5889);
        }
    }

    public void centerShapeInCamera(Shape shape) {
        Camera camera = this.mCamera;
        shape.setPosition((camera.getWidth() - shape.getWidth()) * 0.5f, (camera.getHeight() - shape.getHeight()) * 0.5f);
    }

    public void centerShapeInCameraHorizontally(Shape shape) {
        shape.setPosition((this.mCamera.getWidth() - shape.getWidth()) * 0.5f, shape.getY());
    }

    public void centerShapeInCameraVertically(Shape shape) {
        shape.setPosition(shape.getX(), (this.mCamera.getHeight() - shape.getHeight()) * 0.5f);
    }
}
