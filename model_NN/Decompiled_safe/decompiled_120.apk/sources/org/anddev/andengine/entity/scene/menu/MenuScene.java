package org.anddev.andengine.entity.scene.menu;

import java.util.ArrayList;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.entity.scene.CameraScene;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.scene.menu.animator.IMenuAnimator;
import org.anddev.andengine.entity.scene.menu.item.IMenuItem;
import org.anddev.andengine.input.touch.TouchEvent;

public class MenuScene extends CameraScene implements Scene.IOnAreaTouchListener, Scene.IOnSceneTouchListener {
    private IMenuAnimator mMenuAnimator;
    private final ArrayList mMenuItems;
    private IOnMenuItemClickListener mOnMenuItemClickListener;
    private IMenuItem mSelectedMenuItem;

    public interface IOnMenuItemClickListener {
        boolean onMenuItemClicked(MenuScene menuScene, IMenuItem iMenuItem, float f, float f2);
    }

    public MenuScene() {
        this(null, null);
    }

    public MenuScene(IOnMenuItemClickListener iOnMenuItemClickListener) {
        this(null, iOnMenuItemClickListener);
    }

    public MenuScene(Camera camera) {
        this(camera, null);
    }

    public MenuScene(Camera camera, IOnMenuItemClickListener iOnMenuItemClickListener) {
        super(camera);
        this.mMenuItems = new ArrayList();
        this.mMenuAnimator = IMenuAnimator.DEFAULT;
        this.mOnMenuItemClickListener = iOnMenuItemClickListener;
        setOnSceneTouchListener(this);
        setOnAreaTouchListener(this);
    }

    public IOnMenuItemClickListener getOnMenuItemClickListener() {
        return this.mOnMenuItemClickListener;
    }

    public void setOnMenuItemClickListener(IOnMenuItemClickListener iOnMenuItemClickListener) {
        this.mOnMenuItemClickListener = iOnMenuItemClickListener;
    }

    public int getMenuItemCount() {
        return this.mMenuItems.size();
    }

    public void addMenuItem(IMenuItem iMenuItem) {
        this.mMenuItems.add(iMenuItem);
        attachChild(iMenuItem);
        registerTouchArea(iMenuItem);
    }

    public MenuScene getChildScene() {
        return (MenuScene) super.getChildScene();
    }

    public void setChildScene(Scene scene, boolean z, boolean z2, boolean z3) {
        if (scene instanceof MenuScene) {
            super.setChildScene(scene, z, z2, z3);
            return;
        }
        throw new IllegalArgumentException("MenuScene accepts only MenuScenes as a ChildScene.");
    }

    public void clearChildScene() {
        if (getChildScene() != null) {
            getChildScene().reset();
            super.clearChildScene();
        }
    }

    public void setMenuAnimator(IMenuAnimator iMenuAnimator) {
        this.mMenuAnimator = iMenuAnimator;
    }

    public boolean onAreaTouched(TouchEvent touchEvent, Scene.ITouchArea iTouchArea, float f, float f2) {
        IMenuItem iMenuItem = (IMenuItem) iTouchArea;
        switch (touchEvent.getAction()) {
            case 0:
            case 2:
                if (!(this.mSelectedMenuItem == null || this.mSelectedMenuItem == iMenuItem)) {
                    this.mSelectedMenuItem.onUnselected();
                }
                this.mSelectedMenuItem = iMenuItem;
                this.mSelectedMenuItem.onSelected();
                break;
            case 1:
                if (this.mOnMenuItemClickListener != null) {
                    boolean onMenuItemClicked = this.mOnMenuItemClickListener.onMenuItemClicked(this, iMenuItem, f, f2);
                    iMenuItem.onUnselected();
                    this.mSelectedMenuItem = null;
                    return onMenuItemClicked;
                }
                break;
            case TouchEvent.ACTION_CANCEL /*3*/:
                iMenuItem.onUnselected();
                this.mSelectedMenuItem = null;
                break;
        }
        return true;
    }

    public boolean onSceneTouchEvent(Scene scene, TouchEvent touchEvent) {
        if (this.mSelectedMenuItem == null) {
            return false;
        }
        this.mSelectedMenuItem.onUnselected();
        this.mSelectedMenuItem = null;
        return false;
    }

    public void back() {
        super.back();
        reset();
    }

    public void reset() {
        super.reset();
        ArrayList arrayList = this.mMenuItems;
        for (int size = arrayList.size() - 1; size >= 0; size--) {
            ((IMenuItem) arrayList.get(size)).reset();
        }
        prepareAnimations();
    }

    public void closeMenuScene() {
        back();
    }

    public void buildAnimations() {
        prepareAnimations();
        this.mMenuAnimator.buildAnimations(this.mMenuItems, this.mCamera.getWidthRaw(), this.mCamera.getHeightRaw());
    }

    public void prepareAnimations() {
        this.mMenuAnimator.prepareAnimations(this.mMenuItems, this.mCamera.getWidthRaw(), this.mCamera.getHeightRaw());
    }
}
