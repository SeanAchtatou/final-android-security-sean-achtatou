package org.anddev.andengine.entity.text;

import javax.microedition.khronos.opengles.GL10;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.opengl.font.Font;
import org.anddev.andengine.util.HorizontalAlign;

public class TickerText extends Text {
    private float mCharactersPerSecond;
    private int mCharactersVisible = 0;
    private float mDuration;
    private boolean mReverse = false;
    private float mSecondsElapsed = 0.0f;

    public TickerText(float f, float f2, Font font, String str, HorizontalAlign horizontalAlign, float f3) {
        super(f, f2, font, str, horizontalAlign);
        setCharactersPerSecond(f3);
    }

    public boolean isReverse() {
        return this.mReverse;
    }

    public void setReverse(boolean z) {
        this.mReverse = z;
    }

    public float getCharactersPerSecond() {
        return this.mCharactersPerSecond;
    }

    public void setCharactersPerSecond(float f) {
        this.mCharactersPerSecond = f;
        this.mDuration = ((float) this.mCharactersMaximum) * this.mCharactersPerSecond;
    }

    public int getCharactersVisible() {
        return this.mCharactersVisible;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    /* access modifiers changed from: protected */
    public void onManagedUpdate(float f) {
        super.onManagedUpdate(f);
        if (this.mReverse) {
            if (this.mCharactersVisible < this.mCharactersMaximum) {
                this.mSecondsElapsed = Math.max(0.0f, this.mSecondsElapsed - f);
                this.mCharactersVisible = (int) (this.mSecondsElapsed * this.mCharactersPerSecond);
            }
        } else if (this.mCharactersVisible < this.mCharactersMaximum) {
            this.mSecondsElapsed = Math.min(this.mDuration, this.mSecondsElapsed + f);
            this.mCharactersVisible = (int) (this.mSecondsElapsed * this.mCharactersPerSecond);
        }
    }

    /* access modifiers changed from: protected */
    public void drawVertices(GL10 gl10, Camera camera) {
        gl10.glDrawArrays(4, 0, this.mCharactersVisible * 6);
    }

    public void reset() {
        super.reset();
        this.mCharactersVisible = 0;
        this.mSecondsElapsed = 0.0f;
        this.mReverse = false;
    }
}
