package org.anddev.andengine.entity.scene.background.modifier;

import org.anddev.andengine.util.modifier.IModifier;

public interface IBackgroundModifier extends IModifier {

    public interface IBackgroundModifierListener extends IModifier.IModifierListener {
    }

    IBackgroundModifier clone();
}
