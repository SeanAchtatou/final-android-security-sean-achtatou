package org.anddev.andengine.entity.modifier;

import org.anddev.andengine.util.IMatcher;
import org.anddev.andengine.util.modifier.IModifier;

public interface IEntityModifier extends IModifier {

    public interface IEntityModifierListener extends IModifier.IModifierListener {
    }

    public interface IEntityModifierMatcher extends IMatcher {
    }

    IEntityModifier clone();
}
