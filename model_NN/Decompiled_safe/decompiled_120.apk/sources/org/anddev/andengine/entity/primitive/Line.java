package org.anddev.andengine.entity.primitive;

import javax.microedition.khronos.opengles.GL10;
import org.anddev.andengine.collision.LineCollisionChecker;
import org.anddev.andengine.collision.RectangularShapeCollisionChecker;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.entity.shape.IShape;
import org.anddev.andengine.entity.shape.RectangularShape;
import org.anddev.andengine.entity.shape.Shape;
import org.anddev.andengine.opengl.util.GLHelper;
import org.anddev.andengine.opengl.vertex.LineVertexBuffer;

public class Line extends Shape {
    private static final float LINEWIDTH_DEFAULT = 1.0f;
    private final LineVertexBuffer mLineVertexBuffer;
    private float mLineWidth;
    protected float mX2;
    protected float mY2;

    public Line(float f, float f2, float f3, float f4) {
        this(f, f2, f3, f4, 1.0f);
    }

    public Line(float f, float f2, float f3, float f4, float f5) {
        super(f, f2);
        this.mX2 = f3;
        this.mY2 = f4;
        this.mLineWidth = f5;
        this.mLineVertexBuffer = new LineVertexBuffer(35044, true);
        updateVertexBuffer();
        float width = getWidth();
        float height = getHeight();
        this.mRotationCenterX = width * 0.5f;
        this.mRotationCenterY = height * 0.5f;
        this.mScaleCenterX = this.mRotationCenterX;
        this.mScaleCenterY = this.mRotationCenterY;
    }

    public float getX() {
        return super.getX();
    }

    public float getY() {
        return super.getY();
    }

    public float getX1() {
        return super.getX();
    }

    public float getY1() {
        return super.getY();
    }

    public float getX2() {
        return this.mX2;
    }

    public float getY2() {
        return this.mY2;
    }

    public float getLineWidth() {
        return this.mLineWidth;
    }

    public void setLineWidth(float f) {
        this.mLineWidth = f;
    }

    public float getBaseHeight() {
        return this.mY2 - this.mY;
    }

    public float getBaseWidth() {
        return this.mX2 - this.mX;
    }

    public float getHeight() {
        return this.mY2 - this.mY;
    }

    public float getWidth() {
        return this.mX2 - this.mX;
    }

    public void setPosition(float f, float f2) {
        super.setPosition(f, f2);
        this.mX2 = (this.mX - f) + this.mX2;
        this.mY2 += this.mY - f2;
    }

    public void setPosition(float f, float f2, float f3, float f4) {
        this.mX2 = f3;
        this.mY2 = f4;
        super.setPosition(f, f2);
        updateVertexBuffer();
    }

    /* access modifiers changed from: protected */
    public boolean isCulled(Camera camera) {
        return camera.isLineVisible(this);
    }

    /* access modifiers changed from: protected */
    public void onInitDraw(GL10 gl10) {
        super.onInitDraw(gl10);
        GLHelper.disableTextures(gl10);
        GLHelper.disableTexCoordArray(gl10);
        GLHelper.lineWidth(gl10, this.mLineWidth);
    }

    public LineVertexBuffer getVertexBuffer() {
        return this.mLineVertexBuffer;
    }

    /* access modifiers changed from: protected */
    public void onUpdateVertexBuffer() {
        this.mLineVertexBuffer.update(0.0f, 0.0f, this.mX2 - this.mX, this.mY2 - this.mY);
    }

    /* access modifiers changed from: protected */
    public void drawVertices(GL10 gl10, Camera camera) {
        gl10.glDrawArrays(1, 0, 2);
    }

    public float[] getSceneCenterCoordinates() {
        return null;
    }

    public boolean contains(float f, float f2) {
        return false;
    }

    public float[] convertSceneToLocalCoordinates(float f, float f2) {
        return null;
    }

    public float[] convertLocalToSceneCoordinates(float f, float f2) {
        return null;
    }

    public boolean collidesWith(IShape iShape) {
        if (iShape instanceof Line) {
            Line line = (Line) iShape;
            return LineCollisionChecker.checkLineCollision(this.mX, this.mY, this.mX2, this.mY2, line.mX, line.mY, line.mX2, line.mY2);
        } else if (iShape instanceof RectangularShape) {
            return RectangularShapeCollisionChecker.checkCollision((RectangularShape) iShape, this);
        } else {
            return false;
        }
    }
}
