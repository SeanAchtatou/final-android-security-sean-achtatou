package org.anddev.andengine.entity.primitive;

import org.anddev.andengine.entity.shape.RectangularShape;
import org.anddev.andengine.opengl.vertex.RectangleVertexBuffer;

public abstract class BaseRectangle extends RectangularShape {
    public BaseRectangle(float f, float f2, float f3, float f4) {
        super(f, f2, f3, f4, new RectangleVertexBuffer(35044, true));
        updateVertexBuffer();
    }

    public BaseRectangle(float f, float f2, float f3, float f4, RectangleVertexBuffer rectangleVertexBuffer) {
        super(f, f2, f3, f4, rectangleVertexBuffer);
    }

    public RectangleVertexBuffer getVertexBuffer() {
        return (RectangleVertexBuffer) this.mVertexBuffer;
    }

    /* access modifiers changed from: protected */
    public void onUpdateVertexBuffer() {
        getVertexBuffer().update(this.mWidth, this.mHeight);
    }
}
