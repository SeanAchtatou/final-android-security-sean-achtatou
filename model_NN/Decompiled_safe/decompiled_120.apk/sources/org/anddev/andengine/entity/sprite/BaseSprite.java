package org.anddev.andengine.entity.sprite;

import javax.microedition.khronos.opengles.GL10;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.entity.primitive.BaseRectangle;
import org.anddev.andengine.opengl.texture.region.BaseTextureRegion;
import org.anddev.andengine.opengl.texture.region.buffer.TextureRegionBuffer;
import org.anddev.andengine.opengl.util.GLHelper;
import org.anddev.andengine.opengl.vertex.RectangleVertexBuffer;

public abstract class BaseSprite extends BaseRectangle {
    protected final BaseTextureRegion mTextureRegion;

    public BaseSprite(float f, float f2, float f3, float f4, BaseTextureRegion baseTextureRegion) {
        super(f, f2, f3, f4);
        this.mTextureRegion = baseTextureRegion;
        initBlendFunction();
    }

    public BaseSprite(float f, float f2, float f3, float f4, BaseTextureRegion baseTextureRegion, RectangleVertexBuffer rectangleVertexBuffer) {
        super(f, f2, f3, f4, rectangleVertexBuffer);
        this.mTextureRegion = baseTextureRegion;
        initBlendFunction();
    }

    public BaseTextureRegion getTextureRegion() {
        return this.mTextureRegion;
    }

    public void setFlippedHorizontal(boolean z) {
        this.mTextureRegion.setFlippedHorizontal(z);
    }

    public void setFlippedVertical(boolean z) {
        this.mTextureRegion.setFlippedVertical(z);
    }

    public void reset() {
        super.reset();
        initBlendFunction();
    }

    /* access modifiers changed from: protected */
    public void onInitDraw(GL10 gl10) {
        super.onInitDraw(gl10);
        GLHelper.enableTextures(gl10);
        GLHelper.enableTexCoordArray(gl10);
    }

    /* access modifiers changed from: protected */
    public void doDraw(GL10 gl10, Camera camera) {
        this.mTextureRegion.onApply(gl10);
        super.doDraw(gl10, camera);
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        super.finalize();
        TextureRegionBuffer textureBuffer = this.mTextureRegion.getTextureBuffer();
        if (textureBuffer.isManaged()) {
            textureBuffer.unloadFromActiveBufferObjectManager();
        }
    }

    private void initBlendFunction() {
        if (this.mTextureRegion.getTexture().getTextureOptions().mPreMultipyAlpha) {
            setBlendFunction(1, 771);
        }
    }
}
