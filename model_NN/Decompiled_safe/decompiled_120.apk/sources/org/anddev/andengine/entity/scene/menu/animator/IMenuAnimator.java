package org.anddev.andengine.entity.scene.menu.animator;

import java.util.ArrayList;

public interface IMenuAnimator {
    public static final IMenuAnimator DEFAULT = new AlphaMenuAnimator();

    void buildAnimations(ArrayList arrayList, float f, float f2);

    void prepareAnimations(ArrayList arrayList, float f, float f2);
}
