package org.anddev.andengine.entity.modifier;

import android.util.FloatMath;
import org.anddev.andengine.entity.IEntity;
import org.anddev.andengine.entity.modifier.IEntityModifier;
import org.anddev.andengine.util.modifier.IModifier;
import org.anddev.andengine.util.modifier.SequenceModifier;
import org.anddev.andengine.util.modifier.ease.IEaseFunction;

public class PathModifier extends EntityModifier {
    private final Path mPath;
    /* access modifiers changed from: private */
    public IPathModifierListener mPathModifierListener;
    private final SequenceModifier mSequenceModifier;

    public interface IPathModifierListener {
        void onPathFinished(PathModifier pathModifier, IEntity iEntity);

        void onPathStarted(PathModifier pathModifier, IEntity iEntity);

        void onPathWaypointFinished(PathModifier pathModifier, IEntity iEntity, int i);

        void onPathWaypointStarted(PathModifier pathModifier, IEntity iEntity, int i);
    }

    public PathModifier(float f, Path path) {
        this(f, path, null, null, IEaseFunction.DEFAULT);
    }

    public PathModifier(float f, Path path, IEaseFunction iEaseFunction) {
        this(f, path, null, null, iEaseFunction);
    }

    public PathModifier(float f, Path path, IEntityModifier.IEntityModifierListener iEntityModifierListener) {
        this(f, path, iEntityModifierListener, null, IEaseFunction.DEFAULT);
    }

    public PathModifier(float f, Path path, IPathModifierListener iPathModifierListener) {
        this(f, path, null, iPathModifierListener, IEaseFunction.DEFAULT);
    }

    public PathModifier(float f, Path path, IPathModifierListener iPathModifierListener, IEaseFunction iEaseFunction) {
        this(f, path, null, iPathModifierListener, iEaseFunction);
    }

    public PathModifier(float f, Path path, IEntityModifier.IEntityModifierListener iEntityModifierListener, IEaseFunction iEaseFunction) {
        this(f, path, iEntityModifierListener, null, iEaseFunction);
    }

    public PathModifier(float f, Path path, IEntityModifier.IEntityModifierListener iEntityModifierListener, IPathModifierListener iPathModifierListener) {
        this(f, path, iEntityModifierListener, iPathModifierListener, IEaseFunction.DEFAULT);
    }

    public PathModifier(float f, Path path, IEntityModifier.IEntityModifierListener iEntityModifierListener, IPathModifierListener iPathModifierListener, IEaseFunction iEaseFunction) {
        super(iEntityModifierListener);
        int size = path.getSize();
        if (size < 2) {
            throw new IllegalArgumentException("Path needs at least 2 waypoints!");
        }
        this.mPath = path;
        this.mPathModifierListener = iPathModifierListener;
        MoveModifier[] moveModifierArr = new MoveModifier[(size - 1)];
        float[] coordinatesX = path.getCoordinatesX();
        float[] coordinatesY = path.getCoordinatesY();
        float length = path.getLength() / f;
        int length2 = moveModifierArr.length;
        for (int i = 0; i < length2; i++) {
            moveModifierArr[i] = new MoveModifier(path.getSegmentLength(i) / length, coordinatesX[i], coordinatesX[i + 1], coordinatesY[i], coordinatesY[i + 1], null, iEaseFunction);
        }
        this.mSequenceModifier = new SequenceModifier(new SequenceModifier.ISubSequenceModifierListener() {
            public void onSubSequenceStarted(IModifier iModifier, IEntity iEntity, int i) {
                if (PathModifier.this.mPathModifierListener != null) {
                    PathModifier.this.mPathModifierListener.onPathWaypointStarted(PathModifier.this, iEntity, i);
                }
            }

            public void onSubSequenceFinished(IModifier iModifier, IEntity iEntity, int i) {
                if (PathModifier.this.mPathModifierListener != null) {
                    PathModifier.this.mPathModifierListener.onPathWaypointFinished(PathModifier.this, iEntity, i);
                }
            }
        }, new IEntityModifier.IEntityModifierListener() {
            public void onModifierStarted(IModifier iModifier, IEntity iEntity) {
                PathModifier.this.onModifierStarted(iEntity);
                if (PathModifier.this.mPathModifierListener != null) {
                    PathModifier.this.mPathModifierListener.onPathStarted(PathModifier.this, iEntity);
                }
            }

            public void onModifierFinished(IModifier iModifier, IEntity iEntity) {
                PathModifier.this.onModifierFinished(iEntity);
                if (PathModifier.this.mPathModifierListener != null) {
                    PathModifier.this.mPathModifierListener.onPathFinished(PathModifier.this, iEntity);
                }
            }
        }, moveModifierArr);
    }

    protected PathModifier(PathModifier pathModifier) {
        this.mPath = pathModifier.mPath.clone();
        this.mSequenceModifier = pathModifier.mSequenceModifier.clone();
    }

    public PathModifier clone() {
        return new PathModifier(this);
    }

    public Path getPath() {
        return this.mPath;
    }

    public boolean isFinished() {
        return this.mSequenceModifier.isFinished();
    }

    public float getSecondsElapsed() {
        return this.mSequenceModifier.getSecondsElapsed();
    }

    public float getDuration() {
        return this.mSequenceModifier.getDuration();
    }

    public IPathModifierListener getPathModifierListener() {
        return this.mPathModifierListener;
    }

    public void setPathModifierListener(IPathModifierListener iPathModifierListener) {
        this.mPathModifierListener = iPathModifierListener;
    }

    public void reset() {
        this.mSequenceModifier.reset();
    }

    public float onUpdate(float f, IEntity iEntity) {
        return this.mSequenceModifier.onUpdate(f, iEntity);
    }

    public class Path {
        private final float[] mCoordinatesX;
        private final float[] mCoordinatesY;
        private int mIndex;
        private float mLength;
        private boolean mLengthChanged = false;

        public Path(int i) {
            this.mCoordinatesX = new float[i];
            this.mCoordinatesY = new float[i];
            this.mIndex = 0;
            this.mLengthChanged = false;
        }

        public Path(float[] fArr, float[] fArr2) {
            if (fArr.length != fArr2.length) {
                throw new IllegalArgumentException("Coordinate-Arrays must have the same length.");
            }
            this.mCoordinatesX = fArr;
            this.mCoordinatesY = fArr2;
            this.mIndex = fArr.length;
            this.mLengthChanged = true;
        }

        public Path(Path path) {
            int size = path.getSize();
            this.mCoordinatesX = new float[size];
            this.mCoordinatesY = new float[size];
            System.arraycopy(path.mCoordinatesX, 0, this.mCoordinatesX, 0, size);
            System.arraycopy(path.mCoordinatesY, 0, this.mCoordinatesY, 0, size);
            this.mIndex = path.mIndex;
            this.mLengthChanged = path.mLengthChanged;
            this.mLength = path.mLength;
        }

        public Path clone() {
            return new Path(this);
        }

        public Path to(float f, float f2) {
            this.mCoordinatesX[this.mIndex] = f;
            this.mCoordinatesY[this.mIndex] = f2;
            this.mIndex++;
            this.mLengthChanged = true;
            return this;
        }

        public float[] getCoordinatesX() {
            return this.mCoordinatesX;
        }

        public float[] getCoordinatesY() {
            return this.mCoordinatesY;
        }

        public int getSize() {
            return this.mCoordinatesX.length;
        }

        public float getLength() {
            if (this.mLengthChanged) {
                updateLength();
            }
            return this.mLength;
        }

        public float getSegmentLength(int i) {
            float[] fArr = this.mCoordinatesX;
            float[] fArr2 = this.mCoordinatesY;
            int i2 = i + 1;
            float f = fArr[i] - fArr[i2];
            float f2 = fArr2[i] - fArr2[i2];
            return FloatMath.sqrt((f * f) + (f2 * f2));
        }

        private void updateLength() {
            float f = 0.0f;
            for (int i = this.mIndex - 2; i >= 0; i--) {
                f += getSegmentLength(i);
            }
            this.mLength = f;
        }
    }
}
