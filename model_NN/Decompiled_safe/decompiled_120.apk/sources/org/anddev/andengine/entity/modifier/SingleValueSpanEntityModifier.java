package org.anddev.andengine.entity.modifier;

import org.anddev.andengine.entity.modifier.IEntityModifier;
import org.anddev.andengine.util.modifier.BaseSingleValueSpanModifier;
import org.anddev.andengine.util.modifier.ease.IEaseFunction;

public abstract class SingleValueSpanEntityModifier extends BaseSingleValueSpanModifier implements IEntityModifier {
    public SingleValueSpanEntityModifier(float f, float f2, float f3) {
        super(f, f2, f3);
    }

    public SingleValueSpanEntityModifier(float f, float f2, float f3, IEaseFunction iEaseFunction) {
        super(f, f2, f3, iEaseFunction);
    }

    public SingleValueSpanEntityModifier(float f, float f2, float f3, IEntityModifier.IEntityModifierListener iEntityModifierListener) {
        super(f, f2, f3, iEntityModifierListener);
    }

    public SingleValueSpanEntityModifier(float f, float f2, float f3, IEntityModifier.IEntityModifierListener iEntityModifierListener, IEaseFunction iEaseFunction) {
        super(f, f2, f3, iEntityModifierListener, iEaseFunction);
    }

    protected SingleValueSpanEntityModifier(SingleValueSpanEntityModifier singleValueSpanEntityModifier) {
        super(singleValueSpanEntityModifier);
    }
}
