package org.anddev.andengine.entity.layer.tiled.tmx;

import android.content.Context;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import org.anddev.andengine.entity.layer.tiled.tmx.util.exception.TSXLoadException;
import org.anddev.andengine.opengl.texture.TextureManager;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

public class TSXLoader {
    private final Context mContext;
    private final TextureManager mTextureManager;
    private final TextureOptions mTextureOptions;

    public TSXLoader(Context context, TextureManager textureManager, TextureOptions textureOptions) {
        this.mContext = context;
        this.mTextureManager = textureManager;
        this.mTextureOptions = textureOptions;
    }

    public TMXTileSet loadFromAsset(Context context, int i, String str) {
        try {
            return load(i, context.getAssets().open(str));
        } catch (IOException e) {
            throw new TSXLoadException("Could not load TMXTileSet from asset: " + str, e);
        }
    }

    private TMXTileSet load(int i, InputStream inputStream) {
        try {
            XMLReader xMLReader = SAXParserFactory.newInstance().newSAXParser().getXMLReader();
            TSXParser tSXParser = new TSXParser(this.mContext, this.mTextureManager, this.mTextureOptions, i);
            xMLReader.setContentHandler(tSXParser);
            xMLReader.parse(new InputSource(new BufferedInputStream(inputStream)));
            return tSXParser.getTMXTileSet();
        } catch (SAXException e) {
            throw new TSXLoadException(e);
        } catch (ParserConfigurationException e2) {
            return null;
        } catch (IOException e3) {
            throw new TSXLoadException(e3);
        }
    }
}
