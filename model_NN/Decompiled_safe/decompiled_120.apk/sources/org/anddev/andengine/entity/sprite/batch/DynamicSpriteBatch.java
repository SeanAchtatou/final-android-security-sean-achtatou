package org.anddev.andengine.entity.sprite.batch;

import javax.microedition.khronos.opengles.GL10;
import org.anddev.andengine.opengl.texture.ITexture;
import org.anddev.andengine.opengl.texture.region.buffer.SpriteBatchTextureRegionBuffer;
import org.anddev.andengine.opengl.vertex.SpriteBatchVertexBuffer;

public abstract class DynamicSpriteBatch extends SpriteBatch {
    /* access modifiers changed from: protected */
    public abstract boolean onUpdateSpriteBatch();

    public DynamicSpriteBatch(ITexture iTexture, int i) {
        super(iTexture, i);
    }

    public DynamicSpriteBatch(ITexture iTexture, int i, SpriteBatchVertexBuffer spriteBatchVertexBuffer, SpriteBatchTextureRegionBuffer spriteBatchTextureRegionBuffer) {
        super(iTexture, i, spriteBatchVertexBuffer, spriteBatchTextureRegionBuffer);
    }

    /* access modifiers changed from: protected */
    public void begin(GL10 gl10) {
        super.begin(gl10);
        if (onUpdateSpriteBatch()) {
            submit();
        }
    }
}
