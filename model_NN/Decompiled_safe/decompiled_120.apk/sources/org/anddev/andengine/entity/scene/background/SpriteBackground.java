package org.anddev.andengine.entity.scene.background;

import org.anddev.andengine.entity.sprite.BaseSprite;

public class SpriteBackground extends EntityBackground {
    public SpriteBackground(BaseSprite baseSprite) {
        super(baseSprite);
    }

    public SpriteBackground(float f, float f2, float f3, BaseSprite baseSprite) {
        super(f, f2, f3, baseSprite);
    }
}
