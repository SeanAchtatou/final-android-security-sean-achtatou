package org.anddev.andengine.entity.particle.emitter;

import org.anddev.andengine.util.MathUtils;

public class RectangleParticleEmitter extends BaseRectangleParticleEmitter {
    public RectangleParticleEmitter(float f, float f2, float f3, float f4) {
        super(f, f2, f3, f4);
    }

    public void getPositionOffset(float[] fArr) {
        fArr[0] = (this.mCenterX - this.mWidthHalf) + (MathUtils.RANDOM.nextFloat() * this.mWidth);
        fArr[1] = (this.mCenterY - this.mHeightHalf) + (MathUtils.RANDOM.nextFloat() * this.mHeight);
    }
}
