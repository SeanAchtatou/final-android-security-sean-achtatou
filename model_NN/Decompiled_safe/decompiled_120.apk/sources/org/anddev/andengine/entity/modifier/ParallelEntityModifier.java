package org.anddev.andengine.entity.modifier;

import org.anddev.andengine.entity.modifier.IEntityModifier;
import org.anddev.andengine.util.modifier.ParallelModifier;

public class ParallelEntityModifier extends ParallelModifier implements IEntityModifier {
    public ParallelEntityModifier(IEntityModifier... iEntityModifierArr) {
        super(iEntityModifierArr);
    }

    public ParallelEntityModifier(IEntityModifier.IEntityModifierListener iEntityModifierListener, IEntityModifier... iEntityModifierArr) {
        super(iEntityModifierListener, iEntityModifierArr);
    }

    protected ParallelEntityModifier(ParallelEntityModifier parallelEntityModifier) {
        super(parallelEntityModifier);
    }

    public ParallelEntityModifier clone() {
        return new ParallelEntityModifier(this);
    }
}
