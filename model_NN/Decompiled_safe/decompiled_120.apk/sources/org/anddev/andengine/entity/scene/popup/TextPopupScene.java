package org.anddev.andengine.entity.scene.popup;

import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.entity.modifier.IEntityModifier;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.text.Text;
import org.anddev.andengine.opengl.font.Font;
import org.anddev.andengine.util.HorizontalAlign;

public class TextPopupScene extends PopupScene {
    private final Text mText;

    public TextPopupScene(Camera camera, Scene scene, Font font, String str, float f) {
        this(camera, scene, font, str, f, null, null);
    }

    public TextPopupScene(Camera camera, Scene scene, Font font, String str, float f, IEntityModifier iEntityModifier) {
        this(camera, scene, font, str, f, iEntityModifier, null);
    }

    public TextPopupScene(Camera camera, Scene scene, Font font, String str, float f, Runnable runnable) {
        this(camera, scene, font, str, f, null, runnable);
    }

    public TextPopupScene(Camera camera, Scene scene, Font font, String str, float f, IEntityModifier iEntityModifier, Runnable runnable) {
        super(camera, scene, f, runnable);
        this.mText = new Text(0.0f, 0.0f, font, str, HorizontalAlign.CENTER);
        centerShapeInCamera(this.mText);
        if (iEntityModifier != null) {
            this.mText.registerEntityModifier(iEntityModifier);
        }
        attachChild(this.mText);
    }

    public Text getText() {
        return this.mText;
    }
}
