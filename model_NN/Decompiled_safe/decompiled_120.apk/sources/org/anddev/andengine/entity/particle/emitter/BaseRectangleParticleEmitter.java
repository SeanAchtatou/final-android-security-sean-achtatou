package org.anddev.andengine.entity.particle.emitter;

public abstract class BaseRectangleParticleEmitter extends BaseParticleEmitter {
    protected float mHeight;
    protected float mHeightHalf;
    protected float mWidth;
    protected float mWidthHalf;

    public BaseRectangleParticleEmitter(float f, float f2, float f3) {
        this(f, f2, f3, f3);
    }

    public BaseRectangleParticleEmitter(float f, float f2, float f3, float f4) {
        super(f, f2);
        setWidth(f3);
        setHeight(f4);
    }

    public float getWidth() {
        return this.mWidth;
    }

    public void setWidth(float f) {
        this.mWidth = f;
        this.mWidthHalf = 0.5f * f;
    }

    public float getHeight() {
        return this.mHeight;
    }

    public void setHeight(float f) {
        this.mHeight = f;
        this.mHeightHalf = 0.5f * f;
    }
}
