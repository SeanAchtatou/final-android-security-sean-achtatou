package org.anddev.andengine.entity.layer.tiled.tmx;

import java.util.ArrayList;
import org.anddev.andengine.entity.layer.tiled.tmx.util.constants.TMXConstants;
import org.anddev.andengine.util.SAXUtils;
import org.xml.sax.Attributes;

public class TMXObjectGroup implements TMXConstants {
    private final int mHeight;
    private final String mName;
    private final TMXProperties mTMXObjectGroupProperties = new TMXProperties();
    private final ArrayList mTMXObjects = new ArrayList();
    private final int mWidth;

    public TMXObjectGroup(Attributes attributes) {
        this.mName = attributes.getValue("", "name");
        this.mWidth = SAXUtils.getIntAttributeOrThrow(attributes, "width");
        this.mHeight = SAXUtils.getIntAttributeOrThrow(attributes, "height");
    }

    public String getName() {
        return this.mName;
    }

    public int getWidth() {
        return this.mWidth;
    }

    public int getHeight() {
        return this.mHeight;
    }

    /* access modifiers changed from: package-private */
    public void addTMXObject(TMXObject tMXObject) {
        this.mTMXObjects.add(tMXObject);
    }

    public ArrayList getTMXObjects() {
        return this.mTMXObjects;
    }

    public void addTMXObjectGroupProperty(TMXObjectGroupProperty tMXObjectGroupProperty) {
        this.mTMXObjectGroupProperties.add(tMXObjectGroupProperty);
    }

    public TMXProperties getTMXObjectGroupProperties() {
        return this.mTMXObjectGroupProperties;
    }
}
