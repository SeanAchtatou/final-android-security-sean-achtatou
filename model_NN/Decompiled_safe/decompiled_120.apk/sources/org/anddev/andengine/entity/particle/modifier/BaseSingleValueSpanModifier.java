package org.anddev.andengine.entity.particle.modifier;

import org.anddev.andengine.entity.particle.Particle;

public abstract class BaseSingleValueSpanModifier implements IParticleModifier {
    private final float mDuration = (this.mToTime - this.mFromTime);
    private final float mFromTime;
    private final float mFromValue;
    private final float mSpanValue = (this.mToValue - this.mFromValue);
    private final float mToTime;
    private final float mToValue;

    /* access modifiers changed from: protected */
    public abstract void onSetInitialValue(Particle particle, float f);

    /* access modifiers changed from: protected */
    public abstract void onSetValue(Particle particle, float f);

    public BaseSingleValueSpanModifier(float f, float f2, float f3, float f4) {
        this.mFromValue = f;
        this.mToValue = f2;
        this.mFromTime = f3;
        this.mToTime = f4;
    }

    public void onInitializeParticle(Particle particle) {
        onSetInitialValue(particle, this.mFromValue);
    }

    public void onUpdateParticle(Particle particle) {
        float lifeTime = particle.getLifeTime();
        if (lifeTime > this.mFromTime && lifeTime < this.mToTime) {
            onSetValueInternal(particle, (lifeTime - this.mFromTime) / this.mDuration);
        }
    }

    /* access modifiers changed from: protected */
    public void onSetValueInternal(Particle particle, float f) {
        onSetValue(particle, calculateValue(f));
    }

    /* access modifiers changed from: protected */
    public final float calculateValue(float f) {
        return this.mFromValue + (this.mSpanValue * f);
    }
}
