package org.anddev.andengine.entity.layer.tiled.tmx;

import android.content.Context;
import android.graphics.Color;
import android.util.SparseArray;
import org.anddev.andengine.entity.layer.tiled.tmx.util.constants.TMXConstants;
import org.anddev.andengine.entity.layer.tiled.tmx.util.exception.TMXParseException;
import org.anddev.andengine.opengl.texture.TextureManager;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasFactory;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.anddev.andengine.opengl.texture.atlas.bitmap.source.AssetBitmapTextureAtlasSource;
import org.anddev.andengine.opengl.texture.atlas.bitmap.source.decorator.ColorKeyBitmapTextureAtlasSourceDecorator;
import org.anddev.andengine.opengl.texture.atlas.bitmap.source.decorator.shape.RectangleBitmapTextureAtlasSourceDecoratorShape;
import org.anddev.andengine.opengl.texture.bitmap.BitmapTexture;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.util.SAXUtils;
import org.xml.sax.Attributes;

public class TMXTileSet implements TMXConstants {
    private BitmapTextureAtlas mBitmapTextureAtlas;
    private final int mFirstGlobalTileID;
    private String mImageSource;
    private final int mMargin;
    private final String mName;
    private final int mSpacing;
    private final SparseArray mTMXTileProperties;
    private final TextureOptions mTextureOptions;
    private final int mTileHeight;
    private final int mTileWidth;
    private int mTilesHorizontal;
    private int mTilesVertical;

    TMXTileSet(Attributes attributes, TextureOptions textureOptions) {
        this(SAXUtils.getIntAttribute(attributes, TMXConstants.TAG_TILESET_ATTRIBUTE_FIRSTGID, 1), attributes, textureOptions);
    }

    TMXTileSet(int i, Attributes attributes, TextureOptions textureOptions) {
        this.mTMXTileProperties = new SparseArray();
        this.mFirstGlobalTileID = i;
        this.mName = attributes.getValue("", "name");
        this.mTileWidth = SAXUtils.getIntAttributeOrThrow(attributes, "tilewidth");
        this.mTileHeight = SAXUtils.getIntAttributeOrThrow(attributes, "tileheight");
        this.mSpacing = SAXUtils.getIntAttribute(attributes, TMXConstants.TAG_TILESET_ATTRIBUTE_SPACING, 0);
        this.mMargin = SAXUtils.getIntAttribute(attributes, TMXConstants.TAG_TILESET_ATTRIBUTE_MARGIN, 0);
        this.mTextureOptions = textureOptions;
    }

    public final int getFirstGlobalTileID() {
        return this.mFirstGlobalTileID;
    }

    public final String getName() {
        return this.mName;
    }

    public final int getTileWidth() {
        return this.mTileWidth;
    }

    public final int getTileHeight() {
        return this.mTileHeight;
    }

    public BitmapTextureAtlas getBitmapTextureAtlas() {
        return this.mBitmapTextureAtlas;
    }

    public void setImageSource(Context context, TextureManager textureManager, Attributes attributes) {
        this.mImageSource = attributes.getValue("", "source");
        AssetBitmapTextureAtlasSource assetBitmapTextureAtlasSource = new AssetBitmapTextureAtlasSource(context, this.mImageSource);
        this.mTilesHorizontal = determineCount(assetBitmapTextureAtlasSource.getWidth(), this.mTileWidth, this.mMargin, this.mSpacing);
        this.mTilesVertical = determineCount(assetBitmapTextureAtlasSource.getHeight(), this.mTileHeight, this.mMargin, this.mSpacing);
        this.mBitmapTextureAtlas = BitmapTextureAtlasFactory.createForTextureAtlasSourceSize(BitmapTexture.BitmapTextureFormat.RGBA_8888, assetBitmapTextureAtlasSource, this.mTextureOptions);
        String attribute = SAXUtils.getAttribute(attributes, TMXConstants.TAG_IMAGE_ATTRIBUTE_TRANS, null);
        if (attribute == null) {
            BitmapTextureAtlasTextureRegionFactory.createFromSource(this.mBitmapTextureAtlas, assetBitmapTextureAtlasSource, 0, 0);
        } else {
            try {
                BitmapTextureAtlasTextureRegionFactory.createFromSource(this.mBitmapTextureAtlas, new ColorKeyBitmapTextureAtlasSourceDecorator(assetBitmapTextureAtlasSource, RectangleBitmapTextureAtlasSourceDecoratorShape.getDefaultInstance(), Color.parseColor(attribute.charAt(0) == '#' ? attribute : "#" + attribute)), 0, 0);
            } catch (IllegalArgumentException e) {
                throw new TMXParseException("Illegal value: '" + attribute + "' for attribute 'trans' supplied!", e);
            }
        }
        textureManager.loadTexture(this.mBitmapTextureAtlas);
    }

    public String getImageSource() {
        return this.mImageSource;
    }

    public SparseArray getTMXTileProperties() {
        return this.mTMXTileProperties;
    }

    public TMXProperties getTMXTilePropertiesFromGlobalTileID(int i) {
        return (TMXProperties) this.mTMXTileProperties.get(i - this.mFirstGlobalTileID);
    }

    public void addTMXTileProperty(int i, TMXTileProperty tMXTileProperty) {
        TMXProperties tMXProperties = (TMXProperties) this.mTMXTileProperties.get(i);
        if (tMXProperties != null) {
            tMXProperties.add(tMXTileProperty);
            return;
        }
        TMXProperties tMXProperties2 = new TMXProperties();
        tMXProperties2.add(tMXTileProperty);
        this.mTMXTileProperties.put(i, tMXProperties2);
    }

    public TextureRegion getTextureRegionFromGlobalTileID(int i) {
        int i2 = i - this.mFirstGlobalTileID;
        int i3 = i2 % this.mTilesHorizontal;
        int i4 = i2 / this.mTilesHorizontal;
        return new TextureRegion(this.mBitmapTextureAtlas, this.mMargin + (i3 * (this.mSpacing + this.mTileWidth)), this.mMargin + (i4 * (this.mSpacing + this.mTileHeight)), this.mTileWidth, this.mTileHeight);
    }

    private static int determineCount(int i, int i2, int i3, int i4) {
        int i5 = 0;
        int i6 = i - (i3 * 2);
        while (i6 > 0) {
            i6 = (i6 - i2) - i4;
            i5++;
        }
        return i5;
    }
}
