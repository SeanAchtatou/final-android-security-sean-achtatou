package org.anddev.andengine.entity.particle.modifier;

import org.anddev.andengine.entity.particle.Particle;

public class RotationModifier extends BaseSingleValueSpanModifier {
    public RotationModifier(float f, float f2, float f3, float f4) {
        super(f, f2, f3, f4);
    }

    /* access modifiers changed from: protected */
    public void onSetInitialValue(Particle particle, float f) {
        particle.setRotation(f);
    }

    /* access modifiers changed from: protected */
    public void onSetValue(Particle particle, float f) {
        particle.setRotation(f);
    }
}
