package org.anddev.andengine.entity.layer.tiled.tmx;

import org.anddev.andengine.entity.layer.tiled.tmx.util.constants.TMXConstants;
import org.anddev.andengine.util.SAXUtils;
import org.xml.sax.Attributes;

public class TMXObject implements TMXConstants {
    private final int mHeight;
    private final String mName;
    private final TMXProperties mTMXObjectProperties = new TMXProperties();
    private final String mType;
    private final int mWidth;
    private final int mX;
    private final int mY;

    public TMXObject(Attributes attributes) {
        this.mName = attributes.getValue("", "name");
        this.mType = attributes.getValue("", TMXConstants.TAG_OBJECT_ATTRIBUTE_TYPE);
        this.mX = SAXUtils.getIntAttributeOrThrow(attributes, TMXConstants.TAG_OBJECT_ATTRIBUTE_X);
        this.mY = SAXUtils.getIntAttributeOrThrow(attributes, TMXConstants.TAG_OBJECT_ATTRIBUTE_Y);
        this.mWidth = SAXUtils.getIntAttribute(attributes, "width", 0);
        this.mHeight = SAXUtils.getIntAttribute(attributes, "height", 0);
    }

    public String getName() {
        return this.mName;
    }

    public String getType() {
        return this.mType;
    }

    public int getX() {
        return this.mX;
    }

    public int getY() {
        return this.mY;
    }

    public int getWidth() {
        return this.mWidth;
    }

    public int getHeight() {
        return this.mHeight;
    }

    public void addTMXObjectProperty(TMXObjectProperty tMXObjectProperty) {
        this.mTMXObjectProperties.add(tMXObjectProperty);
    }

    public TMXProperties getTMXObjectProperties() {
        return this.mTMXObjectProperties;
    }
}
