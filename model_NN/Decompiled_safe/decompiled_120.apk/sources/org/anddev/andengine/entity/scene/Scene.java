package org.anddev.andengine.entity.scene;

import android.util.SparseArray;
import java.util.ArrayList;
import javax.microedition.khronos.opengles.GL10;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.engine.handler.runnable.RunnableHandler;
import org.anddev.andengine.entity.Entity;
import org.anddev.andengine.entity.IEntity;
import org.anddev.andengine.entity.scene.background.ColorBackground;
import org.anddev.andengine.entity.scene.background.IBackground;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.opengl.util.GLHelper;
import org.anddev.andengine.util.IMatcher;
import org.anddev.andengine.util.SmartList;

public class Scene extends Entity {
    private static final int TOUCHAREAS_CAPACITY_DEFAULT = 4;
    private IBackground mBackground = new ColorBackground(0.0f, 0.0f, 0.0f);
    private boolean mBackgroundEnabled = true;
    protected Scene mChildScene;
    private boolean mChildSceneModalDraw;
    private boolean mChildSceneModalTouch;
    private boolean mChildSceneModalUpdate;
    private IOnAreaTouchListener mOnAreaTouchListener;
    private boolean mOnAreaTouchTraversalBackToFront = true;
    private IOnSceneTouchListener mOnSceneTouchListener;
    private boolean mOnSceneTouchListenerBindingEnabled = false;
    private final SparseArray mOnSceneTouchListenerBindings = new SparseArray();
    protected Scene mParentScene;
    private final RunnableHandler mRunnableHandler = new RunnableHandler();
    private float mSecondsElapsedTotal;
    private boolean mTouchAreaBindingEnabled = false;
    private final SparseArray mTouchAreaBindings = new SparseArray();
    protected SmartList mTouchAreas = new SmartList(4);

    public interface IOnAreaTouchListener {
        boolean onAreaTouched(TouchEvent touchEvent, ITouchArea iTouchArea, float f, float f2);
    }

    public interface IOnSceneTouchListener {
        boolean onSceneTouchEvent(Scene scene, TouchEvent touchEvent);
    }

    public interface ITouchArea {

        public interface ITouchAreaMatcher extends IMatcher {
        }

        boolean contains(float f, float f2);

        float[] convertLocalToSceneCoordinates(float f, float f2);

        float[] convertSceneToLocalCoordinates(float f, float f2);

        boolean onAreaTouched(TouchEvent touchEvent, float f, float f2);
    }

    public Scene() {
    }

    public Scene(int i) {
        for (int i2 = 0; i2 < i; i2++) {
            attachChild(new Entity());
        }
    }

    public float getSecondsElapsedTotal() {
        return this.mSecondsElapsedTotal;
    }

    public IBackground getBackground() {
        return this.mBackground;
    }

    public void setBackground(IBackground iBackground) {
        this.mBackground = iBackground;
    }

    public boolean isBackgroundEnabled() {
        return this.mBackgroundEnabled;
    }

    public void setBackgroundEnabled(boolean z) {
        this.mBackgroundEnabled = z;
    }

    public void setOnSceneTouchListener(IOnSceneTouchListener iOnSceneTouchListener) {
        this.mOnSceneTouchListener = iOnSceneTouchListener;
    }

    public IOnSceneTouchListener getOnSceneTouchListener() {
        return this.mOnSceneTouchListener;
    }

    public boolean hasOnSceneTouchListener() {
        return this.mOnSceneTouchListener != null;
    }

    public void setOnAreaTouchListener(IOnAreaTouchListener iOnAreaTouchListener) {
        this.mOnAreaTouchListener = iOnAreaTouchListener;
    }

    public IOnAreaTouchListener getOnAreaTouchListener() {
        return this.mOnAreaTouchListener;
    }

    public boolean hasOnAreaTouchListener() {
        return this.mOnAreaTouchListener != null;
    }

    private void setParentScene(Scene scene) {
        this.mParentScene = scene;
    }

    public boolean hasChildScene() {
        return this.mChildScene != null;
    }

    public Scene getChildScene() {
        return this.mChildScene;
    }

    public void setChildSceneModal(Scene scene) {
        setChildScene(scene, true, true, true);
    }

    public void setChildScene(Scene scene) {
        setChildScene(scene, false, false, false);
    }

    public void setChildScene(Scene scene, boolean z, boolean z2, boolean z3) {
        scene.mParentScene = this;
        this.mChildScene = scene;
        this.mChildSceneModalDraw = z;
        this.mChildSceneModalUpdate = z2;
        this.mChildSceneModalTouch = z3;
    }

    public void clearChildScene() {
        this.mChildScene = null;
    }

    public void setOnAreaTouchTraversalBackToFront() {
        this.mOnAreaTouchTraversalBackToFront = true;
    }

    public void setOnAreaTouchTraversalFrontToBack() {
        this.mOnAreaTouchTraversalBackToFront = false;
    }

    public boolean isTouchAreaBindingEnabled() {
        return this.mTouchAreaBindingEnabled;
    }

    public void setTouchAreaBindingEnabled(boolean z) {
        if (this.mTouchAreaBindingEnabled && !z) {
            this.mTouchAreaBindings.clear();
        }
        this.mTouchAreaBindingEnabled = z;
    }

    public boolean isOnSceneTouchListenerBindingEnabled() {
        return this.mOnSceneTouchListenerBindingEnabled;
    }

    public void setOnSceneTouchListenerBindingEnabled(boolean z) {
        if (this.mOnSceneTouchListenerBindingEnabled && !z) {
            this.mOnSceneTouchListenerBindings.clear();
        }
        this.mOnSceneTouchListenerBindingEnabled = z;
    }

    /* access modifiers changed from: protected */
    public void onManagedDraw(GL10 gl10, Camera camera) {
        Scene scene = this.mChildScene;
        if (scene == null || !this.mChildSceneModalDraw) {
            if (this.mBackgroundEnabled) {
                camera.onApplySceneBackgroundMatrix(gl10);
                GLHelper.setModelViewIdentityMatrix(gl10);
                this.mBackground.onDraw(gl10, camera);
            }
            camera.onApplySceneMatrix(gl10);
            GLHelper.setModelViewIdentityMatrix(gl10);
            super.onManagedDraw(gl10, camera);
        }
        if (scene != null) {
            scene.onDraw(gl10, camera);
        }
    }

    /* access modifiers changed from: protected */
    public void onManagedUpdate(float f) {
        this.mSecondsElapsedTotal += f;
        this.mRunnableHandler.onUpdate(f);
        Scene scene = this.mChildScene;
        if (scene == null || !this.mChildSceneModalUpdate) {
            this.mBackground.onUpdate(f);
            super.onManagedUpdate(f);
        }
        if (scene != null) {
            scene.onUpdate(f);
        }
    }

    public boolean onSceneTouchEvent(TouchEvent touchEvent) {
        int size;
        Boolean onAreaTouchEvent;
        Boolean onAreaTouchEvent2;
        int action = touchEvent.getAction();
        boolean isActionDown = touchEvent.isActionDown();
        if (!isActionDown) {
            if (this.mOnSceneTouchListenerBindingEnabled && ((IOnSceneTouchListener) this.mOnSceneTouchListenerBindings.get(touchEvent.getPointerID())) != null) {
                switch (action) {
                    case 1:
                    case TouchEvent.ACTION_CANCEL /*3*/:
                        this.mOnSceneTouchListenerBindings.remove(touchEvent.getPointerID());
                        break;
                }
                Boolean valueOf = Boolean.valueOf(this.mOnSceneTouchListener.onSceneTouchEvent(this, touchEvent));
                if (valueOf != null && valueOf.booleanValue()) {
                    return true;
                }
            }
            if (this.mTouchAreaBindingEnabled) {
                SparseArray sparseArray = this.mTouchAreaBindings;
                ITouchArea iTouchArea = (ITouchArea) sparseArray.get(touchEvent.getPointerID());
                if (iTouchArea != null) {
                    float x = touchEvent.getX();
                    float y = touchEvent.getY();
                    switch (action) {
                        case 1:
                        case TouchEvent.ACTION_CANCEL /*3*/:
                            sparseArray.remove(touchEvent.getPointerID());
                            break;
                    }
                    Boolean onAreaTouchEvent3 = onAreaTouchEvent(touchEvent, x, y, iTouchArea);
                    if (onAreaTouchEvent3 != null && onAreaTouchEvent3.booleanValue()) {
                        return true;
                    }
                }
            }
        }
        if (this.mChildScene != null) {
            if (onChildSceneTouchEvent(touchEvent)) {
                return true;
            }
            if (this.mChildSceneModalTouch) {
                return false;
            }
        }
        float x2 = touchEvent.getX();
        float y2 = touchEvent.getY();
        SmartList smartList = this.mTouchAreas;
        if (smartList != null && (size = smartList.size()) > 0) {
            if (this.mOnAreaTouchTraversalBackToFront) {
                int i = 0;
                while (i < size) {
                    ITouchArea iTouchArea2 = (ITouchArea) smartList.get(i);
                    if (!iTouchArea2.contains(x2, y2) || (onAreaTouchEvent2 = onAreaTouchEvent(touchEvent, x2, y2, iTouchArea2)) == null || !onAreaTouchEvent2.booleanValue()) {
                        i++;
                    } else {
                        if (this.mTouchAreaBindingEnabled && isActionDown) {
                            this.mTouchAreaBindings.put(touchEvent.getPointerID(), iTouchArea2);
                        }
                        return true;
                    }
                }
            } else {
                int i2 = size - 1;
                while (i2 >= 0) {
                    ITouchArea iTouchArea3 = (ITouchArea) smartList.get(i2);
                    if (!iTouchArea3.contains(x2, y2) || (onAreaTouchEvent = onAreaTouchEvent(touchEvent, x2, y2, iTouchArea3)) == null || !onAreaTouchEvent.booleanValue()) {
                        i2--;
                    } else {
                        if (this.mTouchAreaBindingEnabled && isActionDown) {
                            this.mTouchAreaBindings.put(touchEvent.getPointerID(), iTouchArea3);
                        }
                        return true;
                    }
                }
            }
        }
        if (this.mOnSceneTouchListener == null) {
            return false;
        }
        Boolean valueOf2 = Boolean.valueOf(this.mOnSceneTouchListener.onSceneTouchEvent(this, touchEvent));
        if (valueOf2 == null || !valueOf2.booleanValue()) {
            return false;
        }
        if (this.mOnSceneTouchListenerBindingEnabled && isActionDown) {
            this.mOnSceneTouchListenerBindings.put(touchEvent.getPointerID(), this.mOnSceneTouchListener);
        }
        return true;
    }

    private Boolean onAreaTouchEvent(TouchEvent touchEvent, float f, float f2, ITouchArea iTouchArea) {
        float[] convertSceneToLocalCoordinates = iTouchArea.convertSceneToLocalCoordinates(f, f2);
        float f3 = convertSceneToLocalCoordinates[0];
        float f4 = convertSceneToLocalCoordinates[1];
        if (iTouchArea.onAreaTouched(touchEvent, f3, f4)) {
            return Boolean.TRUE;
        }
        if (this.mOnAreaTouchListener != null) {
            return Boolean.valueOf(this.mOnAreaTouchListener.onAreaTouched(touchEvent, iTouchArea, f3, f4));
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public boolean onChildSceneTouchEvent(TouchEvent touchEvent) {
        return this.mChildScene.onSceneTouchEvent(touchEvent);
    }

    public void reset() {
        super.reset();
        clearChildScene();
    }

    public void setParent(IEntity iEntity) {
    }

    public void postRunnable(Runnable runnable) {
        this.mRunnableHandler.postRunnable(runnable);
    }

    public void registerTouchArea(ITouchArea iTouchArea) {
        this.mTouchAreas.add(iTouchArea);
    }

    public boolean unregisterTouchArea(ITouchArea iTouchArea) {
        return this.mTouchAreas.remove(iTouchArea);
    }

    public boolean unregisterTouchAreas(ITouchArea.ITouchAreaMatcher iTouchAreaMatcher) {
        return this.mTouchAreas.removeAll(iTouchAreaMatcher);
    }

    public void clearTouchAreas() {
        this.mTouchAreas.clear();
    }

    public ArrayList getTouchAreas() {
        return this.mTouchAreas;
    }

    public void back() {
        clearChildScene();
        if (this.mParentScene != null) {
            this.mParentScene.clearChildScene();
            this.mParentScene = null;
        }
    }
}
