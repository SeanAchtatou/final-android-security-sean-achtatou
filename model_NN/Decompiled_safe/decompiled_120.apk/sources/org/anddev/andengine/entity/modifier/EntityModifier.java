package org.anddev.andengine.entity.modifier;

import org.anddev.andengine.entity.modifier.IEntityModifier;
import org.anddev.andengine.util.modifier.BaseModifier;

public abstract class EntityModifier extends BaseModifier implements IEntityModifier {
    public EntityModifier() {
    }

    public EntityModifier(IEntityModifier.IEntityModifierListener iEntityModifierListener) {
        super(iEntityModifierListener);
    }
}
