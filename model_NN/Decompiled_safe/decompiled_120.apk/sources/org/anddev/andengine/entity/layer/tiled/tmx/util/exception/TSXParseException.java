package org.anddev.andengine.entity.layer.tiled.tmx.util.exception;

import org.xml.sax.SAXException;

public class TSXParseException extends SAXException {
    private static final long serialVersionUID = -7598783248970268198L;

    public TSXParseException() {
    }

    public TSXParseException(String str) {
        super(str);
    }

    public TSXParseException(Exception exc) {
        super(exc);
    }

    public TSXParseException(String str, Exception exc) {
        super(str, exc);
    }
}
