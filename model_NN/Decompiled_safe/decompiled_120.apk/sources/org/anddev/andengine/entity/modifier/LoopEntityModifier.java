package org.anddev.andengine.entity.modifier;

import org.anddev.andengine.entity.modifier.IEntityModifier;
import org.anddev.andengine.util.modifier.LoopModifier;

public class LoopEntityModifier extends LoopModifier implements IEntityModifier {

    public interface ILoopEntityModifierListener extends LoopModifier.ILoopModifierListener {
    }

    public LoopEntityModifier(IEntityModifier iEntityModifier) {
        super(iEntityModifier);
    }

    public LoopEntityModifier(IEntityModifier iEntityModifier, int i) {
        super(iEntityModifier, i);
    }

    public LoopEntityModifier(IEntityModifier iEntityModifier, int i, ILoopEntityModifierListener iLoopEntityModifierListener) {
        super(iEntityModifier, i, iLoopEntityModifierListener);
    }

    public LoopEntityModifier(IEntityModifier iEntityModifier, int i, IEntityModifier.IEntityModifierListener iEntityModifierListener) {
        super(iEntityModifier, i, iEntityModifierListener);
    }

    public LoopEntityModifier(IEntityModifier.IEntityModifierListener iEntityModifierListener, int i, ILoopEntityModifierListener iLoopEntityModifierListener, IEntityModifier iEntityModifier) {
        super(iEntityModifier, i, iLoopEntityModifierListener, iEntityModifierListener);
    }

    protected LoopEntityModifier(LoopEntityModifier loopEntityModifier) {
        super((LoopModifier) loopEntityModifier);
    }

    public LoopEntityModifier clone() {
        return new LoopEntityModifier(this);
    }
}
