package org.anddev.andengine.entity.layer.tiled.tmx;

import java.util.ArrayList;
import org.anddev.andengine.entity.layer.tiled.tmx.util.constants.TMXConstants;

public class TMXProperties extends ArrayList implements TMXConstants {
    private static final long serialVersionUID = 8912773556975105201L;

    public boolean containsTMXProperty(String str, String str2) {
        for (int size = size() - 1; size >= 0; size--) {
            TMXProperty tMXProperty = (TMXProperty) get(size);
            if (tMXProperty.getName().equals(str) && tMXProperty.getValue().equals(str2)) {
                return true;
            }
        }
        return false;
    }
}
