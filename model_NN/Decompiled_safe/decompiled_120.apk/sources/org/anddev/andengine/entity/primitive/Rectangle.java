package org.anddev.andengine.entity.primitive;

import javax.microedition.khronos.opengles.GL10;
import org.anddev.andengine.opengl.util.GLHelper;
import org.anddev.andengine.opengl.vertex.RectangleVertexBuffer;

public class Rectangle extends BaseRectangle {
    public Rectangle(float f, float f2, float f3, float f4) {
        super(f, f2, f3, f4);
    }

    public Rectangle(float f, float f2, float f3, float f4, RectangleVertexBuffer rectangleVertexBuffer) {
        super(f, f2, f3, f4, rectangleVertexBuffer);
    }

    /* access modifiers changed from: protected */
    public void onInitDraw(GL10 gl10) {
        super.onInitDraw(gl10);
        GLHelper.disableTextures(gl10);
        GLHelper.disableTexCoordArray(gl10);
    }
}
