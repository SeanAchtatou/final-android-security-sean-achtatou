package org.anddev.andengine.entity.sprite;

import org.anddev.andengine.opengl.texture.region.TiledTextureRegion;
import org.anddev.andengine.opengl.vertex.RectangleVertexBuffer;

public class TiledSprite extends BaseSprite {
    public TiledSprite(float f, float f2, TiledTextureRegion tiledTextureRegion) {
        super(f, f2, (float) tiledTextureRegion.getTileWidth(), (float) tiledTextureRegion.getTileHeight(), tiledTextureRegion);
    }

    public TiledSprite(float f, float f2, float f3, float f4, TiledTextureRegion tiledTextureRegion) {
        super(f, f2, f3, f4, tiledTextureRegion);
    }

    public TiledSprite(float f, float f2, TiledTextureRegion tiledTextureRegion, RectangleVertexBuffer rectangleVertexBuffer) {
        super(f, f2, (float) tiledTextureRegion.getTileWidth(), (float) tiledTextureRegion.getTileHeight(), tiledTextureRegion, rectangleVertexBuffer);
    }

    public TiledSprite(float f, float f2, float f3, float f4, TiledTextureRegion tiledTextureRegion, RectangleVertexBuffer rectangleVertexBuffer) {
        super(f, f2, f3, f4, tiledTextureRegion, rectangleVertexBuffer);
    }

    public TiledTextureRegion getTextureRegion() {
        return (TiledTextureRegion) super.getTextureRegion();
    }

    public int getCurrentTileIndex() {
        return getTextureRegion().getCurrentTileIndex();
    }

    public void setCurrentTileIndex(int i) {
        getTextureRegion().setCurrentTileIndex(i);
    }

    public void setCurrentTileIndex(int i, int i2) {
        getTextureRegion().setCurrentTileIndex(i, i2);
    }

    public void nextTile() {
        getTextureRegion().nextTile();
    }
}
