package org.anddev.andengine.entity.modifier;

import org.anddev.andengine.entity.IEntity;
import org.anddev.andengine.entity.modifier.IEntityModifier;
import org.anddev.andengine.util.modifier.ease.IEaseFunction;

public class RotationAtModifier extends RotationModifier {
    private final float mRotationCenterX;
    private final float mRotationCenterY;

    public RotationAtModifier(float f, float f2, float f3, float f4, float f5) {
        super(f, f2, f3, IEaseFunction.DEFAULT);
        this.mRotationCenterX = f4;
        this.mRotationCenterY = f5;
    }

    public RotationAtModifier(float f, float f2, float f3, float f4, float f5, IEaseFunction iEaseFunction) {
        super(f, f2, f3, iEaseFunction);
        this.mRotationCenterX = f4;
        this.mRotationCenterY = f5;
    }

    public RotationAtModifier(float f, float f2, float f3, float f4, float f5, IEntityModifier.IEntityModifierListener iEntityModifierListener) {
        super(f, f2, f3, iEntityModifierListener, IEaseFunction.DEFAULT);
        this.mRotationCenterX = f4;
        this.mRotationCenterY = f5;
    }

    public RotationAtModifier(float f, float f2, float f3, float f4, float f5, IEntityModifier.IEntityModifierListener iEntityModifierListener, IEaseFunction iEaseFunction) {
        super(f, f2, f3, iEntityModifierListener, iEaseFunction);
        this.mRotationCenterX = f4;
        this.mRotationCenterY = f5;
    }

    protected RotationAtModifier(RotationAtModifier rotationAtModifier) {
        super(rotationAtModifier);
        this.mRotationCenterX = rotationAtModifier.mRotationCenterX;
        this.mRotationCenterY = rotationAtModifier.mRotationCenterY;
    }

    public RotationAtModifier clone() {
        return new RotationAtModifier(this);
    }

    /* access modifiers changed from: protected */
    public void onManagedInitialize(IEntity iEntity) {
        super.onManagedInitialize((Object) iEntity);
        iEntity.setRotationCenter(this.mRotationCenterX, this.mRotationCenterY);
    }
}
