package org.anddev.andengine.entity.modifier;

import org.anddev.andengine.entity.modifier.IEntityModifier;
import org.anddev.andengine.util.modifier.BaseDoubleValueSpanModifier;
import org.anddev.andengine.util.modifier.ease.IEaseFunction;

public abstract class DoubleValueSpanEntityModifier extends BaseDoubleValueSpanModifier implements IEntityModifier {
    public DoubleValueSpanEntityModifier(float f, float f2, float f3, float f4, float f5) {
        super(f, f2, f3, f4, f5);
    }

    public DoubleValueSpanEntityModifier(float f, float f2, float f3, float f4, float f5, IEaseFunction iEaseFunction) {
        super(f, f2, f3, f4, f5, iEaseFunction);
    }

    public DoubleValueSpanEntityModifier(float f, float f2, float f3, float f4, float f5, IEntityModifier.IEntityModifierListener iEntityModifierListener) {
        super(f, f2, f3, f4, f5, iEntityModifierListener);
    }

    public DoubleValueSpanEntityModifier(float f, float f2, float f3, float f4, float f5, IEntityModifier.IEntityModifierListener iEntityModifierListener, IEaseFunction iEaseFunction) {
        super(f, f2, f3, f4, f5, iEntityModifierListener, iEaseFunction);
    }

    protected DoubleValueSpanEntityModifier(DoubleValueSpanEntityModifier doubleValueSpanEntityModifier) {
        super(doubleValueSpanEntityModifier);
    }
}
