package org.anddev.andengine.entity.scene.background.modifier;

import org.anddev.andengine.entity.scene.background.modifier.IBackgroundModifier;
import org.anddev.andengine.util.modifier.LoopModifier;

public class LoopBackgroundModifier extends LoopModifier implements IBackgroundModifier {

    public interface ILoopBackgroundModifierListener extends LoopModifier.ILoopModifierListener {
    }

    public LoopBackgroundModifier(IBackgroundModifier iBackgroundModifier) {
        super(iBackgroundModifier);
    }

    public LoopBackgroundModifier(IBackgroundModifier iBackgroundModifier, int i) {
        super(iBackgroundModifier, i);
    }

    public LoopBackgroundModifier(IBackgroundModifier iBackgroundModifier, int i, ILoopBackgroundModifierListener iLoopBackgroundModifierListener) {
        super(iBackgroundModifier, i, iLoopBackgroundModifierListener, null);
    }

    public LoopBackgroundModifier(IBackgroundModifier iBackgroundModifier, int i, IBackgroundModifier.IBackgroundModifierListener iBackgroundModifierListener) {
        super(iBackgroundModifier, i, iBackgroundModifierListener);
    }

    public LoopBackgroundModifier(IBackgroundModifier iBackgroundModifier, int i, ILoopBackgroundModifierListener iLoopBackgroundModifierListener, IBackgroundModifier.IBackgroundModifierListener iBackgroundModifierListener) {
        super(iBackgroundModifier, i, iLoopBackgroundModifierListener, iBackgroundModifierListener);
    }

    protected LoopBackgroundModifier(LoopBackgroundModifier loopBackgroundModifier) {
        super((LoopModifier) loopBackgroundModifier);
    }

    public LoopBackgroundModifier clone() {
        return new LoopBackgroundModifier(this);
    }
}
