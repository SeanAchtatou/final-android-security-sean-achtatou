package org.anddev.andengine.entity.modifier;

import org.anddev.andengine.entity.modifier.IEntityModifier;
import org.anddev.andengine.util.modifier.ease.IEaseFunction;

public class FadeInModifier extends AlphaModifier {
    public FadeInModifier(float f) {
        super(f, 0.0f, 1.0f, IEaseFunction.DEFAULT);
    }

    public FadeInModifier(float f, IEaseFunction iEaseFunction) {
        super(f, 0.0f, 1.0f, iEaseFunction);
    }

    public FadeInModifier(float f, IEntityModifier.IEntityModifierListener iEntityModifierListener) {
        super(f, 0.0f, 1.0f, iEntityModifierListener, IEaseFunction.DEFAULT);
    }

    public FadeInModifier(float f, IEntityModifier.IEntityModifierListener iEntityModifierListener, IEaseFunction iEaseFunction) {
        super(f, 0.0f, 1.0f, iEntityModifierListener, iEaseFunction);
    }

    protected FadeInModifier(FadeInModifier fadeInModifier) {
        super(fadeInModifier);
    }

    public FadeInModifier clone() {
        return new FadeInModifier(this);
    }
}
