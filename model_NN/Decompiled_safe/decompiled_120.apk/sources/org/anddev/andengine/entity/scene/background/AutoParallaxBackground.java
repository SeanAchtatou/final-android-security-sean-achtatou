package org.anddev.andengine.entity.scene.background;

public class AutoParallaxBackground extends ParallaxBackground {
    private float mParallaxChangePerSecond;

    public AutoParallaxBackground(float f, float f2, float f3, float f4) {
        super(f, f2, f3);
        this.mParallaxChangePerSecond = f4;
    }

    public void setParallaxChangePerSecond(float f) {
        this.mParallaxChangePerSecond = f;
    }

    public void onUpdate(float f) {
        super.onUpdate(f);
        this.mParallaxValue += this.mParallaxChangePerSecond * f;
    }
}
