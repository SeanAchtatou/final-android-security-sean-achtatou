package org.anddev.andengine.entity.scene.background;

import javax.microedition.khronos.opengles.GL10;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.entity.IEntity;

public class EntityBackground extends ColorBackground {
    protected IEntity mEntity;

    public EntityBackground(IEntity iEntity) {
        this.mEntity = iEntity;
    }

    public EntityBackground(float f, float f2, float f3, IEntity iEntity) {
        super(f, f2, f3);
        this.mEntity = iEntity;
    }

    public void onDraw(GL10 gl10, Camera camera) {
        super.onDraw(gl10, camera);
        this.mEntity.onDraw(gl10, camera);
    }
}
