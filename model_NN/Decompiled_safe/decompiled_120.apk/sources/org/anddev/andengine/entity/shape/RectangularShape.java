package org.anddev.andengine.entity.shape;

import javax.microedition.khronos.opengles.GL10;
import org.anddev.andengine.collision.RectangularShapeCollisionChecker;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.entity.primitive.Line;
import org.anddev.andengine.opengl.vertex.VertexBuffer;

public abstract class RectangularShape extends Shape {
    protected float mBaseHeight;
    protected float mBaseWidth;
    protected float mHeight;
    protected final VertexBuffer mVertexBuffer;
    protected float mWidth;

    public RectangularShape(float f, float f2, float f3, float f4, VertexBuffer vertexBuffer) {
        super(f, f2);
        this.mBaseWidth = f3;
        this.mBaseHeight = f4;
        this.mWidth = f3;
        this.mHeight = f4;
        this.mVertexBuffer = vertexBuffer;
        this.mRotationCenterX = f3 * 0.5f;
        this.mRotationCenterY = f4 * 0.5f;
        this.mScaleCenterX = this.mRotationCenterX;
        this.mScaleCenterY = this.mRotationCenterY;
    }

    public VertexBuffer getVertexBuffer() {
        return this.mVertexBuffer;
    }

    public float getWidth() {
        return this.mWidth;
    }

    public float getHeight() {
        return this.mHeight;
    }

    public float getBaseWidth() {
        return this.mBaseWidth;
    }

    public float getBaseHeight() {
        return this.mBaseHeight;
    }

    public void setWidth(float f) {
        this.mWidth = f;
        updateVertexBuffer();
    }

    public void setHeight(float f) {
        this.mHeight = f;
        updateVertexBuffer();
    }

    public void setSize(float f, float f2) {
        this.mWidth = f;
        this.mHeight = f2;
        updateVertexBuffer();
    }

    public void setBaseSize() {
        if (this.mWidth != this.mBaseWidth || this.mHeight != this.mBaseHeight) {
            this.mWidth = this.mBaseWidth;
            this.mHeight = this.mBaseHeight;
            updateVertexBuffer();
        }
    }

    /* access modifiers changed from: protected */
    public boolean isCulled(Camera camera) {
        float f = this.mX;
        float f2 = this.mY;
        return f > camera.getMaxX() || f2 > camera.getMaxY() || f + getWidth() < camera.getMinX() || getHeight() + f2 < camera.getMinY();
    }

    /* access modifiers changed from: protected */
    public void drawVertices(GL10 gl10, Camera camera) {
        gl10.glDrawArrays(5, 0, 4);
    }

    public void reset() {
        super.reset();
        setBaseSize();
        float baseWidth = getBaseWidth();
        float baseHeight = getBaseHeight();
        this.mRotationCenterX = baseWidth * 0.5f;
        this.mRotationCenterY = baseHeight * 0.5f;
        this.mScaleCenterX = this.mRotationCenterX;
        this.mScaleCenterY = this.mRotationCenterY;
    }

    public boolean contains(float f, float f2) {
        return RectangularShapeCollisionChecker.checkContains(this, f, f2);
    }

    public float[] getSceneCenterCoordinates() {
        return convertLocalToSceneCoordinates(this.mWidth * 0.5f, this.mHeight * 0.5f);
    }

    public boolean collidesWith(IShape iShape) {
        if (iShape instanceof RectangularShape) {
            return RectangularShapeCollisionChecker.checkCollision(this, (RectangularShape) iShape);
        }
        if (iShape instanceof Line) {
            return RectangularShapeCollisionChecker.checkCollision(this, (Line) iShape);
        }
        return false;
    }
}
