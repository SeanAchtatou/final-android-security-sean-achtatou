package org.anddev.andengine.entity.layer.tiled.tmx.util.exception;

import org.xml.sax.SAXException;

public class TMXParseException extends SAXException {
    private static final long serialVersionUID = 2213964295487921492L;

    public TMXParseException() {
    }

    public TMXParseException(String str) {
        super(str);
    }

    public TMXParseException(Exception exc) {
        super(exc);
    }

    public TMXParseException(String str, Exception exc) {
        super(str, exc);
    }
}
