package org.anddev.andengine.entity.util;

import android.graphics.Bitmap;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import javax.microedition.khronos.opengles.GL10;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.entity.Entity;
import org.anddev.andengine.entity.util.ScreenGrabber;
import org.anddev.andengine.util.Debug;
import org.anddev.andengine.util.StreamUtils;

public class ScreenCapture extends Entity implements ScreenGrabber.IScreenGrabberCallback {
    private String mFilePath;
    private IScreenCaptureCallback mScreenCaptureCallback;
    private final ScreenGrabber mScreenGrabber = new ScreenGrabber();

    public interface IScreenCaptureCallback {
        void onScreenCaptureFailed(String str, Exception exc);

        void onScreenCaptured(String str);
    }

    /* access modifiers changed from: protected */
    public void onManagedDraw(GL10 gl10, Camera camera) {
        this.mScreenGrabber.onManagedDraw(gl10, camera);
    }

    /* access modifiers changed from: protected */
    public void onManagedUpdate(float f) {
    }

    public void reset() {
    }

    public void onScreenGrabbed(Bitmap bitmap) {
        try {
            saveCapture(bitmap, this.mFilePath);
            this.mScreenCaptureCallback.onScreenCaptured(this.mFilePath);
        } catch (FileNotFoundException e) {
            this.mScreenCaptureCallback.onScreenCaptureFailed(this.mFilePath, e);
        }
    }

    public void onScreenGrabFailed(Exception exc) {
        this.mScreenCaptureCallback.onScreenCaptureFailed(this.mFilePath, exc);
    }

    public void capture(int i, int i2, String str, IScreenCaptureCallback iScreenCaptureCallback) {
        capture(0, 0, i, i2, str, iScreenCaptureCallback);
    }

    public void capture(int i, int i2, int i3, int i4, String str, IScreenCaptureCallback iScreenCaptureCallback) {
        this.mFilePath = str;
        this.mScreenCaptureCallback = iScreenCaptureCallback;
        this.mScreenGrabber.grab(i, i2, i3, i4, this);
    }

    private static void saveCapture(Bitmap bitmap, String str) {
        FileOutputStream fileOutputStream;
        FileNotFoundException e;
        try {
            fileOutputStream = new FileOutputStream(str);
            try {
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream);
            } catch (FileNotFoundException e2) {
                e = e2;
                StreamUtils.flushCloseStream(fileOutputStream);
                Debug.e("Error saving file to: " + str, e);
                throw e;
            }
        } catch (FileNotFoundException e3) {
            FileNotFoundException fileNotFoundException = e3;
            fileOutputStream = null;
            e = fileNotFoundException;
            StreamUtils.flushCloseStream(fileOutputStream);
            Debug.e("Error saving file to: " + str, e);
            throw e;
        }
    }
}
