package org.anddev.andengine.entity.particle.modifier;

import org.anddev.andengine.entity.particle.Particle;

public abstract class BaseTripleValueSpanModifier extends BaseDoubleValueSpanModifier {
    private final float mFromValueC;
    private final float mSpanValueC = (this.mToValueC - this.mFromValueC);
    private final float mToValueC;

    /* access modifiers changed from: protected */
    public abstract void onSetInitialValues(Particle particle, float f, float f2, float f3);

    /* access modifiers changed from: protected */
    public abstract void onSetValues(Particle particle, float f, float f2, float f3);

    public BaseTripleValueSpanModifier(float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8) {
        super(f, f2, f3, f4, f7, f8);
        this.mFromValueC = f5;
        this.mToValueC = f6;
    }

    /* access modifiers changed from: protected */
    public void onSetValues(Particle particle, float f, float f2) {
    }

    public void onSetInitialValues(Particle particle, float f, float f2) {
        onSetInitialValues(particle, f, f2, this.mFromValueC);
    }

    /* access modifiers changed from: protected */
    public void onSetValueInternal(Particle particle, float f) {
        onSetValues(particle, super.calculateValue(f), super.calculateValueB(f), calculateValueC(f));
    }

    private final float calculateValueC(float f) {
        return this.mFromValueC + (this.mSpanValueC * f);
    }
}
