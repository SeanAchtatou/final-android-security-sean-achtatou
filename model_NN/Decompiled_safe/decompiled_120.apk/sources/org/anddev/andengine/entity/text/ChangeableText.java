package org.anddev.andengine.entity.text;

import javax.microedition.khronos.opengles.GL10;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.opengl.font.Font;
import org.anddev.andengine.util.HorizontalAlign;
import org.anddev.andengine.util.StringUtils;

public class ChangeableText extends Text {
    private static final String ELLIPSIS = "...";
    private static final int ELLIPSIS_CHARACTER_COUNT = ELLIPSIS.length();
    private int mCharacterCountCurrentText;

    public ChangeableText(float f, float f2, Font font, String str) {
        this(f, f2, font, str, str.length() - StringUtils.countOccurrences(str, 10));
    }

    public ChangeableText(float f, float f2, Font font, String str, int i) {
        this(f, f2, font, str, HorizontalAlign.LEFT, i);
    }

    public ChangeableText(float f, float f2, Font font, String str, HorizontalAlign horizontalAlign, int i) {
        super(f, f2, font, str, horizontalAlign, i);
        this.mCharacterCountCurrentText = str.length() - StringUtils.countOccurrences(str, 10);
    }

    public void setText(String str) {
        setText(str, false);
    }

    public void setText(String str, boolean z) {
        int length = str.length() - StringUtils.countOccurrences(str, 10);
        if (length > this.mCharactersMaximum) {
            if (!z || this.mCharactersMaximum <= ELLIPSIS_CHARACTER_COUNT) {
                updateText(str.substring(0, this.mCharactersMaximum));
            } else {
                updateText(str.substring(0, this.mCharactersMaximum - ELLIPSIS_CHARACTER_COUNT).concat(ELLIPSIS));
            }
            this.mCharacterCountCurrentText = this.mCharactersMaximum;
            return;
        }
        updateText(str);
        this.mCharacterCountCurrentText = length;
    }

    /* access modifiers changed from: protected */
    public void drawVertices(GL10 gl10, Camera camera) {
        gl10.glDrawArrays(4, 0, this.mCharacterCountCurrentText * 6);
    }
}
