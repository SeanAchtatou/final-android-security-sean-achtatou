package org.anddev.andengine.entity.layer.tiled.tmx.util.exception;

public class TMXLoadException extends TMXException {
    private static final long serialVersionUID = -8295358631698809883L;

    public TMXLoadException() {
    }

    public TMXLoadException(String str, Throwable th) {
        super(str, th);
    }

    public TMXLoadException(String str) {
        super(str);
    }

    public TMXLoadException(Throwable th) {
        super(th);
    }
}
