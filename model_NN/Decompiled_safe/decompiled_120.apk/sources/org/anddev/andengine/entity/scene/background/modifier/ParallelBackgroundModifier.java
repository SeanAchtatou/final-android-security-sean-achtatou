package org.anddev.andengine.entity.scene.background.modifier;

import org.anddev.andengine.entity.scene.background.modifier.IBackgroundModifier;
import org.anddev.andengine.util.modifier.ParallelModifier;

public class ParallelBackgroundModifier extends ParallelModifier implements IBackgroundModifier {
    public ParallelBackgroundModifier(IBackgroundModifier... iBackgroundModifierArr) {
        super(iBackgroundModifierArr);
    }

    public ParallelBackgroundModifier(IBackgroundModifier.IBackgroundModifierListener iBackgroundModifierListener, IBackgroundModifier... iBackgroundModifierArr) {
        super(iBackgroundModifierListener, iBackgroundModifierArr);
    }

    protected ParallelBackgroundModifier(ParallelBackgroundModifier parallelBackgroundModifier) {
        super(parallelBackgroundModifier);
    }

    public ParallelBackgroundModifier clone() {
        return new ParallelBackgroundModifier(this);
    }
}
