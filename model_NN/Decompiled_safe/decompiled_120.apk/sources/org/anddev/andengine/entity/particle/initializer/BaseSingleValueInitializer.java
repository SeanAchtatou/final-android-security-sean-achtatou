package org.anddev.andengine.entity.particle.initializer;

import org.anddev.andengine.entity.particle.Particle;
import org.anddev.andengine.util.MathUtils;

public abstract class BaseSingleValueInitializer implements IParticleInitializer {
    protected float mMaxValue;
    protected float mMinValue;

    /* access modifiers changed from: protected */
    public abstract void onInitializeParticle(Particle particle, float f);

    public BaseSingleValueInitializer(float f, float f2) {
        this.mMinValue = f;
        this.mMaxValue = f2;
    }

    public final void onInitializeParticle(Particle particle) {
        onInitializeParticle(particle, getRandomValue());
    }

    private final float getRandomValue() {
        if (this.mMinValue == this.mMaxValue) {
            return this.mMaxValue;
        }
        return (MathUtils.RANDOM.nextFloat() * (this.mMaxValue - this.mMinValue)) + this.mMinValue;
    }
}
