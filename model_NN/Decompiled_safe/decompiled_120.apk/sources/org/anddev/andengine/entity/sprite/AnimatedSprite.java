package org.anddev.andengine.entity.sprite;

import java.util.Arrays;
import org.anddev.andengine.opengl.texture.region.TiledTextureRegion;
import org.anddev.andengine.opengl.vertex.RectangleVertexBuffer;
import org.anddev.andengine.util.MathUtils;
import org.anddev.andengine.util.constants.TimeConstants;

public class AnimatedSprite extends TiledSprite implements TimeConstants {
    private static final int LOOP_CONTINUOUS = -1;
    private long mAnimationDuration;
    private IAnimationListener mAnimationListener;
    private long mAnimationProgress;
    private boolean mAnimationRunning;
    private int mFirstTileIndex;
    private int mFrameCount;
    private long[] mFrameEndsInNanoseconds;
    private int[] mFrames;
    private int mInitialLoopCount;
    private int mLoopCount;

    public interface IAnimationListener {
        void onAnimationEnd(AnimatedSprite animatedSprite);
    }

    public AnimatedSprite(float f, float f2, TiledTextureRegion tiledTextureRegion) {
        super(f, f2, tiledTextureRegion);
    }

    public AnimatedSprite(float f, float f2, float f3, float f4, TiledTextureRegion tiledTextureRegion) {
        super(f, f2, f3, f4, tiledTextureRegion);
    }

    public AnimatedSprite(float f, float f2, TiledTextureRegion tiledTextureRegion, RectangleVertexBuffer rectangleVertexBuffer) {
        super(f, f2, tiledTextureRegion, rectangleVertexBuffer);
    }

    public AnimatedSprite(float f, float f2, float f3, float f4, TiledTextureRegion tiledTextureRegion, RectangleVertexBuffer rectangleVertexBuffer) {
        super(f, f2, f3, f4, tiledTextureRegion, rectangleVertexBuffer);
    }

    public boolean isAnimationRunning() {
        return this.mAnimationRunning;
    }

    /* access modifiers changed from: protected */
    public void onManagedUpdate(float f) {
        super.onManagedUpdate(f);
        if (this.mAnimationRunning) {
            this.mAnimationProgress = ((long) (1.0E9f * f)) + this.mAnimationProgress;
            if (this.mAnimationProgress > this.mAnimationDuration) {
                this.mAnimationProgress %= this.mAnimationDuration;
                if (this.mInitialLoopCount != -1) {
                    this.mLoopCount--;
                }
            }
            if (this.mInitialLoopCount == -1 || this.mLoopCount >= 0) {
                int calculateCurrentFrameIndex = calculateCurrentFrameIndex();
                if (this.mFrames == null) {
                    setCurrentTileIndex(calculateCurrentFrameIndex + this.mFirstTileIndex);
                } else {
                    setCurrentTileIndex(this.mFrames[calculateCurrentFrameIndex]);
                }
            } else {
                this.mAnimationRunning = false;
                if (this.mAnimationListener != null) {
                    this.mAnimationListener.onAnimationEnd(this);
                }
            }
        }
    }

    public void stopAnimation() {
        this.mAnimationRunning = false;
    }

    public void stopAnimation(int i) {
        this.mAnimationRunning = false;
        setCurrentTileIndex(i);
    }

    private int calculateCurrentFrameIndex() {
        long j = this.mAnimationProgress;
        long[] jArr = this.mFrameEndsInNanoseconds;
        int i = this.mFrameCount;
        for (int i2 = 0; i2 < i; i2++) {
            if (jArr[i2] > j) {
                return i2;
            }
        }
        return i - 1;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.anddev.andengine.entity.sprite.AnimatedSprite.animate(long, boolean):org.anddev.andengine.entity.sprite.AnimatedSprite
     arg types: [long, int]
     candidates:
      org.anddev.andengine.entity.sprite.AnimatedSprite.animate(long, int):org.anddev.andengine.entity.sprite.AnimatedSprite
      org.anddev.andengine.entity.sprite.AnimatedSprite.animate(long[], int):org.anddev.andengine.entity.sprite.AnimatedSprite
      org.anddev.andengine.entity.sprite.AnimatedSprite.animate(long[], boolean):org.anddev.andengine.entity.sprite.AnimatedSprite
      org.anddev.andengine.entity.sprite.AnimatedSprite.animate(long, boolean):org.anddev.andengine.entity.sprite.AnimatedSprite */
    public AnimatedSprite animate(long j) {
        return animate(j, true);
    }

    public AnimatedSprite animate(long j, boolean z) {
        return animate(j, z ? -1 : 0, (IAnimationListener) null);
    }

    public AnimatedSprite animate(long j, int i) {
        return animate(j, i, (IAnimationListener) null);
    }

    public AnimatedSprite animate(long j, boolean z, IAnimationListener iAnimationListener) {
        return animate(j, z ? -1 : 0, iAnimationListener);
    }

    public AnimatedSprite animate(long j, int i, IAnimationListener iAnimationListener) {
        long[] jArr = new long[getTextureRegion().getTileCount()];
        Arrays.fill(jArr, j);
        return animate(jArr, i, iAnimationListener);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.anddev.andengine.entity.sprite.AnimatedSprite.animate(long[], boolean):org.anddev.andengine.entity.sprite.AnimatedSprite
     arg types: [long[], int]
     candidates:
      org.anddev.andengine.entity.sprite.AnimatedSprite.animate(long, int):org.anddev.andengine.entity.sprite.AnimatedSprite
      org.anddev.andengine.entity.sprite.AnimatedSprite.animate(long, boolean):org.anddev.andengine.entity.sprite.AnimatedSprite
      org.anddev.andengine.entity.sprite.AnimatedSprite.animate(long[], int):org.anddev.andengine.entity.sprite.AnimatedSprite
      org.anddev.andengine.entity.sprite.AnimatedSprite.animate(long[], boolean):org.anddev.andengine.entity.sprite.AnimatedSprite */
    public AnimatedSprite animate(long[] jArr) {
        return animate(jArr, true);
    }

    public AnimatedSprite animate(long[] jArr, boolean z) {
        return animate(jArr, z ? -1 : 0, (IAnimationListener) null);
    }

    public AnimatedSprite animate(long[] jArr, int i) {
        return animate(jArr, i, (IAnimationListener) null);
    }

    public AnimatedSprite animate(long[] jArr, boolean z, IAnimationListener iAnimationListener) {
        return animate(jArr, z ? -1 : 0, iAnimationListener);
    }

    public AnimatedSprite animate(long[] jArr, int i, IAnimationListener iAnimationListener) {
        return animate(jArr, 0, getTextureRegion().getTileCount() - 1, i, iAnimationListener);
    }

    public AnimatedSprite animate(long[] jArr, int i, int i2, boolean z) {
        return animate(jArr, i, i2, z ? -1 : 0, null);
    }

    public AnimatedSprite animate(long[] jArr, int i, int i2, int i3) {
        return animate(jArr, i, i2, i3, null);
    }

    public AnimatedSprite animate(long[] jArr, int[] iArr, int i) {
        return animate(jArr, iArr, i, (IAnimationListener) null);
    }

    public AnimatedSprite animate(long[] jArr, int[] iArr, int i, IAnimationListener iAnimationListener) {
        int length = iArr.length;
        if (jArr.length == length) {
            return init(jArr, length, iArr, 0, i, iAnimationListener);
        }
        throw new IllegalArgumentException("pFrameDurations must have the same length as pFrames.");
    }

    public AnimatedSprite animate(long[] jArr, int i, int i2, int i3, IAnimationListener iAnimationListener) {
        if (i2 - i <= 0) {
            throw new IllegalArgumentException("An animation needs at least two tiles to animate between.");
        }
        int i4 = (i2 - i) + 1;
        if (jArr.length == i4) {
            return init(jArr, i4, null, i, i3, iAnimationListener);
        }
        throw new IllegalArgumentException("pFrameDurations must have the same length as pFirstTileIndex to pLastTileIndex.");
    }

    private AnimatedSprite init(long[] jArr, int i, int[] iArr, int i2, int i3, IAnimationListener iAnimationListener) {
        this.mFrameCount = i;
        this.mAnimationListener = iAnimationListener;
        this.mInitialLoopCount = i3;
        this.mLoopCount = i3;
        this.mFrames = iArr;
        this.mFirstTileIndex = i2;
        if (this.mFrameEndsInNanoseconds == null || this.mFrameCount > this.mFrameEndsInNanoseconds.length) {
            this.mFrameEndsInNanoseconds = new long[this.mFrameCount];
        }
        long[] jArr2 = this.mFrameEndsInNanoseconds;
        MathUtils.arraySumInto(jArr, jArr2, TimeConstants.NANOSECONDSPERMILLISECOND);
        this.mAnimationDuration = jArr2[this.mFrameCount - 1];
        this.mAnimationProgress = 0;
        this.mAnimationRunning = true;
        return this;
    }
}
