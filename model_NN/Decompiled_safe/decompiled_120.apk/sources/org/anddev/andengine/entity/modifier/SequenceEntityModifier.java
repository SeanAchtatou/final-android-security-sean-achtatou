package org.anddev.andengine.entity.modifier;

import org.anddev.andengine.entity.modifier.IEntityModifier;
import org.anddev.andengine.util.modifier.SequenceModifier;

public class SequenceEntityModifier extends SequenceModifier implements IEntityModifier {

    public interface ISubSequenceShapeModifierListener extends SequenceModifier.ISubSequenceModifierListener {
    }

    public SequenceEntityModifier(IEntityModifier... iEntityModifierArr) {
        super(iEntityModifierArr);
    }

    public SequenceEntityModifier(ISubSequenceShapeModifierListener iSubSequenceShapeModifierListener, IEntityModifier... iEntityModifierArr) {
        super(iSubSequenceShapeModifierListener, iEntityModifierArr);
    }

    public SequenceEntityModifier(IEntityModifier.IEntityModifierListener iEntityModifierListener, IEntityModifier... iEntityModifierArr) {
        super(iEntityModifierListener, iEntityModifierArr);
    }

    public SequenceEntityModifier(ISubSequenceShapeModifierListener iSubSequenceShapeModifierListener, IEntityModifier.IEntityModifierListener iEntityModifierListener, IEntityModifier... iEntityModifierArr) {
        super(iSubSequenceShapeModifierListener, iEntityModifierListener, iEntityModifierArr);
    }

    protected SequenceEntityModifier(SequenceEntityModifier sequenceEntityModifier) {
        super(sequenceEntityModifier);
    }

    public SequenceEntityModifier clone() {
        return new SequenceEntityModifier(this);
    }
}
