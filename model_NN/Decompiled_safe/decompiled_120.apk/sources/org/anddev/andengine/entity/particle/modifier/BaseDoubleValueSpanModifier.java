package org.anddev.andengine.entity.particle.modifier;

import org.anddev.andengine.entity.particle.Particle;

public abstract class BaseDoubleValueSpanModifier extends BaseSingleValueSpanModifier {
    private final float mFromValueB;
    private final float mSpanValueB = (this.mToValueB - this.mFromValueB);
    private final float mToValueB;

    /* access modifiers changed from: protected */
    public abstract void onSetInitialValues(Particle particle, float f, float f2);

    /* access modifiers changed from: protected */
    public abstract void onSetValues(Particle particle, float f, float f2);

    public BaseDoubleValueSpanModifier(float f, float f2, float f3, float f4, float f5, float f6) {
        super(f, f2, f5, f6);
        this.mFromValueB = f3;
        this.mToValueB = f4;
    }

    /* access modifiers changed from: protected */
    public void onSetValue(Particle particle, float f) {
    }

    public void onSetInitialValue(Particle particle, float f) {
        onSetInitialValues(particle, f, this.mFromValueB);
    }

    /* access modifiers changed from: protected */
    public void onSetValueInternal(Particle particle, float f) {
        onSetValues(particle, super.calculateValue(f), calculateValueB(f));
    }

    /* access modifiers changed from: protected */
    public final float calculateValueB(float f) {
        return this.mFromValueB + (this.mSpanValueB * f);
    }
}
