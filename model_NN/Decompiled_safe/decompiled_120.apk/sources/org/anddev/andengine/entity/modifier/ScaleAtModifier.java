package org.anddev.andengine.entity.modifier;

import org.anddev.andengine.entity.IEntity;
import org.anddev.andengine.entity.modifier.IEntityModifier;
import org.anddev.andengine.util.modifier.ease.IEaseFunction;

public class ScaleAtModifier extends ScaleModifier {
    private final float mScaleCenterX;
    private final float mScaleCenterY;

    public ScaleAtModifier(float f, float f2, float f3, float f4, float f5) {
        this(f, f2, f3, f4, f5, IEaseFunction.DEFAULT);
    }

    public ScaleAtModifier(float f, float f2, float f3, float f4, float f5, IEaseFunction iEaseFunction) {
        this(f, f2, f3, f4, f5, (IEntityModifier.IEntityModifierListener) null, iEaseFunction);
    }

    public ScaleAtModifier(float f, float f2, float f3, float f4, float f5, IEntityModifier.IEntityModifierListener iEntityModifierListener) {
        this(f, f2, f3, f4, f5, iEntityModifierListener, IEaseFunction.DEFAULT);
    }

    public ScaleAtModifier(float f, float f2, float f3, float f4, float f5, IEntityModifier.IEntityModifierListener iEntityModifierListener, IEaseFunction iEaseFunction) {
        this(f, f2, f3, f2, f3, f4, f5, iEntityModifierListener, iEaseFunction);
    }

    public ScaleAtModifier(float f, float f2, float f3, float f4, float f5, float f6, float f7) {
        this(f, f2, f3, f4, f5, f6, f7, IEaseFunction.DEFAULT);
    }

    public ScaleAtModifier(float f, float f2, float f3, float f4, float f5, float f6, float f7, IEaseFunction iEaseFunction) {
        this(f, f2, f3, f4, f5, f6, f7, null, iEaseFunction);
    }

    public ScaleAtModifier(float f, float f2, float f3, float f4, float f5, float f6, float f7, IEntityModifier.IEntityModifierListener iEntityModifierListener) {
        this(f, f2, f3, f4, f5, f6, f7, iEntityModifierListener, IEaseFunction.DEFAULT);
    }

    public ScaleAtModifier(float f, float f2, float f3, float f4, float f5, float f6, float f7, IEntityModifier.IEntityModifierListener iEntityModifierListener, IEaseFunction iEaseFunction) {
        super(f, f2, f3, f4, f5, iEntityModifierListener, iEaseFunction);
        this.mScaleCenterX = f6;
        this.mScaleCenterY = f7;
    }

    protected ScaleAtModifier(ScaleAtModifier scaleAtModifier) {
        super(scaleAtModifier);
        this.mScaleCenterX = scaleAtModifier.mScaleCenterX;
        this.mScaleCenterY = scaleAtModifier.mScaleCenterY;
    }

    public ScaleAtModifier clone() {
        return new ScaleAtModifier(this);
    }

    /* access modifiers changed from: protected */
    public void onManagedInitialize(IEntity iEntity) {
        super.onManagedInitialize((Object) iEntity);
        iEntity.setScaleCenter(this.mScaleCenterX, this.mScaleCenterY);
    }
}
