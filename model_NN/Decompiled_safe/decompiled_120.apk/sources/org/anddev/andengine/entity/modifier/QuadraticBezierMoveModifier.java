package org.anddev.andengine.entity.modifier;

import org.anddev.andengine.entity.IEntity;
import org.anddev.andengine.util.modifier.ease.IEaseFunction;

public class QuadraticBezierMoveModifier extends DurationEntityModifier {
    private final IEaseFunction mEaseFunction;
    private final float mX1;
    private final float mX2;
    private final float mX3;
    private final float mY1;
    private final float mY2;
    private final float mY3;

    public QuadraticBezierMoveModifier(float f, float f2, float f3, float f4, float f5, float f6, float f7, IEaseFunction iEaseFunction) {
        super(f);
        this.mX1 = f2;
        this.mY1 = f3;
        this.mX2 = f4;
        this.mY2 = f5;
        this.mX3 = f6;
        this.mY3 = f7;
        this.mEaseFunction = iEaseFunction;
    }

    public QuadraticBezierMoveModifier clone() {
        return new QuadraticBezierMoveModifier(this.mDuration, this.mX1, this.mY1, this.mX2, this.mY2, this.mX3, this.mY3, this.mEaseFunction);
    }

    /* access modifiers changed from: protected */
    public void onManagedUpdate(float f, IEntity iEntity) {
        float percentage = this.mEaseFunction.getPercentage(getSecondsElapsed(), this.mDuration);
        float f2 = 1.0f - percentage;
        float f3 = percentage * percentage;
        float f4 = f2 * f2;
        float f5 = percentage * f2 * 2.0f;
        iEntity.setPosition((this.mX1 * f4) + (this.mX2 * f5) + (this.mX3 * f3), (f5 * this.mY2) + (f4 * this.mY1) + (f3 * this.mY3));
    }

    /* access modifiers changed from: protected */
    public void onManagedInitialize(IEntity iEntity) {
    }

    public void updatePosition(float f, IEntity iEntity) {
        float f2 = 1.0f - f;
        float f3 = f * f;
        float f4 = f2 * f2;
        float f5 = f2 * 2.0f * f;
        iEntity.setPosition((this.mX1 * f4) + (this.mX2 * f5) + (this.mX3 * f3), (f5 * this.mY2) + (f4 * this.mY1) + (f3 * this.mY3));
    }
}
