package org.anddev.andengine.entity.scene.background;

import java.util.ArrayList;
import javax.microedition.khronos.opengles.GL10;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.entity.shape.Shape;

public class ParallaxBackground extends ColorBackground {
    private final ArrayList mParallaxEntities = new ArrayList();
    private int mParallaxEntityCount;
    protected float mParallaxValue;

    public ParallaxBackground(float f, float f2, float f3) {
        super(f, f2, f3);
    }

    public void setParallaxValue(float f) {
        this.mParallaxValue = f;
    }

    public void onDraw(GL10 gl10, Camera camera) {
        super.onDraw(gl10, camera);
        float f = this.mParallaxValue;
        ArrayList arrayList = this.mParallaxEntities;
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.mParallaxEntityCount) {
                ((ParallaxEntity) arrayList.get(i2)).onDraw(gl10, f, camera);
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    public void attachParallaxEntity(ParallaxEntity parallaxEntity) {
        this.mParallaxEntities.add(parallaxEntity);
        this.mParallaxEntityCount++;
    }

    public boolean detachParallaxEntity(ParallaxEntity parallaxEntity) {
        this.mParallaxEntityCount--;
        boolean remove = this.mParallaxEntities.remove(parallaxEntity);
        if (!remove) {
            this.mParallaxEntityCount++;
        }
        return remove;
    }

    public class ParallaxEntity {
        final float mParallaxFactor;
        final Shape mShape;

        public ParallaxEntity(float f, Shape shape) {
            this.mParallaxFactor = f;
            this.mShape = shape;
        }

        public void onDraw(GL10 gl10, float f, Camera camera) {
            gl10.glPushMatrix();
            float width = camera.getWidth();
            float widthScaled = this.mShape.getWidthScaled();
            float f2 = (this.mParallaxFactor * f) % widthScaled;
            while (f2 > 0.0f) {
                f2 -= widthScaled;
            }
            gl10.glTranslatef(f2, 0.0f, 0.0f);
            do {
                this.mShape.onDraw(gl10, camera);
                gl10.glTranslatef(widthScaled, 0.0f, 0.0f);
                f2 += widthScaled;
            } while (f2 < width);
            gl10.glPopMatrix();
        }
    }
}
