package org.anddev.andengine.entity.util;

import org.anddev.andengine.engine.handler.IUpdateHandler;
import org.anddev.andengine.util.Debug;
import org.anddev.andengine.util.constants.TimeConstants;

public class FrameCountCrasher implements IUpdateHandler, TimeConstants {
    private final float[] mFrameLengths;
    private int mFramesLeft;

    public FrameCountCrasher(int i) {
        this.mFramesLeft = i;
        this.mFrameLengths = new float[i];
    }

    public void onUpdate(float f) {
        this.mFramesLeft--;
        float[] fArr = this.mFrameLengths;
        if (this.mFramesLeft >= 0) {
            fArr[this.mFramesLeft] = f;
            return;
        }
        for (int length = fArr.length - 1; length >= 0; length--) {
            Debug.d("Elapsed: " + fArr[length]);
        }
        throw new RuntimeException();
    }

    public void reset() {
    }
}
