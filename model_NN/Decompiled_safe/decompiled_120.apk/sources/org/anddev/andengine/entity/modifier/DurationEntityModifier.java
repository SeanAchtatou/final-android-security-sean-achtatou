package org.anddev.andengine.entity.modifier;

import org.anddev.andengine.entity.modifier.IEntityModifier;
import org.anddev.andengine.util.modifier.BaseDurationModifier;

public abstract class DurationEntityModifier extends BaseDurationModifier implements IEntityModifier {
    public DurationEntityModifier(float f) {
        super(f);
    }

    public DurationEntityModifier(float f, IEntityModifier.IEntityModifierListener iEntityModifierListener) {
        super(f, iEntityModifierListener);
    }

    protected DurationEntityModifier(DurationEntityModifier durationEntityModifier) {
        super(durationEntityModifier);
    }
}
