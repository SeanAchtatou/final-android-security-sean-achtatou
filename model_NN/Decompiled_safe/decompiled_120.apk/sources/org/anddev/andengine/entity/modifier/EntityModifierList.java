package org.anddev.andengine.entity.modifier;

import org.anddev.andengine.entity.IEntity;
import org.anddev.andengine.util.modifier.ModifierList;

public class EntityModifierList extends ModifierList {
    private static final long serialVersionUID = 161652765736600082L;

    public EntityModifierList(IEntity iEntity) {
        super(iEntity);
    }

    public EntityModifierList(IEntity iEntity, int i) {
        super(iEntity, i);
    }
}
