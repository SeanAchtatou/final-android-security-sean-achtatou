package org.anddev.andengine.entity.modifier;

import org.anddev.andengine.entity.IEntity;
import org.anddev.andengine.entity.modifier.IEntityModifier;

public class DelayModifier extends DurationEntityModifier {
    public DelayModifier(float f, IEntityModifier.IEntityModifierListener iEntityModifierListener) {
        super(f, iEntityModifierListener);
    }

    public DelayModifier(float f) {
        super(f);
    }

    protected DelayModifier(DelayModifier delayModifier) {
        super(delayModifier);
    }

    public DelayModifier clone() {
        return new DelayModifier(this);
    }

    /* access modifiers changed from: protected */
    public void onManagedInitialize(IEntity iEntity) {
    }

    /* access modifiers changed from: protected */
    public void onManagedUpdate(float f, IEntity iEntity) {
    }
}
