package org.anddev.andengine.entity.particle.initializer;

import org.anddev.andengine.entity.particle.Particle;

public class AlphaInitializer extends BaseSingleValueInitializer {
    public AlphaInitializer(float f) {
        super(f, f);
    }

    public AlphaInitializer(float f, float f2) {
        super(f, f2);
    }

    /* access modifiers changed from: protected */
    public void onInitializeParticle(Particle particle, float f) {
        particle.setAlpha(f);
    }
}
