package org.anddev.andengine.entity;

import java.util.Collections;
import java.util.Comparator;
import javax.microedition.khronos.opengles.GL10;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.engine.handler.IUpdateHandler;
import org.anddev.andengine.engine.handler.UpdateHandlerList;
import org.anddev.andengine.entity.IEntity;
import org.anddev.andengine.entity.modifier.EntityModifierList;
import org.anddev.andengine.entity.modifier.IEntityModifier;
import org.anddev.andengine.util.IMatcher;
import org.anddev.andengine.util.ParameterCallable;
import org.anddev.andengine.util.SmartList;
import org.anddev.andengine.util.Transformation;

public class Entity implements IEntity {
    private static final int CHILDREN_CAPACITY_DEFAULT = 4;
    private static final int ENTITYMODIFIERS_CAPACITY_DEFAULT = 4;
    private static final ParameterCallable PARAMETERCALLABLE_DETACHCHILD = new ParameterCallable() {
        public void call(IEntity iEntity) {
            iEntity.setParent(null);
            iEntity.onDetached();
        }
    };
    private static final int UPDATEHANDLERS_CAPACITY_DEFAULT = 4;
    private static final float[] VERTICES_LOCAL_TO_SCENE_TMP = new float[2];
    private static final float[] VERTICES_SCENE_TO_LOCAL_TMP = new float[2];
    protected float mAlpha;
    protected float mBlue;
    protected SmartList mChildren;
    protected boolean mChildrenIgnoreUpdate;
    protected boolean mChildrenVisible;
    private EntityModifierList mEntityModifiers;
    protected float mGreen;
    protected boolean mIgnoreUpdate;
    private final float mInitialX;
    private final float mInitialY;
    private final Transformation mLocalToParentTransformation;
    private boolean mLocalToParentTransformationDirty;
    private final Transformation mLocalToSceneTransformation;
    private IEntity mParent;
    private final Transformation mParentToLocalTransformation;
    private boolean mParentToLocalTransformationDirty;
    protected float mRed;
    protected float mRotation;
    protected float mRotationCenterX;
    protected float mRotationCenterY;
    protected float mScaleCenterX;
    protected float mScaleCenterY;
    protected float mScaleX;
    protected float mScaleY;
    private final Transformation mSceneToLocalTransformation;
    private UpdateHandlerList mUpdateHandlers;
    private Object mUserData;
    protected boolean mVisible;
    protected float mX;
    protected float mY;
    protected int mZIndex;

    public Entity() {
        this(0.0f, 0.0f);
    }

    public Entity(float f, float f2) {
        this.mVisible = true;
        this.mIgnoreUpdate = false;
        this.mChildrenVisible = true;
        this.mChildrenIgnoreUpdate = false;
        this.mZIndex = 0;
        this.mRed = 1.0f;
        this.mGreen = 1.0f;
        this.mBlue = 1.0f;
        this.mAlpha = 1.0f;
        this.mRotation = 0.0f;
        this.mRotationCenterX = 0.0f;
        this.mRotationCenterY = 0.0f;
        this.mScaleX = 1.0f;
        this.mScaleY = 1.0f;
        this.mScaleCenterX = 0.0f;
        this.mScaleCenterY = 0.0f;
        this.mLocalToParentTransformationDirty = true;
        this.mParentToLocalTransformationDirty = true;
        this.mLocalToParentTransformation = new Transformation();
        this.mParentToLocalTransformation = new Transformation();
        this.mLocalToSceneTransformation = new Transformation();
        this.mSceneToLocalTransformation = new Transformation();
        this.mInitialX = f;
        this.mInitialY = f2;
        this.mX = f;
        this.mY = f2;
    }

    public boolean isVisible() {
        return this.mVisible;
    }

    public void setVisible(boolean z) {
        this.mVisible = z;
    }

    public boolean isChildrenVisible() {
        return this.mChildrenVisible;
    }

    public void setChildrenVisible(boolean z) {
        this.mChildrenVisible = z;
    }

    public boolean isIgnoreUpdate() {
        return this.mIgnoreUpdate;
    }

    public void setIgnoreUpdate(boolean z) {
        this.mIgnoreUpdate = z;
    }

    public boolean isChildrenIgnoreUpdate() {
        return this.mChildrenIgnoreUpdate;
    }

    public void setChildrenIgnoreUpdate(boolean z) {
        this.mChildrenIgnoreUpdate = z;
    }

    public boolean hasParent() {
        return this.mParent != null;
    }

    public IEntity getParent() {
        return this.mParent;
    }

    public void setParent(IEntity iEntity) {
        this.mParent = iEntity;
    }

    public int getZIndex() {
        return this.mZIndex;
    }

    public void setZIndex(int i) {
        this.mZIndex = i;
    }

    public float getX() {
        return this.mX;
    }

    public float getY() {
        return this.mY;
    }

    public float getInitialX() {
        return this.mInitialX;
    }

    public float getInitialY() {
        return this.mInitialY;
    }

    public void setPosition(IEntity iEntity) {
        setPosition(iEntity.getX(), iEntity.getY());
    }

    public void setPosition(float f, float f2) {
        this.mX = f;
        this.mY = f2;
        this.mLocalToParentTransformationDirty = true;
        this.mParentToLocalTransformationDirty = true;
    }

    public void setInitialPosition() {
        this.mX = this.mInitialX;
        this.mY = this.mInitialY;
        this.mLocalToParentTransformationDirty = true;
        this.mParentToLocalTransformationDirty = true;
    }

    public float getRotation() {
        return this.mRotation;
    }

    public boolean isRotated() {
        return this.mRotation != 0.0f;
    }

    public void setRotation(float f) {
        this.mRotation = f;
        this.mLocalToParentTransformationDirty = true;
        this.mParentToLocalTransformationDirty = true;
    }

    public float getRotationCenterX() {
        return this.mRotationCenterX;
    }

    public float getRotationCenterY() {
        return this.mRotationCenterY;
    }

    public void setRotationCenterX(float f) {
        this.mRotationCenterX = f;
        this.mLocalToParentTransformationDirty = true;
        this.mParentToLocalTransformationDirty = true;
    }

    public void setRotationCenterY(float f) {
        this.mRotationCenterY = f;
        this.mLocalToParentTransformationDirty = true;
        this.mParentToLocalTransformationDirty = true;
    }

    public void setRotationCenter(float f, float f2) {
        this.mRotationCenterX = f;
        this.mRotationCenterY = f2;
        this.mLocalToParentTransformationDirty = true;
        this.mParentToLocalTransformationDirty = true;
    }

    public boolean isScaled() {
        return (this.mScaleX == 1.0f && this.mScaleY == 1.0f) ? false : true;
    }

    public float getScaleX() {
        return this.mScaleX;
    }

    public float getScaleY() {
        return this.mScaleY;
    }

    public void setScaleX(float f) {
        this.mScaleX = f;
        this.mLocalToParentTransformationDirty = true;
        this.mParentToLocalTransformationDirty = true;
    }

    public void setScaleY(float f) {
        this.mScaleY = f;
        this.mLocalToParentTransformationDirty = true;
        this.mParentToLocalTransformationDirty = true;
    }

    public void setScale(float f) {
        this.mScaleX = f;
        this.mScaleY = f;
        this.mLocalToParentTransformationDirty = true;
        this.mParentToLocalTransformationDirty = true;
    }

    public void setScale(float f, float f2) {
        this.mScaleX = f;
        this.mScaleY = f2;
        this.mLocalToParentTransformationDirty = true;
        this.mParentToLocalTransformationDirty = true;
    }

    public float getScaleCenterX() {
        return this.mScaleCenterX;
    }

    public float getScaleCenterY() {
        return this.mScaleCenterY;
    }

    public void setScaleCenterX(float f) {
        this.mScaleCenterX = f;
        this.mLocalToParentTransformationDirty = true;
        this.mParentToLocalTransformationDirty = true;
    }

    public void setScaleCenterY(float f) {
        this.mScaleCenterY = f;
        this.mLocalToParentTransformationDirty = true;
        this.mParentToLocalTransformationDirty = true;
    }

    public void setScaleCenter(float f, float f2) {
        this.mScaleCenterX = f;
        this.mScaleCenterY = f2;
        this.mLocalToParentTransformationDirty = true;
        this.mParentToLocalTransformationDirty = true;
    }

    public float getRed() {
        return this.mRed;
    }

    public float getGreen() {
        return this.mGreen;
    }

    public float getBlue() {
        return this.mBlue;
    }

    public float getAlpha() {
        return this.mAlpha;
    }

    public void setAlpha(float f) {
        this.mAlpha = f;
    }

    public void setColor(float f, float f2, float f3) {
        this.mRed = f;
        this.mGreen = f2;
        this.mBlue = f3;
    }

    public void setColor(float f, float f2, float f3, float f4) {
        this.mRed = f;
        this.mGreen = f2;
        this.mBlue = f3;
        this.mAlpha = f4;
    }

    public int getChildCount() {
        if (this.mChildren == null) {
            return 0;
        }
        return this.mChildren.size();
    }

    public IEntity getChild(int i) {
        if (this.mChildren == null) {
            return null;
        }
        return (IEntity) this.mChildren.get(i);
    }

    public int getChildIndex(IEntity iEntity) {
        if (this.mChildren == null || iEntity.getParent() != this) {
            return -1;
        }
        return this.mChildren.indexOf(iEntity);
    }

    public boolean setChildIndex(IEntity iEntity, int i) {
        if (this.mChildren == null || iEntity.getParent() != this) {
            return false;
        }
        try {
            this.mChildren.remove(iEntity);
            this.mChildren.add(i, iEntity);
            return true;
        } catch (IndexOutOfBoundsException e) {
            return false;
        }
    }

    public IEntity getFirstChild() {
        if (this.mChildren == null) {
            return null;
        }
        return (IEntity) this.mChildren.get(0);
    }

    public IEntity getLastChild() {
        if (this.mChildren == null) {
            return null;
        }
        return (IEntity) this.mChildren.get(this.mChildren.size() - 1);
    }

    public boolean detachSelf() {
        IEntity iEntity = this.mParent;
        if (iEntity != null) {
            return iEntity.detachChild(this);
        }
        return false;
    }

    public void detachChildren() {
        if (this.mChildren != null) {
            this.mChildren.clear(PARAMETERCALLABLE_DETACHCHILD);
        }
    }

    public void attachChild(IEntity iEntity) {
        if (iEntity.hasParent()) {
            throw new IllegalStateException("pEntity already has a parent!");
        }
        if (this.mChildren == null) {
            allocateChildren();
        }
        this.mChildren.add(iEntity);
        iEntity.setParent(this);
        iEntity.onAttached();
    }

    public boolean attachChild(IEntity iEntity, int i) {
        if (iEntity.hasParent()) {
            throw new IllegalStateException("pEntity already has a parent!");
        }
        if (this.mChildren == null) {
            allocateChildren();
        }
        try {
            this.mChildren.add(i, iEntity);
            iEntity.setParent(this);
            iEntity.onAttached();
            return true;
        } catch (IndexOutOfBoundsException e) {
            return false;
        }
    }

    public IEntity findChild(IEntity.IEntityMatcher iEntityMatcher) {
        if (this.mChildren == null) {
            return null;
        }
        return (IEntity) this.mChildren.find(iEntityMatcher);
    }

    public boolean swapChildren(IEntity iEntity, IEntity iEntity2) {
        return swapChildren(getChildIndex(iEntity), getChildIndex(iEntity2));
    }

    public boolean swapChildren(int i, int i2) {
        try {
            Collections.swap(this.mChildren, i, i2);
            return true;
        } catch (IndexOutOfBoundsException e) {
            return false;
        }
    }

    public void sortChildren() {
        if (this.mChildren != null) {
            ZIndexSorter.getInstance().sort(this.mChildren);
        }
    }

    public void sortChildren(Comparator comparator) {
        if (this.mChildren != null) {
            ZIndexSorter.getInstance().sort(this.mChildren, comparator);
        }
    }

    public boolean detachChild(IEntity iEntity) {
        if (this.mChildren == null) {
            return false;
        }
        return this.mChildren.remove(iEntity, PARAMETERCALLABLE_DETACHCHILD);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.anddev.andengine.util.SmartList.remove(org.anddev.andengine.util.IMatcher, org.anddev.andengine.util.ParameterCallable):java.lang.Object
     arg types: [org.anddev.andengine.entity.IEntity$IEntityMatcher, org.anddev.andengine.util.ParameterCallable]
     candidates:
      org.anddev.andengine.util.SmartList.remove(java.lang.Object, org.anddev.andengine.util.ParameterCallable):boolean
      org.anddev.andengine.util.SmartList.remove(org.anddev.andengine.util.IMatcher, org.anddev.andengine.util.ParameterCallable):java.lang.Object */
    public IEntity detachChild(IEntity.IEntityMatcher iEntityMatcher) {
        if (this.mChildren == null) {
            return null;
        }
        return (IEntity) this.mChildren.remove((IMatcher) iEntityMatcher, PARAMETERCALLABLE_DETACHCHILD);
    }

    public boolean detachChildren(IEntity.IEntityMatcher iEntityMatcher) {
        if (this.mChildren == null) {
            return false;
        }
        return this.mChildren.removeAll(iEntityMatcher, PARAMETERCALLABLE_DETACHCHILD);
    }

    public void callOnChildren(IEntity.IEntityCallable iEntityCallable) {
        if (this.mChildren != null) {
            this.mChildren.call(iEntityCallable);
        }
    }

    public void callOnChildren(IEntity.IEntityMatcher iEntityMatcher, IEntity.IEntityCallable iEntityCallable) {
        if (this.mChildren != null) {
            this.mChildren.call(iEntityMatcher, iEntityCallable);
        }
    }

    public void registerUpdateHandler(IUpdateHandler iUpdateHandler) {
        if (this.mUpdateHandlers == null) {
            allocateUpdateHandlers();
        }
        this.mUpdateHandlers.add(iUpdateHandler);
    }

    public boolean unregisterUpdateHandler(IUpdateHandler iUpdateHandler) {
        if (this.mUpdateHandlers == null) {
            return false;
        }
        return this.mUpdateHandlers.remove(iUpdateHandler);
    }

    public boolean unregisterUpdateHandlers(IUpdateHandler.IUpdateHandlerMatcher iUpdateHandlerMatcher) {
        if (this.mUpdateHandlers == null) {
            return false;
        }
        return this.mUpdateHandlers.removeAll(iUpdateHandlerMatcher);
    }

    public void clearUpdateHandlers() {
        if (this.mUpdateHandlers != null) {
            this.mUpdateHandlers.clear();
        }
    }

    public void registerEntityModifier(IEntityModifier iEntityModifier) {
        if (this.mEntityModifiers == null) {
            allocateEntityModifiers();
        }
        this.mEntityModifiers.add(iEntityModifier);
    }

    public boolean unregisterEntityModifier(IEntityModifier iEntityModifier) {
        if (this.mEntityModifiers == null) {
            return false;
        }
        return this.mEntityModifiers.remove(iEntityModifier);
    }

    public boolean unregisterEntityModifiers(IEntityModifier.IEntityModifierMatcher iEntityModifierMatcher) {
        if (this.mEntityModifiers == null) {
            return false;
        }
        return this.mEntityModifiers.removeAll(iEntityModifierMatcher);
    }

    public void clearEntityModifiers() {
        if (this.mEntityModifiers != null) {
            this.mEntityModifiers.clear();
        }
    }

    public float[] getSceneCenterCoordinates() {
        return convertLocalToSceneCoordinates(0.0f, 0.0f);
    }

    public Transformation getLocalToParentTransformation() {
        Transformation transformation = this.mLocalToParentTransformation;
        if (this.mLocalToParentTransformationDirty) {
            transformation.setToIdentity();
            float f = this.mScaleX;
            float f2 = this.mScaleY;
            if (!(f == 1.0f && f2 == 1.0f)) {
                float f3 = this.mScaleCenterX;
                float f4 = this.mScaleCenterY;
                transformation.postTranslate(-f3, -f4);
                transformation.postScale(f, f2);
                transformation.postTranslate(f3, f4);
            }
            float f5 = this.mRotation;
            if (f5 != 0.0f) {
                float f6 = this.mRotationCenterX;
                float f7 = this.mRotationCenterY;
                transformation.postTranslate(-f6, -f7);
                transformation.postRotate(f5);
                transformation.postTranslate(f6, f7);
            }
            transformation.postTranslate(this.mX, this.mY);
            this.mLocalToParentTransformationDirty = false;
        }
        return transformation;
    }

    public Transformation getParentToLocalTransformation() {
        Transformation transformation = this.mParentToLocalTransformation;
        if (this.mParentToLocalTransformationDirty) {
            transformation.setToIdentity();
            transformation.postTranslate(-this.mX, -this.mY);
            float f = this.mRotation;
            if (f != 0.0f) {
                float f2 = this.mRotationCenterX;
                float f3 = this.mRotationCenterY;
                transformation.postTranslate(-f2, -f3);
                transformation.postRotate(-f);
                transformation.postTranslate(f2, f3);
            }
            float f4 = this.mScaleX;
            float f5 = this.mScaleY;
            if (!(f4 == 1.0f && f5 == 1.0f)) {
                float f6 = this.mScaleCenterX;
                float f7 = this.mScaleCenterY;
                transformation.postTranslate(-f6, -f7);
                transformation.postScale(1.0f / f4, 1.0f / f5);
                transformation.postTranslate(f6, f7);
            }
            this.mParentToLocalTransformationDirty = false;
        }
        return transformation;
    }

    public Transformation getLocalToSceneTransformation() {
        Transformation transformation = this.mLocalToSceneTransformation;
        transformation.setTo(getLocalToParentTransformation());
        IEntity iEntity = this.mParent;
        if (iEntity != null) {
            transformation.postConcat(iEntity.getLocalToSceneTransformation());
        }
        return transformation;
    }

    public Transformation getSceneToLocalTransformation() {
        Transformation transformation = this.mSceneToLocalTransformation;
        transformation.setTo(getParentToLocalTransformation());
        IEntity iEntity = this.mParent;
        if (iEntity != null) {
            transformation.postConcat(iEntity.getSceneToLocalTransformation());
        }
        return transformation;
    }

    public float[] convertLocalToSceneCoordinates(float f, float f2) {
        return convertLocalToSceneCoordinates(f, f2, VERTICES_LOCAL_TO_SCENE_TMP);
    }

    public float[] convertLocalToSceneCoordinates(float f, float f2, float[] fArr) {
        fArr[0] = f;
        fArr[1] = f2;
        getLocalToSceneTransformation().transform(fArr);
        return fArr;
    }

    public float[] convertLocalToSceneCoordinates(float[] fArr) {
        return convertSceneToLocalCoordinates(fArr, VERTICES_LOCAL_TO_SCENE_TMP);
    }

    public float[] convertLocalToSceneCoordinates(float[] fArr, float[] fArr2) {
        fArr2[0] = fArr[0];
        fArr2[1] = fArr[1];
        getLocalToSceneTransformation().transform(fArr2);
        return fArr2;
    }

    public float[] convertSceneToLocalCoordinates(float f, float f2) {
        return convertSceneToLocalCoordinates(f, f2, VERTICES_SCENE_TO_LOCAL_TMP);
    }

    public float[] convertSceneToLocalCoordinates(float f, float f2, float[] fArr) {
        fArr[0] = f;
        fArr[1] = f2;
        getSceneToLocalTransformation().transform(fArr);
        return fArr;
    }

    public float[] convertSceneToLocalCoordinates(float[] fArr) {
        return convertSceneToLocalCoordinates(fArr, VERTICES_SCENE_TO_LOCAL_TMP);
    }

    public float[] convertSceneToLocalCoordinates(float[] fArr, float[] fArr2) {
        fArr2[0] = fArr[0];
        fArr2[1] = fArr[1];
        getSceneToLocalTransformation().transform(fArr2);
        return fArr2;
    }

    public void onAttached() {
    }

    public void onDetached() {
    }

    public Object getUserData() {
        return this.mUserData;
    }

    public void setUserData(Object obj) {
        this.mUserData = obj;
    }

    public final void onDraw(GL10 gl10, Camera camera) {
        if (this.mVisible) {
            onManagedDraw(gl10, camera);
        }
    }

    public final void onUpdate(float f) {
        if (!this.mIgnoreUpdate) {
            onManagedUpdate(f);
        }
    }

    public void reset() {
        this.mVisible = true;
        this.mIgnoreUpdate = false;
        this.mChildrenVisible = true;
        this.mChildrenIgnoreUpdate = false;
        this.mX = this.mInitialX;
        this.mY = this.mInitialY;
        this.mRotation = 0.0f;
        this.mScaleX = 1.0f;
        this.mScaleY = 1.0f;
        this.mRed = 1.0f;
        this.mGreen = 1.0f;
        this.mBlue = 1.0f;
        this.mAlpha = 1.0f;
        if (this.mEntityModifiers != null) {
            this.mEntityModifiers.reset();
        }
        if (this.mChildren != null) {
            SmartList smartList = this.mChildren;
            for (int size = smartList.size() - 1; size >= 0; size--) {
                ((IEntity) smartList.get(size)).reset();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void doDraw(GL10 gl10, Camera camera) {
    }

    private void allocateEntityModifiers() {
        this.mEntityModifiers = new EntityModifierList(this, 4);
    }

    private void allocateChildren() {
        this.mChildren = new SmartList(4);
    }

    private void allocateUpdateHandlers() {
        this.mUpdateHandlers = new UpdateHandlerList(4);
    }

    /* access modifiers changed from: protected */
    public void onApplyTransformations(GL10 gl10) {
        applyTranslation(gl10);
        applyRotation(gl10);
        applyScale(gl10);
    }

    /* access modifiers changed from: protected */
    public void applyTranslation(GL10 gl10) {
        gl10.glTranslatef(this.mX, this.mY, 0.0f);
    }

    /* access modifiers changed from: protected */
    public void applyRotation(GL10 gl10) {
        float f = this.mRotation;
        if (f != 0.0f) {
            float f2 = this.mRotationCenterX;
            float f3 = this.mRotationCenterY;
            gl10.glTranslatef(f2, f3, 0.0f);
            gl10.glRotatef(f, 0.0f, 0.0f, 1.0f);
            gl10.glTranslatef(-f2, -f3, 0.0f);
        }
    }

    /* access modifiers changed from: protected */
    public void applyScale(GL10 gl10) {
        float f = this.mScaleX;
        float f2 = this.mScaleY;
        if (f != 1.0f || f2 != 1.0f) {
            float f3 = this.mScaleCenterX;
            float f4 = this.mScaleCenterY;
            gl10.glTranslatef(f3, f4, 0.0f);
            gl10.glScalef(f, f2, 1.0f);
            gl10.glTranslatef(-f3, -f4, 0.0f);
        }
    }

    /* access modifiers changed from: protected */
    public void onManagedDraw(GL10 gl10, Camera camera) {
        gl10.glPushMatrix();
        onApplyTransformations(gl10);
        doDraw(gl10, camera);
        onDrawChildren(gl10, camera);
        gl10.glPopMatrix();
    }

    /* access modifiers changed from: protected */
    public void onDrawChildren(GL10 gl10, Camera camera) {
        if (this.mChildren != null && this.mChildrenVisible) {
            onManagedDrawChildren(gl10, camera);
        }
    }

    public void onManagedDrawChildren(GL10 gl10, Camera camera) {
        SmartList smartList = this.mChildren;
        int size = smartList.size();
        for (int i = 0; i < size; i++) {
            ((IEntity) smartList.get(i)).onDraw(gl10, camera);
        }
    }

    /* access modifiers changed from: protected */
    public void onManagedUpdate(float f) {
        if (this.mEntityModifiers != null) {
            this.mEntityModifiers.onUpdate(f);
        }
        if (this.mUpdateHandlers != null) {
            this.mUpdateHandlers.onUpdate(f);
        }
        if (this.mChildren != null && !this.mChildrenIgnoreUpdate) {
            SmartList smartList = this.mChildren;
            int size = smartList.size();
            for (int i = 0; i < size; i++) {
                ((IEntity) smartList.get(i)).onUpdate(f);
            }
        }
    }
}
