package org.anddev.andengine.entity.particle.modifier;

import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.entity.particle.Particle;

public class OffCameraExpireModifier implements IParticleModifier {
    private final Camera mCamera;

    public OffCameraExpireModifier(Camera camera) {
        this.mCamera = camera;
    }

    public Camera getCamera() {
        return this.mCamera;
    }

    public void onInitializeParticle(Particle particle) {
    }

    public void onUpdateParticle(Particle particle) {
        if (!this.mCamera.isRectangularShapeVisible(particle)) {
            particle.setDead(true);
        }
    }
}
