package org.anddev.andengine.entity.shape;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.entity.Entity;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.opengl.util.GLHelper;
import org.anddev.andengine.opengl.vertex.VertexBuffer;

public abstract class Shape extends Entity implements IShape {
    public static final int BLENDFUNCTION_DESTINATION_DEFAULT = 771;
    public static final int BLENDFUNCTION_DESTINATION_PREMULTIPLYALPHA_DEFAULT = 771;
    public static final int BLENDFUNCTION_SOURCE_DEFAULT = 770;
    public static final int BLENDFUNCTION_SOURCE_PREMULTIPLYALPHA_DEFAULT = 1;
    private boolean mCullingEnabled = false;
    protected int mDestinationBlendFunction = 771;
    protected int mSourceBlendFunction = BLENDFUNCTION_SOURCE_DEFAULT;

    /* access modifiers changed from: protected */
    public abstract void drawVertices(GL10 gl10, Camera camera);

    /* access modifiers changed from: protected */
    public abstract VertexBuffer getVertexBuffer();

    /* access modifiers changed from: protected */
    public abstract boolean isCulled(Camera camera);

    /* access modifiers changed from: protected */
    public abstract void onUpdateVertexBuffer();

    public Shape(float f, float f2) {
        super(f, f2);
    }

    public void setBlendFunction(int i, int i2) {
        this.mSourceBlendFunction = i;
        this.mDestinationBlendFunction = i2;
    }

    public boolean isCullingEnabled() {
        return this.mCullingEnabled;
    }

    public void setCullingEnabled(boolean z) {
        this.mCullingEnabled = z;
    }

    public float getWidthScaled() {
        return getWidth() * this.mScaleX;
    }

    public float getHeightScaled() {
        return getHeight() * this.mScaleY;
    }

    public boolean isVertexBufferManaged() {
        return getVertexBuffer().isManaged();
    }

    public void setVertexBufferManaged(boolean z) {
        getVertexBuffer().setManaged(z);
    }

    /* access modifiers changed from: protected */
    public void doDraw(GL10 gl10, Camera camera) {
        onInitDraw(gl10);
        onApplyVertices(gl10);
        drawVertices(gl10, camera);
    }

    public boolean onAreaTouched(TouchEvent touchEvent, float f, float f2) {
        return false;
    }

    /* access modifiers changed from: protected */
    public void onManagedDraw(GL10 gl10, Camera camera) {
        if (!this.mCullingEnabled || !isCulled(camera)) {
            super.onManagedDraw(gl10, camera);
        }
    }

    public void reset() {
        super.reset();
        this.mSourceBlendFunction = BLENDFUNCTION_SOURCE_DEFAULT;
        this.mDestinationBlendFunction = 771;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        super.finalize();
        VertexBuffer vertexBuffer = getVertexBuffer();
        if (vertexBuffer.isManaged()) {
            vertexBuffer.unloadFromActiveBufferObjectManager();
        }
    }

    /* access modifiers changed from: protected */
    public void onInitDraw(GL10 gl10) {
        GLHelper.setColor(gl10, this.mRed, this.mGreen, this.mBlue, this.mAlpha);
        GLHelper.enableVertexArray(gl10);
        GLHelper.blendFunction(gl10, this.mSourceBlendFunction, this.mDestinationBlendFunction);
    }

    /* access modifiers changed from: protected */
    public void onApplyVertices(GL10 gl10) {
        if (GLHelper.EXTENSIONS_VERTEXBUFFEROBJECTS) {
            GL11 gl11 = (GL11) gl10;
            getVertexBuffer().selectOnHardware(gl11);
            GLHelper.vertexZeroPointer(gl11);
            return;
        }
        GLHelper.vertexPointer(gl10, getVertexBuffer().getFloatBuffer());
    }

    /* access modifiers changed from: protected */
    public void updateVertexBuffer() {
        onUpdateVertexBuffer();
    }
}
