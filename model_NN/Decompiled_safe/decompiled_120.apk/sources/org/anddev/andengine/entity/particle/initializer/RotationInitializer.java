package org.anddev.andengine.entity.particle.initializer;

import org.anddev.andengine.entity.particle.Particle;

public class RotationInitializer extends BaseSingleValueInitializer {
    public RotationInitializer(float f) {
        this(f, f);
    }

    public RotationInitializer(float f, float f2) {
        super(f, f2);
    }

    public float getMinRotation() {
        return this.mMinValue;
    }

    public float getMaxRotation() {
        return this.mMaxValue;
    }

    public void setRotation(float f) {
        this.mMinValue = f;
        this.mMaxValue = f;
    }

    public void setRotation(float f, float f2) {
        this.mMinValue = f;
        this.mMaxValue = f2;
    }

    public void onInitializeParticle(Particle particle, float f) {
        particle.setRotation(f);
    }
}
