package org.anddev.andengine.entity.modifier;

import org.anddev.andengine.entity.IEntity;
import org.anddev.andengine.entity.modifier.IEntityModifier;

public class RotationByModifier extends SingleValueChangeEntityModifier {
    public RotationByModifier(float f, float f2) {
        super(f, f2);
    }

    public RotationByModifier(float f, float f2, IEntityModifier.IEntityModifierListener iEntityModifierListener) {
        super(f, f2, iEntityModifierListener);
    }

    protected RotationByModifier(RotationByModifier rotationByModifier) {
        super(rotationByModifier);
    }

    public RotationByModifier clone() {
        return new RotationByModifier(this);
    }

    /* access modifiers changed from: protected */
    public void onChangeValue(float f, IEntity iEntity, float f2) {
        iEntity.setRotation(iEntity.getRotation() + f2);
    }
}
