package org.anddev.andengine.entity.modifier;

import org.anddev.andengine.entity.IEntity;
import org.anddev.andengine.entity.modifier.IEntityModifier;
import org.anddev.andengine.util.modifier.ease.IEaseFunction;

public class MoveXModifier extends SingleValueSpanEntityModifier {
    public MoveXModifier(float f, float f2, float f3) {
        this(f, f2, f3, null, IEaseFunction.DEFAULT);
    }

    public MoveXModifier(float f, float f2, float f3, IEaseFunction iEaseFunction) {
        this(f, f2, f3, null, iEaseFunction);
    }

    public MoveXModifier(float f, float f2, float f3, IEntityModifier.IEntityModifierListener iEntityModifierListener) {
        super(f, f2, f3, iEntityModifierListener, IEaseFunction.DEFAULT);
    }

    public MoveXModifier(float f, float f2, float f3, IEntityModifier.IEntityModifierListener iEntityModifierListener, IEaseFunction iEaseFunction) {
        super(f, f2, f3, iEntityModifierListener, iEaseFunction);
    }

    protected MoveXModifier(MoveXModifier moveXModifier) {
        super(moveXModifier);
    }

    public MoveXModifier clone() {
        return new MoveXModifier(this);
    }

    /* access modifiers changed from: protected */
    public void onSetInitialValue(IEntity iEntity, float f) {
        iEntity.setPosition(f, iEntity.getY());
    }

    /* access modifiers changed from: protected */
    public void onSetValue(IEntity iEntity, float f, float f2) {
        iEntity.setPosition(f2, iEntity.getY());
    }
}
