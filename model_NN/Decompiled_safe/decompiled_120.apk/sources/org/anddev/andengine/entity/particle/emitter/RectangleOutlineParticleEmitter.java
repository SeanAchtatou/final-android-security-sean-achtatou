package org.anddev.andengine.entity.particle.emitter;

import org.anddev.andengine.util.MathUtils;

public class RectangleOutlineParticleEmitter extends BaseRectangleParticleEmitter {
    public RectangleOutlineParticleEmitter(float f, float f2, float f3, float f4) {
        super(f, f2, f3, f4);
    }

    public void getPositionOffset(float[] fArr) {
        fArr[0] = this.mCenterX + (((float) MathUtils.randomSign()) * this.mWidthHalf);
        fArr[1] = this.mCenterY + (((float) MathUtils.randomSign()) * this.mHeightHalf);
    }
}
