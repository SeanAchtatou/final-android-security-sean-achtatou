package org.anddev.andengine.entity.layer.tiled.tmx;

import android.content.Context;
import org.anddev.andengine.entity.layer.tiled.tmx.util.constants.TMXConstants;
import org.anddev.andengine.entity.layer.tiled.tmx.util.exception.TMXParseException;
import org.anddev.andengine.opengl.texture.TextureManager;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.util.SAXUtils;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

public class TSXParser extends DefaultHandler implements TMXConstants {
    private final Context mContext;
    private final int mFirstGlobalTileID;
    private boolean mInImage;
    private boolean mInProperties;
    private boolean mInProperty;
    private boolean mInTile;
    private boolean mInTileset;
    private int mLastTileSetTileID;
    private TMXTileSet mTMXTileSet;
    private final TextureManager mTextureManager;
    private final TextureOptions mTextureOptions;

    public TSXParser(Context context, TextureManager textureManager, TextureOptions textureOptions, int i) {
        this.mContext = context;
        this.mTextureManager = textureManager;
        this.mTextureOptions = textureOptions;
        this.mFirstGlobalTileID = i;
    }

    /* access modifiers changed from: package-private */
    public TMXTileSet getTMXTileSet() {
        return this.mTMXTileSet;
    }

    public void startElement(String str, String str2, String str3, Attributes attributes) {
        if (str2.equals(TMXConstants.TAG_TILESET)) {
            this.mInTileset = true;
            this.mTMXTileSet = new TMXTileSet(this.mFirstGlobalTileID, attributes, this.mTextureOptions);
        } else if (str2.equals(TMXConstants.TAG_IMAGE)) {
            this.mInImage = true;
            this.mTMXTileSet.setImageSource(this.mContext, this.mTextureManager, attributes);
        } else if (str2.equals(TMXConstants.TAG_TILE)) {
            this.mInTile = true;
            this.mLastTileSetTileID = SAXUtils.getIntAttributeOrThrow(attributes, TMXConstants.TAG_TILE_ATTRIBUTE_ID);
        } else if (str2.equals(TMXConstants.TAG_PROPERTIES)) {
            this.mInProperties = true;
        } else if (str2.equals(TMXConstants.TAG_PROPERTY)) {
            this.mInProperty = true;
            this.mTMXTileSet.addTMXTileProperty(this.mLastTileSetTileID, new TMXTileProperty(attributes));
        } else {
            throw new TMXParseException("Unexpected start tag: '" + str2 + "'.");
        }
    }

    public void endElement(String str, String str2, String str3) {
        if (str2.equals(TMXConstants.TAG_TILESET)) {
            this.mInTileset = false;
        } else if (str2.equals(TMXConstants.TAG_IMAGE)) {
            this.mInImage = false;
        } else if (str2.equals(TMXConstants.TAG_TILE)) {
            this.mInTile = false;
        } else if (str2.equals(TMXConstants.TAG_PROPERTIES)) {
            this.mInProperties = false;
        } else if (str2.equals(TMXConstants.TAG_PROPERTY)) {
            this.mInProperty = false;
        } else {
            throw new TMXParseException("Unexpected end tag: '" + str2 + "'.");
        }
    }
}
