package org.anddev.andengine.entity.modifier;

import org.anddev.andengine.entity.modifier.IEntityModifier;
import org.anddev.andengine.util.modifier.BaseSingleValueChangeModifier;

public abstract class SingleValueChangeEntityModifier extends BaseSingleValueChangeModifier implements IEntityModifier {
    public SingleValueChangeEntityModifier(float f, float f2) {
        super(f, f2);
    }

    public SingleValueChangeEntityModifier(float f, float f2, IEntityModifier.IEntityModifierListener iEntityModifierListener) {
        super(f, f2, iEntityModifierListener);
    }

    protected SingleValueChangeEntityModifier(SingleValueChangeEntityModifier singleValueChangeEntityModifier) {
        super(singleValueChangeEntityModifier);
    }
}
