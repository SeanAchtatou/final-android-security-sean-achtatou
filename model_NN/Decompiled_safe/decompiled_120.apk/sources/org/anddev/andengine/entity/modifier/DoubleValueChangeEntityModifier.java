package org.anddev.andengine.entity.modifier;

import org.anddev.andengine.entity.modifier.IEntityModifier;
import org.anddev.andengine.util.modifier.BaseDoubleValueChangeModifier;

public abstract class DoubleValueChangeEntityModifier extends BaseDoubleValueChangeModifier implements IEntityModifier {
    public DoubleValueChangeEntityModifier(float f, float f2, float f3) {
        super(f, f2, f3);
    }

    public DoubleValueChangeEntityModifier(float f, float f2, float f3, IEntityModifier.IEntityModifierListener iEntityModifierListener) {
        super(f, f2, f3, iEntityModifierListener);
    }

    public DoubleValueChangeEntityModifier(DoubleValueChangeEntityModifier doubleValueChangeEntityModifier) {
        super(doubleValueChangeEntityModifier);
    }
}
