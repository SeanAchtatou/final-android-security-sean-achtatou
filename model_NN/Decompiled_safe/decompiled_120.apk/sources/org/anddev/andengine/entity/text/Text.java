package org.anddev.andengine.entity.text;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.entity.shape.RectangularShape;
import org.anddev.andengine.opengl.font.Font;
import org.anddev.andengine.opengl.texture.buffer.TextTextureBuffer;
import org.anddev.andengine.opengl.util.GLHelper;
import org.anddev.andengine.opengl.vertex.TextVertexBuffer;
import org.anddev.andengine.util.HorizontalAlign;
import org.anddev.andengine.util.StringUtils;

public class Text extends RectangularShape {
    protected final int mCharactersMaximum;
    private final Font mFont;
    private String[] mLines;
    private int mMaximumLineWidth;
    private String mText;
    private final TextTextureBuffer mTextTextureBuffer;
    protected final int mVertexCount;
    private int[] mWidths;

    public Text(float f, float f2, Font font, String str) {
        this(f, f2, font, str, HorizontalAlign.LEFT);
    }

    public Text(float f, float f2, Font font, String str, HorizontalAlign horizontalAlign) {
        this(f, f2, font, str, horizontalAlign, str.length() - StringUtils.countOccurrences(str, 10));
    }

    protected Text(float f, float f2, Font font, String str, HorizontalAlign horizontalAlign, int i) {
        super(f, f2, 0.0f, 0.0f, new TextVertexBuffer(i, horizontalAlign, 35044, true));
        this.mCharactersMaximum = i;
        this.mVertexCount = this.mCharactersMaximum * 6;
        this.mTextTextureBuffer = new TextTextureBuffer(this.mVertexCount * 2, 35044, true);
        this.mFont = font;
        updateText(str);
        initBlendFunction();
    }

    /* access modifiers changed from: protected */
    public void updateText(String str) {
        int i = 0;
        this.mText = str;
        Font font = this.mFont;
        this.mLines = StringUtils.split(this.mText, 10, this.mLines);
        String[] strArr = this.mLines;
        int length = strArr.length;
        if (!(this.mWidths != null && this.mWidths.length == length)) {
            this.mWidths = new int[length];
        }
        int[] iArr = this.mWidths;
        for (int i2 = length - 1; i2 >= 0; i2--) {
            iArr[i2] = font.getStringWidth(strArr[i2]);
            i = Math.max(i, iArr[i2]);
        }
        this.mMaximumLineWidth = i;
        this.mWidth = (float) this.mMaximumLineWidth;
        float f = this.mWidth;
        this.mBaseWidth = f;
        this.mHeight = (float) (((length - 1) * font.getLineGap()) + (font.getLineHeight() * length));
        float f2 = this.mHeight;
        this.mBaseHeight = f2;
        this.mRotationCenterX = f * 0.5f;
        this.mRotationCenterY = f2 * 0.5f;
        this.mScaleCenterX = this.mRotationCenterX;
        this.mScaleCenterY = this.mRotationCenterY;
        this.mTextTextureBuffer.update(font, strArr);
        updateVertexBuffer();
    }

    public String getText() {
        return this.mText;
    }

    public int getCharactersMaximum() {
        return this.mCharactersMaximum;
    }

    public TextVertexBuffer getVertexBuffer() {
        return (TextVertexBuffer) this.mVertexBuffer;
    }

    /* access modifiers changed from: protected */
    public void onInitDraw(GL10 gl10) {
        super.onInitDraw(gl10);
        GLHelper.enableTextures(gl10);
        GLHelper.enableTexCoordArray(gl10);
    }

    /* access modifiers changed from: protected */
    public void drawVertices(GL10 gl10, Camera camera) {
        gl10.glDrawArrays(4, 0, this.mVertexCount);
    }

    /* access modifiers changed from: protected */
    public void onUpdateVertexBuffer() {
        Font font = this.mFont;
        if (font != null) {
            getVertexBuffer().update(font, this.mMaximumLineWidth, this.mWidths, this.mLines);
        }
    }

    /* access modifiers changed from: protected */
    public void onApplyTransformations(GL10 gl10) {
        super.onApplyTransformations(gl10);
        applyTexture(gl10);
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        super.finalize();
        if (this.mTextTextureBuffer.isManaged()) {
            this.mTextTextureBuffer.unloadFromActiveBufferObjectManager();
        }
    }

    private void initBlendFunction() {
        if (this.mFont.getTexture().getTextureOptions().mPreMultipyAlpha) {
            setBlendFunction(1, 771);
        }
    }

    private void applyTexture(GL10 gl10) {
        if (GLHelper.EXTENSIONS_VERTEXBUFFEROBJECTS) {
            GL11 gl11 = (GL11) gl10;
            this.mTextTextureBuffer.selectOnHardware(gl11);
            this.mFont.getTexture().bind(gl10);
            GLHelper.texCoordZeroPointer(gl11);
            return;
        }
        this.mFont.getTexture().bind(gl10);
        GLHelper.texCoordPointer(gl10, this.mTextTextureBuffer.getFloatBuffer());
    }
}
