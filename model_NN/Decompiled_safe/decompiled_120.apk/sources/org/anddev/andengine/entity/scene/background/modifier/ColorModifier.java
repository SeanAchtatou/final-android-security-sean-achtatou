package org.anddev.andengine.entity.scene.background.modifier;

import org.anddev.andengine.entity.scene.background.IBackground;
import org.anddev.andengine.entity.scene.background.modifier.IBackgroundModifier;
import org.anddev.andengine.util.modifier.BaseTripleValueSpanModifier;
import org.anddev.andengine.util.modifier.ease.IEaseFunction;

public class ColorModifier extends BaseTripleValueSpanModifier implements IBackgroundModifier {
    public ColorModifier(float f, float f2, float f3, float f4, float f5, float f6, float f7) {
        this(f, f2, f3, f4, f5, f6, f7, null, IEaseFunction.DEFAULT);
    }

    public ColorModifier(float f, float f2, float f3, float f4, float f5, float f6, float f7, IEaseFunction iEaseFunction) {
        this(f, f2, f3, f4, f5, f6, f7, null, iEaseFunction);
    }

    public ColorModifier(float f, float f2, float f3, float f4, float f5, float f6, float f7, IBackgroundModifier.IBackgroundModifierListener iBackgroundModifierListener) {
        super(f, f2, f3, f4, f5, f6, f7, iBackgroundModifierListener, IEaseFunction.DEFAULT);
    }

    public ColorModifier(float f, float f2, float f3, float f4, float f5, float f6, float f7, IBackgroundModifier.IBackgroundModifierListener iBackgroundModifierListener, IEaseFunction iEaseFunction) {
        super(f, f2, f3, f4, f5, f6, f7, iBackgroundModifierListener, iEaseFunction);
    }

    protected ColorModifier(ColorModifier colorModifier) {
        super(colorModifier);
    }

    public ColorModifier clone() {
        return new ColorModifier(this);
    }

    /* access modifiers changed from: protected */
    public void onSetInitialValues(IBackground iBackground, float f, float f2, float f3) {
        iBackground.setColor(f, f2, f3);
    }

    /* access modifiers changed from: protected */
    public void onSetValues(IBackground iBackground, float f, float f2, float f3, float f4) {
        iBackground.setColor(f2, f3, f4);
    }
}
