package org.anddev.andengine.entity.modifier;

import org.anddev.andengine.entity.modifier.IEntityModifier;
import org.anddev.andengine.util.modifier.BaseTripleValueSpanModifier;
import org.anddev.andengine.util.modifier.ease.IEaseFunction;

public abstract class TripleValueSpanEntityModifier extends BaseTripleValueSpanModifier implements IEntityModifier {
    public TripleValueSpanEntityModifier(float f, float f2, float f3, float f4, float f5, float f6, float f7, IEaseFunction iEaseFunction) {
        super(f, f2, f3, f4, f5, f6, f7, iEaseFunction);
    }

    public TripleValueSpanEntityModifier(float f, float f2, float f3, float f4, float f5, float f6, float f7, IEntityModifier.IEntityModifierListener iEntityModifierListener, IEaseFunction iEaseFunction) {
        super(f, f2, f3, f4, f5, f6, f7, iEntityModifierListener, iEaseFunction);
    }

    protected TripleValueSpanEntityModifier(TripleValueSpanEntityModifier tripleValueSpanEntityModifier) {
        super(tripleValueSpanEntityModifier);
    }
}
