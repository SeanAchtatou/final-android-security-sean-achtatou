package org.anddev.andengine.entity.particle.emitter;

public class PointParticleEmitter extends BaseParticleEmitter {
    public PointParticleEmitter(float f, float f2) {
        super(f, f2);
    }

    public void getPositionOffset(float[] fArr) {
        fArr[0] = this.mCenterX;
        fArr[1] = this.mCenterY;
    }
}
