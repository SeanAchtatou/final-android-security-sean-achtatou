package org.anddev.andengine.entity.particle;

import android.util.FloatMath;
import java.util.ArrayList;
import javax.microedition.khronos.opengles.GL10;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.entity.Entity;
import org.anddev.andengine.entity.particle.emitter.IParticleEmitter;
import org.anddev.andengine.entity.particle.emitter.RectangleParticleEmitter;
import org.anddev.andengine.entity.particle.initializer.IParticleInitializer;
import org.anddev.andengine.entity.particle.modifier.IParticleModifier;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.vertex.RectangleVertexBuffer;
import org.anddev.andengine.util.MathUtils;

public class ParticleSystem extends Entity {
    private static final int BLENDFUNCTION_DESTINATION_DEFAULT = 771;
    private static final int BLENDFUNCTION_SOURCE_DEFAULT = 1;
    private final float[] POSITION_OFFSET;
    private int mDestinationBlendFunction;
    private final IParticleEmitter mParticleEmitter;
    private int mParticleInitializerCount;
    private final ArrayList mParticleInitializers;
    private int mParticleModifierCount;
    private final ArrayList mParticleModifiers;
    private final Particle[] mParticles;
    private int mParticlesAlive;
    private float mParticlesDueToSpawn;
    private final int mParticlesMaximum;
    private boolean mParticlesSpawnEnabled;
    private final float mRateMaximum;
    private final float mRateMinimum;
    private RectangleVertexBuffer mSharedParticleVertexBuffer;
    private int mSourceBlendFunction;
    private final TextureRegion mTextureRegion;

    public ParticleSystem(float f, float f2, float f3, float f4, float f5, float f6, int i, TextureRegion textureRegion) {
        this(new RectangleParticleEmitter((f3 * 0.5f) + f, (0.5f * f4) + f2, f3, f4), f5, f6, i, textureRegion);
    }

    public ParticleSystem(IParticleEmitter iParticleEmitter, float f, float f2, int i, TextureRegion textureRegion) {
        super(0.0f, 0.0f);
        this.POSITION_OFFSET = new float[2];
        this.mSourceBlendFunction = 1;
        this.mDestinationBlendFunction = 771;
        this.mParticleInitializers = new ArrayList();
        this.mParticleModifiers = new ArrayList();
        this.mParticlesSpawnEnabled = true;
        this.mParticleEmitter = iParticleEmitter;
        this.mParticles = new Particle[i];
        this.mRateMinimum = f;
        this.mRateMaximum = f2;
        this.mParticlesMaximum = i;
        this.mTextureRegion = textureRegion;
        registerUpdateHandler(this.mParticleEmitter);
    }

    public boolean isParticlesSpawnEnabled() {
        return this.mParticlesSpawnEnabled;
    }

    public void setParticlesSpawnEnabled(boolean z) {
        this.mParticlesSpawnEnabled = z;
    }

    public void setBlendFunction(int i, int i2) {
        this.mSourceBlendFunction = i;
        this.mDestinationBlendFunction = i2;
    }

    public IParticleEmitter getParticleEmitter() {
        return this.mParticleEmitter;
    }

    public void reset() {
        super.reset();
        this.mParticlesDueToSpawn = 0.0f;
        this.mParticlesAlive = 0;
    }

    /* access modifiers changed from: protected */
    public void onManagedDraw(GL10 gl10, Camera camera) {
        Particle[] particleArr = this.mParticles;
        for (int i = this.mParticlesAlive - 1; i >= 0; i--) {
            particleArr[i].onDraw(gl10, camera);
        }
    }

    /* access modifiers changed from: protected */
    public void onManagedUpdate(float f) {
        super.onManagedUpdate(f);
        if (this.mParticlesSpawnEnabled) {
            spawnParticles(f);
        }
        Particle[] particleArr = this.mParticles;
        ArrayList arrayList = this.mParticleModifiers;
        int i = this.mParticleModifierCount - 1;
        for (int i2 = this.mParticlesAlive - 1; i2 >= 0; i2--) {
            Particle particle = particleArr[i2];
            for (int i3 = i; i3 >= 0; i3--) {
                ((IParticleModifier) arrayList.get(i3)).onUpdateParticle(particle);
            }
            particle.onUpdate(f);
            if (particle.mDead) {
                this.mParticlesAlive--;
                int i4 = this.mParticlesAlive;
                particleArr[i2] = particleArr[i4];
                particleArr[i4] = particle;
            }
        }
    }

    public void addParticleModifier(IParticleModifier iParticleModifier) {
        this.mParticleModifiers.add(iParticleModifier);
        this.mParticleModifierCount++;
    }

    public void removeParticleModifier(IParticleModifier iParticleModifier) {
        this.mParticleModifierCount--;
        this.mParticleModifiers.remove(iParticleModifier);
    }

    public void addParticleInitializer(IParticleInitializer iParticleInitializer) {
        this.mParticleInitializers.add(iParticleInitializer);
        this.mParticleInitializerCount++;
    }

    public void removeParticleInitializer(IParticleInitializer iParticleInitializer) {
        this.mParticleInitializerCount--;
        this.mParticleInitializers.remove(iParticleInitializer);
    }

    private void spawnParticles(float f) {
        this.mParticlesDueToSpawn = (determineCurrentRate() * f) + this.mParticlesDueToSpawn;
        int min = Math.min(this.mParticlesMaximum - this.mParticlesAlive, (int) FloatMath.floor(this.mParticlesDueToSpawn));
        this.mParticlesDueToSpawn -= (float) min;
        for (int i = 0; i < min; i++) {
            spawnParticle();
        }
    }

    private void spawnParticle() {
        Particle particle;
        Particle particle2;
        Particle[] particleArr = this.mParticles;
        int i = this.mParticlesAlive;
        if (i < this.mParticlesMaximum) {
            Particle particle3 = particleArr[i];
            this.mParticleEmitter.getPositionOffset(this.POSITION_OFFSET);
            float f = this.POSITION_OFFSET[0];
            float f2 = this.POSITION_OFFSET[1];
            if (particle3 != null) {
                particle3.reset();
                particle3.setPosition(f, f2);
                particle2 = particle3;
            } else {
                if (i == 0) {
                    particle = new Particle(f, f2, this.mTextureRegion);
                    this.mSharedParticleVertexBuffer = particle.getVertexBuffer();
                } else {
                    particle = new Particle(f, f2, this.mTextureRegion, this.mSharedParticleVertexBuffer);
                }
                particleArr[i] = particle;
                particle2 = particle;
            }
            particle2.setBlendFunction(this.mSourceBlendFunction, this.mDestinationBlendFunction);
            ArrayList arrayList = this.mParticleInitializers;
            for (int i2 = this.mParticleInitializerCount - 1; i2 >= 0; i2--) {
                ((IParticleInitializer) arrayList.get(i2)).onInitializeParticle(particle2);
            }
            ArrayList arrayList2 = this.mParticleModifiers;
            for (int i3 = this.mParticleModifierCount - 1; i3 >= 0; i3--) {
                ((IParticleModifier) arrayList2.get(i3)).onInitializeParticle(particle2);
            }
            this.mParticlesAlive++;
        }
    }

    private float determineCurrentRate() {
        if (this.mRateMinimum == this.mRateMaximum) {
            return this.mRateMinimum;
        }
        return (MathUtils.RANDOM.nextFloat() * (this.mRateMaximum - this.mRateMinimum)) + this.mRateMinimum;
    }
}
