package org.anddev.andengine.entity.modifier;

import org.anddev.andengine.entity.IEntity;
import org.anddev.andengine.entity.modifier.IEntityModifier;

public class MoveByModifier extends DoubleValueChangeEntityModifier {
    public MoveByModifier(float f, float f2, float f3) {
        super(f, f2, f3);
    }

    public MoveByModifier(float f, float f2, float f3, IEntityModifier.IEntityModifierListener iEntityModifierListener) {
        super(f, f2, f3, iEntityModifierListener);
    }

    protected MoveByModifier(DoubleValueChangeEntityModifier doubleValueChangeEntityModifier) {
        super(doubleValueChangeEntityModifier);
    }

    public MoveByModifier clone() {
        return new MoveByModifier(this);
    }

    /* access modifiers changed from: protected */
    public void onChangeValues(float f, IEntity iEntity, float f2, float f3) {
        iEntity.setPosition(iEntity.getX() + f2, iEntity.getY() + f3);
    }
}
