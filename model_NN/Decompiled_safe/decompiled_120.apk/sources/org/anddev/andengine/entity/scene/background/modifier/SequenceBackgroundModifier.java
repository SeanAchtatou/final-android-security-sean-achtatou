package org.anddev.andengine.entity.scene.background.modifier;

import org.anddev.andengine.entity.scene.background.modifier.IBackgroundModifier;
import org.anddev.andengine.util.modifier.SequenceModifier;

public class SequenceBackgroundModifier extends SequenceModifier implements IBackgroundModifier {

    public interface ISubSequenceBackgroundModifierListener extends SequenceModifier.ISubSequenceModifierListener {
    }

    public SequenceBackgroundModifier(IBackgroundModifier... iBackgroundModifierArr) {
        super(iBackgroundModifierArr);
    }

    public SequenceBackgroundModifier(ISubSequenceBackgroundModifierListener iSubSequenceBackgroundModifierListener, IBackgroundModifier... iBackgroundModifierArr) {
        super(iSubSequenceBackgroundModifierListener, iBackgroundModifierArr);
    }

    public SequenceBackgroundModifier(IBackgroundModifier.IBackgroundModifierListener iBackgroundModifierListener, IBackgroundModifier... iBackgroundModifierArr) {
        super(iBackgroundModifierListener, iBackgroundModifierArr);
    }

    public SequenceBackgroundModifier(ISubSequenceBackgroundModifierListener iSubSequenceBackgroundModifierListener, IBackgroundModifier.IBackgroundModifierListener iBackgroundModifierListener, IBackgroundModifier... iBackgroundModifierArr) {
        super(iSubSequenceBackgroundModifierListener, iBackgroundModifierListener, iBackgroundModifierArr);
    }

    protected SequenceBackgroundModifier(SequenceBackgroundModifier sequenceBackgroundModifier) {
        super(sequenceBackgroundModifier);
    }

    public SequenceBackgroundModifier clone() {
        return new SequenceBackgroundModifier(this);
    }
}
