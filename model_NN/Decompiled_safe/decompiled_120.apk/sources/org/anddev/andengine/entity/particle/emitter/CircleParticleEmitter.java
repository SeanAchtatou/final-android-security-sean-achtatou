package org.anddev.andengine.entity.particle.emitter;

import android.util.FloatMath;
import org.anddev.andengine.util.MathUtils;

public class CircleParticleEmitter extends BaseCircleParticleEmitter {
    public CircleParticleEmitter(float f, float f2, float f3) {
        super(f, f2, f3);
    }

    public CircleParticleEmitter(float f, float f2, float f3, float f4) {
        super(f, f2, f3, f4);
    }

    public void getPositionOffset(float[] fArr) {
        float nextFloat = MathUtils.RANDOM.nextFloat() * 3.1415927f * 2.0f;
        fArr[0] = this.mCenterX + (FloatMath.cos(nextFloat) * this.mRadiusX * MathUtils.RANDOM.nextFloat());
        fArr[1] = (FloatMath.sin(nextFloat) * this.mRadiusY * MathUtils.RANDOM.nextFloat()) + this.mCenterY;
    }
}
