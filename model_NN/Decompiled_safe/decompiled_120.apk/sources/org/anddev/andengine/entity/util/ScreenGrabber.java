package org.anddev.andengine.entity.util;

import android.graphics.Bitmap;
import java.nio.IntBuffer;
import javax.microedition.khronos.opengles.GL10;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.entity.Entity;

public class ScreenGrabber extends Entity {
    private int mGrabHeight;
    private int mGrabWidth;
    private int mGrabX;
    private int mGrabY;
    private IScreenGrabberCallback mScreenGrabCallback;
    private boolean mScreenGrabPending = false;

    public interface IScreenGrabberCallback {
        void onScreenGrabFailed(Exception exc);

        void onScreenGrabbed(Bitmap bitmap);
    }

    /* access modifiers changed from: protected */
    public void onManagedDraw(GL10 gl10, Camera camera) {
        if (this.mScreenGrabPending) {
            try {
                this.mScreenGrabCallback.onScreenGrabbed(grab(this.mGrabX, this.mGrabY, this.mGrabWidth, this.mGrabHeight, gl10));
            } catch (Exception e) {
                this.mScreenGrabCallback.onScreenGrabFailed(e);
            }
            this.mScreenGrabPending = false;
        }
    }

    /* access modifiers changed from: protected */
    public void onManagedUpdate(float f) {
    }

    public void reset() {
    }

    public void grab(int i, int i2, IScreenGrabberCallback iScreenGrabberCallback) {
        grab(0, 0, i, i2, iScreenGrabberCallback);
    }

    public void grab(int i, int i2, int i3, int i4, IScreenGrabberCallback iScreenGrabberCallback) {
        this.mGrabX = i;
        this.mGrabY = i2;
        this.mGrabWidth = i3;
        this.mGrabHeight = i4;
        this.mScreenGrabCallback = iScreenGrabberCallback;
        this.mScreenGrabPending = true;
    }

    private static Bitmap grab(int i, int i2, int i3, int i4, GL10 gl10) {
        int[] iArr = new int[((i2 + i4) * i3)];
        IntBuffer wrap = IntBuffer.wrap(iArr);
        wrap.position(0);
        gl10.glReadPixels(i, 0, i3, i2 + i4, 6408, 5121, wrap);
        int[] iArr2 = new int[(i3 * i4)];
        for (int i5 = 0; i5 < i4; i5++) {
            for (int i6 = 0; i6 < i3; i6++) {
                int i7 = iArr[((i2 + i5) * i3) + i6];
                iArr2[(((i4 - i5) - 1) * i3) + i6] = (i7 & -16711936) | ((i7 & 255) << 16) | ((16711680 & i7) >> 16);
            }
        }
        return Bitmap.createBitmap(iArr2, i3, i4, Bitmap.Config.ARGB_8888);
    }
}
