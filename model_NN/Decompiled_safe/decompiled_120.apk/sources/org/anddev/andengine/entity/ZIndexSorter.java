package org.anddev.andengine.entity;

import java.util.Comparator;
import java.util.List;
import org.anddev.andengine.util.sort.InsertionSorter;

public class ZIndexSorter extends InsertionSorter {
    private static ZIndexSorter INSTANCE;
    private final Comparator mZIndexComparator = new Comparator() {
        public int compare(IEntity iEntity, IEntity iEntity2) {
            return iEntity.getZIndex() - iEntity2.getZIndex();
        }
    };

    private ZIndexSorter() {
    }

    public static ZIndexSorter getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ZIndexSorter();
        }
        return INSTANCE;
    }

    public void sort(IEntity[] iEntityArr) {
        sort(iEntityArr, this.mZIndexComparator);
    }

    public void sort(IEntity[] iEntityArr, int i, int i2) {
        sort(iEntityArr, i, i2, this.mZIndexComparator);
    }

    public void sort(List list) {
        sort(list, this.mZIndexComparator);
    }

    public void sort(List list, int i, int i2) {
        sort(list, i, i2, this.mZIndexComparator);
    }
}
