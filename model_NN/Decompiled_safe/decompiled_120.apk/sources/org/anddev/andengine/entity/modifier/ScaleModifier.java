package org.anddev.andengine.entity.modifier;

import org.anddev.andengine.entity.IEntity;
import org.anddev.andengine.entity.modifier.IEntityModifier;
import org.anddev.andengine.util.modifier.ease.IEaseFunction;

public class ScaleModifier extends DoubleValueSpanEntityModifier {
    public ScaleModifier(float f, float f2, float f3) {
        this(f, f2, f3, (IEntityModifier.IEntityModifierListener) null, IEaseFunction.DEFAULT);
    }

    public ScaleModifier(float f, float f2, float f3, IEaseFunction iEaseFunction) {
        this(f, f2, f3, (IEntityModifier.IEntityModifierListener) null, iEaseFunction);
    }

    public ScaleModifier(float f, float f2, float f3, IEntityModifier.IEntityModifierListener iEntityModifierListener) {
        this(f, f2, f3, f2, f3, iEntityModifierListener, IEaseFunction.DEFAULT);
    }

    public ScaleModifier(float f, float f2, float f3, IEntityModifier.IEntityModifierListener iEntityModifierListener, IEaseFunction iEaseFunction) {
        this(f, f2, f3, f2, f3, iEntityModifierListener, iEaseFunction);
    }

    public ScaleModifier(float f, float f2, float f3, float f4, float f5) {
        this(f, f2, f3, f4, f5, null, IEaseFunction.DEFAULT);
    }

    public ScaleModifier(float f, float f2, float f3, float f4, float f5, IEaseFunction iEaseFunction) {
        this(f, f2, f3, f4, f5, null, iEaseFunction);
    }

    public ScaleModifier(float f, float f2, float f3, float f4, float f5, IEntityModifier.IEntityModifierListener iEntityModifierListener) {
        super(f, f2, f3, f4, f5, iEntityModifierListener, IEaseFunction.DEFAULT);
    }

    public ScaleModifier(float f, float f2, float f3, float f4, float f5, IEntityModifier.IEntityModifierListener iEntityModifierListener, IEaseFunction iEaseFunction) {
        super(f, f2, f3, f4, f5, iEntityModifierListener, iEaseFunction);
    }

    protected ScaleModifier(ScaleModifier scaleModifier) {
        super(scaleModifier);
    }

    public ScaleModifier clone() {
        return new ScaleModifier(this);
    }

    /* access modifiers changed from: protected */
    public void onSetInitialValues(IEntity iEntity, float f, float f2) {
        iEntity.setScale(f, f2);
    }

    /* access modifiers changed from: protected */
    public void onSetValues(IEntity iEntity, float f, float f2, float f3) {
        iEntity.setScale(f2, f3);
    }
}
