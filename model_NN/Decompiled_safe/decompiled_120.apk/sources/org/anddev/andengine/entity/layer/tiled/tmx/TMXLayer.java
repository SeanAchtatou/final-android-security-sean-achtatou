package org.anddev.andengine.entity.layer.tiled.tmx;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.util.zip.GZIPInputStream;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;
import org.anddev.andengine.collision.RectangularShapeCollisionChecker;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.entity.layer.tiled.tmx.TMXLoader;
import org.anddev.andengine.entity.layer.tiled.tmx.util.constants.TMXConstants;
import org.anddev.andengine.entity.shape.RectangularShape;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.util.GLHelper;
import org.anddev.andengine.util.Base64InputStream;
import org.anddev.andengine.util.MathUtils;
import org.anddev.andengine.util.SAXUtils;
import org.anddev.andengine.util.StreamUtils;
import org.xml.sax.Attributes;

public class TMXLayer extends RectangularShape implements TMXConstants {
    private final float[] mCullingVertices = new float[8];
    private final int mGlobalTileIDsExpected;
    private final String mName;
    private final TMXProperties mTMXLayerProperties = new TMXProperties();
    private final TMXTiledMap mTMXTiledMap;
    private final TMXTile[][] mTMXTiles;
    private final int mTileColumns;
    private final int mTileRows;
    private int mTilesAdded;

    public TMXLayer(TMXTiledMap tMXTiledMap, Attributes attributes) {
        super(0.0f, 0.0f, 0.0f, 0.0f, null);
        this.mTMXTiledMap = tMXTiledMap;
        this.mName = attributes.getValue("", "name");
        this.mTileColumns = SAXUtils.getIntAttributeOrThrow(attributes, "width");
        this.mTileRows = SAXUtils.getIntAttributeOrThrow(attributes, "height");
        this.mTMXTiles = (TMXTile[][]) Array.newInstance(TMXTile.class, this.mTileRows, this.mTileColumns);
        this.mWidth = (float) (tMXTiledMap.getTileWidth() * this.mTileColumns);
        float f = this.mWidth;
        this.mBaseWidth = f;
        this.mHeight = (float) (tMXTiledMap.getTileHeight() * this.mTileRows);
        float f2 = this.mHeight;
        this.mBaseHeight = f2;
        this.mRotationCenterX = f * 0.5f;
        this.mRotationCenterY = f2 * 0.5f;
        this.mScaleCenterX = this.mRotationCenterX;
        this.mScaleCenterY = this.mRotationCenterY;
        this.mGlobalTileIDsExpected = this.mTileColumns * this.mTileRows;
        setVisible(SAXUtils.getIntAttribute(attributes, TMXConstants.TAG_LAYER_ATTRIBUTE_VISIBLE, 1) == 1);
        setAlpha(SAXUtils.getFloatAttribute(attributes, TMXConstants.TAG_LAYER_ATTRIBUTE_OPACITY, 1.0f));
    }

    public String getName() {
        return this.mName;
    }

    public int getTileColumns() {
        return this.mTileColumns;
    }

    public int getTileRows() {
        return this.mTileRows;
    }

    public TMXTile[][] getTMXTiles() {
        return this.mTMXTiles;
    }

    public TMXTile getTMXTile(int i, int i2) {
        return this.mTMXTiles[i2][i];
    }

    public TMXTile getTMXTileAt(float f, float f2) {
        float[] convertSceneToLocalCoordinates = convertSceneToLocalCoordinates(f, f2);
        TMXTiledMap tMXTiledMap = this.mTMXTiledMap;
        int tileWidth = (int) (convertSceneToLocalCoordinates[0] / ((float) tMXTiledMap.getTileWidth()));
        if (tileWidth < 0 || tileWidth > this.mTileColumns - 1) {
            return null;
        }
        int tileWidth2 = (int) (convertSceneToLocalCoordinates[1] / ((float) tMXTiledMap.getTileWidth()));
        if (tileWidth2 < 0 || tileWidth2 > this.mTileRows - 1) {
            return null;
        }
        return this.mTMXTiles[tileWidth2][tileWidth];
    }

    public void addTMXLayerProperty(TMXLayerProperty tMXLayerProperty) {
        this.mTMXLayerProperties.add(tMXLayerProperty);
    }

    public TMXProperties getTMXLayerProperties() {
        return this.mTMXLayerProperties;
    }

    public void setRotation(float f) {
    }

    /* access modifiers changed from: protected */
    public void onUpdateVertexBuffer() {
    }

    /* access modifiers changed from: protected */
    public void onInitDraw(GL10 gl10) {
        super.onInitDraw(gl10);
        GLHelper.enableTextures(gl10);
        GLHelper.enableTexCoordArray(gl10);
    }

    /* access modifiers changed from: protected */
    public void onApplyVertices(GL10 gl10) {
        if (GLHelper.EXTENSIONS_VERTEXBUFFEROBJECTS) {
            GL11 gl11 = (GL11) gl10;
            this.mTMXTiledMap.getSharedVertexBuffer().selectOnHardware(gl11);
            GLHelper.vertexZeroPointer(gl11);
            return;
        }
        GLHelper.vertexPointer(gl10, this.mTMXTiledMap.getSharedVertexBuffer().getFloatBuffer());
    }

    /* access modifiers changed from: protected */
    public void drawVertices(GL10 gl10, Camera camera) {
        TMXTile[][] tMXTileArr = this.mTMXTiles;
        int i = this.mTileColumns;
        int i2 = this.mTileRows;
        int tileWidth = this.mTMXTiledMap.getTileWidth();
        int tileHeight = this.mTMXTiledMap.getTileHeight();
        float f = ((float) tileWidth) * this.mScaleX;
        float f2 = ((float) tileHeight) * this.mScaleY;
        float[] fArr = this.mCullingVertices;
        RectangularShapeCollisionChecker.fillVertices(this, fArr);
        float f3 = fArr[0];
        float f4 = fArr[1];
        float minX = camera.getMinX();
        float minY = camera.getMinY();
        float width = camera.getWidth();
        float height = camera.getHeight();
        float f5 = (minX - f3) / f;
        int bringToBounds = MathUtils.bringToBounds(0, i - 1, (int) Math.floor((double) f5));
        int bringToBounds2 = MathUtils.bringToBounds(0, i - 1, (int) Math.ceil((double) ((width / f) + f5)));
        float f6 = (minY - f4) / f2;
        int bringToBounds3 = MathUtils.bringToBounds(0, i2 - 1, (int) Math.floor((double) f6));
        int bringToBounds4 = MathUtils.bringToBounds(0, i2 - 1, (int) Math.floor((double) (f6 + (height / f2))));
        int i3 = ((bringToBounds2 - bringToBounds) + 1) * tileWidth;
        gl10.glTranslatef((float) (bringToBounds * tileWidth), (float) (bringToBounds3 * tileHeight), 0.0f);
        for (int i4 = bringToBounds3; i4 <= bringToBounds4; i4++) {
            TMXTile[] tMXTileArr2 = tMXTileArr[i4];
            for (int i5 = bringToBounds; i5 <= bringToBounds2; i5++) {
                TextureRegion textureRegion = tMXTileArr2[i5].mTextureRegion;
                if (textureRegion != null) {
                    textureRegion.onApply(gl10);
                    gl10.glDrawArrays(5, 0, 4);
                }
                gl10.glTranslatef((float) tileWidth, 0.0f, 0.0f);
            }
            gl10.glTranslatef((float) (-i3), (float) tileHeight, 0.0f);
        }
        gl10.glLoadIdentity();
    }

    /* access modifiers changed from: protected */
    public void onManagedUpdate(float f) {
    }

    /* access modifiers changed from: package-private */
    public void initializeTMXTileFromXML(Attributes attributes, TMXLoader.ITMXTilePropertiesListener iTMXTilePropertiesListener) {
        addTileByGlobalTileID(SAXUtils.getIntAttributeOrThrow(attributes, TMXConstants.TAG_TILE_ATTRIBUTE_GID), iTMXTilePropertiesListener);
    }

    /* access modifiers changed from: package-private */
    public void initializeTMXTilesFromDataString(String str, String str2, String str3, TMXLoader.ITMXTilePropertiesListener iTMXTilePropertiesListener) {
        DataInputStream dataInputStream;
        Throwable th;
        try {
            InputStream byteArrayInputStream = new ByteArrayInputStream(str.getBytes("UTF-8"));
            if (str2 != null && str2.equals(TMXConstants.TAG_DATA_ATTRIBUTE_ENCODING_VALUE_BASE64)) {
                byteArrayInputStream = new Base64InputStream(byteArrayInputStream, 0);
            }
            if (str3 != null) {
                if (str3.equals(TMXConstants.TAG_DATA_ATTRIBUTE_COMPRESSION_VALUE_GZIP)) {
                    byteArrayInputStream = new GZIPInputStream(byteArrayInputStream);
                } else {
                    throw new IllegalArgumentException("Supplied compression '" + str3 + "' is not supported yet.");
                }
            }
            DataInputStream dataInputStream2 = new DataInputStream(byteArrayInputStream);
            while (this.mTilesAdded < this.mGlobalTileIDsExpected) {
                try {
                    addTileByGlobalTileID(readGlobalTileID(dataInputStream2), iTMXTilePropertiesListener);
                } catch (Throwable th2) {
                    th = th2;
                    dataInputStream = dataInputStream2;
                }
            }
            StreamUtils.close(dataInputStream2);
        } catch (Throwable th3) {
            Throwable th4 = th3;
            dataInputStream = null;
            th = th4;
            StreamUtils.close(dataInputStream);
            throw th;
        }
    }

    private void addTileByGlobalTileID(int i, TMXLoader.ITMXTilePropertiesListener iTMXTilePropertiesListener) {
        TextureRegion textureRegionFromGlobalTileID;
        TMXProperties tMXTileProperties;
        TMXTiledMap tMXTiledMap = this.mTMXTiledMap;
        int i2 = this.mTileColumns;
        int i3 = this.mTilesAdded % i2;
        int i4 = this.mTilesAdded / i2;
        TMXTile[][] tMXTileArr = this.mTMXTiles;
        if (i == 0) {
            textureRegionFromGlobalTileID = null;
        } else {
            textureRegionFromGlobalTileID = tMXTiledMap.getTextureRegionFromGlobalTileID(i);
        }
        TMXTile tMXTile = new TMXTile(i, i3, i4, this.mTMXTiledMap.getTileWidth(), this.mTMXTiledMap.getTileHeight(), textureRegionFromGlobalTileID);
        tMXTileArr[i4][i3] = tMXTile;
        if (!(i == 0 || iTMXTilePropertiesListener == null || (tMXTileProperties = tMXTiledMap.getTMXTileProperties(i)) == null)) {
            iTMXTilePropertiesListener.onTMXTileWithPropertiesCreated(tMXTiledMap, this, tMXTile, tMXTileProperties);
        }
        this.mTilesAdded++;
    }

    private int readGlobalTileID(DataInputStream dataInputStream) {
        int read = dataInputStream.read();
        int read2 = dataInputStream.read();
        int read3 = dataInputStream.read();
        int read4 = dataInputStream.read();
        if (read >= 0 && read2 >= 0 && read3 >= 0 && read4 >= 0) {
            return read | (read2 << 8) | (read3 << 16) | (read4 << 24);
        }
        throw new IllegalArgumentException("Couldn't read global Tile ID.");
    }
}
