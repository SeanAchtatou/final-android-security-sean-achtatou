package org.anddev.andengine.sensor.accelerometer;

import java.util.Arrays;
import org.anddev.andengine.sensor.BaseSensorData;

public class AccelerometerData extends BaseSensorData {
    private static final IAxisSwap[] AXISSWAPS;

    interface IAxisSwap {
        void swapAxis(float[] fArr);
    }

    static {
        IAxisSwap[] iAxisSwapArr = new IAxisSwap[4];
        AXISSWAPS = iAxisSwapArr;
        iAxisSwapArr[0] = new IAxisSwap() {
            public void swapAxis(float[] fArr) {
                float f = fArr[1];
                fArr[0] = -fArr[0];
                fArr[1] = f;
            }
        };
        AXISSWAPS[1] = new IAxisSwap() {
            public void swapAxis(float[] fArr) {
                float f = fArr[1];
                float f2 = fArr[0];
                fArr[0] = f;
                fArr[1] = f2;
            }
        };
        AXISSWAPS[2] = new IAxisSwap() {
            public void swapAxis(float[] fArr) {
                fArr[0] = fArr[0];
                fArr[1] = -fArr[1];
            }
        };
        AXISSWAPS[3] = new IAxisSwap() {
            public void swapAxis(float[] fArr) {
                fArr[0] = -fArr[1];
                fArr[1] = -fArr[0];
            }
        };
    }

    public AccelerometerData(int i) {
        super(3, i);
    }

    public float getX() {
        return this.mValues[0];
    }

    public float getY() {
        return this.mValues[1];
    }

    public float getZ() {
        return this.mValues[2];
    }

    public void setX(float f) {
        this.mValues[0] = f;
    }

    public void setY(float f) {
        this.mValues[1] = f;
    }

    public void setZ(float f) {
        this.mValues[2] = f;
    }

    public void setValues(float[] fArr) {
        super.setValues(fArr);
        AXISSWAPS[this.mDisplayRotation].swapAxis(this.mValues);
    }

    public String toString() {
        return "Accelerometer: " + Arrays.toString(this.mValues);
    }
}
