package org.anddev.andengine.sensor.orientation;

import android.hardware.SensorManager;
import java.util.Arrays;
import org.anddev.andengine.sensor.BaseSensorData;

public class OrientationData extends BaseSensorData {
    private final float[] mAccelerometerValues = new float[3];
    private int mMagneticFieldAccuracy;
    private final float[] mMagneticFieldValues = new float[3];
    private final float[] mRotationMatrix = new float[16];

    public OrientationData(int i) {
        super(3, i);
    }

    public float getRoll() {
        return this.mValues[2];
    }

    public float getPitch() {
        return this.mValues[1];
    }

    public float getYaw() {
        return this.mValues[0];
    }

    public void setValues(float[] fArr) {
        super.setValues(fArr);
    }

    public void setAccuracy(int i) {
        super.setAccuracy(i);
    }

    public void setAccelerometerValues(float[] fArr) {
        System.arraycopy(fArr, 0, this.mAccelerometerValues, 0, fArr.length);
        updateOrientation();
    }

    public void setMagneticFieldValues(float[] fArr) {
        System.arraycopy(fArr, 0, this.mMagneticFieldValues, 0, fArr.length);
        updateOrientation();
    }

    private void updateOrientation() {
        SensorManager.getRotationMatrix(this.mRotationMatrix, null, this.mAccelerometerValues, this.mMagneticFieldValues);
        switch (this.mDisplayRotation) {
            case 1:
                SensorManager.remapCoordinateSystem(this.mRotationMatrix, 2, 129, this.mRotationMatrix);
                break;
        }
        float[] fArr = this.mValues;
        SensorManager.getOrientation(this.mRotationMatrix, fArr);
        for (int length = fArr.length - 1; length >= 0; length--) {
            fArr[length] = fArr[length] * 57.295776f;
        }
    }

    public int getAccelerometerAccuracy() {
        return getAccuracy();
    }

    public void setAccelerometerAccuracy(int i) {
        super.setAccuracy(i);
    }

    public int getMagneticFieldAccuracy() {
        return this.mMagneticFieldAccuracy;
    }

    public void setMagneticFieldAccuracy(int i) {
        this.mMagneticFieldAccuracy = i;
    }

    public String toString() {
        return "Orientation: " + Arrays.toString(this.mValues);
    }
}
