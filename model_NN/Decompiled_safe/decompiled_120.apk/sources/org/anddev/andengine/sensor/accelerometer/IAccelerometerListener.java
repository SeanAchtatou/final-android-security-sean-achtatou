package org.anddev.andengine.sensor.accelerometer;

public interface IAccelerometerListener {
    void onAccelerometerChanged(AccelerometerData accelerometerData);
}
