package org.anddev.andengine.sensor.location;

import android.location.Criteria;

public class LocationSensorOptions extends Criteria {
    private static final long MINIMUMTRIGGERDISTANCE_DEFAULT = 10;
    private static final long MINIMUMTRIGGERTIME_DEFAULT = 1000;
    private boolean mEnabledOnly = true;
    private long mMinimumTriggerDistance = MINIMUMTRIGGERDISTANCE_DEFAULT;
    private long mMinimumTriggerTime = 1000;

    public LocationSensorOptions() {
    }

    public LocationSensorOptions(int i, boolean z, boolean z2, boolean z3, int i2, boolean z4, boolean z5, long j, long j2) {
        this.mEnabledOnly = z5;
        this.mMinimumTriggerTime = j;
        this.mMinimumTriggerDistance = j2;
        setAccuracy(i);
        setAltitudeRequired(z);
        setBearingRequired(z2);
        setCostAllowed(z3);
        setPowerRequirement(i2);
        setSpeedRequired(z4);
    }

    public void setEnabledOnly(boolean z) {
        this.mEnabledOnly = z;
    }

    public boolean isEnabledOnly() {
        return this.mEnabledOnly;
    }

    public long getMinimumTriggerTime() {
        return this.mMinimumTriggerTime;
    }

    public void setMinimumTriggerTime(long j) {
        this.mMinimumTriggerTime = j;
    }

    public long getMinimumTriggerDistance() {
        return this.mMinimumTriggerDistance;
    }

    public void setMinimumTriggerDistance(long j) {
        this.mMinimumTriggerDistance = j;
    }
}
