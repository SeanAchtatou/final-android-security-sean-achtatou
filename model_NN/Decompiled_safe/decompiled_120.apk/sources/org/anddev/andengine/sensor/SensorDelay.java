package org.anddev.andengine.sensor;

public enum SensorDelay {
    NORMAL(3),
    UI(2),
    GAME(1),
    FASTEST(0);
    
    private final int mDelay;

    private SensorDelay(int i) {
        this.mDelay = i;
    }

    public final int getDelay() {
        return this.mDelay;
    }
}
