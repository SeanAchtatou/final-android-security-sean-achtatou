package org.anddev.andengine.sensor;

import java.util.Arrays;

public class BaseSensorData {
    protected int mAccuracy;
    protected int mDisplayRotation;
    protected final float[] mValues;

    public BaseSensorData(int i, int i2) {
        this.mValues = new float[i];
        this.mDisplayRotation = i2;
    }

    public float[] getValues() {
        return this.mValues;
    }

    public void setValues(float[] fArr) {
        System.arraycopy(fArr, 0, this.mValues, 0, fArr.length);
    }

    public void setAccuracy(int i) {
        this.mAccuracy = i;
    }

    public int getAccuracy() {
        return this.mAccuracy;
    }

    public String toString() {
        return "Values: " + Arrays.toString(this.mValues);
    }
}
