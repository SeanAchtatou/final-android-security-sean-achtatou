package com.maths;

public final class R {

    public static final class attr {
        public static final int adSize = 2130771968;
        public static final int adUnitId = 2130771969;
    }

    public static final class drawable {
        public static final int about = 2130837504;
        public static final int all = 2130837505;
        public static final int blank = 2130837506;
        public static final int divide = 2130837507;
        public static final int equal = 2130837508;
        public static final int formula = 2130837509;
        public static final int icon = 2130837510;
        public static final int math = 2130837511;
        public static final int minus = 2130837512;
        public static final int multiply = 2130837513;
        public static final int next = 2130837514;
        public static final int number = 2130837515;
        public static final int plus = 2130837516;
        public static final int previous = 2130837517;
    }

    public static final class id {
        public static final int BANNER = 2131034112;
        public static final int IAB_BANNER = 2131034114;
        public static final int IAB_LEADERBOARD = 2131034115;
        public static final int IAB_MRECT = 2131034113;
        public static final int b0 = 2131034170;
        public static final int b1 = 2131034160;
        public static final int b2 = 2131034161;
        public static final int b3 = 2131034162;
        public static final int b4 = 2131034163;
        public static final int b5 = 2131034164;
        public static final int b6 = 2131034165;
        public static final int b7 = 2131034166;
        public static final int b8 = 2131034167;
        public static final int b9 = 2131034168;
        public static final int bA = 2131034120;
        public static final int bAddition = 2131034144;
        public static final int bAll = 2131034149;
        public static final int bB = 2131034121;
        public static final int bC = 2131034123;
        public static final int bClear = 2131034128;
        public static final int bClearAnswer = 2131034142;
        public static final int bClose = 2131034127;
        public static final int bCurrentLevel = 2131034156;
        public static final int bD = 2131034124;
        public static final int bDivision = 2131034147;
        public static final int bEight = 2131034140;
        public static final int bFive = 2131034137;
        public static final int bFormula = 2131034151;
        public static final int bFour = 2131034136;
        public static final int bHome = 2131034155;
        public static final int bMinus = 2131034125;
        public static final int bMultiplication = 2131034146;
        public static final int bNextLevel = 2131034157;
        public static final int bNine = 2131034141;
        public static final int bNumber = 2131034150;
        public static final int bOne = 2131034133;
        public static final int bOpen = 2131034126;
        public static final int bPlus = 2131034122;
        public static final int bSeven = 2131034139;
        public static final int bSix = 2131034138;
        public static final int bSolve = 2131034130;
        public static final int bSubtraction = 2131034145;
        public static final int bThree = 2131034135;
        public static final int bTimesTable = 2131034148;
        public static final int bTimesTableClear = 2131034169;
        public static final int bTimesTableOk = 2131034171;
        public static final int bTwo = 2131034134;
        public static final int bZero = 2131034143;
        public static final int etAnswer = 2131034132;
        public static final int etFormula = 2131034119;
        public static final int etTimesTable = 2131034159;
        public static final int menuabout = 2131034172;
        public static final int tvLevel = 2131034152;
        public static final int tvQuestion = 2131034131;
        public static final int tvResult = 2131034118;
        public static final int tvSolve = 2131034129;
        public static final int tvTarget = 2131034117;
        public static final int tvTimeTaken = 2131034154;
        public static final int tvTimesTable = 2131034158;
        public static final int tvWebsite = 2131034116;
        public static final int tvWrongAttempts = 2131034153;
    }

    public static final class layout {
        public static final int about = 2130903040;
        public static final int formula = 2130903041;
        public static final int math = 2130903042;
        public static final int menu = 2130903043;
        public static final int result = 2130903044;
        public static final int timestable = 2130903045;
    }

    public static final class menu {
        public static final int titles = 2131099648;
    }

    public static final class string {
        public static final int app_name = 2130968577;
        public static final int hello = 2130968576;
        public static final int menuabout = 2130968578;
    }

    public static final class styleable {
        public static final int[] com_google_ads_AdView = {R.attr.adSize, R.attr.adUnitId};
        public static final int com_google_ads_AdView_adSize = 0;
        public static final int com_google_ads_AdView_adUnitId = 1;
    }
}
