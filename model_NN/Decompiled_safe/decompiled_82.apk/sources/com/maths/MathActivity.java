package com.maths;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import java.lang.reflect.Array;

public class MathActivity extends Activity {
    private int COLS = 0;
    private int CurAryRow = 0;
    private NumQuestions NumQu = new NumQuestions();
    private Questions Qu = new Questions();
    private int ROWS = 0;
    private TTQuestions TTQu = new TTQuestions();
    private StopWatch TimeTaken = new StopWatch();
    private int WrongAttempts = 0;
    private String[][] ary;
    /* access modifiers changed from: private */
    public EditText etAnswer;
    public Menu menu;
    private String sMath;
    private TextView tvQuestion;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.math);
        this.tvQuestion = (TextView) findViewById(R.id.tvQuestion);
        this.etAnswer = (EditText) findViewById(R.id.etAnswer);
        ((EditText) findViewById(R.id.etAnswer)).setInputType(0);
        Button bClearAnswer = (Button) findViewById(R.id.bClearAnswer);
        bClearAnswer.setTextColor(-65536);
        ((Button) findViewById(R.id.bOne)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                MathActivity.this.AppendAnswer("1");
            }
        });
        ((Button) findViewById(R.id.bTwo)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                MathActivity.this.AppendAnswer("2");
            }
        });
        ((Button) findViewById(R.id.bThree)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                MathActivity.this.AppendAnswer("3");
            }
        });
        ((Button) findViewById(R.id.bFour)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                MathActivity.this.AppendAnswer("4");
            }
        });
        ((Button) findViewById(R.id.bFive)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                MathActivity.this.AppendAnswer("5");
            }
        });
        ((Button) findViewById(R.id.bSix)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                MathActivity.this.AppendAnswer("6");
            }
        });
        ((Button) findViewById(R.id.bSeven)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                MathActivity.this.AppendAnswer("7");
            }
        });
        ((Button) findViewById(R.id.bEight)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                MathActivity.this.AppendAnswer("8");
            }
        });
        ((Button) findViewById(R.id.bNine)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                MathActivity.this.AppendAnswer("9");
            }
        });
        ((Button) findViewById(R.id.bZero)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                MathActivity.this.AppendAnswer("0");
            }
        });
        bClearAnswer.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                MathActivity.this.etAnswer.setText("");
            }
        });
        Questions();
        this.tvQuestion.setText("");
        this.tvQuestion.setText(this.ary[this.CurAryRow][0]);
        this.TimeTaken.start();
    }

    public void Questions() {
        this.ROWS = ((MyVar) getApplication()).get_ROWS(((MyVar) getApplication()).get_iLevel());
        this.COLS = ((MyVar) getApplication()).get_COLS();
        this.ary = (String[][]) Array.newInstance(String.class, this.ROWS, this.COLS);
        this.sMath = ((MyVar) getApplication()).get_sMath();
        if (this.sMath.equals("TimesTable")) {
            this.ROWS = 10;
            this.ary = this.TTQu.get(((MyVar) getApplication()).get_iVar1());
        } else if (this.sMath.equals("Number")) {
            this.ary = this.NumQu.get(((MyVar) getApplication()).get_iLevel());
        } else {
            this.ary = this.Qu.get(this.ROWS, this.COLS, this.sMath, ((MyVar) getApplication()).get_iLevel());
        }
    }

    public void Process() {
        if (this.CurAryRow < this.ROWS && this.ary[this.CurAryRow][2].trim().equals(Integer.toString(this.etAnswer.length()))) {
            if (this.ary[this.CurAryRow][1].trim().equals(this.etAnswer.getText().toString().trim())) {
                this.tvQuestion.setTextColor(-1);
                if (this.CurAryRow < this.ROWS - 1) {
                    this.CurAryRow++;
                    this.tvQuestion.setText("");
                    this.tvQuestion.setText(this.ary[this.CurAryRow][0]);
                    this.etAnswer.setText("");
                    return;
                }
                this.TimeTaken.stop();
                ((MyVar) getApplication()).set_sVar1(Integer.toString(this.WrongAttempts));
                ((MyVar) getApplication()).set_sVar2(String.valueOf(Long.toString(this.TimeTaken.getElapsedTime())) + " seconds");
                launchResultActivity();
                return;
            }
            this.WrongAttempts++;
            vibrate();
            this.tvQuestion.setTextColor(-65536);
            this.etAnswer.setText("");
        }
    }

    public void AppendAnswer(String UserInput) {
        if (!this.ary[this.CurAryRow][2].trim().equals(Integer.toString(this.etAnswer.length()))) {
            this.etAnswer.append(UserInput);
            Process();
        }
    }

    /* access modifiers changed from: protected */
    public void launchResultActivity() {
        Intent j = new Intent(this, ResultActivity.class);
        finish();
        startActivity(j);
    }

    public void onBackPressed() {
        Intent j = new Intent(this, MenuActivity.class);
        finish();
        startActivity(j);
    }

    public void vibrate() {
        ((Vibrator) getSystemService("vibrator")).vibrate(300);
    }

    public boolean onCreateOptionsMenu(Menu menu2) {
        this.menu = menu2;
        getMenuInflater().inflate(R.menu.titles, menu2);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuabout /*2131034172*/:
                startActivity(new Intent(this, AboutActivity.class));
                return true;
            default:
                if (!item.hasSubMenu()) {
                    return true;
                }
                return false;
        }
    }
}
