package com.maths;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class TimesTableActivity extends Activity {
    /* access modifiers changed from: private */
    public EditText etTimesTable;
    public Menu menu;
    /* access modifiers changed from: private */
    public TextView tvTimesTable;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.timestable);
        this.tvTimesTable = (TextView) findViewById(R.id.tvTimesTable);
        this.etTimesTable = (EditText) findViewById(R.id.etTimesTable);
        ((EditText) findViewById(R.id.etTimesTable)).setInputType(0);
        Button bTimesTableOk = (Button) findViewById(R.id.bTimesTableOk);
        Button bTimesTableClear = (Button) findViewById(R.id.bTimesTableClear);
        bTimesTableClear.setTextColor(-65536);
        bTimesTableOk.setTextColor(-16776961);
        ((Button) findViewById(R.id.b1)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                TimesTableActivity.this.AppendAnswer("1");
            }
        });
        ((Button) findViewById(R.id.b2)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                TimesTableActivity.this.AppendAnswer("2");
            }
        });
        ((Button) findViewById(R.id.b3)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                TimesTableActivity.this.AppendAnswer("3");
            }
        });
        ((Button) findViewById(R.id.b4)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                TimesTableActivity.this.AppendAnswer("4");
            }
        });
        ((Button) findViewById(R.id.b5)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                TimesTableActivity.this.AppendAnswer("5");
            }
        });
        ((Button) findViewById(R.id.b6)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                TimesTableActivity.this.AppendAnswer("6");
            }
        });
        ((Button) findViewById(R.id.b7)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                TimesTableActivity.this.AppendAnswer("7");
            }
        });
        ((Button) findViewById(R.id.b8)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                TimesTableActivity.this.AppendAnswer("8");
            }
        });
        ((Button) findViewById(R.id.b9)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                TimesTableActivity.this.AppendAnswer("9");
            }
        });
        ((Button) findViewById(R.id.b0)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                TimesTableActivity.this.AppendAnswer("0");
            }
        });
        bTimesTableOk.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (TimesTableActivity.this.etTimesTable.getText().toString().trim().equals("")) {
                    TimesTableActivity.this.vibrate();
                    TimesTableActivity.this.tvTimesTable.setTextColor(-65536);
                    return;
                }
                ((MyVar) TimesTableActivity.this.getApplication()).set_iVar1(Integer.parseInt(TimesTableActivity.this.etTimesTable.getText().toString().trim()));
                if (((MyVar) TimesTableActivity.this.getApplication()).get_iVar1() <= 0) {
                    TimesTableActivity.this.vibrate();
                    TimesTableActivity.this.tvTimesTable.setTextColor(-65536);
                    TimesTableActivity.this.etTimesTable.setText("");
                } else if (((MyVar) TimesTableActivity.this.getApplication()).get_iVar1() < 101) {
                    TimesTableActivity.this.launchMathActivity();
                } else {
                    TimesTableActivity.this.vibrate();
                    TimesTableActivity.this.tvTimesTable.setTextColor(-65536);
                    TimesTableActivity.this.etTimesTable.setText("");
                }
            }
        });
        bTimesTableClear.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                TimesTableActivity.this.etTimesTable.setText("");
            }
        });
    }

    /* access modifiers changed from: protected */
    public void launchMathActivity() {
        Intent j = new Intent(this, MathActivity.class);
        finish();
        startActivity(j);
    }

    public void AppendAnswer(String UserInput) {
        this.tvTimesTable.setTextColor(-1);
        if (!"3".equals(Integer.toString(this.etTimesTable.length()))) {
            this.etTimesTable.append(UserInput);
        }
    }

    public void onBackPressed() {
        Intent j = new Intent(this, MenuActivity.class);
        finish();
        startActivity(j);
    }

    public void vibrate() {
        ((Vibrator) getSystemService("vibrator")).vibrate(300);
    }

    public boolean onCreateOptionsMenu(Menu menu2) {
        this.menu = menu2;
        getMenuInflater().inflate(R.menu.titles, menu2);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuabout:
                startActivity(new Intent(this, AboutActivity.class));
                return true;
            default:
                if (!item.hasSubMenu()) {
                    return true;
                }
                return false;
        }
    }
}
