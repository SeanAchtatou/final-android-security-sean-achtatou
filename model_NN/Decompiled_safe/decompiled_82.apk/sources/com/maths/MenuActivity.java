package com.maths;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;

public class MenuActivity extends Activity {
    public Menu menu;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.menu);
        ((ImageButton) findViewById(R.id.bAddition)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                MenuActivity.this.launchMathActivity("Addition");
            }
        });
        ((ImageButton) findViewById(R.id.bSubtraction)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                MenuActivity.this.launchMathActivity("Subtraction");
            }
        });
        ((ImageButton) findViewById(R.id.bDivision)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                MenuActivity.this.launchMathActivity("Division");
            }
        });
        ((ImageButton) findViewById(R.id.bMultiplication)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                MenuActivity.this.launchMathActivity("Multiplication");
            }
        });
        ((ImageButton) findViewById(R.id.bTimesTable)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                MenuActivity.this.launchTimesTableActivity("TimesTable");
            }
        });
        ((ImageButton) findViewById(R.id.bAll)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                MenuActivity.this.launchMathActivity("All");
            }
        });
        ((ImageButton) findViewById(R.id.bNumber)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                MenuActivity.this.launchMathActivity("Number");
            }
        });
        ((ImageButton) findViewById(R.id.bFormula)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                MenuActivity.this.launchFormulaActivity("Formula");
            }
        });
    }

    /* access modifiers changed from: protected */
    public void launchFormulaActivity(String sMath) {
        ((MyVar) getApplication()).set_sMath(sMath);
        ((MyVar) getApplication()).set_iLevel(1);
        Intent i = new Intent(this, FormulaActivity.class);
        finish();
        startActivity(i);
    }

    /* access modifiers changed from: protected */
    public void launchMathActivity(String sMath) {
        ((MyVar) getApplication()).set_sMath(sMath);
        ((MyVar) getApplication()).set_iLevel(1);
        Intent i = new Intent(this, MathActivity.class);
        finish();
        startActivity(i);
    }

    /* access modifiers changed from: protected */
    public void launchTimesTableActivity(String sMath) {
        ((MyVar) getApplication()).set_sMath(sMath);
        ((MyVar) getApplication()).set_iLevel(1);
        Intent i = new Intent(this, TimesTableActivity.class);
        finish();
        startActivity(i);
    }

    public boolean onCreateOptionsMenu(Menu menu2) {
        this.menu = menu2;
        getMenuInflater().inflate(R.menu.titles, menu2);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuabout:
                startActivity(new Intent(this, AboutActivity.class));
                return true;
            default:
                if (!item.hasSubMenu()) {
                    return true;
                }
                return false;
        }
    }
}
