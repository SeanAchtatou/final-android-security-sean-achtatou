package com.maths;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class NumQuestions {
    ArrayList<Integer> Seq = new ArrayList<>();
    int a = 0;
    int answer = 0;
    int b = 0;
    int c = 0;
    int d = 0;
    int ilevel = 0;
    String question = "";
    Random rand = new Random();

    public String[][] get(int level) {
        this.ilevel = level;
        for (int i = 0; i < 10; i++) {
            this.Seq.add(Integer.valueOf(i));
        }
        Collections.shuffle(this.Seq);
        String[][] ary1 = (String[][]) Array.newInstance(String.class, 10, 3);
        for (int i2 = 0; i2 < 10; i2++) {
            ary1[i2][0] = getQuestion(this.Seq.get(i2).intValue());
            ary1[i2][1] = getAnswer();
            ary1[i2][2] = getAnswerLength();
        }
        return ary1;
    }

    public String getQuestion(int seq) {
        if (seq == 0) {
            QA0();
        }
        if (seq == 1) {
            QA1();
        }
        if (seq == 2) {
            QA2();
        }
        if (seq == 3) {
            QA3();
        }
        if (seq == 4) {
            QA4();
        }
        if (seq == 5) {
            QA5();
        }
        if (seq == 6) {
            QA6();
        }
        if (seq == 7) {
            QA7();
        }
        if (seq == 8) {
            QA8();
        }
        if (seq == 9) {
            QA9();
        }
        return this.question;
    }

    public String getAnswer() {
        return Integer.toString(this.answer);
    }

    public String getAnswerLength() {
        return Integer.toString(Integer.toString(this.answer).length());
    }

    private void getRandom() {
        int min = ((this.ilevel - 1) * 10) + 1;
        int max = this.ilevel * 10;
        if (min > 11) {
            min -= 10;
        }
        this.a = this.rand.nextInt(max - min) + min;
        this.b = this.rand.nextInt(max - min) + min;
        this.c = this.rand.nextInt(max - min) + min;
        this.d = this.rand.nextInt(max - min) + min;
    }

    private void QA0() {
        do {
            getRandom();
            this.answer = this.a + this.b + this.c + this.d;
            this.question = "(" + this.a + " + " + this.b + ") + (" + this.c + " + " + this.d + ")";
        } while (this.answer <= 1);
    }

    private void QA1() {
        do {
            getRandom();
            this.answer = this.a + this.b + (this.c - this.d);
            this.question = "(" + this.a + " + " + this.b + ") + (" + this.c + " - " + this.d + ")";
        } while (this.answer <= 1);
    }

    private void QA2() {
        do {
            getRandom();
            this.answer = (this.a + this.b) - (this.c - this.d);
            this.question = "(" + this.a + " + " + this.b + ") - (" + this.c + " - " + this.d + ")";
        } while (this.answer <= 1);
    }

    private void QA3() {
        do {
            getRandom();
            this.answer = (this.a - this.b) - (this.c - this.d);
            this.question = "(" + this.a + " - " + this.b + ") - (" + this.c + " - " + this.d + ")";
        } while (this.answer <= 1);
    }

    private void QA4() {
        do {
            getRandom();
            this.answer = (this.a + this.b) - (this.c + this.d);
            this.question = "(" + this.a + " + " + this.b + ") - (" + this.c + " + " + this.d + ")";
        } while (this.answer <= 1);
    }

    private void QA5() {
        do {
            getRandom();
            this.answer = (this.a - this.b) + this.c + this.d;
            this.question = "(" + this.a + " - " + this.b + ") + (" + this.c + " + " + this.d + ")";
        } while (this.answer <= 1);
    }

    private void QA6() {
        do {
            getRandom();
            this.answer = (this.a + this.b) - (this.c - this.d);
            this.question = "(" + this.a + " + " + this.b + ") - (" + this.c + " - " + this.d + ")";
        } while (this.answer <= 1);
    }

    private void QA7() {
        do {
            getRandom();
            this.answer = (this.a - this.b) + (this.c - this.d);
            this.question = "(" + this.a + " - " + this.b + ") + (" + this.c + " - " + this.d + ")";
        } while (this.answer <= 1);
    }

    private void QA8() {
        do {
            getRandom();
            this.answer = (this.a - this.b) - (this.c - this.d);
            this.question = "(" + this.a + " - " + this.b + ") - (" + this.c + " - " + this.d + ")";
        } while (this.answer <= 1);
    }

    private void QA9() {
        do {
            getRandom();
            this.answer = this.a + this.b + (this.c - this.d);
            this.question = "(" + this.a + " + " + this.b + ") + (" + this.c + " - " + this.d + ")";
        } while (this.answer <= 1);
    }
}
