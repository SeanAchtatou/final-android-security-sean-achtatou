package com.maths;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import java.lang.reflect.Array;

public class FormulaActivity extends Activity {
    private int COLS = 0;
    /* access modifiers changed from: private */
    public int CurAryRow = 0;
    /* access modifiers changed from: private */
    public String LastDeletedInput = "";
    /* access modifiers changed from: private */
    public String LastInput = "";
    private FormulaQuestion Qu = new FormulaQuestion();
    private int ROWS = 0;
    private StopWatch TimeTaken = new StopWatch();
    /* access modifiers changed from: private */
    public boolean _bA = false;
    /* access modifiers changed from: private */
    public boolean _bB = false;
    /* access modifiers changed from: private */
    public boolean _bC = false;
    /* access modifiers changed from: private */
    public boolean _bD = false;
    /* access modifiers changed from: private */
    public String[][] ary;
    /* access modifiers changed from: private */
    public EditText etFormula;
    public Menu menu;
    private String r = "";
    private String r1 = "";
    private String s = "";
    private String s1 = "";
    private String t = "";
    private String t1 = "";
    private TextView tvResult;
    /* access modifiers changed from: private */
    public TextView tvSolve;
    private TextView tvTarget;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.formula);
        this.tvTarget = (TextView) findViewById(R.id.tvTarget);
        this.tvResult = (TextView) findViewById(R.id.tvResult);
        this.etFormula = (EditText) findViewById(R.id.etFormula);
        this.tvSolve = (TextView) findViewById(R.id.tvSolve);
        ((EditText) findViewById(R.id.etFormula)).setInputType(0);
        final Button bA = (Button) findViewById(R.id.bA);
        final Button bB = (Button) findViewById(R.id.bB);
        final Button bC = (Button) findViewById(R.id.bC);
        final Button bD = (Button) findViewById(R.id.bD);
        Button bClear = (Button) findViewById(R.id.bClear);
        Button bSolve = (Button) findViewById(R.id.bSolve);
        bClear.setTextColor(-65536);
        final Button button = bA;
        final Button button2 = bC;
        final Button button3 = bD;
        bA.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (FormulaActivity.this.etFormula.getText().toString().trim().equals("") || (!FormulaActivity.this._bA && !FormulaActivity.this.LastInput.equals(button.getText().toString()) && !FormulaActivity.this.LastInput.equals(button2.getText().toString()) && !FormulaActivity.this.LastInput.equals(button3.getText().toString()) && !FormulaActivity.this.LastDeletedInput.equals("(") && !FormulaActivity.this.LastDeletedInput.equals(")") && !FormulaActivity.this.LastDeletedInput.equals("+") && !FormulaActivity.this.LastDeletedInput.equals("-"))) {
                    FormulaActivity.this._bA = true;
                    button.setTextColor(-16776961);
                    FormulaActivity.this.AppendAnswer(button.getText().toString());
                    FormulaActivity.this.LastInput = button.getText().toString();
                }
            }
        });
        bB.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (FormulaActivity.this.etFormula.getText().toString().trim().equals("") || (!FormulaActivity.this._bB && !FormulaActivity.this.LastInput.equals(bA.getText().toString()) && !FormulaActivity.this.LastInput.equals(bC.getText().toString()) && !FormulaActivity.this.LastInput.equals(bD.getText().toString()) && !FormulaActivity.this.LastDeletedInput.equals("(") && !FormulaActivity.this.LastDeletedInput.equals(")") && !FormulaActivity.this.LastDeletedInput.equals("+") && !FormulaActivity.this.LastDeletedInput.equals("-"))) {
                    FormulaActivity.this._bB = true;
                    bB.setTextColor(-16776961);
                    FormulaActivity.this.AppendAnswer(bB.getText().toString());
                    FormulaActivity.this.LastInput = bB.getText().toString();
                }
            }
        });
        final Button button4 = bA;
        final Button button5 = bB;
        final Button button6 = bD;
        final Button button7 = bC;
        bC.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (FormulaActivity.this.etFormula.getText().toString().trim().equals("") || (!FormulaActivity.this._bC && !FormulaActivity.this.LastInput.equals(button4.getText().toString()) && !FormulaActivity.this.LastInput.equals(button5.getText().toString()) && !FormulaActivity.this.LastInput.equals(button6.getText().toString()) && !FormulaActivity.this.LastDeletedInput.equals("(") && !FormulaActivity.this.LastDeletedInput.equals(")") && !FormulaActivity.this.LastDeletedInput.equals("+") && !FormulaActivity.this.LastDeletedInput.equals("-"))) {
                    FormulaActivity.this._bC = true;
                    button7.setTextColor(-16776961);
                    FormulaActivity.this.AppendAnswer(button7.getText().toString());
                    FormulaActivity.this.LastInput = button7.getText().toString();
                }
            }
        });
        final Button button8 = bA;
        final Button button9 = bB;
        final Button button10 = bC;
        final Button button11 = bD;
        bD.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (FormulaActivity.this.etFormula.getText().toString().trim().equals("") || (!FormulaActivity.this._bD && !FormulaActivity.this.LastInput.equals(button8.getText().toString()) && !FormulaActivity.this.LastInput.equals(button9.getText().toString()) && !FormulaActivity.this.LastInput.equals(button10.getText().toString()) && !FormulaActivity.this.LastDeletedInput.equals("(") && !FormulaActivity.this.LastDeletedInput.equals(")") && !FormulaActivity.this.LastDeletedInput.equals("+") && !FormulaActivity.this.LastDeletedInput.equals("-"))) {
                    FormulaActivity.this._bD = true;
                    button11.setTextColor(-16776961);
                    FormulaActivity.this.AppendAnswer(button11.getText().toString());
                    FormulaActivity.this.LastInput = button11.getText().toString();
                }
            }
        });
        ((Button) findViewById(R.id.bPlus)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                FormulaActivity.this.AppendAnswer("+");
                FormulaActivity.this.LastDeletedInput = "";
                FormulaActivity.this.LastInput = "+";
            }
        });
        ((Button) findViewById(R.id.bMinus)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                FormulaActivity.this.AppendAnswer("-");
                FormulaActivity.this.LastDeletedInput = "";
                FormulaActivity.this.LastInput = "-";
            }
        });
        ((Button) findViewById(R.id.bOpen)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                FormulaActivity.this.AppendAnswer("(");
                FormulaActivity.this.LastDeletedInput = "";
                FormulaActivity.this.LastInput = "(";
            }
        });
        ((Button) findViewById(R.id.bClose)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                FormulaActivity.this.AppendAnswer(")");
                FormulaActivity.this.LastDeletedInput = "";
                FormulaActivity.this.LastInput = ")";
            }
        });
        final Button button12 = bA;
        final Button button13 = bB;
        final Button button14 = bC;
        final Button button15 = bD;
        bClear.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                FormulaActivity.this.DeleteChar(button12.getText().toString(), button13.getText().toString(), button14.getText().toString(), button15.getText().toString());
                if (!FormulaActivity.this._bA) {
                    button12.setTextColor(-16777216);
                }
                if (!FormulaActivity.this._bB) {
                    button13.setTextColor(-16777216);
                }
                if (!FormulaActivity.this._bC) {
                    button14.setTextColor(-16777216);
                }
                if (!FormulaActivity.this._bD) {
                    button15.setTextColor(-16777216);
                }
            }
        });
        final Button button16 = bSolve;
        bSolve.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (FormulaActivity.this.tvSolve.getText().toString().equals("")) {
                    FormulaActivity.this.tvSolve.setText(FormulaActivity.this.ary[FormulaActivity.this.CurAryRow][0]);
                    button16.setText("Hide");
                    return;
                }
                FormulaActivity.this.tvSolve.setText("");
                button16.setText("Solve");
            }
        });
        Questions();
        this.tvTarget.setText("");
        this.tvResult.setText("");
        this.etFormula.setText("");
        this.tvSolve.setText("");
        this.tvTarget.setText(this.ary[this.CurAryRow][1]);
        bA.setText(this.ary[this.CurAryRow][3]);
        bB.setText(this.ary[this.CurAryRow][4]);
        bC.setText(this.ary[this.CurAryRow][5]);
        bD.setText(this.ary[this.CurAryRow][6]);
        this.TimeTaken.start();
    }

    public void Questions() {
        this.ROWS = 1;
        this.COLS = 7;
        this.ary = (String[][]) Array.newInstance(String.class, this.ROWS, this.COLS);
        this.ary = this.Qu.get();
    }

    public void Process() {
        try {
            this.tvResult.setText(Long.toString(Math.round(new MathEvaluator(this.etFormula.getText().toString()).getValue().doubleValue())));
            if (!this.tvTarget.getText().equals(this.tvResult.getText())) {
                this.tvTarget.setTextColor(-1);
                this.tvResult.setTextColor(-1);
            } else if (this._bA && this._bB && this._bC && this._bD) {
                this.tvTarget.setTextColor(-16711936);
                this.tvResult.setTextColor(-16711936);
            }
        } catch (Exception e) {
        }
    }

    public void AppendAnswer(String UserInput) {
        if (UserInput.equals("+") || UserInput.equals("-")) {
            this.s = this.etFormula.getText().toString();
            if (this.s != null && this.s.length() > 0) {
                this.s = this.s.substring(this.s.length() - 1);
                if (!this.s.equals("+") && !this.s.equals("-")) {
                    this.etFormula.append(UserInput);
                    Process();
                    return;
                }
                return;
            }
            return;
        }
        this.etFormula.append(UserInput);
        Process();
    }

    public void DeleteChar(String sA, String sB, String sC, String sD) {
        this.s = this.etFormula.getText().toString();
        if (this.s != null && this.s.length() > 0) {
            this.t = this.s.substring(this.s.length() - 1);
            if (!this.t.equals("(") && !this.t.equals(")") && !this.t.equals("+") && !this.t.equals("-")) {
                this.r = "";
                while (true) {
                    this.t = this.s.substring(this.s.length() - 1);
                    if (!this.t.equals("(")) {
                        if (!this.t.equals(")")) {
                            if (!this.t.equals("+")) {
                                if (!this.t.equals("-")) {
                                    this.s = this.s.substring(0, this.s.length() - 1);
                                    this.r = String.valueOf(this.r) + this.t;
                                    if (this.s.length() == 0) {
                                        RemoveNumber(sA, sB, sC, sD);
                                    }
                                    if (this.s != null) {
                                        if (this.s.length() <= 0) {
                                            break;
                                        }
                                    } else {
                                        break;
                                    }
                                } else {
                                    RemoveNumber(sA, sB, sC, sD);
                                    break;
                                }
                            } else {
                                RemoveNumber(sA, sB, sC, sD);
                                break;
                            }
                        } else {
                            RemoveNumber(sA, sB, sC, sD);
                            break;
                        }
                    } else {
                        RemoveNumber(sA, sB, sC, sD);
                        break;
                    }
                }
            } else {
                this.s = this.s.substring(0, this.s.length() - 1);
                this.LastDeletedInput = this.t;
            }
            this.etFormula.setText(this.s);
            GetLastInputAfterDelete_1();
            Process();
        }
        if (this.etFormula.getText().toString().trim().equals("")) {
            this.tvResult.setText("");
        }
        if (this.s.length() == 0) {
            this.LastInput = "";
            this.LastDeletedInput = "";
        }
    }

    public void RemoveNumber(String sA, String sB, String sC, String sD) {
        String reverse = new StringBuffer(this.r).reverse().toString();
        if (reverse.equals(sA)) {
            this._bA = false;
        }
        if (reverse.equals(sB)) {
            this._bB = false;
        }
        if (reverse.equals(sC)) {
            this._bC = false;
        }
        if (reverse.equals(sD)) {
            this._bD = false;
        }
    }

    public void GetLastInputAfterDelete_1() {
        this.s1 = this.etFormula.getText().toString();
        if (this.s1 != null && this.s1.length() > 0) {
            this.t1 = this.s1.substring(this.s1.length() - 1);
            if (this.t1.equals("(") || this.t1.equals(")") || this.t1.equals("+") || this.t1.equals("-")) {
                this.LastInput = this.t1;
                return;
            }
            this.r1 = "";
            do {
                this.t1 = this.s1.substring(this.s1.length() - 1);
                if (this.t1.equals("(")) {
                    GetLastInputAfterDelete_2();
                    return;
                } else if (this.t1.equals(")")) {
                    GetLastInputAfterDelete_2();
                    return;
                } else if (this.t1.equals("+")) {
                    GetLastInputAfterDelete_2();
                    return;
                } else if (this.t1.equals("-")) {
                    GetLastInputAfterDelete_2();
                    return;
                } else {
                    this.s1 = this.s1.substring(0, this.s1.length() - 1);
                    this.r1 = String.valueOf(this.r1) + this.t1;
                    if (this.s1 == null) {
                        return;
                    }
                }
            } while (this.s1.length() > 0);
        }
    }

    public void GetLastInputAfterDelete_2() {
        this.LastInput = new StringBuffer(this.r1).reverse().toString();
    }

    public void onBackPressed() {
        Intent j = new Intent(this, MenuActivity.class);
        finish();
        startActivity(j);
    }

    public boolean onCreateOptionsMenu(Menu menu2) {
        this.menu = menu2;
        getMenuInflater().inflate(R.menu.titles, menu2);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuabout /*2131034172*/:
                startActivity(new Intent(this, AboutActivity.class));
                return true;
            default:
                if (!item.hasSubMenu()) {
                    return true;
                }
                return false;
        }
    }
}
