package com.maths;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

public class ResultActivity extends Activity {
    public Menu menu;
    private TextView tvLevel;
    private TextView tvTimeTaken;
    private TextView tvWrongAttempts;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.result);
        this.tvLevel = (TextView) findViewById(R.id.tvLevel);
        this.tvWrongAttempts = (TextView) findViewById(R.id.tvWrongAttempts);
        this.tvTimeTaken = (TextView) findViewById(R.id.tvTimeTaken);
        this.tvLevel.setText("Level : " + ((MyVar) getApplication()).get_iLevel());
        this.tvWrongAttempts.setText("Wrong attempts : " + ((MyVar) getApplication()).get_sVar1());
        this.tvTimeTaken.setText("Time : " + ((MyVar) getApplication()).get_sVar2());
        ImageButton bHome = (ImageButton) findViewById(R.id.bHome);
        ImageButton bCurrentLevel = (ImageButton) findViewById(R.id.bCurrentLevel);
        ImageButton bNextLevel = (ImageButton) findViewById(R.id.bNextLevel);
        if (((MyVar) getApplication()).get_sVar1().equals("0")) {
            bNextLevel.setClickable(true);
            bNextLevel.setEnabled(true);
        } else {
            bNextLevel.setClickable(false);
            bNextLevel.setEnabled(false);
        }
        bHome.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                ResultActivity.this.finish();
                ResultActivity.this.launchMenuActivity();
            }
        });
        bNextLevel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                ResultActivity.this.finish();
                ResultActivity.this.launchNextLevel();
            }
        });
        bCurrentLevel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                ResultActivity.this.finish();
                ResultActivity.this.launchCurrentLevel();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void launchMenuActivity() {
        Intent j = new Intent(this, MenuActivity.class);
        finish();
        startActivity(j);
    }

    /* access modifiers changed from: protected */
    public void launchNextLevel() {
        ((MyVar) getApplication()).set_iLevel(((MyVar) getApplication()).get_iLevel() + 1);
        ((MyVar) getApplication()).set_iVar1(((MyVar) getApplication()).get_iVar1() + 1);
        Intent j = new Intent(this, MathActivity.class);
        finish();
        startActivity(j);
    }

    /* access modifiers changed from: protected */
    public void launchCurrentLevel() {
        Intent j = new Intent(this, MathActivity.class);
        finish();
        startActivity(j);
    }

    public void onBackPressed() {
        Intent j = new Intent(this, MenuActivity.class);
        finish();
        startActivity(j);
    }

    public boolean onCreateOptionsMenu(Menu menu2) {
        this.menu = menu2;
        getMenuInflater().inflate(R.menu.titles, menu2);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuabout:
                startActivity(new Intent(this, AboutActivity.class));
                return true;
            default:
                if (!item.hasSubMenu()) {
                    return true;
                }
                return false;
        }
    }
}
