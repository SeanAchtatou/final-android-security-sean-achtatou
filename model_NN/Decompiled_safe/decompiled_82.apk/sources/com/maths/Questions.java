package com.maths;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class Questions {
    String _o_sMath = "";
    String _sMath = "";
    ArrayList<Integer> a_numbers1 = new ArrayList<>();
    ArrayList<Integer> a_numbers2 = new ArrayList<>();
    ArrayList<Integer> d_numbers1 = new ArrayList<>();
    ArrayList<Integer> d_numbers2 = new ArrayList<>();
    ArrayList<Integer> d_numbers3 = new ArrayList<>();
    String debug = "";
    ArrayList<Integer> m_numbers1 = new ArrayList<>();
    ArrayList<Integer> m_numbers2 = new ArrayList<>();
    int num1;
    int num2;
    int num3 = 0;
    Random rand = new Random();
    ArrayList<Integer> s_numbers1 = new ArrayList<>();
    ArrayList<Integer> s_numbers2 = new ArrayList<>();

    public String[][] get(int ROWS, int COLS, String sMath, int level) {
        this._o_sMath = sMath;
        this._sMath = sMath;
        if (this._o_sMath.equals("All")) {
            d_fillNumbers(level, ROWS);
            m_fillNumbers(level, ROWS);
            s_fillNumbers(level, ROWS);
            a_fillNumbers(level, ROWS);
        } else {
            fillNumbers(level, ROWS);
        }
        String[][] ary1 = (String[][]) Array.newInstance(String.class, ROWS, COLS);
        for (int i = 0; i < ROWS; i++) {
            if (this._o_sMath.equals("All")) {
                get_Random_sMath(this.rand.nextInt(4));
            }
            getRandom_Numbers(i);
            ary1[i][0] = getQuestion(this.num1, this.num2);
            ary1[i][1] = getAnswer(this.num1, this.num2);
            ary1[i][2] = getAnswerLength(this.num1, this.num2);
        }
        return ary1;
    }

    public void fillNumbers(int level, int ROWS) {
        if (this._sMath.equals("Division")) {
            d_fillNumbers(level, ROWS);
        } else if (this._sMath.equals("Multiplication")) {
            m_fillNumbers(level, ROWS);
        } else if (this._sMath.equals("Subtraction")) {
            s_fillNumbers(level, ROWS);
        } else {
            a_fillNumbers(level, ROWS);
        }
    }

    public void getRandom_Numbers(int i) {
        if (this._sMath.equals("Division")) {
            this.num3 = this.d_numbers3.get(i).intValue();
            this.num1 = this.d_numbers1.get(this.num3).intValue();
            this.num2 = this.d_numbers2.get(this.num3).intValue();
        } else if (this._sMath.equals("Multiplication")) {
            this.num1 = this.m_numbers1.get(i).intValue();
            this.num2 = this.m_numbers2.get(i).intValue();
        } else if (this._sMath.equals("Subtraction")) {
            this.num1 = this.s_numbers1.get(i).intValue();
            this.num2 = this.s_numbers2.get(i).intValue();
            this.num3 = 0;
            if (this.num2 > this.num1) {
                this.num3 = this.num1;
                this.num1 = this.num2;
                this.num2 = this.num3;
            }
        } else {
            this.num1 = this.a_numbers1.get(i).intValue();
            this.num2 = this.a_numbers2.get(i).intValue();
        }
    }

    public String getQuestion(int num12, int num22) {
        if (this._sMath.equals("Addition")) {
            return String.valueOf(Integer.toString(num12)) + " + " + Integer.toString(num22);
        }
        if (this._sMath.equals("Subtraction")) {
            return String.valueOf(Integer.toString(num12)) + " - " + Integer.toString(num22);
        }
        if (this._sMath.equals("Multiplication")) {
            return String.valueOf(Integer.toString(num12)) + " x " + Integer.toString(num22);
        }
        if (this._sMath.equals("Division")) {
            return String.valueOf(Integer.toString(num12)) + " / " + Integer.toString(num22);
        }
        return "";
    }

    public String getAnswer(int num12, int num22) {
        if (this._sMath.equals("Addition")) {
            return Integer.toString(num12 + num22);
        }
        if (this._sMath.equals("Subtraction")) {
            return Integer.toString(num12 - num22);
        }
        if (this._sMath.equals("Multiplication")) {
            return Integer.toString(num12 * num22);
        }
        if (this._sMath.equals("Division")) {
            return Integer.toString(num12 / num22);
        }
        return "";
    }

    public String getAnswerLength(int num12, int num22) {
        if (this._sMath.equals("Addition")) {
            return Integer.toString(Integer.toString(num12 + num22).length());
        }
        if (this._sMath.equals("Subtraction")) {
            return Integer.toString(Integer.toString(num12 - num22).length());
        }
        if (this._sMath.equals("Multiplication")) {
            return Integer.toString(Integer.toString(num12 * num22).length());
        }
        if (this._sMath.equals("Division")) {
            return Integer.toString(Integer.toString(num12 / num22).length());
        }
        return "";
    }

    public void get_Random_sMath(int random) {
        if (random == 0) {
            this._sMath = "Addition";
        } else if (random == 1) {
            this._sMath = "Subtraction";
        } else if (random == 2) {
            this._sMath = "Multiplication";
        } else if (random == 3) {
            this._sMath = "Division";
        }
    }

    public void d_fillNumbers(int level, int ROWS) {
        for (int i = 2; i < (ROWS + 1) * level; i++) {
            int j = 2;
            while (true) {
                if (j >= (ROWS + 1) * level) {
                    break;
                }
                if (i * j < (ROWS + 1) * level && i * j > j && (i * j) % j == 0) {
                    this.d_numbers3.add(Integer.valueOf(this.num3));
                    this.num3++;
                    this.d_numbers1.add(Integer.valueOf(i * j));
                    this.d_numbers2.add(Integer.valueOf(j));
                    if (level == 1 && this.num3 == 8) {
                        this.d_numbers1.add(1);
                        this.d_numbers2.add(1);
                        this.d_numbers3.add(8);
                        this.d_numbers1.add(5);
                        this.d_numbers2.add(1);
                        this.d_numbers3.add(9);
                        break;
                    }
                }
                j++;
            }
            if (level == 1 && this.num3 == 8) {
                break;
            }
        }
        Collections.shuffle(this.d_numbers3);
    }

    public void m_fillNumbers(int level, int ROWS) {
        for (int i = 0; i < level * ROWS; i++) {
            this.m_numbers1.add(Integer.valueOf(i + 1));
        }
        if (level <= 5) {
            for (int i2 = 0; i2 < ROWS * 1; i2++) {
                this.m_numbers2.add(Integer.valueOf(i2 + 1));
            }
        } else {
            for (int i3 = 0; i3 < (level - 5) * ROWS; i3++) {
                this.m_numbers2.add(Integer.valueOf(i3 + 1));
            }
        }
        Collections.shuffle(this.m_numbers1);
        Collections.shuffle(this.m_numbers2);
    }

    public void s_fillNumbers(int level, int ROWS) {
        for (int i = ((level - 1) * ROWS) + 0; i < ((level - 1) * ROWS) + ROWS; i++) {
            this.s_numbers1.add(Integer.valueOf(i + 1));
        }
        for (int i2 = 0; i2 < level * ROWS; i2++) {
            this.s_numbers2.add(Integer.valueOf(i2 + 1));
        }
        Collections.shuffle(this.s_numbers1);
        Collections.shuffle(this.s_numbers2);
    }

    public void a_fillNumbers(int level, int ROWS) {
        if (level <= 2) {
            for (int i = ((level - 1) * ROWS) + 0; i < ((level - 1) * ROWS) + ROWS; i++) {
                this.a_numbers1.add(Integer.valueOf(i + 1));
                this.a_numbers2.add(Integer.valueOf(i + 1));
            }
        } else {
            for (int i2 = ((level - 2) * ROWS) + 0; i2 < ((level - 1) * ROWS) + ROWS; i2++) {
                this.a_numbers1.add(Integer.valueOf(i2 + 1));
                this.a_numbers2.add(Integer.valueOf(i2 + 1));
            }
        }
        Collections.shuffle(this.a_numbers1);
        Collections.shuffle(this.a_numbers2);
    }
}
