package com.maths;

import java.lang.reflect.Array;

public class TTQuestions {
    public String[][] get(int num1) {
        String[][] ary1 = (String[][]) Array.newInstance(String.class, 10, 3);
        for (int i = 0; i < 10; i++) {
            ary1[i][0] = getQuestion(num1, i + 1);
            ary1[i][1] = getAnswer(num1, i + 1);
            ary1[i][2] = getAnswerLength(num1, i + 1);
        }
        return ary1;
    }

    public String getQuestion(int num1, int num2) {
        return String.valueOf(Integer.toString(num1)) + " x " + Integer.toString(num2);
    }

    public String getAnswer(int num1, int num2) {
        return Integer.toString(num1 * num2);
    }

    public String getAnswerLength(int num1, int num2) {
        return Integer.toString(Integer.toString(num1 * num2).length());
    }
}
