package com.maths;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class FormulaQuestion {
    ArrayList<Integer> Seq = new ArrayList<>();
    int a = 0;
    int answer = 0;
    int b = 0;
    int c = 0;
    int d = 0;
    String question = "";
    Random rand = new Random();

    public String[][] get() {
        String[][] ary1 = (String[][]) Array.newInstance(String.class, 1, 7);
        ary1[0][0] = getQuestion(this.rand.nextInt(9));
        ary1[0][1] = getAnswer();
        ary1[0][2] = getAnswerLength();
        this.Seq.add(0, Integer.valueOf(this.a));
        this.Seq.add(1, Integer.valueOf(this.b));
        this.Seq.add(2, Integer.valueOf(this.c));
        this.Seq.add(3, Integer.valueOf(this.d));
        Collections.shuffle(this.Seq);
        ary1[0][3] = Integer.toString(this.Seq.get(0).intValue());
        ary1[0][4] = Integer.toString(this.Seq.get(1).intValue());
        ary1[0][5] = Integer.toString(this.Seq.get(2).intValue());
        ary1[0][6] = Integer.toString(this.Seq.get(3).intValue());
        return ary1;
    }

    public String getQuestion(int seq) {
        if (seq == 0) {
            QA0();
        }
        if (seq == 1) {
            QA1();
        }
        if (seq == 2) {
            QA2();
        }
        if (seq == 3) {
            QA3();
        }
        if (seq == 4) {
            QA4();
        }
        if (seq == 5) {
            QA5();
        }
        if (seq == 6) {
            QA6();
        }
        if (seq == 7) {
            QA7();
        }
        if (seq == 8) {
            QA8();
        }
        if (seq == 9) {
            QA9();
        }
        return this.question;
    }

    public String getAnswer() {
        return Integer.toString(this.answer);
    }

    public String getAnswerLength() {
        return Integer.toString(Integer.toString(this.answer).length());
    }

    private void getRandom() {
        this.a = this.rand.nextInt(100 - 1) + 1;
        this.b = this.rand.nextInt(100 - 1) + 1;
        this.c = this.rand.nextInt(100 - 1) + 1;
        this.d = this.rand.nextInt(100 - 1) + 1;
    }

    private void QA0() {
        do {
            getRandom();
            this.answer = this.a + this.b + this.c + this.d;
            this.question = "(" + this.a + " + " + this.b + ") + (" + this.c + " + " + this.d + ")";
        } while (this.answer <= 1);
    }

    private void QA1() {
        do {
            getRandom();
            this.answer = this.a + this.b + (this.c - this.d);
            this.question = "(" + this.a + " + " + this.b + ") + (" + this.c + " - " + this.d + ")";
        } while (this.answer <= 1);
    }

    private void QA2() {
        do {
            getRandom();
            this.answer = (this.a + this.b) - (this.c - this.d);
            this.question = "(" + this.a + " + " + this.b + ") - (" + this.c + " - " + this.d + ")";
        } while (this.answer <= 1);
    }

    private void QA3() {
        do {
            getRandom();
            this.answer = (this.a - this.b) - (this.c - this.d);
            this.question = "(" + this.a + " - " + this.b + ") - (" + this.c + " - " + this.d + ")";
        } while (this.answer <= 1);
    }

    private void QA4() {
        do {
            getRandom();
            this.answer = (this.a + this.b) - (this.c + this.d);
            this.question = "(" + this.a + " + " + this.b + ") - (" + this.c + " + " + this.d + ")";
        } while (this.answer <= 1);
    }

    private void QA5() {
        do {
            getRandom();
            this.answer = (this.a - this.b) + this.c + this.d;
            this.question = "(" + this.a + " - " + this.b + ") + (" + this.c + " + " + this.d + ")";
        } while (this.answer <= 1);
    }

    private void QA6() {
        do {
            getRandom();
            this.answer = (this.a + this.b) - (this.c - this.d);
            this.question = "(" + this.a + " + " + this.b + ") - (" + this.c + " - " + this.d + ")";
        } while (this.answer <= 1);
    }

    private void QA7() {
        do {
            getRandom();
            this.answer = (this.a - this.b) + (this.c - this.d);
            this.question = "(" + this.a + " - " + this.b + ") + (" + this.c + " - " + this.d + ")";
        } while (this.answer <= 1);
    }

    private void QA8() {
        do {
            getRandom();
            this.answer = (this.a - this.b) - (this.c - this.d);
            this.question = "(" + this.a + " - " + this.b + ") - (" + this.c + " - " + this.d + ")";
        } while (this.answer <= 1);
    }

    private void QA9() {
        do {
            getRandom();
            this.answer = this.a + this.b + (this.c - this.d);
            this.question = "(" + this.a + " + " + this.b + ") + (" + this.c + " - " + this.d + ")";
        } while (this.answer <= 1);
    }
}
