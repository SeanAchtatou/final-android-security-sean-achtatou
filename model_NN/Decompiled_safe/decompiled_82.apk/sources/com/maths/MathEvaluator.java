package com.maths;

import java.util.HashMap;

public class MathEvaluator {
    protected static Operator[] operators = null;
    private String expression = null;
    private Node node = null;
    private HashMap variables = new HashMap();

    public static void main(String[] args) {
        if (args == null || args.length != 1) {
            System.err.println("Math Expression Evaluator by The-Son LAI Lts@writeme.com C(2001):");
            System.err.println("Usage: java MathEvaluator.main [your math expression]");
            System.exit(0);
        }
        try {
            System.out.println(new MathEvaluator(args[0]).getValue());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public MathEvaluator() {
        init();
    }

    public MathEvaluator(String s) {
        init();
        setExpression(s);
    }

    private void init() {
        if (operators == null) {
            initializeOperators();
        }
    }

    public void addVariable(String v, double val) {
        addVariable(v, new Double(val));
    }

    public void addVariable(String v, Double val) {
        this.variables.put(v, val);
    }

    public void setExpression(String s) {
        this.expression = s;
    }

    public void reset() {
        this.node = null;
        this.expression = null;
        this.variables = new HashMap();
    }

    public void trace() {
        try {
            this.node = new Node(this.expression);
            this.node.trace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Double getValue() {
        if (this.expression == null) {
            return null;
        }
        try {
            this.node = new Node(this.expression);
            return evaluate(this.node);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static Double evaluate(Node n) {
        if (n.hasOperator() && n.hasChild()) {
            if (n.getOperator().getType() == 1) {
                n.setValue(evaluateExpression(n.getOperator(), evaluate(n.getLeft()), null));
            } else if (n.getOperator().getType() == 2) {
                n.setValue(evaluateExpression(n.getOperator(), evaluate(n.getLeft()), evaluate(n.getRight())));
            }
        }
        return n.getValue();
    }

    private static Double evaluateExpression(Operator o, Double f1, Double f2) {
        String op = o.getOperator();
        if ("+".equals(op)) {
            return new Double(f1.doubleValue() + f2.doubleValue());
        }
        if ("-".equals(op)) {
            return new Double(f1.doubleValue() - f2.doubleValue());
        }
        if ("*".equals(op)) {
            return new Double(f1.doubleValue() * f2.doubleValue());
        }
        if ("/".equals(op)) {
            return new Double(f1.doubleValue() / f2.doubleValue());
        }
        if ("^".equals(op)) {
            return new Double(Math.pow(f1.doubleValue(), f2.doubleValue()));
        }
        if ("%".equals(op)) {
            return new Double(f1.doubleValue() % f2.doubleValue());
        }
        if ("&".equals(op)) {
            return new Double(f1.doubleValue() + f2.doubleValue());
        }
        if ("|".equals(op)) {
            return new Double(f1.doubleValue() + f2.doubleValue());
        }
        if ("cos".equals(op)) {
            return new Double(Math.cos(f1.doubleValue()));
        }
        if ("sin".equals(op)) {
            return new Double(Math.sin(f1.doubleValue()));
        }
        if ("tan".equals(op)) {
            return new Double(Math.tan(f1.doubleValue()));
        }
        if ("acos".equals(op)) {
            return new Double(Math.acos(f1.doubleValue()));
        }
        if ("asin".equals(op)) {
            return new Double(Math.asin(f1.doubleValue()));
        }
        if ("atan".equals(op)) {
            return new Double(Math.atan(f1.doubleValue()));
        }
        if ("sqr".equals(op)) {
            return new Double(f1.doubleValue() * f1.doubleValue());
        }
        if ("sqrt".equals(op)) {
            return new Double(Math.sqrt(f1.doubleValue()));
        }
        if ("log".equals(op)) {
            return new Double(Math.log(f1.doubleValue()));
        }
        if ("min".equals(op)) {
            return new Double(Math.min(f1.doubleValue(), f2.doubleValue()));
        }
        if ("max".equals(op)) {
            return new Double(Math.max(f1.doubleValue(), f2.doubleValue()));
        }
        if ("exp".equals(op)) {
            return new Double(Math.exp(f1.doubleValue()));
        }
        if ("floor".equals(op)) {
            return new Double(Math.floor(f1.doubleValue()));
        }
        if ("ceil".equals(op)) {
            return new Double(Math.ceil(f1.doubleValue()));
        }
        if ("abs".equals(op)) {
            return new Double(Math.abs(f1.doubleValue()));
        }
        if ("neg".equals(op)) {
            return new Double(-f1.doubleValue());
        }
        if ("rnd".equals(op)) {
            return new Double(Math.random() * f1.doubleValue());
        }
        return null;
    }

    private void initializeOperators() {
        operators = new Operator[25];
        operators[0] = new Operator("+", 2, 0);
        operators[1] = new Operator("-", 2, 0);
        operators[2] = new Operator("*", 2, 10);
        operators[3] = new Operator("/", 2, 10);
        operators[4] = new Operator("^", 2, 10);
        operators[5] = new Operator("%", 2, 10);
        operators[6] = new Operator("&", 2, 0);
        operators[7] = new Operator("|", 2, 0);
        operators[8] = new Operator("cos", 1, 20);
        operators[9] = new Operator("sin", 1, 20);
        operators[10] = new Operator("tan", 1, 20);
        operators[11] = new Operator("acos", 1, 20);
        operators[12] = new Operator("asin", 1, 20);
        operators[13] = new Operator("atan", 1, 20);
        operators[14] = new Operator("sqrt", 1, 20);
        operators[15] = new Operator("sqr", 1, 20);
        operators[16] = new Operator("log", 1, 20);
        operators[17] = new Operator("min", 2, 0);
        operators[18] = new Operator("max", 2, 0);
        operators[19] = new Operator("exp", 1, 20);
        operators[20] = new Operator("floor", 1, 20);
        operators[21] = new Operator("ceil", 1, 20);
        operators[22] = new Operator("abs", 1, 20);
        operators[23] = new Operator("neg", 1, 20);
        operators[24] = new Operator("rnd", 1, 20);
    }

    public Double getVariable(String s) {
        return (Double) this.variables.get(s);
    }

    /* access modifiers changed from: private */
    public Double getDouble(String s) {
        if (s == null) {
            return null;
        }
        try {
            return new Double(Double.parseDouble(s));
        } catch (Exception e) {
            return getVariable(s);
        }
    }

    /* access modifiers changed from: protected */
    public Operator[] getOperators() {
        return operators;
    }

    protected class Operator {
        private String op;
        private int priority;
        private int type;

        public Operator(String o, int t, int p) {
            this.op = o;
            this.type = t;
            this.priority = p;
        }

        public String getOperator() {
            return this.op;
        }

        public void setOperator(String o) {
            this.op = o;
        }

        public int getType() {
            return this.type;
        }

        public int getPriority() {
            return this.priority;
        }
    }

    protected class Node {
        public Node nLeft = null;
        public int nLevel = 0;
        public Operator nOperator = null;
        public Node nParent = null;
        public Node nRight = null;
        public String nString = null;
        public Double nValue = null;

        public Node(String s) throws Exception {
            init(null, s, 0);
        }

        public Node(Node parent, String s, int level) throws Exception {
            init(parent, s, level);
        }

        private void init(Node parent, String s, int level) throws Exception {
            Operator o;
            String s2 = addZero(removeBrackets(removeIllegalCharacters(s)));
            if (checkBrackets(s2) != 0) {
                throw new Exception("Wrong number of brackets in [" + s2 + "]");
            }
            this.nParent = parent;
            this.nString = s2;
            this.nValue = MathEvaluator.this.getDouble(s2);
            this.nLevel = level;
            int sLength = s2.length();
            int inBrackets = 0;
            int startOperator = 0;
            for (int i = 0; i < sLength; i++) {
                if (s2.charAt(i) == '(') {
                    inBrackets++;
                } else if (s2.charAt(i) == ')') {
                    inBrackets--;
                } else if (inBrackets == 0 && (o = getOperator(this.nString, i)) != null && (this.nOperator == null || this.nOperator.getPriority() >= o.getPriority())) {
                    this.nOperator = o;
                    startOperator = i;
                }
            }
            if (this.nOperator == null) {
                return;
            }
            if (startOperator == 0 && this.nOperator.getType() == 1) {
                if (checkBrackets(s2.substring(this.nOperator.getOperator().length())) == 0) {
                    this.nLeft = new Node(this, s2.substring(this.nOperator.getOperator().length()), this.nLevel + 1);
                    this.nRight = null;
                    return;
                }
                throw new Exception("Error during parsing... missing brackets in [" + s2 + "]");
            } else if (startOperator > 0 && this.nOperator.getType() == 2) {
                this.nLeft = new Node(this, s2.substring(0, startOperator), this.nLevel + 1);
                this.nRight = new Node(this, s2.substring(this.nOperator.getOperator().length() + startOperator), this.nLevel + 1);
            }
        }

        private Operator getOperator(String s, int start) {
            Operator[] operators = MathEvaluator.this.getOperators();
            String temp = getNextWord(s.substring(start));
            for (int i = 0; i < operators.length; i++) {
                if (temp.startsWith(operators[i].getOperator())) {
                    return operators[i];
                }
            }
            return null;
        }

        private String getNextWord(String s) {
            int sLength = s.length();
            for (int i = 1; i < sLength; i++) {
                char c = s.charAt(i);
                if ((c > 'z' || c < 'a') && (c > '9' || c < '0')) {
                    return s.substring(0, i);
                }
            }
            return s;
        }

        /* access modifiers changed from: protected */
        public int checkBrackets(String s) {
            int sLength = s.length();
            int inBracket = 0;
            for (int i = 0; i < sLength; i++) {
                if (s.charAt(i) == '(' && inBracket >= 0) {
                    inBracket++;
                } else if (s.charAt(i) == ')') {
                    inBracket--;
                }
            }
            return inBracket;
        }

        /* access modifiers changed from: protected */
        public String addZero(String s) {
            if (s.startsWith("+") || s.startsWith("-")) {
                int sLength = s.length();
                for (int i = 0; i < sLength; i++) {
                    if (getOperator(s, i) != null) {
                        return "0" + s;
                    }
                }
            }
            return s;
        }

        public void trace() {
            _D(String.valueOf(getOperator() == null ? " " : getOperator().getOperator()) + " : " + getString());
            if (hasChild()) {
                if (hasLeft()) {
                    getLeft().trace();
                }
                if (hasRight()) {
                    getRight().trace();
                }
            }
        }

        /* access modifiers changed from: protected */
        public boolean hasChild() {
            return (this.nLeft == null && this.nRight == null) ? false : true;
        }

        /* access modifiers changed from: protected */
        public boolean hasOperator() {
            return this.nOperator != null;
        }

        /* access modifiers changed from: protected */
        public boolean hasLeft() {
            return this.nLeft != null;
        }

        /* access modifiers changed from: protected */
        public Node getLeft() {
            return this.nLeft;
        }

        /* access modifiers changed from: protected */
        public boolean hasRight() {
            return this.nRight != null;
        }

        /* access modifiers changed from: protected */
        public Node getRight() {
            return this.nRight;
        }

        /* access modifiers changed from: protected */
        public Operator getOperator() {
            return this.nOperator;
        }

        /* access modifiers changed from: protected */
        public int getLevel() {
            return this.nLevel;
        }

        /* access modifiers changed from: protected */
        public Double getValue() {
            return this.nValue;
        }

        /* access modifiers changed from: protected */
        public void setValue(Double f) {
            this.nValue = f;
        }

        /* access modifiers changed from: protected */
        public String getString() {
            return this.nString;
        }

        public String removeBrackets(String s) {
            String res = s;
            if (s.length() > 2 && res.startsWith("(") && res.endsWith(")") && checkBrackets(s.substring(1, s.length() - 1)) == 0) {
                res = res.substring(1, res.length() - 1);
            }
            if (res != s) {
                return removeBrackets(res);
            }
            return res;
        }

        public String removeIllegalCharacters(String s) {
            char[] illegalCharacters = {' '};
            String res = s;
            for (int j = 0; j < illegalCharacters.length; j++) {
                int i = res.lastIndexOf(illegalCharacters[j], res.length());
                while (i != -1) {
                    String temp = res;
                    res = String.valueOf(temp.substring(0, i)) + temp.substring(i + 1);
                    i = res.lastIndexOf(illegalCharacters[j], s.length());
                }
            }
            return res;
        }

        /* access modifiers changed from: protected */
        public void _D(String s) {
            String nbSpaces = "";
            for (int i = 0; i < this.nLevel; i++) {
                nbSpaces = String.valueOf(nbSpaces) + "  ";
            }
            System.out.println(String.valueOf(nbSpaces) + "|" + s);
        }
    }

    protected static void _D(String s) {
        System.err.println(s);
    }
}
