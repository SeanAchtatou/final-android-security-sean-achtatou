package com.maths;

public class StopWatch {
    private boolean running = false;
    private long startTime = 0;
    private long stopTime = 0;

    public void start() {
        this.startTime = System.currentTimeMillis();
        this.running = true;
    }

    public void stop() {
        this.stopTime = System.currentTimeMillis();
        this.running = false;
    }

    public long getElapsedTime() {
        if (this.running) {
            return (System.currentTimeMillis() - this.startTime) / 1000;
        }
        return (this.stopTime - this.startTime) / 1000;
    }
}
