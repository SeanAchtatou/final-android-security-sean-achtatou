package com.maths;

import android.app.Application;

public class MyVar extends Application {
    private int COLS = 3;
    private int ROWS = 10;
    private int iLevel = 1;
    private int iVar1 = 0;
    private int iVar2 = 0;
    private String sMath = "Addition";
    private String sVar1 = "1";
    private String sVar2 = "2";

    public int get_ROWS(int level) {
        return this.ROWS;
    }

    public int get_COLS() {
        return this.COLS;
    }

    public int get_iLevel() {
        return this.iLevel;
    }

    public void set_iLevel(int iLevel2) {
        this.iLevel = iLevel2;
    }

    public String get_sMath() {
        return this.sMath;
    }

    public void set_sMath(String sMath2) {
        this.sMath = sMath2;
    }

    public String get_sVar1() {
        return this.sVar1;
    }

    public void set_sVar1(String sVar12) {
        this.sVar1 = sVar12;
    }

    public String get_sVar2() {
        return this.sVar2;
    }

    public void set_sVar2(String sVar22) {
        this.sVar2 = sVar22;
    }

    public int get_iVar1() {
        return this.iVar1;
    }

    public void set_iVar1(int iVar12) {
        this.iVar1 = iVar12;
    }

    public int get_iVar2() {
        return this.iVar2;
    }

    public void set_iVar2(int iVar22) {
        this.iVar2 = iVar22;
    }

    public void onCreate() {
    }
}
