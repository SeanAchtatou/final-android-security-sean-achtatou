package pop.em.up;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import pop.em.up.engines.LevelEngine;

public class BalloonPopperActivity extends Activity {
    GameEngine mEngine;
    private boolean mMenuItemSelected = false;

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.mEngine.mSettings.end(this);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.mEngine.mSettings.pause(true);
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.mEngine.mSettings.pause(false);
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        this.mEngine.mSettings.pause(false);
        this.mEngine.mSettings.mStop = false;
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        this.mEngine.mSettings.pause(true);
        this.mEngine.mSettings.mStop = true;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        this.mEngine = ((BalloonPopperApp) getApplication()).mEngine;
        this.mEngine.startActivity(this);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || event.getRepeatCount() != 0) {
            return false;
        }
        onBackPressed();
        return false;
    }

    public void onBackPressed() {
        if (this.mEngine.mSettings == null || this.mEngine.mSettings.mEngine.onBack(this.mEngine.mSettings)) {
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.levelenginemenu, menu);
        return true;
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 0:
                final Dialog dialog = new Dialog(this);
                dialog.setContentView((int) R.layout.leveldialogue);
                dialog.setTitle("Level Picker");
                final EditText txt = (EditText) dialog.findViewById(R.id.editText1);
                ((Button) dialog.findViewById(R.id.button1)).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        try {
                            int a = Integer.parseInt(txt.getText().toString()) - 1;
                            LevelEngine el = (LevelEngine) BalloonPopperActivity.this.mEngine.mSettings.mEngine;
                            if (a > el.mMax) {
                                Toast.makeText(BalloonPopperActivity.this, "Too high an input.", 3);
                            } else if (a < 0) {
                                Toast.makeText(BalloonPopperActivity.this, "Too low an input.", 3);
                            } else {
                                el.startLevel(a, BalloonPopperActivity.this.mEngine.mSettings, true);
                                dialog.dismiss();
                                BalloonPopperActivity.this.mEngine.mSettings.pause(false);
                            }
                        } catch (Exception e) {
                            Toast.makeText(BalloonPopperActivity.this, "Only integers are accepted.", 3);
                        }
                    }
                });
                ((Button) dialog.findViewById(R.id.button2)).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        BalloonPopperActivity.this.mEngine.mSettings.pause(false);
                        dialog.dismiss();
                    }
                });
                return dialog;
            default:
                return null;
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    /* access modifiers changed from: protected */
    public void onPrepareDialog(int id, Dialog dialog) {
        super.onPrepareDialog(id, dialog);
        switch (id) {
            case 0:
                ((TextView) dialog.findViewById(R.id.leveldialoguetv)).setText("Pick a level between 1-" + (((LevelEngine) this.mEngine.mSettings.mEngine).mMax + 1));
                return;
            default:
                return;
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        this.mMenuItemSelected = true;
        switch (item.getItemId()) {
            case R.id.restart /*2131099655*/:
                if (this.mEngine.mSettings.mEngine instanceof LevelEngine) {
                    this.mEngine.mSettings.pause(false);
                    ((LevelEngine) this.mEngine.mSettings.mEngine).startLevel(((LevelEngine) this.mEngine.mSettings.mEngine).mLevel, this.mEngine.mSettings, true);
                    break;
                }
                break;
            case R.id.picklevel /*2131099656*/:
                if (this.mEngine.mSettings.mEngine instanceof LevelEngine) {
                    showDialog(0);
                    break;
                }
                break;
        }
        return true;
    }

    public void onOptionsMenuClosed(Menu menu) {
        super.onOptionsMenuClosed(menu);
        if (!this.mMenuItemSelected) {
            this.mEngine.mSettings.pause(false);
        }
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        this.mEngine.mSettings.pause(true);
        return super.onPrepareOptionsMenu(menu);
    }
}
