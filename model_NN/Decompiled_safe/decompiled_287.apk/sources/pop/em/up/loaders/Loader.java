package pop.em.up.loaders;

import android.content.Context;
import android.graphics.Canvas;
import pop.em.up.GameSettings;
import pop.em.up.LoadTask;
import pop.em.up.basic.LinkedList;

public abstract class Loader {
    protected boolean mFinished = false;
    public LinkedList<LoadTask> mTasks = new LinkedList<>();

    public abstract void drawCanvas(Canvas canvas);

    public abstract void loadEngine(float f, float f2, Context context);

    public abstract void start();

    public void load(float w, float h, Context c, GameSettings s) {
        loadEngine(w, h, c);
        this.mTasks.reset();
        while (this.mTasks.hasNext()) {
            LoadTask t = this.mTasks.getCursor();
            if (!t.isLoaded()) {
                t.load(c);
            }
            this.mTasks.next();
        }
        this.mTasks.reset();
        while (this.mTasks.hasNext()) {
            this.mTasks.getCursor().finished(s);
            this.mTasks.next();
        }
        this.mFinished = true;
    }

    public boolean isFinished() {
        return this.mFinished;
    }
}
