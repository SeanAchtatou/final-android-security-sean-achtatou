package pop.em.up.loaders;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import pop.em.up.GameEngine;
import pop.em.up.R;
import pop.em.up.StringGetter;
import pop.em.up.TextMeasurer;

public class AnimLoader extends Loader {
    boolean forward = true;
    Bitmap[] mAnim = new Bitmap[11];
    private int mCursor = 0;
    RectF mDst;
    GameEngine mE;
    int mEndLoop = 10;
    float mOffset = 0.0f;
    float mScale = 2.0f;
    int mStartLoop = 0;
    RectF mUpper;
    Paint textPaint = new Paint();
    TextMeasurer tm;

    public AnimLoader(GameEngine ge) {
        this.textPaint.setAntiAlias(true);
        this.textPaint.setTextAlign(Paint.Align.CENTER);
        this.textPaint.setColor(-1);
        this.mE = ge;
    }

    public void loadEngine(float width, float height, Context c) {
        this.mAnim[0] = BitmapFactory.decodeResource(c.getResources(), R.drawable.dragonanim1);
        float w = ((float) this.mAnim[0].getWidth()) * this.mScale;
        float h = ((float) this.mAnim[0].getHeight()) * this.mScale;
        this.mUpper = new RectF(-5.0f, -5.0f, width + 5.0f, height + 5.0f);
        this.mDst = new RectF((width / 2.0f) - (w / 2.0f), 30.0f, (width / 2.0f) + (w / 2.0f), 30.0f + h);
        RectF txtrct = new RectF();
        txtrct.top = this.mDst.bottom + 5.0f;
        txtrct.bottom = this.mUpper.bottom - 10.0f;
        txtrct.left = this.mDst.left;
        txtrct.right = this.mDst.right;
        this.tm = new TextMeasurer(new StringGetter() {
            public String getString() {
                return !AnimLoader.this.mFinished ? "Loading..." : "Loaded!";
            }

            public String getFinalString() {
                return "Loading...";
            }
        }, this.textPaint, txtrct);
        this.mAnim[1] = BitmapFactory.decodeResource(c.getResources(), R.drawable.dragonanim2);
        this.mAnim[2] = BitmapFactory.decodeResource(c.getResources(), R.drawable.dragonanim3);
        this.mAnim[3] = BitmapFactory.decodeResource(c.getResources(), R.drawable.dragonanim4);
        this.mAnim[4] = BitmapFactory.decodeResource(c.getResources(), R.drawable.dragonanim5);
        this.mAnim[5] = BitmapFactory.decodeResource(c.getResources(), R.drawable.dragonanim6);
        this.mAnim[6] = BitmapFactory.decodeResource(c.getResources(), R.drawable.dragonanim7);
        this.mAnim[7] = BitmapFactory.decodeResource(c.getResources(), R.drawable.dragonanim8);
        this.mAnim[8] = BitmapFactory.decodeResource(c.getResources(), R.drawable.dragonanim9);
        this.mAnim[9] = BitmapFactory.decodeResource(c.getResources(), R.drawable.dragonanim10);
        this.mAnim[10] = BitmapFactory.decodeResource(c.getResources(), R.drawable.dragonanim11);
    }

    public void start() {
        this.mE.startEngineThread();
        this.mE.mSettings.mPaused = false;
        this.mE.mLoader = null;
    }

    public void drawCanvas(Canvas c) {
        boolean z;
        boolean z2;
        if (this.mFinished) {
            this.mE.mSettings.drawCanvas(c);
            this.mOffset -= 5.0f;
            this.mDst.offset(0.0f, -5.0f);
            this.mUpper.offset(0.0f, -5.0f);
            c.drawRect(this.mUpper, this.mE.mSettings.BGColor);
            c.drawRect(this.mUpper, this.mE.mSettings.BGOutline);
            if (this.mUpper.bottom <= 0.0f) {
                start();
            }
        } else {
            c.drawARGB(255, 0, 0, 0);
        }
        if (this.tm != null) {
            this.tm.paintText(c, this.textPaint, 0.0f, this.mOffset);
        }
        if (this.mAnim[this.mCursor] != null) {
            c.drawBitmap(this.mAnim[this.mCursor], (Rect) null, this.mDst, (Paint) null);
        }
        if (this.forward) {
            if (this.mCursor == this.mEndLoop) {
                if (this.forward) {
                    z2 = false;
                } else {
                    z2 = true;
                }
                this.forward = z2;
                this.mCursor--;
            } else if (this.mAnim.length - 1 > this.mCursor && this.mAnim[this.mCursor + 1] != null) {
                this.mCursor++;
            }
        } else if (this.mCursor == this.mStartLoop) {
            if (this.forward) {
                z = false;
            } else {
                z = true;
            }
            this.forward = z;
            this.mCursor++;
        } else {
            this.mCursor--;
        }
    }
}
