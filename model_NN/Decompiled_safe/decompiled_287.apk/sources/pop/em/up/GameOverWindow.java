package pop.em.up;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import pop.em.up.abstracts.CanvasButton;
import pop.em.up.engines.LevelEngine;

public class GameOverWindow extends PopupWindow {
    RectF mDst = new RectF();
    LevelEngine mEng;
    CanvasButton mNo;
    TextMeasurer mRow1;
    CanvasButton mYes;
    /* access modifiers changed from: private */
    public boolean yes;

    public GameOverWindow(GameSettings gms, LevelEngine e, Paint mPnt) {
        super(gms);
        gms.destroyOnScreen();
        this.mWidth = 0.7f;
        this.mHeight = 0.5f;
        this.mEng = e;
        RectF r = new RectF();
        r.left = (gms.mWidth / 2.0f) - ((gms.mWidth * this.mWidth) / 2.0f);
        r.right = (gms.mWidth / 2.0f) + ((gms.mWidth * this.mWidth) / 2.0f);
        r.top = (gms.mHeight / 2.0f) - ((gms.mHeight * this.mHeight) / 2.0f);
        r.bottom = (gms.mHeight / 2.0f) + ((gms.mHeight * this.mHeight) / 2.0f);
        float h = r.height();
        this.mDst.left = r.left;
        this.mDst.right = r.right;
        this.mDst.top = r.top;
        r.top += h * 0.5f;
        this.mDst.bottom = r.top;
        r.top += 0.3f * h;
        r.bottom -= h * 0.2f;
        RectF mButton1 = new RectF();
        mButton1.left = r.left;
        mButton1.right = r.centerX();
        mButton1.top = r.bottom;
        mButton1.bottom = r.bottom + (h * 0.15f);
        RectF mButton2 = new RectF();
        mButton2.left = r.centerX();
        mButton2.right = r.right;
        mButton2.top = r.bottom;
        mButton2.bottom = r.bottom + (h * 0.15f);
        r.bottom -= h * 0.2f;
        this.mRow1 = new TextMeasurer(new StaticString("Try Again?"), mPnt, r);
        this.mYes = new CanvasButton(mButton1, "Yes", mPnt) {
            public boolean hit(float x, float y, GameSettings gms, int h) {
                if (!GameOverWindow.this.fullSize(gms) || !this.mRct.contains(x, y)) {
                    return false;
                }
                this.r = 255;
                this.g = 255;
                this.b = 255;
                GameOverWindow.this.mExpanding = false;
                this.mRemoved = true;
                GameOverWindow.this.mNo.mRemoved = true;
                GameOverWindow.this.yes = true;
                return true;
            }
        };
        this.mNo = new CanvasButton(mButton2, "No", mPnt) {
            public boolean hit(float x, float y, GameSettings gms, int h) {
                if (!GameOverWindow.this.fullSize(gms) || !this.mRct.contains(x, y)) {
                    return false;
                }
                this.r = 255;
                this.g = 255;
                this.b = 255;
                GameOverWindow.this.mExpanding = false;
                this.mRemoved = true;
                GameOverWindow.this.mYes.mRemoved = true;
                GameOverWindow.this.yes = false;
                return true;
            }
        };
    }

    public boolean draw(Canvas c, GameSettings gms, Paint mPnt) {
        super.draw(c, gms, mPnt);
        c.save();
        c.clipRect(this.mRct);
        c.drawBitmap(this.mEng.mGameover, (Rect) null, this.mDst, (Paint) null);
        mPnt.setStyle(Paint.Style.FILL);
        this.mRow1.paintText(c, mPnt);
        this.mYes.draw(c, mPnt);
        this.mNo.draw(c, mPnt);
        c.restore();
        return false;
    }

    public boolean tick(GameSettings gms) {
        super.tick(gms);
        return finished(gms);
    }

    public void onFinish(GameSettings gms) {
        if (this.yes) {
            gms.mRecord = true;
            gms.mStats.mScore = gms.mStats.mLastScore;
            this.mEng.startLevel(this.mEng.mLevel, gms, true);
            return;
        }
        gms.mRecord = true;
        gms.loadMenu();
    }

    public void deInit() {
    }

    public boolean hit(float x, float y, GameSettings gms) {
        if (this.mYes.hit(x, y, gms, 0) || this.mNo.hit(x, y, gms, 0)) {
            return true;
        }
        return false;
    }
}
