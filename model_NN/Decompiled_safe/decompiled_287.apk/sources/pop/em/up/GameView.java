package pop.em.up;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ImageView;

public class GameView extends ImageView {
    public GameEngine mEngine;

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        this.mEngine.mSettings.getClass();
        postInvalidateDelayed(33);
        this.mEngine.draw(canvas);
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (this.mEngine.mLoader != null) {
            return false;
        }
        this.mEngine.mSettings.checkDrag(event, event.getX(), event.getY());
        if (event.getAction() == 0) {
            final float x = event.getX();
            final float y = event.getY();
            new Thread(new Runnable() {
                public void run() {
                    GameView.this.mEngine.mSettings.checkHit(x, y);
                }
            }).start();
        }
        return true;
    }

    public GameView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public GameView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public GameView(Context context) {
        super(context);
    }
}
