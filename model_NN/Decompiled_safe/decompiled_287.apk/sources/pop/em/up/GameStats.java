package pop.em.up;

import pop.em.up.balloons.Balloon;
import pop.em.up.balloons.FloatingText;

public class GameStats {
    public long mCombo = 0;
    public long mCreated = 0;
    public long mDestroyed = 0;
    public long mHit = 0;
    public long mLastScore = 0;
    public long mLastTap = 0;
    public int mLives = 5;
    public int mPopped = 0;
    public long mScore = 0;
    public long mTap = 0;

    public boolean balloonsOnScreen() {
        return (this.mCreated - this.mDestroyed) - ((long) this.mPopped) != 0;
    }

    public void resetBetweenLevels() {
        this.mPopped = 0;
        this.mLastTap = 0;
        this.mTap = 0;
        this.mHit = 0;
        this.mDestroyed = 0;
        this.mCombo = 0;
        this.mCreated = 0;
        this.mLives = 5;
        this.mLastScore = this.mScore;
    }

    public void touch(GameSettings s) {
        this.mTap++;
    }

    public void miss(float x, float y, GameSettings s) {
        this.mLastTap = 0;
        this.mCombo = 0;
        FloatingText ft = new FloatingText("MISS", x, y, 20, 255, 0, 0);
        ft.mdA = 25.0f;
        ft.addSelf(s);
    }

    public void hit(float x, float y, GameSettings s) {
        this.mHit++;
        this.mCombo++;
        this.mLastTap = System.currentTimeMillis();
        FloatingText ft = new FloatingText(this.mCombo > 1 ? String.valueOf(this.mCombo) + " HIT Combo" : "HIT", x, y, 20, 0, 0, 0);
        ft.mdA = 15.0f;
        ft.addSelf(s);
    }

    public void pop(Balloon b, GameSettings s) {
        float f;
        float multiplier = (float) (this.mCombo / 10);
        float f2 = (float) this.mScore;
        float f3 = (float) b.mScore;
        if (multiplier > 1.0f) {
            f = multiplier;
        } else {
            f = 1.0f;
        }
        this.mScore = (long) (f2 + (f3 * f));
        this.mPopped++;
    }

    public void create(Balloon b, GameSettings s) {
        this.mCreated++;
    }

    public void remove(Balloon b, GameSettings s) {
        this.mDestroyed++;
        this.mLives -= b.killLives();
        if (this.mLives <= 0) {
            s.mEngine.GameOver(s);
        }
    }
}
