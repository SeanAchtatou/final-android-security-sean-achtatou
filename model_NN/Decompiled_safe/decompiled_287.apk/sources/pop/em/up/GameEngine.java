package pop.em.up;

import android.app.Activity;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import pop.em.up.balloons.Balloon;
import pop.em.up.engines.LevelEngine;
import pop.em.up.loaders.AnimLoader;
import pop.em.up.loaders.Loader;

public class GameEngine {
    public Thread mEngineThread;
    boolean mFirstStart = true;
    public Loader mLoader;
    public GameSettings mSettings;

    public void initialize() {
    }

    public void startActivity(final Activity a) {
        final GameView gv = (GameView) a.findViewById(R.id.gameView1);
        gv.mEngine = this;
        if (this.mFirstStart) {
            this.mFirstStart = false;
            this.mSettings = new GameSettings();
            this.mSettings.mPaused = true;
            this.mSettings.mAct = a;
            this.mSettings.mGV = gv;
            this.mLoader = new AnimLoader(this);
            this.mSettings.mEngine = new LevelEngine();
            this.mSettings.mEngine.load(a, this.mSettings, this.mLoader);
            Balloon.load(this.mSettings, this.mLoader);
            this.mLoader.mTasks.add(new LoadTask() {
                public void load(Context c) {
                    GameEngine.this.mSettings.mBG = BitmapFactory.decodeResource(c.getResources(), R.drawable.background);
                }

                public boolean isLoaded() {
                    return GameEngine.this.mSettings.mBG != null;
                }

                public void finished(GameSettings s) {
                }
            });
            gv.post(new Runnable() {
                public void run() {
                    gv.setKeepScreenOn(true);
                    GameEngine.this.mSettings.setupBounds((float) gv.getWidth(), (float) gv.getHeight(), a);
                    final GameView gameView = gv;
                    new Thread(new Runnable() {
                        public void run() {
                            GameEngine.this.mSettings.mEngine.setupInterface(GameEngine.this.mSettings.mDock, GameEngine.this.mSettings);
                            GameEngine.this.mLoader.load((float) gameView.getWidth(), (float) gameView.getHeight(), gameView.getContext(), GameEngine.this.mSettings);
                        }
                    }).start();
                }
            });
        } else if (this.mEngineThread == null || !this.mEngineThread.isAlive()) {
            startEngineThread();
        }
    }

    public void startEngineThread() {
        final GameSettings s = this.mSettings;
        this.mEngineThread = new Thread(new Runnable() {
            public void run() {
                while (!s.mEnded) {
                    try {
                        if (s.mStop) {
                            s.getClass();
                            Thread.sleep((long) (33 * 50));
                        }
                        if (!s.mPaused) {
                            s.getClass();
                            Thread.sleep(33);
                        } else {
                            s.getClass();
                            Thread.sleep((long) (33 * 10));
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if (!s.mPaused && !s.mEngine.tick(s)) {
                        s.tick();
                    }
                }
            }
        });
        this.mEngineThread.start();
    }

    public void draw(Canvas c) {
        if (this.mLoader != null) {
            this.mLoader.drawCanvas(c);
        } else {
            this.mSettings.drawCanvas(c);
        }
    }
}
