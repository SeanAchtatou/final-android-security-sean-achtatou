package pop.em.up;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import pop.em.up.engines.LevelEngine;
import pop.em.up.uiwidget.Statistics;

public class StatisticsWindow extends PopupWindow {
    boolean mA = false;
    boolean mB = false;
    boolean mC = false;
    boolean mD = false;
    LevelEngine mEng;
    TextMeasurer mHead;
    TextMeasurer mRow1;
    boolean mSkip = false;
    TextMeasurer[] mTxts;

    public StatisticsWindow(final GameSettings gms, LevelEngine e, Paint p) {
        super(gms);
        this.mWidth = 0.5f;
        this.mHeight = 0.7f;
        this.mEng = e;
        RectF r = new RectF();
        r.left = (gms.mWidth / 2.0f) - ((gms.mWidth * this.mWidth) / 2.0f);
        r.right = (gms.mWidth / 2.0f) + ((gms.mWidth * this.mWidth) / 2.0f);
        r.top = (gms.mHeight / 2.0f) - ((gms.mHeight * this.mHeight) / 2.0f);
        r.bottom = (gms.mHeight / 2.0f) + ((gms.mHeight * this.mHeight) / 2.0f);
        float h = r.height();
        RectF mDst = new RectF();
        mDst.left = r.left;
        mDst.right = r.right;
        mDst.top = r.top;
        r.top += 0.3f * h;
        mDst.bottom = r.top;
        this.mHead = new TextMeasurer(new StaticString("Statistics"), p, mDst);
        final float acc = getAcc(gms);
        this.mTxts = TextMeasurer.measureStrings(new StringGetter[]{new StringGetter() {
            private int i = 0;

            public String getString() {
                if (StatisticsWindow.this.mSkip) {
                    return getFinalString();
                }
                if (!StatisticsWindow.this.fullSize(gms)) {
                    return "";
                }
                if (this.i >= StatisticsWindow.this.mEng.mLevel + 1) {
                    StatisticsWindow.this.mA = true;
                    return getFinalString();
                }
                this.i++;
                return "Level:" + this.i;
            }

            public String getFinalString() {
                return "Level:" + (StatisticsWindow.this.mEng.mLevel + 1);
            }
        }, new StringGetter() {
            private float i = 0.0f;

            public String getString() {
                if (StatisticsWindow.this.mSkip) {
                    return getFinalString();
                }
                if (!StatisticsWindow.this.mA) {
                    return "";
                }
                if (this.i >= acc) {
                    StatisticsWindow.this.mB = true;
                    return getFinalString();
                }
                this.i = (float) (((double) this.i) + 3.14d);
                return "Accuracy:" + this.i;
            }

            public String getFinalString() {
                return "Accuracy:" + Statistics.df.format((double) acc);
            }
        }, new StringGetter(gms) {
            private int a = 0;
            private int b = 0;
            private int c = 0;
            private int dC;
            private final /* synthetic */ GameSettings val$gms;

            {
                this.val$gms = r4;
                this.dC = ((r4.mStats.mLives * (StatisticsWindow.this.mEng.mLevel + 1)) * 10) / 25;
            }

            public String getString() {
                if (StatisticsWindow.this.mSkip) {
                    return getFinalString();
                }
                if (!StatisticsWindow.this.mB) {
                    return "";
                }
                if (this.a < this.val$gms.mStats.mLives) {
                    this.a++;
                    return "Lives Bonus:" + this.a;
                } else if (this.b < StatisticsWindow.this.mEng.mLevel + 1) {
                    this.b += 2;
                    return "Lives Bonus:" + this.val$gms.mStats.mLives + "x" + this.b;
                } else if (this.c < this.val$gms.mStats.mLives * (StatisticsWindow.this.mEng.mLevel + 1) * 10) {
                    this.c += this.dC;
                    return "Lives Bonus:" + this.val$gms.mStats.mLives + "x" + (StatisticsWindow.this.mEng.mLevel + 1) + "x10=" + this.c;
                } else {
                    StatisticsWindow.this.mC = true;
                    return getFinalString();
                }
            }

            public String getFinalString() {
                return "Lives Bonus:" + this.val$gms.mStats.mLives + "x" + (StatisticsWindow.this.mEng.mLevel + 1) + "x10=" + (this.val$gms.mStats.mLives * (StatisticsWindow.this.mEng.mLevel + 1) * 10);
            }
        }, new StringGetter(gms) {
            private long i = 0;
            private final /* synthetic */ GameSettings val$gms;
            private long x;

            {
                this.val$gms = r6;
                this.x = r6.mStats.mScore / 25;
            }

            public String getString() {
                if (StatisticsWindow.this.mSkip) {
                    return getFinalString();
                }
                if (!StatisticsWindow.this.mC) {
                    return "";
                }
                if (this.i >= this.val$gms.mStats.mScore) {
                    StatisticsWindow.this.mD = true;
                    return getFinalString();
                }
                this.i += this.x;
                return "Score:" + this.i;
            }

            public String getFinalString() {
                return "Score:" + this.val$gms.mStats.mScore;
            }
        }, new StringGetter() {
            private int i = 0;

            public String getString() {
                if (StatisticsWindow.this.mSkip) {
                    return getFinalString();
                }
                if (!StatisticsWindow.this.mD) {
                    return "";
                }
                if (this.i > 135) {
                    StatisticsWindow.this.mSkip = true;
                    return getFinalString();
                }
                this.i += 3;
                return "Letter Grade:".substring(0, this.i / 10);
            }

            public String getFinalString() {
                return "Letter Grade:" + StatisticsWindow.this.getGradeStr(gms.mStats.mLives, acc);
            }
        }, new StringGetter() {
            public String getString() {
                if (StatisticsWindow.this.mSkip) {
                    return getFinalString();
                }
                return "";
            }

            public String getFinalString() {
                return "Touch screen to continue!";
            }
        }}, r, 5.0f, p);
    }

    public String getGradeStr(int lives, float accuracy) {
        float grade = ((accuracy / 100.0f) + (((float) lives) / 5.0f)) / 2.0f;
        if (grade >= 0.9f) {
            return "A";
        }
        if (((double) grade) >= 0.8d) {
            return "B";
        }
        if (((double) grade) >= 0.7d) {
            return "C";
        }
        return "D";
    }

    public float getAcc(GameSettings gms) {
        GameStats gs = gms.mStats;
        return 100.0f * ((float) (gs.mTap == 0 ? 0.0d : ((double) gs.mHit) / ((double) gs.mTap)));
    }

    public boolean draw(Canvas c, GameSettings gms, Paint mPnt) {
        super.draw(c, gms, mPnt);
        mPnt.setARGB(255, 255, 255, 255);
        c.save();
        c.clipRect(this.mRct);
        mPnt.setARGB(255, 255, 255, 255);
        mPnt.setStyle(Paint.Style.FILL);
        this.mHead.paintText(c, mPnt);
        for (TextMeasurer t : this.mTxts) {
            t.paintText(c, mPnt);
        }
        c.restore();
        return false;
    }

    public boolean tick(GameSettings gms) {
        super.tick(gms);
        return finished(gms);
    }

    public void onFinish(GameSettings gms) {
        if (this.mEng.mLevel >= this.mEng.mSet.length - 1) {
            gms.BGColor = gms.BGColor;
        } else {
            this.mEng.startLevel(this.mEng.mLevel + 1, gms, false);
        }
        gms.mRecord = true;
    }

    public boolean finished(GameSettings gms) {
        return closed(gms) && !this.mExpanding;
    }

    public boolean hit(float x, float y, GameSettings gms) {
        if (!fullSize(gms)) {
            return false;
        }
        if (!this.mSkip) {
            this.mSkip = true;
        } else {
            this.mExpanding = false;
        }
        return true;
    }

    public void deInit() {
    }
}
