package pop.em.up.abstracts;

import android.graphics.Canvas;
import android.graphics.RectF;
import pop.em.up.GameSettings;
import pop.em.up.uiwidget.UIWidget;

public abstract class Button extends UIWidget {
    protected RectF mBounds;

    public void draw(GameSettings gms, Canvas c) {
        c.drawRect(this.mBounds, gms.BGOutline);
    }

    public void setBounds(GameSettings gms, RectF layout) {
        this.mBounds = layout;
    }
}
