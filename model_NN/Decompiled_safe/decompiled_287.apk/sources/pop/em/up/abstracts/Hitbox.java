package pop.em.up.abstracts;

import android.graphics.RectF;
import pop.em.up.GameSettings;

public interface Hitbox {
    RectF getHitBox(GameSettings gameSettings);

    boolean hit(float f, float f2, GameSettings gameSettings, int i);

    boolean scheduleRemoval();
}
