package pop.em.up.abstracts;

import android.graphics.Canvas;
import pop.em.up.GameSettings;

public interface GameDrawable {
    void addSelf(GameSettings gameSettings);

    boolean draw(Canvas canvas, GameSettings gameSettings);

    boolean scheduleRemoval();
}
