package pop.em.up.abstracts;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import pop.em.up.GameSettings;
import pop.em.up.StringGetter;
import pop.em.up.TextMeasurer;

public abstract class CanvasButton implements Hitbox {
    public int a = 255;
    public int b = 155;
    public int g = 155;
    public RectF mRct;
    public boolean mRemoved = false;
    TextMeasurer mText;
    public int r = 155;

    public CanvasButton(RectF r2, final String txt, Paint p) {
        this.mText = new TextMeasurer(new StringGetter() {
            public String getString() {
                return txt;
            }

            public String getFinalString() {
                return txt;
            }
        }, p, r2);
        this.mRct = r2;
    }

    public void draw(Canvas c, Paint p) {
        int tmp = p.getColor();
        p.setARGB(this.a, this.r, this.g, this.b);
        p.setStrokeWidth(2.0f);
        p.setStyle(Paint.Style.FILL);
        this.mText.paintText(c, p);
        p.setColor(tmp);
    }

    public RectF getHitBox(GameSettings gms) {
        return this.mRct;
    }

    public boolean scheduleRemoval() {
        return this.mRemoved;
    }
}
