package pop.em.up.abstracts;

import android.graphics.Canvas;
import pop.em.up.GameSettings;

public abstract class Animation {
    public float mX = 0.0f;
    protected float mY = 0.0f;

    public abstract void draw(Canvas canvas, GameSettings gameSettings);

    public abstract boolean finished();

    public abstract void tick(GameSettings gameSettings);
}
