package pop.em.up.abstracts;

public abstract class Affliction implements GameDrawable, GameTickable {
    public abstract void destroy();
}
