package pop.em.up.abstracts;

import pop.em.up.GameSettings;

public interface GameTickable {
    void addSelf(GameSettings gameSettings);

    boolean scheduleRemoval();

    boolean tick(GameSettings gameSettings);
}
