package pop.em.up;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;

public class TextMeasurer {
    private static Rect mTestRct = new Rect();
    float mOff = 0.0f;
    StringGetter mTxt;
    int mTxtHeight = 0;
    int mTxtSize = 0;
    float mX = 0.0f;

    public TextMeasurer(StringGetter str, Paint p, Rect r) {
        this.mTxt = str;
        this.mTxtSize = 0;
        this.mTxtHeight = 0;
        int i = 1;
        while (true) {
            if (i >= 50) {
                break;
            }
            p.setTextSize((float) i);
            p.getTextBounds(str.getFinalString(), 0, str.getFinalString().length(), mTestRct);
            mTestRct.offsetTo(r.left, r.top);
            if (!r.contains(mTestRct)) {
                this.mTxtSize = i - 1;
                break;
            }
            this.mTxtSize = i;
            this.mTxtHeight = mTestRct.height();
            i++;
        }
        this.mOff = (float) (((r.height() - this.mTxtHeight) / 2) + this.mTxtHeight);
        this.mX = r.exactCenterX();
    }

    public TextMeasurer(StringGetter str, Paint p, RectF r) {
        this.mTxt = str;
        this.mTxtSize = 0;
        this.mTxtHeight = 0;
        int i = 1;
        while (true) {
            if (i >= 50) {
                break;
            }
            p.setTextSize((float) i);
            p.getTextBounds(str.getFinalString(), 0, str.getFinalString().length(), mTestRct);
            mTestRct.offsetTo((int) (r.left + 1.0f), (int) (r.top + 1.0f));
            if (!r.contains((float) mTestRct.left, (float) mTestRct.top, (float) mTestRct.right, (float) mTestRct.bottom)) {
                this.mTxtSize = i - 1;
                break;
            }
            this.mTxtSize = i;
            this.mTxtHeight = mTestRct.height();
            i++;
        }
        this.mOff = r.top + ((r.height() - ((float) this.mTxtHeight)) / 2.0f) + ((float) this.mTxtHeight);
        this.mX = r.centerX();
    }

    public static int measureText(String str, Paint p, RectF r) {
        for (int i = 1; i < 50; i++) {
            p.setTextSize((float) i);
            p.getTextBounds(str, 0, str.length(), mTestRct);
            if (!r.contains((float) mTestRct.left, (float) mTestRct.top, (float) mTestRct.right, (float) mTestRct.bottom)) {
                return i - 1;
            }
        }
        return 50;
    }

    public static int measureText(String str, Paint p, Rect r) {
        for (int i = 1; i < 50; i++) {
            p.setTextSize((float) i);
            p.getTextBounds(str, 0, str.length(), mTestRct);
            if (!r.contains(mTestRct)) {
                return i - 1;
            }
        }
        return 50;
    }

    public static int measureText(String str, Paint p, float h) {
        for (int i = 1; i < 50; i++) {
            p.setTextSize((float) i);
            p.getTextBounds(str, 0, str.length(), mTestRct);
            if (((float) mTestRct.height()) > h) {
                return i - 1;
            }
        }
        return 50;
    }

    public void paintText(Canvas c, Paint p) {
        p.setTextSize((float) this.mTxtSize);
        p.setTextAlign(Paint.Align.CENTER);
        c.drawText(this.mTxt.getString(), this.mX, this.mOff, p);
    }

    public void paintText(Canvas c, Paint p, float mOffX, float mOffY) {
        p.setTextSize((float) this.mTxtSize);
        p.setTextAlign(Paint.Align.CENTER);
        c.drawText(this.mTxt.getString(), this.mX + mOffX, this.mOff + mOffY, p);
    }

    public static TextMeasurer[] measureStrings(StringGetter[] arrayStr, RectF r, float spacing, Paint p) {
        float h = (r.height() - (((float) (arrayStr.length + 1)) * spacing)) / ((float) arrayStr.length);
        TextMeasurer[] list = new TextMeasurer[arrayStr.length];
        for (int i = 0; i < list.length; i++) {
            RectF rct = new RectF();
            rct.left = r.left;
            rct.right = r.right;
            rct.top = r.top + (((float) i) * h) + (((float) (i + 1)) * spacing);
            rct.bottom = rct.top + h;
            list[i] = new TextMeasurer(arrayStr[i], p, rct);
        }
        return list;
    }
}
