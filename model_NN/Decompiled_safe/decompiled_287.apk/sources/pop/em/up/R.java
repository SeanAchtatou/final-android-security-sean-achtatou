package pop.em.up;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int background = 2130837504;
        public static final int balloonoverlay = 2130837505;
        public static final int balloonunderlay = 2130837506;
        public static final int cloud = 2130837507;
        public static final int deflectoroverlay = 2130837508;
        public static final int deflectorring = 2130837509;
        public static final int dragonanim1 = 2130837510;
        public static final int dragonanim10 = 2130837511;
        public static final int dragonanim11 = 2130837512;
        public static final int dragonanim2 = 2130837513;
        public static final int dragonanim3 = 2130837514;
        public static final int dragonanim4 = 2130837515;
        public static final int dragonanim5 = 2130837516;
        public static final int dragonanim6 = 2130837517;
        public static final int dragonanim7 = 2130837518;
        public static final int dragonanim8 = 2130837519;
        public static final int dragonanim9 = 2130837520;
        public static final int e = 2130837521;
        public static final int explosion = 2130837522;
        public static final int fire = 2130837523;
        public static final int gameover = 2130837524;
        public static final int heart = 2130837525;
        public static final int ic_launcher = 2130837526;
        public static final int icon = 2130837527;
        public static final int l = 2130837528;
        public static final int n0 = 2130837529;
        public static final int n1 = 2130837530;
        public static final int n2 = 2130837531;
        public static final int n3 = 2130837532;
        public static final int n4 = 2130837533;
        public static final int n5 = 2130837534;
        public static final int n6 = 2130837535;
        public static final int n7 = 2130837536;
        public static final int n8 = 2130837537;
        public static final int n9 = 2130837538;
        public static final int questionmark = 2130837539;
        public static final int sparkle = 2130837540;
        public static final int tankunderlay = 2130837541;
        public static final int teleportbar = 2130837542;
        public static final int v = 2130837543;
    }

    public static final class id {
        public static final int button1 = 2131099651;
        public static final int button2 = 2131099652;
        public static final int editText1 = 2131099649;
        public static final int gameView1 = 2131099654;
        public static final int leveldialoguetv = 2131099648;
        public static final int linearLayout = 2131099653;
        public static final int linearLayout1 = 2131099650;
        public static final int picklevel = 2131099656;
        public static final int restart = 2131099655;
    }

    public static final class layout {
        public static final int leveldialogue = 2130903040;
        public static final int main = 2130903041;
    }

    public static final class menu {
        public static final int levelenginemenu = 2131034112;
    }

    public static final class string {
        public static final int app_name = 2130968576;
        public static final int picklevel = 2130968577;
        public static final int restart = 2130968578;
    }
}
