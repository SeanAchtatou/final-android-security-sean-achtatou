package pop.em.up.levels;

import pop.em.up.GameSettings;
import pop.em.up.doodads.Cloud;
import pop.em.up.loaders.Loader;

public class MiscEvents {
    public static void createType(int i, GameSettings s) {
        switch (i) {
            case 0:
                s.createCloud(true);
                return;
            case 1:
                s.createCloud(false);
                return;
            default:
                return;
        }
    }

    public static void loadType(int i, Loader l) {
        switch (i) {
            case 0:
            case 1:
                Cloud.load(l);
                return;
            default:
                return;
        }
    }
}
