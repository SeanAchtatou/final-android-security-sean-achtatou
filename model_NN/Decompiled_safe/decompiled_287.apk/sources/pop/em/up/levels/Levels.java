package pop.em.up.levels;

import pop.em.up.GameSettings;
import pop.em.up.balloons.BKey;
import pop.em.up.doodads.Cloud;

public enum Levels {
    a1(0, 1.0f, 1.0f, 1.0f, 1.0f, new int[]{BKey.rb.K, 10}, 30, 30, new int[]{50}),
    a2(1, 1.0f, 1.0f, 0.9f, 0.9f, new int[]{BKey.rb.K, 20, BKey.pub.K, 5}, 30, 30, new int[]{50}),
    a3(2, 1.2f, 1.2f, 0.7f, 0.7f, new int[]{BKey.rb.K, 30, BKey.pub.K, 5}, 25, 25, new int[]{50, 50}),
    a4(-1, 1.4f, 1.4f, 0.7f, 0.7f, new int[]{BKey.rb.K, 40, BKey.pub.K, 10}, 25, 25, new int[]{50, 70}),
    a5(-1, 1.4f, 1.4f, 0.55f, 0.55f, new int[]{BKey.rb.K, 50, BKey.pub.K, 10}, 20, 20, new int[]{50, 100}),
    b1(3, 1.1f, 1.1f, 0.9f, 0.8f, new int[]{BKey.bsb.K, 10}, 25, 25, new int[]{50}),
    b2(4, 1.0f, 1.0f, 1.0f, 1.0f, new int[]{BKey.bsb.K, 7, BKey.pub.K, 3}, 20, 20, new int[]{50}),
    b3(-1, 1.15f, 1.15f, 0.8f, 0.8f, new int[]{BKey.bsb.K, 20, BKey.pub.K, 5}, 25, 25, new int[]{50, 50}),
    b4(-1, 1.25f, 1.25f, 0.75f, 0.75f, new int[]{BKey.rb.K, 30, BKey.bsb.K, 30, BKey.pub.K, 10}, 25, 25, new int[]{50, 100}),
    b5(-1, 1.35f, 1.35f, 0.65f, 0.65f, new int[]{BKey.rb.K, 30, BKey.bsb.K, 30, BKey.pub.K, 10}, 25, 25, new int[]{50, 130}),
    c1(5, 1.0f, 1.0f, 1.0f, 1.0f, new int[]{BKey.phb.K, 10}, 40, 40, new int[]{50}),
    c2(6, 1.1f, 1.15f, 1.0f, 1.0f, new int[]{BKey.pub.K, 5, BKey.phb.K, 10}, 30, 30, new int[]{50}),
    c3(7, 1.15f, 1.15f, 1.0f, 1.0f, new int[]{BKey.ptb.K, 10}, 30, 30, new int[]{50}),
    c4(-1, 1.2f, 1.2f, 0.85f, 0.85f, new int[]{BKey.pub.K, 5, BKey.phb.K, 10, BKey.ptb.K, 10}, 30, 30, new int[]{50}),
    c5(-1, 1.3f, 1.3f, 0.75f, 0.75f, new int[]{BKey.pub.K, 5, BKey.phb.K, 15, BKey.ptb.K, 15}, 25, 25, new int[]{50, 20}),
    d1(8, 1.0f, 1.0f, 1.0f, 1.0f, new int[]{BKey.gtb.K, 5}, 50, 50, new int[]{50}),
    d2(9, 1.0f, 1.0f, 1.0f, 1.0f, new int[]{BKey.gtb.K, 8, BKey.pub.K, 4}, 40, 40, new int[]{50}),
    d3(10, 1.3f, 1.3f, 1.0f, 1.0f, new int[]{BKey.sdb.K, 10}, 10, 10, new int[]{50}),
    d4(-1, 0.7f, 0.7f, 1.0f, 1.0f, new int[]{BKey.pub.K, 5, BKey.gtb.K, 10, BKey.sdb.K, 5}, 25, 25, new int[]{50}),
    d5(-1, 1.0f, 1.0f, 0.7f, 0.7f, new int[]{BKey.gtb.K, 20, BKey.sdb.K, 5}, 20, 20, new int[]{50}),
    e1(11, 0.5f, 0.5f, 2.5f, 2.5f, new int[]{BKey.rb.K, 75, BKey.bqb.K, 100}, 5, 5, new int[]{50}),
    e2(-1, 1.2f, 1.7f, 0.7f, 0.3f, new int[]{BKey.rb.K, 50}, 15, 25, new int[]{50}),
    e3(-1, 1.0f, 1.0f, 1.7f, 1.7f, new int[]{BKey.gtb.K, 15, BKey.bsb.K, 10}, 30, 30, new int[0]),
    e4(-1, 0.8f, 0.8f, 0.8f, 0.8f, new int[]{BKey.ptb.K, 15, BKey.phb.K, 30, BKey.rb.K, 15}, 15, 15, new int[0]),
    e5(12, 1.0f, 2.0f, 1.0f, 0.3f, r8, 30, 10, r11);
    
    private final float cBeginScale;
    private final float cBeginTimeScale;
    private final int cCounterEnd;
    private final int cCounterStart;
    private final float cEndScale;
    private final float cEndTimeScale;
    public final int[] cMisc;
    public final int cTutorial;
    public final int[] cTypes;
    private float i = 0.0f;
    private final int initialTotal;
    private float mBeginScale;
    private float mBeginTimeScale;
    private float mCounter = 0.0f;
    private int mCounterEnd = 0;
    private int mCounterStart = 30;
    private float mEndScale;
    private float mEndTimeScale;
    private int[] mMisc;
    private int mTotal = 0;
    private int[] mTypes;
    private final double[] miscprobabilities;
    private final double[] probabilities;

    static {
        int[] iArr = {BKey.ptb.K, 125, BKey.phb.K, 125, BKey.sdb.K, 125, BKey.ptb.K, 125};
        int[] iArr2 = new int[2];
        iArr2[1] = 150;
    }

    private Levels(int tutorial, float bts, float ets, float bs, float es, int[] types, int counterStart, int counterEnd, int[] misc) {
        this.cTutorial = tutorial;
        this.cBeginTimeScale = bts;
        this.cEndTimeScale = ets;
        this.cBeginScale = bs;
        this.cEndScale = es;
        this.cTypes = types;
        this.cMisc = misc;
        this.cCounterStart = counterStart;
        this.cCounterEnd = counterEnd;
        this.probabilities = new double[((this.cTypes.length / 2) - 1)];
        this.miscprobabilities = new double[this.cMisc.length];
        for (int i2 = 1; i2 < this.cTypes.length; i2 += 2) {
            this.mTotal += this.cTypes[i2];
        }
        this.initialTotal = this.mTotal;
    }

    public void reset(GameSettings s) {
        this.mBeginTimeScale = this.cBeginTimeScale;
        this.mEndTimeScale = this.cEndTimeScale;
        this.mBeginScale = this.cBeginScale;
        this.mEndScale = this.cEndScale;
        this.mTypes = (int[]) this.cTypes.clone();
        this.mMisc = (int[]) this.cMisc.clone();
        if (this.mMisc.length >= 1) {
            this.mMisc[0] = (int) (((((((float) this.cMisc[0]) / 100.0f) * s.mWidth) * s.mHeight) / Cloud.mWidth) / Cloud.mHeight);
        }
        if (this.mMisc.length >= 2) {
            this.mMisc[1] = (int) (((((((float) this.cMisc[1]) / 100.0f) * s.mWidth) * s.mHeight) / Cloud.mWidth) / Cloud.mHeight);
        }
        this.mCounterStart = this.cCounterStart;
        this.mCounterEnd = this.cCounterEnd;
        this.mCounter = (float) this.cCounterStart;
        this.mTotal = this.initialTotal;
    }

    public int balloonsLeft() {
        return this.mTotal;
    }

    public boolean anyLeft() {
        return this.mTotal != 0;
    }

    public void tick(GameSettings s) {
        if (this.mTypes != null && this.mTotal > 0) {
            this.i += s.mTimeScale;
            if (this.i >= this.mCounter) {
                if (this.mMisc != null) {
                    checkMisc(s);
                }
                this.i = 0.0f;
                double ratio = (double) (((float) this.mTotal) / ((float) this.initialTotal));
                double oldRatio = (double) ((((float) this.mTotal) + 1.0f) / ((float) this.initialTotal));
                s.mTimeScale = (float) ((((double) s.mTimeScale) * changeTimeScale(ratio)) / changeTimeScale(oldRatio));
                s.mScale = (float) ((((double) s.mScale) * changeScale(ratio)) / changeScale(oldRatio));
                this.mCounter = (float) ((((double) this.mCounter) * changeCounter(ratio)) / changeCounter(oldRatio));
                int k = 0;
                for (int i2 = 1; i2 < this.mTypes.length - 1; i2 += 2) {
                    this.probabilities[k] = (double) (((float) this.mTypes[i2]) / ((float) this.mTotal));
                    k++;
                }
                double r = Math.random();
                double c = 0.0d;
                for (int i3 = 0; i3 < this.probabilities.length; i3++) {
                    c += this.probabilities[i3];
                    if (r < c) {
                        int[] iArr = this.mTypes;
                        int i4 = (i3 * 2) + 1;
                        iArr[i4] = iArr[i4] - 1;
                        this.mTotal--;
                        s.createBalloon(this.mTypes[i3 * 2]);
                        return;
                    }
                }
                int[] iArr2 = this.mTypes;
                int length = (this.probabilities.length * 2) + 1;
                iArr2[length] = iArr2[length] - 1;
                this.mTotal--;
                s.createBalloon(this.mTypes[this.probabilities.length * 2]);
            }
        }
    }

    public void checkMisc(GameSettings s) {
        for (int i2 = 0; i2 < this.mMisc.length; i2++) {
            if (this.mMisc[i2] != 0) {
                this.miscprobabilities[i2] = (double) ((((float) this.mMisc[i2]) + (((float) this.mTotal) * 0.1f)) / ((float) this.mTotal));
            } else {
                this.mMisc[i2] = 0;
            }
        }
        double r = Math.random();
        double c = 0.0d;
        for (int i3 = 0; i3 < this.miscprobabilities.length; i3++) {
            c += this.miscprobabilities[i3];
            if (r < c) {
                int[] iArr = this.mMisc;
                iArr[i3] = iArr[i3] - 1;
                MiscEvents.createType(i3, s);
                return;
            }
        }
    }

    public double changeTimeScale(double ratio) {
        return ((double) this.mBeginTimeScale) + (((double) (this.mEndTimeScale - this.mBeginTimeScale)) * (1.0d - ratio));
    }

    public double changeScale(double ratio) {
        return ((double) this.mBeginScale) + (((double) (this.mEndScale - this.mBeginScale)) * (1.0d - ratio));
    }

    public double changeCounter(double ratio) {
        return ((double) this.mCounterStart) + (((double) (this.mCounterEnd - this.mCounterStart)) * (1.0d - ratio));
    }
}
