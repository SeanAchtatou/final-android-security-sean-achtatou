package pop.em.up.levels;

import pop.em.up.GameSettings;
import pop.em.up.doodads.Cloud;

public enum CopyOfLevels {
    a1(1, 1.0f, 1.0f, 1.0f, 1.0f, r8, 30, 30, new int[]{50}),
    a2(0, 1.0f, 1.0f, 0.9f, 0.9f, r8, 30, 30, new int[]{50}),
    a3(2, 1.2f, 1.2f, 0.7f, 0.7f, r8, 25, 25, new int[]{50, 50}),
    a4(0, 1.4f, 1.4f, 0.7f, 0.7f, r8, 25, 25, new int[]{50, 70}),
    a5(0, 1.4f, 1.4f, 0.55f, 0.55f, r8, 20, 20, new int[]{50, 100}),
    b1(3, 1.1f, 1.1f, 0.9f, 0.8f, r8, 25, 25, new int[]{50}),
    b2(4, 1.1f, 1.15f, 0.9f, 0.8f, r8, 25, 25, new int[]{50}),
    b3(0, 1.15f, 1.15f, 0.8f, 0.8f, r8, 25, 25, new int[]{50, 50}),
    b4(0, 1.25f, 1.25f, 0.75f, 0.75f, new int[]{30, 30, 10}, 25, 25, new int[]{50, 100}),
    b5(0, 1.35f, 1.35f, 0.65f, 0.65f, new int[]{30, 30, 10}, 25, 25, new int[]{50, 130}),
    c1(5, 1.0f, 1.0f, 1.0f, 1.0f, r8, 40, 40, new int[]{50}),
    c2(0, 1.1f, 1.15f, 1.0f, 1.0f, r8, 30, 30, new int[]{50}),
    c3(0, 1.15f, 1.15f, 1.0f, 1.0f, r8, 30, 30, new int[]{50}),
    c4(0, 1.2f, 1.2f, 0.85f, 0.85f, r8, 30, 30, new int[]{50}),
    c5(0, 1.3f, 1.3f, 0.75f, 0.75f, r8, 25, 25, new int[]{50, 20}),
    d1(0, 1.0f, 1.0f, 1.0f, 1.0f, r8, 50, 50, new int[]{50}),
    d2(0, 1.0f, 1.0f, 1.0f, 1.0f, r8, 50, 50, new int[]{50}),
    d3(0, 1.0f, 1.0f, 1.0f, 1.0f, r8, 25, 25, new int[]{50}),
    d4(0, 0.7f, 0.7f, 1.0f, 1.0f, r8, 25, 25, new int[]{50}),
    d5(0, 1.0f, 1.0f, 0.7f, 0.7f, r8, 20, 20, new int[]{50}),
    e1(0, 1.0f, 1.0f, 1.0f, 1.0f, new int[8], 5, 5, new int[]{500});
    
    private final float cBeginScale;
    private final float cBeginTimeScale;
    private final int cCounterEnd;
    private final int cCounterStart;
    private final float cEndScale;
    private final float cEndTimeScale;
    public final int[] cMisc;
    public final int cTutorial;
    public final int[] cTypes;
    private float i = 0.0f;
    private final int initialTotal;
    private float mBeginScale;
    private float mBeginTimeScale;
    private float mCounter = 0.0f;
    private int mCounterEnd = 0;
    private int mCounterStart = 30;
    private float mEndScale;
    private float mEndTimeScale;
    private int[] mMisc;
    private int mTotal = 0;
    private int[] mTypes;
    private final double[] miscprobabilities;
    private final double[] probabilities;

    static {
        int[] iArr = new int[3];
        iArr[0] = 10;
        iArr[2] = 5;
        int[] iArr2 = new int[3];
        iArr2[0] = 20;
        iArr2[2] = 5;
        int[] iArr3 = new int[3];
        iArr3[0] = 30;
        iArr3[2] = 5;
        int[] iArr4 = new int[3];
        iArr4[0] = 40;
        iArr4[2] = 10;
        int[] iArr5 = new int[3];
        iArr5[0] = 60;
        iArr5[2] = 5;
        int[] iArr6 = new int[3];
        iArr6[1] = 10;
        int[] iArr7 = new int[3];
        iArr7[1] = 10;
        iArr7[2] = 5;
        int[] iArr8 = new int[3];
        iArr8[1] = 20;
        iArr8[2] = 5;
        int[] iArr9 = new int[4];
        iArr9[2] = 5;
        iArr9[3] = 10;
        int[] iArr10 = new int[4];
        iArr10[2] = 5;
        iArr10[3] = 10;
        int[] iArr11 = new int[5];
        iArr11[4] = 10;
        int[] iArr12 = new int[5];
        iArr12[2] = 5;
        iArr12[3] = 10;
        iArr12[4] = 10;
        int[] iArr13 = new int[5];
        iArr13[2] = 10;
        iArr13[3] = 15;
        iArr13[4] = 15;
        int[] iArr14 = new int[6];
        iArr14[5] = 5;
        int[] iArr15 = new int[7];
        iArr15[6] = 10;
        int[] iArr16 = new int[8];
        iArr16[7] = 10;
        int[] iArr17 = new int[8];
        iArr17[2] = 5;
        iArr17[5] = 10;
        iArr17[6] = 6;
        iArr17[7] = 5;
        int[] iArr18 = new int[8];
        iArr18[5] = 20;
        iArr18[7] = 5;
    }

    private CopyOfLevels(int tutorial, float bts, float ets, float bs, float es, int[] types, int counterStart, int counterEnd, int[] misc) {
        this.cTutorial = tutorial;
        this.cBeginTimeScale = bts;
        this.cEndTimeScale = ets;
        this.cBeginScale = bs;
        this.cEndScale = es;
        this.cTypes = types;
        this.cMisc = misc;
        this.cCounterStart = counterStart;
        this.cCounterEnd = counterEnd;
        this.probabilities = new double[(this.cTypes.length - 1)];
        this.miscprobabilities = new double[this.cMisc.length];
        for (int i2 = 0; i2 < this.cTypes.length; i2++) {
            this.mTotal += this.cTypes[i2];
        }
        this.initialTotal = this.mTotal;
    }

    public void reset(GameSettings s) {
        this.mBeginTimeScale = this.cBeginTimeScale;
        this.mEndTimeScale = this.cEndTimeScale;
        this.mBeginScale = this.cBeginScale;
        this.mEndScale = this.cEndScale;
        this.mTypes = (int[]) this.cTypes.clone();
        this.mMisc = (int[]) this.cMisc.clone();
        if (this.mMisc.length >= 1) {
            this.mMisc[0] = (int) (((((((float) this.cMisc[0]) / 100.0f) * s.mWidth) * s.mHeight) / Cloud.mWidth) / Cloud.mHeight);
        }
        if (this.mMisc.length >= 2) {
            this.mMisc[1] = (int) (((((((float) this.cMisc[1]) / 100.0f) * s.mWidth) * s.mHeight) / Cloud.mWidth) / Cloud.mHeight);
        }
        this.mCounterStart = this.cCounterStart;
        this.mCounterEnd = this.cCounterEnd;
        this.mCounter = (float) this.cCounterStart;
        this.mTotal = this.initialTotal;
    }

    public int balloonsLeft() {
        return this.mTotal;
    }

    public boolean anyLeft() {
        return this.mTotal != 0;
    }

    public void tick(GameSettings s) {
        if (this.mTypes != null && this.mTotal > 0) {
            this.i += s.mTimeScale;
            if (this.i >= this.mCounter) {
                if (this.mMisc != null) {
                    checkMisc(s);
                }
                this.i = 0.0f;
                double ratio = (double) (((float) this.mTotal) / ((float) this.initialTotal));
                double oldRatio = (double) ((((float) this.mTotal) + 1.0f) / ((float) this.initialTotal));
                s.mTimeScale = (float) ((((double) s.mTimeScale) * changeTimeScale(ratio)) / changeTimeScale(oldRatio));
                s.mScale = (float) ((((double) s.mScale) * changeScale(ratio)) / changeScale(oldRatio));
                this.mCounter = (float) ((((double) this.mCounter) * changeCounter(ratio)) / changeCounter(oldRatio));
                for (int i2 = 0; i2 < this.mTypes.length - 1; i2++) {
                    this.probabilities[i2] = (double) (((float) this.mTypes[i2]) / ((float) this.mTotal));
                }
                double r = Math.random();
                double c = 0.0d;
                for (int i3 = 0; i3 < this.probabilities.length; i3++) {
                    c += this.probabilities[i3];
                    if (r < c) {
                        int[] iArr = this.mTypes;
                        iArr[i3] = iArr[i3] - 1;
                        this.mTotal--;
                        s.createBalloon(i3);
                        return;
                    }
                }
                int[] iArr2 = this.mTypes;
                int length = this.probabilities.length;
                iArr2[length] = iArr2[length] - 1;
                this.mTotal--;
                s.createBalloon(this.probabilities.length);
            }
        }
    }

    public void checkMisc(GameSettings s) {
        for (int i2 = 0; i2 < this.mMisc.length; i2++) {
            if (this.mMisc[i2] != 0) {
                this.miscprobabilities[i2] = (double) ((((float) this.mMisc[i2]) + (((float) this.mTotal) * 0.1f)) / ((float) this.mTotal));
            } else {
                this.mMisc[i2] = 0;
            }
        }
        double r = Math.random();
        double c = 0.0d;
        for (int i3 = 0; i3 < this.miscprobabilities.length; i3++) {
            c += this.miscprobabilities[i3];
            if (r < c) {
                int[] iArr = this.mMisc;
                iArr[i3] = iArr[i3] - 1;
                MiscEvents.createType(i3, s);
                return;
            }
        }
    }

    public double changeTimeScale(double ratio) {
        return ((double) this.mBeginTimeScale) + (((double) (this.mEndTimeScale - this.mBeginTimeScale)) * (1.0d - ratio));
    }

    public double changeScale(double ratio) {
        return ((double) this.mBeginScale) + (((double) (this.mEndScale - this.mBeginScale)) * (1.0d - ratio));
    }

    public double changeCounter(double ratio) {
        return ((double) this.mCounterStart) + (((double) (this.mCounterEnd - this.mCounterStart)) * (1.0d - ratio));
    }
}
