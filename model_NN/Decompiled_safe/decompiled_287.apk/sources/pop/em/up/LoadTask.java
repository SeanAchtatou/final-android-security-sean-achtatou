package pop.em.up;

import android.content.Context;

public abstract class LoadTask {
    public abstract void finished(GameSettings gameSettings);

    public abstract boolean isLoaded();

    public abstract void load(Context context);
}
