package pop.em.up.doodads;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import pop.em.up.GameSettings;
import pop.em.up.R;
import pop.em.up.abstracts.GameDrawable;
import pop.em.up.abstracts.GameTickable;

public class Pop implements GameDrawable, GameTickable {
    static Bitmap mExplosion;
    public static float mHeight = 0.0f;
    static float mMaxTime = 10.0f;
    public static float mOrigScale = 1.3f;
    public static float mWidth = 0.0f;
    public boolean mChange = true;
    public float mScale = 1.5f;
    float mTime = 0.0f;
    public float mX = 0.0f;
    public float mY = 0.0f;

    public static void load(Context c) {
        mExplosion = BitmapFactory.decodeResource(c.getResources(), R.drawable.explosion);
        mWidth = (float) (mExplosion.getWidth() / 2);
        mHeight = (float) (mExplosion.getHeight() / 2);
    }

    public static boolean isLoaded() {
        return mExplosion != null;
    }

    public boolean draw(Canvas c, GameSettings gms) {
        float scale = (float) (((double) ((this.mChange ? gms.mScale : 1.0f) * this.mScale)) * Math.sin((3.141592653589793d * ((double) this.mTime)) / ((double) mMaxTime)));
        if (scale < 0.0f) {
            return false;
        }
        c.save();
        c.translate(this.mX, this.mY);
        c.scale(scale, scale);
        c.drawBitmap(mExplosion, -mWidth, -mHeight, (Paint) null);
        c.restore();
        return false;
    }

    public boolean scheduleRemoval() {
        return this.mTime > mMaxTime;
    }

    public boolean tick(GameSettings gms) {
        this.mTime += gms.mTimeScale;
        return false;
    }

    public void addSelf(GameSettings gms) {
        gms.mTickables.add(this);
        gms.mDrawables.add(this);
    }
}
