package pop.em.up.doodads;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import pop.em.up.GameSettings;
import pop.em.up.R;
import pop.em.up.abstracts.GameDrawable;
import pop.em.up.abstracts.GameTickable;

public class TeleportAnimation implements GameDrawable, GameTickable {
    static Bitmap mTeleportBar;
    float dY = 0.0f;
    RectF mBar;
    Bitmap mBmp;
    RectF mOrig;
    RectF mRct;
    int mTicks = 0;

    public static void load(Context c) {
        mTeleportBar = BitmapFactory.decodeResource(c.getResources(), R.drawable.teleportbar);
    }

    public TeleportAnimation(RectF r, Bitmap bmp, float dur, GameSettings s) {
        s.getClass();
        this.mTicks = (int) ((1000.0f * dur) / 33.0f);
        this.mOrig = new RectF(r);
        this.mRct = new RectF(r);
        this.mBar = new RectF(0.0f, 0.0f, r.width(), r.height() * 0.04f);
        this.mBmp = bmp;
        this.dY = ((this.mRct.height() - this.mBar.height()) / ((float) this.mTicks)) * 0.5f;
    }

    public boolean tick(GameSettings gms) {
        this.mTicks--;
        this.mRct.top += this.dY;
        this.mRct.bottom -= this.dY;
        return false;
    }

    public boolean draw(Canvas c, GameSettings gms) {
        if (this.mTicks < 0) {
            return false;
        }
        c.save();
        c.clipRect(this.mRct);
        c.drawBitmap(this.mBmp, (Rect) null, this.mOrig, (Paint) null);
        c.restore();
        this.mBar.offsetTo(this.mRct.left, this.mRct.top);
        c.drawBitmap(mTeleportBar, (Rect) null, this.mBar, (Paint) null);
        this.mBar.offsetTo(this.mRct.left, this.mRct.bottom - this.mBar.height());
        c.drawBitmap(mTeleportBar, (Rect) null, this.mBar, (Paint) null);
        return false;
    }

    public boolean scheduleRemoval() {
        return this.mTicks < 0;
    }

    public void addSelf(GameSettings gms) {
        gms.mDrawables.addToEnd(this);
        gms.mTickables.addToEnd(this);
    }
}
