package pop.em.up.doodads;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import pop.em.up.GameSettings;
import pop.em.up.R;
import pop.em.up.abstracts.GameDrawable;
import pop.em.up.abstracts.GameTickable;

public class ReflectionAnimation implements GameTickable, GameDrawable {
    static Bitmap mDeflection;
    static float mOff1X;
    static float mOff1Y;
    static float mOff2X;
    static float mOff2Y;
    static Bitmap mOverlay;
    static float mScale = 0.4f;
    float mAlpha = 255.0f;
    float mRadius = 0.0f;
    float mX;
    float mY;
    float mda = 255.0f;
    float mdr = 100.0f;

    public static void load(Context c) {
        mDeflection = BitmapFactory.decodeResource(c.getResources(), R.drawable.deflectorring);
        mOverlay = BitmapFactory.decodeResource(c.getResources(), R.drawable.deflectoroverlay);
        mOff1X = ((float) mDeflection.getWidth()) / 2.0f;
        mOff1Y = ((float) mDeflection.getHeight()) / 2.0f;
        mOff2X = ((float) mOverlay.getWidth()) / 2.0f;
        mOff2Y = ((float) mOverlay.getHeight()) / 2.0f;
    }

    public ReflectionAnimation(float x, float y, float dur, GameSettings s) {
        s.getClass();
        int ticks = (int) ((1000.0f * dur) / 33.0f);
        this.mX = x;
        this.mY = y;
        this.mda = (float) (255 / ticks);
        this.mdr = mOff1X / ((float) ticks);
    }

    public boolean draw(Canvas c, GameSettings gms) {
        if (this.mAlpha <= 0.0f) {
            return false;
        }
        c.save();
        c.translate(this.mX, this.mY);
        c.save();
        c.scale(mScale, mScale);
        c.drawBitmap(mOverlay, -mOff1X, -mOff1Y, (Paint) null);
        c.restore();
        c.scale((mScale * this.mRadius) / mOff1X, (mScale * this.mRadius) / mOff1X);
        c.drawBitmap(mDeflection, -mOff2X, -mOff2Y, (Paint) null);
        c.restore();
        return false;
    }

    public boolean tick(GameSettings gms) {
        this.mAlpha -= this.mda;
        this.mRadius += this.mdr;
        return false;
    }

    public boolean scheduleRemoval() {
        return this.mAlpha < 0.0f;
    }

    public void addSelf(GameSettings gms) {
        gms.mTickables.add(this);
        gms.mDrawables.addToEnd(this);
    }
}
