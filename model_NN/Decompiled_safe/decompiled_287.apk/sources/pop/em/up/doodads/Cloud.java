package pop.em.up.doodads;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import pop.em.up.GameSettings;
import pop.em.up.LoadTask;
import pop.em.up.R;
import pop.em.up.abstracts.GameDrawable;
import pop.em.up.abstracts.GameTickable;
import pop.em.up.loaders.Loader;

public class Cloud implements GameTickable, GameDrawable {
    public static Bitmap mCloud;
    public static float mHeight = 170.0f;
    public static boolean mKillClouds = false;
    private static Paint mPnt = new Paint();
    /* access modifiers changed from: private */
    public static float mScaleX = 0.0f;
    /* access modifiers changed from: private */
    public static float mScaleY = 0.0f;
    public static float mWidth = 350.0f;
    int mAlpha = 255;
    boolean mBackground = false;
    public float mConcatScale = 0.0f;
    public float mConcatV = 0.0f;
    public int mDirection = -1;
    float mDistance = 0.0f;
    private float mTimeScale = 1.0f;
    public float mWorldScale = 0.0f;
    public float mWorldTScale = 0.0f;
    public float mX = 0.0f;
    public float mY = 0.0f;
    public boolean removed = false;

    static {
        mPnt.setAntiAlias(true);
    }

    public Cloud(GameSettings gms, boolean background) {
        this.mBackground = background;
        reset(gms);
        addSelf(gms);
    }

    public static void load(Loader l) {
        l.mTasks.add(new LoadTask() {
            public void load(Context c) {
                Cloud.mCloud = BitmapFactory.decodeResource(c.getResources(), R.drawable.cloud);
                Cloud.mWidth = (float) Cloud.mCloud.getWidth();
                Cloud.mHeight = (float) Cloud.mCloud.getHeight();
                Cloud.mScaleX = Cloud.mWidth / 2.0f;
                Cloud.mScaleY = Cloud.mHeight / 2.0f;
            }

            public boolean isLoaded() {
                return Cloud.mCloud != null;
            }

            public void finished(GameSettings s) {
            }
        });
    }

    public void remove(GameSettings gms) {
        this.removed = true;
    }

    public void reset(GameSettings gms) {
        this.mAlpha = ((int) (30.0d * Math.random())) + 200;
        this.mTimeScale = gms.mWidth / ((float) gms.durationtoTicks(5.0f + (15.0f * ((float) Math.random()))));
        if (this.mBackground) {
            this.mDistance = 1.3f + (0.25f * ((float) Math.random()));
            if (mKillClouds) {
                this.removed = true;
            }
        } else {
            this.mDistance = 1.0f + (0.35f * ((float) Math.random()));
            if (mKillClouds) {
                this.removed = true;
            }
        }
        this.mY = gms.mTop + (this.mDistance * mScaleY) + ((float) (Math.random() * ((double) ((gms.mHeight - gms.mTop) - (this.mDistance * mScaleY)))));
        if (Math.random() > 0.5d) {
            this.mX = (-mScaleX) * this.mDistance;
            this.mDirection = 1;
        } else {
            this.mX = gms.mWidth + (mScaleX * this.mDistance);
            this.mDirection = -1;
        }
        this.mWorldScale = -1000.0f;
        this.mWorldTScale = -1000.0f;
    }

    public boolean tick(GameSettings gms) {
        if (mKillClouds) {
            this.mAlpha -= 30;
            if (this.mAlpha <= 30) {
                remove(gms);
            }
            return false;
        }
        if (this.mWorldTScale != gms.mTimeScale) {
            this.mWorldTScale = gms.mTimeScale;
            this.mConcatV = ((float) this.mDirection) * this.mTimeScale * gms.mTimeScale;
        }
        this.mX += this.mConcatV;
        if (this.mDirection == -1 && this.mX < (-mScaleX) * this.mDistance * gms.mScale) {
            reset(gms);
        } else if (this.mDirection == 1 && this.mX > gms.mWidth + (this.mDistance * mScaleX)) {
            reset(gms);
        }
        return false;
    }

    public boolean draw(Canvas c, GameSettings gms) {
        if (this.mWorldScale != gms.mScale) {
            float newscale = ((float) Math.sqrt((double) gms.mScale)) * this.mDistance;
            this.mWorldScale = gms.mScale;
            this.mConcatScale = newscale;
        }
        c.save();
        c.translate(this.mX, this.mY);
        c.scale(this.mConcatScale, this.mConcatScale);
        mPnt.setAlpha(this.mAlpha);
        c.drawBitmap(mCloud, -mScaleX, -mScaleY, mPnt);
        c.restore();
        return false;
    }

    public boolean scheduleRemoval() {
        return this.removed;
    }

    public void addSelf(GameSettings gms) {
        if (this.mBackground) {
            gms.mBackground.add(this);
        } else {
            gms.mForeground.add(this);
        }
        gms.mTickables.add(this);
    }
}
