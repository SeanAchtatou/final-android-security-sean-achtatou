package pop.em.up.doodads;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import pop.em.up.GameSettings;
import pop.em.up.R;
import pop.em.up.abstracts.GameDrawable;

public class Sparkle implements GameDrawable {
    private static Bitmap mImage;
    private static Paint mPaint;
    static int mdA = -15;
    int mAlpha = 255;
    float mScale = 0.0f;
    float mX;
    float mY;

    public static void load(Context c) {
        mImage = BitmapFactory.decodeResource(c.getResources(), R.drawable.sparkle);
        mPaint = new Paint();
    }

    public static boolean isLoaded() {
        return (mImage == null || mPaint == null) ? false : true;
    }

    public Sparkle(float x, float y, float scale, GameSettings gms) {
        this.mX = x;
        this.mY = y;
        this.mAlpha = 255;
        this.mScale = 0.15f * scale;
    }

    public boolean draw(Canvas c, GameSettings gms) {
        c.save();
        c.translate(this.mX, this.mY);
        c.scale(this.mScale, this.mScale);
        mPaint.setAlpha(this.mAlpha);
        c.drawBitmap(mImage, 0.0f, 0.0f, mPaint);
        c.restore();
        this.mY += 1.0f;
        this.mAlpha += mdA;
        return false;
    }

    public boolean scheduleRemoval() {
        return this.mAlpha <= mdA;
    }

    public void addSelf(GameSettings gms) {
        gms.mDrawables.addToEnd(this);
    }
}
