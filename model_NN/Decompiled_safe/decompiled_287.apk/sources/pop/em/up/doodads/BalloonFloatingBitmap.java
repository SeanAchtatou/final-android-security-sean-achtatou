package pop.em.up.doodads;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import pop.em.up.GameSettings;
import pop.em.up.abstracts.Animation;
import pop.em.up.balloons.Balloon;
import pop.em.up.balloons.RedBalloon;
import pop.em.up.loaders.Loader;

public class BalloonFloatingBitmap extends Animation {
    static final int STATEDROPPING = 0;
    static final int STATEFALLING = 2;
    static final int STATEFLOATING = 1;
    Bitmap mAttached;
    private float mBalloonScale = 1.0f;
    private float mConstCos;
    public float mEndY = 0.0f;
    Loader mLoader;
    float mOffset = 0.0f;
    private float mScale = 1.0f;
    public float mScaleX;
    public float mScaleY;
    int mState = STATEDROPPING;
    int mTick = STATEDROPPING;
    int mTicksDown;
    int mTicksFall;
    int mTicksStay;
    float mV = 0.0f;
    float mYOff = 0.0f;

    public BalloonFloatingBitmap(Bitmap attached, int mtd, int mts, int mtf, float mey, float ms, Loader l) {
        this.mAttached = attached;
        this.mTicksDown = mtd;
        this.mTicksStay = mts;
        this.mTicksFall = mtf;
        this.mEndY = mey;
        this.mScale = ms;
        this.mScaleX = (float) (attached.getWidth() / STATEFALLING);
        this.mScaleY = (float) (attached.getHeight() / STATEFALLING);
        this.mBalloonScale = (((float) attached.getHeight()) * ms) / (Balloon.mOrigHeight + 10.0f);
        this.mOffset = 0.0f;
        this.mYOff = Balloon.mScaleY * this.mBalloonScale;
        this.mY = (-this.mYOff) - (((float) attached.getHeight()) * this.mScale);
        this.mEndY -= this.mYOff - ((this.mScaleY * this.mScale) * 2.0f);
        this.mV = (this.mEndY - this.mY) / ((float) mtd);
        this.mLoader = l;
    }

    public void draw(Canvas c, GameSettings s) {
        if (this.mState != -1) {
            if (this.mState != STATEFALLING) {
                float px = (-Balloon.mOrigWidth) / 2.0f;
                float py = (-(Balloon.mOrigHeight + 10.0f)) / 2.0f;
                c.save();
                c.translate(this.mX + this.mOffset, this.mY);
                c.scale(this.mBalloonScale, this.mBalloonScale);
                c.clipRect(px, py, -px, -py);
                c.drawBitmap(RedBalloon.mImage, px, py, (Paint) null);
                c.restore();
            }
            c.save();
            c.translate(this.mX, this.mY + this.mYOff);
            c.scale(this.mScale, this.mScale, 0.0f, 0.0f);
            c.drawBitmap(this.mAttached, -this.mScaleX, -this.mScaleY, (Paint) null);
            c.restore();
        }
    }

    public void tick(GameSettings s) {
        switch (this.mState) {
            case STATEDROPPING /*0*/:
                this.mY += this.mV;
                this.mTick += STATEFLOATING;
                if (this.mTick >= this.mTicksDown) {
                    this.mState = STATEFLOATING;
                    this.mTick = STATEDROPPING;
                    this.mConstCos = (float) (15.707963267948966d / ((double) this.mTicksStay));
                    return;
                }
                return;
            case STATEFLOATING /*1*/:
                this.mY = (float) (((double) this.mY) + (((double) (3.0f * max(1.0f - this.mScale, 0.5f))) * Math.sin((double) (this.mConstCos * ((float) this.mTick)))));
                this.mTick += STATEFLOATING;
                if (this.mTick < this.mTicksStay) {
                    return;
                }
                if (this.mLoader == null || this.mLoader.isFinished()) {
                    this.mState = STATEFALLING;
                    this.mTick = STATEDROPPING;
                    this.mV = (s.mHeight - this.mY) / ((float) this.mTicksFall);
                    Pop p = new Pop();
                    p.mScale = this.mBalloonScale * Pop.mOrigScale;
                    p.mChange = false;
                    p.mX = this.mX;
                    p.mY = this.mY;
                    s.mDrawables.addToEnd(p);
                    s.mTickables.addToEnd(p);
                    return;
                }
                return;
            case STATEFALLING /*2*/:
                this.mY += this.mV;
                this.mTick += STATEFLOATING;
                if (this.mTick >= this.mTicksFall) {
                    this.mState = -1;
                    return;
                }
                return;
            default:
                return;
        }
    }

    public float max(float a, float b) {
        return a > b ? a : b;
    }

    public boolean finished() {
        return this.mState == -1;
    }
}
