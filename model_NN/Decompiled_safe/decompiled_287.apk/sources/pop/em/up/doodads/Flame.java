package pop.em.up.doodads;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import pop.em.up.GameSettings;
import pop.em.up.R;
import pop.em.up.abstracts.GameDrawable;

public class Flame implements GameDrawable {
    static Bitmap mFlame;
    static float mOffX;
    static float mOffY;
    static Paint p = new Paint();
    int dAlpha = 0;
    int mAlpha = 255;
    float mDeg = 0.0f;
    float mScale = 0.6f;
    float mX;
    float mY;

    public static void load(Context c) {
        mFlame = BitmapFactory.decodeResource(c.getResources(), R.drawable.fire);
        mOffX = (float) (mFlame.getWidth() / 2);
        mOffY = (float) (mFlame.getHeight() / 2);
    }

    public Flame(float x, float y, float dur, GameSettings s) {
        s.getClass();
        this.mX = x;
        this.mY = y;
        this.dAlpha = 255 / ((int) ((1000.0f * dur) / 33.0f));
    }

    public boolean draw(Canvas c, GameSettings gms) {
        this.mAlpha -= this.dAlpha;
        this.mDeg += 5.0f;
        this.mScale += 0.15f;
        p.setAlpha(this.mAlpha);
        c.save();
        c.translate(this.mX, this.mY);
        c.scale(this.mScale, this.mScale);
        c.rotate(this.mDeg);
        if (this.mAlpha > 0) {
            c.drawBitmap(mFlame, -mOffX, -mOffY, p);
        }
        c.restore();
        return false;
    }

    public boolean scheduleRemoval() {
        return this.mAlpha < 0;
    }

    public void addSelf(GameSettings gms) {
        gms.mDrawables.addToEnd(this);
    }
}
