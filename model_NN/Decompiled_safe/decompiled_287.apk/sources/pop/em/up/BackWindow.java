package pop.em.up;

import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import pop.em.up.abstracts.CanvasButton;
import pop.em.up.engines.LevelEngine;

public class BackWindow extends PopupWindow {
    private static /* synthetic */ int[] $SWITCH_TABLE$pop$em$up$choices;
    /* access modifiers changed from: private */
    public choices choice;
    LevelEngine mEng;
    CanvasButton mMenu;
    CanvasButton mNo;
    TextMeasurer mRow1;
    CanvasButton mYes;

    static /* synthetic */ int[] $SWITCH_TABLE$pop$em$up$choices() {
        int[] iArr = $SWITCH_TABLE$pop$em$up$choices;
        if (iArr == null) {
            iArr = new int[choices.values().length];
            try {
                iArr[choices.menu.ordinal()] = 3;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[choices.no.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[choices.yes.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$pop$em$up$choices = iArr;
        }
        return iArr;
    }

    public BackWindow(GameSettings gms, LevelEngine e, Paint mPnt) {
        super(gms);
        this.mWidth = 0.7f;
        this.mHeight = 0.4f;
        this.mEng = e;
        this.mPriority = 5;
        RectF r = new RectF();
        r.left = (gms.mWidth / 2.0f) - ((gms.mWidth * this.mWidth) / 2.0f);
        r.right = (gms.mWidth / 2.0f) + ((gms.mWidth * this.mWidth) / 2.0f);
        r.top = (gms.mHeight / 2.0f) - ((gms.mHeight * this.mHeight) / 2.0f);
        r.bottom = (gms.mHeight / 2.0f) + ((gms.mHeight * this.mHeight) / 2.0f);
        float h = r.height();
        RectF mDst = new RectF();
        mDst.left = r.left;
        mDst.right = r.right;
        mDst.top = r.top;
        r.top += 0.5f * h;
        mDst.bottom = r.top;
        r.top += 0.1f * h;
        r.bottom -= 0.1f * h;
        RectF mButton1 = new RectF();
        mButton1.left = r.left;
        mButton1.right = r.left + ((r.width() * 1.0f) / 3.0f);
        mButton1.top = r.top;
        mButton1.bottom = r.bottom;
        RectF mButton2 = new RectF();
        mButton2.left = r.left + ((r.width() * 1.0f) / 3.0f);
        mButton2.right = mButton2.left + ((r.width() * 1.0f) / 3.0f);
        mButton2.top = r.top;
        mButton2.bottom = r.bottom;
        RectF mButton3 = new RectF();
        mButton3.left = r.left + ((r.width() * 2.0f) / 3.0f);
        mButton3.right = mButton3.left + ((r.width() * 1.0f) / 3.0f);
        mButton3.top = r.top;
        mButton3.bottom = r.bottom;
        this.mRow1 = new TextMeasurer(new StaticString("Do you want to exit?"), mPnt, mDst);
        this.mYes = new CanvasButton(mButton1, "Yes", mPnt) {
            public boolean hit(float x, float y, GameSettings gms, int hp) {
                if (!BackWindow.this.fullSize(gms) || !this.mRct.contains(x, y)) {
                    return false;
                }
                this.r = 255;
                this.g = 255;
                this.b = 255;
                BackWindow.this.mExpanding = false;
                BackWindow.this.mYes.mRemoved = true;
                BackWindow.this.mMenu.mRemoved = true;
                BackWindow.this.mNo.mRemoved = true;
                BackWindow.this.choice = choices.yes;
                return true;
            }
        };
        this.mMenu = new CanvasButton(mButton2, "Menu", mPnt) {
            public boolean hit(float x, float y, GameSettings gms, int hp) {
                if (!BackWindow.this.fullSize(gms) || !this.mRct.contains(x, y)) {
                    return false;
                }
                this.r = 255;
                this.g = 255;
                this.b = 255;
                BackWindow.this.mExpanding = false;
                BackWindow.this.mYes.mRemoved = true;
                BackWindow.this.mMenu.mRemoved = true;
                BackWindow.this.mNo.mRemoved = true;
                BackWindow.this.choice = choices.menu;
                return true;
            }
        };
        this.mNo = new CanvasButton(mButton3, "No", mPnt) {
            public boolean hit(float x, float y, GameSettings gms, int hp) {
                if (!BackWindow.this.fullSize(gms) || !this.mRct.contains(x, y)) {
                    return false;
                }
                this.r = 255;
                this.g = 255;
                this.b = 255;
                BackWindow.this.mExpanding = false;
                BackWindow.this.mYes.mRemoved = true;
                BackWindow.this.mMenu.mRemoved = true;
                BackWindow.this.mNo.mRemoved = true;
                BackWindow.this.choice = choices.no;
                gms.pause(false);
                return true;
            }
        };
    }

    public boolean draw(Canvas c, GameSettings gms, Paint mPnt) {
        super.draw(c, gms, mPnt);
        c.save();
        c.clipRect(this.mRct);
        mPnt.setARGB(255, 155, 155, 155);
        mPnt.setStyle(Paint.Style.FILL);
        this.mRow1.paintText(c, mPnt);
        this.mYes.draw(c, mPnt);
        this.mMenu.draw(c, mPnt);
        this.mNo.draw(c, mPnt);
        c.restore();
        return false;
    }

    public boolean tick(GameSettings gms) {
        super.tick(gms);
        return finished(gms);
    }

    public void onFinish(GameSettings gms) {
        if (!this.mEng.mWnds.windowsLeft()) {
            gms.pause(true);
        }
        switch ($SWITCH_TABLE$pop$em$up$choices()[this.choice.ordinal()]) {
            case 1:
                gms.mRecord = true;
                Intent i = new Intent("android.intent.action.MAIN");
                i.addCategory("android.intent.category.HOME");
                gms.mAct.startActivity(i);
                return;
            case 2:
                gms.mRecord = true;
                return;
            case 3:
                gms.mRecord = true;
                gms.loadMenu();
                return;
            default:
                return;
        }
    }

    public boolean finished(GameSettings gms) {
        return closed(gms) && !this.mExpanding;
    }

    public void deInit() {
    }

    public boolean hit(float x, float y, GameSettings gms) {
        if (this.mYes.hit(x, y, gms, 0) || this.mMenu.hit(x, y, gms, 0) || this.mNo.hit(x, y, gms, 0)) {
            return true;
        }
        return false;
    }
}
