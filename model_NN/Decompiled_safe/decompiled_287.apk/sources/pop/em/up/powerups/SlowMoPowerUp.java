package pop.em.up.powerups;

import android.graphics.Canvas;
import pop.em.up.GameSettings;
import pop.em.up.abstracts.GameDrawable;
import pop.em.up.abstracts.GameTickable;

public class SlowMoPowerUp implements GameTickable, GameDrawable {
    final int mBound;
    int mCount = 0;
    boolean mRemoved = false;
    float mScale = 0.0f;

    public SlowMoPowerUp(float duration, GameSettings s, float timeScale) {
        PowerUp.mPowerUps = true;
        s.getClass();
        this.mBound = (int) ((1000.0f * duration) / 33.0f);
        this.mScale = timeScale;
        s.mTimeScale *= this.mScale;
    }

    public boolean tick(GameSettings gms) {
        this.mCount++;
        if (this.mCount > this.mBound) {
            gms.mTimeScale /= this.mScale;
            this.mRemoved = true;
        }
        if (PowerUp.mPowerUps) {
            return false;
        }
        this.mRemoved = true;
        return false;
    }

    public boolean draw(Canvas c, GameSettings gms) {
        c.drawARGB(70, 60, 60, 255);
        return false;
    }

    public boolean scheduleRemoval() {
        return this.mRemoved;
    }

    public void addSelf(GameSettings gms) {
        gms.mTickables.add(this);
        gms.mForeground.addToEnd(this);
    }
}
