package pop.em.up.powerups;

import android.content.Context;
import pop.em.up.GameSettings;
import pop.em.up.LoadTask;
import pop.em.up.abstracts.GameTickable;
import pop.em.up.balloons.Balloon;
import pop.em.up.balloons.FloatingText;
import pop.em.up.doodads.Flame;
import pop.em.up.loaders.Loader;

public class PowerUp {
    public static boolean mPowerUps = true;

    public static GameTickable getPowerUp(GameSettings gms, Balloon b) {
        new SizeUpPowerUp(10.0f, gms, 1.25f).addSelf(gms);
        return null;
    }

    public static void loadPowerUps(Loader l) {
        l.mTasks.addToEnd(new LoadTask() {
            public void load(Context c) {
                Flame.load(c);
            }

            public boolean isLoaded() {
                return false;
            }

            public void finished(GameSettings s) {
            }
        });
    }

    public static void loadPowerUps(final int level, Loader l) {
        l.mTasks.addToEnd(new LoadTask() {
            public void load(Context c) {
                if (level >= 11) {
                    Flame.load(c);
                }
            }

            public boolean isLoaded() {
                return false;
            }

            public void finished(GameSettings s) {
            }
        });
    }

    public static GameTickable getPowerUp(GameSettings gms, int level, Balloon b) {
        int total = 50;
        if (level == 6) {
            new FloatingText("Slow-Mo Power Up!", b.x, b.y, 20, 0, 0, 0).addSelf(gms);
            return new SlowMoPowerUp(10.0f, gms, 0.5f);
        } else if (level == 11) {
            new FloatingText("Flame-Thrower Power Up!", b.x, b.y, 20, 0, 0, 0).addSelf(gms);
            return new FlameThrowerPowerUp(b.x, b.y, 6.0f, gms, 0.1f);
        } else if (level == 16) {
            new FloatingText("Triple-Damage Power Up!", b.x, b.y, 20, 0, 0, 0).addSelf(gms);
            return new TripleTap(8.0f, gms);
        } else {
            if (level > 6) {
                total = 50 + 40;
            }
            if (level > 11) {
                total += 20;
            }
            if (level > 16) {
                total += 30;
            }
            int rand = (int) (Math.random() * ((double) total));
            if (rand <= 50) {
                new FloatingText("Size-Up Power Up!", b.x, b.y, 20, 0, 0, 0).addSelf(gms);
                return new SizeUpPowerUp(10.0f, gms, 1.25f);
            } else if (rand <= 90) {
                new FloatingText("Slow-Mo Power Up!", b.x, b.y, 20, 0, 0, 0).addSelf(gms);
                return new SlowMoPowerUp(10.0f, gms, 0.65f);
            } else if (rand <= 110) {
                new FloatingText("Flame-Thrower Power Up!", b.x, b.y, 20, 0, 0, 0).addSelf(gms);
                return new FlameThrowerPowerUp(b.x, b.y, 6.0f, gms, 0.1f);
            } else if (rand > 140) {
                return null;
            } else {
                new FloatingText("Triple-Damage Power Up!", b.x, b.y, 20, 0, 0, 0).addSelf(gms);
                return new TripleTap(8.0f, gms);
            }
        }
    }
}
