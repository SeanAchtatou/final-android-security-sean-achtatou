package pop.em.up.powerups;

import pop.em.up.GameSettings;
import pop.em.up.abstracts.GameTickable;

public class SizeUpPowerUp implements GameTickable {
    final int mBound;
    int mCount = 0;
    float mScale = 0.0f;
    float mTryScale = 1.0f;

    public SizeUpPowerUp(float duration, GameSettings s, float timeScale) {
        PowerUp.mPowerUps = true;
        s.getClass();
        this.mBound = (int) ((1000.0f * duration) / 33.0f);
        this.mScale = timeScale;
    }

    public boolean tick(GameSettings gms) {
        this.mCount++;
        if (this.mCount > this.mBound) {
            float newScale = this.mTryScale - 0.01f;
            gms.mScale *= newScale / this.mTryScale;
            this.mTryScale = newScale;
            if (this.mTryScale > 1.0f) {
                return false;
            }
            gms.mScale /= this.mTryScale;
            return false;
        } else if (this.mTryScale > this.mScale) {
            return false;
        } else {
            float newScale2 = this.mTryScale + 0.01f;
            gms.mScale *= newScale2 / this.mTryScale;
            this.mTryScale = newScale2;
            return false;
        }
    }

    public boolean scheduleRemoval() {
        return !PowerUp.mPowerUps || this.mTryScale <= 1.0f;
    }

    public void addSelf(GameSettings gms) {
        gms.mTickables.add(this);
    }
}
