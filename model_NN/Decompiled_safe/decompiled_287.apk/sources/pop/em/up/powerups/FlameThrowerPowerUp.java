package pop.em.up.powerups;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.RectF;
import java.util.Iterator;
import pop.em.up.GameSettings;
import pop.em.up.abstracts.GameTickable;
import pop.em.up.abstracts.Hitbox;
import pop.em.up.balloons.Balloon;
import pop.em.up.doodads.Flame;

public class FlameThrowerPowerUp implements GameTickable, Hitbox {
    static FlameThrowerPowerUp mCurrent = null;
    static Paint mP = new Paint();
    int mBound;
    RectF mBox;
    int mCount = 0;
    float mRadius = 50.0f;
    boolean mRemoved = false;
    float mX;
    float mY;
    int newCount = 0;
    int newMBound;

    static {
        mP.setColor(-65536);
        mP.setStyle(Paint.Style.STROKE);
        mP.setStrokeWidth(5.0f);
    }

    public void load(Context c) {
        Flame.load(c);
    }

    public FlameThrowerPowerUp(float x, float y, float duration, GameSettings s, float durBetween) {
        this.mX = s.mWidth / 2.0f;
        this.mY = s.mHeight / 2.0f;
        PowerUp.mPowerUps = true;
        s.getClass();
        this.mBound = (int) ((1000.0f * duration) / 33.0f);
        s.getClass();
        this.newMBound = (int) ((1000.0f * durBetween) / 33.0f);
        if (mCurrent == null) {
            mCurrent = this;
            return;
        }
        mCurrent.mBound += this.mBound;
        mCurrent.newMBound = this.newMBound;
        this.mBound = -1;
    }

    public boolean tick(GameSettings gms) {
        this.mCount++;
        if (this.mCount > this.mBound || !PowerUp.mPowerUps) {
            this.mRemoved = true;
            mCurrent = null;
        }
        this.newCount++;
        if (this.newCount > this.newMBound) {
            new Flame(this.mX, this.mY, 0.6f, gms).addSelf(gms);
            Iterator<Balloon> it = gms.mBalloons.iterator();
            while (it.hasNext()) {
                Balloon b = it.next();
                if (b.getHitBox(gms).contains(this.mX, this.mY)) {
                    b.hit(this.mX, this.mY, gms, 1);
                    return false;
                }
            }
            this.newCount = 0;
        }
        return false;
    }

    public boolean scheduleRemoval() {
        return this.mRemoved;
    }

    public void addSelf(GameSettings gms) {
        if (this.mBound >= 0) {
            gms.mTickables.add(this);
            gms.mDragables.add(this);
        }
    }

    public RectF getHitBox(GameSettings gms) {
        if (this.mBox == null) {
            this.mBox = new RectF(0.0f, gms.mTop, gms.mWidth, gms.mHeight);
        }
        return this.mBox;
    }

    public boolean hit(float x, float y, GameSettings gms, int h) {
        if (this.mRemoved) {
            return false;
        }
        this.mX = x;
        this.mY = y;
        return false;
    }
}
