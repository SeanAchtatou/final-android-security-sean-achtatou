package pop.em.up.powerups;

import pop.em.up.GameSettings;
import pop.em.up.abstracts.GameTickable;

public class TripleTap implements GameTickable {
    static TripleTap mCurrent = null;
    int mBound;
    int mCount = 0;
    boolean mRemoved = false;

    public TripleTap(float duration, GameSettings s) {
        PowerUp.mPowerUps = true;
        s.getClass();
        this.mBound = (int) ((1000.0f * duration) / 33.0f);
        if (mCurrent == null) {
            mCurrent = this;
            return;
        }
        mCurrent.mBound += this.mBound;
        this.mBound = -1;
    }

    public boolean tick(GameSettings gms) {
        this.mCount++;
        if (this.mCount <= this.mBound) {
            return false;
        }
        this.mRemoved = true;
        mCurrent = null;
        if (!PowerUp.mPowerUps) {
            return false;
        }
        gms.mDmg -= 2;
        return false;
    }

    public boolean scheduleRemoval() {
        return this.mRemoved;
    }

    public void addSelf(GameSettings gms) {
        if (this.mBound >= 0) {
            gms.mTickables.add(this);
            gms.mDmg += 2;
        }
    }
}
