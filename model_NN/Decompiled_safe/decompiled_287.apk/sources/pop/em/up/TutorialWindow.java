package pop.em.up;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;

public class TutorialWindow extends PopupWindow {
    ParagraphTextPainter mPnter;

    public TutorialWindow(GameSettings gms, String s, Paint p) {
        super(gms);
        this.mWidth = 0.8f;
        this.mHeight = 0.6f;
        this.mPriority = 2;
        RectF r = new RectF();
        r.left = ((gms.mWidth / 2.0f) - ((gms.mWidth * this.mWidth) / 2.0f)) + 5.0f;
        r.right = ((gms.mWidth / 2.0f) + ((gms.mWidth * this.mWidth) / 2.0f)) - 8.0f;
        r.top = (gms.mHeight / 2.0f) - ((gms.mHeight * this.mHeight) / 2.0f);
        r.bottom = (gms.mHeight / 2.0f) + ((gms.mHeight * this.mHeight) / 2.0f);
        this.mPnter = new ParagraphTextPainter(s, p, r, gms);
    }

    public boolean draw(Canvas c, GameSettings gms, Paint p) {
        super.draw(c, gms, p);
        if (this.mRct.width() < 10.0f) {
            return false;
        }
        c.save();
        c.clipRect(this.mRct);
        this.mPnter.paintText(c, p);
        c.restore();
        return false;
    }

    public void deInit() {
    }

    public void onFinish(GameSettings gms) {
    }

    public boolean hit(float x, float y, GameSettings gms) {
        if (!fullSize(gms)) {
            return false;
        }
        this.mExpanding = false;
        return true;
    }
}
