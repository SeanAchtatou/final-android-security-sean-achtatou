package pop.em.up;

import android.app.Application;

public class BalloonPopperApp extends Application {
    GameEngine mEngine;

    public void onTerminate() {
        super.onTerminate();
        this.mEngine.mSettings.mEnded = true;
    }

    public void onCreate() {
        super.onCreate();
        this.mEngine = new GameEngine();
        this.mEngine.initialize();
    }
}
