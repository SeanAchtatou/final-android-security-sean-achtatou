package pop.em.up;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import java.util.ArrayList;
import java.util.Iterator;

public class ParagraphTextPainter {
    float mOff = 0.0f;
    ArrayList<String> mTxt;
    float mTxtSize = 0.0f;
    float mX = 0.0f;

    public ParagraphTextPainter(String str, Paint p, RectF r, GameSettings s) {
        this.mTxtSize = s.mTextSize * 1.2f;
        this.mTxt = new ArrayList<>();
        int lastSpace = 0;
        int start = 0;
        p.setTextSize(this.mTxtSize);
        for (int i = 0; i < str.length(); i++) {
            lastSpace = str.charAt(i) == ' ' ? i : lastSpace;
            if (p.measureText(str, start, i) > r.width()) {
                this.mTxt.add(str.substring(start, lastSpace));
                start = lastSpace + 1;
            }
            if (str.charAt(i) == 10) {
                this.mTxt.add(str.substring(start, i));
                start = i + 1;
                lastSpace = start;
            }
        }
        this.mX = r.left;
        this.mOff = r.top + this.mTxtSize;
        if (start != str.length()) {
            this.mTxt.add(str.substring(start));
        }
    }

    public void paintText(Canvas c, Paint p) {
        p.setTextSize(this.mTxtSize);
        p.setTextAlign(Paint.Align.LEFT);
        float off = 0.0f;
        Iterator<String> it = this.mTxt.iterator();
        while (it.hasNext()) {
            c.drawText(it.next(), this.mX, this.mOff + off, p);
            off += this.mTxtSize;
        }
    }
}
