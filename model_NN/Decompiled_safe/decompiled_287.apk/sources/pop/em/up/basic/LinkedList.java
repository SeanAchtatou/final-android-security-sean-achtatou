package pop.em.up.basic;

public class LinkedList<O> {
    LinkedNode<O> cursor = null;
    LinkedNode<O> root = null;
    LinkedNode<O> tail = null;

    public O getCursor() {
        return this.cursor.data;
    }

    public void add(O obj) {
        LinkedNode<O> node = new LinkedNode<>(obj);
        if (this.root != null) {
            this.root.prev = node;
            node.next = this.root;
        } else {
            this.tail = node;
        }
        this.root = node;
    }

    public void addAfterCursor(O obj) {
        LinkedNode<O> node = new LinkedNode<>(obj);
        if (this.cursor != null) {
            node.prev = this.cursor;
            node.next = this.cursor.next;
            this.cursor.next = node;
            if (this.cursor == this.tail) {
                this.tail = node;
            }
        }
    }

    public void addToEnd(O obj) {
        LinkedNode<O> node = new LinkedNode<>(obj);
        if (this.tail != null) {
            this.tail.next = node;
            node.prev = this.tail;
        } else {
            this.root = node;
        }
        this.tail = node;
    }

    public void reset() {
        this.cursor = this.root;
    }

    public boolean hasNext() {
        return this.cursor != null;
    }

    public void next() {
        this.cursor = this.cursor.next;
    }

    public boolean isEmpty() {
        return this.root == null;
    }

    public void removeCursor() {
        if (this.cursor != null) {
            if (this.cursor.next != null) {
                this.cursor.next.prev = this.cursor.prev;
            }
            if (this.cursor.prev != null) {
                this.cursor.prev.next = this.cursor.next;
            }
            if (this.root == this.cursor) {
                this.root = this.cursor.next;
            }
            if (this.cursor == this.tail) {
                this.tail = this.cursor.prev;
            }
            this.cursor = this.cursor.next;
        }
    }
}
