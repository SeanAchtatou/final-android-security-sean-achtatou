package pop.em.up.basic;

public class LinkedNode<O> {
    O data;
    LinkedNode<O> next;
    LinkedNode<O> prev;

    public LinkedNode(O obj) {
        this.data = obj;
    }
}
