package pop.em.up;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.view.MotionEvent;
import android.widget.TextView;
import java.util.ArrayList;
import pop.em.up.abstracts.GameDrawable;
import pop.em.up.abstracts.GameTickable;
import pop.em.up.abstracts.Hitbox;
import pop.em.up.balloons.Balloon;
import pop.em.up.basic.LinkedList;
import pop.em.up.doodads.Cloud;
import pop.em.up.engines.Engine;
import pop.em.up.uiwidget.UIDock;

public class GameSettings {
    public Paint BGColor;
    public Paint BGOutline;
    public final int ENGINEDELAY = 33;
    private final int ENGINEFPS = 30;
    public final int GRAPHICSDELAY = 33;
    private final int GRAPHICSFPS = 30;
    public Activity mAct;
    Bitmap mBG;
    public LinkedList<GameDrawable> mBackground = new LinkedList<>();
    public ArrayList<Balloon> mBalloons = new ArrayList<>();
    public float mDeviceScale = 0.0f;
    public int mDmg = 1;
    UIDock mDock = new UIDock();
    public LinkedList<Hitbox> mDragables = new LinkedList<>();
    public LinkedList<GameDrawable> mDrawables = new LinkedList<>();
    public boolean mEnded = false;
    public Engine mEngine;
    public LinkedList<GameDrawable> mForeground = new LinkedList<>();
    GameView mGV;
    public float mHeight = 0.0f;
    public LinkedList<Hitbox> mHittables = new LinkedList<>();
    public boolean mPaused = false;
    public boolean mRecord = true;
    public float mScale = 1.0f;
    RectF mScreen = null;
    public GameStats mStats = new GameStats();
    public boolean mStop = false;
    public Paint mTextColor;
    public float mTextSize;
    public LinkedList<GameTickable> mTickables = new LinkedList<>();
    public float mTimeScale = 1.0f;
    public float mTop = 50.0f;
    public float mWidth = 0.0f;
    public RectF upperRect;

    public int durationtoTicks(float time) {
        return (int) ((1000.0f * time) / 33.0f);
    }

    public void resetStats() {
        this.mStats = new GameStats();
    }

    public void pause(boolean pause) {
        this.mPaused = pause;
        if (this.mGV != null) {
            this.mGV.post(new Runnable() {
                public void run() {
                    GameSettings.this.mGV.setKeepScreenOn(!GameSettings.this.mPaused);
                }
            });
        }
    }

    public void end(Activity a) {
        save(a);
    }

    public void setup(Context c) {
        this.upperRect = new RectF(-1.0f, -1.0f, this.mWidth, this.mTop);
        this.mTextSize = new TextView(c).getTextSize();
        this.BGColor = new Paint();
        this.BGColor.setARGB(255, 0, 0, 0);
        this.BGOutline = new Paint();
        this.BGOutline.setStyle(Paint.Style.STROKE);
        this.BGOutline.setStrokeWidth(5.0f);
        this.BGOutline.setARGB(255, 50, 50, 50);
        this.mTextColor = new Paint();
        this.mTextColor.setColor(-1);
        this.mTextColor.setTextSize(16.0f);
        this.mTextColor.setAntiAlias(true);
        this.mScreen = new RectF(0.0f, 0.0f, this.mWidth, this.mHeight);
    }

    public void createCloud(boolean bg) {
        Cloud.mKillClouds = false;
        new Cloud(this, bg);
    }

    public void createBalloon(int i) {
        Balloon.mKillBalloons = false;
        Balloon type = Balloon.mTypes[i].clone(this);
        type.x = (Balloon.mScaleX * type.mScale * this.mScale) + ((float) (((double) (this.mWidth - ((Balloon.mOrigWidth * type.mScale) * this.mScale))) * Math.random()));
        type.y = this.mHeight + (2.0f * Balloon.mScaleY * type.mScale * this.mScale);
        type.addSelf(this);
        this.mBalloons.add(type);
    }

    public void drawGUIOverlay(Canvas c) {
        c.drawRect(this.upperRect, this.BGColor);
        c.drawRect(this.upperRect, this.BGOutline);
        this.mDock.drawUI(this, c);
    }

    public void destroyOnScreen() {
        Cloud.mKillClouds = true;
        Balloon.mKillBalloons = true;
    }

    public void drawCanvas(Canvas c) {
        if (this.mBG != null) {
            c.drawBitmap(this.mBG, (Rect) null, this.mScreen, (Paint) null);
        }
        if (!this.mEngine.draw(c, this)) {
            this.mBackground.reset();
            while (this.mBackground.hasNext()) {
                GameDrawable curr = this.mBackground.getCursor();
                curr.draw(c, this);
                if (curr.scheduleRemoval()) {
                    this.mBackground.removeCursor();
                } else {
                    this.mBackground.next();
                }
            }
            this.mDrawables.reset();
            while (this.mDrawables.hasNext()) {
                GameDrawable curr2 = this.mDrawables.getCursor();
                curr2.draw(c, this);
                if (curr2.scheduleRemoval()) {
                    this.mDrawables.removeCursor();
                } else {
                    this.mDrawables.next();
                }
            }
            this.mForeground.reset();
            while (this.mForeground.hasNext()) {
                GameDrawable curr3 = this.mForeground.getCursor();
                curr3.draw(c, this);
                if (curr3.scheduleRemoval()) {
                    this.mForeground.removeCursor();
                } else {
                    this.mForeground.next();
                }
            }
            drawGUIOverlay(c);
            this.mEngine.drawOnTop(c, this);
        }
    }

    public void checkHit(float x, float y) {
        if ((this.mEngine == null || !this.mEngine.hit(x, y, this)) && !this.mDock.checkUI(this, x, y) && !this.upperRect.contains(x, y) && !this.mPaused) {
            if (this.mRecord) {
                this.mStats.touch(this);
            }
            this.mHittables.reset();
            while (this.mHittables.hasNext()) {
                boolean b = false;
                Hitbox curr = this.mHittables.getCursor();
                if (curr.getHitBox(this).contains(x, y)) {
                    b = curr.hit(x, y, this, this.mDmg);
                }
                if (curr.scheduleRemoval()) {
                    this.mHittables.removeCursor();
                    continue;
                } else {
                    this.mHittables.next();
                    continue;
                }
                if (b) {
                    if (this.mRecord) {
                        this.mStats.hit(x, y, this);
                        return;
                    }
                    return;
                }
            }
            if (this.mRecord) {
                this.mStats.miss(x, y, this);
            }
        }
    }

    public boolean checkDrag(MotionEvent e, float x, float y) {
        if (this.mPaused) {
            return false;
        }
        this.mDragables.reset();
        while (this.mDragables.hasNext()) {
            boolean b = false;
            Hitbox curr = this.mDragables.getCursor();
            if (curr.getHitBox(this).contains(x, y)) {
                b = curr.hit(x, y, this, this.mDmg);
            }
            if (curr.scheduleRemoval()) {
                this.mDragables.removeCursor();
                continue;
            } else {
                this.mDragables.next();
                continue;
            }
            if (b) {
                return true;
            }
        }
        return false;
    }

    public void tick() {
        this.mTickables.reset();
        while (this.mTickables.hasNext()) {
            GameTickable curr = this.mTickables.getCursor();
            curr.tick(this);
            if (curr.scheduleRemoval()) {
                this.mTickables.removeCursor();
            } else {
                this.mTickables.next();
            }
        }
    }

    public void setupBounds(float w, float h, Context c) {
        this.mWidth = w;
        this.mHeight = h;
        this.mDeviceScale = 1.0f;
        this.mTop = (50.0f * h) / 295.0f;
        setup(c);
    }

    public void save(Activity a) {
        if (this.mEngine != null) {
            this.mEngine.save(a, this);
        }
    }

    public void loadMenu() {
    }
}
