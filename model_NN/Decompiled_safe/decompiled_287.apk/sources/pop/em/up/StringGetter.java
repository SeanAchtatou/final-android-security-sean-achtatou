package pop.em.up;

public interface StringGetter {
    String getFinalString();

    String getString();
}
