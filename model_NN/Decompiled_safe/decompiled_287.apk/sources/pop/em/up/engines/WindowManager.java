package pop.em.up.engines;

import android.graphics.Canvas;
import android.graphics.Paint;
import java.util.Stack;
import pop.em.up.GameSettings;
import pop.em.up.PopupWindow;

public class WindowManager {
    public static Paint mPnt = new Paint();
    private Stack<PopupWindow> mWnds = new Stack<>();

    static {
        mPnt.setAntiAlias(true);
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public PopupWindow topWindow() {
        if (!windowsLeft()) {
            return null;
        }
        return this.mWnds.peek();
    }

    public boolean windowsLeft() {
        return !this.mWnds.isEmpty();
    }

    public boolean draw(Canvas c, GameSettings gms) {
        mPnt.setARGB(255, 155, 155, 155);
        c.drawARGB(100, 0, 0, 0);
        for (int i = 0; i < this.mWnds.size(); i++) {
            this.mWnds.get(i).draw(c, gms, mPnt);
        }
        return false;
    }

    public boolean tick(GameSettings gms) {
        if (this.mWnds.peek().finished(gms)) {
            removeTop(gms);
        }
        for (int i = 0; i < this.mWnds.size(); i++) {
            if (this.mWnds.get(i).tick(gms)) {
                return false;
            }
        }
        return false;
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public boolean hit(float x, float y, GameSettings gms) {
        if (!windowsLeft()) {
            return false;
        }
        this.mWnds.peek().hit(x, y, gms);
        return true;
    }

    public void addWnd(PopupWindow wnd) {
        for (int i = this.mWnds.size() - 1; i >= 0; i--) {
            if (this.mWnds.get(i).mPriority >= wnd.mPriority) {
                this.mWnds.add(i, wnd);
                return;
            }
        }
        this.mWnds.add(wnd);
    }

    private void removeTop(GameSettings gms) {
        PopupWindow wnd = this.mWnds.pop();
        wnd.onFinish(gms);
        wnd.deInit();
    }
}
