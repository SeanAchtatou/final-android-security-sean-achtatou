package pop.em.up.engines;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import pop.em.up.GameSettings;
import pop.em.up.abstracts.GameDrawable;
import pop.em.up.abstracts.GameTickable;
import pop.em.up.loaders.Loader;
import pop.em.up.uiwidget.UIDock;

public abstract class Engine implements GameTickable, GameDrawable {
    public WindowManager mWnds = new WindowManager();

    public abstract void GameOver(GameSettings gameSettings);

    public abstract void drawOnTop(Canvas canvas, GameSettings gameSettings);

    public abstract boolean hit(float f, float f2, GameSettings gameSettings);

    public abstract void load(Context context, GameSettings gameSettings, Loader loader);

    public abstract boolean onBack(GameSettings gameSettings);

    public abstract void save(Activity activity, GameSettings gameSettings);

    public abstract void setupInterface(UIDock uIDock, GameSettings gameSettings);
}
