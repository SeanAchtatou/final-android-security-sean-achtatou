package pop.em.up.engines;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import pop.em.up.BackWindow;
import pop.em.up.GameOverWindow;
import pop.em.up.GameSettings;
import pop.em.up.LoadTask;
import pop.em.up.R;
import pop.em.up.StatisticsWindow;
import pop.em.up.balloons.Balloon;
import pop.em.up.doodads.BalloonFloatingBitmap;
import pop.em.up.doodads.Cloud;
import pop.em.up.levels.Levels;
import pop.em.up.levels.MiscEvents;
import pop.em.up.loaders.BackgroundLoader;
import pop.em.up.loaders.Loader;
import pop.em.up.powerups.PowerUp;
import pop.em.up.uiwidget.LevelStats1;
import pop.em.up.uiwidget.LevelStats2;
import pop.em.up.uiwidget.PauseButton;
import pop.em.up.uiwidget.Statistics;
import pop.em.up.uiwidget.UIDock;

public class LevelEngine extends Engine {
    public static final String LEVELKEY = "LEVEL";
    public static final String SAVEKEY = "LEVELENGINE";
    public static final String SCOREKEY = "SCORE";
    private static final int SPACING = 7;
    Bitmap E;
    Bitmap L;
    Bitmap V;
    public int mAnimProgress = 0;
    private int mCount = 0;
    int[] mDigits;
    public Bitmap mGameover;
    public int mLevel = 0;
    public boolean mLevelDisplay = true;
    public int mMax = 0;
    Bitmap[] mNumbers = new Bitmap[10];
    public float mOffset = -50.0f;
    BalloonFloatingBitmap[] mOrder;
    float mScale = 1.0f;
    public Levels[] mSet = Levels.values();
    public float mTop = 0.0f;

    public int balloonsLeft() {
        return this.mSet[this.mLevel].balloonsLeft();
    }

    public boolean tick(GameSettings gms) {
        if (this.mWnds.windowsLeft()) {
            this.mWnds.tick(gms);
            return true;
        }
        if (this.mAnimProgress != -1) {
            if (this.mCount >= SPACING) {
                if (this.mAnimProgress < this.mOrder.length) {
                    this.mAnimProgress++;
                }
                this.mCount = 0;
            }
            this.mCount++;
            for (int i = 0; i < this.mAnimProgress; i++) {
                this.mOrder[i].tick(gms);
            }
            if (this.mOrder[this.mOrder.length - 1].finished()) {
                this.mAnimProgress = -1;
            }
        } else {
            this.mSet[this.mLevel].tick(gms);
            if (!this.mSet[this.mLevel].anyLeft() && !gms.mStats.balloonsOnScreen()) {
                EndofLevel(gms);
            }
        }
        return false;
    }

    public void EndofLevel(GameSettings gms) {
        gms.destroyOnScreen();
        gms.mBalloons.clear();
        gms.mDmg = 1;
        gms.mStats.mScore += (long) (gms.mStats.mLives * (this.mLevel + 1) * 10);
        gms.mRecord = false;
        this.mWnds.addWnd(new StatisticsWindow(gms, this, WindowManager.mPnt));
    }

    public boolean scheduleRemoval() {
        return false;
    }

    public void startLevel(int level, GameSettings gms, boolean retry) {
        this.mSet[level].reset(gms);
        Cloud.mKillClouds = true;
        Balloon.mKillBalloons = true;
        PowerUp.mPowerUps = false;
        this.mLevel = level;
        if (this.mLevel > this.mMax) {
            this.mMax = this.mLevel;
        }
        final GameSettings gameSettings = gms;
        new Thread(new Runnable() {
            public void run() {
                LevelEngine.this.save(gameSettings);
            }
        }).start();
        int places = 0;
        int lvl = level + 1;
        while (lvl > 0) {
            lvl /= 10;
            places++;
        }
        int level2 = level + 1;
        this.mDigits = new int[places];
        int c = 0;
        while (level2 > 0) {
            this.mDigits[(places - c) - 1] = level2 % 10;
            level2 /= 10;
            c++;
        }
        this.mScale = (gms.mWidth * 0.8f) / ((0.75f * ((float) this.L.getHeight())) * ((float) (places + 6)));
        if ((((float) this.L.getHeight()) * this.mScale) / gms.mHeight > 0.33f) {
            this.mScale = (0.33f * (gms.mHeight - gms.mTop)) / ((float) this.L.getHeight());
        }
        this.mTop = (gms.mHeight - gms.mTop) / 2.0f;
        BackgroundLoader backgroundLoader = new BackgroundLoader();
        loadLevel(backgroundLoader);
        this.mOrder = new BalloonFloatingBitmap[(places + 5)];
        this.mOrder[0] = new BalloonFloatingBitmap(this.L, 30, (this.mOrder.length * SPACING) + 10, 10, this.mTop, this.mScale, backgroundLoader);
        this.mOrder[1] = new BalloonFloatingBitmap(this.E, 30, (this.mOrder.length * SPACING) + 10, 10, this.mTop, this.mScale, backgroundLoader);
        this.mOrder[2] = new BalloonFloatingBitmap(this.V, 30, (this.mOrder.length * SPACING) + 10, 10, this.mTop, this.mScale, backgroundLoader);
        this.mOrder[3] = new BalloonFloatingBitmap(this.E, 30, (this.mOrder.length * SPACING) + 10, 10, this.mTop, this.mScale, backgroundLoader);
        this.mOrder[4] = new BalloonFloatingBitmap(this.L, 30, (this.mOrder.length * SPACING) + 10, 10, this.mTop, this.mScale, backgroundLoader);
        for (int i = 0; i < this.mDigits.length; i++) {
            this.mOrder[i + 5] = new BalloonFloatingBitmap(this.mNumbers[this.mDigits[i]], 30, (this.mOrder.length * SPACING) + 10, 10, this.mTop, this.mScale, backgroundLoader);
        }
        this.mAnimProgress = 0;
        float mWidth = 0.0f;
        for (int i2 = 0; i2 < this.mOrder.length; i2++) {
            mWidth += 1.5f * this.mOrder[i2].mScaleX * this.mScale;
            if (i2 == 4) {
                mWidth = (float) (((double) mWidth) + (1.5d * ((double) this.mOrder[i2].mScaleX) * ((double) this.mScale)));
            }
        }
        float start = (((gms.mWidth - mWidth) - (this.mOrder[0].mScaleX * this.mScale)) / 2.0f) + (this.mOrder[0].mScaleX * this.mScale);
        for (int i3 = 0; i3 < this.mOrder.length; i3++) {
            this.mOrder[i3].mX = start;
            start += 1.5f * this.mOrder[i3].mScaleX * this.mScale;
            if (i3 == 4) {
                start += 1.5f * this.mOrder[i3].mScaleX * this.mScale;
            }
        }
        final BackgroundLoader backgroundLoader2 = backgroundLoader;
        final GameSettings gameSettings2 = gms;
        new Thread(new Runnable() {
            public void run() {
                backgroundLoader2.load(0.0f, 0.0f, gameSettings2.mAct, gameSettings2);
            }
        }).start();
        gms.mStats.resetBetweenLevels();
        gms.mScale = (float) this.mSet[this.mLevel].changeScale(1.0d);
        gms.mTimeScale = (float) this.mSet[this.mLevel].changeTimeScale(1.0d);
        if (!retry && this.mLevel == this.mMax && this.mSet[this.mLevel].cTutorial != -1) {
            Tutorials.show(this.mSet[this.mLevel].cTutorial, this.mWnds, gms, WindowManager.mPnt);
        }
    }

    public boolean draw(Canvas c, GameSettings gms) {
        if (this.mAnimProgress == -1 || this.mAnimProgress > this.mOrder.length) {
            return false;
        }
        for (int i = 1; i <= this.mAnimProgress; i++) {
            this.mOrder[this.mAnimProgress - i].draw(c, gms);
        }
        return false;
    }

    public void load(Context c, GameSettings gms, Loader load) {
        SharedPreferences settings = c.getSharedPreferences(SAVEKEY, 0);
        int i = settings.getInt(LEVELKEY, 0);
        this.mLevel = i;
        this.mMax = i;
        gms.mStats.mScore = settings.getLong(SCOREKEY, 0);
        load.mTasks.add(new LoadTask() {
            public void load(Context c) {
                if (LevelEngine.this.mGameover == null) {
                    LevelEngine.this.mGameover = BitmapFactory.decodeResource(c.getResources(), R.drawable.gameover);
                }
                if (LevelEngine.this.L == null) {
                    LevelEngine.this.L = BitmapFactory.decodeResource(c.getResources(), R.drawable.l);
                }
                if (LevelEngine.this.E == null) {
                    LevelEngine.this.E = BitmapFactory.decodeResource(c.getResources(), R.drawable.e);
                }
                if (LevelEngine.this.V == null) {
                    LevelEngine.this.V = BitmapFactory.decodeResource(c.getResources(), R.drawable.v);
                }
                if (LevelEngine.this.mNumbers[0] == null) {
                    LevelEngine.this.mNumbers[0] = BitmapFactory.decodeResource(c.getResources(), R.drawable.n0);
                }
                if (LevelEngine.this.mNumbers[1] == null) {
                    LevelEngine.this.mNumbers[1] = BitmapFactory.decodeResource(c.getResources(), R.drawable.n1);
                }
                if (LevelEngine.this.mNumbers[2] == null) {
                    LevelEngine.this.mNumbers[2] = BitmapFactory.decodeResource(c.getResources(), R.drawable.n2);
                }
                if (LevelEngine.this.mNumbers[3] == null) {
                    LevelEngine.this.mNumbers[3] = BitmapFactory.decodeResource(c.getResources(), R.drawable.n3);
                }
                if (LevelEngine.this.mNumbers[4] == null) {
                    LevelEngine.this.mNumbers[4] = BitmapFactory.decodeResource(c.getResources(), R.drawable.n4);
                }
                if (LevelEngine.this.mNumbers[5] == null) {
                    LevelEngine.this.mNumbers[5] = BitmapFactory.decodeResource(c.getResources(), R.drawable.n5);
                }
                if (LevelEngine.this.mNumbers[6] == null) {
                    LevelEngine.this.mNumbers[6] = BitmapFactory.decodeResource(c.getResources(), R.drawable.n6);
                }
                if (LevelEngine.this.mNumbers[LevelEngine.SPACING] == null) {
                    LevelEngine.this.mNumbers[LevelEngine.SPACING] = BitmapFactory.decodeResource(c.getResources(), R.drawable.n7);
                }
                if (LevelEngine.this.mNumbers[8] == null) {
                    LevelEngine.this.mNumbers[8] = BitmapFactory.decodeResource(c.getResources(), R.drawable.n8);
                }
                if (LevelEngine.this.mNumbers[9] == null) {
                    LevelEngine.this.mNumbers[9] = BitmapFactory.decodeResource(c.getResources(), R.drawable.n9);
                }
            }

            public boolean isLoaded() {
                return (LevelEngine.this.mGameover == null || LevelEngine.this.L == null || LevelEngine.this.E == null || LevelEngine.this.V == null || LevelEngine.this.mNumbers[0] == null || LevelEngine.this.mNumbers[1] == null || LevelEngine.this.mNumbers[2] == null || LevelEngine.this.mNumbers[3] == null || LevelEngine.this.mNumbers[4] == null || LevelEngine.this.mNumbers[5] == null || LevelEngine.this.mNumbers[6] == null || LevelEngine.this.mNumbers[LevelEngine.SPACING] == null || LevelEngine.this.mNumbers[8] == null || LevelEngine.this.mNumbers[9] == null) ? false : true;
            }

            public void finished(GameSettings s) {
                LevelEngine.this.startLevel(LevelEngine.this.mLevel, s, false);
            }
        });
    }

    public void setupInterface(UIDock ui, GameSettings gms) {
        ui.addWidget(new LevelStats1(this));
        ui.addWidget(new LevelStats2(this));
        ui.addWidget(new Statistics());
        ui.addWidget(new PauseButton());
        ui.readyLayout(gms);
    }

    public void addSelf(GameSettings gms) {
        gms.mEngine = this;
    }

    public void GameOver(GameSettings gms) {
        gms.mRecord = false;
        this.mWnds.addWnd(new GameOverWindow(gms, this, WindowManager.mPnt));
    }

    public void save(Activity a, GameSettings gms) {
        SharedPreferences.Editor editor = a.getSharedPreferences(SAVEKEY, 0).edit();
        editor.putLong(SCOREKEY, gms.mStats.mLastScore);
        editor.putInt(LEVELKEY, this.mMax);
        editor.commit();
    }

    public void save(GameSettings gms) {
        if (gms.mAct != null) {
            SharedPreferences.Editor editor = gms.mAct.getSharedPreferences(SAVEKEY, 0).edit();
            editor.putLong(SCOREKEY, gms.mStats.mLastScore);
            editor.putInt(LEVELKEY, this.mMax);
            editor.commit();
        }
    }

    public boolean onBack(GameSettings gms) {
        if (this.mWnds.topWindow() instanceof BackWindow) {
            return true;
        }
        gms.mRecord = false;
        if (gms.mPaused) {
            gms.pause(false);
        }
        this.mWnds.addWnd(new BackWindow(gms, this, WindowManager.mPnt));
        return false;
    }

    public boolean hit(float x, float y, GameSettings gms) {
        return this.mWnds.hit(x, y, gms);
    }

    public void drawOnTop(Canvas c, GameSettings gms) {
        if (this.mWnds.windowsLeft()) {
            this.mWnds.draw(c, gms);
        }
    }

    public void loadLevel(Loader l) {
        for (int i = 0; i < this.mSet[this.mLevel].cTypes.length; i += 2) {
            Balloon.mTypes[this.mSet[this.mLevel].cTypes[i]].load(l);
        }
        for (int i2 = 0; i2 < this.mSet[this.mLevel].cMisc.length; i2++) {
            if (this.mSet[this.mLevel].cMisc[i2] > 0) {
                MiscEvents.loadType(i2, l);
            }
        }
        PowerUp.loadPowerUps(this.mLevel, l);
    }
}
