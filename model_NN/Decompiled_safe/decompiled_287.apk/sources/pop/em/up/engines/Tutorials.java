package pop.em.up.engines;

import android.graphics.Paint;
import pop.em.up.GameSettings;
import pop.em.up.TutorialWindow;

public class Tutorials {
    static String[] tutorialTexts = {"The game is pretty simple enjoy some balloon carnage by popping balloons! You can pop them by tapping them! Initially there are only a few balloons, however as you progress you'll see alot more and they'll make the game difficult in different ways.Enjoy!\nRed Balloon\nHP=1\nLives Lost(if it gets past):1\n(Touch screen to advance)", "Power Up Balloon\nHP=3\nLives Lost:0\nOther:Gives you a power up!The first power up you get is a size up power up which increases the size of the balloons making them easier to hit. As you progress you'll get more powerups to use.", "Some of the clouds in the background are going to start flying in front of the balloons now. This just makes it a little more annoying to hit them. Although you can hit balloons through clouds. ", "Blue Balloon\nHP=3\nLives Lost:1\nOther:Shrinks every time you hit it.", "Slow-Mo Power Up\nSlows everything down making it easier to hit. It also can stack with itself.", "Pink Balloon\nHP=1\nLives Lost:1\nOther:Increases the hit points of every other balloon on the screen by 1 when its popped.", "Flame-Thrower Power Up\n Super strong power up. Tap or drag the screen to aim the target. Anything under the center of the flame will take damage. Getting another power up extends the duration.", "Purple Teleport Balloon\nHP=4\nLives Lost:1\nOther:Every time its hit it teleports to a different point on the screen. (However it will only appear below its current height.)", "Tank Balloon\nHP=10\nLives Lost:1\nOther:Super large balloon that increases in size every time you hit it.", "Triple Damage Power Up\nWhen you get the power up it increases the amount of damage your touch does by 2. Getting it again increases the duration.", "Damage Reflector Balloon\nHP=1\nLives Lost:2\nOther:When it's hit it reflects the damage to the balloon with the greatest health and does 1 extra damage with it.", "For the next 5 levels, we are going to be revisiting some of the old types of balloons and mixing them together to make the levels harder.You will also see a new balloon type:\nJoker Balloon\nHP=1\nOther:It gives you an extra life for free. But there is a chance that by hitting it you will speed up all the balloons for the rest of the level or decrease the size for the rest of the level. If this happens you will gain an extra life on top of the first.", "This is the last level and it's impossible to beat. Probably. Try and prove me wrong. Thanks for playing! More levels are coming soon! Comment and tell me how it was! Also if you have any suggestions I'd be happy to hear them!"};

    public static void show(int cTutorial, WindowManager mWnds, GameSettings gms, Paint p) {
        if (cTutorial >= 0) {
            mWnds.addWnd(new TutorialWindow(gms, tutorialTexts[cTutorial], p));
        }
    }
}
