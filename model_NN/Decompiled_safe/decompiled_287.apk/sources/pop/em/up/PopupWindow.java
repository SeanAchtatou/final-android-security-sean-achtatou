package pop.em.up;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import pop.em.up.abstracts.GameTickable;

public abstract class PopupWindow implements GameTickable {
    private static float mExpand = 10.0f;
    boolean mExpanding = true;
    float mHeight = 0.3f;
    public int mPriority = 1;
    RectF mRct;
    float mWidth = 0.5f;

    public abstract void deInit();

    public abstract boolean hit(float f, float f2, GameSettings gameSettings);

    public abstract void onFinish(GameSettings gameSettings);

    public PopupWindow(GameSettings gms) {
        this.mRct = new RectF(gms.mWidth / 2.0f, gms.mHeight / 2.0f, gms.mWidth / 2.0f, gms.mHeight / 2.0f);
        mExpand = gms.mWidth * 0.05f;
    }

    public boolean fullSize(GameSettings gms) {
        return gms.mWidth * this.mWidth <= this.mRct.width() && gms.mHeight * this.mHeight <= this.mRct.height();
    }

    public boolean closed(GameSettings gms) {
        return 0.0f >= this.mRct.width() && 0.0f >= this.mRct.height();
    }

    public boolean finished(GameSettings gms) {
        return closed(gms) && !this.mExpanding;
    }

    public boolean draw(Canvas c, GameSettings gms, Paint p) {
        c.drawRect(this.mRct, gms.BGColor);
        c.drawRect(this.mRct, gms.BGOutline);
        return false;
    }

    public boolean tick(GameSettings gms) {
        if (this.mExpanding) {
            if (gms.mWidth * this.mWidth > this.mRct.width()) {
                this.mRct.left -= mExpand / 2.0f;
                this.mRct.right += mExpand / 2.0f;
            }
            if (gms.mHeight * this.mHeight <= this.mRct.height()) {
                return false;
            }
            this.mRct.top -= mExpand / 2.0f;
            this.mRct.bottom += mExpand / 2.0f;
            return false;
        }
        if (this.mRct.width() > 0.0f) {
            this.mRct.left += mExpand / 2.0f;
            this.mRct.right -= mExpand / 2.0f;
        }
        if (this.mRct.height() <= 0.0f) {
            return false;
        }
        this.mRct.top += mExpand / 2.0f;
        this.mRct.bottom -= mExpand / 2.0f;
        return false;
    }

    public boolean scheduleRemoval() {
        return false;
    }

    public void addSelf(GameSettings gms) {
    }
}
