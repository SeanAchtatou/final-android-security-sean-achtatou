package pop.em.up;

public class StaticString implements StringGetter {
    String mTxt;

    public StaticString(String txt) {
        this.mTxt = txt;
    }

    public String getString() {
        return this.mTxt;
    }

    public String getFinalString() {
        return this.mTxt;
    }
}
