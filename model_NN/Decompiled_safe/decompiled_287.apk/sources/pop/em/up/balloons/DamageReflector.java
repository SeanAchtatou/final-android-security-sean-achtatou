package pop.em.up.balloons;

import android.content.Context;
import android.graphics.Bitmap;
import java.util.Iterator;
import pop.em.up.GameSettings;
import pop.em.up.LoadTask;
import pop.em.up.doodads.Pop;
import pop.em.up.doodads.ReflectionAnimation;
import pop.em.up.loaders.Loader;

public class DamageReflector extends Balloon {
    public static int key = 7;
    public static Bitmap mImage;

    public DamageReflector() {
    }

    public DamageReflector(GameSettings s) {
        super(s);
        this.mScale = Balloon.mOrigScale * 0.75f;
        this.mScore = 100;
        this.hp = 1;
        setSpeed(10.0f + ((float) (5.0d * Math.random())), s);
    }

    public boolean hit(float x, float y, GameSettings gms, int h) {
        int max = 0;
        Balloon balloon = null;
        boolean bool = super.hit(x, y, gms, h);
        synchronized (gms.mBalloons) {
            Iterator<Balloon> it = gms.mBalloons.iterator();
            while (it.hasNext()) {
                Balloon b = it.next();
                if (b != this && !(b instanceof QuestionBalloon) && !(b instanceof PowerUpBalloon) && b.hp > max) {
                    balloon = b;
                    max = b.hp;
                }
            }
        }
        if (balloon != null) {
            new ReflectionAnimation(this.x, this.y - ((mOrigHeight * this.mConcatScale) * 0.5f), 0.5f, gms).addSelf(gms);
            if (balloon instanceof DamageReflector) {
                balloon.hp -= h + 1;
            } else {
                balloon.hit(balloon.x, balloon.y, gms, h + 1);
            }
            this.hp += h;
        }
        return bool;
    }

    public Bitmap getImage() {
        return mImage;
    }

    public void setImage(Bitmap b) {
        mImage = b;
    }

    public Balloon clone(GameSettings s) {
        return new DamageReflector(s);
    }

    public int killLives() {
        return 2;
    }

    public Balloon load(Loader l) {
        l.mTasks.add(new LoadTask() {
            public void load(Context c) {
                Balloon.load(this, 0.6313726f, 0.6313726f, 0.6313726f);
                ReflectionAnimation.load(c);
                if (!Pop.isLoaded()) {
                    Pop.load(c);
                }
            }

            public boolean isLoaded() {
                return DamageReflector.this.getImage() != null;
            }

            public void finished(GameSettings s) {
            }
        });
        return this;
    }
}
