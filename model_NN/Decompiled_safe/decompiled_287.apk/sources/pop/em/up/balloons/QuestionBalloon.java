package pop.em.up.balloons;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import pop.em.up.GameSettings;
import pop.em.up.LoadTask;
import pop.em.up.R;
import pop.em.up.doodads.Pop;
import pop.em.up.loaders.Loader;

public class QuestionBalloon extends Balloon {
    public static int key = 6;
    public static Bitmap mImage;

    public void pop(GameSettings gms) {
        super.pop(gms);
        gms.mStats.mLives++;
        double d = Math.random();
        if (d < 0.4d) {
            gms.mTimeScale *= 1.3f;
            gms.mStats.mLives++;
        } else if (d < 0.8d) {
            gms.mScale *= 0.7f;
            gms.mStats.mLives++;
        }
    }

    public QuestionBalloon() {
    }

    public QuestionBalloon(GameSettings s) {
        super(s);
        this.mScale = Balloon.mOrigScale;
        this.mScore = 0;
        this.hp = 1;
        setSpeed(5.0f + ((float) (4.0d * Math.random())), s);
    }

    public QuestionBalloon load(Context c) {
        ColorMatrix cm = new ColorMatrix();
        cm.setScale(0.0f, 0.0f, 0.0f, 1.0f);
        Paint drawPaint = new Paint();
        drawPaint.setAntiAlias(true);
        drawPaint.setColorFilter(new ColorMatrixColorFilter(cm));
        setImage(Bitmap.createBitmap((int) mOrigWidth, ((int) mOrigHeight) * 2, Bitmap.Config.ARGB_8888));
        Canvas drawer = new Canvas(getImage());
        drawer.drawBitmap(underlay, 0.0f, 0.0f, drawPaint);
        drawer.drawBitmap(BitmapFactory.decodeResource(c.getResources(), R.drawable.questionmark), 0.0f, 0.0f, mPaint);
        drawer.drawBitmap(overlay, 0.0f, 0.0f, mPaint);
        return this;
    }

    public Bitmap getImage() {
        return mImage;
    }

    public void setImage(Bitmap b) {
        mImage = b;
    }

    public Balloon clone(GameSettings s) {
        return new QuestionBalloon(s);
    }

    public int killLives() {
        return 0;
    }

    public Balloon load(Loader l) {
        l.mTasks.add(new LoadTask() {
            public void load(Context c) {
                QuestionBalloon.this.load(c);
                if (!Pop.isLoaded()) {
                    Pop.load(c);
                }
            }

            public boolean isLoaded() {
                return QuestionBalloon.this.getImage() != null;
            }

            public void finished(GameSettings s) {
            }
        });
        return this;
    }
}
