package pop.em.up.balloons;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.RectF;
import pop.em.up.GameSettings;
import pop.em.up.LoadTask;
import pop.em.up.R;
import pop.em.up.abstracts.Affliction;
import pop.em.up.abstracts.GameDrawable;
import pop.em.up.abstracts.GameTickable;
import pop.em.up.abstracts.Hitbox;
import pop.em.up.basic.LinkedList;
import pop.em.up.doodads.Pop;
import pop.em.up.loaders.Loader;

public abstract class Balloon implements GameDrawable, GameTickable, Hitbox {
    public static boolean mKillBalloons = false;
    public static float mMinScale = 0.25f;
    public static float mOrigHeight;
    public static float mOrigScale = 1.0f;
    public static float mOrigWidth;
    static Paint mPaint;
    public static float mScaleX = 0.0f;
    public static float mScaleY = 0.0f;
    public static Balloon[] mTypes = new Balloon[8];
    static Bitmap overlay;
    static Bitmap underlay;
    public int hp = 1;
    public LinkedList<Affliction> mAfflictions = new LinkedList<>();
    public float mConcatScale = 0.0f;
    float mConcatV = 0.0f;
    RectF mHitBox;
    public float mScale = 1.5f;
    float mScaleModifier = 1.0f;
    public int mScore = 0;
    float mWorldScale = 0.0f;
    float mWorldTScale = 0.0f;
    boolean removed = false;
    private float v = 0.0f;
    public float x = 0.0f;
    public float y = 0.0f;

    public abstract Balloon clone(GameSettings gameSettings);

    public abstract Bitmap getImage();

    public abstract int killLives();

    public abstract Balloon load(Loader loader);

    public abstract void setImage(Bitmap bitmap);

    public Balloon() {
    }

    public Balloon(GameSettings s) {
        s.mStats.create(this, s);
    }

    public void setSpeed(float duration, GameSettings s) {
        s.getClass();
        this.v = s.mHeight / ((float) ((int) ((1000.0f * duration) / 33.0f)));
    }

    public static void load(final GameSettings s, final Loader load) {
        load.mTasks.add(new LoadTask() {
            public void load(Context c) {
                Balloon.overlay = BitmapFactory.decodeResource(c.getResources(), R.drawable.balloonoverlay);
                Balloon.underlay = BitmapFactory.decodeResource(c.getResources(), R.drawable.balloonunderlay);
                Balloon.mPaint = new Paint();
                Balloon.mPaint.setAntiAlias(true);
                Balloon.mOrigWidth = (float) Balloon.overlay.getWidth();
                Balloon.mOrigHeight = (float) (Balloon.overlay.getHeight() / 2);
                Balloon.mOrigScale *= GameSettings.this.mDeviceScale;
                Balloon.mScaleX = Balloon.mOrigWidth / 2.0f;
                Balloon.mScaleY = Balloon.mOrigHeight;
                Balloon.mTypes[RedBalloon.key] = new RedBalloon().load(load);
            }

            public boolean isLoaded() {
                return (Balloon.overlay == null || Balloon.underlay == null) ? false : true;
            }

            public void finished(GameSettings s) {
            }
        });
        mTypes[BlueShrinkBalloon.key] = new BlueShrinkBalloon();
        mTypes[PowerUpBalloon.key] = new PowerUpBalloon();
        mTypes[PinkHealthBalloon.key] = new PinkHealthBalloon();
        mTypes[PurpleTeleportBalloon.key] = new PurpleTeleportBalloon();
        mTypes[TankBalloon.key] = new TankBalloon();
        mTypes[QuestionBalloon.key] = new QuestionBalloon();
        mTypes[DamageReflector.key] = new DamageReflector();
    }

    public static void load(Balloon x2, float r, float g, float b) {
        ColorMatrix cm = new ColorMatrix();
        cm.setScale(r, g, b, 1.0f);
        Paint drawPaint = new Paint();
        drawPaint.setAntiAlias(true);
        drawPaint.setColorFilter(new ColorMatrixColorFilter(cm));
        x2.setImage(Bitmap.createBitmap((int) mOrigWidth, ((int) mOrigHeight) * 2, Bitmap.Config.ARGB_8888));
        Canvas drawer = new Canvas(x2.getImage());
        drawer.drawBitmap(underlay, 0.0f, 0.0f, drawPaint);
        drawer.drawBitmap(overlay, 0.0f, 0.0f, mPaint);
    }

    public void remove(GameSettings gms) {
        this.removed = true;
        if (this.hp <= 0) {
            pop(gms);
            gms.mStats.pop(this, gms);
            return;
        }
        gms.mStats.remove(this, gms);
    }

    public boolean draw(Canvas c, GameSettings gms) {
        if (!(gms.mScale == this.mWorldScale && this.mScaleModifier == 1.0f)) {
            if (this.mScaleModifier != 1.0f) {
                this.mScale *= this.mScaleModifier;
                this.mScaleModifier = 1.0f;
            }
            float newscale = gms.mScale * this.mScale;
            this.mWorldScale = gms.mScale;
            if (newscale < mMinScale) {
                newscale = mMinScale;
            }
            if (this.y - (mScaleY * this.mConcatScale) > gms.mHeight) {
                this.mConcatScale = newscale;
                this.y = gms.mHeight + (mScaleY * this.mConcatScale);
            } else {
                this.mConcatScale = newscale;
            }
        }
        c.save();
        c.translate(this.x, this.y);
        c.scale(this.mConcatScale, this.mConcatScale);
        c.drawBitmap(getImage(), -mScaleX, -mScaleY, mPaint);
        c.restore();
        synchronized (this.mAfflictions) {
            this.mAfflictions.reset();
            while (this.mAfflictions.hasNext()) {
                this.mAfflictions.getCursor().draw(c, gms);
                this.mAfflictions.next();
            }
        }
        return false;
    }

    public boolean tick(GameSettings gms) {
        if (gms.mPaused) {
            return false;
        }
        synchronized (this.mAfflictions) {
            this.mAfflictions.reset();
            while (this.mAfflictions.hasNext()) {
                Affliction curr = this.mAfflictions.getCursor();
                curr.tick(gms);
                if (curr.scheduleRemoval()) {
                    this.mAfflictions.removeCursor();
                } else {
                    this.mAfflictions.next();
                }
            }
        }
        if (this.hp <= 0) {
            remove(gms);
        }
        if (gms.mTimeScale != this.mWorldTScale) {
            this.mWorldTScale = gms.mTimeScale;
            this.mConcatV = this.v * this.mWorldTScale;
        }
        if (this.y < 0.0f) {
            if (this.y < gms.mTop - (mOrigHeight * (2.0f * this.mConcatScale))) {
                remove(gms);
            }
        }
        if (this.v != 0.0f) {
            this.y -= this.mConcatV;
        }
        return false;
    }

    public RectF getHitBox(GameSettings gms) {
        if (this.mHitBox == null) {
            this.mHitBox = new RectF(0.0f, 0.0f, this.mConcatScale * mOrigWidth, this.mConcatScale * mOrigHeight);
        } else {
            this.mHitBox.right = this.mHitBox.left + (this.mConcatScale * mOrigWidth);
            this.mHitBox.bottom = this.mHitBox.top + (this.mConcatScale * mOrigHeight);
        }
        this.mHitBox.offsetTo(this.x - (mScaleX * this.mConcatScale), this.y - (mScaleY * this.mConcatScale));
        return this.mHitBox;
    }

    public boolean scheduleRemoval() {
        if (mKillBalloons) {
            this.removed = true;
        }
        return this.removed || mKillBalloons;
    }

    public boolean hit(float x2, float y2, GameSettings gms, int h) {
        if (this.hp <= 0 || this.removed) {
            return false;
        }
        this.hp -= h;
        return true;
    }

    public void pop(GameSettings gms) {
        RectF rct = getHitBox(gms);
        Pop p = new Pop();
        p.mX = rct.centerX();
        p.mY = rct.centerY();
        p.mScale = Pop.mOrigScale * this.mConcatScale;
        synchronized (gms.mBalloons) {
            gms.mBalloons.remove(this);
        }
        gms.mDrawables.add(p);
        gms.mTickables.addToEnd(p);
    }

    public void addSelf(GameSettings gms) {
        gms.mDrawables.add(this);
        gms.mTickables.add(this);
        gms.mHittables.addToEnd(this);
    }
}
