package pop.em.up.balloons;

public enum BKey {
    rb(RedBalloon.key),
    bsb(BlueShrinkBalloon.key),
    pub(PowerUpBalloon.key),
    ptb(PurpleTeleportBalloon.key),
    phb(PinkHealthBalloon.key),
    gtb(TankBalloon.key),
    bqb(QuestionBalloon.key),
    sdb(DamageReflector.key);
    
    public final int K;

    private BKey(int key) {
        this.K = key;
    }
}
