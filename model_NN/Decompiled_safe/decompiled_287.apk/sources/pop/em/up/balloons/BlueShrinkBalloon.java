package pop.em.up.balloons;

import android.content.Context;
import android.graphics.Bitmap;
import pop.em.up.GameSettings;
import pop.em.up.LoadTask;
import pop.em.up.doodads.Pop;
import pop.em.up.loaders.Loader;

public class BlueShrinkBalloon extends Balloon {
    public static int key = 1;
    public static Bitmap mImage;

    public BlueShrinkBalloon() {
    }

    public BlueShrinkBalloon(GameSettings s) {
        super(s);
        this.mScale = Balloon.mOrigScale;
        this.mScore = 30;
        this.hp = 3;
        setSpeed(2.5f + ((float) (0.66d * Math.random())), s);
    }

    public boolean hit(float x, float y, GameSettings gms, int h) {
        boolean b = super.hit(x, y, gms, h);
        if (b) {
            switch (this.hp) {
                case 0:
                    break;
                case 1:
                    this.mScaleModifier = (mOrigScale * 0.5f) / this.mScale;
                    break;
                case 2:
                    this.mScaleModifier = (mOrigScale * 0.75f) / this.mScale;
                    break;
                default:
                    this.mScaleModifier = mOrigScale / this.mScale;
                    break;
            }
        }
        return b;
    }

    public Bitmap getImage() {
        return mImage;
    }

    public void setImage(Bitmap b) {
        mImage = b;
    }

    public Balloon clone(GameSettings s) {
        return new BlueShrinkBalloon(s);
    }

    public int killLives() {
        return 1;
    }

    public Balloon load(Loader l) {
        l.mTasks.add(new LoadTask() {
            public void load(Context c) {
                Balloon.load(this, 0.0f, 0.0f, 1.0f);
                if (!Pop.isLoaded()) {
                    Pop.load(c);
                }
            }

            public boolean isLoaded() {
                return BlueShrinkBalloon.this.getImage() != null;
            }

            public void finished(GameSettings s) {
            }
        });
        return this;
    }
}
