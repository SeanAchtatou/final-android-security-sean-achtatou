package pop.em.up.balloons;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.RectF;
import pop.em.up.GameSettings;
import pop.em.up.LoadTask;
import pop.em.up.doodads.Pop;
import pop.em.up.doodads.TeleportAnimation;
import pop.em.up.loaders.Loader;

public class PurpleTeleportBalloon extends Balloon {
    public static int key = 4;
    public static Bitmap mImage;
    public boolean mTeleport = false;

    public PurpleTeleportBalloon() {
    }

    public PurpleTeleportBalloon(GameSettings s) {
        super(s);
        this.mScale = Balloon.mOrigScale * 1.4f;
        this.mScore = 50;
        this.hp = 4;
        setSpeed(2.5f + ((float) (0.66d * Math.random())), s);
    }

    public boolean hit(float x, float y, GameSettings gms, int hp) {
        if (!super.hit(x, y, gms, hp)) {
            return false;
        }
        this.mTeleport = true;
        return true;
    }

    public boolean tick(GameSettings gms) {
        super.tick(gms);
        if (this.hp > 0 && this.mTeleport && !this.removed) {
            RectF r = getHitBox(gms);
            float h = r.height();
            r.bottom += h;
            new TeleportAnimation(r, mImage, 0.2f, gms).addSelf(gms);
            r.bottom -= h;
            this.y += ((gms.mHeight - this.y) + h) * 0.5f;
            this.x = (Balloon.mScaleX * gms.mScale * this.mScale) + ((float) (((double) (gms.mWidth - ((Balloon.mOrigWidth * gms.mScale) * this.mScale))) * Math.random()));
            this.mTeleport = false;
        }
        return false;
    }

    public Bitmap getImage() {
        return mImage;
    }

    public void setImage(Bitmap b) {
        mImage = b;
    }

    public Balloon clone(GameSettings s) {
        return new PurpleTeleportBalloon(s);
    }

    public int killLives() {
        return 1;
    }

    public Balloon load(Loader l) {
        l.mTasks.add(new LoadTask() {
            public void load(Context c) {
                Balloon.load(this, 0.5803922f, 0.0f, 0.827451f);
                TeleportAnimation.load(c);
                if (!Pop.isLoaded()) {
                    Pop.load(c);
                }
            }

            public boolean isLoaded() {
                return PurpleTeleportBalloon.this.getImage() != null;
            }

            public void finished(GameSettings s) {
            }
        });
        return this;
    }
}
