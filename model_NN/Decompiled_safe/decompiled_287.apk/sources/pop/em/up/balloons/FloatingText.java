package pop.em.up.balloons;

import android.graphics.Canvas;
import android.graphics.Paint;
import pop.em.up.GameSettings;
import pop.em.up.abstracts.GameDrawable;

public class FloatingText implements GameDrawable {
    static final Paint mTextColor = new Paint();
    private int mAlpha = 0;
    private int mB;
    private int mG;
    private int mR;
    private String mText;
    private int mTextSize = 0;
    private float mX = 0.0f;
    private float mY = 0.0f;
    public float mdA = 2.0f;
    float mdY = 2.0f;

    static {
        mTextColor.setTextAlign(Paint.Align.CENTER);
        mTextColor.setAntiAlias(true);
    }

    public FloatingText(String txt, float x, float y, int size, int r, int g, int b) {
        this.mText = txt;
        this.mAlpha = 255;
        this.mX = x;
        this.mY = y;
        this.mR = r;
        this.mG = g;
        this.mB = b;
        this.mTextSize = size;
    }

    public boolean draw(Canvas c, GameSettings gms) {
        mTextColor.setARGB(this.mAlpha, this.mR, this.mG, this.mB);
        mTextColor.setTextSize((float) this.mTextSize);
        c.drawText(this.mText, this.mX, this.mY, mTextColor);
        this.mAlpha = (int) (((float) this.mAlpha) - this.mdA);
        this.mY -= this.mdY;
        return false;
    }

    public boolean scheduleRemoval() {
        return ((float) this.mAlpha) < this.mdA;
    }

    public void addSelf(GameSettings gms) {
        gms.mDrawables.addToEnd(this);
    }
}
