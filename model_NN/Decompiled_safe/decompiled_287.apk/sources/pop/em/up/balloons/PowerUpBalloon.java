package pop.em.up.balloons;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.RectF;
import pop.em.up.GameSettings;
import pop.em.up.LoadTask;
import pop.em.up.doodads.Pop;
import pop.em.up.doodads.Sparkle;
import pop.em.up.engines.LevelEngine;
import pop.em.up.loaders.Loader;
import pop.em.up.powerups.PowerUp;

public class PowerUpBalloon extends Balloon {
    public static int key = 2;
    public static Bitmap mImage;
    private int mBound = 8;
    private int mCounter = 0;

    public void pop(GameSettings gms) {
        super.pop(gms);
        if (gms.mEngine instanceof LevelEngine) {
            PowerUp.getPowerUp(gms, ((LevelEngine) gms.mEngine).mLevel, this).addSelf(gms);
        } else {
            PowerUp.getPowerUp(gms, this).addSelf(gms);
        }
    }

    public boolean tick(GameSettings gms) {
        this.mCounter++;
        if (this.mBound <= this.mCounter) {
            RectF rct = getHitBox(gms);
            new Sparkle(rct.left + ((float) (Math.random() * ((double) rct.width()))), rct.top + ((float) (Math.random() * ((double) rct.height()))), this.mConcatScale, gms).addSelf(gms);
            this.mCounter = 0;
        }
        return super.tick(gms);
    }

    public PowerUpBalloon() {
    }

    public PowerUpBalloon(GameSettings s) {
        super(s);
        this.mScale = 0.6f * Balloon.mOrigScale;
        this.mScore = 0;
        this.hp = 3;
        setSpeed(5.0f, s);
    }

    public Bitmap getImage() {
        return mImage;
    }

    public void setImage(Bitmap b) {
        mImage = b;
    }

    public Balloon clone(GameSettings s) {
        return new PowerUpBalloon(s);
    }

    public int killLives() {
        return 0;
    }

    public Balloon load(Loader l) {
        l.mTasks.add(new LoadTask() {
            public void load(Context c) {
                Balloon.load(this, 1.0f, 0.7882353f, 0.0f);
                Sparkle.load(c);
                if (!Pop.isLoaded()) {
                    Pop.load(c);
                }
            }

            public boolean isLoaded() {
                return PowerUpBalloon.this.getImage() != null && Sparkle.isLoaded();
            }

            public void finished(GameSettings s) {
            }
        });
        return this;
    }
}
