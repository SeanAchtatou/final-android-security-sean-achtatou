package pop.em.up.balloons;

import android.content.Context;
import android.graphics.Bitmap;
import java.util.Iterator;
import pop.em.up.GameSettings;
import pop.em.up.LoadTask;
import pop.em.up.afflictions.PinkHealthModifier;
import pop.em.up.doodads.Pop;
import pop.em.up.loaders.Loader;

public class PinkHealthBalloon extends Balloon {
    public static int key = 3;
    public static Bitmap mImage;

    public PinkHealthBalloon() {
    }

    public PinkHealthBalloon(GameSettings s) {
        super(s);
        this.mScale = Balloon.mOrigScale * 1.3f;
        this.mScore = 30;
        this.hp = 1;
        setSpeed(4.0f + ((float) (1.0d * Math.random())), s);
    }

    public void pop(GameSettings gms) {
        Iterator<Balloon> it = gms.mBalloons.iterator();
        while (it.hasNext()) {
            Balloon b = it.next();
            if (b != this && !(b instanceof BlueShrinkBalloon)) {
                b.mAfflictions.add(new PinkHealthModifier(b, gms));
            }
        }
        super.pop(gms);
    }

    public Bitmap getImage() {
        return mImage;
    }

    public void setImage(Bitmap b) {
        mImage = b;
    }

    public Balloon clone(GameSettings s) {
        return new PinkHealthBalloon(s);
    }

    public int killLives() {
        return 1;
    }

    public Balloon load(Loader l) {
        l.mTasks.add(new LoadTask() {
            public void load(Context c) {
                Balloon.load(this, 0.98039216f, 0.078431375f, 0.5764706f);
                PinkHealthModifier.load(c);
                if (!Pop.isLoaded()) {
                    Pop.load(c);
                }
            }

            public boolean isLoaded() {
                return PinkHealthBalloon.this.getImage() != null;
            }

            public void finished(GameSettings s) {
            }
        });
        return this;
    }
}
