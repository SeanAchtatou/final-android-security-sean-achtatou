package pop.em.up.balloons;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import pop.em.up.GameSettings;
import pop.em.up.LoadTask;
import pop.em.up.R;
import pop.em.up.doodads.Pop;
import pop.em.up.loaders.Loader;

public class TankBalloon extends Balloon {
    public static int key = 5;
    public static Bitmap mImage;
    private final float dScale = (Balloon.mOrigScale / 6.0f);

    public TankBalloon() {
    }

    public TankBalloon(GameSettings s) {
        super(s);
        this.mScale = Balloon.mOrigScale;
        this.mScore = 100;
        this.hp = 10;
        setSpeed(10.0f + ((float) (5.0d * Math.random())), s);
    }

    public TankBalloon load(Context c) {
        ColorMatrix cm = new ColorMatrix();
        cm.setScale(0.0f, 0.39215687f, 0.0f, 1.0f);
        Paint drawPaint = new Paint();
        drawPaint.setAntiAlias(true);
        drawPaint.setColorFilter(new ColorMatrixColorFilter(cm));
        setImage(Bitmap.createBitmap((int) mOrigWidth, ((int) mOrigHeight) * 2, Bitmap.Config.ARGB_8888));
        Canvas drawer = new Canvas(getImage());
        drawer.drawBitmap(underlay, 0.0f, 0.0f, drawPaint);
        drawer.drawBitmap(BitmapFactory.decodeResource(c.getResources(), R.drawable.tankunderlay), 0.0f, 0.0f, mPaint);
        drawer.drawBitmap(overlay, 0.0f, 0.0f, mPaint);
        return this;
    }

    public boolean hit(float x, float y, GameSettings gms, int h) {
        boolean b = super.hit(x, y, gms, h);
        if (b && this.hp != 0) {
            this.mScaleModifier = 1.0f + (this.dScale / this.mScale);
        }
        return b;
    }

    public Bitmap getImage() {
        return mImage;
    }

    public void setImage(Bitmap b) {
        mImage = b;
    }

    public Balloon clone(GameSettings s) {
        return new TankBalloon(s);
    }

    public int killLives() {
        return 1;
    }

    public Balloon load(Loader l) {
        l.mTasks.add(new LoadTask() {
            public void load(Context c) {
                TankBalloon.this.load(c);
                if (!Pop.isLoaded()) {
                    Pop.load(c);
                }
            }

            public boolean isLoaded() {
                return TankBalloon.this.getImage() != null;
            }

            public void finished(GameSettings s) {
            }
        });
        return this;
    }
}
