package pop.em.up.balloons;

import android.content.Context;
import android.graphics.Bitmap;
import pop.em.up.GameSettings;
import pop.em.up.LoadTask;
import pop.em.up.doodads.Pop;
import pop.em.up.loaders.Loader;

public class RedBalloon extends Balloon {
    public static int key = 0;
    public static Bitmap mImage;

    public RedBalloon() {
    }

    public RedBalloon(GameSettings s) {
        super(s);
        this.mScale = Balloon.mOrigScale;
        this.mScore = 10;
        this.hp = 1;
        setSpeed(2.0f + ((float) (0.66d * Math.random())), s);
    }

    public Bitmap getImage() {
        return mImage;
    }

    public void setImage(Bitmap b) {
        mImage = b;
    }

    public Balloon clone(GameSettings s) {
        return new RedBalloon(s);
    }

    public int killLives() {
        return 1;
    }

    public Balloon load(Loader l) {
        l.mTasks.addToEnd(new LoadTask() {
            public void load(Context c) {
                Balloon.load(this, 1.0f, 0.0f, 0.0f);
                if (!Pop.isLoaded()) {
                    Pop.load(c);
                }
            }

            public boolean isLoaded() {
                return RedBalloon.this.getImage() != null;
            }

            public void finished(GameSettings s) {
            }
        });
        return this;
    }
}
