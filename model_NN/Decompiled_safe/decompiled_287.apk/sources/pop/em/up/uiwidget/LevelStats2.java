package pop.em.up.uiwidget;

import pop.em.up.GameSettings;
import pop.em.up.engines.LevelEngine;

public class LevelStats2 extends TextUIWidget {
    LevelEngine mEngine;

    public LevelStats2(LevelEngine le) {
        this.mEngine = le;
        this.mWeight = 30;
    }

    public String getRow1(GameSettings gms) {
        return "Score:" + gms.mStats.mScore;
    }

    public String getRow2(GameSettings gms) {
        return "Balloons Left:" + this.mEngine.balloonsLeft();
    }
}
