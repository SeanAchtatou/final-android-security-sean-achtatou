package pop.em.up.uiwidget;

import java.text.DecimalFormat;
import pop.em.up.GameSettings;
import pop.em.up.GameStats;

public class Statistics extends TextUIWidget {
    public static DecimalFormat df = new DecimalFormat("0.0");
    static String[] mPossibleStrings = {"Ninja Kills:", "Bloon Massacre:", "Bloon Carnage:", "Balloons Killed:", "Epic Kills", "Balloons Owned:", "Bloons Crying in the Bathroom:", "WTF Kills:", "Insane Kills:", "All your pie belong to us:", "Oh Dayum Kills:", "Jedi Mind Tricked Bloons:", "Balloons Killed:", "Balloons Killed:", "Balloons Killed:", "Balloons Killed:", "Balloons Killed:", "Bloon Carnage:", "Bloon Carnage:", "Bloon Carnage:", "Bloon Carnage:", "Bloon Massacre:", "Bloon Massacre:", "Bloon Massacre:", "Bloon Massacre:", "Bloon Massacre:"};
    final String mDisplay = mPossibleStrings[(int) (Math.random() * ((double) mPossibleStrings.length))];

    public Statistics() {
        this.mWeight = 30;
    }

    public String getRow1(GameSettings gms) {
        GameStats gs = gms.mStats;
        return "Acc:" + df.format((double) (100.0f * ((float) (gs.mTap == 0 ? 0.0d : ((double) gs.mHit) / ((double) gs.mTap)))));
    }

    public String getRow2(GameSettings gms) {
        return String.valueOf(this.mDisplay) + gms.mStats.mPopped;
    }
}
