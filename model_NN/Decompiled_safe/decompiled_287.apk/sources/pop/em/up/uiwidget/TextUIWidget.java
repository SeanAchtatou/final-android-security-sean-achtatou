package pop.em.up.uiwidget;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import pop.em.up.GameSettings;

public abstract class TextUIWidget extends UIWidget {
    private Rect mBounds;
    private float mHPadding = 0.0f;
    private int mLen1 = 0;
    private int mLen2 = 0;
    private String mRow1 = "";
    private String mRow2 = "";
    private int mTextSize = 0;
    private float mTxtHeight = 0.0f;

    public abstract String getRow1(GameSettings gameSettings);

    public abstract String getRow2(GameSettings gameSettings);

    public void draw(GameSettings gms, Canvas c) {
        this.mRow1 = getRow1(gms);
        this.mRow2 = getRow2(gms);
        if (!(this.mRow1.length() == this.mLen1 && this.mRow2.length() == this.mLen2)) {
            measureText(gms.mTextColor);
            this.mLen1 = this.mRow1.length();
            this.mLen2 = this.mRow2.length();
        }
        gms.mTextColor.setTextSize((float) this.mTextSize);
        c.drawText(this.mRow1, (float) this.mBounds.left, ((float) this.mBounds.top) + this.mTxtHeight + this.mHPadding, gms.mTextColor);
        c.drawText(this.mRow2, (float) this.mBounds.left, ((float) this.mBounds.top) + (this.mTxtHeight * 2.0f) + 1.0f + (this.mHPadding * 2.0f), gms.mTextColor);
    }

    public void measureText(Paint p) {
        int h;
        int th;
        Rect r = new Rect();
        Rect r2 = new Rect();
        int i = 1;
        while (i < 50) {
            p.setTextSize((float) i);
            p.getTextBounds(this.mRow1, 0, this.mRow1.length(), r);
            p.getTextBounds(this.mRow2, 0, this.mRow2.length(), r2);
            if (r.height() > r2.height()) {
                h = (r.height() * 2) + 1;
                th = r.height();
            } else {
                h = (r2.height() * 2) + 1;
                th = r2.height();
            }
            int w = r.width();
            if (h > this.mBounds.height()) {
                p.setTextSize((float) (i - 1));
                this.mTextSize = i - 1;
                this.mHPadding = (((float) this.mBounds.height()) - (this.mTxtHeight * 2.0f)) / 3.0f;
                return;
            } else if (w > this.mBounds.width() - 5) {
                p.setTextSize((float) (i - 1));
                this.mTextSize = i - 1;
                this.mHPadding = (((float) this.mBounds.height()) - (this.mTxtHeight * 2.0f)) / 3.0f;
                return;
            } else if (r2.width() > this.mBounds.width() - 5) {
                p.setTextSize((float) (i - 1));
                this.mTextSize = i - 1;
                this.mHPadding = (((float) this.mBounds.height()) - (this.mTxtHeight * 2.0f)) / 3.0f;
                return;
            } else {
                this.mTxtHeight = (float) th;
                i++;
            }
        }
    }

    public void setBounds(GameSettings gms, RectF layout) {
        this.mBounds = new Rect();
        this.mBounds.left = (int) layout.left;
        this.mBounds.right = (int) layout.right;
        this.mBounds.top = (int) layout.top;
        this.mBounds.bottom = (int) layout.bottom;
    }

    public boolean checkHit(GameSettings gms, float x, float y) {
        return false;
    }
}
