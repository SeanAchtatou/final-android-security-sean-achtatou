package pop.em.up.uiwidget;

import pop.em.up.GameSettings;
import pop.em.up.engines.LevelEngine;

public class LevelStats1 extends TextUIWidget {
    LevelEngine mEngine;

    public LevelStats1(LevelEngine le) {
        this.mEngine = le;
        this.mWeight = 15;
        this.mMaxWidth = 100.0f;
    }

    public String getRow1(GameSettings gms) {
        return "Level:" + (this.mEngine.mLevel + 1);
    }

    public String getRow2(GameSettings gms) {
        return "Lives:" + gms.mStats.mLives;
    }
}
