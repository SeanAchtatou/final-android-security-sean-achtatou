package pop.em.up.uiwidget;

import android.graphics.Canvas;
import android.graphics.RectF;
import java.util.ArrayList;
import java.util.Iterator;
import pop.em.up.GameSettings;

public class UIDock {
    private int mCount = 0;
    ArrayList<UIWidget> mWidgets = new ArrayList<>();

    public void addWidget(UIWidget w) {
        this.mWidgets.add(w);
    }

    public void readyLayout(GameSettings gms) {
        int i = 0;
        int totalWeight = 0;
        RectF bounds = gms.upperRect;
        float boundsw = bounds.width();
        Iterator<UIWidget> it = this.mWidgets.iterator();
        while (it.hasNext()) {
            totalWeight += it.next().mWeight;
            i++;
        }
        if (i != 0) {
            float x = 0.0f;
            Iterator<UIWidget> it2 = this.mWidgets.iterator();
            while (it2.hasNext()) {
                UIWidget w = it2.next();
                RectF wbounds = new RectF();
                wbounds.left = x;
                wbounds.bottom = bounds.bottom;
                x += (((float) w.mWeight) / ((float) totalWeight)) * boundsw;
                wbounds.right = x;
                w.setBounds(gms, wbounds);
            }
            this.mCount = i;
        }
    }

    public void drawUI(GameSettings gms, Canvas c) {
        int i = 0;
        Iterator<UIWidget> it = this.mWidgets.iterator();
        while (it.hasNext()) {
            it.next().draw(gms, c);
            i++;
            if (i == this.mCount) {
                return;
            }
        }
    }

    public boolean checkUI(GameSettings gms, float x, float y) {
        int i = 0;
        Iterator<UIWidget> it = this.mWidgets.iterator();
        while (it.hasNext()) {
            if (it.next().checkHit(gms, x, y)) {
                return true;
            }
            i++;
            if (i == this.mCount) {
                return false;
            }
        }
        return false;
    }
}
