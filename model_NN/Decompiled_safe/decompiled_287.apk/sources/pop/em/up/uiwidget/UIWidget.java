package pop.em.up.uiwidget;

import android.graphics.Canvas;
import android.graphics.RectF;
import pop.em.up.GameSettings;

public abstract class UIWidget {
    public float mMaxWidth = 50.0f;
    public int mWeight = 1;

    public abstract boolean checkHit(GameSettings gameSettings, float f, float f2);

    public abstract void draw(GameSettings gameSettings, Canvas canvas);

    public abstract void setBounds(GameSettings gameSettings, RectF rectF);
}
