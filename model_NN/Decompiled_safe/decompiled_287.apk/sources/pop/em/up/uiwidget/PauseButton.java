package pop.em.up.uiwidget;

import android.graphics.Canvas;
import android.graphics.RectF;
import pop.em.up.GameSettings;
import pop.em.up.abstracts.Button;

public class PauseButton extends Button {
    float mHeight;
    float mWidth;

    public PauseButton() {
        this.mWeight = 10;
    }

    public void draw(GameSettings gms, Canvas c) {
        if (!gms.mPaused) {
            float x1 = this.mWidth * 0.4f;
            float x2 = this.mWidth * 0.6f;
            float y1 = this.mHeight * 0.2f;
            float y2 = this.mHeight * 0.8f;
            c.save();
            c.translate(this.mBounds.left, this.mBounds.top);
            c.drawLine(x1, y1, x1, y2, gms.BGOutline);
            c.drawLine(x2, y1, x2, y2, gms.BGOutline);
            c.restore();
            return;
        }
        float x12 = this.mWidth * 0.3f;
        float x22 = this.mWidth * 0.7f;
        float y12 = this.mHeight * 0.2f;
        float y22 = this.mHeight * 0.8f;
        float y3 = this.mHeight * 0.5f;
        c.save();
        c.translate(this.mBounds.left, this.mBounds.top);
        c.drawLine(x12, y12, x12, y22, gms.BGOutline);
        c.drawLine(x12, y12, x22, y3, gms.BGOutline);
        c.drawLine(x12, y22, x22, y3, gms.BGOutline);
        c.restore();
    }

    public void setBounds(GameSettings gms, RectF layout) {
        super.setBounds(gms, layout);
        this.mWidth = this.mBounds.width();
        this.mHeight = this.mBounds.height();
    }

    public boolean checkHit(GameSettings gms, float x, float y) {
        boolean z;
        if (!this.mBounds.contains(x, y)) {
            return false;
        }
        if (gms.mPaused) {
            z = false;
        } else {
            z = true;
        }
        gms.pause(z);
        return true;
    }
}
