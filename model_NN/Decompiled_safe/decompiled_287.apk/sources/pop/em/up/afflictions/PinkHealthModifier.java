package pop.em.up.afflictions;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import pop.em.up.GameSettings;
import pop.em.up.R;
import pop.em.up.abstracts.Affliction;
import pop.em.up.balloons.Balloon;

public class PinkHealthModifier extends Affliction {
    static float mHalfX;
    static float mHalfY;
    private static Bitmap mHeart;
    Balloon b;
    int mHP = 0;
    float mOrigScale = 0.3f;
    boolean mRemove = false;
    float mScale;
    float mX;
    float mY;

    public static void load(Context c) {
        mHeart = BitmapFactory.decodeResource(c.getResources(), R.drawable.heart);
        mHalfX = (float) (mHeart.getWidth() / 2);
        mHalfY = (float) (mHeart.getHeight() / 2);
    }

    public PinkHealthModifier(Balloon b2, GameSettings gms) {
        RectF rct = b2.getHitBox(gms);
        this.mX = (float) (((double) (0.6f * rct.width())) * (Math.random() - 0.5d));
        this.mY = ((float) (((double) rct.height()) * ((Math.random() * 0.6000000238418579d) + 0.20000000298023224d))) - rct.height();
        this.b = b2;
        this.mHP = b2.hp;
    }

    public boolean tick(GameSettings gms) {
        if (this.b.hp >= this.mHP) {
            return false;
        }
        this.b.hp++;
        this.mRemove = true;
        return false;
    }

    public boolean draw(Canvas c, GameSettings gms) {
        float scale = this.mOrigScale * this.b.mConcatScale;
        c.save();
        c.translate(this.b.x + this.mX, this.b.y + this.mY);
        c.scale(scale, scale);
        c.drawBitmap(mHeart, -mHalfX, -mHalfY, (Paint) null);
        c.restore();
        return false;
    }

    public boolean scheduleRemoval() {
        return this.mRemove;
    }

    public void addSelf(GameSettings gms) {
        gms.mTickables.add(this);
    }

    public void destroy() {
    }
}
