package com.scoreloop.client.android.core.model;

import com.scoreloop.client.android.core.util.SetterIntent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GameItem extends BaseEntity {
    public static String a = "item";
    private String c;
    private final Map<String, String> d = new HashMap();
    private String e;
    private String f;
    private Date g;
    private final List<String> h = new ArrayList();

    public GameItem(JSONObject jSONObject) throws JSONException {
        a(jSONObject);
    }

    public String a() {
        return a;
    }

    public void a(JSONObject jSONObject) throws JSONException {
        super.a(jSONObject);
        SetterIntent setterIntent = new SetterIntent();
        if (setterIntent.h(jSONObject, "name", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.f = (String) setterIntent.a();
        }
        if (setterIntent.h(jSONObject, "description", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.c = (String) setterIntent.a();
        }
        if (setterIntent.f(jSONObject, "ownership", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE) && setterIntent.b((JSONObject) setterIntent.a(), "created_at", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.g = (Date) setterIntent.a();
        }
        if (setterIntent.h(jSONObject, "meta_data", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.e = (String) setterIntent.a();
        }
        if (setterIntent.e(jSONObject, "images", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            JSONArray jSONArray = (JSONArray) setterIntent.a();
            int length = jSONArray.length();
            this.d.clear();
            for (int i = 0; i < length; i++) {
                JSONObject jSONObject2 = jSONArray.getJSONObject(i);
                String str = setterIntent.h(jSONObject2, "identifier", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE) ? (String) setterIntent.a() : null;
                String str2 = setterIntent.h(jSONObject2, "url", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE) ? (String) setterIntent.a() : null;
                if (!(str == null || str2 == null)) {
                    this.d.put(str, str2);
                }
            }
        }
        if (setterIntent.h(jSONObject, "tags", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.h.clear();
            Collections.addAll(this.h, ((String) setterIntent.a()).split("\\s"));
        }
    }
}
