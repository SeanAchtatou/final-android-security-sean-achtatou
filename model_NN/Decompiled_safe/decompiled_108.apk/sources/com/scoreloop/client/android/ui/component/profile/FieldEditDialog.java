package com.scoreloop.client.android.ui.component.profile;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.scoreloop.client.android.ui.framework.BaseDialog;
import com.skyd.bestpuzzle.n1648.R;

public class FieldEditDialog extends BaseDialog {
    public static final int BUTTON_CANCEL = 1;
    public static final int BUTTON_OK = 0;
    private final String _currentLabel;
    private String _currentText;
    private String _hint;
    private final String _newLabel;
    private String _newText;
    private TextView _tfCurrentText;
    private EditText _tfEditText;
    private TextView _tfHint;
    private final String _title;
    private final String description;

    public FieldEditDialog(Context context, String title, String currentLabel, String newLabel, String description2) {
        super(context);
        setCancelable(true);
        this._title = title;
        this._currentLabel = currentLabel;
        this._newLabel = newLabel;
        this.description = description2;
    }

    /* access modifiers changed from: protected */
    public int getContentViewLayoutId() {
        return R.layout.sl_dialog_profile_edit;
    }

    public void onClick(View v) {
        if (this._listener != null) {
            switch (v.getId()) {
                case R.id.sl_button_ok /*2131427353*/:
                    this._listener.onAction(this, 0);
                    return;
                case R.id.sl_button_cancel /*2131427357*/:
                    this._listener.onAction(this, 1);
                    return;
                default:
                    return;
            }
        }
    }

    public void setCurrentText(String currentText) {
        this._currentText = currentText;
        refresh();
    }

    public void setEditText(String editText) {
        this._newText = editText;
        refresh();
    }

    public void setHint(String text) {
        this._hint = text;
        refresh();
    }

    public String getEditText() {
        this._newText = this._tfEditText.getText().toString();
        return this._tfEditText.getText().toString();
    }

    private void refresh() {
        if (this._tfEditText != null) {
            this._tfEditText.setText(this._newText);
        }
        if (this._tfCurrentText != null) {
            this._tfCurrentText.setText(this._currentText);
        }
        if (this._tfHint != null) {
            this._tfHint.setVisibility(0);
            this._tfHint.setText(this._hint);
            return;
        }
        this._tfHint.setVisibility(4);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((Button) findViewById(R.id.sl_button_ok)).setOnClickListener(this);
        ((Button) findViewById(R.id.sl_button_cancel)).setOnClickListener(this);
        ((TextView) findViewById(R.id.sl_title)).setText(this._title);
        TextView tvDescription = (TextView) findViewById(R.id.sl_description);
        if (this.description != null) {
            tvDescription.setText(this.description);
        } else {
            tvDescription.setVisibility(4);
        }
        TextView tvCurrentLabel = (TextView) findViewById(R.id.sl_user_profile_edit_current_label);
        this._tfCurrentText = (TextView) findViewById(R.id.sl_user_profile_edit_current_text);
        if (this._currentLabel != null) {
            tvCurrentLabel.setText(this._currentLabel);
        } else {
            tvCurrentLabel.setVisibility(4);
            this._tfCurrentText.setVisibility(4);
        }
        TextView tvNewLabel = (TextView) findViewById(R.id.sl_user_profile_edit_new_label);
        if (this._newLabel != null) {
            tvNewLabel.setText(this._newLabel);
        } else {
            tvNewLabel.setVisibility(4);
        }
        this._tfEditText = (EditText) findViewById(R.id.sl_user_profile_edit_new_text);
        this._tfHint = (TextView) findViewById(R.id.sl_dialog_hint);
        refresh();
    }
}
