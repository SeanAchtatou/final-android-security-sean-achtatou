package com.scoreloop.client.android.ui;

public interface OnScoreSubmitObserver {
    public static final int STATUS_ERROR_BALANCE = 4;
    public static final int STATUS_ERROR_NETWORK = 3;
    public static final int STATUS_SUCCESS_CHALLENGE = 2;
    public static final int STATUS_SUCCESS_SCORE = 1;
    public static final int STATUS_UNDEFINED = 0;

    void onScoreSubmit(int i, Exception exc);
}
