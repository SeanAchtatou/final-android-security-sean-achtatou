package com.scoreloop.client.android.core.controller;

import java.util.Collection;
import org.json.JSONArray;

class c {
    private final String a;
    private final d b;
    private final Object c;

    public c(String str, d dVar, String str2) {
        this.a = str;
        this.b = dVar;
        this.c = str2;
    }

    public c(String str, d dVar, Collection<String> collection) {
        this.a = str;
        this.b = dVar;
        this.c = new JSONArray((Collection) collection);
    }

    public String a() {
        String name = this.b.getName();
        if (name == null) {
            return this.a;
        }
        return String.format("%s_%s", this.a, name);
    }

    public Object b() {
        return this.c;
    }
}
