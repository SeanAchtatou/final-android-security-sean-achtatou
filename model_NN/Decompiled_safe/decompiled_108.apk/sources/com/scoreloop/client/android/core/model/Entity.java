package com.scoreloop.client.android.core.model;

import com.scoreloop.client.android.core.PublishedFor__1_0_0;

public interface Entity {
    String a();

    @PublishedFor__1_0_0
    String getIdentifier();
}
