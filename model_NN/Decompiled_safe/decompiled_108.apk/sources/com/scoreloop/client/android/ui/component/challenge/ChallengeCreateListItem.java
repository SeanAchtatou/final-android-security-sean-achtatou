package com.scoreloop.client.android.ui.component.challenge;

import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.ui.component.base.ComponentActivity;
import com.scoreloop.client.android.ui.component.base.StandardListItem;
import com.skyd.bestpuzzle.n1648.R;

public class ChallengeCreateListItem extends StandardListItem<User> {
    public ChallengeCreateListItem(ComponentActivity activity, User user) {
        super(activity, null, null, null, user);
        if (user == null) {
            setDrawable(activity.getResources().getDrawable(R.drawable.sl_icon_challenge_anyone));
            setTitle(activity.getResources().getString(R.string.sl_against_anyone));
            return;
        }
        setDrawable(activity.getResources().getDrawable(R.drawable.sl_icon_user));
        setTitle(user.getDisplayName());
    }

    /* access modifiers changed from: protected */
    public int getSubTitleId() {
        return 0;
    }

    public int getType() {
        return 5;
    }

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return R.layout.sl_list_item_icon_title;
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    /* access modifiers changed from: protected */
    public String getImageUrl() {
        if (getTarget() != null) {
            return ((User) getTarget()).getImageUrl();
        }
        return null;
    }

    public boolean isEnabled() {
        return true;
    }
}
