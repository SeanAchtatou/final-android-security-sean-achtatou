package com.scoreloop.client.android.ui.component.game;

import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.ui.component.base.ComponentActivity;
import com.scoreloop.client.android.ui.component.base.StandardListItem;
import com.skyd.bestpuzzle.n1648.R;

public class GameDetailUserListItem extends StandardListItem<User> {
    public GameDetailUserListItem(ComponentActivity context, User user) {
        super(context, context.getResources().getDrawable(R.drawable.sl_icon_user), user.getDisplayName(), null, user);
    }

    /* access modifiers changed from: protected */
    public String getImageUrl() {
        return ((User) getTarget()).getImageUrl();
    }

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return R.layout.sl_list_item_icon_title;
    }

    public int getType() {
        return 14;
    }
}
