package com.scoreloop.client.android.ui.component.user;

import android.app.Dialog;
import android.os.Bundle;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.controller.RequestControllerObserver;
import com.scoreloop.client.android.core.controller.UserController;
import com.scoreloop.client.android.core.controller.UsersController;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.ui.component.agent.ManageBuddiesTask;
import com.scoreloop.client.android.ui.component.base.CaptionListItem;
import com.scoreloop.client.android.ui.component.base.ComponentListActivity;
import com.scoreloop.client.android.ui.component.base.Constant;
import com.scoreloop.client.android.ui.component.base.EmptyListItem;
import com.scoreloop.client.android.ui.component.base.ExpandableListItem;
import com.scoreloop.client.android.ui.framework.BaseActivity;
import com.scoreloop.client.android.ui.framework.BaseDialog;
import com.scoreloop.client.android.ui.framework.BaseListAdapter;
import com.scoreloop.client.android.ui.framework.BaseListItem;
import com.scoreloop.client.android.ui.framework.OkCancelDialog;
import com.scoreloop.client.android.ui.framework.ValueStore;
import com.skyd.bestpuzzle.n1648.R;
import java.util.ArrayList;
import java.util.List;

public class UserListActivity extends ComponentListActivity<BaseListItem> implements RequestControllerObserver, BaseDialog.OnActionListener {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$scoreloop$client$android$ui$component$user$UserListActivity$RequestType;
    private BaseListItem _addBuddiesListItem;
    private List<User> _buddies;
    private List<User> _buddiesPlaying;
    private BaseListItem _matchBuddyListItem;
    private RequestType _requestType = RequestType.NONE;
    private BaseListItem _seeMoreListItem;
    private boolean _showSeeMore = true;
    private UserController _userController;
    private UsersController _usersController;

    private enum RequestType {
        LOAD_BUDDIES,
        LOAD_RECOMMENDATIONS,
        NONE
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$scoreloop$client$android$ui$component$user$UserListActivity$RequestType() {
        int[] iArr = $SWITCH_TABLE$com$scoreloop$client$android$ui$component$user$UserListActivity$RequestType;
        if (iArr == null) {
            iArr = new int[RequestType.values().length];
            try {
                iArr[RequestType.LOAD_BUDDIES.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[RequestType.LOAD_RECOMMENDATIONS.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[RequestType.NONE.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$com$scoreloop$client$android$ui$component$user$UserListActivity$RequestType = iArr;
        }
        return iArr;
    }

    private void addUsers(BaseListAdapter<BaseListItem> adapter, List<User> users, Boolean playsSessionGame, boolean showSeeMore) {
        int i = 0;
        for (User user : users) {
            if (!showSeeMore || (i = i + 1) <= 2) {
                adapter.add(new UserListItem(this, getResources().getDrawable(R.drawable.sl_icon_user), user, playsSessionGame.booleanValue()));
            } else {
                adapter.add(getSeeMoreListItem());
                return;
            }
        }
    }

    private BaseListItem getAddBuddiesListItem() {
        if (this._addBuddiesListItem == null) {
            this._addBuddiesListItem = new UserAddBuddiesListItem(this);
        }
        return this._addBuddiesListItem;
    }

    private BaseListItem getMatchBuddiesListItem() {
        if (this._matchBuddyListItem == null) {
            this._matchBuddyListItem = new UserFindMatchListItem(this);
        }
        return this._matchBuddyListItem;
    }

    private BaseListItem getSeeMoreListItem() {
        if (this._seeMoreListItem == null) {
            this._seeMoreListItem = new ExpandableListItem(this);
        }
        return this._seeMoreListItem;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setListAdapter(new BaseListAdapter(this));
        this._userController = new UserController(this);
        this._usersController = new UsersController(this);
        addObservedKeys(ValueStore.concatenateKeys(Constant.USER_VALUES, Constant.NUMBER_BUDDIES));
    }

    public void onListItemClick(BaseListItem item) {
        if (item == this._addBuddiesListItem) {
            display(getFactory().createUserAddBuddyScreenDescription());
        } else if (item == this._matchBuddyListItem) {
            showDialogSafe(10, true);
        } else if (item == this._seeMoreListItem) {
            this._showSeeMore = false;
            updateList();
        } else if (item instanceof UserListItem) {
            UserListItem userListItem = (UserListItem) item;
            display(getFactory().createUserDetailScreenDescription((User) userListItem.getTarget(), Boolean.valueOf(userListItem.playsSessionGame())));
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 10:
                OkCancelDialog dialog = new OkCancelDialog(this);
                dialog.setText(getResources().getString(R.string.sl_leave_accept_match_buddies));
                dialog.setOkButtonText(getResources().getString(R.string.sl_leave_accept_match_buddies_ok));
                dialog.setOnActionListener(this);
                dialog.setCancelable(true);
                dialog.setOnDismissListener(this);
                return dialog;
            default:
                return super.onCreateDialog(id);
        }
    }

    public void onAction(BaseDialog dialog, int actionId) {
        dialog.dismiss();
        if (actionId == 0) {
            setNeedsRefresh(RequestType.LOAD_RECOMMENDATIONS.ordinal(), BaseActivity.RefreshMode.SET);
            refreshIfNeeded();
        }
    }

    public void onRefresh(int flags) {
        this._requestType = RequestType.values()[flags];
        User user = getUser();
        Game game = getGame();
        switch ($SWITCH_TABLE$com$scoreloop$client$android$ui$component$user$UserListActivity$RequestType()[this._requestType.ordinal()]) {
            case 1:
                this._buddies = null;
                this._buddiesPlaying = null;
                this._userController.setUser(user);
                showSpinnerFor(this._userController);
                this._userController.loadBuddies();
                if (game != null) {
                    showSpinnerFor(this._usersController);
                    this._usersController.loadBuddies(user, game);
                    return;
                }
                return;
            case 2:
                showSpinnerFor(this._usersController);
                this._usersController.loadRecommendedBuddies(1);
                return;
            default:
                return;
        }
    }

    public void onValueChanged(ValueStore valueStore, String key, Object oldValue, Object newValue) {
        if (isValueChangedFor(key, Constant.NUMBER_BUDDIES, oldValue, newValue)) {
            setNeedsRefresh(RequestType.LOAD_BUDDIES.ordinal(), BaseActivity.RefreshMode.SET);
        }
    }

    public void onValueSetDirty(ValueStore valueStore, String key) {
        if (Constant.NUMBER_BUDDIES.equals(key)) {
            getUserValues().retrieveValue(key, ValueStore.RetrievalMode.NOT_DIRTY, null);
        }
    }

    public void requestControllerDidReceiveResponseSafe(RequestController aRequestController) {
        RequestType requestType = this._requestType;
        this._requestType = RequestType.NONE;
        switch ($SWITCH_TABLE$com$scoreloop$client$android$ui$component$user$UserListActivity$RequestType()[requestType.ordinal()]) {
            case 1:
                if (aRequestController == this._usersController) {
                    this._buddiesPlaying = this._usersController.getUsers();
                } else if (aRequestController == this._userController) {
                    this._buddies = this._userController.getUser().getBuddyUsers();
                    if (this._buddies != null) {
                        this._buddies = new ArrayList(this._buddies);
                    }
                }
                if ((getGame() == null || this._buddiesPlaying != null) && this._buddies != null) {
                    if (getGame() != null) {
                        for (User buddyPlaying : this._buddiesPlaying) {
                            int i = 0;
                            while (true) {
                                if (i < this._buddies.size()) {
                                    if (this._buddies.get(i).getIdentifier().equals(buddyPlaying.getIdentifier())) {
                                        this._buddies.remove(i);
                                    } else {
                                        i++;
                                    }
                                }
                            }
                        }
                    }
                    updateList();
                    return;
                }
                this._requestType = requestType;
                return;
            case 2:
                if (aRequestController == this._usersController) {
                    List<User> users = this._usersController.getUsers();
                    if (users.size() > 0) {
                        showSpinner();
                        ManageBuddiesTask.addBuddy(this, users.get(0), getSessionUserValues(), new ManageBuddiesTask.ManageBuddiesContinuation() {
                            public void withAddedOrRemovedBuddies(int count) {
                                UserListActivity.this.hideSpinner();
                                UserListActivity.this.setNeedsRefresh(RequestType.LOAD_BUDDIES.ordinal(), BaseActivity.RefreshMode.SET);
                                if (!UserListActivity.this.isPaused()) {
                                    UserListActivity.this.showToast(UserListActivity.this.getResources().getString(R.string.sl_format_one_friend_added));
                                }
                            }
                        });
                        return;
                    }
                    return;
                }
                return;
            default:
                return;
        }
    }

    private void updateList() {
        BaseListAdapter<BaseListItem> adapter = getBaseListAdapter();
        adapter.clear();
        boolean isSessionUser = isSessionUser();
        if (isSessionUser) {
            adapter.add(getAddBuddiesListItem());
        }
        boolean hasBuddies = false;
        int otherBuddiesCount = this._buddies.size();
        if (getGame() != null && this._buddiesPlaying.size() > 0) {
            adapter.add(new CaptionListItem(this, null, String.format(getString(R.string.sl_format_friends_playing), getGame().getName())));
            addUsers(adapter, this._buddiesPlaying, true, this._showSeeMore && otherBuddiesCount > 0);
            hasBuddies = true;
        }
        if (otherBuddiesCount > 0) {
            adapter.add(new CaptionListItem(this, null, getString(R.string.sl_friends)));
            addUsers(adapter, this._buddies, false, false);
            hasBuddies = true;
        }
        if (hasBuddies) {
            return;
        }
        if (isSessionUser) {
            adapter.add(getMatchBuddiesListItem());
            return;
        }
        adapter.add(new CaptionListItem(this, null, getString(R.string.sl_friends)));
        adapter.add(new EmptyListItem(this, getString(R.string.sl_no_friends)));
    }
}
