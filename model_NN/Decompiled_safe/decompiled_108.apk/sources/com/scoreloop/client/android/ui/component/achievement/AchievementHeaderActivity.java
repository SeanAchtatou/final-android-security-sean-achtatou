package com.scoreloop.client.android.ui.component.achievement;

import android.os.Bundle;
import com.scoreloop.client.android.ui.component.base.ComponentHeaderActivity;
import com.scoreloop.client.android.ui.component.base.Constant;
import com.scoreloop.client.android.ui.component.base.StringFormatter;
import com.scoreloop.client.android.ui.framework.ValueStore;
import com.skyd.bestpuzzle.n1648.R;

public class AchievementHeaderActivity extends ComponentHeaderActivity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.sl_header_default);
        getImageView().setImageDrawable(getResources().getDrawable(R.drawable.sl_header_icon_achievements));
        setCaption(getGame().getName());
        setTitle(getString(R.string.sl_achievements));
        addObservedKeys(ValueStore.concatenateKeys(Constant.USER_VALUES, Constant.NUMBER_ACHIEVEMENTS), ValueStore.concatenateKeys(Constant.USER_VALUES, Constant.NUMBER_AWARDS));
    }

    public void onValueChanged(ValueStore valueStore, String key, Object oldValue, Object newValue) {
        setSubTitle(StringFormatter.getAchievementsSubTitle(this, getUserValues(), true));
    }

    public void onValueSetDirty(ValueStore valueStore, String key) {
        if (key.equals(Constant.NUMBER_ACHIEVEMENTS)) {
            getUserValues().retrieveValue(key, ValueStore.RetrievalMode.NOT_DIRTY, null);
        }
        if (key.equals(Constant.NUMBER_AWARDS)) {
            getUserValues().retrieveValue(key, ValueStore.RetrievalMode.NOT_DIRTY, null);
        }
    }
}
