package com.scoreloop.client.android.core.controller;

enum d {
    EXACT(null, "exact match"),
    LIKE("like", "pattern match"),
    BEGINS_WITH("begins_with", "string begins with given value"),
    IS("equals", "is"),
    IS_GREATER("greater_than", "is greater than"),
    IS_LESS("less_than", "is less than"),
    IS_NOT("does_not_equal", "is not"),
    EQUALS_ANY("equals_any", "equals any");
    
    private String a;
    private String b;

    private d(String str, String str2) {
        this.b = str;
        this.a = str2;
    }

    public String getDescription() {
        return this.a;
    }

    public String getName() {
        return this.b;
    }
}
