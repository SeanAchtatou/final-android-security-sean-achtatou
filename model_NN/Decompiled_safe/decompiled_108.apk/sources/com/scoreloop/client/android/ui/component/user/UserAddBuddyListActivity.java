package com.scoreloop.client.android.ui.component.user;

import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.controller.SocialProviderController;
import com.scoreloop.client.android.core.controller.SocialProviderControllerObserver;
import com.scoreloop.client.android.core.controller.UsersController;
import com.scoreloop.client.android.core.model.SocialProvider;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.ui.component.agent.ManageBuddiesTask;
import com.scoreloop.client.android.ui.component.base.CaptionListItem;
import com.scoreloop.client.android.ui.component.base.ComponentListActivity;
import com.scoreloop.client.android.ui.component.base.Configuration;
import com.scoreloop.client.android.ui.framework.BaseListAdapter;
import com.scoreloop.client.android.ui.framework.BaseListItem;
import com.skyd.bestpuzzle.n1648.R;
import java.util.List;

public class UserAddBuddyListActivity extends ComponentListActivity<BaseListItem> implements SocialProviderControllerObserver {
    private final Object _addressBookTarget = new Object();
    private final Object _loginTarget = new Object();
    private UsersController usersSearchController;

    private class LoginDialog extends Dialog implements View.OnClickListener {
        private EditText loginEdit;
        private Button okButton;

        LoginDialog(Context context, int style) {
            super(context, style);
        }

        public void onClick(View v) {
            if (v.getId() == R.id.button_ok) {
                dismiss();
                UserAddBuddyListActivity.this.handleDialogClick(this.loginEdit.getText().toString().trim());
            }
        }

        /* access modifiers changed from: protected */
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView((int) R.layout.sl_dialog_login);
            setTitle(UserAddBuddyListActivity.this.getResources().getString(R.string.sl_scoreloop_username));
            this.loginEdit = (EditText) findViewById(R.id.edit_login);
            this.okButton = (Button) findViewById(R.id.button_ok);
            this.okButton.setOnClickListener(this);
        }
    }

    /* access modifiers changed from: private */
    public void handleDialogClick(String login) {
        showSpinnerFor(this.usersSearchController);
        this.usersSearchController.setSearchOperator(UsersController.LoginSearchOperator.EXACT_MATCH);
        this.usersSearchController.searchByLogin(login);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setListAdapter(new BaseListAdapter(this));
        this.usersSearchController = new UsersController(getRequestControllerObserver());
        this.usersSearchController.setSearchesGlobal(true);
        BaseListAdapter<BaseListItem> adapter = getBaseListAdapter();
        Resources res = getResources();
        Configuration configuration = getConfiguration();
        adapter.clear();
        adapter.add(new CaptionListItem(this, null, getString(R.string.sl_add_friends)));
        adapter.add(new UserAddBuddyListItem(this, res.getDrawable(R.drawable.sl_icon_facebook), getString(R.string.sl_facebook), SocialProvider.getSocialProviderForIdentifier(SocialProvider.FACEBOOK_IDENTIFIER)));
        adapter.add(new UserAddBuddyListItem(this, res.getDrawable(R.drawable.sl_icon_twitter), getString(R.string.sl_twitter), SocialProvider.getSocialProviderForIdentifier(SocialProvider.TWITTER_IDENTIFIER)));
        adapter.add(new UserAddBuddyListItem(this, res.getDrawable(R.drawable.sl_icon_myspace), getString(R.string.sl_myspace), SocialProvider.getSocialProviderForIdentifier(SocialProvider.MYSPACE_IDENTIFIER)));
        if (configuration.isFeatureEnabled(Configuration.Feature.ADDRESS_BOOK)) {
            adapter.add(new UserAddBuddyListItem(this, res.getDrawable(R.drawable.sl_icon_addressbook), getString(R.string.sl_addressbook), this._addressBookTarget));
        }
        adapter.add(new UserAddBuddyListItem(this, res.getDrawable(R.drawable.sl_icon_scoreloop), getString(R.string.sl_scoreloop_username), this._loginTarget));
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 19:
                LoginDialog loginDialog = new LoginDialog(this, R.style.sl_dialog);
                loginDialog.setOnDismissListener(this);
                return loginDialog;
            default:
                return super.onCreateDialog(id);
        }
    }

    public void onListItemClick(BaseListItem item) {
        Object target = ((UserAddBuddyListItem) item).getTarget();
        if (target == this._addressBookTarget) {
            showSpinnerFor(this.usersSearchController);
            this.usersSearchController.searchByLocalAddressBook();
        } else if (target == this._loginTarget) {
            showDialogSafe(19, true);
        } else {
            SocialProvider socialProvider = (SocialProvider) target;
            if (socialProvider.isUserConnected(getSessionUser())) {
                showSpinnerFor(this.usersSearchController);
                this.usersSearchController.searchBySocialProvider(socialProvider);
                return;
            }
            SocialProviderController.getSocialProviderController(null, this, socialProvider).connect(this);
        }
    }

    public void requestControllerDidReceiveResponseSafe(RequestController requestController) {
        if (requestController != this.usersSearchController) {
            return;
        }
        if (!this.usersSearchController.isOverLimit()) {
            List<User> users = this.usersSearchController.getUsers();
            if (users.isEmpty()) {
                showToast(getResources().getString(R.string.sl_found_no_user));
                return;
            }
            showSpinner();
            ManageBuddiesTask.addBuddies(this, users, getSessionUserValues(), new ManageBuddiesTask.ManageBuddiesContinuation() {
                public void withAddedOrRemovedBuddies(int addedBuddies) {
                    UserAddBuddyListActivity.this.hideSpinner();
                    if (!UserAddBuddyListActivity.this.isPaused()) {
                        switch (addedBuddies) {
                            case 0:
                                UserAddBuddyListActivity.this.showToast(UserAddBuddyListActivity.this.getResources().getString(R.string.sl_found_no_user));
                                return;
                            case 1:
                                UserAddBuddyListActivity.this.showToast(UserAddBuddyListActivity.this.getResources().getString(R.string.sl_format_one_friend_added));
                                return;
                            default:
                                UserAddBuddyListActivity.this.showToast(String.format(UserAddBuddyListActivity.this.getResources().getString(R.string.sl_format_friends_added), Integer.valueOf(addedBuddies)));
                                return;
                        }
                    }
                }
            });
        } else if (this.usersSearchController.isMaxUserCount()) {
            showToast(getResources().getString(R.string.sl_found_too_many_users));
        } else {
            showToast(String.format(getResources().getString(R.string.sl_format_found_many_users), Integer.valueOf(this.usersSearchController.getCountOfUsers())));
        }
    }

    public void socialProviderControllerDidCancel(SocialProviderController controller) {
    }

    public void socialProviderControllerDidEnterInvalidCredentials(SocialProviderController controller) {
        showDialogSafe(0);
    }

    public void socialProviderControllerDidFail(SocialProviderController controller, Throwable error) {
        showDialogSafe(0);
    }

    public void socialProviderControllerDidSucceed(SocialProviderController socialProviderController) {
        SocialProvider socialProvider = socialProviderController.getSocialProvider();
        if (socialProvider.isUserConnected(getSessionUser())) {
            showSpinnerFor(this.usersSearchController);
            this.usersSearchController.searchBySocialProvider(socialProvider);
        }
    }
}
