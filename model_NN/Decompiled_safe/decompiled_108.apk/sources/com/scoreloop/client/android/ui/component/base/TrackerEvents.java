package com.scoreloop.client.android.ui.component.base;

public class TrackerEvents {
    public static final String CAT_NAVI = "navigation";
    public static final String CAT_REQUEST = "request";
    public static final String LABEL_ERROR = "error";
    public static final String LABEL_SUCCESS = "success";
    public static final String NAVI_DIALOG = "dialog.%s";
    public static final String NAVI_HEADER_ACCOUNTSETTINGS = "header.account-settings";
    public static final String NAVI_HEADER_GAME_FEATURED = "header.game-featured";
    public static final String NAVI_HEADER_GAME_GET = "header.game-get";
    public static final String NAVI_HEADER_GAME_LAUNCH = "header.game-launch";
    public static final String NAVI_OM_ACCOUNT_SETTINGS = "optionsmenu.account-settings";
    public static final String NAVI_OM_OPENED = "optionsmenu.opened";
    public static final String NAVI_OM_USER_INAPPROPRIATE = "optionsmenu.user-inappropriate";
    public static final String NAVI_OM_USER_REMOVE = "optionsmenu.user-remove";
    public static final String NAVI_SHORTCUT_ACCOUNT_SETTINGS = "shortcut.account-settings";
    public static final String NAVI_SHORTCUT_ACTIVITIES = "shortcut.activities";
    public static final String NAVI_SHORTCUT_FRIENDS = "shortcut.friends";
    public static final String NAVI_SHORTCUT_MARKET = "shortcut.market";
    public static final String REQ_CHANGE_EMAIL = "result.change-email";
    public static final String REQ_CHANGE_USERNAME = "result.change-username";
    public static final String REQ_CHANGE_USERNAME_FIRSTTIME = "result.change-username-firsttime";
    public static final String REQ_MERGE_ACCOUNT = "result.merge-account";
}
