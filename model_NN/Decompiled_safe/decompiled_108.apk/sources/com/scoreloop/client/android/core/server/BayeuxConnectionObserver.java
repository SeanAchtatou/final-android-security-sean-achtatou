package com.scoreloop.client.android.core.server;

import org.json.JSONObject;

interface BayeuxConnectionObserver {

    public interface SecureBayeuxConnectionObserver extends BayeuxConnectionObserver {
    }

    void a(a aVar, Integer num, Object obj, String str, int i);

    void a(a aVar, JSONObject jSONObject);

    JSONObject b(a aVar, JSONObject jSONObject);
}
