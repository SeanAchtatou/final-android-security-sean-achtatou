package com.admob.android.ads;

import android.content.Context;
import android.util.Log;
import android.webkit.WebView;
import com.admob.android.ads.InterstitialAd;
import com.admob.android.ads.view.AdMobWebView;
import com.scoreloop.client.android.ui.component.base.TrackerEvents;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Hashtable;
import java.util.Timer;
import java.util.TimerTask;
import org.json.JSONObject;

/* compiled from: AdMobFlexWebView */
public final class z extends AdMobWebView {
    public JSONObject a = null;
    public JSONObject b = null;
    private boolean e = false;

    public z(Context context, j jVar) {
        super(context, false, null);
        this.d = new a(this, this, jVar);
        setWebViewClient(this.d);
    }

    public final void b() {
        if ((this.d instanceof a) && ((a) this.d).b && !this.e) {
            this.e = true;
            a("init", this.a == null ? "null" : this.a, this.b == null ? "null" : this.b);
        }
    }

    /* compiled from: AdMobFlexWebView */
    public class a extends ad {
        boolean a = false;
        boolean b = false;
        j c;
        private Timer e;
        private TimerTask f;

        public a(z zVar, AdMobWebView adMobWebView, j jVar) {
            super(adMobWebView);
            this.c = jVar;
        }

        public final void onPageFinished(WebView webView, String str) {
            AdMobWebView adMobWebView = (AdMobWebView) this.d.get();
            if (adMobWebView != null) {
                if (str != null && str.equals(adMobWebView.c)) {
                    this.b = true;
                    super.onPageFinished(webView, str);
                    if (adMobWebView instanceof z) {
                        ((z) adMobWebView).b();
                    }
                    if (InterstitialAd.c.a(AdManager.LOG, 3)) {
                        Log.d(AdManager.LOG, "startResponseTimer()");
                    }
                    this.f = new TimerTask() {
                        public final void run() {
                            if (!a.this.a) {
                                a.this.a = true;
                                if (a.this.c != null) {
                                    a.this.c.a(false);
                                }
                            }
                        }
                    };
                    this.e = new Timer();
                    this.e.schedule(this.f, 10000);
                } else if (InterstitialAd.c.a(AdManager.LOG, 4)) {
                    Log.i(AdManager.LOG, "Unexpected page loaded, urlThatFinished: " + str);
                }
            }
        }

        public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
            Hashtable<String, String> a2;
            String str2;
            Hashtable<String, String> a3;
            String str3;
            if (InterstitialAd.c.a(AdManager.LOG, 2)) {
                Log.v(AdManager.LOG, "shouldOverrideUrlLoading, url: " + str);
            }
            try {
                URI uri = new URI(str);
                if ("admob".equals(uri.getScheme())) {
                    String host = uri.getHost();
                    if ("ready".equals(host)) {
                        if (this.a) {
                            return true;
                        }
                        this.a = true;
                        if (InterstitialAd.c.a(AdManager.LOG, 3)) {
                            Log.d(AdManager.LOG, "cancelResponseTimer()");
                        }
                        if (this.e != null) {
                            this.e.cancel();
                        }
                        String query = uri.getQuery();
                        if (query == null || (a3 = a(query)) == null || (str3 = a3.get(TrackerEvents.LABEL_SUCCESS)) == null || !"true".equalsIgnoreCase(str3)) {
                            if (this.c != null) {
                                this.c.a(false);
                            }
                            return true;
                        }
                        if (this.c != null) {
                            this.c.a(true);
                        }
                        return true;
                    } else if ("movie".equals(host)) {
                        String query2 = uri.getQuery();
                        if (query2 != null && (a2 = a(query2)) != null && (str2 = a2.get("action")) != null && !"play".equalsIgnoreCase(str2) && !"pause".equalsIgnoreCase(str2) && !"stop".equalsIgnoreCase(str2) && !"remove".equalsIgnoreCase(str2) && !"replay".equalsIgnoreCase(str2) && InterstitialAd.c.a(AdManager.LOG, 5)) {
                            Log.w(AdManager.LOG, "Unknown actionString, admob://movie?action=" + str2);
                        }
                        return true;
                    }
                }
            } catch (URISyntaxException e2) {
                Log.w(AdManager.LOG, "Bad link URL in AdMob web view.", e2);
            }
            return super.shouldOverrideUrlLoading(webView, str);
        }
    }
}
