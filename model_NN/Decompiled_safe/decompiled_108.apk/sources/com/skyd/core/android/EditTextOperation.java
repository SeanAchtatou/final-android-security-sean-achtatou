package com.skyd.core.android;

import android.widget.EditText;

public class EditTextOperation {
    public static String getSelectedText(EditText c) {
        int s = c.getSelectionStart();
        int e = c.getSelectionEnd();
        return c.getText().subSequence(Math.min(s, e), Math.max(s, e)).toString();
    }

    public static int replaceSelectedText(EditText c, String NewText, boolean SelectNewText) {
        int s = c.getSelectionStart();
        int e = c.getSelectionEnd();
        int a = Math.min(s, e);
        int b = Math.max(s, e);
        c.setSelection(0, 0);
        int o = replaceRangeToNewText(c, a, b - a, NewText);
        if (SelectNewText) {
            c.setSelection(a, NewText.length() + a);
        } else {
            c.setSelection(NewText.length() + a, NewText.length() + a);
        }
        return o;
    }

    public static int replaceSelectedText(EditText c, String NewText) {
        return replaceSelectedText(c, NewText, true);
    }

    public static int replaceRangeToNewText(EditText c, int startIndex, int length, String NewText) {
        c.setText(((Object) c.getText().subSequence(0, startIndex)) + NewText + ((Object) c.getText().subSequence(startIndex + length, c.getText().length())));
        return NewText.length() - length;
    }
}
