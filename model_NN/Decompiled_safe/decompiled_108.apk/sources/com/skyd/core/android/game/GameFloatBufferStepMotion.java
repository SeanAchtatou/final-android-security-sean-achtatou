package com.skyd.core.android.game;

public class GameFloatBufferStepMotion extends GameNumberBufferStepMotion<Float> {
    private IGameValueDuct<GameObject, Float> _ValueDuct = null;

    public IGameValueDuct<GameObject, Float> getValueDuct() {
        return this._ValueDuct;
    }

    public void setValueDuct(IGameValueDuct<GameObject, Float> value) {
        this._ValueDuct = value;
    }

    public void setValueDuctToDefault() {
        setValueDuct(null);
    }

    public GameFloatBufferStepMotion(Float targetValue, Float stepLength, Float tolerance, float buffer, IGameValueDuct<GameObject, Float> valueDuct) {
        super(targetValue, stepLength, tolerance, buffer);
        setValueDuct(valueDuct);
    }

    /* access modifiers changed from: protected */
    public Float mul(Float v1, float v2) {
        return Float.valueOf(v1.floatValue() * v2);
    }

    /* access modifiers changed from: protected */
    public Float getCurrentValue(GameObject obj) {
        return getValueDuct().getValueFrom(obj);
    }

    /* access modifiers changed from: protected */
    public Float plus(Float v1, Float v2) {
        return Float.valueOf(v1.floatValue() + v2.floatValue());
    }

    /* access modifiers changed from: protected */
    public void setCurrentValue(GameObject obj, Float value) {
        getValueDuct().setValueTo(obj, value);
    }
}
