package com.skyd.bestpuzzle;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.widget.Toast;
import com.scoreloop.client.android.core.model.Score;
import com.scoreloop.client.android.ui.ScoreloopManagerSingleton;
import com.skyd.bestpuzzle.n1648.R;
import com.skyd.core.android.game.GameImageSpirit;
import com.skyd.core.android.game.GameMaster;
import com.skyd.core.android.game.GameObject;
import com.skyd.core.android.game.GameScene;
import com.skyd.core.android.game.GameSpirit;
import com.skyd.core.draw.DrawHelper;
import com.skyd.core.vector.Vector2DF;
import com.skyd.core.vector.VectorRect2DF;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

public class PuzzleScene extends GameScene {
    public Controller Controller;
    public Desktop Desktop;
    boolean IsDrawingPuzzle;
    boolean IsDrawnSelectRect;
    public GameImageSpirit MoveButton;
    public UI MoveButtonSlot;
    public OriginalImage OriginalImage;
    public long PastTime;
    public long StartTime;
    public UI SubmitScoreButton;
    /* access modifiers changed from: private */
    public RectF SubmitScoreButtonRect;
    public GameSpirit Time;
    public UI ViewFullButton;
    /* access modifiers changed from: private */
    public RectF ViewFullButtonRect;
    public UI ZoomInButton;
    public UI ZoomOutButton;
    private boolean _IsFinished = false;
    private ArrayList<OnIsFinishedChangedListener> _IsFinishedChangedListenerList = null;
    long savetime = Long.MIN_VALUE;
    RectF selectRect;

    public interface OnIsFinishedChangedListener {
        void OnIsFinishedChangedEvent(Object obj, boolean z);
    }

    public PuzzleScene() {
        setMaxDrawCacheLayer(25.0f);
        this.StartTime = new Date().getTime();
        loadGameState();
    }

    public void loadGameState() {
        Center c = (Center) GameMaster.getContext().getApplicationContext();
        this.PastTime = c.getPastTime().longValue();
        this._IsFinished = c.getIsFinished().booleanValue();
    }

    public void saveGameState() {
        Center c = (Center) GameMaster.getContext().getApplicationContext();
        if (getIsFinished()) {
            c.setPastTime(Long.valueOf(this.PastTime));
        } else {
            c.setPastTime(Long.valueOf(this.PastTime + (new Date().getTime() - this.StartTime)));
        }
        c.setIsFinished(Boolean.valueOf(getIsFinished()));
    }

    public void setFinished() {
        if (!getIsFinished()) {
            this.SubmitScoreButton.show();
        }
        setIsFinished(true);
        this.PastTime += new Date().getTime() - this.StartTime;
    }

    public boolean getIsFinished() {
        return this._IsFinished;
    }

    private void setIsFinished(boolean value) {
        onIsFinishedChanged(value);
        this._IsFinished = value;
    }

    private void setIsFinishedToDefault() {
        setIsFinished(false);
    }

    public boolean addOnIsFinishedChangedListener(OnIsFinishedChangedListener listener) {
        if (this._IsFinishedChangedListenerList == null) {
            this._IsFinishedChangedListenerList = new ArrayList<>();
        } else if (this._IsFinishedChangedListenerList.contains(listener)) {
            return false;
        }
        this._IsFinishedChangedListenerList.add(listener);
        return true;
    }

    public boolean removeOnIsFinishedChangedListener(OnIsFinishedChangedListener listener) {
        if (this._IsFinishedChangedListenerList == null || !this._IsFinishedChangedListenerList.contains(listener)) {
            return false;
        }
        this._IsFinishedChangedListenerList.remove(listener);
        return true;
    }

    public void clearOnIsFinishedChangedListeners() {
        if (this._IsFinishedChangedListenerList != null) {
            this._IsFinishedChangedListenerList.clear();
        }
    }

    /* access modifiers changed from: protected */
    public void onIsFinishedChanged(boolean newValue) {
        if (this._IsFinishedChangedListenerList != null) {
            Iterator<OnIsFinishedChangedListener> it = this._IsFinishedChangedListenerList.iterator();
            while (it.hasNext()) {
                it.next().OnIsFinishedChangedEvent(this, newValue);
            }
        }
    }

    public int getTargetCacheID() {
        return 1;
    }

    /* access modifiers changed from: protected */
    public void drawChilds(Canvas c, Rect drawArea) {
        this.IsDrawingPuzzle = false;
        this.IsDrawnSelectRect = false;
        super.drawChilds(c, drawArea);
    }

    /* access modifiers changed from: protected */
    public boolean onDrawingChild(GameObject child, Canvas c, Rect drawArea) {
        float lvl = ((GameSpirit) child).getLevel();
        if (!this.IsDrawingPuzzle && lvl <= 10.0f) {
            this.IsDrawingPuzzle = true;
            c.setMatrix(this.Desktop.getMatrix());
        } else if (lvl > 10.0f && this.IsDrawingPuzzle) {
            this.IsDrawingPuzzle = false;
            c.setMatrix(new Matrix());
        }
        if (lvl > 25.0f && this.selectRect != null && !this.IsDrawnSelectRect) {
            this.IsDrawnSelectRect = true;
            Paint p = new Paint();
            p.setARGB(100, 180, 225, 255);
            c.drawRect(this.selectRect, p);
        }
        return super.onDrawingChild(child, c, drawArea);
    }

    public void load(Context c) {
        loadOriginalImage(c);
        loadDesktop(c);
        loadInterface(c);
        loadPuzzle(c);
        loadPuzzleState();
        this.Desktop.updateCurrentObserveAreaRectF();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.skyd.core.draw.DrawHelper.calculateScaleSize(float, float, float, float, boolean):com.skyd.core.vector.Vector2DF
     arg types: [int, int, float, float, int]
     candidates:
      com.skyd.core.draw.DrawHelper.calculateScaleSize(double, double, double, double, boolean):com.skyd.core.vector.Vector2D
      com.skyd.core.draw.DrawHelper.calculateScaleSize(float, float, float, float, boolean):com.skyd.core.vector.Vector2DF */
    public void loadOriginalImage(Context c) {
        this.OriginalImage = new OriginalImage() {
            /* access modifiers changed from: protected */
            public boolean onDrawing(Canvas c, Rect drawArea) {
                c.drawARGB(180, 0, 0, 0);
                return super.onDrawing(c, drawArea);
            }
        };
        this.OriginalImage.setLevel(50.0f);
        this.OriginalImage.setName("OriginalImage");
        this.OriginalImage.getSize().resetWith(DrawHelper.calculateScaleSize(320.0f, 240.0f, (float) (GameMaster.getScreenWidth() - 40), (float) (GameMaster.getScreenHeight() - 40), true));
        this.OriginalImage.setTotalSeparateColumn(8);
        this.OriginalImage.setTotalSeparateRow(6);
        this.OriginalImage.getOriginalSize().reset(800.0f, 600.0f);
        this.OriginalImage.hide();
        this.OriginalImage.setIsUseAbsolutePosition(true);
        this.OriginalImage.setIsUseAbsoluteSize(true);
        this.OriginalImage.getImage().setIsUseAbsolutePosition(true);
        this.OriginalImage.getImage().setIsUseAbsoluteSize(true);
        this.OriginalImage.getPosition().reset(((float) (GameMaster.getScreenWidth() / 2)) - (this.OriginalImage.getSize().getX() / 2.0f), ((float) (GameMaster.getScreenHeight() / 2)) - (this.OriginalImage.getSize().getY() / 2.0f));
        getSpiritList().add(this.OriginalImage);
    }

    public void loadDesktop(Context c) {
        this.Desktop = new Desktop();
        this.Desktop.setLevel(-9999.0f);
        this.Desktop.setName("Desktop");
        this.Desktop.getSize().reset(1600.0f, 1200.0f);
        this.Desktop.getCurrentObservePosition().reset(800.0f, 600.0f);
        this.Desktop.setOriginalImage(this.OriginalImage);
        this.Desktop.setIsUseAbsolutePosition(true);
        this.Desktop.setIsUseAbsoluteSize(true);
        this.Desktop.ColorAPaint = new Paint();
        this.Desktop.ColorAPaint.setColor(Color.argb(255, 25, 70, 66));
        this.Desktop.ColorBPaint = new Paint();
        this.Desktop.ColorBPaint.setColor(Color.argb(255, 19, 64, 60));
        getSpiritList().add(this.Desktop);
    }

    public void loadPuzzle(Context c) {
        this.Controller = new Controller();
        this.Controller.setLevel(10.0f);
        this.Controller.setName("Controller");
        this.Controller.setDesktop(this.Desktop);
        this.Controller.setIsUseAbsolutePosition(true);
        this.Controller.setIsUseAbsoluteSize(true);
        getSpiritList().add(this.Controller);
        Puzzle.getRealitySize().reset(100.0f, 100.0f);
        Puzzle.getMaxRadius().reset(76.66666f, 76.66666f);
        c154771092(c);
        c291914853(c);
        c1833396327(c);
        c435179291(c);
        c25315569(c);
        c2117829250(c);
        c1162243469(c);
        c1656792152(c);
        c255067096(c);
        c505905760(c);
        c612016294(c);
        c1809882698(c);
        c1729091838(c);
        c1875826204(c);
        c234316261(c);
        c1811051090(c);
        c2037757932(c);
        c826639736(c);
        c515390279(c);
        c1212010540(c);
        c983352780(c);
        c11363373(c);
        c1098608012(c);
        c761938108(c);
        c1628243632(c);
        c681167814(c);
        c1895508514(c);
        c717599992(c);
        c376602254(c);
        c1378962629(c);
        c20104293(c);
        c342561831(c);
        c1214901854(c);
        c591757449(c);
        c2123219446(c);
        c994920776(c);
        c926875513(c);
        c1341340930(c);
        c1042524943(c);
        c574086540(c);
        c1700115332(c);
        c396314980(c);
        c361616815(c);
        c1147900269(c);
        c275665014(c);
        c1701867261(c);
        c1703300956(c);
        c1045321425(c);
    }

    public void loadInterface(Context c) {
        this.MoveButton = new GameImageSpirit();
        this.MoveButton.setLevel(22.0f);
        this.MoveButton.getImage().loadImageFromResource(c, R.drawable.movebutton);
        this.MoveButton.getSize().reset(155.0f, 155.0f);
        this.MoveButton.setIsUseAbsolutePosition(true);
        getSpiritList().add(this.MoveButton);
        final Vector2DF mvds = this.MoveButton.getDisplaySize();
        mvds.scale(0.5f);
        this.MoveButton.getPosition().reset(5.0f + mvds.getX(), ((float) (GameMaster.getScreenHeight() - 5)) - mvds.getY());
        this.MoveButton.getPositionOffset().resetWith(mvds.negateNew());
        this.MoveButtonSlot = new UI() {
            Vector2DF movevector;
            boolean needmove;

            public void executive(Vector2DF point) {
                this.movevector = point.minusNew(getPosition()).restrainLength(mvds.getX() * 0.4f);
                this.needmove = true;
            }

            /* access modifiers changed from: protected */
            public void updateSelf() {
                if (this.needmove) {
                    PuzzleScene.this.MoveButton.getPosition().resetWith(getPosition().plusNew(this.movevector));
                    PuzzleScene.this.Desktop.getCurrentObservePosition().plus(this.movevector);
                    PuzzleScene.this.refreshDrawCacheBitmap();
                }
                super.updateSelf();
            }

            public boolean isInArea(Vector2DF point) {
                return point.minusNew(getPosition()).getLength() < mvds.getX() + 5.0f;
            }

            public void reset() {
                this.needmove = false;
                PuzzleScene.this.MoveButton.getPosition().resetWith(getPosition());
                PuzzleScene.this.refreshDrawCacheBitmap();
            }
        };
        this.MoveButtonSlot.setLevel(21.0f);
        this.MoveButtonSlot.getImage().loadImageFromResource(c, R.drawable.movebuttonslot);
        this.MoveButtonSlot.getSize().reset(155.0f, 155.0f);
        this.MoveButtonSlot.setIsUseAbsolutePosition(true);
        getSpiritList().add(this.MoveButtonSlot);
        this.MoveButtonSlot.getPosition().resetWith(this.MoveButton.getPosition());
        this.MoveButtonSlot.getPositionOffset().resetWith(this.MoveButton.getPositionOffset());
        this.ZoomInButton = new UI() {
            boolean zoom;

            public void executive(Vector2DF point) {
                this.zoom = true;
            }

            public boolean isInArea(Vector2DF point) {
                return point.minusNew(getPosition()).getLength() < getDisplaySize().getX() / 2.0f;
            }

            public void reset() {
                this.zoom = false;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{java.lang.Math.min(float, float):float}
             arg types: [int, float]
             candidates:
              ClspMth{java.lang.Math.min(double, double):double}
              ClspMth{java.lang.Math.min(long, long):long}
              ClspMth{java.lang.Math.min(int, int):int}
              ClspMth{java.lang.Math.min(float, float):float} */
            /* access modifiers changed from: protected */
            public void updateSelf() {
                if (this.zoom) {
                    PuzzleScene.this.Desktop.setZoom(Math.min(1.3f, PuzzleScene.this.Desktop.getZoom() * 1.05f));
                    PuzzleScene.this.refreshDrawCacheBitmap();
                }
                super.updateSelf();
            }
        };
        this.ZoomInButton.setLevel(21.0f);
        this.ZoomInButton.getImage().loadImageFromResource(c, R.drawable.zoombutton1);
        this.ZoomInButton.getSize().reset(77.0f, 77.0f);
        this.ZoomInButton.setIsUseAbsolutePosition(true);
        getSpiritList().add(this.ZoomInButton);
        Vector2DF zibds = this.ZoomInButton.getDisplaySize().scale(0.5f);
        this.ZoomInButton.getPosition().reset(((float) (GameMaster.getScreenWidth() - 50)) - zibds.getX(), ((float) (GameMaster.getScreenHeight() - 10)) - zibds.getY());
        this.ZoomInButton.getPositionOffset().resetWith(zibds.negateNew());
        this.ZoomOutButton = new UI() {
            boolean zoom;

            public void executive(Vector2DF point) {
                this.zoom = true;
            }

            public boolean isInArea(Vector2DF point) {
                return point.minusNew(getPosition()).getLength() < getDisplaySize().getX() / 2.0f;
            }

            public void reset() {
                this.zoom = false;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{java.lang.Math.max(float, float):float}
             arg types: [int, float]
             candidates:
              ClspMth{java.lang.Math.max(double, double):double}
              ClspMth{java.lang.Math.max(int, int):int}
              ClspMth{java.lang.Math.max(long, long):long}
              ClspMth{java.lang.Math.max(float, float):float} */
            /* access modifiers changed from: protected */
            public void updateSelf() {
                if (this.zoom) {
                    PuzzleScene.this.Desktop.setZoom(Math.max(0.3f, PuzzleScene.this.Desktop.getZoom() * 0.95f));
                    PuzzleScene.this.refreshDrawCacheBitmap();
                    GameMaster.log(this, Float.valueOf(PuzzleScene.this.Desktop.getCurrentObserveAreaRectF().width()));
                }
                super.updateSelf();
            }
        };
        this.ZoomOutButton.setLevel(21.0f);
        this.ZoomOutButton.getImage().loadImageFromResource(c, R.drawable.zoombutton2);
        this.ZoomOutButton.getSize().reset(77.0f, 77.0f);
        this.ZoomOutButton.setIsUseAbsolutePosition(true);
        getSpiritList().add(this.ZoomOutButton);
        Vector2DF zobds = this.ZoomOutButton.getDisplaySize().scale(0.5f);
        this.ZoomOutButton.getPosition().reset((((float) (GameMaster.getScreenWidth() - 100)) - zobds.getX()) - (zibds.getX() * 2.0f), ((float) (GameMaster.getScreenHeight() - 10)) - zobds.getY());
        this.ZoomOutButton.getPositionOffset().resetWith(zobds.negateNew());
        this.ViewFullButton = new UI() {
            public void executive(Vector2DF point) {
                if (PuzzleScene.this.OriginalImage.getImage().getImage() == null) {
                    PuzzleScene.this.OriginalImage.getImage().loadImageFromResource(GameMaster.getContext(), R.drawable.oim);
                }
                PuzzleScene.this.OriginalImage.show();
            }

            public boolean isInArea(Vector2DF point) {
                return point.isIn(PuzzleScene.this.ViewFullButtonRect);
            }

            public void reset() {
                PuzzleScene.this.OriginalImage.hide();
            }
        };
        this.ViewFullButton.getImage().loadImageFromResource(c, R.drawable.viewfull);
        this.ViewFullButton.setLevel(21.0f);
        this.ViewFullButton.setIsUseAbsolutePosition(true);
        this.ViewFullButton.getSize().reset(83.0f, 38.0f);
        getSpiritList().add(this.ViewFullButton);
        this.ViewFullButton.getPosition().reset(((float) (GameMaster.getScreenWidth() - 15)) - this.ViewFullButton.getDisplaySize().getX(), 15.0f);
        this.ViewFullButtonRect = new VectorRect2DF(this.ViewFullButton.getPosition(), this.ViewFullButton.getDisplaySize()).getRectF();
        this.SubmitScoreButton = new UI() {
            public void executive(Vector2DF point) {
                Score score = new Score(Double.valueOf(Math.ceil((double) (PuzzleScene.this.PastTime / 1000))), null);
                score.setMode(21);
                ScoreloopManagerSingleton.get().onGamePlayEnded(score);
                hide();
                Toast.makeText(GameMaster.getContext(), (int) R.string.Submitting, 1).show();
            }

            public boolean isInArea(Vector2DF point) {
                return getVisibleOriginalValue() && point.isIn(PuzzleScene.this.SubmitScoreButtonRect);
            }

            public void reset() {
            }
        };
        this.SubmitScoreButton.getImage().loadImageFromResource(c, R.drawable.submitscore);
        this.SubmitScoreButton.setLevel(21.0f);
        this.SubmitScoreButton.setIsUseAbsolutePosition(true);
        this.SubmitScoreButton.getSize().reset(114.0f, 40.0f);
        getSpiritList().add(this.SubmitScoreButton);
        this.SubmitScoreButton.getPosition().reset(15.0f, 60.0f);
        this.SubmitScoreButtonRect = new VectorRect2DF(this.SubmitScoreButton.getPosition(), this.SubmitScoreButton.getDisplaySize()).getRectF();
        this.SubmitScoreButton.hide();
        this.Time = new GameSpirit() {
            DecimalFormat FMT = new DecimalFormat("00");
            Paint p1;
            Paint p2;

            public GameObject getDisplayContentChild() {
                return null;
            }

            /* access modifiers changed from: protected */
            public void drawSelf(Canvas c, Rect drawArea) {
                if (this.p1 == null) {
                    this.p1 = new Paint();
                    this.p1.setARGB(160, 255, 255, 255);
                    this.p1.setAntiAlias(true);
                    this.p1.setTextSize(28.0f);
                    this.p1.setTypeface(Typeface.createFromAsset(GameMaster.getContext().getAssets(), "fonts/font.ttf"));
                }
                if (this.p2 == null) {
                    this.p2 = new Paint();
                    this.p2.setARGB(100, 0, 0, 0);
                    this.p2.setAntiAlias(true);
                    this.p2.setTextSize(28.0f);
                    this.p2.setTypeface(Typeface.createFromAsset(GameMaster.getContext().getAssets(), "fonts/font.ttf"));
                }
                long t = PuzzleScene.this.getIsFinished() ? PuzzleScene.this.PastTime : PuzzleScene.this.PastTime + (new Date().getTime() - PuzzleScene.this.StartTime);
                long h = t / 3600000;
                long m = (t - (((1000 * h) * 60) * 60)) / 60000;
                String str = String.valueOf(this.FMT.format(h)) + ":" + this.FMT.format(m) + ":" + this.FMT.format(((t - (((1000 * h) * 60) * 60)) - ((1000 * m) * 60)) / 1000) + (PuzzleScene.this.getIsFinished() ? " Finished" : "");
                c.drawText(str, 16.0f, 44.0f, this.p2);
                c.drawText(str, 15.0f, 43.0f, this.p1);
                super.drawSelf(c, drawArea);
            }
        };
        this.Time.setLevel(40.0f);
        getSpiritList().add(this.Time);
    }

    public float getMaxPuzzleLevel() {
        float maxlvl = 0.0f;
        Iterator<Puzzle> it = this.Desktop.getPuzzleList().iterator();
        while (it.hasNext()) {
            Puzzle f = it.next();
            if (f.getLevel() <= 10.0f) {
                maxlvl = Math.max(f.getLevel(), maxlvl);
            }
        }
        return maxlvl;
    }

    public Puzzle getTouchPuzzle(Vector2DF v) {
        Vector2DF rv = this.Desktop.reMapPoint(v);
        Puzzle o = null;
        float olen = 99999.0f;
        for (int i = getSpiritList().size() - 1; i >= 0; i--) {
            if (getSpiritList().get(i) instanceof Puzzle) {
                Puzzle p = (Puzzle) getSpiritList().get(i);
                float len = p.getPositionInDesktop().minusNew(rv).getLength();
                if (len <= Puzzle.getRealitySize().getX() * 0.4f) {
                    return p;
                }
                if (len <= Puzzle.getRealitySize().getX() && olen > len) {
                    o = p;
                    olen = len;
                }
            }
        }
        return o;
    }

    public void startDrawSelectRect(Vector2DF startDragPoint, Vector2DF v) {
        this.selectRect = new VectorRect2DF(startDragPoint, v.minusNew(startDragPoint)).getFixedRectF();
    }

    public void stopDrawSelectRect() {
        this.selectRect = null;
    }

    public ArrayList<Puzzle> getSelectPuzzle(Vector2DF startDragPoint, Vector2DF v) {
        Vector2DF rs = this.Desktop.reMapPoint(startDragPoint);
        ArrayList<Puzzle> l = new ArrayList<>();
        RectF rect = new VectorRect2DF(rs, this.Desktop.reMapPoint(v).minusNew(rs)).getFixedRectF();
        float xo = Puzzle.getRealitySize().getX() / 3.0f;
        float yo = Puzzle.getRealitySize().getY() / 3.0f;
        RectF sr = new RectF(rect.left - xo, rect.top - yo, rect.right + xo, rect.bottom + yo);
        Iterator<Puzzle> it = this.Desktop.getPuzzleList().iterator();
        while (it.hasNext()) {
            Puzzle f = it.next();
            if (f.getPositionInDesktop().isIn(sr)) {
                l.add(f);
            }
        }
        return l;
    }

    public void loadPuzzleState() {
        Center c = (Center) GameMaster.getContext().getApplicationContext();
        SharedPreferences s = c.getSharedPreferences();
        Iterator<Puzzle> it = this.Desktop.getPuzzleList().iterator();
        while (it.hasNext()) {
            it.next().load(c, s);
        }
    }

    public void savePuzzleState() {
        Center c = (Center) GameMaster.getContext().getApplicationContext();
        SharedPreferences.Editor e = c.getSharedPreferences().edit();
        Iterator<Puzzle> it = this.Desktop.getPuzzleList().iterator();
        while (it.hasNext()) {
            it.next().save(c, e);
        }
        e.commit();
    }

    public void reset() {
        this.Controller.reset();
        Iterator<Puzzle> it = this.Desktop.getPuzzleList().iterator();
        while (it.hasNext()) {
            Puzzle f = it.next();
            f.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
            this.Desktop.RandomlyPlaced(f);
        }
        refreshDrawCacheBitmap();
        setIsFinishedToDefault();
        this.StartTime = new Date().getTime();
        this.PastTime = 0;
        this.SubmitScoreButton.hide();
        saveGameState();
        savePuzzleState();
    }

    /* access modifiers changed from: package-private */
    public void c154771092(Context c) {
        Puzzle p154771092 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load154771092(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save154771092(editor, this);
            }
        };
        p154771092.setID(154771092);
        p154771092.setName("154771092");
        p154771092.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p154771092);
        this.Desktop.RandomlyPlaced(p154771092);
        p154771092.setTopEdgeType(EdgeType.Flat);
        p154771092.setBottomEdgeType(EdgeType.Convex);
        p154771092.setLeftEdgeType(EdgeType.Flat);
        p154771092.setRightEdgeType(EdgeType.Convex);
        p154771092.setTopExactPuzzleID(-1);
        p154771092.setBottomExactPuzzleID(291914853);
        p154771092.setLeftExactPuzzleID(-1);
        p154771092.setRightExactPuzzleID(1162243469);
        p154771092.getDisplayImage().loadImageFromResource(c, R.drawable.p154771092h);
        p154771092.setExactRow(0);
        p154771092.setExactColumn(0);
        p154771092.getSize().reset(126.6667f, 126.6667f);
        p154771092.getPositionOffset().reset(-50.0f, -50.0f);
        p154771092.setIsUseAbsolutePosition(true);
        p154771092.setIsUseAbsoluteSize(true);
        p154771092.getImage().setIsUseAbsolutePosition(true);
        p154771092.getImage().setIsUseAbsoluteSize(true);
        p154771092.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p154771092.resetPosition();
        getSpiritList().add(p154771092);
    }

    /* access modifiers changed from: package-private */
    public void c291914853(Context c) {
        Puzzle p291914853 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load291914853(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save291914853(editor, this);
            }
        };
        p291914853.setID(291914853);
        p291914853.setName("291914853");
        p291914853.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p291914853);
        this.Desktop.RandomlyPlaced(p291914853);
        p291914853.setTopEdgeType(EdgeType.Concave);
        p291914853.setBottomEdgeType(EdgeType.Concave);
        p291914853.setLeftEdgeType(EdgeType.Flat);
        p291914853.setRightEdgeType(EdgeType.Concave);
        p291914853.setTopExactPuzzleID(154771092);
        p291914853.setBottomExactPuzzleID(1833396327);
        p291914853.setLeftExactPuzzleID(-1);
        p291914853.setRightExactPuzzleID(1656792152);
        p291914853.getDisplayImage().loadImageFromResource(c, R.drawable.p291914853h);
        p291914853.setExactRow(1);
        p291914853.setExactColumn(0);
        p291914853.getSize().reset(100.0f, 100.0f);
        p291914853.getPositionOffset().reset(-50.0f, -50.0f);
        p291914853.setIsUseAbsolutePosition(true);
        p291914853.setIsUseAbsoluteSize(true);
        p291914853.getImage().setIsUseAbsolutePosition(true);
        p291914853.getImage().setIsUseAbsoluteSize(true);
        p291914853.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p291914853.resetPosition();
        getSpiritList().add(p291914853);
    }

    /* access modifiers changed from: package-private */
    public void c1833396327(Context c) {
        Puzzle p1833396327 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1833396327(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1833396327(editor, this);
            }
        };
        p1833396327.setID(1833396327);
        p1833396327.setName("1833396327");
        p1833396327.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1833396327);
        this.Desktop.RandomlyPlaced(p1833396327);
        p1833396327.setTopEdgeType(EdgeType.Convex);
        p1833396327.setBottomEdgeType(EdgeType.Convex);
        p1833396327.setLeftEdgeType(EdgeType.Flat);
        p1833396327.setRightEdgeType(EdgeType.Convex);
        p1833396327.setTopExactPuzzleID(291914853);
        p1833396327.setBottomExactPuzzleID(435179291);
        p1833396327.setLeftExactPuzzleID(-1);
        p1833396327.setRightExactPuzzleID(255067096);
        p1833396327.getDisplayImage().loadImageFromResource(c, R.drawable.p1833396327h);
        p1833396327.setExactRow(2);
        p1833396327.setExactColumn(0);
        p1833396327.getSize().reset(126.6667f, 153.3333f);
        p1833396327.getPositionOffset().reset(-50.0f, -76.66666f);
        p1833396327.setIsUseAbsolutePosition(true);
        p1833396327.setIsUseAbsoluteSize(true);
        p1833396327.getImage().setIsUseAbsolutePosition(true);
        p1833396327.getImage().setIsUseAbsoluteSize(true);
        p1833396327.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1833396327.resetPosition();
        getSpiritList().add(p1833396327);
    }

    /* access modifiers changed from: package-private */
    public void c435179291(Context c) {
        Puzzle p435179291 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load435179291(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save435179291(editor, this);
            }
        };
        p435179291.setID(435179291);
        p435179291.setName("435179291");
        p435179291.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p435179291);
        this.Desktop.RandomlyPlaced(p435179291);
        p435179291.setTopEdgeType(EdgeType.Concave);
        p435179291.setBottomEdgeType(EdgeType.Concave);
        p435179291.setLeftEdgeType(EdgeType.Flat);
        p435179291.setRightEdgeType(EdgeType.Concave);
        p435179291.setTopExactPuzzleID(1833396327);
        p435179291.setBottomExactPuzzleID(25315569);
        p435179291.setLeftExactPuzzleID(-1);
        p435179291.setRightExactPuzzleID(505905760);
        p435179291.getDisplayImage().loadImageFromResource(c, R.drawable.p435179291h);
        p435179291.setExactRow(3);
        p435179291.setExactColumn(0);
        p435179291.getSize().reset(100.0f, 100.0f);
        p435179291.getPositionOffset().reset(-50.0f, -50.0f);
        p435179291.setIsUseAbsolutePosition(true);
        p435179291.setIsUseAbsoluteSize(true);
        p435179291.getImage().setIsUseAbsolutePosition(true);
        p435179291.getImage().setIsUseAbsoluteSize(true);
        p435179291.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p435179291.resetPosition();
        getSpiritList().add(p435179291);
    }

    /* access modifiers changed from: package-private */
    public void c25315569(Context c) {
        Puzzle p25315569 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load25315569(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save25315569(editor, this);
            }
        };
        p25315569.setID(25315569);
        p25315569.setName("25315569");
        p25315569.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p25315569);
        this.Desktop.RandomlyPlaced(p25315569);
        p25315569.setTopEdgeType(EdgeType.Convex);
        p25315569.setBottomEdgeType(EdgeType.Concave);
        p25315569.setLeftEdgeType(EdgeType.Flat);
        p25315569.setRightEdgeType(EdgeType.Convex);
        p25315569.setTopExactPuzzleID(435179291);
        p25315569.setBottomExactPuzzleID(2117829250);
        p25315569.setLeftExactPuzzleID(-1);
        p25315569.setRightExactPuzzleID(612016294);
        p25315569.getDisplayImage().loadImageFromResource(c, R.drawable.p25315569h);
        p25315569.setExactRow(4);
        p25315569.setExactColumn(0);
        p25315569.getSize().reset(126.6667f, 126.6667f);
        p25315569.getPositionOffset().reset(-50.0f, -76.66666f);
        p25315569.setIsUseAbsolutePosition(true);
        p25315569.setIsUseAbsoluteSize(true);
        p25315569.getImage().setIsUseAbsolutePosition(true);
        p25315569.getImage().setIsUseAbsoluteSize(true);
        p25315569.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p25315569.resetPosition();
        getSpiritList().add(p25315569);
    }

    /* access modifiers changed from: package-private */
    public void c2117829250(Context c) {
        Puzzle p2117829250 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load2117829250(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save2117829250(editor, this);
            }
        };
        p2117829250.setID(2117829250);
        p2117829250.setName("2117829250");
        p2117829250.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p2117829250);
        this.Desktop.RandomlyPlaced(p2117829250);
        p2117829250.setTopEdgeType(EdgeType.Convex);
        p2117829250.setBottomEdgeType(EdgeType.Flat);
        p2117829250.setLeftEdgeType(EdgeType.Flat);
        p2117829250.setRightEdgeType(EdgeType.Concave);
        p2117829250.setTopExactPuzzleID(25315569);
        p2117829250.setBottomExactPuzzleID(-1);
        p2117829250.setLeftExactPuzzleID(-1);
        p2117829250.setRightExactPuzzleID(1809882698);
        p2117829250.getDisplayImage().loadImageFromResource(c, R.drawable.p2117829250h);
        p2117829250.setExactRow(5);
        p2117829250.setExactColumn(0);
        p2117829250.getSize().reset(100.0f, 126.6667f);
        p2117829250.getPositionOffset().reset(-50.0f, -76.66666f);
        p2117829250.setIsUseAbsolutePosition(true);
        p2117829250.setIsUseAbsoluteSize(true);
        p2117829250.getImage().setIsUseAbsolutePosition(true);
        p2117829250.getImage().setIsUseAbsoluteSize(true);
        p2117829250.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p2117829250.resetPosition();
        getSpiritList().add(p2117829250);
    }

    /* access modifiers changed from: package-private */
    public void c1162243469(Context c) {
        Puzzle p1162243469 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1162243469(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1162243469(editor, this);
            }
        };
        p1162243469.setID(1162243469);
        p1162243469.setName("1162243469");
        p1162243469.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1162243469);
        this.Desktop.RandomlyPlaced(p1162243469);
        p1162243469.setTopEdgeType(EdgeType.Flat);
        p1162243469.setBottomEdgeType(EdgeType.Convex);
        p1162243469.setLeftEdgeType(EdgeType.Concave);
        p1162243469.setRightEdgeType(EdgeType.Concave);
        p1162243469.setTopExactPuzzleID(-1);
        p1162243469.setBottomExactPuzzleID(1656792152);
        p1162243469.setLeftExactPuzzleID(154771092);
        p1162243469.setRightExactPuzzleID(1729091838);
        p1162243469.getDisplayImage().loadImageFromResource(c, R.drawable.p1162243469h);
        p1162243469.setExactRow(0);
        p1162243469.setExactColumn(1);
        p1162243469.getSize().reset(100.0f, 126.6667f);
        p1162243469.getPositionOffset().reset(-50.0f, -50.0f);
        p1162243469.setIsUseAbsolutePosition(true);
        p1162243469.setIsUseAbsoluteSize(true);
        p1162243469.getImage().setIsUseAbsolutePosition(true);
        p1162243469.getImage().setIsUseAbsoluteSize(true);
        p1162243469.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1162243469.resetPosition();
        getSpiritList().add(p1162243469);
    }

    /* access modifiers changed from: package-private */
    public void c1656792152(Context c) {
        Puzzle p1656792152 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1656792152(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1656792152(editor, this);
            }
        };
        p1656792152.setID(1656792152);
        p1656792152.setName("1656792152");
        p1656792152.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1656792152);
        this.Desktop.RandomlyPlaced(p1656792152);
        p1656792152.setTopEdgeType(EdgeType.Concave);
        p1656792152.setBottomEdgeType(EdgeType.Concave);
        p1656792152.setLeftEdgeType(EdgeType.Convex);
        p1656792152.setRightEdgeType(EdgeType.Concave);
        p1656792152.setTopExactPuzzleID(1162243469);
        p1656792152.setBottomExactPuzzleID(255067096);
        p1656792152.setLeftExactPuzzleID(291914853);
        p1656792152.setRightExactPuzzleID(1875826204);
        p1656792152.getDisplayImage().loadImageFromResource(c, R.drawable.p1656792152h);
        p1656792152.setExactRow(1);
        p1656792152.setExactColumn(1);
        p1656792152.getSize().reset(126.6667f, 100.0f);
        p1656792152.getPositionOffset().reset(-76.66666f, -50.0f);
        p1656792152.setIsUseAbsolutePosition(true);
        p1656792152.setIsUseAbsoluteSize(true);
        p1656792152.getImage().setIsUseAbsolutePosition(true);
        p1656792152.getImage().setIsUseAbsoluteSize(true);
        p1656792152.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1656792152.resetPosition();
        getSpiritList().add(p1656792152);
    }

    /* access modifiers changed from: package-private */
    public void c255067096(Context c) {
        Puzzle p255067096 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load255067096(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save255067096(editor, this);
            }
        };
        p255067096.setID(255067096);
        p255067096.setName("255067096");
        p255067096.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p255067096);
        this.Desktop.RandomlyPlaced(p255067096);
        p255067096.setTopEdgeType(EdgeType.Convex);
        p255067096.setBottomEdgeType(EdgeType.Convex);
        p255067096.setLeftEdgeType(EdgeType.Concave);
        p255067096.setRightEdgeType(EdgeType.Concave);
        p255067096.setTopExactPuzzleID(1656792152);
        p255067096.setBottomExactPuzzleID(505905760);
        p255067096.setLeftExactPuzzleID(1833396327);
        p255067096.setRightExactPuzzleID(234316261);
        p255067096.getDisplayImage().loadImageFromResource(c, R.drawable.p255067096h);
        p255067096.setExactRow(2);
        p255067096.setExactColumn(1);
        p255067096.getSize().reset(100.0f, 153.3333f);
        p255067096.getPositionOffset().reset(-50.0f, -76.66666f);
        p255067096.setIsUseAbsolutePosition(true);
        p255067096.setIsUseAbsoluteSize(true);
        p255067096.getImage().setIsUseAbsolutePosition(true);
        p255067096.getImage().setIsUseAbsoluteSize(true);
        p255067096.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p255067096.resetPosition();
        getSpiritList().add(p255067096);
    }

    /* access modifiers changed from: package-private */
    public void c505905760(Context c) {
        Puzzle p505905760 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load505905760(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save505905760(editor, this);
            }
        };
        p505905760.setID(505905760);
        p505905760.setName("505905760");
        p505905760.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p505905760);
        this.Desktop.RandomlyPlaced(p505905760);
        p505905760.setTopEdgeType(EdgeType.Concave);
        p505905760.setBottomEdgeType(EdgeType.Convex);
        p505905760.setLeftEdgeType(EdgeType.Convex);
        p505905760.setRightEdgeType(EdgeType.Concave);
        p505905760.setTopExactPuzzleID(255067096);
        p505905760.setBottomExactPuzzleID(612016294);
        p505905760.setLeftExactPuzzleID(435179291);
        p505905760.setRightExactPuzzleID(1811051090);
        p505905760.getDisplayImage().loadImageFromResource(c, R.drawable.p505905760h);
        p505905760.setExactRow(3);
        p505905760.setExactColumn(1);
        p505905760.getSize().reset(126.6667f, 126.6667f);
        p505905760.getPositionOffset().reset(-76.66666f, -50.0f);
        p505905760.setIsUseAbsolutePosition(true);
        p505905760.setIsUseAbsoluteSize(true);
        p505905760.getImage().setIsUseAbsolutePosition(true);
        p505905760.getImage().setIsUseAbsoluteSize(true);
        p505905760.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p505905760.resetPosition();
        getSpiritList().add(p505905760);
    }

    /* access modifiers changed from: package-private */
    public void c612016294(Context c) {
        Puzzle p612016294 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load612016294(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save612016294(editor, this);
            }
        };
        p612016294.setID(612016294);
        p612016294.setName("612016294");
        p612016294.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p612016294);
        this.Desktop.RandomlyPlaced(p612016294);
        p612016294.setTopEdgeType(EdgeType.Concave);
        p612016294.setBottomEdgeType(EdgeType.Concave);
        p612016294.setLeftEdgeType(EdgeType.Concave);
        p612016294.setRightEdgeType(EdgeType.Convex);
        p612016294.setTopExactPuzzleID(505905760);
        p612016294.setBottomExactPuzzleID(1809882698);
        p612016294.setLeftExactPuzzleID(25315569);
        p612016294.setRightExactPuzzleID(2037757932);
        p612016294.getDisplayImage().loadImageFromResource(c, R.drawable.p612016294h);
        p612016294.setExactRow(4);
        p612016294.setExactColumn(1);
        p612016294.getSize().reset(126.6667f, 100.0f);
        p612016294.getPositionOffset().reset(-50.0f, -50.0f);
        p612016294.setIsUseAbsolutePosition(true);
        p612016294.setIsUseAbsoluteSize(true);
        p612016294.getImage().setIsUseAbsolutePosition(true);
        p612016294.getImage().setIsUseAbsoluteSize(true);
        p612016294.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p612016294.resetPosition();
        getSpiritList().add(p612016294);
    }

    /* access modifiers changed from: package-private */
    public void c1809882698(Context c) {
        Puzzle p1809882698 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1809882698(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1809882698(editor, this);
            }
        };
        p1809882698.setID(1809882698);
        p1809882698.setName("1809882698");
        p1809882698.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1809882698);
        this.Desktop.RandomlyPlaced(p1809882698);
        p1809882698.setTopEdgeType(EdgeType.Convex);
        p1809882698.setBottomEdgeType(EdgeType.Flat);
        p1809882698.setLeftEdgeType(EdgeType.Convex);
        p1809882698.setRightEdgeType(EdgeType.Convex);
        p1809882698.setTopExactPuzzleID(612016294);
        p1809882698.setBottomExactPuzzleID(-1);
        p1809882698.setLeftExactPuzzleID(2117829250);
        p1809882698.setRightExactPuzzleID(826639736);
        p1809882698.getDisplayImage().loadImageFromResource(c, R.drawable.p1809882698h);
        p1809882698.setExactRow(5);
        p1809882698.setExactColumn(1);
        p1809882698.getSize().reset(153.3333f, 126.6667f);
        p1809882698.getPositionOffset().reset(-76.66666f, -76.66666f);
        p1809882698.setIsUseAbsolutePosition(true);
        p1809882698.setIsUseAbsoluteSize(true);
        p1809882698.getImage().setIsUseAbsolutePosition(true);
        p1809882698.getImage().setIsUseAbsoluteSize(true);
        p1809882698.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1809882698.resetPosition();
        getSpiritList().add(p1809882698);
    }

    /* access modifiers changed from: package-private */
    public void c1729091838(Context c) {
        Puzzle p1729091838 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1729091838(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1729091838(editor, this);
            }
        };
        p1729091838.setID(1729091838);
        p1729091838.setName("1729091838");
        p1729091838.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1729091838);
        this.Desktop.RandomlyPlaced(p1729091838);
        p1729091838.setTopEdgeType(EdgeType.Flat);
        p1729091838.setBottomEdgeType(EdgeType.Concave);
        p1729091838.setLeftEdgeType(EdgeType.Convex);
        p1729091838.setRightEdgeType(EdgeType.Convex);
        p1729091838.setTopExactPuzzleID(-1);
        p1729091838.setBottomExactPuzzleID(1875826204);
        p1729091838.setLeftExactPuzzleID(1162243469);
        p1729091838.setRightExactPuzzleID(515390279);
        p1729091838.getDisplayImage().loadImageFromResource(c, R.drawable.p1729091838h);
        p1729091838.setExactRow(0);
        p1729091838.setExactColumn(2);
        p1729091838.getSize().reset(153.3333f, 100.0f);
        p1729091838.getPositionOffset().reset(-76.66666f, -50.0f);
        p1729091838.setIsUseAbsolutePosition(true);
        p1729091838.setIsUseAbsoluteSize(true);
        p1729091838.getImage().setIsUseAbsolutePosition(true);
        p1729091838.getImage().setIsUseAbsoluteSize(true);
        p1729091838.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1729091838.resetPosition();
        getSpiritList().add(p1729091838);
    }

    /* access modifiers changed from: package-private */
    public void c1875826204(Context c) {
        Puzzle p1875826204 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1875826204(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1875826204(editor, this);
            }
        };
        p1875826204.setID(1875826204);
        p1875826204.setName("1875826204");
        p1875826204.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1875826204);
        this.Desktop.RandomlyPlaced(p1875826204);
        p1875826204.setTopEdgeType(EdgeType.Convex);
        p1875826204.setBottomEdgeType(EdgeType.Convex);
        p1875826204.setLeftEdgeType(EdgeType.Convex);
        p1875826204.setRightEdgeType(EdgeType.Concave);
        p1875826204.setTopExactPuzzleID(1729091838);
        p1875826204.setBottomExactPuzzleID(234316261);
        p1875826204.setLeftExactPuzzleID(1656792152);
        p1875826204.setRightExactPuzzleID(1212010540);
        p1875826204.getDisplayImage().loadImageFromResource(c, R.drawable.p1875826204h);
        p1875826204.setExactRow(1);
        p1875826204.setExactColumn(2);
        p1875826204.getSize().reset(126.6667f, 153.3333f);
        p1875826204.getPositionOffset().reset(-76.66666f, -76.66666f);
        p1875826204.setIsUseAbsolutePosition(true);
        p1875826204.setIsUseAbsoluteSize(true);
        p1875826204.getImage().setIsUseAbsolutePosition(true);
        p1875826204.getImage().setIsUseAbsoluteSize(true);
        p1875826204.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1875826204.resetPosition();
        getSpiritList().add(p1875826204);
    }

    /* access modifiers changed from: package-private */
    public void c234316261(Context c) {
        Puzzle p234316261 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load234316261(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save234316261(editor, this);
            }
        };
        p234316261.setID(234316261);
        p234316261.setName("234316261");
        p234316261.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p234316261);
        this.Desktop.RandomlyPlaced(p234316261);
        p234316261.setTopEdgeType(EdgeType.Concave);
        p234316261.setBottomEdgeType(EdgeType.Convex);
        p234316261.setLeftEdgeType(EdgeType.Convex);
        p234316261.setRightEdgeType(EdgeType.Concave);
        p234316261.setTopExactPuzzleID(1875826204);
        p234316261.setBottomExactPuzzleID(1811051090);
        p234316261.setLeftExactPuzzleID(255067096);
        p234316261.setRightExactPuzzleID(983352780);
        p234316261.getDisplayImage().loadImageFromResource(c, R.drawable.p234316261h);
        p234316261.setExactRow(2);
        p234316261.setExactColumn(2);
        p234316261.getSize().reset(126.6667f, 126.6667f);
        p234316261.getPositionOffset().reset(-76.66666f, -50.0f);
        p234316261.setIsUseAbsolutePosition(true);
        p234316261.setIsUseAbsoluteSize(true);
        p234316261.getImage().setIsUseAbsolutePosition(true);
        p234316261.getImage().setIsUseAbsoluteSize(true);
        p234316261.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p234316261.resetPosition();
        getSpiritList().add(p234316261);
    }

    /* access modifiers changed from: package-private */
    public void c1811051090(Context c) {
        Puzzle p1811051090 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1811051090(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1811051090(editor, this);
            }
        };
        p1811051090.setID(1811051090);
        p1811051090.setName("1811051090");
        p1811051090.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1811051090);
        this.Desktop.RandomlyPlaced(p1811051090);
        p1811051090.setTopEdgeType(EdgeType.Concave);
        p1811051090.setBottomEdgeType(EdgeType.Concave);
        p1811051090.setLeftEdgeType(EdgeType.Convex);
        p1811051090.setRightEdgeType(EdgeType.Convex);
        p1811051090.setTopExactPuzzleID(234316261);
        p1811051090.setBottomExactPuzzleID(2037757932);
        p1811051090.setLeftExactPuzzleID(505905760);
        p1811051090.setRightExactPuzzleID(11363373);
        p1811051090.getDisplayImage().loadImageFromResource(c, R.drawable.p1811051090h);
        p1811051090.setExactRow(3);
        p1811051090.setExactColumn(2);
        p1811051090.getSize().reset(153.3333f, 100.0f);
        p1811051090.getPositionOffset().reset(-76.66666f, -50.0f);
        p1811051090.setIsUseAbsolutePosition(true);
        p1811051090.setIsUseAbsoluteSize(true);
        p1811051090.getImage().setIsUseAbsolutePosition(true);
        p1811051090.getImage().setIsUseAbsoluteSize(true);
        p1811051090.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1811051090.resetPosition();
        getSpiritList().add(p1811051090);
    }

    /* access modifiers changed from: package-private */
    public void c2037757932(Context c) {
        Puzzle p2037757932 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load2037757932(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save2037757932(editor, this);
            }
        };
        p2037757932.setID(2037757932);
        p2037757932.setName("2037757932");
        p2037757932.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p2037757932);
        this.Desktop.RandomlyPlaced(p2037757932);
        p2037757932.setTopEdgeType(EdgeType.Convex);
        p2037757932.setBottomEdgeType(EdgeType.Concave);
        p2037757932.setLeftEdgeType(EdgeType.Concave);
        p2037757932.setRightEdgeType(EdgeType.Concave);
        p2037757932.setTopExactPuzzleID(1811051090);
        p2037757932.setBottomExactPuzzleID(826639736);
        p2037757932.setLeftExactPuzzleID(612016294);
        p2037757932.setRightExactPuzzleID(1098608012);
        p2037757932.getDisplayImage().loadImageFromResource(c, R.drawable.p2037757932h);
        p2037757932.setExactRow(4);
        p2037757932.setExactColumn(2);
        p2037757932.getSize().reset(100.0f, 126.6667f);
        p2037757932.getPositionOffset().reset(-50.0f, -76.66666f);
        p2037757932.setIsUseAbsolutePosition(true);
        p2037757932.setIsUseAbsoluteSize(true);
        p2037757932.getImage().setIsUseAbsolutePosition(true);
        p2037757932.getImage().setIsUseAbsoluteSize(true);
        p2037757932.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p2037757932.resetPosition();
        getSpiritList().add(p2037757932);
    }

    /* access modifiers changed from: package-private */
    public void c826639736(Context c) {
        Puzzle p826639736 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load826639736(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save826639736(editor, this);
            }
        };
        p826639736.setID(826639736);
        p826639736.setName("826639736");
        p826639736.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p826639736);
        this.Desktop.RandomlyPlaced(p826639736);
        p826639736.setTopEdgeType(EdgeType.Convex);
        p826639736.setBottomEdgeType(EdgeType.Flat);
        p826639736.setLeftEdgeType(EdgeType.Concave);
        p826639736.setRightEdgeType(EdgeType.Convex);
        p826639736.setTopExactPuzzleID(2037757932);
        p826639736.setBottomExactPuzzleID(-1);
        p826639736.setLeftExactPuzzleID(1809882698);
        p826639736.setRightExactPuzzleID(761938108);
        p826639736.getDisplayImage().loadImageFromResource(c, R.drawable.p826639736h);
        p826639736.setExactRow(5);
        p826639736.setExactColumn(2);
        p826639736.getSize().reset(126.6667f, 126.6667f);
        p826639736.getPositionOffset().reset(-50.0f, -76.66666f);
        p826639736.setIsUseAbsolutePosition(true);
        p826639736.setIsUseAbsoluteSize(true);
        p826639736.getImage().setIsUseAbsolutePosition(true);
        p826639736.getImage().setIsUseAbsoluteSize(true);
        p826639736.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p826639736.resetPosition();
        getSpiritList().add(p826639736);
    }

    /* access modifiers changed from: package-private */
    public void c515390279(Context c) {
        Puzzle p515390279 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load515390279(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save515390279(editor, this);
            }
        };
        p515390279.setID(515390279);
        p515390279.setName("515390279");
        p515390279.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p515390279);
        this.Desktop.RandomlyPlaced(p515390279);
        p515390279.setTopEdgeType(EdgeType.Flat);
        p515390279.setBottomEdgeType(EdgeType.Concave);
        p515390279.setLeftEdgeType(EdgeType.Concave);
        p515390279.setRightEdgeType(EdgeType.Convex);
        p515390279.setTopExactPuzzleID(-1);
        p515390279.setBottomExactPuzzleID(1212010540);
        p515390279.setLeftExactPuzzleID(1729091838);
        p515390279.setRightExactPuzzleID(1628243632);
        p515390279.getDisplayImage().loadImageFromResource(c, R.drawable.p515390279h);
        p515390279.setExactRow(0);
        p515390279.setExactColumn(3);
        p515390279.getSize().reset(126.6667f, 100.0f);
        p515390279.getPositionOffset().reset(-50.0f, -50.0f);
        p515390279.setIsUseAbsolutePosition(true);
        p515390279.setIsUseAbsoluteSize(true);
        p515390279.getImage().setIsUseAbsolutePosition(true);
        p515390279.getImage().setIsUseAbsoluteSize(true);
        p515390279.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p515390279.resetPosition();
        getSpiritList().add(p515390279);
    }

    /* access modifiers changed from: package-private */
    public void c1212010540(Context c) {
        Puzzle p1212010540 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1212010540(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1212010540(editor, this);
            }
        };
        p1212010540.setID(1212010540);
        p1212010540.setName("1212010540");
        p1212010540.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1212010540);
        this.Desktop.RandomlyPlaced(p1212010540);
        p1212010540.setTopEdgeType(EdgeType.Convex);
        p1212010540.setBottomEdgeType(EdgeType.Convex);
        p1212010540.setLeftEdgeType(EdgeType.Convex);
        p1212010540.setRightEdgeType(EdgeType.Convex);
        p1212010540.setTopExactPuzzleID(515390279);
        p1212010540.setBottomExactPuzzleID(983352780);
        p1212010540.setLeftExactPuzzleID(1875826204);
        p1212010540.setRightExactPuzzleID(681167814);
        p1212010540.getDisplayImage().loadImageFromResource(c, R.drawable.p1212010540h);
        p1212010540.setExactRow(1);
        p1212010540.setExactColumn(3);
        p1212010540.getSize().reset(153.3333f, 153.3333f);
        p1212010540.getPositionOffset().reset(-76.66666f, -76.66666f);
        p1212010540.setIsUseAbsolutePosition(true);
        p1212010540.setIsUseAbsoluteSize(true);
        p1212010540.getImage().setIsUseAbsolutePosition(true);
        p1212010540.getImage().setIsUseAbsoluteSize(true);
        p1212010540.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1212010540.resetPosition();
        getSpiritList().add(p1212010540);
    }

    /* access modifiers changed from: package-private */
    public void c983352780(Context c) {
        Puzzle p983352780 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load983352780(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save983352780(editor, this);
            }
        };
        p983352780.setID(983352780);
        p983352780.setName("983352780");
        p983352780.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p983352780);
        this.Desktop.RandomlyPlaced(p983352780);
        p983352780.setTopEdgeType(EdgeType.Concave);
        p983352780.setBottomEdgeType(EdgeType.Concave);
        p983352780.setLeftEdgeType(EdgeType.Convex);
        p983352780.setRightEdgeType(EdgeType.Convex);
        p983352780.setTopExactPuzzleID(1212010540);
        p983352780.setBottomExactPuzzleID(11363373);
        p983352780.setLeftExactPuzzleID(234316261);
        p983352780.setRightExactPuzzleID(1895508514);
        p983352780.getDisplayImage().loadImageFromResource(c, R.drawable.p983352780h);
        p983352780.setExactRow(2);
        p983352780.setExactColumn(3);
        p983352780.getSize().reset(153.3333f, 100.0f);
        p983352780.getPositionOffset().reset(-76.66666f, -50.0f);
        p983352780.setIsUseAbsolutePosition(true);
        p983352780.setIsUseAbsoluteSize(true);
        p983352780.getImage().setIsUseAbsolutePosition(true);
        p983352780.getImage().setIsUseAbsoluteSize(true);
        p983352780.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p983352780.resetPosition();
        getSpiritList().add(p983352780);
    }

    /* access modifiers changed from: package-private */
    public void c11363373(Context c) {
        Puzzle p11363373 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load11363373(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save11363373(editor, this);
            }
        };
        p11363373.setID(11363373);
        p11363373.setName("11363373");
        p11363373.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p11363373);
        this.Desktop.RandomlyPlaced(p11363373);
        p11363373.setTopEdgeType(EdgeType.Convex);
        p11363373.setBottomEdgeType(EdgeType.Concave);
        p11363373.setLeftEdgeType(EdgeType.Concave);
        p11363373.setRightEdgeType(EdgeType.Convex);
        p11363373.setTopExactPuzzleID(983352780);
        p11363373.setBottomExactPuzzleID(1098608012);
        p11363373.setLeftExactPuzzleID(1811051090);
        p11363373.setRightExactPuzzleID(717599992);
        p11363373.getDisplayImage().loadImageFromResource(c, R.drawable.p11363373h);
        p11363373.setExactRow(3);
        p11363373.setExactColumn(3);
        p11363373.getSize().reset(126.6667f, 126.6667f);
        p11363373.getPositionOffset().reset(-50.0f, -76.66666f);
        p11363373.setIsUseAbsolutePosition(true);
        p11363373.setIsUseAbsoluteSize(true);
        p11363373.getImage().setIsUseAbsolutePosition(true);
        p11363373.getImage().setIsUseAbsoluteSize(true);
        p11363373.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p11363373.resetPosition();
        getSpiritList().add(p11363373);
    }

    /* access modifiers changed from: package-private */
    public void c1098608012(Context c) {
        Puzzle p1098608012 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1098608012(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1098608012(editor, this);
            }
        };
        p1098608012.setID(1098608012);
        p1098608012.setName("1098608012");
        p1098608012.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1098608012);
        this.Desktop.RandomlyPlaced(p1098608012);
        p1098608012.setTopEdgeType(EdgeType.Convex);
        p1098608012.setBottomEdgeType(EdgeType.Convex);
        p1098608012.setLeftEdgeType(EdgeType.Convex);
        p1098608012.setRightEdgeType(EdgeType.Concave);
        p1098608012.setTopExactPuzzleID(11363373);
        p1098608012.setBottomExactPuzzleID(761938108);
        p1098608012.setLeftExactPuzzleID(2037757932);
        p1098608012.setRightExactPuzzleID(376602254);
        p1098608012.getDisplayImage().loadImageFromResource(c, R.drawable.p1098608012h);
        p1098608012.setExactRow(4);
        p1098608012.setExactColumn(3);
        p1098608012.getSize().reset(126.6667f, 153.3333f);
        p1098608012.getPositionOffset().reset(-76.66666f, -76.66666f);
        p1098608012.setIsUseAbsolutePosition(true);
        p1098608012.setIsUseAbsoluteSize(true);
        p1098608012.getImage().setIsUseAbsolutePosition(true);
        p1098608012.getImage().setIsUseAbsoluteSize(true);
        p1098608012.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1098608012.resetPosition();
        getSpiritList().add(p1098608012);
    }

    /* access modifiers changed from: package-private */
    public void c761938108(Context c) {
        Puzzle p761938108 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load761938108(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save761938108(editor, this);
            }
        };
        p761938108.setID(761938108);
        p761938108.setName("761938108");
        p761938108.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p761938108);
        this.Desktop.RandomlyPlaced(p761938108);
        p761938108.setTopEdgeType(EdgeType.Concave);
        p761938108.setBottomEdgeType(EdgeType.Flat);
        p761938108.setLeftEdgeType(EdgeType.Concave);
        p761938108.setRightEdgeType(EdgeType.Concave);
        p761938108.setTopExactPuzzleID(1098608012);
        p761938108.setBottomExactPuzzleID(-1);
        p761938108.setLeftExactPuzzleID(826639736);
        p761938108.setRightExactPuzzleID(1378962629);
        p761938108.getDisplayImage().loadImageFromResource(c, R.drawable.p761938108h);
        p761938108.setExactRow(5);
        p761938108.setExactColumn(3);
        p761938108.getSize().reset(100.0f, 100.0f);
        p761938108.getPositionOffset().reset(-50.0f, -50.0f);
        p761938108.setIsUseAbsolutePosition(true);
        p761938108.setIsUseAbsoluteSize(true);
        p761938108.getImage().setIsUseAbsolutePosition(true);
        p761938108.getImage().setIsUseAbsoluteSize(true);
        p761938108.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p761938108.resetPosition();
        getSpiritList().add(p761938108);
    }

    /* access modifiers changed from: package-private */
    public void c1628243632(Context c) {
        Puzzle p1628243632 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1628243632(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1628243632(editor, this);
            }
        };
        p1628243632.setID(1628243632);
        p1628243632.setName("1628243632");
        p1628243632.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1628243632);
        this.Desktop.RandomlyPlaced(p1628243632);
        p1628243632.setTopEdgeType(EdgeType.Flat);
        p1628243632.setBottomEdgeType(EdgeType.Convex);
        p1628243632.setLeftEdgeType(EdgeType.Concave);
        p1628243632.setRightEdgeType(EdgeType.Concave);
        p1628243632.setTopExactPuzzleID(-1);
        p1628243632.setBottomExactPuzzleID(681167814);
        p1628243632.setLeftExactPuzzleID(515390279);
        p1628243632.setRightExactPuzzleID(20104293);
        p1628243632.getDisplayImage().loadImageFromResource(c, R.drawable.p1628243632h);
        p1628243632.setExactRow(0);
        p1628243632.setExactColumn(4);
        p1628243632.getSize().reset(100.0f, 126.6667f);
        p1628243632.getPositionOffset().reset(-50.0f, -50.0f);
        p1628243632.setIsUseAbsolutePosition(true);
        p1628243632.setIsUseAbsoluteSize(true);
        p1628243632.getImage().setIsUseAbsolutePosition(true);
        p1628243632.getImage().setIsUseAbsoluteSize(true);
        p1628243632.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1628243632.resetPosition();
        getSpiritList().add(p1628243632);
    }

    /* access modifiers changed from: package-private */
    public void c681167814(Context c) {
        Puzzle p681167814 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load681167814(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save681167814(editor, this);
            }
        };
        p681167814.setID(681167814);
        p681167814.setName("681167814");
        p681167814.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p681167814);
        this.Desktop.RandomlyPlaced(p681167814);
        p681167814.setTopEdgeType(EdgeType.Concave);
        p681167814.setBottomEdgeType(EdgeType.Concave);
        p681167814.setLeftEdgeType(EdgeType.Concave);
        p681167814.setRightEdgeType(EdgeType.Convex);
        p681167814.setTopExactPuzzleID(1628243632);
        p681167814.setBottomExactPuzzleID(1895508514);
        p681167814.setLeftExactPuzzleID(1212010540);
        p681167814.setRightExactPuzzleID(342561831);
        p681167814.getDisplayImage().loadImageFromResource(c, R.drawable.p681167814h);
        p681167814.setExactRow(1);
        p681167814.setExactColumn(4);
        p681167814.getSize().reset(126.6667f, 100.0f);
        p681167814.getPositionOffset().reset(-50.0f, -50.0f);
        p681167814.setIsUseAbsolutePosition(true);
        p681167814.setIsUseAbsoluteSize(true);
        p681167814.getImage().setIsUseAbsolutePosition(true);
        p681167814.getImage().setIsUseAbsoluteSize(true);
        p681167814.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p681167814.resetPosition();
        getSpiritList().add(p681167814);
    }

    /* access modifiers changed from: package-private */
    public void c1895508514(Context c) {
        Puzzle p1895508514 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1895508514(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1895508514(editor, this);
            }
        };
        p1895508514.setID(1895508514);
        p1895508514.setName("1895508514");
        p1895508514.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1895508514);
        this.Desktop.RandomlyPlaced(p1895508514);
        p1895508514.setTopEdgeType(EdgeType.Convex);
        p1895508514.setBottomEdgeType(EdgeType.Convex);
        p1895508514.setLeftEdgeType(EdgeType.Concave);
        p1895508514.setRightEdgeType(EdgeType.Convex);
        p1895508514.setTopExactPuzzleID(681167814);
        p1895508514.setBottomExactPuzzleID(717599992);
        p1895508514.setLeftExactPuzzleID(983352780);
        p1895508514.setRightExactPuzzleID(1214901854);
        p1895508514.getDisplayImage().loadImageFromResource(c, R.drawable.p1895508514h);
        p1895508514.setExactRow(2);
        p1895508514.setExactColumn(4);
        p1895508514.getSize().reset(126.6667f, 153.3333f);
        p1895508514.getPositionOffset().reset(-50.0f, -76.66666f);
        p1895508514.setIsUseAbsolutePosition(true);
        p1895508514.setIsUseAbsoluteSize(true);
        p1895508514.getImage().setIsUseAbsolutePosition(true);
        p1895508514.getImage().setIsUseAbsoluteSize(true);
        p1895508514.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1895508514.resetPosition();
        getSpiritList().add(p1895508514);
    }

    /* access modifiers changed from: package-private */
    public void c717599992(Context c) {
        Puzzle p717599992 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load717599992(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save717599992(editor, this);
            }
        };
        p717599992.setID(717599992);
        p717599992.setName("717599992");
        p717599992.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p717599992);
        this.Desktop.RandomlyPlaced(p717599992);
        p717599992.setTopEdgeType(EdgeType.Concave);
        p717599992.setBottomEdgeType(EdgeType.Convex);
        p717599992.setLeftEdgeType(EdgeType.Concave);
        p717599992.setRightEdgeType(EdgeType.Concave);
        p717599992.setTopExactPuzzleID(1895508514);
        p717599992.setBottomExactPuzzleID(376602254);
        p717599992.setLeftExactPuzzleID(11363373);
        p717599992.setRightExactPuzzleID(591757449);
        p717599992.getDisplayImage().loadImageFromResource(c, R.drawable.p717599992h);
        p717599992.setExactRow(3);
        p717599992.setExactColumn(4);
        p717599992.getSize().reset(100.0f, 126.6667f);
        p717599992.getPositionOffset().reset(-50.0f, -50.0f);
        p717599992.setIsUseAbsolutePosition(true);
        p717599992.setIsUseAbsoluteSize(true);
        p717599992.getImage().setIsUseAbsolutePosition(true);
        p717599992.getImage().setIsUseAbsoluteSize(true);
        p717599992.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p717599992.resetPosition();
        getSpiritList().add(p717599992);
    }

    /* access modifiers changed from: package-private */
    public void c376602254(Context c) {
        Puzzle p376602254 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load376602254(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save376602254(editor, this);
            }
        };
        p376602254.setID(376602254);
        p376602254.setName("376602254");
        p376602254.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p376602254);
        this.Desktop.RandomlyPlaced(p376602254);
        p376602254.setTopEdgeType(EdgeType.Concave);
        p376602254.setBottomEdgeType(EdgeType.Concave);
        p376602254.setLeftEdgeType(EdgeType.Convex);
        p376602254.setRightEdgeType(EdgeType.Convex);
        p376602254.setTopExactPuzzleID(717599992);
        p376602254.setBottomExactPuzzleID(1378962629);
        p376602254.setLeftExactPuzzleID(1098608012);
        p376602254.setRightExactPuzzleID(2123219446);
        p376602254.getDisplayImage().loadImageFromResource(c, R.drawable.p376602254h);
        p376602254.setExactRow(4);
        p376602254.setExactColumn(4);
        p376602254.getSize().reset(153.3333f, 100.0f);
        p376602254.getPositionOffset().reset(-76.66666f, -50.0f);
        p376602254.setIsUseAbsolutePosition(true);
        p376602254.setIsUseAbsoluteSize(true);
        p376602254.getImage().setIsUseAbsolutePosition(true);
        p376602254.getImage().setIsUseAbsoluteSize(true);
        p376602254.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p376602254.resetPosition();
        getSpiritList().add(p376602254);
    }

    /* access modifiers changed from: package-private */
    public void c1378962629(Context c) {
        Puzzle p1378962629 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1378962629(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1378962629(editor, this);
            }
        };
        p1378962629.setID(1378962629);
        p1378962629.setName("1378962629");
        p1378962629.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1378962629);
        this.Desktop.RandomlyPlaced(p1378962629);
        p1378962629.setTopEdgeType(EdgeType.Convex);
        p1378962629.setBottomEdgeType(EdgeType.Flat);
        p1378962629.setLeftEdgeType(EdgeType.Convex);
        p1378962629.setRightEdgeType(EdgeType.Convex);
        p1378962629.setTopExactPuzzleID(376602254);
        p1378962629.setBottomExactPuzzleID(-1);
        p1378962629.setLeftExactPuzzleID(761938108);
        p1378962629.setRightExactPuzzleID(994920776);
        p1378962629.getDisplayImage().loadImageFromResource(c, R.drawable.p1378962629h);
        p1378962629.setExactRow(5);
        p1378962629.setExactColumn(4);
        p1378962629.getSize().reset(153.3333f, 126.6667f);
        p1378962629.getPositionOffset().reset(-76.66666f, -76.66666f);
        p1378962629.setIsUseAbsolutePosition(true);
        p1378962629.setIsUseAbsoluteSize(true);
        p1378962629.getImage().setIsUseAbsolutePosition(true);
        p1378962629.getImage().setIsUseAbsoluteSize(true);
        p1378962629.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1378962629.resetPosition();
        getSpiritList().add(p1378962629);
    }

    /* access modifiers changed from: package-private */
    public void c20104293(Context c) {
        Puzzle p20104293 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load20104293(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save20104293(editor, this);
            }
        };
        p20104293.setID(20104293);
        p20104293.setName("20104293");
        p20104293.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p20104293);
        this.Desktop.RandomlyPlaced(p20104293);
        p20104293.setTopEdgeType(EdgeType.Flat);
        p20104293.setBottomEdgeType(EdgeType.Convex);
        p20104293.setLeftEdgeType(EdgeType.Convex);
        p20104293.setRightEdgeType(EdgeType.Convex);
        p20104293.setTopExactPuzzleID(-1);
        p20104293.setBottomExactPuzzleID(342561831);
        p20104293.setLeftExactPuzzleID(1628243632);
        p20104293.setRightExactPuzzleID(926875513);
        p20104293.getDisplayImage().loadImageFromResource(c, R.drawable.p20104293h);
        p20104293.setExactRow(0);
        p20104293.setExactColumn(5);
        p20104293.getSize().reset(153.3333f, 126.6667f);
        p20104293.getPositionOffset().reset(-76.66666f, -50.0f);
        p20104293.setIsUseAbsolutePosition(true);
        p20104293.setIsUseAbsoluteSize(true);
        p20104293.getImage().setIsUseAbsolutePosition(true);
        p20104293.getImage().setIsUseAbsoluteSize(true);
        p20104293.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p20104293.resetPosition();
        getSpiritList().add(p20104293);
    }

    /* access modifiers changed from: package-private */
    public void c342561831(Context c) {
        Puzzle p342561831 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load342561831(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save342561831(editor, this);
            }
        };
        p342561831.setID(342561831);
        p342561831.setName("342561831");
        p342561831.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p342561831);
        this.Desktop.RandomlyPlaced(p342561831);
        p342561831.setTopEdgeType(EdgeType.Concave);
        p342561831.setBottomEdgeType(EdgeType.Convex);
        p342561831.setLeftEdgeType(EdgeType.Concave);
        p342561831.setRightEdgeType(EdgeType.Concave);
        p342561831.setTopExactPuzzleID(20104293);
        p342561831.setBottomExactPuzzleID(1214901854);
        p342561831.setLeftExactPuzzleID(681167814);
        p342561831.setRightExactPuzzleID(1341340930);
        p342561831.getDisplayImage().loadImageFromResource(c, R.drawable.p342561831h);
        p342561831.setExactRow(1);
        p342561831.setExactColumn(5);
        p342561831.getSize().reset(100.0f, 126.6667f);
        p342561831.getPositionOffset().reset(-50.0f, -50.0f);
        p342561831.setIsUseAbsolutePosition(true);
        p342561831.setIsUseAbsoluteSize(true);
        p342561831.getImage().setIsUseAbsolutePosition(true);
        p342561831.getImage().setIsUseAbsoluteSize(true);
        p342561831.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p342561831.resetPosition();
        getSpiritList().add(p342561831);
    }

    /* access modifiers changed from: package-private */
    public void c1214901854(Context c) {
        Puzzle p1214901854 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1214901854(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1214901854(editor, this);
            }
        };
        p1214901854.setID(1214901854);
        p1214901854.setName("1214901854");
        p1214901854.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1214901854);
        this.Desktop.RandomlyPlaced(p1214901854);
        p1214901854.setTopEdgeType(EdgeType.Concave);
        p1214901854.setBottomEdgeType(EdgeType.Convex);
        p1214901854.setLeftEdgeType(EdgeType.Concave);
        p1214901854.setRightEdgeType(EdgeType.Convex);
        p1214901854.setTopExactPuzzleID(342561831);
        p1214901854.setBottomExactPuzzleID(591757449);
        p1214901854.setLeftExactPuzzleID(1895508514);
        p1214901854.setRightExactPuzzleID(1042524943);
        p1214901854.getDisplayImage().loadImageFromResource(c, R.drawable.p1214901854h);
        p1214901854.setExactRow(2);
        p1214901854.setExactColumn(5);
        p1214901854.getSize().reset(126.6667f, 126.6667f);
        p1214901854.getPositionOffset().reset(-50.0f, -50.0f);
        p1214901854.setIsUseAbsolutePosition(true);
        p1214901854.setIsUseAbsoluteSize(true);
        p1214901854.getImage().setIsUseAbsolutePosition(true);
        p1214901854.getImage().setIsUseAbsoluteSize(true);
        p1214901854.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1214901854.resetPosition();
        getSpiritList().add(p1214901854);
    }

    /* access modifiers changed from: package-private */
    public void c591757449(Context c) {
        Puzzle p591757449 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load591757449(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save591757449(editor, this);
            }
        };
        p591757449.setID(591757449);
        p591757449.setName("591757449");
        p591757449.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p591757449);
        this.Desktop.RandomlyPlaced(p591757449);
        p591757449.setTopEdgeType(EdgeType.Concave);
        p591757449.setBottomEdgeType(EdgeType.Concave);
        p591757449.setLeftEdgeType(EdgeType.Convex);
        p591757449.setRightEdgeType(EdgeType.Concave);
        p591757449.setTopExactPuzzleID(1214901854);
        p591757449.setBottomExactPuzzleID(2123219446);
        p591757449.setLeftExactPuzzleID(717599992);
        p591757449.setRightExactPuzzleID(574086540);
        p591757449.getDisplayImage().loadImageFromResource(c, R.drawable.p591757449h);
        p591757449.setExactRow(3);
        p591757449.setExactColumn(5);
        p591757449.getSize().reset(126.6667f, 100.0f);
        p591757449.getPositionOffset().reset(-76.66666f, -50.0f);
        p591757449.setIsUseAbsolutePosition(true);
        p591757449.setIsUseAbsoluteSize(true);
        p591757449.getImage().setIsUseAbsolutePosition(true);
        p591757449.getImage().setIsUseAbsoluteSize(true);
        p591757449.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p591757449.resetPosition();
        getSpiritList().add(p591757449);
    }

    /* access modifiers changed from: package-private */
    public void c2123219446(Context c) {
        Puzzle p2123219446 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load2123219446(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save2123219446(editor, this);
            }
        };
        p2123219446.setID(2123219446);
        p2123219446.setName("2123219446");
        p2123219446.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p2123219446);
        this.Desktop.RandomlyPlaced(p2123219446);
        p2123219446.setTopEdgeType(EdgeType.Convex);
        p2123219446.setBottomEdgeType(EdgeType.Convex);
        p2123219446.setLeftEdgeType(EdgeType.Concave);
        p2123219446.setRightEdgeType(EdgeType.Convex);
        p2123219446.setTopExactPuzzleID(591757449);
        p2123219446.setBottomExactPuzzleID(994920776);
        p2123219446.setLeftExactPuzzleID(376602254);
        p2123219446.setRightExactPuzzleID(1700115332);
        p2123219446.getDisplayImage().loadImageFromResource(c, R.drawable.p2123219446h);
        p2123219446.setExactRow(4);
        p2123219446.setExactColumn(5);
        p2123219446.getSize().reset(126.6667f, 153.3333f);
        p2123219446.getPositionOffset().reset(-50.0f, -76.66666f);
        p2123219446.setIsUseAbsolutePosition(true);
        p2123219446.setIsUseAbsoluteSize(true);
        p2123219446.getImage().setIsUseAbsolutePosition(true);
        p2123219446.getImage().setIsUseAbsoluteSize(true);
        p2123219446.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p2123219446.resetPosition();
        getSpiritList().add(p2123219446);
    }

    /* access modifiers changed from: package-private */
    public void c994920776(Context c) {
        Puzzle p994920776 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load994920776(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save994920776(editor, this);
            }
        };
        p994920776.setID(994920776);
        p994920776.setName("994920776");
        p994920776.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p994920776);
        this.Desktop.RandomlyPlaced(p994920776);
        p994920776.setTopEdgeType(EdgeType.Concave);
        p994920776.setBottomEdgeType(EdgeType.Flat);
        p994920776.setLeftEdgeType(EdgeType.Concave);
        p994920776.setRightEdgeType(EdgeType.Concave);
        p994920776.setTopExactPuzzleID(2123219446);
        p994920776.setBottomExactPuzzleID(-1);
        p994920776.setLeftExactPuzzleID(1378962629);
        p994920776.setRightExactPuzzleID(396314980);
        p994920776.getDisplayImage().loadImageFromResource(c, R.drawable.p994920776h);
        p994920776.setExactRow(5);
        p994920776.setExactColumn(5);
        p994920776.getSize().reset(100.0f, 100.0f);
        p994920776.getPositionOffset().reset(-50.0f, -50.0f);
        p994920776.setIsUseAbsolutePosition(true);
        p994920776.setIsUseAbsoluteSize(true);
        p994920776.getImage().setIsUseAbsolutePosition(true);
        p994920776.getImage().setIsUseAbsoluteSize(true);
        p994920776.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p994920776.resetPosition();
        getSpiritList().add(p994920776);
    }

    /* access modifiers changed from: package-private */
    public void c926875513(Context c) {
        Puzzle p926875513 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load926875513(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save926875513(editor, this);
            }
        };
        p926875513.setID(926875513);
        p926875513.setName("926875513");
        p926875513.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p926875513);
        this.Desktop.RandomlyPlaced(p926875513);
        p926875513.setTopEdgeType(EdgeType.Flat);
        p926875513.setBottomEdgeType(EdgeType.Concave);
        p926875513.setLeftEdgeType(EdgeType.Concave);
        p926875513.setRightEdgeType(EdgeType.Convex);
        p926875513.setTopExactPuzzleID(-1);
        p926875513.setBottomExactPuzzleID(1341340930);
        p926875513.setLeftExactPuzzleID(20104293);
        p926875513.setRightExactPuzzleID(361616815);
        p926875513.getDisplayImage().loadImageFromResource(c, R.drawable.p926875513h);
        p926875513.setExactRow(0);
        p926875513.setExactColumn(6);
        p926875513.getSize().reset(126.6667f, 100.0f);
        p926875513.getPositionOffset().reset(-50.0f, -50.0f);
        p926875513.setIsUseAbsolutePosition(true);
        p926875513.setIsUseAbsoluteSize(true);
        p926875513.getImage().setIsUseAbsolutePosition(true);
        p926875513.getImage().setIsUseAbsoluteSize(true);
        p926875513.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p926875513.resetPosition();
        getSpiritList().add(p926875513);
    }

    /* access modifiers changed from: package-private */
    public void c1341340930(Context c) {
        Puzzle p1341340930 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1341340930(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1341340930(editor, this);
            }
        };
        p1341340930.setID(1341340930);
        p1341340930.setName("1341340930");
        p1341340930.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1341340930);
        this.Desktop.RandomlyPlaced(p1341340930);
        p1341340930.setTopEdgeType(EdgeType.Convex);
        p1341340930.setBottomEdgeType(EdgeType.Concave);
        p1341340930.setLeftEdgeType(EdgeType.Convex);
        p1341340930.setRightEdgeType(EdgeType.Convex);
        p1341340930.setTopExactPuzzleID(926875513);
        p1341340930.setBottomExactPuzzleID(1042524943);
        p1341340930.setLeftExactPuzzleID(342561831);
        p1341340930.setRightExactPuzzleID(1147900269);
        p1341340930.getDisplayImage().loadImageFromResource(c, R.drawable.p1341340930h);
        p1341340930.setExactRow(1);
        p1341340930.setExactColumn(6);
        p1341340930.getSize().reset(153.3333f, 126.6667f);
        p1341340930.getPositionOffset().reset(-76.66666f, -76.66666f);
        p1341340930.setIsUseAbsolutePosition(true);
        p1341340930.setIsUseAbsoluteSize(true);
        p1341340930.getImage().setIsUseAbsolutePosition(true);
        p1341340930.getImage().setIsUseAbsoluteSize(true);
        p1341340930.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1341340930.resetPosition();
        getSpiritList().add(p1341340930);
    }

    /* access modifiers changed from: package-private */
    public void c1042524943(Context c) {
        Puzzle p1042524943 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1042524943(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1042524943(editor, this);
            }
        };
        p1042524943.setID(1042524943);
        p1042524943.setName("1042524943");
        p1042524943.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1042524943);
        this.Desktop.RandomlyPlaced(p1042524943);
        p1042524943.setTopEdgeType(EdgeType.Convex);
        p1042524943.setBottomEdgeType(EdgeType.Concave);
        p1042524943.setLeftEdgeType(EdgeType.Concave);
        p1042524943.setRightEdgeType(EdgeType.Convex);
        p1042524943.setTopExactPuzzleID(1341340930);
        p1042524943.setBottomExactPuzzleID(574086540);
        p1042524943.setLeftExactPuzzleID(1214901854);
        p1042524943.setRightExactPuzzleID(275665014);
        p1042524943.getDisplayImage().loadImageFromResource(c, R.drawable.p1042524943h);
        p1042524943.setExactRow(2);
        p1042524943.setExactColumn(6);
        p1042524943.getSize().reset(126.6667f, 126.6667f);
        p1042524943.getPositionOffset().reset(-50.0f, -76.66666f);
        p1042524943.setIsUseAbsolutePosition(true);
        p1042524943.setIsUseAbsoluteSize(true);
        p1042524943.getImage().setIsUseAbsolutePosition(true);
        p1042524943.getImage().setIsUseAbsoluteSize(true);
        p1042524943.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1042524943.resetPosition();
        getSpiritList().add(p1042524943);
    }

    /* access modifiers changed from: package-private */
    public void c574086540(Context c) {
        Puzzle p574086540 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load574086540(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save574086540(editor, this);
            }
        };
        p574086540.setID(574086540);
        p574086540.setName("574086540");
        p574086540.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p574086540);
        this.Desktop.RandomlyPlaced(p574086540);
        p574086540.setTopEdgeType(EdgeType.Convex);
        p574086540.setBottomEdgeType(EdgeType.Convex);
        p574086540.setLeftEdgeType(EdgeType.Convex);
        p574086540.setRightEdgeType(EdgeType.Convex);
        p574086540.setTopExactPuzzleID(1042524943);
        p574086540.setBottomExactPuzzleID(1700115332);
        p574086540.setLeftExactPuzzleID(591757449);
        p574086540.setRightExactPuzzleID(1701867261);
        p574086540.getDisplayImage().loadImageFromResource(c, R.drawable.p574086540h);
        p574086540.setExactRow(3);
        p574086540.setExactColumn(6);
        p574086540.getSize().reset(153.3333f, 153.3333f);
        p574086540.getPositionOffset().reset(-76.66666f, -76.66666f);
        p574086540.setIsUseAbsolutePosition(true);
        p574086540.setIsUseAbsoluteSize(true);
        p574086540.getImage().setIsUseAbsolutePosition(true);
        p574086540.getImage().setIsUseAbsoluteSize(true);
        p574086540.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p574086540.resetPosition();
        getSpiritList().add(p574086540);
    }

    /* access modifiers changed from: package-private */
    public void c1700115332(Context c) {
        Puzzle p1700115332 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1700115332(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1700115332(editor, this);
            }
        };
        p1700115332.setID(1700115332);
        p1700115332.setName("1700115332");
        p1700115332.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1700115332);
        this.Desktop.RandomlyPlaced(p1700115332);
        p1700115332.setTopEdgeType(EdgeType.Concave);
        p1700115332.setBottomEdgeType(EdgeType.Convex);
        p1700115332.setLeftEdgeType(EdgeType.Concave);
        p1700115332.setRightEdgeType(EdgeType.Convex);
        p1700115332.setTopExactPuzzleID(574086540);
        p1700115332.setBottomExactPuzzleID(396314980);
        p1700115332.setLeftExactPuzzleID(2123219446);
        p1700115332.setRightExactPuzzleID(1703300956);
        p1700115332.getDisplayImage().loadImageFromResource(c, R.drawable.p1700115332h);
        p1700115332.setExactRow(4);
        p1700115332.setExactColumn(6);
        p1700115332.getSize().reset(126.6667f, 126.6667f);
        p1700115332.getPositionOffset().reset(-50.0f, -50.0f);
        p1700115332.setIsUseAbsolutePosition(true);
        p1700115332.setIsUseAbsoluteSize(true);
        p1700115332.getImage().setIsUseAbsolutePosition(true);
        p1700115332.getImage().setIsUseAbsoluteSize(true);
        p1700115332.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1700115332.resetPosition();
        getSpiritList().add(p1700115332);
    }

    /* access modifiers changed from: package-private */
    public void c396314980(Context c) {
        Puzzle p396314980 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load396314980(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save396314980(editor, this);
            }
        };
        p396314980.setID(396314980);
        p396314980.setName("396314980");
        p396314980.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p396314980);
        this.Desktop.RandomlyPlaced(p396314980);
        p396314980.setTopEdgeType(EdgeType.Concave);
        p396314980.setBottomEdgeType(EdgeType.Flat);
        p396314980.setLeftEdgeType(EdgeType.Convex);
        p396314980.setRightEdgeType(EdgeType.Concave);
        p396314980.setTopExactPuzzleID(1700115332);
        p396314980.setBottomExactPuzzleID(-1);
        p396314980.setLeftExactPuzzleID(994920776);
        p396314980.setRightExactPuzzleID(1045321425);
        p396314980.getDisplayImage().loadImageFromResource(c, R.drawable.p396314980h);
        p396314980.setExactRow(5);
        p396314980.setExactColumn(6);
        p396314980.getSize().reset(126.6667f, 100.0f);
        p396314980.getPositionOffset().reset(-76.66666f, -50.0f);
        p396314980.setIsUseAbsolutePosition(true);
        p396314980.setIsUseAbsoluteSize(true);
        p396314980.getImage().setIsUseAbsolutePosition(true);
        p396314980.getImage().setIsUseAbsoluteSize(true);
        p396314980.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p396314980.resetPosition();
        getSpiritList().add(p396314980);
    }

    /* access modifiers changed from: package-private */
    public void c361616815(Context c) {
        Puzzle p361616815 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load361616815(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save361616815(editor, this);
            }
        };
        p361616815.setID(361616815);
        p361616815.setName("361616815");
        p361616815.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p361616815);
        this.Desktop.RandomlyPlaced(p361616815);
        p361616815.setTopEdgeType(EdgeType.Flat);
        p361616815.setBottomEdgeType(EdgeType.Concave);
        p361616815.setLeftEdgeType(EdgeType.Concave);
        p361616815.setRightEdgeType(EdgeType.Flat);
        p361616815.setTopExactPuzzleID(-1);
        p361616815.setBottomExactPuzzleID(1147900269);
        p361616815.setLeftExactPuzzleID(926875513);
        p361616815.setRightExactPuzzleID(-1);
        p361616815.getDisplayImage().loadImageFromResource(c, R.drawable.p361616815h);
        p361616815.setExactRow(0);
        p361616815.setExactColumn(7);
        p361616815.getSize().reset(100.0f, 100.0f);
        p361616815.getPositionOffset().reset(-50.0f, -50.0f);
        p361616815.setIsUseAbsolutePosition(true);
        p361616815.setIsUseAbsoluteSize(true);
        p361616815.getImage().setIsUseAbsolutePosition(true);
        p361616815.getImage().setIsUseAbsoluteSize(true);
        p361616815.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p361616815.resetPosition();
        getSpiritList().add(p361616815);
    }

    /* access modifiers changed from: package-private */
    public void c1147900269(Context c) {
        Puzzle p1147900269 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1147900269(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1147900269(editor, this);
            }
        };
        p1147900269.setID(1147900269);
        p1147900269.setName("1147900269");
        p1147900269.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1147900269);
        this.Desktop.RandomlyPlaced(p1147900269);
        p1147900269.setTopEdgeType(EdgeType.Convex);
        p1147900269.setBottomEdgeType(EdgeType.Concave);
        p1147900269.setLeftEdgeType(EdgeType.Concave);
        p1147900269.setRightEdgeType(EdgeType.Flat);
        p1147900269.setTopExactPuzzleID(361616815);
        p1147900269.setBottomExactPuzzleID(275665014);
        p1147900269.setLeftExactPuzzleID(1341340930);
        p1147900269.setRightExactPuzzleID(-1);
        p1147900269.getDisplayImage().loadImageFromResource(c, R.drawable.p1147900269h);
        p1147900269.setExactRow(1);
        p1147900269.setExactColumn(7);
        p1147900269.getSize().reset(100.0f, 126.6667f);
        p1147900269.getPositionOffset().reset(-50.0f, -76.66666f);
        p1147900269.setIsUseAbsolutePosition(true);
        p1147900269.setIsUseAbsoluteSize(true);
        p1147900269.getImage().setIsUseAbsolutePosition(true);
        p1147900269.getImage().setIsUseAbsoluteSize(true);
        p1147900269.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1147900269.resetPosition();
        getSpiritList().add(p1147900269);
    }

    /* access modifiers changed from: package-private */
    public void c275665014(Context c) {
        Puzzle p275665014 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load275665014(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save275665014(editor, this);
            }
        };
        p275665014.setID(275665014);
        p275665014.setName("275665014");
        p275665014.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p275665014);
        this.Desktop.RandomlyPlaced(p275665014);
        p275665014.setTopEdgeType(EdgeType.Convex);
        p275665014.setBottomEdgeType(EdgeType.Concave);
        p275665014.setLeftEdgeType(EdgeType.Concave);
        p275665014.setRightEdgeType(EdgeType.Flat);
        p275665014.setTopExactPuzzleID(1147900269);
        p275665014.setBottomExactPuzzleID(1701867261);
        p275665014.setLeftExactPuzzleID(1042524943);
        p275665014.setRightExactPuzzleID(-1);
        p275665014.getDisplayImage().loadImageFromResource(c, R.drawable.p275665014h);
        p275665014.setExactRow(2);
        p275665014.setExactColumn(7);
        p275665014.getSize().reset(100.0f, 126.6667f);
        p275665014.getPositionOffset().reset(-50.0f, -76.66666f);
        p275665014.setIsUseAbsolutePosition(true);
        p275665014.setIsUseAbsoluteSize(true);
        p275665014.getImage().setIsUseAbsolutePosition(true);
        p275665014.getImage().setIsUseAbsoluteSize(true);
        p275665014.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p275665014.resetPosition();
        getSpiritList().add(p275665014);
    }

    /* access modifiers changed from: package-private */
    public void c1701867261(Context c) {
        Puzzle p1701867261 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1701867261(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1701867261(editor, this);
            }
        };
        p1701867261.setID(1701867261);
        p1701867261.setName("1701867261");
        p1701867261.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1701867261);
        this.Desktop.RandomlyPlaced(p1701867261);
        p1701867261.setTopEdgeType(EdgeType.Convex);
        p1701867261.setBottomEdgeType(EdgeType.Convex);
        p1701867261.setLeftEdgeType(EdgeType.Concave);
        p1701867261.setRightEdgeType(EdgeType.Flat);
        p1701867261.setTopExactPuzzleID(275665014);
        p1701867261.setBottomExactPuzzleID(1703300956);
        p1701867261.setLeftExactPuzzleID(574086540);
        p1701867261.setRightExactPuzzleID(-1);
        p1701867261.getDisplayImage().loadImageFromResource(c, R.drawable.p1701867261h);
        p1701867261.setExactRow(3);
        p1701867261.setExactColumn(7);
        p1701867261.getSize().reset(100.0f, 153.3333f);
        p1701867261.getPositionOffset().reset(-50.0f, -76.66666f);
        p1701867261.setIsUseAbsolutePosition(true);
        p1701867261.setIsUseAbsoluteSize(true);
        p1701867261.getImage().setIsUseAbsolutePosition(true);
        p1701867261.getImage().setIsUseAbsoluteSize(true);
        p1701867261.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1701867261.resetPosition();
        getSpiritList().add(p1701867261);
    }

    /* access modifiers changed from: package-private */
    public void c1703300956(Context c) {
        Puzzle p1703300956 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1703300956(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1703300956(editor, this);
            }
        };
        p1703300956.setID(1703300956);
        p1703300956.setName("1703300956");
        p1703300956.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1703300956);
        this.Desktop.RandomlyPlaced(p1703300956);
        p1703300956.setTopEdgeType(EdgeType.Concave);
        p1703300956.setBottomEdgeType(EdgeType.Concave);
        p1703300956.setLeftEdgeType(EdgeType.Concave);
        p1703300956.setRightEdgeType(EdgeType.Flat);
        p1703300956.setTopExactPuzzleID(1701867261);
        p1703300956.setBottomExactPuzzleID(1045321425);
        p1703300956.setLeftExactPuzzleID(1700115332);
        p1703300956.setRightExactPuzzleID(-1);
        p1703300956.getDisplayImage().loadImageFromResource(c, R.drawable.p1703300956h);
        p1703300956.setExactRow(4);
        p1703300956.setExactColumn(7);
        p1703300956.getSize().reset(100.0f, 100.0f);
        p1703300956.getPositionOffset().reset(-50.0f, -50.0f);
        p1703300956.setIsUseAbsolutePosition(true);
        p1703300956.setIsUseAbsoluteSize(true);
        p1703300956.getImage().setIsUseAbsolutePosition(true);
        p1703300956.getImage().setIsUseAbsoluteSize(true);
        p1703300956.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1703300956.resetPosition();
        getSpiritList().add(p1703300956);
    }

    /* access modifiers changed from: package-private */
    public void c1045321425(Context c) {
        Puzzle p1045321425 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1045321425(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1045321425(editor, this);
            }
        };
        p1045321425.setID(1045321425);
        p1045321425.setName("1045321425");
        p1045321425.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1045321425);
        this.Desktop.RandomlyPlaced(p1045321425);
        p1045321425.setTopEdgeType(EdgeType.Convex);
        p1045321425.setBottomEdgeType(EdgeType.Flat);
        p1045321425.setLeftEdgeType(EdgeType.Convex);
        p1045321425.setRightEdgeType(EdgeType.Flat);
        p1045321425.setTopExactPuzzleID(1703300956);
        p1045321425.setBottomExactPuzzleID(-1);
        p1045321425.setLeftExactPuzzleID(396314980);
        p1045321425.setRightExactPuzzleID(-1);
        p1045321425.getDisplayImage().loadImageFromResource(c, R.drawable.p1045321425h);
        p1045321425.setExactRow(5);
        p1045321425.setExactColumn(7);
        p1045321425.getSize().reset(126.6667f, 126.6667f);
        p1045321425.getPositionOffset().reset(-76.66666f, -76.66666f);
        p1045321425.setIsUseAbsolutePosition(true);
        p1045321425.setIsUseAbsoluteSize(true);
        p1045321425.getImage().setIsUseAbsolutePosition(true);
        p1045321425.getImage().setIsUseAbsoluteSize(true);
        p1045321425.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1045321425.resetPosition();
        getSpiritList().add(p1045321425);
    }
}
