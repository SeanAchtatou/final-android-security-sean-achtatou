package com.skyd.bestpuzzle;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.skyd.bestpuzzle.n1648.R;
import com.skyd.core.android.Android;
import com.skyd.core.android.CommonActivity;

public class MoreApps extends CommonActivity {
    private Button _Button01 = null;
    private Button _Button02 = null;

    public Button getButton01() {
        if (this._Button01 == null) {
            this._Button01 = (Button) findViewById(R.id.Button01);
        }
        return this._Button01;
    }

    public Button getButton02() {
        if (this._Button02 == null) {
            this._Button02 = (Button) findViewById(R.id.Button02);
        }
        return this._Button02;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.moreapps);
        getButton02().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MoreApps.this.finish();
            }
        });
        getButton01().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Android.openUrl(MoreApps.this, "market://search?q=pub:" + MoreApps.this.getResources().getString(R.string.author));
            }
        });
    }
}
