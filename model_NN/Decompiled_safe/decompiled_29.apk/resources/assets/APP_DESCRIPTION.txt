A simple to use user friendly flashlight app that uses the screen instead of the cameras' LED light. Click on the screen to show the scroll bars and click again to remove them, users can select how much red, green, blue, and the brightness level they want for their light. Good for phones with no camera LED lights, or for colored light. 
 
* Like this app? Please support me by either downloading and running a sponsors application (Menu -> More Apps), clicking an ad when you see one interesting, or leaving a good rating/comment. Thanks! *

Permissions: 
 
INTERNET - Used by the Admob SDK for the requesting of ads.

ACCESS_NETWORK_STATE & READ_PHONE_STATE - Used to handle errors when the device is not connected to an internet source.

WAKE_LOCK - Used to prevent phone from sleeping while the flashlight is on.