package com.flurry.android;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.NinePatch;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.NinePatchDrawable;
import android.os.Build;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

final class v extends RelativeLayout {
    private q a;
    private ag b;
    private String c;
    private int d;

    public v(Context context, q qVar, ag agVar, f fVar, int i, boolean z) {
        super(context);
        this.a = qVar;
        this.b = agVar;
        s sVar = agVar.b;
        this.d = i;
        switch (this.d) {
            case Constants.MODE_LANDSCAPE:
                if (z) {
                    a(context, fVar, sVar, false);
                } else {
                    a(context, fVar, sVar, true);
                }
            case 1:
                if (!z) {
                    a(context, fVar, sVar, true);
                    break;
                } else {
                    a(context, fVar, sVar, false);
                    break;
                }
        }
        setFocusable(true);
    }

    private void a(Context context, f fVar, s sVar, boolean z) {
        Drawable bitmapDrawable;
        Bitmap bitmap;
        setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        d dVar = fVar.d;
        ImageView imageView = new ImageView(context);
        imageView.setId(1);
        AdImage adImage = sVar.h;
        if (adImage != null) {
            byte[] bArr = adImage.e;
            Bitmap decodeByteArray = BitmapFactory.decodeByteArray(bArr, 0, bArr.length);
            Bitmap createBitmap = Bitmap.createBitmap(decodeByteArray.getWidth(), decodeByteArray.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(createBitmap);
            Paint paint = new Paint();
            Rect rect = new Rect(0, 0, decodeByteArray.getWidth(), decodeByteArray.getHeight());
            RectF rectF = new RectF(rect);
            float a2 = (float) i.a(context, 8);
            paint.setAntiAlias(true);
            canvas.drawARGB(0, 0, 0, 0);
            paint.setColor(-16777216);
            canvas.drawRoundRect(rectF, a2, a2, paint);
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
            canvas.drawBitmap(decodeByteArray, rect, rect, paint);
            if (Integer.parseInt(Build.VERSION.SDK) > 4) {
                BlurMaskFilter blurMaskFilter = new BlurMaskFilter(3.0f, BlurMaskFilter.Blur.OUTER);
                Paint paint2 = new Paint();
                paint2.setMaskFilter(blurMaskFilter);
                int[] iArr = new int[2];
                Bitmap copy = createBitmap.extractAlpha(paint2, iArr).copy(Bitmap.Config.ARGB_8888, true);
                new Canvas(copy).drawBitmap(createBitmap, (float) (-iArr[0]), (float) (-iArr[1]), (Paint) null);
                bitmap = copy;
            } else {
                bitmap = createBitmap;
            }
            imageView.setImageBitmap(bitmap);
            i.a(context, imageView, i.a(context, dVar.m), i.a(context, dVar.n));
            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        }
        AdImage a3 = this.a.a(dVar.c);
        if (a3 != null) {
            byte[] bArr2 = a3.e;
            Bitmap decodeByteArray2 = BitmapFactory.decodeByteArray(bArr2, 0, bArr2.length);
            if (NinePatch.isNinePatchChunk(decodeByteArray2.getNinePatchChunk())) {
                bitmapDrawable = new NinePatchDrawable(decodeByteArray2, decodeByteArray2.getNinePatchChunk(), new Rect(0, 0, 0, 0), null);
            } else {
                bitmapDrawable = new BitmapDrawable(decodeByteArray2);
            }
            setBackgroundDrawable(bitmapDrawable);
        }
        TextView textView = new TextView(context);
        textView.setId(5);
        textView.setPadding(0, 0, 0, 0);
        TextView textView2 = new TextView(context);
        textView2.setId(3);
        textView2.setPadding(0, 0, 0, 0);
        if (z) {
            textView.setTextColor(dVar.f);
            textView.setTextSize((float) dVar.e);
            textView.setText(new String("• " + dVar.b));
            textView.setTypeface(Typeface.create(dVar.d, 0));
            textView2.setTextColor(dVar.i);
            textView2.setTextSize((float) dVar.h);
            textView2.setTypeface(Typeface.create(dVar.g, 0));
            textView2.setText(sVar.d);
        } else {
            textView.setId(3);
            textView.setText(sVar.d);
            textView.setTextColor(dVar.i);
            textView.setTextSize((float) dVar.h);
            textView.setTypeface(Typeface.create(dVar.g, 0));
            textView2.setId(4);
            textView2.setText(sVar.c);
            textView2.setTextColor(dVar.l);
            textView2.setTextSize((float) dVar.k);
            textView2.setTypeface(Typeface.create(dVar.j, 0));
        }
        setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
        addView(new ImageView(context), new RelativeLayout.LayoutParams(-1, -2));
        int i = (dVar.q - (dVar.o << 1)) - dVar.m;
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(9);
        layoutParams.setMargins(dVar.o, dVar.p, i, 0);
        addView(imageView, layoutParams);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams2.addRule(6, imageView.getId());
        layoutParams2.addRule(1, imageView.getId());
        layoutParams2.setMargins(0, 0, 0, 0);
        addView(textView, layoutParams2);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams3.addRule(1, imageView.getId());
        layoutParams3.addRule(3, textView.getId());
        layoutParams3.setMargins(0, -2, 0, 0);
        addView(textView2, layoutParams3);
    }

    /* access modifiers changed from: package-private */
    public final void a(String str) {
        this.c = str;
    }

    /* access modifiers changed from: package-private */
    public final String b(String str) {
        return str + this.c + System.currentTimeMillis();
    }

    /* access modifiers changed from: package-private */
    public final ag a() {
        return this.b;
    }
}
