package com.flurry.android;

import android.webkit.WebView;
import android.webkit.WebViewClient;

final class h extends WebViewClient {
    private /* synthetic */ CatalogActivity a;

    h(CatalogActivity catalogActivity) {
        this.a = catalogActivity;
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        if (str == null) {
            return false;
        }
        if (this.a.f != null) {
            this.a.f.a(new j((byte) 6, this.a.e.i()));
        }
        this.a.e.a(webView.getContext(), this.a.f, str);
        return true;
    }

    public final void onReceivedError(WebView webView, int i, String str, String str2) {
        ah.c("FlurryAgent", "Failed to load url: " + str2);
        webView.loadData("Cannot find Android Market information. <p>Please check your network", "text/html", "UTF-8");
    }

    public final void onPageFinished(WebView webView, String str) {
        try {
            ag a2 = this.a.f;
            j jVar = new j((byte) 5, this.a.e.i());
            long j = this.a.f.c;
            a2.d.add(jVar);
            a2.c = j;
        } catch (Exception e) {
        }
    }
}
