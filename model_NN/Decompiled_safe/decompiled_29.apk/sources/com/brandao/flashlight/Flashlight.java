package com.brandao.flashlight;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.PowerManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import com.brandao.apputil.CheckForUpdate;
import com.flurry.android.Constants;
import com.flurry.android.FlurryAgent;
import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdView;
import java.io.IOException;
import java.io.InputStream;

public class Flashlight extends Activity implements SeekBar.OnSeekBarChangeListener {
    public static final boolean DEBUG = false;
    public static final String TAG = "Flashlight";
    private final int CLOSE_APP_MENU_ID = 1;
    private final int DIALOG_ANALYTICS = 1;
    private final String PREFS_BLUE = "blue";
    private final String PREFS_BRIGHTNESS = "brightness";
    private final String PREFS_GREEN = "green";
    private final String PREFS_RED = "red";
    private final int RECOMMENDED_APPS_MENU_ID = 3;
    private final int SETTINGS_MENU_ID = 2;
    /* access modifiers changed from: private */
    public AdView adView;
    private SeekBar blue;
    private int blueLight = 255;
    private TextView blueTV;
    private SeekBar brightness;
    private int brightnessLight = 255;
    private TextView brightnessTV;
    private int defaultColors = 255;
    private SeekBar green;
    private int greenLight = 255;
    private TextView greenTV;
    private LinearLayout light;
    WindowManager.LayoutParams lp;
    private SeekBar red;
    private int redLight = 255;
    private TextView redTV;
    /* access modifiers changed from: private */
    public boolean visible = true;
    PowerManager.WakeLock wl;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitleBar(false);
        setContentView((int) R.layout.flashlight_view);
        this.adView = (AdView) findViewById(R.id.ad_whirl_layout_ad);
        SharedPreferences settings = getSharedPreferences(getString(R.string.app_name), 0);
        this.redLight = settings.getInt("red", this.defaultColors);
        this.greenLight = settings.getInt("green", this.defaultColors);
        this.blueLight = settings.getInt("blue", this.defaultColors);
        this.brightnessLight = settings.getInt("brightness", 100);
        this.red = (SeekBar) findViewById(R.id.red_flash_light_seek_bar);
        this.red.setProgress(this.redLight);
        this.blue = (SeekBar) findViewById(R.id.blue_flash_light_seek_bar);
        this.blue.setProgress(this.blueLight);
        this.green = (SeekBar) findViewById(R.id.green_flash_light_seek_bar);
        this.green.setProgress(this.greenLight);
        this.brightness = (SeekBar) findViewById(R.id.brightness_seek_bar);
        this.brightness.setProgress(this.brightnessLight - 10);
        this.redTV = (TextView) findViewById(R.id.flashlight_red_num);
        this.blueTV = (TextView) findViewById(R.id.flashlight_blue_num);
        this.greenTV = (TextView) findViewById(R.id.flashlight_green_num);
        this.brightnessTV = (TextView) findViewById(R.id.flashlight_brightness_num);
        this.blueTV.setText(String.valueOf(getString(R.string.blue)) + ": " + this.blueLight);
        this.redTV.setText(String.valueOf(getString(R.string.red)) + ": " + this.redLight);
        this.greenTV.setText(String.valueOf(getString(R.string.green)) + ": " + this.greenLight);
        this.brightnessTV.setText(String.valueOf(getString(R.string.brightness)) + ": " + this.brightnessLight);
        this.red.setOnSeekBarChangeListener(this);
        this.blue.setOnSeekBarChangeListener(this);
        this.green.setOnSeekBarChangeListener(this);
        this.brightness.setOnSeekBarChangeListener(this);
        this.light = (LinearLayout) findViewById(R.id.flashlight_light);
        this.light.setBackgroundColor(Color.rgb(this.redLight, this.greenLight, this.blueLight));
        this.light.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Flashlight.this.visible) {
                    Flashlight.this.removeObjects();
                } else {
                    Flashlight.this.showObjects();
                }
                Flashlight.this.visible = !Flashlight.this.visible;
            }
        });
        this.lp = getWindow().getAttributes();
        this.lp.screenBrightness = ((float) this.brightnessLight) / 100.0f;
        getWindow().setAttributes(this.lp);
        initAds();
        if (Prefs.getCheckForUpdate(this)) {
            new CheckForUpdate(this, 0).startCheckForUpdate();
        }
    }

    public void initAds() {
        this.adView.setAdListener(new AdListener() {
            public void onDismissScreen(Ad arg0) {
            }

            public void onFailedToReceiveAd(Ad arg0, AdRequest.ErrorCode arg1) {
            }

            public void onLeaveApplication(Ad arg0) {
            }

            public void onPresentScreen(Ad arg0) {
            }

            public void onReceiveAd(Ad ad) {
                Animation fadeIn = AnimationUtils.loadAnimation(Flashlight.this, 17432576);
                fadeIn.setDuration(1000);
                Flashlight.this.adView.startAnimation(fadeIn);
            }
        });
        this.adView.loadAd(new AdRequest());
    }

    public void removeObjects() {
        this.red.setVisibility(4);
        this.blue.setVisibility(4);
        this.green.setVisibility(4);
        this.brightness.setVisibility(4);
        this.adView.setVisibility(4);
        this.redTV.setVisibility(4);
        this.blueTV.setVisibility(4);
        this.greenTV.setVisibility(4);
        this.brightnessTV.setVisibility(4);
        setTitleBar(true);
    }

    public void showObjects() {
        this.red.setVisibility(0);
        this.blue.setVisibility(0);
        this.green.setVisibility(0);
        this.brightness.setVisibility(0);
        this.adView.setVisibility(0);
        this.redTV.setVisibility(0);
        this.blueTV.setVisibility(0);
        this.greenTV.setVisibility(0);
        this.brightnessTV.setVisibility(0);
        setTitleBar(false);
    }

    public void setTitleBar(boolean shown) {
        Window win = getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        winParams.flags |= 256;
        winParams.flags |= 512;
        winParams.flags |= 2;
        if (shown) {
            winParams.flags |= 1024;
        } else {
            winParams.flags &= -1025;
        }
        win.setAttributes(winParams);
    }

    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        switch (seekBar.getId()) {
            case R.id.brightness_seek_bar:
                this.lp.screenBrightness = ((float) (progress + 10)) / 100.0f;
                getWindow().setAttributes(this.lp);
                this.brightnessLight = progress + 10;
                break;
            case R.id.red_flash_light_seek_bar:
                this.redLight = progress;
                break;
            case R.id.green_flash_light_seek_bar:
                this.greenLight = progress;
                break;
            case R.id.blue_flash_light_seek_bar:
                this.blueLight = progress;
                break;
        }
        this.light.setBackgroundColor(Color.rgb(this.redLight, this.greenLight, this.blueLight));
        this.blueTV.setText(String.valueOf(getString(R.string.blue)) + ": " + this.blueLight);
        this.redTV.setText(String.valueOf(getString(R.string.red)) + ": " + this.redLight);
        this.greenTV.setText(String.valueOf(getString(R.string.green)) + ": " + this.greenLight);
        this.brightnessTV.setText(String.valueOf(getString(R.string.brightness)) + ": " + this.brightnessLight);
    }

    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    public void onStopTrackingTouch(SeekBar seekBar) {
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        SharedPreferences.Editor editor = getSharedPreferences(getString(R.string.app_name), 0).edit();
        editor.putInt("red", this.redLight);
        editor.putInt("green", this.greenLight);
        editor.putInt("blue", this.blueLight);
        editor.putInt("brightness", this.brightnessLight);
        editor.commit();
        this.wl.release();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, 1, 0, (int) R.string.exit).setIcon((int) R.drawable.power_off_menu_icon);
        menu.add(0, 2, 0, (int) R.string.settings).setIcon((int) R.drawable.information_menu_icon);
        menu.add(0, 3, 0, (int) R.string.more_apps).setIcon((int) R.drawable.more_apps_menu_icon);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                if (Prefs.getAnalytics(this)) {
                    FlurryAgent.onEvent("App Closed - Menu");
                }
                finish();
                return true;
            case Constants.MODE_LANDSCAPE /*2*/:
                if (Prefs.getAnalytics(this)) {
                    FlurryAgent.onEvent("Settings Launched - Menu");
                }
                startActivity(new Intent(this, Prefs.class));
                return true;
            case 3:
                if (Prefs.getAnalytics(this)) {
                    FlurryAgent.onEvent("Flurry Apps Launched");
                }
                try {
                    FlurryAgent.getAppCircle().openCatalog(this, "hookName1");
                } catch (Exception e) {
                    Exception e2 = e;
                    FlurryAgent.onError(e2.toString(), e2.getMessage(), TAG);
                    Toast.makeText(this, (int) R.string.flurry_error, 0).show();
                }
                return true;
            default:
                return false;
        }
    }

    public void onResume() {
        super.onResume();
        this.wl = ((PowerManager) getSystemService("power")).newWakeLock(6, "My Tag");
        this.wl.acquire();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                View layout = LayoutInflater.from(this).inflate((int) R.layout.basic_dialog_layout, (ViewGroup) null);
                TextView display = (TextView) layout.findViewById(R.id.basic_dialog_display);
                display.append(String.valueOf(getString(R.string.thanks_for_downloading)) + " " + getString(R.string.app_name) + "!");
                display.append("\n\n");
                display.append(getString(R.string.app_version_build));
                display.append("\n");
                display.append(getString(R.string.copy_right_description));
                display.append("\n");
                display.append(getString(R.string.brandao_apps_email));
                display.append("\n\n");
                try {
                    InputStream is = getAssets().open("APP_DESCRIPTION.txt");
                    byte[] buffer = new byte[is.available()];
                    is.read(buffer);
                    is.close();
                    display.append(new String(buffer));
                } catch (IOException e) {
                    IOException e2 = e;
                    FlurryAgent.onError(e2.toString(), e2.getMessage(), TAG);
                }
                display.append("\n\n");
                try {
                    InputStream is2 = getAssets().open("CHANGE_LOG.txt");
                    byte[] buffer2 = new byte[is2.available()];
                    is2.read(buffer2);
                    is2.close();
                    display.append(new String(buffer2));
                } catch (IOException e3) {
                    IOException e4 = e3;
                    FlurryAgent.onError(e4.toString(), e4.getMessage(), TAG);
                }
                return new AlertDialog.Builder(this).setTitle((int) R.string.app_name).setView(layout).setIcon((int) R.drawable.icon).setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                }).create();
            default:
                return null;
        }
    }

    public void onStart() {
        super.onStart();
        FlurryAgent.enableAppCircle();
        FlurryAgent.setCaptureUncaughtExceptions(true);
        FlurryAgent.setLogEnabled(false);
        FlurryAgent.setCatalogIntentName("com.brandao.flashlight.flurry.catalog");
        FlurryAgent.onStartSession(this, "JMLQPNU92TR3W5VB3HYY");
        FlurryAgent.onPageView();
        if (Prefs.getFirstOpen(this)) {
            showDialog(1);
            Prefs.putFirstOpen(this, false);
            FlurryAgent.onEvent("New User Started");
        }
    }

    public void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }
}
