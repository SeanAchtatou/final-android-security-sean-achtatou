package com.brandao.flashlight;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.brandao.apputil.CheckForUpdate;
import com.brandao.apputil.DialogHelper;
import com.brandao.apputil.Share;
import com.flurry.android.Constants;
import com.flurry.android.FlurryAgent;
import java.io.IOException;
import java.io.InputStream;

public class Prefs extends PreferenceActivity {
    public static final String TAG = "Prefs";
    private final int ABOUT_DIALOG = 1;
    private final int CHANGE_LOG_DIALOG = 0;
    private final int LICENSE_DIALOG = 2;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings);
        setContentView((int) R.layout.preference_with_ad);
        findPreference("analytics").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                if (Prefs.getAnalytics(Prefs.this)) {
                    FlurryAgent.onEvent("Analytics Enabled");
                    return true;
                }
                FlurryAgent.onEvent("Analytics Disabled");
                return true;
            }
        });
        findPreference("send_feedback_key").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                if (Prefs.getAnalytics(Prefs.this)) {
                    FlurryAgent.onEvent("Feedback Launched");
                }
                Share.startEmailApp(Prefs.this.getString(R.string.brandao_apps_email), String.valueOf(Prefs.this.getResources().getString(R.string.app_name)) + " " + Prefs.this.getString(R.string.feedback), "\n \n \n" + Share.generateUserInfo(Prefs.this), Prefs.this);
                return true;
            }
        });
        findPreference("change_log_key").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                Prefs.this.showDialog(0);
                if (!Prefs.getAnalytics(Prefs.this)) {
                    return true;
                }
                FlurryAgent.onEvent("Change Log Launched");
                return true;
            }
        });
        findPreference("launch_market_key").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                if (Prefs.getAnalytics(Prefs.this)) {
                    FlurryAgent.onEvent("Market Launched");
                }
                Prefs.this.startActivity(new Intent("android.intent.action.VIEW").setData(Uri.parse("market://details?id=com.brandao.flashlight")));
                return true;
            }
        });
        findPreference("launch_more_apps_key").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                if (Prefs.getAnalytics(Prefs.this)) {
                    FlurryAgent.onEvent("More Apps Launched");
                }
                Prefs.this.startActivity(new Intent("android.intent.action.VIEW").setData(Uri.parse("http://market.android.com/search?q=Gabe Brandao")));
                return true;
            }
        });
        findPreference("app_description_key").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                Prefs.this.showDialog(1);
                if (Prefs.getAnalytics(Prefs.this)) {
                    FlurryAgent.onEvent("App Description Launched");
                }
                return true;
            }
        });
        findPreference("android_open_source_license_key").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                if (Prefs.getAnalytics(Prefs.this)) {
                    FlurryAgent.onEvent("Open Source Viewed");
                }
                Prefs.this.showDialog(2);
                return true;
            }
        });
        findPreference("launch_sponsor_apps_key").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                if (Prefs.getAnalytics(Prefs.this)) {
                    FlurryAgent.onEvent("Flurry Apps Launched");
                }
                try {
                    FlurryAgent.getAppCircle().openCatalog(Prefs.this, "hookName1");
                    return true;
                } catch (Exception e) {
                    Exception e2 = e;
                    FlurryAgent.onError(e2.toString(), e2.getMessage(), Prefs.TAG);
                    Toast.makeText(Prefs.this, (int) R.string.flurry_error, 0).show();
                    return true;
                }
            }
        });
        findPreference("launch_check_updates").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                if (Prefs.getAnalytics(Prefs.this)) {
                    FlurryAgent.onEvent("Check For Update");
                }
                new CheckForUpdate(Prefs.this, 1).startCheckForUpdate();
                return true;
            }
        });
        ViewGroup viewGroup = (ViewGroup) findViewById(R.id.flurry_ad);
        View promoView = FlurryAgent.getAppCircle().getHook(this, "hookName", 1);
        if (promoView != null) {
            viewGroup.addView(promoView);
        }
    }

    public static boolean putFirstOpen(Context context, boolean value) {
        return PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean("com.brandao.flashlight.first_open", value).commit();
    }

    public static boolean getFirstOpen(Context context) {
        return getPreferenceValue("com.brandao.flashlight.first_open", true, context);
    }

    public static boolean getAnalytics(Context context) {
        return getPreferenceValue("analytics", true, context);
    }

    public static boolean getCheckForUpdate(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("check_for_updates", true);
    }

    public void onStart() {
        super.onStart();
        FlurryAgent.enableAppCircle();
        FlurryAgent.setCaptureUncaughtExceptions(true);
        FlurryAgent.setLogEnabled(false);
        FlurryAgent.setCatalogIntentName("com.brandao.flashlight.flurry.catalog");
        FlurryAgent.onStartSession(this, "JMLQPNU92TR3W5VB3HYY");
        FlurryAgent.onPageView();
    }

    public void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case CheckForUpdate.AUTO:
                return new AlertDialog.Builder(this).setTitle((int) R.string.change_log).setView(DialogHelper.makeBasicLayout(readAssetTxtFile("CHANGE_LOG.txt"), this)).setIcon((int) R.drawable.icon).setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                }).create();
            case 1:
                return new AlertDialog.Builder(this).setTitle((int) R.string.app_name).setView(DialogHelper.makeBasicLayout(readAssetTxtFile("APP_DESCRIPTION.txt"), this)).setIcon((int) R.drawable.icon).setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                }).create();
            case Constants.MODE_LANDSCAPE /*2*/:
                return new AlertDialog.Builder(this).setTitle((int) R.string.notice).setView(DialogHelper.makeBasicLayout(readAssetTxtFile("NOTICE.txt"), this)).setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                }).create();
            default:
                return null;
        }
    }

    public String readAssetTxtFile(String assentName) {
        try {
            InputStream is = getAssets().open(assentName);
            byte[] buffer = new byte[is.available()];
            is.read(buffer);
            is.close();
            return new String(buffer);
        } catch (IOException e) {
            IOException e2 = e;
            FlurryAgent.onError(e2.toString(), e2.getMessage(), TAG);
            return getString(R.string.asset_file_not_read);
        }
    }

    public static boolean getPreferenceValue(String key, boolean defaultValue, Context context) {
        try {
            return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(key, defaultValue);
        } catch (Exception e) {
            if (!PreferenceManager.getDefaultSharedPreferences(context).contains(key)) {
                return defaultValue;
            }
            PreferenceManager.getDefaultSharedPreferences(context).edit().remove(key);
            PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(key, defaultValue).commit();
            return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(key, defaultValue);
        }
    }
}
