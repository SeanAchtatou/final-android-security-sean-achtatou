package com.brandao.apputil;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.brandao.flashlight.R;

public class DialogHelper {
    public static final String TAG = DialogHelper.class.getSimpleName();

    public static View makeBasicLayout(String message, Context context) {
        View layout = LayoutInflater.from(context).inflate((int) R.layout.basic_dialog_layout, (ViewGroup) null);
        ((TextView) layout.findViewById(R.id.basic_dialog_display)).setText(message);
        return layout;
    }
}
