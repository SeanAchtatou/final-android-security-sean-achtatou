package com.brandao.apputil;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import com.brandao.flashlight.R;
import java.io.BufferedInputStream;
import java.net.URL;
import org.apache.http.util.ByteArrayBuffer;

public class CheckForUpdate {
    public static final int AUTO = 0;
    public static final int MANUAL = 1;
    public static final String TAG = CheckForUpdate.class.getSimpleName();
    private int SIMPLE_NOTFICATION_ID;
    /* access modifiers changed from: private */
    public Context mContext;
    private NotificationManager mNotificationManager;
    /* access modifiers changed from: private */
    public String packageName = this.mContext.getPackageName();
    /* access modifiers changed from: private */
    public ProgressDialog progress;
    /* access modifiers changed from: private */
    public int type;

    public CheckForUpdate(Context _context, int _type) {
        this.mContext = _context;
        this.type = _type;
    }

    public void startCheckForUpdate() {
        CheckForUpdateThread check = new CheckForUpdateThread(this, null);
        if (this.type == 1) {
            this.progress = new ProgressDialog(this.mContext);
            this.progress.setMessage(this.mContext.getString(R.string.checking_for_update));
            this.progress.setCancelable(true);
            this.progress.setIndeterminate(true);
            this.progress.setOnCancelListener(check);
            this.progress.show();
        }
        check.execute(new String[0]);
    }

    private class CheckForUpdateThread extends AsyncTask<String, Integer, String> implements DialogInterface.OnCancelListener {
        Dialog noInternetDialog;
        TextView prompt;
        Dialog updateAvailiableDialog;

        private CheckForUpdateThread() {
        }

        /* synthetic */ CheckForUpdateThread(CheckForUpdate checkForUpdate, CheckForUpdateThread checkForUpdateThread) {
            this();
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            View updateView = LayoutInflater.from(CheckForUpdate.this.mContext).inflate((int) R.layout.basic_dialog_layout, (ViewGroup) null);
            this.prompt = (TextView) updateView.findViewById(R.id.basic_dialog_display);
            this.updateAvailiableDialog = new AlertDialog.Builder(CheckForUpdate.this.mContext).setTitle((int) R.string.update_availiable).setView(updateView).setIcon(CheckForUpdate.this.mContext.getApplicationInfo().icon).setPositiveButton((int) R.string.update, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent("android.intent.action.VIEW");
                    intent.setData(Uri.parse("market://details?id=" + CheckForUpdate.this.packageName));
                    CheckForUpdate.this.mContext.startActivity(intent);
                }
            }).setNegativeButton((int) R.string.close, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                }
            }).create();
            this.noInternetDialog = new AlertDialog.Builder(CheckForUpdate.this.mContext).setIcon(CheckForUpdate.this.mContext.getApplicationInfo().icon).setTitle((int) R.string.unable_to_connect_title).setMessage((int) R.string.unable_to_connect).setPositiveButton((int) R.string.continue_string, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                }
            }).setNegativeButton((int) R.string.settings_title, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    CheckForUpdate.this.mContext.startActivity(new Intent("android.settings.WIRELESS_SETTINGS"));
                }
            }).create();
        }

        /* access modifiers changed from: protected */
        public void onCancelled() {
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            try {
                if (CheckForUpdate.this.type == 1) {
                    CheckForUpdate.this.progress.dismiss();
                }
                if (result == null) {
                    if (CheckForUpdate.this.type == 1) {
                        this.noInternetDialog.show();
                    }
                } else if (result != "no_update") {
                    this.prompt.setText(result);
                    this.updateAvailiableDialog.show();
                    if (CheckForUpdate.this.type == 0) {
                        CheckForUpdate.this.showNotification();
                    }
                } else if (CheckForUpdate.this.type == 1) {
                    CustomToast.makeToast(CheckForUpdate.this.mContext.getString(R.string.no_update_availiable), CheckForUpdate.this.mContext).show();
                }
            } catch (Exception e) {
                if (result != "no_update") {
                    try {
                        Toast.makeText(CheckForUpdate.this.mContext, (int) R.string.update_availiable, 0).show();
                    } catch (Exception e2) {
                    }
                }
            }
        }

        /* access modifiers changed from: protected */
        public String doInBackground(String... params) {
            return CheckForUpdate.this.parseHTMLRequest();
        }

        public void onCancel(DialogInterface arg0) {
            cancel(true);
        }
    }

    public String generateHTMLPage(String url) {
        try {
            BufferedInputStream bis = new BufferedInputStream(new URL(url).openConnection().getInputStream());
            ByteArrayBuffer baf = new ByteArrayBuffer(50);
            while (true) {
                int current = bis.read();
                if (current == -1) {
                    String myString = new String(baf.toByteArray());
                    return myString;
                }
                baf.append((byte) current);
            }
        } catch (Exception e) {
            return null;
        }
    }

    public String parseHTMLRequest() {
        try {
            String html = generateHTMLPage("http://brandao.com/mobile_applications/application_versions.txt");
            if (html == null) {
                return null;
            }
            if (html.indexOf("<AndroidApp android:packageName=\"" + this.packageName + "\"") == -1) {
                return null;
            }
            String appInfo = html.substring(html.indexOf("<AndroidApp android:packageName=\"" + this.packageName + "\""));
            String appInfo2 = appInfo.substring(0, appInfo.indexOf("</AndroidApp>"));
            String onlineVersion = appInfo2.substring(appInfo2.indexOf("android:version=\"") + "android:version=\"".length(), appInfo2.indexOf("\"", appInfo2.indexOf("android:version=\"") + "android:version=\"".length()));
            String updateVersion = onlineVersion;
            String currentVersion = this.mContext.getPackageManager().getPackageInfo(this.packageName, 0).versionName;
            String whatsNew = "";
            if (onlineVersion.equals(currentVersion)) {
                return "no_update";
            }
            for (int i = 0; i < 3; i++) {
                if (i < 2) {
                    if (Integer.parseInt(onlineVersion.substring(0, onlineVersion.indexOf("."))) < Integer.parseInt(currentVersion.substring(0, currentVersion.indexOf(".")))) {
                        return "no_update";
                    }
                    onlineVersion = onlineVersion.substring(onlineVersion.indexOf(".") + 1);
                    currentVersion = currentVersion.substring(currentVersion.indexOf(".") + 1);
                } else if (Integer.parseInt(onlineVersion) < Integer.parseInt(currentVersion)) {
                    return "no_update";
                }
            }
            for (String features = appInfo2.substring(appInfo2.indexOf("android:updateFeatures=<(") + "android:updateFeatures=<(".length(), appInfo2.indexOf(")>")); features.indexOf("/*") != -1; features = features.substring(features.indexOf("/*") + 2)) {
                whatsNew = String.valueOf(whatsNew) + "\n" + "- " + features.substring(features.indexOf("*") + 1, features.indexOf("/*"));
            }
            return String.valueOf(this.mContext.getString(R.string.version)) + ": " + updateVersion + "\n" + whatsNew;
        } catch (Exception e) {
            return null;
        }
    }

    /* access modifiers changed from: private */
    public void showNotification() {
        this.mNotificationManager = (NotificationManager) this.mContext.getSystemService("notification");
        Notification notifyDetails = new Notification(this.mContext.getApplicationInfo().icon, this.mContext.getString(R.string.update_ready), System.currentTimeMillis());
        notifyDetails.defaults = -1;
        notifyDetails.flags = 16;
        CharSequence contentTitle = this.mContext.getString(this.mContext.getApplicationInfo().labelRes);
        CharSequence contentText = this.mContext.getString(R.string.click_here_update);
        Intent notifyIntent = new Intent("android.intent.action.VIEW");
        notifyIntent.setData(Uri.parse("market://details?id=" + this.packageName));
        notifyDetails.setLatestEventInfo(this.mContext, contentTitle, contentText, PendingIntent.getActivity(this.mContext, 0, notifyIntent, 268435456));
        this.mNotificationManager.notify(this.SIMPLE_NOTFICATION_ID, notifyDetails);
    }
}
