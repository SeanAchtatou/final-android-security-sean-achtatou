package com.brandao.apputil;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.widget.Toast;
import com.brandao.flashlight.R;
import java.io.File;
import java.util.ArrayList;

public class Share {
    public static final String TAG = Share.class.getSimpleName();

    public static void startEmailApp(String to, String title, String message, Context context) {
        Intent view = new Intent("android.intent.action.VIEW");
        StringBuilder uri = new StringBuilder("mailto:");
        uri.append(to);
        uri.append("?subject=").append(Uri.encode(title));
        uri.append("&body=").append(Uri.encode(message));
        view.setData(Uri.parse(uri.toString()));
        try {
            context.startActivity(view);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(context, (int) R.string.no_way_to_share, 0).show();
        }
    }

    public static void startSMSApp(String message, Context context) {
        Intent sendIntent = new Intent("android.intent.action.VIEW");
        sendIntent.putExtra("sms_body", message);
        sendIntent.setType("vnd.android-dir/mms-sms");
        try {
            context.startActivity(sendIntent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(context, (int) R.string.no_way_to_share, 0).show();
        }
    }

    public static String generateUserInfo(Context context) {
        String info = "=================================\nBrand: " + Build.BRAND + "\n" + "Model: " + Build.MODEL + "\n" + "Manufacturer: " + Build.MANUFACTURER + "\n" + "Display: " + Build.DISPLAY + "\n" + "OS Version: " + Build.VERSION.RELEASE + "\n" + "App Version: ";
        try {
            return String.valueOf(info) + context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            return info;
        }
    }

    public static void startShareMenu(String title, String message, Context context) {
        Intent intent = new Intent("android.intent.action.SEND");
        intent.setType("text/plain");
        intent.putExtra("android.intent.extra.TEXT", message);
        intent.putExtra("android.intent.extra.SUBJECT", title);
        try {
            context.startActivity(Intent.createChooser(intent, context.getString(R.string.share_via)));
        } catch (ActivityNotFoundException e) {
            Toast.makeText(context, (int) R.string.no_way_to_share, 0).show();
        }
    }

    public static void startEmailApp(Context context, String emailTo, String emailCC, String subject, String emailText, String filePaths) {
        Intent emailIntent = new Intent("android.intent.action.SEND_MULTIPLE");
        emailIntent.setType("plain/text");
        emailIntent.putExtra("android.intent.extra.EMAIL", new String[]{emailTo});
        emailIntent.putExtra("android.intent.extra.CC", new String[]{emailCC});
        emailIntent.putExtra("android.intent.extra.TEXT", subject);
        emailIntent.putExtra("android.intent.extra.SUBJECT", emailText);
        ArrayList<Uri> uris = new ArrayList<>();
        uris.add(Uri.fromFile(new File(filePaths)));
        emailIntent.putParcelableArrayListExtra("android.intent.extra.STREAM", uris);
        try {
            context.startActivity(Intent.createChooser(emailIntent, "Send mail..."));
        } catch (ActivityNotFoundException e) {
            Toast.makeText(context, (int) R.string.no_way_to_share, 0).show();
        }
    }
}
