package com.brandao.apputil;

public final class R {

    public static final class attr {
    }

    public static final class color {
        public static final int blue = 2131165185;
        public static final int green = 2131165186;
        public static final int red = 2131165184;
    }

    public static final class drawable {
        public static final int icon = 2130837504;
        public static final int information_menu_icon = 2130837505;
        public static final int more_apps_menu_icon = 2130837506;
        public static final int power_off_menu_icon = 2130837507;
        public static final int toast_background = 2130837508;
    }

    public static final class id {
        public static final int ad_whirl_layout_ad = 2131230733;
        public static final int basic_dialog_display = 2131230720;
        public static final int blue_flash_light_seek_bar = 2131230732;
        public static final int brightness_seek_bar = 2131230726;
        public static final int flashlight_blue_num = 2131230731;
        public static final int flashlight_brightness_num = 2131230725;
        public static final int flashlight_green_num = 2131230729;
        public static final int flashlight_light = 2131230724;
        public static final int flashlight_red_num = 2131230727;
        public static final int flurry_ad = 2131230734;
        public static final int green_flash_light_seek_bar = 2131230730;
        public static final int image = 2131230722;
        public static final int red_flash_light_seek_bar = 2131230728;
        public static final int text = 2131230723;
        public static final int toast_layout_root = 2131230721;
    }

    public static final class layout {
        public static final int basic_dialog_layout = 2130903040;
        public static final int custom_toast_layout = 2130903041;
        public static final int flashlight_view = 2130903042;
        public static final int preference_with_ad = 2130903043;
    }

    public static final class string {
        public static final int about = 2131034134;
        public static final int about_app = 2131034152;
        public static final int analytics = 2131034146;
        public static final int analytics_is_off = 2131034148;
        public static final int analytics_is_on = 2131034147;
        public static final int android = 2131034157;
        public static final int android_open_source_description = 2131034158;
        public static final int app_description_summary = 2131034161;
        public static final int app_description_title = 2131034162;
        public static final int app_name = 2131034129;
        public static final int app_version_build = 2131034154;
        public static final int app_version_description = 2131034153;
        public static final int asset_file_not_read = 2131034174;
        public static final int blue = 2131034141;
        public static final int brandao_apps_email = 2131034132;
        public static final int brightness = 2131034142;
        public static final int catalog = 2131034167;
        public static final int change_log = 2131034149;
        public static final int change_log_description = 2131034150;
        public static final int check_for_update_cancled = 2131034117;
        public static final int check_for_updates = 2131034172;
        public static final int check_for_updates_is_off = 2131034173;
        public static final int check_for_updates_is_on = 2131034171;
        public static final int checking_for_update = 2131034120;
        public static final int click_here_update = 2131034123;
        public static final int close = 2131034121;
        public static final int continue_string = 2131034128;
        public static final int copy_right = 2131034163;
        public static final int copy_right_description = 2131034164;
        public static final int exit = 2131034144;
        public static final int feedback = 2131034138;
        public static final int flurry_error = 2131034168;
        public static final int green = 2131034140;
        public static final int market = 2131034155;
        public static final int market_description = 2131034156;
        public static final int more_apps = 2131034133;
        public static final int more_apps_description = 2131034165;
        public static final int no_update_availiable = 2131034119;
        public static final int no_update_notes_description = 2131034116;
        public static final int no_way_to_share = 2131034113;
        public static final int notice = 2131034160;
        public static final int ok = 2131034145;
        public static final int open_source_licenses = 2131034159;
        public static final int other = 2131034131;
        public static final int phone_info = 2131034136;
        public static final int red = 2131034139;
        public static final int send_feedback = 2131034130;
        public static final int send_feedback_description = 2131034151;
        public static final int send_mail = 2131034137;
        public static final int settings = 2131034143;
        public static final int settings_title = 2131034127;
        public static final int share_via = 2131034112;
        public static final int short_description = 2131034135;
        public static final int sponsor_apps = 2131034169;
        public static final int sponsor_apps_description = 2131034170;
        public static final int thanks_for_downloading = 2131034166;
        public static final int unable_to_connect = 2131034126;
        public static final int unable_to_connect_title = 2131034125;
        public static final int unable_to_connect_update_check = 2131034122;
        public static final int update = 2131034118;
        public static final int update_availiable = 2131034115;
        public static final int update_ready = 2131034114;
        public static final int version = 2131034124;
    }

    public static final class style {
        public static final int custom_theme = 2131099648;
    }

    public static final class xml {
        public static final int settings = 2130968576;
    }
}
