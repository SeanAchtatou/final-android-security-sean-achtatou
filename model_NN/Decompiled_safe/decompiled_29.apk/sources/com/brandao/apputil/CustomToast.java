package com.brandao.apputil;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.brandao.flashlight.R;

public class CustomToast {
    public static final String TAG = CustomToast.class.getSimpleName();

    public CustomToast(Context context) {
    }

    public static Toast makeToast(String message, int DrawableId, Context context) {
        View layout = ((Activity) context).getLayoutInflater().inflate((int) R.layout.custom_toast_layout, (ViewGroup) ((Activity) context).findViewById(R.id.toast_layout_root));
        ((ImageView) layout.findViewById(R.id.image)).setImageResource(DrawableId);
        ((TextView) layout.findViewById(R.id.text)).setText(message);
        Toast toast = new Toast(context);
        toast.setDuration(0);
        toast.setView(layout);
        return toast;
    }

    public static Toast makeToast(String message, Context context, Toast t) {
        t.cancel();
        TextView text = new TextView(context);
        text.setText(message);
        text.setGravity(17);
        text.setBackgroundResource(R.drawable.toast_background);
        text.setPadding(16, 16, 16, 16);
        text.setTextColor(-1);
        t.setDuration(0);
        t.setView(text);
        return t;
    }

    public static Toast makeToast(String message, int DrawableId, Context context, Toast t) {
        t.cancel();
        View layout = ((Activity) context).getLayoutInflater().inflate((int) R.layout.custom_toast_layout, (ViewGroup) ((Activity) context).findViewById(R.id.toast_layout_root));
        ((ImageView) layout.findViewById(R.id.image)).setImageResource(DrawableId);
        ((TextView) layout.findViewById(R.id.text)).setText(message);
        t.setDuration(0);
        t.setView(layout);
        return t;
    }

    public static Toast makeToast(String message, Context context) {
        TextView text = new TextView(context);
        text.setText(message);
        text.setGravity(17);
        text.setBackgroundResource(R.drawable.toast_background);
        text.setPadding(16, 16, 16, 16);
        text.setTextColor(-1);
        Toast toast = new Toast(context);
        toast.setDuration(0);
        toast.setView(text);
        return toast;
    }
}
