package com.google.zxing.b.a.a.a;

import com.google.zxing.common.a;

abstract class h extends j {
    h(a aVar) {
        super(aVar);
    }

    private static void a(StringBuffer stringBuffer, int i) {
        int i2 = 0;
        for (int i3 = 0; i3 < 13; i3++) {
            int charAt = stringBuffer.charAt(i3 + i) - '0';
            if ((i3 & 1) == 0) {
                charAt *= 3;
            }
            i2 += charAt;
        }
        int i4 = 10 - (i2 % 10);
        if (i4 == 10) {
            i4 = 0;
        }
        stringBuffer.append(i4);
    }

    /* access modifiers changed from: protected */
    public void a(StringBuffer stringBuffer, int i, int i2) {
        for (int i3 = 0; i3 < 4; i3++) {
            int a2 = this.b.a((i3 * 10) + i, 10);
            if (a2 / 100 == 0) {
                stringBuffer.append('0');
            }
            if (a2 / 10 == 0) {
                stringBuffer.append('0');
            }
            stringBuffer.append(a2);
        }
        a(stringBuffer, i2);
    }

    /* access modifiers changed from: protected */
    public void b(StringBuffer stringBuffer, int i) {
        stringBuffer.append("(01)");
        int length = stringBuffer.length();
        stringBuffer.append('9');
        a(stringBuffer, i, length);
    }
}
