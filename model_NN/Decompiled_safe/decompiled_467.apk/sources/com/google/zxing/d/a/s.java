package com.google.zxing.d.a;

public final class s {

    /* renamed from: a  reason: collision with root package name */
    private final int f183a;
    private final int b;

    s(int i, int i2) {
        this.f183a = i;
        this.b = i2;
    }

    public int a() {
        return this.f183a;
    }

    public int b() {
        return this.b;
    }
}
