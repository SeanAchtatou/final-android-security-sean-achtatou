package com.google.zxing.d.b;

class f implements com.google.zxing.common.f {

    /* renamed from: a  reason: collision with root package name */
    private final float f190a;

    public f(float f) {
        this.f190a = f;
    }

    public int a(Object obj, Object obj2) {
        if (((d) obj2).d() != ((d) obj).d()) {
            return ((d) obj2).d() - ((d) obj).d();
        }
        float abs = Math.abs(((d) obj2).c() - this.f190a);
        float abs2 = Math.abs(((d) obj).c() - this.f190a);
        if (abs < abs2) {
            return 1;
        }
        return abs == abs2 ? 0 : -1;
    }
}
