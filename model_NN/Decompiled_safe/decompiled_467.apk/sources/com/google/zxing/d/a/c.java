package com.google.zxing.d.a;

import com.google.zxing.common.b;

abstract class c {

    /* renamed from: a  reason: collision with root package name */
    private static final c[] f176a = {new e(null), new f(null), new g(null), new h(null), new i(null), new j(null), new k(null), new l(null)};

    private c() {
    }

    c(d dVar) {
        this();
    }

    static c a(int i) {
        if (i >= 0 && i <= 7) {
            return f176a[i];
        }
        throw new IllegalArgumentException();
    }

    /* access modifiers changed from: package-private */
    public final void a(b bVar, int i) {
        for (int i2 = 0; i2 < i; i2++) {
            for (int i3 = 0; i3 < i; i3++) {
                if (a(i2, i3)) {
                    bVar.c(i3, i2);
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public abstract boolean a(int i, int i2);
}
