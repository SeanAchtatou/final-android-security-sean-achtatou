package com.tencent.connector.component;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.connector.ConnectionActivity;

/* compiled from: ProGuard */
public class ContentConnectionInitLogined extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    private Context f3521a;
    /* access modifiers changed from: private */
    public ConnectionActivity b;
    private RoundCornerImageView c;
    private TextView d;
    private Button e;
    private TextView f;
    private Button g;
    private View.OnClickListener h;
    private View.OnClickListener i;
    private View.OnClickListener j;

    public ContentConnectionInitLogined(Context context) {
        super(context);
        this.f3521a = context;
    }

    public ContentConnectionInitLogined(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f3521a = context;
    }

    public ContentConnectionInitLogined(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.f3521a = context;
    }

    public void setHostActivity(ConnectionActivity connectionActivity) {
        this.b = connectionActivity;
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        this.c = (RoundCornerImageView) findViewById(R.id.avatar);
        this.d = (TextView) findViewById(R.id.nickname);
        this.e = (Button) findViewById(R.id.connect_pc_now);
        this.f = (TextView) findViewById(R.id.switch_account);
        this.g = (Button) findViewById(R.id.scan);
        this.h = new b(this);
        this.i = new c(this);
        this.j = new d(this);
        if (this.e != null) {
            this.e.setOnClickListener(this.h);
        }
        if (this.f != null) {
            this.f.setOnClickListener(this.i);
        }
        if (this.g != null) {
            this.g.setOnClickListener(this.j);
        }
    }

    public void displayInit(String str, String str2) {
        String str3;
        if (this.d != null) {
            this.d.setText(str2);
        }
        if (this.c != null && str != null) {
            String replace = str.replace("s=40", "s=100");
            StringBuilder append = new StringBuilder().append("displayInit: ");
            if (TextUtils.isEmpty(replace)) {
                str3 = "null";
            } else {
                str3 = replace;
            }
            Log.d("Longer", append.append(str3).toString());
            this.c.updateImageView(replace, R.drawable.common_icon_qq, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
        }
    }
}
