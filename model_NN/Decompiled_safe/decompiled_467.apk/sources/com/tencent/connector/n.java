package com.tencent.connector;

import android.view.View;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
class n implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ UsbDebugModeAlertActivity f3557a;

    n(UsbDebugModeAlertActivity usbDebugModeAlertActivity) {
        this.f3557a = usbDebugModeAlertActivity;
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cancel /*2131165467*/:
                this.f3557a.finish();
                return;
            case R.id.ok /*2131165468*/:
                this.f3557a.b();
                return;
            default:
                return;
        }
    }
}
