package com.tencent.connector.a;

import android.content.Context;
import android.os.Handler;
import com.qq.l.p;
import com.qq.m.d;
import com.qq.m.e;
import com.tencent.assistant.model.OnlinePCListItemModel;
import com.tencent.assistant.st.a;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.wcs.jce.PCInfo;
import com.tencent.wcs.jce.PingPCSResponse;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class b implements e {

    /* renamed from: a  reason: collision with root package name */
    private Context f3514a;
    private Handler b;
    private long c;

    public b(Context context, Handler handler, long j) {
        this.f3514a = context;
        this.b = handler;
        this.c = j;
    }

    public void a() {
        p.m().b(602, 0, -1);
        if (this.c > 0) {
            TemporaryThreadManager.get().start(new d(this.f3514a, this.c, this));
        } else {
            TemporaryThreadManager.get().start(new d(this.f3514a, this));
        }
    }

    public void b() {
        this.f3514a = null;
        this.b = null;
    }

    public void a(int i, int i2, PingPCSResponse pingPCSResponse) {
        ArrayList arrayList = new ArrayList();
        if (i == 200 && pingPCSResponse != null) {
            arrayList.addAll(a(pingPCSResponse.a()));
            if (pingPCSResponse.a() == null || pingPCSResponse.a().isEmpty()) {
                p.m().c(602, 3, 3);
            } else {
                p.m().c(602, 1, -1);
            }
        } else if (i == 0) {
            p.m().c(602, 2, 1);
        } else {
            p.m().c(602, 2, 2);
        }
        a.a().a((short) arrayList.size());
        if (arrayList.isEmpty()) {
            if (this.b != null) {
                this.b.sendEmptyMessage(30);
            }
        } else if (this.b != null) {
            this.b.sendMessage(this.b.obtainMessage(31, arrayList.get(0)));
        }
    }

    private List<OnlinePCListItemModel> a(List<PCInfo> list) {
        ArrayList arrayList = new ArrayList();
        if (list == null || list.isEmpty()) {
            return arrayList;
        }
        for (PCInfo next : list) {
            if (next != null) {
                next.c = 69904;
                arrayList.add(new OnlinePCListItemModel(next));
            }
        }
        return arrayList;
    }
}
