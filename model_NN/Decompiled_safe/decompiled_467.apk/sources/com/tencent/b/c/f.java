package com.tencent.b.c;

import android.content.Context;
import com.tencent.b.b.b;
import com.tencent.b.d.k;

public abstract class f {

    /* renamed from: a  reason: collision with root package name */
    protected Context f3373a = null;

    protected f(Context context) {
        this.f3373a = context;
    }

    public static String d() {
        return k.d("6X8Y4XdM2Vhvn0I=");
    }

    private void d(String str) {
        if (a()) {
            a(b(str));
        }
    }

    public static String e() {
        return k.d("6X8Y4XdM2Vhvn0KfzcEatGnWaNU=");
    }

    public static String f() {
        return k.d("4kU71lN96TJUomD1vOU9lgj9U+kKmxDPLVM+zzjst5U=");
    }

    private String j() {
        if (a()) {
            return c(b());
        }
        return null;
    }

    public void a(b bVar) {
        if (bVar != null) {
            d(bVar.toString());
        }
    }

    /* access modifiers changed from: protected */
    public abstract void a(a aVar);

    /* access modifiers changed from: protected */
    public abstract void a(String str);

    /* access modifiers changed from: protected */
    public abstract boolean a();

    /* access modifiers changed from: protected */
    public abstract String b();

    /* access modifiers changed from: protected */
    public String b(String str) {
        return k.e(str);
    }

    public void b(a aVar) {
        if (aVar != null && a()) {
            a(aVar);
        }
    }

    /* access modifiers changed from: protected */
    public abstract a c();

    /* access modifiers changed from: protected */
    public String c(String str) {
        return k.d(str);
    }

    public b g() {
        String j = j();
        if (j != null) {
            return b.a(j);
        }
        return null;
    }

    public a h() {
        if (a()) {
            return c();
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public String i() {
        return k.d("4kU71lN96TJUomD1vOU9lgj9Tw==");
    }
}
