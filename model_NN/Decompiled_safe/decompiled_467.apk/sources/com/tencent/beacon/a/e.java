package com.tencent.beacon.a;

import android.util.SparseArray;
import com.tencent.beacon.d.a;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/* compiled from: ProGuard */
final class e extends d {
    private ScheduledExecutorService b;
    private SparseArray<ScheduledFuture<?>> c;
    private boolean d;

    public e() {
        this.b = null;
        this.c = null;
        this.d = false;
        this.b = Executors.newScheduledThreadPool(3);
        this.c = new SparseArray<>();
    }

    public final synchronized void a(Runnable runnable) {
        if (runnable == null) {
            a.d("task runner should not be null", new Object[0]);
        } else {
            this.b.execute(runnable);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.beacon.a.e.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.tencent.beacon.a.e.a(java.lang.Runnable, long):void
      com.tencent.beacon.a.d.a(java.lang.Runnable, long):void
      com.tencent.beacon.a.e.a(int, boolean):void */
    public final synchronized void a(int i, Runnable runnable, long j, long j2) {
        long j3;
        long j4 = 0;
        synchronized (this) {
            if (runnable == null) {
                a.d("task runner should not be null", new Object[0]);
            } else {
                if (j > 0) {
                    j4 = j;
                }
                if (f3396a) {
                    if (j2 <= 10000) {
                        j2 = 10000;
                    }
                    j3 = j2;
                } else {
                    j3 = j2;
                }
                a(i, true);
                ScheduledFuture<?> scheduleAtFixedRate = this.b.scheduleAtFixedRate(runnable, j4, j3, TimeUnit.MILLISECONDS);
                if (scheduleAtFixedRate != null) {
                    a.b("add a new future! taskId: %d , periodTime: %d", Integer.valueOf(i), Long.valueOf(j3));
                    this.c.put(i, scheduleAtFixedRate);
                }
            }
        }
    }

    public final synchronized void a(int i, boolean z) {
        ScheduledFuture scheduledFuture = this.c.get(i);
        if (scheduledFuture != null && !scheduledFuture.isCancelled()) {
            a.b("cancel a old future!", new Object[0]);
            scheduledFuture.cancel(true);
        }
        this.c.remove(i);
    }

    public final synchronized void a(Runnable runnable, long j) {
        if (runnable == null) {
            a.d("task runner should not be null", new Object[0]);
        } else {
            if (j <= 0) {
                j = 0;
            }
            this.b.schedule(runnable, j, TimeUnit.MILLISECONDS);
        }
    }
}
