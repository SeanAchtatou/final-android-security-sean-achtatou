package com.tencent.beacon.a.a;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private long f3381a = -1;
    private int b = -1;
    private int c = -1;
    private long d = -1;
    private byte[] e = null;
    private long f = 0;

    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00b7, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00d2, code lost:
        r10.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00df, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00e0, code lost:
        r9 = r0;
        r0 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00e8, code lost:
        r1 = r10;
        r12 = r0;
        r0 = null;
        r9 = r12;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00b7  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00d2  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00df A[ExcHandler: all (r1v10 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:8:0x0027] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.List a(android.content.Context r13) {
        /*
            r11 = 0
            r9 = 0
            java.lang.String r0 = " AnalyticsDAO.queryReqData() start"
            java.lang.Object[] r1 = new java.lang.Object[r11]
            com.tencent.beacon.d.a.b(r0, r1)
            if (r13 != 0) goto L_0x0014
            java.lang.String r0 = " AnalyticsDAO.queryReqData() context null "
            java.lang.Object[] r1 = new java.lang.Object[r11]
            com.tencent.beacon.d.a.d(r0, r1)
            r0 = r9
        L_0x0013:
            return r0
        L_0x0014:
            com.tencent.beacon.a.a.c r10 = com.tencent.beacon.a.a.c.a(r13)     // Catch:{ Exception -> 0x00a7, all -> 0x00c3 }
            android.database.sqlite.SQLiteDatabase r0 = r10.getWritableDatabase()     // Catch:{ Exception -> 0x00e3, all -> 0x00dd }
            java.lang.String r1 = "t_req_data"
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            java.lang.String r7 = "_time DESC "
            java.lang.String r8 = "1"
            android.database.Cursor r2 = r0.query(r1, r2, r3, r4, r5, r6, r7, r8)     // Catch:{ Exception -> 0x00e7, all -> 0x00df }
            boolean r1 = r2.moveToNext()     // Catch:{ Exception -> 0x00e7, all -> 0x00df }
            if (r1 == 0) goto L_0x00f2
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ Exception -> 0x00e7, all -> 0x00df }
            r1.<init>()     // Catch:{ Exception -> 0x00e7, all -> 0x00df }
            java.lang.String r3 = "_id"
            int r3 = r2.getColumnIndex(r3)     // Catch:{ Exception -> 0x00ed, all -> 0x00df }
            int r3 = r2.getInt(r3)     // Catch:{ Exception -> 0x00ed, all -> 0x00df }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x00ed, all -> 0x00df }
            r1.add(r3)     // Catch:{ Exception -> 0x00ed, all -> 0x00df }
            java.lang.String r3 = "_rid"
            int r3 = r2.getColumnIndex(r3)     // Catch:{ Exception -> 0x00ed, all -> 0x00df }
            java.lang.String r3 = r2.getString(r3)     // Catch:{ Exception -> 0x00ed, all -> 0x00df }
            r1.add(r3)     // Catch:{ Exception -> 0x00ed, all -> 0x00df }
            java.lang.String r3 = "_time"
            int r3 = r2.getColumnIndex(r3)     // Catch:{ Exception -> 0x00ed, all -> 0x00df }
            long r3 = r2.getLong(r3)     // Catch:{ Exception -> 0x00ed, all -> 0x00df }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ Exception -> 0x00ed, all -> 0x00df }
            r1.add(r3)     // Catch:{ Exception -> 0x00ed, all -> 0x00df }
            java.lang.String r3 = "_datas"
            int r3 = r2.getColumnIndex(r3)     // Catch:{ Exception -> 0x00ed, all -> 0x00df }
            byte[] r2 = r2.getBlob(r3)     // Catch:{ Exception -> 0x00ed, all -> 0x00df }
            r1.add(r2)     // Catch:{ Exception -> 0x00ed, all -> 0x00df }
        L_0x0072:
            if (r1 == 0) goto L_0x008d
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00ed, all -> 0x00df }
            java.lang.String r3 = " total num: "
            r2.<init>(r3)     // Catch:{ Exception -> 0x00ed, all -> 0x00df }
            int r3 = r1.size()     // Catch:{ Exception -> 0x00ed, all -> 0x00df }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x00ed, all -> 0x00df }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x00ed, all -> 0x00df }
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Exception -> 0x00ed, all -> 0x00df }
            com.tencent.beacon.d.a.a(r2, r3)     // Catch:{ Exception -> 0x00ed, all -> 0x00df }
        L_0x008d:
            if (r0 == 0) goto L_0x0098
            boolean r2 = r0.isOpen()
            if (r2 == 0) goto L_0x0098
            r0.close()
        L_0x0098:
            if (r10 == 0) goto L_0x009d
            r10.close()
        L_0x009d:
            java.lang.String r0 = " AnalyticsDAO.queryReqData() end"
            java.lang.Object[] r2 = new java.lang.Object[r11]
            com.tencent.beacon.d.a.b(r0, r2)
            r0 = r1
            goto L_0x0013
        L_0x00a7:
            r0 = move-exception
            r0 = r9
            r1 = r9
        L_0x00aa:
            if (r9 == 0) goto L_0x00b5
            boolean r2 = r9.isOpen()
            if (r2 == 0) goto L_0x00b5
            r9.close()
        L_0x00b5:
            if (r1 == 0) goto L_0x00ba
            r1.close()
        L_0x00ba:
            java.lang.String r1 = " AnalyticsDAO.queryReqData() end"
            java.lang.Object[] r2 = new java.lang.Object[r11]
            com.tencent.beacon.d.a.b(r1, r2)
            goto L_0x0013
        L_0x00c3:
            r0 = move-exception
            r10 = r9
        L_0x00c5:
            if (r9 == 0) goto L_0x00d0
            boolean r1 = r9.isOpen()
            if (r1 == 0) goto L_0x00d0
            r9.close()
        L_0x00d0:
            if (r10 == 0) goto L_0x00d5
            r10.close()
        L_0x00d5:
            java.lang.String r1 = " AnalyticsDAO.queryReqData() end"
            java.lang.Object[] r2 = new java.lang.Object[r11]
            com.tencent.beacon.d.a.b(r1, r2)
            throw r0
        L_0x00dd:
            r0 = move-exception
            goto L_0x00c5
        L_0x00df:
            r1 = move-exception
            r9 = r0
            r0 = r1
            goto L_0x00c5
        L_0x00e3:
            r0 = move-exception
            r0 = r9
            r1 = r10
            goto L_0x00aa
        L_0x00e7:
            r1 = move-exception
            r1 = r10
            r12 = r0
            r0 = r9
            r9 = r12
            goto L_0x00aa
        L_0x00ed:
            r2 = move-exception
            r9 = r0
            r0 = r1
            r1 = r10
            goto L_0x00aa
        L_0x00f2:
            r1 = r9
            goto L_0x0072
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.beacon.a.a.a.a(android.content.Context):java.util.List");
    }

    public a() {
    }

    public a(int i, int i2, long j, byte[] bArr) {
        this.b = i;
        this.c = i2;
        this.d = j;
        this.e = bArr;
        if (bArr != null) {
            this.f = (long) bArr.length;
        }
    }

    public final long a() {
        return this.f3381a;
    }

    public final a a(long j) {
        this.f3381a = j;
        return this;
    }

    /* JADX WARNING: Removed duplicated region for block: B:41:0x00a6  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean a(android.content.Context r9, byte[] r10, java.lang.String r11) {
        /*
            r0 = 0
            r1 = 0
            java.lang.String r2 = " AnalyticsDAO.insertReqData() start"
            java.lang.Object[] r3 = new java.lang.Object[r1]
            com.tencent.beacon.d.a.b(r2, r3)
            if (r9 == 0) goto L_0x000f
            if (r10 == 0) goto L_0x000f
            if (r11 != 0) goto L_0x0018
        L_0x000f:
            java.lang.String r0 = " AnalyticsDAO.insertReqData() have null args"
            java.lang.Object[] r2 = new java.lang.Object[r1]
            com.tencent.beacon.d.a.d(r0, r2)
            r0 = r1
        L_0x0017:
            return r0
        L_0x0018:
            com.tencent.beacon.a.a.c r3 = com.tencent.beacon.a.a.c.a(r9)     // Catch:{ Throwable -> 0x0071, all -> 0x0094 }
            android.database.sqlite.SQLiteDatabase r2 = r3.getWritableDatabase()     // Catch:{ Throwable -> 0x00be, all -> 0x00b1 }
            android.content.ContentValues r0 = new android.content.ContentValues     // Catch:{ Throwable -> 0x00c1, all -> 0x00b6 }
            r0.<init>()     // Catch:{ Throwable -> 0x00c1, all -> 0x00b6 }
            java.lang.String r4 = "_rid"
            r0.put(r4, r11)     // Catch:{ Throwable -> 0x00c1, all -> 0x00b6 }
            java.lang.String r4 = "_time"
            java.util.Date r5 = new java.util.Date     // Catch:{ Throwable -> 0x00c1, all -> 0x00b6 }
            r5.<init>()     // Catch:{ Throwable -> 0x00c1, all -> 0x00b6 }
            long r5 = r5.getTime()     // Catch:{ Throwable -> 0x00c1, all -> 0x00b6 }
            java.lang.Long r5 = java.lang.Long.valueOf(r5)     // Catch:{ Throwable -> 0x00c1, all -> 0x00b6 }
            r0.put(r4, r5)     // Catch:{ Throwable -> 0x00c1, all -> 0x00b6 }
            java.lang.String r4 = "_datas"
            r0.put(r4, r10)     // Catch:{ Throwable -> 0x00c1, all -> 0x00b6 }
            java.lang.String r4 = "t_req_data"
            r5 = 0
            long r4 = r2.insert(r4, r5, r0)     // Catch:{ Throwable -> 0x00c1, all -> 0x00b6 }
            r6 = 0
            int r0 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r0 >= 0) goto L_0x006f
            java.lang.String r0 = " AnalyticsDAO.insertReqData() failure! return"
            r4 = 0
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Throwable -> 0x00c1, all -> 0x00b6 }
            com.tencent.beacon.d.a.d(r0, r4)     // Catch:{ Throwable -> 0x00c1, all -> 0x00b6 }
            r0 = r1
        L_0x0057:
            if (r2 == 0) goto L_0x0062
            boolean r4 = r2.isOpen()
            if (r4 == 0) goto L_0x0062
            r2.close()
        L_0x0062:
            if (r3 == 0) goto L_0x0067
            r3.close()
        L_0x0067:
            java.lang.String r2 = " AnalyticsDAO.insertReqData() end"
            java.lang.Object[] r1 = new java.lang.Object[r1]
            com.tencent.beacon.d.a.b(r2, r1)
            goto L_0x0017
        L_0x006f:
            r0 = 1
            goto L_0x0057
        L_0x0071:
            r2 = move-exception
            r2 = r0
        L_0x0073:
            java.lang.String r3 = "AnalyticsDAO.insertReqData() failure!"
            r4 = 0
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x00b8 }
            com.tencent.beacon.d.a.b(r3, r4)     // Catch:{ all -> 0x00b8 }
            if (r0 == 0) goto L_0x0086
            boolean r3 = r0.isOpen()
            if (r3 == 0) goto L_0x0086
            r0.close()
        L_0x0086:
            if (r2 == 0) goto L_0x008b
            r2.close()
        L_0x008b:
            java.lang.String r0 = " AnalyticsDAO.insertReqData() end"
            java.lang.Object[] r2 = new java.lang.Object[r1]
            com.tencent.beacon.d.a.b(r0, r2)
            r0 = r1
            goto L_0x0017
        L_0x0094:
            r2 = move-exception
            r3 = r0
            r8 = r0
            r0 = r2
            r2 = r8
        L_0x0099:
            if (r2 == 0) goto L_0x00a4
            boolean r4 = r2.isOpen()
            if (r4 == 0) goto L_0x00a4
            r2.close()
        L_0x00a4:
            if (r3 == 0) goto L_0x00a9
            r3.close()
        L_0x00a9:
            java.lang.String r2 = " AnalyticsDAO.insertReqData() end"
            java.lang.Object[] r1 = new java.lang.Object[r1]
            com.tencent.beacon.d.a.b(r2, r1)
            throw r0
        L_0x00b1:
            r2 = move-exception
            r8 = r2
            r2 = r0
            r0 = r8
            goto L_0x0099
        L_0x00b6:
            r0 = move-exception
            goto L_0x0099
        L_0x00b8:
            r3 = move-exception
            r8 = r3
            r3 = r2
            r2 = r0
            r0 = r8
            goto L_0x0099
        L_0x00be:
            r2 = move-exception
            r2 = r3
            goto L_0x0073
        L_0x00c1:
            r0 = move-exception
            r0 = r2
            r2 = r3
            goto L_0x0073
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.beacon.a.a.a.a(android.content.Context, byte[], java.lang.String):boolean");
    }

    public final byte[] b() {
        return this.e;
    }

    /* JADX WARNING: Removed duplicated region for block: B:38:0x00be  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean a(android.content.Context r7, java.lang.String r8) {
        /*
            r0 = 1
            r2 = 0
            r1 = 0
            java.lang.String r3 = " AnalyticsDAO.deleteReqData() start"
            java.lang.Object[] r4 = new java.lang.Object[r1]
            com.tencent.beacon.d.a.b(r3, r4)
            if (r7 != 0) goto L_0x0014
            java.lang.String r0 = " delete() context is null arg"
            java.lang.Object[] r2 = new java.lang.Object[r1]
            com.tencent.beacon.d.a.a(r0, r2)
        L_0x0013:
            return r1
        L_0x0014:
            if (r8 == 0) goto L_0x0013
            java.lang.String r3 = r8.trim()
            java.lang.String r4 = ""
            boolean r3 = r3.equals(r4)
            if (r3 != 0) goto L_0x0013
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "_rid = '"
            r3.<init>(r4)
            java.lang.StringBuilder r3 = r3.append(r8)
            java.lang.String r4 = "' "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r4 = r3.toString()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r5 = " delete where: "
            r3.<init>(r5)
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            java.lang.Object[] r5 = new java.lang.Object[r1]
            com.tencent.beacon.d.a.b(r3, r5)
            com.tencent.beacon.a.a.c r3 = com.tencent.beacon.a.a.c.a(r7)     // Catch:{ Throwable -> 0x008a, all -> 0x00af }
            android.database.sqlite.SQLiteDatabase r2 = r3.getWritableDatabase()     // Catch:{ Throwable -> 0x00cb }
            java.lang.String r5 = "t_req_data"
            r6 = 0
            int r4 = r2.delete(r5, r4, r6)     // Catch:{ Throwable -> 0x00cb }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00cb }
            java.lang.String r6 = " deleted num: "
            r5.<init>(r6)     // Catch:{ Throwable -> 0x00cb }
            java.lang.StringBuilder r5 = r5.append(r4)     // Catch:{ Throwable -> 0x00cb }
            java.lang.String r5 = r5.toString()     // Catch:{ Throwable -> 0x00cb }
            r6 = 0
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ Throwable -> 0x00cb }
            com.tencent.beacon.d.a.b(r5, r6)     // Catch:{ Throwable -> 0x00cb }
            if (r4 != r0) goto L_0x00cd
        L_0x0071:
            if (r2 == 0) goto L_0x007c
            boolean r4 = r2.isOpen()
            if (r4 == 0) goto L_0x007c
            r2.close()
        L_0x007c:
            if (r3 == 0) goto L_0x0081
            r3.close()
        L_0x0081:
            java.lang.String r2 = " AnalyticsDAO.deleteReqData() end"
            java.lang.Object[] r1 = new java.lang.Object[r1]
            com.tencent.beacon.d.a.b(r2, r1)
        L_0x0088:
            r1 = r0
            goto L_0x0013
        L_0x008a:
            r0 = move-exception
            r3 = r2
        L_0x008c:
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x00c9 }
            r4 = 0
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x00c9 }
            com.tencent.beacon.d.a.b(r0, r4)     // Catch:{ all -> 0x00c9 }
            if (r2 == 0) goto L_0x00a1
            boolean r0 = r2.isOpen()
            if (r0 == 0) goto L_0x00a1
            r2.close()
        L_0x00a1:
            if (r3 == 0) goto L_0x00a6
            r3.close()
        L_0x00a6:
            java.lang.String r0 = " AnalyticsDAO.deleteReqData() end"
            java.lang.Object[] r2 = new java.lang.Object[r1]
            com.tencent.beacon.d.a.b(r0, r2)
            r0 = r1
            goto L_0x0088
        L_0x00af:
            r0 = move-exception
            r3 = r2
        L_0x00b1:
            if (r2 == 0) goto L_0x00bc
            boolean r4 = r2.isOpen()
            if (r4 == 0) goto L_0x00bc
            r2.close()
        L_0x00bc:
            if (r3 == 0) goto L_0x00c1
            r3.close()
        L_0x00c1:
            java.lang.String r2 = " AnalyticsDAO.deleteReqData() end"
            java.lang.Object[] r1 = new java.lang.Object[r1]
            com.tencent.beacon.d.a.b(r2, r1)
            throw r0
        L_0x00c9:
            r0 = move-exception
            goto L_0x00b1
        L_0x00cb:
            r0 = move-exception
            goto L_0x008c
        L_0x00cd:
            r0 = r1
            goto L_0x0071
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.beacon.a.a.a.a(android.content.Context, java.lang.String):boolean");
    }

    /* JADX WARNING: Removed duplicated region for block: B:47:0x00b7  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean a(android.content.Context r11, java.util.List<com.tencent.beacon.a.a.a> r12) {
        /*
            r0 = 0
            r1 = 1
            r2 = 0
            java.lang.String r3 = " AnalyticsDAO.insert() start"
            java.lang.Object[] r4 = new java.lang.Object[r2]
            com.tencent.beacon.d.a.b(r3, r4)
            if (r11 == 0) goto L_0x000e
            if (r12 != 0) goto L_0x0017
        L_0x000e:
            java.lang.String r0 = " AnalyticsDAO.insert() have null args"
            java.lang.Object[] r1 = new java.lang.Object[r2]
            com.tencent.beacon.d.a.d(r0, r1)
            r1 = r2
        L_0x0016:
            return r1
        L_0x0017:
            int r3 = r12.size()
            if (r3 > 0) goto L_0x0025
            java.lang.String r0 = " list size == 0 return true"
            java.lang.Object[] r2 = new java.lang.Object[r2]
            com.tencent.beacon.d.a.b(r0, r2)
            goto L_0x0016
        L_0x0025:
            com.tencent.beacon.a.a.c r4 = com.tencent.beacon.a.a.c.a(r11)     // Catch:{ Throwable -> 0x007d, all -> 0x00a3 }
            android.database.sqlite.SQLiteDatabase r3 = r4.getWritableDatabase()     // Catch:{ Throwable -> 0x00ce, all -> 0x00c2 }
            r3.beginTransaction()     // Catch:{ Throwable -> 0x00d1, all -> 0x00c6 }
            r5 = r2
        L_0x0031:
            int r0 = r12.size()     // Catch:{ Throwable -> 0x00d1, all -> 0x00c6 }
            if (r5 >= r0) goto L_0x005d
            java.lang.Object r0 = r12.get(r5)     // Catch:{ Throwable -> 0x00d1, all -> 0x00c6 }
            com.tencent.beacon.a.a.a r0 = (com.tencent.beacon.a.a.a) r0     // Catch:{ Throwable -> 0x00d1, all -> 0x00c6 }
            android.content.ContentValues r6 = a(r0)     // Catch:{ Throwable -> 0x00d1, all -> 0x00c6 }
            java.lang.String r7 = "t_event"
            java.lang.String r8 = "_id"
            long r6 = r3.insert(r7, r8, r6)     // Catch:{ Throwable -> 0x00d1, all -> 0x00c6 }
            r8 = 0
            int r8 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r8 >= 0) goto L_0x0057
            java.lang.String r8 = " AnalyticsDAO.insert() failure! return"
            r9 = 0
            java.lang.Object[] r9 = new java.lang.Object[r9]     // Catch:{ Throwable -> 0x00d1, all -> 0x00c6 }
            com.tencent.beacon.d.a.d(r8, r9)     // Catch:{ Throwable -> 0x00d1, all -> 0x00c6 }
        L_0x0057:
            r0.f3381a = r6     // Catch:{ Throwable -> 0x00d1, all -> 0x00c6 }
            int r0 = r5 + 1
            r5 = r0
            goto L_0x0031
        L_0x005d:
            r3.setTransactionSuccessful()     // Catch:{ Throwable -> 0x00d1, all -> 0x00c6 }
            if (r3 == 0) goto L_0x006e
            boolean r0 = r3.isOpen()
            if (r0 == 0) goto L_0x006e
            r3.endTransaction()
            r3.close()
        L_0x006e:
            if (r4 == 0) goto L_0x0073
            r4.close()
        L_0x0073:
            java.lang.String r0 = " AnalyticsDAO.insert() end"
            java.lang.Object[] r2 = new java.lang.Object[r2]
            com.tencent.beacon.d.a.b(r0, r2)
            r0 = r1
        L_0x007b:
            r1 = r0
            goto L_0x0016
        L_0x007d:
            r1 = move-exception
            r1 = r0
        L_0x007f:
            java.lang.String r3 = "AnalyticsDAO.insert() failure!"
            r4 = 0
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x00c8 }
            com.tencent.beacon.d.a.b(r3, r4)     // Catch:{ all -> 0x00c8 }
            if (r0 == 0) goto L_0x0095
            boolean r3 = r0.isOpen()
            if (r3 == 0) goto L_0x0095
            r0.endTransaction()
            r0.close()
        L_0x0095:
            if (r1 == 0) goto L_0x009a
            r1.close()
        L_0x009a:
            java.lang.String r0 = " AnalyticsDAO.insert() end"
            java.lang.Object[] r1 = new java.lang.Object[r2]
            com.tencent.beacon.d.a.b(r0, r1)
            r0 = r2
            goto L_0x007b
        L_0x00a3:
            r1 = move-exception
            r3 = r0
            r4 = r0
            r0 = r1
        L_0x00a7:
            if (r3 == 0) goto L_0x00b5
            boolean r1 = r3.isOpen()
            if (r1 == 0) goto L_0x00b5
            r3.endTransaction()
            r3.close()
        L_0x00b5:
            if (r4 == 0) goto L_0x00ba
            r4.close()
        L_0x00ba:
            java.lang.String r1 = " AnalyticsDAO.insert() end"
            java.lang.Object[] r2 = new java.lang.Object[r2]
            com.tencent.beacon.d.a.b(r1, r2)
            throw r0
        L_0x00c2:
            r1 = move-exception
            r3 = r0
            r0 = r1
            goto L_0x00a7
        L_0x00c6:
            r0 = move-exception
            goto L_0x00a7
        L_0x00c8:
            r3 = move-exception
            r4 = r1
            r10 = r0
            r0 = r3
            r3 = r10
            goto L_0x00a7
        L_0x00ce:
            r1 = move-exception
            r1 = r4
            goto L_0x007f
        L_0x00d1:
            r0 = move-exception
            r0 = r3
            r1 = r4
            goto L_0x007f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.beacon.a.a.a.a(android.content.Context, java.util.List):boolean");
    }

    /* JADX WARNING: Removed duplicated region for block: B:40:0x00ee  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x010a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int a(android.content.Context r7, int[] r8, long r9, long r11) {
        /*
            r4 = -1
            r5 = 0
            r1 = 0
            java.lang.String r0 = " AnalyticsDAO.delete() start"
            java.lang.Object[] r2 = new java.lang.Object[r1]
            com.tencent.beacon.d.a.b(r0, r2)
            if (r7 != 0) goto L_0x0014
            java.lang.String r0 = " delete() context is null arg"
            java.lang.Object[] r1 = new java.lang.Object[r1]
            com.tencent.beacon.d.a.a(r0, r1)
        L_0x0013:
            return r4
        L_0x0014:
            int r0 = (r9 > r11 ? 1 : (r9 == r11 ? 0 : -1))
            if (r0 <= 0) goto L_0x001a
            r4 = r1
            goto L_0x0013
        L_0x001a:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r2 = "_time >= "
            r0.<init>(r2)
            java.lang.StringBuilder r0 = r0.append(r9)
            java.lang.String r2 = " and _time"
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r2 = " <= "
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.StringBuilder r0 = r0.append(r11)
            java.lang.String r3 = r0.toString()
            if (r8 == 0) goto L_0x0124
            int r0 = r8.length
            if (r0 <= 0) goto L_0x0124
            java.lang.String r0 = ""
            r2 = r0
            r0 = r1
        L_0x0042:
            int r6 = r8.length
            if (r0 >= r6) goto L_0x0061
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.StringBuilder r2 = r6.append(r2)
            java.lang.String r6 = " or _type = "
            java.lang.StringBuilder r2 = r2.append(r6)
            r6 = r8[r0]
            java.lang.StringBuilder r2 = r2.append(r6)
            java.lang.String r2 = r2.toString()
            int r0 = r0 + 1
            goto L_0x0042
        L_0x0061:
            r0 = 4
            java.lang.String r0 = r2.substring(r0)
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = " and ( "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r2 = " )"
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
        L_0x0083:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = " delete where: "
            r2.<init>(r3)
            java.lang.StringBuilder r2 = r2.append(r0)
            java.lang.String r2 = r2.toString()
            java.lang.Object[] r3 = new java.lang.Object[r1]
            com.tencent.beacon.d.a.b(r2, r3)
            com.tencent.beacon.a.a.c r3 = com.tencent.beacon.a.a.c.a(r7)     // Catch:{ Throwable -> 0x00d5, all -> 0x00fa }
            android.database.sqlite.SQLiteDatabase r2 = r3.getWritableDatabase()     // Catch:{ Throwable -> 0x011d, all -> 0x0115 }
            java.lang.String r5 = "t_event"
            r6 = 0
            int r0 = r2.delete(r5, r0, r6)     // Catch:{ Throwable -> 0x0121, all -> 0x0118 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0121, all -> 0x0118 }
            java.lang.String r6 = " deleted num: "
            r5.<init>(r6)     // Catch:{ Throwable -> 0x0121, all -> 0x0118 }
            java.lang.StringBuilder r5 = r5.append(r0)     // Catch:{ Throwable -> 0x0121, all -> 0x0118 }
            java.lang.String r5 = r5.toString()     // Catch:{ Throwable -> 0x0121, all -> 0x0118 }
            r6 = 0
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ Throwable -> 0x0121, all -> 0x0118 }
            com.tencent.beacon.d.a.b(r5, r6)     // Catch:{ Throwable -> 0x0121, all -> 0x0118 }
            if (r2 == 0) goto L_0x00c6
            boolean r4 = r2.isOpen()
            if (r4 == 0) goto L_0x00c6
            r2.close()
        L_0x00c6:
            if (r3 == 0) goto L_0x00cb
            r3.close()
        L_0x00cb:
            java.lang.String r2 = " AnalyticsDAO.delete() end"
            java.lang.Object[] r1 = new java.lang.Object[r1]
            com.tencent.beacon.d.a.b(r2, r1)
        L_0x00d2:
            r4 = r0
            goto L_0x0013
        L_0x00d5:
            r0 = move-exception
            r2 = r5
        L_0x00d7:
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x011a }
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ all -> 0x011a }
            com.tencent.beacon.d.a.b(r0, r3)     // Catch:{ all -> 0x011a }
            if (r2 == 0) goto L_0x00ec
            boolean r0 = r2.isOpen()
            if (r0 == 0) goto L_0x00ec
            r2.close()
        L_0x00ec:
            if (r5 == 0) goto L_0x00f1
            r5.close()
        L_0x00f1:
            java.lang.String r0 = " AnalyticsDAO.delete() end"
            java.lang.Object[] r1 = new java.lang.Object[r1]
            com.tencent.beacon.d.a.b(r0, r1)
            r0 = r4
            goto L_0x00d2
        L_0x00fa:
            r0 = move-exception
            r2 = r5
            r3 = r5
        L_0x00fd:
            if (r2 == 0) goto L_0x0108
            boolean r4 = r2.isOpen()
            if (r4 == 0) goto L_0x0108
            r2.close()
        L_0x0108:
            if (r3 == 0) goto L_0x010d
            r3.close()
        L_0x010d:
            java.lang.String r2 = " AnalyticsDAO.delete() end"
            java.lang.Object[] r1 = new java.lang.Object[r1]
            com.tencent.beacon.d.a.b(r2, r1)
            throw r0
        L_0x0115:
            r0 = move-exception
            r2 = r5
            goto L_0x00fd
        L_0x0118:
            r0 = move-exception
            goto L_0x00fd
        L_0x011a:
            r0 = move-exception
            r3 = r5
            goto L_0x00fd
        L_0x011d:
            r0 = move-exception
            r2 = r5
            r5 = r3
            goto L_0x00d7
        L_0x0121:
            r0 = move-exception
            r5 = r3
            goto L_0x00d7
        L_0x0124:
            r0 = r3
            goto L_0x0083
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.beacon.a.a.a.a(android.content.Context, int[], long, long):int");
    }

    /* JADX WARNING: Removed duplicated region for block: B:48:0x00eb  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0107  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int a(android.content.Context r11, java.lang.Long[] r12) {
        /*
            r2 = 0
            r1 = 0
            java.lang.String r0 = " AnalyticsDAO.deleteList() start!"
            java.lang.Object[] r3 = new java.lang.Object[r1]
            com.tencent.beacon.d.a.b(r0, r3)
            if (r11 != 0) goto L_0x0014
            java.lang.String r0 = " deleteList() have null args!"
            java.lang.Object[] r1 = new java.lang.Object[r1]
            com.tencent.beacon.d.a.d(r0, r1)
            r0 = -1
        L_0x0013:
            return r0
        L_0x0014:
            if (r12 == 0) goto L_0x0019
            int r0 = r12.length
            if (r0 > 0) goto L_0x001b
        L_0x0019:
            r0 = r1
            goto L_0x0013
        L_0x001b:
            com.tencent.beacon.a.a.c r4 = com.tencent.beacon.a.a.c.a(r11)     // Catch:{ Throwable -> 0x00cf, all -> 0x00f7 }
            android.database.sqlite.SQLiteDatabase r3 = r4.getWritableDatabase()     // Catch:{ Throwable -> 0x0117, all -> 0x0112 }
            java.lang.StringBuffer r5 = new java.lang.StringBuffer     // Catch:{ Throwable -> 0x011c }
            r5.<init>()     // Catch:{ Throwable -> 0x011c }
            r2 = r1
            r0 = r1
        L_0x002a:
            int r6 = r12.length     // Catch:{ Throwable -> 0x0120 }
            if (r2 >= r6) goto L_0x0089
            r6 = r12[r2]     // Catch:{ Throwable -> 0x0120 }
            long r6 = r6.longValue()     // Catch:{ Throwable -> 0x0120 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0120 }
            java.lang.String r9 = " or  _id = "
            r8.<init>(r9)     // Catch:{ Throwable -> 0x0120 }
            java.lang.StringBuilder r6 = r8.append(r6)     // Catch:{ Throwable -> 0x0120 }
            java.lang.String r6 = r6.toString()     // Catch:{ Throwable -> 0x0120 }
            r5.append(r6)     // Catch:{ Throwable -> 0x0120 }
            if (r2 <= 0) goto L_0x0086
            int r6 = r2 % 25
            if (r6 != 0) goto L_0x0086
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0120 }
            java.lang.String r7 = " current "
            r6.<init>(r7)     // Catch:{ Throwable -> 0x0120 }
            java.lang.StringBuilder r6 = r6.append(r2)     // Catch:{ Throwable -> 0x0120 }
            java.lang.String r6 = r6.toString()     // Catch:{ Throwable -> 0x0120 }
            r7 = 0
            java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ Throwable -> 0x0120 }
            com.tencent.beacon.d.a.b(r6, r7)     // Catch:{ Throwable -> 0x0120 }
            r6 = 4
            java.lang.String r6 = r5.substring(r6)     // Catch:{ Throwable -> 0x0120 }
            java.lang.String r7 = "t_event"
            r8 = 0
            int r6 = r3.delete(r7, r6, r8)     // Catch:{ Throwable -> 0x0120 }
            int r0 = r0 + r6
            r6 = 0
            r5.setLength(r6)     // Catch:{ Throwable -> 0x0120 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0120 }
            java.lang.String r7 = " current deleteNum: "
            r6.<init>(r7)     // Catch:{ Throwable -> 0x0120 }
            java.lang.StringBuilder r6 = r6.append(r0)     // Catch:{ Throwable -> 0x0120 }
            java.lang.String r6 = r6.toString()     // Catch:{ Throwable -> 0x0120 }
            r7 = 0
            java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ Throwable -> 0x0120 }
            com.tencent.beacon.d.a.b(r6, r7)     // Catch:{ Throwable -> 0x0120 }
        L_0x0086:
            int r2 = r2 + 1
            goto L_0x002a
        L_0x0089:
            int r2 = r5.length()     // Catch:{ Throwable -> 0x0120 }
            if (r2 <= 0) goto L_0x00a1
            r2 = 4
            java.lang.String r2 = r5.substring(r2)     // Catch:{ Throwable -> 0x0120 }
            java.lang.String r6 = "t_event"
            r7 = 0
            int r2 = r3.delete(r6, r2, r7)     // Catch:{ Throwable -> 0x0120 }
            int r2 = r2 + r0
            r0 = 0
            r5.setLength(r0)     // Catch:{ Throwable -> 0x0122 }
            r0 = r2
        L_0x00a1:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0120 }
            java.lang.String r5 = " total deleteNum: "
            r2.<init>(r5)     // Catch:{ Throwable -> 0x0120 }
            java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ Throwable -> 0x0120 }
            java.lang.String r2 = r2.toString()     // Catch:{ Throwable -> 0x0120 }
            r5 = 0
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Throwable -> 0x0120 }
            com.tencent.beacon.d.a.a(r2, r5)     // Catch:{ Throwable -> 0x0120 }
            if (r3 == 0) goto L_0x00c1
            boolean r2 = r3.isOpen()
            if (r2 == 0) goto L_0x00c1
            r3.close()
        L_0x00c1:
            if (r4 == 0) goto L_0x00c6
            r4.close()
        L_0x00c6:
            java.lang.String r2 = " AnalyticsDAO.deleteList() end!"
            java.lang.Object[] r1 = new java.lang.Object[r1]
            com.tencent.beacon.d.a.b(r2, r1)
            goto L_0x0013
        L_0x00cf:
            r0 = move-exception
            r3 = r2
            r4 = r2
            r2 = r0
            r0 = r1
        L_0x00d4:
            java.lang.String r2 = r2.getMessage()     // Catch:{ all -> 0x0115 }
            r5 = 0
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ all -> 0x0115 }
            com.tencent.beacon.d.a.d(r2, r5)     // Catch:{ all -> 0x0115 }
            if (r3 == 0) goto L_0x00e9
            boolean r2 = r3.isOpen()
            if (r2 == 0) goto L_0x00e9
            r3.close()
        L_0x00e9:
            if (r4 == 0) goto L_0x00ee
            r4.close()
        L_0x00ee:
            java.lang.String r2 = " AnalyticsDAO.deleteList() end!"
            java.lang.Object[] r1 = new java.lang.Object[r1]
            com.tencent.beacon.d.a.b(r2, r1)
            goto L_0x0013
        L_0x00f7:
            r0 = move-exception
            r3 = r2
            r4 = r2
        L_0x00fa:
            if (r3 == 0) goto L_0x0105
            boolean r2 = r3.isOpen()
            if (r2 == 0) goto L_0x0105
            r3.close()
        L_0x0105:
            if (r4 == 0) goto L_0x010a
            r4.close()
        L_0x010a:
            java.lang.String r2 = " AnalyticsDAO.deleteList() end!"
            java.lang.Object[] r1 = new java.lang.Object[r1]
            com.tencent.beacon.d.a.b(r2, r1)
            throw r0
        L_0x0112:
            r0 = move-exception
            r3 = r2
            goto L_0x00fa
        L_0x0115:
            r0 = move-exception
            goto L_0x00fa
        L_0x0117:
            r0 = move-exception
            r3 = r2
            r2 = r0
            r0 = r1
            goto L_0x00d4
        L_0x011c:
            r0 = move-exception
            r2 = r0
            r0 = r1
            goto L_0x00d4
        L_0x0120:
            r2 = move-exception
            goto L_0x00d4
        L_0x0122:
            r0 = move-exception
            r10 = r0
            r0 = r2
            r2 = r10
            goto L_0x00d4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.beacon.a.a.a.a(android.content.Context, java.lang.Long[]):int");
    }

    protected static List<a> a(Cursor cursor) {
        com.tencent.beacon.d.a.b(" in AnalyticsDAO.paserCursor() start", new Object[0]);
        if (cursor == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        int columnIndex = cursor.getColumnIndex("_id");
        int columnIndex2 = cursor.getColumnIndex("_prority");
        int columnIndex3 = cursor.getColumnIndex("_time");
        int columnIndex4 = cursor.getColumnIndex("_type");
        int columnIndex5 = cursor.getColumnIndex("_datas");
        int columnIndex6 = cursor.getColumnIndex("_length");
        while (cursor.moveToNext()) {
            a aVar = new a();
            aVar.f3381a = cursor.getLong(columnIndex);
            aVar.e = cursor.getBlob(columnIndex5);
            aVar.c = cursor.getInt(columnIndex2);
            aVar.d = cursor.getLong(columnIndex3);
            aVar.b = cursor.getInt(columnIndex4);
            aVar.f = cursor.getLong(columnIndex6);
            arrayList.add(aVar);
        }
        com.tencent.beacon.d.a.b(" in AnalyticsDAO.paserCursor() end", new Object[0]);
        return arrayList;
    }

    /* JADX WARNING: Removed duplicated region for block: B:50:0x0126  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x014e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int b(android.content.Context r11, int[] r12, long r13, long r15) {
        /*
            java.lang.String r0 = " AnalyticsDAO.querySum() start"
            r1 = 0
            java.lang.Object[] r1 = new java.lang.Object[r1]
            com.tencent.beacon.d.a.b(r0, r1)
            if (r11 != 0) goto L_0x0014
            java.lang.String r0 = " querySum() context is null arg"
            r1 = 0
            java.lang.Object[] r1 = new java.lang.Object[r1]
            com.tencent.beacon.d.a.a(r0, r1)
            r0 = -1
        L_0x0013:
            return r0
        L_0x0014:
            r0 = -1
            r2 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 <= 0) goto L_0x0021
            r0 = 0
            goto L_0x0013
        L_0x0021:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = "_time >= "
            r0.<init>(r1)
            r1 = -1
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = " and _time"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = " <= 9223372036854775807"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r3 = r0.toString()
            if (r12 == 0) goto L_0x0087
            int r0 = r12.length
            if (r0 <= 0) goto L_0x0087
            java.lang.String r1 = ""
            r0 = 0
        L_0x0046:
            int r2 = r12.length
            if (r0 >= r2) goto L_0x0065
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.StringBuilder r1 = r2.append(r1)
            java.lang.String r2 = " or _type = "
            java.lang.StringBuilder r1 = r1.append(r2)
            r2 = r12[r0]
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            int r0 = r0 + 1
            goto L_0x0046
        L_0x0065:
            r0 = 4
            java.lang.String r0 = r1.substring(r0)
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r2 = " and ( "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r1 = " )"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r3 = r0.toString()
        L_0x0087:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = " query where: "
            r0.<init>(r1)
            java.lang.StringBuilder r0 = r0.append(r3)
            java.lang.String r0 = r0.toString()
            r1 = 0
            java.lang.Object[] r1 = new java.lang.Object[r1]
            com.tencent.beacon.d.a.b(r0, r1)
            r2 = 0
            r1 = 0
            r9 = 0
            com.tencent.beacon.a.a.c r8 = com.tencent.beacon.a.a.c.a(r11)     // Catch:{ Throwable -> 0x0101, all -> 0x0133 }
            android.database.sqlite.SQLiteDatabase r0 = r8.getWritableDatabase()     // Catch:{ Throwable -> 0x016c, all -> 0x015a }
            java.lang.String r1 = "t_event"
            r2 = 1
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ Throwable -> 0x0170, all -> 0x015d }
            r4 = 0
            java.lang.String r5 = "count(*) as sum"
            r2[r4] = r5     // Catch:{ Throwable -> 0x0170, all -> 0x015d }
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r2 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Throwable -> 0x0170, all -> 0x015d }
            r2.moveToNext()     // Catch:{ Throwable -> 0x0177, all -> 0x0163 }
            java.lang.String r1 = "sum"
            int r1 = r2.getColumnIndex(r1)     // Catch:{ Throwable -> 0x0177, all -> 0x0163 }
            int r1 = r2.getInt(r1)     // Catch:{ Throwable -> 0x0177, all -> 0x0163 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0177, all -> 0x0163 }
            java.lang.String r4 = " query sum: "
            r3.<init>(r4)     // Catch:{ Throwable -> 0x0177, all -> 0x0163 }
            java.lang.StringBuilder r3 = r3.append(r1)     // Catch:{ Throwable -> 0x0177, all -> 0x0163 }
            java.lang.String r3 = r3.toString()     // Catch:{ Throwable -> 0x0177, all -> 0x0163 }
            r4 = 0
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Throwable -> 0x0177, all -> 0x0163 }
            com.tencent.beacon.d.a.b(r3, r4)     // Catch:{ Throwable -> 0x0177, all -> 0x0163 }
            if (r2 == 0) goto L_0x00e6
            boolean r3 = r2.isClosed()
            if (r3 != 0) goto L_0x00e6
            r2.close()
        L_0x00e6:
            if (r0 == 0) goto L_0x00f1
            boolean r2 = r0.isOpen()
            if (r2 == 0) goto L_0x00f1
            r0.close()
        L_0x00f1:
            if (r8 == 0) goto L_0x00f6
            r8.close()
        L_0x00f6:
            java.lang.String r0 = " AnalyticsDAO.querySum() end"
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]
            com.tencent.beacon.d.a.b(r0, r2)
            r0 = r1
            goto L_0x0013
        L_0x0101:
            r0 = move-exception
            r3 = r9
        L_0x0103:
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x0168 }
            r4 = 0
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x0168 }
            com.tencent.beacon.d.a.b(r0, r4)     // Catch:{ all -> 0x0168 }
            r0 = -1
            if (r3 == 0) goto L_0x0119
            boolean r4 = r3.isClosed()
            if (r4 != 0) goto L_0x0119
            r3.close()
        L_0x0119:
            if (r1 == 0) goto L_0x0124
            boolean r3 = r1.isOpen()
            if (r3 == 0) goto L_0x0124
            r1.close()
        L_0x0124:
            if (r2 == 0) goto L_0x0129
            r2.close()
        L_0x0129:
            java.lang.String r1 = " AnalyticsDAO.querySum() end"
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]
            com.tencent.beacon.d.a.b(r1, r2)
            goto L_0x0013
        L_0x0133:
            r0 = move-exception
            r8 = r2
            r2 = r9
        L_0x0136:
            if (r2 == 0) goto L_0x0141
            boolean r3 = r2.isClosed()
            if (r3 != 0) goto L_0x0141
            r2.close()
        L_0x0141:
            if (r1 == 0) goto L_0x014c
            boolean r2 = r1.isOpen()
            if (r2 == 0) goto L_0x014c
            r1.close()
        L_0x014c:
            if (r8 == 0) goto L_0x0151
            r8.close()
        L_0x0151:
            java.lang.String r1 = " AnalyticsDAO.querySum() end"
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]
            com.tencent.beacon.d.a.b(r1, r2)
            throw r0
        L_0x015a:
            r0 = move-exception
            r2 = r9
            goto L_0x0136
        L_0x015d:
            r1 = move-exception
            r2 = r9
            r10 = r0
            r0 = r1
            r1 = r10
            goto L_0x0136
        L_0x0163:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
            goto L_0x0136
        L_0x0168:
            r0 = move-exception
            r8 = r2
            r2 = r3
            goto L_0x0136
        L_0x016c:
            r0 = move-exception
            r2 = r8
            r3 = r9
            goto L_0x0103
        L_0x0170:
            r1 = move-exception
            r2 = r8
            r3 = r9
            r10 = r1
            r1 = r0
            r0 = r10
            goto L_0x0103
        L_0x0177:
            r1 = move-exception
            r3 = r2
            r2 = r8
            r10 = r0
            r0 = r1
            r1 = r10
            goto L_0x0103
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.beacon.a.a.a.b(android.content.Context, int[], long, long):int");
    }

    public static boolean b(Context context, List<a> list) {
        com.tencent.beacon.d.a.b(" insertOrUpdate alyticsBeans start!", new Object[0]);
        if (context == null || list == null || list.size() <= 0) {
            com.tencent.beacon.d.a.d(" context == null || list == null|| list.size() <= 0 ? pls check!", new Object[0]);
            return false;
        }
        SQLiteDatabase sQLiteDatabase = null;
        boolean z = true;
        try {
            SQLiteDatabase writableDatabase = c.a(context).getWritableDatabase();
            for (a next : list) {
                ContentValues a2 = a(next);
                if (a2 != null) {
                    long replace = writableDatabase.replace("t_event", "_id", a2);
                    if (replace > 0) {
                        com.tencent.beacon.d.a.b(" result id:" + replace, new Object[0]);
                        next.f3381a = replace;
                    }
                }
                z = false;
            }
            if (writableDatabase != null && writableDatabase.isOpen()) {
                writableDatabase.close();
            }
            com.tencent.beacon.d.a.b(" insertOrUpdate alyticsBeans end", new Object[0]);
            return z;
        } catch (Throwable th) {
            if (sQLiteDatabase != null && sQLiteDatabase.isOpen()) {
                sQLiteDatabase.close();
            }
            com.tencent.beacon.d.a.b(" insertOrUpdate alyticsBeans end", new Object[0]);
            throw th;
        }
    }

    public static ContentValues a(a aVar) {
        ContentValues contentValues = new ContentValues();
        if (aVar.f3381a > 0) {
            contentValues.put("_id", Long.valueOf(aVar.f3381a));
        }
        contentValues.put("_prority", Integer.valueOf(aVar.c));
        contentValues.put("_time", Long.valueOf(aVar.d));
        contentValues.put("_type", Integer.valueOf(aVar.b));
        contentValues.put("_datas", aVar.e);
        contentValues.put("_length", Long.valueOf(aVar.f));
        return contentValues;
    }

    /* JADX WARNING: Removed duplicated region for block: B:106:0x0235  */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x020c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.List<com.tencent.beacon.a.a.a> a(android.content.Context r14, int[] r15, int r16, int r17, int r18, int r19, int r20, int r21, int r22, long r23, long r25) {
        /*
            java.lang.String r2 = " in AnalyticsDAO.query() start"
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]
            com.tencent.beacon.d.a.b(r2, r3)
            if (r14 == 0) goto L_0x0024
            r2 = 0
            int r2 = (r25 > r2 ? 1 : (r25 == r2 ? 0 : -1))
            if (r2 <= 0) goto L_0x0014
            int r2 = (r23 > r25 ? 1 : (r23 == r25 ? 0 : -1))
            if (r2 > 0) goto L_0x0024
        L_0x0014:
            if (r20 <= 0) goto L_0x001c
            r0 = r19
            r1 = r20
            if (r0 > r1) goto L_0x0024
        L_0x001c:
            if (r22 <= 0) goto L_0x002e
            r0 = r21
            r1 = r22
            if (r0 <= r1) goto L_0x002e
        L_0x0024:
            java.lang.String r2 = " query() args context == null || timeStart > timeEnd || miniCount > maxCount || miniUploadCount > maxUploadCount ,pls check"
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]
            com.tencent.beacon.d.a.d(r2, r3)
            r2 = 0
        L_0x002d:
            return r2
        L_0x002e:
            java.lang.String r4 = ""
            if (r15 == 0) goto L_0x0267
            int r2 = r15.length
            if (r2 <= 0) goto L_0x0267
            java.lang.String r3 = ""
            r2 = 0
        L_0x0038:
            int r5 = r15.length
            if (r2 >= r5) goto L_0x0057
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.StringBuilder r3 = r5.append(r3)
            java.lang.String r5 = " or _type = "
            java.lang.StringBuilder r3 = r3.append(r5)
            r5 = r15[r2]
            java.lang.StringBuilder r3 = r3.append(r5)
            java.lang.String r3 = r3.toString()
            int r2 = r2 + 1
            goto L_0x0038
        L_0x0057:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.StringBuilder r2 = r2.append(r4)
            r4 = 4
            java.lang.String r3 = r3.substring(r4)
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
        L_0x006d:
            int r3 = r2.length()
            if (r3 <= 0) goto L_0x0143
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = " ( "
            r3.<init>(r4)
            java.lang.StringBuilder r2 = r3.append(r2)
            java.lang.String r3 = " ) "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r5 = r2.toString()
        L_0x0088:
            r2 = 0
            int r2 = (r23 > r2 ? 1 : (r23 == r2 ? 0 : -1))
            if (r2 < 0) goto L_0x00b9
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.StringBuilder r3 = r2.append(r5)
            int r2 = r5.length()
            if (r2 <= 0) goto L_0x0147
            java.lang.String r2 = " and "
        L_0x009f:
            java.lang.StringBuilder r2 = r3.append(r2)
            java.lang.String r3 = "_time >= "
            java.lang.StringBuilder r2 = r2.append(r3)
            r0 = r23
            java.lang.StringBuilder r2 = r2.append(r0)
            java.lang.String r3 = " "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r5 = r2.toString()
        L_0x00b9:
            r2 = 0
            int r2 = (r25 > r2 ? 1 : (r25 == r2 ? 0 : -1))
            if (r2 < 0) goto L_0x00ea
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.StringBuilder r3 = r2.append(r5)
            int r2 = r5.length()
            if (r2 <= 0) goto L_0x014b
            java.lang.String r2 = " and "
        L_0x00d0:
            java.lang.StringBuilder r2 = r3.append(r2)
            java.lang.String r3 = "_time <= "
            java.lang.StringBuilder r2 = r2.append(r3)
            r0 = r25
            java.lang.StringBuilder r2 = r2.append(r0)
            java.lang.String r3 = " "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r5 = r2.toString()
        L_0x00ea:
            java.lang.String r2 = ""
            switch(r16) {
                case 1: goto L_0x014e;
                case 2: goto L_0x0162;
                default: goto L_0x00ef;
            }
        L_0x00ef:
            switch(r17) {
                case 1: goto L_0x0177;
                case 2: goto L_0x018c;
                default: goto L_0x00f2;
            }
        L_0x00f2:
            java.lang.String r3 = " , "
            boolean r3 = r2.endsWith(r3)
            if (r3 == 0) goto L_0x0264
            r3 = 0
            int r4 = r2.length()
            int r4 = r4 + -3
            java.lang.String r9 = r2.substring(r3, r4)
        L_0x0105:
            java.lang.String r2 = " query:%s"
            r3 = 1
            java.lang.Object[] r3 = new java.lang.Object[r3]
            r4 = 0
            r3[r4] = r5
            com.tencent.beacon.d.a.b(r2, r3)
            r3 = 0
            r4 = 0
            r13 = 0
            r12 = 0
            com.tencent.beacon.a.a.c r11 = com.tencent.beacon.a.a.c.a(r14)     // Catch:{ Throwable -> 0x01e4, all -> 0x0219 }
            android.database.sqlite.SQLiteDatabase r2 = r11.getWritableDatabase()     // Catch:{ Throwable -> 0x0251, all -> 0x0241 }
            java.lang.String r3 = "t_event"
            r4 = 0
            r6 = 0
            r7 = 0
            r8 = 0
            if (r18 < 0) goto L_0x01a1
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0258, all -> 0x0245 }
            r10.<init>()     // Catch:{ Throwable -> 0x0258, all -> 0x0245 }
            r0 = r18
            java.lang.StringBuilder r10 = r10.append(r0)     // Catch:{ Throwable -> 0x0258, all -> 0x0245 }
            java.lang.String r10 = r10.toString()     // Catch:{ Throwable -> 0x0258, all -> 0x0245 }
        L_0x0133:
            android.database.Cursor r5 = r2.query(r3, r4, r5, r6, r7, r8, r9, r10)     // Catch:{ Throwable -> 0x0258, all -> 0x0245 }
            r3 = r12
        L_0x0138:
            boolean r4 = r5.moveToNext()     // Catch:{ Throwable -> 0x025e, all -> 0x024a }
            if (r4 == 0) goto L_0x01a3
            java.util.List r3 = a(r5)     // Catch:{ Throwable -> 0x025e, all -> 0x024a }
            goto L_0x0138
        L_0x0143:
            java.lang.String r5 = ""
            goto L_0x0088
        L_0x0147:
            java.lang.String r2 = ""
            goto L_0x009f
        L_0x014b:
            java.lang.String r2 = ""
            goto L_0x00d0
        L_0x014e:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.StringBuilder r2 = r3.append(r2)
            java.lang.String r3 = "_prority ASC , "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            goto L_0x00ef
        L_0x0162:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.StringBuilder r2 = r3.append(r2)
            java.lang.String r3 = "_prority DESC , "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            goto L_0x00ef
        L_0x0177:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.StringBuilder r2 = r3.append(r2)
            java.lang.String r3 = "_time ASC "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            goto L_0x00f2
        L_0x018c:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.StringBuilder r2 = r3.append(r2)
            java.lang.String r3 = "_time DESC "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            goto L_0x00f2
        L_0x01a1:
            r10 = 0
            goto L_0x0133
        L_0x01a3:
            if (r3 == 0) goto L_0x01be
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x025e, all -> 0x024a }
            java.lang.String r6 = " total num: "
            r4.<init>(r6)     // Catch:{ Throwable -> 0x025e, all -> 0x024a }
            int r6 = r3.size()     // Catch:{ Throwable -> 0x025e, all -> 0x024a }
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ Throwable -> 0x025e, all -> 0x024a }
            java.lang.String r4 = r4.toString()     // Catch:{ Throwable -> 0x025e, all -> 0x024a }
            r6 = 0
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ Throwable -> 0x025e, all -> 0x024a }
            com.tencent.beacon.d.a.a(r4, r6)     // Catch:{ Throwable -> 0x025e, all -> 0x024a }
        L_0x01be:
            if (r5 == 0) goto L_0x01c9
            boolean r4 = r5.isClosed()
            if (r4 != 0) goto L_0x01c9
            r5.close()
        L_0x01c9:
            if (r2 == 0) goto L_0x01d4
            boolean r4 = r2.isOpen()
            if (r4 == 0) goto L_0x01d4
            r2.close()
        L_0x01d4:
            if (r11 == 0) goto L_0x01d9
            r11.close()
        L_0x01d9:
            java.lang.String r2 = " in AnalyticsDAO.query() end"
            r4 = 0
            java.lang.Object[] r4 = new java.lang.Object[r4]
            com.tencent.beacon.d.a.b(r2, r4)
            r2 = r3
            goto L_0x002d
        L_0x01e4:
            r2 = move-exception
            r5 = r13
            r6 = r4
            r4 = r3
            r3 = r2
            r2 = r12
        L_0x01ea:
            java.lang.String r3 = r3.getMessage()     // Catch:{ all -> 0x024e }
            r7 = 0
            java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ all -> 0x024e }
            com.tencent.beacon.d.a.b(r3, r7)     // Catch:{ all -> 0x024e }
            if (r5 == 0) goto L_0x01ff
            boolean r3 = r5.isClosed()
            if (r3 != 0) goto L_0x01ff
            r5.close()
        L_0x01ff:
            if (r6 == 0) goto L_0x020a
            boolean r3 = r6.isOpen()
            if (r3 == 0) goto L_0x020a
            r6.close()
        L_0x020a:
            if (r4 == 0) goto L_0x020f
            r4.close()
        L_0x020f:
            java.lang.String r3 = " in AnalyticsDAO.query() end"
            r4 = 0
            java.lang.Object[] r4 = new java.lang.Object[r4]
            com.tencent.beacon.d.a.b(r3, r4)
            goto L_0x002d
        L_0x0219:
            r2 = move-exception
            r11 = r3
            r5 = r13
            r6 = r4
        L_0x021d:
            if (r5 == 0) goto L_0x0228
            boolean r3 = r5.isClosed()
            if (r3 != 0) goto L_0x0228
            r5.close()
        L_0x0228:
            if (r6 == 0) goto L_0x0233
            boolean r3 = r6.isOpen()
            if (r3 == 0) goto L_0x0233
            r6.close()
        L_0x0233:
            if (r11 == 0) goto L_0x0238
            r11.close()
        L_0x0238:
            java.lang.String r3 = " in AnalyticsDAO.query() end"
            r4 = 0
            java.lang.Object[] r4 = new java.lang.Object[r4]
            com.tencent.beacon.d.a.b(r3, r4)
            throw r2
        L_0x0241:
            r2 = move-exception
            r5 = r13
            r6 = r4
            goto L_0x021d
        L_0x0245:
            r3 = move-exception
            r5 = r13
            r6 = r2
            r2 = r3
            goto L_0x021d
        L_0x024a:
            r3 = move-exception
            r6 = r2
            r2 = r3
            goto L_0x021d
        L_0x024e:
            r2 = move-exception
            r11 = r4
            goto L_0x021d
        L_0x0251:
            r2 = move-exception
            r3 = r2
            r5 = r13
            r6 = r4
            r4 = r11
            r2 = r12
            goto L_0x01ea
        L_0x0258:
            r3 = move-exception
            r4 = r11
            r5 = r13
            r6 = r2
            r2 = r12
            goto L_0x01ea
        L_0x025e:
            r4 = move-exception
            r6 = r2
            r2 = r3
            r3 = r4
            r4 = r11
            goto L_0x01ea
        L_0x0264:
            r9 = r2
            goto L_0x0105
        L_0x0267:
            r2 = r4
            goto L_0x006d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.beacon.a.a.a.a(android.content.Context, int[], int, int, int, int, int, int, int, long, long):java.util.List");
    }
}
