package com.tencent.beacon.event;

import android.content.Context;
import com.tencent.beacon.a.b.f;
import com.tencent.beacon.a.h;
import com.tencent.beacon.a.i;
import com.tencent.beacon.f.j;
import com.tencent.connect.common.Constants;
import java.util.Map;

/* compiled from: ProGuard */
public class a {

    /* renamed from: a  reason: collision with root package name */
    protected static Map<String, String> f3430a = null;
    private static Context b = null;
    private static String c = Constants.STR_EMPTY;
    private static String d = "10000";

    public static void a(Context context) {
        a(context, null, true, 0);
    }

    private static void a(Context context, com.tencent.beacon.f.a aVar, boolean z, long j) {
        if (context == null) {
            com.tencent.beacon.d.a.c(" the context is null! init beacon sdk failed!", new Object[0]);
            return;
        }
        Context applicationContext = context.getApplicationContext();
        if (applicationContext != null) {
            b = applicationContext;
        } else {
            b = context;
        }
        if (j > 0) {
            if (j > 10000) {
                j = 10000;
            }
            f.a(j);
        }
        j a2 = t.a(b, z);
        t.a(b, true, a2, aVar, null);
        com.tencent.beacon.b.f.a(b, a2);
    }

    public static String a() {
        return d;
    }

    public static String b() {
        return c;
    }

    public static void c() {
        t.b(false);
    }

    public static boolean a(String str, boolean z, long j, long j2, Map<String, String> map, boolean z2) {
        if (str == null || Constants.STR_EMPTY.equals(str.trim())) {
            com.tencent.beacon.d.a.c("param eventName is null or \"\", please check it, return false! ", new Object[0]);
            return false;
        }
        String a2 = h.a(str);
        if (a2 == null) {
            return false;
        }
        return t.a(a2, z, j, j2, map, z2);
    }

    public static void b(Context context) {
        i.d(context.getApplicationContext());
    }

    public static String d() {
        if (b == null || t.d() == null) {
            com.tencent.beacon.d.a.d("please initUserAction first!", new Object[0]);
            throw new RuntimeException("please initUserAction first!");
        } else if (t.d().h() > 0) {
            return com.tencent.beacon.b.a.a(b).a();
        } else {
            com.tencent.beacon.d.a.d("call this function getQIMEI untimely!", new Object[0]);
            return Constants.STR_EMPTY;
        }
    }
}
