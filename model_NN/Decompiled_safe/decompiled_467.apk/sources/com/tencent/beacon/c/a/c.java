package com.tencent.beacon.c.a;

import com.tencent.beacon.e.a;
import com.tencent.beacon.e.d;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public final class c extends com.tencent.beacon.e.c {
    private static byte[] j;

    /* renamed from: a  reason: collision with root package name */
    public byte f3410a = 0;
    public int b = 0;
    public byte[] c = null;
    public String d = Constants.STR_EMPTY;
    public byte e = 0;
    public byte f = 0;
    public long g = 0;
    public String h = Constants.STR_EMPTY;
    public String i = Constants.STR_EMPTY;

    public final void a(d dVar) {
        dVar.a(this.f3410a, 0);
        dVar.a(this.b, 1);
        dVar.a(this.c, 2);
        dVar.a(this.d, 3);
        dVar.a(this.e, 4);
        dVar.a(this.f, 5);
        dVar.a(this.g, 6);
        if (this.h != null) {
            dVar.a(this.h, 7);
        }
        if (this.i != null) {
            dVar.a(this.i, 8);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.beacon.e.a.a(byte, int, boolean):byte
     arg types: [byte, int, int]
     candidates:
      com.tencent.beacon.e.a.a(double, int, boolean):double
      com.tencent.beacon.e.a.a(float, int, boolean):float
      com.tencent.beacon.e.a.a(int, int, boolean):int
      com.tencent.beacon.e.a.a(long, int, boolean):long
      com.tencent.beacon.e.a.a(com.tencent.beacon.e.c, int, boolean):com.tencent.beacon.e.c
      com.tencent.beacon.e.a.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.beacon.e.a.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.beacon.e.a.a(short, int, boolean):short
      com.tencent.beacon.e.a.a(byte, int, boolean):byte */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.beacon.e.a.a(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.tencent.beacon.e.a.a(double, int, boolean):double
      com.tencent.beacon.e.a.a(float, int, boolean):float
      com.tencent.beacon.e.a.a(byte, int, boolean):byte
      com.tencent.beacon.e.a.a(long, int, boolean):long
      com.tencent.beacon.e.a.a(com.tencent.beacon.e.c, int, boolean):com.tencent.beacon.e.c
      com.tencent.beacon.e.a.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.beacon.e.a.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.beacon.e.a.a(short, int, boolean):short
      com.tencent.beacon.e.a.a(int, int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.beacon.e.a.a(long, int, boolean):long
     arg types: [long, int, int]
     candidates:
      com.tencent.beacon.e.a.a(double, int, boolean):double
      com.tencent.beacon.e.a.a(float, int, boolean):float
      com.tencent.beacon.e.a.a(byte, int, boolean):byte
      com.tencent.beacon.e.a.a(int, int, boolean):int
      com.tencent.beacon.e.a.a(com.tencent.beacon.e.c, int, boolean):com.tencent.beacon.e.c
      com.tencent.beacon.e.a.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.beacon.e.a.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.beacon.e.a.a(short, int, boolean):short
      com.tencent.beacon.e.a.a(long, int, boolean):long */
    public final void a(a aVar) {
        this.f3410a = aVar.a(this.f3410a, 0, true);
        this.b = aVar.a(this.b, 1, true);
        if (j == null) {
            byte[] bArr = new byte[1];
            j = bArr;
            bArr[0] = 0;
        }
        byte[] bArr2 = j;
        this.c = aVar.c(2, true);
        this.d = aVar.b(3, true);
        this.e = aVar.a(this.e, 4, true);
        this.f = aVar.a(this.f, 5, true);
        this.g = aVar.a(this.g, 6, true);
        this.h = aVar.b(7, false);
        this.i = aVar.b(8, false);
    }
}
