package com.tencent.beacon.b;

import android.content.Context;
import com.tencent.assistant.st.STConst;
import com.tencent.beacon.a.g;
import com.tencent.beacon.a.h;
import com.tencent.beacon.d.a;
import com.tencent.beacon.d.b;
import com.tencent.beacon.event.i;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public final class e implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private Context f3406a;
    private List<b> b;

    public e(Context context, List<b> list) {
        this.f3406a = context;
        this.b = list;
    }

    public final void run() {
        long h;
        String g;
        String str;
        String str2;
        d a2;
        d a3;
        if (this.f3406a != null && this.b != null) {
            g m = g.m();
            if (m == null) {
                a.d(" common info null", new Object[0]);
                return;
            }
            synchronized (m) {
                h = m.h();
                g = m.g();
            }
            String c = b.c(this.f3406a);
            if (c == null) {
                str = "null";
            } else {
                str = c;
            }
            String str3 = "null";
            if (h.a(this.f3406a) == null || (str3 = h.k(this.f3406a)) != null) {
                str2 = str3;
            } else {
                str2 = "null";
            }
            HashMap hashMap = new HashMap();
            Iterator<b> it = this.b.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                b next = it.next();
                a.a(" check " + next.a(), new Object[0]);
                a.b(" address:" + next.b(), new Object[0]);
                i iVar = null;
                hashMap.clear();
                hashMap.put("A28", g);
                hashMap.put("A3", a.a(this.f3406a).a());
                hashMap.put("A33", str2);
                StringBuilder sb = new StringBuilder();
                h.a(this.f3406a);
                hashMap.put("A20", sb.append(h.f(this.f3406a)).toString());
                StringBuilder sb2 = new StringBuilder();
                h.a(this.f3406a);
                hashMap.put("A74", sb2.append(h.l(this.f3406a)).toString());
                if ("PG".equals(next.a())) {
                    if (next.f()) {
                        hashMap.put("test", "Y");
                    }
                    if (str.toLowerCase().contains("wap")) {
                        a.a(" using wap request", new Object[0]);
                        a3 = a.a(next.b(), str);
                    } else {
                        a3 = a.a(next.b(), next.e());
                    }
                    if (a3 == null) {
                        a3 = new d();
                    }
                    if (a3.f3405a + a3.b + a3.c + a3.d + a3.e <= 0) {
                        a.a(" elapse bean is not avilable!try apach}", new Object[0]);
                        long currentTimeMillis = System.currentTimeMillis();
                        if (a(next.b(), " ".getBytes(), "post", 60000, str) != null) {
                            a3.e = System.currentTimeMillis() - currentTimeMillis;
                            a.a(" get a total time}" + a3.e, new Object[0]);
                        }
                    }
                    i iVar2 = new i();
                    iVar2.b(next.b());
                    long time = new Date().getTime();
                    a.a(" loc time:}" + time, new Object[0]);
                    long j = time + h;
                    a.a(" to stime:}" + j, new Object[0]);
                    iVar2.b(j);
                    iVar2.a("DN");
                    hashMap.put("A19", str);
                    hashMap.put("A40", new StringBuilder().append(a3.b).toString());
                    hashMap.put("A34", STConst.ST_INSTALL_FAIL_STR_UNKNOWN);
                    hashMap.put("A39", a3.f);
                    hashMap.put("A35", new StringBuilder().append(a3.f3405a).toString());
                    hashMap.put("A36", new StringBuilder().append(a3.c).toString());
                    hashMap.put("A38", new StringBuilder().append(a3.e).toString());
                    hashMap.put("A37", new StringBuilder().append(a3.d).toString());
                    iVar2.a(hashMap);
                    iVar = iVar2;
                } else if ("IP".equals(next.a())) {
                    String trim = next.b().trim();
                    String[] split = trim.split(":");
                    if (split == null || split.length != 2) {
                        a.d(" ip dest wrong }" + trim, new Object[0]);
                    } else {
                        if (next.f()) {
                            hashMap.put("test", "Y");
                        }
                        long a4 = a.a(split[0], Integer.parseInt(split[1]));
                        iVar = new i();
                        iVar.b(next.b());
                        long time2 = new Date().getTime();
                        a.a(" loc time:}" + time2, new Object[0]);
                        long j2 = time2 + h;
                        a.a(" to stime:}" + j2, new Object[0]);
                        iVar.b(j2);
                        iVar.a("IP");
                        hashMap.put("A19", str);
                        hashMap.put("A26", new StringBuilder().append(a4).toString());
                        iVar.a(hashMap);
                    }
                } else if ("HOST".equals(next.a())) {
                    String str4 = "http://" + next.c() + next.b();
                    a.a(" host domain test:" + str4, new Object[0]);
                    if (str.toLowerCase().contains("wap")) {
                        a.a(" using wap request", new Object[0]);
                        a2 = a.a(str4, str);
                    } else {
                        a2 = a.a(next.d(), next.c(), next.b());
                    }
                    if (a2 == null) {
                        a2 = new d();
                    }
                    if (a2.f3405a + a2.b + a2.c + a2.d + a2.e <= 0) {
                        a.a(" elapse bean is not avilable!try apach", new Object[0]);
                        long currentTimeMillis2 = System.currentTimeMillis();
                        if (a(str4, " ".getBytes(), "post", 60000, str) != null) {
                            a2.e = System.currentTimeMillis() - currentTimeMillis2;
                            a.a(" get a total time}" + a2.e, new Object[0]);
                        }
                    }
                    i iVar3 = new i();
                    iVar3.b(next.b());
                    long time3 = new Date().getTime();
                    a.a("loc time:" + time3, new Object[0]);
                    long j3 = time3 + h;
                    a.a("to stime:" + j3, new Object[0]);
                    iVar3.b(j3);
                    iVar3.a("HO");
                    hashMap.put("A19", str);
                    hashMap.put("A40", new StringBuilder().append(a2.b).toString());
                    hashMap.put("A34", next.c());
                    hashMap.put("hostip", next.d());
                    hashMap.put("A39", a2.f);
                    hashMap.put("A35", new StringBuilder().append(a2.f3405a).toString());
                    hashMap.put("A36", new StringBuilder().append(a2.c).toString());
                    hashMap.put("A38", new StringBuilder().append(a2.e).toString());
                    hashMap.put("A37", new StringBuilder().append(a2.d).toString());
                    iVar3.a(hashMap);
                    iVar = iVar3;
                }
                if (iVar != null) {
                    a.a(" insert record type " + next.a(), new Object[0]);
                    if (!h.a(this.f3406a, iVar)) {
                        a.a(" insert record fail!", new Object[0]);
                    }
                }
            }
            Context context = this.f3406a;
            long time4 = new Date().getTime();
            a.a(" MonitorDAO.deleteSpeedMonitorSources() start}", new Object[0]);
            if (context == null) {
                a.a(" MonitorDAO.deleteSpeedMonitorSources() context is null arg}", new Object[0]);
            } else {
                com.tencent.beacon.a.a.a.a(context, new int[]{5}, -1, time4);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00ed, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00ee, code lost:
        r1 = r0;
        r0 = null;
        r2 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0104, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0105, code lost:
        r1.printStackTrace();
        com.tencent.beacon.d.a.d(r1.getMessage(), new java.lang.Object[0]);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x013d, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x013e, code lost:
        r1.printStackTrace();
        com.tencent.beacon.d.a.d(r1.getMessage(), new java.lang.Object[0]);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x014b, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x014c, code lost:
        r2 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:?, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:?, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:?, code lost:
        return r0;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0100 A[SYNTHETIC, Splitter:B:32:0x0100] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0139 A[SYNTHETIC, Splitter:B:47:0x0139] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x014b A[ExcHandler: all (th java.lang.Throwable), Splitter:B:22:0x00dd] */
    /* JADX WARNING: Removed duplicated region for block: B:60:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static byte[] a(java.lang.String r7, byte[] r8, java.lang.String r9, int r10, java.lang.String r11) {
        /*
            r0 = 60000(0xea60, float:8.4078E-41)
            r2 = 0
            r6 = 0
            org.apache.http.params.BasicHttpParams r3 = new org.apache.http.params.BasicHttpParams
            r3.<init>()
            org.apache.http.params.HttpConnectionParams.setConnectionTimeout(r3, r0)
            org.apache.http.params.HttpConnectionParams.setSoTimeout(r3, r0)
            org.apache.http.entity.ByteArrayEntity r4 = new org.apache.http.entity.ByteArrayEntity
            r4.<init>(r8)
            java.lang.String r0 = r9.toLowerCase()
            java.lang.String r1 = "post"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x008c
            org.apache.http.client.methods.HttpPost r1 = new org.apache.http.client.methods.HttpPost
            r1.<init>(r7)
            r0 = r1
            org.apache.http.client.methods.HttpPost r0 = (org.apache.http.client.methods.HttpPost) r0
            r0.setEntity(r4)
        L_0x002c:
            org.apache.http.impl.client.DefaultHttpClient r0 = new org.apache.http.impl.client.DefaultHttpClient     // Catch:{ Throwable -> 0x014e }
            r0.<init>(r3)     // Catch:{ Throwable -> 0x014e }
            if (r11 == 0) goto L_0x006a
            java.lang.String r3 = r11.toLowerCase()     // Catch:{ Throwable -> 0x014e }
            java.lang.String r4 = "wap"
            boolean r3 = r3.contains(r4)     // Catch:{ Throwable -> 0x014e }
            if (r3 == 0) goto L_0x006a
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x014e }
            java.lang.String r4 = " use proxy }"
            r3.<init>(r4)     // Catch:{ Throwable -> 0x014e }
            java.lang.StringBuilder r3 = r3.append(r11)     // Catch:{ Throwable -> 0x014e }
            java.lang.String r3 = r3.toString()     // Catch:{ Throwable -> 0x014e }
            r4 = 0
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Throwable -> 0x014e }
            com.tencent.beacon.d.a.a(r3, r4)     // Catch:{ Throwable -> 0x014e }
            java.lang.String r3 = android.net.Proxy.getDefaultHost()     // Catch:{ Throwable -> 0x014e }
            int r4 = android.net.Proxy.getDefaultPort()     // Catch:{ Throwable -> 0x014e }
            org.apache.http.HttpHost r5 = new org.apache.http.HttpHost     // Catch:{ Throwable -> 0x014e }
            r5.<init>(r3, r4)     // Catch:{ Throwable -> 0x014e }
            org.apache.http.params.HttpParams r3 = r0.getParams()     // Catch:{ Throwable -> 0x014e }
            java.lang.String r4 = "http.route.default-proxy"
            r3.setParameter(r4, r5)     // Catch:{ Throwable -> 0x014e }
        L_0x006a:
            java.lang.String r3 = " do request!}"
            r4 = 0
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Throwable -> 0x014e }
            com.tencent.beacon.d.a.a(r3, r4)     // Catch:{ Throwable -> 0x014e }
            org.apache.http.HttpResponse r0 = r0.execute(r1)     // Catch:{ Throwable -> 0x014e }
            org.apache.http.StatusLine r1 = r0.getStatusLine()     // Catch:{ Throwable -> 0x014e }
            int r1 = r1.getStatusCode()     // Catch:{ Throwable -> 0x014e }
            r3 = 200(0xc8, float:2.8E-43)
            if (r1 == r3) goto L_0x00bc
            java.lang.String r0 = " request failure!}"
            r1 = 0
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ Throwable -> 0x014e }
            com.tencent.beacon.d.a.d(r0, r1)     // Catch:{ Throwable -> 0x014e }
            r0 = r2
        L_0x008b:
            return r0
        L_0x008c:
            if (r8 != 0) goto L_0x00a7
            java.lang.String r0 = ""
        L_0x0090:
            org.apache.http.client.methods.HttpGet r1 = new org.apache.http.client.methods.HttpGet
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.StringBuilder r4 = r4.append(r7)
            java.lang.StringBuilder r0 = r4.append(r0)
            java.lang.String r0 = r0.toString()
            r1.<init>(r0)
            goto L_0x002c
        L_0x00a7:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = "?"
            r0.<init>(r1)
            java.lang.String r1 = new java.lang.String
            r1.<init>(r8)
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            goto L_0x0090
        L_0x00bc:
            org.apache.http.HttpEntity r0 = r0.getEntity()     // Catch:{ Throwable -> 0x014e }
            if (r0 != 0) goto L_0x00cc
            java.lang.String r0 = " no response!}"
            r1 = 0
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ Throwable -> 0x014e }
            com.tencent.beacon.d.a.d(r0, r1)     // Catch:{ Throwable -> 0x014e }
            r0 = r2
            goto L_0x008b
        L_0x00cc:
            java.lang.String r1 = " get response and read!}"
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x014e }
            com.tencent.beacon.d.a.a(r1, r3)     // Catch:{ Throwable -> 0x014e }
            java.io.DataInputStream r3 = new java.io.DataInputStream     // Catch:{ Throwable -> 0x014e }
            java.io.InputStream r0 = r0.getContent()     // Catch:{ Throwable -> 0x014e }
            r3.<init>(r0)     // Catch:{ Throwable -> 0x014e }
            java.io.ByteArrayOutputStream r0 = new java.io.ByteArrayOutputStream     // Catch:{ Throwable -> 0x00ed, all -> 0x014b }
            r0.<init>()     // Catch:{ Throwable -> 0x00ed, all -> 0x014b }
        L_0x00e2:
            int r1 = r3.read()     // Catch:{ Throwable -> 0x00ed, all -> 0x014b }
            r4 = -1
            if (r1 == r4) goto L_0x0113
            r0.write(r1)     // Catch:{ Throwable -> 0x00ed, all -> 0x014b }
            goto L_0x00e2
        L_0x00ed:
            r0 = move-exception
            r1 = r0
            r0 = r2
            r2 = r3
        L_0x00f1:
            r1.printStackTrace()     // Catch:{ all -> 0x0136 }
            java.lang.String r1 = r1.getMessage()     // Catch:{ all -> 0x0136 }
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ all -> 0x0136 }
            com.tencent.beacon.d.a.d(r1, r3)     // Catch:{ all -> 0x0136 }
            if (r2 == 0) goto L_0x008b
            r2.close()     // Catch:{ IOException -> 0x0104 }
            goto L_0x008b
        L_0x0104:
            r1 = move-exception
            r1.printStackTrace()
            java.lang.String r1 = r1.getMessage()
            java.lang.Object[] r2 = new java.lang.Object[r6]
            com.tencent.beacon.d.a.d(r1, r2)
            goto L_0x008b
        L_0x0113:
            r0.flush()     // Catch:{ Throwable -> 0x00ed, all -> 0x014b }
            byte[] r0 = r0.toByteArray()     // Catch:{ Throwable -> 0x00ed, all -> 0x014b }
            java.lang.String r1 = " read end!}"
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Throwable -> 0x0152, all -> 0x014b }
            com.tencent.beacon.d.a.a(r1, r2)     // Catch:{ Throwable -> 0x0152, all -> 0x014b }
            r3.close()     // Catch:{ IOException -> 0x0127 }
            goto L_0x008b
        L_0x0127:
            r1 = move-exception
            r1.printStackTrace()
            java.lang.String r1 = r1.getMessage()
            java.lang.Object[] r2 = new java.lang.Object[r6]
            com.tencent.beacon.d.a.d(r1, r2)
            goto L_0x008b
        L_0x0136:
            r0 = move-exception
        L_0x0137:
            if (r2 == 0) goto L_0x013c
            r2.close()     // Catch:{ IOException -> 0x013d }
        L_0x013c:
            throw r0
        L_0x013d:
            r1 = move-exception
            r1.printStackTrace()
            java.lang.String r1 = r1.getMessage()
            java.lang.Object[] r2 = new java.lang.Object[r6]
            com.tencent.beacon.d.a.d(r1, r2)
            goto L_0x013c
        L_0x014b:
            r0 = move-exception
            r2 = r3
            goto L_0x0137
        L_0x014e:
            r0 = move-exception
            r1 = r0
            r0 = r2
            goto L_0x00f1
        L_0x0152:
            r1 = move-exception
            r2 = r3
            goto L_0x00f1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.beacon.b.e.a(java.lang.String, byte[], java.lang.String, int, java.lang.String):byte[]");
    }
}
