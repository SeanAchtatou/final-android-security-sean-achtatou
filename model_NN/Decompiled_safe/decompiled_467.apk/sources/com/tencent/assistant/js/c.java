package com.tencent.assistant.js;

import com.tencent.assistant.d.e;
import com.tencent.assistant.protocol.jce.LbsCell;
import com.tencent.assistant.protocol.jce.LbsData;
import com.tencent.assistant.protocol.jce.LbsLocation;
import com.tencent.assistant.protocol.jce.LbsWifiMac;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: ProGuard */
class c implements e {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ JsBridge f1350a;

    c(JsBridge jsBridge) {
        this.f1350a = jsBridge;
    }

    public void a(int i) {
        if (this.f1350a.mLBSReqBundle != null) {
            int i2 = this.f1350a.mLBSReqBundle.getInt("seqId");
            String string = this.f1350a.mLBSReqBundle.getString("callbackFun");
            String string2 = this.f1350a.mLBSReqBundle.getString("method");
            if (i == 0) {
                try {
                    this.f1350a.responseFail(string, i2, string2, -1);
                } catch (Exception e) {
                    e.printStackTrace();
                    this.f1350a.responseFail(string, i2, string2, -3);
                }
            } else {
                LbsData c = this.f1350a.lbsEngine.c();
                LbsLocation a2 = c.a();
                JSONObject jSONObject = new JSONObject();
                JSONObject jSONObject2 = new JSONObject();
                jSONObject2.put("accuracy", a2.d);
                jSONObject2.put("altitude", a2.c);
                jSONObject2.put("bearing", a2.e);
                jSONObject2.put("latitude", a2.f2226a);
                jSONObject2.put("longitude", a2.b);
                jSONObject2.put("speed", a2.f);
                jSONObject2.put("time", a2.g);
                jSONObject.put("location", jSONObject2);
                if (c.b() != null) {
                    JSONArray jSONArray = new JSONArray();
                    JSONObject jSONObject3 = new JSONObject();
                    Iterator<LbsCell> it = c.b().iterator();
                    while (it.hasNext()) {
                        LbsCell next = it.next();
                        jSONObject3.put("cellId", next.d());
                        jSONObject3.put("mcc", next.a());
                        jSONObject3.put("mnc", next.b());
                        jSONObject3.put("lac", next.c());
                        jSONObject3.put("rssi", next.e());
                        jSONArray.put(jSONObject3);
                    }
                    jSONObject.put("cells", jSONArray);
                }
                if (c.c() != null) {
                    JSONArray jSONArray2 = new JSONArray();
                    JSONObject jSONObject4 = new JSONObject();
                    Iterator<LbsWifiMac> it2 = c.c().iterator();
                    while (it2.hasNext()) {
                        LbsWifiMac next2 = it2.next();
                        jSONObject4.put("mac", next2.a());
                        jSONObject4.put("rssi", next2.b());
                        jSONArray2.put(jSONObject4);
                    }
                    jSONObject.put("wifis", jSONArray2);
                }
                this.f1350a.response(string, i2, string2, jSONObject.toString());
            }
        }
    }
}
