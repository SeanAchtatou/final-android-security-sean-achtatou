package com.tencent.assistant.tagpage;

import android.content.Intent;
import android.view.View;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.appdetail.CommentDetailTabView;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistantv2.activity.AppDetailActivityV5;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
class j implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SimpleAppModel f2554a;
    final /* synthetic */ int b;
    final /* synthetic */ TagPageCardAdapter c;

    j(TagPageCardAdapter tagPageCardAdapter, SimpleAppModel simpleAppModel, int i) {
        this.c = tagPageCardAdapter;
        this.f2554a = simpleAppModel;
        this.b = i;
    }

    public void onClick(View view) {
        Intent intent = new Intent(this.c.b, AppDetailActivityV5.class);
        intent.putExtra(CommentDetailTabView.PARAMS_SIMPLE_MODEL_INFO, this.f2554a);
        if (this.c.b instanceof BaseActivity) {
            intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, ((BaseActivity) this.c.b).f());
        }
        this.c.b.startActivity(intent);
        this.c.a(this.c.a(this.b) + "_01", Constants.STR_EMPTY, 200);
    }
}
