package com.tencent.assistant.tagpage;

import com.tencent.assistant.module.BaseEngine;
import com.tencent.assistant.protocol.jce.GetAppFromTagRequest;
import com.tencent.assistant.protocol.jce.GetAppFromTagResponse;
import com.tencent.assistant.protocol.scu.RequestResponePair;
import com.tencent.assistant.utils.XLog;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class b extends BaseEngine<v> {

    /* renamed from: a  reason: collision with root package name */
    private int f2546a = 0;

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, List<RequestResponePair> list) {
        XLog.i("GetTagPageEngine", "retSeq = " + this.f2546a + ", sep = " + i);
        if (this.f2546a != i || list == null || list.size() <= 0) {
            XLog.i("GetTagPageEngine", "[GetTagPageEngine] ---> onRequestSuccessed (error)");
        } else {
            XLog.i("GetTagPageEngine", "[GetTagPageEngine] ---> onRequestSuccessed, (responses.size() = " + list.size() + ")");
            ArrayList arrayList = new ArrayList(1);
            for (RequestResponePair next : list) {
                if (next.response instanceof GetAppFromTagResponse) {
                    arrayList.clear();
                    arrayList.add((GetAppFromTagResponse) next.response);
                }
            }
        }
        runOnUiThread(new c(this, i, list));
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, List<RequestResponePair> list) {
        XLog.i("GetTagPageEngine", "[GetTagPageEngine] ---> onRequestFailed, errorCode = " + i2);
        notifyDataChangedInMainThread(new e(this, i, i2, list));
    }

    public int a(String str, String str2, long j, String str3, int i, int i2) {
        ArrayList arrayList = new ArrayList(1);
        XLog.i("GetTagPageEngine", "[sendRequest] ---> indexBegin = " + i + ", indexEnd = " + i2 + ", tagId = " + str + ", tagName = " + str2 + ", appId = " + j + ", packageName = " + str3);
        GetAppFromTagRequest getAppFromTagRequest = new GetAppFromTagRequest();
        getAppFromTagRequest.f2079a = str;
        getAppFromTagRequest.b = str2;
        getAppFromTagRequest.c = j;
        getAppFromTagRequest.d = str3;
        getAppFromTagRequest.e = i;
        getAppFromTagRequest.f = i2;
        arrayList.add(getAppFromTagRequest);
        this.f2546a = send(arrayList);
        return this.f2546a;
    }
}
