package com.tencent.assistant.tagpage;

import android.content.Context;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.AbsListView;
import com.tencent.assistant.component.txscrollview.TXGetMoreListView;
import com.tencent.assistant.component.txscrollview.TXRefreshScrollViewBase;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;

/* compiled from: ProGuard */
public class TagPageListView extends TXGetMoreListView {

    /* renamed from: a  reason: collision with root package name */
    private static final String f2544a = TagPageListView.class.getSimpleName();
    private AbsListView.OnScrollListener b = null;

    public TagPageListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public TagPageListView(Context context) {
        super(context);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0 && motionEvent.getEdgeFlags() != 0) {
            return false;
        }
        switch (motionEvent.getAction()) {
            case 0:
                if (!isReadyForScroll()) {
                    return false;
                }
                PointF pointF = this.mLastMotionPointF;
                PointF pointF2 = this.mInitialMotionPointF;
                float x = motionEvent.getX();
                pointF2.x = x;
                pointF.x = x;
                PointF pointF3 = this.mLastMotionPointF;
                PointF pointF4 = this.mInitialMotionPointF;
                float y = motionEvent.getY();
                pointF4.y = y;
                pointF3.y = y;
                return true;
            case 1:
            case 3:
                return onTouchEventCancelAndUp();
            case 2:
                if (!this.mIsBeingDragged) {
                    return false;
                }
                if (this.mLastMotionPointF.y < motionEvent.getY() && TXScrollViewBase.ScrollMode.PULL_FROM_END == this.mScrollMode) {
                    return true;
                }
                this.mLastMotionPointF.x = motionEvent.getX();
                this.mLastMotionPointF.y = motionEvent.getY();
                if (this.mScrollMode != TXScrollViewBase.ScrollMode.NOSCROLL) {
                    scrollMoveEvent();
                }
                return true;
            default:
                return false;
        }
    }

    /* access modifiers changed from: protected */
    public int scrollMoveEvent() {
        return super.scrollMoveEvent();
    }

    public void a(boolean z) {
        if (this.mFooterLoadingView != null) {
            this.mFooterLoadingView.setVisibility(z ? 0 : 8);
        }
    }

    public void setOnScrollListener(AbsListView.OnScrollListener onScrollListener) {
        this.b = onScrollListener;
    }

    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
        if (this.b != null) {
            this.b.onScroll(absListView, i, i2, i3);
        }
        super.onScroll(absListView, i, i2, i3);
    }

    public void onScrollStateChanged(AbsListView absListView, int i) {
        if (this.b != null) {
            this.b.onScrollStateChanged(absListView, i);
        }
        super.onScrollStateChanged(absListView, i);
    }

    public void a() {
        this.mGetMoreRefreshState = TXRefreshScrollViewBase.RefreshState.REFRESH_LOAD_FINISH;
        updateFootViewState(true);
    }

    /* access modifiers changed from: protected */
    public void updateFootViewState(boolean z) {
        super.updateFootViewState(z);
    }
}
