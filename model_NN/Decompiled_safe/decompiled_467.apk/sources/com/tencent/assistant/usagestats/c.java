package com.tencent.assistant.usagestats;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

/* compiled from: ProGuard */
public final class c<K, V> implements Map<K, V> {

    /* renamed from: a  reason: collision with root package name */
    public static final c f2593a = new c(true);
    static Object[] b;
    static int c;
    static Object[] d;
    static int e;
    static final int[] f = new int[0];
    int[] g;
    Object[] h;
    int i;
    f<K, V> j;

    /* access modifiers changed from: package-private */
    public int a(Object obj, int i2) {
        int i3 = this.i;
        if (i3 == 0) {
            return -1;
        }
        int a2 = e.a(this.g, i3, i2);
        if (a2 < 0 || obj.equals(this.h[a2 << 1])) {
            return a2;
        }
        int i4 = a2 + 1;
        while (i4 < i3 && this.g[i4] == i2) {
            if (obj.equals(this.h[i4 << 1])) {
                return i4;
            }
            i4++;
        }
        int i5 = a2 - 1;
        while (i5 >= 0 && this.g[i5] == i2) {
            if (obj.equals(this.h[i5 << 1])) {
                return i5;
            }
            i5--;
        }
        return i4 ^ -1;
    }

    /* access modifiers changed from: package-private */
    public int a() {
        int i2 = this.i;
        if (i2 == 0) {
            return -1;
        }
        int a2 = e.a(this.g, i2, 0);
        if (a2 < 0 || this.h[a2 << 1] == null) {
            return a2;
        }
        int i3 = a2 + 1;
        while (i3 < i2 && this.g[i3] == 0) {
            if (this.h[i3 << 1] == null) {
                return i3;
            }
            i3++;
        }
        int i4 = a2 - 1;
        while (i4 >= 0 && this.g[i4] == 0) {
            if (this.h[i4 << 1] == null) {
                return i4;
            }
            i4--;
        }
        return i3 ^ -1;
    }

    private void e(int i2) {
        if (this.g == f) {
            throw new UnsupportedOperationException("ArrayMap is immutable");
        }
        if (i2 == 8) {
            synchronized (c.class) {
                if (d != null) {
                    Object[] objArr = d;
                    this.h = objArr;
                    d = (Object[]) objArr[0];
                    this.g = (int[]) objArr[1];
                    objArr[1] = null;
                    objArr[0] = null;
                    e--;
                    return;
                }
            }
        } else if (i2 == 4) {
            synchronized (c.class) {
                if (b != null) {
                    Object[] objArr2 = b;
                    this.h = objArr2;
                    b = (Object[]) objArr2[0];
                    this.g = (int[]) objArr2[1];
                    objArr2[1] = null;
                    objArr2[0] = null;
                    c--;
                    return;
                }
            }
        }
        this.g = new int[i2];
        this.h = new Object[(i2 << 1)];
    }

    private static void a(int[] iArr, Object[] objArr, int i2) {
        if (iArr.length == 8) {
            synchronized (c.class) {
                if (e < 10) {
                    objArr[0] = d;
                    objArr[1] = iArr;
                    for (int i3 = (i2 << 1) - 1; i3 >= 2; i3--) {
                        objArr[i3] = null;
                    }
                    d = objArr;
                    e++;
                }
            }
        } else if (iArr.length == 4) {
            synchronized (c.class) {
                if (c < 10) {
                    objArr[0] = b;
                    objArr[1] = iArr;
                    for (int i4 = (i2 << 1) - 1; i4 >= 2; i4--) {
                        objArr[i4] = null;
                    }
                    b = objArr;
                    c++;
                }
            }
        }
    }

    public c() {
        this.g = e.b;
        this.h = e.d;
        this.i = 0;
    }

    private c(boolean z) {
        this.g = f;
        this.h = e.d;
        this.i = 0;
    }

    public void clear() {
        if (this.i > 0) {
            a(this.g, this.h, this.i);
            this.g = e.b;
            this.h = e.d;
            this.i = 0;
        }
    }

    public void a(int i2) {
        if (this.g.length < i2) {
            int[] iArr = this.g;
            Object[] objArr = this.h;
            e(i2);
            if (this.i > 0) {
                System.arraycopy(iArr, 0, this.g, 0, this.i);
                System.arraycopy(objArr, 0, this.h, 0, this.i << 1);
            }
            a(iArr, objArr, this.i);
        }
    }

    public boolean containsKey(Object obj) {
        return obj == null ? a() >= 0 : a(obj, obj.hashCode()) >= 0;
    }

    /* access modifiers changed from: package-private */
    public int a(Object obj) {
        int i2 = 1;
        int i3 = this.i * 2;
        Object[] objArr = this.h;
        if (obj == null) {
            while (i2 < i3) {
                if (objArr[i2] == null) {
                    return i2 >> 1;
                }
                i2 += 2;
            }
        } else {
            while (i2 < i3) {
                if (obj.equals(objArr[i2])) {
                    return i2 >> 1;
                }
                i2 += 2;
            }
        }
        return -1;
    }

    public boolean containsValue(Object obj) {
        return a(obj) >= 0;
    }

    public V get(Object obj) {
        int a2 = obj == null ? a() : a(obj, obj.hashCode());
        if (a2 >= 0) {
            return this.h[(a2 << 1) + 1];
        }
        return null;
    }

    public K b(int i2) {
        return this.h[i2 << 1];
    }

    public V c(int i2) {
        return this.h[(i2 << 1) + 1];
    }

    public V a(int i2, V v) {
        int i3 = (i2 << 1) + 1;
        V v2 = this.h[i3];
        this.h[i3] = v;
        return v2;
    }

    public boolean isEmpty() {
        return this.i <= 0;
    }

    public V put(K k, V v) {
        int hashCode;
        int a2;
        int i2 = 8;
        if (k == null) {
            a2 = a();
            hashCode = 0;
        } else {
            hashCode = k.hashCode();
            a2 = a(k, hashCode);
        }
        if (a2 >= 0) {
            int i3 = (a2 << 1) + 1;
            V v2 = this.h[i3];
            this.h[i3] = v;
            return v2;
        }
        int i4 = a2 ^ -1;
        if (this.i >= this.g.length) {
            if (this.i >= 8) {
                i2 = this.i + (this.i >> 1);
            } else if (this.i < 4) {
                i2 = 4;
            }
            int[] iArr = this.g;
            Object[] objArr = this.h;
            e(i2);
            if (this.g.length > 0) {
                System.arraycopy(iArr, 0, this.g, 0, iArr.length);
                System.arraycopy(objArr, 0, this.h, 0, objArr.length);
            }
            a(iArr, objArr, this.i);
        }
        if (i4 < this.i) {
            System.arraycopy(this.g, i4, this.g, i4 + 1, this.i - i4);
            System.arraycopy(this.h, i4 << 1, this.h, (i4 + 1) << 1, (this.i - i4) << 1);
        }
        this.g[i4] = hashCode;
        this.h[i4 << 1] = k;
        this.h[(i4 << 1) + 1] = v;
        this.i++;
        return null;
    }

    public V remove(Object obj) {
        int a2 = obj == null ? a() : a(obj, obj.hashCode());
        if (a2 >= 0) {
            return d(a2);
        }
        return null;
    }

    public V d(int i2) {
        int i3 = 8;
        V v = this.h[(i2 << 1) + 1];
        if (this.i <= 1) {
            a(this.g, this.h, this.i);
            this.g = e.b;
            this.h = e.d;
            this.i = 0;
        } else if (this.g.length <= 8 || this.i >= this.g.length / 3) {
            this.i--;
            if (i2 < this.i) {
                System.arraycopy(this.g, i2 + 1, this.g, i2, this.i - i2);
                System.arraycopy(this.h, (i2 + 1) << 1, this.h, i2 << 1, (this.i - i2) << 1);
            }
            this.h[this.i << 1] = null;
            this.h[(this.i << 1) + 1] = null;
        } else {
            if (this.i > 8) {
                i3 = this.i + (this.i >> 1);
            }
            int[] iArr = this.g;
            Object[] objArr = this.h;
            e(i3);
            this.i--;
            if (i2 > 0) {
                System.arraycopy(iArr, 0, this.g, 0, i2);
                System.arraycopy(objArr, 0, this.h, 0, i2 << 1);
            }
            if (i2 < this.i) {
                System.arraycopy(iArr, i2 + 1, this.g, i2, this.i - i2);
                System.arraycopy(objArr, (i2 + 1) << 1, this.h, i2 << 1, (this.i - i2) << 1);
            }
        }
        return v;
    }

    public int size() {
        return this.i;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Map)) {
            return false;
        }
        Map map = (Map) obj;
        if (size() != map.size()) {
            return false;
        }
        int i2 = 0;
        while (i2 < this.i) {
            try {
                Object b2 = b(i2);
                Object c2 = c(i2);
                Object obj2 = map.get(b2);
                if (c2 == null) {
                    if (obj2 != null || !map.containsKey(b2)) {
                        return false;
                    }
                } else if (!c2.equals(obj2)) {
                    return false;
                }
                i2++;
            } catch (NullPointerException e2) {
                return false;
            } catch (ClassCastException e3) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        int[] iArr = this.g;
        Object[] objArr = this.h;
        int i2 = this.i;
        int i3 = 1;
        int i4 = 0;
        int i5 = 0;
        while (i4 < i2) {
            Object obj = objArr[i3];
            i5 += (obj == null ? 0 : obj.hashCode()) ^ iArr[i4];
            i4++;
            i3 += 2;
        }
        return i5;
    }

    public String toString() {
        if (isEmpty()) {
            return "{}";
        }
        StringBuilder sb = new StringBuilder(this.i * 28);
        sb.append('{');
        for (int i2 = 0; i2 < this.i; i2++) {
            if (i2 > 0) {
                sb.append(", ");
            }
            Object b2 = b(i2);
            if (b2 != this) {
                sb.append(b2);
            } else {
                sb.append("(this Map)");
            }
            sb.append('=');
            Object c2 = c(i2);
            if (c2 != this) {
                sb.append(c2);
            } else {
                sb.append("(this Map)");
            }
        }
        sb.append('}');
        return sb.toString();
    }

    private f<K, V> b() {
        if (this.j == null) {
            this.j = new d(this);
        }
        return this.j;
    }

    public void putAll(Map<? extends K, ? extends V> map) {
        a(this.i + map.size());
        for (Map.Entry next : map.entrySet()) {
            put(next.getKey(), next.getValue());
        }
    }

    public Set<Map.Entry<K, V>> entrySet() {
        return b().d();
    }

    public Set<K> keySet() {
        return b().e();
    }

    public Collection<V> values() {
        return b().f();
    }
}
