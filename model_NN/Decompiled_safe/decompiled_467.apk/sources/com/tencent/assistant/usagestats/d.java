package com.tencent.assistant.usagestats;

import java.util.Map;

/* compiled from: ProGuard */
class d extends f<K, V> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ c f2594a;

    d(c cVar) {
        this.f2594a = cVar;
    }

    /* access modifiers changed from: protected */
    public int a() {
        return this.f2594a.i;
    }

    /* access modifiers changed from: protected */
    public Object a(int i, int i2) {
        return this.f2594a.h[(i << 1) + i2];
    }

    /* access modifiers changed from: protected */
    public int a(Object obj) {
        return obj == null ? this.f2594a.a() : this.f2594a.a(obj, obj.hashCode());
    }

    /* access modifiers changed from: protected */
    public int b(Object obj) {
        return this.f2594a.a(obj);
    }

    /* access modifiers changed from: protected */
    public Map<K, V> b() {
        return this.f2594a;
    }

    /* access modifiers changed from: protected */
    public void a(K k, V v) {
        this.f2594a.put(k, v);
    }

    /* access modifiers changed from: protected */
    public V a(int i, V v) {
        return this.f2594a.a(i, v);
    }

    /* access modifiers changed from: protected */
    public void a(int i) {
        this.f2594a.d(i);
    }

    /* access modifiers changed from: protected */
    public void c() {
        this.f2594a.clear();
    }
}
