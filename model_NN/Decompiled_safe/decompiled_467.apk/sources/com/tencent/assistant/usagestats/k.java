package com.tencent.assistant.usagestats;

import java.util.Collection;
import java.util.Iterator;

/* compiled from: ProGuard */
final class k implements Collection<V> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ f f2600a;

    k(f fVar) {
        this.f2600a = fVar;
    }

    public boolean add(V v) {
        throw new UnsupportedOperationException();
    }

    public boolean addAll(Collection<? extends V> collection) {
        throw new UnsupportedOperationException();
    }

    public void clear() {
        this.f2600a.c();
    }

    public boolean contains(Object obj) {
        return this.f2600a.b(obj) >= 0;
    }

    public boolean containsAll(Collection<?> collection) {
        for (Object contains : collection) {
            if (!contains(contains)) {
                return false;
            }
        }
        return true;
    }

    public boolean isEmpty() {
        return this.f2600a.a() == 0;
    }

    public Iterator<V> iterator() {
        return new g(this.f2600a, 1);
    }

    public boolean remove(Object obj) {
        int b = this.f2600a.b(obj);
        if (b < 0) {
            return false;
        }
        this.f2600a.a(b);
        return true;
    }

    public boolean removeAll(Collection<?> collection) {
        int i = 0;
        int a2 = this.f2600a.a();
        boolean z = false;
        while (i < a2) {
            if (collection.contains(this.f2600a.a(i, 1))) {
                this.f2600a.a(i);
                i--;
                a2--;
                z = true;
            }
            i++;
        }
        return z;
    }

    public boolean retainAll(Collection<?> collection) {
        int i = 0;
        int a2 = this.f2600a.a();
        boolean z = false;
        while (i < a2) {
            if (!collection.contains(this.f2600a.a(i, 1))) {
                this.f2600a.a(i);
                i--;
                a2--;
                z = true;
            }
            i++;
        }
        return z;
    }

    public int size() {
        return this.f2600a.a();
    }

    public Object[] toArray() {
        return this.f2600a.b(1);
    }

    public <T> T[] toArray(T[] tArr) {
        return this.f2600a.a(tArr, 1);
    }
}
