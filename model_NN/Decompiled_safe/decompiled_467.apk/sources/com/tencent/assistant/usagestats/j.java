package com.tencent.assistant.usagestats;

import a.a.a;
import java.util.Iterator;
import java.util.Map;

/* compiled from: ProGuard */
final class j implements Iterator<Map.Entry<K, V>>, Map.Entry<K, V> {

    /* renamed from: a  reason: collision with root package name */
    int f2599a;
    int b;
    boolean c = false;
    final /* synthetic */ f d;

    j(f fVar) {
        this.d = fVar;
        this.f2599a = fVar.a() - 1;
        this.b = -1;
    }

    public boolean hasNext() {
        return this.b < this.f2599a;
    }

    /* renamed from: a */
    public Map.Entry<K, V> next() {
        this.b++;
        this.c = true;
        return this;
    }

    public void remove() {
        if (!this.c) {
            throw new IllegalStateException();
        }
        this.b--;
        this.f2599a--;
        this.c = false;
        this.d.a(this.b);
    }

    public K getKey() {
        if (this.c) {
            return this.d.a(this.b, 0);
        }
        throw new IllegalStateException("This container does not support retaining Map.Entry objects");
    }

    public V getValue() {
        if (this.c) {
            return this.d.a(this.b, 1);
        }
        throw new IllegalStateException("This container does not support retaining Map.Entry objects");
    }

    public V setValue(V v) {
        if (this.c) {
            return this.d.a(this.b, v);
        }
        throw new IllegalStateException("This container does not support retaining Map.Entry objects");
    }

    public final boolean equals(Object obj) {
        boolean z = true;
        if (!this.c) {
            throw new IllegalStateException("This container does not support retaining Map.Entry objects");
        } else if (!(obj instanceof Map.Entry)) {
            return false;
        } else {
            Map.Entry entry = (Map.Entry) obj;
            if (!a.a(entry.getKey(), this.d.a(this.b, 0)) || !a.a(entry.getValue(), this.d.a(this.b, 1))) {
                z = false;
            }
            return z;
        }
    }

    public final int hashCode() {
        int i = 0;
        if (!this.c) {
            throw new IllegalStateException("This container does not support retaining Map.Entry objects");
        }
        Object a2 = this.d.a(this.b, 0);
        Object a3 = this.d.a(this.b, 1);
        int hashCode = a2 == null ? 0 : a2.hashCode();
        if (a3 != null) {
            i = a3.hashCode();
        }
        return i ^ hashCode;
    }

    public final String toString() {
        return getKey() + "=" + getValue();
    }
}
