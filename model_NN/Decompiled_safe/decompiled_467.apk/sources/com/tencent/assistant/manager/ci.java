package com.tencent.assistant.manager;

import com.tencent.assistant.manager.NetworkMonitor;
import com.tencent.assistant.manager.SelfUpdateManager;
import com.tencent.assistant.net.APN;
import com.tencent.assistant.net.c;

/* compiled from: ProGuard */
class ci implements NetworkMonitor.ConnectivityChangeListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SelfUpdateManager f1528a;

    ci(SelfUpdateManager selfUpdateManager) {
        this.f1528a = selfUpdateManager;
    }

    public void onConnected(APN apn) {
        if (apn != APN.WIFI || c.k() || this.f1528a.i != SelfUpdateManager.SelfUpdateType.SILENT) {
            return;
        }
        if (this.f1528a.e == null) {
            this.f1528a.a(false);
        } else {
            this.f1528a.a(this.f1528a.i);
        }
    }

    public void onDisconnected(APN apn) {
    }

    public void onConnectivityChanged(APN apn, APN apn2) {
    }
}
