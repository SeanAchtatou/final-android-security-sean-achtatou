package com.tencent.assistant.manager.smartcard.b;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.a.a;
import com.tencent.assistant.model.a.s;
import com.tencent.assistant.protocol.jce.SmartCardTemplate3;
import com.tencent.assistant.protocol.jce.Template3App;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class b extends a {

    /* renamed from: a  reason: collision with root package name */
    public c f1590a;
    public List<a> b;
    public int c;
    public int d;
    public String e;

    public boolean a(byte b2, JceStruct jceStruct) {
        if (!(jceStruct instanceof SmartCardTemplate3)) {
            return false;
        }
        SmartCardTemplate3 smartCardTemplate3 = (SmartCardTemplate3) jceStruct;
        this.f1590a = new c();
        this.f1590a.a(smartCardTemplate3.f2336a);
        this.b = new ArrayList();
        Iterator<Template3App> it = smartCardTemplate3.b.iterator();
        while (it.hasNext()) {
            a aVar = new a();
            aVar.a(it.next());
            this.b.add(aVar);
        }
        this.i = b2;
        this.j = smartCardTemplate3.c;
        this.c = smartCardTemplate3.d;
        this.d = smartCardTemplate3.e;
        this.e = smartCardTemplate3.f;
        if (smartCardTemplate3.f2336a != null) {
            this.n = smartCardTemplate3.f2336a.d;
        }
        return true;
    }

    public List<SimpleAppModel> a() {
        if (this.b == null || this.b.size() <= 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (a next : this.b) {
            if (next.f1589a != null) {
                arrayList.add(next.f1589a);
            }
        }
        return arrayList;
    }

    public s b() {
        if (this.c <= 0 || this.d <= 0) {
            return null;
        }
        s sVar = new s();
        sVar.f = this.j;
        sVar.e = this.i;
        sVar.f1652a = this.c;
        sVar.b = this.d;
        return sVar;
    }

    public void a(List<Long> list) {
        if (list != null && list.size() != 0 && this.b != null && this.b.size() != 0) {
            ArrayList arrayList = new ArrayList();
            for (a next : this.b) {
                if (list.contains(Long.valueOf(next.f1589a.f1634a))) {
                    arrayList.add(next);
                }
            }
            this.b.removeAll(arrayList);
        }
    }

    public void c() {
        if (this.b != null && this.b.size() > 0) {
            Iterator<a> it = this.b.iterator();
            while (it.hasNext()) {
                a next = it.next();
                if (!(next.f1589a == null || ApkResourceManager.getInstance().getInstalledApkInfo(next.f1589a.c) == null)) {
                    it.remove();
                }
            }
        }
    }
}
