package com.tencent.assistant.manager;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import com.tencent.assistant.db.table.ad;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.model.q;
import com.tencent.assistant.thumbnailCache.o;
import com.tencent.assistant.thumbnailCache.p;
import com.tencent.assistant.utils.FileUtil;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.bf;
import com.tencent.assistant.utils.bj;
import com.tencent.assistant.utils.t;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class cm implements p {

    /* renamed from: a  reason: collision with root package name */
    public List<String> f1532a = null;
    /* access modifiers changed from: private */
    public ad b = new ad();
    private q c;
    private final int d = 1024;
    private Bitmap e = null;
    private final int f = 5;
    private final int g = EventDispatcherEnum.CACHE_EVENT_START;

    public q a() {
        q next;
        try {
            List<q> a2 = this.b.a(true);
            if (a2 != null && !a2.isEmpty()) {
                Iterator<q> it = a2.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    }
                    next = it.next();
                    long currentTimeMillis = System.currentTimeMillis() / 1000;
                    if (next.d() <= currentTimeMillis && next.e() >= currentTimeMillis) {
                        if (next.g() <= 0 || next.h() < next.g()) {
                            String a3 = a(next.i());
                        }
                    }
                }
                String a32 = a(next.i());
                if (c(a32)) {
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inJustDecodeBounds = false;
                    options.inSampleSize = 1;
                    try {
                        this.e = bf.a(a32, options, 0);
                        try {
                            this.c = next;
                            return next;
                        } catch (Exception | OutOfMemoryError e2) {
                            return next;
                        }
                    } catch (OutOfMemoryError e3) {
                        return null;
                    } catch (Exception e4) {
                        return null;
                    }
                }
            }
        } catch (Exception e5) {
            e5.printStackTrace();
        }
        return null;
    }

    public Bitmap b() {
        return this.e;
    }

    public void a(q qVar) {
        TemporaryThreadManager.get().start(new cn(this, qVar));
    }

    public void a(List<q> list) {
        b(list);
    }

    private void b(List<q> list) {
        TemporaryThreadManager.get().start(new co(this, list));
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0085 A[SYNTHETIC, Splitter:B:13:0x0085] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x008e A[SYNTHETIC, Splitter:B:18:0x008e] */
    /* JADX WARNING: Removed duplicated region for block: B:28:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(android.graphics.Bitmap r8, com.tencent.assistant.model.q r9) {
        /*
            r7 = this;
            r5 = 1000(0x3e8, double:4.94E-321)
            r2 = 0
            android.graphics.Bitmap r0 = r7.a(r8)     // Catch:{ Exception -> 0x007e, all -> 0x008b }
            android.graphics.Bitmap r0 = r7.b(r0)     // Catch:{ Exception -> 0x007e, all -> 0x008b }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x007e, all -> 0x008b }
            r1.<init>()     // Catch:{ Exception -> 0x007e, all -> 0x008b }
            java.lang.String r3 = "dpi="
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x007e, all -> 0x008b }
            int r3 = r0.getWidth()     // Catch:{ Exception -> 0x007e, all -> 0x008b }
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x007e, all -> 0x008b }
            java.lang.String r3 = "*"
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x007e, all -> 0x008b }
            int r3 = r0.getHeight()     // Catch:{ Exception -> 0x007e, all -> 0x008b }
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x007e, all -> 0x008b }
            java.lang.String r3 = ";time="
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x007e, all -> 0x008b }
            long r3 = r9.d()     // Catch:{ Exception -> 0x007e, all -> 0x008b }
            long r3 = r3 * r5
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ Exception -> 0x007e, all -> 0x008b }
            java.lang.String r3 = com.tencent.assistant.utils.cv.a(r3)     // Catch:{ Exception -> 0x007e, all -> 0x008b }
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x007e, all -> 0x008b }
            java.lang.String r3 = "~"
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x007e, all -> 0x008b }
            long r3 = r9.e()     // Catch:{ Exception -> 0x007e, all -> 0x008b }
            long r3 = r3 * r5
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ Exception -> 0x007e, all -> 0x008b }
            java.lang.String r3 = com.tencent.assistant.utils.cv.a(r3)     // Catch:{ Exception -> 0x007e, all -> 0x008b }
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x007e, all -> 0x008b }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x007e, all -> 0x008b }
            r9.d(r1)     // Catch:{ Exception -> 0x007e, all -> 0x008b }
            java.lang.String r1 = r9.i()     // Catch:{ Exception -> 0x007e, all -> 0x008b }
            java.lang.String r3 = r7.a(r1)     // Catch:{ Exception -> 0x007e, all -> 0x008b }
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x007e, all -> 0x008b }
            r1.<init>()     // Catch:{ Exception -> 0x007e, all -> 0x008b }
            android.graphics.Bitmap$CompressFormat r2 = android.graphics.Bitmap.CompressFormat.PNG     // Catch:{ Exception -> 0x0099 }
            r4 = 100
            r0.compress(r2, r4, r1)     // Catch:{ Exception -> 0x0099 }
            com.tencent.assistant.utils.FileUtil.write2File(r1, r3)     // Catch:{ Exception -> 0x0099 }
            if (r1 == 0) goto L_0x007d
            r1.close()     // Catch:{ IOException -> 0x0092 }
        L_0x007d:
            return
        L_0x007e:
            r0 = move-exception
            r1 = r2
        L_0x0080:
            r0.printStackTrace()     // Catch:{ all -> 0x0096 }
            if (r1 == 0) goto L_0x007d
            r1.close()     // Catch:{ IOException -> 0x0089 }
            goto L_0x007d
        L_0x0089:
            r0 = move-exception
            goto L_0x007d
        L_0x008b:
            r0 = move-exception
        L_0x008c:
            if (r2 == 0) goto L_0x0091
            r2.close()     // Catch:{ IOException -> 0x0094 }
        L_0x0091:
            throw r0
        L_0x0092:
            r0 = move-exception
            goto L_0x007d
        L_0x0094:
            r1 = move-exception
            goto L_0x0091
        L_0x0096:
            r0 = move-exception
            r2 = r1
            goto L_0x008c
        L_0x0099:
            r0 = move-exception
            goto L_0x0080
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.manager.cm.a(android.graphics.Bitmap, com.tencent.assistant.model.q):void");
    }

    private Bitmap a(Bitmap bitmap) {
        Bitmap bitmap2 = null;
        if (bitmap != null) {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            try {
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                if (byteArrayOutputStream.toByteArray().length / 1024 > 1024) {
                    byteArrayOutputStream.reset();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 50, byteArrayOutputStream);
                    ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inJustDecodeBounds = false;
                    bitmap = BitmapFactory.decodeStream(byteArrayInputStream, null, options);
                }
                int width = bitmap.getWidth();
                int height = bitmap.getHeight();
                int i = t.b;
                bitmap2 = a(bitmap, i, Math.round((((float) height) / ((float) width)) * ((float) i)));
                if (byteArrayOutputStream != null) {
                    try {
                        byteArrayOutputStream.close();
                    } catch (IOException e2) {
                    }
                }
            } catch (Throwable th) {
                if (byteArrayOutputStream != null) {
                    try {
                        byteArrayOutputStream.close();
                    } catch (IOException e3) {
                    }
                }
                throw th;
            }
        }
        return bitmap2;
    }

    private Bitmap a(Bitmap bitmap, int i, int i2) {
        if (bitmap == null) {
            return null;
        }
        return Bitmap.createScaledBitmap(bitmap, i, i2, true);
    }

    private Bitmap b(Bitmap bitmap) {
        if (bitmap == null) {
            return null;
        }
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        int i = t.c;
        if (height > i) {
            return Bitmap.createBitmap(bitmap, 0, (height - i) / 2, width, i);
        }
        return bitmap;
    }

    /* access modifiers changed from: private */
    public String a(String str) {
        return FileUtil.getPicDir() + File.separator + b(str);
    }

    private String b(String str) {
        return bj.b(str);
    }

    /* access modifiers changed from: private */
    public boolean c(String str) {
        return FileUtil.getFileLastModified(str) > 0;
    }

    public void thumbnailRequestCompleted(o oVar) {
        if (!TextUtils.isEmpty(oVar.c())) {
            TemporaryThreadManager.get().start(new cp(this, oVar));
        }
    }

    public void thumbnailRequestFailed(o oVar) {
    }
}
