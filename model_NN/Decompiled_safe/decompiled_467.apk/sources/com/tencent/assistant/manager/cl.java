package com.tencent.assistant.manager;

import android.net.Uri;
import android.text.TextUtils;
import com.tencent.assistant.component.appdetail.CommentDetailTabView;
import com.tencent.assistant.m;
import com.tencent.assistant.utils.t;

/* compiled from: ProGuard */
public class cl {

    /* renamed from: a  reason: collision with root package name */
    public int f1531a = 0;
    public int b = 0;
    public boolean c = false;
    public boolean d = false;
    public String e;
    final /* synthetic */ SelfUpdateManager f;

    public cl(SelfUpdateManager selfUpdateManager) {
        this.f = selfUpdateManager;
        d();
    }

    private void d() {
        a();
        e();
    }

    public void a() {
        this.e = m.a().C();
    }

    private void e() {
        if (!TextUtils.isEmpty(this.e)) {
            Uri parse = Uri.parse(this.e);
            if (parse.getQueryParameter(CommentDetailTabView.PARAMS_VERSION_CODE) != null) {
                this.f1531a = Integer.valueOf(parse.getQueryParameter(CommentDetailTabView.PARAMS_VERSION_CODE)).intValue();
            }
            if (parse.getQueryParameter("popTimes") != null) {
                this.b = Integer.valueOf(parse.getQueryParameter("popTimes")).intValue();
            }
            if (parse.getQueryParameter("isClickLater") != null) {
                this.c = Boolean.valueOf(parse.getQueryParameter("isClickLater")).booleanValue();
            }
            if (parse.getQueryParameter("isManualCheck") != null) {
                this.d = Boolean.valueOf(parse.getQueryParameter("isManualCheck")).booleanValue();
            }
        }
    }

    public void a(int i) {
        if ((i == this.f1531a && this.f.l()) || i > this.f1531a) {
            this.f1531a = i;
            this.b = 0;
            this.c = false;
            this.d = false;
            f();
        }
    }

    public void a(boolean z) {
        this.d = z;
        f();
    }

    public void b(boolean z) {
        this.c = z;
        f();
    }

    public void b() {
        this.b++;
        f();
    }

    private void f() {
        m.a().d(g());
    }

    private String g() {
        return "selfUpdate://exitInfo?" + CommentDetailTabView.PARAMS_VERSION_CODE + "=" + this.f1531a + "&" + "popTimes" + "=" + this.b + "&" + "isClickLater" + "=" + this.c + "&" + "isManualCheck" + "=" + this.d;
    }

    public boolean c() {
        if (this.f.e != null && this.b <= 0 && this.f1531a > t.o() && this.f.a(this.f.e.e) && this.c && !this.d) {
            return true;
        }
        return false;
    }
}
