package com.tencent.assistant.manager;

import android.content.Context;
import android.net.NetworkInfo;
import com.tencent.assistant.manager.NetworkMonitor;
import com.tencent.assistant.net.APN;
import com.tencent.assistant.net.c;

/* compiled from: ProGuard */
public class cq {

    /* renamed from: a  reason: collision with root package name */
    private static cq f1536a = null;
    private NetworkMonitor b = new NetworkMonitor();
    private cg c = new cg();
    private at d = new at();

    private cq() {
    }

    public static synchronized cq a() {
        cq cqVar;
        synchronized (cq.class) {
            if (f1536a == null) {
                f1536a = new cq();
            }
            cqVar = f1536a;
        }
        return cqVar;
    }

    public void a(NetworkMonitor.ConnectivityChangeListener connectivityChangeListener) {
        this.b.a(connectivityChangeListener);
    }

    public void b(NetworkMonitor.ConnectivityChangeListener connectivityChangeListener) {
        this.b.b(connectivityChangeListener);
    }

    public void a(au auVar) {
        this.d.a(auVar);
    }

    public void a(ch chVar) {
        this.c.a(chVar);
    }

    public void b(ch chVar) {
        this.c.b(chVar);
    }

    public void b() {
        this.d.a();
        System.gc();
    }

    public void a(NetworkInfo networkInfo) {
        if (networkInfo != null) {
            APN i = c.i();
            c.j();
            APN i2 = c.i();
            if (i == i2) {
                return;
            }
            if (i == APN.NO_NETWORK) {
                this.b.a(i2);
            } else if (i2 == APN.NO_NETWORK) {
                this.b.b(i);
            } else {
                this.b.a(i, i2);
            }
        }
    }

    public void a(boolean z) {
        if (z) {
            this.c.b();
        } else {
            this.c.a();
        }
    }

    public void a(Context context) {
        this.b.a(context);
        this.c.a(context);
    }
}
