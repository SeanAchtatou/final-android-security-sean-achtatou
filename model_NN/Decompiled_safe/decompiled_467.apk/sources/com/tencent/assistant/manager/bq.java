package com.tencent.assistant.manager;

import com.qq.AppService.AstApp;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.protocol.jce.AdviceApp;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.v;

/* compiled from: ProGuard */
class bq implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppConst.DialogInfo f1511a;
    final /* synthetic */ cd b;
    final /* synthetic */ bo c;

    bq(bo boVar, AppConst.DialogInfo dialogInfo, cd cdVar) {
        this.c = boVar;
        this.f1511a = dialogInfo;
        this.b = cdVar;
    }

    public void run() {
        try {
            if (AstApp.m() != null) {
                if (this.f1511a instanceof AppConst.TwoBtnDialogInfo) {
                    v.a((AppConst.TwoBtnDialogInfo) this.f1511a);
                }
                int f = AstApp.m().f();
                AdviceApp b2 = this.b.b();
                this.c.a(this.f1511a.pageId, f, STConst.ST_DEFAULT_SLOT, 100, b2 != null ? b2.i() : null);
                bo.a("rec_pop_window_exp", this.b.e);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
