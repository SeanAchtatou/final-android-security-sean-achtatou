package com.tencent.assistant.manager.smartcard.a.a;

import android.util.Pair;
import com.tencent.assistant.manager.smartcard.b.d;
import com.tencent.assistant.manager.smartcard.y;
import com.tencent.assistant.model.a.i;
import com.tencent.assistant.model.a.r;
import java.util.List;

/* compiled from: ProGuard */
public class b extends y {
    public boolean a(i iVar, List<Long> list) {
        Pair<Boolean, r> b = b(iVar);
        if (iVar instanceof d) {
            if (!((d) iVar).f() && ((d) iVar).f1592a != null && ((d) iVar).f1592a.size() == 0) {
                return false;
            }
            if (((d) iVar).f1592a == null || ((d) iVar).f1592a.size() < 3) {
                return false;
            }
        }
        if (!((Boolean) b.first).booleanValue()) {
            return false;
        }
        return true;
    }
}
