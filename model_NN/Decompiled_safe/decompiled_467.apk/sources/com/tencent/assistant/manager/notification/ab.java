package com.tencent.assistant.manager.notification;

import android.app.Notification;
import android.graphics.Bitmap;
import com.tencent.assistant.m;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
class ab implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Bitmap f1561a;
    final /* synthetic */ aa b;

    ab(aa aaVar, Bitmap bitmap) {
        this.b = aaVar;
        this.f1561a = bitmap;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.manager.notification.v.a(int, android.app.Notification, int, boolean):void
     arg types: [int, android.app.Notification, int, int]
     candidates:
      com.tencent.assistant.manager.notification.v.a(int, com.tencent.assistant.protocol.jce.PushInfo, byte[], boolean):void
      com.tencent.assistant.manager.notification.v.a(int, android.app.Notification, int, boolean):void */
    public void run() {
        Notification a2 = this.b.b.a(this.b.f1560a.downloadTicket, this.f1561a);
        if (a2 != null) {
            v.a().a(115, a2, 13, true);
            this.b.b.a(0);
            m.a().g(Constants.STR_EMPTY);
        }
    }
}
