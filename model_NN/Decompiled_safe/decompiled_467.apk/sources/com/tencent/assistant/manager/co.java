package com.tencent.assistant.manager;

import android.graphics.Bitmap;
import android.text.TextUtils;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.model.q;
import com.tencent.assistant.thumbnailCache.k;
import com.tencent.assistant.utils.FileUtil;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
class co implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ List f1534a;
    final /* synthetic */ cm b;

    co(cm cmVar, List list) {
        this.b = cmVar;
        this.f1534a = list;
    }

    public void run() {
        boolean z;
        boolean z2;
        List<q> a2 = this.b.b.a(false);
        this.b.b.a();
        this.b.b.a(this.f1534a);
        this.b.f1532a = new ArrayList();
        for (q qVar : this.f1534a) {
            if (qVar.k() != 0 && qVar.e() >= System.currentTimeMillis() / 1000 && !TextUtils.isEmpty(qVar.i())) {
                q qVar2 = null;
                if (a2.contains(qVar)) {
                    qVar2 = a2.remove(a2.indexOf(qVar));
                    z = qVar2.i().equalsIgnoreCase(qVar.i()) && this.b.c(this.b.a(qVar.i()));
                } else {
                    z = false;
                }
                if (!z) {
                    try {
                        Bitmap a3 = k.b().a(qVar.i(), 2, this.b);
                        if (a3 == null || a3.isRecycled()) {
                            this.b.f1532a.add(qVar.i());
                            z2 = false;
                        } else {
                            this.b.a(a3, qVar);
                            z2 = true;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        z2 = false;
                    }
                } else {
                    qVar.d(qVar2.j());
                    qVar.d(qVar2.h());
                    z2 = true;
                }
                if (z2) {
                    this.b.b.a(qVar);
                }
            }
        }
        for (q i : a2) {
            FileUtil.deleteFile(this.b.a(i.i()));
        }
        for (int i2 = 1; i2 <= 5 && this.b.f1532a.size() > 0; i2++) {
            Iterator<String> it = this.b.f1532a.iterator();
            while (it.hasNext()) {
                String next = it.next();
                q a4 = this.b.b.a(next);
                if (a4 == null) {
                    it.remove();
                } else {
                    Bitmap a5 = k.b().a(next, 2, this.b);
                    if (a5 == null || a5.isRecycled()) {
                        try {
                            Thread.sleep((long) (i2 * EventDispatcherEnum.CACHE_EVENT_START));
                        } catch (InterruptedException e2) {
                        }
                    } else {
                        this.b.a(a5, a4);
                        this.b.b.a(a4);
                        it.remove();
                    }
                }
            }
        }
    }
}
