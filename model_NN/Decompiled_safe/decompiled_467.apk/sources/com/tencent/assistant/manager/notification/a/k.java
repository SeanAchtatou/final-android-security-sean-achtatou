package com.tencent.assistant.manager.notification.a;

import android.text.TextUtils;
import android.widget.RemoteViews;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.manager.notification.a.a.f;
import com.tencent.assistant.protocol.jce.PushIconInfo;
import com.tencent.assistant.protocol.jce.PushInfo;
import java.util.List;

/* compiled from: ProGuard */
public class k extends d {
    private final int[] p = {R.id.icon1, R.id.icon2, R.id.icon3};

    public k(int i, PushInfo pushInfo, byte[] bArr) {
        super(i, pushInfo, bArr);
    }

    /* access modifiers changed from: protected */
    public boolean c() {
        if (super.c() && this.c.e != null && this.c.e.size() > 0) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean f() {
        a((int) R.layout.notification_card_4);
        if (this.i == null) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean g() {
        if (this.c == null || this.c.e == null || this.c.e.size() <= 0) {
            return false;
        }
        this.j = b(R.layout.notification_card4_right);
        a(this.j, this.c.e, this.p);
        this.i.removeAllViews(R.id.rightContainer);
        this.i.addView(R.id.rightContainer, this.j);
        this.i.setViewVisibility(R.id.rightContainer, 0);
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean h() {
        return true;
    }

    /* access modifiers changed from: protected */
    public void a(RemoteViews remoteViews, List<PushIconInfo> list, int[] iArr) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < iArr.length && i2 < list.size()) {
                PushIconInfo pushIconInfo = list.get(i2);
                if (pushIconInfo != null && !TextUtils.isEmpty(pushIconInfo.b)) {
                    f fVar = new f(pushIconInfo);
                    fVar.a(new l(this, remoteViews, iArr, i2));
                    a(fVar);
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }
}
