package com.tencent.assistant.manager.notification.a.a;

/* compiled from: ProGuard */
public abstract class a {

    /* renamed from: a  reason: collision with root package name */
    protected boolean f1551a = false;
    protected b b = null;

    public void a(b bVar) {
        this.b = bVar;
    }

    public boolean a() {
        return this.f1551a;
    }

    /* access modifiers changed from: protected */
    public void b() {
        this.f1551a = false;
        a(0);
    }

    /* access modifiers changed from: protected */
    public void a(int i) {
        this.f1551a = true;
        if (this.b != null) {
            this.b.a(i);
        }
    }
}
