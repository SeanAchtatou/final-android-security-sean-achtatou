package com.tencent.assistant.manager.smartcard.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.component.smartcard.NormalSmartcardBaseItem;
import com.tencent.assistant.component.smartcard.SmartcardListener;
import com.tencent.assistant.manager.smartcard.b.b;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.a.i;
import com.tencent.assistantv2.st.b.d;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
public class NormalSmartCardGiftTemplateItem extends NormalSmartcardBaseItem {
    private NormalSmartCardHeader f;
    private LinearLayout i;
    private b j;

    public NormalSmartCardGiftTemplateItem(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public NormalSmartCardGiftTemplateItem(Context context, i iVar, SmartcardListener smartcardListener, IViewInvalidater iViewInvalidater) {
        super(context, iVar, smartcardListener, iViewInvalidater);
    }

    public NormalSmartCardGiftTemplateItem(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public void a() {
        LayoutInflater.from(this.f1115a).inflate((int) R.layout.smartcard_gift_template_layout, this);
        this.f = (NormalSmartCardHeader) findViewById(R.id.smartcard_header);
        this.i = (LinearLayout) findViewById(R.id.node_area);
        this.j = (b) this.smartcardModel;
        f();
    }

    /* access modifiers changed from: protected */
    public void b() {
        this.j = (b) this.smartcardModel;
        f();
    }

    private void f() {
        this.f.a(this.j.f1590a, this.h);
        int childCount = this.i.getChildCount() - this.j.b.size();
        if (childCount > 0) {
            for (int i2 = 0; i2 < childCount; i2++) {
                this.i.removeViewAt(i2);
            }
        } else if (childCount < 0) {
            for (int i3 = 0; i3 < (-childCount); i3++) {
                this.i.addView(new NormalSmartCardGiftNode(this.f1115a));
            }
        }
        STInfoV2 sTInfoV2 = null;
        int i4 = 0;
        while (i4 < this.i.getChildCount() && this.j.b != null && i4 < this.j.b.size()) {
            NormalSmartCardGiftNode normalSmartCardGiftNode = (NormalSmartCardGiftNode) this.i.getChildAt(i4);
            if (this.j.b.get(i4) != null) {
                sTInfoV2 = a(i4, this.j.b.get(i4).f1589a);
            }
            normalSmartCardGiftNode.a(this.j.b.get(i4), sTInfoV2, i4 == this.i.getChildCount() + -1);
            i4++;
        }
    }

    private STInfoV2 a(int i2, SimpleAppModel simpleAppModel) {
        STInfoV2 a2 = a(c(i2), 100);
        if (!(a2 == null || simpleAppModel == null)) {
            a2.updateWithSimpleAppModel(simpleAppModel);
        }
        if (this.e == null) {
            this.e = new d();
        }
        this.e.a(a2);
        return a2;
    }

    private String c(int i2) {
        return "04_" + (i2 + 1);
    }

    /* access modifiers changed from: protected */
    public String b(int i2) {
        if (this.j != null) {
            return this.j.e;
        }
        return super.b(i2);
    }
}
