package com.tencent.assistant.manager.spaceclean;

import android.os.Bundle;
import com.qq.AppService.AstApp;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

/* compiled from: ProGuard */
class f implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Method f1620a;
    final /* synthetic */ Object b;
    final /* synthetic */ SpaceScanManager c;

    f(SpaceScanManager spaceScanManager, Method method, Object obj) {
        this.c = spaceScanManager;
        this.f1620a = method;
        this.b = obj;
    }

    public void run() {
        try {
            Bundle bundle = new Bundle();
            ArrayList arrayList = new ArrayList();
            arrayList.add(AstApp.i().getPackageName());
            arrayList.add("android.process.acore");
            arrayList.add("android.process.media");
            bundle.putStringArrayList("whitelist", arrayList);
            bundle.putBoolean("usePerRoot", false);
            this.f1620a.invoke(this.b, bundle);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e2) {
            e2.printStackTrace();
        } catch (InvocationTargetException e3) {
            e3.printStackTrace();
        }
    }
}
