package com.tencent.assistant.manager;

import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.assistant.module.callback.ai;
import java.util.List;

/* compiled from: ProGuard */
class i implements CallbackHelper.Caller<ai> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ j f1543a;
    final /* synthetic */ List b;
    final /* synthetic */ h c;

    i(h hVar, j jVar, List list) {
        this.c = hVar;
        this.f1543a = jVar;
        this.b = list;
    }

    /* renamed from: a */
    public void call(ai aiVar) {
        aiVar.a(this.f1543a.f1544a, this.f1543a.b, this.f1543a.c, this.f1543a.d, this.f1543a.e, this.f1543a.f, this.b);
    }
}
