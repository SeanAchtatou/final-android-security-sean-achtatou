package com.tencent.assistant.manager.notification;

import com.tencent.assistant.thumbnailCache.o;
import com.tencent.assistant.thumbnailCache.p;

/* compiled from: ProGuard */
class f implements p {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ d f1566a;

    f(d dVar) {
        this.f1566a = dVar;
    }

    public void thumbnailRequestCompleted(o oVar) {
        if (oVar == null) {
            this.f1566a.a(-4, null);
        } else if (oVar.f == null || oVar.f.isRecycled()) {
            this.f1566a.a(-5, oVar.f);
        } else {
            this.f1566a.a(0, oVar.f);
        }
    }

    public void thumbnailRequestFailed(o oVar) {
        this.f1566a.a(-2, null);
    }
}
