package com.tencent.assistant.manager;

import android.text.TextUtils;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.download.SimpleDownloadInfo;

/* compiled from: ProGuard */
class am {

    /* renamed from: a  reason: collision with root package name */
    DownloadInfo f1484a;
    SimpleDownloadInfo.DownloadState b;
    final /* synthetic */ DownloadProxy c;

    public am(DownloadProxy downloadProxy, DownloadInfo downloadInfo, SimpleDownloadInfo.DownloadState downloadState) {
        this.c = downloadProxy;
        this.f1484a = downloadInfo;
        this.b = downloadState;
    }

    public boolean a(am amVar) {
        return b(this) && b(amVar) && this.f1484a.packageName.equals(amVar.f1484a.packageName);
    }

    private boolean b(am amVar) {
        if (amVar == null || amVar.f1484a == null || !TextUtils.isEmpty(amVar.f1484a.packageName)) {
            return false;
        }
        return true;
    }
}
