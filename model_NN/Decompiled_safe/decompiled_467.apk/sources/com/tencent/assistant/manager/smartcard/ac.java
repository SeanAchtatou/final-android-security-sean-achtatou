package com.tencent.assistant.manager.smartcard;

import com.tencent.assistant.model.a.i;
import com.tencent.assistant.model.a.q;
import com.tencent.assistant.model.a.r;
import com.tencent.assistant.model.a.s;
import com.tencent.assistant.model.a.v;
import java.util.List;

/* compiled from: ProGuard */
public class ac extends y {
    public boolean a(i iVar, List<Long> list) {
        if (iVar == null || !(iVar instanceof v)) {
            return false;
        }
        v vVar = (v) iVar;
        return a(vVar, (r) this.f1610a.get(Integer.valueOf(vVar.k())), (s) this.b.get(Integer.valueOf(vVar.k())));
    }

    /* access modifiers changed from: protected */
    public boolean a(v vVar, r rVar, s sVar) {
        if (sVar == null) {
            return false;
        }
        if (rVar == null) {
            rVar = new r();
            rVar.f = vVar.j;
            rVar.e = vVar.i;
            this.f1610a.put(Integer.valueOf(rVar.a()), rVar);
        }
        if (System.currentTimeMillis() / 1000 > sVar.h) {
            return false;
        }
        if (rVar.c >= sVar.c) {
            a(vVar.s, vVar.j + "||" + vVar.i + "|" + 1, vVar.i);
            return false;
        } else if (rVar.f1651a < sVar.f1652a) {
            return true;
        } else {
            a(vVar.s, vVar.j + "||" + vVar.i + "|" + 2, vVar.i);
            return false;
        }
    }

    public void a(q qVar) {
        r rVar;
        int i;
        if (qVar != null) {
            r rVar2 = (r) this.f1610a.get(Integer.valueOf(qVar.a()));
            if (rVar2 == null) {
                r rVar3 = new r();
                rVar3.e = qVar.f1650a;
                rVar3.f = qVar.b;
                rVar = rVar3;
            } else {
                rVar = rVar2;
            }
            if (qVar.d) {
                rVar.d = true;
            }
            if (qVar.c) {
                if (a(qVar.e * 1000)) {
                    rVar.f1651a++;
                }
                s sVar = (s) this.b.get(Integer.valueOf(qVar.a()));
                if (sVar != null) {
                    i = sVar.i;
                } else {
                    i = 7;
                }
                if (a(qVar.e * 1000, (long) i)) {
                    rVar.b++;
                }
                rVar.c++;
            }
            this.f1610a.put(Integer.valueOf(rVar.a()), rVar);
        }
    }

    public void a(s sVar) {
        this.b.put(Integer.valueOf(sVar.a()), sVar);
    }

    public void a(i iVar) {
        if (iVar != null && iVar.i == 0) {
            v vVar = (v) iVar;
            s sVar = (s) this.b.get(Integer.valueOf(vVar.k()));
            if (sVar != null) {
                vVar.p = sVar.d;
            }
        }
    }
}
