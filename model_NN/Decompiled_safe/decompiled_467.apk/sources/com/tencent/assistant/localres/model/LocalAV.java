package com.tencent.assistant.localres.model;

/* compiled from: ProGuard */
public class LocalAV extends LocalMedia {
    public boolean alarmFlag;
    public String album;
    public String albumId;
    public String albumKey;
    public String artist;
    public String artistKey;
    public String audioAlbum;
    public String category;
    public String composer;
    public int createYear;
    public int datetaken;
    public String description;
    public int duration;
    public String language;
    public double latitude;
    public double longitude;
    public boolean musicFlag;
    public boolean notificationFlag;
    public boolean podcastFlag;
    public boolean privateFlag;
    public String resolution;
    public boolean ringtoneFlag;
    public String tags;
    public int type;
}
