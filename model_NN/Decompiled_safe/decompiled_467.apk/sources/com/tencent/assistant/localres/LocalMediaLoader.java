package com.tencent.assistant.localres;

import android.content.Context;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.ba;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: ProGuard */
public abstract class LocalMediaLoader<T> {
    public static final int LocalMediaType_AV = 6;
    public static final int LocalMediaType_Audio = 3;
    public static final int LocalMediaType_EBook = 4;
    public static final int LocalMediaType_Image = 1;
    public static final int LocalMediaType_Receive = 7;
    public static final int LocalMediaType_Ring = 5;
    public static final int LocalMediaType_Video = 2;

    /* renamed from: a  reason: collision with root package name */
    protected Context f1410a;
    protected ArrayList<T> b = new ArrayList<>();
    protected int c;
    protected ReferenceQueue<ILocalMediaLoaderListener<T>> d = new ReferenceQueue<>();
    protected ArrayList<WeakReference<ILocalMediaLoaderListener<T>>> e = new ArrayList<>();

    /* compiled from: ProGuard */
    public interface ILocalMediaLoaderListener<T> {
        void onLocalMediaLoaderFinish(LocalMediaLoader<T> localMediaLoader, ArrayList<T> arrayList, boolean z);

        void onLocalMediaLoaderOver();
    }

    /* access modifiers changed from: protected */
    public abstract void a();

    public LocalMediaLoader(Context context) {
        this.f1410a = context;
    }

    public void getLocalMediaList() {
        TemporaryThreadManager.get().start(new ak(this));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:?, code lost:
        r2 = r3.e.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0021, code lost:
        if (r2.hasNext() == false) goto L_0x0033;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002f, code lost:
        if (((com.tencent.assistant.localres.LocalMediaLoader.ILocalMediaLoaderListener) r2.next().get()) != r4) goto L_0x001d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0033, code lost:
        r3.e.add(new java.lang.ref.WeakReference(r4, r3.d));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void registerListener(com.tencent.assistant.localres.LocalMediaLoader.ILocalMediaLoaderListener<T> r4) {
        /*
            r3 = this;
            if (r4 != 0) goto L_0x0003
        L_0x0002:
            return
        L_0x0003:
            java.util.ArrayList<java.lang.ref.WeakReference<com.tencent.assistant.localres.LocalMediaLoader$ILocalMediaLoaderListener<T>>> r1 = r3.e
            monitor-enter(r1)
        L_0x0006:
            java.lang.ref.ReferenceQueue<com.tencent.assistant.localres.LocalMediaLoader$ILocalMediaLoaderListener<T>> r0 = r3.d     // Catch:{ all -> 0x0014 }
            java.lang.ref.Reference r0 = r0.poll()     // Catch:{ all -> 0x0014 }
            if (r0 == 0) goto L_0x0017
            java.util.ArrayList<java.lang.ref.WeakReference<com.tencent.assistant.localres.LocalMediaLoader$ILocalMediaLoaderListener<T>>> r2 = r3.e     // Catch:{ all -> 0x0014 }
            r2.remove(r0)     // Catch:{ all -> 0x0014 }
            goto L_0x0006
        L_0x0014:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0014 }
            throw r0
        L_0x0017:
            java.util.ArrayList<java.lang.ref.WeakReference<com.tencent.assistant.localres.LocalMediaLoader$ILocalMediaLoaderListener<T>>> r0 = r3.e     // Catch:{ all -> 0x0014 }
            java.util.Iterator r2 = r0.iterator()     // Catch:{ all -> 0x0014 }
        L_0x001d:
            boolean r0 = r2.hasNext()     // Catch:{ all -> 0x0014 }
            if (r0 == 0) goto L_0x0033
            java.lang.Object r0 = r2.next()     // Catch:{ all -> 0x0014 }
            java.lang.ref.WeakReference r0 = (java.lang.ref.WeakReference) r0     // Catch:{ all -> 0x0014 }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x0014 }
            com.tencent.assistant.localres.LocalMediaLoader$ILocalMediaLoaderListener r0 = (com.tencent.assistant.localres.LocalMediaLoader.ILocalMediaLoaderListener) r0     // Catch:{ all -> 0x0014 }
            if (r0 != r4) goto L_0x001d
            monitor-exit(r1)     // Catch:{ all -> 0x0014 }
            goto L_0x0002
        L_0x0033:
            java.lang.ref.WeakReference r0 = new java.lang.ref.WeakReference     // Catch:{ all -> 0x0014 }
            java.lang.ref.ReferenceQueue<com.tencent.assistant.localres.LocalMediaLoader$ILocalMediaLoaderListener<T>> r2 = r3.d     // Catch:{ all -> 0x0014 }
            r0.<init>(r4, r2)     // Catch:{ all -> 0x0014 }
            java.util.ArrayList<java.lang.ref.WeakReference<com.tencent.assistant.localres.LocalMediaLoader$ILocalMediaLoaderListener<T>>> r2 = r3.e     // Catch:{ all -> 0x0014 }
            r2.add(r0)     // Catch:{ all -> 0x0014 }
            monitor-exit(r1)     // Catch:{ all -> 0x0014 }
            goto L_0x0002
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.localres.LocalMediaLoader.registerListener(com.tencent.assistant.localres.LocalMediaLoader$ILocalMediaLoaderListener):void");
    }

    public void unregisterListener(ILocalMediaLoaderListener<T> iLocalMediaLoaderListener) {
        if (iLocalMediaLoaderListener != null) {
            synchronized (this.e) {
                Iterator<WeakReference<ILocalMediaLoaderListener<T>>> it = this.e.iterator();
                while (it.hasNext()) {
                    WeakReference next = it.next();
                    if (((ILocalMediaLoaderListener) next.get()) == iLocalMediaLoaderListener) {
                        this.e.remove(next);
                        return;
                    }
                }
            }
        }
    }

    public int getMediaType() {
        return this.c;
    }

    /* access modifiers changed from: protected */
    public void a(LocalMediaLoader<T> localMediaLoader, ArrayList<T> arrayList, boolean z) {
        ba.a().post(new al(this, localMediaLoader, arrayList, z));
    }

    /* access modifiers changed from: protected */
    public void b() {
        ba.a().post(new am(this));
    }
}
