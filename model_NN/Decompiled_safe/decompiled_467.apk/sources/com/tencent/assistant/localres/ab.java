package com.tencent.assistant.localres;

import com.tencent.assistant.localres.localapk.j;
import com.tencent.assistant.localres.model.LocalApkInfo;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: ProGuard */
class ab implements j {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ aa f1414a;

    ab(aa aaVar) {
        this.f1414a = aaVar;
    }

    public void a(LocalApkInfo localApkInfo) {
        boolean z;
        boolean z2 = false;
        if (localApkInfo != null) {
            if (!this.f1414a.c.contains(localApkInfo)) {
                this.f1414a.c.add(localApkInfo);
                this.f1414a.b.a(localApkInfo);
            } else {
                Iterator it = this.f1414a.c.iterator();
                boolean z3 = false;
                while (true) {
                    if (!it.hasNext()) {
                        z2 = z3;
                        break;
                    }
                    LocalApkInfo localApkInfo2 = (LocalApkInfo) it.next();
                    if (!localApkInfo2.equals(localApkInfo)) {
                        z = z3;
                    } else if (localApkInfo2.mLocalFilePath.equals(localApkInfo.mLocalFilePath)) {
                        break;
                    } else {
                        z = true;
                    }
                    z3 = z;
                }
                if (z2) {
                    this.f1414a.c.add(localApkInfo);
                    this.f1414a.b.a(localApkInfo);
                }
            }
            ArrayList arrayList = new ArrayList(this.f1414a.c);
            if (arrayList.size() != 0 && arrayList.size() % 4 == 0) {
                this.f1414a.a(arrayList);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.localres.aa.a(com.tencent.assistant.localres.aa, boolean, int):void
     arg types: [com.tencent.assistant.localres.aa, int, int]
     candidates:
      com.tencent.assistant.localres.aa.a(java.lang.String, int, int):com.tencent.assistant.localres.model.LocalApkInfo
      com.tencent.assistant.localres.aa.a(com.tencent.assistant.localres.aa, boolean, int):void */
    public void a(int i) {
        if (!this.f1414a.c.isEmpty()) {
            this.f1414a.b.b();
            this.f1414a.b.c();
            this.f1414a.a(true, 0);
            return;
        }
        this.f1414a.a(true, -1);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.localres.aj.a(com.tencent.assistant.localres.model.LocalApkInfo, boolean):void
     arg types: [com.tencent.assistant.localres.model.LocalApkInfo, int]
     candidates:
      com.tencent.assistant.localres.aj.a(java.util.List<com.tencent.assistant.localres.model.LocalApkInfo>, java.lang.String):int
      com.tencent.assistant.localres.aj.a(java.util.List<com.tencent.assistant.localres.model.LocalApkInfo>, boolean):java.util.ArrayList<com.tencent.assistant.localres.model.LocalApkInfo>
      com.tencent.assistant.localres.aj.a(java.util.List<com.tencent.assistant.localres.model.LocalApkInfo>, java.util.List<java.lang.Integer>):void
      com.tencent.assistant.localres.aj.a(com.tencent.assistant.localres.model.LocalApkInfo, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.localres.aa.a(com.tencent.assistant.localres.aa, boolean, int):void
     arg types: [com.tencent.assistant.localres.aa, int, int]
     candidates:
      com.tencent.assistant.localres.aa.a(java.lang.String, int, int):com.tencent.assistant.localres.model.LocalApkInfo
      com.tencent.assistant.localres.aa.a(com.tencent.assistant.localres.aa, boolean, int):void */
    public void a(int i, String str, LocalApkInfo localApkInfo) {
        if (localApkInfo != null && this.f1414a.e == i) {
            localApkInfo.mApkState = 2;
            this.f1414a.c.add(0, localApkInfo);
            this.f1414a.f1413a.a(localApkInfo);
            this.f1414a.b.a(localApkInfo, false);
            this.f1414a.b.b();
            this.f1414a.b.c();
            this.f1414a.a(false, 0);
        }
    }
}
