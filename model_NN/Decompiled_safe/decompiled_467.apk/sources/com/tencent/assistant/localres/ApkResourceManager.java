package com.tencent.assistant.localres;

import android.content.Context;
import android.content.IntentFilter;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.db.table.n;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.download.SimpleDownloadInfo;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistant.localres.callback.b;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.DownloadProxy;
import com.tencent.assistant.module.a.a.a;
import com.tencent.assistant.receiver.PackageChangedReceiver;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.e;
import com.tencent.assistantv2.st.k;
import com.tencent.connect.common.Constants;
import java.io.File;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;

/* compiled from: ProGuard */
public class ApkResourceManager {
    public static final int APK_CHANGED_TYPE_ADD = 1;
    public static final int APK_CHANGED_TYPE_REMOVE = 2;
    public static final int APK_CHANGED_TYPE_REPLACE = 3;
    public static final int APK_CHANGED_TYPE_UPDATE = 4;
    public static final int ERROR_CODE = -1;
    public static final String ERROR_MSG = "unknow error";
    /* access modifiers changed from: private */
    public static volatile boolean j = false;
    private static volatile boolean k = true;
    /* access modifiers changed from: private */
    public static volatile boolean l = false;
    private static volatile boolean m = false;
    private static ApkResourceManager n;

    /* renamed from: a  reason: collision with root package name */
    UIEventListener f1409a = new a(this);
    private Context b;
    /* access modifiers changed from: private */
    public n c;
    private ReferenceQueue<ApkResCallback> d = new ReferenceQueue<>();
    /* access modifiers changed from: private */
    public ConcurrentLinkedQueue<WeakReference<ApkResCallback>> e = new ConcurrentLinkedQueue<>();
    /* access modifiers changed from: private */
    public z f = new z();
    /* access modifiers changed from: private */
    public aa g;
    /* access modifiers changed from: private */
    public Map<String, LocalApkInfo> h = Collections.synchronizedMap(new HashMap());
    private Map<Long, String> i = new HashMap();
    private k o = new k(this);
    private PackageChangedReceiver p = new PackageChangedReceiver();
    private b q = new c(this);

    private void a() {
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_GOFRONT, this.f1409a);
        AstApp.i().k().addUIEventListener(1032, this.f1409a);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_SUCC, this.f1409a);
    }

    private ApkResourceManager() {
    }

    public static synchronized ApkResourceManager getInstance() {
        ApkResourceManager apkResourceManager;
        synchronized (ApkResourceManager.class) {
            if (n == null) {
                n = new ApkResourceManager();
                n.a();
            }
            apkResourceManager = n;
        }
        return apkResourceManager;
    }

    public void init(Context context) {
        if (!m) {
            m = true;
            this.b = context;
            b();
            this.c = new n(AstApp.i());
            if (!j) {
                d();
            }
            c();
            this.g.b();
        }
    }

    private void b() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.PACKAGE_ADDED");
        intentFilter.addAction("android.intent.action.PACKAGE_INSTALL");
        intentFilter.addAction("android.intent.action.PACKAGE_REMOVED");
        intentFilter.addAction("android.intent.action.PACKAGE_REPLACED");
        intentFilter.addDataScheme("package");
        this.b.registerReceiver(this.p, intentFilter);
    }

    public void reset() {
        this.e.clear();
    }

    public aa getLocalApkLoader() {
        if (this.g == null) {
            c();
        }
        return this.g;
    }

    public LocalApkInfo getLocalApkInfo(String str, int i2, int i3) {
        if (this.g == null) {
            return null;
        }
        return this.g.a(str, i2, i3);
    }

    public LocalApkInfo getLocalApkInfoFromCacheOrByFileName(String str, int i2, int i3) {
        LocalApkInfo localApkInfo = getLocalApkInfo(str, i2, i3);
        if (localApkInfo == null) {
            return getLocalApkInfoByFileName(str, i2, i3);
        }
        return localApkInfo;
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x004f  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x00a9  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00da  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.tencent.assistant.localres.model.LocalApkInfo getLocalApkInfoByFileName(java.lang.String r8, int r9, int r10) {
        /*
            r7 = this;
            r3 = 0
            boolean r0 = android.text.TextUtils.isEmpty(r8)
            if (r0 == 0) goto L_0x0009
            r0 = r3
        L_0x0008:
            return r0
        L_0x0009:
            java.lang.String r1 = com.tencent.assistant.utils.FileUtil.getDynamicAPKDir()
            java.lang.String r2 = com.tencent.assistant.utils.FileUtil.getAPKDir()
            r0 = 2
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ Throwable -> 0x00d5 }
            r4 = 0
            java.lang.String r5 = "apk"
            r0[r4] = r5     // Catch:{ Throwable -> 0x00d5 }
            r4 = 1
            java.lang.String r5 = "APK"
            r0[r4] = r5     // Catch:{ Throwable -> 0x00d5 }
            java.util.List r0 = java.util.Arrays.asList(r0)     // Catch:{ Throwable -> 0x00d5 }
            java.util.List r0 = com.tencent.assistant.utils.FileUtil.scanFile(r2, r0)     // Catch:{ Throwable -> 0x00d5 }
            boolean r2 = r2.equals(r1)     // Catch:{ Throwable -> 0x011a }
            if (r2 != 0) goto L_0x0044
            r2 = 2
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ Throwable -> 0x011a }
            r4 = 0
            java.lang.String r5 = "apk"
            r2[r4] = r5     // Catch:{ Throwable -> 0x011a }
            r4 = 1
            java.lang.String r5 = "APK"
            r2[r4] = r5     // Catch:{ Throwable -> 0x011a }
            java.util.List r2 = java.util.Arrays.asList(r2)     // Catch:{ Throwable -> 0x011a }
            java.util.List r1 = com.tencent.assistant.utils.FileUtil.scanFile(r1, r2)     // Catch:{ Throwable -> 0x011a }
            r0.addAll(r1)     // Catch:{ Throwable -> 0x011a }
        L_0x0044:
            r4 = r0
        L_0x0045:
            if (r4 == 0) goto L_0x0117
            int r0 = r4.size()
            if (r0 <= 0) goto L_0x0117
            if (r10 <= 0) goto L_0x00da
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.StringBuilder r0 = r0.append(r8)
            java.lang.String r1 = "_"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r9)
            java.lang.String r1 = "_"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r10)
            java.lang.String r1 = ".apk"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = r0.toString()
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.StringBuilder r0 = r0.append(r8)
            java.lang.String r2 = "_"
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.StringBuilder r0 = r0.append(r9)
            java.lang.String r2 = "_"
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.StringBuilder r0 = r0.append(r10)
            java.lang.String r2 = "_2.apk"
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
            r2 = r1
            r1 = r0
        L_0x009f:
            java.util.Iterator r4 = r4.iterator()
        L_0x00a3:
            boolean r0 = r4.hasNext()
            if (r0 == 0) goto L_0x0117
            java.lang.Object r0 = r4.next()
            java.lang.String r0 = (java.lang.String) r0
            boolean r5 = android.text.TextUtils.isEmpty(r0)
            if (r5 != 0) goto L_0x00a3
            java.lang.String r5 = r0.toLowerCase()
            boolean r6 = r5.contains(r2)
            if (r6 != 0) goto L_0x00c5
            boolean r5 = r5.contains(r1)
            if (r5 == 0) goto L_0x00a3
        L_0x00c5:
            com.tencent.assistant.localres.model.LocalApkInfo r0 = com.tencent.assistant.utils.e.c(r0)
            if (r0 == 0) goto L_0x00a3
            java.lang.String r5 = r0.mPackageName
            boolean r5 = r8.equals(r5)
            if (r5 == 0) goto L_0x00a3
            goto L_0x0008
        L_0x00d5:
            r0 = move-exception
            r0 = r3
        L_0x00d7:
            r4 = r0
            goto L_0x0045
        L_0x00da:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.StringBuilder r0 = r0.append(r8)
            java.lang.String r1 = "_"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r9)
            java.lang.String r1 = ".apk"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = r0.toString()
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.StringBuilder r0 = r0.append(r8)
            java.lang.String r2 = "_"
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.StringBuilder r0 = r0.append(r9)
            java.lang.String r2 = "_2.apk"
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
            r2 = r1
            r1 = r0
            goto L_0x009f
        L_0x0117:
            r0 = r3
            goto L_0x0008
        L_0x011a:
            r1 = move-exception
            goto L_0x00d7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.localres.ApkResourceManager.getLocalApkInfoByFileName(java.lang.String, int, int):com.tencent.assistant.localres.model.LocalApkInfo");
    }

    public String getDdownloadTicket(LocalApkInfo localApkInfo) {
        ArrayList<DownloadInfo> a2;
        String str = Constants.STR_EMPTY;
        if (!(localApkInfo == null || TextUtils.isEmpty(localApkInfo.mPackageName) || (a2 = DownloadProxy.a().a(SimpleDownloadInfo.DownloadType.APK)) == null)) {
            Iterator<DownloadInfo> it = a2.iterator();
            while (it.hasNext()) {
                DownloadInfo next = it.next();
                str = a(next, localApkInfo) ? next.downloadTicket : str;
            }
        }
        return str;
    }

    public String isApkFileValid(String str, int i2, int i3) {
        String str2;
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        if (this.g == null) {
            c();
        }
        LocalApkInfo a2 = this.g.a(str, i2, i3);
        if (a2 != null) {
            str2 = a2.mLocalFilePath;
        } else {
            str2 = null;
        }
        LocalApkInfo b2 = this.g.b(str2);
        if (b2 != null && !TextUtils.isEmpty(b2.mPackageName) && b2.mPackageName.equals(str) && b2.mVersionCode == i2) {
            return b2.mLocalFilePath;
        }
        LocalApkInfo localApkInfoByFileName = getLocalApkInfoByFileName(str, i2, i3);
        if (localApkInfoByFileName != null) {
            return localApkInfoByFileName.mLocalFilePath;
        }
        return null;
    }

    private boolean a(DownloadInfo downloadInfo, LocalApkInfo localApkInfo) {
        if (downloadInfo == null || localApkInfo == null || downloadInfo.packageName == null || !downloadInfo.packageName.equals(localApkInfo.mPackageName) || downloadInfo.versionCode != localApkInfo.mVersionCode || downloadInfo.grayVersionCode != localApkInfo.mGrayVersionCode) {
            return false;
        }
        return true;
    }

    private void c() {
        this.g = new aa();
        this.g.a(this.q);
    }

    public List<LocalApkInfo> getLocalApkInfos() {
        if (this.b == null) {
            this.b = AstApp.i();
        }
        if (!l) {
            return null;
        }
        if (this.h != null && this.h.size() > 0) {
            return new ArrayList(this.h.values());
        }
        if (j) {
            return null;
        }
        TemporaryThreadManager.get().start(new d(this));
        return null;
    }

    public LocalApkInfo getLocalApkInfo(String str) {
        return this.h.get(str);
    }

    public boolean hasLocalPack(String str) {
        LocalApkInfo localApkInfo;
        if (!TextUtils.isEmpty(str) && (localApkInfo = getLocalApkInfo(str)) != null) {
            return new File(localApkInfo.mLocalFilePath).exists();
        }
        return false;
    }

    public LocalApkInfo getLocalApkInfo(long j2) {
        String str = this.i.get(Long.valueOf(j2));
        if (str != null) {
            return this.h.get(str);
        }
        return null;
    }

    public void registerApkResCallback(ApkResCallback apkResCallback, boolean z) {
        boolean z2;
        if (apkResCallback != null) {
            while (true) {
                WeakReference weakReference = (WeakReference) this.d.poll();
                if (weakReference == null) {
                    break;
                }
                this.e.remove(weakReference);
            }
            Iterator<WeakReference<ApkResCallback>> it = this.e.iterator();
            while (true) {
                if (it.hasNext()) {
                    if (((ApkResCallback) it.next().get()) == apkResCallback) {
                        z2 = false;
                        break;
                    }
                } else {
                    z2 = true;
                    break;
                }
            }
            if (z2) {
                this.e.add(new WeakReference(apkResCallback, this.d));
            }
            if (l) {
                if (z) {
                    apkResCallback.onLoadInstalledApkSuccess(new ArrayList(this.h.values()));
                    apkResCallback.onLoadInstalledApkExtraSuccess(new ArrayList(this.h.values()));
                }
            } else if (!j && z) {
                new a().run();
            }
        }
    }

    public void registerApkResCallback(ApkResCallback apkResCallback) {
        registerApkResCallback(apkResCallback, true);
    }

    public void unRegisterApkResCallback(ApkResCallback apkResCallback) {
        if (apkResCallback != null) {
            Iterator<WeakReference<ApkResCallback>> it = this.e.iterator();
            while (it.hasNext()) {
                WeakReference next = it.next();
                ApkResCallback apkResCallback2 = (ApkResCallback) next.get();
                if (apkResCallback2 != null && apkResCallback2 == apkResCallback) {
                    this.e.remove(next);
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void d() {
        if (!j && !l) {
            j = true;
            if (k) {
                if (this.c == null) {
                    this.c = new n(AstApp.i());
                }
                List<LocalApkInfo> a2 = this.c.a(1);
                if (a2 != null && !a2.isEmpty()) {
                    for (LocalApkInfo next : a2) {
                        if (this.h != null) {
                            this.h.put(next.mPackageName, next);
                        }
                        if (next.mAppid > 0) {
                            this.i.put(Long.valueOf(next.mAppid), next.mPackageName);
                        }
                    }
                }
                k = false;
            }
            if (this.h != null && this.h.size() == 0) {
                Map<String, LocalApkInfo> a3 = this.f.a(this.h, true);
                this.h.clear();
                this.h.putAll(a3);
                f();
            }
            e();
        }
    }

    private void e() {
        TemporaryThreadManager.get().start(new e(this));
    }

    public boolean isLocalApkDataReady() {
        return l;
    }

    /* access modifiers changed from: private */
    public void f() {
        Iterator<WeakReference<ApkResCallback>> it = this.e.iterator();
        while (it.hasNext()) {
            ApkResCallback apkResCallback = (ApkResCallback) it.next().get();
            if (apkResCallback != null) {
                apkResCallback.onLoadInstalledApkSuccess(new ArrayList(this.h.values()));
            }
        }
    }

    /* access modifiers changed from: private */
    public void g() {
        Iterator<WeakReference<ApkResCallback>> it = this.e.iterator();
        while (it.hasNext()) {
            ApkResCallback apkResCallback = (ApkResCallback) it.next().get();
            if (apkResCallback != null) {
                apkResCallback.onLoadInstalledApkExtraSuccess(new ArrayList(this.h.values()));
            }
        }
    }

    /* access modifiers changed from: private */
    public void h() {
        Iterator<WeakReference<ApkResCallback>> it = this.e.iterator();
        while (it.hasNext()) {
            ApkResCallback apkResCallback = (ApkResCallback) it.next().get();
            if (apkResCallback != null) {
                apkResCallback.onInstalledAppTimeUpdate(new ArrayList(this.h.values()));
            }
        }
    }

    /* access modifiers changed from: private */
    public void i() {
        Iterator<WeakReference<ApkResCallback>> it = this.e.iterator();
        while (it.hasNext()) {
            ApkResCallback apkResCallback = (ApkResCallback) it.next().get();
            if (apkResCallback != null) {
                apkResCallback.onLoadInstalledApkFail(-1, ERROR_MSG);
            }
        }
    }

    public void exit() {
        this.e.clear();
    }

    public void onPackageAdded(String str, boolean z) {
        TemporaryThreadManager.get().start(new f(this, str, z));
    }

    public void onPackageRemoved(String str, boolean z) {
        TemporaryThreadManager.get().start(new g(this, str, z));
    }

    public void onPackageReplaced(String str, boolean z) {
        TemporaryThreadManager.get().start(new h(this, str, z));
    }

    public void updateAppId(HashMap<String, Long> hashMap) {
        if (this.h != null && !this.h.isEmpty()) {
            for (Map.Entry next : hashMap.entrySet()) {
                long longValue = ((Long) next.getValue()).longValue();
                LocalApkInfo localApkInfo = this.h.get((String) next.getKey());
                if (localApkInfo != null) {
                    localApkInfo.mAppid = longValue;
                    if (localApkInfo.mAppid > 0) {
                        this.i.put(Long.valueOf(localApkInfo.mAppid), localApkInfo.mPackageName);
                    }
                }
            }
            this.c.b(new ArrayList(this.h.values()));
        }
    }

    public void updateAppLauncherTime() {
        c(false);
    }

    /* access modifiers changed from: private */
    public void c(boolean z) {
        TemporaryThreadManager.get().start(new i(this, System.currentTimeMillis(), z));
    }

    public void updateAppLauncherTime(List<String> list) {
        TemporaryThreadManager.get().start(new j(this, list, System.currentTimeMillis()));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean
      com.tencent.assistant.m.a(java.lang.String, long):long */
    /* access modifiers changed from: private */
    public void j() {
        long a2 = m.a().a("key_first_load_installed_app_time", 0L);
        for (LocalApkInfo next : this.h.values()) {
            if (next.mLastLaunchTime == -1) {
                next.mLastLaunchTime = 0;
            }
            if (next.mFakeLastLaunchTime <= 0) {
                next.mFakeLastLaunchTime = a2;
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(LocalApkInfo localApkInfo) {
        Iterator<WeakReference<ApkResCallback>> it = this.e.iterator();
        while (it.hasNext()) {
            ApkResCallback apkResCallback = (ApkResCallback) it.next().get();
            if (apkResCallback != null) {
                apkResCallback.onGetPkgSizeFinish(localApkInfo);
            }
        }
    }

    public long getPkgSize(String str) {
        LocalApkInfo localApkInfo = this.h.get(str);
        if (localApkInfo != null && localApkInfo.occupySize > 0) {
            return localApkInfo.occupySize;
        }
        e.a(AstApp.i(), str, this.o);
        return -1;
    }

    public void getSelfInfo(com.tencent.assistant.localres.callback.a aVar) {
        LocalApkInfo localApkInfo;
        if (aVar != null) {
            String packageName = AstApp.i().getPackageName();
            if (!(this.h == null || (localApkInfo = this.h.get(packageName)) == null)) {
                if (localApkInfo.mInstallDate == new File(localApkInfo.mLocalFilePath).lastModified() && !TextUtils.isEmpty(localApkInfo.manifestMd5)) {
                    aVar.a(localApkInfo);
                    return;
                }
            }
            TemporaryThreadManager.get().start(new b(this, packageName, aVar));
        }
    }

    public static void loadTraffic() {
        List<com.tencent.assistant.localres.model.a> e2 = e.e();
        com.tencent.assistant.db.table.b bVar = new com.tencent.assistant.db.table.b();
        bVar.a(e2);
        k.a(bVar.a());
    }

    public static void loadAppLauncherTime() {
        new n(AstApp.i()).a(e.b(AstApp.i()), System.currentTimeMillis());
    }

    public LocalApkInfo getInstalledApkInfo(String str) {
        return getInstalledApkInfo(str, false);
    }

    public LocalApkInfo getInstalledApkInfo(String str, boolean z) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        LocalApkInfo localApkInfo = this.h.get(str);
        if ((localApkInfo != null && (!z || !TextUtils.isEmpty(localApkInfo.manifestMd5))) || l) {
            return localApkInfo;
        }
        LocalApkInfo b2 = e.b(str);
        if (b2 == null) {
            return b2;
        }
        this.h.put(str, b2);
        return b2;
    }
}
