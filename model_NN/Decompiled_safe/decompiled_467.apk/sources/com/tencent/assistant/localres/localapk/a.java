package com.tencent.assistant.localres.localapk;

import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.localapk.loadapkservice.GetApkInfoService;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.localres.x;
import com.tencent.assistant.manager.be;
import com.tencent.assistant.utils.FileUtil;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.b;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: ProGuard */
public class a extends x<j> {
    /* access modifiers changed from: private */
    public static final String c = a.class.getSimpleName();

    /* renamed from: a  reason: collision with root package name */
    public boolean f1437a = false;
    public boolean b = false;
    /* access modifiers changed from: private */
    public Map<String, LocalApkInfo> d = Collections.synchronizedMap(new HashMap());
    /* access modifiers changed from: private */
    public Map<String, LocalApkInfo> e = Collections.synchronizedMap(new HashMap());
    /* access modifiers changed from: private */
    public int f = 0;
    /* access modifiers changed from: private */
    public int g = 0;
    private int h = 0;
    private int i = 0;
    /* access modifiers changed from: private */
    public Messenger j = null;
    /* access modifiers changed from: private */
    public boolean k;
    private ServiceConnection l = new d(this);
    private final Messenger m = new Messenger(new i(Looper.getMainLooper(), this));

    static /* synthetic */ int c(a aVar) {
        int i2 = aVar.h;
        aVar.h = i2 + 1;
        return i2;
    }

    public a() {
        c();
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        super.finalize();
        d();
    }

    public void a(int i2) {
        if (!this.f1437a) {
            this.f1437a = true;
            this.h = 0;
            this.f++;
            this.g = i2;
            TemporaryThreadManager.get().start(new b(this, i2));
        }
    }

    public void a() {
        this.f1437a = false;
    }

    public LocalApkInfo a(String str, int i2, int i3, int i4) {
        LocalApkInfo localApkInfo = null;
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        String a2 = a(str, i2, i3);
        if ((i4 & 1) != 0) {
            localApkInfo = this.d.get(a2);
        }
        if (localApkInfo != null || (i4 & 2) == 0) {
            return localApkInfo;
        }
        return this.e.get(a2);
    }

    public boolean a(String str) {
        if (!TextUtils.isEmpty(str)) {
            return new File(str).exists();
        }
        return false;
    }

    public void a(LocalApkInfo localApkInfo) {
        if (localApkInfo != null) {
            this.d.put(a(localApkInfo.mPackageName, localApkInfo.mVersionCode, localApkInfo.mGrayVersionCode), localApkInfo);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x008b  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00a2 A[SYNTHETIC, Splitter:B:32:0x00a2] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00af A[SYNTHETIC, Splitter:B:39:0x00af] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00b8  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.tencent.assistant.localres.model.LocalApkInfo a(java.lang.String r12, int r13) {
        /*
            r11 = this;
            r3 = 0
            r9 = 1
            r8 = 0
            com.qq.AppService.AstApp r0 = com.qq.AppService.AstApp.i()
            android.content.Context r0 = r0.getBaseContext()
            android.content.pm.PackageManager r0 = r0.getPackageManager()
            android.content.pm.PackageInfo r1 = com.tencent.assistant.utils.e.a(r0, r12, r8)
            if (r1 == 0) goto L_0x00c1
            com.tencent.assistant.localres.model.LocalApkInfo r2 = new com.tencent.assistant.localres.model.LocalApkInfo
            r2.<init>()
            android.content.pm.ApplicationInfo r4 = r1.applicationInfo
            r4.sourceDir = r12
            r4.publicSourceDir = r12
            int r5 = r4.icon
            r2.mAppIconRes = r5
            java.lang.String r5 = r4.packageName
            r2.mPackageName = r5
            int r5 = r1.versionCode
            r2.mVersionCode = r5
            java.lang.String r5 = r1.versionName
            r2.mVersionName = r5
            r2.mIsSelect = r8
            r2.mLocalFilePath = r12
            android.content.pm.Signature[] r5 = r1.signatures
            if (r5 == 0) goto L_0x0050
            android.content.pm.Signature[] r5 = r1.signatures
            int r5 = r5.length
            if (r5 <= 0) goto L_0x0050
            android.content.pm.Signature[] r5 = r1.signatures
            android.content.pm.Signature[] r1 = r1.signatures
            int r1 = r1.length
            int r1 = r1 + -1
            r1 = r5[r1]
            java.lang.String r1 = r1.toCharsString()
            java.lang.String r1 = com.tencent.assistant.utils.bj.b(r1)
            r2.signature = r1
        L_0x0050:
            int r1 = com.tencent.assistant.utils.e.b(r0, r12)
            r2.mGrayVersionCode = r1
            java.lang.CharSequence r0 = r0.getApplicationLabel(r4)
            if (r0 != 0) goto L_0x008f
            java.lang.String r0 = "未知"
        L_0x005e:
            r2.mAppName = r0
            java.io.File r5 = new java.io.File
            r5.<init>(r12)
            long r0 = r5.length()
            r6 = 0
            int r4 = (r0 > r6 ? 1 : (r0 == r6 ? 0 : -1))
            if (r4 > 0) goto L_0x007e
            java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0099, all -> 0x00ab }
            r4.<init>(r5)     // Catch:{ Exception -> 0x0099, all -> 0x00ab }
            int r0 = r4.available()     // Catch:{ Exception -> 0x00c5 }
            long r0 = (long) r0
            if (r4 == 0) goto L_0x007e
            r4.close()     // Catch:{ IOException -> 0x0094 }
        L_0x007e:
            r2.occupySize = r0
            long r0 = r5.lastModified()
            r2.mLastModified = r0
            r11.d(r2)
            if (r13 != r9) goto L_0x00b8
            r2.mIsInternalDownload = r9
        L_0x008d:
            r0 = r2
        L_0x008e:
            return r0
        L_0x008f:
            java.lang.String r0 = r0.toString()
            goto L_0x005e
        L_0x0094:
            r3 = move-exception
            r3.printStackTrace()
            goto L_0x007e
        L_0x0099:
            r4 = move-exception
            r10 = r4
            r4 = r3
            r3 = r10
        L_0x009d:
            r3.printStackTrace()     // Catch:{ all -> 0x00c3 }
            if (r4 == 0) goto L_0x007e
            r4.close()     // Catch:{ IOException -> 0x00a6 }
            goto L_0x007e
        L_0x00a6:
            r3 = move-exception
            r3.printStackTrace()
            goto L_0x007e
        L_0x00ab:
            r0 = move-exception
            r4 = r3
        L_0x00ad:
            if (r4 == 0) goto L_0x00b2
            r4.close()     // Catch:{ IOException -> 0x00b3 }
        L_0x00b2:
            throw r0
        L_0x00b3:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00b2
        L_0x00b8:
            r0 = 2
            if (r13 != r0) goto L_0x00be
            r2.mIsInternalDownload = r8
            goto L_0x008d
        L_0x00be:
            r2.mIsInternalDownload = r8
            goto L_0x008d
        L_0x00c1:
            r0 = r3
            goto L_0x008e
        L_0x00c3:
            r0 = move-exception
            goto L_0x00ad
        L_0x00c5:
            r3 = move-exception
            goto L_0x009d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.localres.localapk.a.a(java.lang.String, int):com.tencent.assistant.localres.model.LocalApkInfo");
    }

    public boolean b(LocalApkInfo localApkInfo) {
        if (localApkInfo == null) {
            return false;
        }
        if (!TextUtils.isEmpty(localApkInfo.mLocalFilePath)) {
            File file = new File(localApkInfo.mLocalFilePath);
            if (file.exists()) {
                return file.delete();
            }
        }
        return true;
    }

    public void c(LocalApkInfo localApkInfo) {
        if (localApkInfo != null) {
            String a2 = a(localApkInfo.mPackageName, localApkInfo.mVersionCode, localApkInfo.mGrayVersionCode);
            if (localApkInfo.mIsInternalDownload) {
                this.d.remove(a2);
            } else {
                this.e.remove(a2);
            }
        }
    }

    /* access modifiers changed from: private */
    public void d(LocalApkInfo localApkInfo) {
        LocalApkInfo localApkInfo2 = ApkResourceManager.getInstance().getLocalApkInfo(localApkInfo.mPackageName);
        if (localApkInfo2 == null) {
            localApkInfo.mInstall = false;
            localApkInfo.mIsUpdateApk = false;
            if (be.a().c(localApkInfo.mPackageName) && be.a().d(localApkInfo.mPackageName)) {
                localApkInfo.mInstall = true;
            }
        } else if (localApkInfo.mVersionCode < localApkInfo2.mVersionCode) {
            localApkInfo.mInstall = true;
            localApkInfo.mIsUpdateApk = false;
        } else if (localApkInfo.mVersionCode > localApkInfo2.mVersionCode) {
            localApkInfo.mInstall = false;
            localApkInfo.mIsUpdateApk = true;
        } else if (localApkInfo.mGrayVersionCode == 0 || localApkInfo2.mGrayVersionCode == 0) {
            if (localApkInfo.mGrayVersionCode == 0 && localApkInfo2.mGrayVersionCode == 0) {
                localApkInfo.mInstall = true;
                localApkInfo.mIsUpdateApk = false;
                return;
            }
            localApkInfo.mInstall = false;
            localApkInfo.mIsUpdateApk = true;
        } else if (localApkInfo.mGrayVersionCode > localApkInfo2.mGrayVersionCode) {
            localApkInfo.mInstall = false;
            localApkInfo.mIsUpdateApk = true;
        } else {
            localApkInfo.mInstall = true;
            localApkInfo.mIsUpdateApk = false;
        }
    }

    private ArrayList<String> b(int i2) {
        ArrayList<String> arrayList = new ArrayList<>();
        if (i2 == 1) {
            String aPKDir = FileUtil.getAPKDir();
            if (!TextUtils.isEmpty(aPKDir)) {
                arrayList.add(aPKDir);
            }
            for (String next : b.c()) {
                if (!next.equals(aPKDir)) {
                    arrayList.add(next);
                }
            }
        } else if (i2 == 2) {
            List asList = Arrays.asList(com.tencent.assistant.utils.b.a.a(0));
            if (!asList.isEmpty()) {
                arrayList.addAll(asList);
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: private */
    public String a(String str, int i2, int i3) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        return str + i2 + i3;
    }

    /* access modifiers changed from: private */
    public void c() {
        try {
            AstApp i2 = AstApp.i();
            i2.bindService(new Intent(i2, GetApkInfoService.class), this.l, 1);
            this.i = 0;
        } catch (Exception e2) {
            e2.printStackTrace();
            this.i++;
            if (this.i < 2) {
                new Handler(Looper.getMainLooper()).postDelayed(new c(this), 500);
            }
        }
    }

    private void d() {
        XLog.d(c, "unbindService mBound: " + this.k);
        if (this.k) {
            AstApp.i().unbindService(this.l);
            this.k = false;
        }
    }

    private void a(int i2, Bundle bundle) {
        if (!this.k) {
            this.b = true;
            c();
            this.h = 0;
            XLog.e(c, "GetApkInfoService is not bound");
            return;
        }
        Message obtain = Message.obtain(null, i2, this.f, 0);
        obtain.setData(bundle);
        obtain.replyTo = this.m;
        XLog.d(c, "sendMsgToService msg.what" + i2 + " requestId: " + this.f);
        try {
            this.j.send(obtain);
        } catch (RemoteException e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void c(int i2) {
        Bundle bundle = new Bundle();
        if ((i2 & 1) != 0) {
            bundle.putStringArrayList("indirs", b(1));
            if (!this.d.isEmpty()) {
                this.d.clear();
            }
        }
        if ((i2 & 2) != 0) {
            bundle.putStringArrayList("exdirs", b(2));
            if (!this.e.isEmpty()) {
                this.e.clear();
            }
        }
        a(2, bundle);
    }

    public void a(int i2, String str, boolean z) {
        Bundle bundle = new Bundle();
        bundle.putInt("actionid", i2);
        bundle.putString("apkpath", str);
        bundle.putBoolean("isinternal", z);
        a(3, bundle);
    }

    /* access modifiers changed from: private */
    public void e(LocalApkInfo localApkInfo) {
        b(new f(this, localApkInfo));
    }

    /* access modifiers changed from: private */
    public void d(int i2) {
        b(new g(this, i2));
    }

    /* access modifiers changed from: private */
    public void a(int i2, String str, LocalApkInfo localApkInfo) {
        b(new h(this, i2, str, localApkInfo));
    }
}
