package com.tencent.assistant.localres;

import com.tencent.assistant.localres.model.LocalApkInfo;
import java.util.Comparator;

/* compiled from: ProGuard */
final class t implements Comparator<LocalApkInfo> {
    t() {
    }

    /* renamed from: a */
    public int compare(LocalApkInfo localApkInfo, LocalApkInfo localApkInfo2) {
        if (localApkInfo == null || localApkInfo2 == null) {
            return 0;
        }
        long j = localApkInfo2.mLastModified - localApkInfo.mLastModified;
        if (j > 0) {
            return 1;
        }
        if (j != 0) {
            return -1;
        }
        return 0;
    }
}
