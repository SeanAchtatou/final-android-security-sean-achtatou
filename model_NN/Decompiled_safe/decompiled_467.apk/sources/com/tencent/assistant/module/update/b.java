package com.tencent.assistant.module.update;

import android.text.TextUtils;
import com.tencent.assistant.manager.as;
import com.tencent.assistant.protocol.jce.AppUpdateInfo;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

/* compiled from: ProGuard */
public class b {

    /* renamed from: a  reason: collision with root package name */
    private Map<String, AppUpdateInfo> f1879a = Collections.synchronizedMap(new LinkedHashMap());
    private Map<String, ArrayList<Integer>> b = new Hashtable();
    private Map<String, AppUpdateInfo> c;

    private void c() {
        this.f1879a.clear();
        if (this.c != null) {
            this.c.clear();
        }
    }

    private boolean e(String str) {
        return TextUtils.isEmpty(str);
    }

    public AppUpdateInfo a(String str) {
        if (e(str)) {
            return null;
        }
        return this.f1879a.get(str);
    }

    public ArrayList<Integer> b(String str) {
        return this.b.get(str);
    }

    public boolean c(String str) {
        return !TextUtils.isEmpty(str) && this.f1879a.containsKey(str);
    }

    public void d(String str) {
        if (!e(str) && this.f1879a.containsKey(str)) {
            AppUpdateInfo remove = this.f1879a.remove(str);
            if (this.c == null) {
                this.c = Collections.synchronizedMap(new HashMap());
            }
            this.c.put(str, remove);
        }
    }

    public void a(Map<Integer, ArrayList<AppUpdateInfo>> map) {
        c();
        if (map != null && !map.isEmpty()) {
            int i = 1;
            while (true) {
                int i2 = i;
                if (i2 <= 3) {
                    List<AppUpdateInfo> list = map.get(Integer.valueOf(i2));
                    if (list != null && !list.isEmpty()) {
                        for (AppUpdateInfo appUpdateInfo : list) {
                            if (!e(appUpdateInfo.a())) {
                                this.f1879a.put(appUpdateInfo.f2006a, appUpdateInfo);
                                a(appUpdateInfo.f2006a, i2);
                            }
                        }
                    }
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }

    public void a(int i, List<AppUpdateInfo> list) {
        if (list != null && !list.isEmpty()) {
            if (this.f1879a == null) {
                this.f1879a = Collections.synchronizedMap(new LinkedHashMap());
            }
            for (AppUpdateInfo next : list) {
                if (next != null && !TextUtils.isEmpty(next.f2006a) && !this.f1879a.containsKey(next.f2006a)) {
                    this.f1879a.put(next.f2006a, next);
                    a(next.f2006a, i);
                }
            }
        }
    }

    public List<AppUpdateInfo> a() {
        return new ArrayList(this.f1879a.values());
    }

    public Map<Integer, ArrayList<AppUpdateInfo>> b() {
        Hashtable hashtable = new Hashtable();
        for (AppUpdateInfo next : a()) {
            ArrayList arrayList = this.b.get(next.a());
            if (arrayList != null && !arrayList.isEmpty()) {
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    Integer num = (Integer) it.next();
                    ArrayList arrayList2 = (ArrayList) hashtable.get(num);
                    if (arrayList2 == null) {
                        arrayList2 = new ArrayList();
                        hashtable.put(num, arrayList2);
                    }
                    arrayList2.add(next);
                }
            }
        }
        return hashtable;
    }

    private void a(String str, int i) {
        ArrayList arrayList;
        if (!this.b.containsKey(str)) {
            arrayList = new ArrayList();
            this.b.put(str, arrayList);
        } else {
            arrayList = this.b.get(str);
            if (!arrayList.isEmpty()) {
                arrayList.clear();
            }
        }
        if (!arrayList.contains(Integer.valueOf(i))) {
            arrayList.add(Integer.valueOf(i));
        }
    }

    public List<AppUpdateInfo> a(int i) {
        List list;
        if (this.f1879a == null || this.f1879a.isEmpty() || this.b == null || this.b.isEmpty()) {
            Map<Integer, ArrayList<AppUpdateInfo>> h = as.w().h();
            if (h != null) {
                return h.get(Integer.valueOf(i));
            }
            return null;
        }
        Iterator it = new LinkedHashSet(this.f1879a.keySet()).iterator();
        if (it == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        while (it.hasNext()) {
            String str = (String) it.next();
            if (!TextUtils.isEmpty(str) && (list = this.b.get(str)) != null && list.contains(Integer.valueOf(i))) {
                arrayList.add(this.f1879a.get(str));
            }
        }
        return arrayList;
    }
}
