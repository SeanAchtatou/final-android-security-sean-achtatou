package com.tencent.assistant.module;

import com.tencent.assistant.Global;
import com.tencent.assistant.protocol.jce.PostFeedbackRequest;

/* compiled from: ProGuard */
class eu implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f1807a;
    final /* synthetic */ et b;

    eu(et etVar, String str) {
        this.b = etVar;
        this.f1807a = str;
    }

    public void run() {
        synchronized (this.b.b) {
            if (this.b.f1806a > 0) {
                this.b.cancel(this.b.f1806a);
            }
            PostFeedbackRequest postFeedbackRequest = new PostFeedbackRequest();
            postFeedbackRequest.f2259a = this.f1807a;
            postFeedbackRequest.b = Global.getNotNullTerminal();
            int unused = this.b.f1806a = this.b.send(postFeedbackRequest);
        }
    }
}
