package com.tencent.assistant.module.update;

import android.text.TextUtils;
import android.util.Log;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.m;
import com.tencent.assistant.protocol.jce.UpdateCfg;
import com.tencent.assistant.st.STConstAction;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/* compiled from: ProGuard */
public class aa {
    public static void a(long j, UpdateCfg updateCfg) {
        boolean z = true;
        if (updateCfg != null) {
            if (updateCfg.a() != null && !updateCfg.a().isEmpty()) {
                if (j != b((byte) 1)) {
                    z zVar = new z(updateCfg.a());
                    a(zVar.a(), zVar.b(), j, (byte) 1);
                }
            }
            if (updateCfg.b() != null && !updateCfg.b().isEmpty()) {
                Log.d("jiaxinpush", "get timecfg");
                if (j == b((byte) 2)) {
                    z = false;
                }
                if (z) {
                    z zVar2 = new z(updateCfg.b());
                    a(zVar2.a(), zVar2.b(), j, (byte) 2);
                }
            }
        }
    }

    public static ab a(byte b) {
        String a2;
        switch (b) {
            case 1:
                a2 = m.a().a("key_app_update_start_time_span_4", Constants.STR_EMPTY);
                break;
            case 2:
                a2 = m.a().a("key_otherpush_update_start_time_span", Constants.STR_EMPTY);
                break;
            default:
                a2 = m.a().a("key_app_update_start_time_span_4", Constants.STR_EMPTY);
                break;
        }
        if (!TextUtils.isEmpty(a2)) {
            try {
                String[] split = a2.split("#");
                if (!TextUtils.isEmpty(split[0]) && !TextUtils.isEmpty(split[1])) {
                    ab abVar = new ab();
                    abVar.f1878a = a(split[0]);
                    abVar.b = a(split[1]);
                    return abVar;
                }
            } catch (Exception e) {
            }
        }
        ab abVar2 = new ab();
        abVar2.f1878a = b();
        abVar2.b = a();
        a(abVar2.f1878a, abVar2.b, 0, b);
        return abVar2;
    }

    public static long b(byte b) {
        String a2;
        long j;
        switch (b) {
            case 1:
                a2 = m.a().a("key_app_update_start_time_span_4", Constants.STR_EMPTY);
                break;
            case 2:
                a2 = m.a().a("key_otherpush_update_start_time_span", Constants.STR_EMPTY);
                break;
            default:
                a2 = m.a().a("key_app_update_start_time_span_4", Constants.STR_EMPTY);
                break;
        }
        if (TextUtils.isEmpty(a2)) {
            return 0;
        }
        try {
            String[] split = a2.split("#");
            if (split == null || split.length != 3 || TextUtils.isEmpty(split[2])) {
                j = 0;
            } else {
                j = Long.valueOf(split[2]).longValue();
            }
            return j;
        } catch (Exception e) {
            return 0;
        }
    }

    public static int[] a(String str) {
        String[] split;
        if (TextUtils.isEmpty(str) || (split = str.split(" ")) == null || split.length == 0) {
            return null;
        }
        int[] iArr = new int[split.length];
        for (int i = 0; i < iArr.length; i++) {
            iArr[i] = Integer.parseInt(split[i]);
        }
        return iArr;
    }

    public static int a(int i) {
        return i / 100;
    }

    public static int b(int i) {
        return i % 100;
    }

    private static void a(List<Integer> list, List<Integer> list2, long j, byte b) {
        String a2 = a(list, list2, j);
        switch (b) {
            case 1:
                if (!TextUtils.isEmpty(a2)) {
                    m.a().b("key_app_update_start_time_span_4", a2.toString());
                    return;
                } else {
                    m.a().b("key_app_update_start_time_span_4", Constants.STR_EMPTY);
                    return;
                }
            case 2:
                if (!TextUtils.isEmpty(a2)) {
                    m.a().b("key_otherpush_update_start_time_span", a2.toString());
                    return;
                } else {
                    m.a().b("key_otherpush_update_start_time_span", Constants.STR_EMPTY);
                    return;
                }
            default:
                if (!TextUtils.isEmpty(a2)) {
                    m.a().b("key_app_update_start_time_span_4", a2.toString());
                    return;
                } else {
                    m.a().b("key_app_update_start_time_span_4", Constants.STR_EMPTY);
                    return;
                }
        }
    }

    private static String a(List<Integer> list, List<Integer> list2, long j) {
        if (list == null || list.isEmpty() || list2 == null || list2.isEmpty()) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            sb.append(list.get(i).intValue());
            if (i != size - 1) {
                sb.append(" ");
            }
        }
        sb.append("#");
        int size2 = list2.size();
        for (int i2 = 0; i2 < size2; i2++) {
            sb.append(list2.get(i2).intValue());
            if (i2 != size2 - 1) {
                sb.append(" ");
            }
        }
        sb.append("#");
        sb.append(j);
        return sb.toString();
    }

    private static void a(int[] iArr, int[] iArr2, long j, byte b) {
        String a2 = a(iArr, iArr2, j);
        switch (b) {
            case 1:
                if (!TextUtils.isEmpty(a2)) {
                    m.a().b("key_app_update_start_time_span_4", a2.toString());
                    return;
                } else {
                    m.a().b("key_app_update_start_time_span_4", Constants.STR_EMPTY);
                    return;
                }
            case 2:
                if (!TextUtils.isEmpty(a2)) {
                    m.a().b("key_otherpush_update_start_time_span", a2.toString());
                    return;
                } else {
                    m.a().b("key_otherpush_update_start_time_span", Constants.STR_EMPTY);
                    return;
                }
            default:
                if (!TextUtils.isEmpty(a2)) {
                    m.a().b("key_app_update_start_time_span_4", a2.toString());
                    return;
                } else {
                    m.a().b("key_app_update_start_time_span_4", Constants.STR_EMPTY);
                    return;
                }
        }
    }

    private static String a(int[] iArr, int[] iArr2, long j) {
        if (iArr == null || iArr.length <= 0 || iArr2 == null || iArr2.length <= 0) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        int length = iArr.length;
        for (int i = 0; i < length; i++) {
            sb.append(iArr[i]);
            if (i != length - 1) {
                sb.append(" ");
            }
        }
        sb.append("#");
        int length2 = iArr2.length;
        for (int i2 = 0; i2 < length2; i2++) {
            sb.append(iArr2[i2]);
            if (i2 != length2 - 1) {
                sb.append(" ");
            }
        }
        sb.append("#");
        sb.append(j);
        return sb.toString();
    }

    public static int a(long j) {
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(j);
        return instance.get(14) + (instance.get(11) * 10000000) + (instance.get(12) * 100000) + (instance.get(13) * 1000);
    }

    public static Calendar c(int i) {
        int i2 = i % 10000000;
        int i3 = i2 / 100000;
        int i4 = i2 % 100000;
        Calendar instance = Calendar.getInstance();
        instance.set(11, i / 10000000);
        instance.set(12, i3);
        instance.set(13, i4 / 1000);
        instance.set(14, i4 % 1000);
        return instance;
    }

    public static int a(int i, int i2) {
        Calendar instance = Calendar.getInstance();
        instance.set(11, i / 100);
        instance.set(12, i % 100);
        instance.set(13, 0);
        instance.set(14, 0);
        Calendar instance2 = Calendar.getInstance();
        instance2.set(11, i2 / 100);
        instance2.set(12, i2 % 100);
        instance2.set(13, 0);
        instance2.set(14, 0);
        double timeInMillis = ((double) (instance2.getTimeInMillis() - instance.getTimeInMillis())) * Math.random();
        Calendar instance3 = Calendar.getInstance();
        instance3.setTimeInMillis((long) (timeInMillis + ((double) instance.getTimeInMillis())));
        return (instance3.get(11) * 10000000) + (instance3.get(12) * 100000) + (instance3.get(13) * 1000) + instance3.get(14);
    }

    public static int b(int i, int i2) {
        int i3 = i % 10000000;
        int i4 = i3 / 100000;
        int i5 = i3 % 100000;
        Calendar instance = Calendar.getInstance();
        instance.set(11, i / 10000000);
        instance.set(12, i4 + i2);
        instance.set(13, i5 / 1000);
        instance.set(14, i5 % 1000);
        return (instance.get(11) * 10000000) + (instance.get(12) * 100000) + (instance.get(13) * 1000) + instance.get(14);
    }

    public static int[] a() {
        int a2 = a(800, (int) EventDispatcherEnum.UI_EVENT_AUTO_DOWNLOAD_START);
        int a3 = a((int) STConstAction.ACTION_HIT_COMENT, 1700);
        int a4 = a(1900, 2300);
        ArrayList arrayList = new ArrayList();
        arrayList.add(Integer.valueOf(a2));
        while (true) {
            a2 = b(a2, 30);
            if (a2 >= 120000000) {
                break;
            }
            arrayList.add(Integer.valueOf(a2));
        }
        arrayList.add(Integer.valueOf(a3));
        int i = a3;
        while (true) {
            i = b(i, 60);
            if (i >= 190000000) {
                break;
            }
            arrayList.add(Integer.valueOf(i));
        }
        arrayList.add(Integer.valueOf(a4));
        int i2 = a4;
        while (true) {
            i2 = b(i2, 30);
            if (i2 >= 240000000 || i2 <= 190000000) {
                int[] iArr = new int[arrayList.size()];
                int i3 = 0;
            } else {
                arrayList.add(Integer.valueOf(i2));
            }
        }
        int[] iArr2 = new int[arrayList.size()];
        int i32 = 0;
        while (true) {
            int i4 = i32;
            if (i4 >= arrayList.size()) {
                return iArr2;
            }
            iArr2[i4] = ((Integer) arrayList.get(i4)).intValue();
            i32 = i4 + 1;
        }
    }

    public static int[] b() {
        return new int[]{812, STConstAction.ACTION_HIT_COLLECT, 1924};
    }
}
