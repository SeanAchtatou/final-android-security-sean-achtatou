package com.tencent.assistant.module;

import com.qq.AppService.AstApp;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.protocol.ProtocolDecoder;
import com.tencent.assistant.protocol.a.e;
import com.tencent.assistant.protocol.b;
import com.tencent.assistant.protocol.jce.RspHead;
import com.tencent.assistant.protocol.jce.StatCSChannelData;
import com.tencent.assistant.protocol.k;
import com.tencent.assistant.protocol.l;
import com.tencent.assistant.protocol.n;
import com.tencent.assistant.protocol.scu.RequestResponePair;
import com.tencent.assistant.protocol.scu.g;
import com.tencent.assistant.utils.FunctionUtils;
import com.tencent.assistant.utils.r;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: ProGuard */
public abstract class aw implements g {
    public static boolean sKipHtmlResponse;
    private Map<Integer, ay> seqMap = new ConcurrentHashMap(2);

    /* access modifiers changed from: protected */
    public int send(int i, JceStruct jceStruct) {
        if (jceStruct == null) {
            return -1;
        }
        return send(i, wrapRequest(jceStruct), b.a(), false);
    }

    /* access modifiers changed from: protected */
    public int send(JceStruct jceStruct) {
        if (jceStruct == null) {
            return -1;
        }
        return send(getUniqueId(), wrapRequest(jceStruct), b.a(), false);
    }

    /* access modifiers changed from: protected */
    public int send(List<JceStruct> list) {
        return send(getUniqueId(), list);
    }

    /* access modifiers changed from: protected */
    public int send(int i, List<JceStruct> list) {
        if (list == null || list.size() == 0) {
            return -1;
        }
        return send(i, new ArrayList(list), b.a(), true);
    }

    /* access modifiers changed from: protected */
    public int send(int i, List<JceStruct> list, ProtocolDecoder protocolDecoder, boolean z) {
        int a2 = n.a();
        if (i <= 0) {
            i = getUniqueId();
        }
        ay ayVar = new ay(this);
        ayVar.f1707a = i;
        ayVar.b = z;
        ayVar.c = list;
        this.seqMap.put(Integer.valueOf(a2), ayVar);
        int a3 = k.a(a2, list, this, protocolDecoder);
        if (e.a().n()) {
            stRequestSend(a2, list);
        }
        if (a3 != -1) {
            return i;
        }
        this.seqMap.remove(Integer.valueOf(a2));
        return a3;
    }

    /* access modifiers changed from: protected */
    public boolean cancel(int i) {
        Iterator<Map.Entry<Integer, ay>> it = this.seqMap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry next = it.next();
            ay ayVar = (ay) next.getValue();
            if (ayVar != null && ayVar.f1707a == i) {
                int intValue = ((Integer) next.getKey()).intValue();
                k.a(intValue);
                if (e.a().n()) {
                    stRequestCancle(intValue, ayVar);
                }
                it.remove();
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public int send(JceStruct jceStruct, ProtocolDecoder protocolDecoder) {
        if (jceStruct == null) {
            return -1;
        }
        return send(getUniqueId(), wrapRequest(jceStruct), protocolDecoder, false);
    }

    /* access modifiers changed from: protected */
    public int getUniqueId() {
        return r.a();
    }

    public void onUpdateRspHeadData(RspHead rspHead) {
    }

    public void onProtocoRequestFinish(int i, int i2, List<RequestResponePair> list) {
        ay remove = this.seqMap.remove(Integer.valueOf(i));
        if (remove != null) {
            if (e.a().n()) {
                stRequestFinish(i, i2, list);
            }
            if (i2 == -4) {
                onForbidden();
            } else if (i2 == -803 && !sKipHtmlResponse) {
                FunctionUtils.b();
            }
            if (remove.b) {
                onMultiRequestFinish(remove.f1707a, i2, list);
            } else {
                processSingleRequestFinish(remove, list);
            }
        }
    }

    private void onForbidden() {
        AstApp.i().j().sendMessage(AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_APP_FORBIDDEN));
    }

    private void processSingleRequestFinish(ay ayVar, List<RequestResponePair> list) {
        JceStruct jceStruct;
        JceStruct jceStruct2 = null;
        int i = -840;
        if (list == null || list.size() <= 0) {
            jceStruct = null;
        } else {
            jceStruct2 = list.get(0).response;
            jceStruct = list.get(0).request;
            i = list.get(0).errorCode;
        }
        if (i == 0) {
            onRequestSuccessed(ayVar.f1707a, jceStruct, jceStruct2);
        } else {
            onRequestFailed(ayVar.f1707a, i, jceStruct, jceStruct2);
        }
    }

    private List<JceStruct> wrapRequest(JceStruct jceStruct) {
        if (jceStruct == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList(1);
        arrayList.add(jceStruct);
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
    }

    /* access modifiers changed from: protected */
    public void onMultiRequestFinish(int i, int i2, List<RequestResponePair> list) {
        if (i2 == 0) {
            onRequestSuccessed(i, list);
        } else {
            onRequestFailed(i, i2, list);
        }
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, List<RequestResponePair> list) {
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, List<RequestResponePair> list) {
    }

    private void stRequestSend(int i, List<JceStruct> list) {
        if (list != null && list.size() != 0) {
            StatCSChannelData statCSChannelData = new StatCSChannelData();
            statCSChannelData.b = 1;
            statCSChannelData.c = 1;
            statCSChannelData.d = 1;
            statCSChannelData.f = i;
            StringBuilder sb = new StringBuilder();
            sb.append("cmd:");
            for (JceStruct a2 : list) {
                sb.append(l.a(a2)).append(";");
            }
            statCSChannelData.g = sb.toString();
            e.a().a(statCSChannelData);
        }
    }

    private void stRequestFinish(int i, int i2, List<RequestResponePair> list) {
        if (list != null && list.size() != 0) {
            StatCSChannelData statCSChannelData = new StatCSChannelData();
            statCSChannelData.b = 1;
            statCSChannelData.c = 1;
            statCSChannelData.d = 2;
            statCSChannelData.f = i;
            StringBuilder sb = new StringBuilder();
            sb.append("errorCode:").append(i2).append(";");
            for (RequestResponePair next : list) {
                sb.append("cmd:").append(l.a(next.request)).append("; errorCode: ").append(next.errorCode).append(";");
            }
            statCSChannelData.g = sb.toString();
            e.a().a(statCSChannelData);
        }
    }

    private void stRequestCancle(int i, ay ayVar) {
        if (ayVar != null && ayVar.c != null) {
            StatCSChannelData statCSChannelData = new StatCSChannelData();
            statCSChannelData.b = 1;
            statCSChannelData.c = 1;
            statCSChannelData.d = 3;
            statCSChannelData.f = i;
            StringBuilder sb = new StringBuilder();
            for (JceStruct a2 : ayVar.c) {
                sb.append(l.a(a2)).append(";");
            }
            statCSChannelData.g = sb.toString();
            e.a().a(statCSChannelData);
        }
    }
}
