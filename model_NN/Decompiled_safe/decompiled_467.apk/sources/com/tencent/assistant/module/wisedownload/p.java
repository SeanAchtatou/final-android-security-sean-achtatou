package com.tencent.assistant.module.wisedownload;

import android.text.TextUtils;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.download.SimpleDownloadInfo;
import com.tencent.assistant.manager.DownloadProxy;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.protocol.jce.AutoDownloadCfg;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class p {
    public static boolean a(SimpleDownloadInfo.UIType uIType, SimpleAppModel simpleAppModel) {
        DownloadInfo a2;
        if (simpleAppModel == null || (a2 = DownloadProxy.a().a(simpleAppModel)) == null || a2.uiType != uIType || !a2.isDownloaded()) {
            return false;
        }
        return true;
    }

    public static boolean a(SimpleDownloadInfo.UIType uIType, DownloadInfo downloadInfo) {
        if (downloadInfo == null || downloadInfo.uiType != uIType || !downloadInfo.isDownloadedFile()) {
            return false;
        }
        return true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:43:0x00ac  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x014c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static long a(com.tencent.assistant.download.SimpleDownloadInfo.UIType r11) {
        /*
            r2 = 0
            r7 = 0
            r4 = 1
            com.tencent.assistant.manager.DownloadProxy r0 = com.tencent.assistant.manager.DownloadProxy.a()
            com.tencent.assistant.download.SimpleDownloadInfo$DownloadType r1 = com.tencent.assistant.download.SimpleDownloadInfo.DownloadType.APK
            java.util.ArrayList r8 = r0.a(r1)
            r5 = 0
            if (r8 == 0) goto L_0x0158
            int r0 = r8.size()
            if (r0 <= 0) goto L_0x0158
            java.util.Iterator r1 = r8.iterator()
        L_0x001b:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x0158
            java.lang.Object r0 = r1.next()
            com.tencent.assistant.download.DownloadInfo r0 = (com.tencent.assistant.download.DownloadInfo) r0
            if (r0 == 0) goto L_0x001b
            com.tencent.assistant.download.SimpleDownloadInfo$UIType r6 = r0.uiType
            if (r6 != r11) goto L_0x001b
            long r9 = r0.createTime
            boolean r6 = com.tencent.assistant.utils.cv.b(r9)
            if (r6 == 0) goto L_0x001b
            com.tencent.assistant.module.wisedownload.j r6 = com.tencent.assistant.module.wisedownload.j.a()
            boolean r6 = r6.a(r0, r11)
            if (r6 == 0) goto L_0x001b
            r5 = r4
            r6 = r0
        L_0x0041:
            if (r5 != 0) goto L_0x0151
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            int[] r1 = com.tencent.assistant.module.wisedownload.q.f1919a
            int r9 = r11.ordinal()
            r1 = r1[r9]
            switch(r1) {
                case 1: goto L_0x00cf;
                case 2: goto L_0x0105;
                case 3: goto L_0x010f;
                case 4: goto L_0x0119;
                case 5: goto L_0x0123;
                default: goto L_0x0053;
            }
        L_0x0053:
            java.util.ArrayList r0 = com.tencent.assistant.module.u.e(r0)
            java.util.Iterator r9 = r0.iterator()
        L_0x005b:
            boolean r0 = r9.hasNext()
            if (r0 == 0) goto L_0x0151
            java.lang.Object r0 = r9.next()
            com.tencent.assistant.model.SimpleAppModel r0 = (com.tencent.assistant.model.SimpleAppModel) r0
            com.tencent.assistant.manager.DownloadProxy r1 = com.tencent.assistant.manager.DownloadProxy.a()
            com.tencent.assistant.download.DownloadInfo r1 = r1.a(r0)
            if (r1 == 0) goto L_0x012d
            com.tencent.assistant.download.SimpleDownloadInfo$UIType r0 = r1.uiType
            if (r0 != r11) goto L_0x005b
            com.tencent.assistant.module.wisedownload.j r0 = com.tencent.assistant.module.wisedownload.j.a()
            boolean r0 = r0.a(r1, r11)
            if (r0 == 0) goto L_0x005b
            r0 = r4
        L_0x0080:
            if (r0 != 0) goto L_0x014e
            if (r8 == 0) goto L_0x014e
            int r0 = r8.size()
            if (r0 <= 0) goto L_0x014e
            java.util.Iterator r5 = r8.iterator()
        L_0x008e:
            boolean r0 = r5.hasNext()
            if (r0 == 0) goto L_0x014e
            java.lang.Object r0 = r5.next()
            com.tencent.assistant.download.DownloadInfo r0 = (com.tencent.assistant.download.DownloadInfo) r0
            if (r0 == 0) goto L_0x008e
            com.tencent.assistant.download.SimpleDownloadInfo$UIType r6 = r0.uiType
            if (r6 != r11) goto L_0x008e
            com.tencent.assistant.module.wisedownload.j r6 = com.tencent.assistant.module.wisedownload.j.a()
            boolean r6 = r6.a(r0, r11)
            if (r6 == 0) goto L_0x008e
        L_0x00aa:
            if (r0 == 0) goto L_0x014c
            com.tencent.assistant.download.m r1 = r0.response
            if (r1 == 0) goto L_0x013e
            com.tencent.assistant.download.m r1 = r0.response
            long r5 = r1.f1263a
            int r1 = (r5 > r2 ? 1 : (r5 == r2 ? 0 : -1))
            if (r1 <= 0) goto L_0x013e
            com.tencent.assistant.download.m r1 = r0.response
            long r1 = r1.b
            com.tencent.assistant.download.m r3 = r0.response
            long r5 = r3.f1263a
            int r1 = (r1 > r5 ? 1 : (r1 == r5 ? 0 : -1))
            if (r1 <= 0) goto L_0x013e
            com.tencent.assistant.download.m r1 = r0.response
            long r1 = r1.b
            com.tencent.assistant.download.m r0 = r0.response
            long r3 = r0.f1263a
            long r0 = r1 - r3
        L_0x00ce:
            return r0
        L_0x00cf:
            com.tencent.assistant.module.update.u r0 = com.tencent.assistant.module.update.u.a()
            java.util.List r1 = r0.g()
            if (r1 == 0) goto L_0x0155
            boolean r0 = r1.isEmpty()
            if (r0 != 0) goto L_0x0155
            java.util.Iterator r9 = r1.iterator()
        L_0x00e3:
            boolean r0 = r9.hasNext()
            if (r0 == 0) goto L_0x0102
            java.lang.Object r0 = r9.next()
            com.tencent.assistant.protocol.jce.AutoDownloadInfo r0 = (com.tencent.assistant.protocol.jce.AutoDownloadInfo) r0
            java.lang.String r10 = r0.f2010a
            boolean r10 = android.text.TextUtils.isEmpty(r10)
            if (r10 == 0) goto L_0x00e3
            com.qq.AppService.AstApp r10 = com.qq.AppService.AstApp.i()
            java.lang.String r10 = r10.getPackageName()
            r0.f2010a = r10
            goto L_0x00e3
        L_0x0102:
            r0 = r1
            goto L_0x0053
        L_0x0105:
            com.tencent.assistant.module.update.u r0 = com.tencent.assistant.module.update.u.a()
            java.util.List r0 = r0.d()
            goto L_0x0053
        L_0x010f:
            com.tencent.assistant.module.update.u r0 = com.tencent.assistant.module.update.u.a()
            java.util.List r0 = r0.f()
            goto L_0x0053
        L_0x0119:
            com.tencent.assistant.module.update.u r0 = com.tencent.assistant.module.update.u.a()
            java.util.List r0 = r0.h()
            goto L_0x0053
        L_0x0123:
            com.tencent.assistant.module.update.u r0 = com.tencent.assistant.module.update.u.a()
            java.util.List r0 = r0.i()
            goto L_0x0053
        L_0x012d:
            com.tencent.assistant.download.DownloadInfo r1 = com.tencent.assistant.download.DownloadInfo.createDownloadInfo(r0, r7, r11)
            com.tencent.assistant.module.wisedownload.j r0 = com.tencent.assistant.module.wisedownload.j.a()
            boolean r0 = r0.a(r1, r11)
            if (r0 == 0) goto L_0x005b
            r0 = r4
            goto L_0x0080
        L_0x013e:
            int r1 = r0.isUpdate
            if (r1 != r4) goto L_0x0149
            long r1 = r0.fileSize
            long r3 = r0.sllFileSize
            long r0 = r1 - r3
            goto L_0x00ce
        L_0x0149:
            long r0 = r0.fileSize
            goto L_0x00ce
        L_0x014c:
            r0 = r2
            goto L_0x00ce
        L_0x014e:
            r0 = r1
            goto L_0x00aa
        L_0x0151:
            r0 = r5
            r1 = r6
            goto L_0x0080
        L_0x0155:
            r0 = r1
            goto L_0x0053
        L_0x0158:
            r6 = r7
            goto L_0x0041
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.module.wisedownload.p.a(com.tencent.assistant.download.SimpleDownloadInfo$UIType):long");
    }

    public static List<DownloadInfo> b(SimpleDownloadInfo.UIType uIType) {
        ArrayList arrayList = new ArrayList();
        Iterator<DownloadInfo> it = DownloadProxy.a().d().iterator();
        while (it.hasNext()) {
            DownloadInfo next = it.next();
            if ((next.downloadState == SimpleDownloadInfo.DownloadState.SUCC || next.downloadState == SimpleDownloadInfo.DownloadState.INSTALLING || next.downloadState == SimpleDownloadInfo.DownloadState.INSTALLED) && next.uiType == uIType) {
                if (next.downloadState == SimpleDownloadInfo.DownloadState.INSTALLING || next.downloadState == SimpleDownloadInfo.DownloadState.INSTALLED) {
                    arrayList.add(next);
                } else {
                    String filePath = next.getFilePath();
                    if (!TextUtils.isEmpty(filePath) && new File(filePath).exists()) {
                        arrayList.add(next);
                    }
                }
            }
        }
        return arrayList;
    }

    public static boolean a(AutoDownloadCfg autoDownloadCfg, int i) {
        if (autoDownloadCfg == null) {
            return false;
        }
        if (i == 2) {
            return a(autoDownloadCfg);
        }
        if (i == 1) {
            return c(autoDownloadCfg);
        }
        if (i == 3) {
            return b(autoDownloadCfg);
        }
        if (i == 5) {
            return d(autoDownloadCfg);
        }
        if (i == 6) {
            return e(autoDownloadCfg);
        }
        return false;
    }

    public static boolean a(AutoDownloadCfg autoDownloadCfg) {
        if (autoDownloadCfg == null) {
            return false;
        }
        if ((autoDownloadCfg.m & 1) != 1) {
            return true;
        }
        return false;
    }

    public static boolean b(AutoDownloadCfg autoDownloadCfg) {
        if (autoDownloadCfg == null) {
            return false;
        }
        if (((autoDownloadCfg.m >> 1) & 1) != 1) {
            return true;
        }
        return false;
    }

    public static boolean c(AutoDownloadCfg autoDownloadCfg) {
        if (autoDownloadCfg == null) {
            return false;
        }
        if (((autoDownloadCfg.m >> 2) & 1) != 1) {
            return true;
        }
        return false;
    }

    public static boolean d(AutoDownloadCfg autoDownloadCfg) {
        if (autoDownloadCfg == null) {
            return false;
        }
        if (((autoDownloadCfg.m >> 3) & 1) != 1) {
            return true;
        }
        return false;
    }

    public static boolean e(AutoDownloadCfg autoDownloadCfg) {
        if (autoDownloadCfg == null) {
            return false;
        }
        if (((autoDownloadCfg.m >> 4) & 1) != 1) {
            return true;
        }
        return false;
    }

    public static boolean f(AutoDownloadCfg autoDownloadCfg) {
        return a(autoDownloadCfg) || b(autoDownloadCfg) || c(autoDownloadCfg) || d(autoDownloadCfg) || e(autoDownloadCfg);
    }
}
