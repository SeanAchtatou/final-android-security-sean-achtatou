package com.tencent.assistant.module;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.c.c;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.as;
import com.tencent.assistant.manager.ba;
import com.tencent.assistant.manager.cm;
import com.tencent.assistant.model.q;
import com.tencent.assistant.module.callback.GetOpRegularPushCallback;
import com.tencent.assistant.protocol.jce.FlashInfo;
import com.tencent.assistant.protocol.jce.GetOpRegularInfoRequest;
import com.tencent.assistant.protocol.jce.GetOpRegularInfoResponse;
import com.tencent.assistant.protocol.jce.OpRegularInfo;
import com.tencent.assistant.protocol.jce.OpRegularParam;
import com.tencent.assistant.protocol.jce.PopUpInfo;
import com.tencent.assistant.protocol.jce.PushInfo;
import com.tencent.assistant.utils.bh;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: ProGuard */
public class dl extends BaseEngine<GetOpRegularPushCallback> {

    /* renamed from: a  reason: collision with root package name */
    private static dl f1777a = null;
    private static long b = 0;

    public static synchronized dl a() {
        dl dlVar;
        synchronized (dl.class) {
            if (f1777a == null) {
                f1777a = new dl();
            }
            dlVar = f1777a;
        }
        return dlVar;
    }

    public int b() {
        int i = 1;
        int i2 = 0;
        GetOpRegularInfoRequest getOpRegularInfoRequest = new GetOpRegularInfoRequest();
        getOpRegularInfoRequest.f2149a = new ArrayList<>();
        OpRegularParam opRegularParam = new OpRegularParam();
        opRegularParam.f2249a = 1;
        opRegularParam.c = 0;
        getOpRegularInfoRequest.f2149a.add(opRegularParam);
        OpRegularParam opRegularParam2 = new OpRegularParam();
        opRegularParam2.f2249a = 2;
        opRegularParam2.c = 0;
        getOpRegularInfoRequest.f2149a.add(opRegularParam2);
        OpRegularParam opRegularParam3 = new OpRegularParam();
        opRegularParam3.f2249a = 3;
        opRegularParam3.c = 0;
        opRegularParam3.b = ba.a().b();
        opRegularParam3.d = u.a();
        try {
            StringBuilder append = new StringBuilder().append(m.a().t() ? 1 : 0).append(Constants.STR_EMPTY);
            if (!m.a().r()) {
                i = 0;
            }
            i2 = Integer.parseInt(append.append(i).toString(), 2);
        } catch (Exception e) {
        }
        if (c.a().c()) {
            i2 |= 4;
        }
        opRegularParam3.e = i2;
        getOpRegularInfoRequest.f2149a.add(opRegularParam3);
        return send(getOpRegularInfoRequest);
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        if (jceStruct2 != null) {
            GetOpRegularInfoResponse getOpRegularInfoResponse = (GetOpRegularInfoResponse) jceStruct2;
            as.w().a(getOpRegularInfoResponse);
            m.a().b("otherpush_update_refresh_suc_time", Long.valueOf(System.currentTimeMillis()));
            PushInfo pushInfo = null;
            if (getOpRegularInfoResponse.a() != null && !getOpRegularInfoResponse.a().isEmpty()) {
                getOpRegularInfoResponse.a().size();
                a(a((byte) 1));
                pushInfo = b(a((byte) 3));
                a(pushInfo);
            }
            com.tencent.assistantv2.st.page.c.a(pushInfo);
        }
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
    }

    public ArrayList<Object> a(byte b2) {
        GetOpRegularInfoResponse c = as.w().c();
        ArrayList<Object> arrayList = new ArrayList<>();
        if (c == null) {
            return arrayList;
        }
        ArrayList<OpRegularInfo> a2 = c.a();
        if (a2 == null || a2.size() <= 0) {
            return arrayList;
        }
        Class<? extends JceStruct> b3 = b(b2);
        if (b3 == null) {
            return arrayList;
        }
        Iterator<OpRegularInfo> it = a2.iterator();
        while (it.hasNext()) {
            OpRegularInfo next = it.next();
            if (next.f2248a == b2) {
                arrayList.add(bh.b(next.b, b3));
            }
        }
        return arrayList;
    }

    private Class<? extends JceStruct> b(byte b2) {
        switch (b2) {
            case 1:
                return FlashInfo.class;
            case 2:
                return PopUpInfo.class;
            case 3:
                return PushInfo.class;
            default:
                return null;
        }
    }

    private void a(ArrayList<Object> arrayList) {
        ArrayList arrayList2 = new ArrayList();
        if (arrayList != null && arrayList.size() > 0) {
            Iterator<Object> it = arrayList.iterator();
            while (it.hasNext()) {
                FlashInfo flashInfo = (FlashInfo) it.next();
                if (flashInfo != null) {
                    arrayList2.add(new q(flashInfo));
                }
            }
        }
        new cm().a(arrayList2);
    }

    private void a(PushInfo pushInfo) {
        if (pushInfo != null) {
            ba.a().a(pushInfo);
        }
    }

    private PushInfo b(ArrayList<Object> arrayList) {
        if (arrayList == null || arrayList.size() <= 0) {
            return null;
        }
        return (PushInfo) arrayList.get(0);
    }

    public static synchronized long c() {
        long j;
        synchronized (dl.class) {
            j = b;
        }
        return j;
    }
}
