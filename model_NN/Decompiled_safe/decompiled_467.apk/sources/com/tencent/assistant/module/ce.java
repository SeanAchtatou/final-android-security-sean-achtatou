package com.tencent.assistant.module;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.h.a;
import com.tencent.assistant.login.d;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.as;
import com.tencent.assistant.model.QuickEntranceNotify;
import com.tencent.assistant.model.b;
import com.tencent.assistant.protocol.jce.CardItemWrapper;
import com.tencent.assistant.protocol.jce.ColorCardItem;
import com.tencent.assistant.protocol.jce.GetSmartCardsRequest;
import com.tencent.assistant.protocol.jce.GetSmartCardsResponse;
import com.tencent.assistant.protocol.jce.GftGetGameGiftFlagRequest;
import com.tencent.assistant.protocol.jce.GftGetGameGiftFlagResponse;
import com.tencent.assistant.protocol.jce.GftGetRecommendTabPageRequest;
import com.tencent.assistant.protocol.jce.GftGetRecommendTabPageResponse;
import com.tencent.assistant.protocol.jce.GftGetTreasureBoxSettingRequest;
import com.tencent.assistant.protocol.jce.GftGetTreasureBoxSettingResponse;
import com.tencent.assistant.protocol.jce.SmartCardWrapper;
import com.tencent.assistant.protocol.scu.RequestResponePair;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.ba;
import com.tencent.assistantv2.model.a.e;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class ce extends BaseEngine<e> implements eb {

    /* renamed from: a  reason: collision with root package name */
    private static ce f1746a = null;
    private long b = -1;
    /* access modifiers changed from: private */
    public List<CardItemWrapper> c;
    private int d = -1;
    private int e = -1;
    private int f = 0;
    private List<CardItemWrapper> g = new ArrayList();
    /* access modifiers changed from: private */
    public ArrayList<ColorCardItem> h = new ArrayList<>();
    /* access modifiers changed from: private */
    public boolean i = true;
    private co j;
    private b k = new b();
    /* access modifiers changed from: private */
    public a l = new a();
    private cn m;
    /* access modifiers changed from: private */
    public cn n;

    public static synchronized ce a() {
        ce ceVar;
        synchronized (ce.class) {
            if (f1746a == null) {
                f1746a = new ce();
            }
            ceVar = f1746a;
        }
        return ceVar;
    }

    private ce() {
        dy.a().a(this);
        this.j = new co(this);
        this.m = new cn(this);
        this.n = new cn(this);
    }

    public b b() {
        this.k.b(this.b);
        this.k.a(this.h);
        return this.k;
    }

    public GftGetRecommendTabPageResponse c() {
        return as.w().c((byte[]) null);
    }

    public void e() {
        TemporaryThreadManager.get().start(new cf(this));
    }

    /* access modifiers changed from: private */
    public boolean h() {
        boolean z;
        GftGetRecommendTabPageResponse c2 = c();
        if (c2 == null) {
            return false;
        }
        long a2 = m.a().a((byte) 5);
        if (c2.e != a2) {
            return false;
        }
        if (a2 != -1 && c2.e != a2) {
            return false;
        }
        ArrayList<ColorCardItem> a3 = c2.a();
        this.c = c2.g;
        if (this.c == null || this.c.size() == 0 || a3 == null || a3.size() == 0) {
            return false;
        }
        boolean z2 = 1 == c2.f;
        byte[] bArr = c2.d;
        this.m.f1755a = bArr;
        cn cnVar = this.m;
        if (1 == c2.f) {
            z = true;
        } else {
            z = false;
        }
        cnVar.c = z;
        this.m.b = 0;
        this.n.a(this.m);
        if (this.h == null) {
            this.h = new ArrayList<>();
        }
        this.h.clear();
        this.h.addAll(a3);
        if (this.g == null) {
            this.g = new ArrayList();
        }
        this.g.clear();
        this.g.addAll(this.c);
        this.b = c2.c();
        this.k.b(this.b);
        notifyDataChangedInMainThread(new cg(this, z2, bArr, a3));
        return true;
    }

    public void d() {
        if (this.b != m.a().a((byte) 5)) {
            f();
        }
    }

    public void a(ArrayList<QuickEntranceNotify> arrayList) {
    }

    public int f() {
        if (this.d > 0) {
            cancel(this.d);
        }
        this.m.b();
        this.d = b(this.m);
        return this.d;
    }

    private int a(cn cnVar) {
        if (this.e > 0) {
            cancel(this.e);
        }
        this.e = b(cnVar);
        return this.e;
    }

    public int g() {
        if (this.n == null || this.n.f1755a == null || this.n.f1755a.length == 0) {
            return -1;
        }
        if (this.j.c()) {
            int a2 = this.j.a();
            this.j.b();
            return a2;
        } else if (!this.n.equals(this.j.d()) || this.j.e() == null) {
            return a(this.n);
        } else {
            boolean z = this.b != this.j.h();
            this.b = this.j.h();
            this.k.b(this.b);
            int a3 = this.j.a();
            ArrayList arrayList = new ArrayList(this.j.e());
            this.g.addAll(arrayList);
            this.i = this.j.g();
            this.n.a(this.j.d());
            this.m.a(this.n);
            this.l.b(this.j.f());
            notifyDataChangedInMainThread(new ch(this, a3, z, arrayList));
            if (this.j.g()) {
                this.j.a(this.n);
            }
            return this.j.a();
        }
    }

    private int b(cn cnVar) {
        return a(-1, cnVar);
    }

    /* access modifiers changed from: private */
    public int a(int i2, cn cnVar) {
        this.f = 0;
        GftGetRecommendTabPageRequest gftGetRecommendTabPageRequest = new GftGetRecommendTabPageRequest();
        String n2 = d.a().n();
        gftGetRecommendTabPageRequest.c = -2;
        gftGetRecommendTabPageRequest.f2199a = 30;
        gftGetRecommendTabPageRequest.d = n2;
        if (cnVar == null) {
            cnVar = new cn(this);
        }
        gftGetRecommendTabPageRequest.b = cnVar.f1755a;
        GetSmartCardsRequest getSmartCardsRequest = new GetSmartCardsRequest();
        getSmartCardsRequest.f2167a = 4;
        getSmartCardsRequest.e = 60;
        getSmartCardsRequest.d = cnVar.b;
        GftGetTreasureBoxSettingRequest gftGetTreasureBoxSettingRequest = new GftGetTreasureBoxSettingRequest();
        GftGetGameGiftFlagRequest gftGetGameGiftFlagRequest = new GftGetGameGiftFlagRequest();
        ArrayList arrayList = new ArrayList();
        arrayList.add(gftGetRecommendTabPageRequest);
        arrayList.add(getSmartCardsRequest);
        arrayList.add(gftGetTreasureBoxSettingRequest);
        arrayList.add(gftGetGameGiftFlagRequest);
        return send(i2, arrayList);
    }

    public int a(JceStruct jceStruct) {
        return super.send(jceStruct);
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i2, List<RequestResponePair> list) {
        ArrayList<SmartCardWrapper> arrayList;
        boolean z;
        int i3;
        int i4;
        GetSmartCardsResponse getSmartCardsResponse;
        GftGetTreasureBoxSettingResponse gftGetTreasureBoxSettingResponse;
        GftGetGameGiftFlagResponse gftGetGameGiftFlagResponse;
        GetSmartCardsResponse getSmartCardsResponse2;
        GftGetRecommendTabPageResponse gftGetRecommendTabPageResponse;
        GftGetGameGiftFlagResponse gftGetGameGiftFlagResponse2 = null;
        GftGetTreasureBoxSettingResponse gftGetTreasureBoxSettingResponse2 = null;
        GftGetRecommendTabPageResponse gftGetRecommendTabPageResponse2 = null;
        GftGetRecommendTabPageRequest gftGetRecommendTabPageRequest = null;
        int i5 = -9999;
        GetSmartCardsResponse getSmartCardsResponse3 = null;
        int i6 = -9999;
        for (RequestResponePair next : list) {
            if (next.request instanceof GftGetRecommendTabPageRequest) {
                GftGetRecommendTabPageRequest gftGetRecommendTabPageRequest2 = (GftGetRecommendTabPageRequest) next.request;
                if (next.response instanceof GftGetRecommendTabPageResponse) {
                    gftGetRecommendTabPageResponse = (GftGetRecommendTabPageResponse) next.response;
                } else {
                    gftGetRecommendTabPageResponse = gftGetRecommendTabPageResponse2;
                }
                i3 = next.errorCode;
                gftGetRecommendTabPageResponse2 = gftGetRecommendTabPageResponse;
                gftGetRecommendTabPageRequest = gftGetRecommendTabPageRequest2;
            } else {
                i3 = i6;
            }
            if (next.request instanceof GetSmartCardsRequest) {
                if (next.response instanceof GetSmartCardsResponse) {
                    getSmartCardsResponse2 = (GetSmartCardsResponse) next.response;
                } else {
                    getSmartCardsResponse2 = getSmartCardsResponse3;
                }
                i4 = next.errorCode;
                getSmartCardsResponse = getSmartCardsResponse2;
            } else {
                i4 = i5;
                getSmartCardsResponse = getSmartCardsResponse3;
            }
            if (!(next.request instanceof GftGetTreasureBoxSettingRequest) || !(next.response instanceof GftGetTreasureBoxSettingResponse)) {
                gftGetTreasureBoxSettingResponse = gftGetTreasureBoxSettingResponse2;
            } else {
                gftGetTreasureBoxSettingResponse = (GftGetTreasureBoxSettingResponse) next.response;
            }
            if (!(next.request instanceof GftGetGameGiftFlagRequest) || !(next.response instanceof GftGetGameGiftFlagResponse)) {
                gftGetGameGiftFlagResponse = gftGetGameGiftFlagResponse2;
            } else {
                gftGetGameGiftFlagResponse = (GftGetGameGiftFlagResponse) next.response;
            }
            getSmartCardsResponse3 = getSmartCardsResponse;
            gftGetGameGiftFlagResponse2 = gftGetGameGiftFlagResponse;
            gftGetTreasureBoxSettingResponse2 = gftGetTreasureBoxSettingResponse;
            i5 = i4;
            i6 = i3;
        }
        if (gftGetRecommendTabPageRequest == null || gftGetRecommendTabPageResponse2 == null || i6 != 0) {
            onRequestFailed(i2, i6, list);
            return;
        }
        if (gftGetGameGiftFlagResponse2 == null) {
            gftGetGameGiftFlagResponse2 = new GftGetGameGiftFlagResponse();
        }
        as.w().a(gftGetGameGiftFlagResponse2);
        if (gftGetTreasureBoxSettingResponse2 != null) {
            ba.a(Constants.STR_EMPTY).post(new ci(this, gftGetTreasureBoxSettingResponse2));
        }
        if (gftGetRecommendTabPageResponse2 == null || gftGetRecommendTabPageResponse2.d() == null || gftGetRecommendTabPageResponse2.d().size() <= 0) {
            onRequestFailed(i2, -841, list);
            return;
        }
        ArrayList<CardItemWrapper> d2 = gftGetRecommendTabPageResponse2.d();
        ArrayList<CardItemWrapper> d3 = gftGetRecommendTabPageResponse2.d();
        new CardItemWrapper().c = d3.get(0).b();
        if (i5 != 0 || getSmartCardsResponse3 == null) {
            arrayList = null;
        } else {
            arrayList = getSmartCardsResponse3.a();
        }
        if (i2 == this.j.a()) {
            this.n.f1755a = gftGetRecommendTabPageResponse2.b();
            this.n.c = gftGetRecommendTabPageResponse2.f == 1;
            this.n.a();
            this.j.a(gftGetRecommendTabPageResponse2.e, d2, this.n, arrayList);
        } else {
            this.l.b(arrayList);
            this.m.a();
            boolean z2 = gftGetRecommendTabPageResponse2.e != this.b || gftGetRecommendTabPageRequest.b == null || gftGetRecommendTabPageRequest.b.length == 0;
            if (gftGetRecommendTabPageResponse2.f == 1) {
                z = true;
            } else {
                z = false;
            }
            byte[] b2 = gftGetRecommendTabPageResponse2.b();
            this.b = gftGetRecommendTabPageResponse2.e;
            this.k.b(this.b);
            if (z2) {
                if (this.h == null) {
                    this.h = new ArrayList<>();
                }
                this.h.clear();
                this.h.addAll(gftGetRecommendTabPageResponse2.a());
                this.g.clear();
                this.m.b = 60;
            }
            if (!(d2 == null || d2.size() == 0)) {
                this.g.addAll(d2);
            }
            this.m.f1755a = b2;
            this.i = z;
            this.n.a(this.m);
            if (z) {
                this.j.a(this.n);
            }
            notifyDataChangedInMainThread(new cj(this, i2, z, b2, z2, d3));
        }
        as.w().a(gftGetRecommendTabPageRequest.b, gftGetRecommendTabPageResponse2);
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i2, int i3, List<RequestResponePair> list) {
        GftGetRecommendTabPageRequest gftGetRecommendTabPageRequest;
        Iterator<RequestResponePair> it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                gftGetRecommendTabPageRequest = null;
                break;
            }
            RequestResponePair next = it.next();
            if (next.request instanceof GftGetRecommendTabPageRequest) {
                gftGetRecommendTabPageRequest = (GftGetRecommendTabPageRequest) next.request;
                break;
            }
        }
        if (i2 != this.j.a()) {
            if (gftGetRecommendTabPageRequest.b == null || gftGetRecommendTabPageRequest.b.length == 0) {
                TemporaryThreadManager.get().start(new ck(this, i2, i3));
            } else {
                notifyDataChangedInMainThread(new cm(this, i2, i3));
            }
        } else {
            this.j.i();
        }
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i2, int i3, JceStruct jceStruct, JceStruct jceStruct2) {
    }

    public String toString() {
        return getClass().getSimpleName().toString();
    }
}
