package com.tencent.assistant.module;

import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.assistant.module.callback.b;
import com.tencent.assistant.protocol.jce.GetAppListResponse;
import java.util.ArrayList;

/* compiled from: ProGuard */
class p implements CallbackHelper.Caller<b> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f1853a;
    final /* synthetic */ boolean b;
    final /* synthetic */ ArrayList c;
    final /* synthetic */ GetAppListResponse d;
    final /* synthetic */ k e;

    p(k kVar, int i, boolean z, ArrayList arrayList, GetAppListResponse getAppListResponse) {
        this.e = kVar;
        this.f1853a = i;
        this.b = z;
        this.c = arrayList;
        this.d = getAppListResponse;
    }

    /* renamed from: a */
    public void call(b bVar) {
        bVar.a(this.f1853a, 0, this.b, this.c, this.d.f);
    }
}
