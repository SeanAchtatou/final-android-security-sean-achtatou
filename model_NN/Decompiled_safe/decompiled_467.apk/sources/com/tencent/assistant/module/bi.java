package com.tencent.assistant.module;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.module.callback.k;
import com.tencent.assistant.protocol.jce.CommentTagInfo;
import com.tencent.assistant.protocol.jce.GetCommentListRequest;
import com.tencent.assistant.protocol.jce.GetCommentListResponse;
import com.tencent.assistant.utils.XLog;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class bi extends BaseEngine<k> {

    /* renamed from: a  reason: collision with root package name */
    private long f1721a = 0;
    private long b = 0;
    private String c;
    private int d = 0;
    /* access modifiers changed from: private */
    public boolean e = true;
    /* access modifiers changed from: private */
    public byte[] f;
    /* access modifiers changed from: private */
    public bo g = new bo(this);
    /* access modifiers changed from: private */
    public CommentTagInfo h;
    /* access modifiers changed from: private */
    public boolean i = true;
    private int j = -1;
    /* access modifiers changed from: private */
    public ArrayList<CommentTagInfo> k = null;

    public boolean a() {
        return this.e;
    }

    public int a(long j2, long j3, String str, int i2, CommentTagInfo commentTagInfo, ArrayList<CommentTagInfo> arrayList) {
        if (j2 == 0) {
            return -1;
        }
        this.f1721a = j2;
        this.b = j3;
        this.c = str;
        this.d = i2;
        this.h = commentTagInfo;
        this.k = arrayList;
        return a((byte[]) null);
    }

    public int b() {
        if (this.f1721a == 0 || this.b == 0 || this.f == null || this.f.length == 0) {
            return -1;
        }
        if (this.g.c()) {
            int a2 = this.g.a();
            this.g.b();
            return a2;
        } else if (this.f != this.g.d() || this.g.e() == null) {
            return a(this.f);
        } else {
            int a3 = this.g.a();
            runOnUiThread(new bj(this, this.g.e(), a3));
            return a3;
        }
    }

    /* access modifiers changed from: private */
    public int a(byte[] bArr) {
        if (this.j > 0) {
            cancel(this.j);
        }
        GetCommentListRequest getCommentListRequest = new GetCommentListRequest();
        getCommentListRequest.f2109a = this.f1721a;
        getCommentListRequest.b = this.b;
        getCommentListRequest.c = 10;
        getCommentListRequest.f = c();
        getCommentListRequest.g = this.d;
        if (bArr == null) {
            bArr = new byte[0];
        }
        getCommentListRequest.d = bArr;
        getCommentListRequest.h = this.h;
        if (this.k != null && this.k.size() > 0) {
            this.i = false;
        }
        getCommentListRequest.i = this.i;
        XLog.d("comment", "CommentDetailEngine.sendRequest, GetCommentListRequest=" + getCommentListRequest.toString());
        this.j = send(getCommentListRequest);
        return this.j;
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        if (jceStruct2 != null) {
            GetCommentListResponse getCommentListResponse = (GetCommentListResponse) jceStruct2;
            XLog.d("comment", "CommentDetailEngine.onRequestSucessed, GetCommentListResponse=" + getCommentListResponse.toString());
            if (i2 == this.g.a()) {
                this.g.a(getCommentListResponse);
                return;
            }
            GetCommentListRequest getCommentListRequest = (GetCommentListRequest) jceStruct;
            runOnUiThread(new bl(this, getCommentListResponse, i2, getCommentListRequest.d == null || getCommentListRequest.d.length == 0));
        }
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i2, int i3, JceStruct jceStruct, JceStruct jceStruct2) {
        XLog.d("comment", "CommentDetailEngine.onRequestFailed, errorCode=" + i3);
        if (i2 != this.g.a()) {
            GetCommentListRequest getCommentListRequest = (GetCommentListRequest) jceStruct;
            notifyDataChangedInMainThread(new bn(this, i2, i3, getCommentListRequest.d == null || getCommentListRequest.d.length == 0, getCommentListRequest));
            return;
        }
        this.g.f();
    }

    private int c() {
        LocalApkInfo localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(this.c);
        if (localApkInfo != null) {
            return localApkInfo.mVersionCode;
        }
        return -1;
    }
}
