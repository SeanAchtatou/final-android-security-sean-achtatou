package com.tencent.assistant.module;

import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.assistant.module.callback.k;
import com.tencent.assistant.protocol.jce.CommentAppResponse;
import com.tencent.assistant.protocol.jce.CommentDetail;

/* compiled from: ProGuard */
class bg implements CallbackHelper.Caller<k> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f1719a;
    final /* synthetic */ CommentAppResponse b;
    final /* synthetic */ CommentDetail c;
    final /* synthetic */ bf d;

    bg(bf bfVar, int i, CommentAppResponse commentAppResponse, CommentDetail commentDetail) {
        this.d = bfVar;
        this.f1719a = i;
        this.b = commentAppResponse;
        this.c = commentDetail;
    }

    /* renamed from: a */
    public void call(k kVar) {
        kVar.a(this.f1719a, this.b.f2040a, this.c, this.b.c);
    }
}
