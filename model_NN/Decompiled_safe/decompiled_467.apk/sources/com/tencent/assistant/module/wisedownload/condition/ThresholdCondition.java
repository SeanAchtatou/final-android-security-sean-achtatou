package com.tencent.assistant.module.wisedownload.condition;

import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.StatFs;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.module.wisedownload.b;
import com.tencent.assistant.net.c;
import com.tencent.assistant.utils.FileUtil;
import java.io.File;

/* compiled from: ProGuard */
public abstract class ThresholdCondition {

    /* renamed from: a  reason: collision with root package name */
    protected Bundle f1907a;
    protected CONDITION_RESULT_CODE b = CONDITION_RESULT_CODE.OK;

    /* compiled from: ProGuard */
    public enum CONDITION_RESULT_CODE {
        OK,
        FAIL_SWITCH,
        FAIL_PRIMARY_SCREEN,
        FAIL_PRIMARY_WIFI,
        FAIL_PRIMARY_CHARGING,
        FAIL_PRIMARY_UNCHARGING,
        FAIL_PRIMARY_RELIABLE_WIFI,
        FAIL_TIME,
        FAIL_OTHER_DAY,
        FAIL_OTHER_WEEK,
        FAIL_OTHER_PHONE,
        FAIL_OTHER_SPACE,
        FAIL_OTHER_NO_APP,
        FAIL_PRIMARY_UNKNOW,
        FAIL_OTHER_UNKNOW,
        FAIL_NEED_PAUSE,
        OK_BEGIN_DOWNLOAD,
        OK_CONTINUE_DOWNLOAD,
        FAIL_PRIMARY_NO_RECEIVE_BATTERY,
        FAIL_OTHER_MAX_COUNT_NOT_SPECIFIED
    }

    /* compiled from: ProGuard */
    public enum CONDITION_TRIGGER_ACTION {
        SCREEN_OFF,
        SCREEN_ON,
        USER_PRESENT,
        BATTERY_CHANGED,
        CONNECT,
        DISCONNECT,
        CONNECT_CHANGED,
        TIME_POINT,
        DOWNLOAD_SUCC,
        DOWNLOAD_FAIL
    }

    /* compiled from: ProGuard */
    public enum CONDITION_TYPE {
        CONDITION_SWITCH,
        CONDITION_PRIMARY,
        CONDITION_TIME,
        CONDITION_OTHER
    }

    public abstract void a(b bVar);

    public abstract boolean a();

    public void a(Bundle bundle) {
        this.f1907a = bundle;
    }

    public void a(CONDITION_RESULT_CODE condition_result_code) {
        this.b = condition_result_code;
    }

    public CONDITION_RESULT_CODE c() {
        return this.b;
    }

    public static boolean d() {
        return c.d();
    }

    public static boolean e() {
        boolean z;
        PowerManager powerManager = (PowerManager) AstApp.i().getSystemService("power");
        if (powerManager != null) {
            try {
                if (!powerManager.isScreenOn()) {
                    z = true;
                    return z;
                }
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }
        z = false;
        return z;
    }

    public boolean a(long j) {
        long f = f();
        if (f > 0 && f > j) {
            return true;
        }
        return false;
    }

    public static long f() {
        String dynamicAPKDir = FileUtil.getDynamicAPKDir();
        if (TextUtils.isEmpty(dynamicAPKDir)) {
            return 0;
        }
        File file = new File(dynamicAPKDir);
        if (!file.exists()) {
            return 0;
        }
        try {
            StatFs statFs = new StatFs(file.getPath());
            return ((long) statFs.getBlockSize()) * (((long) statFs.getAvailableBlocks()) - 4);
        } catch (IllegalArgumentException e) {
            return 0;
        }
    }

    public static WifiInfo g() {
        try {
            WifiManager wifiManager = (WifiManager) AstApp.i().getSystemService("wifi");
            if (wifiManager != null) {
                return wifiManager.getConnectionInfo();
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }
}
