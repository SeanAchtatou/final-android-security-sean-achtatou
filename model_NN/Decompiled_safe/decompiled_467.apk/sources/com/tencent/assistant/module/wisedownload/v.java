package com.tencent.assistant.module.wisedownload;

import com.tencent.assistant.download.DownloadInfo;
import java.util.Comparator;

/* compiled from: ProGuard */
final class v implements Comparator<DownloadInfo> {
    v() {
    }

    /* renamed from: a */
    public int compare(DownloadInfo downloadInfo, DownloadInfo downloadInfo2) {
        if (downloadInfo == null || downloadInfo2 == null) {
            return 0;
        }
        return (int) ((long) ((int) (downloadInfo2.downloadEndTime - downloadInfo.downloadEndTime)));
    }
}
