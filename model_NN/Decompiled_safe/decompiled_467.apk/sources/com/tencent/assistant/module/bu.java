package com.tencent.assistant.module;

import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.assistant.module.callback.b;
import com.tencent.assistant.protocol.jce.GftGetAppListResponse;
import java.util.ArrayList;

/* compiled from: ProGuard */
class bu implements CallbackHelper.Caller<b> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f1733a;
    final /* synthetic */ boolean b;
    final /* synthetic */ ArrayList c;
    final /* synthetic */ GftGetAppListResponse d;
    final /* synthetic */ bp e;

    bu(bp bpVar, int i, boolean z, ArrayList arrayList, GftGetAppListResponse gftGetAppListResponse) {
        this.e = bpVar;
        this.f1733a = i;
        this.b = z;
        this.c = arrayList;
        this.d = gftGetAppListResponse;
    }

    /* renamed from: a */
    public void call(b bVar) {
        bVar.a(this.f1733a, 0, this.b, this.c, this.d.f);
    }
}
