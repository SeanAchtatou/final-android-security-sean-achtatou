package com.tencent.assistant.module;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.module.callback.o;
import com.tencent.assistant.protocol.jce.GetFeedbackListRequest;
import com.tencent.assistant.protocol.jce.GetFeedbackListResponse;

/* compiled from: ProGuard */
public class cz extends BaseEngine<o> implements ed {
    private static int c = 5;
    private static cz e = null;

    /* renamed from: a  reason: collision with root package name */
    private int f1765a = -1;
    private byte[] b = new byte[0];
    /* access modifiers changed from: private */
    public int d = 0;

    private cz() {
    }

    public static synchronized cz a() {
        cz czVar;
        synchronized (cz.class) {
            if (e == null) {
                e = new cz();
            }
            czVar = e;
        }
        return czVar;
    }

    public int b() {
        return this.d;
    }

    public void c() {
        this.b = new byte[0];
        e();
    }

    public void d() {
        this.d = 0;
        notifyDataChangedInMainThread(new da(this));
    }

    public void a(String str) {
        int parseInt = Integer.parseInt(str);
        this.d = parseInt;
        notifyDataChangedInMainThread(new db(this, parseInt));
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        GetFeedbackListResponse getFeedbackListResponse = (GetFeedbackListResponse) jceStruct2;
        GetFeedbackListRequest getFeedbackListRequest = (GetFeedbackListRequest) jceStruct;
        this.b = getFeedbackListResponse.b;
        notifyDataChangedInMainThread(new dc(this, getFeedbackListRequest.b == null || getFeedbackListRequest.b.length == 0, getFeedbackListResponse.d));
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        GetFeedbackListRequest getFeedbackListRequest = (GetFeedbackListRequest) jceStruct;
        notifyDataChanged(new dd(this, i2, getFeedbackListRequest.b == null || getFeedbackListRequest.b.length == 0));
    }

    private void e() {
        if (this.f1765a > 0) {
            cancel(this.f1765a);
        }
        GetFeedbackListRequest getFeedbackListRequest = new GetFeedbackListRequest();
        getFeedbackListRequest.f2123a = c;
        getFeedbackListRequest.b = this.b;
        this.f1765a = send(getFeedbackListRequest);
    }
}
