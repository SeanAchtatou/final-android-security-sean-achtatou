package com.tencent.assistant.module;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.NetworkMonitor;
import com.tencent.assistant.manager.as;
import com.tencent.assistant.model.QuickEntranceNotify;
import com.tencent.assistant.module.callback.j;
import com.tencent.assistant.net.APN;
import com.tencent.assistant.protocol.jce.AppCategory;
import com.tencent.assistant.protocol.jce.ColorCardItem;
import com.tencent.assistant.protocol.jce.GetAppCategoryRequest;
import com.tencent.assistant.protocol.jce.GetAppCategoryResponse;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class az extends BaseEngine<j> implements eb, NetworkMonitor.ConnectivityChangeListener {
    private static az b = null;

    /* renamed from: a  reason: collision with root package name */
    private final String f1708a = "CategoryEngine";
    private long c = -1;
    /* access modifiers changed from: private */
    public List<ColorCardItem> d = new ArrayList();
    /* access modifiers changed from: private */
    public List<ColorCardItem> e = new ArrayList();
    /* access modifiers changed from: private */
    public List<AppCategory> f = new ArrayList();
    /* access modifiers changed from: private */
    public List<AppCategory> g = new ArrayList();
    /* access modifiers changed from: private */
    public List<AppCategory> h = new ArrayList();
    /* access modifiers changed from: private */
    public List<AppCategory> i = new ArrayList();
    private int j = -1;

    private az() {
        dy.a().a(this);
    }

    public static synchronized az a() {
        az azVar;
        synchronized (az.class) {
            if (b == null) {
                b = new az();
            }
            azVar = b;
        }
        return azVar;
    }

    public List<AppCategory> a(long j2) {
        if (j2 == -1) {
            return this.f;
        }
        return this.g;
    }

    public List<AppCategory> b(long j2) {
        if (j2 == -1) {
            return this.h;
        }
        return this.i;
    }

    public List<ColorCardItem> c(long j2) {
        if (j2 == -1) {
            return this.d;
        }
        return this.e;
    }

    public long b() {
        return this.c;
    }

    public void c() {
        XLog.d("CategoryEngine", "loadData:start");
        if (this.h.size() <= 0 || this.i.size() <= 0) {
            XLog.d("CategoryEngine", "loadData:load from server");
            TemporaryThreadManager.get().start(new bb(this));
            return;
        }
        XLog.d("CategoryEngine", "loadData:load from local");
        notifyDataChangedInMainThread(new ba(this));
    }

    public int e() {
        if (this.j > 0) {
            cancel(this.j);
        }
        this.j = send(new GetAppCategoryRequest());
        return this.j;
    }

    private List<d> a(List<AppCategory> list) {
        ArrayList arrayList = new ArrayList();
        HashMap hashMap = new HashMap();
        ArrayList<d> arrayList2 = new ArrayList<>();
        for (AppCategory next : list) {
            d dVar = new d(next);
            arrayList2.add(dVar);
            hashMap.put(Long.valueOf(next.f1985a), dVar);
        }
        for (d dVar2 : arrayList2) {
            if (dVar2.c.e == 0) {
                arrayList.add(dVar2);
            } else {
                d dVar3 = (d) hashMap.get(Long.valueOf(dVar2.c.e));
                if (dVar3 != null) {
                    dVar3.a(dVar2);
                }
            }
        }
        if (arrayList.isEmpty()) {
            return null;
        }
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public boolean f() {
        XLog.d("CategoryEngine", "(loadLoaclCache)");
        GetAppCategoryResponse b2 = as.w().b();
        if (b2 != null && b2.c != m.a().a((byte) 4)) {
            return false;
        }
        XLog.d("CategoryEngine", "(loadLoaclCache) start");
        if (b2 == null || b2.b == null || b2.b.size() <= 0) {
            return false;
        }
        XLog.d("CategoryEngine", "(loadLoaclCache) color 1");
        ArrayList<ColorCardItem> b3 = b2.b();
        this.d.clear();
        if (b3 != null && b3.size() > 0) {
            this.d.addAll(b3);
        }
        ArrayList<ColorCardItem> c2 = b2.c();
        this.e.clear();
        if (c2 != null && c2.size() > 0) {
            this.e.addAll(c2);
        }
        XLog.d("CategoryEngine", "(loadLoaclCache) common 1");
        ArrayList<AppCategory> arrayList = b2.e;
        if (arrayList != null && arrayList.size() > 0) {
            List<AppCategory> b4 = b(arrayList, -1);
            List<AppCategory> b5 = b(arrayList, -2);
            if (b4 != null && b4.size() > 0) {
                this.f.clear();
                this.f.addAll(b4);
            }
            if (b5 != null && b5.size() > 0) {
                this.g.clear();
                this.g.addAll(b5);
            }
        }
        XLog.d("CategoryEngine", "(loadLoaclCache) normal 1");
        ArrayList<AppCategory> arrayList2 = b2.b;
        XLog.d("CategoryEngine", "(loadLoaclCache) normal 1:" + arrayList2.size());
        if (arrayList2 == null || arrayList2.size() <= 0) {
            return false;
        }
        XLog.d("CategoryEngine", "(loadLoaclCache) normal 2:");
        List<d> a2 = a(arrayList2);
        if (a2 == null) {
            return false;
        }
        XLog.d("CategoryEngine", "(loadLoaclCache) normal 3:");
        this.c = b2.c;
        List<AppCategory> a3 = a(a2, -1);
        List<AppCategory> a4 = a(a2, -2);
        if (a3 != null && a3.size() > 0) {
            this.h.clear();
            this.h.addAll(a3);
        }
        if (a4 != null && a4.size() > 0) {
            this.i.clear();
            this.i.addAll(a4);
        }
        notifyDataChangedInMainThread(new bc(this));
        return true;
    }

    private List<AppCategory> a(List<d> list, long j2) {
        if (list == null || list.size() == 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (d a2 : list) {
            d a3 = a(a2, j2);
            if (a3 != null) {
                Iterator<d> it = a3.f1766a.iterator();
                while (it.hasNext()) {
                    arrayList.add(it.next().c);
                }
                return arrayList;
            }
        }
        return null;
    }

    private List<AppCategory> b(List<AppCategory> list, long j2) {
        if (list == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (AppCategory next : list) {
            if (next != null && next.e == j2) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    private d a(d dVar, long j2) {
        if (dVar.c.a() == j2) {
            return dVar;
        }
        if (dVar.f1766a != null && !dVar.f1766a.isEmpty()) {
            Iterator<d> it = dVar.f1766a.iterator();
            while (it.hasNext()) {
                d a2 = a(it.next(), j2);
                if (a2 != null) {
                    return a2;
                }
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        List<d> a2;
        if (jceStruct2 != null) {
            GetAppCategoryResponse getAppCategoryResponse = (GetAppCategoryResponse) jceStruct2;
            ArrayList<ColorCardItem> b2 = getAppCategoryResponse.b();
            this.d.clear();
            if (b2 != null && b2.size() > 0) {
                this.d.addAll(b2);
            }
            ArrayList<ColorCardItem> c2 = getAppCategoryResponse.c();
            this.e.clear();
            if (c2 != null && c2.size() > 0) {
                this.e.addAll(c2);
            }
            ArrayList<AppCategory> arrayList = getAppCategoryResponse.e;
            if (arrayList != null && arrayList.size() > 0) {
                List<AppCategory> b3 = b(arrayList, -1);
                List<AppCategory> b4 = b(arrayList, -2);
                if (b3 != null && b3.size() > 0) {
                    this.f.clear();
                    this.f.addAll(b3);
                }
                if (b4 != null && b4.size() > 0) {
                    this.g.clear();
                    this.g.addAll(b4);
                }
            }
            ArrayList<AppCategory> a3 = getAppCategoryResponse.a();
            if (a3 != null && a3.size() > 0 && (a2 = a(a3)) != null) {
                this.c = getAppCategoryResponse.c;
                List<AppCategory> a4 = a(a2, -1);
                List<AppCategory> a5 = a(a2, -2);
                if (a4 != null && a4.size() > 0) {
                    this.h.clear();
                    this.h.addAll(a4);
                }
                if (a5 != null && a5.size() > 0) {
                    this.i.clear();
                    this.i.addAll(a5);
                }
                notifyDataChangedInMainThread(new bd(this, i2));
                as.w().a(getAppCategoryResponse);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i2, int i3, JceStruct jceStruct, JceStruct jceStruct2) {
        notifyDataChangedInMainThread(new be(this, i2, i3));
    }

    public void d() {
        if (this.c != m.a().a((byte) 4)) {
            e();
        }
    }

    public void a(ArrayList<QuickEntranceNotify> arrayList) {
    }

    public void onConnected(APN apn) {
    }

    public void onDisconnected(APN apn) {
    }

    public void onConnectivityChanged(APN apn, APN apn2) {
    }
}
