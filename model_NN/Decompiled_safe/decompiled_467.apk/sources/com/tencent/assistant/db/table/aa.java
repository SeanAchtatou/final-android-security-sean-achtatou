package com.tencent.assistant.db.table;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import com.qq.AppService.AstApp;
import com.tencent.assistant.db.helper.AstDbHelper;
import com.tencent.assistant.db.helper.SQLiteDatabaseWrapper;
import com.tencent.assistant.db.helper.SqliteHelper;
import com.tencent.assistant.model.a.s;

/* compiled from: ProGuard */
public class aa implements IBaseTable {
    /* JADX WARNING: Removed duplicated region for block: B:16:0x00ab A[SYNTHETIC, Splitter:B:16:0x00ab] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized java.util.List<com.tencent.assistant.model.a.s> a() {
        /*
            r7 = this;
            r1 = 0
            r2 = 1
            monitor-enter(r7)
            java.util.ArrayList r3 = new java.util.ArrayList     // Catch:{ all -> 0x00c2 }
            r3.<init>()     // Catch:{ all -> 0x00c2 }
            com.tencent.assistant.db.helper.SqliteHelper r0 = r7.getHelper()     // Catch:{ all -> 0x00c2 }
            com.tencent.assistant.db.helper.SQLiteDatabaseWrapper r0 = r0.getReadableDatabaseWrapper()     // Catch:{ all -> 0x00c2 }
            java.lang.String r4 = "select * from smart_card_setting_table"
            r5 = 0
            android.database.Cursor r1 = r0.rawQuery(r4, r5)     // Catch:{ Exception -> 0x00b2 }
            if (r1 == 0) goto L_0x00a9
            boolean r0 = r1.moveToFirst()     // Catch:{ Exception -> 0x00b2 }
            if (r0 == 0) goto L_0x00a9
        L_0x001f:
            com.tencent.assistant.model.a.s r4 = new com.tencent.assistant.model.a.s     // Catch:{ Exception -> 0x00b2 }
            r4.<init>()     // Catch:{ Exception -> 0x00b2 }
            java.lang.String r0 = "max_day_show_count"
            int r0 = r1.getColumnIndex(r0)     // Catch:{ Exception -> 0x00b2 }
            int r0 = r1.getInt(r0)     // Catch:{ Exception -> 0x00b2 }
            r4.f1652a = r0     // Catch:{ Exception -> 0x00b2 }
            java.lang.String r0 = "max_week_show_count"
            int r0 = r1.getColumnIndex(r0)     // Catch:{ Exception -> 0x00b2 }
            int r0 = r1.getInt(r0)     // Catch:{ Exception -> 0x00b2 }
            r4.b = r0     // Catch:{ Exception -> 0x00b2 }
            java.lang.String r0 = "max_show_count"
            int r0 = r1.getColumnIndex(r0)     // Catch:{ Exception -> 0x00b2 }
            int r0 = r1.getInt(r0)     // Catch:{ Exception -> 0x00b2 }
            r4.c = r0     // Catch:{ Exception -> 0x00b2 }
            java.lang.String r0 = "close_able"
            int r0 = r1.getColumnIndex(r0)     // Catch:{ Exception -> 0x00b2 }
            int r0 = r1.getInt(r0)     // Catch:{ Exception -> 0x00b2 }
            if (r0 != r2) goto L_0x00b0
            r0 = r2
        L_0x0055:
            r4.d = r0     // Catch:{ Exception -> 0x00b2 }
            java.lang.String r0 = "card_type"
            int r0 = r1.getColumnIndex(r0)     // Catch:{ Exception -> 0x00b2 }
            int r0 = r1.getInt(r0)     // Catch:{ Exception -> 0x00b2 }
            r4.e = r0     // Catch:{ Exception -> 0x00b2 }
            java.lang.String r0 = "card_id"
            int r0 = r1.getColumnIndex(r0)     // Catch:{ Exception -> 0x00b2 }
            int r0 = r1.getInt(r0)     // Catch:{ Exception -> 0x00b2 }
            r4.f = r0     // Catch:{ Exception -> 0x00b2 }
            java.lang.String r0 = "min_show_num"
            int r0 = r1.getColumnIndex(r0)     // Catch:{ Exception -> 0x00b2 }
            int r0 = r1.getInt(r0)     // Catch:{ Exception -> 0x00b2 }
            r4.g = r0     // Catch:{ Exception -> 0x00b2 }
            java.lang.String r0 = "end_time"
            int r0 = r1.getColumnIndex(r0)     // Catch:{ Exception -> 0x00b2 }
            int r0 = r1.getInt(r0)     // Catch:{ Exception -> 0x00b2 }
            long r5 = (long) r0     // Catch:{ Exception -> 0x00b2 }
            r4.h = r5     // Catch:{ Exception -> 0x00b2 }
            java.lang.String r0 = "max_period"
            int r0 = r1.getColumnIndex(r0)     // Catch:{ Exception -> 0x00b2 }
            int r0 = r1.getInt(r0)     // Catch:{ Exception -> 0x00b2 }
            r4.i = r0     // Catch:{ Exception -> 0x00b2 }
            java.lang.String r0 = "max_show_num"
            int r0 = r1.getColumnIndex(r0)     // Catch:{ Exception -> 0x00b2 }
            int r0 = r1.getInt(r0)     // Catch:{ Exception -> 0x00b2 }
            r4.j = r0     // Catch:{ Exception -> 0x00b2 }
            r3.add(r4)     // Catch:{ Exception -> 0x00b2 }
            boolean r0 = r1.moveToNext()     // Catch:{ Exception -> 0x00b2 }
            if (r0 != 0) goto L_0x001f
        L_0x00a9:
            if (r1 == 0) goto L_0x00ae
            r1.close()     // Catch:{ all -> 0x00c2 }
        L_0x00ae:
            monitor-exit(r7)
            return r3
        L_0x00b0:
            r0 = 0
            goto L_0x0055
        L_0x00b2:
            r0 = move-exception
            java.lang.String r2 = "SmartCardSettingTable"
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x00c5 }
            com.tencent.assistant.utils.XLog.d(r2, r0)     // Catch:{ all -> 0x00c5 }
            if (r1 == 0) goto L_0x00ae
            r1.close()     // Catch:{ all -> 0x00c2 }
            goto L_0x00ae
        L_0x00c2:
            r0 = move-exception
            monitor-exit(r7)
            throw r0
        L_0x00c5:
            r0 = move-exception
            if (r1 == 0) goto L_0x00cb
            r1.close()     // Catch:{ all -> 0x00c2 }
        L_0x00cb:
            throw r0     // Catch:{ all -> 0x00c2 }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.db.table.aa.a():java.util.List");
    }

    public synchronized int a(s sVar) {
        int i = 0;
        synchronized (this) {
            if (sVar != null) {
                b(sVar);
                SQLiteDatabaseWrapper writableDatabaseWrapper = getHelper().getWritableDatabaseWrapper();
                ContentValues contentValues = new ContentValues();
                contentValues.put("card_type", Integer.valueOf(sVar.e));
                contentValues.put("card_id", Integer.valueOf(sVar.f));
                contentValues.put("max_day_show_count", Integer.valueOf(sVar.f1652a));
                contentValues.put("max_week_show_count", Integer.valueOf(sVar.b));
                contentValues.put("max_show_count", Integer.valueOf(sVar.c));
                contentValues.put("min_show_num", Integer.valueOf(sVar.g));
                contentValues.put("end_time", Long.valueOf(sVar.h));
                contentValues.put("close_able", Integer.valueOf(sVar.d ? 1 : 0));
                contentValues.put("max_period", Integer.valueOf(sVar.i));
                contentValues.put("max_show_num", Integer.valueOf(sVar.j));
                i = (int) (((long) 0) + writableDatabaseWrapper.insert("smart_card_setting_table", null, contentValues));
            }
        }
        return i;
    }

    private int b(s sVar) {
        if (sVar == null) {
            return 0;
        }
        return 0 + getHelper().getWritableDatabaseWrapper().delete("smart_card_setting_table", "card_type = ? and card_id = ?", new String[]{Integer.toString(sVar.e), Integer.toString(sVar.f)});
    }

    public int tableVersion() {
        return 1;
    }

    public String tableName() {
        return "smart_card_setting_table";
    }

    public String createTableSQL() {
        return "CREATE TABLE if not exists smart_card_setting_table( _id INTEGER PRIMARY KEY AUTOINCREMENT, max_day_show_count INTEGER, max_week_show_count INTEGER, max_show_count INTEGER, close_able INTEGER, card_type INTEGER, card_id INTEGER, min_show_num INTEGER, end_time INTEGER, max_period INTEGER, max_show_num INTEGER); ";
    }

    public String[] getAlterSQL(int i, int i2) {
        if (i2 == 6) {
            return new String[]{"CREATE TABLE if not exists smart_card_setting_table( _id INTEGER PRIMARY KEY AUTOINCREMENT, max_day_show_count INTEGER, max_week_show_count INTEGER, max_show_count INTEGER, close_able INTEGER, card_type INTEGER, card_id INTEGER, min_show_num INTEGER, end_time INTEGER, max_period INTEGER, max_show_num INTEGER); "};
        } else if (i2 != 9) {
            return null;
        } else {
            return new String[]{"alter table smart_card_setting_table add column max_period INTEGER;", "alter table smart_card_setting_table add column max_show_num INTEGER;"};
        }
    }

    public void beforeTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public void afterTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public SqliteHelper getHelper() {
        return AstDbHelper.get(AstApp.i());
    }
}
