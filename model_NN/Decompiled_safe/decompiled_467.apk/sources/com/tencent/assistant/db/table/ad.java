package com.tencent.assistant.db.table;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.qq.AppService.AstApp;
import com.tencent.assistant.db.helper.AstDbHelper;
import com.tencent.assistant.db.helper.SQLiteDatabaseWrapper;
import com.tencent.assistant.db.helper.SqliteHelper;
import com.tencent.assistant.model.q;
import com.tencent.open.SocialConstants;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class ad implements IBaseTable {
    private q a(Cursor cursor) {
        q qVar = new q();
        qVar.a(cursor.getInt(cursor.getColumnIndex("splash_id")));
        qVar.a(cursor.getString(cursor.getColumnIndex("title")));
        qVar.b(cursor.getString(cursor.getColumnIndex(SocialConstants.PARAM_APP_DESC)));
        qVar.a(cursor.getLong(cursor.getColumnIndex("beginTime")));
        qVar.b(cursor.getLong(cursor.getColumnIndex("endTime")));
        qVar.b(cursor.getInt(cursor.getColumnIndex("runTime")));
        qVar.c(cursor.getInt(cursor.getColumnIndex("runTimes")));
        qVar.d(cursor.getInt(cursor.getColumnIndex("hasRunTimes")));
        qVar.c(cursor.getString(cursor.getColumnIndex("imageUrl")));
        qVar.d(cursor.getString(cursor.getColumnIndex("imageDataDesc")));
        qVar.e(cursor.getInt(cursor.getColumnIndex("status")));
        return qVar;
    }

    public synchronized int a(List<q> list) {
        int i;
        int i2 = 0;
        SQLiteDatabaseWrapper writableDatabaseWrapper = getHelper().getWritableDatabaseWrapper();
        Iterator<q> it = list.iterator();
        while (true) {
            i = i2;
            if (it.hasNext()) {
                q next = it.next();
                ContentValues contentValues = new ContentValues();
                contentValues.put("splash_id", Integer.valueOf(next.a()));
                contentValues.put("title", next.b());
                contentValues.put(SocialConstants.PARAM_APP_DESC, next.c());
                contentValues.put("beginTime", Long.valueOf(next.d()));
                contentValues.put("endTime", Long.valueOf(next.e()));
                contentValues.put("imageUrl", next.i());
                contentValues.put("imageDataDesc", next.j());
                contentValues.put("runTime", Integer.valueOf(next.f()));
                contentValues.put("runTimes", Integer.valueOf(next.g()));
                contentValues.put("hasRunTimes", Integer.valueOf(next.h()));
                contentValues.put("status", Integer.valueOf(next.k()));
                writableDatabaseWrapper.insert("splash_infos", null, contentValues);
                i2 = i + 1;
            }
        }
        return i;
    }

    public synchronized boolean a(q qVar) {
        boolean z = true;
        synchronized (this) {
            ContentValues contentValues = new ContentValues();
            contentValues.put("title", qVar.b());
            contentValues.put(SocialConstants.PARAM_APP_DESC, qVar.c());
            contentValues.put("beginTime", Long.valueOf(qVar.d()));
            contentValues.put("endTime", Long.valueOf(qVar.e()));
            contentValues.put("imageUrl", qVar.i());
            contentValues.put("imageDataDesc", qVar.j());
            contentValues.put("runTime", Integer.valueOf(qVar.f()));
            contentValues.put("runTimes", Integer.valueOf(qVar.g()));
            contentValues.put("hasRunTimes", Integer.valueOf(qVar.h()));
            contentValues.put("status", Integer.valueOf(qVar.k()));
            if (getHelper().getWritableDatabaseWrapper().update("splash_infos", contentValues, "splash_id=?", new String[]{String.valueOf(qVar.a())}) <= 0) {
                z = false;
            }
        }
        return z;
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x005f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List<com.tencent.assistant.model.q> a(boolean r11) {
        /*
            r10 = this;
            r8 = 0
            java.util.ArrayList r9 = new java.util.ArrayList
            r9.<init>()
            com.tencent.assistant.db.helper.SqliteHelper r0 = r10.getHelper()
            com.tencent.assistant.db.helper.SQLiteDatabaseWrapper r0 = r0.getReadableDatabaseWrapper()
            if (r11 == 0) goto L_0x0043
            java.lang.String r1 = "splash_infos"
            r2 = 0
            java.lang.String r3 = "status = ?"
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x0051, all -> 0x005c }
            r5 = 0
            r6 = 1
            java.lang.String r6 = java.lang.Integer.toString(r6)     // Catch:{ Exception -> 0x0051, all -> 0x005c }
            r4[r5] = r6     // Catch:{ Exception -> 0x0051, all -> 0x005c }
            r5 = 0
            r6 = 0
            java.lang.String r7 = "beginTime desc,endTime desc,splash_id desc"
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x0051, all -> 0x005c }
        L_0x0028:
            if (r1 == 0) goto L_0x003d
            boolean r0 = r1.moveToFirst()     // Catch:{ Exception -> 0x0066 }
            if (r0 == 0) goto L_0x003d
        L_0x0030:
            com.tencent.assistant.model.q r0 = r10.a(r1)     // Catch:{ Exception -> 0x0066 }
            r9.add(r0)     // Catch:{ Exception -> 0x0066 }
            boolean r0 = r1.moveToNext()     // Catch:{ Exception -> 0x0066 }
            if (r0 != 0) goto L_0x0030
        L_0x003d:
            if (r1 == 0) goto L_0x0042
            r1.close()
        L_0x0042:
            return r9
        L_0x0043:
            java.lang.String r1 = "splash_infos"
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            java.lang.String r7 = "beginTime desc,endTime desc,splash_id desc"
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x0051, all -> 0x005c }
            goto L_0x0028
        L_0x0051:
            r0 = move-exception
            r1 = r8
        L_0x0053:
            r0.printStackTrace()     // Catch:{ all -> 0x0063 }
            if (r1 == 0) goto L_0x0042
            r1.close()
            goto L_0x0042
        L_0x005c:
            r0 = move-exception
        L_0x005d:
            if (r8 == 0) goto L_0x0062
            r8.close()
        L_0x0062:
            throw r0
        L_0x0063:
            r0 = move-exception
            r8 = r1
            goto L_0x005d
        L_0x0066:
            r0 = move-exception
            goto L_0x0053
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.db.table.ad.a(boolean):java.util.List");
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x002a  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:27:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.tencent.assistant.model.q a(java.lang.String r10) {
        /*
            r9 = this;
            r8 = 0
            com.tencent.assistant.db.helper.SqliteHelper r0 = r9.getHelper()
            com.tencent.assistant.db.helper.SQLiteDatabaseWrapper r0 = r0.getReadableDatabaseWrapper()
            java.lang.String r1 = "splash_infos"
            r2 = 0
            java.lang.String r3 = "imageUrl = ?"
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x002e, all -> 0x003a }
            r5 = 0
            r4[r5] = r10     // Catch:{ Exception -> 0x002e, all -> 0x003a }
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x002e, all -> 0x003a }
            if (r1 == 0) goto L_0x0048
            boolean r0 = r1.moveToFirst()     // Catch:{ Exception -> 0x0044 }
            if (r0 == 0) goto L_0x0048
            com.tencent.assistant.model.q r8 = r9.a(r1)     // Catch:{ Exception -> 0x0044 }
            r0 = r8
        L_0x0028:
            if (r1 == 0) goto L_0x002d
            r1.close()
        L_0x002d:
            return r0
        L_0x002e:
            r0 = move-exception
            r1 = r8
        L_0x0030:
            r0.printStackTrace()     // Catch:{ all -> 0x0041 }
            if (r1 == 0) goto L_0x0046
            r1.close()
            r0 = r8
            goto L_0x002d
        L_0x003a:
            r0 = move-exception
        L_0x003b:
            if (r8 == 0) goto L_0x0040
            r8.close()
        L_0x0040:
            throw r0
        L_0x0041:
            r0 = move-exception
            r8 = r1
            goto L_0x003b
        L_0x0044:
            r0 = move-exception
            goto L_0x0030
        L_0x0046:
            r0 = r8
            goto L_0x002d
        L_0x0048:
            r0 = r8
            goto L_0x0028
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.db.table.ad.a(java.lang.String):com.tencent.assistant.model.q");
    }

    public int a() {
        return getHelper().getWritableDatabaseWrapper().delete("splash_infos", null, null);
    }

    public int tableVersion() {
        return 1;
    }

    public String tableName() {
        return "splash_infos";
    }

    public String createTableSQL() {
        return "CREATE TABLE if not exists splash_infos (row_id INTEGER PRIMARY KEY AUTOINCREMENT,splash_id INTEGER,title TEXT,desc TEXT,beginTime INTEGER,endTime INTEGER,runTime INTEGER,runTimes INTEGER,hasRunTimes INTEGER,imageUrl TEXT,imageDataDesc TEXT,status INTEGER);";
    }

    public String[] getAlterSQL(int i, int i2) {
        if (i2 != 4) {
            return null;
        }
        return new String[]{"CREATE TABLE if not exists splash_infos (row_id INTEGER PRIMARY KEY AUTOINCREMENT,splash_id INTEGER,title TEXT,desc TEXT,beginTime INTEGER,endTime INTEGER,runTime INTEGER,runTimes INTEGER,hasRunTimes INTEGER,imageUrl TEXT,imageDataDesc TEXT,status INTEGER);"};
    }

    public void beforeTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public void afterTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public SqliteHelper getHelper() {
        return AstDbHelper.get(AstApp.i());
    }
}
