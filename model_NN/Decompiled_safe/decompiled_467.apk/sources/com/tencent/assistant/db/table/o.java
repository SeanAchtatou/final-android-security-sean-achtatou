package com.tencent.assistant.db.table;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.qq.AppService.AstApp;
import com.tencent.assistant.component.appdetail.CommentDetailTabView;
import com.tencent.assistant.db.helper.DownloadDbHelper;
import com.tencent.assistant.db.helper.SqliteHelper;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.download.SimpleDownloadInfo;
import com.tencent.assistant.download.m;
import com.tencent.assistant.utils.ct;
import com.tencent.assistantv2.st.model.StatInfo;

/* compiled from: ProGuard */
public class o implements IBaseTable {
    public DownloadInfo a(Cursor cursor) {
        DownloadInfo downloadInfo = new DownloadInfo();
        downloadInfo.downloadState = SimpleDownloadInfo.DownloadState.values()[cursor.getInt(cursor.getColumnIndexOrThrow("state"))];
        if (downloadInfo.downloadState == SimpleDownloadInfo.DownloadState.DELETED) {
            downloadInfo.downloadState = SimpleDownloadInfo.DownloadState.INSTALLED;
        }
        downloadInfo.fileType = SimpleDownloadInfo.DownloadType.values()[cursor.getInt(cursor.getColumnIndexOrThrow("fileType"))];
        downloadInfo.downloadTicket = cursor.getString(cursor.getColumnIndexOrThrow("downloadTicket"));
        downloadInfo.appId = cursor.getLong(cursor.getColumnIndexOrThrow("appId"));
        downloadInfo.apkId = cursor.getLong(cursor.getColumnIndexOrThrow(CommentDetailTabView.PARAMS_APK_ID));
        downloadInfo.packageName = cursor.getString(cursor.getColumnIndexOrThrow("packageName"));
        downloadInfo.name = cursor.getString(cursor.getColumnIndexOrThrow("name"));
        downloadInfo.iconUrl = cursor.getString(cursor.getColumnIndexOrThrow("iconUrl"));
        downloadInfo.versionName = cursor.getString(cursor.getColumnIndexOrThrow("versionName"));
        downloadInfo.versionCode = (int) cursor.getLong(cursor.getColumnIndexOrThrow(CommentDetailTabView.PARAMS_VERSION_CODE));
        downloadInfo.apkUrlList = ct.b(cursor.getString(cursor.getColumnIndexOrThrow("apkUrl")));
        downloadInfo.fileSize = cursor.getLong(cursor.getColumnIndexOrThrow("fileSize"));
        downloadInfo.fileMd5 = cursor.getString(cursor.getColumnIndexOrThrow("fileMd5"));
        downloadInfo.signatrue = cursor.getString(cursor.getColumnIndexOrThrow("signatrue"));
        downloadInfo.sllApkUrlList = ct.b(cursor.getString(cursor.getColumnIndexOrThrow("sllApkUrl")));
        downloadInfo.sllFileSize = cursor.getLong(cursor.getColumnIndexOrThrow("sllFileSize"));
        downloadInfo.sllFileMd5 = cursor.getString(cursor.getColumnIndexOrThrow("sllFileMd5"));
        downloadInfo.sllUpdate = cursor.getInt(cursor.getColumnIndexOrThrow("sllUpdate"));
        downloadInfo.isUpdate = cursor.getInt(cursor.getColumnIndexOrThrow("isUpdate"));
        downloadInfo.createTime = cursor.getLong(cursor.getColumnIndexOrThrow("createTime"));
        if (downloadInfo.statInfo == null) {
            downloadInfo.statInfo = new StatInfo();
        }
        downloadInfo.statInfo.sourceScene = (int) cursor.getLong(cursor.getColumnIndexOrThrow("sourceSence"));
        downloadInfo.statInfo.f3356a = cursor.getLong(cursor.getColumnIndexOrThrow("sourceVersion"));
        downloadInfo.statInfo.extraData = cursor.getString(cursor.getColumnIndexOrThrow("st_extraInfo"));
        downloadInfo.statInfo.searchId = cursor.getLong(cursor.getColumnIndexOrThrow("searchId"));
        downloadInfo.response = new m();
        downloadInfo.hostAppId = cursor.getString(cursor.getColumnIndexOrThrow("hostAppId"));
        downloadInfo.via = cursor.getString(cursor.getColumnIndexOrThrow("via"));
        downloadInfo.uin = cursor.getString(cursor.getColumnIndexOrThrow("uin"));
        downloadInfo.uinType = cursor.getString(cursor.getColumnIndexOrThrow("uinType"));
        downloadInfo.downloadEndTime = cursor.getLong(cursor.getColumnIndexOrThrow("download_end_time"));
        downloadInfo.grayVersionCode = cursor.getInt(cursor.getColumnIndexOrThrow("grayVersionCode"));
        downloadInfo.channelId = cursor.getString(cursor.getColumnIndexOrThrow("channelId"));
        downloadInfo.setFilePath(cursor.getString(cursor.getColumnIndexOrThrow("filePath")));
        downloadInfo.actionFlag = (byte) cursor.getInt(cursor.getColumnIndexOrThrow("actionFlag"));
        downloadInfo.uiType = SimpleDownloadInfo.UIType.values()[cursor.getInt(cursor.getColumnIndexOrThrow("uiType"))];
        downloadInfo.sdkId = cursor.getString(cursor.getColumnIndexOrThrow("sdkId"));
        downloadInfo.categoryId = cursor.getLong(cursor.getColumnIndexOrThrow("categoryId"));
        downloadInfo.downloadingPath = cursor.getString(cursor.getColumnIndexOrThrow("downloadingPath"));
        downloadInfo.statInfo.callerVia = cursor.getString(cursor.getColumnIndexOrThrow("stVia"));
        downloadInfo.statInfo.callerUin = cursor.getString(cursor.getColumnIndexOrThrow("stUin"));
        downloadInfo.minQLauncherVersionCode = cursor.getInt(cursor.getColumnIndex("minQLauncherVersionCode"));
        downloadInfo.maxQLauncherVersionCode = cursor.getInt(cursor.getColumnIndex("maxQLauncherVersionCode"));
        downloadInfo.themeVersionCode = cursor.getInt(cursor.getColumnIndex("themeVersionCode"));
        downloadInfo.localVersionCode = cursor.getInt(cursor.getColumnIndex("localVersionCode"));
        downloadInfo.sllLocalManifestMd5 = cursor.getString(cursor.getColumnIndex("sllLocalManifestMd5"));
        downloadInfo.sllLocalVersionCode = cursor.getInt(cursor.getColumnIndex("sllLocalVersionCode"));
        return downloadInfo;
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x002e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.ArrayList<com.tencent.assistant.download.DownloadInfo> a() {
        /*
            r5 = this;
            r2 = 0
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            com.tencent.assistant.db.helper.SqliteHelper r1 = r5.getHelper()
            com.tencent.assistant.db.helper.SQLiteDatabaseWrapper r1 = r1.getReadableDatabaseWrapper()
            java.lang.String r3 = "select * from downloadsinfo"
            r4 = 0
            android.database.Cursor r2 = r1.rawQuery(r3, r4)     // Catch:{ Exception -> 0x0032 }
            if (r2 == 0) goto L_0x002c
            boolean r1 = r2.moveToFirst()     // Catch:{ Exception -> 0x0032 }
            if (r1 == 0) goto L_0x002c
        L_0x001d:
            com.tencent.assistant.download.DownloadInfo r1 = r5.a(r2)     // Catch:{ Exception -> 0x0032 }
            if (r1 == 0) goto L_0x0026
            r0.add(r1)     // Catch:{ Exception -> 0x0032 }
        L_0x0026:
            boolean r1 = r2.moveToNext()     // Catch:{ Exception -> 0x0032 }
            if (r1 != 0) goto L_0x001d
        L_0x002c:
            if (r2 == 0) goto L_0x0031
            r2.close()
        L_0x0031:
            return r0
        L_0x0032:
            r1 = move-exception
            r1.printStackTrace()     // Catch:{ all -> 0x003c }
            if (r2 == 0) goto L_0x0031
            r2.close()
            goto L_0x0031
        L_0x003c:
            r0 = move-exception
            if (r2 == 0) goto L_0x0042
            r2.close()
        L_0x0042:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.db.table.o.a():java.util.ArrayList");
    }

    public void b() {
        getHelper().getWritableDatabaseWrapper().delete("downloadsinfo", null, null);
    }

    public int tableVersion() {
        return 1;
    }

    public String tableName() {
        return "downloadsinfo";
    }

    public String createTableSQL() {
        return "CREATE TABLE if not exists downloadsinfo(_id INTEGER PRIMARY KEY AUTOINCREMENT,fileType TEXT,downloadUrl TEXT UNIQUE,appId INTEGER,packageName TEXT,name TEXT,iconUrl TEXT,downloadTimes INTEGER,rating INTEGER,versionName TEXT,versionCode INTEGER,apkUrl TEXT,fileSize INTEGER,fileMd5 TEXT,newFeature TEXT,signatrue TEXT, apkDate INTEGER, sllApkUrl TEXT, sllFileSize INTEGER, sllFileMd5 TEXT,sllUpdate INTEGER,isUpdate INTEGER,flag INTEGER, mergeFilePath TEXT,createTime INTEGER,sourceSence INTEGER,sourceVersion INTEGER,st_extraInfo TEXT,state INTEGER,downloadTicket TEXT,subType INTEGER,apkId INTEGER,searchId INTEGER,fakePercent INTEGER,hostAppId TEXT,hostPackageName TEXT,hostVersionCode TEXT,via TEXT,taskId TEXT,uin TEXT,uinType TEXT,download_end_time INTEGER,grayVersionCode INTEGER,filePath TEXT ,channelId TEXT,actionFlag INTEGER,uiType INTEGER,categoryId INTEGER,sdkId TEXT,downloadingPath TEXT,stVia TEXT,stUin TEXT,stBeaconSN INTEGER,stSourceBeaconSN INTEGER,minQLauncherVersionCode INTEGER DEFAULT 0,maxQLauncherVersionCode INTEGER DEFAULT 0,themeVersionCode INTEGER DEFAULT 0,localVersionCode INTEGER,sllLocalManifestMd5 TEXT,sllLocalVersionCode INTEGER)";
    }

    public String[] getAlterSQL(int i, int i2) {
        if (i == 1 && i2 == 2) {
            return new String[]{"alter table downloadsinfo add column hostAppId TEXT;", "alter table downloadsinfo add column hostPackageName TEXT;", "alter table downloadsinfo add column hostVersionCode TEXT;", "alter table downloadsinfo add column via TEXT;", "alter table downloadsinfo add column taskId TEXT;", "alter table downloadsinfo add column uin TEXT;", "alter table downloadsinfo add column uinType TEXT;", "alter table downloadsinfo add column download_end_time INTEGER;", "alter table downloadsinfo add column grayVersionCode INTEGER;", "alter table downloadsinfo add column filePath TEXT;", "alter table downloadsinfo add column channelId TEXT;"};
        } else if (i == 2 && i2 == 3) {
            return new String[]{"alter table downloadsinfo add column actionFlag INTEGER;"};
        } else if (i == 3 && i2 == 4) {
            return new String[]{"alter table downloadsinfo add column uiType INTEGER;"};
        } else {
            if (!(i == 4 && i2 == 5)) {
                if (i == 5 && i2 == 6) {
                    return new String[]{"alter table downloadsinfo add column sdkId TEXT;", "alter table downloadsinfo add column downloadingPath TEXT;", "alter table downloadsinfo add column categoryId INTEGER;"};
                } else if (i == 6 && i2 == 7) {
                    return new String[]{"alter table downloadsinfo add column stVia TEXT;", "alter table downloadsinfo add column stUin TEXT;", "alter table downloadsinfo add column stBeaconSN INTEGER;", "alter table downloadsinfo add column stSourceBeaconSN INTEGER;"};
                } else if (i == 7 && i2 == 8) {
                    return new String[]{"alter table downloadsinfo add column minQLauncherVersionCode INTEGER DEFAULT 0;", "alter table downloadsinfo add column maxQLauncherVersionCode INTEGER DEFAULT 0;", "alter table downloadsinfo add column themeVersionCode INTEGER DEFAULT 0;"};
                } else if (i == 8 && i2 == 9) {
                    return new String[]{"alter table downloadsinfo add column localVersionCode INTEGER;"};
                } else if (i == 9 && i2 == 10) {
                    return new String[]{"alter table downloadsinfo add column sllLocalManifestMd5 TEXT;", "alter table downloadsinfo add column sllLocalVersionCode INTEGER;"};
                }
            }
            return null;
        }
    }

    public void beforeTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
        if (i == 1 && i2 == 2) {
            Cursor cursor = null;
            try {
                cursor = sQLiteDatabase.rawQuery("select * from downloadsinfo as a left outer join tbl_download as b on a.downloadTicket = b.id", new String[0]);
                if (cursor != null && cursor.moveToFirst()) {
                    while (!cursor.isAfterLast()) {
                        String string = cursor.getString(cursor.getColumnIndexOrThrow(CommentDetailTabView.PARAMS_APK_ID));
                        String string2 = cursor.getString(cursor.getColumnIndexOrThrow("save_name"));
                        ContentValues contentValues = new ContentValues();
                        contentValues.put("id", string);
                        sQLiteDatabase.update("tbl_download", contentValues, " save_name = ? ", new String[]{String.valueOf(string2)});
                        cursor.moveToNext();
                    }
                }
                sQLiteDatabase.execSQL("update downloadsinfo set downloadTicket = apkId");
                if (cursor != null) {
                    cursor.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
                if (cursor != null) {
                    cursor.close();
                }
            } catch (Throwable th) {
                if (cursor != null) {
                    cursor.close();
                }
                throw th;
            }
        }
    }

    public SqliteHelper getHelper() {
        return DownloadDbHelper.get(AstApp.i());
    }

    public void afterTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
        if (i == 3 && i2 == 4) {
            sQLiteDatabase.execSQL("update downloadsinfo set uiType = " + SimpleDownloadInfo.UIType.NORMAL.ordinal());
        } else if (i == 5 && i2 == 6) {
            sQLiteDatabase.execSQL("update downloadsinfo set sdkId = 0");
            sQLiteDatabase.execSQL("update downloadsinfo set downloadingPath = filePath");
        }
    }
}
