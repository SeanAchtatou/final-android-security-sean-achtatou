package com.tencent.assistant.db.table;

import android.database.sqlite.SQLiteDatabase;
import com.qq.AppService.AstApp;
import com.tencent.assistant.db.helper.AstDbHelper;
import com.tencent.assistant.db.helper.SqliteHelper;

/* compiled from: ProGuard */
public class a implements IBaseTable {
    public int tableVersion() {
        return 1;
    }

    public String tableName() {
        return "action_task_table";
    }

    public String createTableSQL() {
        return "CREATE TABLE if not exists action_task_table( _id INTEGER PRIMARY KEY AUTOINCREMENT, packageName TEXT , version TEXT,action TEXT, result INTEGER);";
    }

    public String[] getAlterSQL(int i, int i2) {
        return null;
    }

    public SqliteHelper getHelper() {
        return AstDbHelper.get(AstApp.i());
    }

    public void beforeTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public void afterTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }
}
