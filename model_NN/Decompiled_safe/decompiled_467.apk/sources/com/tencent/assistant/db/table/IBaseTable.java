package com.tencent.assistant.db.table;

import android.database.sqlite.SQLiteDatabase;
import com.tencent.assistant.db.helper.SqliteHelper;

/* compiled from: ProGuard */
public interface IBaseTable {
    void afterTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase);

    void beforeTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase);

    String createTableSQL();

    String[] getAlterSQL(int i, int i2);

    SqliteHelper getHelper();

    String tableName();

    int tableVersion();
}
