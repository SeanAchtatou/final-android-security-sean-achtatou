package com.tencent.assistant.g;

import com.tencent.assistant.model.ShareAppModel;
import com.tencent.assistant.utils.v;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
class k implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ e f1317a;

    k(e eVar) {
        this.f1317a = eVar;
    }

    public void run() {
        if (this.f1317a.e == null) {
            return;
        }
        if (!this.f1317a.m) {
            v.a(this.f1317a.e.get(), this.f1317a.d, this.f1317a.o);
        } else if (this.f1317a.d instanceof ShareAppModel) {
            ShareAppModel shareAppModel = (ShareAppModel) this.f1317a.d;
            if (shareAppModel.b == null) {
                shareAppModel.b = Constants.STR_EMPTY;
            }
            this.f1317a.a(shareAppModel.e, this.f1317a.c(shareAppModel), this.f1317a.b(shareAppModel), shareAppModel.b, shareAppModel.f, null);
        }
    }
}
