package com.tencent.assistant.g;

import android.content.Context;
import android.os.Handler;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.ShareBaseActivity;
import com.tencent.assistant.component.ShareAppContentView;
import com.tencent.assistant.component.ShareAppDialog;
import com.tencent.assistant.model.ShareAppModel;
import com.tencent.assistant.model.ShareBaseModel;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.v;

/* compiled from: ProGuard */
public class p {

    /* renamed from: a  reason: collision with root package name */
    private static p f1322a;
    private int b;
    /* access modifiers changed from: private */
    public r c = new r(this, null);

    private p() {
    }

    public static p a() {
        if (f1322a == null) {
            f1322a = new p();
        }
        return f1322a;
    }

    public void b() {
        d();
    }

    public ShareAppDialog a(ShareBaseActivity shareBaseActivity) {
        ShareAppModel b2 = this.c.b();
        if (b2 == null || b2.e == 0) {
            XLog.i("YYBShareOganizer", "use default data");
            b2 = b(shareBaseActivity);
        }
        d();
        return v.a(shareBaseActivity, b2, 0);
    }

    public boolean a(int i, ShareBaseActivity shareBaseActivity) {
        boolean z = false;
        XLog.i("YYBShareOganizer", "showShareDialogIfNeed");
        int d = o.d();
        XLog.i("YYBShareOganizer", "downloadNum = " + d);
        if (d < 10) {
            int i2 = d + 1;
            o.c(i2);
            if (i2 >= 10) {
                new Handler(shareBaseActivity.getMainLooper()).postDelayed(new q(this, i, shareBaseActivity), 2000);
                z = true;
            }
        }
        d();
        return z;
    }

    /* access modifiers changed from: private */
    public CharSequence a(Context context) {
        String string = context.getString(R.string.share_content_once);
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(string);
        spannableStringBuilder.setSpan(new AbsoluteSizeSpan(context.getResources().getDimensionPixelSize(R.dimen.share_content_textsize_1)), 0, 3, 33);
        spannableStringBuilder.setSpan(new AbsoluteSizeSpan(context.getResources().getDimensionPixelSize(R.dimen.share_content_textsize_2)), 3, string.length(), 33);
        return spannableStringBuilder;
    }

    public ShareAppDialog a(ShareBaseActivity shareBaseActivity, ShareAppModel shareAppModel, boolean z, int i, int i2) {
        ShareAppContentView shareAppContentView = new ShareAppContentView(shareBaseActivity);
        shareAppContentView.setContentLength(16);
        shareAppContentView.setShareContentText(a(shareBaseActivity, shareAppModel.l));
        if (i != 0) {
            shareAppContentView.setMascotSrc(i);
        }
        if (z) {
            o.f();
            return v.a(shareBaseActivity, shareAppModel, shareAppContentView, (int) STConst.ST_PAGE_APP_SHARE_BETA, i2);
        } else if (!o.g()) {
            return null;
        } else {
            o.f();
            return v.a(shareBaseActivity, shareAppModel, shareAppContentView, (int) STConst.ST_PAGE_APP_DETAIL_OTHERS, i2);
        }
    }

    private CharSequence a(Context context, String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(str);
        spannableStringBuilder.setSpan(new AbsoluteSizeSpan(context.getResources().getDimensionPixelSize(R.dimen.share_content_textsize_3)), 0, str.length(), 33);
        return spannableStringBuilder;
    }

    public ShareAppDialog a(ShareBaseActivity shareBaseActivity, ShareBaseModel shareBaseModel) {
        ShareAppContentView shareAppContentView = new ShareAppContentView(shareBaseActivity);
        shareAppContentView.setContentLength(16);
        shareAppContentView.setShareContentText(a(shareBaseActivity, shareBaseModel.f));
        return v.a(shareBaseActivity, shareBaseModel, shareAppContentView, (int) STConst.ST_PAGE_APP_SHARE_H5, (int) R.string.dialog_share_to_friend);
    }

    private void d() {
        ShareAppModel b2 = this.c.b();
        if (b2 == null || b2.e == 0 || System.currentTimeMillis() - o.c() > 604800000) {
            this.c.a();
            XLog.i("YYBShareOganizer", "use appdetailengine");
            o.a(System.currentTimeMillis());
        }
    }

    /* access modifiers changed from: private */
    public ShareAppModel b(Context context) {
        ShareAppModel shareAppModel = new ShareAppModel();
        shareAppModel.e = 5848;
        shareAppModel.g = context.getString(R.string.app_name);
        shareAppModel.f = "http://shp.qpic.cn/ma_icon/0/icon_5848_17266235_1390032639/72";
        shareAppModel.f1632a = context.getString(R.string.share_default_category);
        shareAppModel.i = 220000000;
        shareAppModel.h = 3.665d;
        return shareAppModel;
    }

    public void a(int i) {
        this.b = i;
    }

    public void c() {
        this.b = 0;
    }

    /* access modifiers changed from: private */
    public boolean b(int i) {
        return this.b != 0 && this.b == i;
    }
}
