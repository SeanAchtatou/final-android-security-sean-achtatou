package com.tencent.assistant.net;

/* compiled from: ProGuard */
/* synthetic */ class a {

    /* renamed from: a  reason: collision with root package name */
    static final /* synthetic */ int[] f1924a = new int[APN.values().length];

    static {
        try {
            f1924a[APN.UN_DETECT.ordinal()] = 1;
        } catch (NoSuchFieldError e) {
        }
        try {
            f1924a[APN.WIFI.ordinal()] = 2;
        } catch (NoSuchFieldError e2) {
        }
        try {
            f1924a[APN.CMWAP.ordinal()] = 3;
        } catch (NoSuchFieldError e3) {
        }
        try {
            f1924a[APN.CMNET.ordinal()] = 4;
        } catch (NoSuchFieldError e4) {
        }
        try {
            f1924a[APN.UNIWAP.ordinal()] = 5;
        } catch (NoSuchFieldError e5) {
        }
        try {
            f1924a[APN.UNINET.ordinal()] = 6;
        } catch (NoSuchFieldError e6) {
        }
        try {
            f1924a[APN.WAP3G.ordinal()] = 7;
        } catch (NoSuchFieldError e7) {
        }
        try {
            f1924a[APN.NET3G.ordinal()] = 8;
        } catch (NoSuchFieldError e8) {
        }
        try {
            f1924a[APN.CTWAP.ordinal()] = 9;
        } catch (NoSuchFieldError e9) {
        }
        try {
            f1924a[APN.CTNET.ordinal()] = 10;
        } catch (NoSuchFieldError e10) {
        }
        try {
            f1924a[APN.UNKNOWN.ordinal()] = 11;
        } catch (NoSuchFieldError e11) {
        }
        try {
            f1924a[APN.UNKNOW_WAP.ordinal()] = 12;
        } catch (NoSuchFieldError e12) {
        }
        try {
            f1924a[APN.NO_NETWORK.ordinal()] = 13;
        } catch (NoSuchFieldError e13) {
        }
    }
}
