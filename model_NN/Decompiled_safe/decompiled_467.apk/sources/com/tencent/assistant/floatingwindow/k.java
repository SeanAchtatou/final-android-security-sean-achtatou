package com.tencent.assistant.floatingwindow;

import android.content.Intent;
import com.qq.AppService.AstApp;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.spaceclean.SpaceScanManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.t;

/* compiled from: ProGuard */
class k implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ j f1297a;

    k(j jVar) {
        this.f1297a = jVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, long):long
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean */
    public void run() {
        if (t.y()) {
            XLog.d("miles", "is xiaomi rom. don't show floating window.");
        } else if (m.a().o() || (m.a().a("key_float_window_manual_state", true) && m.a().a("key_float_window_should_auto_show", false) && !SpaceScanManager.a().k())) {
            AstApp.i().startService(new Intent(AstApp.i(), FloatingWindowService.class));
            m.a().f(true);
            this.f1297a.h();
        }
    }
}
