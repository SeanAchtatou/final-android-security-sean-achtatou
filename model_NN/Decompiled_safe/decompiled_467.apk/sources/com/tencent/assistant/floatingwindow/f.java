package com.tencent.assistant.floatingwindow;

import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
class f implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ e f1292a;

    f(e eVar) {
        this.f1292a = eVar;
    }

    public void run() {
        XLog.d("floatingwindow", "onAnimationEnd.. isAnimationFinish = true. isAccelerateFinish = " + this.f1292a.f1291a.t);
        if (!j.a().e()) {
            XLog.d("floatingwindow", "window not showing. do nothing but return");
            return;
        }
        boolean unused = this.f1292a.f1291a.v = true;
        this.f1292a.f1291a.g();
        this.f1292a.f1291a.d.setVisibility(8);
        this.f1292a.f1291a.f.width = FloatWindowSmallView.f1280a;
        this.f1292a.f1291a.f.height = FloatWindowSmallView.b;
        this.f1292a.f1291a.f.x = (int) (this.f1292a.f1291a.j - this.f1292a.f1291a.l);
        this.f1292a.f1291a.f.y = (int) (this.f1292a.f1291a.k - this.f1292a.f1291a.m);
        try {
            this.f1292a.f1291a.c.updateViewLayout(this.f1292a.f1291a, this.f1292a.f1291a.f);
            this.f1292a.f1291a.g.topMargin = 0;
            this.f1292a.f1291a.g.leftMargin = 0;
            this.f1292a.f1291a.updateViewLayout(this.f1292a.f1291a.r, this.f1292a.f1291a.g);
            if (!this.f1292a.f1291a.t) {
                this.f1292a.f1291a.d.setVisibility(8);
            } else {
                this.f1292a.f1291a.l();
            }
        } catch (Throwable th) {
            th.printStackTrace();
            XLog.d("floatingwindow", "FloatWindowSmallView >> <onAnimationEnd> updateViewLayout exception");
        }
    }
}
