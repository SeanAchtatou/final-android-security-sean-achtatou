package com.tencent.assistant.floatingwindow;

/* compiled from: ProGuard */
class v implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Wave f1307a;

    private v(Wave wave) {
        this.f1307a = wave;
    }

    public void run() {
        long j = 0;
        synchronized (this.f1307a) {
            long currentTimeMillis = System.currentTimeMillis();
            this.f1307a.d();
            this.f1307a.invalidate();
            long currentTimeMillis2 = 16 - (System.currentTimeMillis() - currentTimeMillis);
            Wave wave = this.f1307a;
            if (currentTimeMillis2 >= 0) {
                j = currentTimeMillis2;
            }
            wave.postDelayed(this, j);
        }
    }
}
