package com.tencent.assistant.model.a;

import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.manager.SelfUpdateManager;
import com.tencent.assistant.protocol.jce.SmartCardAppList;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class o extends i {

    /* renamed from: a  reason: collision with root package name */
    public SelfUpdateManager.SelfUpdateInfo f1648a;
    public LocalApkInfo b;

    public void a(SmartCardAppList smartCardAppList, int i) {
        this.i = i;
        if (smartCardAppList != null) {
            this.k = smartCardAppList.f2314a;
            this.m = smartCardAppList.b;
            this.n = smartCardAppList.c;
            this.o = smartCardAppList.d;
        }
    }

    public List<Long> h() {
        ArrayList arrayList = new ArrayList();
        if (this.f1648a != null) {
            arrayList.add(Long.valueOf(this.f1648a.f1469a));
        }
        return arrayList;
    }
}
