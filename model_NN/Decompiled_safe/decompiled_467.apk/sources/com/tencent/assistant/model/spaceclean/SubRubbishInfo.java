package com.tencent.assistant.model.spaceclean;

import android.text.TextUtils;
import java.util.ArrayList;
import org.json.JSONArray;

/* compiled from: ProGuard */
public class SubRubbishInfo {

    /* renamed from: a  reason: collision with root package name */
    public RubbishType f1673a;
    public String b;
    public long c;
    public boolean d = false;
    public boolean e = false;
    public ArrayList<String> f = new ArrayList<>();

    /* compiled from: ProGuard */
    public enum RubbishType {
        UNKNOWN,
        BIG_FILE,
        DOC_FILE,
        IMAGE_FILE,
        MUSIC_FILE,
        VIDEO_FILE,
        ZIP_FILE,
        APK_FILE
    }

    public SubRubbishInfo() {
    }

    public SubRubbishInfo(RubbishType rubbishType, String str, long j, boolean z, String str2) {
        this.f1673a = rubbishType;
        this.b = str;
        if (this.b == null || TextUtils.isEmpty(this.b)) {
            this.b = "未知";
        }
        this.c = j;
        this.d = z;
        this.e = z;
        this.f.add(str2);
    }

    public SubRubbishInfo(String str, long j, boolean z, JSONArray jSONArray) {
        this.b = str;
        if (this.b == null || TextUtils.isEmpty(this.b)) {
            this.b = "未知";
        }
        this.c = j;
        this.d = z;
        this.e = z;
        if (jSONArray != null && jSONArray.length() > 0) {
            for (int i = 0; i < jSONArray.length(); i++) {
                Object obj = jSONArray.get(i);
                if (obj != null) {
                    this.f.add(obj.toString());
                }
            }
        }
    }

    public void a(JSONArray jSONArray) {
        if (jSONArray != null && jSONArray.length() > 0) {
            for (int i = 0; i < jSONArray.length(); i++) {
                Object obj = jSONArray.get(i);
                if (obj != null) {
                    this.f.add(obj.toString());
                }
            }
        }
    }
}
