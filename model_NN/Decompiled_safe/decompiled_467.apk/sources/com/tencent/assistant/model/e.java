package com.tencent.assistant.model;

import android.text.TextUtils;
import com.tencent.assistant.model.a.i;
import com.tencent.assistantv2.model.SimpleEbookModel;
import com.tencent.assistantv2.model.SimpleVideoModel;
import java.util.List;

/* compiled from: ProGuard */
public class e {

    /* renamed from: a  reason: collision with root package name */
    public int f1662a = 0;
    public int b = 1;
    public SimpleAppModel c;
    public List<SimpleAppModel> d;
    public SimpleVideoModel e;
    public SimpleEbookModel f;
    public i g;
    public String h;

    public int a() {
        return this.f1662a;
    }

    public List<SimpleAppModel> b() {
        return this.d;
    }

    public e() {
    }

    public e(SimpleAppModel simpleAppModel) {
        this.c = simpleAppModel;
        this.b = 1;
    }

    public SimpleAppModel c() {
        return this.c;
    }

    public SimpleVideoModel d() {
        return this.e;
    }

    public SimpleEbookModel e() {
        return this.f;
    }

    public i f() {
        return this.g;
    }

    public boolean g() {
        if (this.b == 1 && this.c != null) {
            if (!TextUtils.isEmpty(this.c.X)) {
                return true;
            }
            if (this.c.av == null || this.c.av.a() == 0) {
                return false;
            }
            return true;
        }
        return false;
    }
}
