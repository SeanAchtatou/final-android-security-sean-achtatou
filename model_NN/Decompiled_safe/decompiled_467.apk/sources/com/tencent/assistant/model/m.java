package com.tencent.assistant.model;

import android.os.Parcel;
import android.os.Parcelable;

/* compiled from: ProGuard */
final class m implements Parcelable.Creator<ShareBaseModel> {
    m() {
    }

    /* renamed from: a */
    public ShareBaseModel createFromParcel(Parcel parcel) {
        ShareBaseModel shareBaseModel = new ShareBaseModel();
        shareBaseModel.f1633a = parcel.readString();
        shareBaseModel.d = parcel.readString();
        shareBaseModel.c = parcel.readString();
        shareBaseModel.d = parcel.readString();
        shareBaseModel.e = parcel.readString();
        return shareBaseModel;
    }

    /* renamed from: a */
    public ShareBaseModel[] newArray(int i) {
        return null;
    }
}
