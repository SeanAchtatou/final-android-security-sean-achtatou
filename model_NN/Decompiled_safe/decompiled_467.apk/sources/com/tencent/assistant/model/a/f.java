package com.tencent.assistant.model.a;

import com.tencent.assistant.model.SimpleAppModel;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class f extends i {

    /* renamed from: a  reason: collision with root package name */
    public int f1640a;
    public ArrayList<SimpleAppModel> b = new ArrayList<>();

    public List<Long> h() {
        ArrayList arrayList = new ArrayList();
        if (this.b != null && this.b.size() > 0) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= this.f1640a || i2 >= this.b.size()) {
                    break;
                }
                arrayList.add(Long.valueOf(this.b.get(i2).f1634a));
                i = i2 + 1;
            }
        }
        return arrayList;
    }

    public void a(List<Long> list) {
        if (list != null && list.size() != 0 && this.b != null && this.b.size() != 0) {
            ArrayList arrayList = new ArrayList();
            Iterator<SimpleAppModel> it = this.b.iterator();
            while (it.hasNext()) {
                SimpleAppModel next = it.next();
                if (list.contains(Long.valueOf(next.f1634a))) {
                    arrayList.add(next);
                }
            }
            this.b.removeAll(arrayList);
        }
    }
}
