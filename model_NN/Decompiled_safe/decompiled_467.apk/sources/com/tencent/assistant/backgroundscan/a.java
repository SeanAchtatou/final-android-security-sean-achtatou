package com.tencent.assistant.backgroundscan;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.text.TextUtils;
import android.util.SparseArray;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.db.table.f;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.notification.y;
import com.tencent.assistant.protocol.jce.ContextItem;
import com.tencent.assistant.protocol.jce.PushMsgItem;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.bt;
import com.tencent.connect.common.Constants;
import com.tencent.open.SocialConstants;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/* compiled from: ProGuard */
public class a {
    private static a c = null;
    /* access modifiers changed from: private */
    public static long g = 60000;

    /* renamed from: a  reason: collision with root package name */
    private Context f850a;
    /* access modifiers changed from: private */
    public volatile boolean b;
    /* access modifiers changed from: private */
    public String d;
    /* access modifiers changed from: private */
    public String e;
    private boolean f;

    private a() {
        this.f850a = null;
        this.b = false;
        this.d = null;
        this.e = null;
        this.f = false;
        this.f850a = AstApp.i();
    }

    /* access modifiers changed from: package-private */
    public void a(byte b2, int i) {
        long j;
        XLog.d("BackgroundScan", "<push> adjustWeight");
        PushMsgItem pushMsgItem = BackgroundScanManager.a().c().get(b2);
        BackgroundScan a2 = BackgroundScanManager.a().a(b2);
        XLog.d("BackgroundScan", "<push> Before adjustWeight: type = " + ((int) a2.f846a) + ", timeGap = " + a2.b + ", weight = " + a2.c);
        switch (i) {
            case 1:
                XLog.d("BackgroundScan", "<push> adjust click weight");
                a2.c = a2.c <= 0.0d ? pushMsgItem.b * pushMsgItem.h : a2.c * pushMsgItem.h;
                a2.b = a2.b <= 0 ? (long) (((double) a(pushMsgItem.e)) * (2.0d - pushMsgItem.h)) : (long) (((double) a2.b) * (2.0d - pushMsgItem.h));
                break;
            case 2:
                XLog.d("BackgroundScan", "<push> adjust cancel weight");
                a2.c = a2.c <= 0.0d ? pushMsgItem.b * pushMsgItem.i : a2.c * pushMsgItem.i;
                a2.b = a2.b <= 0 ? (long) (((double) a(pushMsgItem.e)) * (2.0d - pushMsgItem.i)) : (long) (((double) a2.b) * (2.0d - pushMsgItem.i));
                break;
            case 3:
                XLog.d("BackgroundScan", "<push> adjust refresh weight");
                a2.c = a2.c <= 0.0d ? pushMsgItem.b * pushMsgItem.j : a2.c * pushMsgItem.j;
                if (a2.b <= 0) {
                    j = (long) (((double) a(pushMsgItem.e)) * (2.0d - pushMsgItem.j));
                } else {
                    j = (long) (((double) a2.b) * (2.0d - pushMsgItem.j));
                }
                a2.b = j;
                break;
        }
        if (a2.c < pushMsgItem.c) {
            a2.c = pushMsgItem.c;
        }
        if (a2.c > pushMsgItem.d) {
            a2.c = pushMsgItem.d;
        }
        if (a2.b < a(pushMsgItem.f)) {
            a2.b = a(pushMsgItem.f);
        }
        if (a2.b > a(pushMsgItem.g)) {
            a2.b = a(pushMsgItem.g);
        }
        XLog.d("BackgroundScan", "<push> After adjustWeight: type = " + ((int) a2.f846a) + ", timeGap = " + a2.b + ", weight = " + a2.c);
        f.a().b(a2);
    }

    public static long a(double d2) {
        return (long) (d2 * 60.0d * 60.0d * 1000.0d);
    }

    /* access modifiers changed from: private */
    public void a(byte b2) {
        byte a2;
        if (this.f && (a2 = m.a().a("background_scan_current_send_push_type", (byte) 0)) > 0 && a2 != b2) {
            a(a2, 3);
            n.a().a("b_new_scan_push_refresh", b2, this.d, this.e);
        }
        if (b2 > 0) {
            m.a().b("background_scan_current_send_push_type", Byte.valueOf(b2));
            this.f = true;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.b(byte, int):void
      com.tencent.assistant.m.b(byte, java.lang.String):void
      com.tencent.assistant.m.b(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.b(java.lang.Long, int):boolean
      com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean */
    /* access modifiers changed from: package-private */
    public void a() {
        m.a().b("background_scan_current_send_push_type", (Object) 0);
        this.b = false;
    }

    public static synchronized a b() {
        a aVar;
        synchronized (a.class) {
            if (c == null) {
                c = new a();
            }
            aVar = c;
        }
        return aVar;
    }

    public synchronized void c() {
        XLog.d("BackgroundScan", "<push> push");
        if (this.b) {
            XLog.d("BackgroundScan", "<push> Push is running now, will appear right now !");
        } else {
            this.b = true;
            List<BackgroundScan> b2 = f.a().b();
            String str = Constants.STR_EMPTY;
            for (BackgroundScan backgroundScan : b2) {
                str = str + backgroundScan.toString() + " | ";
            }
            n.a().a("b_new_scan_push_begin_push", (byte) 0, null, null, str, 0);
            if (!h()) {
                XLog.d("BackgroundScan", "<push> Push condation not satified !!");
                this.b = false;
            } else {
                TemporaryThreadManager.get().start(new b(this, str));
            }
        }
    }

    private boolean h() {
        boolean z;
        int i;
        int d2 = BackgroundScanManager.a().d();
        boolean s = m.a().s();
        if (e() <= d2 && d2 > 0) {
            if (!BackgroundScanManager.a().f()) {
                if (s) {
                    i = 3;
                    Iterator<BackgroundScan> it = f.a().b().iterator();
                    while (true) {
                        if (it.hasNext()) {
                            if (it.next().e >= 0) {
                                z = true;
                                break;
                            }
                        } else {
                            z = false;
                            break;
                        }
                    }
                } else {
                    XLog.d("BackgroundScan", "<push> Push switch has been closed !");
                    i = 2;
                    z = false;
                }
            } else {
                XLog.d("BackgroundScan", "<push> Background scan is running, will push later!");
                i = 1;
                z = false;
            }
        } else {
            XLog.d("BackgroundScan", "<push> has reached the day push limit today, limit = " + d2);
            i = 0;
            z = false;
        }
        n.a().a("b_new_scan_push_scan_can_push", (byte) i, Constants.STR_EMPTY, Constants.STR_EMPTY, String.valueOf(z), -1);
        return z;
    }

    /* access modifiers changed from: package-private */
    public boolean d() {
        return this.b;
    }

    private SparseArray<Integer> i() {
        boolean z;
        boolean z2 = true;
        XLog.i("BackgroundScan", "<push> getWeights");
        List<BackgroundScan> b2 = f.a().b();
        SparseArray<PushMsgItem> c2 = BackgroundScanManager.a().c();
        SparseArray<Integer> sparseArray = new SparseArray<>();
        if (b2.size() > 0 && c2.size() > 0) {
            for (BackgroundScan next : b2) {
                if (next.e > 0 && Math.abs(System.currentTimeMillis() - next.d) > next.b) {
                    switch (next.f846a) {
                        case 1:
                            if (next.e > 60) {
                                break;
                            }
                            sparseArray.put(next.f846a, Integer.valueOf((int) (next.c * 100.0d)));
                            XLog.d("BackgroundScan", "<push> Maybe push, type = " + ((int) next.f846a) + ", weight = " + next.c);
                            break;
                        case 2:
                            if (next.e == 1) {
                                break;
                            }
                            sparseArray.put(next.f846a, Integer.valueOf((int) (next.c * 100.0d)));
                            XLog.d("BackgroundScan", "<push> Maybe push, type = " + ((int) next.f846a) + ", weight = " + next.c);
                            break;
                        case 3:
                            if (next.e < 3) {
                                break;
                            }
                            sparseArray.put(next.f846a, Integer.valueOf((int) (next.c * 100.0d)));
                            XLog.d("BackgroundScan", "<push> Maybe push, type = " + ((int) next.f846a) + ", weight = " + next.c);
                            break;
                        case 4:
                            if (next.e < 83886080) {
                                break;
                            }
                            sparseArray.put(next.f846a, Integer.valueOf((int) (next.c * 100.0d)));
                            XLog.d("BackgroundScan", "<push> Maybe push, type = " + ((int) next.f846a) + ", weight = " + next.c);
                            break;
                        case 5:
                            if (next.e < 536870912) {
                                break;
                            }
                            sparseArray.put(next.f846a, Integer.valueOf((int) (next.c * 100.0d)));
                            XLog.d("BackgroundScan", "<push> Maybe push, type = " + ((int) next.f846a) + ", weight = " + next.c);
                            break;
                        case 6:
                            if (next.e <= 0) {
                                break;
                            }
                            sparseArray.put(next.f846a, Integer.valueOf((int) (next.c * 100.0d)));
                            XLog.d("BackgroundScan", "<push> Maybe push, type = " + ((int) next.f846a) + ", weight = " + next.c);
                            break;
                    }
                }
            }
        } else {
            n a2 = n.a();
            String str = this.d;
            String str2 = this.e;
            StringBuilder sb = new StringBuilder();
            if (b2.size() > 0) {
                z = true;
            } else {
                z = false;
            }
            StringBuilder append = sb.append(z).append(",");
            if (c2.size() <= 0) {
                z2 = false;
            }
            a2.a("b_new_scan_push_weight", (byte) 0, str, str2, append.append(z2).toString(), 0);
        }
        return sparseArray;
    }

    /* access modifiers changed from: private */
    public byte j() {
        XLog.i("BackgroundScan", "<push> randomSelect");
        SparseArray<Integer> i = i();
        if (i == null || i.size() <= 0) {
            n.a().a("b_new_scan_push_select", (byte) 0, this.d, this.e);
            return 0;
        }
        int i2 = 0;
        for (int i3 = 0; i3 < i.size(); i3++) {
            i2 += i.valueAt(i3).intValue();
        }
        if (i2 <= 0) {
            return 0;
        }
        SparseArray sparseArray = new SparseArray();
        int i4 = 0;
        while (i4 < i.size()) {
            int intValue = (i.valueAt(i4).intValue() * 10000) / i2;
            sparseArray.put(i.keyAt(i4), Integer.valueOf(i4 == 0 ? intValue : ((Integer) sparseArray.valueAt(i4 - 1)).intValue() + intValue));
            i4++;
        }
        int b2 = (int) BackgroundScanTimerJob.b(10000);
        for (int i5 = 0; i5 < sparseArray.size(); i5++) {
            if (b2 <= ((Integer) sparseArray.valueAt(i5)).intValue()) {
                return (byte) sparseArray.keyAt(i5);
            }
        }
        return 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.b(byte, int):void
      com.tencent.assistant.m.b(byte, java.lang.String):void
      com.tencent.assistant.m.b(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.b(java.lang.Long, int):boolean
      com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean */
    /* access modifiers changed from: package-private */
    public int e() {
        int a2 = m.a().a("key_background_scan_push_day", 0);
        int a3 = a(System.currentTimeMillis());
        if (a2 != 0 && a2 == a3) {
            return m.a().a("key_background_scan_day_push_times", 0);
        }
        m.a().b("key_background_scan_push_day", Integer.valueOf(a3));
        m.a().b("key_background_scan_day_push_times", (Object) 0);
        return 0;
    }

    public void f() {
        m.a().b("key_background_scan_day_push_times", Integer.valueOf(m.a().a("key_background_scan_day_push_times", 0) + 1));
    }

    static int a(long j) {
        try {
            return Integer.parseInt(new SimpleDateFormat("yyyyMMdd").format(new Date(j)));
        } catch (NumberFormatException e2) {
            return 0;
        }
    }

    /* access modifiers changed from: private */
    public Notification b(byte b2) {
        XLog.i("BackgroundScan", "<push> getNotification");
        ContextItem c2 = c(b2);
        CharSequence a2 = a(b2, c2);
        CharSequence b3 = b(b2, c2);
        XLog.d("BackgroundScan", "The random selected content text is : <" + ((Object) b3) + ">");
        this.d = a2.toString();
        this.e = b3.toString();
        Intent intent = new Intent(this.f850a, BackgroundPushReceiver.class);
        intent.setAction("com.tencent.android.qqdownloader.background.PUSH_CLICK");
        intent.putExtra(SocialConstants.PARAM_TYPE, b2);
        intent.putExtra("contentTitle", a2.toString());
        intent.putExtra("contentText", b3.toString());
        PendingIntent broadcast = PendingIntent.getBroadcast(this.f850a, 1, intent, 134217728);
        Intent intent2 = new Intent(this.f850a, BackgroundPushReceiver.class);
        intent2.setAction("com.tencent.android.qqdownloader.background.PUSH_CANCEL");
        intent2.putExtra(SocialConstants.PARAM_TYPE, b2);
        return y.a(this.f850a, R.drawable.notification_manual_icon, a2, b3, b3, System.currentTimeMillis(), broadcast, PendingIntent.getBroadcast(this.f850a, 2, intent2, 134217728), true, false);
    }

    private CharSequence a(byte b2, ContextItem contextItem) {
        String string = this.f850a.getResources().getString(R.string.app_name);
        BackgroundScan a2 = BackgroundScanManager.a().a(b2);
        if (TextUtils.isEmpty(contextItem.b)) {
            return string;
        }
        if (contextItem.b != null && contextItem.b.contains("$para")) {
            string = contextItem.b.replace("$para", a(b2, a2));
        } else if (contextItem.b != null) {
            string = contextItem.b;
        }
        return Html.fromHtml(string.toString());
    }

    private CharSequence b(byte b2, ContextItem contextItem) {
        BackgroundScan a2 = BackgroundScanManager.a().a(b2);
        String str = contextItem.c;
        if (contextItem.c != null && contextItem.c.contains("$para")) {
            str = contextItem.c.replace("$para", a(b2, a2));
        } else if (contextItem.c != null) {
            str = contextItem.c;
        }
        return Html.fromHtml(str.toString());
    }

    private String a(byte b2, BackgroundScan backgroundScan) {
        switch (b2) {
            case 1:
                return String.valueOf(backgroundScan.e);
            case 2:
                return backgroundScan.e == 2 ? "85%" : Constants.STR_EMPTY;
            case 3:
                return String.valueOf(backgroundScan.e);
            case 4:
                return bt.a(backgroundScan.e);
            case 5:
                return bt.c(backgroundScan.e);
            case 6:
                return String.valueOf(backgroundScan.e);
            default:
                return Constants.STR_EMPTY;
        }
    }

    private ContextItem c(byte b2) {
        ArrayList<ContextItem> arrayList;
        int intValue;
        XLog.i("BackgroundScan", "<push> getContentText");
        PushMsgItem pushMsgItem = BackgroundScanManager.a().c().get(b2);
        if (!(pushMsgItem == null || (arrayList = pushMsgItem.m) == null || arrayList.size() <= 0)) {
            Iterator<ContextItem> it = arrayList.iterator();
            double d2 = 0.0d;
            while (it.hasNext()) {
                d2 = it.next().d + d2;
            }
            if (d2 > 0.0d) {
                LinkedHashMap linkedHashMap = new LinkedHashMap();
                int i = 0;
                while (true) {
                    int i2 = i;
                    if (i2 >= arrayList.size()) {
                        break;
                    }
                    ContextItem contextItem = arrayList.get(i2);
                    int i3 = (int) ((contextItem.d / d2) * 10000.0d);
                    if (i2 == 0) {
                        intValue = i3;
                    } else {
                        intValue = ((Integer) linkedHashMap.get(arrayList.get(i2 - 1))).intValue() + i3;
                    }
                    linkedHashMap.put(contextItem, Integer.valueOf(intValue));
                    i = i2 + 1;
                }
                int b3 = (int) BackgroundScanTimerJob.b(10000);
                for (Map.Entry entry : linkedHashMap.entrySet()) {
                    if (b3 <= ((Integer) entry.getValue()).intValue()) {
                        return (ContextItem) entry.getKey();
                    }
                }
            }
        }
        return null;
    }
}
