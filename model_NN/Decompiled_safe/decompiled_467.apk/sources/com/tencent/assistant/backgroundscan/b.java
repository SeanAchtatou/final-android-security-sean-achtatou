package com.tencent.assistant.backgroundscan;

import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.q;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/* compiled from: ProGuard */
class b implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f851a;
    final /* synthetic */ a b;

    b(a aVar, String str) {
        this.b = aVar;
        this.f851a = str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.backgroundscan.a.a(com.tencent.assistant.backgroundscan.a, boolean):boolean
     arg types: [com.tencent.assistant.backgroundscan.a, int]
     candidates:
      com.tencent.assistant.backgroundscan.a.a(com.tencent.assistant.backgroundscan.a, byte):android.app.Notification
      com.tencent.assistant.backgroundscan.a.a(byte, com.tencent.assistant.protocol.jce.ContextItem):java.lang.CharSequence
      com.tencent.assistant.backgroundscan.a.a(byte, com.tencent.assistant.backgroundscan.BackgroundScan):java.lang.String
      com.tencent.assistant.backgroundscan.a.a(byte, int):void
      com.tencent.assistant.backgroundscan.a.a(com.tencent.assistant.backgroundscan.a, boolean):boolean */
    public void run() {
        byte a2 = this.b.j();
        XLog.d("BackgroundScan", "<push> The random selected push type is : " + ((int) a2));
        long b2 = BackgroundScanTimerJob.b(a.g);
        if (a2 <= 0) {
            XLog.d("BackgroundScan", "<push> There is no push !");
            boolean unused = this.b.b = false;
            n.a().a("b_new_scan_push_unsatisfy", a2, this.b.d, this.b.e, this.f851a, 0);
            return;
        }
        n.a().a("b_new_scan_push_satisfy", a2, this.b.d, this.b.e, this.f851a, b2);
        Executors.newSingleThreadScheduledExecutor(new q("backgroupscan")).schedule(new c(this, a2), b2, TimeUnit.MILLISECONDS);
    }
}
