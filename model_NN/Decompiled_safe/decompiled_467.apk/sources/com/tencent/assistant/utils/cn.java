package com.tencent.assistant.utils;

import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
public class cn {
    public static int a(int i) {
        switch (i) {
            case 1:
                return R.drawable.pic_title_book_label;
            case 2:
                return R.drawable.pic_title_gift_label;
            case 3:
                return R.drawable.pic_title_hot_label;
            case 4:
                return R.drawable.pic_title_music_label;
            case 5:
                return R.drawable.pic_title_spread_label;
            case 6:
                return R.drawable.pic_title_task_label;
            case 7:
                return R.drawable.pic_title_topic_label;
            case 8:
                return R.drawable.pic_title_video_label;
            case 9:
                return R.drawable.pic_title_ent_label;
            case 10:
                return R.drawable.pic_title_pre_label;
            default:
                return 0;
        }
    }
}
