package com.tencent.assistant.utils;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.os.PowerManager;
import android.os.StatFs;
import android.os.SystemProperties;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.kapalaiadapter.a;
import com.tencent.assistant.kapalaiadapter.d;
import com.tencent.assistant.m;
import com.tencent.connect.common.Constants;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.List;
import java.util.regex.Pattern;

/* compiled from: ProGuard */
public class t {

    /* renamed from: a  reason: collision with root package name */
    public static boolean f2723a = true;
    public static int b;
    public static int c;
    public static float d;
    public static String e;
    static int f = 0;
    private static boolean g = true;
    private static String h;

    public static void a(Context context) {
        int i = context.getResources().getConfiguration().orientation;
        if (i == 1) {
            b = context.getResources().getDisplayMetrics().widthPixels;
            c = context.getResources().getDisplayMetrics().heightPixels;
        } else if (i == 2) {
            b = context.getResources().getDisplayMetrics().heightPixels;
            c = context.getResources().getDisplayMetrics().widthPixels;
        } else {
            int i2 = context.getResources().getDisplayMetrics().widthPixels;
            int i3 = context.getResources().getDisplayMetrics().heightPixels;
            b = Math.min(i2, i3);
            c = Math.max(i2, i3);
        }
        d = context.getResources().getDisplayMetrics().density;
        if ((b > c ? b : c) < 800 || d <= 1.0f) {
            g = false;
        }
        if (r.d() < 7 || !r.e()) {
            f2723a = false;
        }
        m.a().a(n());
    }

    public static final int b(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivityManager == null) {
            return -1;
        }
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo == null || !activeNetworkInfo.isAvailable()) {
            return -1;
        }
        if (activeNetworkInfo.getType() == 1) {
            return 0;
        }
        switch (((TelephonyManager) context.getSystemService("phone")).getNetworkType()) {
            case 1:
                return 2;
            case 2:
                return 2;
            case 3:
            default:
                return 1;
            case 4:
                return 2;
        }
    }

    public static final int a() {
        return (int) ((Runtime.getRuntime().maxMemory() / 1024) / 1024);
    }

    public static final float b() {
        return ((float) a()) / 48.0f;
    }

    public static long c() {
        File dataDirectory = Environment.getDataDirectory();
        if (!dataDirectory.exists()) {
            return -1;
        }
        StatFs statFs = new StatFs(dataDirectory.getPath());
        return ((long) statFs.getAvailableBlocks()) * ((long) statFs.getBlockSize());
    }

    public static long d() {
        File dataDirectory = Environment.getDataDirectory();
        if (!dataDirectory.exists()) {
            return -1;
        }
        StatFs statFs = new StatFs(dataDirectory.getPath());
        return ((long) statFs.getBlockCount()) * ((long) statFs.getBlockSize());
    }

    public static long e() {
        if (!FileUtil.isSDCardExistAndCanWrite()) {
            return -1;
        }
        File externalStorageDirectory = Environment.getExternalStorageDirectory();
        if (!externalStorageDirectory.exists()) {
            return -1;
        }
        try {
            StatFs statFs = new StatFs(externalStorageDirectory.getPath());
            return ((long) statFs.getAvailableBlocks()) * ((long) statFs.getBlockSize());
        } catch (Throwable th) {
            return -1;
        }
    }

    public static long f() {
        if (!FileUtil.isSDCardExistAndCanWrite()) {
            return -1;
        }
        File externalStorageDirectory = Environment.getExternalStorageDirectory();
        if (!externalStorageDirectory.exists()) {
            return -1;
        }
        try {
            StatFs statFs = new StatFs(externalStorageDirectory.getPath());
            return ((long) statFs.getBlockCount()) * ((long) statFs.getBlockSize());
        } catch (Exception e2) {
            return -1;
        }
    }

    public static String g() {
        int i = -1;
        try {
            i = AstApp.i().checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE");
        } catch (Exception e2) {
        }
        if (i != 0) {
            return Constants.STR_EMPTY;
        }
        try {
            String deviceId = ((TelephonyManager) AstApp.i().getSystemService("phone")).getDeviceId();
            if (TextUtils.isEmpty(deviceId)) {
                return Constants.STR_EMPTY;
            }
            return deviceId;
        } catch (Throwable th) {
            return Constants.STR_EMPTY;
        }
    }

    public static String h() {
        if (AstApp.i().checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") != 0) {
            return Constants.STR_EMPTY;
        }
        try {
            String subscriberId = ((TelephonyManager) AstApp.i().getSystemService("phone")).getSubscriberId();
            if (TextUtils.isEmpty(subscriberId)) {
                return Constants.STR_EMPTY;
            }
            return subscriberId;
        } catch (Exception e2) {
            return Constants.STR_EMPTY;
        }
    }

    public static String[] i() {
        String[] b2;
        if (AstApp.i().checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") != 0) {
            return null;
        }
        String[] strArr = new String[2];
        try {
            if (!d.f) {
                b2 = a.a().a(AstApp.i());
            } else {
                b2 = a.a().b(AstApp.i());
            }
            if (b2 == null || b2.length == 0) {
                return null;
            }
            return b2;
        } catch (Exception e2) {
            return null;
        }
    }

    public static String[] j() {
        String[] d2;
        int i = -1;
        try {
            i = AstApp.i().checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE");
        } catch (Exception e2) {
        }
        if (i != 0) {
            return null;
        }
        String[] strArr = new String[2];
        try {
            if (!d.f) {
                d2 = a.a().c(AstApp.i());
            } else {
                d2 = a.a().d(AstApp.i());
            }
            if (d2 == null || d2.length == 0) {
                return null;
            }
            return d2;
        } catch (Exception e3) {
            return null;
        }
    }

    public static boolean a(String[] strArr, int i) {
        try {
            if (strArr[i] == null || TextUtils.isEmpty(strArr[i])) {
                return false;
            }
            return true;
        } catch (Exception e2) {
            return false;
        }
    }

    public static String k() {
        try {
            WifiInfo connectionInfo = ((WifiManager) AstApp.i().getSystemService("wifi")).getConnectionInfo();
            if (connectionInfo != null) {
                return connectionInfo.getMacAddress();
            }
            return Constants.STR_EMPTY;
        } catch (Throwable th) {
            th.printStackTrace();
            return Constants.STR_EMPTY;
        }
    }

    public static String l() {
        return Settings.Secure.getString(AstApp.i().getContentResolver(), "android_id");
    }

    public static String m() {
        if (FileUtil.isSDCardExistAndCanRead()) {
            return FileUtil.read(FileUtil.getCommonRootDir() + "/" + ".aid");
        }
        return Constants.STR_EMPTY;
    }

    public static void a(String str, String str2) {
        if (!TextUtils.isEmpty(str) && TextUtils.isEmpty(str2) && FileUtil.isSDCardExistAndCanWrite()) {
            TemporaryThreadManager.get().start(new u(str));
        }
    }

    public static AppConst.ROOT_STATUS n() {
        int a2 = ci.a(false);
        if (a2 > 0) {
            return AppConst.ROOT_STATUS.ROOTED;
        }
        if (a2 == 0) {
            return AppConst.ROOT_STATUS.UNROOTED;
        }
        return AppConst.ROOT_STATUS.UNKNOWN;
    }

    public static int o() {
        if (f > 0) {
            return f;
        }
        try {
            PackageInfo packageInfo = AstApp.i().getPackageManager().getPackageInfo(AstApp.i().getPackageName(), 0);
            if (packageInfo != null) {
                f = packageInfo.versionCode;
            }
        } catch (PackageManager.NameNotFoundException e2) {
            e2.printStackTrace();
        }
        return f;
    }

    public static boolean p() {
        if (r.d() >= 11) {
            try {
                return ((Boolean) Environment.class.getMethod("isExternalStorageEmulated", new Class[0]).invoke(null, new Object[0])).booleanValue();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:32:0x0056 A[SYNTHETIC, Splitter:B:32:0x0056] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x005b A[SYNTHETIC, Splitter:B:35:0x005b] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0066 A[SYNTHETIC, Splitter:B:41:0x0066] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x006b A[SYNTHETIC, Splitter:B:44:0x006b] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static long q() {
        /*
            r0 = 0
            r4 = 0
            java.lang.String r2 = "/proc/meminfo"
            java.io.FileReader r5 = new java.io.FileReader     // Catch:{ Exception -> 0x004f, all -> 0x0061 }
            r5.<init>(r2)     // Catch:{ Exception -> 0x004f, all -> 0x0061 }
            java.io.BufferedReader r3 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0092, all -> 0x008a }
            r2 = 8192(0x2000, float:1.14794E-41)
            r3.<init>(r5, r2)     // Catch:{ Exception -> 0x0092, all -> 0x008a }
            java.lang.String r2 = r3.readLine()     // Catch:{ Exception -> 0x0096, all -> 0x008d }
            if (r2 != 0) goto L_0x0022
            if (r5 == 0) goto L_0x001c
            r5.close()     // Catch:{ IOException -> 0x007e }
        L_0x001c:
            if (r3 == 0) goto L_0x0021
            r3.close()     // Catch:{ IOException -> 0x0083 }
        L_0x0021:
            return r0
        L_0x0022:
            r4 = 58
            int r4 = r2.indexOf(r4)     // Catch:{ Exception -> 0x0096, all -> 0x008d }
            r6 = 107(0x6b, float:1.5E-43)
            int r6 = r2.indexOf(r6)     // Catch:{ Exception -> 0x0096, all -> 0x008d }
            int r4 = r4 + 1
            java.lang.String r2 = r2.substring(r4, r6)     // Catch:{ Exception -> 0x0096, all -> 0x008d }
            java.lang.String r2 = r2.trim()     // Catch:{ Exception -> 0x0096, all -> 0x008d }
            long r0 = java.lang.Long.parseLong(r2)     // Catch:{ Exception -> 0x0096, all -> 0x008d }
            r6 = 1024(0x400, double:5.06E-321)
            long r0 = r0 * r6
            if (r5 == 0) goto L_0x0044
            r5.close()     // Catch:{ IOException -> 0x0085 }
        L_0x0044:
            if (r3 == 0) goto L_0x0021
            r3.close()     // Catch:{ IOException -> 0x004a }
            goto L_0x0021
        L_0x004a:
            r2 = move-exception
        L_0x004b:
            r2.printStackTrace()
            goto L_0x0021
        L_0x004f:
            r2 = move-exception
            r3 = r4
        L_0x0051:
            r2.printStackTrace()     // Catch:{ all -> 0x008f }
            if (r4 == 0) goto L_0x0059
            r4.close()     // Catch:{ IOException -> 0x0079 }
        L_0x0059:
            if (r3 == 0) goto L_0x0021
            r3.close()     // Catch:{ IOException -> 0x005f }
            goto L_0x0021
        L_0x005f:
            r2 = move-exception
            goto L_0x004b
        L_0x0061:
            r0 = move-exception
            r3 = r4
            r5 = r4
        L_0x0064:
            if (r5 == 0) goto L_0x0069
            r5.close()     // Catch:{ IOException -> 0x006f }
        L_0x0069:
            if (r3 == 0) goto L_0x006e
            r3.close()     // Catch:{ IOException -> 0x0074 }
        L_0x006e:
            throw r0
        L_0x006f:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0069
        L_0x0074:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x006e
        L_0x0079:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0059
        L_0x007e:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x001c
        L_0x0083:
            r2 = move-exception
            goto L_0x004b
        L_0x0085:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0044
        L_0x008a:
            r0 = move-exception
            r3 = r4
            goto L_0x0064
        L_0x008d:
            r0 = move-exception
            goto L_0x0064
        L_0x008f:
            r0 = move-exception
            r5 = r4
            goto L_0x0064
        L_0x0092:
            r2 = move-exception
            r3 = r4
            r4 = r5
            goto L_0x0051
        L_0x0096:
            r2 = move-exception
            r4 = r5
            goto L_0x0051
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.utils.t.q():long");
    }

    public static long r() {
        FileReader fileReader;
        BufferedReader bufferedReader;
        FileReader fileReader2;
        BufferedReader bufferedReader2 = null;
        try {
            fileReader = new FileReader("/proc/meminfo");
            try {
                bufferedReader = new BufferedReader(fileReader, 8192);
                try {
                    bufferedReader.readLine();
                    String readLine = bufferedReader.readLine();
                    long b2 = b(bufferedReader.readLine()) + b(readLine) + b(bufferedReader.readLine());
                    p.a(fileReader);
                    p.a(bufferedReader);
                    return b2;
                } catch (Exception e2) {
                    e = e2;
                    bufferedReader2 = bufferedReader;
                    fileReader2 = fileReader;
                    try {
                        e.printStackTrace();
                        p.a(fileReader2);
                        p.a(bufferedReader2);
                        return 0;
                    } catch (Throwable th) {
                        th = th;
                        fileReader = fileReader2;
                        bufferedReader = bufferedReader2;
                        p.a(fileReader);
                        p.a(bufferedReader);
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    p.a(fileReader);
                    p.a(bufferedReader);
                    throw th;
                }
            } catch (Exception e3) {
                e = e3;
                fileReader2 = fileReader;
                e.printStackTrace();
                p.a(fileReader2);
                p.a(bufferedReader2);
                return 0;
            } catch (Throwable th3) {
                th = th3;
                bufferedReader = null;
                p.a(fileReader);
                p.a(bufferedReader);
                throw th;
            }
        } catch (Exception e4) {
            e = e4;
            fileReader2 = null;
            e.printStackTrace();
            p.a(fileReader2);
            p.a(bufferedReader2);
            return 0;
        } catch (Throwable th4) {
            th = th4;
            bufferedReader = null;
            fileReader = null;
            p.a(fileReader);
            p.a(bufferedReader);
            throw th;
        }
    }

    private static long b(String str) {
        if (str == null) {
            return 0;
        }
        try {
            return Long.parseLong(str.substring(str.indexOf(58) + 1, str.indexOf(107)).trim()) * 1024;
        } catch (Exception e2) {
            return 0;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x001c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String s() {
        /*
            r2 = 0
            java.lang.String r1 = android.os.Build.CPU_ABI
            int r0 = android.os.Build.VERSION.SDK_INT
            r3 = 8
            if (r0 < r3) goto L_0x0052
            java.lang.Class<android.os.Build> r0 = android.os.Build.class
            java.lang.String r3 = "CPU_ABI2"
            java.lang.reflect.Field r0 = r0.getField(r3)     // Catch:{ SecurityException -> 0x0034, NoSuchFieldException -> 0x003a }
        L_0x0011:
            if (r0 == 0) goto L_0x0050
            r3 = 0
            java.lang.Object r0 = r0.get(r3)     // Catch:{ IllegalArgumentException -> 0x0040, IllegalAccessException -> 0x0046, Throwable -> 0x004c }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ IllegalArgumentException -> 0x0040, IllegalAccessException -> 0x0046, Throwable -> 0x004c }
        L_0x001a:
            if (r0 == 0) goto L_0x0052
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.StringBuilder r1 = r2.append(r1)
            java.lang.String r2 = ","
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r0 = r0.toString()
        L_0x0033:
            return r0
        L_0x0034:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r2
            goto L_0x0011
        L_0x003a:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r2
            goto L_0x0011
        L_0x0040:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r2
            goto L_0x001a
        L_0x0046:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r2
            goto L_0x001a
        L_0x004c:
            r0 = move-exception
            r0.printStackTrace()
        L_0x0050:
            r0 = r2
            goto L_0x001a
        L_0x0052:
            r0 = r1
            goto L_0x0033
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.utils.t.s():java.lang.String");
    }

    public static String t() {
        if (TextUtils.isEmpty(e)) {
            e = Build.MODEL;
        }
        return e;
    }

    public static String u() {
        XLog.d("BACKUP_TAG", "android.os.Build.MANUFACTURER = " + Build.MANUFACTURER + " android.os.Build.MODEL = " + Build.MODEL);
        return Build.MANUFACTURER + "-" + Build.MODEL;
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x0075 A[SYNTHETIC, Splitter:B:27:0x0075] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x007a  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0083 A[SYNTHETIC, Splitter:B:34:0x0083] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0088  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String v() {
        /*
            r2 = 0
            java.lang.String r0 = com.tencent.assistant.utils.t.h
            if (r0 == 0) goto L_0x0008
            java.lang.String r0 = com.tencent.assistant.utils.t.h
        L_0x0007:
            return r0
        L_0x0008:
            java.lang.Runtime r0 = java.lang.Runtime.getRuntime()     // Catch:{ Exception -> 0x006b, all -> 0x007e }
            java.lang.String r1 = "cat /proc/version"
            java.lang.Process r3 = r0.exec(r1)     // Catch:{ Exception -> 0x006b, all -> 0x007e }
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ Exception -> 0x00a3, all -> 0x009b }
            java.io.InputStreamReader r0 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x00a3, all -> 0x009b }
            java.io.InputStream r4 = r3.getInputStream()     // Catch:{ Exception -> 0x00a3, all -> 0x009b }
            r0.<init>(r4)     // Catch:{ Exception -> 0x00a3, all -> 0x009b }
            r4 = 8192(0x2000, float:1.14794E-41)
            r1.<init>(r0, r4)     // Catch:{ Exception -> 0x00a3, all -> 0x009b }
            java.lang.String r0 = ""
        L_0x0024:
            java.lang.String r2 = r1.readLine()     // Catch:{ Exception -> 0x00a7, all -> 0x009e }
            if (r2 == 0) goto L_0x003c
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00a7, all -> 0x009e }
            r4.<init>()     // Catch:{ Exception -> 0x00a7, all -> 0x009e }
            java.lang.StringBuilder r0 = r4.append(r0)     // Catch:{ Exception -> 0x00a7, all -> 0x009e }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x00a7, all -> 0x009e }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x00a7, all -> 0x009e }
            goto L_0x0024
        L_0x003c:
            boolean r2 = android.text.TextUtils.isEmpty(r0)     // Catch:{ Exception -> 0x00a7, all -> 0x009e }
            if (r2 != 0) goto L_0x005e
            java.lang.String r2 = "version "
            int r4 = r0.indexOf(r2)     // Catch:{ Exception -> 0x00a7, all -> 0x009e }
            int r2 = r2.length()     // Catch:{ Exception -> 0x00a7, all -> 0x009e }
            int r2 = r2 + r4
            java.lang.String r0 = r0.substring(r2)     // Catch:{ Exception -> 0x00a7, all -> 0x009e }
            java.lang.String r2 = " "
            int r2 = r0.indexOf(r2)     // Catch:{ Exception -> 0x00a7, all -> 0x009e }
            r4 = 0
            java.lang.String r0 = r0.substring(r4, r2)     // Catch:{ Exception -> 0x00a7, all -> 0x009e }
            com.tencent.assistant.utils.t.h = r0     // Catch:{ Exception -> 0x00a7, all -> 0x009e }
        L_0x005e:
            if (r1 == 0) goto L_0x0063
            r1.close()     // Catch:{ IOException -> 0x0096 }
        L_0x0063:
            if (r3 == 0) goto L_0x0068
            r3.destroy()
        L_0x0068:
            java.lang.String r0 = com.tencent.assistant.utils.t.h
            goto L_0x0007
        L_0x006b:
            r0 = move-exception
            r1 = r2
        L_0x006d:
            r3 = 0
            com.tencent.assistant.utils.t.h = r3     // Catch:{ all -> 0x00a0 }
            r0.printStackTrace()     // Catch:{ all -> 0x00a0 }
            if (r1 == 0) goto L_0x0078
            r1.close()     // Catch:{ IOException -> 0x0091 }
        L_0x0078:
            if (r2 == 0) goto L_0x0068
            r2.destroy()
            goto L_0x0068
        L_0x007e:
            r0 = move-exception
            r1 = r2
            r3 = r2
        L_0x0081:
            if (r1 == 0) goto L_0x0086
            r1.close()     // Catch:{ IOException -> 0x008c }
        L_0x0086:
            if (r3 == 0) goto L_0x008b
            r3.destroy()
        L_0x008b:
            throw r0
        L_0x008c:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0086
        L_0x0091:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0078
        L_0x0096:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0063
        L_0x009b:
            r0 = move-exception
            r1 = r2
            goto L_0x0081
        L_0x009e:
            r0 = move-exception
            goto L_0x0081
        L_0x00a0:
            r0 = move-exception
            r3 = r2
            goto L_0x0081
        L_0x00a3:
            r0 = move-exception
            r1 = r2
            r2 = r3
            goto L_0x006d
        L_0x00a7:
            r0 = move-exception
            r2 = r3
            goto L_0x006d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.utils.t.v():java.lang.String");
    }

    public static boolean w() {
        PowerManager powerManager = (PowerManager) AstApp.i().getSystemService("power");
        return powerManager != null && powerManager.isScreenOn();
    }

    public static List<ActivityManager.RecentTaskInfo> a(int i) {
        if (AstApp.i().checkCallingOrSelfPermission("android.permission.GET_TASKS") == 0) {
            return ((ActivityManager) AstApp.i().getSystemService("activity")).getRecentTasks(i, 1);
        }
        return null;
    }

    public static boolean a(int i, String str) {
        List<ActivityManager.RecentTaskInfo> a2 = a(i);
        if (a2 != null && a2.size() > 0) {
            for (ActivityManager.RecentTaskInfo recentTaskInfo : a2) {
                Intent intent = recentTaskInfo.baseIntent;
                if (intent != null && intent.getComponent().getPackageName().equals(str)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static long x() {
        File file = new File("/system/usr");
        if (file != null && file.exists()) {
            return file.lastModified();
        }
        File file2 = new File("/system/app");
        if (file2 == null || !file2.exists()) {
            return 0;
        }
        return file2.lastModified();
    }

    public static boolean y() {
        if (!TextUtils.isEmpty(SystemProperties.get("ro.miui.ui.version.name", null))) {
            return true;
        }
        if (Build.MANUFACTURER != null) {
            return a(Build.MANUFACTURER).toLowerCase().contains("xiaomi");
        }
        return false;
    }

    public static String a(String str) {
        if (str != null) {
            return Pattern.compile("\\s*|\t|\r|\n").matcher(str).replaceAll(Constants.STR_EMPTY);
        }
        return Constants.STR_EMPTY;
    }
}
