package com.tencent.assistant.utils;

import java.util.concurrent.FutureTask;

/* compiled from: ProGuard */
class cr implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ FutureTask f2673a;
    final /* synthetic */ TemporaryThreadManager b;

    cr(TemporaryThreadManager temporaryThreadManager, FutureTask futureTask) {
        this.b = temporaryThreadManager;
        this.f2673a = futureTask;
    }

    public void run() {
        if (!this.f2673a.isDone() && !this.f2673a.isCancelled()) {
            this.f2673a.cancel(true);
        }
    }
}
