package com.tencent.assistant.utils.installuninstall;

/* compiled from: ProGuard */
class ab implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f2688a;
    final /* synthetic */ String b;
    final /* synthetic */ String c;
    final /* synthetic */ boolean d;
    final /* synthetic */ String e;
    final /* synthetic */ p f;

    ab(p pVar, int i, String str, String str2, boolean z, String str3) {
        this.f = pVar;
        this.f2688a = i;
        this.b = str;
        this.c = str2;
        this.d = z;
        this.e = str3;
    }

    public void run() {
        InstallUninstallTaskBean installUninstallTaskBean = new InstallUninstallTaskBean(this.f2688a, -1, this.b, this.c);
        if (!this.d) {
            installUninstallTaskBean.trySystemAfterSilentFail = false;
            installUninstallTaskBean.isSystemApp = true;
            installUninstallTaskBean.filePath = this.e;
        }
        this.f.c(installUninstallTaskBean);
    }
}
