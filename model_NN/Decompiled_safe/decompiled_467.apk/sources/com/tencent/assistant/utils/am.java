package com.tencent.assistant.utils;

import android.app.Dialog;
import android.view.View;
import com.tencent.assistant.AppConst;

/* compiled from: ProGuard */
final class am implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppConst.TwoBtnDialogInfo f2634a;
    final /* synthetic */ Dialog b;

    am(AppConst.TwoBtnDialogInfo twoBtnDialogInfo, Dialog dialog) {
        this.f2634a = twoBtnDialogInfo;
        this.b = dialog;
    }

    public void onClick(View view) {
        if (this.f2634a != null) {
            this.f2634a.onRightBtnClick();
            this.b.dismiss();
        }
    }
}
