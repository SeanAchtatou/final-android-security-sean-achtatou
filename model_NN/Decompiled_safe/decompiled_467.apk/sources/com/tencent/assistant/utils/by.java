package com.tencent.assistant.utils;

import android.content.Context;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.m;
import com.tencent.assistant.module.dl;
import com.tencent.assistant.module.timer.RecoverAppListReceiver;
import com.tencent.assistant.net.c;
import com.tencent.assistant.protocol.jce.CardItem;
import com.tencent.assistant.protocol.jce.GetPhoneUserAppListResponse;
import com.tencent.assistant.protocol.jce.PopUpInfo;
import com.tencent.assistantv2.activity.MainActivity;
import com.tencent.connect.common.Constants;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
final class by implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GetPhoneUserAppListResponse f2661a;
    final /* synthetic */ Context b;
    final /* synthetic */ int c;
    final /* synthetic */ boolean d;
    final /* synthetic */ boolean e;

    by(GetPhoneUserAppListResponse getPhoneUserAppListResponse, Context context, int i, boolean z, boolean z2) {
        this.f2661a = getPhoneUserAppListResponse;
        this.b = context;
        this.c = i;
        this.d = z;
        this.e = z2;
    }

    public void run() {
        boolean z;
        boolean z2;
        int i;
        if (this.f2661a != null) {
            ArrayList<CardItem> arrayList = new ArrayList<>();
            Iterator<CardItem> it = this.f2661a.c.iterator();
            while (it.hasNext()) {
                CardItem next = it.next();
                XLog.e("zhangyuanchao", "-------cardItemList------:" + next.f.b);
                if (ApkResourceManager.getInstance().getLocalApkInfo(next.f.f) == null) {
                    arrayList.add(next);
                }
            }
            this.f2661a.c = arrayList;
            if (arrayList.size() > 0) {
                ba.a().post(new bz(this));
                return;
            }
            boolean z3 = m.a().a("key_re_app_list_state", 0) == RecoverAppListReceiver.RecoverAppListState.SECONDLY_SHORT.ordinal();
            m.a().b(Constants.STR_EMPTY, "key_re_app_list_state", Integer.valueOf(RecoverAppListReceiver.RecoverAppListState.NOMAL.ordinal()));
            MainActivity.x = RecoverAppListReceiver.RecoverAppListState.NOMAL.ordinal();
            ba.a().post(new ca(this, z3));
            return;
        }
        List<LocalApkInfo> localApkInfos = ApkResourceManager.getInstance().getLocalApkInfos();
        XLog.e("zhangyuanchao", "------------localApkInfos:" + localApkInfos);
        if (localApkInfos != null && localApkInfos.size() != 0) {
            if (bx.c == null || bx.c.size() == 0) {
                ArrayList unused = bx.c = bx.b.a();
            }
            long currentTimeMillis = System.currentTimeMillis();
            if (bx.c.size() > 0) {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                if (simpleDateFormat.format(Long.valueOf(((PopUpInfo) bx.c.get(0)).d)).equals(simpleDateFormat.format(Long.valueOf(currentTimeMillis)))) {
                    bx.a(this.c, 0, 3);
                    return;
                }
            }
            ArrayList<Object> a2 = dl.a().a((byte) 2);
            if (a2 != null && a2.size() != 0) {
                ArrayList arrayList2 = new ArrayList();
                int i2 = 0;
                for (LocalApkInfo next2 : localApkInfos) {
                    arrayList2.add(next2.mPackageName);
                    if ((next2.flags & 1) == 0 && (next2.flags & 128) == 0) {
                        i2++;
                    }
                }
                int i3 = 0;
                while (true) {
                    int i4 = i3;
                    if (i4 < a2.size()) {
                        PopUpInfo popUpInfo = (PopUpInfo) a2.get(i4);
                        if (popUpInfo != null) {
                            long j = currentTimeMillis / 1000;
                            if (j <= popUpInfo.d || j >= popUpInfo.e) {
                                bx.a(this.c, popUpInfo.f2258a, 3);
                            } else if (popUpInfo.m == 0) {
                                bx.a(this.c, popUpInfo.f2258a, 3);
                            } else {
                                PopUpInfo popUpInfo2 = null;
                                int i5 = 0;
                                while (true) {
                                    if (i5 >= bx.c.size()) {
                                        z = false;
                                        z2 = true;
                                        break;
                                    }
                                    popUpInfo2 = (PopUpInfo) bx.c.get(i5);
                                    if (popUpInfo2.f2258a != popUpInfo.f2258a) {
                                        i5++;
                                    } else if (popUpInfo2.d + ((long) (popUpInfo2.f * 86400000)) < currentTimeMillis) {
                                        z = true;
                                        z2 = true;
                                    } else {
                                        z2 = false;
                                        z = false;
                                    }
                                }
                                if (z2) {
                                    if (z) {
                                        bx.c.remove(popUpInfo2);
                                        bx.b.a(popUpInfo2.f2258a);
                                    }
                                    boolean z4 = false;
                                    int c2 = c.c();
                                    if ((popUpInfo.g & 1) > 0) {
                                        z4 = c2 == 4;
                                    }
                                    if (((popUpInfo.g >> 1) & 1) > 0) {
                                        z4 = z4 || c2 == 3;
                                    }
                                    if (((popUpInfo.g >> 2) & 1) > 0) {
                                        z4 = z4 || c2 == 1;
                                    }
                                    if (((popUpInfo.g >> 3) & 1) > 0) {
                                        z4 = z4 || c2 == 2;
                                    }
                                    if (!z4) {
                                        bx.a(this.c, popUpInfo.f2258a, 3);
                                    } else if (i2 > popUpInfo.h) {
                                        bx.a(this.c, popUpInfo.f2258a, 2);
                                    } else {
                                        if (popUpInfo.l != null && popUpInfo.l.size() > 0) {
                                            int i6 = 0;
                                            Iterator<CardItem> it2 = popUpInfo.l.iterator();
                                            while (true) {
                                                i = i6;
                                                if (!it2.hasNext()) {
                                                    break;
                                                } else if (arrayList2.contains(it2.next().f.f)) {
                                                    i6 = i + 1;
                                                } else {
                                                    i6 = i;
                                                }
                                            }
                                            if (i < popUpInfo.j) {
                                                bx.a(this.c, popUpInfo.f2258a, 3);
                                            }
                                        }
                                        if (popUpInfo.k == null) {
                                            bx.a(this.c, popUpInfo.f2258a, 3);
                                        } else {
                                            if (popUpInfo.k.size() > 0) {
                                                Iterator<CardItem> it3 = popUpInfo.k.iterator();
                                                int i7 = 0;
                                                while (it3.hasNext()) {
                                                    if (arrayList2.contains(it3.next().f.f)) {
                                                        it3.remove();
                                                        i7++;
                                                    }
                                                }
                                                if (i7 > popUpInfo.i) {
                                                    bx.a(this.c, popUpInfo.f2258a, 3);
                                                }
                                            }
                                            if (popUpInfo.k.size() == 0) {
                                                bx.a(this.c, popUpInfo.f2258a, 3);
                                            } else {
                                                if (this.e) {
                                                    long c3 = dl.c();
                                                    if (c3 > 0 && System.currentTimeMillis() - c3 > 1000) {
                                                        bx.a(this.c, popUpInfo.f2258a, 1);
                                                        return;
                                                    }
                                                }
                                                ba.a().post(new cb(this, popUpInfo));
                                                return;
                                            }
                                        }
                                    }
                                } else {
                                    bx.a(this.c, popUpInfo.f2258a, 3);
                                }
                            }
                        }
                        i3 = i4 + 1;
                    } else {
                        return;
                    }
                }
            }
        }
    }
}
