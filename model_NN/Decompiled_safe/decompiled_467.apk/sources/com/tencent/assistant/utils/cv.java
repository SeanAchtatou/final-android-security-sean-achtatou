package com.tencent.assistant.utils;

import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.connect.common.Constants;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/* compiled from: ProGuard */
public class cv {

    /* renamed from: a  reason: collision with root package name */
    private static ThreadLocal<SimpleDateFormat> f2676a = new cw();
    private static ThreadLocal<SimpleDateFormat> b = new cx();
    private static ThreadLocal<SimpleDateFormat> c = new cy();
    private static ThreadLocal<SimpleDateFormat> d = new cz();
    private static ThreadLocal<SimpleDateFormat> e = new da();
    private static ThreadLocal<SimpleDateFormat> f = new db();
    private static ThreadLocal<SimpleDateFormat> g = new dc();

    public static String a() {
        return a(Long.valueOf(System.currentTimeMillis()));
    }

    public static String b() {
        try {
            return b(Long.valueOf(System.currentTimeMillis()));
        } catch (Exception e2) {
            return Constants.STR_EMPTY;
        }
    }

    public static String a(Long l) {
        if (l == null) {
            return null;
        }
        return f.get().format(l);
    }

    public static String b(Long l) {
        if (l == null) {
            return null;
        }
        return g.get().format(l);
    }

    public static String c(Long l) {
        if (l == null) {
            return null;
        }
        return d.get().format(l);
    }

    public static String d(Long l) {
        if (l == null) {
            return null;
        }
        return e.get().format(l);
    }

    public static String a(long j) {
        long j2 = j / 1000;
        int i = (int) (((float) j2) / 3600.0f);
        int i2 = (int) (((float) (j2 - ((long) ((i * 60) * 60)))) / 60.0f);
        int i3 = (int) ((j2 - ((long) ((i * 60) * 60))) - ((long) (i2 * 60)));
        if (i > 0) {
            return String.format("%02d:%02d:%02d", Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(i3));
        } else if (i2 <= 0) {
            return i3 + AstApp.i().getBaseContext().getResources().getString(R.string.second);
        } else {
            return String.format("%02d:%02d", Integer.valueOf(i2), Integer.valueOf(i3));
        }
    }

    @Deprecated
    public static boolean b(long j) {
        return f(j) == 0;
    }

    public static boolean c(long j) {
        long d2 = d();
        if (j - d2 < 0 || j - d2 >= TesDownloadConfig.TES_CONFIG_CHECK_PERIOD) {
            return false;
        }
        return true;
    }

    public static boolean d(long j) {
        if (f(j) < 7) {
            return true;
        }
        return false;
    }

    public static int c() {
        Calendar instance = Calendar.getInstance();
        instance.setFirstDayOfWeek(2);
        instance.setTimeInMillis(System.currentTimeMillis());
        return instance.get(3);
    }

    public static boolean a(int i) {
        return c() == i;
    }

    public static boolean a(long j, int i) {
        if (f(j) < i) {
            return true;
        }
        return false;
    }

    public static boolean e(long j) {
        if (f(j) > 30) {
            return true;
        }
        return false;
    }

    public static int a(long j, long j2) {
        if (j - j2 > 0) {
            return 0;
        }
        return ((int) (((float) (j2 - j)) / ((float) TesDownloadConfig.TES_CONFIG_CHECK_PERIOD))) + 1;
    }

    public static int f(long j) {
        long d2 = d();
        if (j - d2 > 0) {
            return 0;
        }
        return ((int) (((float) (d2 - j)) / ((float) TesDownloadConfig.TES_CONFIG_CHECK_PERIOD))) + 1;
    }

    public static long d() {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.set(11, 0);
        gregorianCalendar.set(12, 0);
        gregorianCalendar.set(13, 0);
        gregorianCalendar.set(14, 0);
        return gregorianCalendar.getTime().getTime();
    }

    public static long b(int i) {
        long currentTimeMillis = System.currentTimeMillis() - (TesDownloadConfig.TES_CONFIG_CHECK_PERIOD * ((long) i));
        if (currentTimeMillis < 0) {
            return 0;
        }
        return currentTimeMillis;
    }

    public static String g(long j) {
        if (j == 0) {
            return Constants.STR_EMPTY;
        }
        long currentTimeMillis = System.currentTimeMillis();
        long j2 = currentTimeMillis - j;
        if (j2 < 60000) {
            return "刚刚";
        }
        if (j2 < 3600000) {
            return (j2 / 60000) + "分钟前";
        }
        if (j2 < TesDownloadConfig.TES_CONFIG_CHECK_PERIOD) {
            return (j2 / 3600000) + "小时前";
        }
        Date date = new Date(currentTimeMillis);
        Date date2 = new Date(j);
        if (date.getYear() == date2.getYear()) {
            return f2676a.get().format(date2);
        }
        return b.get().format(date2);
    }

    public static long h(long j) {
        return (System.currentTimeMillis() - j) / TesDownloadConfig.TES_CONFIG_CHECK_PERIOD;
    }
}
