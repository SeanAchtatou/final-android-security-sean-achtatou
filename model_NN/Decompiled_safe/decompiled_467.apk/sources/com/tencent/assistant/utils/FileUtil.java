package com.tencent.assistant.utils;

import android.graphics.Bitmap;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.connect.common.Constants;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/* compiled from: ProGuard */
public class FileUtil {
    public static final String APK_DIT_PATH = "/apk";
    public static final String APK_TMP_CACHE_PATH = "/apk/tmp";
    public static final String APP_LINK_DIR_PATH = "/applink";
    public static final String APP_SDCARD_AMOUNT_ROOT_PATH = "/tencent/tassistant";
    public static final String APP_SDCARD_AMOUNT_TMP_ROOT_PATH = "/tmp";
    public static final String APP_SDCARD_UNAMOUNT_ROOT_PATH = "/tassistant";
    public static final String CACHE_DIR_PATH = "/cache";
    public static final String FILE_DIR_PATH = "/file";
    public static final String GIF_DIR_PATH = "/gif";
    public static final String LOG_DIR_PATH = "/log";
    public static final String PERMISSION_DIR_PATH = "/permission";
    public static final String PIC_DIR_PATH = "/pic";
    public static final String PLUGIN_DIR_NAME = "plugin";
    public static final String PLUGIN_DIR_PATH = "/plugin";
    public static final String SWITCH_PHONE_PATH = "/switch_phone";
    public static final String SYSTEM_IMAGE_PATH = "/DCIM";
    public static final String VIDEO_DIR_PATH = "/video";
    public static final String WCS_CONFIG_DIR_PATH = "/wcsconfig";
    public static final String WEBVIEW_CACHE_PATH = "/webview_cache";
    public static final String WIFI_DIR_PATH = "/wifi";
    public static final String WIFI_IMAGE_PATH = "/DCIM/tassistant";

    public static void initCommonDir() {
        long currentTimeMillis = System.currentTimeMillis();
        getRootDir();
        getCommonRootDir();
        getTmpRootDir();
        getLogDir();
        getAPKDir();
        getPicDir();
        getGifDir();
        getWebViewCacheDir();
        getCacheDir();
        getPluginDir();
        getWifiRootDir();
        getWifiImageDir();
        XLog.d("FileUtil", "FileUtil initCommonDir spend " + (System.currentTimeMillis() - currentTimeMillis) + "ms");
    }

    public static List<String> scanFile(String str, List<String> list) {
        File file;
        List<String> scanFile;
        if (TextUtils.isEmpty(str) || (file = new File(str)) == null || !file.exists()) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        File[] listFiles = file.listFiles();
        if (listFiles != null) {
            for (File file2 : listFiles) {
                String absolutePath = file2.getAbsolutePath();
                if (file2.isFile()) {
                    if (isSpecfiedSuffixExist(absolutePath, list)) {
                        arrayList.add(absolutePath);
                    }
                } else if (file2.isDirectory() && absolutePath.indexOf("/.") == -1 && (scanFile = scanFile(absolutePath, list)) != null && !scanFile.isEmpty()) {
                    arrayList.addAll(scanFile);
                }
            }
        }
        return arrayList;
    }

    public static boolean isSpecfiedSuffixExist(String str, List<String> list) {
        if (TextUtils.isEmpty(str) || list == null || list.isEmpty()) {
            return true;
        }
        for (String endsWith : list) {
            try {
                if (str.endsWith(endsWith)) {
                    return true;
                }
            } catch (NullPointerException e) {
                return false;
            }
        }
        return false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0047 A[SYNTHETIC, Splitter:B:20:0x0047] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x004c A[SYNTHETIC, Splitter:B:23:0x004c] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean copy(java.lang.String r5, java.lang.String r6) {
        /*
            r2 = 0
            r0 = 0
            boolean r1 = android.text.TextUtils.isEmpty(r5)
            if (r1 != 0) goto L_0x000e
            boolean r1 = android.text.TextUtils.isEmpty(r6)
            if (r1 == 0) goto L_0x000f
        L_0x000e:
            return r0
        L_0x000f:
            java.io.File r1 = new java.io.File
            r1.<init>(r5)
            boolean r3 = r1.exists()
            if (r3 == 0) goto L_0x000e
            java.io.BufferedInputStream r3 = new java.io.BufferedInputStream     // Catch:{ all -> 0x0073 }
            java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch:{ all -> 0x0073 }
            r0.<init>(r1)     // Catch:{ all -> 0x0073 }
            r3.<init>(r0)     // Catch:{ all -> 0x0073 }
            java.io.BufferedOutputStream r1 = new java.io.BufferedOutputStream     // Catch:{ all -> 0x0076 }
            java.io.FileOutputStream r0 = new java.io.FileOutputStream     // Catch:{ all -> 0x0076 }
            java.io.File r4 = new java.io.File     // Catch:{ all -> 0x0076 }
            r4.<init>(r6)     // Catch:{ all -> 0x0076 }
            r0.<init>(r4)     // Catch:{ all -> 0x0076 }
            r1.<init>(r0)     // Catch:{ all -> 0x0076 }
            r0 = 5120(0x1400, float:7.175E-42)
            byte[] r0 = new byte[r0]     // Catch:{ all -> 0x0043 }
        L_0x0037:
            int r2 = r3.read(r0)     // Catch:{ all -> 0x0043 }
            r4 = -1
            if (r2 == r4) goto L_0x0050
            r4 = 0
            r1.write(r0, r4, r2)     // Catch:{ all -> 0x0043 }
            goto L_0x0037
        L_0x0043:
            r0 = move-exception
            r2 = r3
        L_0x0045:
            if (r2 == 0) goto L_0x004a
            r2.close()     // Catch:{ IOException -> 0x0064 }
        L_0x004a:
            if (r1 == 0) goto L_0x004f
            r1.close()     // Catch:{ IOException -> 0x0069 }
        L_0x004f:
            throw r0
        L_0x0050:
            r1.flush()     // Catch:{ all -> 0x0043 }
            r0 = 1
            if (r3 == 0) goto L_0x0059
            r3.close()     // Catch:{ IOException -> 0x006e }
        L_0x0059:
            if (r1 == 0) goto L_0x000e
            r1.close()     // Catch:{ IOException -> 0x005f }
            goto L_0x000e
        L_0x005f:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x000e
        L_0x0064:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x004a
        L_0x0069:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x004f
        L_0x006e:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0059
        L_0x0073:
            r0 = move-exception
            r1 = r2
            goto L_0x0045
        L_0x0076:
            r0 = move-exception
            r1 = r2
            r2 = r3
            goto L_0x0045
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.utils.FileUtil.copy(java.lang.String, java.lang.String):boolean");
    }

    public static boolean isSDCardExistAndCanWrite() {
        try {
            return "mounted".equals(Environment.getExternalStorageState()) && Environment.getExternalStorageDirectory().canWrite();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } catch (Throwable th) {
            return false;
        }
    }

    public static boolean isSDCardExistAndCanRead() {
        try {
            return "mounted".equals(Environment.getExternalStorageState()) && Environment.getExternalStorageDirectory().canRead();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } catch (Throwable th) {
            return false;
        }
    }

    public static String getCommonRootDir() {
        String str;
        if (isSDCardExistAndCanWrite()) {
            str = Environment.getExternalStorageDirectory().getPath() + APP_SDCARD_AMOUNT_ROOT_PATH;
        } else {
            try {
                str = AstApp.i().getFilesDir().getAbsolutePath() + APP_SDCARD_UNAMOUNT_ROOT_PATH;
            } catch (Exception e) {
                return null;
            }
        }
        File file = new File(str);
        if (!file.exists()) {
            file.mkdirs();
        }
        return file.getAbsolutePath();
    }

    public static String getRootDir() {
        String absolutePath;
        if (isSDCardExistAndCanWrite()) {
            absolutePath = Environment.getExternalStorageDirectory().getPath();
        } else {
            absolutePath = AstApp.i().getFilesDir().getAbsolutePath();
        }
        File file = new File(absolutePath);
        if (!file.exists()) {
            file.mkdirs();
        }
        return file.getAbsolutePath();
    }

    public static String getTmpRootDir() {
        String internalCachePath;
        if (isSDCardExistAndCanWrite()) {
            internalCachePath = Environment.getExternalStorageDirectory().getPath() + APP_SDCARD_AMOUNT_ROOT_PATH + APP_SDCARD_AMOUNT_TMP_ROOT_PATH;
        } else {
            internalCachePath = getInternalCachePath();
        }
        File file = new File(internalCachePath);
        if (!file.exists()) {
            file.mkdirs();
        }
        return file.getAbsolutePath();
    }

    public static String getCommonPath(String str) {
        String commonRootDir = getCommonRootDir();
        if (!TextUtils.isEmpty(str)) {
            commonRootDir = commonRootDir + str;
        }
        return getPath(commonRootDir, false);
    }

    public static String getTmpPath(String str) {
        String tmpRootDir = getTmpRootDir();
        if (!TextUtils.isEmpty(str)) {
            tmpRootDir = tmpRootDir + str;
        }
        return getPath(tmpRootDir, false);
    }

    public static String getPath(String str, boolean z) {
        File file = new File(str);
        if (!file.exists() || !file.isDirectory()) {
            file.mkdirs();
            if (z) {
                try {
                    new File(str + File.separator + ".nomedia").createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return file.getAbsolutePath();
    }

    public static String getAPKDir() {
        return getCommonPath(APK_DIT_PATH);
    }

    public static String getDynamicAPKDir() {
        return b.d();
    }

    public static String getDynamicVideoDir() {
        String e = b.e();
        if (e == null) {
            return getCommonPath(VIDEO_DIR_PATH);
        }
        return e;
    }

    public static String getDynamicFileDir() {
        String f = b.f();
        if (f == null) {
            return getCommonPath(FILE_DIR_PATH);
        }
        return f;
    }

    public static String getAPKTmpCacheDir() {
        return getCommonPath(APK_TMP_CACHE_PATH);
    }

    public static String getLogDir() {
        return getCommonPath(LOG_DIR_PATH);
    }

    public static String getWcsConfigDir() {
        File file = new File(AstApp.i().getFilesDir().getAbsolutePath() + WCS_CONFIG_DIR_PATH);
        if (!file.exists()) {
            file.mkdirs();
        }
        return file.getAbsolutePath();
    }

    public static String getPicDir() {
        return getPath(getCommonRootDir() + PIC_DIR_PATH, true);
    }

    public static String getGifDir() {
        return getPath(getCommonRootDir() + GIF_DIR_PATH, true);
    }

    public static String getAppLinkDirPath() {
        return getCommonPath(APP_LINK_DIR_PATH);
    }

    public static String getSwitchPhoneDir(String str) {
        String[] split = str.split("/");
        if (split == null || split.length != 2) {
            return null;
        }
        return getPath(getPath(getCommonPath(SWITCH_PHONE_PATH) + "/" + split[0], false) + "/" + split[1], false);
    }

    public static String getSwitchPhoneDir() {
        return getCommonPath(SWITCH_PHONE_PATH);
    }

    public static String getCacheDir() {
        return getCommonPath(CACHE_DIR_PATH);
    }

    public static String getPluginDir() {
        File dir = AstApp.i().getDir(PLUGIN_DIR_NAME, 0);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        return dir.getAbsolutePath();
    }

    public static String getWebViewCacheDir() {
        return getCommonPath(WEBVIEW_CACHE_PATH);
    }

    public static String getWifiDir(String str) {
        String str2;
        if (str.equals("jpg") || str.equals("gif") || str.equals("png") || str.equals("jpeg") || str.equals("bmp")) {
            return getWifiImageDir();
        }
        if (str.equals("m4a") || str.equals("mp3") || str.equals("mid") || str.equals("xmf") || str.equals("ogg") || str.equals("wav")) {
            str2 = "audio";
        } else if (str.equals("3gp") || str.equals("mp4")) {
            str2 = "video";
        } else if (str.equals("apk")) {
            str2 = "apk";
        } else {
            str2 = "other";
        }
        return getPath(getCommonRootDir() + WIFI_DIR_PATH + File.separator + str2, false);
    }

    public static String getWifiRootDir() {
        return getPath(getCommonRootDir() + WIFI_DIR_PATH, false);
    }

    public static String getWifiImageDir() {
        return getPath(getRootDir() + WIFI_IMAGE_PATH, false);
    }

    public static String getPermissiomDir() {
        return getCommonPath(PERMISSION_DIR_PATH);
    }

    public static String getInternalCachePath() {
        String str = AstApp.i().getCacheDir() + File.separator;
        File file = new File(str);
        if (!file.exists()) {
            file.mkdirs();
        }
        return str;
    }

    public static void clearInternalCache(String str) {
        File[] listFiles;
        File file = new File(getInternalCachePath() + str);
        if (file.exists() && (listFiles = file.listFiles()) != null) {
            for (File delete : listFiles) {
                delete.delete();
            }
        }
    }

    public static String getFileName(String str) {
        String substring = str.substring(str.lastIndexOf("/") + 1);
        if (substring.contains(".")) {
            return substring.substring(0, substring.indexOf("."));
        }
        return substring;
    }

    public static String getFileExtension(String str) {
        String substring = str.substring(str.lastIndexOf("/") + 1);
        if (substring.contains(".")) {
            return substring.substring(substring.indexOf(".") + 1);
        }
        return null;
    }

    public static long getFileSize(String str) {
        if (TextUtils.isEmpty(str)) {
            return 0;
        }
        File file = new File(str);
        if (!file.exists()) {
            return 0;
        }
        return file.length();
    }

    public static long getFileLastModified(String str) {
        if (str == null) {
            return 0;
        }
        File file = new File(str);
        if (file.exists()) {
            return file.lastModified();
        }
        return 0;
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x002c A[SYNTHETIC, Splitter:B:19:0x002c] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x003b A[SYNTHETIC, Splitter:B:27:0x003b] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0044 A[SYNTHETIC, Splitter:B:32:0x0044] */
    /* JADX WARNING: Removed duplicated region for block: B:47:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:49:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean write2File(java.io.ByteArrayOutputStream r5, java.lang.String r6) {
        /*
            r1 = 1
            r0 = 0
            if (r5 != 0) goto L_0x0005
        L_0x0004:
            return r0
        L_0x0005:
            java.io.File r4 = new java.io.File
            r4.<init>(r6)
            boolean r2 = r4.exists()
            if (r2 == 0) goto L_0x0012
            r0 = r1
            goto L_0x0004
        L_0x0012:
            r3 = 0
            r4.createNewFile()     // Catch:{ FileNotFoundException -> 0x0025, IOException -> 0x0035 }
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x0025, IOException -> 0x0035 }
            r2.<init>(r4)     // Catch:{ FileNotFoundException -> 0x0025, IOException -> 0x0035 }
            r5.writeTo(r2)     // Catch:{ FileNotFoundException -> 0x0058, IOException -> 0x0055 }
            if (r2 == 0) goto L_0x0023
            r2.close()     // Catch:{ Exception -> 0x004d }
        L_0x0023:
            r0 = r1
            goto L_0x0004
        L_0x0025:
            r1 = move-exception
            r2 = r3
        L_0x0027:
            r1.printStackTrace()     // Catch:{ all -> 0x0052 }
            if (r2 == 0) goto L_0x0004
            r2.close()     // Catch:{ Exception -> 0x0030 }
            goto L_0x0004
        L_0x0030:
            r1 = move-exception
        L_0x0031:
            r1.printStackTrace()
            goto L_0x0004
        L_0x0035:
            r1 = move-exception
        L_0x0036:
            r1.printStackTrace()     // Catch:{ all -> 0x0041 }
            if (r3 == 0) goto L_0x0004
            r3.close()     // Catch:{ Exception -> 0x003f }
            goto L_0x0004
        L_0x003f:
            r1 = move-exception
            goto L_0x0031
        L_0x0041:
            r0 = move-exception
        L_0x0042:
            if (r3 == 0) goto L_0x0047
            r3.close()     // Catch:{ Exception -> 0x0048 }
        L_0x0047:
            throw r0
        L_0x0048:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0047
        L_0x004d:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0023
        L_0x0052:
            r0 = move-exception
            r3 = r2
            goto L_0x0042
        L_0x0055:
            r1 = move-exception
            r3 = r2
            goto L_0x0036
        L_0x0058:
            r1 = move-exception
            goto L_0x0027
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.utils.FileUtil.write2File(java.io.ByteArrayOutputStream, java.lang.String):boolean");
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0031 A[SYNTHETIC, Splitter:B:22:0x0031] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0040 A[SYNTHETIC, Splitter:B:31:0x0040] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean write2File(byte[] r5, java.lang.String r6) {
        /*
            r0 = 0
            if (r5 != 0) goto L_0x0004
        L_0x0003:
            return r0
        L_0x0004:
            java.io.File r4 = new java.io.File
            r4.<init>(r6)
            boolean r1 = r4.exists()
            if (r1 == 0) goto L_0x0012
            deleteFile(r6)
        L_0x0012:
            r3 = 0
            r4.createNewFile()     // Catch:{ FileNotFoundException -> 0x002a, IOException -> 0x0037 }
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x002a, IOException -> 0x0037 }
            r2.<init>(r4)     // Catch:{ FileNotFoundException -> 0x002a, IOException -> 0x0037 }
            r2.write(r5)     // Catch:{ FileNotFoundException -> 0x005d, IOException -> 0x005a }
            r0 = 1
            if (r2 == 0) goto L_0x0003
            r2.close()     // Catch:{ Exception -> 0x0025 }
            goto L_0x0003
        L_0x0025:
            r1 = move-exception
        L_0x0026:
            r1.printStackTrace()
            goto L_0x0003
        L_0x002a:
            r1 = move-exception
            r2 = r3
        L_0x002c:
            r1.printStackTrace()     // Catch:{ all -> 0x0057 }
            if (r2 == 0) goto L_0x0003
            r2.close()     // Catch:{ Exception -> 0x0035 }
            goto L_0x0003
        L_0x0035:
            r1 = move-exception
            goto L_0x0026
        L_0x0037:
            r1 = move-exception
        L_0x0038:
            r4.delete()     // Catch:{ Exception -> 0x0046 }
        L_0x003b:
            r1.printStackTrace()     // Catch:{ all -> 0x004b }
            if (r3 == 0) goto L_0x0003
            r3.close()     // Catch:{ Exception -> 0x0044 }
            goto L_0x0003
        L_0x0044:
            r1 = move-exception
            goto L_0x0026
        L_0x0046:
            r2 = move-exception
            r2.printStackTrace()     // Catch:{ all -> 0x004b }
            goto L_0x003b
        L_0x004b:
            r0 = move-exception
        L_0x004c:
            if (r3 == 0) goto L_0x0051
            r3.close()     // Catch:{ Exception -> 0x0052 }
        L_0x0051:
            throw r0
        L_0x0052:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0051
        L_0x0057:
            r0 = move-exception
            r3 = r2
            goto L_0x004c
        L_0x005a:
            r1 = move-exception
            r3 = r2
            goto L_0x0038
        L_0x005d:
            r1 = move-exception
            goto L_0x002c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.utils.FileUtil.write2File(byte[], java.lang.String):boolean");
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0035 A[SYNTHETIC, Splitter:B:20:0x0035] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x003f A[SYNTHETIC, Splitter:B:27:0x003f] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0047 A[Catch:{ IOException -> 0x0030 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean append2File(byte[] r5, java.lang.String r6) {
        /*
            r3 = 0
            if (r5 == 0) goto L_0x0009
            boolean r0 = android.text.TextUtils.isEmpty(r6)
            if (r0 == 0) goto L_0x000a
        L_0x0009:
            return r3
        L_0x000a:
            java.io.File r0 = new java.io.File
            r0.<init>(r6)
            boolean r1 = r0.exists()
            if (r1 != 0) goto L_0x0018
            r0.createNewFile()     // Catch:{ IOException -> 0x0039 }
        L_0x0018:
            r1 = 0
            java.io.RandomAccessFile r0 = new java.io.RandomAccessFile     // Catch:{ FileNotFoundException -> 0x003b, IOException -> 0x0043, all -> 0x0032 }
            java.lang.String r2 = "rw"
            r0.<init>(r6, r2)     // Catch:{ FileNotFoundException -> 0x003b, IOException -> 0x0043, all -> 0x0032 }
            long r1 = r0.length()     // Catch:{ FileNotFoundException -> 0x0054, IOException -> 0x0052, all -> 0x004d }
            r0.seek(r1)     // Catch:{ FileNotFoundException -> 0x0054, IOException -> 0x0052, all -> 0x004d }
            r0.write(r5)     // Catch:{ FileNotFoundException -> 0x0054, IOException -> 0x0052, all -> 0x004d }
            if (r0 == 0) goto L_0x0009
            r0.close()     // Catch:{ IOException -> 0x0030 }
            goto L_0x0009
        L_0x0030:
            r0 = move-exception
            goto L_0x0009
        L_0x0032:
            r0 = move-exception
        L_0x0033:
            if (r1 == 0) goto L_0x0038
            r1.close()     // Catch:{ IOException -> 0x004b }
        L_0x0038:
            throw r0
        L_0x0039:
            r0 = move-exception
            goto L_0x0009
        L_0x003b:
            r0 = move-exception
            r0 = r1
        L_0x003d:
            if (r0 == 0) goto L_0x0009
            r0.close()     // Catch:{ IOException -> 0x0030 }
            goto L_0x0009
        L_0x0043:
            r0 = move-exception
            r0 = r1
        L_0x0045:
            if (r0 == 0) goto L_0x0009
            r0.close()     // Catch:{ IOException -> 0x0030 }
            goto L_0x0009
        L_0x004b:
            r1 = move-exception
            goto L_0x0038
        L_0x004d:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x0033
        L_0x0052:
            r1 = move-exception
            goto L_0x0045
        L_0x0054:
            r1 = move-exception
            goto L_0x003d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.utils.FileUtil.append2File(byte[], java.lang.String):boolean");
    }

    public static boolean deleteFile(String str) {
        File file = new File(str);
        if (!file.exists() || !file.isFile()) {
            return false;
        }
        return file.delete();
    }

    public static void deleteFileOrDir(String str) {
        File file = new File(str);
        if (!file.exists()) {
            XLog.d("FileUtil", "<deleteFileOrDir> file " + str + " not exist.");
        } else if (!file.isDirectory()) {
            file.delete();
        } else {
            LinkedList linkedList = new LinkedList();
            linkedList.add(file);
            while (!linkedList.isEmpty()) {
                File file2 = (File) linkedList.remove(0);
                if (file2.exists()) {
                    File[] listFiles = file2.listFiles();
                    if (listFiles == null || listFiles.length == 0) {
                        file2.delete();
                    } else {
                        for (File file3 : listFiles) {
                            if (file3.isDirectory()) {
                                linkedList.add(file3);
                            } else {
                                file3.delete();
                            }
                        }
                    }
                }
            }
        }
    }

    public static boolean updateFileLastModified(String str, Long l) {
        File file = new File(str);
        if (file.exists()) {
            return file.setLastModified(l.longValue());
        }
        return false;
    }

    public static int freeSpaceOnSd() {
        StatFs statFs = new StatFs(Environment.getExternalStorageDirectory().getPath());
        return (int) ((((double) statFs.getAvailableBlocks()) * ((double) statFs.getBlockSize())) / 1048576.0d);
    }

    public static boolean readFile(String str, ByteArrayOutputStream byteArrayOutputStream) {
        return readFile(str, byteArrayOutputStream, null);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x002b, code lost:
        if (r7 != null) goto L_0x002d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x002d, code lost:
        r7.a(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x004d, code lost:
        if (r7 != null) goto L_0x002d;
     */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0036 A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0048 A[SYNTHETIC, Splitter:B:30:0x0048] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x004d  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0054 A[SYNTHETIC, Splitter:B:37:0x0054] */
    /* JADX WARNING: Removed duplicated region for block: B:56:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean readFile(java.lang.String r5, java.io.ByteArrayOutputStream r6, com.tencent.assistant.utils.n r7) {
        /*
            r2 = 0
            r0 = 0
            java.io.File r1 = new java.io.File
            r1.<init>(r5)
            if (r6 == 0) goto L_0x000f
            boolean r3 = r1.exists()
            if (r3 != 0) goto L_0x0010
        L_0x000f:
            return r0
        L_0x0010:
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0070, all -> 0x0050 }
            r3.<init>(r1)     // Catch:{ Exception -> 0x0070, all -> 0x0050 }
            if (r7 == 0) goto L_0x0038
            r1 = 1024(0x400, float:1.435E-42)
            byte[] r2 = r7.a(r1)     // Catch:{ Exception -> 0x0042 }
        L_0x001d:
            int r1 = r3.read(r2)     // Catch:{ Exception -> 0x0042 }
            r4 = -1
            if (r4 != r1) goto L_0x003d
            if (r3 == 0) goto L_0x0029
            r3.close()     // Catch:{ IOException -> 0x0069 }
        L_0x0029:
            if (r2 == 0) goto L_0x0030
            if (r7 == 0) goto L_0x0030
        L_0x002d:
            r7.a(r2)
        L_0x0030:
            int r1 = r6.size()
            if (r1 <= 0) goto L_0x000f
            r0 = 1
            goto L_0x000f
        L_0x0038:
            r1 = 1024(0x400, float:1.435E-42)
            byte[] r2 = new byte[r1]     // Catch:{ Exception -> 0x0042 }
            goto L_0x001d
        L_0x003d:
            r4 = 0
            r6.write(r2, r4, r1)     // Catch:{ Exception -> 0x0042 }
            goto L_0x001d
        L_0x0042:
            r1 = move-exception
        L_0x0043:
            r1.printStackTrace()     // Catch:{ all -> 0x006e }
            if (r3 == 0) goto L_0x004b
            r3.close()     // Catch:{ IOException -> 0x0064 }
        L_0x004b:
            if (r2 == 0) goto L_0x0030
            if (r7 == 0) goto L_0x0030
            goto L_0x002d
        L_0x0050:
            r0 = move-exception
            r3 = r2
        L_0x0052:
            if (r3 == 0) goto L_0x0057
            r3.close()     // Catch:{ IOException -> 0x005f }
        L_0x0057:
            if (r2 == 0) goto L_0x005e
            if (r7 == 0) goto L_0x005e
            r7.a(r2)
        L_0x005e:
            throw r0
        L_0x005f:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0057
        L_0x0064:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x004b
        L_0x0069:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0029
        L_0x006e:
            r0 = move-exception
            goto L_0x0052
        L_0x0070:
            r1 = move-exception
            r3 = r2
            goto L_0x0043
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.utils.FileUtil.readFile(java.lang.String, java.io.ByteArrayOutputStream, com.tencent.assistant.utils.n):boolean");
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0027 A[SYNTHETIC, Splitter:B:17:0x0027] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0032 A[SYNTHETIC, Splitter:B:24:0x0032] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x003e A[SYNTHETIC, Splitter:B:30:0x003e] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x004c A[SYNTHETIC, Splitter:B:38:0x004c] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean createFileWithSpecialSize(java.lang.String r3, long r4) {
        /*
            java.io.File r0 = new java.io.File
            r0.<init>(r3)
            boolean r1 = r0.exists()
            if (r1 == 0) goto L_0x000e
            r0.delete()
        L_0x000e:
            r2 = 0
            java.io.RandomAccessFile r1 = new java.io.RandomAccessFile     // Catch:{ IOException -> 0x0020, Exception -> 0x002c, OutOfMemoryError -> 0x0049 }
            java.lang.String r0 = "rw"
            r1.<init>(r3, r0)     // Catch:{ IOException -> 0x0020, Exception -> 0x002c, OutOfMemoryError -> 0x0049 }
            r1.setLength(r4)     // Catch:{ IOException -> 0x0060, Exception -> 0x005d, OutOfMemoryError -> 0x005a }
            r0 = 1
            if (r1 == 0) goto L_0x001f
            r1.close()     // Catch:{ IOException -> 0x0052 }
        L_0x001f:
            return r0
        L_0x0020:
            r0 = move-exception
            r1 = r2
        L_0x0022:
            r0.printStackTrace()     // Catch:{ all -> 0x0057 }
            if (r1 == 0) goto L_0x002a
            r1.close()     // Catch:{ IOException -> 0x0047 }
        L_0x002a:
            r0 = 0
            goto L_0x001f
        L_0x002c:
            r0 = move-exception
        L_0x002d:
            r0.printStackTrace()     // Catch:{ all -> 0x003b }
            if (r2 == 0) goto L_0x002a
            r2.close()     // Catch:{ IOException -> 0x0036 }
            goto L_0x002a
        L_0x0036:
            r0 = move-exception
        L_0x0037:
            r0.printStackTrace()
            goto L_0x002a
        L_0x003b:
            r0 = move-exception
        L_0x003c:
            if (r2 == 0) goto L_0x0041
            r2.close()     // Catch:{ IOException -> 0x0042 }
        L_0x0041:
            throw r0
        L_0x0042:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0041
        L_0x0047:
            r0 = move-exception
            goto L_0x0037
        L_0x0049:
            r0 = move-exception
        L_0x004a:
            if (r2 == 0) goto L_0x002a
            r2.close()     // Catch:{ IOException -> 0x0050 }
            goto L_0x002a
        L_0x0050:
            r0 = move-exception
            goto L_0x0037
        L_0x0052:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x001f
        L_0x0057:
            r0 = move-exception
            r2 = r1
            goto L_0x003c
        L_0x005a:
            r0 = move-exception
            r2 = r1
            goto L_0x004a
        L_0x005d:
            r0 = move-exception
            r2 = r1
            goto L_0x002d
        L_0x0060:
            r0 = move-exception
            goto L_0x0022
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.utils.FileUtil.createFileWithSpecialSize(java.lang.String, long):boolean");
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0024 A[SYNTHETIC, Splitter:B:13:0x0024] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x002c A[SYNTHETIC, Splitter:B:19:0x002c] */
    /* JADX WARNING: Removed duplicated region for block: B:31:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean write(java.lang.String r4, java.lang.String r5) {
        /*
            java.io.File r2 = new java.io.File
            r2.<init>(r4)
            boolean r0 = r2.exists()
            if (r0 != 0) goto L_0x000e
            r2.delete()
        L_0x000e:
            r1 = 0
            r2.createNewFile()     // Catch:{ IOException -> 0x0028, all -> 0x0021 }
            java.io.FileWriter r0 = new java.io.FileWriter     // Catch:{ IOException -> 0x0028, all -> 0x0021 }
            r0.<init>(r2)     // Catch:{ IOException -> 0x0028, all -> 0x0021 }
            r0.write(r5)     // Catch:{ IOException -> 0x0041, all -> 0x003c }
            if (r0 == 0) goto L_0x001f
            r0.close()     // Catch:{ IOException -> 0x003a }
        L_0x001f:
            r0 = 0
            return r0
        L_0x0021:
            r0 = move-exception
        L_0x0022:
            if (r1 == 0) goto L_0x0027
            r1.close()     // Catch:{ IOException -> 0x0035 }
        L_0x0027:
            throw r0
        L_0x0028:
            r0 = move-exception
            r0 = r1
        L_0x002a:
            if (r0 == 0) goto L_0x001f
            r0.close()     // Catch:{ IOException -> 0x0030 }
            goto L_0x001f
        L_0x0030:
            r0 = move-exception
        L_0x0031:
            r0.printStackTrace()
            goto L_0x001f
        L_0x0035:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0027
        L_0x003a:
            r0 = move-exception
            goto L_0x0031
        L_0x003c:
            r1 = move-exception
            r3 = r1
            r1 = r0
            r0 = r3
            goto L_0x0022
        L_0x0041:
            r1 = move-exception
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.utils.FileUtil.write(java.lang.String, java.lang.String):boolean");
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0034 A[SYNTHETIC, Splitter:B:20:0x0034] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0039 A[SYNTHETIC, Splitter:B:23:0x0039] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0041 A[SYNTHETIC, Splitter:B:29:0x0041] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0046 A[SYNTHETIC, Splitter:B:32:0x0046] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String read(java.lang.String r4) {
        /*
            r0 = 0
            java.io.File r1 = new java.io.File
            r1.<init>(r4)
            boolean r2 = r1.exists()
            if (r2 != 0) goto L_0x000f
            java.lang.String r0 = ""
        L_0x000e:
            return r0
        L_0x000f:
            java.io.FileReader r2 = new java.io.FileReader     // Catch:{ IOException -> 0x003d, all -> 0x002d }
            r2.<init>(r1)     // Catch:{ IOException -> 0x003d, all -> 0x002d }
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ IOException -> 0x006c, all -> 0x0065 }
            r1.<init>(r2)     // Catch:{ IOException -> 0x006c, all -> 0x0065 }
            java.lang.String r0 = r1.readLine()     // Catch:{ IOException -> 0x006f, all -> 0x006a }
            if (r1 == 0) goto L_0x0022
            r1.close()     // Catch:{ IOException -> 0x0060 }
        L_0x0022:
            if (r2 == 0) goto L_0x000e
            r2.close()     // Catch:{ IOException -> 0x0028 }
            goto L_0x000e
        L_0x0028:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x000e
        L_0x002d:
            r1 = move-exception
            r2 = r0
            r3 = r0
            r0 = r1
            r1 = r3
        L_0x0032:
            if (r1 == 0) goto L_0x0037
            r1.close()     // Catch:{ IOException -> 0x0056 }
        L_0x0037:
            if (r2 == 0) goto L_0x003c
            r2.close()     // Catch:{ IOException -> 0x005b }
        L_0x003c:
            throw r0
        L_0x003d:
            r1 = move-exception
            r1 = r0
        L_0x003f:
            if (r0 == 0) goto L_0x0044
            r0.close()     // Catch:{ IOException -> 0x004c }
        L_0x0044:
            if (r1 == 0) goto L_0x0049
            r1.close()     // Catch:{ IOException -> 0x0051 }
        L_0x0049:
            java.lang.String r0 = ""
            goto L_0x000e
        L_0x004c:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0044
        L_0x0051:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0049
        L_0x0056:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0037
        L_0x005b:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x003c
        L_0x0060:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0022
        L_0x0065:
            r1 = move-exception
            r3 = r1
            r1 = r0
            r0 = r3
            goto L_0x0032
        L_0x006a:
            r0 = move-exception
            goto L_0x0032
        L_0x006c:
            r1 = move-exception
            r1 = r2
            goto L_0x003f
        L_0x006f:
            r0 = move-exception
            r0 = r1
            r1 = r2
            goto L_0x003f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.utils.FileUtil.read(java.lang.String):java.lang.String");
    }

    public static String getUnusedFilePath(String str) {
        int i = 0;
        while (new File(str).exists()) {
            if (str.contains(".")) {
                int lastIndexOf = str.lastIndexOf(".");
                str = str.substring(0, lastIndexOf) + i + str.substring(lastIndexOf);
            } else {
                str = str + i;
            }
            i++;
        }
        return str;
    }

    public static boolean writeToFile(File file, byte[] bArr) {
        boolean z = false;
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            try {
                fileOutputStream.write(bArr);
                z = true;
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                fileOutputStream.close();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        } catch (FileNotFoundException e3) {
            e3.printStackTrace();
        }
        return z;
    }

    public static byte[] compressBitmap(Bitmap bitmap, Bitmap.CompressFormat compressFormat, int i) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(compressFormat, i, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        try {
            byteArrayOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return byteArray;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r2v0 */
    /* JADX WARN: Type inference failed for: r2v1, types: [java.io.ObjectOutputStream] */
    /* JADX WARN: Type inference failed for: r2v2, types: [java.io.ObjectOutputStream] */
    /* JADX WARN: Type inference failed for: r2v3, types: [java.io.FileOutputStream] */
    /* JADX WARN: Type inference failed for: r2v4 */
    /* JADX WARN: Type inference failed for: r2v5 */
    /* JADX WARN: Type inference failed for: r2v6 */
    /* JADX WARN: Type inference failed for: r2v7 */
    /* JADX WARN: Type inference failed for: r2v8 */
    /* JADX WARN: Type inference failed for: r2v9 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0034 A[SYNTHETIC, Splitter:B:21:0x0034] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0039 A[Catch:{ IOException -> 0x003d }] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0046 A[SYNTHETIC, Splitter:B:31:0x0046] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x004b A[SYNTHETIC, Splitter:B:34:0x004b] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0055 A[SYNTHETIC, Splitter:B:40:0x0055] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x005a A[Catch:{ IOException -> 0x005e }] */
    /* JADX WARNING: Removed duplicated region for block: B:64:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:65:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void writeObj2File(java.lang.Object r4, java.lang.String r5) {
        /*
            r2 = 0
            boolean r0 = android.text.TextUtils.isEmpty(r5)
            if (r0 == 0) goto L_0x0008
        L_0x0007:
            return
        L_0x0008:
            java.io.File r0 = new java.io.File     // Catch:{ FileNotFoundException -> 0x002d, IOException -> 0x003f, all -> 0x0051 }
            r0.<init>(r5)     // Catch:{ FileNotFoundException -> 0x002d, IOException -> 0x003f, all -> 0x0051 }
            java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x002d, IOException -> 0x003f, all -> 0x0051 }
            r3.<init>(r0)     // Catch:{ FileNotFoundException -> 0x002d, IOException -> 0x003f, all -> 0x0051 }
            java.io.ObjectOutputStream r1 = new java.io.ObjectOutputStream     // Catch:{ FileNotFoundException -> 0x0075, IOException -> 0x0070 }
            r1.<init>(r3)     // Catch:{ FileNotFoundException -> 0x0075, IOException -> 0x0070 }
            r1.writeObject(r4)     // Catch:{ FileNotFoundException -> 0x0079, IOException -> 0x0072, all -> 0x0069 }
            r1.flush()     // Catch:{ FileNotFoundException -> 0x0079, IOException -> 0x0072, all -> 0x0069 }
            if (r3 == 0) goto L_0x0022
            r3.close()     // Catch:{ IOException -> 0x0063 }
        L_0x0022:
            if (r1 == 0) goto L_0x0007
            r1.close()     // Catch:{ IOException -> 0x0028 }
            goto L_0x0007
        L_0x0028:
            r0 = move-exception
        L_0x0029:
            r0.printStackTrace()
            goto L_0x0007
        L_0x002d:
            r0 = move-exception
            r1 = r2
        L_0x002f:
            r0.printStackTrace()     // Catch:{ all -> 0x006c }
            if (r2 == 0) goto L_0x0037
            r2.close()     // Catch:{ IOException -> 0x003d }
        L_0x0037:
            if (r1 == 0) goto L_0x0007
            r1.close()     // Catch:{ IOException -> 0x003d }
            goto L_0x0007
        L_0x003d:
            r0 = move-exception
            goto L_0x0029
        L_0x003f:
            r0 = move-exception
            r3 = r2
        L_0x0041:
            r0.printStackTrace()     // Catch:{ all -> 0x0067 }
            if (r3 == 0) goto L_0x0049
            r3.close()     // Catch:{ IOException -> 0x0065 }
        L_0x0049:
            if (r2 == 0) goto L_0x0007
            r2.close()     // Catch:{ IOException -> 0x004f }
            goto L_0x0007
        L_0x004f:
            r0 = move-exception
            goto L_0x0029
        L_0x0051:
            r0 = move-exception
            r3 = r2
        L_0x0053:
            if (r3 == 0) goto L_0x0058
            r3.close()     // Catch:{ IOException -> 0x005e }
        L_0x0058:
            if (r2 == 0) goto L_0x005d
            r2.close()     // Catch:{ IOException -> 0x005e }
        L_0x005d:
            throw r0
        L_0x005e:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x005d
        L_0x0063:
            r0 = move-exception
            goto L_0x0029
        L_0x0065:
            r0 = move-exception
            goto L_0x0029
        L_0x0067:
            r0 = move-exception
            goto L_0x0053
        L_0x0069:
            r0 = move-exception
            r2 = r1
            goto L_0x0053
        L_0x006c:
            r0 = move-exception
            r3 = r2
            r2 = r1
            goto L_0x0053
        L_0x0070:
            r0 = move-exception
            goto L_0x0041
        L_0x0072:
            r0 = move-exception
            r2 = r1
            goto L_0x0041
        L_0x0075:
            r0 = move-exception
            r1 = r2
            r2 = r3
            goto L_0x002f
        L_0x0079:
            r0 = move-exception
            r2 = r3
            goto L_0x002f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.utils.FileUtil.writeObj2File(java.lang.Object, java.lang.String):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x0039 A[SYNTHETIC, Splitter:B:24:0x0039] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x003e A[Catch:{ IOException -> 0x0042 }] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x004c A[SYNTHETIC, Splitter:B:34:0x004c] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0051 A[SYNTHETIC, Splitter:B:37:0x0051] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x005f A[SYNTHETIC, Splitter:B:45:0x005f] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0064 A[SYNTHETIC, Splitter:B:48:0x0064] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x0072 A[SYNTHETIC, Splitter:B:56:0x0072] */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x0077 A[SYNTHETIC, Splitter:B:59:0x0077] */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x0085 A[SYNTHETIC, Splitter:B:67:0x0085] */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x008a A[SYNTHETIC, Splitter:B:70:0x008a] */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x0097 A[SYNTHETIC, Splitter:B:76:0x0097] */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x009c A[Catch:{ IOException -> 0x00a0 }] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:64:0x0080=Splitter:B:64:0x0080, B:21:0x0034=Splitter:B:21:0x0034, B:31:0x0047=Splitter:B:31:0x0047, B:42:0x005a=Splitter:B:42:0x005a, B:53:0x006d=Splitter:B:53:0x006d} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.Object readObjFromFile(java.lang.String r4) {
        /*
            r0 = 0
            boolean r1 = android.text.TextUtils.isEmpty(r4)
            if (r1 == 0) goto L_0x0008
        L_0x0007:
            return r0
        L_0x0008:
            java.io.File r1 = new java.io.File
            r1.<init>(r4)
            boolean r2 = r1.exists()
            if (r2 == 0) goto L_0x0007
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ StreamCorruptedException -> 0x0031, OptionalDataException -> 0x0044, FileNotFoundException -> 0x0057, IOException -> 0x006a, ClassNotFoundException -> 0x007d, all -> 0x0091 }
            r3.<init>(r1)     // Catch:{ StreamCorruptedException -> 0x0031, OptionalDataException -> 0x0044, FileNotFoundException -> 0x0057, IOException -> 0x006a, ClassNotFoundException -> 0x007d, all -> 0x0091 }
            java.io.ObjectInputStream r2 = new java.io.ObjectInputStream     // Catch:{ StreamCorruptedException -> 0x00cb, OptionalDataException -> 0x00c5, FileNotFoundException -> 0x00c0, IOException -> 0x00bb, ClassNotFoundException -> 0x00b6, all -> 0x00b0 }
            r2.<init>(r3)     // Catch:{ StreamCorruptedException -> 0x00cb, OptionalDataException -> 0x00c5, FileNotFoundException -> 0x00c0, IOException -> 0x00bb, ClassNotFoundException -> 0x00b6, all -> 0x00b0 }
            java.lang.Object r0 = r2.readObject()     // Catch:{ StreamCorruptedException -> 0x00cf, OptionalDataException -> 0x00c8, FileNotFoundException -> 0x00c3, IOException -> 0x00be, ClassNotFoundException -> 0x00b9 }
            if (r3 == 0) goto L_0x0026
            r3.close()     // Catch:{ IOException -> 0x00a5 }
        L_0x0026:
            if (r2 == 0) goto L_0x0007
            r2.close()     // Catch:{ IOException -> 0x002c }
            goto L_0x0007
        L_0x002c:
            r1 = move-exception
        L_0x002d:
            r1.printStackTrace()
            goto L_0x0007
        L_0x0031:
            r1 = move-exception
            r2 = r0
            r3 = r0
        L_0x0034:
            r1.printStackTrace()     // Catch:{ all -> 0x00b4 }
            if (r3 == 0) goto L_0x003c
            r3.close()     // Catch:{ IOException -> 0x0042 }
        L_0x003c:
            if (r2 == 0) goto L_0x0007
            r2.close()     // Catch:{ IOException -> 0x0042 }
            goto L_0x0007
        L_0x0042:
            r1 = move-exception
            goto L_0x002d
        L_0x0044:
            r1 = move-exception
            r2 = r0
            r3 = r0
        L_0x0047:
            r1.printStackTrace()     // Catch:{ all -> 0x00b4 }
            if (r3 == 0) goto L_0x004f
            r3.close()     // Catch:{ IOException -> 0x00ad }
        L_0x004f:
            if (r2 == 0) goto L_0x0007
            r2.close()     // Catch:{ IOException -> 0x0055 }
            goto L_0x0007
        L_0x0055:
            r1 = move-exception
            goto L_0x002d
        L_0x0057:
            r1 = move-exception
            r2 = r0
            r3 = r0
        L_0x005a:
            r1.printStackTrace()     // Catch:{ all -> 0x00b4 }
            if (r3 == 0) goto L_0x0062
            r3.close()     // Catch:{ IOException -> 0x00ab }
        L_0x0062:
            if (r2 == 0) goto L_0x0007
            r2.close()     // Catch:{ IOException -> 0x0068 }
            goto L_0x0007
        L_0x0068:
            r1 = move-exception
            goto L_0x002d
        L_0x006a:
            r1 = move-exception
            r2 = r0
            r3 = r0
        L_0x006d:
            r1.printStackTrace()     // Catch:{ all -> 0x00b4 }
            if (r3 == 0) goto L_0x0075
            r3.close()     // Catch:{ IOException -> 0x00a9 }
        L_0x0075:
            if (r2 == 0) goto L_0x0007
            r2.close()     // Catch:{ IOException -> 0x007b }
            goto L_0x0007
        L_0x007b:
            r1 = move-exception
            goto L_0x002d
        L_0x007d:
            r1 = move-exception
            r2 = r0
            r3 = r0
        L_0x0080:
            r1.printStackTrace()     // Catch:{ all -> 0x00b4 }
            if (r3 == 0) goto L_0x0088
            r3.close()     // Catch:{ IOException -> 0x00a7 }
        L_0x0088:
            if (r2 == 0) goto L_0x0007
            r2.close()     // Catch:{ IOException -> 0x008f }
            goto L_0x0007
        L_0x008f:
            r1 = move-exception
            goto L_0x002d
        L_0x0091:
            r1 = move-exception
            r2 = r0
            r3 = r0
            r0 = r1
        L_0x0095:
            if (r3 == 0) goto L_0x009a
            r3.close()     // Catch:{ IOException -> 0x00a0 }
        L_0x009a:
            if (r2 == 0) goto L_0x009f
            r2.close()     // Catch:{ IOException -> 0x00a0 }
        L_0x009f:
            throw r0
        L_0x00a0:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x009f
        L_0x00a5:
            r1 = move-exception
            goto L_0x002d
        L_0x00a7:
            r1 = move-exception
            goto L_0x002d
        L_0x00a9:
            r1 = move-exception
            goto L_0x002d
        L_0x00ab:
            r1 = move-exception
            goto L_0x002d
        L_0x00ad:
            r1 = move-exception
            goto L_0x002d
        L_0x00b0:
            r1 = move-exception
            r2 = r0
            r0 = r1
            goto L_0x0095
        L_0x00b4:
            r0 = move-exception
            goto L_0x0095
        L_0x00b6:
            r1 = move-exception
            r2 = r0
            goto L_0x0080
        L_0x00b9:
            r1 = move-exception
            goto L_0x0080
        L_0x00bb:
            r1 = move-exception
            r2 = r0
            goto L_0x006d
        L_0x00be:
            r1 = move-exception
            goto L_0x006d
        L_0x00c0:
            r1 = move-exception
            r2 = r0
            goto L_0x005a
        L_0x00c3:
            r1 = move-exception
            goto L_0x005a
        L_0x00c5:
            r1 = move-exception
            r2 = r0
            goto L_0x0047
        L_0x00c8:
            r1 = move-exception
            goto L_0x0047
        L_0x00cb:
            r1 = move-exception
            r2 = r0
            goto L_0x0034
        L_0x00cf:
            r1 = move-exception
            goto L_0x0034
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.utils.FileUtil.readObjFromFile(java.lang.String):java.lang.Object");
    }

    public static String remoteFileIsExit(String str, String str2, long j) {
        int lastIndexOf;
        if (str == null || (lastIndexOf = str.lastIndexOf("/")) < 0) {
            return null;
        }
        String str3 = getWifiDir(str2) + str.substring(lastIndexOf);
        File file = new File(str3);
        if (file == null || !file.exists() || file.length() != j) {
            return null;
        }
        return str3;
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0055 A[SYNTHETIC, Splitter:B:16:0x0055] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x005c A[SYNTHETIC, Splitter:B:21:0x005c] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0069 A[SYNTHETIC, Splitter:B:29:0x0069] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0073 A[SYNTHETIC, Splitter:B:35:0x0073] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.Map<java.lang.String, java.lang.String> readConfigFile(java.lang.String r8) {
        /*
            r6 = 1
            java.util.HashMap r2 = new java.util.HashMap
            r2.<init>()
            r1 = 0
            java.io.BufferedReader r0 = new java.io.BufferedReader     // Catch:{ FileNotFoundException -> 0x008f, IOException -> 0x0065, Exception -> 0x006f, all -> 0x0059 }
            java.io.InputStreamReader r3 = new java.io.InputStreamReader     // Catch:{ FileNotFoundException -> 0x008f, IOException -> 0x0065, Exception -> 0x006f, all -> 0x0059 }
            java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x008f, IOException -> 0x0065, Exception -> 0x006f, all -> 0x0059 }
            r4.<init>(r8)     // Catch:{ FileNotFoundException -> 0x008f, IOException -> 0x0065, Exception -> 0x006f, all -> 0x0059 }
            java.lang.String r5 = "gb2312"
            r3.<init>(r4, r5)     // Catch:{ FileNotFoundException -> 0x008f, IOException -> 0x0065, Exception -> 0x006f, all -> 0x0059 }
            r0.<init>(r3)     // Catch:{ FileNotFoundException -> 0x008f, IOException -> 0x0065, Exception -> 0x006f, all -> 0x0059 }
        L_0x0018:
            java.lang.String r1 = r0.readLine()     // Catch:{ FileNotFoundException -> 0x0052, IOException -> 0x008d, Exception -> 0x008b, all -> 0x0086 }
            if (r1 == 0) goto L_0x007e
            if (r1 == 0) goto L_0x0018
            int r3 = r1.length()     // Catch:{ FileNotFoundException -> 0x0052, IOException -> 0x008d, Exception -> 0x008b, all -> 0x0086 }
            if (r3 == 0) goto L_0x0018
            java.lang.String r3 = "#"
            boolean r3 = r1.startsWith(r3)     // Catch:{ FileNotFoundException -> 0x0052, IOException -> 0x008d, Exception -> 0x008b, all -> 0x0086 }
            if (r3 != 0) goto L_0x0018
            java.lang.String r3 = "#"
            java.lang.String[] r1 = r1.split(r3)     // Catch:{ FileNotFoundException -> 0x0052, IOException -> 0x008d, Exception -> 0x008b, all -> 0x0086 }
            int r3 = r1.length     // Catch:{ FileNotFoundException -> 0x0052, IOException -> 0x008d, Exception -> 0x008b, all -> 0x0086 }
            if (r3 < r6) goto L_0x0018
            r3 = 0
            r1 = r1[r3]     // Catch:{ FileNotFoundException -> 0x0052, IOException -> 0x008d, Exception -> 0x008b, all -> 0x0086 }
            java.lang.String r3 = "="
            java.lang.String[] r1 = r1.split(r3)     // Catch:{ FileNotFoundException -> 0x0052, IOException -> 0x008d, Exception -> 0x008b, all -> 0x0086 }
            r3 = 0
            r3 = r1[r3]     // Catch:{ FileNotFoundException -> 0x0052, IOException -> 0x008d, Exception -> 0x008b, all -> 0x0086 }
            java.lang.String r3 = r3.trim()     // Catch:{ FileNotFoundException -> 0x0052, IOException -> 0x008d, Exception -> 0x008b, all -> 0x0086 }
            r4 = 1
            r1 = r1[r4]     // Catch:{ FileNotFoundException -> 0x0052, IOException -> 0x008d, Exception -> 0x008b, all -> 0x0086 }
            java.lang.String r1 = r1.trim()     // Catch:{ FileNotFoundException -> 0x0052, IOException -> 0x008d, Exception -> 0x008b, all -> 0x0086 }
            r2.put(r3, r1)     // Catch:{ FileNotFoundException -> 0x0052, IOException -> 0x008d, Exception -> 0x008b, all -> 0x0086 }
            goto L_0x0018
        L_0x0052:
            r1 = move-exception
        L_0x0053:
            if (r0 == 0) goto L_0x0058
            r0.close()     // Catch:{ IOException -> 0x0060 }
        L_0x0058:
            return r2
        L_0x0059:
            r0 = move-exception
        L_0x005a:
            if (r1 == 0) goto L_0x005f
            r1.close()     // Catch:{ IOException -> 0x0079 }
        L_0x005f:
            throw r0
        L_0x0060:
            r0 = move-exception
        L_0x0061:
            r0.printStackTrace()
            goto L_0x0058
        L_0x0065:
            r0 = move-exception
            r0 = r1
        L_0x0067:
            if (r0 == 0) goto L_0x0058
            r0.close()     // Catch:{ IOException -> 0x006d }
            goto L_0x0058
        L_0x006d:
            r0 = move-exception
            goto L_0x0061
        L_0x006f:
            r0 = move-exception
            r0 = r1
        L_0x0071:
            if (r0 == 0) goto L_0x0058
            r0.close()     // Catch:{ IOException -> 0x0077 }
            goto L_0x0058
        L_0x0077:
            r0 = move-exception
            goto L_0x0061
        L_0x0079:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x005f
        L_0x007e:
            if (r0 == 0) goto L_0x0058
            r0.close()     // Catch:{ IOException -> 0x0084 }
            goto L_0x0058
        L_0x0084:
            r0 = move-exception
            goto L_0x0061
        L_0x0086:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
            goto L_0x005a
        L_0x008b:
            r1 = move-exception
            goto L_0x0071
        L_0x008d:
            r1 = move-exception
            goto L_0x0067
        L_0x008f:
            r0 = move-exception
            r0 = r1
            goto L_0x0053
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.utils.FileUtil.readConfigFile(java.lang.String):java.util.Map");
    }

    public static boolean writeToAppData(String str, String str2, int i) {
        if (TextUtils.isEmpty(str) || TextUtils.isEmpty(str2)) {
            return false;
        }
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = AstApp.i().openFileOutput(str, i);
            fileOutputStream.write(str2.getBytes());
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (IOException e) {
                    e = e;
                    e.printStackTrace();
                    return false;
                }
            }
            return true;
        } catch (FileNotFoundException e2) {
            e2.printStackTrace();
            if (fileOutputStream == null) {
                return false;
            }
            try {
                fileOutputStream.close();
                return false;
            } catch (IOException e3) {
                e = e3;
            }
        } catch (IOException e4) {
            e4.printStackTrace();
            if (fileOutputStream == null) {
                return false;
            }
            try {
                fileOutputStream.close();
                return false;
            } catch (IOException e5) {
                e = e5;
            }
        } catch (Throwable th) {
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (IOException e6) {
                    e = e6;
                    e.printStackTrace();
                    return false;
                }
            }
            throw th;
        }
    }

    public static String readFromAppData(String str) {
        BufferedReader bufferedReader;
        Throwable th;
        BufferedReader bufferedReader2 = null;
        try {
            bufferedReader = new BufferedReader(new InputStreamReader(AstApp.i().openFileInput(str)));
            try {
                String readLine = bufferedReader.readLine();
                p.a(bufferedReader);
                return readLine;
            } catch (Exception e) {
                bufferedReader2 = bufferedReader;
                p.a(bufferedReader2);
                return Constants.STR_EMPTY;
            } catch (Throwable th2) {
                th = th2;
                p.a(bufferedReader);
                throw th;
            }
        } catch (Exception e2) {
            p.a(bufferedReader2);
            return Constants.STR_EMPTY;
        } catch (Throwable th3) {
            Throwable th4 = th3;
            bufferedReader = null;
            th = th4;
            p.a(bufferedReader);
            throw th;
        }
    }

    public static String getFilesDir() {
        return AstApp.i().getFilesDir().getAbsolutePath();
    }

    public static boolean isFileExists(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        return new File(str).exists();
    }

    public static void tryRefreshPath(int i) {
        if (i == -12 || i == -13 || i == -14) {
            b.a();
        }
    }

    /* JADX WARN: Type inference failed for: r2v0 */
    /* JADX WARN: Type inference failed for: r2v1, types: [java.util.jar.JarFile] */
    /* JADX WARN: Type inference failed for: r2v2 */
    /* JADX WARN: Type inference failed for: r2v3, types: [java.io.FileOutputStream] */
    /* JADX WARN: Type inference failed for: r2v4 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0072 A[SYNTHETIC, Splitter:B:24:0x0072] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0077 A[SYNTHETIC, Splitter:B:27:0x0077] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x008f A[SYNTHETIC, Splitter:B:42:0x008f] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0094 A[SYNTHETIC, Splitter:B:45:0x0094] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x009b A[SYNTHETIC, Splitter:B:51:0x009b] */
    /* JADX WARNING: Removed duplicated region for block: B:97:0x000e A[SYNTHETIC] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void extractSoToFilePath(java.lang.String r9, java.lang.String r10) {
        /*
            r2 = 0
            java.util.jar.JarFile r4 = new java.util.jar.JarFile     // Catch:{ Throwable -> 0x00dd, all -> 0x00d7 }
            r4.<init>(r9)     // Catch:{ Throwable -> 0x00dd, all -> 0x00d7 }
            java.util.Enumeration r5 = r4.entries()     // Catch:{ Throwable -> 0x0080, all -> 0x0098 }
            r0 = 1024(0x400, float:1.435E-42)
            byte[] r6 = new byte[r0]     // Catch:{ Throwable -> 0x0080, all -> 0x0098 }
        L_0x000e:
            boolean r0 = r5.hasMoreElements()     // Catch:{ Throwable -> 0x0080, all -> 0x0098 }
            if (r0 == 0) goto L_0x00cf
            java.lang.Object r0 = r5.nextElement()     // Catch:{ Throwable -> 0x0080, all -> 0x0098 }
            java.util.jar.JarEntry r0 = (java.util.jar.JarEntry) r0     // Catch:{ Throwable -> 0x0080, all -> 0x0098 }
            java.lang.String r1 = r0.getName()     // Catch:{ Throwable -> 0x0080, all -> 0x0098 }
            r3 = 47
            int r3 = r1.lastIndexOf(r3)     // Catch:{ Throwable -> 0x0080, all -> 0x0098 }
            int r3 = r3 + 1
            java.lang.String r7 = r1.substring(r3)     // Catch:{ Throwable -> 0x0080, all -> 0x0098 }
            boolean r3 = matchCpuABI(r1)     // Catch:{ Throwable -> 0x0080, all -> 0x0098 }
            java.lang.String r8 = ".so"
            boolean r8 = r7.endsWith(r8)     // Catch:{ Throwable -> 0x0080, all -> 0x0098 }
            if (r8 == 0) goto L_0x000e
            java.lang.String r8 = "lib/"
            boolean r1 = r1.startsWith(r8)     // Catch:{ Throwable -> 0x0080, all -> 0x0098 }
            if (r1 == 0) goto L_0x000e
            if (r3 == 0) goto L_0x000e
            java.io.InputStream r3 = r4.getInputStream(r0)     // Catch:{ Exception -> 0x00e4, all -> 0x008b }
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x00e8, all -> 0x00df }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00e8, all -> 0x00df }
            r0.<init>()     // Catch:{ Exception -> 0x00e8, all -> 0x00df }
            java.lang.StringBuilder r0 = r0.append(r10)     // Catch:{ Exception -> 0x00e8, all -> 0x00df }
            java.lang.String r8 = java.io.File.separator     // Catch:{ Exception -> 0x00e8, all -> 0x00df }
            java.lang.StringBuilder r0 = r0.append(r8)     // Catch:{ Exception -> 0x00e8, all -> 0x00df }
            java.lang.StringBuilder r0 = r0.append(r7)     // Catch:{ Exception -> 0x00e8, all -> 0x00df }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x00e8, all -> 0x00df }
            r1.<init>(r0)     // Catch:{ Exception -> 0x00e8, all -> 0x00df }
        L_0x0060:
            int r0 = r3.read(r6)     // Catch:{ Exception -> 0x006c }
            r7 = -1
            if (r0 == r7) goto L_0x00b8
            r7 = 0
            r1.write(r6, r7, r0)     // Catch:{ Exception -> 0x006c }
            goto L_0x0060
        L_0x006c:
            r0 = move-exception
        L_0x006d:
            r0.printStackTrace()     // Catch:{ all -> 0x00e1 }
            if (r1 == 0) goto L_0x0075
            r1.close()     // Catch:{ Exception -> 0x00b3 }
        L_0x0075:
            if (r3 == 0) goto L_0x000e
            r3.close()     // Catch:{ Exception -> 0x007b }
            goto L_0x000e
        L_0x007b:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ Throwable -> 0x0080, all -> 0x0098 }
            goto L_0x000e
        L_0x0080:
            r0 = move-exception
            r2 = r4
        L_0x0082:
            r0.printStackTrace()     // Catch:{ all -> 0x00da }
            if (r2 == 0) goto L_0x008a
            r2.close()     // Catch:{ IOException -> 0x00a4 }
        L_0x008a:
            return
        L_0x008b:
            r0 = move-exception
            r3 = r2
        L_0x008d:
            if (r2 == 0) goto L_0x0092
            r2.close()     // Catch:{ Exception -> 0x00a9 }
        L_0x0092:
            if (r3 == 0) goto L_0x0097
            r3.close()     // Catch:{ Exception -> 0x00ae }
        L_0x0097:
            throw r0     // Catch:{ Throwable -> 0x0080, all -> 0x0098 }
        L_0x0098:
            r0 = move-exception
        L_0x0099:
            if (r4 == 0) goto L_0x009e
            r4.close()     // Catch:{ IOException -> 0x009f }
        L_0x009e:
            throw r0
        L_0x009f:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x009e
        L_0x00a4:
            r0 = move-exception
        L_0x00a5:
            r0.printStackTrace()
            goto L_0x008a
        L_0x00a9:
            r1 = move-exception
            r1.printStackTrace()     // Catch:{ Throwable -> 0x0080, all -> 0x0098 }
            goto L_0x0092
        L_0x00ae:
            r1 = move-exception
            r1.printStackTrace()     // Catch:{ Throwable -> 0x0080, all -> 0x0098 }
            goto L_0x0097
        L_0x00b3:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ Throwable -> 0x0080, all -> 0x0098 }
            goto L_0x0075
        L_0x00b8:
            if (r1 == 0) goto L_0x00bd
            r1.close()     // Catch:{ Exception -> 0x00ca }
        L_0x00bd:
            if (r3 == 0) goto L_0x000e
            r3.close()     // Catch:{ Exception -> 0x00c4 }
            goto L_0x000e
        L_0x00c4:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ Throwable -> 0x0080, all -> 0x0098 }
            goto L_0x000e
        L_0x00ca:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ Throwable -> 0x0080, all -> 0x0098 }
            goto L_0x00bd
        L_0x00cf:
            if (r4 == 0) goto L_0x008a
            r4.close()     // Catch:{ IOException -> 0x00d5 }
            goto L_0x008a
        L_0x00d5:
            r0 = move-exception
            goto L_0x00a5
        L_0x00d7:
            r0 = move-exception
            r4 = r2
            goto L_0x0099
        L_0x00da:
            r0 = move-exception
            r4 = r2
            goto L_0x0099
        L_0x00dd:
            r0 = move-exception
            goto L_0x0082
        L_0x00df:
            r0 = move-exception
            goto L_0x008d
        L_0x00e1:
            r0 = move-exception
            r2 = r1
            goto L_0x008d
        L_0x00e4:
            r0 = move-exception
            r1 = r2
            r3 = r2
            goto L_0x006d
        L_0x00e8:
            r0 = move-exception
            r1 = r2
            goto L_0x006d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.utils.FileUtil.extractSoToFilePath(java.lang.String, java.lang.String):void");
    }

    private static boolean matchCpuABI(String str) {
        String str2 = Build.CPU_ABI;
        if (str2 == null || !str2.equals("x86")) {
            if (str == null || str.contains("x86") || str.contains("mips")) {
                return false;
            }
            return true;
        } else if (str == null || !str.contains("x86")) {
            return false;
        } else {
            return true;
        }
    }

    public static String getCameraDir(String str) {
        File file = new File(getRootDir() + "/" + "DCIM");
        if (!file.exists() || !file.isDirectory()) {
            file.mkdirs();
        }
        File file2 = new File(file.getAbsolutePath() + "/" + "Camera");
        if (!file2.exists() || !file2.isDirectory()) {
            file2.mkdirs();
        }
        File file3 = new File(file2.getAbsolutePath() + "/" + str + "/");
        if (!file3.exists() || !file3.isDirectory()) {
            XLog.d("BussinessEngine", "destRootPath ---not exist--mkDir");
            file3.mkdirs();
        }
        return file3.getAbsolutePath();
    }

    public static void rename(String str, String str2) {
        if (!TextUtils.isEmpty(str) && !TextUtils.isEmpty(str2)) {
            new File(str).renameTo(new File(str2));
        }
    }
}
