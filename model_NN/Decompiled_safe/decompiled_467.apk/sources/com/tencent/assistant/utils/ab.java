package com.tencent.assistant.utils;

import android.app.Dialog;
import android.view.View;
import com.tencent.assistant.AppConst;

/* compiled from: ProGuard */
final class ab implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppConst.OneBtnDialogInfo f2624a;
    final /* synthetic */ Dialog b;

    ab(AppConst.OneBtnDialogInfo oneBtnDialogInfo, Dialog dialog) {
        this.f2624a = oneBtnDialogInfo;
        this.b = dialog;
    }

    public void onClick(View view) {
        if (this.f2624a != null) {
            this.f2624a.onBtnClick();
            this.b.dismiss();
        }
    }
}
