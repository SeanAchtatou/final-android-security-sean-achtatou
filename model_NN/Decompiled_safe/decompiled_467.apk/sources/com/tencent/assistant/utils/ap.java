package com.tencent.assistant.utils;

import android.os.Environment;
import android.os.StatFs;
import java.util.List;

/* compiled from: ProGuard */
public class ap {
    public static boolean a() {
        String externalStorageState = Environment.getExternalStorageState();
        if ("mounted".equals(externalStorageState) || "mounted_ro".equals(externalStorageState)) {
            return true;
        }
        return false;
    }

    public static boolean b() {
        if ("mounted".equals(Environment.getExternalStorageState())) {
            return true;
        }
        return false;
    }

    public static long c() {
        List<String> e = e();
        int i = 0;
        long j = 0;
        while (true) {
            int i2 = i;
            if (i2 >= e.size()) {
                return j;
            }
            try {
                StatFs statFs = new StatFs(e.get(i2));
                j += ((long) statFs.getAvailableBlocks()) * ((long) statFs.getBlockSize());
            } catch (IllegalArgumentException e2) {
                e2.printStackTrace();
            }
            i = i2 + 1;
        }
    }

    public static long d() {
        List<String> e = e();
        int i = 0;
        long j = 0;
        while (true) {
            int i2 = i;
            if (i2 >= e.size()) {
                return j;
            }
            try {
                StatFs statFs = new StatFs(e.get(i2));
                j += ((long) statFs.getBlockCount()) * ((long) statFs.getBlockSize());
            } catch (IllegalArgumentException e2) {
                e2.printStackTrace();
            }
            i = i2 + 1;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:37:0x009a A[SYNTHETIC, Splitter:B:37:0x009a] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x009f A[SYNTHETIC, Splitter:B:40:0x009f] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00a4 A[SYNTHETIC, Splitter:B:43:0x00a4] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00a9  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x00bd A[SYNTHETIC, Splitter:B:52:0x00bd] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x00c2 A[SYNTHETIC, Splitter:B:55:0x00c2] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x00c7 A[SYNTHETIC, Splitter:B:58:0x00c7] */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x00cc  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.List<java.lang.String> e() {
        /*
            r2 = 0
            r9 = 1
            java.lang.Runtime r1 = java.lang.Runtime.getRuntime()
            java.util.ArrayList r6 = new java.util.ArrayList
            r6.<init>()
            java.io.File r0 = android.os.Environment.getExternalStorageDirectory()
            java.lang.String r7 = r0.getAbsolutePath()
            java.io.File r0 = android.os.Environment.getExternalStorageDirectory()
            java.lang.String r0 = r0.getParent()
            java.lang.String r3 = "/"
            java.lang.String[] r0 = r0.split(r3)
            if (r0 == 0) goto L_0x00ad
            int r3 = r0.length
            if (r3 <= r9) goto L_0x00ad
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "/"
            java.lang.StringBuilder r3 = r3.append(r4)
            r0 = r0[r9]
            java.lang.StringBuilder r0 = r3.append(r0)
            java.lang.String r0 = r0.toString()
        L_0x003b:
            java.lang.String r3 = "mount"
            java.lang.Process r5 = r1.exec(r3)     // Catch:{ IOException -> 0x0125, all -> 0x00b6 }
            java.io.InputStream r4 = r5.getInputStream()     // Catch:{ IOException -> 0x012b, all -> 0x0112 }
            java.io.InputStreamReader r3 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x0131, all -> 0x0117 }
            r3.<init>(r4)     // Catch:{ IOException -> 0x0131, all -> 0x0117 }
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ IOException -> 0x0137, all -> 0x011b }
            r1.<init>(r3)     // Catch:{ IOException -> 0x0137, all -> 0x011b }
        L_0x004f:
            java.lang.String r2 = r1.readLine()     // Catch:{ IOException -> 0x0091, all -> 0x011e }
            if (r2 == 0) goto L_0x00ee
            java.lang.String r8 = "secure"
            boolean r8 = r2.contains(r8)     // Catch:{ IOException -> 0x0091, all -> 0x011e }
            if (r8 != 0) goto L_0x004f
            java.lang.String r8 = "asec"
            boolean r8 = r2.contains(r8)     // Catch:{ IOException -> 0x0091, all -> 0x011e }
            if (r8 != 0) goto L_0x004f
            java.lang.String r8 = "fat"
            boolean r8 = r2.contains(r8)     // Catch:{ IOException -> 0x0091, all -> 0x011e }
            if (r8 == 0) goto L_0x004f
            java.lang.String r8 = " "
            java.lang.String[] r2 = r2.split(r8)     // Catch:{ IOException -> 0x0091, all -> 0x011e }
            if (r2 == 0) goto L_0x004f
            int r8 = r2.length     // Catch:{ IOException -> 0x0091, all -> 0x011e }
            if (r8 <= r9) goto L_0x004f
            r8 = 1
            r8 = r2[r8]     // Catch:{ IOException -> 0x0091, all -> 0x011e }
            boolean r8 = r7.equals(r8)     // Catch:{ IOException -> 0x0091, all -> 0x011e }
            if (r8 != 0) goto L_0x004f
            r8 = 1
            r8 = r2[r8]     // Catch:{ IOException -> 0x0091, all -> 0x011e }
            boolean r8 = r8.contains(r0)     // Catch:{ IOException -> 0x0091, all -> 0x011e }
            if (r8 == 0) goto L_0x004f
            r8 = 1
            r2 = r2[r8]     // Catch:{ IOException -> 0x0091, all -> 0x011e }
            r6.add(r2)     // Catch:{ IOException -> 0x0091, all -> 0x011e }
            goto L_0x004f
        L_0x0091:
            r0 = move-exception
            r2 = r3
            r3 = r4
            r4 = r5
        L_0x0095:
            r0.printStackTrace()     // Catch:{ all -> 0x0120 }
            if (r1 == 0) goto L_0x009d
            r1.close()     // Catch:{ IOException -> 0x00df }
        L_0x009d:
            if (r2 == 0) goto L_0x00a2
            r2.close()     // Catch:{ IOException -> 0x00e4 }
        L_0x00a2:
            if (r3 == 0) goto L_0x00a7
            r3.close()     // Catch:{ IOException -> 0x00e9 }
        L_0x00a7:
            if (r4 == 0) goto L_0x00ac
            r4.destroy()
        L_0x00ac:
            return r6
        L_0x00ad:
            java.io.File r0 = android.os.Environment.getExternalStorageDirectory()
            java.lang.String r0 = r0.getParent()
            goto L_0x003b
        L_0x00b6:
            r0 = move-exception
            r1 = r2
            r3 = r2
            r4 = r2
            r5 = r2
        L_0x00bb:
            if (r1 == 0) goto L_0x00c0
            r1.close()     // Catch:{ IOException -> 0x00d0 }
        L_0x00c0:
            if (r3 == 0) goto L_0x00c5
            r3.close()     // Catch:{ IOException -> 0x00d5 }
        L_0x00c5:
            if (r4 == 0) goto L_0x00ca
            r4.close()     // Catch:{ IOException -> 0x00da }
        L_0x00ca:
            if (r5 == 0) goto L_0x00cf
            r5.destroy()
        L_0x00cf:
            throw r0
        L_0x00d0:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00c0
        L_0x00d5:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00c5
        L_0x00da:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00ca
        L_0x00df:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x009d
        L_0x00e4:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00a2
        L_0x00e9:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00a7
        L_0x00ee:
            if (r1 == 0) goto L_0x00f3
            r1.close()     // Catch:{ IOException -> 0x0103 }
        L_0x00f3:
            if (r3 == 0) goto L_0x00f8
            r3.close()     // Catch:{ IOException -> 0x0108 }
        L_0x00f8:
            if (r4 == 0) goto L_0x00fd
            r4.close()     // Catch:{ IOException -> 0x010d }
        L_0x00fd:
            if (r5 == 0) goto L_0x00ac
            r5.destroy()
            goto L_0x00ac
        L_0x0103:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00f3
        L_0x0108:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00f8
        L_0x010d:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00fd
        L_0x0112:
            r0 = move-exception
            r1 = r2
            r3 = r2
            r4 = r2
            goto L_0x00bb
        L_0x0117:
            r0 = move-exception
            r1 = r2
            r3 = r2
            goto L_0x00bb
        L_0x011b:
            r0 = move-exception
            r1 = r2
            goto L_0x00bb
        L_0x011e:
            r0 = move-exception
            goto L_0x00bb
        L_0x0120:
            r0 = move-exception
            r5 = r4
            r4 = r3
            r3 = r2
            goto L_0x00bb
        L_0x0125:
            r0 = move-exception
            r1 = r2
            r3 = r2
            r4 = r2
            goto L_0x0095
        L_0x012b:
            r0 = move-exception
            r1 = r2
            r3 = r2
            r4 = r5
            goto L_0x0095
        L_0x0131:
            r0 = move-exception
            r1 = r2
            r3 = r4
            r4 = r5
            goto L_0x0095
        L_0x0137:
            r0 = move-exception
            r1 = r2
            r2 = r3
            r3 = r4
            r4 = r5
            goto L_0x0095
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.utils.ap.e():java.util.List");
    }
}
