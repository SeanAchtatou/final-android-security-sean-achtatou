package com.tencent.assistant.utils.installuninstall;

import com.qq.AppService.AstApp;
import com.tencent.assistant.st.STConstAction;

/* compiled from: ProGuard */
class k extends o {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ InstallUninstallDialogManager f2705a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    k(InstallUninstallDialogManager installUninstallDialogManager) {
        super(installUninstallDialogManager);
        this.f2705a = installUninstallDialogManager;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager, boolean):boolean
     arg types: [com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager, int]
     candidates:
      com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager, com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_RESULT):com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_RESULT
      com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_TYPE, com.tencent.assistant.utils.installuninstall.InstallUninstallTaskBean):boolean
      com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager, boolean):boolean */
    public void onLeftBtnClick() {
        this.f2705a.a(this.pageId, AstApp.m() != null ? AstApp.m().f() : 2000, "04_001", STConstAction.ACTION_HIT_SEARCH_CANCEL);
        boolean unused = this.f2705a.c = false;
        this.f2705a.c();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager, boolean):boolean
     arg types: [com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager, int]
     candidates:
      com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager, com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_RESULT):com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_RESULT
      com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_TYPE, com.tencent.assistant.utils.installuninstall.InstallUninstallTaskBean):boolean
      com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager, boolean):boolean */
    public void onRightBtnClick() {
        this.f2705a.a(this.pageId, AstApp.m() != null ? AstApp.m().f() : 2000, "03_001", 200);
        boolean unused = this.f2705a.c = true;
        this.f2705a.c();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager, boolean):boolean
     arg types: [com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager, int]
     candidates:
      com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager, com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_RESULT):com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_RESULT
      com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_TYPE, com.tencent.assistant.utils.installuninstall.InstallUninstallTaskBean):boolean
      com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager, boolean):boolean */
    public void onCancell() {
        this.f2705a.a(this.pageId, AstApp.m() != null ? AstApp.m().f() : 2000, "05_001", 200);
        boolean unused = this.f2705a.c = false;
        this.f2705a.c();
    }
}
