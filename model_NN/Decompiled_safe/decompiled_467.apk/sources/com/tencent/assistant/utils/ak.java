package com.tencent.assistant.utils;

import android.content.DialogInterface;
import com.tencent.assistant.AppConst;

/* compiled from: ProGuard */
final class ak implements DialogInterface.OnCancelListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppConst.TwoBtnDialogInfo f2632a;

    ak(AppConst.TwoBtnDialogInfo twoBtnDialogInfo) {
        this.f2632a = twoBtnDialogInfo;
    }

    public void onCancel(DialogInterface dialogInterface) {
        this.f2632a.onCancell();
    }
}
