package com.tencent.assistant.utils.installuninstall;

import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager;

/* compiled from: ProGuard */
public class a {
    private static a c = null;

    /* renamed from: a  reason: collision with root package name */
    private final Object f2686a = new Object();
    private final long b = 90000;

    private a() {
    }

    public static a a() {
        if (c == null) {
            c = new a();
        }
        return c;
    }

    public void b() {
        synchronized (this.f2686a) {
            try {
                this.f2686a.wait(90000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void c() {
        synchronized (this.f2686a) {
            this.f2686a.notifyAll();
        }
    }

    public InstallUninstallDialogManager.DIALOG_DEALWITH_RESULT a(InstallUninstallTaskBean installUninstallTaskBean) {
        InstallUninstallDialogManager.DIALOG_DEALWITH_RESULT dialog_dealwith_result = InstallUninstallDialogManager.DIALOG_DEALWITH_RESULT.RESULT_RIGHT_BUTTON;
        if (((Boolean) InstallUninstallUtil.a(installUninstallTaskBean.fileSize).first).booleanValue()) {
            return InstallUninstallDialogManager.DIALOG_DEALWITH_RESULT.RESULT_LEFT_BUTTON;
        }
        InstallUninstallDialogManager installUninstallDialogManager = new InstallUninstallDialogManager();
        InstallUninstallDialogManager.DIALOG_DEALWITH_RESULT dialog_dealwith_result2 = dialog_dealwith_result;
        int i = 1;
        while (dialog_dealwith_result2 == InstallUninstallDialogManager.DIALOG_DEALWITH_RESULT.RESULT_RIGHT_BUTTON) {
            XLog.d("million", "InstallStorageLowManager.storageLowClear retry " + i + " times wait begin...");
            b();
            XLog.d("million", "InstallStorageLowManager.storageLowClear retry " + i + " times wait end...");
            if (!((Boolean) InstallUninstallUtil.a(installUninstallTaskBean.fileSize).first).booleanValue()) {
                installUninstallDialogManager.a(true);
                InstallUninstallDialogManager.DIALOG_DEALWITH_RESULT b2 = installUninstallDialogManager.b(InstallUninstallDialogManager.DIALOG_DEALWITH_TYPE.INSTALL_SPACE_NOT_ENOUGH, installUninstallTaskBean);
                installUninstallDialogManager.a(false);
                i++;
                dialog_dealwith_result2 = b2;
            } else {
                c();
                return InstallUninstallDialogManager.DIALOG_DEALWITH_RESULT.RESULT_LEFT_BUTTON;
            }
        }
        return dialog_dealwith_result2;
    }
}
