package com.tencent.assistant.utils;

import android.text.TextUtils;
import com.tencent.assistant.login.a.c;
import com.tencent.assistantv2.model.h;
import com.tencent.connect.common.Constants;
import com.tencent.d.a.a;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: ProGuard */
public class de {

    /* renamed from: a  reason: collision with root package name */
    private static final String f2677a = de.class.getSimpleName();

    public static int a(h hVar) {
        if (hVar == null || hVar.k == null || 0 == hVar.k.f3314a) {
            return 0;
        }
        XLog.i(f2677a, "[getVideoDownloadPercent] ---> length = " + hVar.k.b + " , totalLength = " + hVar.k.f3314a);
        return (int) ((((double) hVar.k.b) / ((double) hVar.k.f3314a)) * 100.0d);
    }

    public static String b(h hVar) {
        if (hVar == null) {
            return Constants.STR_EMPTY;
        }
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("videoDownloadState", hVar.i);
        jSONObject.put("videoId", hVar.c);
        jSONObject.put("percent", a(hVar));
        return jSONObject.toString();
    }

    public static String a(List<h> list) {
        if (list == null) {
            return Constants.STR_EMPTY;
        }
        JSONArray jSONArray = new JSONArray();
        JSONObject jSONObject = new JSONObject();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("videoDownloadState", list.get(i).i);
            jSONObject2.put("videoId", list.get(i).c);
            jSONObject2.put("percent", a(list.get(i)));
            jSONArray.put(jSONObject2);
        }
        jSONObject.put("videoDownloadInfoListSize", size);
        jSONObject.put("videoDownloadInfoList", jSONArray);
        return jSONObject.toString();
    }

    private static String a() {
        return "MIICXQIBAAKBgQCv1UJVKMxJt4J4d8lSIjgVkPkiU4wb+c09zCR9bT/W6WmCE/IQiRIh/xB23AJs35F6qo/4l3BsfVvvD9xXfmz7s6SKJ5zWPyHimKpZcNfzuwMCos4F3GWu9hPtyD5b0SIBiCdCUgfSx8WfKOvkTnVevv9VM6P0T+DbUEYodo2F+wIDAQABAoGAWC+oHhfhJZOxDwRc5yGcaUyVdt7aJWnKwxSwtGtnmjzkmsWwIRTuEDjhpAtB+CLEzvXeUTp6ux7ATgzxYLxaoalWMMW5cr5KhY0UgkY9hNuXVDsPj3qVuTE7J+SHHB5i/i3KCYUUzik08ktFJcGTf4/DH/djkxuFPNIzqEcIIuECQQDggL0k01djIZIfVGY+UELad0TRyTfNkakQb9ygT7D3//bUbik15rjJ8qhLXNRLnhlDGxImSG1jgna3Z8IteSKrAkEAyIB9p5MG5yz3jktK5dSf+amMpwhaLfkLIZoOzmtLN5lHfDJ6tuBYmmU0eIPowl2jL2838xcr3gcE24jW1uOp8QJBAITUpxS61uGYY1SWI0iLRILuPpHBdHr0zAslpGxVumeB0xEtfMSfloYmRN0SN6nmCRxjUAHGqLJP5t3tTj6JhbECQQCUfIwSSVGIbQXdDKeoM+JRzB4NKaNXfqOeu/ARMOagg1SshtnYi6cymJbWVaJQJ3aNz4kY72UeZgZT6zBoLmyRAkA+06BGYDyGEkONQYHQ+U86h2SRUnVcxDd02g7RLygNmgIbVGQwXPqVoL/isbkvNNaRmGdo37SX6yLPelzlL2C2";
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.login.a.c.a(java.lang.String, java.lang.String, boolean):byte[]
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.tencent.assistant.login.a.c.a(byte[], java.lang.String, boolean):java.lang.String
      com.tencent.assistant.login.a.c.a(java.lang.String, java.lang.String, boolean):byte[] */
    public static String a(String str) {
        byte[] c;
        StringBuilder sb = new StringBuilder();
        if (!TextUtils.isEmpty(str)) {
            sb.append("vid:" + str + "[" + (System.currentTimeMillis() / 1000) + "]");
        }
        String k = t.k();
        if (!TextUtils.isEmpty(k)) {
            sb.append(";mac:" + k);
        }
        String h = t.h();
        if (!TextUtils.isEmpty(h)) {
            sb.append(";imsi:" + h);
        }
        XLog.i(f2677a, "[getTencentVideoRSAKey] : " + sb.toString());
        byte[] a2 = c.a(sb.toString(), a(), false);
        if (a2 == null || (c = a.c(a2, 0)) == null) {
            return Constants.STR_EMPTY;
        }
        return new String(c);
    }
}
