package com.tencent.assistant.adapter;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class h extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ l f798a;
    final /* synthetic */ LocalApkInfo b;
    final /* synthetic */ STInfoV2 c;
    final /* synthetic */ d d;

    h(d dVar, l lVar, LocalApkInfo localApkInfo, STInfoV2 sTInfoV2) {
        this.d = dVar;
        this.f798a = lVar;
        this.b = localApkInfo;
        this.c = sTInfoV2;
    }

    public void onTMAClick(View view) {
        this.f798a.b.setSelected(!this.f798a.b.isSelected());
        this.d.notifyDataSetChanged();
        this.b.mIsSelect = this.f798a.b.isSelected();
        if (this.d.f759a != null) {
            this.d.f759a.sendMessage(this.d.f759a.obtainMessage(110005, this.b));
        }
    }

    public STInfoV2 getStInfo() {
        this.c.actionId = 200;
        this.c.status = "01";
        return this.c;
    }
}
