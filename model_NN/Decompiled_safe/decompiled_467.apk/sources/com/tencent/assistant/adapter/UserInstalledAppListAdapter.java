package com.tencent.assistant.adapter;

import android.content.Context;
import android.os.Handler;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.LocalPkgSizeTextView;
import com.tencent.assistant.component.appdetail.HorizonImageListView;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.m;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.ct;
import com.tencent.assistant.utils.cv;
import com.tencent.assistant.utils.df;
import com.tencent.assistantv2.component.MovingProgressBar;
import com.tencent.assistantv2.st.b.b;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class UserInstalledAppListAdapter extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    private Context f684a;
    private List<LocalApkInfo> b = new ArrayList();
    private LayoutInflater c;
    private IViewInvalidater d;
    private boolean e = true;
    private int f = 0;
    /* access modifiers changed from: private */
    public Handler g;
    private boolean h;

    public UserInstalledAppListAdapter(Context context) {
        this.f684a = context;
        this.c = LayoutInflater.from(context);
    }

    public void a(List<LocalApkInfo> list, int i) {
        this.b.clear();
        this.b.addAll(list);
        this.f = i;
        notifyDataSetChanged();
    }

    public int getCount() {
        if (this.b != null) {
            return this.b.size();
        }
        return 0;
    }

    public Object getItem(int i) {
        if (this.b != null) {
            return this.b.get(i);
        }
        return null;
    }

    public long getItemId(int i) {
        return 0;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        el elVar;
        if (view == null || view.getTag() == null) {
            view = this.c.inflate((int) R.layout.installed_app_list_item, (ViewGroup) null);
            elVar = new el(null);
            elVar.f795a = view.findViewById(R.id.container_layout);
            elVar.c = (TextView) view.findViewById(R.id.soft_name_txt);
            elVar.b = (TXImageView) view.findViewById(R.id.soft_icon_img);
            elVar.d = (LocalPkgSizeTextView) view.findViewById(R.id.soft_size_txt);
            elVar.e = (TextView) view.findViewById(R.id.tv_check);
            elVar.f = (TextView) view.findViewById(R.id.last_used_time_txt);
            elVar.g = (TextView) view.findViewById(R.id.popbar_title);
            elVar.j = view.findViewById(R.id.rl_appdetail);
            elVar.h = view.findViewById(R.id.uninstall_progress);
            elVar.i = (MovingProgressBar) view.findViewById(R.id.app_uninstall_progress_bar);
            elVar.i.a(AstApp.i().getResources().getDimensionPixelSize(R.dimen.app_detail_pic_gap));
            elVar.i.b(AstApp.i().getResources().getDimensionPixelSize(R.dimen.install_progress_bar_width));
            elVar.k = view.findViewById(R.id.top_margin);
            elVar.l = view.findViewById(R.id.bottom_margin);
            elVar.m = view.findViewById(R.id.last_line);
            view.setTag(elVar);
        } else {
            elVar = (el) view.getTag();
        }
        a(this.b.get(i), elVar, i, a(this.f, i));
        return view;
    }

    private void a(LocalApkInfo localApkInfo, el elVar, int i, STInfoV2 sTInfoV2) {
        elVar.c.setText(localApkInfo.mAppName);
        elVar.b.updateImageView(localApkInfo.mPackageName, R.drawable.pic_defaule, TXImageView.TXImageViewType.INSTALL_APK_ICON);
        if (this.h) {
            elVar.e.setEnabled(false);
            elVar.f795a.setEnabled(false);
            if (localApkInfo.mIsSelect) {
                elVar.h.setVisibility(0);
                elVar.j.setVisibility(8);
                elVar.i.a();
            } else {
                elVar.h.setVisibility(8);
                elVar.j.setVisibility(0);
                elVar.i.b();
            }
        } else {
            elVar.e.setEnabled(true);
            elVar.f795a.setEnabled(true);
            elVar.h.setVisibility(8);
            elVar.j.setVisibility(0);
            elVar.i.b();
        }
        elVar.e.setSelected(localApkInfo.mIsSelect);
        if (this.f == 1) {
            elVar.d.setTextColor(this.f684a.getResources().getColor(R.color.appadmin_highlight_text));
        } else {
            elVar.d.setTextColor(this.f684a.getResources().getColor(R.color.appadmin_card_size_text));
        }
        elVar.d.updateTextView(localApkInfo.mPackageName, localApkInfo.occupySize);
        elVar.d.setInvalidater(this.d);
        if (i == 0) {
            elVar.g.setText(this.f684a.getString(R.string.popbar_title_installed_count, Integer.valueOf(getCount())));
            elVar.g.setVisibility(0);
        } else {
            elVar.g.setVisibility(8);
        }
        if (this.f == 3) {
            a(elVar.f, localApkInfo.mInstallDate);
        } else {
            a(elVar.f, localApkInfo.mLastLaunchTime, localApkInfo.mFakeLastLaunchTime);
        }
        if (this.e) {
            ((RelativeLayout.LayoutParams) elVar.e.getLayoutParams()).rightMargin = df.a(this.f684a, 16.0f);
            ((LinearLayout.LayoutParams) elVar.m.getLayoutParams()).rightMargin = df.a(this.f684a, 16.0f);
        } else {
            ((RelativeLayout.LayoutParams) elVar.e.getLayoutParams()).rightMargin = df.a(this.f684a, 46.0f);
            ((LinearLayout.LayoutParams) elVar.m.getLayoutParams()).rightMargin = df.a(this.f684a, 30.0f);
        }
        elVar.e.setOnClickListener(new ej(this, elVar, localApkInfo, sTInfoV2));
        elVar.f795a.setOnClickListener(new ek(this, elVar, localApkInfo, sTInfoV2));
        a(elVar.f795a, elVar.m, elVar.k, elVar.l, i, this.b.size());
    }

    private STInfoV2 a(int i, int i2) {
        String b2 = b(i, i2);
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f684a, 100);
        buildSTInfo.slotId = b2;
        buildSTInfo.scene = STConst.ST_PAGE_USER_APP_UNINSTALL;
        b.getInstance().exposure(buildSTInfo);
        return buildSTInfo;
    }

    private String b(int i, int i2) {
        if (i == 0) {
            return "03_" + ct.a(i2 + 1);
        }
        if (i == 1) {
            return "04_" + ct.a(i2 + 1);
        }
        if (i == 2) {
            return HorizonImageListView.TMA_ST_HORIZON_IMAGE_TAG + ct.a(i2 + 1);
        }
        return "06_" + ct.a(i2 + 1);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean
      com.tencent.assistant.m.a(java.lang.String, long):long */
    private void a(TextView textView, long j, long j2) {
        int f2;
        long a2 = m.a().a("key_first_load_installed_app_time", 0L);
        boolean z = a2 != 0 && (System.currentTimeMillis() - a2) - 259200000 >= 0;
        if (j > 0 || z) {
            if (j > 0) {
                f2 = cv.f(j);
            } else {
                f2 = cv.f(j2);
            }
            if (f2 == 0) {
                textView.setVisibility(0);
                textView.setText(this.f684a.getResources().getString(R.string.today));
                return;
            }
            String format = String.format(this.f684a.getResources().getString(R.string.last_used_time), Integer.valueOf(f2));
            if (this.f == 0) {
                SpannableString spannableString = new SpannableString(format);
                spannableString.setSpan(new ForegroundColorSpan(this.f684a.getResources().getColor(R.color.appadmin_highlight_text)), 0, format.indexOf("天") + 1, 33);
                textView.setText(spannableString);
            } else {
                textView.setText(format);
            }
            textView.setVisibility(0);
            return;
        }
        textView.setVisibility(8);
    }

    private void a(TextView textView, long j) {
        if (j == 0) {
            textView.setVisibility(8);
            return;
        }
        int f2 = cv.f(j);
        if (f2 == 0) {
            textView.setVisibility(0);
            textView.setText(this.f684a.getResources().getString(R.string.today_install));
            return;
        }
        String format = String.format(this.f684a.getResources().getString(R.string.install_time), Integer.valueOf(f2));
        SpannableString spannableString = new SpannableString(format);
        spannableString.setSpan(new ForegroundColorSpan(this.f684a.getResources().getColor(R.color.appadmin_highlight_text)), 0, format.indexOf("天") + 1, 33);
        textView.setText(spannableString);
        textView.setVisibility(0);
    }

    public void a(View view, View view2, View view3, View view4, int i, int i2) {
        if (i2 == 1) {
            view2.setVisibility(8);
            view4.setVisibility(0);
            view3.setVisibility(0);
        } else if (i == 0) {
            view2.setVisibility(0);
            view4.setVisibility(8);
            view3.setVisibility(0);
        } else if (i == i2 - 1) {
            view2.setVisibility(8);
            view4.setVisibility(0);
            view3.setVisibility(8);
        } else {
            view2.setVisibility(0);
            view4.setVisibility(8);
            view3.setVisibility(8);
        }
        view.setBackgroundResource(R.drawable.helper_cardbg_all_selector_new);
    }

    public int a(int i) {
        for (int i2 = 0; i2 < getCount(); i2++) {
            String str = this.b.get(i2).mSortKey;
            if (!TextUtils.isEmpty(str)) {
                char charAt = str.toUpperCase().charAt(0);
                if (charAt == i) {
                    return i2;
                }
                if (i == 35 && charAt >= '0' && charAt <= '9') {
                    return i2;
                }
            }
        }
        return -1;
    }

    public void a(IViewInvalidater iViewInvalidater) {
        this.d = iViewInvalidater;
    }

    public void a(boolean z) {
        this.e = z;
    }

    public void a(Handler handler) {
        this.g = handler;
    }

    public void a(boolean z, boolean z2) {
        this.h = z;
        if (z2) {
            notifyDataSetChanged();
        }
    }
}
