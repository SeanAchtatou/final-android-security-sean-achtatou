package com.tencent.assistant.adapter;

import android.text.Html;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXImageView;

/* compiled from: ProGuard */
class ei {

    /* renamed from: a  reason: collision with root package name */
    LinearLayout f792a;
    TXImageView b;
    TextView c;
    TextView d;
    final /* synthetic */ UserCenterAdapter e;

    private ei(UserCenterAdapter userCenterAdapter) {
        this.e = userCenterAdapter;
        this.f792a = null;
        this.b = null;
        this.c = null;
        this.d = null;
    }

    public void a(String str, int i, TXImageView.TXImageViewType tXImageViewType) {
        if (this.b != null) {
            this.b.updateImageView(str, i, tXImageViewType);
        }
    }

    public void a(String str) {
        if (this.c != null) {
            this.c.setText(Html.fromHtml(str));
        }
    }

    public void b(String str) {
        if (this.d != null) {
            this.d.setText(Html.fromHtml(str));
        }
    }

    public void a(int i) {
        if (this.f792a == null) {
            return;
        }
        if (1 == i) {
            this.f792a.setBackgroundResource(R.drawable.uc_cardbg_top_selector);
        } else if (2 == i) {
            this.f792a.setBackgroundResource(R.drawable.uc_cardbg_middle_selector);
        } else if (3 == i) {
            this.f792a.setBackgroundResource(R.drawable.uc_cardbg_bottom_selector);
        } else {
            this.f792a.setBackgroundResource(R.drawable.uc_pop_cardbg_selector);
        }
    }
}
