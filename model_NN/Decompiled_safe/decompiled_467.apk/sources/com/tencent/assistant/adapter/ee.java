package com.tencent.assistant.adapter;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import java.util.List;

/* compiled from: ProGuard */
public class ee extends PagerAdapter {

    /* renamed from: a  reason: collision with root package name */
    private List<View> f789a = null;

    public ee(List<View> list) {
        this.f789a = list;
    }

    public void destroyItem(ViewGroup viewGroup, int i, Object obj) {
        viewGroup.removeView(this.f789a.get(i));
    }

    public Object instantiateItem(ViewGroup viewGroup, int i) {
        viewGroup.addView(this.f789a.get(i));
        return this.f789a.get(i);
    }

    public int getCount() {
        return this.f789a.size();
    }

    public boolean isViewFromObject(View view, Object obj) {
        return view == obj;
    }
}
