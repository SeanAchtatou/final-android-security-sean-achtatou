package com.tencent.assistant.adapter;

import android.view.View;
import android.widget.TextView;
import com.tencent.assistant.component.listener.OnTMAParamExClickListener;
import com.tencent.assistant.k;
import com.tencent.assistantv2.model.ItemElement;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class aq extends OnTMAParamExClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ar f703a;
    final /* synthetic */ ItemElement b;
    final /* synthetic */ int c;
    final /* synthetic */ ChildSettingAdapter d;

    aq(ChildSettingAdapter childSettingAdapter, ar arVar, ItemElement itemElement, int i) {
        this.d = childSettingAdapter;
        this.f703a = arVar;
        this.b = itemElement;
        this.c = i;
    }

    public void onTMAClick(View view) {
        this.f703a.e.setSelected(!this.f703a.e.isSelected());
        k.a(this.b.d, this.f703a.e.isSelected());
    }

    public STInfoV2 getStInfo(View view) {
        if (!(view instanceof TextView)) {
            return null;
        }
        return this.d.a(this.d.a(this.c), this.d.a(view.isSelected()), 200);
    }
}
