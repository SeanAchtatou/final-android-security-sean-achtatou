package com.tencent.assistant.adapter;

import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class ag extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SimpleAppModel f693a;
    final /* synthetic */ x b;
    final /* synthetic */ STInfoV2 c;
    final /* synthetic */ ac d;

    ag(ac acVar, SimpleAppModel simpleAppModel, x xVar, STInfoV2 sTInfoV2) {
        this.d = acVar;
        this.f693a = simpleAppModel;
        this.b = xVar;
        this.c = sTInfoV2;
    }

    public void onTMAClick(View view) {
        this.d.r.clear();
        if (this.d.s == null || !this.d.s.equals(this.f693a.c)) {
            String unused = this.d.s = this.f693a.c;
            this.d.notifyDataSetChanged();
            return;
        }
        String unused2 = this.d.s = (String) null;
        this.b.j.setVisibility(0);
        this.b.k.setVisibility(8);
        this.d.r.delete(this.f693a.c.hashCode());
        this.b.m.setImageDrawable(this.d.i.getResources().getDrawable(R.drawable.icon_open));
    }

    public STInfoV2 getStInfo() {
        this.c.actionId = 200;
        this.c.status = "02";
        return this.c;
    }
}
