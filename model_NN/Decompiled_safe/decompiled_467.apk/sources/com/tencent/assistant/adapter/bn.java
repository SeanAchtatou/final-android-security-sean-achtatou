package com.tencent.assistant.adapter;

import com.tencent.assistant.AppConst;
import com.tencent.assistant.utils.TemporaryThreadManager;

/* compiled from: ProGuard */
class bn extends AppConst.TwoBtnDialogInfo {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ bm f727a;

    bn(bm bmVar) {
        this.f727a = bmVar;
    }

    public void onLeftBtnClick() {
    }

    public void onRightBtnClick() {
        TemporaryThreadManager.get().start(new bo(this));
    }

    public void onCancell() {
    }
}
