package com.tencent.assistant.adapter.a;

import android.widget.TextView;

/* compiled from: ProGuard */
class b implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ c f685a;
    final /* synthetic */ int b;
    final /* synthetic */ TextView c;
    final /* synthetic */ c d;
    final /* synthetic */ a e;

    b(a aVar, c cVar, int i, TextView textView, c cVar2) {
        this.e = aVar;
        this.f685a = cVar;
        this.b = i;
        this.c = textView;
        this.d = cVar2;
    }

    public void run() {
        Integer num;
        if (this.f685a != null && (num = (Integer) this.f685a.f686a.getTag()) != null && num.intValue() == this.b) {
            if (this.c != null) {
                this.c.setVisibility(0);
            }
            e.a(this.d);
        }
    }
}
