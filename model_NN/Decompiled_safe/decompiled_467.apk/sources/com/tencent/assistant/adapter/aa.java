package com.tencent.assistant.adapter;

import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class aa extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SimpleAppModel f687a;
    final /* synthetic */ x b;
    final /* synthetic */ STInfoV2 c;
    final /* synthetic */ AppUpdateIgnoreListAdapter d;

    aa(AppUpdateIgnoreListAdapter appUpdateIgnoreListAdapter, SimpleAppModel simpleAppModel, x xVar, STInfoV2 sTInfoV2) {
        this.d = appUpdateIgnoreListAdapter;
        this.f687a = simpleAppModel;
        this.b = xVar;
        this.c = sTInfoV2;
    }

    public void onTMAClick(View view) {
        this.d.h.clear();
        if (this.d.m == null || !this.d.m.equals(this.f687a.c)) {
            String unused = this.d.m = this.f687a.c;
            this.d.notifyDataSetChanged();
            return;
        }
        String unused2 = this.d.m = (String) null;
        this.b.j.setVisibility(0);
        this.b.k.setVisibility(8);
        this.d.h.delete(this.f687a.c.hashCode());
        this.b.m.setImageDrawable(this.d.e.getResources().getDrawable(R.drawable.icon_open));
    }

    public STInfoV2 getStInfo() {
        if (this.c != null) {
            this.c.actionId = 200;
            this.c.status = "02";
        }
        return this.c;
    }
}
