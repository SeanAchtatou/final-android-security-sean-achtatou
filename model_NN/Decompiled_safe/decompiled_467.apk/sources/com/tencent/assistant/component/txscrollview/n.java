package com.tencent.assistant.component.txscrollview;

import com.tencent.assistant.component.txscrollview.TXScrollViewBase;

/* compiled from: ProGuard */
class n implements TXScrollViewBase.ISmoothScrollRunnableListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TXRefreshGetMoreListView f1210a;

    n(TXRefreshGetMoreListView tXRefreshGetMoreListView) {
        this.f1210a = tXRefreshGetMoreListView;
    }

    public void onSmoothScrollFinished() {
        if (this.f1210a.mRefreshListViewListener != null) {
            this.f1210a.mRefreshListViewListener.onTXRefreshListViewRefresh(this.f1210a.mCurScrollState);
        }
    }
}
