package com.tencent.assistant.component;

import android.os.Bundle;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.link.b;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class aq extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f946a;
    final /* synthetic */ Bundle b;
    final /* synthetic */ int c;
    final /* synthetic */ String d;
    final /* synthetic */ HomePageBanner e;

    aq(HomePageBanner homePageBanner, String str, Bundle bundle, int i, String str2) {
        this.e = homePageBanner;
        this.f946a = str;
        this.b = bundle;
        this.c = i;
        this.d = str2;
    }

    public void onTMAClick(View view) {
        b.b(this.e.context, this.f946a, this.b);
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.e.context, 200);
        buildSTInfo.slotId = a.a(this.e.bannerSlotTag, this.c);
        buildSTInfo.extraData = this.d;
        return buildSTInfo;
    }
}
