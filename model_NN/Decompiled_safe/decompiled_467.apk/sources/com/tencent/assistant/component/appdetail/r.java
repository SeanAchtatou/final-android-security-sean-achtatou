package com.tencent.assistant.component.appdetail;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.activity.AppDetailActivityV5;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class r extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Context f941a;
    final /* synthetic */ HorizonImageListView b;

    r(HorizonImageListView horizonImageListView, Context context) {
        this.b = horizonImageListView;
        this.f941a = context;
    }

    public void onTMAClick(View view) {
        String str = (String) ((ImageView) view).getTag();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.b.c.size()) {
                String str2 = (String) this.b.c.get(i2);
                if (!str2.equals(str) || this.b.d == null) {
                    i = i2 + 1;
                } else {
                    this.b.d.onHorizonImageListViewImageClick(i2, str2, view);
                    return;
                }
            } else {
                return;
            }
        }
    }

    public STInfoV2 getStInfo() {
        if (!(this.f941a instanceof AppDetailActivityV5)) {
            return null;
        }
        STInfoV2 i = ((AppDetailActivityV5) this.f941a).i();
        if (this.clickViewId >= 0) {
            i.slotId = a.a("07", this.clickViewId - 1);
        } else if (this.clickViewId == -1) {
            i.slotId = a.a("16", 0);
        }
        i.actionId = 200;
        return i;
    }
}
