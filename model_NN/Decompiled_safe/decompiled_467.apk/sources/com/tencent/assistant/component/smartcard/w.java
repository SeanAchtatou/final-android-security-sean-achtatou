package com.tencent.assistant.component.smartcard;

import android.content.Intent;
import android.view.View;
import com.tencent.assistant.component.appdetail.CommentDetailTabView;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistantv2.activity.AppDetailActivityV5;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
class w extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SimpleAppModel f1162a;
    final /* synthetic */ int b;
    final /* synthetic */ SearchSmartCardAppListItem c;

    w(SearchSmartCardAppListItem searchSmartCardAppListItem, SimpleAppModel simpleAppModel, int i) {
        this.c = searchSmartCardAppListItem;
        this.f1162a = simpleAppModel;
        this.b = i;
    }

    public void onTMAClick(View view) {
        Intent intent = new Intent(this.c.f1115a, AppDetailActivityV5.class);
        intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, this.c.a(0));
        intent.putExtra("statInfo", new StatInfo(this.f1162a.b, this.c.a(0), 0, Constants.STR_EMPTY, this.c.f));
        intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, this.c.a(0));
        intent.putExtra(CommentDetailTabView.PARAMS_SIMPLE_MODEL_INFO, this.f1162a);
        this.c.f1115a.startActivity(intent);
    }

    public STInfoV2 getStInfo() {
        STInfoV2 a2 = this.c.a(this.c.c(this.b), 200);
        if (a2 != null) {
            a2.updateWithSimpleAppModel(this.f1162a);
            a2.searchId = this.c.f;
        }
        return a2;
    }
}
