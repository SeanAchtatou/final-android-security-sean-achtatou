package com.tencent.assistant.component.listener;

import android.view.View;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
public abstract class OnTMASwitchButtonClickListener {
    public abstract void onSwitchButtonClick(View view, boolean z);

    public void userActionReport(View view) {
        k.a(getStInfo(view));
    }

    public STInfoV2 getStInfo(View view) {
        return null;
    }
}
