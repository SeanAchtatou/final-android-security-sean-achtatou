package com.tencent.assistant.component.smartcard;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.utils.ct;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.component.appdetail.TXDwonloadProcessBar;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
public class NormalSmartCardAppTinyNode extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    TXImageView f1118a;
    TextView b;
    TextView c;
    DownloadButton d;
    TXDwonloadProcessBar e;

    public NormalSmartCardAppTinyNode(Context context) {
        this(context, null);
    }

    public NormalSmartCardAppTinyNode(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a();
    }

    private void a() {
        LayoutInflater.from(getContext()).inflate((int) R.layout.smartcard_app_item_for_pic_merge, this);
        this.f1118a = (TXImageView) findViewById(R.id.icon);
        this.b = (TextView) findViewById(R.id.name);
        this.c = (TextView) findViewById(R.id.down_times);
        this.e = (TXDwonloadProcessBar) findViewById(R.id.progress);
        this.d = (DownloadButton) findViewById(R.id.btn);
        setOrientation(1);
        setGravity(1);
    }

    public void setData(SimpleAppModel simpleAppModel, STInfoV2 sTInfoV2, int i) {
        this.f1118a.updateImageView(simpleAppModel.e, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
        this.b.setText(simpleAppModel.d);
        this.c.setText(ct.a(simpleAppModel.p, 0));
        this.e.a(simpleAppModel, new View[]{this.c});
        this.d.a(simpleAppModel);
        this.d.a(sTInfoV2);
        this.f1118a.setTag(simpleAppModel.q());
        setOnClickListener(new c(this, i, simpleAppModel, sTInfoV2));
    }
}
