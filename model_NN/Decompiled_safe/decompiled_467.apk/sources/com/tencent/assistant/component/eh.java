package com.tencent.assistant.component;

import com.tencent.assistant.component.SideBar;

/* compiled from: ProGuard */
class eh implements SideBar.OnTouchingLetterChangedListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ UserAppListView f1043a;

    eh(UserAppListView userAppListView) {
        this.f1043a = userAppListView;
    }

    public void onTouchingLetterChanged(String str) {
        int a2 = this.f1043a.mAdapter.a(str.charAt(0));
        if (a2 != -1) {
            this.f1043a.mListView.post(new ei(this, a2));
        }
    }
}
