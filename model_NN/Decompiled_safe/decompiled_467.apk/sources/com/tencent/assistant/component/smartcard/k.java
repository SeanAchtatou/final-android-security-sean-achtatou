package com.tencent.assistant.component.smartcard;

import com.tencent.assistantv2.component.o;

/* compiled from: ProGuard */
class k implements o {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ long f1150a;
    final /* synthetic */ NormalSmartCardSelfUpdateItem b;

    k(NormalSmartCardSelfUpdateItem normalSmartCardSelfUpdateItem, long j) {
        this.b = normalSmartCardSelfUpdateItem;
        this.f1150a = j;
    }

    public void a(boolean z) {
        Long l = (Long) this.b.r.getTag();
        if (l != null && this.f1150a == l.longValue()) {
            if (!z) {
                this.b.p.setVisibility(8);
                this.b.q.setOnClickListener(null);
                return;
            }
            this.b.p.setVisibility(0);
        }
    }
}
