package com.tencent.assistant.component.appdetail;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.CommentDetailView;
import com.tencent.assistant.component.appdetail.InnerScrollView;
import com.tencent.assistant.component.invalidater.ViewPageScrollListener;
import com.tencent.assistant.module.bf;
import com.tencent.assistant.module.ek;
import com.tencent.assistant.protocol.jce.CommentTagInfo;
import com.tencent.assistantv2.component.appdetail.b;
import java.util.Map;

/* compiled from: ProGuard */
public class CommentDetailTabView extends LinearLayout implements IAppdetailView, InnerScrollView.IOnScrolledToPageBottom {
    public static final String PARAMS_ALLTAG_LIST = "alltaglist";
    public static final String PARAMS_APK_ID = "apkId";
    public static final String PARAMS_COMMENT_COUNT = "count";
    public static final String PARAMS_RATING_INFO = "ratingInfo";
    public static final String PARAMS_REPLY_ID = "replyId";
    public static final String PARAMS_SIMPLE_MODEL_INFO = "simpleModeInfo";
    public static final String PARAMS_VERSION_CODE = "versionCode";

    /* renamed from: a  reason: collision with root package name */
    private InnerScrollView f902a;
    private CommentDetailView b;

    public CommentDetailTabView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    public CommentDetailTabView(Context context, CommentDetailView.CommentSucceedListener commentSucceedListener) {
        super(context);
        a(context);
        this.b.setCommentListener(commentSucceedListener);
    }

    private void a(Context context) {
        View inflate = LayoutInflater.from(context).inflate((int) R.layout.commentdetail_view_layout, this);
        this.f902a = (InnerScrollView) inflate.findViewById(R.id.inner_scrollview);
        this.f902a.setOnScrolledToPageBottom(this);
        this.b = (CommentDetailView) inflate.findViewById(R.id.detail_view);
    }

    public IInnerScrollListener getInnerScrollView() {
        return this.f902a;
    }

    public void initDetailView(Map<String, Object> map) {
        this.b.initDetailView(map);
        refreshData();
    }

    public void hideBar() {
        if (this.b != null) {
            this.b.hideBar();
        }
    }

    public void onResume() {
        this.b.onResume();
    }

    public void onPause() {
        this.b.onPause();
    }

    public void onDestroy() {
        this.b.onDestroy();
    }

    public void refreshData() {
        this.b.refreshFirstPageData(null);
    }

    public void setPagerHeight(int i) {
        this.b.setPagerHeight(i);
    }

    public bf getCommentEngine() {
        return this.b.getCommentEngine();
    }

    public ek getModifyAppCommentEngine() {
        return this.b.getModifyAppCommentEngine();
    }

    public void onNotifyScrollToBottom() {
        if (1 == this.b.getFooterViewState()) {
            this.b.requestNextPageData();
        }
    }

    public void clickCommentTag(CommentTagInfo commentTagInfo) {
        this.b.clickCommentTag(commentTagInfo);
    }

    public void setPageChangeListener(ViewPageScrollListener viewPageScrollListener) {
        this.b.setPageChangeListener(viewPageScrollListener);
    }

    public void setPagerHeightListener(b bVar) {
        if (this.b != null) {
            this.b.setPagerHeightListener(bVar);
        }
    }
}
