package com.tencent.assistant.component;

import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.PhotoBackupNewActivity;

/* compiled from: ProGuard */
class bl implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PhotoBackupBottomView f967a;

    bl(PhotoBackupBottomView photoBackupBottomView) {
        this.f967a = photoBackupBottomView;
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_backup /*2131166217*/:
                if (this.f967a.activity != null && (this.f967a.activity instanceof PhotoBackupNewActivity)) {
                    ((PhotoBackupNewActivity) this.f967a.activity).w();
                    return;
                }
                return;
            default:
                return;
        }
    }
}
