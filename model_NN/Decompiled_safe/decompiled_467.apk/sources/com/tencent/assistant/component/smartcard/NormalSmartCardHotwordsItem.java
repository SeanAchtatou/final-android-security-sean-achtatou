package com.tencent.assistant.component.smartcard;

import android.content.Context;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.model.a.h;
import com.tencent.assistant.model.a.i;
import com.tencent.assistantv2.component.HotwordsCardView;

/* compiled from: ProGuard */
public class NormalSmartCardHotwordsItem extends NormalSmartcardBaseItem {
    private TextView f;
    private HotwordsCardView i;

    public NormalSmartCardHotwordsItem(Context context, i iVar, SmartcardListener smartcardListener, IViewInvalidater iViewInvalidater) {
        super(context, iVar, smartcardListener, iViewInvalidater);
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.b.inflate((int) R.layout.smartcard_hotwords, this);
        this.f = (TextView) findViewById(R.id.title);
        this.i = (HotwordsCardView) findViewById(R.id.hotwords_view);
        f();
    }

    private void f() {
        if (this.smartcardModel instanceof h) {
            h hVar = (h) this.smartcardModel;
            this.f.setText(hVar.k);
            this.i.a(a(contextToPageId(this.f1115a)));
            this.i.a(2, this.f1115a.getResources().getString(R.string.hotwords_card_more_text), hVar.n, hVar.a());
        }
    }

    /* access modifiers changed from: protected */
    public void b() {
        f();
    }
}
