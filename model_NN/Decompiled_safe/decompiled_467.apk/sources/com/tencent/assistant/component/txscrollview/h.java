package com.tencent.assistant.component.txscrollview;

import android.graphics.Bitmap;
import android.text.TextUtils;
import com.tencent.assistant.manager.cq;

/* compiled from: ProGuard */
class h implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f1204a;
    final /* synthetic */ Bitmap b;
    final /* synthetic */ g c;

    h(g gVar, String str, Bitmap bitmap) {
        this.c = gVar;
        this.f1204a = str;
        this.b = bitmap;
    }

    public void run() {
        try {
            if (TextUtils.isEmpty(this.f1204a) || !this.f1204a.equals(this.c.f1203a.mImageUrlString) || this.b == null || this.b.isRecycled()) {
                this.c.f1203a.setImageResource(this.c.f1203a.defaultResId);
                boolean unused = this.c.f1203a.mIsLoadFinish = true;
                this.c.f1203a.onImageLoadFinishCallListener(this.b);
            }
            this.c.f1203a.setImageBitmap(this.b);
            boolean unused2 = this.c.f1203a.mIsLoadFinish = true;
            this.c.f1203a.onImageLoadFinishCallListener(this.b);
        } catch (Throwable th) {
            cq.a().b();
        }
    }
}
