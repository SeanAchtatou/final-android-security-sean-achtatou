package com.tencent.assistant.component.smartcard;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.model.a.i;
import com.tencent.assistant.model.a.t;
import com.tencent.assistant.model.a.u;
import com.tencent.assistant.utils.df;
import com.tencent.assistantv2.st.b.d;
import com.tencent.assistantv2.st.page.STInfoV2;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class SmartHorizontalCard extends NormalSmartcardLibaoItem {
    private LinearLayout f;
    private List<t> i;

    public SmartHorizontalCard(Context context) {
        this(context, null);
    }

    public SmartHorizontalCard(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.i = new ArrayList();
    }

    public SmartHorizontalCard(Context context, i iVar, SmartcardListener smartcardListener, IViewInvalidater iViewInvalidater) {
        super(context, iVar, smartcardListener, iViewInvalidater);
        this.i = new ArrayList();
    }

    public void setDataSource(List<t> list) {
        this.i = list;
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.b.inflate((int) R.layout.smartcard_horizontal, this);
        setMinimumHeight(df.a(getContext(), 123.0f));
        this.f = (LinearLayout) findViewById(R.id.content);
        f();
    }

    /* access modifiers changed from: protected */
    public void b() {
        f();
    }

    private void f() {
        u uVar;
        if (this.smartcardModel instanceof u) {
            uVar = (u) this.smartcardModel;
        } else {
            uVar = null;
        }
        if (uVar != null) {
            setDataSource(uVar.f1654a);
            this.f.removeAllViews();
            for (t next : this.i) {
                SmartSquareAppItem smartSquareAppItem = new SmartSquareAppItem(getContext());
                smartSquareAppItem.setData(next, a(uVar, this.i.indexOf(next)));
                this.f.addView(smartSquareAppItem);
            }
        }
    }

    private STInfoV2 a(u uVar, int i2) {
        STInfoV2 a2 = a(com.tencent.assistant.st.i.a(uVar, i2), 100);
        if (!(a2 == null || uVar == null || uVar.a() == null || uVar.a().size() <= i2)) {
            a2.updateWithSimpleAppModel(uVar.a().get(i2).f1653a);
        }
        if (this.e == null) {
            this.e = new d();
        }
        this.e.a(a2);
        return a2;
    }
}
