package com.tencent.assistant.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class CommentProgressBar extends RelativeLayout {
    private static int totalNum = 5;
    private int[] barId = {R.id.bar1, R.id.bar2, R.id.bar3, R.id.bar4, R.id.bar5};
    private int[] barStyle = {R.drawable.comment_progressbar1, R.drawable.comment_progressbar2, R.drawable.comment_progressbar3, R.drawable.comment_progressbar4, R.drawable.comment_progressbar5};
    private ProgressBar[] bars = new ProgressBar[totalNum];
    private int[] countId = {R.id.count1, R.id.count2, R.id.count3, R.id.count4, R.id.count5};
    private TextView[] counts = new TextView[totalNum];
    private boolean mHasSetProgressDrawable = false;
    private LayoutInflater mInflater;
    private ArrayList<Integer> mNumbers = new ArrayList<>();
    private ArrayList<String> textCounts = new ArrayList<>();
    private int[] textViewId = {R.id.text1, R.id.text2, R.id.text3, R.id.text4, R.id.text5};
    private TextView[] textViews = new TextView[totalNum];

    public ArrayList<String> getTextCounts() {
        return this.textCounts;
    }

    public void setTextCounts(ArrayList<String> arrayList) {
        this.textCounts = arrayList;
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < totalNum) {
                this.counts[i2].setText(arrayList.get(i2) + "%");
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    public ArrayList<Integer> getmNumbers() {
        return this.mNumbers;
    }

    public void setmNumbers(ArrayList<Integer> arrayList) {
        this.mNumbers = arrayList;
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < totalNum) {
                if (arrayList != null && arrayList.size() > 0 && i2 < arrayList.size()) {
                    this.bars[i2].setProgress(arrayList.get(i2).intValue());
                }
                i = i2 + 1;
            } else {
                this.mHasSetProgressDrawable = true;
                return;
            }
        }
    }

    public CommentProgressBar(Context context) {
        super(context);
        init(context);
    }

    public CommentProgressBar(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        init(context);
    }

    public CommentProgressBar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init(context);
    }

    private void init(Context context) {
        this.mInflater = LayoutInflater.from(context);
        View inflate = this.mInflater.inflate((int) R.layout.comment_detail_progress_layout, this);
        for (int i = 0; i < totalNum; i++) {
            this.bars[i] = (ProgressBar) inflate.findViewById(this.barId[i]);
        }
        for (int i2 = 0; i2 < totalNum; i2++) {
            this.counts[i2] = (TextView) inflate.findViewById(this.countId[i2]);
        }
    }
}
