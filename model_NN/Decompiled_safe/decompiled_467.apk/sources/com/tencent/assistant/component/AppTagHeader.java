package com.tencent.assistant.component;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.protocol.jce.TagGroup;
import com.tencent.assistant.utils.ct;
import com.tencent.assistant.utils.df;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class AppTagHeader extends LinearLayout {
    private static final int COLUMN_TYPE_FOUR = 2;
    private static final int COLUMN_TYPE_NONE = 0;
    private static final int COLUMN_TYPE_THREE = 1;
    private static final int MAX_BUTTONS = 12;
    private static final String TMA_ST_SOFTWEARE_CATEGORY_SUB_SLOT = "03_";
    private int mColumnType = 0;
    private Context mContext;
    /* access modifiers changed from: private */
    public int mCurrentSelected = 0;
    private List<TagGroup> mFlagStrArray = new ArrayList();
    private LinearLayout[] mRowLayouts;
    /* access modifiers changed from: private */
    public List<Button> mTagArray = new ArrayList();
    private LinearLayout mTagHeader;
    /* access modifiers changed from: private */
    public View.OnClickListener tagClickListener;
    private boolean tagExisted = false;
    private ImageView topPaddingView;

    public AppTagHeader(Context context) {
        super(context);
        this.mContext = context;
        setOrientation(1);
        this.mTagHeader = new LinearLayout(getContext());
        int a2 = df.a(this.mContext, 3.5f);
        this.mTagHeader.setPadding(a2, df.a(this.mContext, 2.0f), a2, 0);
        addView(this.mTagHeader, new LinearLayout.LayoutParams(-1, -2));
        this.topPaddingView = new ImageView(getContext());
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, df.a(getContext(), 8.0f));
        this.topPaddingView.setBackgroundColor(getResources().getColor(17170445));
        addView(this.topPaddingView, layoutParams);
        setTag(0);
    }

    public void setTagData(List<TagGroup> list, long j) {
        int i = 0;
        if (!list.isEmpty()) {
            this.tagExisted = true;
            this.mFlagStrArray.clear();
            this.mFlagStrArray.add(new TagGroup(0, this.mContext.getResources().getString(R.string.all), null, 0));
            this.mFlagStrArray.addAll(list);
            Iterator<TagGroup> it = this.mFlagStrArray.iterator();
            while (true) {
                if (it.hasNext()) {
                    TagGroup next = it.next();
                    if (next != null && next.a() == j) {
                        i = this.mFlagStrArray.indexOf(next);
                        break;
                    }
                } else {
                    break;
                }
            }
            setTag(i);
        }
    }

    private void setTag(int i) {
        setRowType();
        setButton();
        setRow();
        selectTag(i);
    }

    private void setButton() {
        int size;
        o oVar = new o(this);
        int i = 0;
        while (true) {
            int i2 = i;
            if (this.mFlagStrArray.size() > 12) {
                size = 12;
            } else {
                size = this.mFlagStrArray.size();
            }
            if (i2 < size) {
                if (this.mFlagStrArray.get(i2) != null) {
                    Button button = new Button(this.mContext);
                    button.setText(this.mFlagStrArray.get(i2).b());
                    button.setTag(this.mFlagStrArray.get(i2));
                    button.setBackgroundDrawable(this.mContext.getResources().getDrawable(R.drawable.tag_btn_bg_selector));
                    button.setTextColor(this.mContext.getResources().getColor(R.drawable.tag_btn_txt_selector));
                    button.setTextSize(2, 16.0f);
                    button.setGravity(17);
                    button.setMaxEms(4);
                    button.setSingleLine();
                    button.setEllipsize(TextUtils.TruncateAt.END);
                    button.setOnClickListener(oVar);
                    button.setTag(R.id.tma_st_slot_tag, TMA_ST_SOFTWEARE_CATEGORY_SUB_SLOT + ct.a(i2 + 1));
                    button.setTag(R.id.category_detail_btn_index, Integer.valueOf(i2));
                    this.mTagArray.add(button);
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    private void setRowType() {
        int i = 12;
        this.mColumnType = 0;
        if (this.mFlagStrArray != null) {
            if (this.mFlagStrArray.size() <= 12) {
                i = this.mFlagStrArray.size();
            }
            if (i <= 0) {
                return;
            }
            if (i % 4 == 0) {
                this.mColumnType = 2;
            } else if (i % 3 == 0) {
                this.mColumnType = 1;
            } else if (i % 4 > i % 3) {
                this.mColumnType = 2;
            } else {
                this.mColumnType = 1;
            }
        }
    }

    private void setRow() {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, df.a(this.mContext, 33.0f), 1.0f);
        int a2 = df.a(this.mContext, 3.5f);
        layoutParams.leftMargin = a2;
        layoutParams.rightMargin = a2;
        switch (this.mColumnType) {
            case 0:
                this.mTagHeader.setVisibility(8);
                return;
            case 1:
                initRowViews(3);
                return;
            case 2:
                initRowViews(4);
                return;
            default:
                return;
        }
    }

    private void initRowViews(int i) {
        int i2 = 12;
        if (this.mTagArray == null || i <= 0) {
            this.mTagHeader.setVisibility(8);
            return;
        }
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, df.a(this.mContext, 33.0f), 1.0f);
        int a2 = df.a(this.mContext, 3.5f);
        layoutParams.leftMargin = a2;
        layoutParams.rightMargin = a2;
        this.mTagHeader.setOrientation(1);
        if (this.mTagArray.size() <= 12) {
            i2 = this.mTagArray.size();
        }
        int max = Math.max(0, ((i2 + i) - 1) / i);
        this.mRowLayouts = new LinearLayout[max];
        this.mTagHeader.setOrientation(1);
        for (int i3 = 0; i3 < max; i3++) {
            this.mRowLayouts[i3] = new LinearLayout(this.mContext);
            this.mRowLayouts[i3].setWeightSum((float) i);
            this.mRowLayouts[i3].setOrientation(0);
            if (max == 1 || i3 > 0) {
                layoutParams.topMargin = df.a(this.mContext, 7.0f);
            }
            for (int i4 = 0; i4 < i; i4++) {
                int i5 = (i3 * i) + i4;
                if (i5 < this.mTagArray.size()) {
                    this.mRowLayouts[i3].addView(this.mTagArray.get(i5), layoutParams);
                } else {
                    this.mRowLayouts[i3].addView(new View(this.mContext), layoutParams);
                }
            }
            this.mTagHeader.addView(this.mRowLayouts[i3], new LinearLayout.LayoutParams(-1, -2));
        }
        this.mTagHeader.setVisibility(0);
    }

    public void selectTag(int i) {
        if (!this.mTagArray.isEmpty() && this.mTagArray.get(this.mCurrentSelected) != null) {
            if (this.mCurrentSelected != i) {
                this.mTagArray.get(this.mCurrentSelected).setSelected(false);
                this.mTagArray.get(this.mCurrentSelected).setTextColor(this.mContext.getResources().getColor(R.color.tag_btn_unselected_txt));
                this.mCurrentSelected = i;
                this.mTagArray.get(this.mCurrentSelected).setSelected(true);
                this.mTagArray.get(this.mCurrentSelected).setTextColor(this.mContext.getResources().getColor(R.color.tag_btn_selected_txt));
            } else if (!this.mTagArray.get(this.mCurrentSelected).isSelected()) {
                this.mTagArray.get(this.mCurrentSelected).setSelected(true);
                this.mTagArray.get(this.mCurrentSelected).setTextColor(this.mContext.getResources().getColor(R.color.tag_btn_selected_txt));
            }
        }
    }

    public boolean isTagExisted() {
        return this.tagExisted;
    }

    public void setTagClickListener(View.OnClickListener onClickListener) {
        this.tagClickListener = onClickListener;
    }
}
