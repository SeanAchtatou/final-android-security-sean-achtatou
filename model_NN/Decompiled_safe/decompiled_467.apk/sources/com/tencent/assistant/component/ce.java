package com.tencent.assistant.component;

import android.app.Activity;
import android.text.Editable;
import android.text.Selection;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.login.a.a;
import com.tencent.assistant.manager.be;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import java.util.Date;

/* compiled from: ProGuard */
class ce extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PopViewDialog f987a;

    ce(PopViewDialog popViewDialog) {
        this.f987a = popViewDialog;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.g.e.a(android.app.Activity, com.tencent.assistant.model.ShareAppModel, boolean):void
     arg types: [android.app.Activity, com.tencent.assistant.model.ShareAppModel, int]
     candidates:
      com.tencent.assistant.g.e.a(android.app.Activity, com.tencent.assistant.model.ShareBaseModel, boolean):void
      com.tencent.assistant.g.e.a(android.content.Context, com.tencent.assistant.model.ShareAppModel, boolean):void
      com.tencent.assistant.g.e.a(android.content.Context, com.tencent.assistant.model.ShareBaseModel, boolean):void
      com.tencent.assistant.g.e.a(android.app.Activity, com.tencent.assistant.model.ShareAppModel, boolean):void */
    public void onTMAClick(View view) {
        String str;
        int i;
        switch (view.getId()) {
            case R.id.cancel /*2131165467*/:
                this.f987a.hiddenKeyboard();
                if (this.f987a.loginProxy.j()) {
                    k.a(new STInfoV2(STConst.ST_PAGE_COMMENT_DIALOG_LOGIN, "03_001", STConst.ST_PAGE_APP_DETAIL_COMMENT, STConst.ST_DEFAULT_SLOT, 200));
                } else {
                    k.a(new STInfoV2(STConst.ST_PAGE_COMMENT_DIALOG_NOT_LOGIN, "03_001", STConst.ST_PAGE_APP_DETAIL_COMMENT, STConst.ST_DEFAULT_SLOT, 200));
                }
                this.f987a.dismiss();
                return;
            case R.id.submit /*2131165815*/:
                this.f987a.hiddenKeyboard();
                int round = Math.round(this.f987a.ratingView.getRating());
                if (round <= 0) {
                    this.f987a.tipsText.setText((int) R.string.comment_pop_star_text);
                    this.f987a.tipsText.setTextColor(-65536);
                    return;
                } else if (!this.f987a.hasCommented || round != this.f987a.oldScore || !this.f987a.getContentView().getText().toString().equals(this.f987a.oldContent)) {
                    String obj = this.f987a.getContentView().getText().toString();
                    if (!TextUtils.isEmpty(obj)) {
                        if (obj.length() > 100) {
                            this.f987a.errorTips.setVisibility(0);
                            this.f987a.errorTips.setText((int) R.string.comment_expendcount_error);
                            return;
                        }
                        String str2 = null;
                        CharSequence text = this.f987a.errorTips.getText();
                        if (text != null) {
                            str2 = text.toString();
                        }
                        String string = this.f987a.mContext.getString(R.string.comment_expendcount_error);
                        if (this.f987a.errorTips.getVisibility() == 0 && !TextUtils.isEmpty(str2) && str2.equals(string)) {
                            this.f987a.errorTips.setVisibility(8);
                        }
                    }
                    String str3 = "03_002";
                    if (!this.f987a.loginProxy.j()) {
                        this.f987a.showLoginDialog();
                        k.a(new STInfoV2(STConst.ST_PAGE_COMMENT_DIALOG_NOT_LOGIN, str3, STConst.ST_PAGE_APP_DETAIL_COMMENT, STConst.ST_DEFAULT_SLOT, 200));
                        return;
                    }
                    if (TextUtils.isEmpty(this.f987a.getContentView().getText().toString())) {
                        this.f987a.mDetail.b(Constants.STR_EMPTY);
                        this.f987a.getContentView().setText(Constants.STR_EMPTY);
                    } else {
                        this.f987a.mDetail.b(this.f987a.getContentView().getText().toString());
                    }
                    this.f987a.mDetail.a(new Date().getTime() / 1000);
                    this.f987a.mDetail.a(this.f987a.loginProxy.q());
                    this.f987a.mDetail.a(round);
                    if (!be.a().c(this.f987a.pkgName) || !be.a().d(this.f987a.pkgName)) {
                        LocalApkInfo localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(this.f987a.pkgName);
                        if (localApkInfo != null) {
                            int i2 = localApkInfo.mVersionCode;
                            str = localApkInfo.mVersionName;
                            i = i2;
                        } else {
                            return;
                        }
                    } else if (be.a().a(this.f987a.pkgName) != null) {
                        int access$2000 = this.f987a.versionCode;
                        str = this.f987a.versionName;
                        i = access$2000;
                    } else {
                        return;
                    }
                    if (i > 0) {
                        this.f987a.mDetail.b(i);
                        this.f987a.mDetail.c(a.f().f1456a);
                        if (this.f987a.saveListener != null) {
                            this.f987a.saveListener.onSavedParams(this.f987a.contentView.getText().toString(), round, i, this.f987a.shareToQZ || this.f987a.shareToWX);
                        }
                        if (this.f987a.versionCode != this.f987a.oldVersion) {
                            if (i == this.f987a.versionCode) {
                                this.f987a.commEngine.a(this.f987a.mDetail, this.f987a.appId, this.f987a.apkId, str, this.f987a.appName, this.f987a.pkgName);
                            } else {
                                this.f987a.commEngine.a(this.f987a.mDetail, this.f987a.appId, -1, str, this.f987a.appName, this.f987a.pkgName);
                            }
                        }
                        if ((this.f987a.shareToQZ || this.f987a.shareToWX) && this.f987a.shareAppModel != null) {
                            if (this.f987a.shareToQZ) {
                                this.f987a.shareAppModel.b = this.f987a.getContentView().getText().toString();
                                str3 = "04_001";
                                this.f987a.shareEngine.a(false);
                                this.f987a.shareEngine.a((Activity) this.f987a.mContext, this.f987a.shareAppModel, false);
                            } else if (this.f987a.shareToWX && this.f987a.loginProxy.v()) {
                                this.f987a.shareEngine.a(false);
                                this.f987a.shareAppModel.m = this.f987a.getContentView().getText().toString();
                                str3 = "04_002";
                                this.f987a.shareEngine.a(this.f987a.mContext, this.f987a.shareAppModel, true, true);
                            }
                        }
                        k.a(new STInfoV2(STConst.ST_PAGE_COMMENT_DIALOG_LOGIN, str3, STConst.ST_PAGE_APP_DETAIL_COMMENT, STConst.ST_DEFAULT_SLOT, 200));
                        this.f987a.dismiss();
                        return;
                    }
                    return;
                } else {
                    this.f987a.dismiss();
                    return;
                }
            default:
                view.setSelected(true);
                String str4 = ((Object) ((TextView) view).getText()) + "。";
                int selectionEnd = this.f987a.getContentView().getSelectionEnd();
                String obj2 = this.f987a.getContentView().getText().subSequence(0, selectionEnd).toString();
                if (obj2.length() < 100) {
                    this.f987a.getContentView().setText(obj2 + str4 + this.f987a.getContentView().getText().subSequence(selectionEnd, this.f987a.getContentView().getText().length()).toString());
                    int length = (obj2 + str4).length();
                    Editable text2 = this.f987a.contentView.getText();
                    if (length >= text2.length()) {
                        length = text2.length();
                    }
                    Selection.setSelection(text2, length);
                    return;
                }
                return;
        }
    }

    public STInfoV2 getStInfo() {
        STInfoV2 access$200 = this.f987a.buildFloatingSTInfo();
        if (access$200 != null) {
            if (this.clickViewId == R.id.cancel) {
                access$200.slotId = com.tencent.assistantv2.st.page.a.a(this.f987a.getColumn(), "003");
            } else if (this.clickViewId == R.id.submit) {
                access$200.slotId = com.tencent.assistantv2.st.page.a.a(this.f987a.getColumn(), "004");
            }
            access$200.actionId = 200;
        }
        return access$200;
    }
}
