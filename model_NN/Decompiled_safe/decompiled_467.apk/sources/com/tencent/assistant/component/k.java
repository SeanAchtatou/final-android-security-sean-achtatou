package com.tencent.assistant.component;

import com.tencent.assistant.component.invalidater.ViewInvalidateMessage;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessageHandler;
import java.util.List;
import java.util.Map;

/* compiled from: ProGuard */
class k extends ViewInvalidateMessageHandler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppListView f1070a;

    k(AppListView appListView) {
        this.f1070a = appListView;
    }

    public void handleMessage(ViewInvalidateMessage viewInvalidateMessage) {
        if (viewInvalidateMessage.what == 1) {
            int i = viewInvalidateMessage.arg1;
            int i2 = viewInvalidateMessage.arg2;
            Map map = (Map) viewInvalidateMessage.params;
            boolean booleanValue = ((Boolean) map.get("isFirstPage")).booleanValue();
            Object obj = map.get("key_data");
            if (!(obj == null || this.f1070a.mAdapter == null)) {
                this.f1070a.mAdapter.a(booleanValue, (List) obj);
            }
            this.f1070a.onAppListLoadedFinishedHandler(i2, i, booleanValue);
            return;
        }
        this.f1070a.mListener.onNetworkNoError();
        this.f1070a.mAdapter.notifyDataSetChanged();
    }
}
