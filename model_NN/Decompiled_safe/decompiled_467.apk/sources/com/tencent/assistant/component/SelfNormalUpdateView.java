package com.tencent.assistant.component;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.text.Html;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.download.SimpleDownloadInfo;
import com.tencent.assistant.download.a;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.SelfUpdateManager;
import com.tencent.assistant.utils.FunctionUtils;
import com.tencent.assistant.utils.bt;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public class SelfNormalUpdateView extends RelativeLayout {
    private static final String TMA_ST_BTN_SLOT_CANCEL = "03_001";
    private static final String TMA_ST_BTN_SLOT_UPDATE = "03_002";
    public AstApp mApp;
    public SimpleDownloadInfo mDownloadInfo;
    private Button mIgnoreBtn;
    private View mRootView;
    private TXImageView mSelfUpdateBanner;
    private Button mUpdateBtn;
    public SelfUpdateManager.SelfUpdateInfo mUpdateInfo;
    private UpdateListener mUpdateListener;
    private TextView mVersionFeatureText;
    private TextView mVersionTips1;
    private TextView mVersionTips2;

    /* compiled from: ProGuard */
    public interface UpdateListener {
        void dissmssDialog();
    }

    public SelfNormalUpdateView(Context context) {
        this(context, null);
    }

    public SelfNormalUpdateView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.mApp = AstApp.i();
        initLayout();
    }

    private void initLayout() {
        this.mRootView = LayoutInflater.from(getContext()).inflate((int) R.layout.dialog_selfupdate_normal, this);
        this.mSelfUpdateBanner = (TXImageView) this.mRootView.findViewById(R.id.self_update_banner);
        this.mVersionFeatureText = (TextView) this.mRootView.findViewById(R.id.msg_versionfeature);
        this.mIgnoreBtn = (Button) this.mRootView.findViewById(R.id.btn_ignore);
        this.mUpdateBtn = (Button) this.mRootView.findViewById(R.id.btn_update);
        this.mVersionTips1 = (TextView) this.mRootView.findViewById(R.id.title_tip_1);
        this.mVersionTips2 = (TextView) this.mRootView.findViewById(R.id.title_tip_2);
    }

    public void initView() {
        this.mUpdateInfo = SelfUpdateManager.a().d();
        if (this.mUpdateInfo != null) {
            if (!TextUtils.isEmpty(this.mUpdateInfo.h)) {
                this.mVersionFeatureText.setText(String.format(getResources().getString(R.string.dialog_update_feature), this.mUpdateInfo.h));
            }
            if (SelfUpdateManager.a().a(this.mUpdateInfo.e)) {
                this.mVersionTips1.setVisibility(0);
                this.mVersionTips1.setText((int) R.string.selfupdate_apk_file_ready);
                this.mUpdateBtn.setText((int) R.string.selfupdate_update_right_now);
                this.mUpdateBtn.setTextColor(getResources().getColor(R.color.appdetail_btn_text_color_to_install));
                this.mUpdateBtn.setBackgroundResource(R.drawable.appdetail_bar_btn_to_install_selector_v5);
            } else {
                this.mVersionTips1.setVisibility(8);
                this.mUpdateBtn.setText(Constants.STR_EMPTY);
                this.mUpdateBtn.append(new SpannableString(getResources().getString(R.string.download)));
                SpannableString spannableString = new SpannableString(" (" + bt.b(this.mUpdateInfo.a()) + "B)");
                spannableString.setSpan(new AbsoluteSizeSpan(15, true), 0, spannableString.length(), 17);
                this.mUpdateBtn.append(spannableString);
            }
            try {
                this.mVersionTips2.setText(Html.fromHtml(this.mUpdateInfo.q));
            } catch (Exception e) {
            }
            this.mSelfUpdateBanner.setImageBitmap(FunctionUtils.a(BitmapFactory.decodeResource(this.mApp.getResources(), R.drawable.self_update_banner)));
            this.mIgnoreBtn.setOnClickListener(new di(this));
            this.mUpdateBtn.setOnClickListener(new dj(this));
        }
    }

    public void setUpdateListener(UpdateListener updateListener) {
        this.mUpdateListener = updateListener;
    }

    /* access modifiers changed from: private */
    public void ignoreUpdate() {
        m.a().b("update_newest_versioncode", Integer.valueOf(this.mUpdateInfo.e));
        m.a().b("update_newest_versionname", this.mUpdateInfo.f);
        SelfUpdateManager.a().n().a();
        if (this.mUpdateListener != null) {
            this.mUpdateListener.dissmssDialog();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.download.a.a(com.tencent.assistant.download.DownloadInfo, boolean):boolean
     arg types: [com.tencent.assistant.download.DownloadInfo, int]
     candidates:
      com.tencent.assistant.download.a.a(com.tencent.assistant.download.a, java.util.List):java.util.List
      com.tencent.assistant.download.a.a(java.util.ArrayList<com.tencent.assistant.download.DownloadInfo>, boolean):void
      com.tencent.assistant.download.a.a(long, long):boolean
      com.tencent.assistant.download.a.a(com.tencent.assistant.download.a, boolean):boolean
      com.tencent.assistant.download.a.a(java.lang.String, int):boolean
      com.tencent.assistant.download.a.a(java.util.List<com.tencent.assistant.model.SimpleAppModel>, com.tencent.assistantv2.st.model.StatInfo):int
      com.tencent.assistant.download.a.a(com.tencent.assistant.download.DownloadInfo, com.tencent.assistant.download.SimpleDownloadInfo$UIType):void
      com.tencent.assistant.download.a.a(com.tencent.assistant.model.SimpleAppModel, com.tencent.assistantv2.st.model.StatInfo):void
      com.tencent.assistant.download.a.a(java.lang.String, java.lang.String):void
      com.tencent.assistant.download.a.a(java.lang.String, boolean):boolean
      com.tencent.assistant.download.a.a(com.tencent.assistant.download.DownloadInfo, boolean):boolean */
    /* access modifiers changed from: private */
    public void update() {
        SelfUpdateManager.a().n().a();
        if (!SelfUpdateManager.a().a(this.mUpdateInfo.e)) {
            startDownload();
        } else if (!a.a().a(SelfUpdateManager.a().b(SelfUpdateManager.SelfUpdateType.NORMAL), false)) {
            startDownload();
        }
        updateUI();
    }

    private void startDownload() {
        SelfUpdateManager.a().a(SelfUpdateManager.SelfUpdateType.NORMAL);
    }

    /* access modifiers changed from: protected */
    public void updateUI() {
        if (this.mUpdateListener != null) {
            this.mUpdateListener.dissmssDialog();
        }
    }

    public void onResume() {
    }

    public void onDestroy() {
    }
}
