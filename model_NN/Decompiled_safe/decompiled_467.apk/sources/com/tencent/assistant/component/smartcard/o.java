package com.tencent.assistant.component.smartcard;

import android.os.Bundle;
import android.view.View;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.link.b;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistantv2.st.page.STPageInfo;

/* compiled from: ProGuard */
class o implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ NormalSmartcardBaseItem f1154a;

    o(NormalSmartcardBaseItem normalSmartcardBaseItem) {
        this.f1154a = normalSmartcardBaseItem;
    }

    public void onClick(View view) {
        int i;
        STPageInfo q;
        Bundle bundle = new Bundle();
        if (!(this.f1154a.f1115a instanceof BaseActivity) || (q = ((BaseActivity) this.f1154a.f1115a).q()) == null) {
            i = 0;
        } else {
            i = q.f3358a;
        }
        bundle.putInt(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, this.f1154a.a(i));
        b.b(this.f1154a.f1115a, this.f1154a.smartcardModel.n, bundle);
        this.f1154a.a("03_001", 200, this.f1154a.smartcardModel.r, -1);
    }
}
