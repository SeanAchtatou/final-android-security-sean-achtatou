package com.tencent.assistant.component;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamExClickListener;
import com.tencent.assistant.protocol.jce.CommentDetail;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.ba;
import com.tencent.assistant.utils.ct;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import java.util.Random;

/* compiled from: ProGuard */
class x extends OnTMAParamExClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Drawable f1220a;
    final /* synthetic */ CommentDetail b;
    final /* synthetic */ int c;
    final /* synthetic */ Drawable d;
    final /* synthetic */ TextView e;
    final /* synthetic */ CommentDetailView f;

    x(CommentDetailView commentDetailView, Drawable drawable, CommentDetail commentDetail, int i, Drawable drawable2, TextView textView) {
        this.f = commentDetailView;
        this.f1220a = drawable;
        this.b = commentDetail;
        this.c = i;
        this.d = drawable2;
        this.e = textView;
    }

    public STInfoV2 getStInfo(View view) {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f.mContext, 200);
        buildSTInfo.scene = STConst.ST_PAGE_APP_DETAIL_COMMENT;
        String str = STConst.ST_DEFAULT_SLOT;
        if (view.getTag(R.id.tma_st_slot_tag) instanceof String) {
            str = this.f.praiseColId + ((String) view.getTag(R.id.tma_st_slot_tag));
        }
        buildSTInfo.slotId = str;
        return buildSTInfo;
    }

    public void onTMAClick(View view) {
        TextView textView = (TextView) view;
        CommentDetailView.access$2308(this.f);
        ba.a().postDelayed(new y(this, textView), 2000);
        if (((Boolean) textView.getTag()).booleanValue()) {
            textView.setTag(false);
            textView.setCompoundDrawables(this.f1220a, null, null, null);
            long parseInt = ((long) Integer.parseInt(textView.getTag(R.id.comment_praise_count) + Constants.STR_EMPTY)) - 1;
            if (parseInt < 0) {
                parseInt = 0;
            }
            textView.setText(ct.a(parseInt) + Constants.STR_EMPTY);
            textView.setTextColor(Color.parseColor("#6e6e6e"));
            textView.setTag(R.id.comment_praise_count, Long.valueOf(parseInt));
            this.f.praiseEngine.a(this.b.h, this.f.simpleAppModel.f1634a, (byte) 2, this.f.apkId, 0, Constants.STR_EMPTY, this.b.d, Constants.STR_EMPTY, this.f.simpleAppModel.d, this.f.simpleAppModel.c);
            k.a(new STInfoV2(STConst.ST_PAGE_APP_DETAIL_COMMENT, this.f.cancelpraiseColId + ct.a(this.c + 1), 0, STConst.ST_DEFAULT_SLOT, 200));
            return;
        }
        textView.setTag(true);
        textView.setCompoundDrawables(this.d, null, null, null);
        this.e.setText(this.f.praiseTextList[new Random().nextInt(this.f.praiseTextList.length)]);
        this.e.setVisibility(0);
        this.e.startAnimation(this.f.praiseAnimation);
        ba.a().postDelayed(new z(this), 1000);
        long parseInt2 = ((long) Integer.parseInt(textView.getTag(R.id.comment_praise_count) + Constants.STR_EMPTY)) + 1;
        if (parseInt2 < 0) {
            parseInt2 = 0;
        }
        textView.setText(parseInt2 + Constants.STR_EMPTY);
        textView.setTextColor(Color.parseColor("#b68a46"));
        textView.setTag(R.id.comment_praise_count, Long.valueOf(parseInt2));
        this.f.praiseEngine.a(this.b.h, this.f.simpleAppModel.f1634a, (byte) 1, this.f.apkId, 0, Constants.STR_EMPTY, this.b.d, Constants.STR_EMPTY, this.f.simpleAppModel.d, this.f.simpleAppModel.c);
    }
}
