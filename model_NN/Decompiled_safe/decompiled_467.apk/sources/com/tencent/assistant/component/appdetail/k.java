package com.tencent.assistant.component.appdetail;

import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class k extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppdetailFloatingDialog f920a;

    k(AppdetailFloatingDialog appdetailFloatingDialog) {
        this.f920a = appdetailFloatingDialog;
    }

    public void onTMAClick(View view) {
        switch (view.getId()) {
            case R.id.tv_share_qq /*2131165663*/:
                if (this.f920a.g != null && this.f920a.c()) {
                    this.f920a.g.shareToQQ();
                    break;
                }
            case R.id.tv_share_qz /*2131165664*/:
                if (this.f920a.g != null && this.f920a.b()) {
                    this.f920a.g.shareToQZ();
                    break;
                }
            case R.id.tv_share_wx /*2131165665*/:
                if (this.f920a.g != null && this.f920a.d()) {
                    this.f920a.g.shareToWX();
                    break;
                }
            case R.id.tv_share_timeline /*2131165668*/:
                if (this.f920a.g != null && this.f920a.e()) {
                    this.f920a.g.shareToTimeLine();
                    break;
                }
            case R.id.tv_permission /*2131165669*/:
                if (this.f920a.g != null) {
                    this.f920a.g.showPermission();
                    break;
                }
                break;
            case R.id.tv_report /*2131165670*/:
                if (this.f920a.g != null) {
                    this.f920a.g.showReport();
                    break;
                }
                break;
        }
        this.f920a.dismiss();
    }

    public STInfoV2 getStInfo() {
        STInfoV2 f = this.f920a.a();
        f.actionId = 200;
        if (this.clickViewId == R.id.tv_share_qq) {
            f.slotId = a.a(this.f920a.n, "001");
        } else if (this.clickViewId == R.id.tv_share_qz) {
            f.slotId = a.a(this.f920a.n, "002");
        } else if (this.clickViewId == R.id.tv_share_wx) {
            f.slotId = a.a(this.f920a.n, "003");
        } else if (this.clickViewId == R.id.tv_share_timeline) {
            f.slotId = a.a(this.f920a.n, "004");
        } else if (this.clickViewId == R.id.tv_permission) {
            f.slotId = a.a(this.f920a.n, "005");
        } else if (this.clickViewId == R.id.tv_report) {
            f.slotId = a.a(this.f920a.n, "006");
        }
        return f;
    }
}
