package com.tencent.assistant.component.smartcard;

import android.content.Context;
import android.graphics.Canvas;
import android.text.Html;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.appdetail.process.s;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.a.i;
import com.tencent.assistant.model.a.w;
import com.tencent.assistant.model.d;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.bt;
import com.tencent.assistant.utils.ct;
import com.tencent.assistant.utils.df;
import com.tencent.assistantv2.adapter.smartlist.aa;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.component.appdetail.TXDwonloadProcessBar;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class SearchSmartCardVideoItem extends SearchSmartCardBaseItem {
    private View i;
    private TextView j;
    private TextView k;
    private ImageView l;
    private LinearLayout m;
    private aa n;

    public SearchSmartCardVideoItem(Context context) {
        super(context);
    }

    public SearchSmartCardVideoItem(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public SearchSmartCardVideoItem(Context context, i iVar, SmartcardListener smartcardListener, aa aaVar) {
        super(context, iVar, smartcardListener);
        this.n = aaVar;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!this.hasInit) {
            this.hasInit = true;
            h();
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.c = this.b.inflate((int) R.layout.smartcard_video, this);
        this.i = findViewById(R.id.title_ly);
        this.j = (TextView) findViewById(R.id.title);
        this.k = (TextView) findViewById(R.id.desc);
        this.l = (ImageView) findViewById(R.id.divider);
        this.m = (LinearLayout) findViewById(R.id.app_list);
        i();
    }

    /* access modifiers changed from: protected */
    public void b() {
        i();
    }

    private void i() {
        this.m.removeAllViews();
        w wVar = (w) this.smartcardModel;
        if (wVar == null || wVar.f1656a <= 0 || wVar.b == null || wVar.b.size() == 0 || wVar.b.size() < wVar.f1656a) {
            setAllVisibility(8);
            setBackgroundResource(17170445);
            setPadding(0, 0, 0, 0);
            return;
        }
        setBackgroundResource(R.drawable.bg_card_selector_padding);
        setAllVisibility(0);
        ArrayList arrayList = new ArrayList(wVar.b);
        this.j.setText(Html.fromHtml(wVar.k));
        int size = arrayList.size() > wVar.f1656a ? wVar.f1656a : arrayList.size();
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.l.getLayoutParams();
        if (TextUtils.isEmpty(wVar.m)) {
            layoutParams.setMargins(0, df.b(14.0f), 0, 0);
            this.k.setVisibility(8);
        }
        this.m.addView(a(arrayList.subList(0, size)));
    }

    public void setAllVisibility(int i2) {
        this.c.setVisibility(i2);
        this.i.setVisibility(i2);
        this.m.setVisibility(i2);
    }

    private View a(List<SimpleAppModel> list) {
        LinearLayout linearLayout = new LinearLayout(this.f1115a);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        linearLayout.setOrientation(0);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -1);
        layoutParams.weight = 1.0f;
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= list.size()) {
                return linearLayout;
            }
            View inflate = this.b.inflate((int) R.layout.smartcard_app_item, (ViewGroup) null);
            SimpleAppModel simpleAppModel = list.get(i3);
            ((TXImageView) inflate.findViewById(R.id.icon)).updateImageView(simpleAppModel.e, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            ((TextView) inflate.findViewById(R.id.name)).setText(simpleAppModel.d);
            TextView textView = (TextView) inflate.findViewById(R.id.size);
            textView.setText(bt.a(simpleAppModel.k));
            TXDwonloadProcessBar tXDwonloadProcessBar = (TXDwonloadProcessBar) inflate.findViewById(R.id.progress);
            tXDwonloadProcessBar.a(simpleAppModel, new View[]{textView});
            DownloadButton downloadButton = (DownloadButton) inflate.findViewById(R.id.btn);
            downloadButton.a(simpleAppModel);
            downloadButton.setTag(R.id.tma_st_smartcard_tag, STConst.ST_PAGE_TYPE_SMARTCARD);
            if (s.a(simpleAppModel)) {
                downloadButton.setClickable(false);
            } else {
                downloadButton.setClickable(true);
                STInfoV2 a2 = a(c(i3), 200);
                if (a2 != null) {
                    a2.scene = a(0);
                    a2.searchPreId = "|" + this.f;
                    a2.extraData = b(0);
                    a2.updateWithSimpleAppModel(simpleAppModel);
                }
                downloadButton.a(a2, new ad(this), (d) null, downloadButton, tXDwonloadProcessBar);
            }
            inflate.setTag(R.id.tma_st_smartcard_tag, STConst.ST_PAGE_TYPE_SMARTCARD);
            inflate.setOnClickListener(new ae(this, simpleAppModel, i3));
            linearLayout.addView(inflate, layoutParams);
            i2 = i3 + 1;
        }
    }

    /* access modifiers changed from: private */
    public String c(int i2) {
        return c() + ct.a(i2 + 1);
    }

    /* access modifiers changed from: protected */
    public String c() {
        return a.a("09", this.n == null ? 0 : this.n.a());
    }

    /* access modifiers changed from: protected */
    public int d() {
        return com.tencent.assistantv2.st.page.d.f3360a;
    }

    /* access modifiers changed from: protected */
    public String e() {
        if (this.n == null) {
            return null;
        }
        return this.n.d();
    }

    /* access modifiers changed from: protected */
    public long f() {
        if (this.n == null) {
            return 0;
        }
        return this.n.c();
    }

    /* access modifiers changed from: protected */
    public int g() {
        if (this.n == null) {
            return 2000;
        }
        return this.n.b();
    }
}
