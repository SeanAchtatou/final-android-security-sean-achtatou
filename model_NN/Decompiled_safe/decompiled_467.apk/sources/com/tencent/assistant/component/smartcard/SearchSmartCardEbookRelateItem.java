package com.tencent.assistant.component.smartcard;

import android.content.Context;
import android.graphics.Canvas;
import android.text.Html;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.model.a.e;
import com.tencent.assistant.model.a.i;
import com.tencent.assistantv2.adapter.smartlist.aa;
import com.tencent.assistantv2.model.SimpleEbookModel;
import com.tencent.assistantv2.st.page.a;
import com.tencent.assistantv2.st.page.d;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class SearchSmartCardEbookRelateItem extends SearchSmartCardBaseItem {
    private View i;
    private TextView j;
    private TextView k;
    private LinearLayout l;
    /* access modifiers changed from: private */
    public aa m;
    private final String n = Constants.VIA_REPORT_TYPE_SHARE_TO_QZONE;

    public SearchSmartCardEbookRelateItem(Context context) {
        super(context);
    }

    public SearchSmartCardEbookRelateItem(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public SearchSmartCardEbookRelateItem(Context context, i iVar, SmartcardListener smartcardListener) {
        super(context, iVar, smartcardListener);
    }

    public SearchSmartCardEbookRelateItem(Context context, i iVar, SmartcardListener smartcardListener, aa aaVar) {
        super(context, iVar, smartcardListener);
        this.m = aaVar;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!this.hasInit) {
            this.hasInit = true;
            h();
        }
    }

    /* access modifiers changed from: protected */
    public String c() {
        return a.a(Constants.VIA_REPORT_TYPE_SHARE_TO_QZONE, this.m == null ? 0 : this.m.a());
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.c = this.b.inflate((int) R.layout.smartcard_ebook_relate, this);
        this.i = findViewById(R.id.title_ly);
        this.j = (TextView) findViewById(R.id.title);
        this.k = (TextView) findViewById(R.id.more);
        this.l = (LinearLayout) findViewById(R.id.list);
        i();
    }

    /* access modifiers changed from: protected */
    public void b() {
        i();
    }

    private void i() {
        this.l.removeAllViews();
        e eVar = (e) this.smartcardModel;
        if (eVar == null || eVar.b <= 0 || eVar.f1639a == null || eVar.f1639a.size() == 0 || eVar.f1639a.size() < eVar.b) {
            setAllVisibility(8);
            setBackgroundResource(17170445);
            setPadding(0, 0, 0, 0);
            return;
        }
        setBackgroundResource(R.drawable.common_card_normal);
        setAllVisibility(0);
        ArrayList arrayList = new ArrayList(eVar.f1639a);
        this.j.setText(Html.fromHtml(eVar.k));
        this.k.setOnClickListener(new aa(this, getContext(), eVar));
        this.l.addView(a(arrayList.subList(0, arrayList.size() > eVar.b ? eVar.b : arrayList.size())));
    }

    public void setAllVisibility(int i2) {
        this.c.setVisibility(i2);
        this.i.setVisibility(i2);
        this.l.setVisibility(i2);
    }

    private View a(List<SimpleEbookModel> list) {
        int i2 = 0;
        LinearLayout linearLayout = new LinearLayout(this.f1115a);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        linearLayout.setOrientation(0);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -1);
        layoutParams.weight = 1.0f;
        while (true) {
            int i3 = i2;
            if (i3 >= list.size()) {
                return linearLayout;
            }
            SimpleEbookModel simpleEbookModel = list.get(i3);
            View inflate = this.b.inflate((int) R.layout.smartcard_ebook_relate_item, (ViewGroup) null);
            inflate.setOnClickListener(new z(this, getContext(), i3, simpleEbookModel));
            ((TXImageView) inflate.findViewById(R.id.icon)).updateImageView(simpleEbookModel.c, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            ((TextView) inflate.findViewById(R.id.tag)).setText(simpleEbookModel.i);
            ((TextView) inflate.findViewById(R.id.name)).setText(simpleEbookModel.b);
            linearLayout.addView(inflate, layoutParams);
            i2 = i3 + 1;
        }
    }

    /* access modifiers changed from: protected */
    public int d() {
        return d.b;
    }

    /* access modifiers changed from: protected */
    public String e() {
        if (this.m == null) {
            return null;
        }
        return this.m.d();
    }

    /* access modifiers changed from: protected */
    public long f() {
        if (this.m == null) {
            return 0;
        }
        return this.m.c();
    }

    /* access modifiers changed from: protected */
    public int g() {
        if (this.m == null) {
            return 2000;
        }
        return this.m.b();
    }
}
