package com.tencent.assistant.component.smartcard;

import android.content.Context;
import android.view.View;
import com.tencent.assistant.b.a;
import com.tencent.assistant.component.listener.OnTMAParamExClickListener;
import com.tencent.assistant.link.b;
import com.tencent.assistant.model.a.e;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
class aa extends OnTMAParamExClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SearchSmartCardEbookRelateItem f1126a;
    private Context b;
    private e c;

    public aa(SearchSmartCardEbookRelateItem searchSmartCardEbookRelateItem, Context context, e eVar) {
        this.f1126a = searchSmartCardEbookRelateItem;
        this.b = context;
        this.c = eVar;
    }

    public void onTMAClick(View view) {
        String str;
        if (this.c != null) {
            if (this.f1126a.m != null) {
                str = "tpmast://search?" + PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME + "=" + this.f1126a.m.b();
            } else {
                str = "tpmast://search?" + PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME + "=" + 2000;
            }
            b.a(this.f1126a.getContext(), str + "&" + a.ab + "=" + 3);
        }
    }

    public STInfoV2 getStInfo(View view) {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f1126a.getContext(), 200);
        if (this.f1126a.m != null) {
            buildSTInfo.slotId = com.tencent.assistantv2.st.page.a.a(Constants.VIA_REPORT_TYPE_SHARE_TO_QZONE, this.f1126a.m.a());
        } else {
            buildSTInfo.slotId = com.tencent.assistantv2.st.page.a.a(Constants.VIA_REPORT_TYPE_SHARE_TO_QZONE, 0);
        }
        if (this.f1126a.m != null) {
            buildSTInfo.extraData = this.f1126a.m.d();
        }
        buildSTInfo.status = "01";
        return buildSTInfo;
    }
}
