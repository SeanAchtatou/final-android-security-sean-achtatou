package com.tencent.assistant.component;

import android.content.Context;
import android.text.Html;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.protocol.jce.AppUpdateInfo;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public class AssistantUpdateOverTurnView extends OverTurnView {
    private TXImageView iv_appIcon;
    private LayoutInflater layoutInflater;
    private Context mContext;
    private TextView tv_desc;

    public AssistantUpdateOverTurnView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public View makeView() {
        View inflate = this.layoutInflater.inflate((int) R.layout.item_asistant_update, (ViewGroup) null);
        this.iv_appIcon = (TXImageView) inflate.findViewById(R.id.app_icon);
        this.tv_desc = (TextView) inflate.findViewById(R.id.app_text);
        return inflate;
    }

    public void updateView(AppUpdateInfo appUpdateInfo) {
        if (appUpdateInfo == null) {
            if (this.iv_appIcon != null) {
                this.iv_appIcon.setVisibility(8);
            }
            if (this.tv_desc != null) {
                this.tv_desc.setText((int) R.string.p_no_update);
                return;
            }
            return;
        }
        View nextView = getNextView();
        this.iv_appIcon = (TXImageView) nextView.findViewById(R.id.app_icon);
        this.tv_desc = (TextView) nextView.findViewById(R.id.app_text);
        String a2 = appUpdateInfo.e != null ? appUpdateInfo.e.a() : Constants.STR_EMPTY;
        String str = TextUtils.isEmpty(appUpdateInfo.x) ? Constants.STR_EMPTY : appUpdateInfo.x;
        if (this.iv_appIcon != null) {
            this.iv_appIcon.setVisibility(0);
            this.iv_appIcon.updateImageView(a2, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
        }
        if (this.tv_desc != null) {
            if (TextUtils.isEmpty(str)) {
                this.tv_desc.setText(this.mContext.getString(R.string.p_default_update_tips));
            } else {
                try {
                    this.tv_desc.setText(Html.fromHtml(str));
                } catch (Throwable th) {
                    this.tv_desc.setText(this.mContext.getString(R.string.p_default_update_tips));
                }
            }
        }
        next();
    }

    public void clearView() {
        if (this.iv_appIcon != null) {
            this.iv_appIcon.setVisibility(8);
        }
        if (this.tv_desc != null) {
            this.tv_desc.setText(Constants.STR_EMPTY);
        }
    }

    public void doBeforeInit(Context context) {
        this.mContext = context;
        this.layoutInflater = LayoutInflater.from(this.mContext);
    }

    public Animation setInDownAnim(Context context) {
        return AnimationUtils.loadAnimation(context, R.anim.mgr_fade_in);
    }

    public Animation setOutDownAnim(Context context) {
        return AnimationUtils.loadAnimation(context, R.anim.mgr_fade_out);
    }

    public Animation setInUpAnim(Context context) {
        return AnimationUtils.loadAnimation(context, R.anim.mgr_fade_in);
    }

    public Animation setOutUpAnim(Context context) {
        return AnimationUtils.loadAnimation(context, R.anim.mgr_fade_out);
    }
}
