package com.tencent.assistant.component;

import android.widget.AbsListView;
import com.tencent.assistant.component.invalidater.ListViewScrollListener;

/* compiled from: ProGuard */
class ed extends ListViewScrollListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ UpdateListView f1039a;

    ed(UpdateListView updateListView) {
        this.f1039a = updateListView;
    }

    public void onScrollStateChanged(AbsListView absListView, int i) {
        super.onScrollStateChanged(absListView, i);
    }

    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
        if (this.f1039a.layoutPopBar != null && this.f1039a.mUpdateListView != null) {
            if (i == 0) {
                this.f1039a.layoutPopBar.setVisibility(8);
            }
            this.f1039a.freshPopBar();
        }
    }
}
