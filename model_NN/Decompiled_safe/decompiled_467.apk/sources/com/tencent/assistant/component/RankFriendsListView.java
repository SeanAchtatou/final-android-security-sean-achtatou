package com.tencent.assistant.component;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.HeaderViewListAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.tencent.assistant.adapter.RankFriendsListAdapter;
import com.tencent.assistantv2.component.RankNormalListView;

/* compiled from: ProGuard */
public class RankFriendsListView extends RankNormalListView {
    public RankFriendsListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.enginecallBack = new cp(this);
    }

    /* access modifiers changed from: protected */
    public void initAdapter() {
        ListAdapter adapter = ((ListView) this.mScrollContentView).getAdapter();
        if (adapter instanceof HeaderViewListAdapter) {
            this.mAdapter = (RankFriendsListAdapter) ((HeaderViewListAdapter) adapter).getWrappedAdapter();
        } else {
            this.mAdapter = (RankFriendsListAdapter) ((ListView) this.mScrollContentView).getAdapter();
        }
        if (this.mAdapter == null) {
        }
    }
}
