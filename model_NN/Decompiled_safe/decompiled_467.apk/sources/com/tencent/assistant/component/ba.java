package com.tencent.assistant.component;

import android.content.Intent;
import android.view.View;
import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.link.b;
import com.tencent.assistantv2.activity.MainActivity;

/* compiled from: ProGuard */
class ba implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ NormalErrorPage f956a;

    ba(NormalErrorPage normalErrorPage) {
        this.f956a = normalErrorPage;
    }

    /* access modifiers changed from: protected */
    public Object clone() {
        return super.clone();
    }

    public void onClick(View view) {
        if (3 == this.f956a.currentState) {
            Intent intent = new Intent("android.settings.SETTINGS");
            if (b.a(this.f956a.getContext(), intent)) {
                this.f956a.getContext().startActivity(intent);
            } else {
                Toast.makeText(this.f956a.getContext(), (int) R.string.dialog_not_found_activity, 0).show();
            }
        } else {
            if (4 == this.f956a.currentState) {
                this.f956a.context.startActivity(new Intent(this.f956a.context, MainActivity.class));
            }
            if (this.f956a.listener != null) {
                this.f956a.listener.onClick(view);
            }
        }
    }
}
