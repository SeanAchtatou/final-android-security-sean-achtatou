package com.tencent.assistant.component;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.activity.ShareBaseActivity;
import com.tencent.assistant.component.ShareAppBar;
import com.tencent.assistant.g.a;
import com.tencent.assistant.model.ShareAppModel;
import com.tencent.assistant.model.ShareBaseModel;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.df;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public class ShareAppDialog extends Dialog implements ShareAppBar.ShareAppCallback {
    private static final String LOG_TAG = "ShareYYBDialog";
    protected View mContentView;
    protected int mContentViewId;
    private a mController;
    private int mPageId = 0;
    private ShareAppBar mShareAppBar;
    private ShareBaseActivity mShareBaseActivity;
    private String mTitle;
    protected TextView mTitleView;
    private ShareAppBar shareAppBar = null;
    private LinearLayout shareAppBottomBarLayout = null;
    private TextView tvMsg = null;

    /* compiled from: ProGuard */
    public interface OnDismissListener {
        void onDismiss(boolean z);
    }

    public ShareAppDialog(ShareBaseActivity shareBaseActivity, int i, int i2) {
        super(shareBaseActivity, i);
        this.mShareBaseActivity = shareBaseActivity;
        this.mContentViewId = i2;
    }

    public ShareAppDialog(ShareBaseActivity shareBaseActivity, int i, View view) {
        super(shareBaseActivity, i);
        this.mShareBaseActivity = shareBaseActivity;
        this.mContentView = view;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (this.mContentViewId != 0) {
            this.mContentView = LayoutInflater.from(this.mShareBaseActivity).inflate(this.mContentViewId, (ViewGroup) null);
            setContentView(this.mContentView);
        } else if (this.mContentView != null) {
            setContentView(this.mContentView);
        } else {
            this.mContentView = LayoutInflater.from(this.mShareBaseActivity).inflate((int) R.layout.dialog_share_yyb, (ViewGroup) null);
            this.shareAppBottomBarLayout = (LinearLayout) this.mContentView.findViewById(R.id.share_app_bottom_bar_layout);
            this.tvMsg = (TextView) this.mContentView.findViewById(R.id.msg);
            this.shareAppBar = (ShareAppBar) this.mContentView.findViewById(R.id.layout_share);
            if (this.shareAppBar != null) {
                this.shareAppBar.setShareAppCallback(this);
            }
            setContentView(this.mContentView);
        }
        this.mTitleView = (TextView) findViewById(R.id.title);
        if (!TextUtils.isEmpty(this.mTitle)) {
            this.mTitleView.setText(this.mTitle);
        }
        this.mShareAppBar = (ShareAppBar) findViewById(R.id.layout_share);
        this.mController = new dl(this, this.mShareBaseActivity, this.mShareAppBar);
        this.mController.a(getPageId());
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        XLog.i(LOG_TAG, "onStart");
        this.mController.a();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        XLog.i(LOG_TAG, "onStop");
        this.mController.b();
    }

    public void show() {
        super.show();
        alertDialogActionReport(100, STConst.ST_DEFAULT_SLOT);
    }

    public void setShareAppModel(ShareAppModel shareAppModel) {
        if (this.mController != null) {
            this.mController.a(shareAppModel);
        }
    }

    public void setShareBaseModel(ShareBaseModel shareBaseModel) {
        if (this.mController != null) {
            this.mController.a(shareBaseModel);
        }
    }

    public void setPageId(int i) {
        this.mPageId = i;
    }

    public void setTile(int i) {
        if (i != 0) {
            this.mTitle = this.mShareBaseActivity.getString(i);
        }
    }

    public void refreshState() {
        this.mShareAppBar.refreshState();
    }

    private int getActivityPrePageId() {
        if (this.mShareBaseActivity == null || !(this.mShareBaseActivity instanceof BaseActivity)) {
            return 0;
        }
        return this.mShareBaseActivity.f();
    }

    private int getPageId() {
        return this.mPageId == 0 ? STConst.ST_PAGE_SHARE_YYB : this.mPageId;
    }

    private void alertDialogActionReport(int i, String str) {
        logReportV2(i, str, Constants.STR_EMPTY);
    }

    private void logReportV2(int i, String str, String str2) {
        STInfoV2 sTInfoV2 = new STInfoV2(getPageId(), str, getActivityPrePageId(), STConst.ST_DEFAULT_SLOT, i);
        if (sTInfoV2 != null) {
            sTInfoV2.extraData = str2;
            XLog.i(LOG_TAG, "[logReportV2] --> pageId = " + sTInfoV2.scene + ", slotId = " + str + ", prePageId = " + sTInfoV2.sourceScene + ", actionId = " + i + ", extraData = " + str2);
            k.a(sTInfoV2);
        }
    }

    public Bitmap getAnimBitmap() {
        if (this.mContentView != null) {
            return df.a(this.mContentView);
        }
        return null;
    }

    public void noSupportShareApp() {
        if (this.shareAppBottomBarLayout != null) {
            this.shareAppBottomBarLayout.setVisibility(8);
        }
        if (this.tvMsg != null) {
            this.tvMsg.setVisibility(8);
        }
    }
}
