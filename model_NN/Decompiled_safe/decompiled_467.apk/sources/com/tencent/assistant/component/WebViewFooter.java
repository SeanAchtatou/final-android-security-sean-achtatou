package com.tencent.assistant.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
public class WebViewFooter extends LinearLayout {
    private View backView;
    private Context context;
    private View.OnClickListener defaultListener = new el(this);
    private View forwardView;
    private View freshView;
    /* access modifiers changed from: private */
    public IWebViewFooterListener listener;

    /* compiled from: ProGuard */
    public interface IWebViewFooterListener {
        void onBack();

        void onForward();

        void onFresh();
    }

    public WebViewFooter(Context context2, AttributeSet attributeSet) {
        super(context2, attributeSet);
        init(context2);
    }

    public WebViewFooter(Context context2) {
        super(context2);
        init(context2);
    }

    private void init(Context context2) {
        this.context = context2;
        View inflate = LayoutInflater.from(context2).inflate((int) R.layout.webview_footer_layout, this);
        this.backView = inflate.findViewById(R.id.webview_back_img);
        this.forwardView = inflate.findViewById(R.id.webview_forward_img);
        this.freshView = inflate.findViewById(R.id.webview_fresh_img);
        this.backView.setOnClickListener(this.defaultListener);
        this.forwardView.setOnClickListener(this.defaultListener);
        this.freshView.setOnClickListener(this.defaultListener);
    }

    public void setBackEnable(boolean z) {
        this.backView.setEnabled(z);
    }

    public void setForwardEnable(boolean z) {
        this.forwardView.setEnabled(z);
    }

    public void setBackListener(View.OnClickListener onClickListener) {
        this.backView.setOnClickListener(onClickListener);
    }

    public void setForwardListener(View.OnClickListener onClickListener) {
        this.forwardView.setOnClickListener(onClickListener);
    }

    public void setFreshListener(View.OnClickListener onClickListener) {
        this.freshView.setOnClickListener(onClickListener);
    }

    public void setWebViewFooterListener(IWebViewFooterListener iWebViewFooterListener) {
        this.listener = iWebViewFooterListener;
    }
}
