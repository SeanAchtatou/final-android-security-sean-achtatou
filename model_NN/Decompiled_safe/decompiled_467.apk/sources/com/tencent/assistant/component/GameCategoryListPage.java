package com.tencent.assistant.component;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.GameCategoryListView;
import com.tencent.assistant.component.invalidater.ViewPageScrollListener;

/* compiled from: ProGuard */
public class GameCategoryListPage extends RelativeLayout implements GameCategoryListView.GameCategoryApplistRefreshListener {
    private NormalErrorRecommendPage errorPage;
    private LayoutInflater inflater;
    private GameCategoryListView listView;
    private LoadingView loadingView;
    private Context mContext;
    private View.OnClickListener refreshClick = new am(this);

    public GameCategoryListPage(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        initView(context);
        this.mContext = context;
        this.listView.registerListener(this);
    }

    public GameCategoryListPage(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        initView(context);
        this.mContext = context;
        this.listView.registerListener(this);
    }

    public GameCategoryListPage(Context context) {
        super(context);
        initView(context);
        this.mContext = context;
        this.listView.registerListener(this);
    }

    private void initView(Context context) {
        LayoutInflater.from(context).inflate((int) R.layout.game_applist_component_category_view, this);
        this.listView = (GameCategoryListView) findViewById(R.id.applist);
        this.listView.setVisibility(8);
        this.listView.setDivider(null);
        this.listView.setSelector(new ColorDrawable(0));
        this.listView.setCacheColorHint(17170445);
        this.loadingView = (LoadingView) findViewById(R.id.loading_view);
        this.loadingView.setVisibility(0);
        this.errorPage = (NormalErrorRecommendPage) findViewById(R.id.error_page);
        this.errorPage.setButtonClickListener(this.refreshClick);
    }

    public void setViewPageListener(ViewPageScrollListener viewPageScrollListener) {
        this.listView.setViewPageListener(viewPageScrollListener);
    }

    public void setAdapter(BaseAdapter baseAdapter) {
        this.listView.setAdapter(baseAdapter);
    }

    public void onErrorHappened(int i) {
        this.listView.setVisibility(8);
        this.errorPage.setVisibility(0);
        this.loadingView.setVisibility(8);
        this.errorPage.setErrorType(i);
    }

    public void onNetworkNoError() {
        this.listView.setVisibility(0);
        this.errorPage.setVisibility(8);
        this.loadingView.setVisibility(8);
    }

    public void onNextPageLoadFailed() {
        Toast.makeText(this.mContext, "拉取失败", 0).show();
    }

    public void onNetworkLoading() {
        this.loadingView.setVisibility(0);
        this.listView.setVisibility(8);
        this.errorPage.setVisibility(8);
    }

    public void recycleData() {
        this.listView.recycleData();
    }

    public void refreshData() {
        this.listView.refreshData();
    }

    public void onPause() {
        this.listView.onPause();
    }

    public void onResume() {
        this.listView.onResume();
    }

    public void setCategoryType(long j) {
        if (this.listView != null) {
            this.listView.setCategoryType(j);
        }
    }

    public ListView getListView() {
        return this.listView.getListView();
    }

    public boolean isScrollStateIdle() {
        return this.listView.isScrollStateIdle();
    }
}
