package com.tencent.assistant.component;

import android.content.Context;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
public class FeedbackInputView extends RelativeLayout {
    private Context context;
    private View.OnClickListener defaultClickListener = new al(this);
    /* access modifiers changed from: private */
    public EditText inputView;
    private TextView overFlowText;
    private TextView sendButton;

    public FeedbackInputView(Context context2) {
        super(context2);
        this.context = context2;
        initView();
    }

    public FeedbackInputView(Context context2, AttributeSet attributeSet) {
        super(context2, attributeSet);
        this.context = context2;
        initView();
    }

    private void initView() {
        View inflate = LayoutInflater.from(this.context).inflate((int) R.layout.feedback_input, this);
        this.inputView = (EditText) inflate.findViewById(R.id.feedback_input);
        this.inputView.setOnClickListener(this.defaultClickListener);
        this.overFlowText = (TextView) inflate.findViewById(R.id.overflow_text);
        this.sendButton = (TextView) inflate.findViewById(R.id.send_button);
    }

    public EditText getInputView() {
        return this.inputView;
    }

    public void showOverflowView() {
        this.sendButton.setEnabled(false);
        this.overFlowText.setVisibility(0);
    }

    public void hideOverflowView() {
        this.sendButton.setEnabled(true);
        this.overFlowText.setVisibility(8);
    }

    public void setSendButtonClickListener(View.OnClickListener onClickListener) {
        if (this.sendButton != null) {
            this.sendButton.setOnClickListener(onClickListener);
        }
    }

    public void setOnKeyListener(View.OnKeyListener onKeyListener) {
        if (this.inputView != null) {
            this.inputView.setOnKeyListener(onKeyListener);
        }
    }

    public void addTextChangedListener(TextWatcher textWatcher) {
        if (this.inputView != null) {
            this.inputView.addTextChangedListener(textWatcher);
        }
    }
}
