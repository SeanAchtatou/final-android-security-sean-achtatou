package com.tencent.assistant.component.txscrollview;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListAdapter;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;

/* compiled from: ProGuard */
public class TXGridView extends TXScrollViewBase<GridView> {
    public TXGridView(Context context) {
        super(context, TXScrollViewBase.ScrollDirection.SCROLL_DIRECTION_VERTICAL, TXScrollViewBase.ScrollMode.NONE);
    }

    public TXGridView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void setAdapter(ListAdapter listAdapter) {
        if (listAdapter != null) {
            ((GridView) this.mScrollContentView).setAdapter(listAdapter);
        }
    }

    public ListAdapter getAdapter() {
        return ((GridView) this.mScrollContentView).getAdapter();
    }

    public void setSelector(Drawable drawable) {
        if (this.mScrollContentView != null && drawable != null) {
            ((GridView) this.mScrollContentView).setSelector(drawable);
        }
    }

    public void setHorizontalSpacing(int i) {
        ((GridView) this.mScrollContentView).setHorizontalSpacing(i);
    }

    public void setVerticalSpacing(int i) {
        ((GridView) this.mScrollContentView).setVerticalSpacing(i);
    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener onItemClickListener) {
        ((GridView) this.mScrollContentView).setOnItemClickListener(onItemClickListener);
    }

    public void setNumColumns(int i) {
        ((GridView) this.mScrollContentView).setNumColumns(i);
    }

    /* access modifiers changed from: protected */
    public boolean isReadyForScrollStart() {
        View childAt;
        ListAdapter adapter = ((GridView) this.mScrollContentView).getAdapter();
        if (adapter == null || adapter.isEmpty()) {
            return true;
        }
        if (((GridView) this.mScrollContentView).getFirstVisiblePosition() > 1 || (childAt = ((GridView) this.mScrollContentView).getChildAt(0)) == null) {
            return false;
        }
        return childAt.getTop() >= ((GridView) this.mScrollContentView).getTop();
    }

    /* access modifiers changed from: protected */
    public boolean isReadyForScrollEnd() {
        View childAt;
        ListAdapter adapter = ((GridView) this.mScrollContentView).getAdapter();
        if (adapter == null || adapter.isEmpty()) {
            return true;
        }
        int count = ((GridView) this.mScrollContentView).getCount() - 1;
        int lastVisiblePosition = ((GridView) this.mScrollContentView).getLastVisiblePosition();
        if (lastVisiblePosition < count - 1 || (childAt = ((GridView) this.mScrollContentView).getChildAt(lastVisiblePosition - ((GridView) this.mScrollContentView).getFirstVisiblePosition())) == null) {
            return false;
        }
        return childAt.getBottom() <= ((GridView) this.mScrollContentView).getBottom();
    }

    /* access modifiers changed from: protected */
    public boolean isContentFullScreen() {
        return false;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public GridView createScrollContentView(Context context) {
        return new GridView(context);
    }
}
