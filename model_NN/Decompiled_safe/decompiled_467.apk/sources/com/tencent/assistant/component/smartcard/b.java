package com.tencent.assistant.component.smartcard;

import android.content.Intent;
import android.view.View;
import com.tencent.assistant.component.appdetail.CommentDetailTabView;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistantv2.activity.AppDetailActivityV5;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class b extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f1141a;
    final /* synthetic */ SimpleAppModel b;
    final /* synthetic */ STInfoV2 c;
    final /* synthetic */ NormalSmartCardAppNode d;

    b(NormalSmartCardAppNode normalSmartCardAppNode, int i, SimpleAppModel simpleAppModel, STInfoV2 sTInfoV2) {
        this.d = normalSmartCardAppNode;
        this.f1141a = i;
        this.b = simpleAppModel;
        this.c = sTInfoV2;
    }

    public void onTMAClick(View view) {
        Intent intent = new Intent(this.d.getContext(), AppDetailActivityV5.class);
        intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, this.f1141a);
        intent.putExtra(CommentDetailTabView.PARAMS_SIMPLE_MODEL_INFO, this.b);
        this.d.getContext().startActivity(intent);
    }

    public STInfoV2 getStInfo() {
        if (this.c != null) {
            this.c.actionId = 200;
            this.c.updateStatusToDetail(this.b);
        }
        return this.c;
    }
}
