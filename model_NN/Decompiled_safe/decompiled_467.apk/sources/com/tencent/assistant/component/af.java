package com.tencent.assistant.component;

import android.view.View;
import android.widget.ImageView;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
class af implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CommentHeaderTagView f884a;

    af(CommentHeaderTagView commentHeaderTagView) {
        this.f884a = commentHeaderTagView;
    }

    public void onClick(View view) {
        ImageView imageView = (ImageView) view;
        if (this.f884a.isSelect) {
            boolean unused = this.f884a.isSelect = false;
            imageView.setImageResource(R.drawable.icon_open);
            this.f884a.taglistlayout.a(false);
            this.f884a.taglistlayout.requestLayout();
            return;
        }
        boolean unused2 = this.f884a.isSelect = true;
        imageView.setImageResource(R.drawable.icon_close);
        this.f884a.taglistlayout.a(true);
        this.f884a.taglistlayout.requestLayout();
    }
}
