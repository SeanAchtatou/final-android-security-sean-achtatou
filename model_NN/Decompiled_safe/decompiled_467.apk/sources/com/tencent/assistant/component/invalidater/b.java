package com.tencent.assistant.component.invalidater;

/* compiled from: ProGuard */
class b implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ViewInvalidateMessage f1068a;
    final /* synthetic */ CommonViewInvalidater b;

    b(CommonViewInvalidater commonViewInvalidater, ViewInvalidateMessage viewInvalidateMessage) {
        this.b = commonViewInvalidater;
        this.f1068a = viewInvalidateMessage;
    }

    public void run() {
        if (this.f1068a.target != null) {
            this.f1068a.target.handleMessage(this.f1068a);
        } else {
            this.b.handleMessage(this.f1068a);
        }
        this.b.handleQueueMsg();
    }
}
