package com.tencent.assistant.component.listview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.connect.common.Constants;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* compiled from: ProGuard */
public class BubbleAnimationView extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    boolean f1078a = false;
    private TextView b = null;
    private TextView c = null;
    private Animation d;
    /* access modifiers changed from: private */
    public List<Double> e = Collections.synchronizedList(new ArrayList());
    private int f = 0;
    private DecimalFormat g = new DecimalFormat("0.0");

    private void a(double d2) {
        if (d2 > 0.0d) {
            this.e.clear();
            double d3 = d2 / 1.0d;
            for (int i = 0; i < 1; i++) {
                this.e.add(Double.valueOf(d3));
            }
        }
    }

    public BubbleAnimationView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        initView(context);
    }

    public void initView(Context context) {
        LayoutInflater.from(context).inflate((int) R.layout.bubble_animview_layout, this);
        this.b = (TextView) findViewById(R.id.bubble_txt);
        this.c = (TextView) findViewById(R.id.bubble_size_unit);
        this.d = AnimationUtils.loadAnimation(getContext(), R.anim.manager_bubble_animation);
        this.d.setFillEnabled(true);
        this.d.setFillBefore(false);
    }

    public void startAnimation(double d2) {
        a(d2);
        if (!this.f1078a) {
            a();
        }
    }

    /* access modifiers changed from: private */
    public void a() {
        this.f1078a = true;
        if (!this.e.isEmpty()) {
            c(this.e.remove(0).doubleValue());
            this.d.setAnimationListener(new j(this));
            startAnimation(this.d);
        }
    }

    private double b(double d2) {
        while (d2 >= 1000.0d) {
            d2 /= 1024.0d;
        }
        return d2;
    }

    private void c(double d2) {
        double abs = Math.abs(d2);
        this.b.setText(Constants.STR_EMPTY + "-" + this.g.format(b(abs)));
        d(abs);
    }

    private void d(double d2) {
        char c2;
        String str;
        double abs = Math.abs(d2) / 1024.0d;
        if (abs < 1000.0d) {
            c2 = 0;
        } else {
            double d3 = abs / 1024.0d;
            if (d3 < 1000.0d) {
                c2 = 1;
            } else {
                double d4 = d3 / 1024.0d;
                c2 = 2;
            }
        }
        switch (c2) {
            case 0:
                str = "KB";
                break;
            case 1:
                str = "MB";
                break;
            case 2:
                str = "GB";
                break;
            default:
                str = "KB";
                break;
        }
        this.c.setText(str);
    }
}
