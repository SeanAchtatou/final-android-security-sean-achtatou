package com.tencent.assistant.component.hotwords;

import com.tencent.assistant.protocol.jce.AdvancedHotWord;

/* compiled from: ProGuard */
public interface HotwordsItemCallback {
    void onHotwordsItemAction(AdvancedHotWord advancedHotWord, int i);
}
