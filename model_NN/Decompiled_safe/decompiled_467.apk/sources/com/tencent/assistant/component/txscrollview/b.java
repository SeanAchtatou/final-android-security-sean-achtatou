package com.tencent.assistant.component.txscrollview;

import android.view.View;
import com.tencent.assistant.component.txscrollview.TXRefreshScrollViewBase;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;

/* compiled from: ProGuard */
class b implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RankRefreshGetMoreListView f1198a;

    b(RankRefreshGetMoreListView rankRefreshGetMoreListView) {
        this.f1198a = rankRefreshGetMoreListView;
    }

    public void onClick(View view) {
        if (this.f1198a.mRefreshListViewListener != null && this.f1198a.mGetMoreRefreshState == TXRefreshScrollViewBase.RefreshState.RESET) {
            this.f1198a.mRefreshListViewListener.onTXRefreshListViewRefresh(TXScrollViewBase.ScrollState.ScrollState_FromEnd);
        }
    }
}
