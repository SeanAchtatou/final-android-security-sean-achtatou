package com.tencent.assistant.component.smartcard;

import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.link.b;
import com.tencent.assistant.model.a.m;
import com.tencent.assistant.net.c;
import com.tencent.assistant.utils.v;
import com.tencent.assistantv2.component.dh;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class f extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ m f1145a;
    final /* synthetic */ STInfoV2 b;
    final /* synthetic */ NormalSmartCardPlayerSHowItem c;

    f(NormalSmartCardPlayerSHowItem normalSmartCardPlayerSHowItem, m mVar, STInfoV2 sTInfoV2) {
        this.c = normalSmartCardPlayerSHowItem;
        this.f1145a = mVar;
        this.b = sTInfoV2;
    }

    public void onTMAClick(View view) {
        if (c.d()) {
            b.b(this.c.getContext(), this.f1145a.e);
        } else if (!c.a()) {
            dh.a(this.c.getContext(), this.c.getContext().getResources().getString(R.string.no_network_please_check), 0);
        } else {
            g gVar = new g(this);
            gVar.contentRes = this.c.getContext().getResources().getString(R.string.smartcard_play_video_23G);
            v.a(gVar);
        }
    }

    public STInfoV2 getStInfo() {
        if (this.b != null) {
            this.b.status = "06";
        }
        return this.b;
    }
}
