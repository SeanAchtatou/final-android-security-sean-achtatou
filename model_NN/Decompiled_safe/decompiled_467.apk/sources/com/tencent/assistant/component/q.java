package com.tencent.assistant.component;

import com.tencent.assistant.component.invalidater.ViewInvalidateMessage;
import com.tencent.assistant.module.callback.j;
import com.tencent.assistant.protocol.jce.AppCategory;
import com.tencent.assistant.protocol.jce.ColorCardItem;
import java.util.HashMap;
import java.util.List;

/* compiled from: ProGuard */
class q implements j {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CategoryListView f1104a;

    q(CategoryListView categoryListView) {
        this.f1104a = categoryListView;
    }

    public void a(int i, int i2, List<ColorCardItem> list, List<AppCategory> list2, List<AppCategory> list3, List<ColorCardItem> list4, List<AppCategory> list5, List<AppCategory> list6) {
        if (this.f1104a.viewPagerlistener != null) {
            ViewInvalidateMessage viewInvalidateMessage = new ViewInvalidateMessage(1, null, this.f1104a.pageMessageHandler);
            viewInvalidateMessage.arg1 = i2;
            viewInvalidateMessage.arg2 = i;
            HashMap hashMap = new HashMap();
            if (this.f1104a.mCategoryId == -1) {
                hashMap.put(0, list);
                hashMap.put(1, list2);
                if (list3 != null) {
                    hashMap.put(2, list3);
                }
            } else if (this.f1104a.mCategoryId == -2) {
                hashMap.put(0, list4);
                hashMap.put(1, list5);
                if (list6 != null) {
                    hashMap.put(2, list6);
                }
            }
            viewInvalidateMessage.params = hashMap;
            this.f1104a.viewPagerlistener.sendMessage(viewInvalidateMessage);
        }
    }
}
