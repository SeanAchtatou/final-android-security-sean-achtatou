package com.tencent.assistant.component;

import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.PhotoBackupNewActivity;
import com.tencent.assistant.localres.model.LocalImage;
import com.tencent.connector.ipc.a;
import java.io.File;

/* compiled from: ProGuard */
class bn implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PhotoBackupMiddleGridView f969a;

    bn(PhotoBackupMiddleGridView photoBackupMiddleGridView) {
        this.f969a = photoBackupMiddleGridView;
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        File file;
        if (a.a().d()) {
            LocalImage localImage = (LocalImage) this.f969a.mImageAdapter.getItem(i);
            if (localImage.path == null || ((file = new File(localImage.path)) != null && file.exists())) {
                if (this.f969a.mImageAdapter.c(localImage.id)) {
                    this.f969a.mImageAdapter.b(localImage.id);
                } else {
                    this.f969a.mImageAdapter.a(localImage.id);
                }
                int a2 = this.f969a.mImageAdapter.a();
                if (this.f969a.activity instanceof PhotoBackupNewActivity) {
                    ((PhotoBackupNewActivity) this.f969a.activity).f(a2);
                    return;
                }
                return;
            }
            Toast.makeText(this.f969a.getContext(), this.f969a.getContext().getString(R.string.wifi_file_del), 1).show();
            if (this.f969a.mImageAdapter != null) {
                this.f969a.mImageAdapter.e(i);
            }
        }
    }
}
