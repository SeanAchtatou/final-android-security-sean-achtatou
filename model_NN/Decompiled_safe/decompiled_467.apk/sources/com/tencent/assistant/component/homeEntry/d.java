package com.tencent.assistant.component.homeEntry;

import android.text.TextUtils;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.link.b;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.bc;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class d extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ HomeEntryTemplateView f1057a;

    d(HomeEntryTemplateView homeEntryTemplateView) {
        this.f1057a = homeEntryTemplateView;
    }

    public void onTMAClick(View view) {
        XLog.v("home_entry", "HomeEntryTemplateView-----OnClickListener---onTMAClick--");
        if (this.f1057a.mSourceData != null && this.f1057a.mSourceData.d != null && !TextUtils.isEmpty(this.f1057a.mSourceData.d.f1970a)) {
            XLog.v("home_entry", "HomeEntryTemplateView------OnClickListener---onClick actionUrl is ok");
            b.a(this.f1057a.b, this.f1057a.mSourceData.d);
        }
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f1057a.b, 200);
        buildSTInfo.scene = STConst.ST_PAGE_COMPETITIVE;
        if (this.f1057a.mSourceData != null) {
            bc.a(this.f1057a.mSourceData.e, buildSTInfo);
        }
        XLog.v("home_entry", "HomeEntryTemplateView----OnClickListener---getStInfo--stInfo.slotId= " + buildSTInfo.slotId + "--stInfo.pushInfo= " + buildSTInfo.pushInfo);
        return buildSTInfo;
    }
}
