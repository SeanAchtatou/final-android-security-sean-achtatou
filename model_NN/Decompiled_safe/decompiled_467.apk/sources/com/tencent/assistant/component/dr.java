package com.tencent.assistant.component;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;

/* compiled from: ProGuard */
class dr extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TotalTabLayout f1026a;

    dr(TotalTabLayout totalTabLayout) {
        this.f1026a = totalTabLayout;
    }

    public void handleMessage(Message message) {
        super.handleMessage(message);
        if (this.f1026a.mContext == null) {
            return;
        }
        if (!(this.f1026a.mContext instanceof Activity) || !((Activity) this.f1026a.mContext).isFinishing()) {
            super.handleMessage(message);
            switch (message.what) {
                case 0:
                    this.f1026a.startTranslateAnimation(message.arg1, message.arg2);
                    return;
                case 1:
                    this.f1026a.layoutAnimImageOnResume();
                    return;
                default:
                    return;
            }
        }
    }
}
