package com.tencent.assistant.component.smartcard;

import android.content.Context;
import android.graphics.Canvas;
import android.text.Html;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.appdetail.process.s;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.a.i;
import com.tencent.assistant.model.a.y;
import com.tencent.assistant.model.d;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.bt;
import com.tencent.assistant.utils.ct;
import com.tencent.assistantv2.adapter.smartlist.aa;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.component.appdetail.TXDwonloadProcessBar;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class SearchSmartCardYuyiTagItem extends SearchSmartCardBaseItem {
    private final String i = "SearchSmartCardYuyiTagItem";
    private View j;
    private TextView k;
    private TextView l;
    private LinearLayout m;
    private aa n;
    private final String o = "18";

    public SearchSmartCardYuyiTagItem(Context context) {
        super(context);
    }

    public SearchSmartCardYuyiTagItem(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public SearchSmartCardYuyiTagItem(Context context, i iVar, SmartcardListener smartcardListener) {
        super(context, iVar, smartcardListener);
    }

    public SearchSmartCardYuyiTagItem(Context context, i iVar, SmartcardListener smartcardListener, aa aaVar) {
        super(context, iVar, smartcardListener);
        this.n = aaVar;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!this.hasInit) {
            this.hasInit = true;
            h();
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.c = this.b.inflate((int) R.layout.smartcard_yuyi_tag, this);
        this.j = findViewById(R.id.title_ly);
        this.k = (TextView) findViewById(R.id.title);
        this.l = (TextView) findViewById(R.id.more);
        this.m = (LinearLayout) findViewById(R.id.app_list);
        i();
    }

    /* access modifiers changed from: protected */
    public void b() {
        i();
    }

    private void i() {
        this.m.removeAllViews();
        y yVar = (y) this.smartcardModel;
        if (yVar == null || yVar.f1658a <= 0 || yVar.b == null || yVar.b.size() == 0 || yVar.b.size() < yVar.f1658a) {
            setAllVisibility(8);
            setBackgroundResource(17170445);
            setPadding(0, 0, 0, 0);
            return;
        }
        setBackgroundResource(R.drawable.bg_card_selector_padding);
        setAllVisibility(0);
        ArrayList arrayList = new ArrayList(yVar.b);
        this.k.setText(Html.fromHtml(yVar.k));
        this.l.setOnClickListener(new aj(this, getContext(), yVar));
        this.m.addView(a(arrayList.subList(0, arrayList.size() > yVar.f1658a ? yVar.f1658a : arrayList.size())));
    }

    public void setAllVisibility(int i2) {
        this.c.setVisibility(i2);
        this.j.setVisibility(i2);
        this.m.setVisibility(i2);
    }

    private View a(List<SimpleAppModel> list) {
        LinearLayout linearLayout = new LinearLayout(this.f1115a);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        linearLayout.setOrientation(0);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -1);
        layoutParams.weight = 1.0f;
        for (int i2 = 0; i2 < list.size(); i2++) {
            View inflate = this.b.inflate((int) R.layout.smartcard_app_item, (ViewGroup) null);
            SimpleAppModel simpleAppModel = list.get(i2);
            ((TXImageView) inflate.findViewById(R.id.icon)).updateImageView(simpleAppModel.e, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            ((TextView) inflate.findViewById(R.id.name)).setText(simpleAppModel.d);
            TextView textView = (TextView) inflate.findViewById(R.id.size);
            textView.setText(bt.a(simpleAppModel.k));
            TXDwonloadProcessBar tXDwonloadProcessBar = (TXDwonloadProcessBar) inflate.findViewById(R.id.progress);
            tXDwonloadProcessBar.a(simpleAppModel, new View[]{textView});
            DownloadButton downloadButton = (DownloadButton) inflate.findViewById(R.id.btn);
            downloadButton.a(simpleAppModel);
            downloadButton.setTag(R.id.tma_st_smartcard_tag, STConst.ST_PAGE_TYPE_SMARTCARD);
            if (s.a(simpleAppModel)) {
                downloadButton.setClickable(false);
            } else {
                downloadButton.setClickable(true);
                STInfoV2 a2 = a(c(i2), 200);
                if (a2 != null) {
                    a2.scene = a(0);
                    a2.searchId = this.f;
                    a2.extraData = b(0);
                    a2.updateWithSimpleAppModel(simpleAppModel);
                }
                downloadButton.a(a2, new ah(this), (d) null, downloadButton, tXDwonloadProcessBar);
            }
            inflate.setTag(R.id.tma_st_smartcard_tag, STConst.ST_PAGE_TYPE_SMARTCARD);
            inflate.setOnClickListener(new ai(this, simpleAppModel, i2));
            linearLayout.addView(inflate, layoutParams);
        }
        return linearLayout;
    }

    /* access modifiers changed from: private */
    public String c(int i2) {
        return c() + ct.a(i2 + 1);
    }

    /* access modifiers changed from: protected */
    public String c() {
        return a.a("18", this.n == null ? 0 : this.n.a());
    }

    /* access modifiers changed from: protected */
    public int d() {
        return com.tencent.assistantv2.st.page.d.f3360a;
    }

    /* access modifiers changed from: protected */
    public String e() {
        if (this.n == null) {
            return null;
        }
        return this.n.d();
    }

    /* access modifiers changed from: protected */
    public long f() {
        if (this.n == null) {
            return 0;
        }
        return this.n.c();
    }

    /* access modifiers changed from: protected */
    public int g() {
        if (this.n == null) {
            return 2000;
        }
        return this.n.b();
    }
}
