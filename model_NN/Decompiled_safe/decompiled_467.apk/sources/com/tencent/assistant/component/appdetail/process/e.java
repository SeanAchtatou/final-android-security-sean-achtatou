package com.tencent.assistant.component.appdetail.process;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.component.PopViewDialog;
import com.tencent.assistant.component.appdetail.process.AppdetailActionUIListener;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.bt;
import com.tencent.assistant.utils.cp;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
public abstract class e {

    /* renamed from: a  reason: collision with root package name */
    protected boolean f930a;
    protected String b;
    protected int c;
    protected long d;
    protected int e;
    protected boolean f = false;
    protected AppdetailActionUIListener g;
    protected a h;
    protected m i;
    protected final int j = R.drawable.icon_wx;
    protected final int k = R.drawable.icon_wx_disable;
    protected final int l = R.drawable.icon_qq;
    protected final int m = R.drawable.icon_qq_disable;
    private boolean n = false;

    public abstract void a();

    public abstract void a(int i2);

    public abstract void a(View view);

    public abstract void a(SimpleAppModel simpleAppModel, long j2, boolean z, PopViewDialog popViewDialog, StatInfo statInfo, Bundle bundle, boolean z2, Context context);

    public abstract void a(boolean z, AppConst.AppState appState, long j2, String str, int i2, int i3, boolean z2);

    public e(a aVar) {
        this.h = aVar;
    }

    public void a(boolean z) {
        this.n = z;
    }

    public boolean f() {
        return this.n;
    }

    public void a(m mVar) {
        this.i = mVar;
    }

    public int g() {
        return 0;
    }

    public void a(AppdetailActionUIListener appdetailActionUIListener) {
        this.g = appdetailActionUIListener;
    }

    public void a(int i2, int i3) {
        if (this.g != null) {
            this.g.a(i2, i3);
        }
    }

    public void a(String str) {
        if (this.g != null && !TextUtils.isEmpty(str)) {
            this.g.a(str, 0);
        }
    }

    public void a(String str, String str2) {
        if (this.g != null && !TextUtils.isEmpty(str)) {
            this.g.a(str, str2);
        }
    }

    public void a(String str, int i2) {
        if (this.g != null && !TextUtils.isEmpty(str)) {
            this.g.a(str, i2);
        }
    }

    public void a(String str, String str2, String str3) {
        if (this.g != null && !TextUtils.isEmpty(str) && !TextUtils.isEmpty(str2) && !TextUtils.isEmpty(str3)) {
            this.g.a(str, str2, str3);
        }
    }

    public void b(int i2) {
        if (this.g != null) {
            this.g.a(i2);
        }
    }

    public void c(int i2) {
        if (this.g != null) {
            this.g.c(i2);
        }
    }

    public void a(Drawable drawable) {
        if (this.g != null) {
            this.g.a(drawable);
        }
    }

    public void d(int i2) {
        if (this.g != null) {
            this.g.b(i2);
        }
    }

    public void b(boolean z) {
        if (this.g != null) {
            this.g.a(z);
        }
    }

    public void c(boolean z) {
        if (this.g != null) {
            this.g.b(z);
        }
    }

    public void d(boolean z) {
        if (this.g != null) {
            this.g.c(z);
        }
    }

    public void a(boolean z, AppdetailActionUIListener.AuthType authType) {
        if (this.g != null) {
            this.g.a(z, authType);
        }
    }

    public void b(String str) {
        if (this.g != null) {
            this.g.a(str);
        }
    }

    public void h() {
        if (this.h != null) {
            this.h.c();
        }
    }

    public STInfoV2 i() {
        if (this.h != null) {
            return this.h.d();
        }
        return null;
    }

    public void b() {
    }

    public void c() {
        XLog.i("YYBShareOganizer", "onResume class = " + getClass().getSimpleName());
    }

    public void d() {
    }

    public void e() {
    }

    public void a(Context context, SimpleAppModel simpleAppModel) {
        b(context, simpleAppModel);
    }

    public void b(Context context, SimpleAppModel simpleAppModel) {
        String string;
        if (simpleAppModel != null && cp.a().a(simpleAppModel.f1634a)) {
            string = cp.a().b(simpleAppModel.f1634a);
            a(context.getResources().getDrawable(cp.a().d(simpleAppModel.f1634a)));
        } else {
            string = context.getString(R.string.download);
        }
        a(" " + string, bt.a(simpleAppModel.k));
    }
}
