package com.tencent.assistant.component;

import android.view.MotionEvent;
import com.tencent.assistant.component.TouchBehavior;

/* compiled from: ProGuard */
public class TouchAnalizer {
    public static final int CLICK_AREA = 400;
    public static final int CLICK_AREA_SEC = 900;
    private TouchBehavior[] analizers = new TouchBehavior[BehaviorType.values().length];
    private TouchBehaviorListener[] listeners = new TouchBehaviorListener[BehaviorType.values().length];

    /* compiled from: ProGuard */
    public enum BehaviorType {
        SINGLE_CLICK,
        DOUBLE_CLICK,
        DRAG,
        SINGLE_DRAG,
        SLASH,
        MULTI_SLASH,
        LONG_CLICK,
        PINCH,
        ROTATE
    }

    public void setListener(BehaviorType behaviorType, TouchBehaviorListener touchBehaviorListener) {
        setListener(behaviorType, touchBehaviorListener, null);
    }

    public void setListener(BehaviorType behaviorType, TouchBehaviorListener touchBehaviorListener, TouchBehavior.TouchBehaviorEventJudger touchBehaviorEventJudger) {
        int ordinal = behaviorType.ordinal();
        this.listeners[ordinal] = touchBehaviorListener;
        if (touchBehaviorListener == null) {
            this.analizers[ordinal] = null;
        } else if (this.analizers[ordinal] == null) {
            this.analizers[ordinal] = createTouchBehavior(behaviorType);
        }
        if (this.analizers[ordinal] != null && touchBehaviorEventJudger != null) {
            this.analizers[ordinal].setJudger(touchBehaviorEventJudger);
        }
    }

    private TouchBehavior createTouchBehavior(BehaviorType behaviorType) {
        switch (dv.f1030a[behaviorType.ordinal()]) {
            case 1:
                return new SingleClickBehavior(this);
            case 2:
                return new DragBehavior(this);
            case 3:
                return new DragBehaviorSinglePoint(this);
            default:
                return null;
        }
    }

    public boolean inputTouchEvent(MotionEvent motionEvent) {
        boolean z = false;
        for (TouchBehavior touchBehavior : this.analizers) {
            if (touchBehavior != null && touchBehavior.onTouchEvent(motionEvent)) {
                z = true;
            }
        }
        return z;
    }

    public void pauseBehavior(BehaviorType behaviorType) {
        if (this.analizers[behaviorType.ordinal()] != null) {
            this.analizers[behaviorType.ordinal()].pause();
        }
    }

    public boolean onBehavior(BehaviorType behaviorType, float f, float f2) {
        return onBehavior(behaviorType, f, f2, -1);
    }

    public boolean onBehavior(BehaviorType behaviorType, float f, float f2, int i) {
        if (this.listeners[behaviorType.ordinal()] != null) {
            return this.listeners[behaviorType.ordinal()].onInvoke(behaviorType, f, f2, i);
        }
        return false;
    }
}
