package com.tencent.assistant.component.smartcard;

import android.content.Intent;
import android.view.View;
import com.tencent.assistant.component.appdetail.CommentDetailTabView;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.activity.AppDetailActivityV5;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class q extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SimpleAppModel f1156a;
    final /* synthetic */ NormalSmartcardCpaAdvertiseItem b;

    q(NormalSmartcardCpaAdvertiseItem normalSmartcardCpaAdvertiseItem, SimpleAppModel simpleAppModel) {
        this.b = normalSmartcardCpaAdvertiseItem;
        this.f1156a = simpleAppModel;
    }

    public void onTMAClick(View view) {
        Intent intent = new Intent(this.b.f1115a, AppDetailActivityV5.class);
        intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, this.b.a(this.b.contextToPageId(this.b.f1115a)));
        intent.putExtra(CommentDetailTabView.PARAMS_SIMPLE_MODEL_INFO, this.f1156a);
        this.b.f1115a.startActivity(intent);
    }

    public STInfoV2 getStInfo() {
        STInfoV2 a2 = this.b.a(STConst.ST_DEFAULT_SLOT, 200);
        if (a2 != null) {
            a2.updateStatusToDetail(this.f1156a);
        }
        return a2;
    }
}
