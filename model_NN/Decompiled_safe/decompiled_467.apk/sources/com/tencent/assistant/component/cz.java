package com.tencent.assistant.component;

import android.content.Intent;
import android.view.View;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.listener.OnTMAClickListener;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistantv2.activity.MainActivity;

/* compiled from: ProGuard */
class cz extends OnTMAClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SecondNavigationTitleView f1008a;

    cz(SecondNavigationTitleView secondNavigationTitleView) {
        this.f1008a = secondNavigationTitleView;
    }

    public void onTMAClick(View view) {
        Intent intent = new Intent(this.f1008a.context, MainActivity.class);
        if (this.f1008a.context instanceof BaseActivity) {
            intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, ((BaseActivity) this.f1008a.context).f());
        }
        this.f1008a.context.startActivity(intent);
    }
}
