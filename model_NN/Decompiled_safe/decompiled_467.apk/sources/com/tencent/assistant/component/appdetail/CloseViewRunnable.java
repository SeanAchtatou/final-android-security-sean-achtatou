package com.tencent.assistant.component.appdetail;

import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.LinearLayout;
import android.widget.ListView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.TouchAnalizer;

/* compiled from: ProGuard */
public class CloseViewRunnable implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    long f901a = 0;
    int b = 0;
    int c;
    int d = 50;
    int e = TouchAnalizer.CLICK_AREA;
    LinearLayout.LayoutParams f;
    View g;
    private long h = -1;
    private Interpolator i = new DecelerateInterpolator();
    public boolean isRunning = false;
    public ListView mListView;

    public CloseViewRunnable(View view) {
        this.g = view;
        this.b = view.getResources().getDimensionPixelSize(R.dimen.hide_installed_apps_area_height);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    public void run() {
        if (this.isRunning) {
            if (this.h == -1) {
                this.h = System.currentTimeMillis();
            } else {
                this.f901a = ((System.currentTimeMillis() - this.h) * 1000) / ((long) this.e);
                this.f901a = Math.max(Math.min(this.f901a, 1000L), 0L);
                this.c = Math.round(((float) this.b) - (((float) this.b) * this.i.getInterpolation(((float) this.f901a) / 1000.0f)));
                this.f = (LinearLayout.LayoutParams) this.g.getLayoutParams();
                this.f.height = this.c;
                this.g.setLayoutParams(this.f);
            }
            if (this.f901a >= 1000) {
                this.isRunning = false;
                if (this.mListView == null) {
                    this.g.setVisibility(8);
                } else {
                    if (this.mListView.getFirstVisiblePosition() == 0) {
                        this.mListView.setSelection(1);
                    }
                    this.f.height = this.mListView.getResources().getDimensionPixelSize(R.dimen.hide_installed_apps_area_height);
                    this.g.setLayoutParams(this.f);
                    this.g.setClickable(true);
                }
                this.h = -1;
                this.f901a = 0;
                this.g.findViewById(R.id.tips).setVisibility(8);
            } else if (this.isRunning) {
                this.g.postDelayed(this, (long) this.d);
            }
        }
    }
}
