package com.tencent.assistant.component.smartcard;

import android.os.Bundle;
import android.view.View;
import com.tencent.assistant.link.b;
import com.tencent.assistant.plugin.PluginActivity;

/* compiled from: ProGuard */
class ao implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f1140a;
    final /* synthetic */ SmartSquareCardWithUser b;

    ao(SmartSquareCardWithUser smartSquareCardWithUser, String str) {
        this.b = smartSquareCardWithUser;
        this.f1140a = str;
    }

    public void onClick(View view) {
        Bundle bundle = new Bundle();
        bundle.putInt(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, this.b.a(this.b.contextToPageId(this.b.f1115a)));
        b.b(this.b.f1115a, this.f1140a, bundle);
    }
}
