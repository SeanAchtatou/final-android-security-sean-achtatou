package com.tencent.assistant.component.appdetail;

import android.content.Intent;
import android.content.res.Resources;
import android.view.View;
import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.pictureprocessor.ShowPictureActivity;
import com.tencent.assistant.component.appdetail.HorizonImageListView;
import com.tencent.assistant.link.b;
import com.tencent.assistant.net.c;
import com.tencent.assistant.protocol.jce.SnapshotsPic;
import com.tencent.assistant.protocol.jce.Video;
import com.tencent.assistant.utils.v;
import java.util.ArrayList;

/* compiled from: ProGuard */
class s implements HorizonImageListView.IHorizonImageListViewListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ HorizonScrollPicViewer f942a;

    s(HorizonScrollPicViewer horizonScrollPicViewer) {
        this.f942a = horizonScrollPicViewer;
    }

    public void onHorizonImageListViewImageClick(int i, String str, View view) {
        int[] iArr = new int[2];
        view.getLocationInWindow(iArr);
        view.getLocationOnScreen(iArr);
        int[] iArr2 = {iArr[0], iArr[1], view.getWidth(), view.getHeight()};
        if (i < this.f942a.g.size()) {
            String str2 = "tpmast://videoplay?" + ((Video) this.f942a.g.get(i)).b + "&" + "from_detail" + "=1";
            if (!c.a()) {
                Toast.makeText(this.f942a.getContext(), (int) R.string.disconnected, 4).show();
            } else if (c.d()) {
                b.b(this.f942a.f908a, str2);
            } else {
                t tVar = new t(this, str2);
                Resources resources = this.f942a.getContext().getResources();
                tVar.hasTitle = false;
                tVar.contentRes = resources.getString(R.string.detail_video_play_tips);
                tVar.lBtnTxtRes = resources.getString(R.string.detail_video_play_ignore);
                tVar.rBtnTxtRes = resources.getString(R.string.detail_video_play_continue);
                v.a(tVar);
            }
        } else {
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            for (int i2 = 0; i2 < this.f942a.f.size(); i2++) {
                arrayList.add(((SnapshotsPic) this.f942a.f.get(i2)).f2343a);
                if (this.f942a.c) {
                    arrayList2.add(((SnapshotsPic) this.f942a.f.get(i2)).b);
                } else {
                    arrayList2.add(((SnapshotsPic) this.f942a.f.get(i2)).c);
                }
            }
            Intent intent = new Intent(this.f942a.f908a, ShowPictureActivity.class);
            intent.putExtra("imagePos", iArr2);
            intent.putStringArrayListExtra("picUrls", arrayList);
            intent.putExtra("startPos", i);
            intent.putStringArrayListExtra("thumbnails", arrayList2);
            if (this.f942a.i != null) {
                this.f942a.i.showPicture(true);
            }
            this.f942a.f908a.startActivity(intent);
        }
    }
}
