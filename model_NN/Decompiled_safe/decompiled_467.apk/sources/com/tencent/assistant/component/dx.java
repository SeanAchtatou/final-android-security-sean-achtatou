package com.tencent.assistant.component;

import android.view.View;
import android.widget.ExpandableListView;

/* compiled from: ProGuard */
class dx implements ExpandableListView.OnGroupClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ UpdateListView f1032a;

    dx(UpdateListView updateListView) {
        this.f1032a = updateListView;
    }

    public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long j) {
        int unused = this.f1032a.groupExpandPostion = i;
        return true;
    }
}
