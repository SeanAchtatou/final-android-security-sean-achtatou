package com.tencent.assistant.component.txscrollview;

import android.view.View;
import com.tencent.assistant.component.txscrollview.TXRefreshScrollViewBase;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;

/* compiled from: ProGuard */
class e implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TXGetMoreListView f1201a;

    e(TXGetMoreListView tXGetMoreListView) {
        this.f1201a = tXGetMoreListView;
    }

    public void onClick(View view) {
        if (this.f1201a.mRefreshListViewListener != null && this.f1201a.mGetMoreRefreshState == TXRefreshScrollViewBase.RefreshState.RESET) {
            this.f1201a.mRefreshListViewListener.onTXRefreshListViewRefresh(TXScrollViewBase.ScrollState.ScrollState_FromEnd);
        }
    }
}
