package com.tencent.assistant.component;

import android.content.Intent;
import android.view.View;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.appdetail.CommentDetailTabView;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistantv2.activity.AppDetailActivityV5;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class f extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppIconView f1049a;

    f(AppIconView appIconView) {
        this.f1049a = appIconView;
    }

    public void onTMAClick(View view) {
        if (this.f1049a.iconClickListener != null) {
            this.f1049a.iconClickListener.onClick();
            return;
        }
        Intent intent = new Intent(this.f1049a.mContext, AppDetailActivityV5.class);
        if (this.f1049a.mContext instanceof BaseActivity) {
            intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, ((BaseActivity) this.f1049a.mContext).f());
        }
        intent.putExtra(CommentDetailTabView.PARAMS_SIMPLE_MODEL_INFO, this.f1049a.mAppModel);
        intent.putExtra("statInfo", this.f1049a.statInfo);
        this.f1049a.mContext.startActivity(intent);
    }

    public STInfoV2 getStInfo() {
        if (this.f1049a.statInfo == null || !(this.f1049a.statInfo instanceof STInfoV2)) {
            return null;
        }
        this.f1049a.statInfo.updateStatus(this.f1049a.mAppModel);
        return (STInfoV2) this.f1049a.statInfo;
    }
}
