package com.tencent.assistant.component.smartcard;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.link.b;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class an extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f1139a;
    final /* synthetic */ SmartSquareCard b;

    an(SmartSquareCard smartSquareCard, String str) {
        this.b = smartSquareCard;
        this.f1139a = str;
    }

    public void onTMAClick(View view) {
        b.b(this.b.getContext(), this.f1139a);
    }

    public STInfoV2 getStInfo() {
        return this.b.a("03_001", 200);
    }
}
