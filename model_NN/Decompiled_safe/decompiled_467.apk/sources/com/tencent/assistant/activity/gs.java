package com.tencent.assistant.activity;

import android.view.View;
import android.widget.TextView;
import com.tencent.assistant.component.appdetail.HorizonImageListView;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import java.util.ArrayList;

/* compiled from: ProGuard */
class gs extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ StartPopWindowActivity f615a;

    gs(StartPopWindowActivity startPopWindowActivity) {
        this.f615a = startPopWindowActivity;
    }

    public void onTMAClick(View view) {
        boolean z = false;
        if (this.f615a.E != null && this.f615a.E.getCount() != 0) {
            ArrayList<Boolean> c = this.f615a.E.c();
            if (this.f615a.B.isSelected()) {
                for (int i = 0; i < c.size(); i++) {
                    if (c.get(i).booleanValue()) {
                        this.f615a.E.a(i);
                    }
                }
            } else {
                for (int i2 = 0; i2 < c.size(); i2++) {
                    if (!c.get(i2).booleanValue()) {
                        this.f615a.E.a(i2);
                    }
                }
            }
            TextView h = this.f615a.B;
            if (!this.f615a.B.isSelected()) {
                z = true;
            }
            h.setSelected(z);
            this.f615a.j();
        }
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f615a.J, 200);
        buildSTInfo.slotId = a.a(HorizonImageListView.TMA_ST_HORIZON_IMAGE_TAG, "001");
        return buildSTInfo;
    }
}
