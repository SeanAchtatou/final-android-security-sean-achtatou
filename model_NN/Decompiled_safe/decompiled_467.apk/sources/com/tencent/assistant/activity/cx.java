package com.tencent.assistant.activity;

import android.support.v4.view.ViewPager;
import com.tencent.assistantv2.activity.ay;

/* compiled from: ProGuard */
public class cx implements ViewPager.OnPageChangeListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GameRankActivity f465a;

    public cx(GameRankActivity gameRankActivity) {
        this.f465a = gameRankActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.component.TabBarView.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.tencent.assistantv2.component.TabBarView.a(android.content.Context, android.util.AttributeSet):void
      com.tencent.assistantv2.component.TabBarView.a(int, float):void
      com.tencent.assistantv2.component.TabBarView.a(int, boolean):void */
    public void onPageSelected(int i) {
        this.f465a.w.a(i, true);
        if (!(this.f465a.v == i || this.f465a.u.a(this.f465a.v) == null)) {
            ((ay) this.f465a.u.a(this.f465a.v)).C();
        }
        ((ay) this.f465a.u.a(i)).I();
        int unused = this.f465a.v = i;
        this.f465a.d(i);
    }

    public void onPageScrolled(int i, float f, int i2) {
        this.f465a.w.a(i, f);
    }

    public void onPageScrollStateChanged(int i) {
    }
}
