package com.tencent.assistant.activity;

import android.os.Handler;
import android.os.Message;
import com.tencent.assistant.module.cz;

/* renamed from: com.tencent.assistant.activity.do  reason: invalid class name */
/* compiled from: ProGuard */
class Cdo extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ HelperFAQActivity f530a;

    Cdo(HelperFAQActivity helperFAQActivity) {
        this.f530a = helperFAQActivity;
    }

    public void handleMessage(Message message) {
        switch (message.what) {
            case 0:
                this.f530a.z.updateFeedbackViewState(cz.a().b());
                break;
        }
        super.handleMessage(message);
    }
}
