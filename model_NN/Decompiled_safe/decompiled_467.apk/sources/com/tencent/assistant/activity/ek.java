package com.tencent.assistant.activity;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.module.u;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class ek extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ej f553a;

    ek(ej ejVar) {
        this.f553a = ejVar;
    }

    public void onTMAClick(View view) {
        this.f553a.f552a.v();
    }

    public STInfoV2 getStInfo() {
        return STInfoBuilder.buildSTInfo(this.f553a.f552a, this.f553a.f552a.G, "03_001", a.a(u.d(this.f553a.f552a.G)), a.a(u.d(this.f553a.f552a.G), this.f553a.f552a.G));
    }
}
