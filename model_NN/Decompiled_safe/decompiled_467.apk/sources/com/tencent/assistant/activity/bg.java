package com.tencent.assistant.activity;

import android.view.KeyEvent;
import android.view.View;

/* compiled from: ProGuard */
class bg implements View.OnKeyListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ BaseActivity f422a;

    bg(BaseActivity baseActivity) {
        this.f422a = baseActivity;
    }

    public boolean onKey(View view, int i, KeyEvent keyEvent) {
        if ((i != 82 && i != 4) || this.f422a.v == null || !this.f422a.v.isShowing()) {
            return false;
        }
        this.f422a.v.dismiss();
        return false;
    }
}
