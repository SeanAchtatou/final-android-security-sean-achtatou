package com.tencent.assistant.activity.debug;

import android.view.View;
import android.widget.Toast;
import com.tencent.assistant.plugin.QReaderClient;
import com.tencent.assistant.plugin.SimpleLoginInfo;
import com.tencent.assistant.plugin.mgr.f;

/* compiled from: ProGuard */
class as implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ QReaderPluginDebugActivity f493a;

    as(QReaderPluginDebugActivity qReaderPluginDebugActivity) {
        this.f493a = qReaderPluginDebugActivity;
    }

    public void onClick(View view) {
        SimpleLoginInfo userLoginInfo = QReaderClient.getInstance().getUserLoginInfo();
        if (userLoginInfo != null) {
            f.a().a(this.f493a, userLoginInfo.uin, 2);
        } else {
            Toast.makeText(this.f493a, "没有登录qq无法充值", 0).show();
        }
    }
}
