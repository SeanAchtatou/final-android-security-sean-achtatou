package com.tencent.assistant.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.view.Menu;
import android.view.View;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.Global;
import com.tencent.assistant.activity.debug.DActivity;
import com.tencent.assistant.activity.debug.ServerAdressSettingActivity;
import com.tencent.assistant.adapter.Cdo;
import com.tencent.assistant.adapter.dv;
import com.tencent.assistant.component.SwitchButton;
import com.tencent.assistant.component.txscrollview.TXExpandableListView;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.login.d;
import com.tencent.assistant.module.ee;
import com.tencent.assistant.protocol.jce.AppSecretUserProfile;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ba;
import com.tencent.assistant.utils.t;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import com.tencent.assistantv2.model.ItemElement;
import com.tencent.beacon.event.a;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/* compiled from: ProGuard */
public class SettingActivity extends BaseActivity implements UIEventListener {
    private static int[] x = {EventDispatcherEnum.UI_EVENT_LOGIN_CANCEL, EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS, EventDispatcherEnum.UI_EVENT_LOGIN_FAIL, EventDispatcherEnum.UI_EVENT_SETTING_LOGIN_CANCEL, EventDispatcherEnum.UI_EVENT_GET_USERINFO_SUCCESS, EventDispatcherEnum.UI_EVENT_GET_USERINFO_FAIL, EventDispatcherEnum.UI_EVENT_SETUSERPROFILE_STATUS_CHANGED_SUCC, EventDispatcherEnum.UI_EVENT_SETUSERPROFILE_STATUS_CHANGED_FAIL, EventDispatcherEnum.UI_EVENT_SELFUPDATE_UPDATE, EventDispatcherEnum.UI_EVENT_SERVER_ENVIRONMENT_CHANGE};
    private Context n;
    private SecondNavigationTitleViewV5 t;
    private TXExpandableListView u;
    /* access modifiers changed from: private */
    public Cdo v;
    private boolean w = true;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_setting);
        this.n = this;
        this.v = new Cdo(this.n);
        v();
        x();
        i();
        if (d.a().j()) {
            ee.a().b();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.v != null && !this.w && !this.v.c) {
            this.v.notifyDataSetChanged();
        }
        this.w = false;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        j();
    }

    private void i() {
        for (int addUIEventListener : x) {
            AstApp.i().k().addUIEventListener(addUIEventListener, this);
        }
    }

    private void j() {
        for (int removeUIEventListener : x) {
            AstApp.i().k().removeUIEventListener(removeUIEventListener, this);
        }
    }

    private void v() {
        if (!t.y()) {
            ArrayList arrayList = new ArrayList();
            arrayList.add(new ItemElement(this.n.getString(R.string.setting_float_window_item_title), null, 1, 16, 0));
            this.v.a("floatwindow_setting", (int) R.string.setting_group_float_window, -1, arrayList);
        }
        ArrayList arrayList2 = new ArrayList();
        arrayList2.add(new ItemElement(this.n.getString(R.string.setting_thumbnail_item_title), this.n.getString(R.string.setting_thumbnail_item_des), 1, 0, 0));
        arrayList2.add(new ItemElement(this.n.getString(R.string.setting_auto_install_item_title), null, 1, 4, 0));
        arrayList2.add(new ItemElement(this.n.getString(R.string.setting_wise_udate_download_item_title), this.n.getString(R.string.setting_wise_udate_download_item_des), 1, 1, 0));
        arrayList2.add(new ItemElement(this.n.getString(R.string.setting_download_location_item_title), this.n.getString(R.string.setting_download_location_item_desc), 0, 17, 0));
        this.v.a("download_setting", (int) R.string.setting_group_download, -1, arrayList2);
        ArrayList arrayList3 = new ArrayList();
        arrayList3.add(new ItemElement(this.n.getString(R.string.setting_root_install_item_title), this.n.getString(R.string.setting_root_install_item_des), 1, 3, 0));
        arrayList3.add(new ItemElement(this.n.getString(R.string.setting_auto_del_package_item_title), null, 1, 5, 0));
        this.v.a("install_setting", (int) R.string.setting_group_install, -1, arrayList3);
        ArrayList arrayList4 = new ArrayList();
        arrayList4.add(new ItemElement(this.n.getString(R.string.setting_message_tip_item_title), this.n.getString(R.string.setting_message_tip_item_des), 0, 7, 0));
        arrayList4.add(new ItemElement(this.n.getString(R.string.setting_privacy_protection_item_title), this.n.getString(R.string.setting_privacy_protection_item_des), 1, 8, 0));
        if (Global.isDev()) {
            arrayList4.add(new ItemElement(this.n.getString(R.string.setting_server_item_title) + ":" + Global.getServerAddressName(), null, 0, 9, 0));
            arrayList4.add(new ItemElement(this.n.getString(R.string.setting_debug_item_title), null, 0, 10, 0));
        }
        this.v.a("other_setting", (int) R.string.setting_group_other, -1, arrayList4);
    }

    private void w() {
        this.v.a();
        v();
    }

    private void x() {
        this.t = (SecondNavigationTitleViewV5) findViewById(R.id.title_view);
        this.t.a(this);
        this.t.b(getString(R.string.setting_title));
        this.t.d();
        this.t.c(new fi(this));
        this.u = (TXExpandableListView) findViewById(R.id.list_view);
        this.u.setAdapter(this.v);
        this.u.setDivider(null);
        for (int i = 0; i < this.v.getGroupCount(); i++) {
            this.u.expandGroup(i);
        }
        this.u.setSelector(R.drawable.transparent_selector);
        this.u.setOnGroupClickListener(new fj(this));
    }

    public void a(ItemElement itemElement, View view, int i) {
        boolean z = true;
        switch (itemElement.d) {
            case 7:
                Intent intent = new Intent(this.n, ChildSettingActivity.class);
                List<ItemElement> y = y();
                Bundle bundle = new Bundle();
                bundle.putSerializable("child_setting_page_key", (Serializable) y);
                intent.putExtras(bundle);
                intent.putExtra("child_setting_title", itemElement.f3309a);
                this.n.startActivity(intent);
                return;
            case 9:
                a((Context) this);
                return;
            case 10:
                this.n.startActivity(new Intent(this.n, DActivity.class));
                return;
            case 17:
                HashMap hashMap = new HashMap();
                hashMap.put("B1", Global.getPhoneGuidAndGen());
                hashMap.put("B2", Global.getQUAForBeacon());
                XLog.d("beacon", "beacon report >> DownloadLocationClick. " + hashMap.toString());
                a.a("DownloadLocationClick", true, -1, -1, hashMap, true);
                return;
            default:
                dv dvVar = (dv) view.getTag();
                SwitchButton switchButton = dvVar.g;
                if (dvVar.g.getSwitchState()) {
                    z = false;
                }
                switchButton.updateSwitchStateWithAnim(z);
                ba.a().postDelayed(new fk(this, itemElement, view, i, dvVar), (long) dvVar.g.getAnimationDuration());
                return;
        }
    }

    private void a(Context context) {
        startActivity(new Intent(context, ServerAdressSettingActivity.class));
    }

    private List<ItemElement> y() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new ItemElement(this.n.getString(R.string.setting_appupdate_message_push_item_title), this.n.getString(R.string.setting_appupdate_message_push_item_des), 0, 11, 0));
        arrayList.add(new ItemElement(this.n.getString(R.string.setting_recommend_message_push_item_title), this.n.getString(R.string.setting_recommend_message_push_item_des), 0, 12, 0));
        arrayList.add(new ItemElement(this.n.getString(R.string.setting_phone_manager_push_item_title), this.n.getString(R.string.setting_phone_manager_push_item_des), 0, 13, 0));
        arrayList.add(new ItemElement(this.n.getString(R.string.setting_personal_message_item_title), this.n.getString(R.string.setting_personal_message_item_des), 0, 14, 0));
        arrayList.add(new ItemElement(this.n.getString(R.string.setting_freewifi_protection_item_title), this.n.getString(R.string.setting_freewifi_protection_item_des), 0, 15, 0));
        return arrayList;
    }

    public int f() {
        return STConst.ST_PAGE_SETTING;
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_SELFUPDATE_UPDATE /*1034*/:
            case EventDispatcherEnum.UI_EVENT_GET_USERINFO_SUCCESS /*1077*/:
            case EventDispatcherEnum.UI_EVENT_GET_USERINFO_FAIL /*1078*/:
            case EventDispatcherEnum.UI_EVENT_LOGIN_FAIL /*1082*/:
            case EventDispatcherEnum.UI_EVENT_LOGIN_CANCEL /*1083*/:
            case EventDispatcherEnum.UI_EVENT_SETTING_LOGIN_CANCEL /*1086*/:
            case EventDispatcherEnum.UI_EVENT_SETUSERPROFILE_STATUS_CHANGED_FAIL /*1176*/:
                ba.a().post(new fl(this));
                XLog.i("SettingActivity", "UI_EVENT_LOGIN_FAIL");
                return;
            case EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS /*1081*/:
                XLog.i("SettingActivity", "UI_EVENT_LOGIN_SUCCESS");
                return;
            case EventDispatcherEnum.UI_EVENT_SETUSERPROFILE_STATUS_CHANGED_SUCC /*1175*/:
                com.tencent.assistant.login.a.a.a((AppSecretUserProfile) message.obj, false);
                ba.a().post(new fm(this));
                XLog.i("SettingActivity", "UI_EVENT_SETUSERPROFILE_STATUS_CHANGED_SUCC");
                return;
            case EventDispatcherEnum.UI_EVENT_SERVER_ENVIRONMENT_CHANGE /*1180*/:
                com.tencent.assistant.module.nac.d.a().b();
                w();
                this.v.notifyDataSetChanged();
                return;
            default:
                return;
        }
    }

    public boolean onMenuOpened(int i, Menu menu) {
        return true;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }
}
