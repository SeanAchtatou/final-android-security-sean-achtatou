package com.tencent.assistant.activity;

import android.view.animation.Animation;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
class am implements Animation.AnimationListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ al f401a;

    am(al alVar) {
        this.f401a = alVar;
    }

    public void onAnimationStart(Animation animation) {
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationEnd(Animation animation) {
        this.f401a.f400a.K();
        this.f401a.f400a.n.setEnabled(true);
        this.f401a.f400a.n.setText(this.f401a.f400a.getResources().getString(R.string.app_backup_list));
    }
}
