package com.tencent.assistant.activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.text.SpannableString;
import android.view.KeyEvent;
import android.view.ViewStub;
import android.widget.RelativeLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.Global;
import com.tencent.assistant.component.FooterView;
import com.tencent.assistant.component.NormalErrorPage;
import com.tencent.assistant.component.listview.BigFileListView;
import com.tencent.assistant.component.spaceclean.RubbishItemView;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.spaceclean.SpaceScanManager;
import com.tencent.assistant.model.PluginStartEntry;
import com.tencent.assistant.model.spaceclean.SubRubbishInfo;
import com.tencent.assistant.model.spaceclean.a;
import com.tencent.assistant.module.callback.ak;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.bt;
import com.tencent.assistant.utils.t;
import com.tencent.assistantv2.activity.AssistantTabActivity;
import com.tencent.assistantv2.component.ScaningProgressView;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import com.tencent.assistantv2.component.TxManagerCommContainView;
import com.tencent.assistantv2.st.b.b;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/* compiled from: ProGuard */
public class BigFileCleanActivity extends BaseActivity {
    private ViewStub A = null;
    private NormalErrorPage B = null;
    /* access modifiers changed from: private */
    public ArrayList<a> C = new ArrayList<>();
    /* access modifiers changed from: private */
    public int D = 0;
    /* access modifiers changed from: private */
    public long E = 0;
    /* access modifiers changed from: private */
    public boolean F = false;
    /* access modifiers changed from: private */
    public boolean G = false;
    /* access modifiers changed from: private */
    public boolean H = false;
    private PluginStartEntry I = null;
    /* access modifiers changed from: private */
    public long J = 0;
    /* access modifiers changed from: private */
    public long K = 0;
    /* access modifiers changed from: private */
    public long L = 0;
    /* access modifiers changed from: private */
    public long M = 0;
    private ak N = new bl(this);
    /* access modifiers changed from: private */
    public Handler O = new br(this);
    /* access modifiers changed from: private */
    public Context n;
    private boolean t = false;
    private boolean u = false;
    private SecondNavigationTitleViewV5 v;
    /* access modifiers changed from: private */
    public FooterView w = null;
    /* access modifiers changed from: private */
    public TxManagerCommContainView x;
    /* access modifiers changed from: private */
    public ScaningProgressView y;
    private BigFileListView z;

    static /* synthetic */ int a(BigFileCleanActivity bigFileCleanActivity) {
        int i = bigFileCleanActivity.D;
        bigFileCleanActivity.D = i + 1;
        return i;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_rubbish_clear);
        this.n = this;
        w();
        j();
        i();
        x();
        y();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        if (this.v != null) {
            this.v.l();
        }
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        if (this.v != null) {
            this.v.m();
        }
        super.onPause();
    }

    private void j() {
        SpaceScanManager.a().a(this.N);
    }

    private void v() {
        SpaceScanManager.a().b(this.N);
    }

    private void w() {
        this.v = (SecondNavigationTitleViewV5) findViewById(R.id.title_view);
        this.v.a(this);
        this.v.b(getString(R.string.space_clean_big_file_title));
        this.v.d();
        this.v.c(new bo(this));
        this.x = (TxManagerCommContainView) findViewById(R.id.contentView);
        this.y = new ScaningProgressView(this.n);
        this.z = new BigFileListView(this.n);
        this.z.setHandleToAdapter(this.O);
        this.x.a(this.y);
        this.x.b(this.z);
        this.w = new FooterView(this.n);
        this.x.a(this.w, new RelativeLayout.LayoutParams(-1, -2));
        this.w.updateContent(getString(R.string.rubbish_clear_one_key_delete));
        this.w.setFooterViewEnable(false);
        this.w.setOnFooterViewClickListener(new bp(this));
        this.A = (ViewStub) findViewById(R.id.error_stub);
    }

    private void a(String str, String str2, String str3, String str4, String str5) {
        HashMap hashMap = new HashMap();
        hashMap.put("B1", Global.getQUAForBeacon());
        hashMap.put("B2", Global.getPhoneGuidAndGen());
        hashMap.put("B3", str);
        hashMap.put("B4", str2);
        hashMap.put("B5", str3);
        hashMap.put("B6", str4);
        hashMap.put("B7", str5);
        hashMap.put("B8", Constants.STR_EMPTY);
        hashMap.put("B9", t.g());
        XLog.d("beacon", "beacon report >> cleanLargeFiles. " + hashMap.toString());
        com.tencent.beacon.event.a.a("cleanLargeFiles", true, -1, -1, hashMap, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean
     arg types: [java.lang.String, boolean]
     candidates:
      com.tencent.assistant.m.b(byte, int):void
      com.tencent.assistant.m.b(byte, java.lang.String):void
      com.tencent.assistant.m.b(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.b(java.lang.Long, int):boolean
      com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean */
    public void i() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.t = extras.getBoolean(com.tencent.assistant.b.a.O);
            if (this.r) {
                m.a().b("key_space_clean_last_push_clicked", (Object) true);
            }
            this.I = (PluginStartEntry) extras.getSerializable("dock_plugin");
        }
    }

    private void x() {
        XLog.d("miles", "initData called...");
        RubbishItemView.isDeleting = false;
        this.C.clear();
        this.y.a(0, 1);
        this.x.a();
        this.J = System.currentTimeMillis();
        if (!SpaceScanManager.a().o()) {
            TemporaryThreadManager.get().start(new bq(this));
        }
    }

    private void y() {
        HashMap hashMap = new HashMap();
        hashMap.put("B1", Global.getPhoneGuidAndGen());
        hashMap.put("B2", Global.getQUAForBeacon());
        hashMap.put("B3", t.g());
        XLog.d("beacon", "beacon report >> expose_bigfileclean. " + hashMap.toString());
        com.tencent.beacon.event.a.a("expose_bigfileclean", true, -1, -1, hashMap, true);
    }

    /* access modifiers changed from: private */
    public void z() {
        this.u = true;
        this.L = System.currentTimeMillis();
        TemporaryThreadManager.get().start(new bs(this));
    }

    /* access modifiers changed from: private */
    public void A() {
        TemporaryThreadManager.get().start(new bt(this));
    }

    /* access modifiers changed from: private */
    public boolean a(PackageManager packageManager, String str) {
        try {
            ApplicationInfo applicationInfo = packageManager.getApplicationInfo(str, 0);
            if ((applicationInfo.flags & 1) == 0 || (applicationInfo.flags & 128) != 0) {
                return true;
            }
            return false;
        } catch (Throwable th) {
            th.printStackTrace();
            return false;
        }
    }

    /* access modifiers changed from: private */
    public long a(a aVar) {
        long j = 0;
        if (this.C == null || this.C.size() <= 0) {
            return 0;
        }
        Iterator<a> it = this.C.iterator();
        while (true) {
            long j2 = j;
            if (!it.hasNext()) {
                return j2;
            }
            a next = it.next();
            if (aVar != null && aVar.b.equals(next.b)) {
                next.c = aVar.c;
                next.a(aVar.f);
            }
            j = j2 + next.c;
        }
    }

    /* access modifiers changed from: private */
    public ArrayList<String> B() {
        long j;
        long j2;
        long j3;
        long j4;
        long j5;
        this.E = 0;
        ArrayList<String> arrayList = new ArrayList<>();
        if (this.C == null || this.C.size() <= 0) {
            j = 0;
            j2 = 0;
            j3 = 0;
            j4 = 0;
            j5 = 0;
        } else {
            Iterator<a> it = this.C.iterator();
            long j6 = 0;
            long j7 = 0;
            long j8 = 0;
            long j9 = 0;
            long j10 = 0;
            while (it.hasNext()) {
                a next = it.next();
                Iterator<SubRubbishInfo> it2 = next.f.iterator();
                long j11 = j6;
                long j12 = j7;
                long j13 = j8;
                long j14 = j9;
                long j15 = j10;
                while (it2.hasNext()) {
                    SubRubbishInfo next2 = it2.next();
                    if (next2.d) {
                        arrayList.addAll(next2.f);
                        this.E = this.E + next2.c;
                        switch (bn.f428a[next2.f1673a.ordinal()]) {
                            case 1:
                                j14 += next2.c;
                                break;
                            case 2:
                                j15 += next2.c;
                                break;
                            case 3:
                                j13 += next2.c;
                                break;
                            case 4:
                                j12 += next2.c;
                                break;
                            case 5:
                                j11 += next2.c;
                                break;
                        }
                        it2.remove();
                    }
                }
                if (next.f.isEmpty()) {
                    it.remove();
                }
                j6 = j11;
                j7 = j12;
                j8 = j13;
                j10 = j15;
                j9 = j14;
            }
            j = j9;
            j2 = j10;
            j3 = j7;
            j4 = j8;
            j5 = j6;
        }
        DecimalFormat decimalFormat = new DecimalFormat("#0.00");
        a(decimalFormat.format(((((double) j) * 1.0d) / 1024.0d) / 1024.0d), decimalFormat.format(((((double) j2) * 1.0d) / 1024.0d) / 1024.0d), decimalFormat.format(((((double) j4) * 1.0d) / 1024.0d) / 1024.0d), decimalFormat.format(((((double) j3) * 1.0d) / 1024.0d) / 1024.0d), decimalFormat.format(((((double) j5) * 1.0d) / 1024.0d) / 1024.0d));
        return arrayList;
    }

    /* access modifiers changed from: private */
    public void a(long j, boolean z2) {
        if (z2) {
            b(bt.c(j));
        } else {
            b(1);
        }
    }

    private void b(String str) {
        STInfoV2 C2 = C();
        this.x.a(getString(R.string.apkmgr_clear_success), new SpannableString(String.format(getString(R.string.rubbish_clear_big_file_delete_tips), str)));
        float c = (float) t.c();
        float d = (float) t.d();
        if (!(d == 0.0f || c == 0.0f)) {
            float f = c / d;
            XLog.d("miles", "SpaceCleanActivity >> internal avaliable memory percent is " + f);
            if (f < 0.2f) {
                this.x.a(R.drawable.icon_space_clean_xiaobao, getString(R.string.rubbish_skip_to_app_uninstall_tip), 20, 20, 4);
                this.x.a(this, InstalledAppManagerActivity.class, (PluginStartEntry) null);
                C2.slotId = "03_001";
                k.a(C2);
                c(getString(R.string.soft_admin));
                return;
            }
        }
        if (this.F && this.I != null) {
            this.x.a(R.drawable.icon_space_clean_xiaobao, getString(R.string.rubbish_skip_to_accelerate_plugin_tip), 20, 20, 3);
            this.x.a(this, this.I);
            C2.slotId = "03_001";
            k.a(C2);
            c(getString(R.string.mobile_accelerate_title));
        }
    }

    private void c(String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("B1", Global.getPhoneGuidAndGen());
        hashMap.put("B2", Global.getQUAForBeacon());
        hashMap.put("B3", t.g());
        hashMap.put("B4", getString(R.string.space_clean_big_file_title));
        hashMap.put("B5", str);
        XLog.d("beacon", "beacon report >> featureLinkExp. " + hashMap.toString());
        com.tencent.beacon.event.a.a("featureLinkExp", true, -1, -1, hashMap, true);
    }

    private STInfoV2 C() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this, 100);
        buildSTInfo.scene = STConst.ST_PAGE_RUBBISH_CLEAR_FINISH_STEWARD;
        b.getInstance().exposure(buildSTInfo);
        return buildSTInfo;
    }

    private void b(int i) {
        if (this.B == null) {
            this.A.inflate();
            this.B = (NormalErrorPage) findViewById(R.id.error);
        }
        this.B.setErrorType(i);
        if (i == 1) {
            this.B.setErrorHint(getResources().getString(R.string.rubbish_clear_empty_tips));
            this.B.setErrorImage(R.drawable.emptypage_pic_02);
            this.B.setErrorHintTextColor(getResources().getColor(R.color.common_listiteminfo));
            this.B.setErrorHintTextSize(getResources().getDimension(R.dimen.appadmin_empty_page_text_size));
            this.B.setErrorTextVisibility(8);
            this.B.setErrorHintVisibility(0);
            this.B.setFreshButtonVisibility(8);
        }
        this.x.setVisibility(8);
        this.w.setVisibility(8);
        this.B.setVisibility(0);
    }

    /* access modifiers changed from: private */
    public void a(long j, boolean z2, int i) {
        if (z2) {
            this.y.a(j);
        } else if (i == 1) {
            this.y.b(j);
            this.y.c();
            this.O.post(new bu(this, i));
        } else if (i == 2) {
            this.y.a(j);
            this.O.postDelayed(new bw(this), 1000);
        } else {
            this.y.b(j);
            this.O.post(new bm(this));
        }
    }

    /* access modifiers changed from: private */
    public void b(boolean z2) {
        if (z2) {
            this.z.setVisibility(8);
        } else if (this.C != null && !this.C.isEmpty()) {
            this.z.refreshData(this.C);
            this.z.setVisibility(0);
            if (this.B != null) {
                this.B.setVisibility(8);
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(long j) {
        String c = bt.c(j);
        String string = getString(R.string.rubbish_clear_one_key_delete);
        if (j > 0) {
            this.w.setFooterViewEnable(true);
            this.w.updateContent(string, " " + String.format(getString(R.string.rubbish_clear_one_key_delete_extra), c));
            return;
        }
        this.w.setFooterViewEnable(false);
        this.w.updateContent(string);
    }

    public int f() {
        return STConst.ST_PAGE_BIG_FILE_CLEAN_STEWARD;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean
     arg types: [java.lang.String, boolean]
     candidates:
      com.tencent.assistant.m.b(byte, int):void
      com.tencent.assistant.m.b(byte, java.lang.String):void
      com.tencent.assistant.m.b(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.b(java.lang.Long, int):boolean
      com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean */
    /* access modifiers changed from: protected */
    public void onDestroy() {
        v();
        com.tencent.assistant.utils.b.a();
        if (this.t && this.u) {
            m.a().b("key_space_clean_has_run_clean", (Object) true);
        }
        super.onDestroy();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (4 != i) {
            return super.onKeyDown(i, keyEvent);
        }
        D();
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: private */
    public void D() {
        if (!this.r) {
            finish();
            return;
        }
        Intent intent = new Intent(this, AssistantTabActivity.class);
        intent.setFlags(67108864);
        intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, f());
        intent.putExtra(com.tencent.assistant.b.a.G, true);
        startActivity(intent);
        this.r = false;
        finish();
        XLog.i("miles", "BigFileCleanActivity >> key back finish");
    }
}
