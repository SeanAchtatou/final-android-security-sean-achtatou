package com.tencent.assistant.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.widget.RelativeLayout;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.LoadingView;
import com.tencent.assistant.component.NormalErrorRecommendPage;
import com.tencent.assistant.component.UpdateIgnoreListView;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.manager.be;
import com.tencent.assistant.module.u;
import com.tencent.assistant.protocol.jce.StatUpdateManageAction;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;

/* compiled from: ProGuard */
public class UpdateIgnoreListActivity extends BaseActivity implements UIEventListener {
    private String A = null;
    private Context n = this;
    private AstApp t;
    private SecondNavigationTitleViewV5 u;
    private RelativeLayout v;
    private NormalErrorRecommendPage w;
    private LoadingView x;
    private UpdateIgnoreListView y;
    private StatUpdateManageAction z = null;

    public int f() {
        return STConst.ST_PAGE_UPDATE_IGNORE;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_updateignorelist);
        j();
        this.t.k().addUIEventListener(1016, this);
        this.t.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_UPDATE_LIST_CHANGED, this);
        this.t.k().addUIEventListener(1019, this);
        this.t.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_UPDATE_IGNORE_LIST_DELETE, this);
        this.t.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_ADD, this);
        this.t.k().addUIEventListener(1013, this);
    }

    private void j() {
        this.z = new StatUpdateManageAction();
        this.t = AstApp.i();
        this.u = (SecondNavigationTitleViewV5) findViewById(R.id.title_view);
        this.v = (RelativeLayout) findViewById(R.id.layout_container);
        this.w = (NormalErrorRecommendPage) findViewById(R.id.error_page);
        this.x = (LoadingView) findViewById(R.id.loading_view);
        this.u.d(false);
        this.u.a(this);
        this.u.i();
        this.u.d();
        this.w.setErrorType(10);
        if (u.b(false) > 0) {
            this.x.setVisibility(8);
        }
        this.y = new UpdateIgnoreListView(this.n, this.z);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.A = extras.getString(STConst.ST_PUSH_TO_UPDATE_KEY);
        }
        this.y.initData(this.A);
        this.v.addView(this.y);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        i();
        this.y.notifyChange();
        this.u.l();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.y.mUpdateIgnoreListAdapter.b();
        super.onPause();
        this.u.m();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.t.k().removeUIEventListener(1016, this);
        this.t.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_UPDATE_LIST_CHANGED, this);
        this.t.k().removeUIEventListener(1019, this);
        this.t.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_UPDATE_IGNORE_LIST_DELETE, this);
        this.t.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_ADD, this);
        this.t.k().removeUIEventListener(1013, this);
        this.y.mUpdateIgnoreListAdapter.c();
        super.onDestroy();
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case 1013:
            case EventDispatcherEnum.UI_EVENT_APP_UPDATE_LIST_CHANGED /*1018*/:
            case 1019:
            case EventDispatcherEnum.UI_EVENT_APP_UPDATE_IGNORE_LIST_DELETE /*1020*/:
                break;
            case EventDispatcherEnum.UI_EVENT_APP_STATE_UNINSTALL /*1014*/:
            case EventDispatcherEnum.UI_EVENT_CHECKUPDATE_FAIL /*1017*/:
            default:
                return;
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_ADD /*1015*/:
            case 1016:
                this.y.mUpdateIgnoreListAdapter.a();
                break;
        }
        this.y.refreshData();
        i();
        be.a().a(u.i());
        if (this.y.mUpdateIgnoreListAdapter.c != null) {
            this.y.mUpdateIgnoreListAdapter.c.dismiss();
        }
    }

    public void i() {
        int b = u.b(false);
        if (b > 0) {
            this.u.b(getResources().getString(R.string.app_update_ignore));
            this.u.c("(" + b + ")");
            return;
        }
        this.n.startActivity(new Intent(this.n, UpdateListActivity.class));
    }
}
