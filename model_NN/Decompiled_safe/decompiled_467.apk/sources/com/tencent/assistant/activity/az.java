package com.tencent.assistant.activity;

import android.content.DialogInterface;
import android.view.KeyEvent;

/* compiled from: ProGuard */
class az implements DialogInterface.OnKeyListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppTreasureBoxActivity f414a;

    az(AppTreasureBoxActivity appTreasureBoxActivity) {
        this.f414a = appTreasureBoxActivity;
    }

    public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
        if (i != 4) {
            return false;
        }
        if (keyEvent.getAction() != 1) {
            return true;
        }
        if (this.f414a.A != null) {
            this.f414a.A.dismiss();
        }
        if (this.f414a.x == null) {
            return true;
        }
        this.f414a.x.setVisibility(0);
        return true;
    }
}
