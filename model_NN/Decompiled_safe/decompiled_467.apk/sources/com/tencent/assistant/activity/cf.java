package com.tencent.assistant.activity;

import android.widget.AbsListView;
import com.tencent.assistant.component.invalidater.ListViewScrollListener;

/* compiled from: ProGuard */
class cf extends ListViewScrollListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DownloadActivity f447a;

    cf(DownloadActivity downloadActivity) {
        this.f447a = downloadActivity;
    }

    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
        if (this.f447a.z != null && this.f447a.v != null) {
            if (i == 0) {
                this.f447a.z.setVisibility(8);
            }
            this.f447a.B();
        }
    }
}
