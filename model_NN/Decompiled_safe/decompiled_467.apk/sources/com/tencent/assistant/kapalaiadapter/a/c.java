package com.tencent.assistant.kapalaiadapter.a;

import android.content.Context;
import android.telephony.TelephonyManager;
import com.tencent.assistant.kapalaiadapter.g;

/* compiled from: ProGuard */
public class c implements j {

    /* renamed from: a  reason: collision with root package name */
    private TelephonyManager[] f1381a = null;

    public TelephonyManager a(int i, Context context) {
        char c;
        if (this.f1381a == null) {
            try {
                this.f1381a = new TelephonyManager[2];
                this.f1381a[0] = (TelephonyManager) g.a("android.telephony.TelephonyManager", "getDefault", new Object[]{0});
                this.f1381a[1] = (TelephonyManager) g.a("android.telephony.TelephonyManager", "getDefault", new Object[]{1});
            } catch (Exception e) {
            }
        }
        if (this.f1381a == null || this.f1381a.length <= i) {
            return null;
        }
        TelephonyManager[] telephonyManagerArr = this.f1381a;
        if (i <= 0) {
            c = 0;
        } else {
            c = 1;
        }
        return telephonyManagerArr[c];
    }

    public String b(int i, Context context) {
        try {
            TelephonyManager a2 = a(i, context);
            if (a2 != null) {
                return a2.getSubscriberId();
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    public String c(int i, Context context) {
        try {
            TelephonyManager a2 = a(i, context);
            if (a2 != null) {
                return a2.getDeviceId();
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }
}
