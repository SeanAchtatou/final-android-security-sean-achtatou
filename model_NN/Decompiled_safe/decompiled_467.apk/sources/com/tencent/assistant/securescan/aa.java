package com.tencent.assistant.securescan;

import android.os.Message;
import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistant.localres.model.LocalApkInfo;

/* compiled from: ProGuard */
class aa extends ApkResCallback.Stub {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ StartScanActivity f2498a;

    private aa(StartScanActivity startScanActivity) {
        this.f2498a = startScanActivity;
    }

    /* synthetic */ aa(StartScanActivity startScanActivity, q qVar) {
        this(startScanActivity);
    }

    public void onInstalledApkDataChanged(LocalApkInfo localApkInfo, int i) {
        if (localApkInfo != null) {
            Message obtainMessage = this.f2498a.ab.obtainMessage();
            obtainMessage.obj = localApkInfo;
            obtainMessage.arg1 = i;
            switch (i) {
                case 1:
                    obtainMessage.what = 1;
                    this.f2498a.ab.removeMessages(1);
                    break;
                case 2:
                    obtainMessage.what = 2;
                    this.f2498a.ab.removeMessages(2);
                    break;
            }
            this.f2498a.ab.sendMessage(obtainMessage);
        }
    }
}
