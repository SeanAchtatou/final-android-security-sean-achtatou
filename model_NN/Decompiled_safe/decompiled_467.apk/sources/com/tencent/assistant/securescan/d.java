package com.tencent.assistant.securescan;

import android.view.animation.ScaleAnimation;

/* compiled from: ProGuard */
class d implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ScaleAnimation f2502a;
    final /* synthetic */ NoRiskFoundPage b;

    d(NoRiskFoundPage noRiskFoundPage, ScaleAnimation scaleAnimation) {
        this.b = noRiskFoundPage;
        this.f2502a = scaleAnimation;
    }

    public void run() {
        this.b.b.startAnimation(this.f2502a);
    }
}
