package com.tencent.assistant.securescan;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class g extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SimpleAppModel f2505a;
    final /* synthetic */ int b;
    final /* synthetic */ e c;

    g(e eVar, SimpleAppModel simpleAppModel, int i) {
        this.c = eVar;
        this.f2505a = simpleAppModel;
        this.b = i;
    }

    public void onTMAClick(View view) {
        this.c.a(this.f2505a, this.b, view);
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.c.f, this.f2505a, this.c.a(this.b), 200, null);
        if (buildSTInfo != null) {
            buildSTInfo.updateStatus(this.f2505a);
        }
        return buildSTInfo;
    }
}
