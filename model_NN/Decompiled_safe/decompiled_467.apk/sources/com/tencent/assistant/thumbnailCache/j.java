package com.tencent.assistant.thumbnailCache;

import com.tencent.assistant.protocol.c;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;

/* compiled from: ProGuard */
public class j {

    /* renamed from: a  reason: collision with root package name */
    private DefaultHttpClient f2576a;

    public synchronized DefaultHttpClient a() {
        if (this.f2576a == null) {
            this.f2576a = c.a();
        }
        return this.f2576a;
    }

    public synchronized void b() {
        if (this.f2576a != null) {
            c.a((HttpClient) this.f2576a);
            c.b((HttpClient) this.f2576a);
        } else {
            try {
                this.f2576a = a();
            } catch (OutOfMemoryError e) {
                System.gc();
                try {
                    Thread.sleep(50);
                } catch (Exception e2) {
                }
                this.f2576a = a();
            }
        }
        return;
    }
}
