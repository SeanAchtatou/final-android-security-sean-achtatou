package com.tencent.assistant.download;

import com.tencent.assistant.AppConst;
import com.tencent.assistant.module.u;
import com.tencent.assistantv2.model.b;
import com.tencent.assistantv2.model.d;
import com.tencent.assistantv2.model.h;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public class DownloadInfoWrapper {

    /* renamed from: a  reason: collision with root package name */
    public InfoType f1246a;
    public DownloadInfo b;
    public h c;
    public b d;
    public d e;

    /* compiled from: ProGuard */
    public enum InfoType {
        App,
        Video,
        Book,
        CommonFile
    }

    public DownloadInfoWrapper(DownloadInfo downloadInfo) {
        this.f1246a = InfoType.App;
        this.b = downloadInfo;
    }

    public DownloadInfoWrapper(h hVar) {
        this.f1246a = InfoType.Video;
        this.c = hVar;
    }

    public DownloadInfoWrapper(b bVar) {
        this.f1246a = InfoType.Book;
        this.d = bVar;
    }

    public DownloadInfoWrapper(d dVar) {
        this.f1246a = InfoType.CommonFile;
        this.e = dVar;
    }

    public long a() {
        if (this.f1246a == InfoType.App) {
            return this.b.createTime;
        }
        if (this.f1246a == InfoType.Video) {
            return this.c.e;
        }
        if (this.f1246a == InfoType.Book) {
            return this.d.i;
        }
        if (this.f1246a == InfoType.CommonFile) {
            return this.e.e;
        }
        return 0;
    }

    public long b() {
        if (this.f1246a == InfoType.App) {
            return this.b.downloadEndTime;
        }
        if (this.f1246a == InfoType.Video) {
            return this.c.f;
        }
        if (this.f1246a == InfoType.Book) {
            return this.d.j;
        }
        if (this.f1246a == InfoType.CommonFile) {
            return this.e.f;
        }
        return 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.module.u.a(com.tencent.assistant.download.DownloadInfo, boolean, boolean):com.tencent.assistant.AppConst$AppState
     arg types: [com.tencent.assistant.download.DownloadInfo, int, int]
     candidates:
      com.tencent.assistant.module.u.a(com.tencent.assistant.download.DownloadInfo, com.tencent.assistant.model.j, com.tencent.assistant.plugin.PluginInfo):com.tencent.assistant.AppConst$AppState
      com.tencent.assistant.module.u.a(java.util.ArrayList<com.tencent.assistant.protocol.jce.CardItem>, com.tencent.assistant.module.w, int):java.util.ArrayList<com.tencent.assistant.model.SimpleAppModel>
      com.tencent.assistant.module.u.a(com.tencent.assistant.download.DownloadInfo, boolean, boolean):com.tencent.assistant.AppConst$AppState */
    public boolean c() {
        boolean z = true;
        if (this.f1246a != InfoType.App) {
            return false;
        }
        if (u.a(this.b, true, true) != AppConst.AppState.DOWNLOADED) {
            z = false;
        }
        return z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.module.u.a(com.tencent.assistant.download.DownloadInfo, boolean, boolean):com.tencent.assistant.AppConst$AppState
     arg types: [com.tencent.assistant.download.DownloadInfo, int, int]
     candidates:
      com.tencent.assistant.module.u.a(com.tencent.assistant.download.DownloadInfo, com.tencent.assistant.model.j, com.tencent.assistant.plugin.PluginInfo):com.tencent.assistant.AppConst$AppState
      com.tencent.assistant.module.u.a(java.util.ArrayList<com.tencent.assistant.protocol.jce.CardItem>, com.tencent.assistant.module.w, int):java.util.ArrayList<com.tencent.assistant.model.SimpleAppModel>
      com.tencent.assistant.module.u.a(com.tencent.assistant.download.DownloadInfo, boolean, boolean):com.tencent.assistant.AppConst$AppState */
    public boolean d() {
        boolean z = true;
        if (this.f1246a != InfoType.App) {
            return false;
        }
        if (u.a(this.b, true, true) != AppConst.AppState.INSTALLED) {
            z = false;
        }
        return z;
    }

    public String e() {
        if (this.f1246a == InfoType.App) {
            return this.b.name;
        }
        if (this.f1246a == InfoType.Video) {
            return this.c.n;
        }
        if (this.f1246a == InfoType.Book) {
            return this.d.f3315a;
        }
        if (this.f1246a == InfoType.CommonFile) {
            return this.e.g();
        }
        return Constants.STR_EMPTY;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass() || !(obj instanceof DownloadInfoWrapper)) {
            return false;
        }
        DownloadInfoWrapper downloadInfoWrapper = (DownloadInfoWrapper) obj;
        if (this.f1246a == InfoType.App && downloadInfoWrapper.f1246a == InfoType.App && this.b != null && downloadInfoWrapper.b != null) {
            return this.b.downloadTicket.equals(downloadInfoWrapper.b.downloadTicket);
        }
        if (this.f1246a == InfoType.Video && downloadInfoWrapper.f1246a == InfoType.Video && this.c != null && downloadInfoWrapper.c != null) {
            return this.c.c.equals(downloadInfoWrapper.c.c);
        }
        if (this.f1246a == InfoType.Book && downloadInfoWrapper.f1246a == InfoType.Book && this.d != null && downloadInfoWrapper.d != null) {
            return this.d.c.equals(downloadInfoWrapper.d.c);
        }
        if (this.f1246a != InfoType.CommonFile || downloadInfoWrapper.f1246a != InfoType.CommonFile || this.e == null || downloadInfoWrapper.e == null) {
            return false;
        }
        return this.e.c.equals(downloadInfoWrapper.e.c);
    }

    public int hashCode() {
        int hashCode = this.f1246a.hashCode();
        if (this.f1246a == InfoType.App) {
            return (hashCode * 31) + this.b.downloadTicket.hashCode();
        }
        if (this.f1246a == InfoType.Video) {
            return (hashCode * 31) + this.c.c.hashCode();
        }
        if (this.f1246a == InfoType.Book) {
            return (hashCode * 31) + this.d.c.hashCode();
        }
        if (this.f1246a == InfoType.CommonFile) {
            return (hashCode * 31) + this.e.c.hashCode();
        }
        return hashCode;
    }
}
