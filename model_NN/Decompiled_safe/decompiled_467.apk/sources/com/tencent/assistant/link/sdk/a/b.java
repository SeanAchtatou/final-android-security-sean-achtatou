package com.tencent.assistant.link.sdk.a;

import java.io.UnsupportedEncodingException;

/* compiled from: ProGuard */
public class b {

    /* renamed from: a  reason: collision with root package name */
    static final /* synthetic */ boolean f1401a = (!b.class.desiredAssertionStatus());

    public static String a(byte[] bArr, int i) {
        try {
            return new String(b(bArr, i), "US-ASCII");
        } catch (UnsupportedEncodingException e) {
            throw new AssertionError(e);
        }
    }

    public static byte[] b(byte[] bArr, int i) {
        return a(bArr, 0, bArr.length, i);
    }

    public static byte[] a(byte[] bArr, int i, int i2, int i3) {
        int i4;
        d dVar = new d(i3, null);
        int i5 = (i2 / 3) * 4;
        if (!dVar.d) {
            switch (i2 % 3) {
                case 1:
                    i5 += 2;
                    break;
                case 2:
                    i5 += 3;
                    break;
            }
        } else if (i2 % 3 > 0) {
            i5 += 4;
        }
        if (dVar.e && i2 > 0) {
            int i6 = ((i2 - 1) / 57) + 1;
            if (dVar.f) {
                i4 = 2;
            } else {
                i4 = 1;
            }
            i5 += i4 * i6;
        }
        dVar.f1402a = new byte[i5];
        dVar.a(bArr, i, i2, true);
        if (f1401a || dVar.b == i5) {
            return dVar.f1402a;
        }
        throw new AssertionError();
    }

    private b() {
    }
}
