package com.tencent.assistant.link;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import com.tencent.assistant.utils.bg;

/* compiled from: ProGuard */
public class LinkProxyActivity extends Activity {

    /* renamed from: a  reason: collision with root package name */
    private Uri f1393a = null;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f1393a = getIntent().getData();
        if (this.f1393a != null) {
            a();
        }
        finish();
    }

    private void a() {
        b.a(this, this.f1393a, bg.a(getIntent()));
    }
}
