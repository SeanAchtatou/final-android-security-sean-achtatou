package com.tencent.assistant.protocol.jce;

import java.io.Serializable;

/* compiled from: ProGuard */
public final class ClientWifiCmd implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    public static final ClientWifiCmd f2034a = new ClientWifiCmd(0, 0, "UserIdentity");
    public static final ClientWifiCmd b = new ClientWifiCmd(1, 1, "ConnOption");
    public static final ClientWifiCmd c = new ClientWifiCmd(2, 2, "FileOption");
    static final /* synthetic */ boolean d = (!ClientWifiCmd.class.desiredAssertionStatus());
    private static ClientWifiCmd[] e = new ClientWifiCmd[3];
    private int f;
    private String g = new String();

    public String toString() {
        return this.g;
    }

    private ClientWifiCmd(int i, int i2, String str) {
        this.g = str;
        this.f = i2;
        e[i] = this;
    }
}
