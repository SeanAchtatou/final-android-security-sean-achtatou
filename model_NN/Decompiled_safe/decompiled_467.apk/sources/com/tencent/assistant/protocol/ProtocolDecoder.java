package com.tencent.assistant.protocol;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.protocol.jce.PkgRsp;
import com.tencent.assistant.protocol.jce.Response;

/* compiled from: ProGuard */
public interface ProtocolDecoder {
    Response decodeResponse(byte[] bArr);

    JceStruct decodeResponseBody(JceStruct jceStruct, byte[] bArr);

    PkgRsp decodeResponseV2(byte[] bArr);
}
