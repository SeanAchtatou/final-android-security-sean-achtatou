package com.tencent.assistant.login;

import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.Global;
import com.tencent.assistant.component.appdetail.AppBarTabView;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.js.k;
import com.tencent.assistant.login.model.MoblieQIdentityInfo;
import com.tencent.assistant.login.model.c;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.st.STConstAction;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.t;
import com.tencent.assistantv2.st.page.STInfoV2;
import java.util.HashMap;

/* compiled from: ProGuard */
public abstract class a {

    /* renamed from: a  reason: collision with root package name */
    protected Bundle f1454a;
    private String b = "LoginBaseEngine";

    public abstract void d();

    public abstract AppConst.LoginEgnineType e();

    public abstract void g();

    /* access modifiers changed from: protected */
    public void a(String str) {
        if (!TextUtils.isEmpty(str)) {
            d.a().a(new c(str, false));
        }
    }

    public void a(Bundle bundle) {
        this.f1454a = bundle;
        if (this.f1454a != null) {
            this.f1454a.putInt(AppConst.KEY_LOGIN_ANIM, 0);
            if (AstApp.m() != null) {
                this.f1454a.putInt("login_scene", AstApp.m().f());
                this.f1454a.putInt("login_source_scene", AstApp.m().p());
                return;
            }
            this.f1454a.putInt("login_scene", 0);
            this.f1454a.putInt("login_source_scene", 0);
        }
    }

    public Bundle a() {
        return this.f1454a;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.login.a.a(boolean, java.lang.String):void
     arg types: [int, java.lang.String]
     candidates:
      com.tencent.assistant.login.a.a(com.tencent.assistant.login.model.MoblieQIdentityInfo, boolean):void
      com.tencent.assistant.login.a.a(boolean, java.lang.String):void */
    /* access modifiers changed from: protected */
    public void a(String str, long j, byte[] bArr, byte[] bArr2, byte[] bArr3, String str2, boolean z) {
        XLog.d(this.b, "onLoginSuccess");
        if (!(bArr == null || bArr2 == null || bArr3 == null)) {
            d.a().a((com.tencent.assistant.login.model.a) new MoblieQIdentityInfo(str, bArr, bArr3, bArr2, str2, j));
            a(z);
        }
        if (!z) {
            b(STConstAction.ACTION_HIT_LOGIN_SUCCESS_QQ);
            a(true, "success");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.login.a.a(boolean, java.lang.String):void
     arg types: [int, java.lang.String]
     candidates:
      com.tencent.assistant.login.a.a(com.tencent.assistant.login.model.MoblieQIdentityInfo, boolean):void
      com.tencent.assistant.login.a.a(boolean, java.lang.String):void */
    /* access modifiers changed from: protected */
    public void a(MoblieQIdentityInfo moblieQIdentityInfo, boolean z) {
        XLog.d(this.b, "onLoginSuccess");
        if (moblieQIdentityInfo != null) {
            if (!z) {
                d.a().a((com.tencent.assistant.login.model.a) moblieQIdentityInfo);
            } else {
                d.a().a(moblieQIdentityInfo);
            }
            a(z);
        }
        if (!z) {
            b(STConstAction.ACTION_HIT_LOGIN_SUCCESS_QQ);
            a(true, "success");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.login.a.a(boolean, java.lang.String):void
     arg types: [int, java.lang.String]
     candidates:
      com.tencent.assistant.login.a.a(com.tencent.assistant.login.model.MoblieQIdentityInfo, boolean):void
      com.tencent.assistant.login.a.a(boolean, java.lang.String):void */
    /* access modifiers changed from: protected */
    public void b() {
        XLog.d(this.b, "onLoginFail");
        d.a().f();
        a((int) EventDispatcherEnum.UI_EVENT_LOGIN_FAIL);
        a(false, "fail");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.login.a.a(boolean, java.lang.String):void
     arg types: [int, java.lang.String]
     candidates:
      com.tencent.assistant.login.a.a(com.tencent.assistant.login.model.MoblieQIdentityInfo, boolean):void
      com.tencent.assistant.login.a.a(boolean, java.lang.String):void */
    /* access modifiers changed from: protected */
    public void c() {
        XLog.d(this.b, "onLoginCancel");
        d.a().f();
        a((int) EventDispatcherEnum.UI_EVENT_LOGIN_CANCEL);
        a(false, "cancel");
    }

    /* access modifiers changed from: protected */
    public void a(int i) {
        Message obtainMessage = AstApp.i().j().obtainMessage(i);
        obtainMessage.arg1 = e().ordinal();
        if (this.f1454a != null) {
            obtainMessage.obj = this.f1454a;
        }
        AstApp.i().j().sendMessage(obtainMessage);
    }

    /* access modifiers changed from: protected */
    public void a(boolean z) {
        Message obtainMessage = AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS);
        obtainMessage.arg1 = e().ordinal();
        obtainMessage.arg2 = z ? 1 : 2;
        if (this.f1454a != null) {
            obtainMessage.obj = this.f1454a;
        }
        AstApp.i().j().sendMessage(obtainMessage);
        k.a(AstApp.i(), "http://www.qq.com", AppBarTabView.AUTH_TYPE_ALL);
    }

    public void f() {
    }

    private void b(int i) {
        int i2;
        int i3 = 2000;
        if (this.f1454a != null) {
            int i4 = this.f1454a.getInt("login_scene");
            if (i4 == 0) {
                i4 = 2000;
            }
            int i5 = this.f1454a.getInt("login_source_scene");
            if (i5 != 0) {
                i3 = i5;
            }
            i2 = i3;
            i3 = i4;
        } else {
            i2 = 2000;
        }
        STInfoV2 sTInfoV2 = new STInfoV2(i3, STConst.ST_DEFAULT_SLOT, i2, STConst.ST_DEFAULT_SLOT, i);
        if (this.f1454a != null && this.f1454a.containsKey(AppConst.KEY_FROM_TYPE)) {
            sTInfoV2.extraData = String.valueOf(this.f1454a.getInt(AppConst.KEY_FROM_TYPE));
        }
        com.tencent.assistantv2.st.k.a(sTInfoV2);
    }

    private void a(boolean z, String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("B1", str);
        hashMap.put("B2", Global.getPhoneGuidAndGen());
        hashMap.put("B3", Global.getQUAForBeacon());
        hashMap.put("B4", t.g());
        if (this instanceof b) {
            com.tencent.beacon.event.a.a("Login_QQ", z, -1, -1, hashMap, true);
            XLog.d("beacon", "beacon report >> event: Login_QQ, params : " + hashMap.toString());
        } else if (this instanceof com.tencent.assistant.login.b.a) {
            com.tencent.beacon.event.a.a("Login_WX", z, -1, -1, hashMap, true);
            XLog.d("beacon", "beacon report >> event: Login_WX, params : " + hashMap.toString());
        }
    }
}
