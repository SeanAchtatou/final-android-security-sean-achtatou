package com.tencent.assistant.appbakcup;

import android.view.View;
import android.widget.TextView;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;

/* compiled from: ProGuard */
class a extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TextView f829a;
    final /* synthetic */ int b;
    final /* synthetic */ BackupAppListAdapter c;

    a(BackupAppListAdapter backupAppListAdapter, TextView textView, int i) {
        this.c = backupAppListAdapter;
        this.f829a = textView;
        this.b = i;
    }

    public void onTMAClick(View view) {
        this.f829a.setSelected(!this.f829a.isSelected());
        this.c.e[this.b] = this.f829a.isSelected();
        d a2 = this.c.a();
        if (a2 != null) {
            this.c.c.a(a2.f832a, a2.b, a2.c);
        } else {
            this.c.c.a(0, false, "0M");
        }
    }
}
