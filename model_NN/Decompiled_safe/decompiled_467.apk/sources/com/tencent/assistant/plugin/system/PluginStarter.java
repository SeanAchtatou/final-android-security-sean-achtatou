package com.tencent.assistant.plugin.system;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import com.tencent.assistant.Global;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.plugin.PluginInfo;
import com.tencent.assistant.plugin.mgr.d;
import com.tencent.assistant.plugin.proxy.PluginProxyActivity;
import com.tencent.assistant.utils.FileUtil;
import com.tencent.assistant.utils.bg;

/* compiled from: ProGuard */
public class PluginStarter extends Activity implements UIEventListener {

    /* renamed from: a  reason: collision with root package name */
    private boolean f1956a = false;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        a();
    }

    private void a() {
        this.f1956a = true;
        Intent intent = getIntent();
        String a2 = bg.a(intent, "plugin_package");
        intent.removeExtra("plugin_package");
        String a3 = bg.a(intent, "plugin_activity");
        intent.removeExtra("plugin_activity");
        intent.setFlags(0);
        if (TextUtils.isEmpty(a2) || TextUtils.isEmpty(a3)) {
            finish();
            return;
        }
        PluginInfo a4 = d.b().a(a2);
        if (a4 == null) {
            if (Global.isLite()) {
                FileUtil.writeToAppData("plugin.ini", "com.assistant.accelerate:0", 1);
            }
            finish();
            return;
        }
        PluginProxyActivity.a(this, a4.getPackageName(), a4.getVersion(), a3, a4.getInProcess(), intent, null);
        finish();
    }

    public void handleUIEvent(Message message) {
        if (!this.f1956a) {
            a();
        }
    }
}
