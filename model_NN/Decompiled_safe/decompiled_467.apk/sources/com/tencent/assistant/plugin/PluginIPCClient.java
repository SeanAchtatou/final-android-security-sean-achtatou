package com.tencent.assistant.plugin;

import android.content.Context;
import android.content.Intent;
import com.qq.taf.jce.JceInputStream;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.plugin.a.e;
import com.tencent.assistant.sdk.PluginSupportService;
import com.tencent.assistant.sdk.SDKClient;
import com.tencent.assistant.sdk.param.jce.IPCResponse;
import com.tencent.assistant.sdk.param.jce.PluginLoginInfo;
import com.tencent.assistant.sdk.param.jce.QueryLoginInfoRequest;
import com.tencent.assistant.sdk.param.jce.QueryLoginInfoResponse;
import com.tencent.assistant.sdk.param.jce.QueryLoginStateRequest;
import com.tencent.assistant.sdk.param.jce.QueryLoginStateResponse;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public class PluginIPCClient extends SDKClient {
    public static final String LOGIN_ADDTION_INFO_GET_USER_INFO = "get_user_info";
    public static final String LOGIN_ADDTION_INFO_LOGIN = "start_login_process";

    /* renamed from: a  reason: collision with root package name */
    private LoginStateCallback f1932a;
    private boolean b = false;

    /* access modifiers changed from: protected */
    public void a(byte[] bArr) {
        JceInputStream jceInputStream = new JceInputStream(bArr);
        IPCResponse iPCResponse = new IPCResponse();
        iPCResponse.readFrom(jceInputStream);
        JceStruct a2 = e.a(iPCResponse);
        if (this.f1932a != null && a2 != null) {
            if (a2 instanceof QueryLoginInfoResponse) {
                UserLoginInfo userLoginInfo = new UserLoginInfo();
                userLoginInfo.setState(((QueryLoginInfoResponse) a2).a());
                PluginLoginInfo b2 = ((QueryLoginInfoResponse) a2).b();
                if (b2 != null) {
                    userLoginInfo.setNickName(b2.f2483a);
                    userLoginInfo.setPic(b2.b());
                    userLoginInfo.setA2(b2.c());
                    userLoginInfo.setUin(b2.d());
                }
                this.f1932a.gotUserInfo(userLoginInfo);
            } else if (a2 instanceof QueryLoginStateResponse) {
                UserStateInfo userStateInfo = new UserStateInfo(((QueryLoginStateResponse) a2).f2489a);
                PluginLoginInfo a3 = ((QueryLoginStateResponse) a2).a();
                if (a3 != null) {
                    UserLoginInfo userLoginInfo2 = new UserLoginInfo();
                    userLoginInfo2.setUin(a3.d());
                    userLoginInfo2.setNickName(a3.a());
                    userLoginInfo2.setA2(a3.c());
                    userLoginInfo2.setPic(a3.b());
                    userLoginInfo2.setState(userStateInfo.getStateChangeType());
                    userStateInfo.setUserLoginInfo(userLoginInfo2);
                }
                this.f1932a.gotUserStateChange(userStateInfo);
            }
        }
    }

    /* access modifiers changed from: protected */
    public Intent a(Context context) {
        return new Intent(context, PluginSupportService.class);
    }

    /* access modifiers changed from: protected */
    public String a() {
        return "__plugin_ipc__userinfo";
    }

    public void sendQueryLoginInfo(String str) {
        if (str == null) {
            str = Constants.STR_EMPTY;
        }
        send(e.a(new QueryLoginInfoRequest(str)));
    }

    public void sendQueryLoginState() {
        this.b = true;
        send(e.a(new QueryLoginStateRequest()));
    }

    /* access modifiers changed from: protected */
    public void b() {
        sendQueryLoginState();
    }

    public LoginStateCallback getLoginStateCallback() {
        return this.f1932a;
    }

    public void setLoginStateCallback(LoginStateCallback loginStateCallback) {
        this.f1932a = loginStateCallback;
    }
}
