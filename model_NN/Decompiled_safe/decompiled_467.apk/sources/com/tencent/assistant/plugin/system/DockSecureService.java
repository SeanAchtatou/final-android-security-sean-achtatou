package com.tencent.assistant.plugin.system;

import com.tencent.assistant.plugin.PluginInfo;

/* compiled from: ProGuard */
public class DockSecureService extends BaseAppService {
    public int a() {
        return 0;
    }

    public String a(PluginInfo pluginInfo) {
        if (pluginInfo != null) {
            return pluginInfo.getExtendServiceImpl(PluginInfo.META_DATA_DOCK_SEC_SERVICE);
        }
        return null;
    }
}
