package com.tencent.assistant.plugin.system;

import android.content.Intent;
import com.tencent.assistant.plugin.PluginInfo;

/* compiled from: ProGuard */
public class DockDeepAccelerationService extends BaseAppService {
    public int a() {
        return 0;
    }

    public String a(PluginInfo pluginInfo) {
        if (pluginInfo != null) {
            return pluginInfo.getExtendServiceImpl(PluginInfo.META_DATA_DEEP_ACC_SERVICE);
        }
        return null;
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        super.onStartCommand(intent, i, i2);
        return 2;
    }
}
