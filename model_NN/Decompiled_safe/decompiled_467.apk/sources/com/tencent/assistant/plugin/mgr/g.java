package com.tencent.assistant.plugin.mgr;

import android.content.Intent;
import android.os.Bundle;
import com.qq.AppService.AstApp;
import com.tencent.assistant.js.k;
import com.tencent.assistant.plugin.PluginInfo;
import com.tencent.assistant.plugin.proxy.PluginProxyActivity;

/* compiled from: ProGuard */
class g implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Bundle f1951a;
    final /* synthetic */ f b;

    g(f fVar, Bundle bundle) {
        this.b = fVar;
        this.f1951a = bundle;
    }

    public void run() {
        PluginInfo a2 = d.b().a("com.qqreader");
        Intent intent = new Intent();
        intent.putExtras(this.f1951a);
        PluginProxyActivity.a(AstApp.i(), a2.getPackageName(), a2.getVersion(), "com.qqreader.AppSplashActivity", a2.getInProcess(), intent, null);
        k.a(2);
    }
}
