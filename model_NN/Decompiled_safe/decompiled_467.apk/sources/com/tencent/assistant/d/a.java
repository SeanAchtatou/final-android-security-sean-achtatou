package com.tencent.assistant.d;

import com.tencent.assistant.protocol.jce.LbsCell;
import com.tencent.assistant.protocol.jce.LbsData;
import com.tencent.assistant.protocol.jce.LbsLocation;
import com.tencent.assistant.protocol.jce.LbsWifiMac;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private ArrayList<LbsCell> f1223a;
    private ArrayList<LbsWifiMac> b;
    private LbsLocation c;

    public void a(ArrayList<LbsCell> arrayList) {
        this.f1223a = arrayList;
    }

    public void b(ArrayList<LbsWifiMac> arrayList) {
        this.b = arrayList;
    }

    public LbsData a() {
        try {
            LbsData lbsData = new LbsData();
            if (this.f1223a == null || this.f1223a.size() <= 0) {
                lbsData.a(new ArrayList());
            } else {
                lbsData.a(this.f1223a);
            }
            if (this.b == null || this.b.size() <= 0) {
                lbsData.b(new ArrayList());
            } else {
                lbsData.b(this.b);
            }
            if (this.c != null) {
                lbsData.a(this.c);
                return lbsData;
            }
            lbsData.a(new LbsLocation());
            return lbsData;
        } catch (Exception e) {
            return null;
        }
    }
}
