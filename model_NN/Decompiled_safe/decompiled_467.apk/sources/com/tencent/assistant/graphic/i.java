package com.tencent.assistant.graphic;

import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.utils.FileUtil;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.bj;
import com.tencent.assistant.utils.r;
import java.io.File;
import java.io.FileInputStream;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

/* compiled from: ProGuard */
public class i {
    private static i d;

    /* renamed from: a  reason: collision with root package name */
    private ReferenceQueue<h> f1331a = new ReferenceQueue<>();
    private ConcurrentLinkedQueue<WeakReference<h>> b = new ConcurrentLinkedQueue<>();
    private Map<String, g> c = new ConcurrentHashMap();

    private i() {
    }

    public static synchronized i a() {
        i iVar;
        synchronized (i.class) {
            if (d == null) {
                d = new i();
            }
            iVar = d;
        }
        return iVar;
    }

    public void a(h hVar) {
        if (hVar != null) {
            while (true) {
                Reference<? extends h> poll = this.f1331a.poll();
                if (poll == null) {
                    break;
                }
                this.b.remove(poll);
            }
            Iterator<WeakReference<h>> it = this.b.iterator();
            while (it.hasNext()) {
                if (((h) it.next().get()) == hVar) {
                    return;
                }
            }
            this.b.add(new WeakReference(hVar, this.f1331a));
        }
    }

    /* access modifiers changed from: private */
    public void a(g gVar, e eVar) {
        if (gVar != null && !TextUtils.isEmpty(gVar.f1330a)) {
            this.c.remove(gVar.f1330a);
            Iterator<WeakReference<h>> it = this.b.iterator();
            while (it.hasNext()) {
                h hVar = (h) it.next().get();
                if (hVar != null) {
                    hVar.a(gVar, eVar);
                }
            }
        }
    }

    public boolean a(g gVar) {
        if (gVar == null || TextUtils.isEmpty(gVar.f1330a)) {
            return false;
        }
        if (this.c.get(gVar.f1330a) != null) {
            return true;
        }
        this.c.put(gVar.f1330a, gVar);
        b(gVar);
        return true;
    }

    private void b(g gVar) {
        TemporaryThreadManager.get().start(new j(this, gVar));
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0072 A[SYNTHETIC, Splitter:B:31:0x0072] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0089 A[SYNTHETIC, Splitter:B:40:0x0089] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:16:0x0052=Splitter:B:16:0x0052, B:28:0x006d=Splitter:B:28:0x006d} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(java.lang.String r8, java.lang.String r9) {
        /*
            r7 = this;
            r2 = 0
            r1 = 0
            boolean r0 = android.text.TextUtils.isEmpty(r8)
            if (r0 == 0) goto L_0x000a
            r0 = r1
        L_0x0009:
            return r0
        L_0x000a:
            org.apache.http.impl.client.DefaultHttpClient r3 = com.tencent.assistant.protocol.c.a()     // Catch:{ ClientProtocolException -> 0x0050, IOException -> 0x006b, all -> 0x0085 }
            org.apache.http.client.methods.HttpGet r4 = new org.apache.http.client.methods.HttpGet     // Catch:{ ClientProtocolException -> 0x00a6, IOException -> 0x00a2 }
            r4.<init>(r8)     // Catch:{ ClientProtocolException -> 0x00a6, IOException -> 0x00a2 }
            com.qq.AppService.AstApp r0 = com.qq.AppService.AstApp.i()     // Catch:{ ClientProtocolException -> 0x00a6, IOException -> 0x00a2 }
            java.lang.String r5 = "power"
            java.lang.Object r0 = r0.getSystemService(r5)     // Catch:{ ClientProtocolException -> 0x00a6, IOException -> 0x00a2 }
            android.os.PowerManager r0 = (android.os.PowerManager) r0     // Catch:{ ClientProtocolException -> 0x00a6, IOException -> 0x00a2 }
            r5 = 1
            java.lang.Class r6 = r7.getClass()     // Catch:{ ClientProtocolException -> 0x00a6, IOException -> 0x00a2 }
            java.lang.String r6 = r6.getSimpleName()     // Catch:{ ClientProtocolException -> 0x00a6, IOException -> 0x00a2 }
            android.os.PowerManager$WakeLock r2 = r0.newWakeLock(r5, r6)     // Catch:{ ClientProtocolException -> 0x00a6, IOException -> 0x00a2 }
            r0 = 0
            r2.setReferenceCounted(r0)     // Catch:{ ClientProtocolException -> 0x00a6, IOException -> 0x00a2 }
            r2.acquire()     // Catch:{ ClientProtocolException -> 0x00a6, IOException -> 0x00a2 }
            org.apache.http.HttpResponse r0 = r3.execute(r4)     // Catch:{ ClientProtocolException -> 0x00a6, IOException -> 0x00a2 }
            boolean r0 = r7.a(r0, r9)     // Catch:{ ClientProtocolException -> 0x00a6, IOException -> 0x00a2 }
            if (r3 == 0) goto L_0x0044
            org.apache.http.conn.ClientConnectionManager r1 = r3.getConnectionManager()     // Catch:{ Exception -> 0x00a8 }
            r1.shutdown()     // Catch:{ Exception -> 0x00a8 }
        L_0x0044:
            if (r2 == 0) goto L_0x0009
            boolean r1 = r2.isHeld()
            if (r1 == 0) goto L_0x0009
            r2.release()
            goto L_0x0009
        L_0x0050:
            r0 = move-exception
            r3 = r2
        L_0x0052:
            r0.printStackTrace()     // Catch:{ all -> 0x009e }
            if (r3 == 0) goto L_0x005e
            org.apache.http.conn.ClientConnectionManager r0 = r3.getConnectionManager()     // Catch:{ Exception -> 0x00a4 }
            r0.shutdown()     // Catch:{ Exception -> 0x00a4 }
        L_0x005e:
            if (r2 == 0) goto L_0x0069
            boolean r0 = r2.isHeld()
            if (r0 == 0) goto L_0x0069
            r2.release()
        L_0x0069:
            r0 = r1
            goto L_0x0009
        L_0x006b:
            r0 = move-exception
            r3 = r2
        L_0x006d:
            r0.printStackTrace()     // Catch:{ all -> 0x009e }
            if (r3 == 0) goto L_0x0079
            org.apache.http.conn.ClientConnectionManager r0 = r3.getConnectionManager()     // Catch:{ Exception -> 0x00a0 }
            r0.shutdown()     // Catch:{ Exception -> 0x00a0 }
        L_0x0079:
            if (r2 == 0) goto L_0x0069
            boolean r0 = r2.isHeld()
            if (r0 == 0) goto L_0x0069
            r2.release()
            goto L_0x0069
        L_0x0085:
            r0 = move-exception
            r3 = r2
        L_0x0087:
            if (r3 == 0) goto L_0x0090
            org.apache.http.conn.ClientConnectionManager r1 = r3.getConnectionManager()     // Catch:{ Exception -> 0x009c }
            r1.shutdown()     // Catch:{ Exception -> 0x009c }
        L_0x0090:
            if (r2 == 0) goto L_0x009b
            boolean r1 = r2.isHeld()
            if (r1 == 0) goto L_0x009b
            r2.release()
        L_0x009b:
            throw r0
        L_0x009c:
            r1 = move-exception
            goto L_0x0090
        L_0x009e:
            r0 = move-exception
            goto L_0x0087
        L_0x00a0:
            r0 = move-exception
            goto L_0x0079
        L_0x00a2:
            r0 = move-exception
            goto L_0x006d
        L_0x00a4:
            r0 = move-exception
            goto L_0x005e
        L_0x00a6:
            r0 = move-exception
            goto L_0x0052
        L_0x00a8:
            r1 = move-exception
            goto L_0x0044
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.graphic.i.a(java.lang.String, java.lang.String):boolean");
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r3v0 */
    /* JADX WARN: Type inference failed for: r3v1, types: [java.io.FileOutputStream] */
    /* JADX WARN: Type inference failed for: r3v2, types: [java.io.FileOutputStream] */
    /* JADX WARN: Type inference failed for: r3v3, types: [java.io.InputStream] */
    /* JADX WARN: Type inference failed for: r3v4 */
    /* JADX WARN: Type inference failed for: r3v5 */
    /* JADX WARN: Type inference failed for: r3v6 */
    /* JADX WARN: Type inference failed for: r3v7 */
    /* JADX WARN: Type inference failed for: r3v8 */
    /* JADX WARN: Type inference failed for: r3v10 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0047 A[SYNTHETIC, Splitter:B:27:0x0047] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x004c A[SYNTHETIC, Splitter:B:30:0x004c] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0051 A[SYNTHETIC, Splitter:B:33:0x0051] */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x008d A[SYNTHETIC, Splitter:B:65:0x008d] */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x0092 A[SYNTHETIC, Splitter:B:68:0x0092] */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x0097 A[SYNTHETIC, Splitter:B:71:0x0097] */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x00a7 A[SYNTHETIC, Splitter:B:78:0x00a7] */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x00ac A[SYNTHETIC, Splitter:B:81:0x00ac] */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x00b1 A[SYNTHETIC, Splitter:B:84:0x00b1] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean a(org.apache.http.HttpResponse r8, java.lang.String r9) {
        /*
            r7 = this;
            r3 = 0
            r0 = 0
            if (r8 == 0) goto L_0x000a
            boolean r1 = android.text.TextUtils.isEmpty(r9)
            if (r1 == 0) goto L_0x000b
        L_0x000a:
            return r0
        L_0x000b:
            if (r8 == 0) goto L_0x000a
            org.apache.http.StatusLine r1 = r8.getStatusLine()
            int r1 = r1.getStatusCode()
            r2 = 200(0xc8, float:2.8E-43)
            if (r1 != r2) goto L_0x000a
            r1 = 0
            r2 = 0
            org.apache.http.HttpEntity r5 = r8.getEntity()     // Catch:{ IllegalStateException -> 0x00e3, IOException -> 0x0085, all -> 0x00a2 }
            if (r5 == 0) goto L_0x0070
            java.io.InputStream r4 = r5.getContent()     // Catch:{ IllegalStateException -> 0x00e8, IOException -> 0x00db, all -> 0x00ce }
            java.io.File r1 = new java.io.File     // Catch:{ IllegalStateException -> 0x00ed, IOException -> 0x00de }
            r1.<init>(r9)     // Catch:{ IllegalStateException -> 0x00ed, IOException -> 0x00de }
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ IllegalStateException -> 0x00ed, IOException -> 0x00de }
            r2.<init>(r1)     // Catch:{ IllegalStateException -> 0x00ed, IOException -> 0x00de }
            r1 = 2048(0x800, float:2.87E-42)
            byte[] r1 = new byte[r1]     // Catch:{ IllegalStateException -> 0x003f, IOException -> 0x00e0, all -> 0x00d3 }
        L_0x0033:
            int r3 = r4.read(r1)     // Catch:{ IllegalStateException -> 0x003f, IOException -> 0x00e0, all -> 0x00d3 }
            r6 = -1
            if (r3 == r6) goto L_0x005a
            r6 = 0
            r2.write(r1, r6, r3)     // Catch:{ IllegalStateException -> 0x003f, IOException -> 0x00e0, all -> 0x00d3 }
            goto L_0x0033
        L_0x003f:
            r1 = move-exception
            r3 = r4
            r4 = r5
        L_0x0042:
            r1.printStackTrace()     // Catch:{ all -> 0x00d6 }
            if (r3 == 0) goto L_0x004a
            r3.close()     // Catch:{ Exception -> 0x00c2 }
        L_0x004a:
            if (r2 == 0) goto L_0x004f
            r2.close()     // Catch:{ Exception -> 0x00c4 }
        L_0x004f:
            if (r4 == 0) goto L_0x000a
            r4.consumeContent()     // Catch:{ IOException -> 0x0055 }
            goto L_0x000a
        L_0x0055:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x000a
        L_0x005a:
            r0 = 1
            if (r4 == 0) goto L_0x0060
            r4.close()     // Catch:{ Exception -> 0x00ba }
        L_0x0060:
            if (r2 == 0) goto L_0x0065
            r2.close()     // Catch:{ Exception -> 0x00bc }
        L_0x0065:
            if (r5 == 0) goto L_0x000a
            r5.consumeContent()     // Catch:{ IOException -> 0x006b }
            goto L_0x000a
        L_0x006b:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x000a
        L_0x0070:
            if (r3 == 0) goto L_0x0075
            r1.close()     // Catch:{ Exception -> 0x00be }
        L_0x0075:
            if (r3 == 0) goto L_0x007a
            r2.close()     // Catch:{ Exception -> 0x00c0 }
        L_0x007a:
            if (r5 == 0) goto L_0x000a
            r5.consumeContent()     // Catch:{ IOException -> 0x0080 }
            goto L_0x000a
        L_0x0080:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x000a
        L_0x0085:
            r1 = move-exception
            r4 = r3
            r5 = r3
        L_0x0088:
            r1.printStackTrace()     // Catch:{ all -> 0x00d1 }
            if (r4 == 0) goto L_0x0090
            r4.close()     // Catch:{ Exception -> 0x00c6 }
        L_0x0090:
            if (r3 == 0) goto L_0x0095
            r3.close()     // Catch:{ Exception -> 0x00c8 }
        L_0x0095:
            if (r5 == 0) goto L_0x000a
            r5.consumeContent()     // Catch:{ IOException -> 0x009c }
            goto L_0x000a
        L_0x009c:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x000a
        L_0x00a2:
            r0 = move-exception
            r4 = r3
            r5 = r3
        L_0x00a5:
            if (r4 == 0) goto L_0x00aa
            r4.close()     // Catch:{ Exception -> 0x00ca }
        L_0x00aa:
            if (r3 == 0) goto L_0x00af
            r3.close()     // Catch:{ Exception -> 0x00cc }
        L_0x00af:
            if (r5 == 0) goto L_0x00b4
            r5.consumeContent()     // Catch:{ IOException -> 0x00b5 }
        L_0x00b4:
            throw r0
        L_0x00b5:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00b4
        L_0x00ba:
            r1 = move-exception
            goto L_0x0060
        L_0x00bc:
            r1 = move-exception
            goto L_0x0065
        L_0x00be:
            r1 = move-exception
            goto L_0x0075
        L_0x00c0:
            r1 = move-exception
            goto L_0x007a
        L_0x00c2:
            r1 = move-exception
            goto L_0x004a
        L_0x00c4:
            r1 = move-exception
            goto L_0x004f
        L_0x00c6:
            r1 = move-exception
            goto L_0x0090
        L_0x00c8:
            r1 = move-exception
            goto L_0x0095
        L_0x00ca:
            r1 = move-exception
            goto L_0x00aa
        L_0x00cc:
            r1 = move-exception
            goto L_0x00af
        L_0x00ce:
            r0 = move-exception
            r4 = r3
            goto L_0x00a5
        L_0x00d1:
            r0 = move-exception
            goto L_0x00a5
        L_0x00d3:
            r0 = move-exception
            r3 = r2
            goto L_0x00a5
        L_0x00d6:
            r0 = move-exception
            r5 = r4
            r4 = r3
            r3 = r2
            goto L_0x00a5
        L_0x00db:
            r1 = move-exception
            r4 = r3
            goto L_0x0088
        L_0x00de:
            r1 = move-exception
            goto L_0x0088
        L_0x00e0:
            r1 = move-exception
            r3 = r2
            goto L_0x0088
        L_0x00e3:
            r1 = move-exception
            r2 = r3
            r4 = r3
            goto L_0x0042
        L_0x00e8:
            r1 = move-exception
            r2 = r3
            r4 = r5
            goto L_0x0042
        L_0x00ed:
            r1 = move-exception
            r2 = r3
            r3 = r4
            r4 = r5
            goto L_0x0042
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.graphic.i.a(org.apache.http.HttpResponse, java.lang.String):boolean");
    }

    /* access modifiers changed from: private */
    public String a(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        if (str.startsWith("http")) {
            return FileUtil.getGifDir() + File.separator + b(str);
        }
        return str;
    }

    /* access modifiers changed from: private */
    public e a(String str, int i, int i2) {
        int i3;
        int i4;
        int i5;
        float f = r.f();
        if (((double) f) < 0.6d) {
            i3 = 5;
        } else {
            i3 = (int) (f * ((float) i));
        }
        e eVar = new e();
        eVar.f1328a = false;
        eVar.d = str;
        try {
            c cVar = new c(new FileInputStream(str), null, true, 0);
            cVar.a(i3);
            if (cVar.a() == 1) {
                a aVar = new a();
                aVar.a(1);
                aVar.b(i2);
                BitmapDrawable bitmapDrawable = new BitmapDrawable(BitmapFactory.decodeFile(str));
                bitmapDrawable.setTargetDensity(AstApp.i().getResources().getDisplayMetrics());
                aVar.addFrame(bitmapDrawable, 1000);
                eVar.c = aVar;
                eVar.f1328a = true;
            } else {
                if (cVar.b() == -1) {
                    i4 = cVar.c();
                } else {
                    i4 = 0;
                }
                if (i4 > i3) {
                    i5 = i4 / i3;
                } else {
                    i5 = 0;
                }
                if (i5 > 4) {
                    i5 = 4;
                }
                c cVar2 = new c(new FileInputStream(str), null, false, i5);
                cVar2.a();
                if (cVar2.b() == -1) {
                    int c2 = cVar2.c();
                    if (c2 == 0) {
                        eVar.f1328a = false;
                    } else {
                        a aVar2 = new a();
                        aVar2.a(c2);
                        aVar2.b(i2);
                        f c3 = cVar2.c(0);
                        for (int i6 = 0; i6 < c2 && c3 != null; i6++) {
                            BitmapDrawable bitmapDrawable2 = new BitmapDrawable(c3.f1329a);
                            bitmapDrawable2.setTargetDensity(AstApp.i().getResources().getDisplayMetrics());
                            aVar2.addFrame(bitmapDrawable2, c3.b <= 0 ? 100 : c3.b);
                            c3 = c3.c;
                        }
                        aVar2.setOneShot(false);
                        eVar.c = aVar2;
                        eVar.f1328a = true;
                    }
                } else {
                    eVar.f1328a = false;
                }
            }
        } catch (Throwable th) {
            eVar.b = true;
            th.printStackTrace();
        }
        return eVar;
    }

    private String b(String str) {
        return bj.b(str);
    }
}
