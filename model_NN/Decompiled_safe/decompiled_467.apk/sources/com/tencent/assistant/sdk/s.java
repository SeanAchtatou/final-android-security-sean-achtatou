package com.tencent.assistant.sdk;

import android.content.Context;
import com.tencent.assistant.sdk.param.a;
import com.tencent.assistant.sdk.param.jce.IPCHead;
import com.tencent.assistant.sdk.param.jce.IPCRequest;

/* compiled from: ProGuard */
public class s {
    public static r a(Context context, byte[] bArr, String str) {
        IPCHead a2;
        IPCRequest a3 = a.a(bArr);
        if (a3 == null || (a2 = a3.a()) == null) {
            return null;
        }
        switch (a2.b) {
            case 1:
                return new o(context, a3);
            case 2:
            case 3:
            case 8:
            default:
                return null;
            case 4:
                return new v(context, a3);
            case 5:
                return new w(context, a3);
            case 6:
                return new b(context, a3);
            case 7:
                return new c(context, a3);
            case 9:
                return u.a(context, a3, str);
        }
    }
}
