package com.tencent.assistant.sdk;

import android.content.Intent;
import android.os.IBinder;

/* compiled from: ProGuard */
public class PluginSupportService extends SDKSupportService {

    /* renamed from: a  reason: collision with root package name */
    a f2450a;

    /* access modifiers changed from: protected */
    public byte a() {
        return 2;
    }

    public IBinder onBind(Intent intent) {
        this.f2450a = new a();
        this.f2450a.a();
        return super.onBind(intent);
    }

    public boolean onUnbind(Intent intent) {
        if (this.f2450a != null) {
            this.f2450a.b();
        }
        return super.onUnbind(intent);
    }
}
