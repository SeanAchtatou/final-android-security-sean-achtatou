package com.tencent.assistant.sdk;

import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.download.SimpleDownloadInfo;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.manager.DownloadProxy;
import com.tencent.assistant.plugin.UserStateInfo;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;

/* compiled from: ProGuard */
public class SDKIPCBroadcaster implements UIEventListener {

    /* renamed from: a  reason: collision with root package name */
    private static SDKIPCBroadcaster f2455a;
    private static AstApp b;
    /* access modifiers changed from: private */
    public static ConcurrentHashMap<String, ArrayList<r>> c = new ConcurrentHashMap<>();
    private ConcurrentHashMap<String, n> d = new ConcurrentHashMap<>();
    /* access modifiers changed from: private */
    public ConcurrentHashMap<String, Long> e = new ConcurrentHashMap<>();
    private BroadcastQueue f;
    private Object g = new Object();
    private m h;

    public static synchronized SDKIPCBroadcaster a() {
        SDKIPCBroadcaster sDKIPCBroadcaster;
        synchronized (SDKIPCBroadcaster.class) {
            if (f2455a == null) {
                f2455a = new SDKIPCBroadcaster();
            }
            sDKIPCBroadcaster = f2455a;
        }
        return sDKIPCBroadcaster;
    }

    public SDKIPCBroadcaster() {
        b();
    }

    public void b() {
        this.f = new BroadcastQueue();
        Thread thread = new Thread(this.f, "Thread_SDKIPC");
        thread.setDaemon(true);
        thread.start();
        this.h = new m(this);
        Thread thread2 = new Thread(this.h, "Thread_SDKIPC_Check");
        thread2.setDaemon(true);
        thread2.start();
        b = AstApp.i();
        b.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING, this);
        b.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_PAUSE, this);
        b.k().addUIEventListener(1007, this);
        b.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_SUCC, this);
        b.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_QUEUING, this);
        b.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE, this);
        b.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_START, this);
        b.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_SUCC, this);
        b.k().addUIEventListener(1013, this);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002b A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x005a  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x008d  */
    /* JADX WARNING: Removed duplicated region for block: B:39:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:50:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void handleUIEvent(android.os.Message r7) {
        /*
            r6 = this;
            java.lang.String r0 = ""
            r1 = 0
            java.lang.Object r0 = r7.obj
            boolean r0 = r0 instanceof java.lang.String
            if (r0 == 0) goto L_0x0024
            java.lang.Object r0 = r7.obj
            java.lang.String r0 = (java.lang.String) r0
            boolean r2 = android.text.TextUtils.isEmpty(r0)
            if (r2 != 0) goto L_0x0024
            com.tencent.assistant.manager.DownloadProxy r1 = com.tencent.assistant.manager.DownloadProxy.a()
            com.tencent.assistant.download.DownloadInfo r0 = r1.d(r0)
            if (r0 == 0) goto L_0x0025
            boolean r1 = r0.ignoreState()
            if (r1 == 0) goto L_0x0025
        L_0x0023:
            return
        L_0x0024:
            r0 = r1
        L_0x0025:
            int r1 = r7.what
            switch(r1) {
                case 1003: goto L_0x002b;
                case 1005: goto L_0x0034;
                case 1006: goto L_0x0034;
                case 1007: goto L_0x003e;
                case 1008: goto L_0x002b;
                case 1009: goto L_0x005a;
                case 1025: goto L_0x008d;
                default: goto L_0x002a;
            }
        L_0x002a:
            goto L_0x0023
        L_0x002b:
            if (r0 == 0) goto L_0x0034
            int r1 = r0.errorCode
            if (r1 == 0) goto L_0x0034
            r1 = 0
            r0.errorCode = r1
        L_0x0034:
            boolean r1 = r6.b(r0)
            if (r1 == 0) goto L_0x0023
            r6.a(r0)
            goto L_0x0023
        L_0x003e:
            if (r0 == 0) goto L_0x0023
            boolean r1 = com.tencent.assistant.net.c.a()
            if (r1 != 0) goto L_0x0050
            if (r0 == 0) goto L_0x0050
            int r1 = r0.errorCode
            if (r1 != 0) goto L_0x0050
            r1 = -15
            r0.errorCode = r1
        L_0x0050:
            boolean r1 = r6.b(r0)
            if (r1 == 0) goto L_0x0023
            r6.a(r0)
            goto L_0x0023
        L_0x005a:
            java.lang.Object r0 = r7.obj
            boolean r0 = r0 instanceof com.tencent.assistant.download.DownloadInfo
            if (r0 == 0) goto L_0x0023
            java.lang.Object r0 = r7.obj
            com.tencent.assistant.download.DownloadInfo r0 = (com.tencent.assistant.download.DownloadInfo) r0
            com.tencent.assistant.download.DownloadInfo r1 = r0.clone()
            com.tencent.assistant.localres.ApkResourceManager r2 = com.tencent.assistant.localres.ApkResourceManager.getInstance()
            java.lang.String r3 = r1.packageName
            int r4 = r1.versionCode
            int r5 = r1.grayVersionCode
            com.tencent.assistant.localres.model.LocalApkInfo r2 = r2.getLocalApkInfo(r3, r4, r5)
            if (r2 != 0) goto L_0x0023
            com.tencent.assistant.download.SimpleDownloadInfo$DownloadState r2 = com.tencent.assistant.download.SimpleDownloadInfo.DownloadState.DELETED
            r1.downloadState = r2
            boolean r2 = r6.b(r1)
            if (r2 == 0) goto L_0x0023
            java.util.concurrent.ConcurrentHashMap<java.lang.String, com.tencent.assistant.sdk.n> r2 = r6.d
            java.lang.String r0 = r0.downloadTicket
            r2.remove(r0)
            r6.a(r1)
            goto L_0x0023
        L_0x008d:
            boolean r1 = r6.b(r0)
            if (r1 == 0) goto L_0x0023
            com.tencent.assistant.download.DownloadInfo r1 = r0.clone()
            com.tencent.assistant.download.SimpleDownloadInfo$DownloadState r2 = com.tencent.assistant.download.SimpleDownloadInfo.DownloadState.INSTALLING
            r0.downloadState = r2
            r6.a(r1)
            goto L_0x0023
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.sdk.SDKIPCBroadcaster.handleUIEvent(android.os.Message):void");
    }

    private boolean b(DownloadInfo downloadInfo) {
        if (downloadInfo == null || TextUtils.isEmpty(downloadInfo.hostPackageName) || !c(downloadInfo)) {
            return false;
        }
        return true;
    }

    private boolean c(DownloadInfo downloadInfo) {
        synchronized (this.g) {
            if (this.d.containsKey(downloadInfo.downloadTicket)) {
                n nVar = this.d.get(downloadInfo.downloadTicket);
                SimpleDownloadInfo.DownloadState downloadState = nVar.f2468a;
                long j = nVar.b;
                if (downloadState != SimpleDownloadInfo.DownloadState.DOWNLOADING) {
                    this.d.put(downloadInfo.downloadTicket, new n(this, downloadInfo.downloadState, System.currentTimeMillis(), false));
                    if (downloadState == downloadInfo.downloadState) {
                        return false;
                    }
                    return true;
                } else if (downloadInfo.downloadState != SimpleDownloadInfo.DownloadState.DOWNLOADING) {
                    this.d.put(downloadInfo.downloadTicket, new n(this, downloadInfo.downloadState, System.currentTimeMillis(), false));
                    return true;
                } else if (System.currentTimeMillis() - j <= 1000) {
                    return false;
                } else {
                    this.d.put(downloadInfo.downloadTicket, new n(this, downloadInfo.downloadState, System.currentTimeMillis(), true));
                    return true;
                }
            } else {
                this.d.put(downloadInfo.downloadTicket, new n(this, downloadInfo.downloadState, System.currentTimeMillis(), false));
                return true;
            }
        }
    }

    public void a(DownloadInfo downloadInfo) {
        if (downloadInfo != null) {
            synchronized (this.g) {
                for (String next : c.keySet()) {
                    ArrayList arrayList = new ArrayList();
                    arrayList.addAll(c.get(next));
                    Iterator it = arrayList.iterator();
                    while (it.hasNext()) {
                        r rVar = (r) it.next();
                        if (rVar != null && rVar.a(downloadInfo)) {
                            n nVar = this.d.get(downloadInfo.downloadTicket);
                            if (nVar != null) {
                                rVar.a(downloadInfo.packageName, downloadInfo, nVar.c);
                            } else {
                                rVar.a(downloadInfo.packageName, downloadInfo, false);
                            }
                            this.f.a(new l(this, next, rVar.a(downloadInfo.packageName)));
                        }
                    }
                }
            }
        }
    }

    private void b(String str, r rVar) {
        if (a(str)) {
            this.f.a(new l(this, str, rVar.c()));
        }
    }

    /* access modifiers changed from: private */
    public boolean a(String str) {
        return !TextUtils.isEmpty(str) && str.startsWith("__plugin_ipc_");
    }

    public void a(UserStateInfo userStateInfo) {
        if (userStateInfo != null) {
            synchronized (this.g) {
                for (String next : c.keySet()) {
                    Iterator it = c.get(next).iterator();
                    while (true) {
                        if (it.hasNext()) {
                            r rVar = (r) it.next();
                            if (rVar != null && (rVar instanceof c)) {
                                ((c) rVar).a(userStateInfo);
                                if (a(next)) {
                                    this.f.a(new l(this, next, rVar.c()));
                                    return;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:88:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(java.lang.String r9, com.tencent.assistant.sdk.r r10) {
        /*
            r8 = this;
            r5 = 1
            boolean r1 = r10 instanceof com.tencent.assistant.sdk.b
            if (r1 == 0) goto L_0x0009
            r8.b(r9, r10)
        L_0x0008:
            return
        L_0x0009:
            com.tencent.assistant.sdk.param.jce.IPCBaseParam r1 = r10.b
            if (r1 == 0) goto L_0x003a
            com.tencent.assistant.manager.DownloadProxy r1 = com.tencent.assistant.manager.DownloadProxy.a()
            com.tencent.assistant.sdk.param.jce.IPCBaseParam r2 = r10.b
            java.lang.String r2 = r2.d
            com.tencent.assistant.sdk.param.jce.IPCBaseParam r3 = r10.b
            java.lang.String r3 = r3.c
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            int r3 = r3.intValue()
            r4 = 0
            com.tencent.assistant.download.DownloadInfo r1 = r1.a(r2, r3, r4)
            if (r1 == 0) goto L_0x003a
            com.tencent.assistant.sdk.param.jce.IPCBaseParam r2 = r10.b
            java.lang.String r2 = r2.f2475a
            r1.hostAppId = r2
            java.lang.String r2 = r10.e()
            r1.hostPackageName = r2
            java.lang.String r2 = r10.f()
            r1.hostVersionCode = r2
        L_0x003a:
            com.tencent.assistant.sdk.m r1 = r8.h
            r1.a(r5)
            java.util.concurrent.ConcurrentHashMap<java.lang.String, java.lang.Long> r1 = r8.e
            long r2 = java.lang.System.currentTimeMillis()
            java.lang.Long r2 = java.lang.Long.valueOf(r2)
            r1.put(r9, r2)
            java.lang.Object r4 = r8.g
            monitor-enter(r4)
            java.util.concurrent.ConcurrentHashMap<java.lang.String, java.util.ArrayList<com.tencent.assistant.sdk.r>> r1 = com.tencent.assistant.sdk.SDKIPCBroadcaster.c     // Catch:{ all -> 0x00ca }
            java.lang.Object r1 = r1.get(r9)     // Catch:{ all -> 0x00ca }
            java.util.ArrayList r1 = (java.util.ArrayList) r1     // Catch:{ all -> 0x00ca }
            if (r1 != 0) goto L_0x0133
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ all -> 0x00ca }
            r1.<init>()     // Catch:{ all -> 0x00ca }
            java.util.concurrent.ConcurrentHashMap<java.lang.String, java.util.ArrayList<com.tencent.assistant.sdk.r>> r2 = com.tencent.assistant.sdk.SDKIPCBroadcaster.c     // Catch:{ all -> 0x00ca }
            r2.put(r9, r1)     // Catch:{ all -> 0x00ca }
            r3 = r1
        L_0x0064:
            boolean r1 = r3.isEmpty()     // Catch:{ all -> 0x00ca }
            if (r1 != 0) goto L_0x012f
            int r1 = r10.e     // Catch:{ all -> 0x00ca }
            if (r1 != 0) goto L_0x00da
            com.tencent.assistant.sdk.param.jce.IPCBaseParam r2 = r10.b()     // Catch:{ all -> 0x00ca }
            if (r2 == 0) goto L_0x00d7
            java.util.Iterator r5 = r3.iterator()     // Catch:{ all -> 0x00ca }
        L_0x0078:
            boolean r1 = r5.hasNext()     // Catch:{ all -> 0x00ca }
            if (r1 == 0) goto L_0x00cd
            java.lang.Object r1 = r5.next()     // Catch:{ all -> 0x00ca }
            com.tencent.assistant.sdk.r r1 = (com.tencent.assistant.sdk.r) r1     // Catch:{ all -> 0x00ca }
            com.tencent.assistant.sdk.param.jce.IPCBaseParam r1 = r1.b()     // Catch:{ all -> 0x00ca }
            if (r1 == 0) goto L_0x0078
            java.lang.String r6 = r1.c     // Catch:{ all -> 0x00ca }
            boolean r6 = android.text.TextUtils.isEmpty(r6)     // Catch:{ all -> 0x00ca }
            if (r6 != 0) goto L_0x0078
            java.lang.String r6 = r1.c     // Catch:{ all -> 0x00ca }
            java.lang.String r7 = r2.c     // Catch:{ all -> 0x00ca }
            boolean r6 = r6.equals(r7)     // Catch:{ all -> 0x00ca }
            if (r6 == 0) goto L_0x0078
            java.lang.String r6 = r1.d     // Catch:{ all -> 0x00ca }
            boolean r6 = android.text.TextUtils.isEmpty(r6)     // Catch:{ all -> 0x00ca }
            if (r6 != 0) goto L_0x0078
            java.lang.String r6 = r1.d     // Catch:{ all -> 0x00ca }
            java.lang.String r7 = r2.d     // Catch:{ all -> 0x00ca }
            boolean r6 = r6.equals(r7)     // Catch:{ all -> 0x00ca }
            if (r6 == 0) goto L_0x0078
            java.lang.String r6 = r1.h     // Catch:{ all -> 0x00ca }
            boolean r6 = android.text.TextUtils.isEmpty(r6)     // Catch:{ all -> 0x00ca }
            if (r6 != 0) goto L_0x0078
            java.lang.String r1 = r1.h     // Catch:{ all -> 0x00ca }
            java.lang.String r6 = r2.h     // Catch:{ all -> 0x00ca }
            boolean r1 = r1.equals(r6)     // Catch:{ all -> 0x00ca }
            if (r1 == 0) goto L_0x0078
            java.lang.String r1 = "IPC"
            java.lang.String r2 = "Already has same resolver"
            com.tencent.assistant.utils.XLog.i(r1, r2)     // Catch:{ all -> 0x00ca }
            monitor-exit(r4)     // Catch:{ all -> 0x00ca }
            goto L_0x0008
        L_0x00ca:
            r1 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x00ca }
            throw r1
        L_0x00cd:
            java.lang.String r1 = "IPC"
            java.lang.String r2 = "add resolver succ"
            com.tencent.assistant.utils.XLog.i(r1, r2)     // Catch:{ all -> 0x00ca }
            r3.add(r10)     // Catch:{ all -> 0x00ca }
        L_0x00d7:
            monitor-exit(r4)     // Catch:{ all -> 0x00ca }
            goto L_0x0008
        L_0x00da:
            int r1 = r10.e     // Catch:{ all -> 0x00ca }
            if (r1 != r5) goto L_0x00d7
            boolean r1 = r10 instanceof com.tencent.assistant.sdk.t     // Catch:{ all -> 0x00ca }
            if (r1 == 0) goto L_0x00d7
            r0 = r10
            com.tencent.assistant.sdk.t r0 = (com.tencent.assistant.sdk.t) r0     // Catch:{ all -> 0x00ca }
            r1 = r0
            boolean r1 = r8.a(r1)     // Catch:{ all -> 0x00ca }
            if (r1 == 0) goto L_0x012b
            java.util.Iterator r5 = r3.iterator()     // Catch:{ all -> 0x00ca }
        L_0x00f0:
            boolean r1 = r5.hasNext()     // Catch:{ all -> 0x00ca }
            if (r1 == 0) goto L_0x0120
            java.lang.Object r1 = r5.next()     // Catch:{ all -> 0x00ca }
            com.tencent.assistant.sdk.r r1 = (com.tencent.assistant.sdk.r) r1     // Catch:{ all -> 0x00ca }
            boolean r2 = r1 instanceof com.tencent.assistant.sdk.t     // Catch:{ all -> 0x00ca }
            if (r2 == 0) goto L_0x00f0
            r0 = r1
            com.tencent.assistant.sdk.t r0 = (com.tencent.assistant.sdk.t) r0     // Catch:{ all -> 0x00ca }
            r2 = r0
            boolean r2 = r8.a(r2)     // Catch:{ all -> 0x00ca }
            if (r2 == 0) goto L_0x00f0
            r0 = r10
            com.tencent.assistant.sdk.t r0 = (com.tencent.assistant.sdk.t) r0     // Catch:{ all -> 0x00ca }
            r2 = r0
            int r2 = r2.k     // Catch:{ all -> 0x00ca }
            com.tencent.assistant.sdk.t r1 = (com.tencent.assistant.sdk.t) r1     // Catch:{ all -> 0x00ca }
            int r1 = r1.k     // Catch:{ all -> 0x00ca }
            if (r2 != r1) goto L_0x00f0
            java.lang.String r1 = "IPC"
            java.lang.String r2 = "Already has same resolver"
            com.tencent.assistant.utils.XLog.i(r1, r2)     // Catch:{ all -> 0x00ca }
            monitor-exit(r4)     // Catch:{ all -> 0x00ca }
            goto L_0x0008
        L_0x0120:
            java.lang.String r1 = "IPC"
            java.lang.String r2 = "add resolver succ"
            com.tencent.assistant.utils.XLog.i(r1, r2)     // Catch:{ all -> 0x00ca }
            r3.add(r10)     // Catch:{ all -> 0x00ca }
            goto L_0x00d7
        L_0x012b:
            r3.add(r10)     // Catch:{ all -> 0x00ca }
            goto L_0x00d7
        L_0x012f:
            r3.add(r10)     // Catch:{ all -> 0x00ca }
            goto L_0x00d7
        L_0x0133:
            r3 = r1
            goto L_0x0064
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.sdk.SDKIPCBroadcaster.a(java.lang.String, com.tencent.assistant.sdk.r):void");
    }

    private boolean a(t tVar) {
        if (tVar == null) {
            return false;
        }
        if (tVar.k == 1 || tVar.k == 2 || tVar.k == 5 || tVar.k == 6) {
            return true;
        }
        return false;
    }

    /* compiled from: ProGuard */
    class BroadcastQueue extends LinkedBlockingQueue<l> implements Runnable {
        private Object b = new Object();

        BroadcastQueue() {
        }

        public void run() {
            while (true) {
                synchronized (this.b) {
                    while (size() == 0) {
                        try {
                            this.b.wait();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    try {
                        l lVar = (l) take();
                        RequestHandler.a().a(lVar.f2466a, lVar.b);
                        SDKIPCBroadcaster.this.e.put(lVar.f2466a, Long.valueOf(System.currentTimeMillis()));
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }
            }
        }

        public void a(l lVar) {
            synchronized (this.b) {
                try {
                    super.put(lVar);
                    this.b.notify();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void b(String str) {
        this.e.remove(str);
        synchronized (this.g) {
            c.remove(str);
            if (!c.keySet().iterator().hasNext()) {
                this.h.a(false);
            }
        }
    }

    /* access modifiers changed from: private */
    public boolean a(ArrayList<r> arrayList) {
        DownloadInfo a2;
        if (arrayList == null || arrayList.size() == 0) {
            return false;
        }
        Iterator<r> it = arrayList.iterator();
        while (it.hasNext()) {
            r next = it.next();
            if (!(next == null || next.b == null || (a2 = DownloadProxy.a().a(next.b.d, Integer.valueOf(next.b.c).intValue(), 0)) == null)) {
                if (a2.downloadState == SimpleDownloadInfo.DownloadState.QUEUING || a2.downloadState == SimpleDownloadInfo.DownloadState.DOWNLOADING) {
                    return true;
                }
            }
        }
        return false;
    }
}
