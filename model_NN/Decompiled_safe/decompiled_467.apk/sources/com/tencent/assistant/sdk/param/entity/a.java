package com.tencent.assistant.sdk.param.entity;

import android.os.Parcel;
import android.os.Parcelable;

/* compiled from: ProGuard */
final class a implements Parcelable.Creator<BatchDownloadParam> {
    a() {
    }

    /* renamed from: a */
    public BatchDownloadParam[] newArray(int i) {
        return new BatchDownloadParam[i];
    }

    /* renamed from: a */
    public BatchDownloadParam createFromParcel(Parcel parcel) {
        BatchDownloadParam batchDownloadParam = new BatchDownloadParam();
        batchDownloadParam.f2470a = parcel.readString();
        batchDownloadParam.b = parcel.readString();
        batchDownloadParam.c = parcel.readString();
        batchDownloadParam.d = parcel.readString();
        batchDownloadParam.e = parcel.readString();
        batchDownloadParam.f = parcel.readString();
        batchDownloadParam.g = parcel.readString();
        batchDownloadParam.h = parcel.readString();
        batchDownloadParam.i = parcel.readString();
        batchDownloadParam.j = parcel.readString();
        batchDownloadParam.k = parcel.readString();
        batchDownloadParam.l = parcel.readString();
        return batchDownloadParam;
    }
}
