package com.tencent.assistant.sdk;

import android.os.Message;
import com.qq.AppService.AstApp;
import com.tencent.assistant.event.EventController;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.login.d;
import com.tencent.assistant.login.model.MoblieQIdentityInfo;
import com.tencent.assistant.plugin.UserLoginInfo;
import com.tencent.assistant.plugin.UserStateInfo;
import com.tencent.assistant.plugin.a.b;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.bh;

/* compiled from: ProGuard */
public class a implements UIEventListener {
    public void a() {
        EventController k = AstApp.i().k();
        k.addUIEventListener(EventDispatcherEnum.UI_EVENT_GET_USERINFO_SUCCESS, this);
        k.addUIEventListener(EventDispatcherEnum.UI_EVENT_GET_USERINFO_FAIL, this);
        k.addUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_FAIL, this);
        k.addUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_CANCEL, this);
    }

    public void handleUIEvent(Message message) {
        XLog.i("LoginPluginTools", "handleUIEvent msg = " + message);
        UserLoginInfo userLoginInfo = new UserLoginInfo();
        UserStateInfo userStateInfo = new UserStateInfo(0);
        d a2 = d.a();
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_GET_USERINFO_SUCCESS:
            case EventDispatcherEnum.UI_EVENT_GET_USERINFO_FAIL:
                if (a2.k()) {
                    a(a2, userLoginInfo);
                    userStateInfo.setStateChangeType(userLoginInfo.getState());
                    userStateInfo.setUserLoginInfo(userLoginInfo);
                    b.a(userStateInfo);
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_GET_WXLOGIN_SUCCESS:
            case EventDispatcherEnum.UI_EVENT_GET_WXLOGIN_FAIL:
            case EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS:
            default:
                return;
            case EventDispatcherEnum.UI_EVENT_LOGIN_FAIL:
                if (!a2.k()) {
                    userLoginInfo.setState(1);
                    userStateInfo.setStateChangeType(userLoginInfo.getState());
                    userStateInfo.setUserLoginInfo(userLoginInfo);
                    b.a(userStateInfo);
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_LOGIN_CANCEL:
                if (!a2.k()) {
                    userLoginInfo.setState(0);
                    userStateInfo.setStateChangeType(userLoginInfo.getState());
                    userStateInfo.setUserLoginInfo(userLoginInfo);
                    b.a(userStateInfo);
                    return;
                }
                return;
        }
    }

    private static void a(d dVar, UserLoginInfo userLoginInfo) {
        MoblieQIdentityInfo moblieQIdentityInfo = (MoblieQIdentityInfo) dVar.c();
        com.tencent.assistant.login.a.b f = com.tencent.assistant.login.a.a.f();
        if (moblieQIdentityInfo != null) {
            int i = dVar.w() > 83 ? 2 : 3;
            userLoginInfo.setA2(bh.b(moblieQIdentityInfo.getGTKey_ST_A2(), moblieQIdentityInfo.getKey()));
            userLoginInfo.setState(i);
            userLoginInfo.setUin(moblieQIdentityInfo.getUin());
            if (f != null) {
                userLoginInfo.setNickName(f.b);
                userLoginInfo.setPic(f.f1456a);
                return;
            }
            return;
        }
        userLoginInfo.setState(0);
    }

    public void b() {
        EventController k = AstApp.i().k();
        k.removeUIEventListener(EventDispatcherEnum.UI_EVENT_GET_USERINFO_SUCCESS, this);
        k.removeUIEventListener(EventDispatcherEnum.UI_EVENT_GET_USERINFO_FAIL, this);
        k.removeUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_FAIL, this);
        k.removeUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_CANCEL, this);
    }
}
