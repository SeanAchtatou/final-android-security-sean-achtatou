package com.tencent.assistant.e;

import android.app.ActivityManager;
import android.os.FileObserver;
import android.os.Process;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.Global;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.l;
import com.tencent.assistant.utils.t;
import com.tencent.connect.common.Constants;
import java.io.File;
import java.util.HashMap;
import java.util.List;

/* compiled from: ProGuard */
public class a extends FileObserver {

    /* renamed from: a  reason: collision with root package name */
    private String f1265a;

    public a(String str, int i) {
        super(str, i);
        this.f1265a = str;
    }

    public void onEvent(int i, String str) {
        int i2 = i & 4095;
        switch (i2) {
            case Process.PROC_COMBINE:
                if (l.a()) {
                    XLog.d("miles", "CREATE: " + this.f1265a + File.separator + str);
                    File file = new File(this.f1265a + File.separator + str);
                    if (file.isDirectory()) {
                        a(file.getAbsolutePath());
                        XLog.d("miles", str + " is a directory.");
                        return;
                    }
                    return;
                }
                return;
            default:
                XLog.d("miles", "DEFAULT (" + i2 + " : " + str);
                return;
        }
    }

    private void a(String str) {
        String a2 = a(a(), str);
        if (!TextUtils.isEmpty(a2)) {
            LocalApkInfo localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(a2);
            String str2 = "unkown";
            if (localApkInfo != null) {
                str2 = localApkInfo.mAppName;
            }
            HashMap hashMap = new HashMap();
            hashMap.put("B1", a2);
            hashMap.put("B2", str);
            hashMap.put("B3", Global.getPhoneGuidAndGen());
            hashMap.put("B4", Global.getQUAForBeacon());
            hashMap.put("B5", t.g());
            hashMap.put("B6", str2);
            XLog.d("beacon", "beacon report >> Package2DirMap. " + hashMap.toString());
            com.tencent.beacon.event.a.a("Package2DirMap", true, -1, -1, hashMap, false);
        }
    }

    public static String[] a() {
        List<ActivityManager.RunningTaskInfo> runningTasks = ((ActivityManager) AstApp.i().getSystemService("activity")).getRunningTasks(2);
        if (runningTasks != null) {
            try {
                if (runningTasks.size() > 0) {
                    return new String[]{runningTasks.get(0).topActivity.getPackageName(), runningTasks.get(1).topActivity.getPackageName()};
                }
            } catch (Throwable th) {
                XLog.d("miles", "getTop2AppPkgName throws a exception.");
            }
        }
        return null;
    }

    private String a(String[] strArr, String str) {
        int i;
        String replace = str.toLowerCase().replace("/sdcard/", Constants.STR_EMPTY);
        String str2 = null;
        int length = strArr.length;
        int i2 = 0;
        int i3 = 0;
        while (i2 < length) {
            String str3 = strArr[i2];
            int a2 = a(str3, replace);
            if (a2 > i3) {
                i = a2;
            } else {
                str3 = str2;
                i = i3;
            }
            i2++;
            i3 = i;
            str2 = str3;
        }
        if (i3 < 3) {
            return strArr[0];
        }
        return str2;
    }

    private int a(String str, String str2) {
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        while (i3 < str2.length() && i2 < str.length()) {
            if (str2.charAt(i3) == str.charAt(i2)) {
                i++;
                i3++;
            }
            i2++;
        }
        XLog.d("miles", "getMostMatchCount >> pkgName = " + str + ", dir = " + str2 + ", MostMatchCount = " + i);
        return i;
    }
}
