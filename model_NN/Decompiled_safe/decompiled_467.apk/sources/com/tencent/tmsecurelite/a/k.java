package com.tencent.tmsecurelite.a;

import android.os.IBinder;
import android.os.Parcel;
import com.tencent.tmsecurelite.commom.DataEntity;
import java.util.ArrayList;

/* compiled from: ProGuard */
public final class k implements r {

    /* renamed from: a  reason: collision with root package name */
    private IBinder f3813a;

    public k(IBinder iBinder) {
        this.f3813a = iBinder;
    }

    public IBinder asBinder() {
        return this.f3813a;
    }

    public void a(int i) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInt(i);
            this.f3813a.transact(1, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void a(int i, ArrayList<DataEntity> arrayList) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInt(i);
            DataEntity.writeToParcel(arrayList, obtain);
            this.f3813a.transact(2, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void a(int i, int i2) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInt(i);
            obtain.writeInt(i2);
            this.f3813a.transact(3, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }
}
