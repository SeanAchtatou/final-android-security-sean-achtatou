package com.tencent.tmsecurelite.a;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import com.tencent.tmsecurelite.commom.DataEntity;

/* compiled from: ProGuard */
public abstract class l extends Binder implements r {
    public l() {
        attachInterface(this, "com.tencent.tmsecurelite.IGetEncryptFileSetListener");
    }

    public static r a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.tencent.tmsecurelite.IGetEncryptFileSetListener");
        if (queryLocalInterface == null || !(queryLocalInterface instanceof r)) {
            return new k(iBinder);
        }
        return (r) queryLocalInterface;
    }

    /* access modifiers changed from: protected */
    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
        switch (i) {
            case 1:
                a(parcel.readInt());
                parcel2.writeNoException();
                return true;
            case 2:
                a(parcel.readInt(), DataEntity.readFromParcel(parcel));
                parcel2.writeNoException();
                return true;
            case 3:
                a(parcel.readInt(), parcel.readInt());
                parcel2.writeNoException();
                return true;
            default:
                return true;
        }
    }
}
