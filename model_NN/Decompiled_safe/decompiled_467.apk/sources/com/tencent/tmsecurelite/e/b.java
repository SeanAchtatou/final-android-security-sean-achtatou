package com.tencent.tmsecurelite.e;

import android.os.IBinder;
import android.os.Parcel;

/* compiled from: ProGuard */
public final class b implements a {

    /* renamed from: a  reason: collision with root package name */
    private IBinder f3818a;

    public b(IBinder iBinder) {
        this.f3818a = iBinder;
    }

    public IBinder asBinder() {
        return this.f3818a;
    }

    public void a(String str) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.tmsecurelite.IRootService");
            obtain.writeString(str);
            this.f3818a.transact(1, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }
}
