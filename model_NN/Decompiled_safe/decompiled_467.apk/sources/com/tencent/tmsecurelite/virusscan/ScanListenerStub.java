package com.tencent.tmsecurelite.virusscan;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import com.tencent.tmsecurelite.commom.DataEntity;
import org.json.JSONException;

/* compiled from: ProGuard */
public abstract class ScanListenerStub extends Binder implements IScanListener {
    public ScanListenerStub() {
        attachInterface(this, "com.tencent.tmsecurelite.IScanListener");
    }

    public IBinder asBinder() {
        return this;
    }

    public static IScanListener asInterface(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.tencent.tmsecurelite.IScanListener");
        if (queryLocalInterface == null || !(queryLocalInterface instanceof IScanListener)) {
            return new a(iBinder);
        }
        return (IScanListener) queryLocalInterface;
    }

    /* access modifiers changed from: protected */
    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
        DataEntity dataEntity;
        DataEntity dataEntity2 = null;
        switch (i) {
            case 1:
                onScanStarted();
                parcel2.writeNoException();
                return true;
            case 2:
                int readInt = parcel.readInt();
                try {
                    dataEntity = new DataEntity(parcel);
                } catch (JSONException e) {
                    e.printStackTrace();
                    dataEntity = null;
                }
                onPackageScanProgress(readInt, dataEntity);
                parcel2.writeNoException();
                return true;
            case 3:
                int readInt2 = parcel.readInt();
                try {
                    dataEntity2 = new DataEntity(parcel);
                } catch (JSONException e2) {
                    e2.printStackTrace();
                }
                onSdcardScanProgress(readInt2, dataEntity2);
                parcel2.writeNoException();
                return true;
            case 4:
                onCloudScan();
                parcel2.writeNoException();
                return true;
            case 5:
                onCloudScanError(parcel.readInt());
                parcel2.writeNoException();
                return true;
            case 6:
                onScanPaused();
                parcel2.writeNoException();
                return true;
            case 7:
                onScanContinue();
                parcel2.writeNoException();
                return true;
            case 8:
                onScanCanceled();
                parcel2.writeNoException();
                return true;
            case 9:
            default:
                return true;
            case 10:
                onScanFinished(DataEntity.readFromParcel(parcel));
                parcel2.writeNoException();
                return true;
        }
    }
}
