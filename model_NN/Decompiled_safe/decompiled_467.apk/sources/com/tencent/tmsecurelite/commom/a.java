package com.tencent.tmsecurelite.commom;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONException;

/* compiled from: ProGuard */
class a implements Parcelable.Creator<DataEntity> {
    a() {
    }

    /* renamed from: a */
    public DataEntity createFromParcel(Parcel parcel) {
        try {
            return new DataEntity(parcel);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    /* renamed from: a */
    public DataEntity[] newArray(int i) {
        return new DataEntity[i];
    }
}
