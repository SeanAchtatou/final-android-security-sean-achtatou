package com.tencent.tmsecurelite.commom;

import android.os.IBinder;
import android.os.Parcel;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class c implements b {

    /* renamed from: a  reason: collision with root package name */
    private IBinder f3816a;

    public c(IBinder iBinder) {
        this.f3816a = iBinder;
    }

    public IBinder asBinder() {
        return this.f3816a;
    }

    public void a(int i, DataEntity dataEntity) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInt(i);
            dataEntity.writeToParcel(obtain, 0);
            this.f3816a.transact(1, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void a(int i, ArrayList<DataEntity> arrayList) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInt(i);
            DataEntity.writeToParcel(arrayList, obtain);
            this.f3816a.transact(2, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }
}
