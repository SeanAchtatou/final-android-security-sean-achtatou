package com.tencent.assistantv2.st.b;

import android.text.TextUtils;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoV2;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/* compiled from: ProGuard */
public class e {

    /* renamed from: a  reason: collision with root package name */
    Map<String, STInfoV2> f3323a = new HashMap();

    public void b(STInfoV2 sTInfoV2) {
        if (sTInfoV2 != null) {
            String a2 = a(sTInfoV2.appId, sTInfoV2.scene, sTInfoV2.slotId);
            if (!TextUtils.isEmpty(a2)) {
                STInfoV2 sTInfoV22 = this.f3323a.get(a2);
                if (sTInfoV22 == null || !Arrays.equals(sTInfoV22.recommendId, sTInfoV2.recommendId)) {
                    sTInfoV2.hasExposure = true;
                    sTInfoV2.latestExposureTime = System.currentTimeMillis();
                } else {
                    long currentTimeMillis = System.currentTimeMillis() - sTInfoV22.latestExposureTime;
                    if (!sTInfoV22.hasExposure || currentTimeMillis >= 1200000) {
                        sTInfoV2.hasExposure = true;
                        sTInfoV2.latestExposureTime = System.currentTimeMillis();
                    } else {
                        return;
                    }
                }
                this.f3323a.put(a2, sTInfoV2);
                k.a(sTInfoV2);
            }
        }
    }

    private String a(long j, int i, String str) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(j).append("_");
        stringBuffer.append(i).append("_");
        stringBuffer.append(str);
        return stringBuffer.toString();
    }
}
