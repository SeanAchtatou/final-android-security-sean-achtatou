package com.tencent.assistantv2.st.business;

import com.tencent.assistant.db.table.w;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.utils.XLog;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
class q implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ LocalApkInfo f3340a;
    final /* synthetic */ p b;

    q(p pVar, LocalApkInfo localApkInfo) {
        this.b = pVar;
        this.f3340a = localApkInfo;
    }

    public void run() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        XLog.d("InstallStManager", "mApkResListener.onInstalledApkDataChanged begin, packageName=" + this.f3340a.mPackageName + ", versionCode=" + this.f3340a.mVersionCode);
        if (this.b.f3339a.a(this.f3340a.mPackageName, this.f3340a.mVersionCode) == null) {
            XLog.d("InstallStManager", "mApkResListener.onInstalledApkDataChanged begin, packageName=" + this.f3340a.mPackageName + ", versionCode=" + this.f3340a.mVersionCode + " ignored!");
            return;
        }
        this.b.f3339a.a(this.f3340a.mPackageName, this.f3340a.mVersionCode, (byte) 0, (byte) 0, Constants.STR_EMPTY);
        this.b.f3339a.b(this.f3340a.mPackageName, this.f3340a.mVersionCode, (byte) 0, (byte) 0, Constants.STR_EMPTY);
        this.b.f3339a.a(this.f3340a.mPackageName, this.f3340a.mVersionCode, (byte) 0, (byte) 0, Constants.STR_EMPTY, true);
        w.a().a(this.f3340a.mPackageName, this.f3340a.mVersionCode);
    }
}
