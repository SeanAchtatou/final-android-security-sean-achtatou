package com.tencent.assistantv2.st.page;

import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistantv2.st.model.STCommonInfo;

/* compiled from: ProGuard */
public class STInfoV2 extends STCommonInfo {
    public long appId;
    public boolean hasExposure = false;
    public boolean isImmediately = false;
    public long latestExposureTime = 0;
    public String packageName;
    public int resourceType = d.f3360a;

    public STInfoV2(int i, String str, int i2, String str2, int i3) {
        this.scene = i;
        this.slotId = str;
        this.sourceScene = i2;
        this.sourceSceneSlotId = str2;
        this.actionId = i3;
    }

    public void updateWithSimpleAppModel(SimpleAppModel simpleAppModel) {
        boolean z = true;
        if (simpleAppModel != null) {
            this.appId = simpleAppModel.f1634a;
            this.packageName = simpleAppModel.c;
            this.recommendId = simpleAppModel.y;
            if (simpleAppModel.au != 1) {
                z = false;
            }
            this.isImmediately = z;
        }
    }

    public void updateWithDownloadInfo(DownloadInfo downloadInfo) {
        if (downloadInfo != null) {
            this.appId = downloadInfo.appId;
            this.packageName = downloadInfo.packageName;
        }
    }
}
