package com.tencent.assistantv2.st.business;

import com.tencent.assistant.protocol.jce.StatAppInfo;
import com.tencent.assistant.protocol.jce.StatAppUnInstall;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.st.h;
import com.tencent.assistant.utils.bh;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class x extends BaseSTManagerV2 {

    /* renamed from: a  reason: collision with root package name */
    private StatAppUnInstall f3346a;

    public x() {
        this.f3346a = null;
        this.f3346a = new StatAppUnInstall();
    }

    public void a(short s, short s2, ArrayList<StatAppInfo> arrayList) {
        this.f3346a.f2354a = s;
        this.f3346a.c = s2;
        if (this.f3346a.c > 0) {
            this.f3346a.b = true;
        }
        this.f3346a.d = h.a();
        this.f3346a.e = STConst.ST_PAGE_ASSISTANT;
        this.f3346a.f = arrayList;
        a();
    }

    private void a() {
        if (this.f3346a != null) {
            byte[] a2 = bh.a(this.f3346a);
            if (a2.length > 0) {
                com.tencent.assistant.db.table.x.a().a(getSTType(), a2);
            }
        }
    }

    public byte getSTType() {
        return 10;
    }

    public void flush() {
        a();
    }
}
