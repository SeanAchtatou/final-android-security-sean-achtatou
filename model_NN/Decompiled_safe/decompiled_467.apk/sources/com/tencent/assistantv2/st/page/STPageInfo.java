package com.tencent.assistantv2.st.page;

import com.tencent.assistant.st.STConst;
import java.io.Serializable;

/* compiled from: ProGuard */
public class STPageInfo implements Serializable, Cloneable {

    /* renamed from: a  reason: collision with root package name */
    public int f3358a = 2000;
    public String b = STConst.ST_DEFAULT_SLOT;
    public int c = 2000;
    public String d = STConst.ST_DEFAULT_SLOT;
    public String e = STConst.ST_DEFAULT_SLOT;
}
