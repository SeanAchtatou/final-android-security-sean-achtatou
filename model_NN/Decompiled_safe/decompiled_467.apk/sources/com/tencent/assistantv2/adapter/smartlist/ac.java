package com.tencent.assistantv2.adapter.smartlist;

/* compiled from: ProGuard */
public class ac {
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0022  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x003c  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00c3  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.view.View a(android.content.Context r7, com.tencent.assistantv2.adapter.smartlist.ab r8, android.view.View r9, com.tencent.assistantv2.adapter.smartlist.SmartItemType r10, int r11, com.tencent.assistant.model.e r12, com.tencent.assistant.component.invalidater.IViewInvalidater r13) {
        /*
            r2 = 0
            r1 = 0
            r3 = 1
            int[] r0 = com.tencent.assistantv2.adapter.smartlist.ad.f2911a
            int r4 = r10.ordinal()
            r0 = r0[r4]
            switch(r0) {
                case 1: goto L_0x0042;
                case 2: goto L_0x0049;
                case 3: goto L_0x0050;
                case 4: goto L_0x0057;
                case 5: goto L_0x005e;
                case 6: goto L_0x0065;
                case 7: goto L_0x006c;
                case 8: goto L_0x0073;
                default: goto L_0x000e;
            }
        L_0x000e:
            r4 = r1
        L_0x000f:
            if (r4 == 0) goto L_0x00c8
            if (r9 == 0) goto L_0x00ce
            com.tencent.assistantv2.adapter.smartlist.SmartItemType r0 = com.tencent.assistantv2.adapter.smartlist.SmartItemType.NORMAL
            if (r10 != r0) goto L_0x007a
            java.lang.Object r0 = r9.getTag()
            boolean r0 = r0 instanceof com.tencent.assistantv2.adapter.smartlist.y
            if (r0 == 0) goto L_0x007a
            r0 = r3
        L_0x0020:
            if (r9 == 0) goto L_0x002a
            java.lang.Object r5 = r9.getTag()
            if (r5 == 0) goto L_0x002a
            if (r0 != 0) goto L_0x00b9
        L_0x002a:
            android.util.Pair r5 = r4.a()     // Catch:{ Throwable -> 0x00ad }
            java.lang.Object r0 = r5.first     // Catch:{ Throwable -> 0x00ad }
            android.view.View r0 = (android.view.View) r0     // Catch:{ Throwable -> 0x00ad }
            java.lang.Object r1 = r5.second     // Catch:{ Throwable -> 0x00cb }
            r0.setTag(r1)     // Catch:{ Throwable -> 0x00cb }
            r6 = r2
            r2 = r1
            r1 = r6
        L_0x003a:
            if (r1 == 0) goto L_0x00c3
            android.view.View r0 = new android.view.View
            r0.<init>(r7)
        L_0x0041:
            return r0
        L_0x0042:
            com.tencent.assistantv2.adapter.smartlist.x r0 = new com.tencent.assistantv2.adapter.smartlist.x
            r0.<init>(r7, r8, r13)
            r4 = r0
            goto L_0x000f
        L_0x0049:
            com.tencent.assistantv2.adapter.smartlist.s r0 = new com.tencent.assistantv2.adapter.smartlist.s
            r0.<init>(r7, r8, r13)
            r4 = r0
            goto L_0x000f
        L_0x0050:
            com.tencent.assistantv2.adapter.smartlist.b r0 = new com.tencent.assistantv2.adapter.smartlist.b
            r0.<init>(r7, r8, r13)
            r4 = r0
            goto L_0x000f
        L_0x0057:
            com.tencent.assistantv2.adapter.smartlist.ah r0 = new com.tencent.assistantv2.adapter.smartlist.ah
            r0.<init>(r7, r8, r13)
            r4 = r0
            goto L_0x000f
        L_0x005e:
            com.tencent.assistantv2.adapter.smartlist.al r0 = new com.tencent.assistantv2.adapter.smartlist.al
            r0.<init>(r7, r8, r13)
            r4 = r0
            goto L_0x000f
        L_0x0065:
            com.tencent.assistantv2.adapter.smartlist.g r0 = new com.tencent.assistantv2.adapter.smartlist.g
            r0.<init>(r7, r8, r13)
            r4 = r0
            goto L_0x000f
        L_0x006c:
            com.tencent.assistantv2.adapter.smartlist.k r0 = new com.tencent.assistantv2.adapter.smartlist.k
            r0.<init>(r7, r8, r13)
            r4 = r0
            goto L_0x000f
        L_0x0073:
            com.tencent.assistantv2.adapter.smartlist.n r0 = new com.tencent.assistantv2.adapter.smartlist.n
            r0.<init>(r7, r8, r13)
            r4 = r0
            goto L_0x000f
        L_0x007a:
            com.tencent.assistantv2.adapter.smartlist.SmartItemType r0 = com.tencent.assistantv2.adapter.smartlist.SmartItemType.NORMAL_NO_REASON
            if (r10 != r0) goto L_0x0090
            java.lang.Object r0 = r9.getTag()
            boolean r0 = r0 instanceof com.tencent.assistantv2.adapter.smartlist.v
            if (r0 == 0) goto L_0x0090
            java.lang.Object r0 = r9.getTag()
            boolean r0 = r0 instanceof com.tencent.assistantv2.adapter.smartlist.y
            if (r0 != 0) goto L_0x0090
            r0 = r3
            goto L_0x0020
        L_0x0090:
            com.tencent.assistantv2.adapter.smartlist.SmartItemType r0 = com.tencent.assistantv2.adapter.smartlist.SmartItemType.COMPETITIVE
            if (r10 != r0) goto L_0x009e
            java.lang.Object r0 = r9.getTag()
            boolean r0 = r0 instanceof com.tencent.assistantv2.adapter.smartlist.f
            if (r0 == 0) goto L_0x009e
            r0 = r3
            goto L_0x0020
        L_0x009e:
            com.tencent.assistantv2.adapter.smartlist.SmartItemType r0 = com.tencent.assistantv2.adapter.smartlist.SmartItemType.NORMAL_LIST
            if (r10 != r0) goto L_0x00ce
            java.lang.Object r0 = r9.getTag()
            boolean r0 = r0 instanceof com.tencent.assistantv2.adapter.smartlist.q
            if (r0 == 0) goto L_0x00ce
            r0 = r3
            goto L_0x0020
        L_0x00ad:
            r0 = move-exception
        L_0x00ae:
            com.tencent.assistant.manager.cq r0 = com.tencent.assistant.manager.cq.a()
            r0.b()
            r2 = r1
            r0 = r9
            r1 = r3
            goto L_0x003a
        L_0x00b9:
            java.lang.Object r1 = r9.getTag()
            r0 = r9
            r6 = r1
            r1 = r2
            r2 = r6
            goto L_0x003a
        L_0x00c3:
            r4.a(r0, r2, r11, r12)
            goto L_0x0041
        L_0x00c8:
            r0 = r1
            goto L_0x0041
        L_0x00cb:
            r2 = move-exception
            r9 = r0
            goto L_0x00ae
        L_0x00ce:
            r0 = r2
            goto L_0x0020
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistantv2.adapter.smartlist.ac.a(android.content.Context, com.tencent.assistantv2.adapter.smartlist.ab, android.view.View, com.tencent.assistantv2.adapter.smartlist.SmartItemType, int, com.tencent.assistant.model.e, com.tencent.assistant.component.invalidater.IViewInvalidater):android.view.View");
    }
}
