package com.tencent.assistantv2.adapter.smartlist;

import android.content.Context;
import android.text.TextUtils;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.adapter.c;
import com.tencent.assistant.component.appdetail.process.s;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.component.txscrollview.TXAppIconView;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.d;
import com.tencent.assistant.model.e;
import com.tencent.assistant.utils.bt;
import com.tencent.assistant.utils.df;
import com.tencent.assistantv2.adapter.smartlist.SmartListAdapter;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
public class b extends x {
    public b(Context context, ab abVar, IViewInvalidater iViewInvalidater) {
        super(context, abVar, iViewInvalidater);
    }

    public Pair<View, Object> a() {
        f fVar = new f();
        View inflate = LayoutInflater.from(this.f2908a).inflate((int) R.layout.competitive_card, (ViewGroup) null);
        fVar.b = (TXImageView) inflate.findViewById(R.id.pic);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, (((df.b() - (df.b(13.0f) * 2)) * 270) / 690) + df.b(7.0f));
        fVar.b.setLayoutParams(layoutParams);
        fVar.c = (ImageView) inflate.findViewById(R.id.vedio);
        fVar.c.setLayoutParams(layoutParams);
        fVar.d = (TXAppIconView) inflate.findViewById(R.id.icon);
        fVar.d.setInvalidater(this.e);
        fVar.e = (DownloadButton) inflate.findViewById(R.id.download_soft_btn);
        fVar.f = (TextView) inflate.findViewById(R.id.name);
        fVar.g = (TextView) inflate.findViewById(R.id.app_size_sumsize);
        fVar.h = (ImageView) inflate.findViewById(R.id.app_size_redline);
        fVar.i = (TextView) inflate.findViewById(R.id.app_score_truesize);
        fVar.j = (TextView) inflate.findViewById(R.id.app_size_text);
        fVar.k = (TextView) inflate.findViewById(R.id.description);
        fVar.k.setMaxLines(2);
        fVar.f2920a = (ImageView) inflate.findViewById(R.id.sort_num_image);
        return Pair.create(inflate, fVar);
    }

    public void a(View view, Object obj, int i, e eVar) {
        SimpleAppModel simpleAppModel;
        f fVar = (f) obj;
        if (eVar != null) {
            simpleAppModel = eVar.c();
        } else {
            simpleAppModel = null;
        }
        if (simpleAppModel != null) {
            view.setOnClickListener(new c(this, simpleAppModel));
            a(fVar, simpleAppModel, i);
        }
    }

    private void a(f fVar, SimpleAppModel simpleAppModel, int i) {
        if (simpleAppModel != null && fVar != null) {
            if (!TextUtils.isEmpty(simpleAppModel.Y)) {
                fVar.b.updateImageView(simpleAppModel.Y, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            } else {
                fVar.b.setImageResource(R.drawable.pic_defaule);
            }
            if (simpleAppModel.Z == null || simpleAppModel.Z.length() == 0) {
                fVar.c.setVisibility(8);
                fVar.b.setDuplicateParentStateEnabled(true);
                fVar.b.setClickable(false);
            } else {
                fVar.c.setVisibility(0);
                fVar.b.setDuplicateParentStateEnabled(false);
                fVar.b.setClickable(true);
                fVar.b.setOnClickListener(new d(this, simpleAppModel));
            }
            StatInfo a2 = a.a(this.b.e());
            a2.f3356a = this.b.i().d();
            a2.extraData = this.b.i().b();
            a2.searchId = this.b.i().a();
            a2.slotId = ae.a(i);
            fVar.d.updateImageView(simpleAppModel.e, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            d a3 = this.b.a();
            AppConst.AppState appState = a3 != null ? a3.c : null;
            fVar.e.a(simpleAppModel, a3);
            fVar.f.setText(simpleAppModel.d);
            if (simpleAppModel.a()) {
                fVar.h.setVisibility(0);
                fVar.g.setVisibility(0);
                fVar.i.setVisibility(0);
                fVar.j.setVisibility(8);
                fVar.g.setText(bt.a(simpleAppModel.k));
                fVar.i.setText(bt.a(simpleAppModel.v));
            } else {
                fVar.h.setVisibility(8);
                fVar.g.setVisibility(8);
                fVar.i.setVisibility(8);
                fVar.j.setVisibility(0);
                fVar.j.setText(bt.a(simpleAppModel.k));
            }
            if (TextUtils.isEmpty(simpleAppModel.X)) {
                fVar.k.setVisibility(8);
            } else {
                fVar.k.setVisibility(0);
                fVar.k.setText(simpleAppModel.X);
            }
            if (s.a(simpleAppModel, appState)) {
                fVar.e.setClickable(false);
            } else {
                fVar.e.setClickable(true);
                fVar.e.a(this.b.e(), new e(this), a3);
            }
            if (this.b.f() == SmartListAdapter.SmartListType.SearchPage) {
                c.a(this.f2908a, simpleAppModel, fVar.f, true);
            } else {
                c.a(this.f2908a, simpleAppModel, fVar.f, false);
            }
        }
    }
}
