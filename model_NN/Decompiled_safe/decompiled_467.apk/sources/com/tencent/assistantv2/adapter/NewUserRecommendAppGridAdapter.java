package com.tencent.assistantv2.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.utils.bt;
import com.tencent.assistant.utils.ct;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class NewUserRecommendAppGridAdapter extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    private LayoutInflater f2871a;
    private ArrayList<SimpleAppModel> b;
    private ArrayList<Boolean> c;

    public int getCount() {
        return this.b.size();
    }

    public Object getItem(int i) {
        return this.b.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        t tVar;
        if (view == null) {
            t tVar2 = new t(this);
            view = this.f2871a.inflate((int) R.layout.popwindow_newuser_recommend_item, (ViewGroup) null);
            tVar2.f2931a = (TXImageView) view.findViewById(R.id.icon);
            tVar2.b = (TextView) view.findViewById(R.id.name);
            tVar2.c = (TextView) view.findViewById(R.id.size);
            tVar2.d = (ImageView) view.findViewById(R.id.check_box);
            view.setTag(tVar2);
            tVar = tVar2;
        } else {
            tVar = (t) view.getTag();
        }
        view.setTag(R.id.tma_st_slot_tag, a(i));
        a(tVar, i);
        return view;
    }

    private String a(int i) {
        return "03_" + ct.a(i + 1);
    }

    private void a(t tVar, int i) {
        SimpleAppModel simpleAppModel = this.b.get(i);
        tVar.f2931a.updateImageView(simpleAppModel.e, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
        tVar.b.setText(simpleAppModel.d);
        tVar.c.setText(bt.a(simpleAppModel.k));
        tVar.d.setSelected(this.c.get(i).booleanValue());
    }
}
