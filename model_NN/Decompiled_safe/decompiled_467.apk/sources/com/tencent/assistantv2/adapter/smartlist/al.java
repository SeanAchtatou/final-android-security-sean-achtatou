package com.tencent.assistantv2.adapter.smartlist;

import android.content.Context;
import android.text.TextUtils;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.component.txscrollview.TXAppIconView;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.model.e;
import com.tencent.assistant.utils.ct;
import com.tencent.assistant.utils.df;
import com.tencent.assistantv2.model.SimpleVideoModel;

/* compiled from: ProGuard */
public class al extends ah {
    private IViewInvalidater c;
    private ab d;

    public al(Context context, ab abVar, IViewInvalidater iViewInvalidater) {
        super(context, abVar, iViewInvalidater);
        this.c = iViewInvalidater;
        this.d = abVar;
    }

    public Pair<View, Object> a() {
        View inflate = LayoutInflater.from(this.f2908a).inflate((int) R.layout.video_rich_universal_item, (ViewGroup) null);
        an anVar = new an();
        anVar.f2916a = (TextView) inflate.findViewById(R.id.video_match_result);
        anVar.b = (TXAppIconView) inflate.findViewById(R.id.video_icon);
        anVar.b.setInvalidater(this.c);
        anVar.c = (TextView) inflate.findViewById(R.id.video_title);
        anVar.d = (TextView) inflate.findViewById(R.id.video_update_info);
        anVar.e = (TextView) inflate.findViewById(R.id.video_view_count);
        anVar.f = (TextView) inflate.findViewById(R.id.video_year);
        anVar.g = (TextView) inflate.findViewById(R.id.video_author);
        anVar.h = (TextView) inflate.findViewById(R.id.video_label);
        anVar.i = (TextView) inflate.findViewById(R.id.video_desc);
        return Pair.create(inflate, anVar);
    }

    public void a(View view, Object obj, int i, e eVar) {
        SimpleVideoModel simpleVideoModel;
        an anVar = (an) obj;
        if (eVar != null) {
            simpleVideoModel = eVar.d();
        } else {
            simpleVideoModel = null;
        }
        view.setOnClickListener(new aj(this, this.f2908a, i, simpleVideoModel, this.b));
        a(anVar, simpleVideoModel, i);
    }

    private void a(an anVar, SimpleVideoModel simpleVideoModel, int i) {
        if (simpleVideoModel != null && anVar != null) {
            anVar.f2916a.setText(simpleVideoModel.k);
            anVar.b.updateImageView(simpleVideoModel.c, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            anVar.c.setText(simpleVideoModel.b);
            if (!TextUtils.isEmpty(ct.f(simpleVideoModel.i))) {
                anVar.d.setPadding(0, 0, df.a(this.f2908a, 10.0f), 0);
                anVar.d.setText(ct.f(simpleVideoModel.i));
            } else {
                anVar.d.setPadding(0, 0, 0, 0);
            }
            anVar.e.setText(simpleVideoModel.h);
            if (!TextUtils.isEmpty(ct.f(simpleVideoModel.d))) {
                anVar.f.setPadding(0, 0, df.a(this.f2908a, 10.0f), 0);
                anVar.f.setText(ct.f(simpleVideoModel.d));
            } else {
                anVar.f.setPadding(0, 0, 0, 0);
            }
            anVar.g.setText(simpleVideoModel.e);
            anVar.h.setText(simpleVideoModel.g);
            anVar.i.setText(ct.e(simpleVideoModel.f));
        }
    }
}
