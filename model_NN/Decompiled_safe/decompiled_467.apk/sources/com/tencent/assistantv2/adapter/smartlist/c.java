package com.tencent.assistantv2.adapter.smartlist;

import android.os.Bundle;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.link.b;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class c extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SimpleAppModel f2917a;
    final /* synthetic */ b b;

    c(b bVar, SimpleAppModel simpleAppModel) {
        this.b = bVar;
        this.f2917a = simpleAppModel;
    }

    public void onTMAClick(View view) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("com.tencent.assistant.ACTION_URL", this.f2917a.aa);
        b.b(this.b.f2908a, this.f2917a.aa.f1970a, bundle);
    }

    public STInfoV2 getStInfo() {
        if (this.b.b.e() != null) {
            this.b.b.e().updateStatus(this.f2917a);
        }
        return this.b.b.e();
    }
}
