package com.tencent.assistantv2.adapter.smartlist;

import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
public class aa {

    /* renamed from: a  reason: collision with root package name */
    private int f2909a;
    private int b;
    private long c;
    private String d;
    private STInfoV2 e = null;

    public aa a(int i) {
        this.f2909a = i;
        return this;
    }

    public int a() {
        return this.f2909a;
    }

    public aa b(int i) {
        this.b = i;
        return this;
    }

    public int b() {
        return this.b;
    }

    public aa a(long j) {
        this.c = j;
        return this;
    }

    public long c() {
        return this.c;
    }

    public aa a(String str) {
        this.d = str;
        return this;
    }

    public String d() {
        return this.d;
    }
}
