package com.tencent.assistantv2.adapter.smartlist;

import android.content.Context;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamExClickListener;
import com.tencent.assistant.link.b;
import com.tencent.assistantv2.model.SimpleVideoModel;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
public class aj extends OnTMAParamExClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ah f2914a;
    private Context b;
    private int c;
    private SimpleVideoModel d;
    private ab e;

    public aj(ah ahVar, Context context, int i, SimpleVideoModel simpleVideoModel, ab abVar) {
        this.f2914a = ahVar;
        this.b = context;
        this.c = i;
        this.d = simpleVideoModel;
        this.e = abVar;
    }

    public void onTMAClick(View view) {
        if (this.d != null) {
            b.a(this.b, this.d.l);
        }
    }

    public STInfoV2 getStInfo(View view) {
        if (this.e == null || this.e.e() == null) {
            return STInfoBuilder.buildSTInfo(this.b, 200);
        }
        STInfoV2 e2 = this.e.e();
        e2.actionId = 200;
        return e2;
    }
}
