package com.tencent.assistantv2.adapter.smartlist;

import com.tencent.assistant.component.smartcard.SmartcardListener;
import com.tencent.assistant.manager.smartcard.o;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.a.i;
import com.tencent.assistant.model.e;
import java.util.Iterator;

/* compiled from: ProGuard */
class ag implements SmartcardListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SmartListAdapter f2913a;

    ag(SmartListAdapter smartListAdapter) {
        this.f2913a = smartListAdapter;
    }

    public void onActionClose(int i, int i2) {
        e eVar;
        i f;
        Iterator it = this.f2913a.h.iterator();
        while (true) {
            if (!it.hasNext()) {
                eVar = null;
                break;
            }
            eVar = (e) it.next();
            if (eVar.b == 2 && (f = eVar.f()) != null && f.i == i && f.j == i2) {
                break;
            }
        }
        if (eVar != null) {
            this.f2913a.h.remove(eVar);
            this.f2913a.notifyDataSetChanged();
        }
        o.a().b(i, i2);
    }

    public void onSmartcardExposure(int i, int i2) {
        o.a().a(i, i2);
    }

    public void onSmartcardContentAppExposure(int i, int i2, SimpleAppModel simpleAppModel) {
        o.a().a(i, i2, simpleAppModel);
    }
}
