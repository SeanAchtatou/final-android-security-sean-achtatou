package com.tencent.assistantv2.adapter.smartlist;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import com.tencent.assistant.model.b;
import com.tencent.assistant.model.d;
import com.tencent.assistant.model.e;
import com.tencent.assistantv2.adapter.smartlist.SmartListAdapter;
import com.tencent.assistantv2.component.RankNormalListView;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public class SearchPageListAdapter extends SmartListAdapter {
    public SearchPageListAdapter(Context context, View view, b bVar) {
        super(context, view, bVar);
    }

    /* access modifiers changed from: protected */
    public SmartListAdapter.SmartListType a() {
        return SmartListAdapter.SmartListType.SearchPage;
    }

    /* access modifiers changed from: protected */
    public String a(int i) {
        return "07";
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public STInfoV2 a(int i, e eVar, int i2, d dVar) {
        String str;
        String str2;
        String str3;
        if (eVar == null) {
            return null;
        }
        String str4 = "07";
        String e = e();
        switch (z.f2930a[SmartItemType.values()[getItemViewType(i)].ordinal()]) {
            case 1:
            case 2:
                if (eVar.c != null) {
                    if (!TextUtils.isEmpty(eVar.c.ae)) {
                        if (eVar.c.ao == null || eVar.c.ao.size() <= 0) {
                            str4 = "05";
                        } else {
                            str4 = "06";
                        }
                    }
                    String str5 = e + ";" + eVar.c.f1634a;
                    str3 = str4;
                    str2 = str5;
                    break;
                }
                String str6 = e;
                str3 = str4;
                str2 = str6;
                break;
            case 3:
                str = Constants.VIA_REPORT_TYPE_SET_AVATAR;
                if (eVar.e != null) {
                    str2 = e + ";" + eVar.e.f3312a;
                    str3 = str;
                    break;
                }
                str2 = e;
                str3 = str;
                break;
            case 4:
                str = RankNormalListView.ST_HIDE_INSTALLED_APPS;
                if (eVar.e != null) {
                    str2 = e + ";" + eVar.e.f3312a;
                    str3 = str;
                    break;
                }
                str2 = e;
                str3 = str;
                break;
            case 5:
                str = Constants.VIA_REPORT_TYPE_JOININ_GROUP;
                if (eVar.f != null) {
                    str2 = e + ";" + eVar.f.f3310a;
                    str3 = str;
                    break;
                }
                str2 = e;
                str3 = str;
                break;
            case 6:
                str = "09";
                if (eVar.f != null) {
                    str2 = e + ";" + eVar.f.f3310a;
                    str3 = str;
                    break;
                }
                str2 = e;
                str3 = str;
                break;
            default:
                String str62 = e;
                str3 = str4;
                str2 = str62;
                break;
        }
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(g(), eVar.c, a.a(str3, i), i2, null);
        buildSTInfo.extraData = str2;
        buildSTInfo.searchId = f();
        return buildSTInfo;
    }

    public boolean b() {
        return false;
    }

    public boolean c() {
        return true;
    }
}
