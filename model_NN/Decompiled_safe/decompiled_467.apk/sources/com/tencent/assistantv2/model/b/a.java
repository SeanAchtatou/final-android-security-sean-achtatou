package com.tencent.assistantv2.model.b;

import android.text.TextUtils;
import com.tencent.assistant.protocol.jce.ExplicitHotWord;
import com.tencent.assistant.utils.XLog;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class a extends d {

    /* renamed from: a  reason: collision with root package name */
    private final String f3316a = "ExplicitHotwordsModel";
    private List<ExplicitHotWord> b = null;
    private final int c = 1;
    private final int d = 2;

    public List<ExplicitHotWord> a() {
        List<ExplicitHotWord> c2 = c();
        ExplicitHotWord explicitHotWord = null;
        if (c2 != null) {
            Iterator<ExplicitHotWord> it = c2.iterator();
            while (true) {
                if (it.hasNext()) {
                    ExplicitHotWord next = it.next();
                    if (next != null && !TextUtils.isEmpty(next.b) && !TextUtils.isEmpty(next.f2067a)) {
                        explicitHotWord = next;
                        break;
                    }
                } else {
                    break;
                }
            }
        }
        if (explicitHotWord == null) {
            return this.b;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(explicitHotWord);
        return arrayList;
    }

    public void a(ExplicitHotWord explicitHotWord) {
        if (explicitHotWord != null) {
            if (this.b == null) {
                this.b = new ArrayList();
            }
            this.b.add(explicitHotWord);
        }
    }

    public int a(String str) {
        int i = 0;
        if (TextUtils.isEmpty(str) || this.b == null) {
            XLog.d("ExplicitHotwordsModel", "delByPackageName: null or data null");
            return 0;
        }
        XLog.d("ExplicitHotwordsModel", "delByPackageName:pkg:" + str);
        XLog.d("ExplicitHotwordsModel", "delByPackageName:(before)data size:" + this.b.size());
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(this.b);
        Iterator<ExplicitHotWord> it = this.b.iterator();
        while (true) {
            int i2 = i;
            if (it.hasNext()) {
                ExplicitHotWord next = it.next();
                XLog.d("ExplicitHotwordsModel", "delByPackageName:" + (TextUtils.isEmpty(next.c) ? "null" : next.c));
                if (next != null && str.equals(next.c)) {
                    XLog.d("ExplicitHotwordsModel", "delByPackageName:get one");
                    arrayList.remove(next);
                    i2++;
                }
                i = i2;
            } else {
                this.b = arrayList;
                XLog.d("ExplicitHotwordsModel", "delByPackageName:(after)data size:" + this.b.size());
                XLog.d("ExplicitHotwordsModel", "delByPackageName:del_count:" + i2);
                return i2;
            }
        }
    }

    public void a(d dVar) {
        if (dVar != null && dVar.b() > 0) {
            if (dVar instanceof a) {
                a aVar = (a) dVar;
                if (aVar != null && aVar.b != null) {
                    if (this.b == null) {
                        this.b = aVar.b;
                    } else {
                        this.b.addAll(aVar.b);
                    }
                }
            } else {
                XLog.d("ExplicitHotwordsModel", "model is not ExplicitHotwordsModel instance!");
            }
        }
    }

    public int b() {
        if (this.b == null) {
            return 0;
        }
        return this.b.size();
    }

    private List<ExplicitHotWord> c() {
        ArrayList<ExplicitHotWord> arrayList = new ArrayList<>();
        if (this.b != null) {
            for (ExplicitHotWord explicitHotWord : arrayList) {
                if (explicitHotWord != null && explicitHotWord.g == 1) {
                    arrayList.add(explicitHotWord);
                }
            }
        }
        return arrayList;
    }
}
