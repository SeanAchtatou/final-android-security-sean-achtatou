package com.tencent.assistantv2.b;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.module.BaseEngine;
import com.tencent.assistant.protocol.jce.GetAppFromTagRequest;
import com.tencent.assistant.protocol.jce.GetAppFromTagResponse;
import com.tencent.assistantv2.model.a.b;

/* compiled from: ProGuard */
public class a extends BaseEngine<b> {
    public int a(String str, String str2, long j, String str3, int i, int i2) {
        return send(new GetAppFromTagRequest(str, str2, j, str3, i, i2));
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        notifyDataChangedInMainThread(new b(this, i, (GetAppFromTagResponse) jceStruct2));
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        notifyDataChangedInMainThread(new c(this, i, i2, (GetAppFromTagResponse) jceStruct2));
    }
}
