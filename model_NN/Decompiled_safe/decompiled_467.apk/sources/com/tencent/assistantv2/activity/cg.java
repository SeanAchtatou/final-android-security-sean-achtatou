package com.tencent.assistantv2.activity;

import com.tencent.assistantv2.adapter.RankNormalListAdapter;
import com.tencent.assistantv2.adapter.u;

/* compiled from: ProGuard */
class cg implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ cf f2803a;

    cg(cf cfVar) {
        this.f2803a = cfVar;
    }

    public void run() {
        for (Object next : this.f2803a.f2802a.S) {
            if (next instanceof RankNormalListAdapter) {
                ((RankNormalListAdapter) next).notifyDataSetChanged();
            } else if (next instanceof u) {
                ((u) next).notifyDataSetChanged();
            }
        }
    }
}
