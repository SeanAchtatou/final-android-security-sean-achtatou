package com.tencent.assistantv2.activity;

import android.os.Bundle;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.login.d;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class cr extends AppConst.TwoBtnDialogInfo {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ MainActivity f2814a;

    cr(MainActivity mainActivity) {
        this.f2814a = mainActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistantv2.activity.MainActivity, boolean):void
     arg types: [com.tencent.assistantv2.activity.MainActivity, int]
     candidates:
      com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistantv2.activity.MainActivity, int):int
      com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistantv2.activity.MainActivity, com.tencent.assistant.db.table.z):com.tencent.assistant.db.table.z
      com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistantv2.activity.MainActivity, java.util.ArrayList):java.util.ArrayList
      com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistant.protocol.jce.GetPhoneUserAppListResponse, boolean):void
      com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistantv2.activity.MainActivity, android.content.Intent):void
      com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistantv2.activity.MainActivity, com.tencent.assistant.protocol.jce.DesktopShortCut):void
      com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistant.protocol.jce.DesktopShortCut, com.tencent.assistant.protocol.jce.DesktopShortCut):boolean
      com.tencent.assistantv2.activity.MainActivity.a(int, boolean):void
      com.tencent.assistant.activity.BaseActivity.a(java.lang.String, java.lang.String):void
      com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistantv2.activity.MainActivity, boolean):void */
    public void onLeftBtnClick() {
        this.f2814a.b(true);
        k.a(new STInfoV2(STConst.ST_PAGE_GUIDE_RESULT_HUANJI_QQ_LOGIN, "03_002", 0, STConst.ST_DEFAULT_SLOT, 200));
    }

    public void onRightBtnClick() {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConst.KEY_LOGIN_TYPE, 5);
        bundle.putInt(AppConst.KEY_FROM_TYPE, 18);
        d.a().a(AppConst.IdentityType.MOBILEQ, bundle);
        k.a(new STInfoV2(STConst.ST_PAGE_GUIDE_RESULT_HUANJI_QQ_LOGIN, "03_001", 0, STConst.ST_DEFAULT_SLOT, 200));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistantv2.activity.MainActivity, boolean):void
     arg types: [com.tencent.assistantv2.activity.MainActivity, int]
     candidates:
      com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistantv2.activity.MainActivity, int):int
      com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistantv2.activity.MainActivity, com.tencent.assistant.db.table.z):com.tencent.assistant.db.table.z
      com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistantv2.activity.MainActivity, java.util.ArrayList):java.util.ArrayList
      com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistant.protocol.jce.GetPhoneUserAppListResponse, boolean):void
      com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistantv2.activity.MainActivity, android.content.Intent):void
      com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistantv2.activity.MainActivity, com.tencent.assistant.protocol.jce.DesktopShortCut):void
      com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistant.protocol.jce.DesktopShortCut, com.tencent.assistant.protocol.jce.DesktopShortCut):boolean
      com.tencent.assistantv2.activity.MainActivity.a(int, boolean):void
      com.tencent.assistant.activity.BaseActivity.a(java.lang.String, java.lang.String):void
      com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistantv2.activity.MainActivity, boolean):void */
    public void onCancell() {
        this.f2814a.b(true);
        k.a(new STInfoV2(STConst.ST_PAGE_GUIDE_RESULT_HUANJI_QQ_LOGIN, "03_002", 0, STConst.ST_DEFAULT_SLOT, 200));
    }
}
