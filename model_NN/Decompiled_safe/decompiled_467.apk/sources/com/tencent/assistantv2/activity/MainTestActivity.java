package com.tencent.assistantv2.activity;

import android.content.Context;
import android.os.Bundle;
import android.widget.LinearLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.invalidater.ListViewScrollListener;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.module.k;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.adapter.RankNormalListAdapter;
import com.tencent.assistantv2.component.RankNormalListPage;

/* compiled from: ProGuard */
public class MainTestActivity extends BaseActivity {
    RankNormalListPage n = null;
    RankNormalListAdapter t = null;
    private LinearLayout u;
    private Context v;
    private ListViewScrollListener w = new de(this);

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.v = this;
        setContentView((int) R.layout.main_test_layout);
        this.u = (LinearLayout) findViewById(R.id.main_test_layout);
        k kVar = new k(0, 5, 20);
        this.n = new RankNormalListPage(this.v, TXScrollViewBase.ScrollMode.PULL_FROM_END, kVar);
        this.t = new RankNormalListAdapter(this.v, this.n, kVar.a());
        this.t.a((int) STConst.ST_PAGE_RANK_CLASSIC, -100);
        this.t.a(RankNormalListAdapter.ListType.LISTTYPEGAMESORT);
        this.t.a(true);
        this.n.setAdapter(this.t, this.w);
        this.u.addView(this.n, new LinearLayout.LayoutParams(-1, -1));
        this.n.loadFirstPage();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.t.b();
        this.n.onPause();
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        this.t.c();
        this.n.onResume();
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }
}
