package com.tencent.assistantv2.activity;

import android.os.Bundle;
import android.widget.RelativeLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.protocol.jce.PopUpInfo;

/* compiled from: ProGuard */
public class PopWindowActivity extends BaseActivity {
    private RelativeLayout n;
    private PopUpInfo t;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_popwindow);
        i();
        j();
    }

    private void i() {
        this.t = (PopUpInfo) getIntent().getSerializableExtra("extra_pop_info");
    }

    private void j() {
        this.n = (RelativeLayout) findViewById(R.id.page);
    }
}
