package com.tencent.assistantv2.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.adapter.AppAdapter;
import com.tencent.assistant.component.LoadingView;
import com.tencent.assistant.component.NormalErrorRecommendPage;
import com.tencent.assistant.component.txscrollview.ITXRefreshListViewListener;
import com.tencent.assistant.component.txscrollview.TXGetMoreListView;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.protocol.jce.CardItem;
import com.tencent.assistant.protocol.jce.GetAppFromTagResponse;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.ct;
import com.tencent.assistant.utils.df;
import com.tencent.assistantv2.b.a;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import com.tencent.assistantv2.model.a.b;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.model.STCommonInfo;
import com.tencent.assistantv2.st.page.STInfoV2;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class ShowSameTagAppsActivity extends BaseActivity implements ITXRefreshListViewListener, b {
    private static int D = STConst.ST_TAG_DETAIL_PAGE;
    /* access modifiers changed from: private */
    public static int E = 29;
    private AppAdapter A;
    private LoadingView B;
    private NormalErrorRecommendPage C;
    /* access modifiers changed from: private */
    public int F = 0;
    private String G = "04";
    private View.OnClickListener H = new dw(this);
    /* access modifiers changed from: private */
    public a n;
    /* access modifiers changed from: private */
    public String t;
    /* access modifiers changed from: private */
    public String u;
    /* access modifiers changed from: private */
    public String v;
    /* access modifiers changed from: private */
    public String w;
    private ArrayList<SimpleAppModel> x;
    private SecondNavigationTitleViewV5 y;
    private TXGetMoreListView z;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.activity.ShowSameTagAppsActivity.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.tencent.assistant.activity.BaseActivity.a(java.lang.String, java.lang.String):void
      com.tencent.assistantv2.activity.ShowSameTagAppsActivity.a(boolean, boolean):void */
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        v();
        super.onCreate(bundle);
        setContentView((int) R.layout.author_other_apps_layout);
        w();
        this.n = new a();
        this.n.register(this);
        if (this.v == null || this.w == null) {
            this.n.a(this.t, this.u, 1, "1", this.F, this.F + E);
        } else {
            this.n.a(this.t, this.u, (long) Integer.parseInt(this.v), this.w, this.F, this.F + E);
        }
        a(false, false);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.y.l();
        this.A.notifyDataSetChanged();
    }

    /* access modifiers changed from: private */
    public void a(boolean z2, boolean z3) {
        if (z2) {
            this.B.setVisibility(4);
            if (z3) {
                this.z.setVisibility(0);
                this.C.setVisibility(4);
                return;
            }
            this.z.setVisibility(4);
            this.C.setVisibility(0);
            return;
        }
        this.B.setVisibility(0);
        this.z.setVisibility(4);
        this.C.setVisibility(4);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.y.m();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.n.unregister(this);
        super.onDestroy();
    }

    public void h() {
        k.a(i());
    }

    /* access modifiers changed from: package-private */
    public STInfoV2 i() {
        STInfoV2 s = s();
        s.slotId = STConst.ST_DEFAULT_SLOT;
        s.appId = ct.c(this.v);
        s.packageName = this.w;
        s.contentId = com.tencent.assistantv2.st.page.a.a(STCommonInfo.ContentIdType.DETAILAPPTAG, this.t);
        s.extraData = this.u;
        return s;
    }

    public int f() {
        return D;
    }

    private void v() {
        Intent intent = getIntent();
        this.t = intent.getStringExtra("tag_id");
        this.u = intent.getStringExtra("tag_name");
        this.v = intent.getStringExtra("tag_app_id");
        this.w = intent.getStringExtra("tag_pkg_name");
    }

    private void w() {
        this.y = (SecondNavigationTitleViewV5) findViewById(R.id.title);
        this.y.a(this);
        this.y.b(this.u);
        this.y.i();
        this.z = (TXGetMoreListView) findViewById(R.id.list);
        ImageView imageView = new ImageView(this);
        imageView.setLayoutParams(new AbsListView.LayoutParams(-1, df.a(this, 6.0f)));
        imageView.setBackgroundColor(getResources().getColor(17170445));
        this.z.addHeaderView(imageView);
        com.tencent.assistant.model.b bVar = new com.tencent.assistant.model.b();
        bVar.b(this.x);
        this.A = new AppAdapter(this, this.z, bVar);
        this.A.a(f(), 0, this.G);
        this.z.setAdapter(this.A);
        this.z.setDivider(null);
        this.z.setRefreshListViewListener(this);
        this.B = (LoadingView) findViewById(R.id.loading_view);
        this.C = (NormalErrorRecommendPage) findViewById(R.id.error_page);
        this.C.setButtonClickListener(this.H);
    }

    public void onTXRefreshListViewRefresh(TXScrollViewBase.ScrollState scrollState) {
        if (TXScrollViewBase.ScrollState.ScrollState_FromEnd != scrollState) {
            return;
        }
        if (this.v == null || this.w == null) {
            this.n.a(this.t, this.u, 1, "1", this.F, this.F + E);
        } else {
            this.n.a(this.t, this.u, (long) Integer.parseInt(this.v), this.w, this.F, this.F + E);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.activity.ShowSameTagAppsActivity.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.tencent.assistant.activity.BaseActivity.a(java.lang.String, java.lang.String):void
      com.tencent.assistantv2.activity.ShowSameTagAppsActivity.a(boolean, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.adapter.AppAdapter.a(boolean, java.util.List<com.tencent.assistant.model.SimpleAppModel>):void
     arg types: [int, java.util.ArrayList<com.tencent.assistant.model.SimpleAppModel>]
     candidates:
      com.tencent.assistant.adapter.AppAdapter.a(com.tencent.assistant.model.SimpleAppModel, int):com.tencent.assistantv2.st.page.STInfoV2
      com.tencent.assistant.adapter.AppAdapter.a(boolean, java.util.List<com.tencent.assistant.model.SimpleAppModel>):void */
    public void a(int i, int i2, GetAppFromTagResponse getAppFromTagResponse) {
        if (i2 == 0) {
            if (getAppFromTagResponse.c == null || getAppFromTagResponse.c.size() == 0) {
                this.C.setErrorType(10);
                a(true, false);
                return;
            }
            a(true, true);
            if (getAppFromTagResponse.c.size() > 0) {
                this.x = new ArrayList<>();
                Iterator<CardItem> it = getAppFromTagResponse.c.iterator();
                while (it.hasNext()) {
                    CardItem next = it.next();
                    SimpleAppModel simpleAppModel = new SimpleAppModel();
                    simpleAppModel.f1634a = next.f.f2310a;
                    simpleAppModel.d = next.f.b;
                    simpleAppModel.e = next.f.c;
                    simpleAppModel.k = next.f.d;
                    simpleAppModel.q = next.f.k.b;
                    simpleAppModel.i = next.f.e;
                    simpleAppModel.g = next.f.h;
                    simpleAppModel.p = next.f.i;
                    simpleAppModel.c = next.f.f;
                    simpleAppModel.f = next.f.g;
                    simpleAppModel.b = next.f.p;
                    simpleAppModel.ac = next.f.s;
                    simpleAppModel.U = SimpleAppModel.CARD_TYPE.NORMAL;
                    if (!TextUtils.isEmpty(next.e)) {
                        try {
                            simpleAppModel.X = Html.fromHtml(next.e);
                        } catch (Exception e) {
                            simpleAppModel.X = next.e;
                            e.printStackTrace();
                        }
                    }
                    simpleAppModel.y = next.h;
                    simpleAppModel.au = next.q;
                    this.x.add(simpleAppModel);
                }
            }
            this.A.a(false, (List<SimpleAppModel>) this.x);
            this.A.notifyDataSetChanged();
            if (getAppFromTagResponse.e <= this.F + E + 1) {
                this.z.onRefreshComplete(false);
            } else {
                this.z.onRefreshComplete(true);
            }
            this.F += E + 1;
        } else if (i2 == -800) {
            this.C.setErrorType(30);
            a(true, false);
        } else {
            this.C.setErrorType(20);
            a(true, false);
        }
    }
}
