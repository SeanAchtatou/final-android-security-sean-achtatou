package com.tencent.assistantv2.activity;

import com.qq.AppService.AstApp;
import com.tencent.assistant.m;
import com.tencent.assistant.module.timer.RecoverAppListReceiver;
import com.tencent.assistant.protocol.jce.GetPhoneUserAppListResponse;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.bh;
import com.tencent.assistantv2.model.a.i;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
class db implements i {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ MainActivity f2825a;

    private db(MainActivity mainActivity) {
        this.f2825a = mainActivity;
    }

    /* synthetic */ db(MainActivity mainActivity, ck ckVar) {
        this(mainActivity);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistantv2.activity.MainActivity, com.tencent.assistant.protocol.jce.GetPhoneUserAppListResponse, boolean):void
     arg types: [com.tencent.assistantv2.activity.MainActivity, com.tencent.assistant.protocol.jce.GetPhoneUserAppListResponse, int]
     candidates:
      com.tencent.assistantv2.activity.MainActivity.a(int, int, java.lang.String):void
      com.tencent.assistantv2.activity.MainActivity.a(android.graphics.Bitmap, java.lang.String, android.content.Intent):void
      com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistantv2.b.al, int, int):void
      com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistantv2.activity.MainActivity, com.tencent.assistant.protocol.jce.DesktopShortCut, com.tencent.assistant.protocol.jce.DesktopShortCut):boolean
      com.tencent.assistant.activity.BaseActivity.a(com.tencent.assistantv2.st.page.STPageInfo, java.lang.String, java.lang.String):void
      android.support.v4.app.FragmentActivity.a(java.lang.String, java.io.PrintWriter, android.view.View):void
      android.support.v4.app.FragmentActivity.a(java.lang.String, boolean, boolean):android.support.v4.app.z
      android.support.v4.app.FragmentActivity.a(android.support.v4.app.Fragment, android.content.Intent, int):void
      com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistantv2.activity.MainActivity, com.tencent.assistant.protocol.jce.GetPhoneUserAppListResponse, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistantv2.activity.MainActivity, boolean):void
     arg types: [com.tencent.assistantv2.activity.MainActivity, int]
     candidates:
      com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistantv2.activity.MainActivity, int):int
      com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistantv2.activity.MainActivity, com.tencent.assistant.db.table.z):com.tencent.assistant.db.table.z
      com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistantv2.activity.MainActivity, java.util.ArrayList):java.util.ArrayList
      com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistant.protocol.jce.GetPhoneUserAppListResponse, boolean):void
      com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistantv2.activity.MainActivity, android.content.Intent):void
      com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistantv2.activity.MainActivity, com.tencent.assistant.protocol.jce.DesktopShortCut):void
      com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistant.protocol.jce.DesktopShortCut, com.tencent.assistant.protocol.jce.DesktopShortCut):boolean
      com.tencent.assistantv2.activity.MainActivity.a(int, boolean):void
      com.tencent.assistant.activity.BaseActivity.a(java.lang.String, java.lang.String):void
      com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistantv2.activity.MainActivity, boolean):void */
    public void a(int i, int i2, GetPhoneUserAppListResponse getPhoneUserAppListResponse) {
        if (i2 != 0 || getPhoneUserAppListResponse == null || getPhoneUserAppListResponse.c == null || getPhoneUserAppListResponse.c.size() <= 0) {
            this.f2825a.b(true);
            return;
        }
        m.a().b(Constants.STR_EMPTY, "key_re_app_list_state", Integer.valueOf(RecoverAppListReceiver.RecoverAppListState.SECONDLY_SHORT.ordinal()));
        m.a().a("key_re_app_list_second_response", bh.a(getPhoneUserAppListResponse));
        MainActivity.x = RecoverAppListReceiver.RecoverAppListState.SECONDLY_SHORT.ordinal();
        XLog.e("zhangyuanchao", "-------onRecoverAppListGet------:" + AstApp.i().l() + ",----");
        this.f2825a.a(getPhoneUserAppListResponse, false);
    }
}
