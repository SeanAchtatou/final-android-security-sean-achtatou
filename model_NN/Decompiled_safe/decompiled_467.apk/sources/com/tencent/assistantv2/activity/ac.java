package com.tencent.assistantv2.activity;

import android.content.Intent;
import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.AboutActivity;
import com.tencent.assistant.activity.SettingActivity;
import com.tencent.assistant.activity.ShareBaseActivity;
import com.tencent.assistant.component.listener.OnTMAParamExClickListener;
import com.tencent.assistant.g.p;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class ac extends OnTMAParamExClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AssistantTabActivity f2750a;

    ac(AssistantTabActivity assistantTabActivity) {
        this.f2750a = assistantTabActivity;
    }

    public void onTMAClick(View view) {
        switch (view.getId()) {
            case R.id.layout_score /*2131165352*/:
            case R.id.switcher_tips /*2131165353*/:
                this.f2750a.i();
                return;
            case R.id.layout_toast /*2131165358*/:
                if (this.f2750a.af != null) {
                    this.f2750a.u.startActivity(new Intent(this.f2750a.u, this.f2750a.af));
                    return;
                }
                return;
            case R.id.layout_title /*2131165363*/:
                this.f2750a.finish();
                return;
            case R.id.layout_update /*2131165367*/:
                this.f2750a.K();
                return;
            case R.id.layout_setting /*2131165373*/:
                Intent intent = new Intent(this.f2750a.u, SettingActivity.class);
                intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, this.f2750a.f());
                this.f2750a.u.startActivity(intent);
                return;
            case R.id.layout_share /*2131165376*/:
                p.a().a((ShareBaseActivity) this.f2750a.u);
                return;
            case R.id.layout_about /*2131165377*/:
                this.f2750a.u.startActivity(new Intent(this.f2750a.u, AboutActivity.class));
                return;
            default:
                return;
        }
    }

    public STInfoV2 getStInfo(View view) {
        String str = STConst.ST_DEFAULT_SLOT;
        if (view.getTag(R.id.tma_st_slot_tag) instanceof String) {
            str = (String) view.getTag(R.id.tma_st_slot_tag);
        }
        return this.f2750a.a(str, STConst.ST_STATUS_DEFAULT, 200);
    }
}
