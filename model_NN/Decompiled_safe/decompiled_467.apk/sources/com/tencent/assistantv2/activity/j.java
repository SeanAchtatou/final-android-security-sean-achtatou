package com.tencent.assistantv2.activity;

import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.model.c;
import com.tencent.assistant.module.t;
import com.tencent.assistant.module.update.k;
import com.tencent.assistant.protocol.jce.AppUpdateInfo;

/* compiled from: ProGuard */
class j implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppDetailActivityV5 f2852a;

    j(AppDetailActivityV5 appDetailActivityV5) {
        this.f2852a = appDetailActivityV5;
    }

    public void run() {
        t a2;
        AppUpdateInfo a3 = k.b().a(this.f2852a.ac.c);
        if (a3 == null || a3.d != this.f2852a.ac.g) {
            LocalApkInfo unused = this.f2852a.aQ = ApkResourceManager.getInstance().getInstalledApkInfo(this.f2852a.ac.c, true);
            a2 = this.f2852a.Z.a(this.f2852a.ac, this.f2852a.aQ, this.f2852a.aL, this.f2852a.aN, this.f2852a.aO, this.f2852a.aP);
        } else {
            a2 = this.f2852a.Z.a(this.f2852a.ac, this.f2852a.aL, this.f2852a.aN, this.f2852a.aO, this.f2852a.aP);
        }
        if (a2 != null) {
            c unused2 = this.f2852a.ae = a2.f1857a;
            this.f2852a.v[3] = a2.b;
        }
    }
}
