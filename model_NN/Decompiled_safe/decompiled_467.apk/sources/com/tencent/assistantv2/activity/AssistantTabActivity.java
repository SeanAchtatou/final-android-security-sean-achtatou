package com.tencent.assistantv2.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewSwitcher;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.Global;
import com.tencent.assistant.activity.ApkMgrActivity;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.activity.ShareBaseActivity;
import com.tencent.assistant.activity.SpaceCleanActivity;
import com.tencent.assistant.activity.UpdateListActivity;
import com.tencent.assistant.component.AssistantUpdateOverTurnView;
import com.tencent.assistant.component.listener.OnTMAParamExClickListener;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.cq;
import com.tencent.assistant.manager.spaceclean.SpaceScanManager;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.callback.g;
import com.tencent.assistant.module.callback.o;
import com.tencent.assistant.module.cz;
import com.tencent.assistant.module.u;
import com.tencent.assistant.module.update.AppUpdateConst;
import com.tencent.assistant.module.update.c;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.plugin.PluginProxyUtils;
import com.tencent.assistant.protocol.jce.AppUpdateInfo;
import com.tencent.assistant.securescan.StartScanActivity;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.bk;
import com.tencent.assistant.utils.br;
import com.tencent.assistant.utils.df;
import com.tencent.assistant.utils.t;
import com.tencent.assistantv2.adapter.AssistantTabAdapter;
import com.tencent.assistantv2.component.ScaleRelativeLayout;
import com.tencent.assistantv2.component.SimpleScrollView;
import com.tencent.assistantv2.component.bn;
import com.tencent.assistantv2.component.cu;
import com.tencent.assistantv2.component.de;
import com.tencent.assistantv2.component.j;
import com.tencent.assistantv2.model.e;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.STPageInfo;
import com.tencent.beacon.event.a;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: ProGuard */
public class AssistantTabActivity extends ShareBaseActivity implements Handler.Callback, UIEventListener {
    private FrameLayout A;
    private RelativeLayout B;
    private RelativeLayout C;
    private RelativeLayout D;
    /* access modifiers changed from: private */
    public ScaleRelativeLayout E;
    /* access modifiers changed from: private */
    public RelativeLayout F;
    private RelativeLayout G;
    private ImageView H;
    /* access modifiers changed from: private */
    public ImageView I;
    /* access modifiers changed from: private */
    public ImageView J;
    private ImageView K;
    private ImageView L;
    private Animation M;
    private Animation N;
    private Animation O;
    /* access modifiers changed from: private */
    public ViewSwitcher P;
    private TextView Q;
    private TextView R;
    private TextView S;
    /* access modifiers changed from: private */
    public TextView T;
    private TextView U;
    private TextView V;
    private TextView W;
    private AssistantUpdateOverTurnView X;
    private List<AppUpdateInfo> Y;
    private List<AppUpdateInfo> Z;
    private final String aA = "05_002";
    private final String aB = "05_003";
    /* access modifiers changed from: private */
    public int aC = 80;
    /* access modifiers changed from: private */
    public int aD = 99;
    private String aE = Constants.STR_EMPTY;
    private boolean aF = true;
    /* access modifiers changed from: private */
    public boolean aG = false;
    /* access modifiers changed from: private */
    public boolean aH = false;
    private Animation aI;
    private Animation aJ;
    private Animation aK;
    private Animation aL;
    private bk aM;
    /* access modifiers changed from: private */
    public j aN;
    private j aO;
    /* access modifiers changed from: private */
    public float aP = 0.0f;
    /* access modifiers changed from: private */
    public float aQ = 0.0f;
    private float aR;
    private int aS = 0;
    private int aT = 0;
    /* access modifiers changed from: private */
    public volatile boolean aU = true;
    private final int aV = 100;
    private final int aW = 90;
    private final int aX = 75;
    private final int aY = 0;
    /* access modifiers changed from: private */
    public boolean aZ = false;
    private int aa = 4;
    private long ab = 5000;
    private final int ac = 300;
    private final int ad = EventDispatcherEnum.CACHE_EVENT_END;
    private AtomicInteger ae = new AtomicInteger(2);
    /* access modifiers changed from: private */
    public Class<?> af;
    /* access modifiers changed from: private */
    public List<e> ag = new ArrayList();
    /* access modifiers changed from: private */
    public int ah = -1;
    /* access modifiers changed from: private */
    public long ai = 0;
    /* access modifiers changed from: private */
    public long aj = 0;
    /* access modifiers changed from: private */
    public int ak = 0;
    /* access modifiers changed from: private */
    public int al = 0;
    private boolean am = false;
    private int an = 0;
    private TextView ao;
    private final String ap = "03_001";
    private final String aq = "06_001";
    private final String ar = "08_001";
    private final String as = "08_002";
    private final String at = "08_003";
    private final String au = "05_001";
    private final String av = "05_002";
    private final String aw = "05_003";
    private final String ax = "04_001";
    private final String ay = "04_002";
    private final String az = "05_001";
    private boolean ba = false;
    private boolean bb = true;
    private bn bc;
    private volatile boolean bd = false;
    /* access modifiers changed from: private */
    public volatile boolean be = false;
    /* access modifiers changed from: private */
    public int bf = 2;
    /* access modifiers changed from: private */
    public Handler bg;
    private boolean bh = false;
    private boolean bi = false;
    private final int bj = 1;
    private final int bk = 2;
    private Typeface bl;
    private boolean bm = false;
    private de bn = new au(this);
    private cu bo = new av(this);
    private OnTMAParamExClickListener bp = new ac(this);
    private g bq = new ao(this);
    o n = new y(this);
    private String t = "Assistant";
    /* access modifiers changed from: private */
    public Context u;
    /* access modifiers changed from: private */
    public SimpleScrollView v;
    private GridView w;
    /* access modifiers changed from: private */
    public AssistantTabAdapter x;
    private LinearLayout y;
    private FrameLayout z;

    static /* synthetic */ int n(AssistantTabActivity assistantTabActivity) {
        int i = assistantTabActivity.bf;
        assistantTabActivity.bf = i - 1;
        return i;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        try {
            setContentView((int) R.layout.activity_asistant_layout_v5);
            this.u = this;
            x();
            C();
            I();
            D();
            cz.a().register(this.n);
            if (m.a().L()) {
                R();
            }
            com.tencent.assistant.floatingwindow.j.a().b();
            w();
        } catch (Throwable th) {
            this.bm = true;
            finish();
            cq.a().b();
        }
    }

    private void w() {
        HashMap hashMap = new HashMap();
        hashMap.put("B1", Global.getPhoneGuidAndGen());
        hashMap.put("B2", Global.getQUAForBeacon());
        hashMap.put("B3", t.g());
        XLog.d("beacon", "beacon report >> expose_managehome. " + hashMap.toString());
        a.a("expose_managehome", true, -1, -1, hashMap, true);
    }

    private void x() {
        this.bg = new Handler(this);
        this.aM = y();
        this.aC = br.a();
        this.aE = d(this.aC);
        b("03_001", String.valueOf(this.aC));
        b("06_001", String.valueOf(u.i()));
        this.aR = getResources().getDimension(R.dimen.manager_layout_score_height);
        if (this.aR == 0.0f) {
            this.aR = (float) df.a(getApplicationContext(), 127.0f);
        }
        this.aP = 0.0f;
        this.aQ = this.aR;
        TemporaryThreadManager.get().start(new aj(this));
    }

    /* access modifiers changed from: private */
    public bk y() {
        if (this.aM == null) {
            this.aM = new bk();
        }
        return this.aM;
    }

    /* access modifiers changed from: private */
    public void c(int i) {
        boolean z2 = true;
        int i2 = 0;
        boolean z3 = m.a().a("update_newest_versioncode", 0) > t.o();
        if (i <= 0 && !z3) {
            z2 = false;
        }
        if (this.H != null) {
            ImageView imageView = this.H;
            if (!z2) {
                i2 = 8;
            }
            imageView.setVisibility(i2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean
      com.tencent.assistant.m.a(java.lang.String, long):long */
    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (!this.bm) {
            this.bb = true;
            if (this.x != null) {
                this.x.d();
            }
            b(d(this.aC));
            z();
            B();
            L();
            this.bg.sendEmptyMessageDelayed(3, 300);
            if (this.ae.get() == 2 && !this.aG && !this.be) {
                this.bg.sendEmptyMessageDelayed(6, 300);
            }
            if (System.currentTimeMillis() - m.a().a("app_update_response_succ_time", -1L) >= 600000) {
                c.a().a(AppUpdateConst.RequestLaunchType.TYPE_ASSISTANT_RETRY, (Map<String, String>) null);
            }
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_ADD, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE, this);
            AstApp.i().k().addUIEventListener(1016, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_UPDATE_LIST_CHANGED, this);
            AstApp.i().k().addUIEventListener(1019, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_UPDATE_IGNORE_LIST_DELETE, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_UPDATE_START, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_UPDATE_DELETE, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_MGR_APK_DEL_SUCCESS, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_MGR_APK_DEL_FAIL, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_MGR_MEMORY_CLEAN_SUCCESS, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_MGR_MEMORY_CLEAN_FAIL, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_MGR_SPACE_CLEAN_SUCCESS, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_MGR_SPACE_CLEAN_FAIL, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_MGR_SAFE_SCAN_SUCCESS, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_MGR_SAFE_SCAN_FAIL, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_INSTALL, this);
        }
    }

    /* access modifiers changed from: private */
    public void z() {
        String name;
        if (this.aF) {
            this.aF = false;
        } else if (this.aC == 100) {
            M();
        } else if (this.ah >= 0 && this.ah < this.ag.size()) {
            int i = this.ag.get(this.ah).c;
            if (this.af == null) {
                name = this.ag.get(this.ah).f3320a.getName();
            } else {
                name = this.af.getName();
            }
            if (name == null) {
                name = Constants.STR_EMPTY;
            }
            if (!TextUtils.isEmpty(name)) {
                if (name.equals(SpaceCleanActivity.class.getName())) {
                    if (br.c() && !this.aH) {
                        v();
                    }
                    if (this.aH) {
                        br.b(0, SpaceCleanActivity.class);
                    }
                }
                if (name.equals(StartScanActivity.class.getName())) {
                    if (br.b() == 0) {
                        br.a(0, StartScanActivity.class);
                    }
                    if (br.i() == 0) {
                        this.al = 0;
                    }
                }
                if (name.equals(ApkMgrActivity.class.getName()) && br.i() == 0) {
                    this.ak = 0;
                }
                if (br.i() != i) {
                    this.aC += i - br.i();
                    if (this.aC >= 100) {
                        this.aC = 100;
                    }
                    D();
                    String string = getString(R.string.manage_need_more);
                    if (this.aC == 100) {
                        string = getString(R.string.manage_great);
                    }
                    b(string);
                    A();
                    this.bg.postDelayed(new as(this), 350);
                } else if (this.ba && this.ag != null && this.ag.size() > this.ah) {
                    this.bg.postDelayed(new ar(this, this.ag.get(this.ah).b), 350);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (!this.bm) {
            if (this.x != null) {
                this.x.c();
            }
            this.bb = false;
            this.bg.removeMessages(5);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_ADD, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE, this);
            AstApp.i().k().removeUIEventListener(1016, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_UPDATE_LIST_CHANGED, this);
            AstApp.i().k().removeUIEventListener(1019, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_UPDATE_IGNORE_LIST_DELETE, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_UPDATE_START, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_UPDATE_DELETE, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_INSTALL, this);
        }
    }

    /* access modifiers changed from: private */
    public String d(int i) {
        if (i >= 90 && i < 100) {
            return getString(R.string.manage_opt_90_99);
        }
        if (i >= 75 && i < 90) {
            return getString(R.string.manage_opt_75_90);
        }
        if (i > 0 && i < 75) {
            return getString(R.string.manage_opt_0_75);
        }
        if (i == 100) {
            return getString(R.string.manage_opt_100);
        }
        return Constants.STR_EMPTY;
    }

    /* access modifiers changed from: private */
    public void b(String str) {
        this.Q.setText(str);
        this.U.setText(str);
    }

    private void A() {
        if (this.ah >= 0 && this.ah < this.ag.size()) {
            this.ag.remove(this.ah);
        }
    }

    private void B() {
        if (this.Z == null) {
            this.Z = new ArrayList();
        }
        this.Z.clear();
        long currentTimeMillis = System.currentTimeMillis();
        this.Y = a(u.g());
        XLog.i("Jie", ">updateAppinfo cost=" + (System.currentTimeMillis() - currentTimeMillis));
        int size = this.Y.size();
        if (size >= 1) {
            if (size > this.aa) {
                size = this.aa;
            }
            int nextInt = new Random().nextInt(size);
            if (this.Y != null) {
                for (int i = nextInt; i < size; i++) {
                    this.Z.add(this.Y.get(i));
                }
                for (int i2 = 0; i2 < nextInt; i2++) {
                    this.Z.add(this.Y.get(i2));
                }
            }
        }
    }

    private void C() {
        int f = ((BaseActivity) this.u).f();
        this.y = (LinearLayout) findViewById(R.id.layout_title);
        this.v = (SimpleScrollView) findViewById(R.id.scroll_layout);
        this.w = (GridView) findViewById(R.id.sliding_frame_layout);
        this.w.setFocusable(false);
        this.x = new AssistantTabAdapter(this.u);
        this.w.setAdapter((ListAdapter) this.x);
        this.w.setOnItemClickListener(new at(this, f));
        this.G = (RelativeLayout) findViewById(R.id.layout_update);
        this.G.setTag(R.id.tma_st_slot_tag, "06_001");
        this.X = (AssistantUpdateOverTurnView) findViewById(R.id.app_overturn);
        this.ao = (TextView) findViewById(R.id.tv_update_number);
        this.bl = PluginProxyUtils.getTypeFace();
        this.V = (TextView) findViewById(R.id.tv_score);
        this.W = (TextView) findViewById(R.id.tv_point);
        this.V.setTypeface(this.bl);
        this.W.setTypeface(this.bl);
        this.z = (FrameLayout) findViewById(R.id.layout_score);
        this.z.setTag(R.id.tma_st_slot_tag, "03_001");
        this.A = (FrameLayout) findViewById(R.id.layout_circle);
        this.I = (ImageView) findViewById(R.id.iv_outer_circle);
        this.J = (ImageView) findViewById(R.id.iv_inner_circle);
        this.K = (ImageView) findViewById(R.id.iv_admin_circle);
        this.L = (ImageView) findViewById(R.id.pb_scanning);
        this.B = (RelativeLayout) findViewById(R.id.layout_setting);
        this.B.setTag(R.id.tma_st_slot_tag, "08_001");
        this.C = (RelativeLayout) findViewById(R.id.layout_share);
        this.C.setTag(R.id.tma_st_slot_tag, "08_002");
        this.D = (RelativeLayout) findViewById(R.id.layout_about);
        this.D.setTag(R.id.tma_st_slot_tag, "08_003");
        this.E = (ScaleRelativeLayout) findViewById(R.id.layout_toast);
        this.F = (RelativeLayout) findViewById(R.id.layout_toast_info);
        this.Q = (TextView) findViewById(R.id.tv_tips);
        this.U = (TextView) findViewById(R.id.tv_tips2);
        b(this.aE);
        this.R = (TextView) findViewById(R.id.tv_toast);
        this.S = (TextView) findViewById(R.id.tv_no_update);
        this.T = (TextView) findViewById(R.id.tv_tips_score);
        this.P = (ViewSwitcher) findViewById(R.id.switcher_tips);
        this.P.setTag(R.id.tma_st_slot_tag, "03_001");
        this.B.setOnClickListener(this.bp);
        this.C.setOnClickListener(this.bp);
        this.D.setOnClickListener(this.bp);
        this.z.setOnClickListener(this.bp);
        this.E.setOnClickListener(this.bp);
        this.E.a(this.bo);
        this.G.setOnClickListener(this.bp);
        this.y.setOnClickListener(this.bp);
        this.P.setOnClickListener(this.bp);
        this.H = (ImageView) findViewById(R.id.about_promot);
        this.v.a(this.bn);
        this.v.a(this.aQ);
    }

    /* access modifiers changed from: private */
    public void a(float f) {
        this.V.setText(String.valueOf(Math.round(f)));
    }

    /* access modifiers changed from: private */
    public void D() {
        if (this.aN == null || !this.aN.a()) {
            if (this.bc == null) {
                this.bc = new bn((float) this.aD, (float) this.aC, new aw(this));
            }
            this.bc.a((float) this.aD, (float) this.aC);
            this.bc.setDuration((long) Math.min(Math.abs(this.aD - this.aC) * 100, (int) EventDispatcherEnum.CACHE_EVENT_START));
            this.y.startAnimation(this.bc);
            this.T.setText(getString(R.string.manage_score, new Object[]{Integer.valueOf(this.aC)}));
            return;
        }
        a((float) this.aC);
    }

    private void F() {
        this.y.clearAnimation();
    }

    private void b(boolean z2) {
        a((float) this.aC);
        if (this.aN == null || this.aN.a() || z2) {
            if (this.bg != null) {
                this.bg.removeMessages(6);
            }
            this.I.clearAnimation();
            this.J.clearAnimation();
            this.I.setVisibility(8);
            this.J.setVisibility(8);
        }
    }

    /* access modifiers changed from: private */
    public void G() {
        if (this.be) {
            return;
        }
        if (this.aN == null || !this.aN.a()) {
            this.I.setVisibility(0);
            this.J.setVisibility(0);
            this.I.startAnimation(this.M);
            this.J.startAnimation(this.N);
            if (this.bg != null && !isFinishing()) {
                this.bg.removeMessages(6);
                this.bg.sendEmptyMessageDelayed(6, 4000);
            }
        }
    }

    private boolean H() {
        if ((this.aN != null && this.aN.a()) || this.L.getVisibility() == 0) {
            return false;
        }
        this.I.startAnimation(this.O);
        this.K.setVisibility(8);
        this.L.setVisibility(0);
        try {
            this.L.setBackgroundResource(R.drawable.admin_loadcircle);
        } catch (Throwable th) {
            cq.a().b();
        }
        this.L.startAnimation(this.aJ);
        b(getString(R.string.manage_opt));
        return true;
    }

    /* access modifiers changed from: private */
    public void c(boolean z2) {
        if (this.aN == null || this.aN.a() || z2) {
            if (this.I != null) {
                this.I.clearAnimation();
            }
            if (this.L != null) {
                this.L.clearAnimation();
                this.L.setVisibility(8);
            }
            if (this.K != null) {
                this.K.setVisibility(0);
            }
        }
    }

    private void I() {
        this.aJ = AnimationUtils.loadAnimation(this, R.anim.speedcircle);
        this.M = AnimationUtils.loadAnimation(this, R.anim.mgr_outter_circle_scale);
        this.M.setAnimationListener(new ax(this));
        this.N = AnimationUtils.loadAnimation(this, R.anim.mgr_inner_circle_scale);
        this.N.setAnimationListener(new z(this));
        this.O = AnimationUtils.loadAnimation(this, R.anim.mgr_click_circle_scale);
        this.aI = AnimationUtils.loadAnimation(this, R.anim.mgr_fade_in);
        this.aI.setFillAfter(true);
        this.O.setAnimationListener(new aa(this));
        this.aK = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.mgr_switcher_in);
        this.aL = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.mgr_switcher_out);
        this.aK.setAnimationListener(new ab(this));
        O();
        P();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.activity.AssistantTabActivity.a(boolean, long):void
     arg types: [int, long]
     candidates:
      com.tencent.assistantv2.activity.AssistantTabActivity.a(com.tencent.assistantv2.activity.AssistantTabActivity, java.lang.Class):java.lang.Class
      com.tencent.assistantv2.activity.AssistantTabActivity.a(com.tencent.assistantv2.activity.AssistantTabActivity, float):void
      com.tencent.assistantv2.activity.AssistantTabActivity.a(com.tencent.assistantv2.activity.AssistantTabActivity, int):void
      com.tencent.assistantv2.activity.AssistantTabActivity.a(com.tencent.assistantv2.activity.AssistantTabActivity, java.lang.String):void
      com.tencent.assistantv2.activity.AssistantTabActivity.a(com.tencent.assistantv2.activity.AssistantTabActivity, boolean):boolean
      com.tencent.assistant.activity.BaseActivity.a(java.lang.String, java.lang.String):void
      com.tencent.assistantv2.activity.AssistantTabActivity.a(boolean, long):void */
    public void i() {
        J();
        b(true);
        if (this.aN != null && this.aN.a()) {
            a(true, (long) ((1.0f - this.aN.b()) * 500.0f));
            this.aZ = true;
        } else if (H() && !this.be) {
            if (this.aG) {
                this.bg.sendEmptyMessageDelayed(7, 1000);
                return;
            }
            this.be = true;
            this.bg.sendEmptyMessageDelayed(7, 35000);
            TemporaryThreadManager.get().start(new ad(this));
            TemporaryThreadManager.get().start(new ae(this));
            TemporaryThreadManager.get().start(new af(this));
            TemporaryThreadManager.get().start(new ag(this));
        }
    }

    private void J() {
        HashMap hashMap = new HashMap();
        hashMap.put("B1", Global.getPhoneGuidAndGen());
        hashMap.put("B2", Global.getQUAForBeacon());
        hashMap.put("B3", t.g());
        XLog.d("beacon", "beacon report >> click_manage_circle. " + hashMap.toString());
        a.a("click_manage_circle", true, -1, -1, hashMap, true);
    }

    /* access modifiers changed from: private */
    public void K() {
        Intent intent = new Intent(this.u, UpdateListActivity.class);
        intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, f());
        this.u.startActivity(intent);
    }

    public int o() {
        return 0;
    }

    public int f() {
        return STConst.ST_PAGE_ASSISTANT;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (!this.bm) {
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_MGR_APK_DEL_SUCCESS, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_MGR_APK_DEL_FAIL, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_MGR_MEMORY_CLEAN_SUCCESS, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_MGR_MEMORY_CLEAN_FAIL, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_MGR_SPACE_CLEAN_SUCCESS, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_MGR_SPACE_CLEAN_FAIL, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_MGR_SAFE_SCAN_SUCCESS, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_MGR_SAFE_SCAN_FAIL, this);
            b(true);
            this.bg.removeCallbacksAndMessages(null);
            this.v.a();
            y().g();
            cz.a().unregister(this.n);
            SpaceScanManager.a().b(this.bq);
        }
    }

    public boolean handleMessage(Message message) {
        switch (message.what) {
            case 3:
                c(cz.a().b());
                break;
            case 5:
                if (this.am) {
                    if (this.X != null) {
                        this.X.clearView();
                    }
                    B();
                    this.am = false;
                }
                L();
                break;
            case 6:
                this.bf = 2;
                G();
                break;
            case 7:
                this.ae.set(0);
                N();
                break;
        }
        return true;
    }

    private void L() {
        int i = u.i();
        this.an = i;
        if (i > 0) {
            this.ao.setVisibility(0);
            this.X.setVisibility(0);
            this.S.setVisibility(8);
            if (i > 99) {
                this.ao.setText("99+");
            } else {
                this.ao.setText(u.i() + Constants.STR_EMPTY);
            }
        } else {
            this.ao.setVisibility(8);
            this.X.setVisibility(8);
            this.S.setVisibility(0);
        }
        if (this.Z.size() != 0) {
            AppUpdateInfo remove = this.Z.remove(0);
            this.Z.add(remove);
            this.X.updateView(remove);
            if (this.Z.size() > 1 && this.bg != null) {
                this.bg.sendEmptyMessageDelayed(5, this.ab);
            }
        }
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE:
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_ADD:
            case 1016:
            case EventDispatcherEnum.UI_EVENT_APP_UPDATE_LIST_CHANGED:
            case 1019:
            case EventDispatcherEnum.UI_EVENT_APP_UPDATE_IGNORE_LIST_DELETE:
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_UPDATE_START:
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_UPDATE_DELETE:
                this.am = true;
                if (this.ao.getVisibility() != 0 || this.an < 2) {
                    if (this.bg != null) {
                        this.bg.removeMessages(5);
                    }
                    B();
                    L();
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_APP_INSTALL:
                if ((message.obj instanceof String) && AppConst.TENCENT_MOBILE_MANAGER_PKGNAME.equals((String) message.obj)) {
                    v();
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_MGR_MEMORY_CLEAN_SUCCESS:
                this.ae.decrementAndGet();
                long longValue = ((Long) message.obj).longValue();
                XLog.i("Jie", ">>memory clean success>>memory free size=" + longValue + " opcount=" + this.ae.get());
                this.ai = longValue * 1024;
                N();
                return;
            case EventDispatcherEnum.UI_EVENT_MGR_MEMORY_CLEAN_FAIL:
                this.ae.decrementAndGet();
                N();
                return;
            case EventDispatcherEnum.UI_EVENT_MGR_SPACE_CLEAN_SUCCESS:
                this.ae.decrementAndGet();
                this.aj = ((Long) message.obj).longValue();
                N();
                return;
            case EventDispatcherEnum.UI_EVENT_MGR_SPACE_CLEAN_FAIL:
                this.ae.decrementAndGet();
                N();
                return;
            case EventDispatcherEnum.UI_EVENT_MGR_APK_DEL_SUCCESS:
                this.ak = ((Integer) message.obj).intValue();
                return;
            case EventDispatcherEnum.UI_EVENT_MGR_APK_DEL_FAIL:
            case EventDispatcherEnum.UI_EVENT_MGR_SAFE_SCAN_FAIL:
            default:
                return;
            case EventDispatcherEnum.UI_EVENT_MGR_SAFE_SCAN_SUCCESS:
                this.al = ((Integer) message.obj).intValue();
                return;
        }
    }

    /* access modifiers changed from: private */
    public void c(String str) {
        if (!this.bb) {
            this.ba = true;
            return;
        }
        this.ba = false;
        if (this.E != null) {
            this.E.a(500);
            this.E.setVisibility(0);
            this.R.setText(str);
        }
        this.bg.postDelayed(new ah(this), 4000);
    }

    /* access modifiers changed from: private */
    public boolean M() {
        if (this.E == null || this.E.b() == -1) {
            return false;
        }
        this.E.a(400);
        this.E.setVisibility(-1);
        return true;
    }

    private void N() {
        if (this.ae.get() == 0) {
            this.ag.clear();
            br.e();
            br.f();
            runOnUiThread(new ai(this));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* access modifiers changed from: private */
    public void a(float f, float f2, float f3) {
        boolean z2 = true;
        if (f3 != 0.0f && this.aR != 0.0f) {
            F();
            b(f2 > 0.0f);
            if (f2 <= 0.0f) {
                z2 = false;
            }
            c(z2);
            O();
            P();
            float min = Math.min(Math.max(1.0f - ((f2 - f) / this.aR), this.aP), 1.0f);
            float min2 = Math.min(Math.max(1.0f - (f2 / this.aR), this.aP), 1.0f);
            this.aN.a(min, min2);
            this.aO.a(min, min2);
            this.aN.setDuration(0);
            this.aO.setDuration(0);
            this.z.startAnimation(this.aN);
            this.A.startAnimation(this.aO);
        }
    }

    /* access modifiers changed from: private */
    public void d(boolean z2) {
        if (!z2) {
            this.P.getChildAt(1).setVisibility(4);
            this.P.getChildAt(1).clearAnimation();
            this.P.getChildAt(0).clearAnimation();
            this.P.setInAnimation(null);
            this.P.setOutAnimation(null);
            this.P.setDisplayedChild(0);
        } else if (this.aU) {
            this.T.setText(getString(R.string.manage_score, new Object[]{Integer.valueOf(this.aC)}));
            this.P.getChildAt(1).setVisibility(0);
            this.P.setInAnimation(this.aK);
            this.P.setOutAnimation(this.aL);
            this.P.setDisplayedChild(1);
        }
    }

    public void onWindowFocusChanged(boolean z2) {
        super.onWindowFocusChanged(z2);
        if (!this.bh) {
            j();
        }
    }

    public void j() {
        boolean z2 = false;
        int[] iArr = new int[2];
        this.v.getLocationInWindow(iArr);
        if (this.aS == 0) {
            this.aS = iArr[1];
        }
        if (this.aT == 0) {
            this.aT = this.aS + this.v.getHeight();
        }
        if (((int) ((((float) ((int) ((((float) iArr[1]) + this.aR) - ((float) this.z.getHeight())))) - this.aR) + ((float) this.v.getChildAt(0).getMeasuredHeight()))) > this.aT) {
            z2 = true;
        }
        this.bi = z2;
        this.bh = true;
        this.v.a(this.bi);
    }

    private j O() {
        if (this.aN == null) {
            this.aN = new j(this.z, 1.0f, 1.0f, true);
            this.aN.setFillAfter(true);
            this.aN.a(new ak(this));
        }
        return this.aN;
    }

    private j P() {
        if (this.aO == null) {
            this.aO = new j(this.A, 1.0f, 1.0f, false);
            this.aO.setFillAfter(true);
        }
        return this.aO;
    }

    /* access modifiers changed from: private */
    public void a(boolean z2, long j) {
        float f = 1.0f;
        if (z2) {
            d(!z2);
        }
        F();
        b(false);
        c(false);
        O();
        P();
        this.aN.a(z2 ? 1.0f : this.aP);
        j jVar = this.aO;
        if (!z2) {
            f = this.aP;
        }
        jVar.a(f);
        this.aN.setDuration(j);
        this.aO.setDuration(j);
        this.z.startAnimation(this.aN);
        this.A.startAnimation(this.aO);
    }

    public void v() {
        if (SpaceScanManager.a().e()) {
            this.aH = true;
            br.b(0, SpaceCleanActivity.class);
            z();
            return;
        }
        SpaceScanManager.a().a(this.bq);
        SpaceScanManager.a().c();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (4 != i) {
            return super.onKeyDown(i, keyEvent);
        }
        finish();
        return true;
    }

    public STPageInfo q() {
        this.o.f3358a = STConst.ST_PAGE_ASSISTANT;
        return this.o;
    }

    /* access modifiers changed from: private */
    public STInfoV2 a(String str, String str2, int i) {
        STInfoV2 sTInfoV2 = new STInfoV2(this.o.f3358a, str, this.o.c, this.o.d, i);
        sTInfoV2.status = str2;
        sTInfoV2.slotId = str;
        sTInfoV2.actionId = i;
        return sTInfoV2;
    }

    /* access modifiers changed from: private */
    public void b(String str, String str2) {
        STInfoV2 a2 = a(str, STConst.ST_STATUS_DEFAULT, 100);
        a2.extraData = str2;
        k.a(a2);
    }

    /* access modifiers changed from: private */
    public void Q() {
        if (!this.bd && this.B.getTop() < t.c) {
            this.bd = true;
            b("08_003", (String) null);
            b("08_001", (String) null);
            b("08_002", (String) null);
        }
    }

    public List<AppUpdateInfo> a(List<AppUpdateInfo> list) {
        boolean z2;
        boolean z3;
        boolean z4;
        boolean z5;
        boolean z6;
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList();
        ArrayList arrayList4 = new ArrayList();
        ArrayList arrayList5 = new ArrayList();
        ArrayList arrayList6 = new ArrayList();
        SimpleAppModel simpleAppModel = new SimpleAppModel();
        for (AppUpdateInfo next : list) {
            if (d(next.f2006a) == 1) {
                arrayList2.add(next);
            } else {
                arrayList3.add(next);
            }
        }
        Iterator it = arrayList2.iterator();
        while (it.hasNext()) {
            AppUpdateInfo appUpdateInfo = (AppUpdateInfo) it.next();
            u.a(appUpdateInfo, simpleAppModel);
            AppConst.AppState d = u.d(simpleAppModel);
            if (AppConst.AppState.DOWNLOADED == d) {
                z4 = true;
            } else {
                z4 = false;
            }
            if (AppConst.AppState.INSTALLING == d) {
                z5 = true;
            } else {
                z5 = false;
            }
            if (AppConst.AppState.INSTALLED == d) {
                z6 = true;
            } else {
                z6 = false;
            }
            if (z4 || z5 || z6) {
                arrayList4.add(appUpdateInfo);
            } else {
                arrayList5.add(appUpdateInfo);
            }
        }
        arrayList.addAll(arrayList4);
        arrayList.addAll(arrayList5);
        if (arrayList.size() >= this.aa) {
            return arrayList;
        }
        Iterator it2 = arrayList3.iterator();
        while (it2.hasNext()) {
            AppUpdateInfo appUpdateInfo2 = (AppUpdateInfo) it2.next();
            u.a(appUpdateInfo2, simpleAppModel);
            AppConst.AppState d2 = u.d(simpleAppModel);
            boolean z7 = AppConst.AppState.DOWNLOADED == d2;
            if (AppConst.AppState.INSTALLING == d2) {
                z2 = true;
            } else {
                z2 = false;
            }
            if (AppConst.AppState.INSTALLED == d2) {
                z3 = true;
            } else {
                z3 = false;
            }
            if (z7 || z2 || z3) {
                arrayList.add(appUpdateInfo2);
            } else {
                arrayList6.add(appUpdateInfo2);
            }
            if (arrayList.size() >= this.aa) {
                return arrayList;
            }
        }
        arrayList.addAll(arrayList6);
        return arrayList;
    }

    private int d(String str) {
        int i = 0;
        ArrayList<Integer> c = com.tencent.assistant.module.update.k.b().c(str);
        if (c != null && c.size() > 0) {
            i = c.get(0).intValue();
        }
        return e(i);
    }

    private int e(int i) {
        switch (i) {
            case 1:
            case 2:
                return 1;
            default:
                return 2;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    private void R() {
        if (!m.a().M()) {
            Bitmap bitmap = ((BitmapDrawable) getResources().getDrawable(R.drawable.icon_shortcut_setting)).getBitmap();
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("tmast://mobilemanage"));
            intent.setFlags(67108864);
            intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, S());
            intent.putExtra(com.tencent.assistant.b.a.G, true);
            com.tencent.assistant.utils.e.a(this, getString(R.string.mobile_manage_short_cut), intent);
            com.tencent.assistant.utils.e.a(this, bitmap, getString(R.string.mobile_manage_short_cut), intent);
            TemporaryThreadManager.get().start(new aq(this));
            k.a(new STInfoV2(204005, "03_001", 2000, STConst.ST_DEFAULT_SLOT, 100));
        }
    }

    private int S() {
        return STConst.ST_PAGE_SPACECLEAN_DESK_PAGEID;
    }
}
