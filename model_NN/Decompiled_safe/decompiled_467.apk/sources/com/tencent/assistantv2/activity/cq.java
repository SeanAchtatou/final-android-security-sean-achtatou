package com.tencent.assistantv2.activity;

import android.os.Bundle;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.login.d;

/* compiled from: ProGuard */
class cq implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ cp f2813a;

    cq(cp cpVar) {
        this.f2813a = cpVar;
    }

    public void run() {
        if (this.f2813a.f2812a.getBooleanExtra("action_key_from_guide", false)) {
            Bundle bundle = new Bundle();
            bundle.putInt(AppConst.KEY_LOGIN_TYPE, 5);
            bundle.putInt(AppConst.KEY_FROM_TYPE, 18);
            d.a().a(AppConst.IdentityType.MOBILEQ, bundle);
            return;
        }
        this.f2813a.b.b(this.f2813a.f2812a);
    }
}
