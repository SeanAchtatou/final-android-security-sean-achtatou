package com.tencent.assistantv2.activity;

import com.tencent.assistant.AppConst;
import com.tencent.assistant.component.CommentDetailView;

/* compiled from: ProGuard */
class c implements CommentDetailView.CommentSucceedListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppDetailActivityV5 f2797a;

    c(AppDetailActivityV5 appDetailActivityV5) {
        this.f2797a = appDetailActivityV5;
    }

    public void onCommentSucceed(boolean z, long j) {
        if (!z) {
            this.f2797a.X.setCommentButtonClickable(true);
        }
        this.f2797a.X.setHasCommented(z, AppConst.AppState.INSTALLED, j, this.f2797a.al, this.f2797a.am, this.f2797a.an, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.activity.AppDetailActivityV5.c(com.tencent.assistantv2.activity.AppDetailActivityV5, boolean):boolean
     arg types: [com.tencent.assistantv2.activity.AppDetailActivityV5, int]
     candidates:
      com.tencent.assistantv2.activity.AppDetailActivityV5.c(com.tencent.assistantv2.activity.AppDetailActivityV5, int):void
      com.tencent.assistantv2.activity.AppDetailActivityV5.c(com.tencent.assistantv2.activity.AppDetailActivityV5, boolean):boolean */
    public void onGetOldComment(long j, String str, int i, int i2) {
        this.f2797a.X.setCommentButtonClickable(true);
        if (j == -1) {
            this.f2797a.X.setHasCommented(false, AppConst.AppState.INSTALLED, j, str, i, i2, false);
            if (this.f2797a.af != null && !this.f2797a.isFinishing() && this.f2797a.N) {
                this.f2797a.af.setHasComment(false);
                return;
            }
            return;
        }
        this.f2797a.X.setHasCommented(true, AppConst.AppState.INSTALLED, j, str, i, i2, false);
        if (this.f2797a.af != null && !this.f2797a.isFinishing() && this.f2797a.N) {
            try {
                this.f2797a.af.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
            this.f2797a.af.setHasComment(true);
            boolean unused = this.f2797a.N = false;
        }
    }
}
