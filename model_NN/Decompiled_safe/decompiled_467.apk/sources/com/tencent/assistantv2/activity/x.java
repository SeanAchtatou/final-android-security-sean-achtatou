package com.tencent.assistantv2.activity;

import android.view.View;
import android.widget.AbsListView;
import com.tencent.assistant.component.invalidater.TXRefreshGetMoreListViewScrollListener;
import com.tencent.assistantv2.adapter.smartlist.SmartListAdapter;

/* compiled from: ProGuard */
class x extends TXRefreshGetMoreListViewScrollListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ v f2864a;

    private x(v vVar) {
        this.f2864a = vVar;
    }

    /* synthetic */ x(v vVar, w wVar) {
        this(vVar);
    }

    public void onScroll(View view, int i, int i2, int i3) {
        super.onScroll(view, i, i2, i3);
        if (this.f2864a.Z != null) {
            SmartListAdapter.BannerType unused = this.f2864a.ad = this.f2864a.Z.p();
            if (this.f2864a.ad != SmartListAdapter.BannerType.None && this.f2864a.ad != SmartListAdapter.BannerType.HomePage && this.f2864a.ae != null) {
                this.f2864a.ae.a((AbsListView) view, i, i2, i3);
            }
        }
    }

    public void onScrollStateChanged(AbsListView absListView, int i) {
        super.onScrollStateChanged(absListView, i);
        if (this.f2864a.ae != null) {
            this.f2864a.ae.b(absListView, i);
        }
    }
}
