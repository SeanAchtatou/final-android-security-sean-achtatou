package com.tencent.assistantv2.activity;

import android.view.View;
import com.tencent.assistant.protocol.jce.TagGroup;

/* compiled from: ProGuard */
class ba implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CategoryDetailActivity f2774a;

    ba(CategoryDetailActivity categoryDetailActivity) {
        this.f2774a = categoryDetailActivity;
    }

    public void onClick(View view) {
        TagGroup tagGroup = (TagGroup) view.getTag();
        if (tagGroup != null && this.f2774a.B != null) {
            this.f2774a.B.k();
            this.f2774a.B.d(tagGroup.b());
        }
    }
}
