package com.tencent.assistantv2.component.search;

import android.view.View;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class h extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SearchHotwordsView f3238a;

    h(SearchHotwordsView searchHotwordsView) {
        this.f3238a = searchHotwordsView;
    }

    public void onTMAClick(View view) {
        this.f3238a.b();
    }

    public STInfoV2 getStInfo() {
        if (!(this.f3238a.getContext() instanceof BaseActivity)) {
            return null;
        }
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f3238a.getContext(), 200);
        buildSTInfo.slotId = a.a("04", "102");
        return buildSTInfo;
    }
}
