package com.tencent.assistantv2.component.appdetail;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistantv2.activity.AppDetailActivityV5;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
public class DetailGameNewsView extends RelativeLayout {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Context f3050a;
    private TextView[] b;
    private TextView[] c;
    private TextView d;
    private TXImageView[] e;
    /* access modifiers changed from: private */
    public ImageView[] f;
    /* access modifiers changed from: private */
    public View g;
    private View h;
    private View i;
    /* access modifiers changed from: private */
    public View[] j;
    private View k;

    public DetailGameNewsView(Context context) {
        super(context);
        a(context);
    }

    public DetailGameNewsView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    private void a(Context context) {
        this.f3050a = context;
        LayoutInflater.from(this.f3050a).inflate((int) R.layout.detail_game_news_view, this);
        this.b = new TextView[3];
        this.c = new TextView[3];
        this.d = (TextView) findViewById(R.id.title);
        this.b[0] = (TextView) findViewById(R.id.news_title_1);
        this.c[0] = (TextView) findViewById(R.id.news_content_1);
        this.b[1] = (TextView) findViewById(R.id.news_title_2);
        this.c[1] = (TextView) findViewById(R.id.news_content_2);
        this.b[2] = (TextView) findViewById(R.id.news_title_3);
        this.c[2] = (TextView) findViewById(R.id.news_content_3);
        this.e = new TXImageView[3];
        this.e[0] = (TXImageView) findViewById(R.id.news_icon_1);
        this.e[1] = (TXImageView) findViewById(R.id.news_icon_2);
        this.e[2] = (TXImageView) findViewById(R.id.news_icon_3);
        this.f = new ImageView[3];
        this.f[0] = (ImageView) findViewById(R.id.play_video_1);
        this.f[1] = (ImageView) findViewById(R.id.play_video_2);
        this.f[2] = (ImageView) findViewById(R.id.play_video_3);
        this.g = findViewById(R.id.layout_0);
        this.h = findViewById(R.id.divider_1);
        this.i = findViewById(R.id.divider_2);
        this.j = new View[3];
        this.j[0] = findViewById(R.id.layout_1);
        this.j[1] = findViewById(R.id.layout_2);
        this.j[2] = findViewById(R.id.layout_3);
        this.k = findViewById(R.id.tips_btn);
    }

    public void a(r rVar) {
        if (rVar == null || rVar.c == null || rVar.c.length <= 0) {
            setVisibility(8);
        } else {
            p pVar = new p(this);
            if (!TextUtils.isEmpty(rVar.f3082a)) {
                this.d.setText(rVar.f3082a);
            }
            if (rVar.d) {
                this.k.setVisibility(0);
            } else {
                this.k.setVisibility(8);
            }
            s[] sVarArr = rVar.c;
            int length = sVarArr.length;
            int i2 = 0;
            int i3 = 0;
            while (i3 < length) {
                s sVar = sVarArr[i3];
                if (i2 >= 2 || sVar == null) {
                    break;
                }
                this.b[i2].setText(sVar.b);
                this.c[i2].setText(sVar.c);
                this.e[i2].updateImageView(sVar.f3083a, R.drawable.app_treasure_box_above_icon, TXImageView.TXImageViewType.NETWORK_IMAGE_MIDDLE);
                if (sVar.d) {
                    this.f[i2].setVisibility(0);
                    this.f[i2].setOnClickListener(pVar);
                } else {
                    this.f[i2].setVisibility(8);
                }
                i3++;
                i2++;
            }
            if (i2 == 1) {
                this.h.setVisibility(8);
                this.i.setVisibility(8);
            } else if (i2 == 2) {
                this.i.setVisibility(8);
            }
            for (int i4 = 1; i4 <= i2; i4++) {
                if (this.f3050a instanceof AppDetailActivityV5) {
                    STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f3050a, 100);
                    buildSTInfo.slotId = a.a("09", i4);
                    k.a(buildSTInfo);
                }
            }
            if (i2 < 3) {
                while (i2 < 3) {
                    this.j[i2].setVisibility(8);
                    i2++;
                }
            }
        }
        q qVar = new q(this, rVar);
        this.g.setOnClickListener(qVar);
        this.f[0].setOnClickListener(qVar);
        this.f[1].setOnClickListener(qVar);
        this.f[2].setOnClickListener(qVar);
        this.j[0].setOnClickListener(qVar);
        this.j[1].setOnClickListener(qVar);
        this.j[2].setOnClickListener(qVar);
    }
}
