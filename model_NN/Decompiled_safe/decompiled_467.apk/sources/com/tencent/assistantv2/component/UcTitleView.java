package com.tencent.assistantv2.component;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.usercenter.UserCenterActivityV2;
import com.tencent.assistant.utils.df;
import com.tencent.assistant.utils.v;
import com.tencent.connect.common.Constants;
import java.lang.ref.WeakReference;

/* compiled from: ProGuard */
public class UcTitleView extends LinearLayout implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private Context f3023a = null;
    private LayoutInflater b = null;
    private LinearLayout c = null;
    private TextView d = null;
    private ImageView e = null;
    private RelativeLayout f = null;
    private PopupWindow g = null;
    /* access modifiers changed from: private */
    public WeakReference<Activity> h;

    public UcTitleView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f3023a = context;
        this.b = LayoutInflater.from(this.f3023a);
        a();
    }

    public UcTitleView(Context context) {
        super(context);
        this.f3023a = context;
        this.b = LayoutInflater.from(this.f3023a);
        a();
    }

    private void a() {
        View inflate = this.b.inflate((int) R.layout.uc_titleview_layout, this);
        this.c = (LinearLayout) inflate.findViewById(R.id.back_layout);
        this.d = (TextView) inflate.findViewById(R.id.tv_uc_title_txt);
        this.e = (ImageView) inflate.findViewById(R.id.iv_uc_title_menu);
        this.f = (RelativeLayout) inflate.findViewById(R.id.uc_title_menu_layout);
        this.c.setOnClickListener(this);
        this.c.setTag(R.id.tma_st_slot_tag, "00_001");
        this.f.setOnClickListener(this);
        this.f.setTag(R.id.tma_st_slot_tag, "00_002");
        a(getResources().getString(R.string.user_center));
    }

    private void a(String str) {
        if (this.d != null) {
            this.d.setText(str);
        }
    }

    public void a(Activity activity) {
        this.h = new WeakReference<>(activity);
    }

    private void b() {
        View inflate = ((LayoutInflater) this.f3023a.getSystemService("layout_inflater")).inflate((int) R.layout.uc_pop_menu_layout, (ViewGroup) null);
        LinearLayout linearLayout = (LinearLayout) inflate.findViewById(R.id.uc_menu_exit_layout);
        linearLayout.setOnClickListener(this);
        linearLayout.setTag(R.id.tma_st_slot_tag, "00_003");
        this.g = new PopupWindow(inflate, this.f3023a.getResources().getDimensionPixelSize(R.dimen.uc_pop_menu_width), this.f3023a.getResources().getDimensionPixelSize(R.dimen.uc_pop_menu_height));
        this.g.setBackgroundDrawable(getResources().getDrawable(R.drawable.common_bg_popup));
        this.g.setFocusable(true);
        this.g.setTouchable(true);
        int[] iArr = new int[2];
        this.e.getLocationOnScreen(iArr);
        this.g.showAtLocation(this.e, 0, (df.b() - this.g.getWidth()) - df.a(this.f3023a, 4.0f), iArr[1] + this.e.getHeight() + df.a(this.f3023a, 4.0f));
        this.g.setOutsideTouchable(true);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back_layout /*2131166315*/:
                c();
                break;
            case R.id.uc_menu_exit_layout /*2131166497*/:
                this.g.dismiss();
                d();
                break;
            case R.id.uc_title_menu_layout /*2131166503*/:
                b();
                break;
        }
        String str = STConst.ST_DEFAULT_SLOT;
        if (view.getTag(R.id.tma_st_slot_tag) instanceof String) {
            str = (String) view.getTag(R.id.tma_st_slot_tag);
        }
        if (e() != null) {
            e().a(str, Constants.STR_EMPTY, 200);
        }
    }

    private void c() {
        ((Activity) this.f3023a).finish();
    }

    private void d() {
        el elVar = new el(this);
        Resources resources = getContext().getResources();
        elVar.titleRes = resources.getString(R.string.login_exit);
        elVar.contentRes = resources.getString(R.string.exit_tip);
        elVar.lBtnTxtRes = resources.getString(R.string.cancel);
        elVar.rBtnTxtRes = resources.getString(R.string.menu_exit);
        v.a(elVar);
    }

    private UserCenterActivityV2 e() {
        if (this.h == null || this.h.get() == null) {
            return null;
        }
        return (UserCenterActivityV2) this.h.get();
    }
}
