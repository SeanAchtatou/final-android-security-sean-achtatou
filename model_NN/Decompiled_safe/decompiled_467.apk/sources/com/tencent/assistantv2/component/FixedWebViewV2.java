package com.tencent.assistantv2.component;

import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.webkit.WebView;
import com.tencent.assistant.utils.XLog;
import com.tencent.connect.common.Constants;
import java.lang.reflect.Method;

/* compiled from: ProGuard */
public class FixedWebViewV2 extends WebView {
    private static String b = Constants.STR_EMPTY;

    /* renamed from: a  reason: collision with root package name */
    private String f2991a;

    static {
        b += ".*HUAWEI_HUAWEI U9508.*";
        b += "|.*Xiaomi_MI.*";
    }

    public FixedWebViewV2(Context context, boolean z) {
        super(context);
        this.f2991a = "FixedWebView";
        if ((Build.MANUFACTURER + "_" + Build.MODEL).matches(b) && !z) {
            a();
        }
        setBackgroundColor(0);
    }

    public FixedWebViewV2(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f2991a = "FixedWebView";
        String str = Build.MANUFACTURER + "_" + Build.MODEL;
        XLog.i(this.f2991a, "mask=" + str + " version=" + Build.VERSION.SDK + "," + Build.VERSION.RELEASE);
        XLog.i(this.f2991a, "match=" + str.matches(b));
        if (str.matches(b)) {
            a();
        }
        setBackgroundColor(0);
        try {
            Method method = getClass().getMethod("removeJavascriptInterface", String.class);
            if (method != null) {
                method.invoke(this, "searchBoxJavaBridge_");
            }
        } catch (Exception e) {
        }
    }

    public void a() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.component.FixedWebViewV2.<init>(android.content.Context, boolean):void
     arg types: [android.content.Context, int]
     candidates:
      com.tencent.assistantv2.component.FixedWebViewV2.<init>(android.content.Context, android.util.AttributeSet):void
      com.tencent.assistantv2.component.FixedWebViewV2.<init>(android.content.Context, boolean):void */
    public FixedWebViewV2(Context context) {
        this(context, true);
    }
}
