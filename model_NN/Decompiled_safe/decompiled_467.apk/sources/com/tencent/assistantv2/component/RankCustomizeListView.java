package com.tencent.assistantv2.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.HeaderViewListAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.cy;
import com.tencent.assistant.component.SwitchButton;
import com.tencent.assistant.component.appdetail.CloseViewRunnable;
import com.tencent.assistant.component.invalidater.ListViewScrollListener;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessage;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessageHandler;
import com.tencent.assistant.component.txscrollview.ITXRefreshListViewListener;
import com.tencent.assistant.component.txscrollview.TXExpandableListView;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.m;
import com.tencent.assistant.module.callback.q;
import com.tencent.assistant.module.de;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistantv2.activity.ay;
import com.tencent.assistantv2.adapter.u;
import com.tencent.assistantv2.manager.i;
import com.tencent.assistantv2.st.business.CostTimeSTManager;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
public class RankCustomizeListView extends TXExpandableListView implements ITXRefreshListViewListener {
    /* access modifiers changed from: private */
    public static String[] t;
    /* access modifiers changed from: private */
    public ViewInvalidateMessageHandler A = new by(this);

    /* renamed from: a  reason: collision with root package name */
    LinearLayout f3006a;
    View b;
    SwitchButton c;
    CloseViewRunnable d;
    public boolean e = false;
    TextView f;
    boolean[] g = new boolean[10];
    int h = 0;
    public boolean i = false;
    protected ay j;
    q k = new bv(this);
    /* access modifiers changed from: private */
    public de l = null;
    /* access modifiers changed from: private */
    public u m = null;
    /* access modifiers changed from: private */
    public cd n;
    private int o = 1;
    /* access modifiers changed from: private */
    public ListViewScrollListener p;
    private int q;
    private int r;
    private long s = 0;
    private final int u = 1;
    private final int v = 2;
    private final String w = "isFirstPage";
    private final String x = "hasNext";
    private final String y = "key_Appid";
    private final String z = "key_data";

    public void a(ListViewScrollListener listViewScrollListener) {
        this.p = listViewScrollListener;
        setOnScrollListener(this.p);
    }

    public void a(cd cdVar) {
        this.n = cdVar;
    }

    public void a(de deVar, int i2, int i3) {
        this.l = deVar;
        this.l.register(this.k);
        this.q = i2;
        this.r = i3;
        setRefreshListViewListener(this);
    }

    public RankCustomizeListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setGroupIndicator(null);
        setDivider(null);
        setChildDivider(null);
        setSelector(R.drawable.transparent_selector);
        setRefreshListViewListener(this);
        setOnGroupClickListener(new bw(this));
        i();
    }

    public ExpandableListView a() {
        return (ExpandableListView) this.mScrollContentView;
    }

    private void i() {
        ExpandableListAdapter expandableListAdapter = ((ExpandableListView) this.mScrollContentView).getExpandableListAdapter();
        if (expandableListAdapter instanceof HeaderViewListAdapter) {
            this.m = (u) ((HeaderViewListAdapter) expandableListAdapter).getWrappedAdapter();
        } else {
            this.m = (u) ((ExpandableListView) this.mScrollContentView).getExpandableListAdapter();
        }
        if (this.m != null && t == null) {
            t = AstApp.i().getResources().getStringArray(R.array.logo_tips3);
        }
    }

    public void a(int i2, int i3) {
        if (this.m == null) {
            i();
        }
        this.q = i2;
        this.r = i3;
        if (this.m.getGroupCount() <= 0 || g()) {
            this.n.a();
            this.l.b();
            b(this.q, this.r);
            return;
        }
        this.p.sendMessage(new ViewInvalidateMessage(2, null, this.A));
        if (this.r > 0) {
            a().setSelection(0);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.component.RankCustomizeListView.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.tencent.assistantv2.component.RankCustomizeListView.a(int, int):void
      com.tencent.assistantv2.component.RankCustomizeListView.a(boolean, boolean):void */
    public void onTXRefreshListViewRefresh(TXScrollViewBase.ScrollState scrollState) {
        if (scrollState == TXScrollViewBase.ScrollState.ScrollState_FromEnd) {
            TemporaryThreadManager.get().start(new bx(this));
        } else if (scrollState == TXScrollViewBase.ScrollState.ScrollState_FromStart && this.b != null) {
            a(true, true);
        }
    }

    public void b() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.component.RankCustomizeListView.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.tencent.assistantv2.component.RankCustomizeListView.a(int, int):void
      com.tencent.assistantv2.component.RankCustomizeListView.a(boolean, boolean):void */
    public void c() {
        if (this.c != null) {
            this.c.setSwitchState(i.a().b().c());
        }
        if (this.m != null && this.m.getGroupCount() > 0) {
            this.m.a();
        }
        if (this.c != null && this.m != null && this.m.getGroupCount() > 0) {
            if (!this.i) {
                if (m.a().ak()) {
                    a(true, false);
                    m.a().y(false);
                }
            } else if (m.a().al()) {
                a(true, false);
                m.a().z(false);
            }
        }
    }

    public void recycleData() {
        super.recycleData();
        this.l.unregister(this.k);
    }

    public void d() {
        if (this.f3006a == null) {
            this.f3006a = (LinearLayout) LayoutInflater.from(getContext()).inflate((int) R.layout.v5_hide_installed_apps_area, (ViewGroup) null);
            ((ExpandableListView) this.mScrollContentView).addHeaderView(this.f3006a);
            this.b = this.f3006a.findViewById(R.id.header_container);
            this.c = (SwitchButton) this.f3006a.findViewById(R.id.hide_btn);
            this.c.setClickable(false);
            this.f = (TextView) this.f3006a.findViewById(R.id.tips);
            this.c.setSwitchState(i.a().b().c());
            this.b.setOnClickListener(new ca(this));
        }
    }

    public void a(ay ayVar) {
        this.j = ayVar;
    }

    /* access modifiers changed from: package-private */
    public void e() {
        STInfoV2 sTInfoV2 = new STInfoV2(this.j.J(), RankNormalListView.ST_HIDE_INSTALLED_APPS, this.j.J(), RankNormalListView.ST_HIDE_INSTALLED_APPS, 100);
        if (!this.i || !(this.j instanceof cy)) {
            sTInfoV2.slotId = a.a(RankNormalListView.ST_HIDE_INSTALLED_APPS, 0);
        } else {
            sTInfoV2.slotId = a.a(((cy) this.j).K(), 0);
        }
        if (!i.a().b().c()) {
            sTInfoV2.status = "01";
        } else {
            sTInfoV2.status = "02";
        }
        k.a(sTInfoV2);
    }

    /* access modifiers changed from: private */
    public void a(int i2, int i3, boolean z2, boolean z3) {
        if (i3 == 0) {
            this.n.b();
            if (this.m == null) {
                i();
            }
            if (this.m.getGroupCount() == 0) {
                this.n.a(10);
                return;
            }
            this.m.notifyDataSetChanged();
            this.s = System.currentTimeMillis();
            if (z2) {
                a().setSelection(0);
            }
            onRefreshComplete(z3, true);
        } else if (z2) {
            k.a((int) STConst.ST_PAGE_RANK_RECOMMEND, CostTimeSTManager.TIMETYPE.CANCEL, System.currentTimeMillis());
            if (-800 == i3) {
                this.n.a(30);
            } else if (this.o <= 0) {
                this.n.a(30);
            } else {
                this.o--;
                b(this.q, this.r);
            }
        } else {
            onRefreshComplete(z3, false);
            this.n.c();
        }
    }

    public void b(int i2, int i3) {
        if (this.l != null) {
            TemporaryThreadManager.get().start(new cc(this, i2, i3));
        }
    }

    public u f() {
        return this.m;
    }

    public boolean g() {
        return this.s == 0 || System.currentTimeMillis() - this.s > m.a().ah();
    }

    public void a(boolean z2, boolean z3) {
        if (this.b == null) {
            return;
        }
        if (!z2) {
            if (z3) {
                this.b.setClickable(false);
                this.e = false;
                if (this.d == null) {
                    this.d = new CloseViewRunnable(this.b);
                    this.d.mListView = a();
                }
                this.d.isRunning = true;
                this.b.postDelayed(this.d, 5);
                return;
            }
            this.b.setVisibility(8);
            this.e = false;
            this.f.setVisibility(8);
            if (this.d != null) {
                this.d.isRunning = false;
            }
        } else if (!this.e) {
            if (this.d != null) {
                this.d.isRunning = false;
            }
            this.b.setVisibility(0);
            this.b.setBackgroundResource(R.drawable.v2_button_background_selector);
            this.b.setPressed(false);
            a().setSelection(1);
            this.b.setClickable(true);
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) this.b.getLayoutParams();
            if (layoutParams.height != getResources().getDimensionPixelSize(R.dimen.hide_installed_apps_area_height)) {
                layoutParams.height = getResources().getDimensionPixelSize(R.dimen.hide_installed_apps_area_height);
                this.b.setLayoutParams(layoutParams);
            }
            this.e = true;
        }
    }
}
