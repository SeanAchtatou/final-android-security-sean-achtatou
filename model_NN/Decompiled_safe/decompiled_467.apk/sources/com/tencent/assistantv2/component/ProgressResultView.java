package com.tencent.assistantv2.component;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import com.tencent.assistant.utils.df;

/* compiled from: ProGuard */
public class ProgressResultView extends View {

    /* renamed from: a  reason: collision with root package name */
    private Paint f3003a;
    private boolean b = false;
    private final int c = 1;
    /* access modifiers changed from: private */
    public float d;
    /* access modifiers changed from: private */
    public float e;
    private final float f = 10.0f;
    private final long g = 10;
    private br h;
    private volatile float i = 4.0f;
    private Handler j = new bq(this);

    static /* synthetic */ float a(ProgressResultView progressResultView, float f2) {
        float f3 = progressResultView.d + f2;
        progressResultView.d = f3;
        return f3;
    }

    public ProgressResultView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        float a2 = (float) (df.a(getContext(), this.i) / 2);
        RectF rectF = new RectF(a2, a2, ((float) getMeasuredWidth()) - a2, ((float) getMeasuredHeight()) - a2);
        if (this.b && this.d <= this.e) {
            canvas.save();
            canvas.drawArc(rectF, -90.0f, this.d, false, this.f3003a);
            canvas.restore();
            this.j.sendEmptyMessageDelayed(1, 10);
        }
        if (!this.b) {
            canvas.drawArc(rectF, -90.0f, this.e, false, this.f3003a);
        }
        if (this.d >= this.e && this.b) {
            this.h.a();
            this.b = false;
        }
    }

    public void a(float f2, br brVar) {
        this.b = true;
        this.d = 0.0f;
        this.e = f2;
        this.h = brVar;
        this.b = true;
        this.j.sendEmptyMessage(1);
    }

    private void a() {
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth((float) df.a(getContext(), this.i));
        paint.setColor(Color.argb(255, 0, 173, 255));
        paint.setStrokeCap(Paint.Cap.ROUND);
        this.f3003a = paint;
    }
}
