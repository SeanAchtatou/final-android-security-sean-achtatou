package com.tencent.assistantv2.component.banner.floatheader;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.manager.as;
import com.tencent.assistant.protocol.jce.GftGetGameGiftFlagResponse;
import com.tencent.assistant.utils.df;
import com.tencent.assistantv2.adapter.smartlist.SmartListAdapter;
import com.tencent.assistantv2.component.SoftwareBannerView;
import com.tencent.assistantv2.component.banner.e;
import com.tencent.assistantv2.component.banner.f;

/* compiled from: ProGuard */
public class FloatOriginalBannerView extends SoftwareBannerView {
    private int f;
    private LinearLayout g;
    private LinearLayout.LayoutParams h;
    private View i;
    private ImageView j;
    private ImageView k;
    private TextView l;
    private ViewGroup.LayoutParams m;
    private ViewGroup.LayoutParams n;
    private float o;
    private float p;
    private boolean q;
    private boolean r;
    private boolean s;
    private int t;
    private int u;
    private int v;
    private int w;

    public FloatOriginalBannerView(Context context) {
        this(context, null);
    }

    public FloatOriginalBannerView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, SmartListAdapter.SmartListType.AppPage.ordinal());
    }

    public FloatOriginalBannerView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.f = 0;
        this.q = false;
        this.r = false;
        this.s = false;
        this.t = -1;
        this.u = -1;
        this.v = -1;
        this.w = 0;
        this.f = j() + o();
    }

    /* access modifiers changed from: protected */
    public void b() {
        int i2;
        if (this.b != null && this.b.size() != 0) {
            removeAllViews();
            setOrientation(1);
            this.g = g();
            this.h = new LinearLayout.LayoutParams(-1, -2);
            this.h.setMargins(df.a(getContext(), 2.0f), df.a(getContext(), 2.5f), df.a(getContext(), 2.0f), df.a(getContext(), 5.0f));
            this.g.setLayoutParams(this.h);
            addView(this.g);
            this.f3004a = e.a(this.b, this.d == SmartListAdapter.SmartListType.GamePage.ordinal() ? 10 : 3, 5);
            this.g.setBackgroundResource(R.drawable.common_cardbg_normal_full);
            int i3 = 0;
            int i4 = 0;
            for (f fVar : this.f3004a) {
                if (i4 >= this.e) {
                    this.g = c();
                    addView(this.g);
                    i2 = 0;
                } else {
                    i2 = i4;
                }
                fVar.a(a());
                View b = fVar.b(getContext(), this, this.d, this.c, i3);
                b.setBackgroundResource(R.drawable.v2_button_background_light_selector);
                if (b != null) {
                    ViewGroup.LayoutParams layoutParams = b.getLayoutParams();
                    if (layoutParams == null) {
                        layoutParams = new LinearLayout.LayoutParams(-1, -1);
                    }
                    layoutParams.height = j();
                    if (layoutParams instanceof LinearLayout.LayoutParams) {
                        ((LinearLayout.LayoutParams) layoutParams).weight = (float) fVar.a();
                    }
                    this.g.addView(b, layoutParams);
                }
                i3++;
                i4 = fVar.a() + i2;
            }
        }
    }

    public void a(int i2, int i3) {
        if (this.g != null && i2 != this.w) {
            this.h = (LinearLayout.LayoutParams) this.g.getLayoutParams();
            if (this.h == null) {
                this.h = new LinearLayout.LayoutParams(-1, -2);
            }
            this.h.height = this.f - i2;
            this.o = (((float) this.h.height) * 1.0f) / ((float) this.f);
            this.p = 1.0f - ((((float) i2) * 1.0f) / ((float) i3));
            if (this.g.getBackground() != null) {
                this.g.getBackground().setAlpha((int) (this.p * 255.0f));
            }
            this.q = false;
            if (this.t != ((int) (((float) q()) * this.o))) {
                this.t = (int) (((float) q()) * this.o);
                this.q = true;
            }
            this.s = false;
            if (this.u != ((int) (((float) r()) * this.o))) {
                this.u = (int) (((float) r()) * this.o);
                this.s = true;
            }
            this.r = false;
            if (this.v != ((int) (((float) s()) * this.o))) {
                this.v = (int) (((float) s()) * this.o);
                this.r = true;
            }
            if (this.q || this.r) {
                for (int i4 = 0; i4 < this.g.getChildCount(); i4++) {
                    this.i = this.g.getChildAt(i4);
                    if (this.i != null) {
                        this.j = (ImageView) this.i.findViewById(R.id.title_img);
                        this.l = (TextView) this.i.findViewById(R.id.title);
                        this.k = (ImageView) this.i.findViewById(R.id.new_gift);
                        if (this.q && this.j != null) {
                            this.m = this.j.getLayoutParams();
                            if (this.m == null) {
                                this.m = new ViewGroup.LayoutParams(q(), q());
                            }
                            this.m.width = this.t;
                            this.m.height = this.t;
                            this.j.setLayoutParams(this.m);
                            this.j.setAlpha((int) (this.p * 255.0f));
                        }
                        if (this.r) {
                            this.v = (int) (((float) s()) * this.o);
                            if (this.l != null) {
                                this.l.setTextSize((float) this.v);
                                this.l.setTextColor(Color.argb((int) (this.p * 255.0f), 144, 144, 144));
                            }
                        }
                        if (this.s && this.k != null && this.k.getVisibility() == 0) {
                            GftGetGameGiftFlagResponse r2 = as.w().r();
                            if (r2 == null || r2.b == 0) {
                                this.k.setVisibility(8);
                            } else {
                                this.n = this.k.getLayoutParams();
                                if (this.n == null) {
                                    this.n = new ViewGroup.LayoutParams(r(), r());
                                }
                                this.n.width = this.u;
                                this.n.height = this.u;
                                this.k.setLayoutParams(this.n);
                                this.k.setAlpha((int) (this.p * 255.0f));
                            }
                        }
                    }
                }
                this.g.setLayoutParams(this.h);
            }
        }
    }

    public int p() {
        return df.a(getContext(), 8.0f) + j() + df.a(getContext(), 4.0f);
    }

    public int q() {
        return getContext().getResources().getDimensionPixelSize(R.dimen.banner_apptab_img_size);
    }

    public int r() {
        return getContext().getResources().getDimensionPixelSize(R.dimen.banner_gift_img_size);
    }

    public int s() {
        return 13;
    }
}
