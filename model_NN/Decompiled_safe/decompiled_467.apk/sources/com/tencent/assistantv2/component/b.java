package com.tencent.assistantv2.component;

import android.widget.AbsListView;
import com.tencent.assistant.component.invalidater.ListViewScrollListener;

/* compiled from: ProGuard */
class b extends ListViewScrollListener {

    /* renamed from: a  reason: collision with root package name */
    boolean f3098a = false;
    final /* synthetic */ AppRankListPage b;

    b(AppRankListPage appRankListPage) {
        this.b = appRankListPage;
    }

    public void onScrollStateChanged(AbsListView absListView, int i) {
        super.onScrollStateChanged(absListView, i);
        if (i == 0 && this.f3098a) {
            this.b.e.a(false, false);
        }
    }

    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
        super.onScroll(absListView, i, i2, i3);
        if (i == 2) {
            this.f3098a = true;
        } else if (i == 1) {
            this.f3098a = false;
        }
    }
}
