package com.tencent.assistantv2.component.appdetail;

import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.manager.cq;
import com.tencent.assistantv2.activity.AppDetailActivityV5;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class e extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppDetailViewV5 f3069a;

    e(AppDetailViewV5 appDetailViewV5) {
        this.f3069a = appDetailViewV5;
    }

    public void onTMAClick(View view) {
        try {
            if (this.f3069a.d.a()) {
                this.f3069a.d.a(false);
                this.f3069a.e.setImageResource(R.drawable.icon_close);
                return;
            }
            this.f3069a.d.a(true);
            this.f3069a.e.setImageResource(R.drawable.icon_open);
        } catch (OutOfMemoryError e) {
            cq.a().b();
        } catch (Throwable th) {
        }
    }

    public STInfoV2 getStInfo() {
        if (!(this.f3069a.m instanceof AppDetailActivityV5)) {
            return null;
        }
        STInfoV2 i = ((AppDetailActivityV5) this.f3069a.m).i();
        i.slotId = a.a("07", "009");
        if (this.f3069a.d.a()) {
            i.status = "01";
        } else {
            i.status = "02";
        }
        i.actionId = 200;
        return i;
    }
}
