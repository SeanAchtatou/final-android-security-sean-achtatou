package com.tencent.assistantv2.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.LoadingView;
import com.tencent.assistant.component.NormalErrorRecommendPage;
import com.tencent.assistant.module.k;
import com.tencent.assistantv2.adapter.RankNormalListAdapter;

/* compiled from: ProGuard */
public class AppRankListPage extends RelativeLayout implements g {

    /* renamed from: a  reason: collision with root package name */
    protected Context f2977a = null;
    protected LayoutInflater b = null;
    protected LoadingView c;
    protected NormalErrorRecommendPage d;
    protected AppRankListView e;
    protected View.OnClickListener f = new a(this);

    public AppRankListPage(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a(context);
    }

    public AppRankListPage(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    public void a(k kVar) {
        this.e.a(kVar);
    }

    /* access modifiers changed from: protected */
    public void a(Context context) {
        this.f2977a = context;
        this.b = LayoutInflater.from(context);
        View inflate = this.b.inflate((int) R.layout.apprank_component_view, this);
        this.e = (AppRankListView) inflate.findViewById(R.id.applist);
        this.e.setVisibility(8);
        this.e.a(this);
        this.e.setDivider(null);
        this.e.setTopPaddingSize(8);
        this.e.setShowHeaderFilterInfo(false);
        this.c = (LoadingView) inflate.findViewById(R.id.loading_view);
        this.c.setVisibility(0);
        this.d = (NormalErrorRecommendPage) inflate.findViewById(R.id.error_page);
        this.d.setButtonClickListener(this.f);
    }

    public void a(int i) {
        this.c.setVisibility(8);
        this.e.setVisibility(8);
        this.d.setVisibility(0);
        this.d.setErrorType(i);
    }

    public void a() {
        this.c.setVisibility(0);
        this.e.setVisibility(8);
        this.d.setVisibility(8);
    }

    public void a(RankNormalListAdapter rankNormalListAdapter) {
        this.e.c();
        this.e.setAdapter(rankNormalListAdapter);
        b bVar = new b(this);
        this.e.a(bVar);
        rankNormalListAdapter.a(bVar);
    }

    public void b() {
        this.e.b();
    }

    public void c() {
        this.e.setVisibility(0);
        this.d.setVisibility(8);
        this.c.setVisibility(8);
    }

    public void d() {
    }
}
