package com.tencent.assistantv2.component.search;

import android.content.Intent;
import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.appdetail.CommentDetailTabView;
import com.tencent.assistant.module.u;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.activity.AppDetailActivityV5;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class d implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SearchExplicitHotWordView f3235a;

    d(SearchExplicitHotWordView searchExplicitHotWordView) {
        this.f3235a = searchExplicitHotWordView;
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.container /*2131165298*/:
                if (this.f3235a.k != null) {
                    this.f3235a.k.onClick(view);
                }
                this.f3235a.a(this.f3235a.o, "02", 200);
                return;
            case R.id.state_app_btn /*2131165510*/:
                this.f3235a.a(this.f3235a.o, view, "001");
                AppConst.AppState d = u.d(this.f3235a.o);
                int a2 = a.a(d);
                this.f3235a.a(this.f3235a.o, a.a(d, this.f3235a.o), a2);
                return;
            case R.id.app_view /*2131166305*/:
                Intent intent = new Intent(this.f3235a.p, AppDetailActivityV5.class);
                intent.putExtra(CommentDetailTabView.PARAMS_SIMPLE_MODEL_INFO, this.f3235a.o);
                StatInfo statInfo = new StatInfo();
                statInfo.sourceScene = STConst.ST_PAGE_SEARCH;
                statInfo.extraData = this.f3235a.n;
                intent.putExtra("statInfo", statInfo);
                if (this.f3235a.p instanceof BaseActivity) {
                    intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, ((BaseActivity) this.f3235a.p).f());
                }
                this.f3235a.p.startActivity(intent);
                this.f3235a.a(this.f3235a.o, "01", 200);
                return;
            default:
                return;
        }
    }
}
