package com.tencent.assistantv2.component;

import com.tencent.assistant.component.invalidater.ViewInvalidateMessage;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.callback.b;
import com.tencent.assistant.protocol.jce.TagGroup;
import java.util.HashMap;
import java.util.List;

/* compiled from: ProGuard */
class c implements b {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppRankListView f3134a;

    c(AppRankListView appRankListView) {
        this.f3134a = appRankListView;
    }

    public void a(int i, int i2, boolean z, List<SimpleAppModel> list, List<TagGroup> list2) {
        if (this.f3134a.e != null) {
            ViewInvalidateMessage viewInvalidateMessage = new ViewInvalidateMessage(1, null, this.f3134a.r);
            viewInvalidateMessage.arg1 = i2;
            viewInvalidateMessage.arg2 = i;
            HashMap hashMap = new HashMap();
            hashMap.put("isFirstPage", Boolean.valueOf(z));
            viewInvalidateMessage.params = hashMap;
            hashMap.put("key_data", list);
            this.f3134a.e.sendMessage(viewInvalidateMessage);
        }
    }
}
