package com.tencent.assistantv2.component;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.js.JsBridge;
import com.tencent.assistant.m;
import com.tencent.assistant.net.c;
import com.tencent.assistant.utils.XLog;
import com.tencent.smtt.sdk.WebView;

/* compiled from: ProGuard */
public class VideoDetailView extends LinearLayout implements ek {
    private static final String c = VideoDetailView.class.getSimpleName();

    /* renamed from: a  reason: collision with root package name */
    private TxWebViewContainer f3024a;
    private Context b = null;

    public VideoDetailView(Context context) {
        super(context);
        this.b = context;
        e();
    }

    public VideoDetailView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.b = context;
        e();
    }

    private void e() {
        LayoutInflater.from(this.b).inflate((int) R.layout.video_detail_layout, this);
        this.f3024a = (TxWebViewContainer) findViewById(R.id.txwebviewcontainer);
        this.f3024a.a(this);
        dz dzVar = new dz();
        if (!c.a()) {
            dzVar.d = 1;
        } else {
            dzVar.d = -1;
        }
        this.f3024a.a(dzVar);
        if (4 == m.a().d()) {
            a("http://test-qzs.yybv.qq.com/open/video/index.html");
            XLog.i(c, "url : http://test-qzs.yybv.qq.com/open/video/index.html");
            return;
        }
        a("http://qzs.qq.com/open/video/index.html");
        XLog.i(c, "url : http://qzs.qq.com/open/video/index.html");
    }

    public void c() {
        if (this.f3024a != null) {
            this.f3024a.a();
        }
    }

    public void d() {
        if (this.f3024a != null) {
            this.f3024a.b();
        }
    }

    public void a(String str) {
        if (this.f3024a != null && !TextUtils.isEmpty(str)) {
            this.f3024a.a(str);
            this.f3024a.a(0);
        }
    }

    public void a(WebView webView, int i) {
    }

    public void a() {
    }

    public void b() {
        if (!JsBridge.getPreferences().getBoolean(JsBridge.WB_REPORT, false)) {
            a("javascript:(function () { var video = document.createElement('video'); window.location.href = 'jsb://webViewCompatibilityReport/0/console.log?canPlayType1=' + video.canPlayType('video/mp4; codecs=\"avc1.42E01E\"') + '&canPlayType2=' + video.canPlayType('video/mp4; codecs=\"mp4v.20.8\"') + '&canPlayType3=' + video.canPlayType('video/mp4'); })();");
            XLog.i(c, "[onPageFinished] ---> load js for report");
        }
    }
}
