package com.tencent.assistantv2.component.fps;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import com.tencent.assistant.component.appdetail.CommentDetailTabView;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistantv2.activity.AppDetailActivityV5;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class a extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SimpleAppModel f3214a;
    final /* synthetic */ STInfoV2 b;
    final /* synthetic */ FPSRankNormalItem c;

    a(FPSRankNormalItem fPSRankNormalItem, SimpleAppModel simpleAppModel, STInfoV2 sTInfoV2) {
        this.c = fPSRankNormalItem;
        this.f3214a = simpleAppModel;
        this.b = sTInfoV2;
    }

    public void onTMAClick(View view) {
        Intent intent = new Intent(this.c.i, AppDetailActivityV5.class);
        intent.putExtra(CommentDetailTabView.PARAMS_SIMPLE_MODEL_INFO, this.f3214a);
        if (!(this.c.i instanceof Activity)) {
            intent.setFlags(268435456);
        }
        this.c.i.startActivity(intent);
    }

    public STInfoV2 getStInfo() {
        if (this.b != null) {
            this.b.actionId = 200;
            this.b.status = "01";
        }
        return this.b;
    }
}
