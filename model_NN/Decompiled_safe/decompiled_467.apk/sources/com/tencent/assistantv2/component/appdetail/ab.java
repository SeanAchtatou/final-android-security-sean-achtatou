package com.tencent.assistantv2.component.appdetail;

import com.tencent.assistant.model.c;
import com.tencent.assistant.module.callback.a;
import com.tencent.assistantv2.activity.AppDetailActivityV5;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
class ab implements a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RecommendAppViewV5 f3064a;

    ab(RecommendAppViewV5 recommendAppViewV5) {
        this.f3064a = recommendAppViewV5;
    }

    public void a(int i, int i2, c cVar, int i3) {
        if (i2 != 0 || cVar == null || !AppDetailActivityV5.a(cVar)) {
            for (int i4 = 0; i4 < this.f3064a.m.size(); i4++) {
                if (this.f3064a.k[i4].getTag() != null && this.f3064a.k[i4].getTag().toString().equals(Constants.STR_EMPTY + i)) {
                    this.f3064a.k[i4].c();
                }
            }
            return;
        }
        this.f3064a.w.post(new ac(this, i, cVar));
    }
}
