package com.tencent.assistantv2.component.appdetail;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.activity.DownloadActivity;
import com.tencent.assistant.component.appdetail.CommentDetailTabView;
import com.tencent.assistant.component.appdetail.CustomTextView;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.manager.DownloadProxy;
import com.tencent.assistant.manager.f;
import com.tencent.assistant.manager.g;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.b;
import com.tencent.assistant.module.callback.a;
import com.tencent.assistant.module.h;
import com.tencent.assistant.module.u;
import com.tencent.assistant.protocol.jce.CardItem;
import com.tencent.assistant.protocol.jce.RecommendAppInfo;
import com.tencent.assistant.protocol.jce.SimpleAppInfo;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.bt;
import com.tencent.assistant.utils.ct;
import com.tencent.assistantv2.activity.AppDetailActivityV5;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class RecommendAppViewV5 extends LinearLayout implements CustomTextView.CustomTextViewInterface {
    private View.OnClickListener A = new z(this);
    private a B = new ab(this);

    /* renamed from: a  reason: collision with root package name */
    public String f3058a = Constants.STR_EMPTY;
    public String b = Constants.STR_EMPTY;
    /* access modifiers changed from: private */
    public Context c;
    private View d;
    private View e;
    private CustomTextView f;
    private LinearLayout[] g = new LinearLayout[4];
    private TXImageView[] h = new TXImageView[4];
    private TextView[] i = new TextView[4];
    /* access modifiers changed from: private */
    public TextView[] j = new TextView[4];
    /* access modifiers changed from: private */
    public DwonloadButtonForAppDetail[] k = new DwonloadButtonForAppDetail[4];
    /* access modifiers changed from: private */
    public TXDwonloadProcessBar[] l = new TXDwonloadProcessBar[4];
    /* access modifiers changed from: private */
    public List<RecommendAppInfo> m = new ArrayList();
    private List<CardItem> n = new ArrayList();
    /* access modifiers changed from: private */
    public List<SimpleAppModel> o = new ArrayList();
    private List<SimpleAppInfo> p = new ArrayList();
    private boolean q = false;
    private int r = 0;
    private long s = -100;
    private int t = 2000;
    private b u = new b();
    /* access modifiers changed from: private */
    public h v = new h();
    /* access modifiers changed from: private */
    public Handler w;
    private String x = Constants.STR_EMPTY;
    private boolean y = false;
    private String z = Constants.STR_EMPTY;

    public RecommendAppViewV5(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.c = context;
        d();
    }

    public RecommendAppViewV5(Context context) {
        super(context);
        this.c = context;
        d();
    }

    private void d() {
        setOrientation(0);
        inflate(this.c, R.layout.recommend_app_layout_v5, this);
        this.w = new Handler();
        this.d = findViewById(R.id.same_category_layout);
        this.e = findViewById(R.id.same_category_layout);
        this.f = (CustomTextView) findViewById(R.id.recommend_title);
        this.f.setStListener(this);
        this.g[0] = (LinearLayout) findViewById(R.id.ralate_app_layout1);
        this.g[1] = (LinearLayout) findViewById(R.id.ralate_app_layout2);
        this.g[2] = (LinearLayout) findViewById(R.id.ralate_app_layout3);
        this.g[3] = (LinearLayout) findViewById(R.id.ralate_app_layout4);
        for (int i2 = 0; i2 < this.g.length; i2++) {
            this.g[i2].setOnClickListener(this.A);
            this.g[i2].setBackgroundResource(R.drawable.v2_button_background_selector);
        }
        this.h[0] = (TXImageView) findViewById(R.id.soft_icon_img1);
        this.h[1] = (TXImageView) findViewById(R.id.soft_icon_img2);
        this.h[2] = (TXImageView) findViewById(R.id.soft_icon_img3);
        this.h[3] = (TXImageView) findViewById(R.id.soft_icon_img4);
        this.i[0] = (TextView) findViewById(R.id.soft_name_txt1);
        this.i[1] = (TextView) findViewById(R.id.soft_name_txt2);
        this.i[2] = (TextView) findViewById(R.id.soft_name_txt3);
        this.i[3] = (TextView) findViewById(R.id.soft_name_txt4);
        this.j[0] = (TextView) findViewById(R.id.reason1);
        this.j[1] = (TextView) findViewById(R.id.reason2);
        this.j[2] = (TextView) findViewById(R.id.reason3);
        this.j[3] = (TextView) findViewById(R.id.reason4);
        this.k[0] = (DwonloadButtonForAppDetail) findViewById(R.id.app_state_bar_1);
        this.k[1] = (DwonloadButtonForAppDetail) findViewById(R.id.app_state_bar_2);
        this.k[2] = (DwonloadButtonForAppDetail) findViewById(R.id.app_state_bar_3);
        this.k[3] = (DwonloadButtonForAppDetail) findViewById(R.id.app_state_bar_4);
        this.l[0] = (TXDwonloadProcessBar) findViewById(R.id.download_info1);
        this.l[1] = (TXDwonloadProcessBar) findViewById(R.id.download_info2);
        this.l[2] = (TXDwonloadProcessBar) findViewById(R.id.download_info3);
        this.l[3] = (TXDwonloadProcessBar) findViewById(R.id.download_info4);
    }

    public void a(String str) {
        this.z = str;
        for (int i2 = 0; i2 < this.g.length; i2++) {
            this.g[i2].setTag(R.id.tma_st_slot_tag, c(i2));
            this.g[i2].setTag(R.id.tma_st_pos, Integer.valueOf(i2));
        }
    }

    /* access modifiers changed from: private */
    public String c(int i2) {
        return this.z + "_" + ct.a(i2 + 1) + "|" + ((i2 + 1) % 4);
    }

    /* access modifiers changed from: private */
    public SimpleAppModel d(int i2) {
        switch (i2) {
            case R.id.ralate_app_layout1 /*2131165582*/:
                if (this.m.size() > 0) {
                    return this.o.get(0);
                }
                return null;
            case R.id.ralate_app_layout2 /*2131165586*/:
                if (this.m.size() > 1) {
                    return this.o.get(1);
                }
                return null;
            case R.id.ralate_app_layout3 /*2131165590*/:
                if (this.m.size() > 2) {
                    return this.o.get(2);
                }
                return null;
            case R.id.ralate_app_layout4 /*2131165603*/:
                if (this.m.size() > 3) {
                    return this.o.get(3);
                }
                return null;
            default:
                return null;
        }
    }

    public void a(int i2) {
        this.t = i2;
    }

    public int a() {
        return this.t;
    }

    /* access modifiers changed from: private */
    public void a(SimpleAppModel simpleAppModel) {
        if (simpleAppModel != null && !this.q) {
            this.q = true;
            Intent intent = new Intent(this.c, AppDetailActivityV5.class);
            intent.putExtra(CommentDetailTabView.PARAMS_SIMPLE_MODEL_INFO, simpleAppModel);
            if (!TextUtils.isEmpty(simpleAppModel.ac)) {
                intent.putExtra(com.tencent.assistant.b.a.C, simpleAppModel.ac);
            }
            intent.putExtra("same_tag_app", this.y);
            this.c.startActivity(intent);
            this.q = false;
        }
    }

    public void a(boolean z2) {
        this.y = z2;
    }

    public void a(int i2, List<RecommendAppInfo> list, List<SimpleAppInfo> list2) {
        a(i2, list, Constants.STR_EMPTY, list2);
    }

    public void a(int i2, List<RecommendAppInfo> list, String str, List<SimpleAppInfo> list2) {
        a(i2, list, list2, str);
    }

    public void a(int i2, List<RecommendAppInfo> list, List<SimpleAppInfo> list2, String str) {
        if (list != null && list.size() > 0) {
            if (list.size() > 4) {
                list = list.subList(0, 4);
            }
            this.m.clear();
            this.m.addAll(list);
            if (list2 != null) {
                this.p = list2;
            }
            if (!TextUtils.isEmpty(str)) {
                this.f.setText(str);
            } else {
                f(i2);
            }
            e(i2);
            this.v.register(this.B);
        }
    }

    public void a(String str, int i2, List<CardItem> list) {
        int i3 = 0;
        if (list != null && list.size() > 0) {
            if (list.size() > 4) {
                list = list.subList(0, 4);
            }
            this.n = list;
            this.x = str;
            this.m.clear();
            while (true) {
                int i4 = i3;
                if (i4 < list.size()) {
                    RecommendAppInfo recommendAppInfo = new RecommendAppInfo();
                    recommendAppInfo.d = list.get(i4).f.c;
                    recommendAppInfo.f2272a = list.get(i4).f.f2310a;
                    recommendAppInfo.c = list.get(i4).f.b;
                    recommendAppInfo.j = list.get(i4).f.d;
                    recommendAppInfo.f = bt.a(recommendAppInfo.j);
                    this.m.add(recommendAppInfo);
                    i3 = i4 + 1;
                } else {
                    this.v.register(this.B);
                    f(i2);
                    e(i2);
                    return;
                }
            }
        }
    }

    private void e(int i2) {
        int size;
        if (this.y) {
            size = this.n.size();
        } else {
            size = this.m.size();
        }
        for (int i3 = 0; i3 < size; i3++) {
            this.g[i3].setVisibility(0);
            RecommendAppInfo recommendAppInfo = this.m.get(i3);
            this.h[i3].updateImageView(recommendAppInfo.d, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            this.i[i3].setText(recommendAppInfo.c);
            if (!TextUtils.isEmpty(recommendAppInfo.f)) {
                this.j[i3].setText(recommendAppInfo.f);
                this.j[i3].setVisibility(0);
            } else {
                this.j[i3].setVisibility(8);
            }
            if (this.y) {
                this.o.add(i3, a(this.n.get(i3)));
            } else if (i3 < this.p.size()) {
                this.o.add(i3, a(this.m.get(i3), this.p.get(i3)));
            } else {
                this.o.add(i3, a(this.m.get(i3)));
            }
            if (i2 == 1 || i2 == 3 || i2 == 4 || i2 == 5) {
                new com.tencent.assistant.component.appdetail.process.a();
                this.l[i3].a(this.o.get(i3), this.j[i3]);
                this.k[i3].a(this.o.get(i3));
                this.k[i3].b().setOnClickListener(new aa(this, i3));
                if (i2 == 5) {
                    this.d.setBackgroundResource(R.drawable.bg_card_selector);
                }
            } else {
                this.k[i3].setVisibility(8);
                this.l[i3].setVisibility(8);
                this.d.setBackgroundResource(R.drawable.bg_card_selector);
            }
        }
        for (int size2 = this.m.size(); size2 < this.g.length; size2++) {
            this.g[size2].setVisibility(8);
        }
    }

    private void f(int i2) {
        this.f.setVisibility(0);
        if (i2 == 3) {
            this.f.setText((int) R.string.appdetail_page_default_relate_title);
        } else if (i2 == 6) {
            this.f.setText((int) R.string.appdetail_page_default_relate_title);
        } else if (i2 == -1) {
            this.f.setVisibility(8);
        } else if (i2 == 4) {
            this.f.setText(String.format(getResources().getString(R.string.smae_tag_apps_title), this.x));
        } else if (this.m.size() > 0 && this.m.size() < 4) {
            a(this.m.get(0).e);
        } else if (this.m.get(0).e == this.m.get(1).e || this.m.get(0).e == this.m.get(2).e) {
            a(this.m.get(0).e);
        } else if (this.m.get(1).e == this.m.get(2).e) {
            a(this.m.get(1).e);
        } else if (this.r == 1) {
            this.f.setText((int) R.string.recommend_reason_detail_common);
        } else if (this.r == 2) {
            this.f.setText((int) R.string.recommend_reason_download_common);
        } else if (this.r == 5) {
            this.f.setText((int) R.string.recommend_reason_friends);
        }
    }

    private void a(byte b2) {
        switch (b2) {
            case 2:
                this.f.setText((int) R.string.recommend_reason_detail_friend);
                return;
            case 3:
                if (this.r == 1) {
                    this.f.setText((int) R.string.recommend_reson_detail_percent);
                    return;
                } else if (this.r == 2) {
                    this.f.setText((int) R.string.recommend_reason_download_common);
                    return;
                } else {
                    return;
                }
            default:
                if (this.r == 1) {
                    this.f.setText((int) R.string.recommend_reason_detail_common);
                    return;
                } else if (this.r == 2) {
                    this.f.setText((int) R.string.recommend_reason_download_common);
                    return;
                } else {
                    return;
                }
        }
    }

    public void viewExposureST() {
        boolean z2;
        int i2;
        if ((this.c instanceof AppDetailActivityV5) || (this.c instanceof DownloadActivity)) {
            for (int i3 = 0; i3 < this.m.size(); i3++) {
                STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.c, 100);
                buildSTInfo.slotId = c(i3);
                buildSTInfo.appId = this.m.get(i3).f2272a;
                buildSTInfo.packageName = this.m.get(i3).b;
                buildSTInfo.recommendId = this.m.get(i3).g;
                if (this.m.get(i3).n == 1) {
                    z2 = true;
                } else {
                    z2 = false;
                }
                buildSTInfo.isImmediately = z2;
                k.a(buildSTInfo);
            }
            return;
        }
        int a2 = a();
        if (this.c instanceof BaseActivity) {
            BaseActivity baseActivity = (BaseActivity) this.c;
            if (a2 == 2000) {
                a2 = baseActivity.f();
            }
            i2 = baseActivity.p();
        } else {
            i2 = 2000;
        }
        k.a(new STInfoV2(a2, c(0), i2, STConst.ST_DEFAULT_SLOT, 100));
    }

    public void b(int i2) {
        this.r = i2;
    }

    /* access modifiers changed from: private */
    public void a(SimpleAppModel simpleAppModel, View view, int i2) {
        DownloadInfo downloadInfo;
        if (simpleAppModel != null) {
            if (simpleAppModel.d() || simpleAppModel.e() || simpleAppModel.h() || simpleAppModel.c() || simpleAppModel.e() || simpleAppModel.i()) {
                a(simpleAppModel);
                return;
            }
            DownloadInfo a2 = DownloadProxy.a().a(simpleAppModel);
            StatInfo buildDownloadSTInfo = STInfoBuilder.buildDownloadSTInfo(this.c, simpleAppModel);
            buildDownloadSTInfo.slotId = c(i2);
            buildDownloadSTInfo.recommendId = simpleAppModel.y;
            if (a2 != null && a2.needReCreateInfo(simpleAppModel)) {
                DownloadProxy.a().b(a2.downloadTicket);
                a2 = null;
            }
            if (a2 == null) {
                a2 = DownloadInfo.createDownloadInfo(simpleAppModel, buildDownloadSTInfo);
                if ((view instanceof g) && simpleAppModel != null) {
                    f.a().a(simpleAppModel.q(), (g) view);
                    downloadInfo = a2;
                }
                downloadInfo = a2;
            } else {
                a2.updateDownloadInfoStatInfo(buildDownloadSTInfo);
                downloadInfo = a2;
            }
            switch (ad.f3066a[u.d(simpleAppModel).ordinal()]) {
                case 1:
                case 2:
                    com.tencent.assistant.download.a.a().a(downloadInfo);
                    com.tencent.assistant.utils.a.a((ImageView) findViewWithTag(downloadInfo.downloadTicket));
                    return;
                case 3:
                case 4:
                    com.tencent.assistant.download.a.a().b(downloadInfo.downloadTicket);
                    return;
                case 5:
                    com.tencent.assistant.download.a.a().b(downloadInfo);
                    return;
                case 6:
                    com.tencent.assistant.download.a.a().d(downloadInfo);
                    return;
                case 7:
                    com.tencent.assistant.download.a.a().c(downloadInfo);
                    return;
                case 8:
                case 9:
                    com.tencent.assistant.download.a.a().a(downloadInfo);
                    return;
                case 10:
                    Toast.makeText(this.c, (int) R.string.unsupported, 0).show();
                    return;
                case 11:
                    Toast.makeText(this.c, (int) R.string.tips_slicent_install, 0).show();
                    return;
                case 12:
                    Toast.makeText(this.c, (int) R.string.tips_slicent_uninstall, 0).show();
                    return;
                default:
                    return;
            }
        }
    }

    public void b() {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < this.o.size() && i3 < this.k.length) {
                this.k[i3].a(this.o.get(i3));
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: private */
    public SimpleAppModel a(RecommendAppInfo recommendAppInfo) {
        SimpleAppModel simpleAppModel = new SimpleAppModel();
        simpleAppModel.f1634a = recommendAppInfo.f2272a;
        simpleAppModel.c = recommendAppInfo.b;
        simpleAppModel.d = recommendAppInfo.c;
        simpleAppModel.e = recommendAppInfo.d;
        simpleAppModel.P = recommendAppInfo.e;
        simpleAppModel.ae = recommendAppInfo.f;
        simpleAppModel.g = recommendAppInfo.h;
        simpleAppModel.k = recommendAppInfo.j;
        simpleAppModel.i = recommendAppInfo.k;
        simpleAppModel.b = recommendAppInfo.m;
        simpleAppModel.au = recommendAppInfo.n;
        simpleAppModel.y = recommendAppInfo.g;
        return simpleAppModel;
    }

    private SimpleAppModel a(RecommendAppInfo recommendAppInfo, SimpleAppInfo simpleAppInfo) {
        SimpleAppModel simpleAppModel = new SimpleAppModel();
        simpleAppModel.f1634a = recommendAppInfo.f2272a;
        simpleAppModel.c = recommendAppInfo.b;
        simpleAppModel.d = recommendAppInfo.c;
        simpleAppModel.e = recommendAppInfo.d;
        simpleAppModel.P = recommendAppInfo.e;
        simpleAppModel.ae = recommendAppInfo.f;
        simpleAppModel.g = recommendAppInfo.h;
        simpleAppModel.k = recommendAppInfo.j;
        simpleAppModel.au = recommendAppInfo.n;
        simpleAppModel.y = recommendAppInfo.g;
        simpleAppModel.i = simpleAppInfo.e;
        simpleAppModel.b = simpleAppInfo.p;
        simpleAppModel.B = simpleAppInfo.m;
        simpleAppModel.P = simpleAppInfo.r;
        simpleAppModel.ac = simpleAppInfo.s;
        return simpleAppModel;
    }

    private SimpleAppModel a(CardItem cardItem) {
        SimpleAppModel simpleAppModel = new SimpleAppModel();
        simpleAppModel.f1634a = cardItem.f.f2310a;
        simpleAppModel.c = cardItem.f.f;
        simpleAppModel.d = cardItem.f.b;
        simpleAppModel.e = cardItem.f.c;
        simpleAppModel.P = cardItem.f.r;
        simpleAppModel.g = cardItem.f.h;
        simpleAppModel.k = cardItem.f.d;
        simpleAppModel.i = cardItem.f.e;
        simpleAppModel.B = (long) cardItem.f2027a;
        simpleAppModel.b = cardItem.f.p;
        simpleAppModel.ac = cardItem.f.s;
        simpleAppModel.au = cardItem.q;
        simpleAppModel.y = cardItem.h;
        return simpleAppModel;
    }

    public void c() {
        for (int i2 = 0; i2 < 4; i2++) {
            this.k[i2].d();
            this.l[i2].a();
        }
    }
}
