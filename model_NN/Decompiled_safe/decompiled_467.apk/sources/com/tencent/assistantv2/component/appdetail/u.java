package com.tencent.assistantv2.component.appdetail;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamExClickListener;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class u extends OnTMAParamExClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ FriendTalkView f3085a;

    u(FriendTalkView friendTalkView) {
        this.f3085a = friendTalkView;
    }

    public STInfoV2 getStInfo(View view) {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f3085a.d, 200);
        buildSTInfo.scene = STConst.ST_PAGE_APP_DETAIL;
        buildSTInfo.slotId = this.f3085a.c + "_" + "011";
        return buildSTInfo;
    }

    public void onTMAClick(View view) {
        if (this.f3085a.k != null) {
            this.f3085a.k.a(view);
        }
    }
}
