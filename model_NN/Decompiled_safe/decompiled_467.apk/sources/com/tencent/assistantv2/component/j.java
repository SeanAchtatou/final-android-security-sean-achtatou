package com.tencent.assistantv2.component;

import android.content.res.Resources;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;

/* compiled from: ProGuard */
public class j extends Animation {
    private boolean A;

    /* renamed from: a  reason: collision with root package name */
    private final Resources f3218a;
    private float b;
    private float c;
    private float d;
    private float e;
    private float f;
    private float g;
    private int h;
    private int i;
    private int j;
    private int k;
    private int l;
    private int m;
    private int n;
    private int o;
    private int p;
    private int q;
    private float r;
    private float s;
    private float t;
    private float u;
    private boolean v;
    private View w;
    private int x;
    private int y;
    private k z;

    public j(View view, float f2, float f3, boolean z2) {
        this(f2, f3, f2, f3, 1, 0.5f, 1, 0.2f);
        a(view);
        this.v = z2;
    }

    public j(float f2, float f3, float f4, float f5, int i2, float f6, int i3, float f7) {
        this.f = 1.0f;
        this.g = 1.0f;
        this.h = 0;
        this.i = 0;
        this.j = 0;
        this.k = 0;
        this.l = 0;
        this.m = 0;
        this.n = 0;
        this.o = 0;
        this.p = 0;
        this.q = 0;
        this.r = 0.0f;
        this.s = 0.0f;
        this.v = false;
        this.A = false;
        this.f3218a = null;
        this.b = f2;
        this.c = f3;
        this.d = f4;
        this.e = f5;
        this.r = f6;
        this.p = i2;
        this.s = f7;
        this.q = i3;
        c();
    }

    private void c() {
        if (this.p == 0) {
            this.t = this.r;
        }
        if (this.q == 0) {
            this.u = this.s;
        }
    }

    public void a(View view) {
        this.w = view;
        ViewGroup.LayoutParams layoutParams = this.w.getLayoutParams();
        if (layoutParams != null) {
            this.x = layoutParams.height;
            this.y = layoutParams.width;
            return;
        }
        this.x = this.w.getHeight();
        this.y = this.w.getWidth();
    }

    /* access modifiers changed from: protected */
    public void applyTransformation(float f2, Transformation transformation) {
        float f3;
        float f4;
        if (this.b == 1.0f && this.c == 1.0f) {
            f3 = 1.0f;
        } else {
            f3 = this.b + ((this.c - this.b) * f2);
        }
        if (this.d == 1.0f && this.e == 1.0f) {
            f4 = 1.0f;
        } else {
            f4 = this.d + ((this.e - this.d) * f2);
        }
        this.f = f3;
        this.g = f4;
        if (this.t == 0.0f && this.u == 0.0f) {
            transformation.getMatrix().setScale(f3, f4);
        } else {
            transformation.getMatrix().setScale(f3, f4, this.t * 1.0f, this.u * 1.0f);
        }
        if (this.v && !hasEnded() && this.w != null) {
            ViewGroup.LayoutParams layoutParams = this.w.getLayoutParams();
            if (layoutParams == null) {
                layoutParams = new ViewGroup.LayoutParams(this.y, this.x);
            }
            layoutParams.height = (int) (f4 * ((float) this.x));
            this.w.setLayoutParams(layoutParams);
        }
        if (this.z == null) {
            return;
        }
        if (f2 == 0.0f) {
            this.A = false;
            this.z.a(this.b, this.c, this.d, this.e);
        } else if (f2 != 1.0f) {
            this.z.a(f2, transformation);
        } else if (!this.A) {
            this.A = true;
            this.z.b(this.b, this.c, this.d, this.e);
        }
    }

    /* access modifiers changed from: package-private */
    public float a(float f2, int i2, int i3, int i4, int i5) {
        float complexToDimension;
        if (i2 == 6) {
            complexToDimension = TypedValue.complexToFraction(i3, (float) i4, (float) i5);
        } else if (i2 != 5) {
            return f2;
        } else {
            complexToDimension = TypedValue.complexToDimension(i3, this.f3218a.getDisplayMetrics());
        }
        if (i4 == 0) {
            return 1.0f;
        }
        return complexToDimension / ((float) i4);
    }

    public void a(float f2, float f3) {
        this.b = f2;
        this.d = f2;
        this.c = f3;
        this.e = f3;
    }

    public void a(float f2) {
        this.b = this.f;
        this.d = this.g;
        this.c = f2;
        this.e = f2;
    }

    public void initialize(int i2, int i3, int i4, int i5) {
        super.initialize(i2, i3, i4, i5);
        this.b = a(this.b, this.h, this.l, i2, i4);
        this.c = a(this.c, this.i, this.m, i2, i4);
        this.d = a(this.d, this.j, this.n, i3, i5);
        this.e = a(this.e, this.k, this.o, i3, i5);
        this.t = resolveSize(this.p, this.r, i2, i4);
        this.u = resolveSize(this.q, this.s, i3, i5);
    }

    public boolean a() {
        return (this.f == 1.0f && this.g == 1.0f) ? false : true;
    }

    public float b() {
        return this.g;
    }

    public void a(k kVar) {
        this.z = kVar;
    }
}
