package com.tencent.assistantv2.component.topbanner;

import android.graphics.Bitmap;
import com.tencent.assistant.manager.notification.a.a.e;
import com.tencent.assistant.utils.ba;
import com.tencent.assistantv2.component.topbanner.TopBannerView;

/* compiled from: ProGuard */
class k implements e {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TopBannerView f3261a;

    k(TopBannerView topBannerView) {
        this.f3261a = topBannerView;
    }

    public void a(Bitmap bitmap) {
        if (bitmap == null || bitmap.isRecycled()) {
            ba.a().post(new m(this));
            return;
        }
        this.f3261a.a(bitmap, TopBannerView.ImageType.BANNERBG);
        Bitmap unused = this.f3261a.f = this.f3261a.b(bitmap, TopBannerView.ImageType.BANNERBG);
        ba.a().postDelayed(new l(this), 500);
    }
}
