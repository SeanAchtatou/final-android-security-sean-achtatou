package com.tencent.assistantv2.component.fps;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ProgressBar;

/* compiled from: ProGuard */
public class FPSProgressBar extends ProgressBar {
    public FPSProgressBar(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public FPSProgressBar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public FPSProgressBar(Context context) {
        super(context);
    }

    public void setVisibility(int i) {
        if (getVisibility() != i) {
            super.setVisibility(i);
        }
    }
}
