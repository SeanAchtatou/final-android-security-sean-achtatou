package com.tencent.assistantv2.component.search;

import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.e;
import com.tencent.assistant.module.callback.ac;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
class b extends ac {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ NativeSearchResultPage f3234a;

    private b(NativeSearchResultPage nativeSearchResultPage) {
        this.f3234a = nativeSearchResultPage;
    }

    /* synthetic */ b(NativeSearchResultPage nativeSearchResultPage, a aVar) {
        this(nativeSearchResultPage);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.component.search.NativeSearchResultPage.a(com.tencent.assistantv2.component.search.NativeSearchResultPage, boolean):boolean
     arg types: [com.tencent.assistantv2.component.search.NativeSearchResultPage, int]
     candidates:
      com.tencent.assistantv2.component.search.NativeSearchResultPage.a(long, java.lang.String):void
      com.tencent.assistantv2.component.search.NativeSearchResultPage.a(java.lang.String, int):void
      com.tencent.assistantv2.component.search.ISearchResultPage.a(java.lang.String, int):void
      com.tencent.assistantv2.component.search.NativeSearchResultPage.a(com.tencent.assistantv2.component.search.NativeSearchResultPage, boolean):boolean */
    public void a(int i, int i2, boolean z, int i3, ArrayList<String> arrayList, boolean z2, List<e> list, long j, String str, int i4) {
        this.f3234a.a(false);
        if (i2 == 0) {
            this.f3234a.f = j;
            if (this.f3234a.r == null || !(this.f3234a.r.b().e() == null || this.f3234a.r.b().e().size() == 0)) {
                boolean unused = this.f3234a.q = false;
                this.f3234a.a(z, i3, arrayList, z2, list, j, str, i4);
            } else {
                boolean unused2 = this.f3234a.q = true;
                this.f3234a.o();
            }
        } else {
            boolean unused3 = this.f3234a.q = false;
            this.f3234a.a(i2, z, z2);
        }
        if (z) {
            this.f3234a.a(j, this.f3234a.f3224a);
        }
    }

    public void a(ArrayList<SimpleAppModel> arrayList, ArrayList<String> arrayList2, int i) {
    }
}
