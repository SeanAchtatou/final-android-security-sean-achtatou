package com.tencent.assistantv2.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.LoadingView;
import com.tencent.assistant.component.NormalErrorRecommendPage;
import com.tencent.assistant.component.invalidater.ListViewScrollListener;
import com.tencent.assistant.manager.NetworkMonitor;
import com.tencent.assistant.manager.cq;
import com.tencent.assistant.module.de;
import com.tencent.assistant.net.APN;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.activity.ay;
import com.tencent.assistantv2.adapter.u;

/* compiled from: ProGuard */
public class RankCustomizeListPage extends RelativeLayout implements NetworkMonitor.ConnectivityChangeListener, cd {

    /* renamed from: a  reason: collision with root package name */
    private Context f3005a = null;
    private LayoutInflater b = null;
    private LoadingView c;
    private NormalErrorRecommendPage d;
    private RankCustomizeListView e;
    /* access modifiers changed from: private */
    public int f;
    /* access modifiers changed from: private */
    public int g;
    private View.OnClickListener h = new bu(this);
    private APN i = APN.NO_NETWORK;

    public RankCustomizeListPage(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f3005a = context;
        a(context);
    }

    public RankCustomizeListPage(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.f3005a = context;
        a(context);
    }

    public RankCustomizeListPage(Context context, de deVar, int i2, int i3) {
        super(context);
        this.f3005a = context;
        a(context);
        this.e.a(deVar, i2, i3);
        this.e.a(this);
        this.f = i2;
        this.g = i3;
    }

    private void a(Context context) {
        cq.a().a(this);
        this.b = LayoutInflater.from(context);
        View inflate = this.b.inflate((int) R.layout.rank_customize_component_view, this);
        this.e = (RankCustomizeListView) inflate.findViewById(R.id.applist);
        this.e.setVisibility(8);
        this.c = (LoadingView) inflate.findViewById(R.id.loading_view);
        this.c.setVisibility(0);
        this.d = (NormalErrorRecommendPage) inflate.findViewById(R.id.error_page);
        this.d.setButtonClickListener(this.h);
    }

    public void a(int i2) {
        this.c.setVisibility(8);
        this.e.setVisibility(8);
        this.d.setVisibility(0);
        this.d.setErrorType(i2);
    }

    public void a() {
        this.c.setVisibility(0);
        this.e.setVisibility(8);
        this.d.setVisibility(8);
    }

    public void a(u uVar, ListViewScrollListener listViewScrollListener) {
        this.e.d();
        this.e.setAdapter(uVar);
        this.e.a(listViewScrollListener);
        uVar.a(listViewScrollListener);
    }

    public void a(int i2, int i3) {
        a();
        this.e.a(i2, i3);
    }

    public void b() {
        this.e.setVisibility(0);
        this.d.setVisibility(8);
        this.c.setVisibility(8);
    }

    public void c() {
    }

    public void d() {
        this.e.b();
    }

    public void e() {
        if (this.e != null) {
            this.e.c();
        }
    }

    public int f() {
        return STConst.ST_PAGE_RANK_RECOMMEND;
    }

    public void onConnected(APN apn) {
        if (this.e.f() == null || this.e.f().getGroupCount() <= 0) {
            a(this.f, this.g);
        }
    }

    public void onDisconnected(APN apn) {
        this.i = apn;
    }

    public void onConnectivityChanged(APN apn, APN apn2) {
        this.i = apn2;
    }

    public boolean g() {
        if (this.e != null) {
            return this.e.g();
        }
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.component.RankCustomizeListView.a(boolean, boolean):void
     arg types: [boolean, int]
     candidates:
      com.tencent.assistantv2.component.RankCustomizeListView.a(int, int):void
      com.tencent.assistantv2.component.RankCustomizeListView.a(boolean, boolean):void */
    public void a(boolean z) {
        if (this.e != null) {
            this.e.a(z, false);
        }
    }

    public void h() {
        if (this.e != null) {
            this.e.e();
        }
    }

    public void a(ay ayVar) {
        if (this.e != null) {
            this.e.a(ayVar);
        }
    }
}
