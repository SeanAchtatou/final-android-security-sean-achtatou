package com.tencent.assistantv2.component;

import android.view.View;
import android.view.animation.Animation;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
class cn implements Animation.AnimationListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TextView f3159a;
    final /* synthetic */ View b;
    final /* synthetic */ RankSecondNavigationView c;

    cn(RankSecondNavigationView rankSecondNavigationView, TextView textView, View view) {
        this.c = rankSecondNavigationView;
        this.f3159a = textView;
        this.b = view;
    }

    public void onAnimationStart(Animation animation) {
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationEnd(Animation animation) {
        this.f3159a.setTextColor(this.c.f3009a.getResources().getColor(R.color.navigation_title_selected));
        this.c.a(this.b, this.f3159a);
    }
}
