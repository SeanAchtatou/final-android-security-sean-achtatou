package com.tencent.assistantv2.component;

import com.tencent.assistant.AppConst;
import com.tencent.assistant.utils.v;
import com.tencent.assistantv2.mediadownload.o;
import com.tencent.assistantv2.model.h;

/* compiled from: ProGuard */
class eo extends AppConst.TwoBtnDialogInfo {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ h f3202a;
    final /* synthetic */ VideoDownloadButton b;

    eo(VideoDownloadButton videoDownloadButton, h hVar) {
        this.b = videoDownloadButton;
        this.f3202a = hVar;
    }

    public void onLeftBtnClick() {
    }

    public void onRightBtnClick() {
        int c = o.c().c(this.f3202a);
        if (c != 0) {
            v.a(new er(this.b.getContext(), this.f3202a, c, new ep(this)));
            return;
        }
        o.c().b(this.f3202a.c);
        o.c().a(this.f3202a);
    }

    public void onCancell() {
    }
}
