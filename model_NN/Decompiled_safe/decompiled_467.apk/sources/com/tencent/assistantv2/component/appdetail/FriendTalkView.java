package com.tencent.assistantv2.component.appdetail;

import android.content.Context;
import android.graphics.Color;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.appdetail.CustomTextView;
import com.tencent.assistant.protocol.jce.CommentDetail;
import com.tencent.assistant.protocol.jce.CommentTagInfo;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.ct;
import com.tencent.assistantv2.activity.AppDetailActivityV5;
import com.tencent.assistantv2.component.FlowLayout;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class FriendTalkView extends RelativeLayout implements CustomTextView.CustomTextViewInterface {

    /* renamed from: a  reason: collision with root package name */
    ArrayList<CommentDetail> f3057a;
    ArrayList<CommentTagInfo> b;
    String c = Constants.VIA_REPORT_TYPE_SHARE_TO_QQ;
    /* access modifiers changed from: private */
    public Context d;
    private CustomTextView e;
    private LinearLayout f;
    private TextView g;
    private ImageView h;
    private final int i = 3;
    /* access modifiers changed from: private */
    public FlowLayout j;
    /* access modifiers changed from: private */
    public y k;

    public FriendTalkView(Context context) {
        super(context);
        a(context);
    }

    public FriendTalkView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    private void a(Context context) {
        this.d = context;
        LayoutInflater.from(this.d).inflate((int) R.layout.friend_talk_view, this);
        this.e = (CustomTextView) findViewById(R.id.title);
        this.j = (FlowLayout) findViewById(R.id.comment_taglist);
        this.f = (LinearLayout) findViewById(R.id.friends_talk_content);
        this.g = (TextView) findViewById(R.id.comment_more_txt);
        this.h = (ImageView) findViewById(R.id.comment_more_icon);
        this.e.setStListener(this);
    }

    public void a(String str, ArrayList<CommentDetail> arrayList, ArrayList<CommentTagInfo> arrayList2) {
        this.f3057a = arrayList;
        this.b = arrayList2;
        this.g.setOnClickListener(new u(this));
        this.h.setOnClickListener(new v(this));
        a(arrayList2);
        if (!TextUtils.isEmpty(str)) {
            this.e.setText(str);
            this.e.setVisibility(0);
        } else {
            this.e.setVisibility(8);
        }
        if (arrayList.size() > 0) {
            this.f.removeAllViews();
            for (int i2 = 0; i2 < arrayList.size(); i2++) {
                FriendTalkItemView a2 = new FriendTalkItemView(getContext()).a(arrayList.get(i2));
                if (i2 == arrayList.size() - 1) {
                    a2.findViewById(R.id.divider).setVisibility(4);
                }
                a2.setTag(R.id.tma_st_slot_tag, ct.a(i2 + 1));
                a2.setOnClickListener(new w(this));
                this.f.addView(a2);
            }
            setVisibility(0);
            return;
        }
        setVisibility(8);
    }

    public void viewExposureST() {
        if (this.d instanceof AppDetailActivityV5) {
            STInfoV2 i2 = ((AppDetailActivityV5) this.d).i();
            for (int i3 = 0; i3 < this.f.getChildCount(); i3++) {
                STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(getContext(), 100);
                if (buildSTInfo != null) {
                    buildSTInfo.slotId = a.a(this.c, i3);
                    if (i2 != null) {
                        buildSTInfo.appId = i2.appId;
                        buildSTInfo.contentId = i2.contentId;
                    }
                    k.a(buildSTInfo);
                }
            }
        }
    }

    public void a(y yVar) {
        this.k = yVar;
        this.j.a(yVar);
    }

    public void a(List<CommentTagInfo> list) {
        if (list != null && list.size() > 0) {
            this.j.removeAllViews();
            for (int i2 = 0; i2 < list.size(); i2++) {
                CommentTagInfo commentTagInfo = list.get(i2);
                String a2 = commentTagInfo.a();
                byte b2 = commentTagInfo.b();
                TextView textView = new TextView(this.d);
                if (b2 == 2) {
                    textView.setBackgroundResource(R.drawable.comment_tag_color_good_background);
                    textView.setTextColor(Color.parseColor("#bb9359"));
                } else if (b2 == 3) {
                    textView.setBackgroundResource(R.drawable.comment_tag_color_bad_background);
                    textView.setTextColor(Color.parseColor("#8891b7"));
                } else {
                    textView.setBackgroundResource(R.drawable.comment_tag_score_normal_background);
                    textView.setTextColor(Color.parseColor("#6e6e6e"));
                }
                textView.setTag(textView.getBackground());
                String str = a2 + "(" + ct.a(commentTagInfo.e) + ")";
                SpannableString spannableString = new SpannableString(str);
                spannableString.setSpan(new AbsoluteSizeSpan(13, true), 0, a2.length(), 33);
                spannableString.setSpan(new AbsoluteSizeSpan(11, true), a2.length(), str.length(), 33);
                textView.setText(spannableString);
                textView.setTextSize(0, (float) this.d.getResources().getDimensionPixelSize(R.dimen.app_detail_comment_tag_text_size));
                textView.setTag(R.id.tma_st_slot_tag, ct.a(i2 + 4));
                textView.setOnClickListener(new x(this, commentTagInfo));
                ViewGroup.MarginLayoutParams marginLayoutParams = new ViewGroup.MarginLayoutParams(-2, -2);
                marginLayoutParams.leftMargin = 0;
                marginLayoutParams.rightMargin = 16;
                marginLayoutParams.topMargin = 0;
                marginLayoutParams.bottomMargin = 0;
                this.j.addView(textView, marginLayoutParams);
                k.a(new STInfoV2(STConst.ST_PAGE_APP_DETAIL, this.c + "_" + ct.a(i2 + 4), 0, STConst.ST_DEFAULT_SLOT, 100));
            }
        }
    }
}
