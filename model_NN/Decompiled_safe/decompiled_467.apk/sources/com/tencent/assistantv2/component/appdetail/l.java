package com.tencent.assistantv2.component.appdetail;

import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class l extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CustomRelateAppViewV5 f3076a;

    l(CustomRelateAppViewV5 customRelateAppViewV5) {
        this.f3076a = customRelateAppViewV5;
    }

    public void onTMAClick(View view) {
        switch (view.getId()) {
            case R.id.ralate_app_layout1 /*2131165582*/:
                if (this.f3076a.m.size() > 0) {
                    this.f3076a.a((SimpleAppModel) this.f3076a.m.get(0));
                    return;
                }
                return;
            case R.id.ralate_app_layout2 /*2131165586*/:
                if (this.f3076a.m.size() > 1) {
                    this.f3076a.a((SimpleAppModel) this.f3076a.m.get(1));
                    return;
                }
                return;
            case R.id.ralate_app_layout3 /*2131165590*/:
                if (this.f3076a.m.size() > 2) {
                    this.f3076a.a((SimpleAppModel) this.f3076a.m.get(2));
                    return;
                }
                return;
            case R.id.ralate_app_layout4 /*2131165603*/:
                if (this.f3076a.m.size() > 3) {
                    this.f3076a.a((SimpleAppModel) this.f3076a.m.get(3));
                    return;
                }
                return;
            default:
                return;
        }
    }

    public STInfoV2 getStInfo() {
        int parseInt = Integer.parseInt(this.f3076a.findViewById(this.clickViewId).getTag(R.id.tma_st_slot_tag).toString());
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f3076a.f, (SimpleAppModel) this.f3076a.m.get(parseInt), this.f3076a.a(parseInt), 200, "01");
        buildSTInfo.recommendId = ((SimpleAppModel) this.f3076a.m.get(parseInt)).y;
        return buildSTInfo;
    }
}
