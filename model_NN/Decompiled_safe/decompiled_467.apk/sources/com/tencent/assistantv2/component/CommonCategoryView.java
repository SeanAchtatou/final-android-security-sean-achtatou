package com.tencent.assistantv2.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.adapter.AppCategoryListAdapter;
import com.tencent.assistant.adapter.s;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.protocol.jce.AppCategory;
import com.tencent.assistant.protocol.jce.TagGroup;
import com.tencent.assistant.utils.df;
import com.tencent.assistantv2.st.b.b;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class CommonCategoryView extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    private LinearLayout f2986a;
    /* access modifiers changed from: private */
    public View.OnClickListener b;
    private AppCategoryListAdapter.CategoryType c;
    private final int d;
    private b e;
    private View.OnClickListener f;

    public CommonCategoryView(Context context) {
        this(context, null);
    }

    public CommonCategoryView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, AppCategoryListAdapter.CategoryType.CATEGORYTYPESOFTWARE, null);
    }

    public CommonCategoryView(Context context, AttributeSet attributeSet, AppCategoryListAdapter.CategoryType categoryType, View.OnClickListener onClickListener) {
        super(context, attributeSet);
        this.d = 4;
        this.e = null;
        this.f = new s(this);
        a(context, attributeSet);
        this.c = categoryType;
        this.b = onClickListener;
    }

    private void a(Context context, AttributeSet attributeSet) {
        LayoutInflater.from(getContext()).inflate((int) R.layout.common_category_view, this);
        setOrientation(1);
        this.f2986a = (LinearLayout) findViewById(R.id.item_container);
    }

    public void a(List<AppCategory> list) {
        switch (t.f3249a[this.c.ordinal()]) {
            case 1:
                b(list);
                return;
            case 2:
                c(list);
                return;
            default:
                return;
        }
    }

    private void b(List<AppCategory> list) {
        s sVar;
        if (list != null) {
            int size = 4 < list.size() ? 4 : list.size();
            this.f2986a.removeAllViews();
            this.f2986a.setPadding(0, 0, 0, 0);
            for (int i = 0; i < size; i++) {
                View inflate = LayoutInflater.from(getContext()).inflate((int) R.layout.common_category_app_item_view, (ViewGroup) null);
                TXImageView tXImageView = (TXImageView) inflate.findViewById(R.id.icon);
                TextView textView = (TextView) inflate.findViewById(R.id.title);
                AppCategory appCategory = list.get(i);
                if (appCategory != null) {
                    tXImageView.updateImageView(appCategory.c.f2252a, R.color.app_category_parent_cover, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
                    textView.setText(appCategory.b);
                    sVar = new s(appCategory.f1985a, 0, appCategory.b, 0, null, appCategory.b);
                } else {
                    sVar = null;
                }
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, -2);
                layoutParams.weight = 1.0f;
                this.f2986a.addView(inflate, layoutParams);
                inflate.setTag(R.id.category_data, sVar);
                inflate.setOnClickListener(this.f);
                if (i < list.size() - 1) {
                    View view = new View(getContext());
                    view.setBackgroundResource(R.color.app_separator_line);
                    this.f2986a.addView(view, new LinearLayout.LayoutParams(1, df.a(getContext(), 44.0f)));
                }
                a(sVar, i, 100);
            }
        }
    }

    private void c(List<AppCategory> list) {
        s sVar;
        ArrayList<TagGroup> arrayList;
        String str;
        if (list != null) {
            int size = 4 < list.size() ? 4 : list.size();
            this.f2986a.removeAllViews();
            this.f2986a.setPadding(0, 0, df.a(getContext(), 22.0f), 0);
            for (int i = 0; i < size; i++) {
                View inflate = LayoutInflater.from(getContext()).inflate((int) R.layout.common_category_game_item_view, (ViewGroup) null);
                TextView textView = (TextView) inflate.findViewById(R.id.title);
                AppCategory appCategory = list.get(i);
                if (appCategory == null || (arrayList = appCategory.f) == null || arrayList.size() <= 0 || arrayList.get(0) == null) {
                    sVar = null;
                } else {
                    textView.setText(arrayList.get(0).b);
                    if (arrayList.get(0).c != null) {
                        str = arrayList.get(0).c.f1970a;
                    } else {
                        str = null;
                    }
                    sVar = new s(appCategory.f1985a, arrayList.get(0).f2389a, arrayList.get(0).b, arrayList.get(0).d, str, arrayList.get(0).b);
                }
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, -2);
                layoutParams.weight = 1.0f;
                this.f2986a.addView(inflate, layoutParams);
                inflate.setTag(R.id.category_pos, Integer.valueOf(i));
                inflate.setTag(R.id.category_data, sVar);
                inflate.setOnClickListener(this.f);
                a(sVar, i, 100);
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(s sVar, int i, int i2) {
        if (getContext() instanceof BaseActivity) {
            STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(getContext(), i2);
            buildSTInfo.slotId = a.a("04", i);
            if (sVar != null) {
                buildSTInfo.recommendId = (String.valueOf(sVar.f808a) + "," + String.valueOf(sVar.b)).getBytes();
                buildSTInfo.extraData = sVar.c;
            }
            if (i2 == 100) {
                if (this.e == null) {
                    this.e = new b();
                }
                this.e.exposure(buildSTInfo);
                return;
            }
            k.a(buildSTInfo);
        }
    }
}
