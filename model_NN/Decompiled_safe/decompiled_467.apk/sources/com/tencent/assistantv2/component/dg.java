package com.tencent.assistantv2.component;

import android.view.View;

/* compiled from: ProGuard */
class dg implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TabBarView f3174a;

    private dg(TabBarView tabBarView) {
        this.f3174a = tabBarView;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.component.TabBarView.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.tencent.assistantv2.component.TabBarView.a(android.content.Context, android.util.AttributeSet):void
      com.tencent.assistantv2.component.TabBarView.a(int, float):void
      com.tencent.assistantv2.component.TabBarView.a(int, boolean):void */
    public void onClick(View view) {
        int id = view.getId();
        for (int i = 0; i < this.f3174a.i.length; i++) {
            if (this.f3174a.b(i) != null && id == this.f3174a.b(i).getId()) {
                this.f3174a.a(i, true);
                if (this.f3174a.l != null) {
                    this.f3174a.l.a(view, 1);
                }
            }
        }
    }
}
