package com.tencent.assistantv2.manager;

import com.tencent.assistant.h.c;
import com.tencent.assistant.protocol.jce.CardItemWrapper;
import com.tencent.assistant.protocol.jce.ColorCardItem;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
class e implements com.tencent.assistantv2.model.a.e {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f3274a;

    private e(a aVar) {
        this.f3274a = aVar;
    }

    /* synthetic */ e(a aVar, b bVar) {
        this(aVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.manager.a.a(com.tencent.assistantv2.manager.a, boolean):boolean
     arg types: [com.tencent.assistantv2.manager.a, int]
     candidates:
      com.tencent.assistantv2.manager.a.a(com.tencent.assistantv2.manager.a, long):long
      com.tencent.assistantv2.manager.a.a(com.tencent.assistantv2.manager.d, com.tencent.assistant.h.c):void
      com.tencent.assistantv2.manager.a.a(com.tencent.assistantv2.manager.a, boolean):boolean */
    public void a(int i, int i2, boolean z, byte[] bArr, boolean z2, ArrayList<ColorCardItem> arrayList, List<CardItemWrapper> list, c cVar) {
        long unused = this.f3274a.f3270a = System.currentTimeMillis();
        d dVar = new d(this.f3274a, null);
        dVar.f3273a = i;
        dVar.b = i2;
        dVar.c = z;
        dVar.d = bArr;
        dVar.e = z2;
        dVar.f = arrayList;
        dVar.g = null;
        dVar.h = list;
        if (i2 >= 0 && list != null && list.size() > 0) {
            boolean unused2 = this.f3274a.d = true;
        }
        this.f3274a.a(dVar, cVar);
    }
}
