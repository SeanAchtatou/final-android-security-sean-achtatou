package com.tencent.downloadsdk;

import com.tencent.downloadsdk.network.c;
import com.tencent.downloadsdk.network.d;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import org.apache.http.util.ByteArrayBuffer;

class q implements d<byte[]> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ o f3635a;

    q(o oVar) {
        this.f3635a = oVar;
    }

    /* renamed from: a */
    public byte[] b(int i, c<byte[]> cVar, InputStream inputStream) {
        if (i == 200) {
            try {
                ByteArrayBuffer byteArrayBuffer = new ByteArrayBuffer(4096);
                BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream, 4096);
                byte[] bArr = new byte[4096];
                while (true) {
                    int read = bufferedInputStream.read(bArr);
                    if (read == -1) {
                        return byteArrayBuffer.buffer();
                    }
                    byteArrayBuffer.append(bArr, 0, read);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
