package com.tencent.downloadsdk;

import com.tencent.downloadsdk.a.c;

class f implements ah {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ c f3606a;

    f(c cVar) {
        this.f3606a = cVar;
    }

    public void a() {
        com.tencent.downloadsdk.utils.f.b("DownloadScheduler", "onSave2FileSucceed");
        this.f3606a.g();
    }

    public void a(int i) {
        com.tencent.downloadsdk.utils.f.b("DownloadScheduler", "onSave2FileFailed  tid: " + Thread.currentThread().getId() + " errorCode: " + i);
        int unused = this.f3606a.v = i;
        this.f3606a.e();
    }

    public void a(long j) {
        if (this.f3606a.o != null) {
            this.f3606a.o.a(j, c.c(this.f3606a.z.c()));
        }
    }

    public void a(String str) {
        if (this.f3606a.I != null) {
            this.f3606a.I.s = str;
        }
    }

    public void b() {
        com.tencent.downloadsdk.utils.f.b("DownloadScheduler", "DownloadSetting.mMaxThreadNum: " + this.f3606a.p.i);
        com.tencent.downloadsdk.utils.f.b("DownloadScheduler", "DownloadSetting.mSocketBufferSize: " + this.f3606a.p.h);
        com.tencent.downloadsdk.utils.f.b("DownloadScheduler", "DownloadSetting.mReadBufferSize: " + this.f3606a.p.h);
        this.f3606a.o.e();
        this.f3606a.g();
    }
}
