package com.tencent.downloadsdk.utils;

import com.tencent.downloadsdk.DownloadTask;
import java.util.Comparator;

public class h implements Comparator<DownloadTask> {
    /* renamed from: a */
    public int compare(DownloadTask downloadTask, DownloadTask downloadTask2) {
        return downloadTask.b.ordinal() == downloadTask2.b.ordinal() ? downloadTask.h > downloadTask2.h ? 1 : -1 : downloadTask2.b.ordinal() - downloadTask.b.ordinal();
    }
}
