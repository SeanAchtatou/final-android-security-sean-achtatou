package com.tencent.downloadsdk;

class aa implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f3586a;
    final /* synthetic */ byte[] b;
    final /* synthetic */ DownloadTask c;

    aa(DownloadTask downloadTask, int i, byte[] bArr) {
        this.c = downloadTask;
        this.f3586a = i;
        this.b = bArr;
    }

    public void run() {
        this.c.x.a(this.c.c, this.c.d, this.c.f3580a.ordinal());
        if (this.c.q != null) {
            int i = this.f3586a;
            if ((-this.f3586a) > 32000) {
                i = this.f3586a + 32000;
            } else if ((-this.f3586a) > 30000) {
                i = this.f3586a + 30000;
            } else if ((-this.f3586a) > 2000) {
                i = this.f3586a + 2000;
            }
            this.c.q.a(this.c.c, this.c.d, i, this.b, this.c.d());
        }
    }
}
