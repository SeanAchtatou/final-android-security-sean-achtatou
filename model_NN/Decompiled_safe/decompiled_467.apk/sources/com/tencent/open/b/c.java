package com.tencent.open.b;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import com.tencent.connect.common.Constants;
import com.tencent.open.a.n;
import com.tencent.open.utils.Global;
import java.util.Locale;

/* compiled from: ProGuard */
public class c {

    /* renamed from: a  reason: collision with root package name */
    static String f3763a = null;
    static String b = null;
    static String c = null;
    private static String d;
    private static String e = null;

    public static String a() {
        try {
            Context context = Global.getContext();
            if (context == null) {
                return Constants.STR_EMPTY;
            }
            WifiManager wifiManager = (WifiManager) context.getSystemService("wifi");
            if (wifiManager == null) {
                return Constants.STR_EMPTY;
            }
            WifiInfo connectionInfo = wifiManager.getConnectionInfo();
            if (connectionInfo == null) {
                return Constants.STR_EMPTY;
            }
            return connectionInfo.getMacAddress();
        } catch (SecurityException e2) {
            n.b("MobileInfoUtil", "getLocalMacAddress>>>", e2);
            return Constants.STR_EMPTY;
        }
    }

    public static String a(Context context) {
        if (!TextUtils.isEmpty(d)) {
            return d;
        }
        if (context == null) {
            return Constants.STR_EMPTY;
        }
        d = Constants.STR_EMPTY;
        WindowManager windowManager = (WindowManager) context.getSystemService("window");
        if (windowManager != null) {
            int width = windowManager.getDefaultDisplay().getWidth();
            d = width + "x" + windowManager.getDefaultDisplay().getHeight();
        }
        return d;
    }

    public static String b() {
        return Locale.getDefault().getLanguage();
    }

    public static String b(Context context) {
        if (f3763a != null && f3763a.length() > 0) {
            return f3763a;
        }
        if (context == null) {
            return Constants.STR_EMPTY;
        }
        try {
            f3763a = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
            return f3763a;
        } catch (Exception e2) {
            return Constants.STR_EMPTY;
        }
    }

    public static String c(Context context) {
        if (b != null && b.length() > 0) {
            return b;
        }
        if (context == null) {
            return Constants.STR_EMPTY;
        }
        try {
            b = ((TelephonyManager) context.getSystemService("phone")).getSimSerialNumber();
            return b;
        } catch (Exception e2) {
            return Constants.STR_EMPTY;
        }
    }

    public static String d(Context context) {
        if (c != null && c.length() > 0) {
            return c;
        }
        if (context == null) {
            return Constants.STR_EMPTY;
        }
        try {
            c = Settings.Secure.getString(context.getContentResolver(), "android_id");
            return c;
        } catch (Exception e2) {
            return Constants.STR_EMPTY;
        }
    }

    public static String e(Context context) {
        try {
            if (e == null) {
                DisplayMetrics displayMetrics = new DisplayMetrics();
                ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
                StringBuilder sb = new StringBuilder();
                sb.append("imei=").append(b(context)).append('&');
                sb.append("model=").append(Build.MODEL).append('&');
                sb.append("os=").append(Build.VERSION.RELEASE).append('&');
                sb.append("apilevel=").append(Build.VERSION.SDK_INT).append('&');
                String b2 = a.b(context);
                if (b2 == null) {
                    b2 = Constants.STR_EMPTY;
                }
                sb.append("network=").append(b2).append('&');
                sb.append("sdcard=").append(Environment.getExternalStorageState().equals("mounted") ? 1 : 0).append('&');
                sb.append("display=").append(displayMetrics.widthPixels).append('*').append(displayMetrics.heightPixels).append('&');
                sb.append("manu=").append(Build.MANUFACTURER).append("&");
                sb.append("wifi=").append(a.e(context));
                e = sb.toString();
            }
            return e;
        } catch (Exception e2) {
            return null;
        }
    }
}
