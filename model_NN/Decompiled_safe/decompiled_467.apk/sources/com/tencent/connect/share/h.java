package com.tencent.connect.share;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import com.tencent.assistant.component.txscrollview.TXTabBarLayout;
import com.tencent.open.a.n;
import com.tencent.open.utils.Util;
import java.util.ArrayList;

/* compiled from: ProGuard */
final class h implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ArrayList f3506a;
    final /* synthetic */ Handler b;

    h(ArrayList arrayList, Handler handler) {
        this.f3506a = arrayList;
        this.b = handler;
    }

    public void run() {
        Bitmap a2;
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.f3506a.size()) {
                String str = (String) this.f3506a.get(i2);
                if (!Util.isValidUrl(str) && Util.fileExists(str) && (a2 = d.a(str, 10000)) != null) {
                    String str2 = Environment.getExternalStorageDirectory() + "/tmp/";
                    String str3 = "share2qzone_temp" + Util.encrypt(str) + ".jpg";
                    if (!d.b(str, 640, 10000)) {
                        n.b("AsynScaleCompressImage", "not out of bound,not compress!");
                    } else {
                        n.b("AsynScaleCompressImage", "out of bound, compress!");
                        str = d.a(a2, str2, str3);
                    }
                    if (str != null) {
                        this.f3506a.set(i2, str);
                    }
                }
                i = i2 + 1;
            } else {
                Message obtainMessage = this.b.obtainMessage(TXTabBarLayout.TABITEM_TIPS_TEXT_ID);
                Bundle bundle = new Bundle();
                bundle.putStringArrayList("images", this.f3506a);
                obtainMessage.setData(bundle);
                this.b.sendMessage(obtainMessage);
                return;
            }
        }
    }
}
