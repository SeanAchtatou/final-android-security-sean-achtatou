package com.tencent.connect.auth;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.webkit.CookieSyncManager;
import android.widget.Toast;
import com.tencent.connect.a.a;
import com.tencent.connect.common.BaseApi;
import com.tencent.connect.common.Constants;
import com.tencent.open.a.n;
import com.tencent.open.utils.ApkExternalInfoTool;
import com.tencent.open.utils.Global;
import com.tencent.tauth.IUiListener;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;

/* compiled from: ProGuard */
public class QQAuth {

    /* renamed from: a  reason: collision with root package name */
    private AuthAgent f3496a = new AuthAgent(this.b);
    private QQToken b;

    private QQAuth(String str, Context context) {
        n.c(n.d, "new QQAuth() --start");
        this.b = new QQToken(str);
        a.c(context, this.b);
        n.c(n.d, "new QQAuth() --end");
    }

    public static QQAuth createInstance(String str, Context context) {
        Global.setContext(context.getApplicationContext());
        n.c(n.d, "QQAuth -- createInstance() --start");
        try {
            PackageManager packageManager = context.getPackageManager();
            packageManager.getActivityInfo(new ComponentName(context.getPackageName(), "com.tencent.tauth.AuthActivity"), 0);
            packageManager.getActivityInfo(new ComponentName(context.getPackageName(), "com.tencent.connect.common.AssistActivity"), 0);
            QQAuth qQAuth = new QQAuth(str, context);
            n.c(n.d, "QQAuth -- createInstance()  --end");
            return qQAuth;
        } catch (PackageManager.NameNotFoundException e) {
            n.b(n.d, "createInstance() error --end", e);
            Toast.makeText(context.getApplicationContext(), "请参照文档在Androidmanifest.xml加上AuthActivity和AssitActivity的定义 ", 1).show();
            return null;
        }
    }

    public int login(Activity activity, String str, IUiListener iUiListener) {
        n.c(n.d, "login()");
        return login(activity, str, iUiListener, Constants.STR_EMPTY);
    }

    public int login(Activity activity, String str, IUiListener iUiListener, String str2) {
        n.c(n.d, "-->login activity: " + activity);
        return a(activity, null, str, iUiListener, str2);
    }

    public int login(Fragment fragment, String str, IUiListener iUiListener, String str2) {
        FragmentActivity c = fragment.c();
        n.c(n.d, "-->login activity: " + c);
        return a(c, fragment, str, iUiListener, str2);
    }

    private int a(Activity activity, Fragment fragment, String str, IUiListener iUiListener, String str2) {
        String str3;
        String packageName = activity.getApplicationContext().getPackageName();
        Iterator<ApplicationInfo> it = activity.getPackageManager().getInstalledApplications(128).iterator();
        while (true) {
            if (!it.hasNext()) {
                str3 = null;
                break;
            }
            ApplicationInfo next = it.next();
            if (packageName.equals(next.packageName)) {
                str3 = next.sourceDir;
                break;
            }
        }
        if (str3 != null) {
            try {
                String readChannelId = ApkExternalInfoTool.readChannelId(new File(str3));
                if (!TextUtils.isEmpty(readChannelId)) {
                    n.b(n.d, "-->login channelId: " + readChannelId);
                    return loginWithOEM(activity, str, iUiListener, readChannelId, readChannelId, Constants.STR_EMPTY);
                }
            } catch (IOException e) {
                n.b(n.d, "-->login get channel id exception." + e.getMessage());
                e.printStackTrace();
            }
        }
        n.b(n.d, "-->login channelId is null ");
        BaseApi.isOEM = false;
        return this.f3496a.doLogin(activity, str, iUiListener, false, fragment);
    }

    @Deprecated
    public int loginWithOEM(Activity activity, String str, IUiListener iUiListener, String str2, String str3, String str4) {
        n.c(n.d, "loginWithOEM");
        BaseApi.isOEM = true;
        if (str2.equals(Constants.STR_EMPTY)) {
            str2 = "null";
        }
        if (str3.equals(Constants.STR_EMPTY)) {
            str3 = "null";
        }
        if (str4.equals(Constants.STR_EMPTY)) {
            str4 = "null";
        }
        BaseApi.installChannel = str3;
        BaseApi.registerChannel = str2;
        BaseApi.businessId = str4;
        return this.f3496a.doLogin(activity, str, iUiListener);
    }

    public int reAuth(Activity activity, String str, IUiListener iUiListener) {
        n.c(n.d, "reAuth()");
        return this.f3496a.doLogin(activity, str, iUiListener, true, null);
    }

    public void reportDAU() {
        this.f3496a.a((IUiListener) null);
    }

    public void checkLogin(IUiListener iUiListener) {
        this.f3496a.b(iUiListener);
    }

    public void logout(Context context) {
        n.c(n.d, "logout() --start");
        CookieSyncManager.createInstance(context);
        setAccessToken(null, null);
        setOpenId(context, null);
        n.c(n.d, "logout() --end");
    }

    public QQToken getQQToken() {
        return this.b;
    }

    public void setAccessToken(String str, String str2) {
        n.a(n.d, "setAccessToken(), validTimeInSecond = " + str2 + Constants.STR_EMPTY);
        this.b.setAccessToken(str, str2);
    }

    public boolean isSessionValid() {
        n.a(n.d, "isSessionValid(), result = " + (this.b.isSessionValid() ? "true" : "false") + Constants.STR_EMPTY);
        return this.b.isSessionValid();
    }

    public void setOpenId(Context context, String str) {
        n.a(n.d, "setOpenId() --start");
        this.b.setOpenId(str);
        a.d(context, this.b);
        n.a(n.d, "setOpenId() --end");
    }

    public boolean onActivityResult(int i, int i2, Intent intent) {
        n.c(n.d, "onActivityResult() ,resultCode = " + i2 + Constants.STR_EMPTY);
        return true;
    }
}
