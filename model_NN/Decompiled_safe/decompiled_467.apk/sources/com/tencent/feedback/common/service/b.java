package com.tencent.feedback.common.service;

import android.content.Intent;
import android.os.Parcelable;
import com.tencent.feedback.common.g;
import com.tencent.feedback.proguard.C0005b;
import com.tencent.feedback.proguard.C0008j;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/* compiled from: ProGuard */
public class b {

    /* renamed from: a  reason: collision with root package name */
    private StringBuilder f3689a;
    private int b = 0;

    private void a(String str) {
        for (int i = 0; i < this.b; i++) {
            this.f3689a.append(9);
        }
        if (str != null) {
            this.f3689a.append(str).append(": ");
        }
    }

    public b(StringBuilder sb, int i) {
        this.f3689a = sb;
        this.b = i;
    }

    protected static a a(Intent intent) {
        boolean equals;
        boolean z = false;
        if (intent == null) {
            return null;
        }
        Parcelable parcelableExtra = intent.getParcelableExtra("com.tencent.feedback.104");
        if (parcelableExtra == null && !a.class.isInstance(parcelableExtra)) {
            return null;
        }
        a cast = a.class.cast(parcelableExtra);
        if (cast != null) {
            switch (cast.b()) {
                case 1000:
                    equals = "2000".equals(cast.a());
                    break;
                default:
                    equals = false;
                    break;
            }
            if (!equals) {
                g.d("verify task fail %s %d", cast.a(), Integer.valueOf(cast.b()));
            }
            z = equals;
        }
        if (!z) {
            return null;
        }
        return cast;
    }

    public b a(boolean z, String str) {
        a(str);
        this.f3689a.append(z ? 'T' : 'F').append(10);
        return this;
    }

    public b a(byte b2, String str) {
        a(str);
        this.f3689a.append((int) b2).append(10);
        return this;
    }

    public b a(char c, String str) {
        a(str);
        this.f3689a.append(c).append(10);
        return this;
    }

    public b a(short s, String str) {
        a(str);
        this.f3689a.append((int) s).append(10);
        return this;
    }

    public b a(int i, String str) {
        a(str);
        this.f3689a.append(i).append(10);
        return this;
    }

    public b a(long j, String str) {
        a(str);
        this.f3689a.append(j).append(10);
        return this;
    }

    public b a(float f, String str) {
        a(str);
        this.f3689a.append(f).append(10);
        return this;
    }

    public b a(double d, String str) {
        a(str);
        this.f3689a.append(d).append(10);
        return this;
    }

    public b a(String str, String str2) {
        a(str2);
        if (str == null) {
            this.f3689a.append("null\n");
        } else {
            this.f3689a.append(str).append(10);
        }
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.common.service.b.a(char, java.lang.String):com.tencent.feedback.common.service.b
     arg types: [int, ?[OBJECT, ARRAY]]
     candidates:
      com.tencent.feedback.common.service.b.a(byte, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(double, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(float, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(int, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(long, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(com.tencent.feedback.proguard.j, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.lang.Object, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.lang.String, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.util.Collection, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.util.Map, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(short, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(boolean, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(byte[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(double[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(float[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(int[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(long[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.lang.Object[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(short[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(char, java.lang.String):com.tencent.feedback.common.service.b */
    public b a(byte[] bArr, String str) {
        a(str);
        if (bArr == null) {
            this.f3689a.append("null\n");
        } else if (bArr.length == 0) {
            this.f3689a.append(bArr.length).append(", []\n");
        } else {
            this.f3689a.append(bArr.length).append(", [\n");
            b bVar = new b(this.f3689a, this.b + 1);
            for (byte a2 : bArr) {
                bVar.a(a2, (String) null);
            }
            a(']', (String) null);
        }
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.common.service.b.a(char, java.lang.String):com.tencent.feedback.common.service.b
     arg types: [int, ?[OBJECT, ARRAY]]
     candidates:
      com.tencent.feedback.common.service.b.a(byte, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(double, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(float, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(int, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(long, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(com.tencent.feedback.proguard.j, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.lang.Object, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.lang.String, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.util.Collection, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.util.Map, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(short, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(boolean, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(byte[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(double[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(float[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(int[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(long[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.lang.Object[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(short[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(char, java.lang.String):com.tencent.feedback.common.service.b */
    public b a(short[] sArr, String str) {
        a(str);
        if (sArr == null) {
            this.f3689a.append("null\n");
        } else if (sArr.length == 0) {
            this.f3689a.append(sArr.length).append(", []\n");
        } else {
            this.f3689a.append(sArr.length).append(", [\n");
            b bVar = new b(this.f3689a, this.b + 1);
            for (short a2 : sArr) {
                bVar.a(a2, (String) null);
            }
            a(']', (String) null);
        }
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.common.service.b.a(char, java.lang.String):com.tencent.feedback.common.service.b
     arg types: [int, ?[OBJECT, ARRAY]]
     candidates:
      com.tencent.feedback.common.service.b.a(byte, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(double, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(float, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(int, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(long, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(com.tencent.feedback.proguard.j, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.lang.Object, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.lang.String, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.util.Collection, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.util.Map, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(short, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(boolean, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(byte[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(double[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(float[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(int[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(long[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.lang.Object[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(short[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(char, java.lang.String):com.tencent.feedback.common.service.b */
    public b a(int[] iArr, String str) {
        a(str);
        if (iArr == null) {
            this.f3689a.append("null\n");
        } else if (iArr.length == 0) {
            this.f3689a.append(iArr.length).append(", []\n");
        } else {
            this.f3689a.append(iArr.length).append(", [\n");
            b bVar = new b(this.f3689a, this.b + 1);
            for (int a2 : iArr) {
                bVar.a(a2, (String) null);
            }
            a(']', (String) null);
        }
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.common.service.b.a(char, java.lang.String):com.tencent.feedback.common.service.b
     arg types: [int, ?[OBJECT, ARRAY]]
     candidates:
      com.tencent.feedback.common.service.b.a(byte, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(double, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(float, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(int, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(long, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(com.tencent.feedback.proguard.j, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.lang.Object, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.lang.String, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.util.Collection, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.util.Map, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(short, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(boolean, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(byte[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(double[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(float[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(int[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(long[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.lang.Object[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(short[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(char, java.lang.String):com.tencent.feedback.common.service.b */
    public b a(long[] jArr, String str) {
        a(str);
        if (jArr == null) {
            this.f3689a.append("null\n");
        } else if (jArr.length == 0) {
            this.f3689a.append(jArr.length).append(", []\n");
        } else {
            this.f3689a.append(jArr.length).append(", [\n");
            b bVar = new b(this.f3689a, this.b + 1);
            for (long a2 : jArr) {
                bVar.a(a2, (String) null);
            }
            a(']', (String) null);
        }
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.common.service.b.a(char, java.lang.String):com.tencent.feedback.common.service.b
     arg types: [int, ?[OBJECT, ARRAY]]
     candidates:
      com.tencent.feedback.common.service.b.a(byte, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(double, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(float, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(int, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(long, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(com.tencent.feedback.proguard.j, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.lang.Object, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.lang.String, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.util.Collection, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.util.Map, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(short, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(boolean, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(byte[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(double[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(float[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(int[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(long[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.lang.Object[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(short[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(char, java.lang.String):com.tencent.feedback.common.service.b */
    public b a(float[] fArr, String str) {
        a(str);
        if (fArr == null) {
            this.f3689a.append("null\n");
        } else if (fArr.length == 0) {
            this.f3689a.append(fArr.length).append(", []\n");
        } else {
            this.f3689a.append(fArr.length).append(", [\n");
            b bVar = new b(this.f3689a, this.b + 1);
            for (float a2 : fArr) {
                bVar.a(a2, (String) null);
            }
            a(']', (String) null);
        }
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.common.service.b.a(char, java.lang.String):com.tencent.feedback.common.service.b
     arg types: [int, ?[OBJECT, ARRAY]]
     candidates:
      com.tencent.feedback.common.service.b.a(byte, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(double, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(float, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(int, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(long, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(com.tencent.feedback.proguard.j, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.lang.Object, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.lang.String, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.util.Collection, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.util.Map, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(short, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(boolean, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(byte[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(double[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(float[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(int[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(long[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.lang.Object[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(short[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(char, java.lang.String):com.tencent.feedback.common.service.b */
    public b a(double[] dArr, String str) {
        a(str);
        if (dArr == null) {
            this.f3689a.append("null\n");
        } else if (dArr.length == 0) {
            this.f3689a.append(dArr.length).append(", []\n");
        } else {
            this.f3689a.append(dArr.length).append(", [\n");
            b bVar = new b(this.f3689a, this.b + 1);
            for (double a2 : dArr) {
                bVar.a(a2, (String) null);
            }
            a(']', (String) null);
        }
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.common.service.b.a(char, java.lang.String):com.tencent.feedback.common.service.b
     arg types: [int, ?[OBJECT, ARRAY]]
     candidates:
      com.tencent.feedback.common.service.b.a(byte, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(double, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(float, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(int, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(long, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(com.tencent.feedback.proguard.j, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.lang.Object, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.lang.String, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.util.Collection, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.util.Map, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(short, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(boolean, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(byte[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(double[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(float[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(int[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(long[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.lang.Object[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(short[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(char, java.lang.String):com.tencent.feedback.common.service.b */
    public <K, V> b a(Map<K, V> map, String str) {
        a(str);
        if (map == null) {
            this.f3689a.append("null\n");
        } else if (map.isEmpty()) {
            this.f3689a.append(map.size()).append(", {}\n");
        } else {
            this.f3689a.append(map.size()).append(", {\n");
            b bVar = new b(this.f3689a, this.b + 1);
            b bVar2 = new b(this.f3689a, this.b + 2);
            for (Map.Entry next : map.entrySet()) {
                bVar.a('(', (String) null);
                bVar2.a(next.getKey(), (String) null);
                bVar2.a(next.getValue(), (String) null);
                bVar.a(')', (String) null);
            }
            a('}', (String) null);
        }
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.common.service.b.a(java.lang.Object, java.lang.String):com.tencent.feedback.common.service.b
     arg types: [T, ?[OBJECT, ARRAY]]
     candidates:
      com.tencent.feedback.common.service.b.a(byte, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(char, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(double, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(float, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(int, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(long, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(com.tencent.feedback.proguard.j, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.lang.String, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.util.Collection, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.util.Map, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(short, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(boolean, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(byte[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(double[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(float[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(int[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(long[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.lang.Object[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(short[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.lang.Object, java.lang.String):com.tencent.feedback.common.service.b */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.common.service.b.a(char, java.lang.String):com.tencent.feedback.common.service.b
     arg types: [int, ?[OBJECT, ARRAY]]
     candidates:
      com.tencent.feedback.common.service.b.a(byte, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(double, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(float, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(int, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(long, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(com.tencent.feedback.proguard.j, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.lang.Object, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.lang.String, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.util.Collection, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.util.Map, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(short, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(boolean, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(byte[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(double[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(float[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(int[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(long[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.lang.Object[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(short[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(char, java.lang.String):com.tencent.feedback.common.service.b */
    public <T> b a(T[] tArr, String str) {
        a(str);
        if (tArr == null) {
            this.f3689a.append("null\n");
        } else if (tArr.length == 0) {
            this.f3689a.append(tArr.length).append(", []\n");
        } else {
            this.f3689a.append(tArr.length).append(", [\n");
            b bVar = new b(this.f3689a, this.b + 1);
            for (T t : tArr) {
                bVar.a((Object) t, (String) null);
            }
            a(']', (String) null);
        }
        return this;
    }

    public <T> b a(Collection<T> collection, String str) {
        if (collection != null) {
            return a(collection.toArray(), str);
        }
        a(str);
        this.f3689a.append("null\t");
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.common.service.b.a(java.util.Collection, java.lang.String):com.tencent.feedback.common.service.b
     arg types: [java.util.List, java.lang.String]
     candidates:
      com.tencent.feedback.common.service.b.a(byte, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(char, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(double, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(float, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(int, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(long, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(com.tencent.feedback.proguard.j, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.lang.Object, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.lang.String, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.util.Map, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(short, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(boolean, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(byte[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(double[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(float[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(int[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(long[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.lang.Object[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(short[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.util.Collection, java.lang.String):com.tencent.feedback.common.service.b */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.common.service.b.a(java.lang.Object, java.lang.String):com.tencent.feedback.common.service.b
     arg types: [boolean[], java.lang.String]
     candidates:
      com.tencent.feedback.common.service.b.a(byte, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(char, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(double, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(float, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(int, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(long, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(com.tencent.feedback.proguard.j, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.lang.String, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.util.Collection, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.util.Map, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(short, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(boolean, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(byte[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(double[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(float[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(int[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(long[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.lang.Object[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(short[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.lang.Object, java.lang.String):com.tencent.feedback.common.service.b */
    public <T> b a(T t, String str) {
        if (t == null) {
            this.f3689a.append("null\n");
        } else if (t instanceof Byte) {
            a(((Byte) t).byteValue(), str);
        } else if (t instanceof Boolean) {
            a(((Boolean) t).booleanValue(), str);
        } else if (t instanceof Short) {
            a(((Short) t).shortValue(), str);
        } else if (t instanceof Integer) {
            a(((Integer) t).intValue(), str);
        } else if (t instanceof Long) {
            a(((Long) t).longValue(), str);
        } else if (t instanceof Float) {
            a(((Float) t).floatValue(), str);
        } else if (t instanceof Double) {
            a(((Double) t).doubleValue(), str);
        } else if (t instanceof String) {
            a((String) t, str);
        } else if (t instanceof Map) {
            a((Map) t, str);
        } else if (t instanceof List) {
            a((Collection) ((List) t), str);
        } else if (t instanceof C0008j) {
            a((C0008j) t, str);
        } else if (t instanceof byte[]) {
            a((byte[]) t, str);
        } else if (t instanceof boolean[]) {
            a((Object) ((boolean[]) t), str);
        } else if (t instanceof short[]) {
            a((short[]) t, str);
        } else if (t instanceof int[]) {
            a((int[]) t, str);
        } else if (t instanceof long[]) {
            a((long[]) t, str);
        } else if (t instanceof float[]) {
            a((float[]) t, str);
        } else if (t instanceof double[]) {
            a((double[]) t, str);
        } else if (t.getClass().isArray()) {
            a((Object[]) t, str);
        } else {
            throw new C0005b("write object error: unsupport type.");
        }
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.common.service.b.a(char, java.lang.String):com.tencent.feedback.common.service.b
     arg types: [int, java.lang.String]
     candidates:
      com.tencent.feedback.common.service.b.a(byte, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(double, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(float, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(int, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(long, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(com.tencent.feedback.proguard.j, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.lang.Object, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.lang.String, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.util.Collection, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.util.Map, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(short, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(boolean, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(byte[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(double[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(float[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(int[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(long[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.lang.Object[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(short[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(char, java.lang.String):com.tencent.feedback.common.service.b */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.common.service.b.a(char, java.lang.String):com.tencent.feedback.common.service.b
     arg types: [int, ?[OBJECT, ARRAY]]
     candidates:
      com.tencent.feedback.common.service.b.a(byte, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(double, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(float, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(int, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(long, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(com.tencent.feedback.proguard.j, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.lang.Object, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.lang.String, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.util.Collection, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.util.Map, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(short, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(boolean, java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(byte[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(double[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(float[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(int[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(long[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(java.lang.Object[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(short[], java.lang.String):com.tencent.feedback.common.service.b
      com.tencent.feedback.common.service.b.a(char, java.lang.String):com.tencent.feedback.common.service.b */
    public b a(C0008j jVar, String str) {
        a('{', str);
        if (jVar == null) {
            this.f3689a.append(9).append("null");
        } else {
            jVar.a(this.f3689a, this.b + 1);
        }
        a('}', (String) null);
        return this;
    }
}
