package com.tencent.feedback.common;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import java.util.Locale;

/* compiled from: ProGuard */
public final class f {

    /* renamed from: a  reason: collision with root package name */
    private static f f3680a;

    public static synchronized f a(Context context) {
        f fVar;
        synchronized (f.class) {
            if (f3680a == null && context != null) {
                f3680a = new f(context.getApplicationContext());
            }
            fVar = f3680a;
        }
        return fVar;
    }

    private f(Context context) {
        context.getApplicationContext();
    }

    public static String a() {
        try {
            return Build.MODEL;
        } catch (Throwable th) {
            g.d("rqdp{  getDeviceName error}", new Object[0]);
            th.printStackTrace();
            return "fail";
        }
    }

    public static String b() {
        try {
            return Build.VERSION.RELEASE;
        } catch (Throwable th) {
            g.d("rqdp{  getVersion error}", new Object[0]);
            th.printStackTrace();
            return "fail";
        }
    }

    public static String c() {
        try {
            return Build.VERSION.SDK;
        } catch (Throwable th) {
            g.d("rqdp{  getApiLevel error}", new Object[0]);
            th.printStackTrace();
            return "fail";
        }
    }

    public static String b(Context context) {
        String str;
        Throwable th;
        String str2 = "fail";
        if (context == null) {
            g.d("rqdp{  getImei but context == null!}", new Object[0]);
            return str2;
        }
        try {
            str2 = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
            if (str2 == null) {
                str = "null";
            } else {
                str = str2.toLowerCase();
            }
            try {
                g.a("rqdp{  IMEI:}" + str, new Object[0]);
                return str;
            } catch (Throwable th2) {
                th = th2;
            }
        } catch (Throwable th3) {
            Throwable th4 = th3;
            str = str2;
            th = th4;
        }
        g.d("rqdp{  getImei error!}", new Object[0]);
        th.printStackTrace();
        return str;
    }

    public static String c(Context context) {
        if (context == null) {
            g.d("rqdp{  getImsi but context == null!}", new Object[0]);
            return "fail";
        }
        try {
            String subscriberId = ((TelephonyManager) context.getSystemService("phone")).getSubscriberId();
            if (subscriberId == null) {
                return "null";
            }
            return subscriberId.toLowerCase();
        } catch (Throwable th) {
            Throwable th2 = th;
            String str = "fail";
            g.d("rqdp{  getImsi error!}", new Object[0]);
            th2.printStackTrace();
            return str;
        }
    }

    public static String d(Context context) {
        String str = "fail";
        if (context == null) {
            g.d("rqdp{  getAndroidId but context == null!}", new Object[0]);
            return str;
        }
        try {
            String string = Settings.Secure.getString(context.getContentResolver(), "android_id");
            if (string == null) {
                return "null";
            }
            try {
                return string.toLowerCase();
            } catch (Throwable th) {
                Throwable th2 = th;
                str = string;
                th = th2;
            }
        } catch (Throwable th3) {
            th = th3;
        }
        g.d("rqdp{  getAndroidId error!}", new Object[0]);
        th.printStackTrace();
        return str;
    }

    public static String e(Context context) {
        if (context == null) {
            g.d("rqdp{  getMacAddress but context == null!}", new Object[0]);
            return "fail";
        }
        try {
            String macAddress = ((WifiManager) context.getSystemService("wifi")).getConnectionInfo().getMacAddress();
            if (macAddress == null) {
                return "null";
            }
            return macAddress.toLowerCase();
        } catch (Throwable th) {
            Throwable th2 = th;
            String str = "fail";
            th2.printStackTrace();
            g.d("rqdp{  getMacAddress error!}", new Object[0]);
            return str;
        }
    }

    public static String d() {
        try {
            return System.getProperty("os.arch");
        } catch (Throwable th) {
            g.c("rqdp{  ge cuabi fa!}", new Object[0]);
            th.printStackTrace();
            return "fail";
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x0068 A[SYNTHETIC, Splitter:B:23:0x0068] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0076 A[SYNTHETIC, Splitter:B:31:0x0076] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] e() {
        /*
            r1 = 0
            java.io.File r0 = new java.io.File     // Catch:{ Throwable -> 0x0059, all -> 0x0072 }
            java.lang.String r2 = "/proc/cpuinfo"
            r0.<init>(r2)     // Catch:{ Throwable -> 0x0059, all -> 0x0072 }
            boolean r2 = r0.exists()     // Catch:{ Throwable -> 0x0059, all -> 0x0072 }
            if (r2 == 0) goto L_0x006b
            boolean r2 = r0.canRead()     // Catch:{ Throwable -> 0x0059, all -> 0x0072 }
            if (r2 == 0) goto L_0x006b
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ Throwable -> 0x0059, all -> 0x0072 }
            r2.<init>(r0)     // Catch:{ Throwable -> 0x0059, all -> 0x0072 }
            java.io.ByteArrayOutputStream r0 = new java.io.ByteArrayOutputStream     // Catch:{ Throwable -> 0x0081 }
            r0.<init>()     // Catch:{ Throwable -> 0x0081 }
            r3 = 1000(0x3e8, float:1.401E-42)
            byte[] r5 = new byte[r3]     // Catch:{ Throwable -> 0x0081 }
            r3 = 0
        L_0x0024:
            int r6 = r2.read(r5)     // Catch:{ Throwable -> 0x0081 }
            if (r6 <= 0) goto L_0x0034
            r7 = 0
            r0.write(r5, r7, r6)     // Catch:{ Throwable -> 0x0081 }
            r0.flush()     // Catch:{ Throwable -> 0x0081 }
            long r6 = (long) r6     // Catch:{ Throwable -> 0x0081 }
            long r3 = r3 + r6
            goto L_0x0024
        L_0x0034:
            byte[] r0 = r0.toByteArray()     // Catch:{ Throwable -> 0x0081 }
            int r5 = r0.length     // Catch:{ Throwable -> 0x0081 }
            long r5 = (long) r5     // Catch:{ Throwable -> 0x0081 }
            java.lang.String r7 = "cpuInfo read:%d write:%d"
            r8 = 2
            java.lang.Object[] r8 = new java.lang.Object[r8]     // Catch:{ Throwable -> 0x0081 }
            r9 = 0
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ Throwable -> 0x0081 }
            r8[r9] = r3     // Catch:{ Throwable -> 0x0081 }
            r3 = 1
            java.lang.Long r4 = java.lang.Long.valueOf(r5)     // Catch:{ Throwable -> 0x0081 }
            r8[r3] = r4     // Catch:{ Throwable -> 0x0081 }
            com.tencent.feedback.common.g.a(r7, r8)     // Catch:{ Throwable -> 0x0081 }
            r2.close()     // Catch:{ IOException -> 0x0054 }
        L_0x0053:
            return r0
        L_0x0054:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0053
        L_0x0059:
            r0 = move-exception
            r2 = r1
        L_0x005b:
            java.lang.String r3 = "rqdp{  ge cuabi fa!}"
            r4 = 0
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x007f }
            com.tencent.feedback.common.g.c(r3, r4)     // Catch:{ all -> 0x007f }
            r0.printStackTrace()     // Catch:{ all -> 0x007f }
            if (r2 == 0) goto L_0x006b
            r2.close()     // Catch:{ IOException -> 0x006d }
        L_0x006b:
            r0 = r1
            goto L_0x0053
        L_0x006d:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x006b
        L_0x0072:
            r0 = move-exception
            r2 = r1
        L_0x0074:
            if (r2 == 0) goto L_0x0079
            r2.close()     // Catch:{ IOException -> 0x007a }
        L_0x0079:
            throw r0
        L_0x007a:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0079
        L_0x007f:
            r0 = move-exception
            goto L_0x0074
        L_0x0081:
            r0 = move-exception
            goto L_0x005b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.feedback.common.f.e():byte[]");
    }

    public static long f() {
        try {
            StatFs statFs = new StatFs(Environment.getDataDirectory().getPath());
            return ((long) statFs.getBlockCount()) * ((long) statFs.getBlockSize());
        } catch (Throwable th) {
            g.d("rqdp{  getDisplayMetrics error!}", new Object[0]);
            th.printStackTrace();
            return -1;
        }
    }

    public static long g() {
        try {
            StatFs statFs = new StatFs(Environment.getDataDirectory().getPath());
            return ((long) statFs.getAvailableBlocks()) * ((long) statFs.getBlockSize());
        } catch (Throwable th) {
            g.d("rqdp{  getDisplayMetrics error!}", new Object[0]);
            th.printStackTrace();
            return -1;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0054 A[SYNTHETIC, Splitter:B:22:0x0054] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0059 A[SYNTHETIC, Splitter:B:25:0x0059] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x006e A[SYNTHETIC, Splitter:B:35:0x006e] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0073 A[SYNTHETIC, Splitter:B:38:0x0073] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static long h() {
        /*
            r1 = 0
            java.lang.String r0 = "/proc/meminfo"
            java.io.FileReader r3 = new java.io.FileReader     // Catch:{ Throwable -> 0x0045, all -> 0x0069 }
            r3.<init>(r0)     // Catch:{ Throwable -> 0x0045, all -> 0x0069 }
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ Throwable -> 0x008a, all -> 0x0081 }
            r0 = 2048(0x800, float:2.87E-42)
            r2.<init>(r3, r0)     // Catch:{ Throwable -> 0x008a, all -> 0x0081 }
            java.lang.String r0 = r2.readLine()     // Catch:{ Throwable -> 0x008d, all -> 0x0084 }
            java.lang.String r1 = ":\\s+"
            r4 = 2
            java.lang.String[] r0 = r0.split(r1, r4)     // Catch:{ Throwable -> 0x008d, all -> 0x0084 }
            r1 = 1
            r0 = r0[r1]     // Catch:{ Throwable -> 0x008d, all -> 0x0084 }
            java.lang.String r0 = r0.toLowerCase()     // Catch:{ Throwable -> 0x008d, all -> 0x0084 }
            java.lang.String r1 = "kb"
            java.lang.String r4 = ""
            java.lang.String r0 = r0.replace(r1, r4)     // Catch:{ Throwable -> 0x008d, all -> 0x0084 }
            java.lang.String r0 = r0.trim()     // Catch:{ Throwable -> 0x008d, all -> 0x0084 }
            long r0 = java.lang.Long.parseLong(r0)     // Catch:{ Throwable -> 0x008d, all -> 0x0084 }
            r4 = 1000(0x3e8, double:4.94E-321)
            long r0 = r0 * r4
            r2.close()     // Catch:{ IOException -> 0x003b }
        L_0x0037:
            r3.close()     // Catch:{ IOException -> 0x0040 }
        L_0x003a:
            return r0
        L_0x003b:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0037
        L_0x0040:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x003a
        L_0x0045:
            r0 = move-exception
            r2 = r1
        L_0x0047:
            java.lang.String r3 = "rqdp{  getFreeMem error!}"
            r4 = 0
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x0086 }
            com.tencent.feedback.common.g.d(r3, r4)     // Catch:{ all -> 0x0086 }
            r0.printStackTrace()     // Catch:{ all -> 0x0086 }
            if (r1 == 0) goto L_0x0057
            r1.close()     // Catch:{ IOException -> 0x005f }
        L_0x0057:
            if (r2 == 0) goto L_0x005c
            r2.close()     // Catch:{ IOException -> 0x0064 }
        L_0x005c:
            r0 = -2
            goto L_0x003a
        L_0x005f:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0057
        L_0x0064:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x005c
        L_0x0069:
            r0 = move-exception
            r2 = r1
            r3 = r1
        L_0x006c:
            if (r2 == 0) goto L_0x0071
            r2.close()     // Catch:{ IOException -> 0x0077 }
        L_0x0071:
            if (r3 == 0) goto L_0x0076
            r3.close()     // Catch:{ IOException -> 0x007c }
        L_0x0076:
            throw r0
        L_0x0077:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0071
        L_0x007c:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0076
        L_0x0081:
            r0 = move-exception
            r2 = r1
            goto L_0x006c
        L_0x0084:
            r0 = move-exception
            goto L_0x006c
        L_0x0086:
            r0 = move-exception
            r3 = r2
            r2 = r1
            goto L_0x006c
        L_0x008a:
            r0 = move-exception
            r2 = r3
            goto L_0x0047
        L_0x008d:
            r0 = move-exception
            r1 = r2
            r2 = r3
            goto L_0x0047
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.feedback.common.f.h():long");
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0057 A[SYNTHETIC, Splitter:B:22:0x0057] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x005c A[SYNTHETIC, Splitter:B:25:0x005c] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0071 A[SYNTHETIC, Splitter:B:35:0x0071] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0076 A[SYNTHETIC, Splitter:B:38:0x0076] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static long i() {
        /*
            r1 = 0
            java.lang.String r0 = "/proc/meminfo"
            java.io.FileReader r3 = new java.io.FileReader     // Catch:{ Throwable -> 0x0048, all -> 0x006c }
            r3.<init>(r0)     // Catch:{ Throwable -> 0x0048, all -> 0x006c }
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ Throwable -> 0x008d, all -> 0x0084 }
            r0 = 2048(0x800, float:2.87E-42)
            r2.<init>(r3, r0)     // Catch:{ Throwable -> 0x008d, all -> 0x0084 }
            r2.readLine()     // Catch:{ Throwable -> 0x0090, all -> 0x0087 }
            java.lang.String r0 = r2.readLine()     // Catch:{ Throwable -> 0x0090, all -> 0x0087 }
            java.lang.String r1 = ":\\s+"
            r4 = 2
            java.lang.String[] r0 = r0.split(r1, r4)     // Catch:{ Throwable -> 0x0090, all -> 0x0087 }
            r1 = 1
            r0 = r0[r1]     // Catch:{ Throwable -> 0x0090, all -> 0x0087 }
            java.lang.String r0 = r0.toLowerCase()     // Catch:{ Throwable -> 0x0090, all -> 0x0087 }
            java.lang.String r1 = "kb"
            java.lang.String r4 = ""
            java.lang.String r0 = r0.replace(r1, r4)     // Catch:{ Throwable -> 0x0090, all -> 0x0087 }
            java.lang.String r0 = r0.trim()     // Catch:{ Throwable -> 0x0090, all -> 0x0087 }
            long r0 = java.lang.Long.parseLong(r0)     // Catch:{ Throwable -> 0x0090, all -> 0x0087 }
            r4 = 1000(0x3e8, double:4.94E-321)
            long r0 = r0 * r4
            r2.close()     // Catch:{ IOException -> 0x003e }
        L_0x003a:
            r3.close()     // Catch:{ IOException -> 0x0043 }
        L_0x003d:
            return r0
        L_0x003e:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x003a
        L_0x0043:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x003d
        L_0x0048:
            r0 = move-exception
            r2 = r1
        L_0x004a:
            java.lang.String r3 = "rqdp{  getFreeMem error!}"
            r4 = 0
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x0089 }
            com.tencent.feedback.common.g.d(r3, r4)     // Catch:{ all -> 0x0089 }
            r0.printStackTrace()     // Catch:{ all -> 0x0089 }
            if (r1 == 0) goto L_0x005a
            r1.close()     // Catch:{ IOException -> 0x0062 }
        L_0x005a:
            if (r2 == 0) goto L_0x005f
            r2.close()     // Catch:{ IOException -> 0x0067 }
        L_0x005f:
            r0 = -2
            goto L_0x003d
        L_0x0062:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x005a
        L_0x0067:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x005f
        L_0x006c:
            r0 = move-exception
            r2 = r1
            r3 = r1
        L_0x006f:
            if (r2 == 0) goto L_0x0074
            r2.close()     // Catch:{ IOException -> 0x007a }
        L_0x0074:
            if (r3 == 0) goto L_0x0079
            r3.close()     // Catch:{ IOException -> 0x007f }
        L_0x0079:
            throw r0
        L_0x007a:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0074
        L_0x007f:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0079
        L_0x0084:
            r0 = move-exception
            r2 = r1
            goto L_0x006f
        L_0x0087:
            r0 = move-exception
            goto L_0x006f
        L_0x0089:
            r0 = move-exception
            r3 = r2
            r2 = r1
            goto L_0x006f
        L_0x008d:
            r0 = move-exception
            r2 = r3
            goto L_0x004a
        L_0x0090:
            r0 = move-exception
            r1 = r2
            r2 = r3
            goto L_0x004a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.feedback.common.f.i():long");
    }

    public final long j() {
        if (!(Environment.getExternalStorageState().equals("mounted"))) {
            return 0;
        }
        try {
            StatFs statFs = new StatFs(Environment.getExternalStorageDirectory().getPath());
            return ((long) statFs.getBlockCount()) * ((long) statFs.getBlockSize());
        } catch (Throwable th) {
            g.d("rqdp{  get total sd error %s}", th.toString());
            th.printStackTrace();
            return -2;
        }
    }

    public final long k() {
        if (!(Environment.getExternalStorageState().equals("mounted"))) {
            return 0;
        }
        try {
            StatFs statFs = new StatFs(Environment.getExternalStorageDirectory().getPath());
            return ((long) statFs.getAvailableBlocks()) * ((long) statFs.getBlockSize());
        } catch (Throwable th) {
            g.d("rqdp{  get free sd error %s}", th.toString());
            th.printStackTrace();
            return -2;
        }
    }

    public static String l() {
        try {
            return Locale.getDefault().getCountry();
        } catch (Throwable th) {
            g.d("rqdp{  getCountry error!}", new Object[0]);
            th.printStackTrace();
            return "fail";
        }
    }

    public static String m() {
        try {
            return Build.BRAND;
        } catch (Throwable th) {
            g.d("rqdp{  getBrand error!}", new Object[0]);
            th.printStackTrace();
            return "fail";
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:48:0x00ef A[SYNTHETIC, Splitter:B:48:0x00ef] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00f4 A[SYNTHETIC, Splitter:B:51:0x00f4] */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x0108 A[SYNTHETIC, Splitter:B:61:0x0108] */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x010d A[SYNTHETIC, Splitter:B:64:0x010d] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static long n() {
        /*
            r1 = 0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r2 = "/proc/"
            r0.<init>(r2)
            int r2 = android.os.Process.myPid()
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r2 = "/maps"
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r2 = r0.toString()
            java.lang.Thread r0 = java.lang.Thread.currentThread()     // Catch:{ Throwable -> 0x00e0, all -> 0x0104 }
            java.lang.String r3 = "main"
            java.lang.String r0 = r0.getName()     // Catch:{ Throwable -> 0x00e0, all -> 0x0104 }
            boolean r0 = r3.equals(r0)     // Catch:{ Throwable -> 0x00e0, all -> 0x0104 }
            if (r0 == 0) goto L_0x00a6
            java.lang.String r0 = "[stack]"
        L_0x002c:
            java.lang.String r3 = "stack:%s"
            r4 = 1
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Throwable -> 0x00e0, all -> 0x0104 }
            r5 = 0
            r4[r5] = r0     // Catch:{ Throwable -> 0x00e0, all -> 0x0104 }
            com.tencent.feedback.common.g.b(r3, r4)     // Catch:{ Throwable -> 0x00e0, all -> 0x0104 }
            java.io.FileReader r3 = new java.io.FileReader     // Catch:{ Throwable -> 0x00e0, all -> 0x0104 }
            r3.<init>(r2)     // Catch:{ Throwable -> 0x00e0, all -> 0x0104 }
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ Throwable -> 0x0123, all -> 0x011b }
            r4 = 2048(0x800, float:2.87E-42)
            r2.<init>(r3, r4)     // Catch:{ Throwable -> 0x0123, all -> 0x011b }
        L_0x0043:
            java.lang.String r1 = r2.readLine()     // Catch:{ Throwable -> 0x0126, all -> 0x011d }
            if (r1 == 0) goto L_0x00cd
            boolean r4 = r1.contains(r0)     // Catch:{ Throwable -> 0x0126, all -> 0x011d }
            if (r4 == 0) goto L_0x0043
            java.lang.String r4 = "\\s+"
            java.lang.String[] r1 = r1.split(r4)     // Catch:{ Throwable -> 0x0126, all -> 0x011d }
            int r4 = r1.length     // Catch:{ Throwable -> 0x0126, all -> 0x011d }
            if (r4 <= 0) goto L_0x0043
            r4 = 0
            r4 = r1[r4]     // Catch:{ Throwable -> 0x0126, all -> 0x011d }
            java.lang.String r5 = "-"
            int r4 = r4.indexOf(r5)     // Catch:{ Throwable -> 0x0126, all -> 0x011d }
            if (r4 <= 0) goto L_0x0043
            r5 = 0
            r5 = r1[r5]     // Catch:{ Throwable -> 0x0126, all -> 0x011d }
            int r5 = r5.length()     // Catch:{ Throwable -> 0x0126, all -> 0x011d }
            if (r5 <= r4) goto L_0x0043
            r0 = 0
            r0 = r1[r0]     // Catch:{ Throwable -> 0x0126, all -> 0x011d }
            r5 = 0
            java.lang.String r0 = r0.substring(r5, r4)     // Catch:{ Throwable -> 0x0126, all -> 0x011d }
            r5 = 16
            long r5 = java.lang.Long.parseLong(r0, r5)     // Catch:{ Throwable -> 0x0126, all -> 0x011d }
            r0 = 0
            r0 = r1[r0]     // Catch:{ Throwable -> 0x0126, all -> 0x011d }
            int r1 = r4 + 1
            java.lang.String r0 = r0.substring(r1)     // Catch:{ Throwable -> 0x0126, all -> 0x011d }
            r1 = 16
            long r0 = java.lang.Long.parseLong(r0, r1)     // Catch:{ Throwable -> 0x0126, all -> 0x011d }
            long r0 = r0 - r5
            java.lang.String r4 = "st:%d"
            r5 = 1
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Throwable -> 0x0126, all -> 0x011d }
            r6 = 0
            java.lang.Long r7 = java.lang.Long.valueOf(r0)     // Catch:{ Throwable -> 0x0126, all -> 0x011d }
            r5[r6] = r7     // Catch:{ Throwable -> 0x0126, all -> 0x011d }
            com.tencent.feedback.common.g.b(r4, r5)     // Catch:{ Throwable -> 0x0126, all -> 0x011d }
            r4 = 0
            int r4 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
            if (r4 <= 0) goto L_0x00c1
        L_0x009f:
            r2.close()     // Catch:{ IOException -> 0x00c3 }
        L_0x00a2:
            r3.close()     // Catch:{ IOException -> 0x00c8 }
        L_0x00a5:
            return r0
        L_0x00a6:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00e0, all -> 0x0104 }
            java.lang.String r3 = "[stack:"
            r0.<init>(r3)     // Catch:{ Throwable -> 0x00e0, all -> 0x0104 }
            int r3 = android.os.Process.myTid()     // Catch:{ Throwable -> 0x00e0, all -> 0x0104 }
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Throwable -> 0x00e0, all -> 0x0104 }
            java.lang.String r3 = "]"
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Throwable -> 0x00e0, all -> 0x0104 }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x00e0, all -> 0x0104 }
            goto L_0x002c
        L_0x00c1:
            long r0 = -r0
            goto L_0x009f
        L_0x00c3:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x00a2
        L_0x00c8:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x00a5
        L_0x00cd:
            r2.close()     // Catch:{ IOException -> 0x00d6 }
        L_0x00d0:
            r3.close()     // Catch:{ IOException -> 0x00db }
        L_0x00d3:
            r0 = -1
            goto L_0x00a5
        L_0x00d6:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00d0
        L_0x00db:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00d3
        L_0x00e0:
            r0 = move-exception
            r2 = r1
        L_0x00e2:
            java.lang.String r3 = "rqdp{  getFreeMem error!}"
            r4 = 0
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x0120 }
            com.tencent.feedback.common.g.d(r3, r4)     // Catch:{ all -> 0x0120 }
            r0.printStackTrace()     // Catch:{ all -> 0x0120 }
            if (r1 == 0) goto L_0x00f2
            r1.close()     // Catch:{ IOException -> 0x00fa }
        L_0x00f2:
            if (r2 == 0) goto L_0x00f7
            r2.close()     // Catch:{ IOException -> 0x00ff }
        L_0x00f7:
            r0 = -2
            goto L_0x00a5
        L_0x00fa:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00f2
        L_0x00ff:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00f7
        L_0x0104:
            r0 = move-exception
            r3 = r1
        L_0x0106:
            if (r1 == 0) goto L_0x010b
            r1.close()     // Catch:{ IOException -> 0x0111 }
        L_0x010b:
            if (r3 == 0) goto L_0x0110
            r3.close()     // Catch:{ IOException -> 0x0116 }
        L_0x0110:
            throw r0
        L_0x0111:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x010b
        L_0x0116:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0110
        L_0x011b:
            r0 = move-exception
            goto L_0x0106
        L_0x011d:
            r0 = move-exception
            r1 = r2
            goto L_0x0106
        L_0x0120:
            r0 = move-exception
            r3 = r2
            goto L_0x0106
        L_0x0123:
            r0 = move-exception
            r2 = r3
            goto L_0x00e2
        L_0x0126:
            r0 = move-exception
            r1 = r2
            r2 = r3
            goto L_0x00e2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.feedback.common.f.n():long");
    }

    /* JADX WARNING: Removed duplicated region for block: B:37:0x00aa A[SYNTHETIC, Splitter:B:37:0x00aa] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00af A[SYNTHETIC, Splitter:B:40:0x00af] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x00c4 A[SYNTHETIC, Splitter:B:50:0x00c4] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x00c9 A[SYNTHETIC, Splitter:B:53:0x00c9] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static long o() {
        /*
            r6 = 0
            r1 = 0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r2 = "/proc/"
            r0.<init>(r2)
            int r2 = android.os.Process.myPid()
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r2 = "/maps"
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
            java.lang.String r8 = "[heap]"
            java.io.FileReader r5 = new java.io.FileReader     // Catch:{ Throwable -> 0x009b, all -> 0x00bf }
            r5.<init>(r0)     // Catch:{ Throwable -> 0x009b, all -> 0x00bf }
            java.io.BufferedReader r4 = new java.io.BufferedReader     // Catch:{ Throwable -> 0x00e0, all -> 0x00d7 }
            r0 = 2048(0x800, float:2.87E-42)
            r4.<init>(r5, r0)     // Catch:{ Throwable -> 0x00e0, all -> 0x00d7 }
            r2 = r6
        L_0x002b:
            java.lang.String r0 = r4.readLine()     // Catch:{ Throwable -> 0x00e3, all -> 0x00da }
            if (r0 == 0) goto L_0x008a
            boolean r1 = r0.contains(r8)     // Catch:{ Throwable -> 0x00e3, all -> 0x00da }
            if (r1 == 0) goto L_0x002b
            java.lang.String r1 = "\\s+"
            java.lang.String[] r0 = r0.split(r1)     // Catch:{ Throwable -> 0x00e3, all -> 0x00da }
            int r1 = r0.length     // Catch:{ Throwable -> 0x00e3, all -> 0x00da }
            if (r1 <= 0) goto L_0x00e7
            r1 = 0
            r1 = r0[r1]     // Catch:{ Throwable -> 0x00e3, all -> 0x00da }
            java.lang.String r9 = "-"
            int r1 = r1.indexOf(r9)     // Catch:{ Throwable -> 0x00e3, all -> 0x00da }
            if (r1 <= 0) goto L_0x00e7
            r9 = 0
            r9 = r0[r9]     // Catch:{ Throwable -> 0x00e3, all -> 0x00da }
            int r9 = r9.length()     // Catch:{ Throwable -> 0x00e3, all -> 0x00da }
            if (r9 <= r1) goto L_0x00e7
            r9 = 0
            r9 = r0[r9]     // Catch:{ Throwable -> 0x00e3, all -> 0x00da }
            r10 = 0
            java.lang.String r9 = r9.substring(r10, r1)     // Catch:{ Throwable -> 0x00e3, all -> 0x00da }
            r10 = 16
            long r9 = java.lang.Long.parseLong(r9, r10)     // Catch:{ Throwable -> 0x00e3, all -> 0x00da }
            r11 = 0
            r0 = r0[r11]     // Catch:{ Throwable -> 0x00e3, all -> 0x00da }
            int r1 = r1 + 1
            java.lang.String r0 = r0.substring(r1)     // Catch:{ Throwable -> 0x00e3, all -> 0x00da }
            r1 = 16
            long r0 = java.lang.Long.parseLong(r0, r1)     // Catch:{ Throwable -> 0x00e3, all -> 0x00da }
            long r0 = r0 - r9
            java.lang.String r9 = "hp:%d"
            r10 = 1
            java.lang.Object[] r10 = new java.lang.Object[r10]     // Catch:{ Throwable -> 0x00e3, all -> 0x00da }
            r11 = 0
            java.lang.Long r12 = java.lang.Long.valueOf(r0)     // Catch:{ Throwable -> 0x00e3, all -> 0x00da }
            r10[r11] = r12     // Catch:{ Throwable -> 0x00e3, all -> 0x00da }
            com.tencent.feedback.common.g.b(r9, r10)     // Catch:{ Throwable -> 0x00e3, all -> 0x00da }
            int r9 = (r0 > r6 ? 1 : (r0 == r6 ? 0 : -1))
            if (r9 <= 0) goto L_0x0088
        L_0x0085:
            long r0 = r0 + r2
        L_0x0086:
            r2 = r0
            goto L_0x002b
        L_0x0088:
            long r0 = -r0
            goto L_0x0085
        L_0x008a:
            r4.close()     // Catch:{ IOException -> 0x0091 }
        L_0x008d:
            r5.close()     // Catch:{ IOException -> 0x0096 }
        L_0x0090:
            return r2
        L_0x0091:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x008d
        L_0x0096:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0090
        L_0x009b:
            r0 = move-exception
            r2 = r1
        L_0x009d:
            java.lang.String r3 = "rqdp{  getFreeMem error!}"
            r4 = 0
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x00dc }
            com.tencent.feedback.common.g.d(r3, r4)     // Catch:{ all -> 0x00dc }
            r0.printStackTrace()     // Catch:{ all -> 0x00dc }
            if (r1 == 0) goto L_0x00ad
            r1.close()     // Catch:{ IOException -> 0x00b5 }
        L_0x00ad:
            if (r2 == 0) goto L_0x00b2
            r2.close()     // Catch:{ IOException -> 0x00ba }
        L_0x00b2:
            r2 = -2
            goto L_0x0090
        L_0x00b5:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00ad
        L_0x00ba:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00b2
        L_0x00bf:
            r0 = move-exception
            r4 = r1
            r5 = r1
        L_0x00c2:
            if (r4 == 0) goto L_0x00c7
            r4.close()     // Catch:{ IOException -> 0x00cd }
        L_0x00c7:
            if (r5 == 0) goto L_0x00cc
            r5.close()     // Catch:{ IOException -> 0x00d2 }
        L_0x00cc:
            throw r0
        L_0x00cd:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00c7
        L_0x00d2:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00cc
        L_0x00d7:
            r0 = move-exception
            r4 = r1
            goto L_0x00c2
        L_0x00da:
            r0 = move-exception
            goto L_0x00c2
        L_0x00dc:
            r0 = move-exception
            r4 = r1
            r5 = r2
            goto L_0x00c2
        L_0x00e0:
            r0 = move-exception
            r2 = r5
            goto L_0x009d
        L_0x00e3:
            r0 = move-exception
            r1 = r4
            r2 = r5
            goto L_0x009d
        L_0x00e7:
            r0 = r2
            goto L_0x0086
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.feedback.common.f.o():long");
    }
}
