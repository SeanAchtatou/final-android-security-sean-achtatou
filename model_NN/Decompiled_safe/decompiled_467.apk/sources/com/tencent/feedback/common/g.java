package com.tencent.feedback.common;

import android.util.Log;
import java.util.Locale;

/* compiled from: ProGuard */
public final class g {

    /* renamed from: a  reason: collision with root package name */
    public static boolean f3681a = false;
    public static boolean b = false;

    public static void a(String str, Object... objArr) {
        if (f3681a) {
            if (str == null) {
                str = "null";
            } else if (!(objArr == null || objArr.length == 0)) {
                str = String.format(Locale.US, str, objArr);
            }
            Log.i("eup", str);
        }
    }

    public static void b(String str, Object... objArr) {
        if (f3681a) {
            if (str == null) {
                str = "null";
            } else if (!(objArr == null || objArr.length == 0)) {
                str = String.format(Locale.US, str, objArr);
            }
            Log.d("eup", str);
        }
    }

    public static void c(String str, Object... objArr) {
        if (f3681a) {
            if (str == null) {
                str = "null";
            } else if (!(objArr == null || objArr.length == 0)) {
                str = String.format(Locale.US, str, objArr);
            }
            Log.w("eup", str);
        }
    }

    public static void d(String str, Object... objArr) {
        if (f3681a) {
            if (str == null) {
                str = "null";
            } else if (!(objArr == null || objArr.length == 0)) {
                str = String.format(Locale.US, str, objArr);
            }
            Log.e("eup", str);
            if (b) {
                throw new RuntimeException(String.format("RQD ERROR CHECK:\n %s \n this error will throw only Constants.Is_AutoCheckOpen == true!\n this throw is for found error early!", str));
            }
        }
    }

    private static void a(String str, String str2, Object... objArr) {
        if (f3681a) {
            if (str2 == null) {
                str2 = "null";
            } else if (!(objArr == null || objArr.length == 0)) {
                str2 = String.format(Locale.US, str2, objArr);
            }
            Log.d(str, str2);
        }
    }

    public static void e(String str, Object... objArr) {
        a("eup_step_api", str, objArr);
    }

    public static void f(String str, Object... objArr) {
        a("eup_step_buffer", str, objArr);
    }

    public static void g(String str, Object... objArr) {
        a("eup_step_db", str, objArr);
    }

    public static void h(String str, Object... objArr) {
        a("eup_step_upload", str, objArr);
    }
}
