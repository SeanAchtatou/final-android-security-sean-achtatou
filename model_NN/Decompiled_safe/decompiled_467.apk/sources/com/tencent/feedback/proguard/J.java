package com.tencent.feedback.proguard;

import com.tencent.connect.common.Constants;
import java.util.HashMap;
import java.util.Map;

public final class J extends C0008j {
    private static Map<String, String> m;

    /* renamed from: a  reason: collision with root package name */
    public String f3710a = Constants.STR_EMPTY;
    public String b = Constants.STR_EMPTY;
    public String c = Constants.STR_EMPTY;
    public String d = Constants.STR_EMPTY;
    public String e = Constants.STR_EMPTY;
    public long f = 0;
    public long g = 0;
    public String h = Constants.STR_EMPTY;
    public String i = Constants.STR_EMPTY;
    public Map<String, String> j = null;
    private String k = Constants.STR_EMPTY;
    private String l = Constants.STR_EMPTY;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ag.a(long, int, boolean):long
     arg types: [long, int, int]
     candidates:
      com.tencent.feedback.proguard.ag.a(double, int, boolean):double
      com.tencent.feedback.proguard.ag.a(java.lang.Object[], int, boolean):T[]
      com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte
      com.tencent.feedback.proguard.ag.a(float, int, boolean):float
      com.tencent.feedback.proguard.ag.a(int, int, boolean):int
      com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j
      com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.feedback.proguard.ag.a(short, int, boolean):short
      com.tencent.feedback.proguard.ag.a(long, int, boolean):long */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object
     arg types: [java.util.Map<java.lang.String, java.lang.String>, int, int]
     candidates:
      com.tencent.feedback.proguard.ag.a(double, int, boolean):double
      com.tencent.feedback.proguard.ag.a(java.lang.Object[], int, boolean):T[]
      com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte
      com.tencent.feedback.proguard.ag.a(float, int, boolean):float
      com.tencent.feedback.proguard.ag.a(int, int, boolean):int
      com.tencent.feedback.proguard.ag.a(long, int, boolean):long
      com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j
      com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.feedback.proguard.ag.a(short, int, boolean):short
      com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object */
    public final void a(ag agVar) {
        this.f3710a = agVar.b(0, true);
        this.b = agVar.b(1, false);
        this.k = agVar.b(2, false);
        this.c = agVar.b(3, false);
        this.d = agVar.b(4, false);
        this.l = agVar.b(5, false);
        this.e = agVar.b(6, false);
        this.f = agVar.a(this.f, 7, false);
        this.g = agVar.a(this.g, 8, false);
        this.h = agVar.b(9, false);
        this.i = agVar.b(10, false);
        if (m == null) {
            m = new HashMap();
            m.put(Constants.STR_EMPTY, Constants.STR_EMPTY);
        }
        this.j = (Map) agVar.a((Object) m, 11, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ah.a(java.util.Map, int):void
     arg types: [java.util.Map<java.lang.String, java.lang.String>, int]
     candidates:
      com.tencent.feedback.proguard.ah.a(byte, int):void
      com.tencent.feedback.proguard.ah.a(float, int):void
      com.tencent.feedback.proguard.ah.a(int, int):void
      com.tencent.feedback.proguard.ah.a(long, int):void
      com.tencent.feedback.proguard.ah.a(com.tencent.feedback.proguard.j, int):void
      com.tencent.feedback.proguard.ah.a(java.lang.Object, int):void
      com.tencent.feedback.proguard.ah.a(java.lang.String, int):void
      com.tencent.feedback.proguard.ah.a(java.util.Collection, int):void
      com.tencent.feedback.proguard.ah.a(short, int):void
      com.tencent.feedback.proguard.ah.a(boolean, int):void
      com.tencent.feedback.proguard.ah.a(byte[], int):void
      com.tencent.feedback.proguard.ah.a(java.util.Map, int):void */
    public final void a(ah ahVar) {
        ahVar.a(this.f3710a, 0);
        if (this.b != null) {
            ahVar.a(this.b, 1);
        }
        if (this.k != null) {
            ahVar.a(this.k, 2);
        }
        if (this.c != null) {
            ahVar.a(this.c, 3);
        }
        if (this.d != null) {
            ahVar.a(this.d, 4);
        }
        if (this.l != null) {
            ahVar.a(this.l, 5);
        }
        if (this.e != null) {
            ahVar.a(this.e, 6);
        }
        ahVar.a(this.f, 7);
        ahVar.a(this.g, 8);
        if (this.h != null) {
            ahVar.a(this.h, 9);
        }
        if (this.i != null) {
            ahVar.a(this.i, 10);
        }
        if (this.j != null) {
            ahVar.a((Map) this.j, 11);
        }
    }

    public final void a(StringBuilder sb, int i2) {
    }
}
