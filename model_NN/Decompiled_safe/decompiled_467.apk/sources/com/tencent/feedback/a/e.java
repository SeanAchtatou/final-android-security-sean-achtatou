package com.tencent.feedback.a;

import android.content.Context;

/* compiled from: ProGuard */
public abstract class e {

    /* renamed from: a  reason: collision with root package name */
    private static e f3669a = null;

    public abstract byte[] a(String str, byte[] bArr, d dVar);

    public static synchronized e a(Context context) {
        e eVar;
        synchronized (e.class) {
            if (f3669a == null) {
                f3669a = new f(context.getApplicationContext());
            }
            eVar = f3669a;
        }
        return eVar;
    }
}
