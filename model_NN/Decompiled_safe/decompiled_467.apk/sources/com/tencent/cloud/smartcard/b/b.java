package com.tencent.cloud.smartcard.b;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.a.a;
import com.tencent.assistant.model.a.s;
import com.tencent.assistant.model.a.t;
import com.tencent.assistant.module.u;
import com.tencent.assistant.protocol.jce.SmartCardPicDownloadNode;
import com.tencent.assistant.protocol.jce.SmartCardPicNode;
import com.tencent.assistant.protocol.jce.SmartCardPicTemplate;
import com.tencent.assistant.protocol.jce.SmartCardTitle;
import com.tencent.assistant.utils.XLog;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class b extends a {

    /* renamed from: a  reason: collision with root package name */
    public List<t> f3464a;
    private List<SmartCardPicNode> b;
    private byte c;
    private int d = 100;
    private boolean e = false;
    private int f;
    private int g;
    private boolean h = true;
    private String u;

    public boolean a(byte b2, JceStruct jceStruct) {
        if (!(jceStruct instanceof SmartCardPicTemplate)) {
            return false;
        }
        SmartCardPicTemplate smartCardPicTemplate = (SmartCardPicTemplate) jceStruct;
        SmartCardTitle smartCardTitle = smartCardPicTemplate.b;
        this.i = b2;
        if (smartCardTitle != null) {
            this.c = smartCardTitle.f2339a;
            this.k = smartCardTitle.b;
            this.o = smartCardTitle.c;
            this.n = smartCardTitle.d;
        }
        this.m = smartCardPicTemplate.c;
        this.j = smartCardPicTemplate.f2326a;
        this.b = smartCardPicTemplate.d;
        if (this.f3464a == null) {
            this.f3464a = new ArrayList();
        } else {
            this.f3464a.clear();
        }
        if (smartCardPicTemplate.e != null) {
            Iterator<SmartCardPicDownloadNode> it = smartCardPicTemplate.e.iterator();
            while (it.hasNext()) {
                SmartCardPicDownloadNode next = it.next();
                t tVar = new t();
                tVar.b = next.b;
                tVar.f1653a = u.a(next.f2324a);
                if (tVar.f1653a != null) {
                    u.a(tVar.f1653a);
                }
                this.f3464a.add(tVar);
            }
        }
        if (this.k.contains("疯狂大促全场")) {
            XLog.d("title疯狂大促全场9.9元包邮");
            XLog.d("item size" + this.f3464a.size());
        }
        this.d = smartCardPicTemplate.b();
        this.e = smartCardPicTemplate.a();
        this.f = smartCardPicTemplate.c();
        this.g = smartCardPicTemplate.d();
        this.h = !smartCardPicTemplate.k;
        this.u = smartCardPicTemplate.f;
        return true;
    }

    public s b() {
        if (this.f <= 0 || this.g <= 0) {
            return null;
        }
        s sVar = new s();
        sVar.f = this.j;
        sVar.e = this.i;
        sVar.f1652a = this.f;
        sVar.b = this.g;
        return sVar;
    }

    public List<SimpleAppModel> a() {
        if (this.f3464a == null || this.f3464a.size() <= 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList(this.f3464a.size());
        for (t tVar : this.f3464a) {
            arrayList.add(tVar.f1653a);
        }
        return arrayList;
    }

    public List<SmartCardPicNode> e() {
        return this.b;
    }

    public String d() {
        int i = 0;
        StringBuilder append = new StringBuilder().append(i()).append("_").append(this.b == null ? 0 : this.b.size()).append("_");
        if (this.f3464a != null) {
            i = this.f3464a.size();
        }
        return append.append(i).toString();
    }

    public void c() {
        if (this.h && this.f3464a != null && this.f3464a.size() > 0) {
            Iterator<t> it = this.f3464a.iterator();
            while (it.hasNext()) {
                t next = it.next();
                if (!(next.f1653a == null || ApkResourceManager.getInstance().getInstalledApkInfo(next.f1653a.c) == null)) {
                    it.remove();
                }
            }
        }
    }

    public byte f() {
        return this.c;
    }

    public int g() {
        return this.d;
    }

    public boolean l() {
        return this.e;
    }

    public String m() {
        return this.u;
    }
}
