package com.tencent.cloud.smartcard.view;

import android.content.Context;
import android.text.Html;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.component.smartcard.NormalSmartcardBaseItem;
import com.tencent.assistant.component.smartcard.SmartcardListener;
import com.tencent.assistant.model.a.i;
import com.tencent.assistant.protocol.jce.SmartCardTagInfo;
import com.tencent.assistant.utils.df;
import com.tencent.cloud.smartcard.b.a;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class NormalSmartCardLabelItem extends NormalSmartcardBaseItem {
    private TextView f;
    private LinearLayout i;
    private LinearLayout j;
    private List<Button> k;

    public NormalSmartCardLabelItem(Context context, i iVar, SmartcardListener smartcardListener, IViewInvalidater iViewInvalidater) {
        super(context, iVar, smartcardListener, iViewInvalidater);
        setBackgroundDrawable(null);
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.c = inflate(this.f1115a, R.layout.smartcard_label_frame_layout, this);
        this.f = (TextView) this.c.findViewById(R.id.card_title);
        this.k = new ArrayList(6);
        this.i = (LinearLayout) this.c.findViewById(R.id.label_line_1);
        this.j = (LinearLayout) this.c.findViewById(R.id.label_line_2);
        int a2 = df.a(this.f1115a, 2.5f);
        int a3 = df.a(this.f1115a, 3.0f);
        int a4 = df.a(this.f1115a, 49.0f);
        if (this.smartcardModel instanceof a) {
            for (int i2 = 0; i2 < 6; i2++) {
                Button button = new Button(this.f1115a);
                button.setBackgroundResource(R.drawable.bg_card_selector);
                button.setGravity(17);
                button.setSingleLine(true);
                this.k.add(button);
                button.setTextSize(2, 15.0f);
                button.setSingleLine(true);
                button.setPadding(a3, 0, a3, 0);
                button.setEllipsize(TextUtils.TruncateAt.END);
                button.setTextColor(getResources().getColor(R.color.smartcard_tag_font_bg));
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, a4, 1.0f);
                layoutParams.setMargins(a2, a2, a2, a2);
                if (i2 >= 3) {
                    this.j.addView(button, layoutParams);
                } else {
                    this.i.addView(button, layoutParams);
                }
            }
        }
        f();
    }

    private void f() {
        if (this.smartcardModel instanceof a) {
            a aVar = (a) this.smartcardModel;
            this.f.setText(aVar.k);
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 < 6) {
                    SmartCardTagInfo smartCardTagInfo = aVar.e().b().get(i3);
                    Button button = this.k.get(i3);
                    button.setText(Html.fromHtml(smartCardTagInfo.f2334a));
                    button.setOnClickListener(new a(this, smartCardTagInfo, i3));
                    i2 = i3 + 1;
                } else {
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void b() {
        f();
    }

    /* access modifiers changed from: protected */
    public String b(int i2) {
        if (this.smartcardModel instanceof a) {
            return ((a) this.smartcardModel).e().c();
        }
        return super.b(i2);
    }
}
