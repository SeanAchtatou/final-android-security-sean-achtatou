package com.tencent.cloud.updaterec;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ListView;

/* compiled from: ProGuard */
public class UpdateRecOneMoreListView extends ListView {
    public UpdateRecOneMoreListView(Context context) {
        super(context);
    }

    public UpdateRecOneMoreListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public UpdateRecOneMoreListView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public void onMeasure(int i, int i2) {
        super.onMeasure(i, View.MeasureSpec.makeMeasureSpec(536870911, Integer.MIN_VALUE));
    }
}
