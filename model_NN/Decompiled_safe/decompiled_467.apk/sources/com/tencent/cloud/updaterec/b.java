package com.tencent.cloud.updaterec;

import android.widget.TextView;
import com.tencent.cloud.b.a;

/* compiled from: ProGuard */
class b implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ g f3470a;
    final /* synthetic */ int b;
    final /* synthetic */ TextView c;
    final /* synthetic */ g d;
    final /* synthetic */ a e;

    b(a aVar, g gVar, int i, TextView textView, g gVar2) {
        this.e = aVar;
        this.f3470a = gVar;
        this.b = i;
        this.c = textView;
        this.d = gVar2;
    }

    public void run() {
        Integer num;
        if (this.f3470a != null && (num = (Integer) this.f3470a.f3474a.getTag()) != null && num.intValue() == this.b) {
            if (this.c != null) {
                this.c.setVisibility(0);
            }
            a.a(this.d);
        }
    }
}
