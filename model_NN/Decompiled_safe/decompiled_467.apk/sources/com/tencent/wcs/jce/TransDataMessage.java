package com.tencent.wcs.jce;

import com.qq.taf.jce.JceDisplayer;
import com.qq.taf.jce.JceInputStream;
import com.qq.taf.jce.JceOutputStream;
import com.qq.taf.jce.JceStruct;
import com.qq.taf.jce.JceUtil;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public final class TransDataMessage extends JceStruct implements Cloneable {
    static byte[] j;
    static final /* synthetic */ boolean k = (!TransDataMessage.class.desiredAssertionStatus());

    /* renamed from: a  reason: collision with root package name */
    public int f3888a = 0;
    public String b = Constants.STR_EMPTY;
    public String c = Constants.STR_EMPTY;
    public byte[] d = null;
    public int e = 0;
    public int f = 0;
    public int g = 0;
    public int h = 0;
    public int i = 0;

    public int a() {
        return this.f3888a;
    }

    public void a(int i2) {
        this.f3888a = i2;
    }

    public String b() {
        return this.b;
    }

    public void a(String str) {
        this.b = str;
    }

    public String c() {
        return this.c;
    }

    public void b(String str) {
        this.c = str;
    }

    public byte[] d() {
        return this.d;
    }

    public void a(byte[] bArr) {
        this.d = bArr;
    }

    public int e() {
        return this.e;
    }

    public void b(int i2) {
        this.e = i2;
    }

    public int f() {
        return this.f;
    }

    public void c(int i2) {
        this.f = i2;
    }

    public int g() {
        return this.g;
    }

    public void d(int i2) {
        this.g = i2;
    }

    public void e(int i2) {
        this.h = i2;
    }

    public int h() {
        return this.i;
    }

    public void f(int i2) {
        this.i = i2;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        TransDataMessage transDataMessage = (TransDataMessage) obj;
        if (!JceUtil.equals(this.f3888a, transDataMessage.f3888a) || !JceUtil.equals(this.b, transDataMessage.b) || !JceUtil.equals(this.c, transDataMessage.c) || !JceUtil.equals(this.d, transDataMessage.d) || !JceUtil.equals(this.e, transDataMessage.e) || !JceUtil.equals(this.f, transDataMessage.f) || !JceUtil.equals(this.g, transDataMessage.g) || !JceUtil.equals(this.h, transDataMessage.h) || !JceUtil.equals(this.i, transDataMessage.i)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        try {
            throw new Exception("Need define key first!");
        } catch (Exception e2) {
            e2.printStackTrace();
            return 0;
        }
    }

    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e2) {
            if (k) {
                return null;
            }
            throw new AssertionError();
        }
    }

    public void writeTo(JceOutputStream jceOutputStream) {
        jceOutputStream.write(this.f3888a, 0);
        jceOutputStream.write(this.b, 1);
        jceOutputStream.write(this.c, 2);
        if (this.d != null) {
            jceOutputStream.write(this.d, 3);
        }
        jceOutputStream.write(this.e, 4);
        jceOutputStream.write(this.f, 5);
        jceOutputStream.write(this.g, 6);
        jceOutputStream.write(this.h, 7);
        jceOutputStream.write(this.i, 8);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
     arg types: [byte[], int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[] */
    public void readFrom(JceInputStream jceInputStream) {
        this.f3888a = jceInputStream.read(this.f3888a, 0, true);
        this.b = jceInputStream.readString(1, true);
        this.c = jceInputStream.readString(2, true);
        if (j == null) {
            j = new byte[1];
            j[0] = 0;
        }
        this.d = jceInputStream.read(j, 3, false);
        this.e = jceInputStream.read(this.e, 4, true);
        this.f = jceInputStream.read(this.f, 5, true);
        this.g = jceInputStream.read(this.g, 6, true);
        this.h = jceInputStream.read(this.h, 7, true);
        this.i = jceInputStream.read(this.i, 8, true);
    }

    public void display(StringBuilder sb, int i2) {
        JceDisplayer jceDisplayer = new JceDisplayer(sb, i2);
        jceDisplayer.display(this.f3888a, "nChannelId");
        jceDisplayer.display(this.b, "sSrcDeviceId");
        jceDisplayer.display(this.c, "sDestDeviceId");
        jceDisplayer.display(this.d, "vecMessage");
        jceDisplayer.display(this.e, "nMessageId");
        jceDisplayer.display(this.f, "nSequenceId");
        jceDisplayer.display(this.g, "eType");
        jceDisplayer.display(this.h, "nDataLen");
        jceDisplayer.display(this.i, "nIsCompress");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceDisplayer.displaySimple(int, boolean):com.qq.taf.jce.JceDisplayer
     arg types: [int, int]
     candidates:
      com.qq.taf.jce.JceDisplayer.displaySimple(byte, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(char, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(com.qq.taf.jce.JceStruct, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.String, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Collection, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Map, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(boolean, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(byte[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(char[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int, boolean):com.qq.taf.jce.JceDisplayer */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.String, boolean):com.qq.taf.jce.JceDisplayer
     arg types: [java.lang.String, int]
     candidates:
      com.qq.taf.jce.JceDisplayer.displaySimple(byte, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(char, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(com.qq.taf.jce.JceStruct, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Collection, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Map, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(boolean, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(byte[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(char[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.String, boolean):com.qq.taf.jce.JceDisplayer */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceDisplayer.displaySimple(byte[], boolean):com.qq.taf.jce.JceDisplayer
     arg types: [byte[], int]
     candidates:
      com.qq.taf.jce.JceDisplayer.displaySimple(byte, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(char, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(com.qq.taf.jce.JceStruct, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.String, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Collection, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Map, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(boolean, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(char[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(byte[], boolean):com.qq.taf.jce.JceDisplayer */
    public void displaySimple(StringBuilder sb, int i2) {
        JceDisplayer jceDisplayer = new JceDisplayer(sb, i2);
        jceDisplayer.displaySimple(this.f3888a, true);
        jceDisplayer.displaySimple(this.b, true);
        jceDisplayer.displaySimple(this.c, true);
        jceDisplayer.displaySimple(this.d, true);
        jceDisplayer.displaySimple(this.e, true);
        jceDisplayer.displaySimple(this.f, true);
        jceDisplayer.displaySimple(this.g, true);
        jceDisplayer.displaySimple(this.h, true);
        jceDisplayer.displaySimple(this.i, false);
    }
}
