package com.tencent.wcs.jce;

import java.io.Serializable;

/* compiled from: ProGuard */
public final class enumServiceResultCode implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    public static final enumServiceResultCode f3894a = new enumServiceResultCode(0, 0, "SERVICE_RESULT_CODE_SUCCESS");
    public static final enumServiceResultCode b = new enumServiceResultCode(1, 1, "SERVICE_RESULT_CODE_REMOTE_OFFLINE");
    public static final enumServiceResultCode c = new enumServiceResultCode(2, 2, "SERVICE_RESULT_CODE_OVERLOAD");
    public static final enumServiceResultCode d = new enumServiceResultCode(3, 3, "SERVICE_RESULT_CODE_INVALID_SENDER");
    public static final enumServiceResultCode e = new enumServiceResultCode(4, 4, "SERVICE_RESULT_CODE_INVALID_STATUS");
    public static final enumServiceResultCode f = new enumServiceResultCode(5, 5, "SERVICE_RESULT_CODE_SESSION_NOT_FOUND");
    public static final enumServiceResultCode g = new enumServiceResultCode(6, 6, "SERVICE_RESULT_CODE_PARAM_ERROR");
    public static final enumServiceResultCode h = new enumServiceResultCode(7, 99, "SERVICE_RESULT_CODE_END");
    static final /* synthetic */ boolean i = (!enumServiceResultCode.class.desiredAssertionStatus());
    private static enumServiceResultCode[] j = new enumServiceResultCode[8];
    private int k;
    private String l = new String();

    public String toString() {
        return this.l;
    }

    private enumServiceResultCode(int i2, int i3, String str) {
        this.l = str;
        this.k = i3;
        j[i2] = this;
    }
}
