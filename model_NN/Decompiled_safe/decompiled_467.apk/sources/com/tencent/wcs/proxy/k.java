package com.tencent.wcs.proxy;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.tencent.wcs.b.c;

/* compiled from: ProGuard */
class k implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ j f3922a;

    k(j jVar) {
        this.f3922a = jVar;
    }

    public void run() {
        this.f3922a.f3921a.q();
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.f3922a.f3921a.f3906a.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
            if (activeNetworkInfo.getType() == 1) {
                this.f3922a.f3921a.r();
            } else if (activeNetworkInfo.getType() == 0 && c.m) {
                this.f3922a.f3921a.r();
            }
        }
        boolean unused = this.f3922a.f3921a.k = false;
    }
}
