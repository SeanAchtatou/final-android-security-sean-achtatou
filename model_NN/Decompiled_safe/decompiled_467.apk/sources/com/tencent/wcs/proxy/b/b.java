package com.tencent.wcs.proxy.b;

import com.tencent.assistant.utils.q;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/* compiled from: ProGuard */
public class b {

    /* renamed from: a  reason: collision with root package name */
    private ExecutorService f3898a;

    private b() {
        this.f3898a = new ThreadPoolExecutor(20, 200, 60, TimeUnit.SECONDS, new LinkedBlockingQueue(20), new q("AgentSession"));
    }

    public static b a() {
        return d.f3899a;
    }

    public void a(Runnable runnable) {
        if (this.f3898a != null) {
            this.f3898a.execute(runnable);
        }
    }
}
