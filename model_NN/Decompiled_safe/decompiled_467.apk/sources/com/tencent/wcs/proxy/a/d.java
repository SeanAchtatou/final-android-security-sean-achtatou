package com.tencent.wcs.proxy.a;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;

/* compiled from: ProGuard */
public class d implements c {

    /* renamed from: a  reason: collision with root package name */
    private Context f3896a;
    private WifiManager b;
    private WifiManager.WifiLock c = this.b.createWifiLock(d.class.getName());

    public d(Context context) {
        this.f3896a = context;
        this.b = (WifiManager) context.getSystemService("wifi");
    }

    public boolean a(Context context) {
        ConnectivityManager connectivityManager;
        NetworkInfo activeNetworkInfo;
        if (context == null || (connectivityManager = (ConnectivityManager) context.getSystemService("connectivity")) == null || (activeNetworkInfo = connectivityManager.getActiveNetworkInfo()) == null || !activeNetworkInfo.isAvailable()) {
            return false;
        }
        return activeNetworkInfo.getType() == 1;
    }

    public void a() {
    }

    public void b() {
    }

    public boolean c() {
        return false;
    }
}
