package com.tencent.wcs.agent;

/* compiled from: ProGuard */
class c implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f3828a;
    final /* synthetic */ a b;

    c(a aVar, int i) {
        this.b = aVar;
        this.f3828a = i;
    }

    public void run() {
        h hVar;
        synchronized (this.b.f) {
            hVar = (h) this.b.f.get(this.f3828a);
            this.b.f.remove(this.f3828a);
        }
        if (hVar != null) {
            hVar.b();
            hVar.a((p) null);
        }
    }
}
