package com.tencent.wcs.b;

import android.content.Context;
import android.text.TextUtils;
import com.qq.AppService.AppService;
import com.qq.taf.jce.JceInputStream;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.FileUtil;
import com.tencent.wcs.jce.QueryClientConfigRequest;
import com.tencent.wcs.jce.QueryClientConfigResponse;
import com.tencent.wcs.proxy.c.f;
import com.tencent.wcs.proxy.l;

/* compiled from: ProGuard */
public class b implements com.tencent.wcs.proxy.b {

    /* renamed from: a  reason: collision with root package name */
    private Context f3845a;
    private l b;

    public b(Context context, l lVar) {
        this.f3845a = context;
        this.b = lVar;
    }

    public void a() {
        if (this.b != null) {
            QueryClientConfigRequest queryClientConfigRequest = new QueryClientConfigRequest();
            queryClientConfigRequest.a(AppService.v());
            queryClientConfigRequest.a(0);
            queryClientConfigRequest.b(c.b);
            queryClientConfigRequest.b("129");
            this.b.a((int) STConst.ST_PAGE_COMPETITIVE, queryClientConfigRequest);
        }
    }

    public void b() {
        String a2 = a.a(c());
        if (TextUtils.isEmpty(a2)) {
            a2 = a.a(this.f3845a, "WcsConfig.ini");
            if (!TextUtils.isEmpty(a2)) {
                a.a(a2.getBytes(), c());
            }
        }
        if (!TextUtils.isEmpty(a2)) {
            a.b(a2);
        }
    }

    public void a(f fVar) {
        JceInputStream jceInputStream = new JceInputStream(fVar.c());
        QueryClientConfigResponse queryClientConfigResponse = new QueryClientConfigResponse();
        queryClientConfigResponse.readFrom(jceInputStream);
        byte[] a2 = queryClientConfigResponse.a();
        if (a2 != null && a2.length > 0) {
            a.a(queryClientConfigResponse.a());
            a.a(queryClientConfigResponse.a(), c());
        }
    }

    private String c() {
        return FileUtil.getWcsConfigDir() + "/" + "WcsConfig.ini";
    }
}
