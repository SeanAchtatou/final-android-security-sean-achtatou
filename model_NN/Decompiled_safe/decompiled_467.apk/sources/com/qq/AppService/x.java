package com.qq.AppService;

import android.app.Notification;
import android.os.IInterface;

/* compiled from: ProGuard */
public interface x extends IInterface {
    String A();

    String B();

    int a(int i);

    long a(int[] iArr);

    void a(int i, String str, ParcelObject parcelObject);

    void a(Notification notification, Notification notification2);

    void a(aa aaVar);

    void a(String str, String str2, String str3);

    boolean a();

    boolean a(String str);

    void b(String str);

    void b(String str, String str2, String str3);

    boolean b();

    int c(String str);

    boolean c();

    int d();

    void e();

    int f();

    void g();

    int h();

    void i();

    void j();

    void k();

    void l();

    int m();

    String n();

    String o();

    String p();

    Notification q();

    Notification r();

    boolean s();

    boolean t();

    boolean u();

    boolean v();

    boolean w();

    void x();

    String y();

    int z();
}
