package com.qq.AppService;

import android.os.Process;
import com.tencent.assistant.service.DownloadingService;
import com.tencent.assistant.utils.XLog;
import com.tencent.connector.ipc.a;

/* compiled from: ProGuard */
class n implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AstApp f247a;

    n(AstApp astApp) {
        this.f247a = astApp;
    }

    public void run() {
        XLog.i("KillMe", "kill process in main called.last create time:" + AstApp.w + ",startKill time:" + AstApp.v);
        if (AstApp.w > AstApp.v) {
            DownloadingService.b();
            return;
        }
        a.a().b();
        Process.killProcess(Process.myPid());
    }
}
