package com.qq.AppService;

import android.app.Notification;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.qq.provider.ebooks3.DBHelper;
import com.qq.provider.h;
import com.qq.util.b;
import com.qq.util.n;
import com.tencent.android.qqdownloader.R;
import com.tencent.open.SocialConstants;

/* compiled from: ProGuard */
class ae extends y {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ IPCService f214a;

    ae(IPCService iPCService) {
        this.f214a = iPCService;
    }

    public boolean a() {
        Log.d("com.qq.connect", "isServiceRunning");
        return AppService.c;
    }

    public void e() {
        this.f214a.b = null;
    }

    public int z() {
        return PowerReceiver.b;
    }

    public String y() {
        return PowerReceiver.f207a;
    }

    public boolean a(String str) {
        return true;
    }

    public void k() {
        if (!AppService.d) {
            h.e(0);
        } else {
            WifiPage.a();
        }
        IPCService iPCService = this.f214a;
        Intent intent = new Intent();
        intent.setClass(iPCService, AppService.class);
        iPCService.stopService(intent);
        Log.d("com.qq.connect", " ipc stopService");
    }

    public int h() {
        Log.d("com.qq.connect", "startWifiConnection");
        if (AppService.c && AppService.d) {
            return 1;
        }
        int a2 = af.a(this.f214a);
        if (a2 == 0 || a2 == -1) {
            Log.d("com.qq.connect", "could not find ips");
            return -1;
        }
        ay.b = n.a(a2 >>> 16);
        WifiPage.b(this.f214a.getApplicationContext());
        return 0;
    }

    public void g() {
        Log.d("com.qq.connect", "startUSBConnection");
        if (!AppService.c) {
            IPCService iPCService = this.f214a;
            Intent intent = new Intent();
            intent.setClass(iPCService, AppService.class);
            iPCService.startService(intent);
        } else if (!AppService.c && AppService.d) {
            AppService.c(false);
        }
    }

    public int d() {
        Log.d("com.qq.connect", "getVersion");
        return Integer.parseInt("129");
    }

    public int f() {
        Log.d("com.qq.connect", "getServiceState");
        if (!AppService.c) {
            return 0;
        }
        int i = 1;
        if (AppService.e) {
            i = 3;
        }
        if (!h.b || h.e == null || !h.e.c()) {
            return i;
        }
        return i + 4;
    }

    public String n() {
        Log.d("com.qq.connect", "generatePassword");
        int a2 = af.a(this.f214a);
        if (a2 != 0 && a2 != -1) {
            return n.a(a2 >>> 16);
        }
        Log.d("com.qq.connect", "could not find ips");
        return null;
    }

    public void l() {
        Log.d("com.qq.connect", "clearWifiLoginInfo");
        SQLiteDatabase writableDatabase = new DBHelper(this.f214a).getWritableDatabase();
        writableDatabase.delete("login", "_id > 0", null);
        writableDatabase.close();
    }

    public void a(String str, String str2, String str3) {
        SQLiteDatabase writableDatabase;
        Log.d("com.qq.connect", "acceptWifiLogin " + str2);
        t.a(0);
        IPCService iPCService = this.f214a;
        if (str3 != null && iPCService != null && (writableDatabase = new DBHelper(iPCService).getWritableDatabase()) != null) {
            ContentValues contentValues = new ContentValues();
            contentValues.put("key", str3);
            contentValues.put(SocialConstants.PARAM_TYPE, "1");
            if (str2 != null) {
                contentValues.put("name", str2);
            } else {
                contentValues.putNull("name");
            }
            contentValues.put("date", Long.valueOf(System.currentTimeMillis()));
            try {
                writableDatabase.insertOrThrow("login", null, contentValues);
            } catch (Exception e) {
                e.printStackTrace();
            }
            writableDatabase.close();
        }
    }

    public int m() {
        Log.d("com.qq.connect", "getWifiOrApInfo");
        return af.a(this.f214a);
    }

    public void a(int i, String str, ParcelObject parcelObject) {
    }

    public void a(aa aaVar) {
        Log.d("com.qq.connect", "set callback" + aaVar);
        this.f214a.b = aaVar;
    }

    public boolean b() {
        Log.d("com.qq.connect", "isConnected");
        return h.b && h.e != null && h.e.c();
    }

    public int a(int i) {
        return 0;
    }

    public String o() {
        Log.d("com.qq.connect", "getConnectPCName");
        return AppService.h;
    }

    public String p() {
        Log.d("com.qq.connect", "getConnectPCName");
        return AppService.i;
    }

    public void b(String str, String str2, String str3) {
        Log.d("com.qq.connect", "denyWifiLogin");
        t.a(-1);
    }

    public void a(Notification notification, Notification notification2) {
        Log.d("com.qq.connect", "setNotification");
        if (notification != null) {
            notification.icon = R.drawable.logo72;
        }
        if (notification2 != null) {
            notification2.icon = R.drawable.logo72;
        }
        this.f214a.c = notification;
        this.f214a.d = notification2;
        AppService.f();
    }

    public Notification q() {
        return this.f214a.c;
    }

    public Notification r() {
        return this.f214a.d;
    }

    public boolean c() {
        return false;
    }

    public void i() {
    }

    public void j() {
        AppService.i();
    }

    public long a(int[] iArr) {
        return h.a(this.f214a, iArr);
    }

    public boolean u() {
        return AppService.s();
    }

    public boolean v() {
        return AppService.t();
    }

    public void b(String str) {
        t.b(str);
    }

    public void x() {
        t.a();
    }

    public boolean s() {
        return AppService.r();
    }

    public boolean t() {
        return AppService.q();
    }

    public boolean w() {
        return AppService.u();
    }

    public int c(String str) {
        return b.b().b(str);
    }

    public String A() {
        return AuthorReceiver.c();
    }

    public String B() {
        return AuthorReceiver.c();
    }
}
