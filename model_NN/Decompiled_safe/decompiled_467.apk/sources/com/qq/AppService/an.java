package com.qq.AppService;

import android.os.Parcel;
import android.os.Parcelable;

/* compiled from: ProGuard */
final class an implements Parcelable.Creator<ParcelObject> {
    an() {
    }

    /* renamed from: a */
    public ParcelObject createFromParcel(Parcel parcel) {
        return new ParcelObject(parcel, null);
    }

    /* renamed from: a */
    public ParcelObject[] newArray(int i) {
        return new ParcelObject[i];
    }
}
