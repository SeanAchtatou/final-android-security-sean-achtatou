package com.qq.g;

import com.qq.AppService.s;
import com.qq.ndk.Native;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class c {

    /* renamed from: a  reason: collision with root package name */
    public boolean f293a;
    public byte[] b;
    public int c;
    public int d;
    private int e;
    private ArrayList<byte[]> f;

    public c() {
        this.f293a = false;
        this.f = new ArrayList<>();
        this.b = null;
        this.c = 0;
        this.d = 0;
        this.e = 0;
        this.f = null;
    }

    public int a() {
        return this.e;
    }

    public void a(int i) {
        this.e = i;
    }

    public ArrayList<byte[]> b() {
        return this.f;
    }

    public void a(ArrayList<byte[]> arrayList) {
        this.b = null;
        this.f = arrayList;
    }

    public c(byte[] bArr, int i) {
        this.f293a = false;
        this.f = new ArrayList<>();
        this.b = null;
        this.c = 0;
        this.d = 0;
        this.b = bArr;
        b(i);
    }

    public void b(int i) {
        this.d = i;
        if (this.b == null) {
            this.c = 0;
            return;
        }
        int i2 = 8;
        while (i2 < this.b.length) {
            i2 = i2 + 4 + s.a(this.b, i2);
            this.c++;
        }
    }

    private void c(int i) {
        int i2;
        if (this.b != null && this.b.length > 8 && s.b(d(), s.hs)) {
            this.f293a = true;
            Native nativeR = new Native();
            int a2 = s.a(this.b, i);
            if (a2 < this.b.length - (i + 4)) {
                byte[] unEncrypt = nativeR.unEncrypt(this.b, i + 4, a2);
                if (unEncrypt == null) {
                    this.b = s.hs;
                    return;
                }
                if (a2 % 8 != 0) {
                    i2 = 8 - (a2 % 8);
                } else {
                    i2 = 0;
                }
                byte[] bArr = new byte[(((this.b.length - i) - 4) - i2)];
                System.arraycopy(unEncrypt, 0, bArr, 0, unEncrypt.length);
                System.arraycopy(this.b, i + 4 + a2 + i2, bArr, unEncrypt.length, this.b.length - (i2 + ((a2 + i) + 4)));
                this.b = bArr;
            } else if (a2 == this.b.length - (i + 4)) {
                byte[] unEncrypt2 = nativeR.unEncrypt(this.b, i + 4, a2);
                if (unEncrypt2 == null) {
                    nativeR.setDefaultKey();
                    this.b = nativeR.unEncrypt(this.b, i + 4, a2);
                    if (this.b == null) {
                        this.b = s.hs;
                        return;
                    }
                    return;
                }
                this.b = unEncrypt2;
            } else {
                byte[] bArr2 = new byte[8];
                System.arraycopy(this.b, 0, bArr2, 0, 8);
                this.b = bArr2;
            }
        }
    }

    public void c() {
        int i = 8;
        this.d = 8;
        if (this.b == null) {
            this.c = 0;
            return;
        }
        c(8);
        while (i < this.b.length && i + 4 <= this.b.length && (i = i + 4 + s.a(this.b, i)) <= this.b.length) {
            this.c++;
        }
    }

    public byte[] d() {
        if (this.b == null || this.b.length < 8) {
            return null;
        }
        byte[] bArr = new byte[4];
        System.arraycopy(this.b, 4, bArr, 0, 4);
        return bArr;
    }

    public int e() {
        return this.c;
    }

    public int f() {
        return this.d;
    }

    public void g() {
        int a2 = s.a(this.b, this.d);
        this.d += 4;
        this.d = a2 + this.d;
    }

    public int h() {
        s.a(this.b, this.d);
        this.d += 4;
        int a2 = s.a(this.b, this.d);
        this.d += 4;
        return a2;
    }

    public long i() {
        if (((long) s.a(this.b, this.d)) != 8) {
            return -1;
        }
        this.d += 4;
        long c2 = s.c(this.b, this.d);
        this.d += 8;
        return c2;
    }

    public String j() {
        int a2 = s.a(this.b, this.d);
        this.d += 4;
        String a3 = s.a(this.b, this.d, a2);
        this.d = a2 + this.d;
        return a3;
    }

    public byte[] k() {
        int a2 = s.a(this.b, this.d);
        this.d += 4;
        if (a2 == 0) {
            return null;
        }
        if (a2 == 4 && ((((this.b[this.d + 3] + this.b[this.d]) << (this.b[this.d + 1] + 24)) << (this.b[this.d + 2] + 16)) << 8) == 32) {
            return null;
        }
        byte[] bArr = new byte[a2];
        System.arraycopy(this.b, this.d, bArr, 0, a2);
        this.d = a2 + this.d;
        return bArr;
    }
}
