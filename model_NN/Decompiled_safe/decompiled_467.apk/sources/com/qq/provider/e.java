package com.qq.provider;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import com.qq.AppService.s;
import com.qq.g.c;
import com.tencent.connect.common.Constants;
import com.tencent.open.SocialConstants;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;

/* compiled from: ProGuard */
public class e {

    /* renamed from: a  reason: collision with root package name */
    public com.qq.a.a.e f338a = new com.qq.a.a.e();

    private e() {
    }

    public static e a() {
        return new e();
    }

    public void a(Context context, c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        long i = cVar.i();
        ArrayList arrayList = new ArrayList();
        arrayList.add(s.a(s.b(i)));
        cVar.a(arrayList);
        cVar.a(0);
    }

    public void b(Context context, c cVar) {
        TimeZone timeZone = Calendar.getInstance().getTimeZone();
        ArrayList arrayList = new ArrayList();
        arrayList.add(s.a(timeZone.getRawOffset() / 60000));
        arrayList.add(s.a(timeZone.getDisplayName()));
        arrayList.add(s.a(timeZone.getID()));
        cVar.a(arrayList);
        cVar.a(0);
    }

    public String a(int i) {
        StringBuilder sb = new StringBuilder();
        sb.append("GMT");
        if (i > 0) {
            sb.append('+');
        } else {
            sb.append('-');
        }
        int abs = Math.abs(i);
        int i2 = abs / 60;
        int i3 = abs % 60;
        if (i2 > 10) {
            sb.append(i2);
        } else {
            sb.append(0);
            sb.append(i2);
        }
        sb.append(':');
        if (i3 > 10) {
            sb.append(i3);
        } else {
            sb.append(0);
            sb.append(i3);
        }
        return sb.toString();
    }

    public void c(Context context, c cVar) {
        int i;
        ArrayList arrayList = new ArrayList();
        arrayList.add(s.a(25));
        int i2 = -12;
        int i3 = 0;
        while (i2 <= 14) {
            String[] availableIDs = TimeZone.getAvailableIDs(3600000 * i2);
            if (availableIDs == null) {
                i = i3;
            } else {
                arrayList.add(s.a(i2 * 60));
                arrayList.add(s.a(a(i2 * 60)));
                arrayList.add(s.a(availableIDs.length));
                for (String a2 : availableIDs) {
                    arrayList.add(s.a(a2));
                }
                i = i3 + 1;
            }
            i2++;
            i3 = i;
        }
        arrayList.set(0, s.a(i3));
        cVar.a(0);
        cVar.a(arrayList);
    }

    public void d(Context context, c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        String[] availableIDs = TimeZone.getAvailableIDs(60000 * h);
        if (availableIDs == null) {
            cVar.a(7);
            return;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(s.a(h));
        arrayList.add(s.a(a(h)));
        arrayList.add(s.a(availableIDs.length));
        for (String a2 : availableIDs) {
            arrayList.add(s.a(a2));
        }
        cVar.a(arrayList);
        cVar.a(0);
    }

    public Uri a(Context context) {
        Cursor query = context.getContentResolver().query(this.f338a.c, null, null, null, null);
        if (query != null) {
            query.close();
            return this.f338a.c;
        }
        Cursor query2 = context.getContentResolver().query(this.f338a.d, null, null, null, null);
        if (query2 == null) {
            return null;
        }
        query2.close();
        return this.f338a.d;
    }

    public Uri b(Context context) {
        Cursor query = context.getContentResolver().query(this.f338a.f260a, null, null, null, null);
        if (query != null) {
            query.close();
            return this.f338a.f260a;
        }
        Cursor query2 = context.getContentResolver().query(this.f338a.b, null, null, null, null);
        if (query2 == null) {
            return null;
        }
        query2.close();
        return this.f338a.b;
    }

    public void e(Context context, c cVar) {
        Uri a2 = a(context);
        if (a2 == null) {
            cVar.a(7);
            return;
        }
        ContentResolver contentResolver = context.getContentResolver();
        StringBuilder sb = new StringBuilder();
        this.f338a.getClass();
        Cursor query = contentResolver.query(a2, null, sb.append("selected").append(" = ").append(1).toString(), null, null);
        if (query == null) {
            cVar.a(8);
            return;
        }
        ArrayList arrayList = new ArrayList();
        int count = query.getCount();
        arrayList.add(s.a(count));
        int i = 0;
        boolean moveToFirst = query.moveToFirst();
        while (i < count && moveToFirst) {
            this.f338a.getClass();
            int i2 = query.getInt(query.getColumnIndex("_id"));
            this.f338a.getClass();
            byte[] blob = query.getBlob(query.getColumnIndex(SocialConstants.PARAM_URL));
            this.f338a.getClass();
            byte[] blob2 = query.getBlob(query.getColumnIndex("name"));
            this.f338a.getClass();
            byte[] blob3 = query.getBlob(query.getColumnIndex("displayName"));
            this.f338a.getClass();
            int i3 = query.getInt(query.getColumnIndex("hidden"));
            this.f338a.getClass();
            byte[] blob4 = query.getBlob(query.getColumnIndex("timezone"));
            if (blob == null) {
                blob = s.hr;
            }
            if (blob2 == null) {
                blob2 = s.hr;
            }
            if (blob3 == null) {
                blob3 = s.hr;
            }
            if (blob4 == null) {
                blob3 = s.hr;
            }
            arrayList.add(s.a(i2));
            arrayList.add(blob);
            arrayList.add(blob2);
            arrayList.add(blob3);
            arrayList.add(s.a(i3));
            arrayList.add(blob4);
            i++;
            moveToFirst = query.moveToNext();
        }
        query.close();
        cVar.a(arrayList);
    }

    public void f(Context context, c cVar) {
        Uri a2 = a(context);
        if (a2 == null) {
            cVar.a(7);
            return;
        }
        ContentResolver contentResolver = context.getContentResolver();
        StringBuilder sb = new StringBuilder();
        this.f338a.getClass();
        contentResolver.delete(a2, sb.append("selected").append(" = ").append(1).toString(), null);
        Uri b = b(context);
        if (b != null) {
            context.getContentResolver().delete(b, null, null);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void g(Context context, c cVar) {
        if (cVar.e() < 4) {
            cVar.a(1);
            return;
        }
        Uri a2 = a(context);
        if (a2 == null) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        String j2 = cVar.j();
        int h = cVar.h();
        String j3 = cVar.j();
        String j4 = cVar.j();
        ContentValues contentValues = new ContentValues();
        if (j != null) {
            this.f338a.getClass();
            contentValues.put(SocialConstants.PARAM_URL, j);
        }
        if (j2 != null) {
            this.f338a.getClass();
            contentValues.put("name", j2);
        }
        this.f338a.getClass();
        contentValues.put("selected", (Integer) 1);
        this.f338a.getClass();
        contentValues.put("hidden", Integer.valueOf(h));
        if (j3 != null) {
            this.f338a.getClass();
            contentValues.put("displayName", j3);
        }
        if (j4 != null) {
            this.f338a.getClass();
            contentValues.put("timezone", j4);
        }
        try {
            int parseInt = Integer.parseInt(context.getContentResolver().insert(a2, contentValues).getLastPathSegment());
            ArrayList arrayList = new ArrayList();
            arrayList.add(s.a(parseInt));
            cVar.a(arrayList);
            cVar.a(0);
        } catch (Exception e) {
            cVar.a(8);
        }
    }

    public void h(Context context, c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        Uri a2 = a(context);
        if (a2 == null) {
            cVar.a(1);
            return;
        }
        if (context.getContentResolver().delete(Uri.withAppendedPath(a2, Constants.STR_EMPTY + cVar.h()), null, null) > 0) {
            cVar.a(0);
        } else {
            cVar.a(1);
        }
    }

    public void i(Context context, c cVar) {
        Uri b = b(context);
        if (b == null) {
            cVar.a(7);
            return;
        }
        Cursor query = context.getContentResolver().query(b, null, null, null, null);
        if (query == null) {
            cVar.a(8);
            return;
        }
        int count = query.getCount();
        ArrayList arrayList = new ArrayList();
        arrayList.add(s.a(count));
        boolean moveToFirst = query.moveToFirst();
        for (int i = 0; i < count && moveToFirst; i++) {
            this.f338a.getClass();
            int i2 = query.getInt(query.getColumnIndex("_id"));
            this.f338a.getClass();
            int i3 = query.getInt(query.getColumnIndex("calendar_id"));
            this.f338a.getClass();
            String string = query.getString(query.getColumnIndex("htmlUri"));
            this.f338a.getClass();
            String string2 = query.getString(query.getColumnIndex("commentsUri"));
            this.f338a.getClass();
            String string3 = query.getString(query.getColumnIndex("title"));
            this.f338a.getClass();
            String string4 = query.getString(query.getColumnIndex("eventLocation"));
            this.f338a.getClass();
            String string5 = query.getString(query.getColumnIndex(SocialConstants.PARAM_COMMENT));
            this.f338a.getClass();
            int i4 = query.getInt(query.getColumnIndex("eventStatus"));
            this.f338a.getClass();
            long j = query.getLong(query.getColumnIndex("dtstart"));
            this.f338a.getClass();
            long j2 = query.getLong(query.getColumnIndex("dtend"));
            this.f338a.getClass();
            String string6 = query.getString(query.getColumnIndex("timezone"));
            this.f338a.getClass();
            int i5 = query.getInt(query.getColumnIndex("allDay"));
            this.f338a.getClass();
            int i6 = query.getInt(query.getColumnIndex("visibility"));
            this.f338a.getClass();
            int i7 = query.getInt(query.getColumnIndex("transparency"));
            this.f338a.getClass();
            int i8 = query.getInt(query.getColumnIndex("hasAlarm"));
            arrayList.add(s.a(i2));
            arrayList.add(s.a(i3));
            arrayList.add(s.a(string));
            arrayList.add(s.a(string2));
            arrayList.add(s.a(string3));
            arrayList.add(s.a(string4));
            arrayList.add(s.a(string5));
            arrayList.add(s.a(i4));
            arrayList.add(s.a(j));
            arrayList.add(s.a(j2));
            arrayList.add(s.a(string6));
            arrayList.add(s.a(i5));
            arrayList.add(s.a(i6));
            arrayList.add(s.a(i7));
            arrayList.add(s.a(i8));
            moveToFirst = query.moveToNext();
        }
        query.close();
        cVar.a(arrayList);
    }

    public void j(Context context, c cVar) {
        if (cVar.e() < 14) {
            cVar.a(1);
            return;
        }
        Uri b = b(context);
        if (b == null) {
            cVar.a(7);
            return;
        }
        int h = cVar.h();
        String j = cVar.j();
        String j2 = cVar.j();
        String j3 = cVar.j();
        String j4 = cVar.j();
        String j5 = cVar.j();
        int h2 = cVar.h();
        long i = cVar.i();
        long i2 = cVar.i();
        String j6 = cVar.j();
        int h3 = cVar.h();
        int h4 = cVar.h();
        int h5 = cVar.h();
        int h6 = cVar.h();
        ContentValues contentValues = new ContentValues();
        this.f338a.getClass();
        contentValues.put("calendar_id", Integer.valueOf(h));
        if (j != null) {
            this.f338a.getClass();
            contentValues.put("htmlUri", j);
        }
        if (j2 != null) {
            this.f338a.getClass();
            contentValues.put("commentsUri", j2);
        }
        if (!s.b(j3)) {
            this.f338a.getClass();
            contentValues.put("title", j3);
        }
        if (!s.b(j4)) {
            this.f338a.getClass();
            contentValues.put("eventLocation", j4);
        }
        if (!s.b(j5)) {
            this.f338a.getClass();
            contentValues.put(SocialConstants.PARAM_COMMENT, j5);
        }
        this.f338a.getClass();
        contentValues.put("eventStatus", Integer.valueOf(h2));
        this.f338a.getClass();
        contentValues.put("dtstart", Long.valueOf(i));
        this.f338a.getClass();
        contentValues.put("dtend", Long.valueOf(i2));
        if (!s.b(j6)) {
            this.f338a.getClass();
            contentValues.put("eventTimezone", j6);
        }
        this.f338a.getClass();
        contentValues.put("allDay", Integer.valueOf(h3));
        this.f338a.getClass();
        contentValues.put("visibility", Integer.valueOf(h4));
        this.f338a.getClass();
        contentValues.put("transparency", Integer.valueOf(h5));
        this.f338a.getClass();
        contentValues.put("hasAlarm", Integer.valueOf(h6));
        try {
            int parseInt = Integer.parseInt(context.getContentResolver().insert(b, contentValues).getLastPathSegment());
            ArrayList arrayList = new ArrayList();
            arrayList.add(s.a(parseInt));
            cVar.a(arrayList);
        } catch (Exception e) {
            cVar.a(8);
        }
    }

    public void k(Context context, c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        Uri b = b(context);
        if (b == null) {
            cVar.a(7);
            return;
        }
        int h = cVar.h();
        ArrayList arrayList = new ArrayList();
        arrayList.add(s.a(h));
        for (int i = 0; i < h; i++) {
            int h2 = cVar.h();
            String j = cVar.j();
            String j2 = cVar.j();
            String j3 = cVar.j();
            String j4 = cVar.j();
            String j5 = cVar.j();
            int h3 = cVar.h();
            long i2 = cVar.i();
            long i3 = cVar.i();
            String j6 = cVar.j();
            int h4 = cVar.h();
            int h5 = cVar.h();
            int h6 = cVar.h();
            int h7 = cVar.h();
            ContentValues contentValues = new ContentValues();
            this.f338a.getClass();
            contentValues.put("calendar_id", Integer.valueOf(h2));
            if (j != null) {
                this.f338a.getClass();
                contentValues.put("htmlUri", j);
            }
            if (j2 != null) {
                this.f338a.getClass();
                contentValues.put("commentsUri", j2);
            }
            if (!s.b(j3)) {
                this.f338a.getClass();
                contentValues.put("title", j3);
            }
            if (!s.b(j4)) {
                this.f338a.getClass();
                contentValues.put("eventLocation", j4);
            }
            if (!s.b(j5)) {
                this.f338a.getClass();
                contentValues.put(SocialConstants.PARAM_COMMENT, j5);
            }
            this.f338a.getClass();
            contentValues.put("eventStatus", Integer.valueOf(h3));
            this.f338a.getClass();
            contentValues.put("dtstart", Long.valueOf(i2));
            this.f338a.getClass();
            contentValues.put("dtend", Long.valueOf(i3));
            if (!s.b(j6)) {
                this.f338a.getClass();
                contentValues.put("eventTimezone", j6);
            }
            this.f338a.getClass();
            contentValues.put("allDay", Integer.valueOf(h4));
            this.f338a.getClass();
            contentValues.put("visibility", Integer.valueOf(h5));
            this.f338a.getClass();
            contentValues.put("transparency", Integer.valueOf(h6));
            this.f338a.getClass();
            contentValues.put("hasAlarm", Integer.valueOf(h7));
            int i4 = 0;
            try {
                i4 = Integer.parseInt(context.getContentResolver().insert(b, contentValues).getLastPathSegment());
            } catch (Exception e) {
            }
            arrayList.add(s.a(i4));
        }
        cVar.a(arrayList);
    }

    public void l(Context context, c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        Uri b = b(context);
        if (b == null) {
            cVar.a(7);
            return;
        }
        if (context.getContentResolver().delete(Uri.withAppendedPath(b, Constants.STR_EMPTY + cVar.h()), null, null) > 0) {
            cVar.a(0);
        } else {
            cVar.a(1);
        }
    }

    public void m(Context context, c cVar) {
        if (cVar.e() < 2) {
            cVar.a(1);
            return;
        }
        Uri b = b(context);
        if (b == null) {
            cVar.a(7);
            return;
        }
        int h = cVar.h();
        int h2 = cVar.h();
        Uri withAppendedPath = Uri.withAppendedPath(b, Constants.STR_EMPTY + h);
        ContentValues contentValues = new ContentValues();
        this.f338a.getClass();
        contentValues.put("eventStatus", Integer.valueOf(h2));
        if (context.getContentResolver().update(withAppendedPath, contentValues, null, null) > 0) {
            cVar.a(0);
        } else {
            cVar.a(1);
        }
    }

    public void n(Context context, c cVar) {
        if (cVar.e() < 14) {
            cVar.a(1);
            return;
        }
        Uri b = b(context);
        if (b == null) {
            cVar.a(7);
            return;
        }
        int h = cVar.h();
        int h2 = cVar.h();
        String j = cVar.j();
        String j2 = cVar.j();
        String j3 = cVar.j();
        String j4 = cVar.j();
        String j5 = cVar.j();
        int h3 = cVar.h();
        long i = cVar.i();
        long i2 = cVar.i();
        String j6 = cVar.j();
        int h4 = cVar.h();
        int h5 = cVar.h();
        int h6 = cVar.h();
        int h7 = cVar.h();
        ContentValues contentValues = new ContentValues();
        this.f338a.getClass();
        contentValues.put("calendar_id", Integer.valueOf(h2));
        if (j != null) {
            this.f338a.getClass();
            contentValues.put("htmlUri", j);
        }
        if (j2 != null) {
            this.f338a.getClass();
            contentValues.put("commentsUri", j2);
        }
        if (!s.b(j3)) {
            this.f338a.getClass();
            contentValues.put("title", j3);
        }
        if (!s.b(j4)) {
            this.f338a.getClass();
            contentValues.put("eventLocation", j4);
        }
        if (!s.b(j5)) {
            this.f338a.getClass();
            contentValues.put(SocialConstants.PARAM_COMMENT, j5);
        }
        this.f338a.getClass();
        contentValues.put("eventStatus", Integer.valueOf(h3));
        this.f338a.getClass();
        contentValues.put("dtstart", Long.valueOf(i));
        this.f338a.getClass();
        contentValues.put("dtend", Long.valueOf(i2));
        if (!s.b(j6)) {
            this.f338a.getClass();
            contentValues.put("eventTimezone", j6);
        }
        this.f338a.getClass();
        contentValues.put("allDay", Integer.valueOf(h4));
        this.f338a.getClass();
        contentValues.put("visibility", Integer.valueOf(h5));
        this.f338a.getClass();
        contentValues.put("transparency", Integer.valueOf(h6));
        this.f338a.getClass();
        contentValues.put("hasAlarm", Integer.valueOf(h7));
        if (context.getContentResolver().update(Uri.withAppendedPath(b, Constants.STR_EMPTY + h), contentValues, null, null) > 0) {
            cVar.a(0);
        } else {
            cVar.a(1);
        }
    }
}
