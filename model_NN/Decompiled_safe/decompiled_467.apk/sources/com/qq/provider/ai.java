package com.qq.provider;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import com.qq.AppService.s;
import com.qq.a.a.b;
import com.qq.g.c;
import com.qq.ndk.Native;
import com.qq.util.g;
import com.qq.util.j;
import com.tencent.connect.common.Constants;
import com.tencent.open.SocialConstants;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class ai {

    /* renamed from: a  reason: collision with root package name */
    private static ai f327a;

    private ai() {
    }

    public static ai a() {
        if (f327a != null) {
            return f327a;
        }
        f327a = new ai();
        return f327a;
    }

    public void a(Context context, c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        if (s.b(j)) {
            cVar.a(1);
            return;
        }
        File file = new File(j);
        if (!file.exists()) {
            cVar.a(1);
            return;
        }
        Cursor query = context.getContentResolver().query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, null, "_data = '" + file.getAbsolutePath() + "'", null, null);
        if (query == null) {
            cVar.a(8);
            return;
        }
        ArrayList arrayList = new ArrayList();
        if (query.moveToFirst()) {
            arrayList.add(s.a(query.getInt(query.getColumnIndex("_id"))));
            cVar.a(0);
        } else {
            cVar.a(7);
        }
        query.close();
        cVar.a(arrayList);
    }

    public void b(Context context, c cVar) {
        Uri uri;
        long j;
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        if (cVar.h() > 0) {
            uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
        } else {
            uri = MediaStore.Video.Media.INTERNAL_CONTENT_URI;
        }
        Cursor query = context.getContentResolver().query(uri, new String[]{"sum(_size)"}, null, null, null);
        if (query == null) {
            cVar.a(8);
            return;
        }
        if (query.moveToFirst()) {
            j = query.getLong(0);
        } else {
            j = 0;
        }
        query.close();
        ArrayList arrayList = new ArrayList();
        arrayList.add(s.a(j));
        cVar.a(0);
        cVar.a(arrayList);
    }

    public void c(Context context, c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        if (s.b(j)) {
            cVar.a(1);
        } else if (!new File(j).exists()) {
            cVar.a(6);
        } else {
            new b(context).a(j);
            cVar.a(0);
        }
    }

    public void d(Context context, c cVar) {
        Uri uri;
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        if (h <= 0 || Environment.getExternalStorageState().equals("mounted")) {
            if (h > 0) {
                uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
            } else {
                uri = MediaStore.Video.Media.INTERNAL_CONTENT_URI;
            }
            Cursor query = context.getContentResolver().query(uri, null, null, null, null);
            if (query == null) {
                cVar.a(8);
                return;
            }
            int count = query.getCount();
            query.close();
            ArrayList arrayList = new ArrayList();
            arrayList.add(s.a(count));
            cVar.a(0);
            cVar.a(arrayList);
            return;
        }
        cVar.a(9);
    }

    public void e(Context context, c cVar) {
        Uri uri;
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        if (cVar.h() > 0) {
            uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
        } else {
            uri = MediaStore.Video.Media.INTERNAL_CONTENT_URI;
        }
        Cursor query = context.getContentResolver().query(uri, null, null, null, "_id");
        if (query == null) {
            cVar.a(8);
            return;
        }
        int count = query.getCount();
        ArrayList arrayList = new ArrayList();
        arrayList.add(s.a(count));
        for (boolean moveToFirst = query.moveToFirst(); moveToFirst; moveToFirst = query.moveToNext()) {
            arrayList.add(s.a(query.getInt(query.getColumnIndex("_id"))));
        }
        query.close();
        cVar.a(0);
        cVar.a(arrayList);
    }

    public void f(Context context, c cVar) {
        Uri withAppendedPath;
        if (cVar.e() < 2) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        if (cVar.h() > 0) {
            withAppendedPath = Uri.withAppendedPath(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, Constants.STR_EMPTY + h);
        } else {
            withAppendedPath = Uri.withAppendedPath(MediaStore.Video.Media.INTERNAL_CONTENT_URI, Constants.STR_EMPTY + h);
        }
        Cursor query = context.getContentResolver().query(withAppendedPath, null, null, null, null);
        if (query == null) {
            cVar.a(8);
        } else if (!query.moveToFirst()) {
            query.close();
            cVar.a(7);
        } else {
            ArrayList arrayList = new ArrayList();
            String string = query.getString(query.getColumnIndex("_data"));
            arrayList.add(s.a(query.getInt(query.getColumnIndex("_id"))));
            arrayList.add(s.a(string));
            arrayList.add(s.a(query.getString(query.getColumnIndex("_display_name"))));
            arrayList.add(s.a(query.getLong(query.getColumnIndex("_size"))));
            arrayList.add(s.a(query.getString(query.getColumnIndex("title"))));
            arrayList.add(s.a(query.getString(query.getColumnIndex("mime_type"))));
            long j = query.getLong(query.getColumnIndex("date_added")) * 1000;
            long j2 = query.getLong(query.getColumnIndex("date_modified")) * 1000;
            long j3 = query.getLong(query.getColumnIndex("datetaken"));
            if (j2 == 0 && j3 != 0) {
                j2 = j3;
            } else if (j2 == 0 && j != 0) {
                j2 = j;
            } else if (j2 == 0 || j2 > System.currentTimeMillis()) {
                j2 = new File(string).lastModified();
            }
            arrayList.add(s.a(s.b(j)));
            arrayList.add(s.a(s.b(j2)));
            arrayList.add(s.a(s.b(j3)));
            long j4 = query.getLong(query.getColumnIndex("duration"));
            if (j4 <= 0) {
                MediaPlayer mediaPlayer = new MediaPlayer();
                try {
                    mediaPlayer.setDataSource(string);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (IllegalStateException e2) {
                    e2.printStackTrace();
                } catch (IOException e3) {
                    e3.printStackTrace();
                }
                j4 = (long) mediaPlayer.getDuration();
                mediaPlayer.release();
                ContentValues contentValues = new ContentValues();
                contentValues.put("duration", Long.valueOf(j4));
                context.getContentResolver().update(withAppendedPath, contentValues, null, null);
            }
            arrayList.add(s.a(j4));
            arrayList.add(s.a(query.getString(query.getColumnIndex("artist"))));
            arrayList.add(s.a(query.getString(query.getColumnIndex("album"))));
            arrayList.add(s.a(query.getString(query.getColumnIndex("resolution"))));
            arrayList.add(s.a(query.getString(query.getColumnIndex(SocialConstants.PARAM_COMMENT))));
            arrayList.add(s.a(query.getInt(query.getColumnIndex("isprivate"))));
            arrayList.add(s.a(query.getString(query.getColumnIndex("tags"))));
            arrayList.add(s.a(query.getString(query.getColumnIndex("category"))));
            arrayList.add(s.a(query.getString(query.getColumnIndex("language"))));
            query.close();
            cVar.a(0);
            cVar.a(arrayList);
        }
    }

    public void g(Context context, c cVar) {
        Uri withAppendedPath;
        if (cVar.e() < 2) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        if (cVar.h() > 0) {
            withAppendedPath = Uri.withAppendedPath(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, Constants.STR_EMPTY + h);
        } else {
            withAppendedPath = Uri.withAppendedPath(MediaStore.Video.Media.INTERNAL_CONTENT_URI, Constants.STR_EMPTY + h);
        }
        Cursor query = context.getContentResolver().query(withAppendedPath, null, null, null, null);
        if (query == null) {
            cVar.a(8);
        } else if (!query.moveToFirst()) {
            query.close();
            cVar.a(7);
        } else {
            ArrayList arrayList = new ArrayList();
            String string = query.getString(query.getColumnIndex("_data"));
            arrayList.add(s.a(query.getInt(query.getColumnIndex("_id"))));
            arrayList.add(s.a(string));
            arrayList.add(s.a(query.getString(query.getColumnIndex("_display_name"))));
            arrayList.add(s.a(query.getLong(query.getColumnIndex("_size"))));
            arrayList.add(s.a(query.getString(query.getColumnIndex("title"))));
            arrayList.add(s.a(query.getString(query.getColumnIndex("mime_type"))));
            long j = query.getLong(query.getColumnIndex("date_added")) * 1000;
            long j2 = query.getLong(query.getColumnIndex("date_modified")) * 1000;
            long j3 = query.getLong(query.getColumnIndex("datetaken"));
            if (j2 == 0 && j3 != 0) {
                j2 = j3;
            } else if (j2 == 0 && j != 0) {
                j2 = j;
            } else if (j2 == 0 || j2 > System.currentTimeMillis()) {
                j2 = new File(string).lastModified();
            }
            arrayList.add(s.a(s.b(j)));
            arrayList.add(s.a(s.b(j2)));
            arrayList.add(s.a(s.b(j3)));
            long j4 = query.getLong(query.getColumnIndex("duration"));
            if (j4 <= 0) {
                MediaPlayer mediaPlayer = new MediaPlayer();
                try {
                    mediaPlayer.setDataSource(string);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (IllegalStateException e2) {
                    e2.printStackTrace();
                } catch (IOException e3) {
                    e3.printStackTrace();
                }
                j4 = (long) mediaPlayer.getDuration();
                mediaPlayer.release();
                ContentValues contentValues = new ContentValues();
                contentValues.put("duration", Long.valueOf(j4));
                context.getContentResolver().update(withAppendedPath, contentValues, null, null);
            }
            arrayList.add(s.a(j4));
            arrayList.add(s.a(query.getString(query.getColumnIndex("artist"))));
            arrayList.add(s.a(query.getString(query.getColumnIndex("album"))));
            arrayList.add(s.a(query.getString(query.getColumnIndex("resolution"))));
            arrayList.add(s.a(query.getString(query.getColumnIndex(SocialConstants.PARAM_COMMENT))));
            arrayList.add(s.a(query.getInt(query.getColumnIndex("isprivate"))));
            arrayList.add(s.a(query.getString(query.getColumnIndex("tags"))));
            arrayList.add(s.a(query.getString(query.getColumnIndex("category"))));
            arrayList.add(s.a(query.getString(query.getColumnIndex("language"))));
            arrayList.add(s.a(g.a().a(string)));
            query.close();
            cVar.a(0);
            cVar.a(arrayList);
        }
    }

    public void h(Context context, c cVar) {
        Uri uri;
        int i;
        if (cVar.e() < 3) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        int h2 = cVar.h();
        int h3 = cVar.h();
        if (h2 <= 0) {
            cVar.a(1);
            return;
        }
        if (h3 > 0) {
            uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
        } else {
            uri = MediaStore.Video.Media.INTERNAL_CONTENT_URI;
        }
        Cursor query = context.getContentResolver().query(uri, null, "_id > " + h, null, "_id");
        if (query == null) {
            cVar.a(8);
            return;
        }
        boolean moveToFirst = query.moveToFirst();
        int count = query.getCount();
        if (count > h2) {
            count = h2;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(s.a(count));
        int i2 = 0;
        int i3 = 0;
        while (i3 < count && moveToFirst) {
            String string = query.getString(query.getColumnIndex("_data"));
            int i4 = query.getInt(query.getColumnIndex("_id"));
            if (string == null) {
                i = i3 - 1;
                moveToFirst = query.moveToNext();
            } else {
                File file = new File(string);
                if (file == null || !file.exists()) {
                    i = i3 - 1;
                    moveToFirst = query.moveToNext();
                } else {
                    int i5 = i2 + 1;
                    arrayList.add(s.a(i4));
                    arrayList.add(s.a(string));
                    arrayList.add(s.a(query.getString(query.getColumnIndex("_display_name"))));
                    arrayList.add(s.a(query.getLong(query.getColumnIndex("_size"))));
                    arrayList.add(s.a(query.getString(query.getColumnIndex("title"))));
                    arrayList.add(s.a(query.getString(query.getColumnIndex("mime_type"))));
                    long j = query.getLong(query.getColumnIndex("date_added")) * 1000;
                    long j2 = query.getLong(query.getColumnIndex("date_modified")) * 1000;
                    long j3 = query.getLong(query.getColumnIndex("datetaken"));
                    if (j2 == 0 && j3 != 0) {
                        j2 = j3;
                    } else if (j2 == 0 && j != 0) {
                        j2 = j;
                    } else if (j2 == 0 || j2 > System.currentTimeMillis()) {
                        j2 = new File(string).lastModified();
                    }
                    arrayList.add(s.a(s.b(j)));
                    arrayList.add(s.a(s.b(j2)));
                    arrayList.add(s.a(s.b(j3)));
                    long j4 = query.getLong(query.getColumnIndex("duration"));
                    if (j4 <= 0) {
                        MediaPlayer mediaPlayer = new MediaPlayer();
                        try {
                            mediaPlayer.setDataSource(string);
                        } catch (IllegalArgumentException e) {
                            e.printStackTrace();
                        } catch (IllegalStateException e2) {
                            e2.printStackTrace();
                        } catch (IOException e3) {
                            e3.printStackTrace();
                        }
                        try {
                            mediaPlayer.prepare();
                        } catch (IllegalStateException e4) {
                            e4.printStackTrace();
                        } catch (IOException e5) {
                            e5.printStackTrace();
                        }
                        j4 = (long) mediaPlayer.getDuration();
                        mediaPlayer.release();
                        ContentValues contentValues = new ContentValues();
                        contentValues.put("duration", Long.valueOf(j4));
                        context.getContentResolver().update(uri, contentValues, "_id = " + i4, null);
                    }
                    arrayList.add(s.a(j4));
                    arrayList.add(s.a(query.getString(query.getColumnIndex("artist"))));
                    arrayList.add(s.a(query.getString(query.getColumnIndex("album"))));
                    arrayList.add(s.a(query.getString(query.getColumnIndex("resolution"))));
                    arrayList.add(s.a(query.getString(query.getColumnIndex(SocialConstants.PARAM_COMMENT))));
                    arrayList.add(s.a(query.getInt(query.getColumnIndex("isprivate"))));
                    arrayList.add(s.a(query.getString(query.getColumnIndex("tags"))));
                    arrayList.add(s.a(query.getString(query.getColumnIndex("category"))));
                    arrayList.add(s.a(query.getString(query.getColumnIndex("language"))));
                    i2 = i5;
                    moveToFirst = query.moveToNext();
                    i = i3;
                }
            }
            i3 = i + 1;
        }
        query.close();
        arrayList.set(0, s.a(i2));
        cVar.a(0);
        cVar.a(arrayList);
    }

    public void i(Context context, c cVar) {
        Uri uri;
        int i;
        if (cVar.e() < 3) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        int h2 = cVar.h();
        int h3 = cVar.h();
        if (h2 <= 0) {
            cVar.a(1);
            return;
        }
        if (h3 > 0) {
            uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
        } else {
            uri = MediaStore.Video.Media.INTERNAL_CONTENT_URI;
        }
        Cursor query = context.getContentResolver().query(uri, null, "_id > " + h, null, "_id");
        if (query == null) {
            cVar.a(8);
            return;
        }
        boolean moveToFirst = query.moveToFirst();
        int count = query.getCount();
        if (count > h2) {
            count = h2;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(s.a(count));
        int i2 = 0;
        int i3 = 0;
        while (i3 < count && moveToFirst) {
            String string = query.getString(query.getColumnIndex("_data"));
            int i4 = query.getInt(query.getColumnIndex("_id"));
            if (string == null) {
                i = i3 - 1;
                moveToFirst = query.moveToNext();
            } else {
                File file = new File(string);
                if (file == null || !file.exists()) {
                    i = i3 - 1;
                    moveToFirst = query.moveToNext();
                } else {
                    int i5 = i2 + 1;
                    arrayList.add(s.a(i4));
                    arrayList.add(s.a(string));
                    arrayList.add(s.a(query.getString(query.getColumnIndex("_display_name"))));
                    arrayList.add(s.a(query.getLong(query.getColumnIndex("_size"))));
                    arrayList.add(s.a(query.getString(query.getColumnIndex("title"))));
                    arrayList.add(s.a(query.getString(query.getColumnIndex("mime_type"))));
                    long j = query.getLong(query.getColumnIndex("date_added")) * 1000;
                    long j2 = query.getLong(query.getColumnIndex("date_modified")) * 1000;
                    long j3 = query.getLong(query.getColumnIndex("datetaken"));
                    if (j2 == 0 && j3 != 0) {
                        j2 = j3;
                    } else if (j2 == 0 && j != 0) {
                        j2 = j;
                    } else if (j2 == 0 || j2 > System.currentTimeMillis()) {
                        j2 = new File(string).lastModified();
                    }
                    arrayList.add(s.a(s.b(j)));
                    arrayList.add(s.a(s.b(j2)));
                    arrayList.add(s.a(s.b(j3)));
                    long j4 = query.getLong(query.getColumnIndex("duration"));
                    if (j4 <= 0) {
                        MediaPlayer mediaPlayer = new MediaPlayer();
                        try {
                            mediaPlayer.setDataSource(string);
                        } catch (IllegalArgumentException e) {
                            e.printStackTrace();
                        } catch (IllegalStateException e2) {
                            e2.printStackTrace();
                        } catch (IOException e3) {
                            e3.printStackTrace();
                        }
                        try {
                            mediaPlayer.prepare();
                        } catch (IllegalStateException e4) {
                            e4.printStackTrace();
                        } catch (IOException e5) {
                            e5.printStackTrace();
                        }
                        j4 = (long) mediaPlayer.getDuration();
                        mediaPlayer.release();
                        ContentValues contentValues = new ContentValues();
                        contentValues.put("duration", Long.valueOf(j4));
                        context.getContentResolver().update(uri, contentValues, "_id = " + i4, null);
                    }
                    arrayList.add(s.a(j4));
                    arrayList.add(s.a(query.getString(query.getColumnIndex("artist"))));
                    arrayList.add(s.a(query.getString(query.getColumnIndex("album"))));
                    arrayList.add(s.a(query.getString(query.getColumnIndex("resolution"))));
                    arrayList.add(s.a(query.getString(query.getColumnIndex(SocialConstants.PARAM_COMMENT))));
                    arrayList.add(s.a(query.getInt(query.getColumnIndex("isprivate"))));
                    arrayList.add(s.a(query.getString(query.getColumnIndex("tags"))));
                    arrayList.add(s.a(query.getString(query.getColumnIndex("category"))));
                    arrayList.add(s.a(query.getString(query.getColumnIndex("language"))));
                    arrayList.add(s.a(g.a().a(string)));
                    i2 = i5;
                    moveToFirst = query.moveToNext();
                    i = i3;
                }
            }
            i3 = i + 1;
        }
        query.close();
        arrayList.set(0, s.a(i2));
        cVar.a(0);
        cVar.a(arrayList);
    }

    public void j(Context context, c cVar) {
        if (cVar.e() < 18) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        cVar.g();
        String j = cVar.j();
        cVar.g();
        String j2 = cVar.j();
        cVar.g();
        cVar.g();
        cVar.g();
        cVar.g();
        cVar.g();
        cVar.g();
        cVar.g();
        String j3 = cVar.j();
        Uri withAppendedPath = Uri.withAppendedPath(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, Constants.STR_EMPTY + h);
        ContentValues contentValues = new ContentValues();
        if (!s.b(j2)) {
            contentValues.put("title", j2);
        } else {
            contentValues.putNull("title");
        }
        if (s.b(j)) {
            contentValues.putNull("_display_name");
        } else {
            contentValues.put("_display_name", j);
        }
        if (s.b(j3)) {
            contentValues.putNull(SocialConstants.PARAM_COMMENT);
        } else {
            contentValues.put(SocialConstants.PARAM_COMMENT, j3);
        }
        if (context.getContentResolver().update(withAppendedPath, contentValues, null, null) > 0) {
            cVar.a(0);
        } else {
            cVar.a(1);
        }
    }

    public void k(Context context, c cVar) {
        String str;
        long j;
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        String j2 = cVar.j();
        if (s.b(j2)) {
            cVar.a(1);
            return;
        }
        String realLinkFile = new Native().getRealLinkFile(j2);
        if (realLinkFile != null) {
        }
        String[] strArr = {"_size"};
        long j3 = 0;
        long j4 = 0;
        if (realLinkFile != null) {
            str = "_data like '" + j2 + "%'  or " + "_data" + " like '" + realLinkFile + "%'";
        } else {
            str = "_data like '" + j2 + "%'";
        }
        Cursor query = context.getContentResolver().query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, strArr, str, null, null);
        if (query != null) {
            long j5 = 0;
            for (boolean moveToFirst = query.moveToFirst(); moveToFirst; moveToFirst = query.moveToNext()) {
                long j6 = query.getLong(0);
                if (j6 > 0) {
                    j5 += j6;
                }
            }
            query.close();
            j4 = j5;
        }
        Cursor query2 = context.getContentResolver().query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, strArr, str, null, null);
        if (query2 != null) {
            long j7 = 0;
            for (boolean moveToFirst2 = query2.moveToFirst(); moveToFirst2; moveToFirst2 = query2.moveToNext()) {
                long j8 = query2.getLong(0);
                if (j8 > 0) {
                    j7 += j8;
                }
            }
            query2.close();
            j3 = j7;
        }
        Cursor query3 = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, strArr, str, null, null);
        if (query3 != null) {
            j = 0;
            for (boolean moveToFirst3 = query3.moveToFirst(); moveToFirst3; moveToFirst3 = query3.moveToNext()) {
                long j9 = query3.getLong(0);
                if (j9 > 0) {
                    j += j9;
                }
            }
            query3.close();
        } else {
            j = 0;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(s.a(j4));
        arrayList.add(s.a(j3));
        arrayList.add(s.a(j));
        cVar.a(arrayList);
        cVar.a(0);
    }

    public void l(Context context, c cVar) {
        String[] strArr;
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        if (cVar.e() < h + 1) {
            cVar.a(1);
            return;
        }
        if (h > 0) {
            strArr = new String[(h + 1)];
            strArr[0] = "/DCIM/";
            for (int i = 1; i <= h; i++) {
                strArr[i] = cVar.j();
            }
        } else {
            strArr = new String[]{"/DCIM/"};
        }
        StringBuilder sb = new StringBuilder(1024);
        for (int i2 = 0; i2 < strArr.length; i2++) {
            if (i2 > 0) {
                sb.append(" or ");
            }
            sb.append("_data like '%" + strArr[i2] + "%' ");
        }
        Cursor query = context.getContentResolver().query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, j.b, null, null, null);
        if (query == null) {
            cVar.a(8);
            return;
        }
        int count = query.getCount();
        query.close();
        Cursor query2 = context.getContentResolver().query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, j.b, sb.toString(), null, null);
        if (query2 == null) {
            cVar.a(8);
            return;
        }
        int count2 = query2.getCount();
        query2.close();
        ArrayList arrayList = new ArrayList();
        arrayList.add(s.a(count));
        arrayList.add(s.a(count2));
        cVar.a(arrayList);
        cVar.a(0);
    }

    public void m(Context context, c cVar) {
        File b = n.b();
        String str = null;
        if (b == null) {
            str = n.c();
        }
        if (b == null && str == null) {
            cVar.a(9);
            return;
        }
        if (b != null) {
            str = b.getAbsolutePath();
        }
        String str2 = str + "/Music";
        String str3 = str + File.separator + "DCIM/Camera/MobileAssistant";
        String str4 = str + "/Movies";
        String str5 = str + File.separator + "image";
        ArrayList arrayList = new ArrayList();
        arrayList.add(s.a(str2));
        arrayList.add(s.a(str3));
        arrayList.add(s.a(str4));
        arrayList.add(s.a(str5));
        arrayList.add(s.a(str + "/Pictures"));
        cVar.a(arrayList);
        cVar.a(0);
    }
}
