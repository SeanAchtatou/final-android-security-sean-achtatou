package com.qq.provider;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.provider.Settings;
import com.qq.AppService.s;
import com.qq.d.h.a;
import com.qq.g.c;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class af {

    /* renamed from: a  reason: collision with root package name */
    private a f324a;

    private af() {
        this.f324a = null;
        this.f324a = new a();
    }

    public static void a(Context context, c cVar) {
        Cursor query = context.getContentResolver().query(Settings.System.CONTENT_URI, null, null, null, null);
        if (query == null) {
            cVar.a(8);
            return;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(s.a(query.getCount()));
        for (boolean moveToFirst = query.moveToFirst(); moveToFirst; moveToFirst = query.moveToNext()) {
            int i = query.getInt(query.getColumnIndex("_id"));
            String string = query.getString(query.getColumnIndex("name"));
            String string2 = query.getString(query.getColumnIndex("value"));
            arrayList.add(s.a(i));
            arrayList.add(s.a(string));
            arrayList.add(s.a(string2));
        }
        query.close();
        cVar.a(arrayList);
        cVar.a(0);
    }

    public static void b(Context context, c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        context.getContentResolver().delete(Settings.System.CONTENT_URI, null, null);
        int h = cVar.h();
        if (cVar.e() < (h * 2) + 1) {
            cVar.a(1);
            return;
        }
        ContentValues contentValues = new ContentValues();
        for (int i = 0; i < h; i++) {
            String j = cVar.j();
            String j2 = cVar.j();
            contentValues.put("name", j);
            contentValues.put("value", j2);
            context.getContentResolver().insert(Settings.System.CONTENT_URI, contentValues);
            contentValues.clear();
        }
        cVar.a(0);
    }
}
