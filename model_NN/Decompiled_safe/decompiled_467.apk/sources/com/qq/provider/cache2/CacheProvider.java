package com.qq.provider.cache2;

import android.content.ContentProvider;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class CacheProvider extends ContentProvider {

    /* renamed from: a  reason: collision with root package name */
    public static final Uri f335a = Uri.parse("content://com.qq.provider.cache2/app_ico");
    private static final UriMatcher d = new UriMatcher(-1);
    private CacheDBHelper b;
    private SQLiteDatabase c;

    static {
        d.addURI("com.qq.provider.cache2", CacheDBHelper.APP_ICON_CACHE, 1);
        d.addURI("com.qq.provider.cache2", "app_ico/#", 2);
    }

    public boolean onCreate() {
        this.b = new CacheDBHelper(getContext());
        this.c = this.b.getWritableDatabase();
        return this.c != null;
    }

    /* JADX INFO: finally extract failed */
    public int bulkInsert(Uri uri, ContentValues[] contentValuesArr) {
        if (this.c == null) {
            return 0;
        }
        this.c.beginTransaction();
        try {
            for (ContentValues insert : contentValuesArr) {
                insert(uri, insert);
            }
            this.c.setTransactionSuccessful();
            this.c.endTransaction();
            return r1;
        } catch (Throwable th) {
            this.c.endTransaction();
            throw th;
        }
    }

    public ContentProviderResult[] applyBatch(ArrayList<ContentProviderOperation> arrayList) {
        if (this.c == null) {
            return null;
        }
        this.c.beginTransaction();
        try {
            ContentProviderResult[] applyBatch = super.applyBatch(arrayList);
            this.c.setTransactionSuccessful();
            return applyBatch;
        } finally {
            this.c.endTransaction();
        }
    }

    public Cursor query(Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        String str3;
        SQLiteQueryBuilder sQLiteQueryBuilder = new SQLiteQueryBuilder();
        if (TextUtils.isEmpty(str2)) {
            str3 = "_id";
        } else {
            str3 = str2;
        }
        switch (d.match(uri)) {
            case 1:
                sQLiteQueryBuilder.setTables(CacheDBHelper.APP_ICON_CACHE);
                Cursor query = sQLiteQueryBuilder.query(this.c, strArr, str, strArr2, null, null, str3);
                query.setNotificationUri(getContext().getContentResolver(), uri);
                return query;
            case 2:
                sQLiteQueryBuilder.setTables(CacheDBHelper.APP_ICON_CACHE);
                sQLiteQueryBuilder.appendWhere("_id=" + uri.getPathSegments().get(1));
                Cursor query2 = sQLiteQueryBuilder.query(this.c, strArr, str, strArr2, null, null, str3);
                query2.setNotificationUri(getContext().getContentResolver(), uri);
                return query2;
            default:
                return null;
        }
    }

    public String getType(Uri uri) {
        switch (d.match(uri)) {
            case 1:
                return "vnd.android.cursor.dir/vnd.qq.android.EBOOKS";
            case 2:
                return "vnd.android.cursor.item/vnd.qq.android.EBOOKS";
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }

    private long a(ContentValues contentValues) {
        if (contentValues == null || !contentValues.containsKey("pkg")) {
            return 0;
        }
        if (!contentValues.containsKey("icon")) {
            contentValues.putNull("icon");
        }
        if (!contentValues.containsKey("time")) {
            contentValues.put("time", Long.valueOf(System.currentTimeMillis()));
        }
        return this.c.insert(CacheDBHelper.APP_ICON_CACHE, null, contentValues);
    }

    public Uri insert(Uri uri, ContentValues contentValues) {
        ContentValues contentValues2;
        Uri uri2;
        if (contentValues != null) {
            contentValues2 = new ContentValues(contentValues);
        } else {
            contentValues2 = new ContentValues();
        }
        switch (d.match(uri)) {
            case 1:
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        long a2 = a(contentValues2);
        if (a2 > 0) {
            uri2 = ContentUris.withAppendedId(f335a, a2);
            getContext().getContentResolver().notifyChange(uri2, null);
        } else {
            uri2 = null;
        }
        if (uri2 != null) {
            return uri2;
        }
        throw new SQLException("Failed to insert row into " + uri);
    }

    public int delete(Uri uri, String str, String[] strArr) {
        int delete;
        switch (d.match(uri)) {
            case 1:
                delete = this.c.delete(CacheDBHelper.APP_ICON_CACHE, str, strArr);
                break;
            case 2:
                delete = this.c.delete(CacheDBHelper.APP_ICON_CACHE, "_id=" + uri.getPathSegments().get(1) + (!TextUtils.isEmpty(str) ? " AND (" + str + ")" : Constants.STR_EMPTY), strArr);
                break;
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return delete;
    }

    public int update(Uri uri, ContentValues contentValues, String str, String[] strArr) {
        int update;
        switch (d.match(uri)) {
            case 1:
                update = this.c.update(CacheDBHelper.APP_ICON_CACHE, contentValues, str, strArr);
                break;
            case 2:
                update = this.c.update(CacheDBHelper.APP_ICON_CACHE, contentValues, "_id=" + uri.getPathSegments().get(1) + (!TextUtils.isEmpty(str) ? " AND (" + str + ")" : Constants.STR_EMPTY), strArr);
                break;
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return update;
    }
}
