package com.qq.d.g;

import android.os.SystemProperties;
import android.text.TextUtils;
import com.tencent.connect.common.Constants;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* compiled from: ProGuard */
public abstract class r {

    /* renamed from: a  reason: collision with root package name */
    protected final String f285a = "cm";
    protected r b;

    public abstract boolean a(Map<String, String> map, boolean z, s sVar);

    public r a(r rVar) {
        this.b = rVar;
        return this.b;
    }

    /* access modifiers changed from: protected */
    public String b(String str) {
        Matcher matcher = Pattern.compile("(\\d+\\.)+\\d+").matcher(str);
        if (matcher.find()) {
            return matcher.group();
        }
        return Constants.STR_EMPTY;
    }

    /* access modifiers changed from: protected */
    public void a(Map<String, String> map, String str, String str2) {
        if (str2 == null) {
            str2 = Constants.STR_EMPTY;
        }
        map.put(str, str2.replaceAll("[\\[\\]]", Constants.STR_EMPTY));
    }

    /* access modifiers changed from: protected */
    public boolean c(String str) {
        return !TextUtils.isEmpty(SystemProperties.get("ro.build.version.nanji.display"));
    }
}
