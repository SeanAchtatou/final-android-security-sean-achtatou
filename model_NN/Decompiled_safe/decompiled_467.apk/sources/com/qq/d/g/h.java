package com.qq.d.g;

import java.util.Map;

/* compiled from: ProGuard */
public class h extends r {
    public boolean a(Map<String, String> map, boolean z, s sVar) {
        boolean z2;
        boolean z3;
        boolean z4 = false;
        String c = sVar.c();
        if (c.toLowerCase().contains("lezo")) {
            if (!z) {
                a(map, "rombrand", "lezo");
                a(map, "romversion", b(c));
            }
            z2 = true;
        } else {
            z2 = false;
        }
        if (this.b == null) {
            return z2;
        }
        r rVar = this.b;
        if (z || z2) {
            z3 = true;
        } else {
            z3 = false;
        }
        boolean a2 = rVar.a(map, z3, sVar);
        if (z2 || a2) {
            z4 = true;
        }
        return z4;
    }
}
