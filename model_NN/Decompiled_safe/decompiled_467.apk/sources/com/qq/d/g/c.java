package com.qq.d.g;

import android.os.SystemProperties;
import android.text.TextUtils;
import java.util.Map;

/* compiled from: ProGuard */
public class c extends r {
    public r a(r rVar) {
        throw new RuntimeException("Cm handler must be at the end of the responsbility chain");
    }

    public boolean a(Map<String, String> map, boolean z, s sVar) {
        boolean z2 = false;
        String str = SystemProperties.get("ro.modversion");
        if (!TextUtils.isEmpty(str)) {
            z2 = true;
            if (!z) {
                a(map, "rombrand", "cm");
                a(map, "romversion", str);
            } else {
                a(map, "rombase", "cm");
                a(map, "rombasever", str);
            }
        }
        return z2;
    }
}
