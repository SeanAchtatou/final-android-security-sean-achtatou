package com.qq.d;

import com.qq.AppService.s;
import com.tencent.tmsecurelite.commom.DataEntity;
import java.util.ArrayList;
import org.json.JSONArray;

/* compiled from: ProGuard */
public class c {

    /* renamed from: a  reason: collision with root package name */
    public boolean f277a = false;
    public long b = 0;
    public String c = null;
    public String d = null;
    public String e = null;
    public ArrayList<String> f = new ArrayList<>();
    public int g = 0;

    public c(int i, DataEntity dataEntity) {
        try {
            this.g = i;
            this.f277a = dataEntity.getBoolean("rubbish.suggest");
            this.b = dataEntity.getLong("rubbish.size");
            this.c = dataEntity.getString("app.name");
            this.d = dataEntity.getString("app.pkg");
            this.e = dataEntity.getString("rubbish.desc");
            JSONArray jSONArray = dataEntity.getJSONArray("rubbish.path.array");
            int length = jSONArray.length();
            for (int i2 = 0; i2 < length; i2++) {
                this.f.add((String) jSONArray.get(i2));
            }
        } catch (Throwable th) {
        }
    }

    public void a(ArrayList<byte[]> arrayList) {
        int i;
        byte[] a2 = s.a(this.c);
        arrayList.add(s.a(a2.length));
        arrayList.add(a2);
        byte[] a3 = s.a(this.d);
        arrayList.add(s.a(a3.length));
        arrayList.add(a3);
        byte[] a4 = s.a(this.e);
        arrayList.add(s.a(a4.length));
        arrayList.add(a4);
        arrayList.add(s.a(4));
        arrayList.add(s.a(this.f277a ? 1 : 0));
        arrayList.add(s.a(8));
        arrayList.add(s.a(this.b));
        arrayList.add(s.a(4));
        int i2 = this.g;
        if (i2 > 1) {
            i2 = 1 << (i2 - 1);
        }
        arrayList.add(s.a(i2));
        if (this.f != null) {
            i = this.f.size();
        } else {
            i = 0;
        }
        arrayList.add(s.a(4));
        arrayList.add(s.a(i));
        for (int i3 = 0; i3 < i; i3++) {
            byte[] a5 = s.a(this.f.get(i3));
            arrayList.add(s.a(a5.length));
            arrayList.add(a5);
        }
    }
}
