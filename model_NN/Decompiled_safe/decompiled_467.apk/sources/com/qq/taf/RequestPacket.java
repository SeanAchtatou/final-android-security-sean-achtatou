package com.qq.taf;

import com.qq.taf.jce.HexUtil;
import com.qq.taf.jce.JceDisplayer;
import com.qq.taf.jce.JceInputStream;
import com.qq.taf.jce.JceOutputStream;
import com.qq.taf.jce.JceStruct;
import com.qq.taf.jce.JceUtil;
import com.tencent.connect.common.Constants;
import java.util.HashMap;
import java.util.Map;

public final class RequestPacket extends JceStruct {
    static final /* synthetic */ boolean $assertionsDisabled;
    static Map<String, String> cache_context = null;
    static byte[] cache_sBuffer = null;
    public byte cPacketType = 0;
    public Map<String, String> context;
    public int iMessageType = 0;
    public int iRequestId = 0;
    public int iTimeout = 0;
    public short iVersion = 0;
    public byte[] sBuffer;
    public String sFuncName = null;
    public String sServantName = null;
    public Map<String, String> status;

    static {
        boolean z;
        if (!RequestPacket.class.desiredAssertionStatus()) {
            z = true;
        } else {
            z = false;
        }
        $assertionsDisabled = z;
    }

    public RequestPacket() {
    }

    public RequestPacket(short iVersion2, byte cPacketType2, int iMessageType2, int iRequestId2, String sServantName2, String sFuncName2, byte[] sBuffer2, int iTimeout2, Map<String, String> context2, Map<String, String> status2) {
        this.iVersion = iVersion2;
        this.cPacketType = cPacketType2;
        this.iMessageType = iMessageType2;
        this.iRequestId = iRequestId2;
        this.sServantName = sServantName2;
        this.sFuncName = sFuncName2;
        this.sBuffer = sBuffer2;
        this.iTimeout = iTimeout2;
        this.context = context2;
        this.status = status2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceUtil.equals(int, int):boolean
     arg types: [int, short]
     candidates:
      com.qq.taf.jce.JceUtil.equals(byte, byte):boolean
      com.qq.taf.jce.JceUtil.equals(char, char):boolean
      com.qq.taf.jce.JceUtil.equals(double, double):boolean
      com.qq.taf.jce.JceUtil.equals(float, float):boolean
      com.qq.taf.jce.JceUtil.equals(long, long):boolean
      com.qq.taf.jce.JceUtil.equals(java.lang.Object, java.lang.Object):boolean
      com.qq.taf.jce.JceUtil.equals(short, short):boolean
      com.qq.taf.jce.JceUtil.equals(boolean, boolean):boolean
      com.qq.taf.jce.JceUtil.equals(int, int):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceUtil.equals(int, int):boolean
     arg types: [int, byte]
     candidates:
      com.qq.taf.jce.JceUtil.equals(byte, byte):boolean
      com.qq.taf.jce.JceUtil.equals(char, char):boolean
      com.qq.taf.jce.JceUtil.equals(double, double):boolean
      com.qq.taf.jce.JceUtil.equals(float, float):boolean
      com.qq.taf.jce.JceUtil.equals(long, long):boolean
      com.qq.taf.jce.JceUtil.equals(java.lang.Object, java.lang.Object):boolean
      com.qq.taf.jce.JceUtil.equals(short, short):boolean
      com.qq.taf.jce.JceUtil.equals(boolean, boolean):boolean
      com.qq.taf.jce.JceUtil.equals(int, int):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceUtil.equals(java.lang.Object, java.lang.Object):boolean
     arg types: [int, java.lang.String]
     candidates:
      com.qq.taf.jce.JceUtil.equals(byte, byte):boolean
      com.qq.taf.jce.JceUtil.equals(char, char):boolean
      com.qq.taf.jce.JceUtil.equals(double, double):boolean
      com.qq.taf.jce.JceUtil.equals(float, float):boolean
      com.qq.taf.jce.JceUtil.equals(int, int):boolean
      com.qq.taf.jce.JceUtil.equals(long, long):boolean
      com.qq.taf.jce.JceUtil.equals(short, short):boolean
      com.qq.taf.jce.JceUtil.equals(boolean, boolean):boolean
      com.qq.taf.jce.JceUtil.equals(java.lang.Object, java.lang.Object):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceUtil.equals(java.lang.Object, java.lang.Object):boolean
     arg types: [int, byte[]]
     candidates:
      com.qq.taf.jce.JceUtil.equals(byte, byte):boolean
      com.qq.taf.jce.JceUtil.equals(char, char):boolean
      com.qq.taf.jce.JceUtil.equals(double, double):boolean
      com.qq.taf.jce.JceUtil.equals(float, float):boolean
      com.qq.taf.jce.JceUtil.equals(int, int):boolean
      com.qq.taf.jce.JceUtil.equals(long, long):boolean
      com.qq.taf.jce.JceUtil.equals(short, short):boolean
      com.qq.taf.jce.JceUtil.equals(boolean, boolean):boolean
      com.qq.taf.jce.JceUtil.equals(java.lang.Object, java.lang.Object):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceUtil.equals(java.lang.Object, java.lang.Object):boolean
     arg types: [int, java.util.Map<java.lang.String, java.lang.String>]
     candidates:
      com.qq.taf.jce.JceUtil.equals(byte, byte):boolean
      com.qq.taf.jce.JceUtil.equals(char, char):boolean
      com.qq.taf.jce.JceUtil.equals(double, double):boolean
      com.qq.taf.jce.JceUtil.equals(float, float):boolean
      com.qq.taf.jce.JceUtil.equals(int, int):boolean
      com.qq.taf.jce.JceUtil.equals(long, long):boolean
      com.qq.taf.jce.JceUtil.equals(short, short):boolean
      com.qq.taf.jce.JceUtil.equals(boolean, boolean):boolean
      com.qq.taf.jce.JceUtil.equals(java.lang.Object, java.lang.Object):boolean */
    public boolean equals(Object o) {
        RequestPacket t = (RequestPacket) o;
        if (!JceUtil.equals(1, (int) t.iVersion) || !JceUtil.equals(1, (int) t.cPacketType) || !JceUtil.equals(1, t.iMessageType) || !JceUtil.equals(1, t.iRequestId) || !JceUtil.equals((Object) 1, (Object) t.sServantName) || !JceUtil.equals((Object) 1, (Object) t.sFuncName) || !JceUtil.equals((Object) 1, (Object) t.sBuffer) || !JceUtil.equals(1, t.iTimeout) || !JceUtil.equals((Object) 1, (Object) t.context) || !JceUtil.equals((Object) 1, (Object) t.status)) {
            return false;
        }
        return true;
    }

    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            if ($assertionsDisabled) {
                return null;
            }
            throw new AssertionError();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceOutputStream.write(java.util.Map, int):void
     arg types: [java.util.Map<java.lang.String, java.lang.String>, int]
     candidates:
      com.qq.taf.jce.JceOutputStream.write(byte, int):void
      com.qq.taf.jce.JceOutputStream.write(double, int):void
      com.qq.taf.jce.JceOutputStream.write(float, int):void
      com.qq.taf.jce.JceOutputStream.write(int, int):void
      com.qq.taf.jce.JceOutputStream.write(long, int):void
      com.qq.taf.jce.JceOutputStream.write(com.qq.taf.jce.JceStruct, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Boolean, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Byte, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Double, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Float, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Integer, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Long, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Object, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Short, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.String, int):void
      com.qq.taf.jce.JceOutputStream.write(java.util.Collection, int):void
      com.qq.taf.jce.JceOutputStream.write(short, int):void
      com.qq.taf.jce.JceOutputStream.write(boolean, int):void
      com.qq.taf.jce.JceOutputStream.write(byte[], int):void
      com.qq.taf.jce.JceOutputStream.write(double[], int):void
      com.qq.taf.jce.JceOutputStream.write(float[], int):void
      com.qq.taf.jce.JceOutputStream.write(int[], int):void
      com.qq.taf.jce.JceOutputStream.write(long[], int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Object[], int):void
      com.qq.taf.jce.JceOutputStream.write(short[], int):void
      com.qq.taf.jce.JceOutputStream.write(boolean[], int):void
      com.qq.taf.jce.JceOutputStream.write(java.util.Map, int):void */
    public void writeTo(JceOutputStream _os) {
        _os.write(this.iVersion, 1);
        _os.write(this.cPacketType, 2);
        _os.write(this.iMessageType, 3);
        _os.write(this.iRequestId, 4);
        _os.write(this.sServantName, 5);
        _os.write(this.sFuncName, 6);
        _os.write(this.sBuffer, 7);
        _os.write(this.iTimeout, 8);
        _os.write((Map) this.context, 9);
        _os.write((Map) this.status, 10);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
     arg types: [short, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
     arg types: [byte, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
     arg types: [byte[], int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
     arg types: [java.util.Map<java.lang.String, java.lang.String>, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object */
    public void readFrom(JceInputStream _is) {
        try {
            this.iVersion = _is.read(this.iVersion, 1, true);
            this.cPacketType = _is.read(this.cPacketType, 2, true);
            this.iMessageType = _is.read(this.iMessageType, 3, true);
            this.iRequestId = _is.read(this.iRequestId, 4, true);
            this.sServantName = _is.readString(5, true);
            this.sFuncName = _is.readString(6, true);
            if (cache_sBuffer == null) {
                cache_sBuffer = new byte[]{0};
            }
            this.sBuffer = _is.read(cache_sBuffer, 7, true);
            this.iTimeout = _is.read(this.iTimeout, 8, true);
            if (cache_context == null) {
                cache_context = new HashMap();
                cache_context.put(Constants.STR_EMPTY, Constants.STR_EMPTY);
            }
            this.context = (Map) _is.read((Object) cache_context, 9, true);
            if (cache_context == null) {
                cache_context = new HashMap();
                cache_context.put(Constants.STR_EMPTY, Constants.STR_EMPTY);
            }
            this.status = (Map) _is.read((Object) cache_context, 10, true);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("RequestPacket decode error " + HexUtil.bytes2HexStr(this.sBuffer));
            throw new RuntimeException(e);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceDisplayer.display(java.util.Map, java.lang.String):com.qq.taf.jce.JceDisplayer
     arg types: [java.util.Map<java.lang.String, java.lang.String>, java.lang.String]
     candidates:
      com.qq.taf.jce.JceDisplayer.display(byte, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(char, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(double, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(float, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(int, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(long, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(com.qq.taf.jce.JceStruct, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(java.lang.Object, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(java.lang.String, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(java.util.Collection, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(short, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(boolean, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(byte[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(char[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(double[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(float[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(int[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(long[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(java.lang.Object[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(short[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(java.util.Map, java.lang.String):com.qq.taf.jce.JceDisplayer */
    public void display(StringBuilder _os, int _level) {
        JceDisplayer _ds = new JceDisplayer(_os, _level);
        _ds.display(this.iVersion, "iVersion");
        _ds.display(this.cPacketType, "cPacketType");
        _ds.display(this.iMessageType, "iMessageType");
        _ds.display(this.iRequestId, "iRequestId");
        _ds.display(this.sServantName, "sServantName");
        _ds.display(this.sFuncName, "sFuncName");
        _ds.display(this.sBuffer, "sBuffer");
        _ds.display(this.iTimeout, "iTimeout");
        _ds.display((Map) this.context, "context");
        _ds.display((Map) this.status, "status");
    }
}
