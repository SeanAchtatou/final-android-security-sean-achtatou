package com.qq.util;

/* compiled from: ProGuard */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private static final byte[] f361a = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 43, 47};
    private static final byte[] b = new byte[128];

    static {
        for (int i = 0; i < 128; i++) {
            b[i] = -1;
        }
        for (int i2 = 65; i2 <= 90; i2++) {
            b[i2] = (byte) (i2 - 65);
        }
        for (int i3 = 97; i3 <= 122; i3++) {
            b[i3] = (byte) ((i3 - 97) + 26);
        }
        for (int i4 = 48; i4 <= 57; i4++) {
            b[i4] = (byte) ((i4 - 48) + 52);
        }
        b[43] = 62;
        b[47] = 63;
    }

    public static byte[] a(byte[] bArr) {
        byte[] bArr2;
        int i = 0;
        int length = bArr.length % 3;
        if (length == 0) {
            bArr2 = new byte[((bArr.length * 4) / 3)];
        } else {
            bArr2 = new byte[(((bArr.length / 3) + 1) * 4)];
        }
        int length2 = bArr.length - length;
        int i2 = 0;
        while (i2 < length2) {
            byte b2 = bArr[i2] & 255;
            byte b3 = bArr[i2 + 1] & 255;
            byte b4 = bArr[i2 + 2] & 255;
            bArr2[i] = f361a[(b2 >>> 2) & 63];
            bArr2[i + 1] = f361a[((b2 << 4) | (b3 >>> 4)) & 63];
            bArr2[i + 2] = f361a[((b3 << 2) | (b4 >>> 6)) & 63];
            bArr2[i + 3] = f361a[b4 & 63];
            i2 += 3;
            i += 4;
        }
        switch (length) {
            case 1:
                byte b5 = bArr[bArr.length - 1] & 255;
                bArr2[bArr2.length - 4] = f361a[(b5 >>> 2) & 63];
                bArr2[bArr2.length - 3] = f361a[(b5 << 4) & 63];
                bArr2[bArr2.length - 2] = 61;
                bArr2[bArr2.length - 1] = 61;
                break;
            case 2:
                byte b6 = bArr[bArr.length - 2] & 255;
                byte b7 = bArr[bArr.length - 1] & 255;
                bArr2[bArr2.length - 4] = f361a[(b6 >>> 2) & 63];
                bArr2[bArr2.length - 3] = f361a[((b6 << 4) | (b7 >>> 4)) & 63];
                bArr2[bArr2.length - 2] = f361a[(b7 << 2) & 63];
                bArr2[bArr2.length - 1] = 61;
                break;
        }
        return bArr2;
    }

    public static byte[] b(byte[] bArr) {
        byte[] bArr2;
        int i = 0;
        byte[] c = c(bArr);
        if (c[c.length - 2] == 61) {
            bArr2 = new byte[((((c.length / 4) - 1) * 3) + 1)];
        } else if (c[c.length - 1] == 61) {
            bArr2 = new byte[((((c.length / 4) - 1) * 3) + 2)];
        } else {
            bArr2 = new byte[((c.length / 4) * 3)];
        }
        int i2 = 0;
        while (i2 < c.length - 4) {
            byte b2 = b[c[i2]];
            byte b3 = b[c[i2 + 1]];
            byte b4 = b[c[i2 + 2]];
            byte b5 = b[c[i2 + 3]];
            bArr2[i] = (byte) ((b2 << 2) | (b3 >> 4));
            bArr2[i + 1] = (byte) ((b3 << 4) | (b4 >> 2));
            bArr2[i + 2] = (byte) ((b4 << 6) | b5);
            i2 += 4;
            i += 3;
        }
        if (c[c.length - 2] == 61) {
            bArr2[bArr2.length - 1] = (byte) ((b[c[c.length - 4]] << 2) | (b[c[c.length - 3]] >> 4));
        } else if (c[c.length - 1] == 61) {
            byte b6 = b[c[c.length - 4]];
            byte b7 = b[c[c.length - 3]];
            byte b8 = b[c[c.length - 2]];
            bArr2[bArr2.length - 2] = (byte) ((b6 << 2) | (b7 >> 4));
            bArr2[bArr2.length - 1] = (byte) ((b7 << 4) | (b8 >> 2));
        } else {
            byte b9 = b[c[c.length - 4]];
            byte b10 = b[c[c.length - 3]];
            byte b11 = b[c[c.length - 2]];
            byte b12 = b[c[c.length - 1]];
            bArr2[bArr2.length - 3] = (byte) ((b9 << 2) | (b10 >> 4));
            bArr2[bArr2.length - 2] = (byte) ((b10 << 4) | (b11 >> 2));
            bArr2[bArr2.length - 1] = (byte) ((b11 << 6) | b12);
        }
        return bArr2;
    }

    public static byte[] a(String str) {
        byte[] bArr;
        int i = 0;
        String b2 = b(str);
        if (b2.charAt(b2.length() - 2) == '=') {
            bArr = new byte[((((b2.length() / 4) - 1) * 3) + 1)];
        } else if (b2.charAt(b2.length() - 1) == '=') {
            bArr = new byte[((((b2.length() / 4) - 1) * 3) + 2)];
        } else {
            bArr = new byte[((b2.length() / 4) * 3)];
        }
        int i2 = 0;
        while (i2 < b2.length() - 4) {
            byte b3 = b[b2.charAt(i2)];
            byte b4 = b[b2.charAt(i2 + 1)];
            byte b5 = b[b2.charAt(i2 + 2)];
            byte b6 = b[b2.charAt(i2 + 3)];
            bArr[i] = (byte) ((b3 << 2) | (b4 >> 4));
            bArr[i + 1] = (byte) ((b4 << 4) | (b5 >> 2));
            bArr[i + 2] = (byte) ((b5 << 6) | b6);
            i2 += 4;
            i += 3;
        }
        if (b2.charAt(b2.length() - 2) == '=') {
            bArr[bArr.length - 1] = (byte) ((b[b2.charAt(b2.length() - 4)] << 2) | (b[b2.charAt(b2.length() - 3)] >> 4));
        } else if (b2.charAt(b2.length() - 1) == '=') {
            byte b7 = b[b2.charAt(b2.length() - 4)];
            byte b8 = b[b2.charAt(b2.length() - 3)];
            byte b9 = b[b2.charAt(b2.length() - 2)];
            bArr[bArr.length - 2] = (byte) ((b7 << 2) | (b8 >> 4));
            bArr[bArr.length - 1] = (byte) ((b8 << 4) | (b9 >> 2));
        } else {
            byte b10 = b[b2.charAt(b2.length() - 4)];
            byte b11 = b[b2.charAt(b2.length() - 3)];
            byte b12 = b[b2.charAt(b2.length() - 2)];
            byte b13 = b[b2.charAt(b2.length() - 1)];
            bArr[bArr.length - 3] = (byte) ((b10 << 2) | (b11 >> 4));
            bArr[bArr.length - 2] = (byte) ((b11 << 4) | (b12 >> 2));
            bArr[bArr.length - 1] = (byte) ((b12 << 6) | b13);
        }
        return bArr;
    }

    private static byte[] c(byte[] bArr) {
        byte[] bArr2 = new byte[bArr.length];
        int i = 0;
        for (int i2 = 0; i2 < bArr.length; i2++) {
            if (a(bArr[i2])) {
                bArr2[i] = bArr[i2];
                i++;
            }
        }
        byte[] bArr3 = new byte[i];
        System.arraycopy(bArr2, 0, bArr3, 0, i);
        return bArr3;
    }

    private static String b(String str) {
        StringBuffer stringBuffer = new StringBuffer();
        int length = str.length();
        for (int i = 0; i < length; i++) {
            if (a((byte) str.charAt(i))) {
                stringBuffer.append(str.charAt(i));
            }
        }
        return stringBuffer.toString();
    }

    private static boolean a(byte b2) {
        if (b2 == 61) {
            return true;
        }
        if (b2 < 0 || b2 >= 128) {
            return false;
        }
        if (b[b2] == -1) {
            return false;
        }
        return true;
    }
}
