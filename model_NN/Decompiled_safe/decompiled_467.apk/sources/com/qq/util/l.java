package com.qq.util;

import android.util.Log;
import com.qq.AppService.s;
import com.qq.c.e;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/* compiled from: ProGuard */
public class l {
    public static int a(String str, boolean z) {
        boolean mkdir;
        boolean mkdir2;
        File file = new File(str);
        if (!file.exists()) {
            File parentFile = file.getParentFile();
            if (parentFile == null) {
                return 3;
            }
            if (parentFile.exists()) {
                if (file.isFile()) {
                    mkdir2 = file.createNewFile();
                } else {
                    mkdir2 = file.mkdir();
                }
                if (!mkdir2) {
                    return 4;
                }
                return 0;
            } else if (!parentFile.mkdirs()) {
                return 4;
            } else {
                if (file.isFile()) {
                    mkdir = file.createNewFile();
                } else {
                    mkdir = file.mkdir();
                }
                if (!mkdir) {
                    return 4;
                }
                return 0;
            }
        } else if (z) {
            return 0;
        } else {
            return 6;
        }
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:19:0x005c */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:26:0x0075 */
    /* JADX WARN: Type inference failed for: r6v1, types: [java.lang.String] */
    /* JADX WARN: Type inference failed for: r6v2, types: [java.io.BufferedOutputStream] */
    /* JADX WARN: Type inference failed for: r6v3, types: [java.io.BufferedOutputStream] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* JADX WARNING: Multi-variable type inference failed */
    public static int a(java.io.BufferedInputStream r10, java.lang.String r11, java.io.BufferedOutputStream r12) {
        /*
            r0 = 12
            byte[] r0 = new byte[r0]
            int r1 = r10.read(r0)     // Catch:{ Exception -> 0x000e }
            int r2 = r0.length
            if (r1 >= r2) goto L_0x0015
            r0 = 10
        L_0x000d:
            return r0
        L_0x000e:
            r0 = move-exception
            r0.printStackTrace()
            r0 = 8
            goto L_0x000d
        L_0x0015:
            r1 = 4
            long r4 = com.qq.AppService.s.c(r0, r1)
            long r0 = com.qq.provider.n.a(r11)
            java.lang.String r2 = "com.qq.connect"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r6 = " avb=="
            java.lang.StringBuilder r3 = r3.append(r6)
            java.lang.StringBuilder r3 = r3.append(r0)
            java.lang.String r6 = " length:"
            java.lang.StringBuilder r3 = r3.append(r6)
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            android.util.Log.d(r2, r3)
            int r0 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
            if (r0 > 0) goto L_0x0047
            r0 = 11
            goto L_0x000d
        L_0x0047:
            r0 = 0
            byte[] r0 = com.qq.AppService.s.a(r0)     // Catch:{ IOException -> 0x0057 }
            a(r12, r0)     // Catch:{ IOException -> 0x0057 }
        L_0x004f:
            r0 = 0
            int r0 = (r4 > r0 ? 1 : (r4 == r0 ? 0 : -1))
            if (r0 != 0) goto L_0x005c
            r0 = 0
            goto L_0x000d
        L_0x0057:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x004f
        L_0x005c:
            java.io.BufferedOutputStream r6 = new java.io.BufferedOutputStream     // Catch:{ Exception -> 0x0087 }
            java.io.FileOutputStream r0 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0087 }
            r0.<init>(r11)     // Catch:{ Exception -> 0x0087 }
            r6.<init>(r0)     // Catch:{ Exception -> 0x0087 }
            r0 = 16384(0x4000, float:2.2959E-41)
            byte[] r7 = new byte[r0]
            r1 = 0
            r0 = 0
        L_0x006d:
            r3 = 100
            if (r0 >= r3) goto L_0x00a6
            int r3 = (r1 > r4 ? 1 : (r1 == r4 ? 0 : -1))
            if (r3 >= 0) goto L_0x00a6
            int r3 = r7.length     // Catch:{ Exception -> 0x00b4 }
            long r8 = (long) r3     // Catch:{ Exception -> 0x00b4 }
            long r8 = r8 + r1
            int r3 = (r8 > r4 ? 1 : (r8 == r4 ? 0 : -1))
            if (r3 > 0) goto L_0x008a
            r3 = 0
            int r8 = r7.length     // Catch:{ Exception -> 0x00b4 }
            int r3 = r10.read(r7, r3, r8)     // Catch:{ Exception -> 0x00b4 }
        L_0x0082:
            if (r3 > 0) goto L_0x0093
        L_0x0084:
            int r0 = r0 + 1
            goto L_0x006d
        L_0x0087:
            r0 = move-exception
            r0 = 4
            goto L_0x000d
        L_0x008a:
            r3 = 0
            long r8 = r4 - r1
            int r8 = (int) r8     // Catch:{ Exception -> 0x00b4 }
            int r3 = r10.read(r7, r3, r8)     // Catch:{ Exception -> 0x00b4 }
            goto L_0x0082
        L_0x0093:
            long r8 = (long) r3
            long r1 = r1 + r8
            r0 = 0
            r8 = 0
            r6.write(r7, r8, r3)     // Catch:{ Exception -> 0x009b }
            goto L_0x0084
        L_0x009b:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ Exception -> 0x00b4 }
            r6.close()     // Catch:{ Exception -> 0x00b4 }
            r0 = 11
            goto L_0x000d
        L_0x00a6:
            r6.close()     // Catch:{ Exception -> 0x00b4 }
            int r0 = (r1 > r4 ? 1 : (r1 == r4 ? 0 : -1))
            if (r0 >= 0) goto L_0x00b1
            r0 = 10
            goto L_0x000d
        L_0x00b1:
            r0 = 0
            goto L_0x000d
        L_0x00b4:
            r0 = move-exception
            r0.printStackTrace()
            if (r6 == 0) goto L_0x00bd
            r6.close()     // Catch:{ IOException -> 0x00c1 }
        L_0x00bd:
            r0 = 8
            goto L_0x000d
        L_0x00c1:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00bd
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qq.util.l.a(java.io.BufferedInputStream, java.lang.String, java.io.BufferedOutputStream):int");
    }

    public static int a(BufferedOutputStream bufferedOutputStream, String str) {
        int i;
        int read;
        byte[] bArr = new byte[4];
        FileInputStream fileInputStream = null;
        File file = new File(str);
        for (int i2 = 0; i2 < 8 && !file.exists(); i2++) {
            e.a(str, 509);
            if (i2 > 6 && !file.exists()) {
                return 3;
            }
        }
        long length = file.length();
        byte[] bArr2 = new byte[16384];
        int i3 = 0;
        while (true) {
            if (i3 >= 15) {
                break;
            }
            try {
                fileInputStream = new FileInputStream(file);
            } catch (Throwable th) {
            }
            if (fileInputStream != null) {
                break;
            } else if (i3 >= 14) {
                Log.d("com.qq.connect", "try download_file_s over! mark file length = 0 ;");
                length = 0;
                break;
            } else {
                try {
                    Thread.sleep(35);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                e.a(str, 509);
                i3++;
            }
        }
        byte[] a2 = s.a(length);
        if (length > 0 && fileInputStream == null) {
            return 8;
        }
        try {
            a(bufferedOutputStream, s.a(0));
            a(bufferedOutputStream, s.a(8));
            bufferedOutputStream.write(a2);
            int i4 = 0;
            int i5 = 0;
            int i6 = 0;
            while (i4 < 100 && ((long) i6) < length) {
                if (((long) (i6 + 16384)) < length) {
                    read = fileInputStream.read(bArr2, 0, 16384);
                } else {
                    read = fileInputStream.read(bArr2, 0, (int) (length - ((long) i6)));
                }
                if (read > 0) {
                    bufferedOutputStream.write(bArr2, 0, read);
                    if (i5 % 4 == 0) {
                        bufferedOutputStream.flush();
                    }
                    i6 += read;
                    i5++;
                    i4 = 0;
                }
                i4++;
            }
            i = 0;
        } catch (IOException e2) {
            e2.printStackTrace();
            i = 11;
        }
        if (fileInputStream == null) {
            return i;
        }
        try {
            fileInputStream.close();
            return i;
        } catch (IOException e3) {
            e3.printStackTrace();
            return i;
        }
    }

    public static int b(BufferedOutputStream bufferedOutputStream, String str) {
        int i;
        int read;
        byte[] bArr = new byte[4];
        File file = new File(str);
        if (!file.exists()) {
            return 3;
        }
        long length = file.length();
        byte[] a2 = s.a(length);
        byte[] bArr2 = new byte[16384];
        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            try {
                a(bufferedOutputStream, s.a(0));
                a(bufferedOutputStream, s.a(8));
                bufferedOutputStream.write(a2);
                int i2 = 0;
                int i3 = 0;
                int i4 = 0;
                while (i2 < 100 && ((long) i4) < length) {
                    if (((long) (i4 + 16384)) < length) {
                        read = fileInputStream.read(bArr2, 0, 16384);
                    } else {
                        read = fileInputStream.read(bArr2, 0, (int) (length - ((long) i4)));
                    }
                    if (read > 0) {
                        bufferedOutputStream.write(bArr2, 0, read);
                        if (i3 % 4 == 0) {
                            bufferedOutputStream.flush();
                        }
                        i4 += read;
                        i3++;
                        i2 = 0;
                    }
                    i2++;
                }
                i = 0;
            } catch (IOException e) {
                e.printStackTrace();
                i = 11;
            }
            try {
                fileInputStream.close();
                return i;
            } catch (IOException e2) {
                e2.printStackTrace();
                return i;
            }
        } catch (Exception e3) {
            return 8;
        }
    }

    public static void a(BufferedOutputStream bufferedOutputStream, byte[] bArr) {
        if (bufferedOutputStream != null && bArr != null) {
            int length = bArr.length;
            int i = 0;
            while (length > 0) {
                if (length > 16384) {
                    bufferedOutputStream.write(bArr, i, 16384);
                } else {
                    bufferedOutputStream.write(bArr, i, length);
                }
                i += 16384;
                length -= 16384;
                bufferedOutputStream.flush();
            }
        }
    }
}
