package com.qq.ndk;

/* compiled from: ProGuard */
public final class FileNative {
    private a callback = null;
    public String[] data = null;

    public FileNative(a aVar) {
        setCallback(aVar);
    }

    public synchronized void setCallback(a aVar) {
        this.callback = aVar;
    }

    public int addFiles(String[] strArr) {
        dumpFiles(strArr);
        return 0;
    }

    public int dumpFiles(String[] strArr) {
        if (this.callback == null) {
            return 0;
        }
        this.callback.a(strArr);
        return 0;
    }
}
