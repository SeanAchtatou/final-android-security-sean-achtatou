package android.content.b.a;

import java.util.ArrayList;

/* compiled from: ProGuard */
public class f {

    /* renamed from: a  reason: collision with root package name */
    public g[] f25a = null;
    private ArrayList<Integer> b = new ArrayList<>();

    public void a(int i) {
        this.b.add(Integer.valueOf(i));
    }

    public void a() {
        if (this.b.size() > 0) {
            this.f25a = new g[this.b.size()];
        }
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.b.size()) {
                this.f25a[i2] = new g(this);
                this.f25a[i2].f26a = this.b.get(i2).intValue();
                i = i2 + 1;
            } else {
                return;
            }
        }
    }
}
