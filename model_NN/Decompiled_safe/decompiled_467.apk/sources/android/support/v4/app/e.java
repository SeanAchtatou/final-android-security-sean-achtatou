package android.support.v4.app;

import android.view.View;

/* compiled from: ProGuard */
class e implements k {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Fragment f56a;

    e(Fragment fragment) {
        this.f56a = fragment;
    }

    public View a(int i) {
        if (this.f56a.I != null) {
            return this.f56a.I.findViewById(i);
        }
        throw new IllegalStateException("Fragment does not have a view");
    }
}
