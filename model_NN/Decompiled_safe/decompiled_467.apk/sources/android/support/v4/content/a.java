package android.support.v4.content;

import java.io.FileDescriptor;
import java.io.PrintWriter;

/* compiled from: ProGuard */
public class a<D> {

    /* renamed from: a  reason: collision with root package name */
    int f71a;
    b<D> b;
    boolean c;
    boolean d;
    boolean e;
    boolean f;

    public void a(int i, b<D> bVar) {
        if (this.b != null) {
            throw new IllegalStateException("There is already a listener registered");
        }
        this.b = bVar;
        this.f71a = i;
    }

    public void a(b<Object> bVar) {
        if (this.b == null) {
            throw new IllegalStateException("No listener register");
        } else if (this.b != bVar) {
            throw new IllegalArgumentException("Attempting to unregister the wrong listener");
        } else {
            this.b = null;
        }
    }

    public final void a() {
        this.c = true;
        this.e = false;
        this.d = false;
        b();
    }

    /* access modifiers changed from: protected */
    public void b() {
    }

    public void c() {
        this.c = false;
        d();
    }

    /* access modifiers changed from: protected */
    public void d() {
    }

    public void e() {
        f();
        this.e = true;
        this.c = false;
        this.d = false;
        this.f = false;
    }

    /* access modifiers changed from: protected */
    public void f() {
    }

    public String a(Object obj) {
        StringBuilder sb = new StringBuilder(64);
        android.support.v4.b.a.a(obj, sb);
        sb.append("}");
        return sb.toString();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(64);
        android.support.v4.b.a.a(this, sb);
        sb.append(" id=");
        sb.append(this.f71a);
        sb.append("}");
        return sb.toString();
    }

    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        printWriter.print(str);
        printWriter.print("mId=");
        printWriter.print(this.f71a);
        printWriter.print(" mListener=");
        printWriter.println(this.b);
        printWriter.print(str);
        printWriter.print("mStarted=");
        printWriter.print(this.c);
        printWriter.print(" mContentChanged=");
        printWriter.print(this.f);
        printWriter.print(" mAbandoned=");
        printWriter.print(this.d);
        printWriter.print(" mReset=");
        printWriter.println(this.e);
    }
}
