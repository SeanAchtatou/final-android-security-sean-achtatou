package defpackage;

import com.qq.taf.jce.JceInputStream;
import com.qq.taf.jce.JceOutputStream;
import com.qq.taf.jce.JceStruct;
import com.qq.taf.jce.JceUtil;
import com.tencent.connect.common.Constants;

/* renamed from: h  reason: default package */
public final class h extends JceStruct implements Cloneable {
    static final /* synthetic */ boolean k = (!h.class.desiredAssertionStatus());

    /* renamed from: a  reason: collision with root package name */
    public String f3931a = Constants.STR_EMPTY;
    public String b = Constants.STR_EMPTY;
    public String c = Constants.STR_EMPTY;
    public String d = Constants.STR_EMPTY;
    public String e = Constants.STR_EMPTY;
    public int f = 0;
    public String g = Constants.STR_EMPTY;
    public String h = Constants.STR_EMPTY;
    public String i = Constants.STR_EMPTY;
    public String j = Constants.STR_EMPTY;

    public h() {
        a(this.f3931a);
        b(this.b);
        c(this.c);
        d(this.d);
        e(this.e);
        a(this.f);
        f(this.g);
        g(this.h);
        h(this.i);
        i(this.j);
    }

    public void a(int i2) {
        this.f = i2;
    }

    public void a(String str) {
        this.f3931a = str;
    }

    public void b(String str) {
        this.b = str;
    }

    public void c(String str) {
        this.c = str;
    }

    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e2) {
            if (k) {
                return null;
            }
            throw new AssertionError();
        }
    }

    public void d(String str) {
        this.d = str;
    }

    public void display(StringBuilder sb, int i2) {
    }

    public void e(String str) {
        this.e = str;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        h hVar = (h) obj;
        return JceUtil.equals(this.f3931a, hVar.f3931a) && JceUtil.equals(this.b, hVar.b) && JceUtil.equals(this.c, hVar.c) && JceUtil.equals(this.d, hVar.d) && JceUtil.equals(this.e, hVar.e) && JceUtil.equals(this.f, hVar.f) && JceUtil.equals(this.g, hVar.g) && JceUtil.equals(this.h, hVar.h) && JceUtil.equals(this.i, hVar.i) && JceUtil.equals(this.j, hVar.j);
    }

    public void f(String str) {
        this.g = str;
    }

    public void g(String str) {
        this.h = str;
    }

    public void h(String str) {
        this.i = str;
    }

    public int hashCode() {
        try {
            throw new Exception("Need define key first!");
        } catch (Exception e2) {
            e2.printStackTrace();
            return 0;
        }
    }

    public void i(String str) {
        this.j = str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int */
    public void readFrom(JceInputStream jceInputStream) {
        a(jceInputStream.readString(0, true));
        b(jceInputStream.readString(1, false));
        c(jceInputStream.readString(2, false));
        d(jceInputStream.readString(3, false));
        e(jceInputStream.readString(4, false));
        a(jceInputStream.read(this.f, 5, false));
        f(jceInputStream.readString(6, false));
        g(jceInputStream.readString(7, false));
        h(jceInputStream.readString(8, false));
        i(jceInputStream.readString(9, false));
    }

    public void writeTo(JceOutputStream jceOutputStream) {
        jceOutputStream.write(this.f3931a, 0);
        if (this.b != null) {
            jceOutputStream.write(this.b, 1);
        }
        if (this.c != null) {
            jceOutputStream.write(this.c, 2);
        }
        if (this.d != null) {
            jceOutputStream.write(this.d, 3);
        }
        if (this.e != null) {
            jceOutputStream.write(this.e, 4);
        }
        jceOutputStream.write(this.f, 5);
        if (this.g != null) {
            jceOutputStream.write(this.g, 6);
        }
        if (this.h != null) {
            jceOutputStream.write(this.h, 7);
        }
        if (this.i != null) {
            jceOutputStream.write(this.i, 8);
        }
        if (this.j != null) {
            jceOutputStream.write(this.j, 9);
        }
    }
}
