package com.admob.android.ads;

import android.content.Context;
import android.util.Log;
import com.admob.android.ads.InterstitialAd;
import org.json.JSONObject;
import org.json.JSONTokener;

/* compiled from: DeveloperNotice */
final class c {
    private static boolean a = false;

    c() {
    }

    public static void a(Context context) {
        byte[] a2;
        String string;
        if (!a) {
            a = true;
            if (AdManager.isEmulator()) {
                try {
                    String a3 = b.a(context, null, null, 0);
                    e a4 = g.a("http://api.admob.com/v1/pubcode/android_sdk_emulator_notice" + "?" + a3, "developer_message", AdManager.getUserId(context));
                    if (a4.d() && (a2 = a4.a()) != null && (string = new JSONObject(new JSONTokener(new String(a2))).getString("data")) != null && !string.equals("")) {
                        Log.w(AdManager.LOG, string);
                    }
                } catch (Exception e) {
                    if (InterstitialAd.c.a(AdManager.LOG, 2)) {
                        Log.v(AdManager.LOG, "Unhandled exception retrieving developer message.", e);
                    }
                }
            }
        }
    }
}
