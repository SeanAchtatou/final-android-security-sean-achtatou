package com.m41m41.assaultrifle2;

import android.app.Activity;
import android.content.Intent;
import android.database.CursorJoiner;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.util.ByteArrayBuffer;
import org.apache.http.util.EncodingUtils;
import org.apache.http.util.EntityUtils;

public class GoogleImageSearchParser {
    public static final String INTENT_STR_GROUP_ID = "group_id";
    public static final String INTENT_STR_GROUP_NAME = "group_name";
    public static final String INTENT_STR_KEYWORD = "keyword";
    public static final String INTENT_STR_LIST_URL = "list_url";
    public static final String INTENT_STR_SEQUENCE = "SEQUENCE";
    public static final String KEY_DESCRIPTION = "Key_discription";
    public static final String KEY_SEQUENCE = "Key_random";
    public static final String KEY_SOURCE_PAGE = "Key_source_page";
    public static final String KEY_SOURCE_URL = "Key_source_url";
    public static final String KEY_STARTNUMBER = "Key_start_number";
    public static final String KEY_error_message = "Key_error_message";
    public static final String LOG_TAG = "ImageSearchParser";
    private static final String STR_AIM = "imgurl\\x3dhttp://";
    ByteArrayBuffer baf;
    InputStream bis;
    /* access modifiers changed from: private */
    public boolean cancel = false;
    /* access modifiers changed from: private */
    public AsyncTask<Activity, Integer, CursorJoiner.Result> downloadtask;
    URLConnection uconnection;
    URL uri;

    private String getColor(int i) {
        switch (i) {
            case R.styleable.com_admob_android_ads_AdView_testing /*0*/:
                return "red";
            case R.styleable.com_admob_android_ads_AdView_backgroundColor /*1*/:
                return "orange";
            case 2:
                return "yellow";
            case R.styleable.com_admob_android_ads_AdView_keywords /*3*/:
                return "green";
            case 4:
                return "teal";
            case 5:
                return "blue";
            case 6:
                return "purple";
            case 7:
                return "pink";
            case 8:
                return "while";
            case 9:
                return "gray";
            case 10:
                return "black";
            case 11:
                return "brown";
            default:
                return "";
        }
    }

    public class LoadimageTask extends AsyncTask<Activity, Integer, CursorJoiner.Result> {
        private static final int IO_BUFFER_SIZE = 4096;
        Activity act;
        ImageView img;

        public LoadimageTask() {
        }

        private final void copy(InputStream in, OutputStream out) throws IOException {
            byte[] b = new byte[IO_BUFFER_SIZE];
            while (true) {
                int read = in.read(b);
                if (read != -1) {
                    out.write(b, 0, read);
                } else {
                    return;
                }
            }
        }

        private final Bitmap getRemotePic(String url) {
            Bitmap bm = null;
            try {
                URL url2 = new URL(url);
                URLConnection conn = url2.openConnection();
                conn.setRequestProperty("User-agent", "Mozilla/4.0");
                conn.setUseCaches(true);
                conn.setConnectTimeout(2000);
                BufferedInputStream in = new BufferedInputStream(url2.openStream(), IO_BUFFER_SIZE);
                ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
                BufferedOutputStream out = new BufferedOutputStream(dataStream, IO_BUFFER_SIZE);
                copy(in, out);
                out.flush();
                byte[] data = dataStream.toByteArray();
                bm = BitmapFactory.decodeByteArray(data, 0, data.length);
            } catch (MalformedURLException e) {
                Log.e(GoogleImageSearchParser.LOG_TAG, "getRemotePic Error!" + url, e);
            } catch (IOException e2) {
                Log.e(GoogleImageSearchParser.LOG_TAG, "getRemotePic Error!" + url, e2);
            }
            if (bm == null) {
                Log.e(GoogleImageSearchParser.LOG_TAG, "getRemotePic Error!" + url);
            } else {
                Log.d(GoogleImageSearchParser.LOG_TAG, "getRemotePic OK:" + url);
            }
            return bm;
        }

        private boolean loadImg(String start) {
            GoogleImageSearchParser.this.cancel = false;
            String address = "http://images.google.com/images?ijn&hl=en&q=" + this.act.getIntent().getStringExtra(GoogleImageSearchParser.INTENT_STR_KEYWORD);
            if (this.act.getIntent().getStringExtra(GoogleImageSearchParser.KEY_SEQUENCE) == null) {
                start = String.valueOf((int) (Math.random() * 200.0d));
                address = String.valueOf(address) + "&imgtype=photo" + "&imgsz=m";
            }
            Log.i(GoogleImageSearchParser.LOG_TAG, String.format("number=%s", start));
            String address2 = String.valueOf(address) + "&start=" + start;
            try {
                Log.i(GoogleImageSearchParser.LOG_TAG, String.format("step_0.01 ok", new Object[0]));
                String string = GoogleImageSearchParser.this.ApacheHttpGet(address2);
                if (string.compareToIgnoreCase("error") == 0) {
                    return false;
                }
                if (GoogleImageSearchParser.this.cancel) {
                    return false;
                }
                String[] items = string.split("\\],\\[");
                if (items.length < 4) {
                    return false;
                }
                String[] arrary = items[0].split(",");
                if (arrary.length < 25) {
                    return false;
                }
                String[] srcArray = arrary[0].split("\\\\x3d");
                if (srcArray == null || srcArray.length < 3) {
                    return false;
                }
                int end1 = srcArray[1].indexOf("\\x26");
                int end2 = srcArray[2].indexOf("\\x26");
                if (end1 == -1 || end2 == -1) {
                    return false;
                }
                String jpgUrl = srcArray[1].substring(0, end1);
                String source_page = srcArray[2].substring(0, end2);
                this.act.getIntent().putExtra(GoogleImageSearchParser.KEY_DESCRIPTION, arrary[6].replaceAll("\"", "").replaceAll("\\\\x3cb\\\\x3e", "").replaceAll("\\\\x3c/b\\\\x3e", "").replaceAll("\\\\x26gt;", "").replaceAll("\\\\x26middot;", ".").replaceAll("\\\\x26amp;", "&"));
                this.act.getIntent().putExtra(GoogleImageSearchParser.KEY_SOURCE_PAGE, source_page);
                Log.i(GoogleImageSearchParser.LOG_TAG, String.format("step_1 ok", new Object[0]));
                Bitmap bmp = getRemotePic(jpgUrl);
                if (bmp != null) {
                    Log.i(GoogleImageSearchParser.LOG_TAG, String.format("width=%d,number=%s", Integer.valueOf(bmp.getWidth()), start));
                    this.img.setTag(bmp);
                    this.act.getIntent().putExtra(GoogleImageSearchParser.KEY_SOURCE_URL, jpgUrl);
                    return true;
                }
                this.act.getIntent().putExtra(GoogleImageSearchParser.KEY_SOURCE_URL, "");
                return false;
            } catch (IOException e) {
                this.act.getIntent().putExtra(GoogleImageSearchParser.KEY_error_message, e.getLocalizedMessage());
                this.act.getIntent().putExtra(GoogleImageSearchParser.KEY_SOURCE_URL, "");
                return false;
            }
        }

        /* access modifiers changed from: protected */
        public CursorJoiner.Result doInBackground(Activity... arg0) {
            this.act = arg0[0];
            this.img = (ImageView) arg0[0].findViewById(R.id.img_View);
            int retry = 3;
            String start_number = this.act.getIntent().getStringExtra(GoogleImageSearchParser.KEY_STARTNUMBER);
            while (true) {
                int retry2 = retry;
                retry = retry2 - 1;
                if (retry2 > 0) {
                    System.gc();
                    if (loadImg(start_number) || GoogleImageSearchParser.this.cancel) {
                        break;
                    }
                    Log.i(GoogleImageSearchParser.LOG_TAG, "retry");
                    start_number = String.valueOf((Integer.parseInt(start_number) + 1) % 1000);
                    this.act.getIntent().putExtra(GoogleImageSearchParser.KEY_STARTNUMBER, start_number);
                } else {
                    break;
                }
            }
            this.act.getIntent().putExtra(GoogleImageSearchParser.KEY_STARTNUMBER, String.valueOf((Integer.parseInt(start_number) + 1) % 1000));
            return null;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(CursorJoiner.Result result) {
            ((RelativeLayout) this.act.findViewById(R.id.RelativeLayout01)).setVisibility(4);
            Bitmap bp = (Bitmap) this.img.getTag();
            if (bp == null) {
                Toast.makeText(this.act, "retry later\nerror:" + this.act.getIntent().getStringExtra(GoogleImageSearchParser.KEY_error_message), 1).show();
            } else {
                TextView descriptionView = (TextView) this.act.findViewById(R.id.TextView02);
                if (descriptionView != null) {
                    final String discription = this.act.getIntent().getStringExtra(GoogleImageSearchParser.KEY_DESCRIPTION);
                    if (discription != null) {
                        descriptionView.setText(discription);
                        final String sourcepage = this.act.getIntent().getStringExtra(GoogleImageSearchParser.KEY_SOURCE_PAGE);
                        if (!(sourcepage == null || sourcepage.length() == 0)) {
                            descriptionView.setOnClickListener(new View.OnClickListener() {
                                public void onClick(View v) {
                                    Intent i = new Intent(LoadimageTask.this.act, BrowserActivity.class);
                                    i.putExtra(GoogleImageSearchParser.INTENT_STR_KEYWORD, discription);
                                    i.setData(Uri.parse(sourcepage));
                                    LoadimageTask.this.act.startActivity(i);
                                }
                            });
                        }
                    } else {
                        descriptionView.setOnClickListener(null);
                        descriptionView.setText("");
                    }
                }
                this.img.setImageBitmap(bp);
                System.gc();
            }
            ((ImageButton) this.act.findViewById(R.id.ImageButtonNext)).setVisibility(0);
            super.onPostExecute((Object) result);
        }

        /* access modifiers changed from: protected */
        public void onCancelled() {
            GoogleImageSearchParser.this.cancel = true;
        }
    }

    public void HttpGetHelper(String address, int size) throws IOException {
        this.uri = new URL(address);
        this.uconnection = this.uri.openConnection();
        this.uconnection.setRequestProperty("User-agent", "Mozilla/4.0");
        this.uconnection.setUseCaches(true);
        this.uconnection.setConnectTimeout(5000);
        Log.i(LOG_TAG, String.format("step_0.02 ok", new Object[0]));
        this.bis = this.uconnection.getInputStream();
        Log.i(LOG_TAG, String.format("step_0.03 ok", new Object[0]));
        this.baf = new ByteArrayBuffer(size);
    }

    /* access modifiers changed from: private */
    public String ApacheHttpGet(String address) throws IOException, ClientProtocolException {
        DefaultHttpClient httpclient = new DefaultHttpClient();
        HttpConnectionParams.setConnectionTimeout(httpclient.getParams(), 15000);
        HttpProtocolParams.setUserAgent(httpclient.getParams(), "Mozilla/5.0 (Windows; U; Windows NT 5.1; zh-CN; rv:1.9.1.2)");
        HttpResponse response = httpclient.execute(new HttpGet(address));
        if (response.getStatusLine().getStatusCode() == 200) {
            Log.i(LOG_TAG, String.format("step_url_get ok", new Object[0]));
            String string = EntityUtils.toString(response.getEntity());
            httpclient.getConnectionManager().shutdown();
            return string;
        }
        httpclient.getConnectionManager().shutdown();
        return "error";
    }

    public ByteArrayBuffer read() throws IOException {
        this.baf.clear();
        Log.i(LOG_TAG, String.format("step_0.1 ok", new Object[0]));
        while (true) {
            int current = this.bis.read();
            if (current == -1) {
                Log.i(LOG_TAG, String.format("step_0.2 ok", new Object[0]));
                return this.baf;
            } else if (this.cancel) {
                this.bis.close();
                this.baf.clear();
                return this.baf;
            } else {
                this.baf.append((byte) current);
            }
        }
    }

    public String decodeURL(ByteArrayBuffer buffer) {
        return EncodingUtils.getString(buffer.toByteArray(), "gb2312");
    }

    public void cancel() {
        if (this.downloadtask != null) {
            this.downloadtask.cancel(true);
        }
    }

    public void startPreview(final Activity acti) {
        acti.setContentView((int) R.layout.img_preview);
        ((TextView) acti.findViewById(R.id.TextView01)).setText(String.valueOf(URLDecoder.decode(acti.getIntent().getStringExtra(INTENT_STR_KEYWORD))) + " ...");
        TextView descriptionView = (TextView) acti.findViewById(R.id.TextView02);
        descriptionView.setTextSize(descriptionView.getTextSize() * 1.1f);
        descriptionView.setTextColor(-16777216);
        ((RelativeLayout) acti.findViewById(R.id.RelativeLayout01)).setVisibility(0);
        acti.getIntent().putExtra(KEY_STARTNUMBER, "0");
        this.downloadtask = new LoadimageTask().execute(acti);
        ImageView imgVIew = (ImageView) acti.findViewById(R.id.img_View);
        BitmapDrawable bd = new BitmapDrawable(BitmapFactory.decodeResource(acti.getResources(), R.drawable.bg));
        bd.setTileModeXY(Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
        bd.setDither(true);
        imgVIew.setBackgroundDrawable(bd);
        imgVIew.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            }
        });
        ImageButton nextButton = (ImageButton) acti.findViewById(R.id.ImageButtonNext);
        nextButton.setVisibility(4);
        nextButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                RelativeLayout progress = (RelativeLayout) acti.findViewById(R.id.RelativeLayout01);
                if (!progress.isShown()) {
                    progress.setVisibility(0);
                    ((ImageButton) acti.findViewById(R.id.ImageButtonNext)).setVisibility(4);
                    GoogleImageSearchParser.this.downloadtask = new LoadimageTask().execute(acti);
                }
            }
        });
        ((ImageButton) acti.findViewById(R.id.ImageButtonSave)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ((ImageBrowserActivity) acti).savePicture(URLDecoder.decode(acti.getIntent().getStringExtra(GoogleImageSearchParser.INTENT_STR_KEYWORD)), acti.getIntent().getStringExtra(GoogleImageSearchParser.KEY_STARTNUMBER));
            }
        });
    }
}
