package com.m41m41.assaultrifle2;

import android.app.Activity;
import android.content.Intent;
import android.database.CursorJoiner;
import android.net.Uri;
import android.os.AsyncTask;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import org.apache.http.util.ByteArrayBuffer;
import org.apache.http.util.EncodingUtils;

public class LoadHotSearchListTask extends AsyncTask<Activity, Integer, CursorJoiner.Result> {
    public static final String app_url = "market://search?q=pname:com.m41m41.assaultrifle2";
    public static final String english_name = "Assault Rifle";
    private static final String refresh_err = "Refresh error,please retry";
    private final String RAN = "ran";
    private final String SEQ = "seq";
    private final String URL = "url";
    /* access modifiers changed from: private */
    public final activity activity;
    private ByteArrayBuffer baf;
    InputStream bis;
    /* access modifiers changed from: private */
    public String[] hotStrings = new String[(this.metaData.length / 3)];
    private String last_adress = "";
    private final String[] metaData = {"ran", "2B-A-40", "2B-A-40 assault rifle", "ran", "AC-556", "AC-556 assault rifle", "ran", "Ak 5", "Ak 5 assault rifle", "ran", "AK-47", "AK-47 assault rifle", "ran", "AK-74", "AK-74 assault rifle", "ran", "AKM", "AKM assault rifle", "ran", "AO-38", "AO-38 assault rifle", "ran", "AO-63", "AO-63 assault rifle", "ran", "Armtech C30R", "Armtech C30R assault rifle", "ran", "AVB-7.62", "AVB-7.62 assault rifle", "ran", "Barrett REC7", "Barrett REC7 assault rifle", "ran", "Beretta AR70", "Beretta AR70 assault rifle", "ran", "Beretta ARX-160", "Beretta ARX-160 assault rifle", "ran", "Bushmaster ACR", "Bushmaster ACR assault rifle", "ran", "CETME", "CETME assault rifle", "ran", "Cherkashin", "Cherkashin assault rifle", "ran", "Z 522", "Z 522 assault rifle", "ran", "Daewoo K2", "Daewoo K2 assault rifle", "ran", "EM-2", "EM-2 assault rifle", "ran", "FAMAS", "FAMAS assault rifle", "ran", "FARA 83", "FARA 83 assault rifle", "ran", "FN F2000", "FN F2000 assault rifle", "ran", "Franchi LF-58", "Franchi LF-58 assault rifle", "ran", "Gordon CSWS", "Gordon CSWS assault rifle", "ran", "Heckler & Koch G11", "Heckler & Koch G11 assault rifle", "ran", "Heckler & Koch G41", "Heckler & Koch G41 assault rifle", "ran", "Heckler & Koch HK416", "Heckler & Koch HK416 assault rifle", "ran", "Howa Type 89", "Howa Type 89 assault rifle", "ran", "IMI Galil", "IMI Galil assault rifle", "ran", "IMI Tavor TAR-21", "IMI Tavor TAR-21 assault rifle", "ran", "INSAS rifle", "INSAS rifle assault rifle", "ran", "Interdynamics MKR", "Interdynamics MKR assault rifle", "ran", "LAPA FA-03", "LAPA FA-03 assault rifle", "ran", "Leader Dynamics T2 MK5", "Leader Dynamics T2 MK5 assault rifle", "ran", "M4 Carbine", "M4 Carbine assault rifle", "ran", "M16", "M16 assault rifle", "ran", "NIVA XM1970", "NIVA XM1970 assault rifle", "ran", "Pindad SS1", "Pindad SS1 assault rifle", "ran", "QBZ-95", "QBZ-95 assault rifle", "ran", "RH-70", "RH-70 assault rifle", "ran", "Rk 95 TP", "Rk 95 TP assault rifle", "ran", "Rung Paisarn RPS-001", "Rung Paisarn RPS-001 assault rifle", "ran", "SA80", "SA80 assault rifle", "ran", "Safir T-16", "Safir T-16 assault rifle", "ran", "San Cristobal", "San Cristobal assault rifle", "ran", "SIG SG 540", "SIG SG 540 assault rifle", "ran", "SOAR", "SOAR assault rifle", "ran", "SOCIMI AR-831", "SOCIMI AR-831 assault rifle", "ran", "Sterling SAR-87", "Sterling SAR-87 assault rifle", "ran", "Steyr ACR", "Steyr ACR assault rifle", "ran", "Sturmgewehr 44", "Sturmgewehr 44 assault rifle", "ran", "StG45", "StG45 assault rifle", "ran", "TKB-022PM", "TKB-022PM assault rifle", "ran", "TKB-517", "TKB-517 assault rifle", "ran", "T91", "T91 assault rifle", "ran", "Truvelo Raptor", "Truvelo Raptor assault rifle", "ran", "Valmet M82", "Valmet M82 assault rifle", "ran", "VB Berapi LP06", "VB Berapi LP06 assault rifle", "ran", "Vektor CR-21", "Vektor CR-21 assault rifle", "ran", "W+F Bern C42", "W+F Bern C42 assault rifle", "ran", "Wimmersperg Spz-kr", "Wimmersperg Spz-kr assault rifle", "ran", "XM8 rifle", "XM8 rifle assault rifle", "ran", "Z-M Weapons LR 300", "Z-M Weapons LR 300 assault rifle", "ran", "Zastava M21", "Zastava M21 assault rifle", "ran", "ZB-530", "ZB-530 assault rifle"};
    /* access modifiers changed from: private */
    public String[] searchOption = new String[(this.metaData.length / 3)];
    /* access modifiers changed from: private */
    public String[] stringToLink = new String[(this.metaData.length / 3)];
    URLConnection uconnection;
    URL uri;

    public String[] getHotStrings() {
        return this.hotStrings;
    }

    LoadHotSearchListTask(activity activity2) {
        this.activity = activity2;
        for (int i = 0; i < this.metaData.length; i += 3) {
            this.searchOption[i / 3] = this.metaData[i];
            this.hotStrings[i / 3] = this.metaData[i + 1];
            this.stringToLink[i / 3] = this.metaData[i + 2];
        }
    }

    /* access modifiers changed from: protected */
    public CursorJoiner.Result doInBackground(Activity... arg0) {
        this.activity.list_hot_search = getHotStrings();
        Intent intent = this.activity.getIntent();
        this.activity.getClass();
        this.activity.getClass();
        intent.putExtra("KEY_NET_status", "sucess");
        return null;
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(CursorJoiner.Result result) {
        activity activity2 = this.activity;
        this.activity.getClass();
        activity2.removeDialog(5);
        Intent intent = this.activity.getIntent();
        this.activity.getClass();
        String stringExtra = intent.getStringExtra("KEY_NET_status");
        this.activity.getClass();
        if (stringExtra.compareTo("fail") == 0) {
            Toast.makeText(this.activity, refresh_err, 1).show();
        } else if (!(this.activity.list_hot_search == null || this.activity.list_hot_search.length == 0)) {
            ListView list1 = (ListView) this.activity.findViewById(R.id.ListView01);
            list1.setAdapter((ListAdapter) new ArrayAdapter(this.activity, 17367043, this.activity.list_hot_search));
            list1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> adapterView, View arg1, int position, long arg3) {
                    if (LoadHotSearchListTask.this.searchOption[position].compareTo("url") == 0) {
                        Intent i = new Intent(LoadHotSearchListTask.this.activity, BrowserActivity.class);
                        i.putExtra(GoogleImageSearchParser.INTENT_STR_KEYWORD, LoadHotSearchListTask.this.hotStrings[position]);
                        i.setData(Uri.parse(LoadHotSearchListTask.this.stringToLink[position]));
                        LoadHotSearchListTask.this.activity.startActivity(i);
                    } else if (LoadHotSearchListTask.this.searchOption[position].compareTo("ran") == 0) {
                        LoadHotSearchListTask.this.activity.startSearch(LoadHotSearchListTask.this.stringToLink[position], false);
                    } else if (LoadHotSearchListTask.this.searchOption[position].compareTo("seq") == 0) {
                        LoadHotSearchListTask.this.activity.startSearch(LoadHotSearchListTask.this.stringToLink[position], true);
                    }
                }
            });
        }
        super.onPostExecute((Object) result);
    }

    public ByteArrayBuffer read(String address, int size) throws IOException {
        if (address.equalsIgnoreCase(this.last_adress)) {
            return this.baf;
        }
        this.uri = new URL(address);
        this.uconnection = this.uri.openConnection();
        this.uconnection.setUseCaches(true);
        this.bis = this.uconnection.getInputStream();
        this.baf = new ByteArrayBuffer(size);
        this.baf.clear();
        while (true) {
            int current = this.bis.read();
            if (current == -1) {
                this.last_adress = address;
                return this.baf;
            }
            this.baf.append((byte) current);
        }
    }

    public String decodeURL(ByteArrayBuffer buffer) {
        return EncodingUtils.getString(buffer.toByteArray(), "UTF-8");
    }
}
