package android.pushMessage;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.common.Mainaty;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;
import com.baidu.android.common.logging.Log;
import com.baidu.android.pushservice.PushConstants;
import xt.com.tongyong.id884999.R;

public class PushMessageReceiver extends BroadcastReceiver {
    public static final String LAG = PushMessageReceiver.class.getSimpleName();
    static int requestCode = 0;

    public void onReceive(Context context, Intent intent) {
        String message;
        int delimiter;
        if (intent.getAction().equals(PushConstants.ACTION_MESSAGE)) {
            String message2 = intent.getExtras().getString(PushConstants.EXTRA_PUSH_MESSAGE_STRING);
            System.out.println(message2 + PushConstants.EXTRA_PUSH_MESSAGE);
            if (message2.length() >= 3 && (delimiter = (message = message2.substring(1, message2.length() - 1)).indexOf(36)) != -1 && delimiter + 1 != message.length()) {
                String url = message.substring(delimiter + 1);
                NotificationManager nm = (NotificationManager) context.getSystemService("notification");
                Notification notification = new Notification(R.drawable.icon, context.getResources().getString(R.string.have_a_new_message), System.currentTimeMillis());
                notification.flags = 16;
                RemoteViews remoteView = new RemoteViews(context.getPackageName(), R.layout.notifi);
                try {
                    String title = message.substring(0, delimiter);
                    System.out.println(title + "title");
                    remoteView.setTextViewText(R.id.NotifiText, title);
                } catch (Exception e) {
                    remoteView.setTextViewText(R.id.NotifiText, "you hava a new message !");
                }
                remoteView.setImageViewResource(R.id.Notifiimage, R.drawable.icon);
                notification.contentView = remoteView;
                Intent targetIntent = new Intent(context, Mainaty.class);
                targetIntent.putExtra("pushUrl", url);
                int i = requestCode;
                requestCode = i + 1;
                notification.contentIntent = PendingIntent.getActivity(context, i, targetIntent, 0);
                nm.notify(0, notification);
            }
        } else if (intent.getAction().equals(PushConstants.ACTION_RECEIVE)) {
            String method = intent.getStringExtra(PushConstants.EXTRA_METHOD);
            int errorCode = intent.getIntExtra(PushConstants.EXTRA_ERROR_CODE, 0);
            String content = new String(intent.getByteArrayExtra(PushConstants.EXTRA_CONTENT));
            Log.d(LAG, "onMessage: method : " + method);
            Log.d(LAG, "onMessage: result : " + errorCode);
            Log.d(LAG, "onMessage: content : " + content);
        } else if (intent.getAction().equals(PushConstants.ACTION_RECEIVER_NOTIFICATION_CLICK)) {
            String stringExtra = intent.getStringExtra("pushUrl");
        }
    }
}
