package android.common;

import android.app.Activity;
import android.content.Context;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.TextView;
import xt.com.tongyong.id884999.R;

public class OutBrowserWebChromeClient extends WebChromeClient {
    final Activity activity;
    private Context context;
    TextView txtView;

    public OutBrowserWebChromeClient(Activity activity2, TextView txtView2) {
        this.activity = activity2;
        this.txtView = txtView2;
    }

    public void onProgressChanged(WebView view, int progress) {
        this.activity.setProgress(progress * 100);
    }

    public void onReceivedTitle(WebView view, String title) {
        super.onReceivedTitle(view, title);
        if (title == null || title.trim().equals("")) {
            this.txtView.setText((int) R.string.notitle);
        } else {
            this.txtView.setText(title);
        }
    }
}
