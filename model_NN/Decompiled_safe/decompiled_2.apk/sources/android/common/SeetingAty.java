package android.common;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;
import xt.com.tongyong.id884999.R;

public class SeetingAty extends Activity {
    Context _context;
    ImageButton back_about_us;
    Button imageBtn_clearcache;
    Button imageBtn_more_use;
    Button imageBtn_share_app;
    Button imageBtn_version;
    int num = 0;
    TextView txt_business;
    TextView txt_versiong;
    UpdateDom updateDom;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.seeting);
        this._context = this;
        this.back_about_us = (ImageButton) findViewById(R.id.imgbackaboutus);
        this.imageBtn_clearcache = (Button) findViewById(R.id.imgbtn_DeleteUrlCache);
        this.imageBtn_version = (Button) findViewById(R.id.imgbtn_version);
        this.imageBtn_more_use = (Button) findViewById(R.id.imgbtn_more_use);
        this.imageBtn_share_app = (Button) findViewById(R.id.imgbtn_share_app);
        this.txt_business = (TextView) findViewById(R.id.business_txt);
        this.txt_versiong = (TextView) findViewById(R.id.about_version_txt);
        this.updateDom = new UpdateDom(this._context);
        this.txt_versiong.setText(this._context.getString(R.string.version_) + ((float) (((double) this.updateDom.oldXml.GetAppInfo().Version) / 10.0d)));
        String developer = this.updateDom.oldXml.GetAppInfo().developer;
        TextView textView = this.txt_business;
        if (developer.equals("")) {
            developer = this.updateDom.oldXml.GetAppInfo().name;
        }
        textView.setText(developer);
        this.imageBtn_clearcache.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                DialogHelper.EnsureAndCancelDialog(SeetingAty.this._context, SeetingAty.this._context.getString(R.string.cache_sure), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        int unused = AnonymousClass1.this.clearCacheFolder(SeetingAty.this.getCacheDir().getAbsoluteFile(), System.currentTimeMillis());
                    }
                }, null);
            }

            /* access modifiers changed from: private */
            public int clearCacheFolder(File dir, long numDays) {
                int deletedFiles = 0;
                if (dir != null && dir.isDirectory()) {
                    try {
                        for (File child : dir.listFiles()) {
                            if (child.isDirectory()) {
                                deletedFiles += clearCacheFolder(child, numDays);
                                Log.v("deleteFiles", child.isDirectory() + "isDirectory");
                                Log.v("deleteFiles", dir.listFiles() + " listFiles");
                            }
                            if (child.lastModified() < numDays && child.delete()) {
                                Log.v("deleteFiles", child.delete() + "child delete");
                                deletedFiles++;
                                Log.v("deleteFiles", deletedFiles + "deletefiles");
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                Toast.makeText(SeetingAty.this._context, SeetingAty.this._context.getString(R.string.cache_over), 0).show();
                Log.v("deleteFiles", deletedFiles + "dele");
                return deletedFiles;
            }
        });
        this.imageBtn_version.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SeetingAty.this.updateDom.HasNewVersionWindow(true);
            }
        });
        this.back_about_us.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SeetingAty.this.finish();
            }
        });
        this.imageBtn_share_app.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent("android.intent.action.SEND");
                intent.setType("image/*");
                intent.putExtra("android.intent.extra.TEXT", SeetingAty.this.updateDom.oldXml.GetAppInfo().name + "【" + SeetingAty.this.updateDom.oldXml.GetWebApi().getappshop + "】");
                intent.setFlags(268435456);
                SeetingAty.this._context.startActivity(Intent.createChooser(intent, SeetingAty.this._context.getString(R.string.share)));
            }
        });
    }
}
