package android.common;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import xt.com.tongyong.id884999.R;

public class UpdateDom {
    /* access modifiers changed from: private */
    public Context context;
    private HttpURLConnection httpConn;
    /* access modifiers changed from: private */
    public Dom newXml = null;
    public Dom oldXml;
    public InputStream streamLocal;
    public InputStream streamNet;

    public UpdateDom(Context context_) {
        this.context = context_;
        try {
            this.streamLocal = this.context.getResources().getAssets().open("update.xml");
            this.oldXml = new Dom(this.streamLocal);
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: package-private */
    public void loadNewXml() {
        if (this.newXml == null) {
            try {
                this.streamNet = HttpHelper.getContent(this.oldXml.GetWebApi().getversion);
                this.newXml = new Dom(this.streamNet);
            } catch (Exception e) {
            }
        }
    }

    public Boolean HasNewVersion() {
        boolean z;
        try {
            loadNewXml();
            if (this.oldXml.GetAppInfo().Version < this.newXml.GetAppInfo().Version) {
                z = true;
            } else {
                z = false;
            }
            return Boolean.valueOf(z);
        } catch (Exception e) {
            return false;
        }
    }

    public void HasNewVersionWindow(Boolean showMes) {
        loadNewXml();
        if (this.streamNet == null) {
            if (showMes.booleanValue()) {
                DialogHelper.EnsureDialog(this.context, this.context.getResources().getString(R.string.version_ex), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
            }
        } else if (HasNewVersion().booleanValue()) {
            DialogHelper.EnsureAndCancelDialog(this.context, this.context.getString(R.string.have_a_new_version), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent("android.intent.action.VIEW");
                    intent.setData(Uri.parse(UpdateDom.this.newXml.GetWebApi().getappshop));
                    UpdateDom.this.context.startActivity(intent);
                }
            }, null);
        } else if (showMes.booleanValue()) {
            DialogHelper.EnsureDialog(this.context, this.context.getResources().getString(R.string.version_newwst), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                }
            });
        }
    }

    public HttpURLConnection Url(String path, int seconds) {
        try {
            this.httpConn = (HttpURLConnection) new URL(path).openConnection();
            this.httpConn.setConnectTimeout(seconds * 1000);
            this.httpConn.connect();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this.httpConn;
    }
}
