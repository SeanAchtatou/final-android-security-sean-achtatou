package android.common;

import android.content.Context;
import android.content.SharedPreferences;

class PKV {
    static String TAG = PKV.class.getSimpleName();
    Context m_context;
    String m_name;
    SharedPreferences m_pref;

    public PKV(Context context, String name) {
        this.m_context = context;
        this.m_name = name;
        this.m_pref = context.getSharedPreferences(name, 32768);
    }

    public void AddKV(String key, String value) {
        SharedPreferences.Editor editor = this.m_pref.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public void Delete(String key) {
        SharedPreferences.Editor editor = this.m_pref.edit();
        editor.remove(key);
        editor.commit();
    }

    public boolean ExistKey(String key) {
        if (this.m_pref.getString(key, "").length() == 0) {
            return false;
        }
        return true;
    }

    public String ReadVByK(String key) {
        return this.m_pref.getString(key, "");
    }

    public void clear(Context context) {
        SharedPreferences.Editor editor = this.m_pref.edit();
        editor.clear();
        editor.commit();
    }
}
