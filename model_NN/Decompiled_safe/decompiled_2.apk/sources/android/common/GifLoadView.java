package android.common;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Message;
import android.widget.ImageView;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

public class GifLoadView {
    Context context;
    int delay = 0;
    Handler handler = null;
    ImageView imgView;
    Timer mTimer;
    TimerTask mTimerTask;
    int period = 300;
    int pngindex = 0;

    public GifLoadView(Context context2, ImageView imgView2, int period2) {
        this.context = context2;
        this.imgView = imgView2;
        this.period = period2;
        this.handler = new Handler() {
            public void handleMessage(Message msg) {
                GifLoadView.this.loadPng();
            }
        };
    }

    /* access modifiers changed from: package-private */
    public void loadPng() {
        try {
            if (this.pngindex == 8) {
                this.pngindex = 0;
            }
            this.imgView.setImageBitmap(BitmapFactory.decodeStream(this.context.getResources().getAssets().open("animloading/load" + this.pngindex + ".png")));
            this.pngindex++;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onStart() {
        this.imgView.setVisibility(0);
        if (this.mTimer == null) {
            this.mTimer = new Timer(true);
        }
        if (this.mTimerTask == null) {
            this.mTimerTask = new TimerTask() {
                public void run() {
                    Message message = new Message();
                    message.what = 1;
                    GifLoadView.this.handler.sendMessage(message);
                }
            };
        }
        this.pngindex = 0;
        this.mTimer.schedule(this.mTimerTask, (long) this.delay, (long) this.period);
    }

    public void onStop() {
        this.imgView.setVisibility(4);
        if (this.mTimer != null) {
            this.mTimer.cancel();
            this.mTimer = null;
        }
        if (this.mTimerTask != null) {
            this.mTimerTask.cancel();
            this.mTimerTask = null;
        }
    }
}
