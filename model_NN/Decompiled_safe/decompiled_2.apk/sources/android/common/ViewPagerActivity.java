package android.common;

import android.app.Activity;
import android.app.LocalActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.List;
import xt.com.tongyong.id884999.R;

public class ViewPagerActivity extends Activity {
    Context context = null;
    LocalActivityManager manager = null;
    public ViewPager viewPager = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.view_pager);
        this.context = this;
        this.manager = new LocalActivityManager(this, true);
        this.manager.dispatchCreate(savedInstanceState);
        this.viewPager = (ViewPager) findViewById(R.id.viewpage);
        ArrayList<View> list = new ArrayList<>();
        list.add(getView("LeftActivity", new Intent(this.context, LeftActivity.class)));
        list.add(getView("Mainaty", new Intent(this.context, Mainaty.class)));
        this.viewPager.setAdapter(new MyPagerAdapter(list));
        this.viewPager.setCurrentItem(1);
        this.viewPager.setOnPageChangeListener(new MyOnPageChangeListener());
        ((MyApplication) getApplicationContext()).viewPagerActivity = this;
    }

    private View getView(String id, Intent intent) {
        return this.manager.startActivity(id, intent).getDecorView();
    }

    public class MyPagerAdapter extends PagerAdapter {
        List<View> list = new ArrayList();

        public MyPagerAdapter(ArrayList<View> list2) {
            this.list = list2;
        }

        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewPager) container).removeView(this.list.get(position));
        }

        public boolean isViewFromObject(View arg0, Object arg1) {
            return arg0 == arg1;
        }

        public int getCount() {
            return this.list.size();
        }

        public Object instantiateItem(View arg0, int arg1) {
            ((ViewPager) arg0).addView(this.list.get(arg1));
            return this.list.get(arg1);
        }

        public void restoreState(Parcelable arg0, ClassLoader arg1) {
        }

        public Parcelable saveState() {
            return null;
        }

        public void startUpdate(View arg0) {
        }
    }

    public class MyOnPageChangeListener implements ViewPager.OnPageChangeListener {
        public MyOnPageChangeListener() {
        }

        public void onPageSelected(int arg0) {
        }

        public void onPageScrollStateChanged(int arg0) {
        }

        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }
    }
}
