package android.common;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.PopupWindow;
import xt.com.tongyong.id884999.R;

public class PopWindowShare extends PopupWindow {
    private Button btn_album = ((Button) this.mMenuView.findViewById(R.id.btn_album));
    private Button btn_cancel = ((Button) this.mMenuView.findViewById(R.id.btn_cancel));
    private Button btn_photo = ((Button) this.mMenuView.findViewById(R.id.f0btn_photos));
    /* access modifiers changed from: private */
    public View mMenuView;

    public PopWindowShare(Context context, View.OnClickListener itemsOnClick) {
        super(context);
        this.mMenuView = ((LayoutInflater) context.getSystemService("layout_inflater")).inflate((int) R.layout.popwindow_share, (ViewGroup) null);
        this.btn_cancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PopWindowShare.this.dismiss();
            }
        });
        this.btn_photo.setOnClickListener(itemsOnClick);
        this.btn_album.setOnClickListener(itemsOnClick);
        setContentView(this.mMenuView);
        setWidth(-1);
        setHeight(-2);
        setFocusable(true);
        setBackgroundDrawable(new ColorDrawable(0));
        this.mMenuView.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                int height = PopWindowShare.this.mMenuView.findViewById(R.id.rel_bg_microblogging).getTop();
                int y = (int) event.getY();
                if (event.getAction() == 1 && y < height) {
                    PopWindowShare.this.dismiss();
                }
                return true;
            }
        });
    }
}
