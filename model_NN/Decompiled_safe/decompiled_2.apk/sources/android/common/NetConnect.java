package android.common;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetConnect {
    Context context_;

    public NetConnect(Context context) {
        this.context_ = context;
    }

    public boolean isConnectingToInternet() {
        NetworkInfo[] info;
        ConnectivityManager connectivity = (ConnectivityManager) this.context_.getSystemService("connectivity");
        if (!(connectivity.getActiveNetworkInfo() == null || (info = connectivity.getAllNetworkInfo()) == null)) {
            for (NetworkInfo state : info) {
                if (state.getState() == NetworkInfo.State.CONNECTED) {
                    return true;
                }
            }
        }
        return false;
    }
}
