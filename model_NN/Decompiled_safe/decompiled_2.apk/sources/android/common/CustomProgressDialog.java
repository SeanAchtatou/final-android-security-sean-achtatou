package android.common;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.widget.ImageView;
import xt.com.tongyong.id884999.R;

public class CustomProgressDialog extends Dialog {
    private static CustomProgressDialog customProgressDialog = null;
    private Context context;

    public CustomProgressDialog(Context context2) {
        super(context2);
        this.context = context2;
    }

    public CustomProgressDialog(Context context2, int theme) {
        super(context2, theme);
    }

    public static CustomProgressDialog createDialog(Context context2) {
        customProgressDialog = new CustomProgressDialog(context2, R.style.DialogStyle);
        customProgressDialog.setContentView((int) R.layout.adminor);
        customProgressDialog.getWindow().getAttributes().gravity = 17;
        customProgressDialog.getWindow().getAttributes().gravity = 80;
        return customProgressDialog;
    }

    public void onWindowFocusChanged(boolean hasFocus) {
        if (customProgressDialog != null) {
            ((AnimationDrawable) ((ImageView) customProgressDialog.findViewById(R.id.loadingImageView)).getBackground()).start();
        }
    }

    public CustomProgressDialog setTitile(String strTitle) {
        return customProgressDialog;
    }
}
