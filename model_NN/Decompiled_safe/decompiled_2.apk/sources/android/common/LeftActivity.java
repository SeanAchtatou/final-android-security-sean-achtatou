package android.common;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageButton;
import java.io.IOException;
import java.io.InputStream;
import xt.com.tongyong.id884999.R;

public class LeftActivity extends Activity {
    ImageButton imgBtn;
    public InputStream update;
    private WebView webView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.left);
        this.imgBtn = (ImageButton) findViewById(R.id.imgBtnBack);
        this.imgBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ((MyApplication) LeftActivity.this.getApplicationContext()).viewPagerActivity.viewPager.setCurrentItem(1, true);
            }
        });
        this.webView = (WebView) findViewById(R.id.webView);
        this.webView.getSettings().setJavaScriptEnabled(true);
        this.webView.setScrollBarStyle(0);
        this.webView.setWebViewClient(new LeftWebViewClient(this));
        try {
            this.update = getResources().getAssets().open("update.xml");
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.webView.loadUrl(new Dom(this.update).GetAppInfo().navpage);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return false;
        }
        ((MyApplication) getApplicationContext()).viewPagerActivity.viewPager.setCurrentItem(1, true);
        return true;
    }
}
