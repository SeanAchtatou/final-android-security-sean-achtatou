package android.common;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.TextView;
import android.xtcore.Gloabl;
import com.baidu.android.pushservice.PushConstants;
import java.io.IOException;
import xt.com.tongyong.id884999.R;

public class TitleBar {
    Activity activity_;
    ImageButton imgLeft = ((ImageButton) this.activity_.findViewById(R.id.imgBtnBack));
    ImageButton imgRight = ((ImageButton) this.activity_.findViewById(R.id.imgBtn_Right));
    TextView textTitle = ((TextView) this.activity_.findViewById(R.id.titleTxt));
    UrlData urlData;
    WebView webView_;

    public TitleBar(Activity activity, WebView webView) {
        this.activity_ = activity;
        this.webView_ = webView;
        if (Gloabl.LoadUrls > 0) {
            hideBtn();
        }
        Gloabl.LoadUrls++;
    }

    /* access modifiers changed from: package-private */
    public void hideBtn() {
        this.imgLeft.setVisibility(4);
        this.imgRight.setVisibility(4);
    }

    public void TitleBarLayout(final Bundle bundle_) {
        hideBtn();
        if (bundle_.getString("style") != "") {
            if (bundle_.getString("style").equals("home")) {
                this.imgLeft.setVisibility(0);
                this.imgLeft.setBackgroundResource(R.drawable.menupre);
                this.textTitle.setText(bundle_.getString("firstpage_title"));
                this.imgRight.setVisibility(0);
                this.imgRight.setBackgroundResource(R.drawable.aboutpre);
            } else if (bundle_.getString("style").equals("text")) {
                this.imgLeft.setVisibility(0);
                this.imgLeft.setBackgroundResource(R.drawable.backpre);
                this.textTitle.setText(bundle_.getString("describe"));
                if (bundle_.getString("fun") != "") {
                    String fun = bundle_.getString("fun");
                    this.imgRight.setVisibility(0);
                    if (fun.equals("member")) {
                        this.imgRight.setBackgroundResource(R.drawable.memberpre);
                    } else if (fun.equals("home")) {
                        this.imgRight.setBackgroundResource(R.drawable.homepre);
                    } else if (fun.equals("refresh")) {
                        this.imgRight.setBackgroundResource(R.drawable.refreshpre);
                    } else if (fun.equals("category")) {
                        this.imgRight.setBackgroundResource(R.drawable.categopre);
                    } else if (fun.equals("shopping")) {
                        this.imgRight.setBackgroundResource(R.drawable.shoppingpre);
                    } else if (fun.equals("menu")) {
                        this.imgRight.setBackgroundResource(R.drawable.menupre);
                    } else {
                        this.imgRight.setVisibility(4);
                    }
                }
            }
        }
        this.imgRight.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.v("fun", bundle_.getString("fun"));
                if (bundle_.getString("style").equals("home") || bundle_.getString("fun") == "") {
                    Intent intent = new Intent();
                    intent.setClass(TitleBar.this.activity_, SeetingAty.class);
                    TitleBar.this.activity_.startActivity(intent);
                } else if (bundle_.getString("fun").equals("menu")) {
                    ((MyApplication) TitleBar.this.activity_.getApplicationContext()).viewPagerActivity.viewPager.setCurrentItem(0, true);
                } else {
                    TitleBar.this.imgRight(bundle_);
                }
            }
        });
        this.imgLeft.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (bundle_.getString("style") == null) {
                    return;
                }
                if (bundle_.getString("style").equals("home")) {
                    ((MyApplication) TitleBar.this.activity_.getApplicationContext()).viewPagerActivity.viewPager.setCurrentItem(0, true);
                    return;
                }
                try {
                    Runtime.getRuntime().exec("input keyevent 4");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void imgRight(Bundle bundle_) {
        String link = bundle_.getString("link");
        String target = bundle_.getString("target");
        if (target == null) {
            return;
        }
        if (target.equals(PushConstants.EXTRA_APP)) {
            this.webView_.loadUrl(link);
        } else if (target.equals("browser")) {
            Uri uri = Uri.parse(link);
            Intent intent = new Intent();
            intent.setData(uri);
            intent.setAction("android.intent.action.VIEW");
            intent.setClassName("com.android.browser", "com.android.browser.BrowserActivity");
            this.activity_.startActivity(intent);
        }
    }
}
