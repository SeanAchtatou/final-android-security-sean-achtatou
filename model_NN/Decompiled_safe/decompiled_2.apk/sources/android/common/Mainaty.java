package android.common;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.baidu.android.pushservice.PushManager;
import com.baidu.android.pushservice.PushSettings;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import xt.com.tongyong.id884999.R;

public class Mainaty extends Activity {
    /* access modifiers changed from: private */
    public static String IMAGE_UNSPECIFIED = "image/*";
    static final int JUDGE_VERSION = 1;
    static final int NET_WRONG = 2;
    /* access modifiers changed from: private */
    public static int PHOTO_GRAPH = 1;
    /* access modifiers changed from: private */
    public static int PHOTO_ZOOM = 2;
    static final int Text_IMG = 3;
    /* access modifiers changed from: private */
    public static Context context;
    public static Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                    intent.putExtra("output", Uri.fromFile(new File(Environment.getExternalStorageDirectory(), "temp.jpg")));
                    ((Activity) Mainaty.context).startActivityForResult(intent, Mainaty.PHOTO_GRAPH);
                    return;
                case 2:
                    Intent intentAlbum = new Intent("android.intent.action.PICK", (Uri) null);
                    intentAlbum.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, Mainaty.IMAGE_UNSPECIFIED);
                    ((Activity) Mainaty.context).startActivityForResult(intentAlbum, Mainaty.PHOTO_ZOOM);
                    return;
                default:
                    return;
            }
        }
    };
    public static ThreadHandler handlerMap;
    public InputStream IntentIs = null;
    private int NONE = 0;
    private int PHOTO_RESOULT = 3;
    public ProgressDialog dialog;
    String firstpage_title = null;
    String loadUrlString = null;
    NetConnect netConnect;
    ProgressBar progressBar;
    String pushUrlString = null;
    RelativeLayout reltUserPhoto;
    TitleBar titleBar;
    TextView titleTxt;
    public UpdateDom updateDom;
    public WebView webview;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        ((MyApplication) getApplicationContext()).mainActivity = this;
        context = this;
        this.titleTxt = (TextView) findViewById(R.id.titleTxt);
        this.progressBar = (ProgressBar) findViewById(R.id.progressBar);
        handlerMap = new ThreadHandler();
        this.updateDom = new UpdateDom(context);
        this.firstpage_title = this.updateDom.oldXml.GetAppInfo().startpagenavname;
        if (this.firstpage_title == null || this.firstpage_title.equals("")) {
            this.firstpage_title = this.updateDom.oldXml.GetAppInfo().name;
        }
        this.webview = (WebView) findViewById(R.id.webView);
        this.titleBar = new TitleBar(this, this.webview);
        this.webview.getSettings().setJavaScriptEnabled(true);
        this.webview.setWebChromeClient(new MyWebChromeClient());
        this.webview.setScrollBarStyle(0);
        this.webview.setWebViewClient(new MyWebViewClient(context, this.dialog, this.webview, this.titleBar, this.progressBar, this.firstpage_title, this.updateDom.oldXml.GetWebApi().setappinstall));
        this.webview.getSettings().setSavePassword(false);
        this.pushUrlString = getIntent().getStringExtra("pushUrl");
        if (this.pushUrlString != null) {
            this.loadUrlString = this.pushUrlString;
        } else {
            this.loadUrlString = this.updateDom.oldXml.GetAppInfo().startpage;
        }
        new Thread(new ThreadMap()).start();
        String baiduKey = "";
        try {
            baiduKey = this.updateDom.oldXml.GetCloudpush().baiduapikey;
        } catch (Exception e) {
        }
        PushSettings.enableDebugMode(context, true);
        PushManager.startWork(context, 0, baiduKey);
    }

    class ThreadMap extends Thread {
        ThreadMap() {
        }

        public void run() {
            try {
                new Thread(new Runnable() {
                    public void run() {
                        Mainaty.this.netConnect = new NetConnect(Mainaty.context);
                        if (Mainaty.this.netConnect.isConnectingToInternet()) {
                            Mainaty.this.webview.loadUrl(Mainaty.this.loadUrlString);
                            return;
                        }
                        Message msg = Mainaty.handlerMap.obtainMessage();
                        msg.what = 2;
                        msg.sendToTarget();
                    }
                }).start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    class ThreadHandler extends Handler {
        ThreadHandler() {
        }

        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    Mainaty.this.titleTxt.setTag(Mainaty.this.firstpage_title);
                    Mainaty.this.updateDom.HasNewVersionWindow(false);
                    return;
                case 2:
                    Mainaty.this.install();
                    return;
                case 3:
                    Mainaty.this.webview.stopLoading();
                    Mainaty.this.webview.clearView();
                    Mainaty.this.webview.loadUrl("file:///android_asset/404.html");
                    return;
                default:
                    return;
            }
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || !this.webview.canGoBack()) {
            if (keyCode == 4) {
                DialogHelper.EnsureAndCancelDialog(context, context.getResources().getString(R.string.dialogExit), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Mainaty.this.finish();
                        System.exit(0);
                    }
                }, null);
            }
            return false;
        }
        this.webview.goBack();
        return true;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 2, 3, context.getString(R.string.exit)).setIcon(17301552);
        menu.add(0, 3, 1, context.getString(R.string.about_app)).setIcon(17301569);
        menu.add(0, 4, 2, context.getString(R.string.reload)).setIcon(17301581);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 2:
                finish();
                System.exit(0);
                break;
            case 3:
                Intent intent = new Intent();
                intent.setClass(context, SeetingAty.class);
                startActivity(intent);
                break;
            case 4:
                this.webview.clearCache(true);
                this.webview.reload();
                break;
        }
        return false;
    }

    public void install() {
        DialogHelper.EnsureAndCancelDialog(context, context.getResources().getString(R.string.internet), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent;
                new Intent();
                if (Build.VERSION.SDK_INT > 10) {
                    intent = new Intent("android.settings.WIRELESS_SETTINGS");
                } else {
                    intent = new Intent();
                    intent.setComponent(new ComponentName("com.android.settings", "com.android.settings.WirelessSettings"));
                    intent.setAction("android.intent.action.VIEW");
                }
                Mainaty.context.startActivity(intent);
                Mainaty.this.finish();
            }
        }, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Mainaty.this.finish();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Bundle extras;
        if (resultCode != this.NONE) {
            if (requestCode == PHOTO_GRAPH) {
                File picture = new File(Environment.getExternalStorageDirectory() + "/temp.jpg");
                Toast.makeText(context, picture.toString(), 1).show();
                startPhotoZoom(Uri.fromFile(picture));
            }
            if (data != null) {
                if (requestCode == PHOTO_ZOOM) {
                    startPhotoZoom(data.getData());
                }
                if (requestCode == this.PHOTO_RESOULT && (extras = data.getExtras()) != null) {
                    ((Bitmap) extras.getParcelable("data")).compress(Bitmap.CompressFormat.JPEG, 75, new ByteArrayOutputStream());
                }
                if (requestCode == 110) {
                    this.webview.loadUrl(data.getStringExtra("url"));
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void startPhotoZoom(Uri uri) {
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, IMAGE_UNSPECIFIED);
        intent.putExtra("crop", "true");
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        intent.putExtra("outputX", 103);
        intent.putExtra("outputY", 103);
        intent.putExtra("return-data", true);
        startActivityForResult(intent, this.PHOTO_RESOULT);
    }
}
