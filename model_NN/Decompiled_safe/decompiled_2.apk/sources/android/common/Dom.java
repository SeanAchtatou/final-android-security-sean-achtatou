package android.common;

import java.io.InputStream;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class Dom {
    private AppInfo appInfo;
    private DocumentBuilder builder = null;
    private Cloudpush cloudpush;
    private Document document = null;
    private Element element = null;
    private DocumentBuilderFactory factory = null;
    private Webapi webapi;

    class AppInfo {
        public float Version;
        public String appId;
        public String category;
        public String description;
        public String developer;
        public String identifier;
        public String name;
        public String navpage;
        public String startpage;
        public String startpagenavname;

        AppInfo() {
        }
    }

    class Cloudpush {
        public String baiduapikey;

        Cloudpush() {
        }
    }

    class Startpicture {
        public ArrayList<SPNode> pics_;
        public float version_;

        Startpicture() {
        }

        class SPNode {
            public String imgSrc_;
            public int index_;

            SPNode() {
            }
        }
    }

    class Webapi {
        public String getappshop;
        public String getversion;
        public String setappinstall;
        public String setdevicetoken;

        Webapi() {
        }
    }

    public Webapi GetWebApi() {
        return this.webapi;
    }

    public AppInfo GetAppInfo() {
        return this.appInfo;
    }

    public Cloudpush GetCloudpush() {
        return this.cloudpush;
    }

    public Dom(InputStream is) {
        if (is != null) {
            try {
                this.factory = DocumentBuilderFactory.newInstance();
                this.builder = this.factory.newDocumentBuilder();
                this.document = this.builder.parse(is);
                this.element = this.document.getDocumentElement();
                this.webapi = getWebApi();
                this.appInfo = getInfo();
                this.cloudpush = getCloudPush();
            } catch (Exception e) {
            }
        }
    }

    private Webapi getWebApi() {
        Webapi webapi_ = new Webapi();
        Element WebapiElement = (Element) this.element.getElementsByTagName("webapi").item(0);
        if (WebapiElement == null) {
            return null;
        }
        NodeList childNodes = WebapiElement.getChildNodes();
        int childNodesLength = childNodes.getLength();
        for (int i = 0; i < childNodesLength; i++) {
            if (childNodes.item(i).getNodeType() == 1) {
                String appNodeName = childNodes.item(i).getNodeName();
                if ("getversion".equals(appNodeName)) {
                    webapi_.getversion = childNodes.item(i).getTextContent();
                    System.out.println(webapi_.getversion + "webapi_.getversion");
                } else if ("getappshop".equals(appNodeName)) {
                    webapi_.getappshop = childNodes.item(i).getTextContent();
                    System.out.println(webapi_.getappshop + "webapi_.getappshop");
                } else if ("setdevicetoken".equals(appNodeName)) {
                    webapi_.setdevicetoken = childNodes.item(i).getTextContent();
                    System.out.println(webapi_.setdevicetoken + "webapi_.setdevicetoken");
                } else if ("setappinstall".equals(appNodeName)) {
                    webapi_.setappinstall = childNodes.item(i).getTextContent();
                    System.out.println(webapi_.setappinstall + "webapi_.setappinstall");
                }
            }
        }
        return webapi_;
    }

    private AppInfo getInfo() {
        AppInfo appinfo_ = new AppInfo();
        Element appinfoElement = (Element) this.element.getElementsByTagName("appinfo").item(0);
        if (appinfoElement == null) {
            return null;
        }
        NodeList childNodes = appinfoElement.getChildNodes();
        int childNodesLength = childNodes.getLength();
        for (int i = 0; i < childNodesLength; i++) {
            if (childNodes.item(i).getNodeType() == 1) {
                String appNodeName = childNodes.item(i).getNodeName();
                String xmlValueString = childNodes.item(i).getTextContent().trim();
                if ("identifier".equals(appNodeName)) {
                    appinfo_.identifier = xmlValueString;
                } else if ("name".equals(appNodeName)) {
                    appinfo_.name = xmlValueString;
                } else if ("appid".equals(appNodeName)) {
                    appinfo_.appId = xmlValueString;
                } else if ("version".equals(appNodeName)) {
                    appinfo_.Version = Float.parseFloat(xmlValueString);
                } else if ("startpage".equals(appNodeName)) {
                    appinfo_.startpage = xmlValueString;
                } else if ("navpage".equals(appNodeName)) {
                    appinfo_.navpage = xmlValueString;
                } else if ("developer".equals(appNodeName)) {
                    appinfo_.developer = xmlValueString;
                } else if ("description".equals(appNodeName)) {
                    appinfo_.description = xmlValueString;
                } else if ("category".equals(appNodeName)) {
                    appinfo_.category = xmlValueString;
                } else if ("startpagenavname".equals(appNodeName)) {
                    appinfo_.startpagenavname = xmlValueString;
                }
            }
        }
        return appinfo_;
    }

    private Cloudpush getCloudPush() {
        Cloudpush cloudpush_ = new Cloudpush();
        Element cloudpushElement = (Element) this.element.getElementsByTagName("cloudpush").item(0);
        if (cloudpushElement == null) {
            return null;
        }
        NodeList childNodes = cloudpushElement.getChildNodes();
        int cloudpushNodelstLength = childNodes.getLength();
        for (int i = 0; i < cloudpushNodelstLength; i++) {
            if (childNodes.item(i).getNodeType() == 1 && "baiduapikey".equals(childNodes.item(i).getNodeName())) {
                cloudpush_.baiduapikey = childNodes.item(i).getTextContent();
                System.out.println(cloudpush_.baiduapikey + "cloudpush.baiduapikey");
            }
        }
        return cloudpush_;
    }
}
