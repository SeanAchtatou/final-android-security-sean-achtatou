package android.common;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import xt.com.tongyong.id884999.R;

public class OutBrowserActivity extends Activity {
    private ImageButton backBtn;
    /* access modifiers changed from: private */
    public Context context;
    private ImageView imgView;
    GifLoadView loadView;
    TextView txtView;
    private WebView webview;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(2);
        setContentView((int) R.layout.outbrowser);
        this.context = this;
        this.backBtn = (ImageButton) findViewById(R.id.imgbackaboutus);
        this.imgView = (ImageView) findViewById(R.id.imgViewLoding);
        this.webview = (WebView) findViewById(R.id.webView);
        this.txtView = (TextView) findViewById(R.id.txtWebTitle);
        this.loadView = new GifLoadView(this.context, this.imgView, 100);
        webviewSetting();
        setViewClient();
        this.webview.loadUrl(getIntent().getStringExtra("weburl"));
        this.backBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                OutBrowserActivity.this.finish();
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void webviewSetting() {
        this.webview.setWebChromeClient(new OutBrowserWebChromeClient(this, this.txtView));
        this.webview.getSettings().setJavaScriptEnabled(true);
        this.webview.getSettings().setPluginsEnabled(true);
        this.webview.setScrollBarStyle(0);
        this.webview.getSettings().setBuiltInZoomControls(false);
    }

    /* access modifiers changed from: package-private */
    public void setViewClient() {
        this.webview.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Log.v("OutBrowser_OverrideUrlLoading", url);
                String lowerUrl = url.toLowerCase();
                if (!lowerUrl.startsWith("http") || lowerUrl.startsWith("https") || !lowerUrl.startsWith("ftp")) {
                    return false;
                }
                view.loadUrl(url);
                return true;
            }

            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                try {
                    Log.v("OutBrowser_onPageStarted", url);
                    OutBrowserActivity.this.txtView.setText((int) R.string.loading);
                    OutBrowserActivity.this.loadView.onStart();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            public void onPageFinished(WebView view, String url) {
                try {
                    Log.v("OutBrowser_onPageFinished", url);
                    if (OutBrowserActivity.this.txtView.getText().equals(OutBrowserActivity.this.context.getString(R.string.loading))) {
                        OutBrowserActivity.this.txtView.setText((int) R.string.notitle);
                    }
                    OutBrowserActivity.this.loadView.onStop();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            if (this.webview.canGoBack()) {
                this.webview.goBack();
                return true;
            }
            finish();
        }
        return false;
    }
}
