package android.common;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.HttpAuthHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.xtcore.Gloabl;
import com.baidu.android.pushservice.PushConstants;
import java.net.URLDecoder;
import xt.com.tongyong.id884999.R;

public class MyWebViewClient extends WebViewClient {
    static final int IMGBTN_RIGHT = 3;
    static final int Text_IMG = 2;
    public static String UrlEnd;
    public static String UrlLoading;
    public static String UrlStart;
    ProgressBar _progressBar;
    Bundle bundle;
    NetConnect contet;
    public Context context_;
    public ProgressDialog dialog_;
    String firstpage_title;
    String installUrl;
    public View.OnClickListener itemsOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            MyWebViewClient.this.popWindow.dismiss();
            switch (v.getId()) {
                case R.id.f0btn_photos /*2131230736*/:
                    new Thread(new Runnable() {
                        public void run() {
                            Message message = Mainaty.handler.obtainMessage();
                            message.what = 1;
                            message.sendToTarget();
                        }
                    }).start();
                    return;
                case R.id.btn_album /*2131230737*/:
                    new Thread(new Runnable() {
                        public void run() {
                            Message message = Mainaty.handler.obtainMessage();
                            message.what = 2;
                            message.sendToTarget();
                        }
                    }).start();
                    return;
                case R.id.btn_cancel /*2131230738*/:
                default:
                    return;
            }
        }
    };
    PopupWindow popWindow;
    TitleBar titleBar_;
    UriParaMap uriParaMap = new UriParaMap("", "");
    public WebView webview_;

    public void onReceivedHttpAuthRequest(WebView view, HttpAuthHandler handler, String host, String realm) {
        super.onReceivedHttpAuthRequest(view, handler, host, realm);
    }

    public MyWebViewClient(Context context, ProgressDialog dialog, WebView webview, TitleBar titleBar, ProgressBar progressBar, String firstpageTitle, String installUrl2) {
        this.dialog_ = dialog;
        this.context_ = context;
        this.webview_ = webview;
        this.titleBar_ = titleBar;
        this._progressBar = progressBar;
        this.firstpage_title = firstpageTitle;
        this.installUrl = installUrl2;
    }

    public void onPageFinished(WebView view, String url) {
        UrlEnd = url;
        view.loadUrl("javascript: var allLinks = document.getElementsByTagName('a'); if (allLinks) {var i;for (i=0; i<allLinks.length; i++) {var link = allLinks[i];var target = link.getAttribute('target'); if (target && target == '_blank') {link.setAttribute('target','_self');link.href = 'newtab:'+link.href;}}}");
        view.loadUrl("javascript: var allLinks = document.getElementsByTagName('form'); if (allLinks) {var i;for (i=0; i<allLinks.length; i++) {var link = allLinks[i];var target = link.getAttribute('target'); if (target && target == '_blank') {link.setAttribute('target','_self');link.action = 'newtab:'+link.action;}}}");
        if (UrlEnd.equals(UrlLoading) || UrlEnd != UrlStart) {
            this._progressBar.setVisibility(8);
        }
        super.onPageFinished(view, url);
        if (Gloabl.LoadUrls == 1) {
            Gloabl.LoadUrls++;
            try {
                new Thread(new Runnable() {
                    public void run() {
                        Message msg = Mainaty.handlerMap.obtainMessage();
                        msg.what = 1;
                        msg.sendToTarget();
                    }
                }).start();
                new RegisterApp(this.context_).Register(this.installUrl);
            } catch (Exception e) {
            }
        }
    }

    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        this.contet = new NetConnect(this.context_);
        if (UrlStart != UrlLoading) {
            this._progressBar.setVisibility(8);
        }
        this.webview_.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);
                if (newProgress == MyWebViewClient.this._progressBar.getMax()) {
                    MyWebViewClient.this._progressBar.setVisibility(8);
                    return;
                }
                MyWebViewClient.this._progressBar.setVisibility(0);
                MyWebViewClient.this._progressBar.setProgress(newProgress);
            }
        });
        UrlStart = url;
        super.onPageStarted(view, url, favicon);
    }

    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        view.setWebChromeClient(new MyWebChromeClient());
        UrlLoading = url;
        if (url.startsWith("appcall://share?")) {
            this.uriParaMap.InitPara(url, "appcall://share?");
            String content = this.uriParaMap.getParaValue(PushConstants.EXTRA_CONTENT);
            Intent intent = new Intent("android.intent.action.SEND");
            intent.setType("image/*");
            intent.putExtra("android.intent.extra.TEXT", content);
            intent.setFlags(268435456);
            this.context_.startActivity(Intent.createChooser(intent, this.context_.getString(R.string.share)));
            return true;
        } else if (url.startsWith("tel:")) {
            this.uriParaMap.InitPara(url, "tel:");
            this.context_.startActivity(new Intent("android.intent.action.DIAL", Uri.parse("tel:" + this.uriParaMap.getOriUri(true))));
            return true;
        } else if (url.startsWith("mailto:")) {
            this.uriParaMap.InitPara(url, "mailto:");
            String to = "";
            String content2 = "";
            String[] strTemp = this.uriParaMap.getOriUri(false).split("\\?");
            try {
                if (strTemp.length == 2) {
                    to = URLDecoder.decode(strTemp[0], "utf-8");
                    String[] strContents = strTemp[1].split("\\=");
                    if (strContents.length == 2) {
                        content2 = URLDecoder.decode(strContents[1], "utf-8");
                    }
                }
            } catch (Exception e) {
            }
            Intent email = new Intent("android.intent.action.SEND");
            email.setType("plain/text");
            email.putExtra("android.intent.extra.EMAIL", new String[]{to});
            email.putExtra("android.intent.extra.SUBJECT", "");
            email.putExtra("android.intent.extra.TEXT", content2);
            this.context_.startActivity(Intent.createChooser(email, this.context_.getString(R.string.email)));
            return true;
        } else if (url.startsWith("appcall://openMap?")) {
            this.uriParaMap.InitPara(url, "appcall://openMap?");
            String zoom = this.uriParaMap.getParaValue("zoom");
            String mark = this.uriParaMap.getParaValue("mark");
            String latitude = this.uriParaMap.getParaValue("latitude");
            String longitude = this.uriParaMap.getParaValue("longitude");
            Uri uriMap = Uri.parse("http://maps.google.com/maps?hl=zh-CN&mrt=loc&iwloc=A&q=" + latitude + ",+" + longitude + "+(" + mark + ")" + "&z=" + zoom);
            try {
                Intent intentGoogle = new Intent("android.intent.action.VIEW", uriMap);
                intentGoogle.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                this.context_.startActivity(intentGoogle);
                return true;
            } catch (Exception e2) {
                try {
                    Intent intentBaidu = new Intent("android.intent.action.VIEW");
                    intentBaidu.setData(Uri.parse("geo:" + latitude + "," + longitude + "," + mark));
                    intentBaidu.setPackage("com.baidu.BaiduMap");
                    this.context_.startActivity(intentBaidu);
                    return true;
                } catch (Exception e3) {
                    try {
                        Intent intentWeb = new Intent("android.intent.action.VIEW");
                        intentWeb.setData(uriMap);
                        this.context_.startActivity(intentWeb);
                        return true;
                    } catch (Exception e4) {
                        return true;
                    }
                }
            }
        } else if (url.startsWith("appcall://callPhoto?")) {
            this.uriParaMap.InitPara(url, "appcall://callPhoto?");
            this.popWindow = new PopWindowShare(this.context_, this.itemsOnClick);
            this.popWindow.showAtLocation(view, 81, 0, 0);
            return true;
        } else if (url.startsWith("appcall://navswitch?")) {
            this.uriParaMap.InitPara(url, "appcall://navswitch?");
            this.bundle = new Bundle();
            this.bundle.putString("firstpage_title", this.firstpage_title);
            this.bundle.putString("urlend", UrlEnd);
            this.bundle.putString("style", this.uriParaMap.getParaValue("style"));
            this.bundle.putString("describe", this.uriParaMap.getParaValue("describe"));
            this.bundle.putString("fun", this.uriParaMap.getParaValue("fun"));
            this.bundle.putString("link", this.uriParaMap.getParaValue("link"));
            this.bundle.putString("target", this.uriParaMap.getParaValue("target"));
            this.titleBar_.TitleBarLayout(this.bundle);
            return true;
        } else if (url.startsWith("appcall://update")) {
            new UpdateDom(this.context_).HasNewVersionWindow(true);
            return true;
        } else if (url.startsWith("newtab:")) {
            this.uriParaMap.InitPara(url, "newtab:");
            Uri content_url = Uri.parse(this.uriParaMap.getOriUri(true));
            Intent intent2 = new Intent();
            intent2.setAction("android.intent.action.VIEW");
            intent2.setClassName("com.android.browser", "com.android.browser.BrowserActivity");
            intent2.setData(content_url);
            this.context_.startActivity(intent2);
            return true;
        } else {
            this.contet = new NetConnect(this.context_);
            if (this.contet.isConnectingToInternet()) {
                synCookies(url);
                view.loadUrl(url);
                return true;
            }
            Mainaty.handlerMap.obtainMessage(2).sendToTarget();
            return true;
        }
    }

    public void synCookies(String url) {
        CookieSyncManager cookieSyncManager = CookieSyncManager.createInstance(this.webview_.getContext());
        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.setAcceptCookie(true);
        cookieManager.setCookie(url, "accesstype=app");
        cookieSyncManager.sync();
    }
}
