package android.common;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import xt.com.tongyong.id884999.R;

public class UrlData {
    public InputStream Net_is;
    /* access modifiers changed from: private */
    public Context _context;
    private HttpURLConnection connection;
    public InputStream is;
    public Dom new_dom;
    public Dom old_dom;

    public UrlData(Context context) {
        this._context = context;
        try {
            this.is = this._context.getResources().getAssets().open("update.xml");
            this.old_dom = new Dom(this.is);
            this.Net_is = HttpHelper.getContent(this.old_dom.GetWebApi().getversion);
            this.new_dom = new Dom(this.Net_is);
        } catch (Exception e) {
        }
    }

    public String Url(String path) {
        try {
            this.connection = (HttpURLConnection) new URL(path).openConnection();
            this.connection.setConnectTimeout(6000);
            this.connection.connect();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return path;
    }

    public void WeatherHasNewVersion(Context _context2) {
        if (this.Net_is != null && this.old_dom.GetAppInfo().Version < this.new_dom.GetAppInfo().Version) {
            DialogHelper.EnsureAndCancelDialog(_context2, _context2.getString(R.string.have_a_new_version), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent("android.intent.action.VIEW");
                    intent.setData(Uri.parse(UrlData.this.new_dom.GetWebApi().getappshop));
                    UrlData.this._context.startActivity(intent);
                }
            }, null);
        }
    }
}
