package android.common;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import xt.com.tongyong.id884999.R;

public class DialogHelper {
    public static void EnsureAndCancelDialog(Context context, String message, DialogInterface.OnClickListener confirmHandler, DialogInterface.OnClickListener cancelHandler) {
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle((int) R.string.notification).setMessage(message).setPositiveButton((int) R.string.confirm, confirmHandler).setNegativeButton((int) R.string.cancel, cancelHandler);
        alert.create().show();
    }

    public static void EnsureDialog(Context context, String message, DialogInterface.OnClickListener confirmHandler) {
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle((int) R.string.notification).setMessage(message).setPositiveButton((int) R.string.confirm, confirmHandler);
        alert.create().show();
    }
}
