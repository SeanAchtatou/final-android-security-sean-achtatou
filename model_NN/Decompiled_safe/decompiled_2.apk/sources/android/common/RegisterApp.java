package android.common;

import android.content.Context;

public class RegisterApp {
    static final String TAG = "RegisterAPP";
    Context context;
    PKV kv_ = new PKV(this.context, TAG);

    public RegisterApp(Context ctx) {
        this.context = ctx;
    }

    /* JADX WARN: Type inference failed for: r7v11, types: [java.net.URLConnection] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void Register(java.lang.String r10) {
        /*
            r9 = this;
            r3 = 0
            r2 = 0
            android.common.PKV r7 = r9.kv_
            java.lang.String r8 = "uuid"
            boolean r7 = r7.ExistKey(r8)
            if (r7 == 0) goto L_0x0053
            android.common.PKV r7 = r9.kv_
            java.lang.String r8 = "uuid"
            java.lang.String r2 = r7.ReadVByK(r8)
        L_0x0014:
            r4 = 0
            java.net.URL r5 = new java.net.URL     // Catch:{ MalformedURLException -> 0x0063 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0063 }
            r7.<init>()     // Catch:{ MalformedURLException -> 0x0063 }
            java.lang.StringBuilder r7 = r7.append(r10)     // Catch:{ MalformedURLException -> 0x0063 }
            java.lang.String r8 = "&guid="
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ MalformedURLException -> 0x0063 }
            java.lang.StringBuilder r7 = r7.append(r2)     // Catch:{ MalformedURLException -> 0x0063 }
            java.lang.String r8 = "&ostype=android"
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ MalformedURLException -> 0x0063 }
            java.lang.String r7 = r7.toString()     // Catch:{ MalformedURLException -> 0x0063 }
            r5.<init>(r7)     // Catch:{ MalformedURLException -> 0x0063 }
            java.net.URLConnection r7 = r5.openConnection()     // Catch:{ IOException -> 0x0065, all -> 0x006c }
            r0 = r7
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ IOException -> 0x0065, all -> 0x006c }
            r3 = r0
            r7 = 3000(0xbb8, float:4.204E-42)
            r3.setConnectTimeout(r7)     // Catch:{ IOException -> 0x0065, all -> 0x006c }
            r7 = 3000(0xbb8, float:4.204E-42)
            r3.setReadTimeout(r7)     // Catch:{ IOException -> 0x0065, all -> 0x006c }
            r3.getResponseCode()     // Catch:{ IOException -> 0x0065, all -> 0x006c }
            if (r3 == 0) goto L_0x0051
            r3.disconnect()
        L_0x0051:
            r4 = r5
        L_0x0052:
            return
        L_0x0053:
            java.util.UUID r6 = java.util.UUID.randomUUID()
            java.lang.String r2 = r6.toString()
            android.common.PKV r7 = r9.kv_
            java.lang.String r8 = "uuid"
            r7.AddKV(r8, r2)
            goto L_0x0014
        L_0x0063:
            r1 = move-exception
            goto L_0x0052
        L_0x0065:
            r7 = move-exception
            if (r3 == 0) goto L_0x0051
            r3.disconnect()
            goto L_0x0051
        L_0x006c:
            r7 = move-exception
            if (r3 == 0) goto L_0x0072
            r3.disconnect()
        L_0x0072:
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: android.common.RegisterApp.Register(java.lang.String):void");
    }
}
