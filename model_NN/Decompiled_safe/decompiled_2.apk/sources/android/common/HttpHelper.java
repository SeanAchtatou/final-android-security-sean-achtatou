package android.common;

import android.util.Log;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpHelper {
    static String TAG = "HttpHelper";
    private static int connectTimeout = 30000;
    private static int readTimeout = 30000;

    public static InputStream getContent(String url) throws IOException {
        Log.v(TAG, "get content:" + url);
        InputStream is = null;
        HttpURLConnection httpConnection = (HttpURLConnection) new URL(url).openConnection();
        httpConnection.setConnectTimeout(connectTimeout);
        httpConnection.setReadTimeout(readTimeout);
        int error = httpConnection.getResponseCode();
        Log.v(TAG, "responseCode:" + String.valueOf(error));
        if (error != 404) {
            try {
                is = httpConnection.getInputStream();
            } catch (IOException ex) {
                Log.v(TAG, ex.getMessage());
            }
            System.out.println(is + "HttpHelper0000");
            Log.v(TAG, "成功");
            return is;
        }
        Log.v(TAG, "失败");
        return null;
    }
}
