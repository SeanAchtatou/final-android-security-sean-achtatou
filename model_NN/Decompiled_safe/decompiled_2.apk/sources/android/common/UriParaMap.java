package android.common;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

public class UriParaMap {
    private Map<String, String> paraMap = new HashMap();
    private String tempUriString = "";

    public UriParaMap(String uri, String strReplarePrefix) {
        InitPara(uri, strReplarePrefix);
    }

    public void InitPara(String uri, String strReplarePrefix) {
        this.paraMap.clear();
        this.tempUriString = "";
        if (uri != null && uri.length() > 0) {
            this.tempUriString = uri;
            if (strReplarePrefix != null && !strReplarePrefix.equals("")) {
                this.tempUriString = this.tempUriString.replace(strReplarePrefix, "");
            }
            formatPara(this.tempUriString);
        }
    }

    public String getOriUri(Boolean decode) {
        String retVal = this.tempUriString;
        if (!decode.equals(true)) {
            return retVal;
        }
        try {
            return URLDecoder.decode(this.tempUriString, "utf-8");
        } catch (Exception e) {
            return retVal;
        }
    }

    public Map<String, String> getParaMap() {
        return this.paraMap;
    }

    public String getParaValue(String key) {
        if (this.paraMap.containsKey(key)) {
            return this.paraMap.get(key);
        }
        return "";
    }

    /* access modifiers changed from: package-private */
    public void formatPara(String uri) {
        for (String v : uri.split("\\&")) {
            String[] strTemp = v.split("\\=");
            if (strTemp.length == 2) {
                try {
                    this.paraMap.put(strTemp[0], URLDecoder.decode(strTemp[1], "utf-8"));
                } catch (UnsupportedEncodingException e) {
                    this.paraMap.put(strTemp[0], "");
                }
            } else {
                this.paraMap.put(strTemp[0], "");
            }
        }
    }
}
