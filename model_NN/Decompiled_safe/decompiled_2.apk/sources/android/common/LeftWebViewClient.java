package android.common;

import android.app.Activity;
import android.graphics.Bitmap;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class LeftWebViewClient extends WebViewClient {
    private Activity context_;
    private int count = 0;

    public LeftWebViewClient(Activity context) {
        this.context_ = context;
    }

    public void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);
    }

    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        super.onPageStarted(view, url, favicon);
    }

    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        ((MyApplication) this.context_.getApplicationContext()).mainActivity.webview.loadUrl(url);
        ((MyApplication) this.context_.getApplicationContext()).viewPagerActivity.viewPager.setCurrentItem(1, false);
        return true;
    }
}
