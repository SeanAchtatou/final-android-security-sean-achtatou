package android.xtcore;

import android.app.Activity;
import android.common.ViewPagerActivity;
import android.content.Intent;
import android.os.Bundle;
import xt.com.tongyong.id884999.R;

public class Start extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_start);
        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
        }
        Intent intent = new Intent();
        intent.setClass(this, ViewPagerActivity.class);
        startActivity(intent);
        finish();
    }
}
