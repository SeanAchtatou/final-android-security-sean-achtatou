package xt.com.tongyong.id884999;

public final class R {

    public static final class anim {
        public static final int admi = 2130968576;
        public static final int in_from_left = 2130968577;
        public static final int out_to_right = 2130968578;
    }

    public static final class attr {
    }

    public static final class dimen {
        public static final int activity_horizontal_margin = 2131034112;
        public static final int activity_vertical_margin = 2131034113;
    }

    public static final class drawable {
        public static final int about = 2130837504;
        public static final int about_app_btn = 2130837505;
        public static final int about_app_btnpress = 2130837506;
        public static final int about_btn = 2130837507;
        public static final int aboutpre = 2130837508;
        public static final int admin1 = 2130837509;
        public static final int admin2 = 2130837510;
        public static final int admin3 = 2130837511;
        public static final int admin4 = 2130837512;
        public static final int admin5 = 2130837513;
        public static final int admin6 = 2130837514;
        public static final int admin7 = 2130837515;
        public static final int admin8 = 2130837516;
        public static final int android_layout_bg = 2130837517;
        public static final int auout_pressed = 2130837518;
        public static final int back = 2130837519;
        public static final int back_pressed = 2130837520;
        public static final int backpre = 2130837521;
        public static final int bargroundline = 2130837522;
        public static final int btn_click_cancellation_commodity_evaluation = 2130837523;
        public static final int btn_click_share_commodity_evaluation = 2130837524;
        public static final int button2_click = 2130837525;
        public static final int button2_gray = 2130837526;
        public static final int button2_normal = 2130837527;
        public static final int button2_normal_gray = 2130837528;
        public static final int button2_red = 2130837529;
        public static final int button_ground = 2130837530;
        public static final int categopre = 2130837531;
        public static final int category = 2130837532;
        public static final int category_pressed = 2130837533;
        public static final int home = 2130837534;
        public static final int home_pressed = 2130837535;
        public static final int homepre = 2130837536;
        public static final int icon = 2130837537;
        public static final int loading = 2130837538;
        public static final int member = 2130837539;
        public static final int member_pressed = 2130837540;
        public static final int memberpre = 2130837541;
        public static final int menu = 2130837542;
        public static final int menu_pressed = 2130837543;
        public static final int menu_refresh = 2130837544;
        public static final int menupre = 2130837545;
        public static final int progressbar_xml = 2130837546;
        public static final int progresstopbar1 = 2130837547;
        public static final int progresstopbar2 = 2130837548;
        public static final int refresh = 2130837549;
        public static final int refresh_pressed = 2130837550;
        public static final int refreshpre = 2130837551;
        public static final int shopping = 2130837552;
        public static final int shopping_pressed = 2130837553;
        public static final int shoppingpre = 2130837554;
        public static final int tit_bg = 2130837555;
        public static final int topbar = 2130837556;
        public static final int topbar1 = 2130837557;
        public static final int topbar2 = 2130837558;
        public static final int topbar_44 = 2130837559;
        public static final int transparent_bg2 = 2130837560;
        public static final int welcome = 2130837561;
    }

    public static final class id {
        public static final int FirstImage = 2131230720;
        public static final int NotifiText = 2131230730;
        public static final int Notifiimage = 2131230729;
        public static final int about_version_txt = 2131230742;
        public static final int btn_album = 2131230737;
        public static final int btn_cancel = 2131230738;
        public static final int btn_photos = 2131230736;
        public static final int business_txt = 2131230743;
        public static final int image_ = 2131230741;
        public static final int imgBtnBack = 2131230724;
        public static final int imgBtn_Right = 2131230727;
        public static final int imgViewLoding = 2131230733;
        public static final int imgbackaboutus = 2131230731;
        public static final int imgbtn_DeleteUrlCache = 2131230745;
        public static final int imgbtn_more_use = 2131230746;
        public static final int imgbtn_share_app = 2131230747;
        public static final int imgbtn_version = 2131230744;
        public static final int lin_bj_microblogging = 2131230735;
        public static final int loadingImageView = 2131230721;
        public static final int pic = 2131230722;
        public static final int progressBar = 2131230728;
        public static final int rel_bg_microblogging = 2131230734;
        public static final int showpage = 2131230748;
        public static final int textView = 2131230740;
        public static final int title = 2131230739;
        public static final int titleLayout = 2131230723;
        public static final int titleTxt = 2131230725;
        public static final int txtWebTitle = 2131230732;
        public static final int viewpage = 2131230749;
        public static final int webView = 2131230726;
        /* added by JADX */

        /* renamed from: btn_photos   reason: not valid java name */
        public static final int f0btn_photos = 2131230736;
    }

    public static final class layout {
        public static final int activity_start = 2130903040;
        public static final int adminor = 2130903041;
        public static final int image_page = 2130903042;
        public static final int left = 2130903043;
        public static final int main = 2130903044;
        public static final int notifi = 2130903045;
        public static final int outbrowser = 2130903046;
        public static final int popwindow_share = 2130903047;
        public static final int seeting = 2130903048;
        public static final int showpage = 2130903049;
        public static final int view_pager = 2130903050;
    }

    public static final class string {
        public static final int about_app = 2131099670;
        public static final int action_settings = 2131099675;
        public static final int app_name = 2131099648;
        public static final int btnAlbum = 2131099656;
        public static final int btnCancel = 2131099657;
        public static final int btnPhoto = 2131099655;
        public static final int cache = 2131099666;
        public static final int cache_over = 2131099668;
        public static final int cache_sure = 2131099667;
        public static final int cancel = 2131099660;
        public static final int confirm = 2131099659;
        public static final int dialogExit = 2131099658;
        public static final int email = 2131099651;
        public static final int exit = 2131099662;
        public static final int have_a_new_message = 2131099653;
        public static final int have_a_new_version = 2131099654;
        public static final int hello_world = 2131099676;
        public static final int internet = 2131099649;
        public static final int internet_wrong = 2131099650;
        public static final int loading = 2131099678;
        public static final int more_use = 2131099665;
        public static final int notification = 2131099661;
        public static final int notitle = 2131099677;
        public static final int reload = 2131099663;
        public static final int share = 2131099652;
        public static final int share_app = 2131099672;
        public static final int title_activity_out_browser = 2131099674;
        public static final int title_activity_start = 2131099679;
        public static final int version = 2131099664;
        public static final int version_ = 2131099671;
        public static final int version_ex = 2131099673;
        public static final int version_newwst = 2131099669;
    }

    public static final class style {
        public static final int AppBaseTheme = 2131165186;
        public static final int AppTheme = 2131165187;
        public static final int AppThemeFirst = 2131165188;
        public static final int CustomDialog = 2131165184;
        public static final int CustomProgressDialog = 2131165185;
        public static final int DialogStyle = 2131165189;
        public static final int MyTheme_CustomBackground = 2131165190;
    }
}
