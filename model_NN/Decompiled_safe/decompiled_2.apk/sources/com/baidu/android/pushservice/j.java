package com.baidu.android.pushservice;

import com.baidu.android.common.logging.Log;
import com.baidu.android.pushservice.message.b;
import java.io.IOException;
import java.io.OutputStream;

class j extends Thread {
    final /* synthetic */ e a;

    j(e eVar, OutputStream outputStream) {
        this.a = eVar;
        setName("PushService-PushConnection-SendThread");
    }

    public void run() {
        b bVar;
        while (!this.a.g) {
            synchronized (this.a.l) {
                if (this.a.l.size() == 0) {
                    try {
                        this.a.l.wait();
                    } catch (InterruptedException e) {
                        Log.e("PushConnection", "SendThread wait exception: " + e);
                    }
                }
                bVar = this.a.l.size() > 0 ? (b) this.a.l.removeFirst() : null;
            }
            if (!this.a.g) {
                if (!(bVar == null || bVar.c == null)) {
                    try {
                        if (b.a()) {
                            Log.d("PushConnection", "SendThread send msg :" + bVar.toString());
                        }
                        if (bVar.d) {
                            this.a.a.removeCallbacks(this.a.t);
                            this.a.a.postDelayed(this.a.t, 20000);
                        }
                        this.a.k.write(bVar.c);
                        this.a.k.flush();
                    } catch (IOException e2) {
                        Log.e("PushConnection", "SendThread exception: " + e2);
                        this.a.f();
                    }
                }
            } else {
                return;
            }
        }
    }
}
