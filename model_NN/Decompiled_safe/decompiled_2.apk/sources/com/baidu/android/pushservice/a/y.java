package com.baidu.android.pushservice.a;

import android.content.Context;
import com.baidu.android.common.logging.Log;
import com.baidu.android.pushservice.PushConstants;
import com.baidu.android.pushservice.b;
import java.util.Iterator;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

public class y extends d {
    public y(k kVar, Context context) {
        super(kVar, context);
    }

    /* access modifiers changed from: protected */
    public void a(List list) {
        b.a(list);
        list.add(new BasicNameValuePair(PushConstants.EXTRA_METHOD, "unbindapp"));
        list.add(new BasicNameValuePair("appid", this.b.f));
        if (b.a()) {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                Log.d("UnbindApp", "UNBINDAPP param -- " + ((NameValuePair) it.next()).toString());
            }
        }
    }
}
