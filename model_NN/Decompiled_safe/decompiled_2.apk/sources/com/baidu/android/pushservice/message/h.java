package com.baidu.android.pushservice.message;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.baidu.android.common.logging.Log;
import com.baidu.android.common.net.ProxyHttpClient;
import com.baidu.android.pushservice.PushConstants;
import com.baidu.android.pushservice.a.b;
import com.baidu.android.pushservice.util.e;
import com.baidu.android.pushservice.util.j;
import com.baidu.android.pushservice.x;
import java.io.IOException;
import java.util.ArrayList;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;

class h implements Runnable {
    final /* synthetic */ Context a;
    final /* synthetic */ String b;
    final /* synthetic */ String c;
    final /* synthetic */ String d;
    final /* synthetic */ String e;
    final /* synthetic */ String f;
    final /* synthetic */ int g;
    final /* synthetic */ PublicMsg h;

    h(PublicMsg publicMsg, Context context, String str, String str2, String str3, String str4, String str5, int i) {
        this.h = publicMsg;
        this.a = context;
        this.b = str;
        this.c = str2;
        this.d = str3;
        this.e = str4;
        this.f = str5;
        this.g = i;
    }

    public void run() {
        ProxyHttpClient proxyHttpClient = new ProxyHttpClient(this.a);
        try {
            HttpPost httpPost = new HttpPost(x.g + this.b);
            httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded");
            ArrayList<NameValuePair> arrayList = new ArrayList<>();
            b.a(arrayList);
            arrayList.add(new BasicNameValuePair(PushConstants.EXTRA_METHOD, "feedback"));
            arrayList.add(new BasicNameValuePair("channel_token", this.c));
            arrayList.add(new BasicNameValuePair("data", this.d));
            if (com.baidu.android.pushservice.b.a()) {
                for (NameValuePair obj : arrayList) {
                    Log.d("PublicMsg", "feedback param -- " + obj.toString());
                }
            }
            httpPost.setEntity(new UrlEncodedFormEntity(arrayList, "UTF-8"));
            HttpResponse execute = proxyHttpClient.execute(httpPost);
            if (execute.getStatusLine().getStatusCode() != 200) {
                if (com.baidu.android.pushservice.b.a()) {
                    Log.e("PublicMsg", "networkRegister request failed  " + execute.getStatusLine());
                }
                SQLiteDatabase a2 = e.a(this.a);
                if (a2 != null) {
                    j jVar = new j();
                    jVar.c = this.e;
                    jVar.b = this.f;
                    jVar.d = this.g;
                    e.a(a2, jVar);
                }
            } else if (com.baidu.android.pushservice.b.a()) {
                Log.i("PublicMsg", "<<< Private Notification send result return OK!");
            }
        } catch (IOException e2) {
            if (com.baidu.android.pushservice.b.a()) {
                Log.e("PublicMsg", e2.getMessage());
                Log.e("PublicMsg", "io exception do something ? ");
            }
            SQLiteDatabase a3 = e.a(this.a);
            if (a3 != null) {
                j jVar2 = new j();
                jVar2.c = this.e;
                jVar2.b = this.f;
                jVar2.d = this.g;
                e.a(a3, jVar2);
            }
        } catch (Exception e3) {
            if (com.baidu.android.pushservice.b.a()) {
                Log.e("PublicMsg", e3.getMessage());
            }
            SQLiteDatabase a4 = e.a(this.a);
            if (a4 != null) {
                j jVar3 = new j();
                jVar3.c = this.e;
                jVar3.b = this.f;
                jVar3.d = this.g;
                e.a(a4, jVar3);
            }
        } finally {
            proxyHttpClient.close();
        }
    }
}
