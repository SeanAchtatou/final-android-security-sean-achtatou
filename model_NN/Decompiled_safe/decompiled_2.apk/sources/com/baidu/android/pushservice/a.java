package com.baidu.android.pushservice;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.text.TextUtils;
import com.baidu.android.common.logging.Log;
import com.baidu.android.common.security.AESUtil;
import com.baidu.android.common.security.Base64;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class a {
    private static volatile a c;
    public ArrayList a = new ArrayList();
    public ArrayList b = new ArrayList();
    private Context d;

    private a(Context context) {
        this.d = context.getApplicationContext();
        c(this.d);
        d(this.d);
        SharedPreferences sharedPreferences = this.d.getSharedPreferences(this.d.getPackageName() + ".push_sync", 1);
        String string = sharedPreferences.getString("r", "");
        if (!TextUtils.isEmpty(string)) {
            try {
                String str = new String(AESUtil.decrypt("2011121211143000", "1234567890123456", Base64.decode(string.getBytes())));
                if (b.a()) {
                    Log.i("ClientManager", "ClientManager init strApps : " + str);
                }
                ArrayList c2 = c(str);
                if (c2 != null) {
                    Iterator it = c2.iterator();
                    while (it.hasNext()) {
                        this.a.add((d) it.next());
                    }
                }
            } catch (Exception e) {
                Log.e("ClientManager", e);
            }
        } else if (b.a()) {
            Log.i("ClientManager", "ClientManager init strApps empty.");
        }
        String string2 = sharedPreferences.getString("r_v2", "");
        if (!TextUtils.isEmpty(string2)) {
            try {
                String str2 = new String(AESUtil.decrypt("2011121211143000", "1234567890123456", Base64.decode(string2.getBytes())));
                if (b.a()) {
                    Log.i("ClientManager", "ClientManager init strAppsV2 : " + str2);
                }
                ArrayList c3 = c(str2);
                if (c3 != null) {
                    Iterator it2 = c3.iterator();
                    while (it2.hasNext()) {
                        this.b.add((d) it2.next());
                    }
                }
            } catch (Exception e2) {
                Log.e("ClientManager", e2);
            }
        } else if (b.a()) {
            Log.i("ClientManager", "ClientManager init strAppsV2 empty.");
        }
    }

    public static synchronized a a(Context context) {
        a aVar;
        synchronized (a.class) {
            if (c == null) {
                c = new a(context);
            }
            aVar = c;
        }
        return aVar;
    }

    static String a(List list) {
        if (list == null || list.size() == 0) {
            return "";
        }
        StringBuffer stringBuffer = new StringBuffer();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= list.size()) {
                return stringBuffer.toString();
            }
            d dVar = (d) list.get(i2);
            stringBuffer.append(dVar.a);
            stringBuffer.append(",");
            stringBuffer.append(dVar.b);
            stringBuffer.append(",");
            stringBuffer.append(dVar.c);
            if (i2 != list.size() - 1) {
                stringBuffer.append(";");
            }
            i = i2 + 1;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x00a4 A[SYNTHETIC, Splitter:B:25:0x00a4] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x010c A[Catch:{ Exception -> 0x00f5 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized void b(android.content.Context r7) {
        /*
            java.lang.Class<com.baidu.android.pushservice.a> r3 = com.baidu.android.pushservice.a.class
            monitor-enter(r3)
            com.baidu.android.pushservice.a r0 = com.baidu.android.pushservice.a.c     // Catch:{ all -> 0x011a }
            if (r0 == 0) goto L_0x00fb
            com.baidu.android.pushservice.a r0 = com.baidu.android.pushservice.a.c     // Catch:{ all -> 0x011a }
            java.util.ArrayList r0 = r0.a     // Catch:{ all -> 0x011a }
            r0.clear()     // Catch:{ all -> 0x011a }
            com.baidu.android.pushservice.a r0 = com.baidu.android.pushservice.a.c     // Catch:{ all -> 0x011a }
            java.util.ArrayList r0 = r0.b     // Catch:{ all -> 0x011a }
            r0.clear()     // Catch:{ all -> 0x011a }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x011a }
            r0.<init>()     // Catch:{ all -> 0x011a }
            java.lang.String r1 = r7.getPackageName()     // Catch:{ all -> 0x011a }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x011a }
            java.lang.String r1 = ".push_sync"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x011a }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x011a }
            r1 = 1
            android.content.SharedPreferences r4 = r7.getSharedPreferences(r0, r1)     // Catch:{ all -> 0x011a }
            java.lang.String r0 = "r"
            java.lang.String r1 = ""
            java.lang.String r2 = r4.getString(r0, r1)     // Catch:{ all -> 0x011a }
            boolean r0 = android.text.TextUtils.isEmpty(r2)     // Catch:{ all -> 0x011a }
            if (r0 != 0) goto L_0x00fd
            byte[] r0 = r2.getBytes()     // Catch:{ Exception -> 0x011d }
            byte[] r0 = com.baidu.android.common.security.Base64.decode(r0)     // Catch:{ Exception -> 0x011d }
            java.lang.String r1 = new java.lang.String     // Catch:{ Exception -> 0x011d }
            java.lang.String r5 = "2011121211143000"
            java.lang.String r6 = "1234567890123456"
            byte[] r0 = com.baidu.android.common.security.AESUtil.decrypt(r5, r6, r0)     // Catch:{ Exception -> 0x011d }
            r1.<init>(r0)     // Catch:{ Exception -> 0x011d }
            boolean r0 = com.baidu.android.pushservice.b.a()     // Catch:{ Exception -> 0x0090 }
            if (r0 == 0) goto L_0x0072
            java.lang.String r0 = "ClientManager"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0090 }
            r2.<init>()     // Catch:{ Exception -> 0x0090 }
            java.lang.String r5 = "ClientManager init strApps : "
            java.lang.StringBuilder r2 = r2.append(r5)     // Catch:{ Exception -> 0x0090 }
            java.lang.StringBuilder r2 = r2.append(r1)     // Catch:{ Exception -> 0x0090 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0090 }
            com.baidu.android.common.logging.Log.i(r0, r2)     // Catch:{ Exception -> 0x0090 }
        L_0x0072:
            java.util.ArrayList r0 = c(r1)     // Catch:{ Exception -> 0x0090 }
            if (r0 == 0) goto L_0x0096
            java.util.Iterator r2 = r0.iterator()     // Catch:{ Exception -> 0x0090 }
        L_0x007c:
            boolean r0 = r2.hasNext()     // Catch:{ Exception -> 0x0090 }
            if (r0 == 0) goto L_0x0096
            java.lang.Object r0 = r2.next()     // Catch:{ Exception -> 0x0090 }
            com.baidu.android.pushservice.d r0 = (com.baidu.android.pushservice.d) r0     // Catch:{ Exception -> 0x0090 }
            com.baidu.android.pushservice.a r5 = com.baidu.android.pushservice.a.c     // Catch:{ Exception -> 0x0090 }
            java.util.ArrayList r5 = r5.a     // Catch:{ Exception -> 0x0090 }
            r5.add(r0)     // Catch:{ Exception -> 0x0090 }
            goto L_0x007c
        L_0x0090:
            r0 = move-exception
        L_0x0091:
            java.lang.String r2 = "ClientManager"
            com.baidu.android.common.logging.Log.e(r2, r0)     // Catch:{ all -> 0x011a }
        L_0x0096:
            java.lang.String r0 = "r_v2"
            java.lang.String r2 = ""
            java.lang.String r0 = r4.getString(r0, r2)     // Catch:{ all -> 0x011a }
            boolean r2 = android.text.TextUtils.isEmpty(r0)     // Catch:{ all -> 0x011a }
            if (r2 != 0) goto L_0x010c
            byte[] r0 = r0.getBytes()     // Catch:{ Exception -> 0x00f5 }
            byte[] r0 = com.baidu.android.common.security.Base64.decode(r0)     // Catch:{ Exception -> 0x00f5 }
            java.lang.String r2 = new java.lang.String     // Catch:{ Exception -> 0x00f5 }
            java.lang.String r4 = "2011121211143000"
            java.lang.String r5 = "1234567890123456"
            byte[] r0 = com.baidu.android.common.security.AESUtil.decrypt(r4, r5, r0)     // Catch:{ Exception -> 0x00f5 }
            r2.<init>(r0)     // Catch:{ Exception -> 0x00f5 }
            boolean r0 = com.baidu.android.pushservice.b.a()     // Catch:{ Exception -> 0x00f5 }
            if (r0 == 0) goto L_0x00d7
            java.lang.String r0 = "ClientManager"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00f5 }
            r4.<init>()     // Catch:{ Exception -> 0x00f5 }
            java.lang.String r5 = "ClientManager init strApps : "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x00f5 }
            java.lang.StringBuilder r1 = r4.append(r1)     // Catch:{ Exception -> 0x00f5 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x00f5 }
            com.baidu.android.common.logging.Log.i(r0, r1)     // Catch:{ Exception -> 0x00f5 }
        L_0x00d7:
            java.util.ArrayList r0 = c(r2)     // Catch:{ Exception -> 0x00f5 }
            if (r0 == 0) goto L_0x00fb
            java.util.Iterator r1 = r0.iterator()     // Catch:{ Exception -> 0x00f5 }
        L_0x00e1:
            boolean r0 = r1.hasNext()     // Catch:{ Exception -> 0x00f5 }
            if (r0 == 0) goto L_0x00fb
            java.lang.Object r0 = r1.next()     // Catch:{ Exception -> 0x00f5 }
            com.baidu.android.pushservice.d r0 = (com.baidu.android.pushservice.d) r0     // Catch:{ Exception -> 0x00f5 }
            com.baidu.android.pushservice.a r2 = com.baidu.android.pushservice.a.c     // Catch:{ Exception -> 0x00f5 }
            java.util.ArrayList r2 = r2.b     // Catch:{ Exception -> 0x00f5 }
            r2.add(r0)     // Catch:{ Exception -> 0x00f5 }
            goto L_0x00e1
        L_0x00f5:
            r0 = move-exception
            java.lang.String r1 = "ClientManager"
            com.baidu.android.common.logging.Log.e(r1, r0)     // Catch:{ all -> 0x011a }
        L_0x00fb:
            monitor-exit(r3)
            return
        L_0x00fd:
            boolean r0 = com.baidu.android.pushservice.b.a()     // Catch:{ all -> 0x011a }
            if (r0 == 0) goto L_0x010a
            java.lang.String r0 = "ClientManager"
            java.lang.String r1 = "ClientManager init strApps empty."
            com.baidu.android.common.logging.Log.i(r0, r1)     // Catch:{ all -> 0x011a }
        L_0x010a:
            r1 = r2
            goto L_0x0096
        L_0x010c:
            boolean r0 = com.baidu.android.pushservice.b.a()     // Catch:{ all -> 0x011a }
            if (r0 == 0) goto L_0x00fb
            java.lang.String r0 = "ClientManager"
            java.lang.String r1 = "ClientManager init strAppsV2 empty."
            com.baidu.android.common.logging.Log.i(r0, r1)     // Catch:{ all -> 0x011a }
            goto L_0x00fb
        L_0x011a:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        L_0x011d:
            r0 = move-exception
            r1 = r2
            goto L_0x0091
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.android.pushservice.a.b(android.content.Context):void");
    }

    private static String c(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(context.getPackageName() + ".push_sync", 1);
        if (!"nodata".equals(sharedPreferences.getString("r", "nodata"))) {
            return null;
        }
        for (ResolveInfo resolveInfo : context.getPackageManager().queryBroadcastReceivers(new Intent("com.baidu.android.pushservice.action.BIND_SYNC"), 0)) {
            d dVar = new d();
            dVar.a = resolveInfo.activityInfo.packageName;
            try {
                String string = context.createPackageContext(dVar.a, 2).getSharedPreferences(dVar.a + ".push_sync", 1).getString("r", "nodata");
                if (!"nodata".equals(string)) {
                    SharedPreferences.Editor edit = sharedPreferences.edit();
                    edit.putString("r", string);
                    edit.commit();
                    return dVar.a;
                }
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    static ArrayList c(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (String trim : str.trim().split(";")) {
            String[] split = trim.trim().split(",");
            if (split.length >= 3) {
                d dVar = new d();
                dVar.a = split[0].trim();
                dVar.b = split[1].trim();
                dVar.c = split[2].trim();
                arrayList.add(dVar);
            }
        }
        return arrayList;
    }

    private static String d(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(context.getPackageName() + ".push_sync", 1);
        if (!"nodata".equals(sharedPreferences.getString("r_v2", "nodata"))) {
            return null;
        }
        for (ResolveInfo resolveInfo : context.getPackageManager().queryBroadcastReceivers(new Intent("com.baidu.android.pushservice.action.BIND_SYNC"), 0)) {
            d dVar = new d();
            dVar.a = resolveInfo.activityInfo.packageName;
            try {
                String string = context.createPackageContext(dVar.a, 2).getSharedPreferences(dVar.a + ".push_sync", 1).getString("r_v2", "nodata");
                if (!"nodata".equals(string)) {
                    SharedPreferences.Editor edit = sharedPreferences.edit();
                    edit.putString("r_v2", string);
                    edit.commit();
                    return dVar.a;
                }
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public d a(String str) {
        Iterator it = this.b.iterator();
        while (it.hasNext()) {
            d dVar = (d) it.next();
            if (dVar.a.equals(str)) {
                return dVar;
            }
        }
        Iterator it2 = this.a.iterator();
        while (it2.hasNext()) {
            d dVar2 = (d) it2.next();
            if (dVar2.a.equals(str)) {
                return dVar2;
            }
        }
        return null;
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String a(com.baidu.android.pushservice.d r9, boolean r10) {
        /*
            r8 = this;
            r1 = 1
            boolean r0 = com.baidu.android.pushservice.b.a()
            if (r0 == 0) goto L_0x0029
            java.lang.String r0 = "ClientManager"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "sync addOrRemove:"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r10)
            java.lang.String r3 = ", "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r9)
            java.lang.String r2 = r2.toString()
            com.baidu.android.common.logging.Log.d(r0, r2)
        L_0x0029:
            java.util.ArrayList r3 = r8.a
            monitor-enter(r3)
            r2 = 0
            java.lang.String r0 = "ClientManager"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0131 }
            r4.<init>()     // Catch:{ all -> 0x0131 }
            java.lang.String r5 = "client.packageName="
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0131 }
            java.lang.String r5 = r9.a     // Catch:{ all -> 0x0131 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0131 }
            java.lang.String r5 = " client.appId="
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0131 }
            java.lang.String r5 = r9.b     // Catch:{ all -> 0x0131 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0131 }
            java.lang.String r5 = " client.userId="
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0131 }
            java.lang.String r5 = r9.c     // Catch:{ all -> 0x0131 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0131 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x0131 }
            com.baidu.android.common.logging.Log.d(r0, r4)     // Catch:{ all -> 0x0131 }
            java.util.ArrayList r0 = r8.a     // Catch:{ all -> 0x0131 }
            java.util.Iterator r4 = r0.iterator()     // Catch:{ all -> 0x0131 }
        L_0x0065:
            boolean r0 = r4.hasNext()     // Catch:{ all -> 0x0131 }
            if (r0 == 0) goto L_0x0134
            java.lang.Object r0 = r4.next()     // Catch:{ all -> 0x0131 }
            com.baidu.android.pushservice.d r0 = (com.baidu.android.pushservice.d) r0     // Catch:{ all -> 0x0131 }
            java.lang.String r5 = "ClientManager"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x0131 }
            r6.<init>()     // Catch:{ all -> 0x0131 }
            java.lang.String r7 = "c.packageName="
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ all -> 0x0131 }
            java.lang.String r7 = r0.a     // Catch:{ all -> 0x0131 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ all -> 0x0131 }
            java.lang.String r7 = " c.appId="
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ all -> 0x0131 }
            java.lang.String r7 = r0.b     // Catch:{ all -> 0x0131 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ all -> 0x0131 }
            java.lang.String r7 = " c.userId="
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ all -> 0x0131 }
            java.lang.String r7 = r0.c     // Catch:{ all -> 0x0131 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ all -> 0x0131 }
            java.lang.String r6 = r6.toString()     // Catch:{ all -> 0x0131 }
            com.baidu.android.common.logging.Log.d(r5, r6)     // Catch:{ all -> 0x0131 }
            java.lang.String r5 = r0.a     // Catch:{ all -> 0x0131 }
            java.lang.String r6 = r9.a     // Catch:{ all -> 0x0131 }
            boolean r5 = r5.equals(r6)     // Catch:{ all -> 0x0131 }
            if (r5 == 0) goto L_0x0065
            java.util.ArrayList r2 = r8.a     // Catch:{ all -> 0x0131 }
            r2.remove(r0)     // Catch:{ all -> 0x0131 }
            if (r10 == 0) goto L_0x00b9
            java.util.ArrayList r0 = r8.a     // Catch:{ all -> 0x0131 }
            r0.add(r9)     // Catch:{ all -> 0x0131 }
        L_0x00b9:
            r0 = r1
        L_0x00ba:
            if (r0 != 0) goto L_0x00c3
            if (r10 == 0) goto L_0x00c3
            java.util.ArrayList r0 = r8.a     // Catch:{ all -> 0x0131 }
            r0.add(r9)     // Catch:{ all -> 0x0131 }
        L_0x00c3:
            android.content.Context r0 = r8.d     // Catch:{ all -> 0x0131 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0131 }
            r1.<init>()     // Catch:{ all -> 0x0131 }
            android.content.Context r2 = r8.d     // Catch:{ all -> 0x0131 }
            java.lang.String r2 = r2.getPackageName()     // Catch:{ all -> 0x0131 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0131 }
            java.lang.String r2 = ".push_sync"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0131 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0131 }
            r2 = 1
            android.content.SharedPreferences r1 = r0.getSharedPreferences(r1, r2)     // Catch:{ all -> 0x0131 }
            java.util.ArrayList r0 = r8.a     // Catch:{ all -> 0x0131 }
            java.lang.String r0 = a(r0)     // Catch:{ all -> 0x0131 }
            boolean r2 = com.baidu.android.pushservice.b.a()     // Catch:{ all -> 0x0131 }
            if (r2 == 0) goto L_0x0107
            java.lang.String r2 = "ClientManager"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0131 }
            r4.<init>()     // Catch:{ all -> 0x0131 }
            java.lang.String r5 = "sync  strApps: "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0131 }
            java.lang.StringBuilder r4 = r4.append(r0)     // Catch:{ all -> 0x0131 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x0131 }
            com.baidu.android.common.logging.Log.i(r2, r4)     // Catch:{ all -> 0x0131 }
        L_0x0107:
            java.lang.String r2 = "2011121211143000"
            java.lang.String r4 = "1234567890123456"
            byte[] r0 = r0.getBytes()     // Catch:{ Exception -> 0x0127 }
            byte[] r0 = com.baidu.android.common.security.AESUtil.encrypt(r2, r4, r0)     // Catch:{ Exception -> 0x0127 }
            java.lang.String r2 = "utf-8"
            java.lang.String r0 = com.baidu.android.common.security.Base64.encode(r0, r2)     // Catch:{ Exception -> 0x0127 }
            android.content.SharedPreferences$Editor r1 = r1.edit()     // Catch:{ Exception -> 0x0127 }
            java.lang.String r2 = "r"
            r1.putString(r2, r0)     // Catch:{ Exception -> 0x0127 }
            r1.commit()     // Catch:{ Exception -> 0x0127 }
            monitor-exit(r3)     // Catch:{ all -> 0x0131 }
        L_0x0126:
            return r0
        L_0x0127:
            r0 = move-exception
            java.lang.String r1 = "ClientManager"
            com.baidu.android.common.logging.Log.e(r1, r0)     // Catch:{ all -> 0x0131 }
            monitor-exit(r3)     // Catch:{ all -> 0x0131 }
            java.lang.String r0 = ""
            goto L_0x0126
        L_0x0131:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0131 }
            throw r0
        L_0x0134:
            r0 = r2
            goto L_0x00ba
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.android.pushservice.a.a(com.baidu.android.pushservice.d, boolean):java.lang.String");
    }

    public d b(String str) {
        Iterator it = this.b.iterator();
        while (it.hasNext()) {
            d dVar = (d) it.next();
            if (dVar.b.equals(str)) {
                return dVar;
            }
        }
        Iterator it2 = this.a.iterator();
        while (it2.hasNext()) {
            d dVar2 = (d) it2.next();
            if (dVar2.b.equals(str)) {
                return dVar2;
            }
        }
        return null;
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String b(com.baidu.android.pushservice.d r9, boolean r10) {
        /*
            r8 = this;
            r1 = 1
            boolean r0 = com.baidu.android.pushservice.b.a()
            if (r0 == 0) goto L_0x0029
            java.lang.String r0 = "ClientManager"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "syncV2 addOrRemove:"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r10)
            java.lang.String r3 = ", "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r9)
            java.lang.String r2 = r2.toString()
            com.baidu.android.common.logging.Log.d(r0, r2)
        L_0x0029:
            java.util.ArrayList r3 = r8.b
            monitor-enter(r3)
            r2 = 0
            java.lang.String r0 = "ClientManager"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0131 }
            r4.<init>()     // Catch:{ all -> 0x0131 }
            java.lang.String r5 = "client.packageName="
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0131 }
            java.lang.String r5 = r9.a     // Catch:{ all -> 0x0131 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0131 }
            java.lang.String r5 = " client.appId="
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0131 }
            java.lang.String r5 = r9.b     // Catch:{ all -> 0x0131 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0131 }
            java.lang.String r5 = " client.userId="
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0131 }
            java.lang.String r5 = r9.c     // Catch:{ all -> 0x0131 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0131 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x0131 }
            com.baidu.android.common.logging.Log.d(r0, r4)     // Catch:{ all -> 0x0131 }
            java.util.ArrayList r0 = r8.b     // Catch:{ all -> 0x0131 }
            java.util.Iterator r4 = r0.iterator()     // Catch:{ all -> 0x0131 }
        L_0x0065:
            boolean r0 = r4.hasNext()     // Catch:{ all -> 0x0131 }
            if (r0 == 0) goto L_0x0134
            java.lang.Object r0 = r4.next()     // Catch:{ all -> 0x0131 }
            com.baidu.android.pushservice.d r0 = (com.baidu.android.pushservice.d) r0     // Catch:{ all -> 0x0131 }
            java.lang.String r5 = "ClientManager"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x0131 }
            r6.<init>()     // Catch:{ all -> 0x0131 }
            java.lang.String r7 = "c.packageName="
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ all -> 0x0131 }
            java.lang.String r7 = r0.a     // Catch:{ all -> 0x0131 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ all -> 0x0131 }
            java.lang.String r7 = " c.appId="
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ all -> 0x0131 }
            java.lang.String r7 = r0.b     // Catch:{ all -> 0x0131 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ all -> 0x0131 }
            java.lang.String r7 = " c.userId="
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ all -> 0x0131 }
            java.lang.String r7 = r0.c     // Catch:{ all -> 0x0131 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ all -> 0x0131 }
            java.lang.String r6 = r6.toString()     // Catch:{ all -> 0x0131 }
            com.baidu.android.common.logging.Log.d(r5, r6)     // Catch:{ all -> 0x0131 }
            java.lang.String r5 = r0.a     // Catch:{ all -> 0x0131 }
            java.lang.String r6 = r9.a     // Catch:{ all -> 0x0131 }
            boolean r5 = r5.equals(r6)     // Catch:{ all -> 0x0131 }
            if (r5 == 0) goto L_0x0065
            java.util.ArrayList r2 = r8.b     // Catch:{ all -> 0x0131 }
            r2.remove(r0)     // Catch:{ all -> 0x0131 }
            if (r10 == 0) goto L_0x00b9
            java.util.ArrayList r0 = r8.b     // Catch:{ all -> 0x0131 }
            r0.add(r9)     // Catch:{ all -> 0x0131 }
        L_0x00b9:
            r0 = r1
        L_0x00ba:
            if (r0 != 0) goto L_0x00c3
            if (r10 == 0) goto L_0x00c3
            java.util.ArrayList r0 = r8.b     // Catch:{ all -> 0x0131 }
            r0.add(r9)     // Catch:{ all -> 0x0131 }
        L_0x00c3:
            android.content.Context r0 = r8.d     // Catch:{ all -> 0x0131 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0131 }
            r1.<init>()     // Catch:{ all -> 0x0131 }
            android.content.Context r2 = r8.d     // Catch:{ all -> 0x0131 }
            java.lang.String r2 = r2.getPackageName()     // Catch:{ all -> 0x0131 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0131 }
            java.lang.String r2 = ".push_sync"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0131 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0131 }
            r2 = 1
            android.content.SharedPreferences r1 = r0.getSharedPreferences(r1, r2)     // Catch:{ all -> 0x0131 }
            java.util.ArrayList r0 = r8.b     // Catch:{ all -> 0x0131 }
            java.lang.String r0 = a(r0)     // Catch:{ all -> 0x0131 }
            boolean r2 = com.baidu.android.pushservice.b.a()     // Catch:{ all -> 0x0131 }
            if (r2 == 0) goto L_0x0107
            java.lang.String r2 = "ClientManager"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0131 }
            r4.<init>()     // Catch:{ all -> 0x0131 }
            java.lang.String r5 = "syncV2  strApps: "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0131 }
            java.lang.StringBuilder r4 = r4.append(r0)     // Catch:{ all -> 0x0131 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x0131 }
            com.baidu.android.common.logging.Log.i(r2, r4)     // Catch:{ all -> 0x0131 }
        L_0x0107:
            java.lang.String r2 = "2011121211143000"
            java.lang.String r4 = "1234567890123456"
            byte[] r0 = r0.getBytes()     // Catch:{ Exception -> 0x0127 }
            byte[] r0 = com.baidu.android.common.security.AESUtil.encrypt(r2, r4, r0)     // Catch:{ Exception -> 0x0127 }
            java.lang.String r2 = "utf-8"
            java.lang.String r0 = com.baidu.android.common.security.Base64.encode(r0, r2)     // Catch:{ Exception -> 0x0127 }
            android.content.SharedPreferences$Editor r1 = r1.edit()     // Catch:{ Exception -> 0x0127 }
            java.lang.String r2 = "r_v2"
            r1.putString(r2, r0)     // Catch:{ Exception -> 0x0127 }
            r1.commit()     // Catch:{ Exception -> 0x0127 }
            monitor-exit(r3)     // Catch:{ all -> 0x0131 }
        L_0x0126:
            return r0
        L_0x0127:
            r0 = move-exception
            java.lang.String r1 = "ClientManager"
            com.baidu.android.common.logging.Log.e(r1, r0)     // Catch:{ all -> 0x0131 }
            monitor-exit(r3)     // Catch:{ all -> 0x0131 }
            java.lang.String r0 = ""
            goto L_0x0126
        L_0x0131:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0131 }
            throw r0
        L_0x0134:
            r0 = r2
            goto L_0x00ba
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.android.pushservice.a.b(com.baidu.android.pushservice.d, boolean):java.lang.String");
    }
}
