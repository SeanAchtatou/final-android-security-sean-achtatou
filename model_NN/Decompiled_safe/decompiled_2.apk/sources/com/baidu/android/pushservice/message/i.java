package com.baidu.android.pushservice.message;

import android.os.Parcel;
import android.os.Parcelable;

final class i implements Parcelable.Creator {
    i() {
    }

    /* renamed from: a */
    public PublicMsg createFromParcel(Parcel parcel) {
        return new PublicMsg(parcel);
    }

    /* renamed from: a */
    public PublicMsg[] newArray(int i) {
        return new PublicMsg[i];
    }
}
