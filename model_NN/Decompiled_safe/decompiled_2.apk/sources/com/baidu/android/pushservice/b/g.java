package com.baidu.android.pushservice.b;

import android.content.Context;
import android.util.Log;
import com.baidu.android.common.net.ProxyHttpClient;
import com.baidu.android.pushservice.PushConstants;
import com.baidu.android.pushservice.a.b;
import com.baidu.android.pushservice.util.e;
import com.baidu.android.pushservice.x;
import java.io.IOException;
import java.util.ArrayList;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;

class g implements Runnable {
    final /* synthetic */ Context a;
    final /* synthetic */ String b;
    final /* synthetic */ String c;
    final /* synthetic */ String d;
    final /* synthetic */ f e;

    g(f fVar, Context context, String str, String str2, String str3) {
        this.e = fVar;
        this.a = context;
        this.b = str;
        this.c = str2;
        this.d = str3;
    }

    public void run() {
        ProxyHttpClient proxyHttpClient = new ProxyHttpClient(this.a);
        try {
            HttpPost httpPost = new HttpPost(x.g + this.b);
            httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded");
            ArrayList<NameValuePair> arrayList = new ArrayList<>();
            b.a(arrayList);
            arrayList.add(new BasicNameValuePair(PushConstants.EXTRA_METHOD, "feedback"));
            arrayList.add(new BasicNameValuePair("channel_token", this.c));
            arrayList.add(new BasicNameValuePair("data", this.d));
            if (com.baidu.android.pushservice.b.a(this.e.a)) {
                for (NameValuePair obj : arrayList) {
                    Log.d("StatisticsInfoManager", "feedback param -- " + obj.toString());
                }
            }
            httpPost.setEntity(new UrlEncodedFormEntity(arrayList, "UTF-8"));
            HttpResponse execute = proxyHttpClient.execute(httpPost);
            if (execute.getStatusLine().getStatusCode() == 200) {
                if (com.baidu.android.pushservice.b.a(this.e.a)) {
                    Log.i("StatisticsInfoManager", "<<< Msg result send result return OK!");
                }
                e.d(e.a(this.a));
            } else if (com.baidu.android.pushservice.b.a(this.e.a)) {
                Log.e("StatisticsInfoManager", "networkRegister request failed  " + execute.getStatusLine());
            }
        } catch (IOException e2) {
            if (com.baidu.android.pushservice.b.a(this.e.a)) {
                Log.e("StatisticsInfoManager", "" + e2.getMessage());
                Log.e("StatisticsInfoManager", "io exception do something ? ");
            }
        } catch (Exception e3) {
            if (com.baidu.android.pushservice.b.a(this.e.a)) {
                Log.e("StatisticsInfoManager", "" + e3.getMessage());
            }
        } finally {
            proxyHttpClient.close();
        }
    }
}
