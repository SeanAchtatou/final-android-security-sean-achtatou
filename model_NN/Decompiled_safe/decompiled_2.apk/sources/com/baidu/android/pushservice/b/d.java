package com.baidu.android.pushservice.b;

import android.content.Context;
import android.text.TextUtils;
import com.baidu.android.common.logging.Log;
import com.baidu.android.common.net.ConnectManager;
import com.baidu.android.common.net.ProxyHttpClient;
import com.baidu.android.pushservice.PushConstants;
import com.baidu.android.pushservice.a.b;
import com.baidu.android.pushservice.x;
import com.baidu.android.pushservice.z;
import java.util.ArrayList;
import org.apache.http.HttpResponse;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

public abstract class d {
    protected Context a;
    protected String b;
    /* access modifiers changed from: private */
    public boolean c = false;

    public d(Context context) {
        this.a = context.getApplicationContext();
        this.b = x.g;
    }

    /* access modifiers changed from: private */
    public void f() {
        if (!TextUtils.isEmpty(this.b)) {
            try {
                String b2 = b();
                while (!TextUtils.isEmpty(b2)) {
                    ProxyHttpClient proxyHttpClient = new ProxyHttpClient(this.a);
                    HttpPost httpPost = new HttpPost(this.b + z.a().b());
                    httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded");
                    ArrayList arrayList = new ArrayList();
                    b.a(arrayList);
                    arrayList.add(new BasicNameValuePair(PushConstants.EXTRA_METHOD, "appusestat"));
                    arrayList.add(new BasicNameValuePair("channel_token", z.a().c()));
                    if (com.baidu.android.pushservice.b.a(this.a)) {
                        Log.d("Statistics-BaseSender", "Sending statistics data: " + b2);
                    }
                    arrayList.add(new BasicNameValuePair("data", b2));
                    httpPost.setEntity(new UrlEncodedFormEntity(arrayList, "UTF-8"));
                    HttpResponse execute = proxyHttpClient.execute(httpPost);
                    if (execute.getStatusLine().getStatusCode() == 200) {
                        if (com.baidu.android.pushservice.b.a(this.a)) {
                            Log.d("Statistics-BaseSender", "Send statistics data OK, continue!");
                        }
                        c();
                        proxyHttpClient.close();
                        b2 = b();
                    } else {
                        if (com.baidu.android.pushservice.b.a(this.a)) {
                            Log.w("Statistics-BaseSender", "Send statistics data failed, abort!" + execute.getStatusLine());
                            Log.w("Statistics-BaseSender", "Response info: " + execute.getStatusLine() + EntityUtils.toString(execute.getEntity()));
                        }
                        d();
                        proxyHttpClient.close();
                        return;
                    }
                }
            } catch (Exception e) {
                Log.e("Statistics-BaseSender", "startSendLoop Exception: " + e);
            }
        } else if (com.baidu.android.pushservice.b.a(this.a)) {
            Log.e("Statistics-BaseSender", "mUrl is null");
        }
    }

    /* access modifiers changed from: package-private */
    public abstract boolean a();

    /* access modifiers changed from: package-private */
    public abstract String b();

    /* access modifiers changed from: package-private */
    public abstract void c();

    /* access modifiers changed from: package-private */
    public abstract void d();

    public synchronized void e() {
        if (!this.c) {
            if (!a()) {
                if (com.baidu.android.pushservice.b.a(this.a)) {
                    Log.w("Statistics-BaseSender", "No new data producted, do nothing!");
                }
            } else if (!ConnectManager.isNetworkConnected(this.a)) {
                if (com.baidu.android.pushservice.b.a(this.a)) {
                    Log.w("Statistics-BaseSender", "Network is not reachable!");
                }
            } else if (z.a().d()) {
                this.c = true;
                Thread thread = new Thread(new e(this));
                thread.setName("PushService-stats-sender");
                thread.start();
            } else if (com.baidu.android.pushservice.b.a(this.a)) {
                Log.e("Statistics-BaseSender", "Fail Send Statistics. Token invalid!");
            }
        }
    }
}
