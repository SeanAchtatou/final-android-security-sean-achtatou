package com.baidu.android.pushservice.b;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.location.LocationManager;
import android.net.wifi.WifiManager;
import android.telephony.CellLocation;
import android.telephony.TelephonyManager;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import android.text.TextUtils;
import com.baidu.android.common.logging.Log;
import com.baidu.android.pushservice.a;
import com.baidu.android.pushservice.b;
import com.baidu.android.pushservice.d;
import com.baidu.android.pushservice.util.e;
import com.baidu.android.pushservice.util.l;
import com.baidu.android.pushservice.z;
import java.util.List;

public class c extends d {
    private static c e = null;
    private SQLiteDatabase c = null;
    private a d = null;
    private int f = 0;
    private Location g;
    private int h = 0;
    private String i;
    private String j;
    private String k;
    private TelephonyManager l = null;
    private WifiManager m = null;
    private LocationManager n = null;

    private c(Context context) {
        super(context);
        this.l = (TelephonyManager) context.getSystemService("phone");
        this.m = (WifiManager) context.getSystemService("wifi");
        this.k = z.a().b();
        this.n = (LocationManager) context.getSystemService("location");
    }

    public static c a(Context context) {
        if (e == null) {
            e = new c(context);
        }
        return e;
    }

    private SQLiteDatabase f() {
        try {
            if (this.c == null) {
                this.c = e.a(this.a);
            }
        } catch (Exception e2) {
        }
        return this.c;
    }

    private void g() {
        if (TextUtils.isEmpty(this.i)) {
            this.i = this.l.getDeviceId();
        }
        if (TextUtils.isEmpty(this.k)) {
            this.k = z.a().b();
        }
        CellLocation cellLocation = this.l.getCellLocation();
        int phoneType = this.l.getPhoneType();
        int cid = phoneType == 1 ? ((GsmCellLocation) cellLocation).getCid() : phoneType == 2 ? ((CdmaCellLocation) cellLocation).getNetworkId() : 0;
        if (cid <= 0) {
            cid = this.h;
        }
        this.h = cid;
        String macAddress = this.m.getConnectionInfo().getMacAddress();
        if (TextUtils.isEmpty(macAddress)) {
            macAddress = this.j;
        }
        this.j = macAddress;
        Location lastKnownLocation = this.n.getLastKnownLocation("gps");
        if (lastKnownLocation == null) {
            lastKnownLocation = this.g;
        }
        this.g = lastKnownLocation;
        if (b.a(this.a)) {
            Log.d("AppStatisticsSender", ">>> Completed update client info: ");
            Log.d("AppStatisticsSender", "    imei=" + this.i);
            Log.d("AppStatisticsSender", "    channelid=" + this.k);
            Log.d("AppStatisticsSender", "    cellid=" + this.h);
            Log.d("AppStatisticsSender", "    wifi=" + this.j);
            Log.d("AppStatisticsSender", "    location=" + this.g);
        }
    }

    private void h() {
        if (this.d != null && this.d.a.size() != 0) {
            for (b bVar : this.d.a) {
                e.b(f(), bVar.a);
            }
            this.d = null;
        } else if (b.a(this.a)) {
            Log.d("AppStatisticsSender", "stat info has no record, cancel sync database mStatInfo=" + this.d);
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        return e.c(f()) != 0;
    }

    /* access modifiers changed from: protected */
    public String b() {
        if (b.a(this.a)) {
            Log.d("AppStatisticsSender", "start productSendData");
        }
        g();
        this.d = new a();
        a aVar = this.d;
        int i2 = this.f;
        this.f = i2 + 1;
        aVar.a(i2, this.g, this.h, this.j);
        List<l> a = e.a(f(), 10);
        if (a == null || a.size() == 0) {
            if (b.a(this.a)) {
                Log.d("AppStatisticsSender", "feedbackList is null, return null.");
            }
            return null;
        }
        for (l lVar : a) {
            b bVar = new b();
            d a2 = a.a(this.a).a(lVar.b);
            if (a2 != null) {
                bVar.b = a2.b;
                bVar.c = a2.c;
                bVar.a = lVar.a;
                bVar.d = lVar.c;
                bVar.e = lVar.d;
                bVar.f = lVar.e;
                bVar.g = lVar.f;
                bVar.h = lVar.g;
                bVar.i = lVar.h;
                if (b.a(this.a)) {
                    try {
                        Log.d("AppStatisticsSender", ">>> Get one App statistics record: " + bVar.a().toString());
                    } catch (Exception e2) {
                        Log.d("AppStatisticsSender", ">>> Get one App statistics Exception!");
                    }
                }
                this.d.a(bVar);
            }
        }
        if (this.d.a.size() != 0) {
            return this.d.a();
        }
        if (b.a(this.a)) {
            Log.d("AppStatisticsSender", "recordList num is 0.");
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public void c() {
        h();
    }

    /* access modifiers changed from: protected */
    public void d() {
        if (b.a(this.a)) {
            Log.d("AppStatisticsSender", "The last send if fail, maybe has network problem now. Abort task, try later.");
        }
        this.d = null;
    }
}
