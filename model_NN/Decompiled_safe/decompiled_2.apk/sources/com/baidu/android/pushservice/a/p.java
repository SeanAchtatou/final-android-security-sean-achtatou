package com.baidu.android.pushservice.a;

import android.content.Context;
import com.baidu.android.common.logging.Log;
import com.baidu.android.pushservice.PushConstants;
import com.baidu.android.pushservice.b;
import java.util.Iterator;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

public class p extends c {
    public p(k kVar, Context context) {
        super(kVar, context);
    }

    /* access modifiers changed from: protected */
    public void a(List list) {
        super.a(list);
        list.add(new BasicNameValuePair(PushConstants.EXTRA_METHOD, "glist"));
        if (b.a()) {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                Log.d("Glist", "Glist param -- " + ((NameValuePair) it.next()).toString());
            }
        }
    }
}
