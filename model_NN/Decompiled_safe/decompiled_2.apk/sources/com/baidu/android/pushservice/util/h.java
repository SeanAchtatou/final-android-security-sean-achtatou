package com.baidu.android.pushservice.util;

enum h {
    belongTo,
    downloadUrl,
    title,
    description,
    savePath,
    fileName,
    downloadBytes,
    totalBytes,
    downloadStatus,
    timeStamp
}
