package com.baidu.android.pushservice.message;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.text.TextUtils;
import com.baidu.android.common.logging.Log;
import com.baidu.android.pushservice.PushConstants;
import com.baidu.android.pushservice.PushService;
import com.baidu.android.pushservice.b;
import com.baidu.android.pushservice.d;
import com.baidu.android.pushservice.e;
import com.baidu.android.pushservice.util.a;
import com.baidu.android.pushservice.util.n;
import com.baidu.android.pushservice.z;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Iterator;
import org.json.JSONException;
import org.json.JSONObject;

public class c extends a {
    private a e;

    public c(Context context, e eVar, InputStream inputStream, OutputStream outputStream) {
        super(context, eVar, inputStream, outputStream);
        this.e = new a(inputStream);
    }

    private int a(String str, String str2, String str3, byte[] bArr) {
        d b = com.baidu.android.pushservice.a.a(this.c).b(str);
        if (b == null || !b.c.equals(str2)) {
            String str4 = ">>> NOT delivere message to app: " + (b == null ? "client not found." : "userid not match: msg_userID-" + str2 + " client_userId-" + b.c + " in " + b.a);
            Log.i("MessageHandler", str4);
            n.a(str4);
            return 2;
        }
        PublicMsg a = j.a(str3, str, bArr);
        if (a != null) {
            Intent intent = new Intent(PushConstants.ACTION_MESSAGE);
            intent.setPackage(b.a);
            intent.putExtra(PushConstants.EXTRA_PUSH_MESSAGE_STRING, a.d);
            intent.setFlags(32);
            if (!TextUtils.isEmpty(a.n)) {
                try {
                    JSONObject jSONObject = new JSONObject(a.n);
                    Iterator<String> keys = jSONObject.keys();
                    while (keys.hasNext()) {
                        String next = keys.next();
                        intent.putExtra(next, jSONObject.getString(next));
                    }
                    intent.putExtra(PushConstants.EXTRA_EXTRA, a.n);
                } catch (JSONException e2) {
                    Log.w("MessageHandler", "Custom content to JSONObject exception::" + e2.getMessage());
                }
            }
            this.c.sendBroadcast(intent);
            String str5 = ">>> Deliver message to client: " + b.a;
            Log.i("MessageHandler", str5);
            n.a(str5);
        }
        return 0;
    }

    private int a(String str, String str2, byte[] bArr) {
        PublicMsg a = j.a(str2, str, bArr);
        if (a == null || TextUtils.isEmpty(a.e)) {
            Log.d("MessageHandler", ">>> Don't Show rich media Notification! url is null");
            n.a(">>> Don't Show rich media Notification! url is null");
        } else {
            d b = com.baidu.android.pushservice.a.a(this.c).b(str);
            if (!(b == null || b.a == null)) {
                a.f = b.a;
            }
            b(a, str2);
            n.a(">>> Show rich media Notification!");
        }
        return 0;
    }

    private int a(byte[] bArr) {
        String str;
        Intent intent;
        String str2 = null;
        boolean z = true;
        try {
            JSONObject jSONObject = new JSONObject(new String(bArr));
            str = jSONObject.getString("action");
            try {
                str2 = jSONObject.getString(PushConstants.EXTRA_PUSH_MESSAGE);
            } catch (JSONException e2) {
                e = e2;
            }
        } catch (JSONException e3) {
            e = e3;
            str = null;
            if (b.a()) {
                Log.d("MessageHandler", "Supper message parsing action Fail:\r\n" + e.getMessage());
            }
            z = false;
            if (z) {
            }
            intent = new Intent("com.baidu.pushservice.action.supper.MESSAGE");
            intent.putExtra(PushConstants.EXTRA_PUSH_MESSAGE, bArr);
            n.a(">>> Deliver baidu supper msg with g action: com.baidu.pushservice.action.supper.MESSAGE");
            intent.setFlags(32);
            this.c.sendBroadcast(intent);
            return 0;
        }
        if (z || TextUtils.isEmpty(str)) {
            intent = new Intent("com.baidu.pushservice.action.supper.MESSAGE");
            intent.putExtra(PushConstants.EXTRA_PUSH_MESSAGE, bArr);
            n.a(">>> Deliver baidu supper msg with g action: com.baidu.pushservice.action.supper.MESSAGE");
        } else {
            intent = new Intent(str);
            intent.putExtra(PushConstants.EXTRA_PUSH_MESSAGE, str2);
            if (b.a()) {
                n.a(">>> Deliver baidu supper msg with s action: " + str);
            }
        }
        intent.setFlags(32);
        this.c.sendBroadcast(intent);
        return 0;
    }

    private void a(PublicMsg publicMsg, String str) {
        NotificationManager notificationManager = (NotificationManager) this.c.getSystemService("notification");
        Intent intent = new Intent(this.c, PushService.class);
        intent.setAction("com.baidu.pushservice.action.publicmsg.CLICK_V2");
        intent.setData(Uri.parse("content://" + str));
        intent.putExtra("public_msg", publicMsg);
        Intent intent2 = new Intent(this.c, PushService.class);
        intent2.setAction("com.baidu.pushservice.action.publicmsg.DELETE_V2");
        intent2.setData(Uri.parse("content://" + str));
        intent2.putExtra("public_msg", publicMsg);
        try {
            Class.forName("com.baidu.android.moplus.MoPlusService");
            intent.setClassName(this.c.getPackageName(), "com.baidu.android.moplus.MoPlusService");
            intent2.setClassName(this.c.getPackageName(), "com.baidu.android.moplus.MoPlusService");
        } catch (ClassNotFoundException e2) {
            intent.setClass(this.c, PushService.class);
            intent2.setClass(this.c, PushService.class);
        }
        PendingIntent service = PendingIntent.getService(this.c, 0, intent, 0);
        PendingIntent service2 = PendingIntent.getService(this.c, 0, intent2, 0);
        Notification notification = new Notification();
        notification.icon = 17301569;
        notification.tickerText = publicMsg.c;
        notification.setLatestEventInfo(this.c, publicMsg.c, publicMsg.d, service);
        notification.sound = RingtoneManager.getDefaultUri(2);
        notification.deleteIntent = service2;
        notification.flags |= 16;
        notificationManager.notify((int) Long.parseLong(str), notification);
    }

    private void a(PublicMsg publicMsg, String str, String str2) {
        Intent intent = new Intent("com.baidu.android.pushservice.action.notification.SHOW");
        intent.setPackage(publicMsg.f);
        intent.putExtra("public_msg", publicMsg);
        intent.putExtra("notify_type", "private");
        intent.putExtra("pushService_package_name", this.c.getPackageName());
        intent.putExtra("message_id", str);
        intent.putExtra(PushConstants.EXTRA_APP_ID, str2);
        try {
            Class.forName("com.baidu.android.moplus.MoPlusService");
            intent.putExtra("service_name", "com.baidu.android.moplus.MoPlusService");
        } catch (ClassNotFoundException e2) {
            intent.putExtra("service_name", "com.baidu.android.pushservice.PushService");
        }
        this.c.sendBroadcast(intent);
    }

    /* JADX WARNING: Removed duplicated region for block: B:38:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean a(com.baidu.android.pushservice.message.PublicMsg r7) {
        /*
            r6 = this;
            r1 = 1
            r2 = 0
            int r0 = r7.i
            if (r0 != r1) goto L_0x0054
            android.content.Context r0 = r6.c
            android.content.Context r0 = r0.getApplicationContext()
            java.lang.String r3 = "connectivity"
            java.lang.Object r0 = r0.getSystemService(r3)
            android.net.ConnectivityManager r0 = (android.net.ConnectivityManager) r0
            android.net.NetworkInfo r0 = r0.getActiveNetworkInfo()
            if (r0 == 0) goto L_0x00bb
            boolean r3 = com.baidu.android.pushservice.b.a()
            if (r3 == 0) goto L_0x0040
            java.lang.String r3 = "MessageHandler"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "network type : "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r5 = r0.getTypeName()
            java.lang.String r5 = r5.toLowerCase()
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r4 = r4.toString()
            com.baidu.android.common.logging.Log.d(r3, r4)
        L_0x0040:
            java.lang.String r3 = "wifi"
            java.lang.String r0 = r0.getTypeName()
            java.lang.String r0 = r0.toLowerCase()
            boolean r0 = r3.equals(r0)
            if (r0 == 0) goto L_0x00bb
            r0 = r1
        L_0x0051:
            if (r0 != 0) goto L_0x0054
        L_0x0053:
            return r2
        L_0x0054:
            java.lang.String r0 = r7.o
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 == 0) goto L_0x006b
            boolean r0 = com.baidu.android.pushservice.b.a()
            if (r0 == 0) goto L_0x0069
            java.lang.String r0 = "MessageHandler"
            java.lang.String r2 = ">>> isNeedShowNotification supportapp = null"
            com.baidu.android.common.logging.Log.d(r0, r2)
        L_0x0069:
            r2 = r1
            goto L_0x0053
        L_0x006b:
            android.content.Context r0 = r6.c
            android.content.pm.PackageManager r0 = r0.getPackageManager()
            java.lang.String r3 = r7.o     // Catch:{ NameNotFoundException -> 0x00a9 }
            r4 = 0
            android.content.pm.PackageInfo r0 = r0.getPackageInfo(r3, r4)     // Catch:{ NameNotFoundException -> 0x00a9 }
            if (r0 == 0) goto L_0x00b9
            boolean r0 = com.baidu.android.pushservice.b.a()     // Catch:{ NameNotFoundException -> 0x00a9 }
            if (r0 == 0) goto L_0x009a
            java.lang.String r0 = "MessageHandler"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ NameNotFoundException -> 0x00a9 }
            r3.<init>()     // Catch:{ NameNotFoundException -> 0x00a9 }
            java.lang.String r4 = ">>> isNeedShowNotification supportapp found \r\n pckname = "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ NameNotFoundException -> 0x00a9 }
            java.lang.String r4 = r7.o     // Catch:{ NameNotFoundException -> 0x00a9 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ NameNotFoundException -> 0x00a9 }
            java.lang.String r3 = r3.toString()     // Catch:{ NameNotFoundException -> 0x00a9 }
            com.baidu.android.common.logging.Log.d(r0, r3)     // Catch:{ NameNotFoundException -> 0x00a9 }
        L_0x009a:
            r0 = r1
        L_0x009b:
            boolean r3 = r7.p
            if (r3 == 0) goto L_0x00a1
            if (r0 != 0) goto L_0x00a7
        L_0x00a1:
            boolean r3 = r7.p
            if (r3 != 0) goto L_0x0053
            if (r0 != 0) goto L_0x0053
        L_0x00a7:
            r2 = r1
            goto L_0x0053
        L_0x00a9:
            r0 = move-exception
            boolean r3 = com.baidu.android.pushservice.b.a()
            if (r3 == 0) goto L_0x00b9
            java.lang.String r3 = "MessageHandler"
            java.lang.String r0 = r0.getMessage()
            com.baidu.android.common.logging.Log.d(r3, r0)
        L_0x00b9:
            r0 = r2
            goto L_0x009b
        L_0x00bb:
            r0 = r2
            goto L_0x0051
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.android.pushservice.message.c.a(com.baidu.android.pushservice.message.PublicMsg):boolean");
    }

    private byte[] a(long j, String str, String str2, String str3, long j2, int i, int i2) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        com.baidu.android.pushservice.util.c cVar = new com.baidu.android.pushservice.util.c(byteArrayOutputStream);
        if (b.a()) {
            Log.i("MessageHandler", "wrapMsgHead , appid:" + str + " userid:" + str2 + " msgid: " + j2 + " type: " + i + " result: " + i2);
        }
        try {
            cVar.a(Long.parseLong(z.a().b()));
            cVar.a(a(str, 128));
            cVar.a(a(str2, 128));
            cVar.a(a(str3, 128));
            cVar.a(j2);
            cVar.b(i);
            cVar.b(0);
            cVar.b(i2);
            cVar.b(0);
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            try {
                byteArrayOutputStream.close();
                cVar.a();
                return byteArray;
            } catch (IOException e2) {
                e2.printStackTrace();
                return byteArray;
            }
        } catch (Exception e3) {
            Log.e("MessageHandler", e3);
            try {
                byteArrayOutputStream.close();
                cVar.a();
            } catch (IOException e4) {
                e4.printStackTrace();
            }
            return null;
        } catch (Throwable th) {
            try {
                byteArrayOutputStream.close();
                cVar.a();
            } catch (IOException e5) {
                e5.printStackTrace();
            }
            throw th;
        }
    }

    private byte[] a(String str, int i) {
        byte[] bArr = new byte[i];
        if (str != null) {
            byte[] bytes = str.getBytes();
            System.arraycopy(bytes, 0, bArr, 0, Math.min(bArr.length, bytes.length));
        }
        return bArr;
    }

    private byte[] a(short s, byte[] bArr) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        com.baidu.android.pushservice.util.c cVar = new com.baidu.android.pushservice.util.c(byteArrayOutputStream);
        int length = bArr != null ? bArr.length : 0;
        try {
            cVar.a((int) s);
            if (!(s == 5 || s == 6)) {
                cVar.a(9);
                cVar.b(0);
                cVar.a(a(n.e(this.c, this.c.getPackageName()) ? "BaiduApp" : "DevApp", 16));
                cVar.b(-76508268);
                cVar.b(1);
                cVar.b(length);
                if (bArr != null) {
                    cVar.a(bArr);
                }
            }
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            try {
                byteArrayOutputStream.close();
                cVar.a();
                return byteArray;
            } catch (IOException e2) {
                e2.printStackTrace();
                return byteArray;
            }
        } catch (IOException e3) {
            e3.printStackTrace();
            try {
                byteArrayOutputStream.close();
                cVar.a();
            } catch (IOException e4) {
                e4.printStackTrace();
            }
            return null;
        } catch (Throwable th) {
            try {
                byteArrayOutputStream.close();
                cVar.a();
            } catch (IOException e5) {
                e5.printStackTrace();
            }
            throw th;
        }
    }

    private int b(String str, String str2, byte[] bArr) {
        PublicMsg a = j.a(str2, str, bArr);
        if (a == null || TextUtils.isEmpty(a.d)) {
            Log.e("MessageHandler", ">>> pMsg JSON parsing error!");
            n.a(">>> pMsg JSON parsing error!");
            return 2;
        }
        d b = com.baidu.android.pushservice.a.a(this.c).b(str);
        if (b == null || b.a == null) {
            Log.d("MessageHandler", ">>> Don't Show pMsg private Notification! package name is null");
            n.a(">>> Don't Show pMsg private Notification! package name is null");
            return 0;
        }
        a.f = b.a;
        if (TextUtils.isEmpty(a.c)) {
            PackageManager packageManager = this.c.getPackageManager();
            a.c = packageManager.getApplicationLabel(packageManager.getApplicationInfo(a.f, 128)).toString();
        }
        a(a, str2, str);
        n.a(">>> Show pMsg private Notification!");
        return 1;
    }

    private String b(byte[] bArr) {
        if (bArr == null || bArr.length == 0) {
            return "";
        }
        int i = 0;
        while (true) {
            if (i >= bArr.length) {
                i = 0;
                break;
            } else if (bArr[i] == 0) {
                break;
            } else {
                i++;
            }
        }
        return new String(bArr, 0, i);
    }

    private void b(PublicMsg publicMsg, String str) {
        Intent intent = new Intent("com.baidu.android.pushservice.action.notification.SHOW");
        intent.setPackage(publicMsg.f);
        intent.putExtra("public_msg", publicMsg);
        intent.putExtra("notify_type", "rich_media");
        intent.putExtra("pushService_package_name", this.c.getPackageName());
        try {
            Class.forName("com.baidu.android.moplus.MoPlusService");
            intent.putExtra("service_name", "com.baidu.android.moplus.MoPlusService");
        } catch (ClassNotFoundException e2) {
            intent.putExtra("service_name", "com.baidu.android.pushservice.PushService");
        }
        this.c.sendBroadcast(intent);
    }

    private int c(String str, String str2, byte[] bArr) {
        PublicMsg a = j.a(str2, str, bArr);
        if (a == null || TextUtils.isEmpty(a.c) || TextUtils.isEmpty(a.d) || TextUtils.isEmpty(a.e)) {
            Log.e("MessageHandler", ">>> pMsg JSON parsing error!");
            n.a(">>> pMsg JSON parsing error!");
            return 2;
        } else if (!a(a) || !n.e(this.c, this.c.getPackageName())) {
            String str3 = ">>> Don't Show pMsg Notification! --- IsBaiduApp = " + n.e(this.c, this.c.getPackageName());
            if (b.a()) {
                Log.d("MessageHandler", str3);
            }
            n.a(str3);
            return 0;
        } else {
            Log.d("MessageHandler", ">>> Show pMsg Notification!");
            n.a(">>> Show pMsg Notification!");
            a(a, str2);
            return 1;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent} */
    private void c(b bVar) {
        String str = new String(bVar.c);
        if (b.a()) {
            Log.i("MessageHandler", "handleMessage MSG_ID_HANDSHAKE : " + str);
        }
        int i = new JSONObject(str).getInt("ret");
        if (b.a()) {
            Log.i("MessageHandler", "handleMessage MSG_ID_HANDSHAKE : result = " + i);
        }
        if (i == 0) {
            Intent intent = new Intent("com.baidu.pushservice.action.connect.STAMP_V2");
            intent.putExtra("last_connect_stamp", System.currentTimeMillis());
            this.c.sendStickyBroadcast(intent);
            com.baidu.android.pushservice.a.b.b(this.c);
        } else if (i == 5003) {
            Intent intent2 = new Intent("com.baidu.pushservice.action.connect.STAMP_V2");
            intent2.putExtra("last_connect_stamp", 0L);
            this.c.sendStickyBroadcast(intent2);
            com.baidu.android.pushservice.a.b.b(this.c);
        } else if (i == 2002) {
            z.a().a((String) null, (String) null);
            n.d(this.c);
        } else {
            throw new d("MessageHandler handle handshake msg failed. ret = " + i);
        }
        Intent intent3 = new Intent("com.baidu.pushservice.action.connect.GETCHANNELID");
        intent3.putExtra("channel_id", z.a().b());
        this.c.sendStickyBroadcast(intent3);
    }

    private int d(String str, String str2, byte[] bArr) {
        PackageInfo packageInfo;
        d b = com.baidu.android.pushservice.a.a(this.c).b(str);
        if (b != null) {
            try {
                packageInfo = this.c.getPackageManager().getPackageInfo(b.a, 0);
            } catch (PackageManager.NameNotFoundException e2) {
                Log.e("MessageHandler", Log.getStackTraceString(e2));
                packageInfo = null;
            }
            if (packageInfo == null) {
                com.baidu.android.pushservice.a.b.b(this.c);
                String str3 = ">>> NOT deliver to app: " + b.a + ", package has been uninstalled.";
                Log.i("MessageHandler", str3);
                n.a(str3);
                return 0;
            } else if (b.c.equals(str2)) {
                Intent intent = new Intent(PushConstants.ACTION_MESSAGE);
                intent.setPackage(b.a);
                intent.putExtra(PushConstants.EXTRA_PUSH_MESSAGE, bArr);
                intent.putExtra(PushConstants.EXTRA_PUSH_MESSAGE_STRING, new String(bArr, "UTF-8"));
                intent.setFlags(32);
                this.c.sendBroadcast(intent);
                String str4 = ">>> Deliver message to client: " + b.a;
                Log.d("MessageHandler", str4);
                n.a(str4);
                return 0;
            } else {
                String str5 = ">>> Not deliver message to client: client userid: " + b.c;
                Log.d("MessageHandler", str5);
                n.a(str5);
            }
        } else {
            Log.d("MessageHandler", ">>> Not deliver message to client: client NOT found");
            n.a(">>> Not deliver message to client: client NOT found");
        }
        return 2;
    }

    private void d(b bVar) {
        if (b.a()) {
            Log.i("MessageHandler", "handleMessage: server heart beat id - " + bVar.a);
        }
        if (b.a()) {
            Log.i("MessageHandler", "handleServerHeartbeatMsg, send handshake return msg ");
        }
        byte[] a = a((short) bVar.a, (byte[]) null);
        b bVar2 = new b();
        bVar2.c = a;
        a(bVar2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.android.pushservice.message.c.a(short, byte[]):byte[]
     arg types: [int, byte[]]
     candidates:
      com.baidu.android.pushservice.message.c.a(com.baidu.android.pushservice.message.PublicMsg, java.lang.String):void
      com.baidu.android.pushservice.message.c.a(java.lang.String, int):byte[]
      com.baidu.android.pushservice.message.c.a(short, byte[]):byte[] */
    private void e(b bVar) {
        int i;
        byte[] bArr = bVar.c;
        if (bArr != null) {
            a aVar = new a(new ByteArrayInputStream(bArr));
            long c = aVar.c();
            byte[] bArr2 = new byte[128];
            aVar.a(bArr2);
            String b = b(bArr2);
            byte[] bArr3 = new byte[128];
            aVar.a(bArr3);
            String b2 = b(bArr3);
            byte[] bArr4 = new byte[128];
            aVar.a(bArr4);
            String b3 = b(bArr4);
            com.baidu.android.pushservice.util.b d = aVar.d();
            String str = d.a;
            long j = d.b;
            int a = aVar.a();
            int a2 = aVar.a();
            int a3 = aVar.a();
            int a4 = aVar.a();
            int length = bArr.length - 416;
            if (length <= 0) {
                length = 0;
            }
            byte[] bArr5 = new byte[length];
            System.arraycopy(bArr, 416, bArr5, 0, length);
            String str2 = "type:" + a + " channelId:" + c + " appid:" + b + " userid:" + b2 + " msgId:" + str + " msgKey:" + b3 + " bodyLength:" + a2 + " result:" + a3 + " reserved:" + a4;
            if (b.a()) {
                Log.i("MessageHandler", "New MSG: " + str2);
                Log.i("MessageHandler", "msgBody :" + new String(bArr5));
            }
            n.a("New MSG: " + str2 + " msgBody :" + new String(bArr5));
            b bVar2 = new b();
            if (this.d.a(str)) {
                if (b.a()) {
                    Log.d("MessageHandler", "Message ID(" + str + ") received duplicated, ack success to server directly.");
                }
                n.a(">>> MSG ID duplicated, not deliver to app.");
                bVar2.c = a((short) 3, a(c, b, b2, b3, j, a, 0));
                a(bVar2);
                return;
            }
            if (a == 0 || a == 1) {
                i = d(b, b2, bArr5);
            } else if (a == 6) {
                i = a(b, b2, str, bArr5);
            } else if (a == 2 || a == 3) {
                i = c(b, str, bArr5);
            } else if (a == 5) {
                i = b(b, str, bArr5);
            } else if (a == 7) {
                i = a(b, str, bArr5);
            } else if (a == 10) {
                i = a(bArr5);
            } else {
                Log.e("MessageHandler", ">>> Unknown msg_type : " + a);
                n.a(">>> Unknown msg_type : " + a);
                i = 2;
            }
            bVar2.c = a((short) 3, a(c, b, b2, b3, j, a, i));
            a(bVar2);
        }
    }

    public b a() {
        byte[] bArr;
        int i = 20480;
        short b = this.e.b();
        b bVar = new b();
        bVar.a = b;
        if (b == 6 || b == 5) {
            if (b.a()) {
                Log.i("MessageHandler", "readMessage tiny heart beat from server, msgId:" + ((int) b));
            }
            return bVar;
        }
        short b2 = this.e.b();
        int a = this.e.a();
        this.e.a(new byte[16]);
        int a2 = this.e.a();
        int a3 = this.e.a();
        int a4 = this.e.a();
        if (b.a()) {
            Log.i("MessageHandler", "readMessage nshead, msgId:" + ((int) b) + " magicNum:" + Integer.toHexString(a2) + " length:" + a4 + " version =" + ((int) b2) + " logId =" + a + " reserved = " + a3);
        }
        if (a4 > 0) {
            if (a4 <= 20480) {
                i = a4;
            }
            bArr = new byte[i];
            this.e.a(bArr);
        } else {
            bArr = null;
        }
        bVar.c = bArr;
        return bVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.android.pushservice.message.c.a(short, byte[]):byte[]
     arg types: [int, byte[]]
     candidates:
      com.baidu.android.pushservice.message.c.a(com.baidu.android.pushservice.message.PublicMsg, java.lang.String):void
      com.baidu.android.pushservice.message.c.a(java.lang.String, int):byte[]
      com.baidu.android.pushservice.message.c.a(short, byte[]):byte[] */
    public void b() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("channel_token", z.a().c());
            jSONObject.put("channel_id", z.a().b());
            jSONObject.put("period", 1800);
            jSONObject.put("channel_type", 3);
            jSONObject.put("tinyheart", 1);
            jSONObject.put("connect_version", 2);
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
        String jSONObject2 = jSONObject.toString();
        if (b.a()) {
            Log.i("MessageHandler", "onSessionOpened, send handshake msg :" + jSONObject2);
        }
        byte[] a = a((short) 1, jSONObject2.getBytes());
        b bVar = new b();
        bVar.c = a;
        bVar.d = true;
        a(bVar);
    }

    public void b(b bVar) {
        if (bVar != null) {
            int i = bVar.a;
            if (i == 1) {
                c(bVar);
            } else if (i == 2 || i == 6) {
                d(bVar);
            } else if (i == 4) {
                if (b.a()) {
                    Log.i("MessageHandler", "handleMessage MSG_ID_HEARTBEAT_CLIENT");
                }
            } else if (i == 5) {
                if (b.a()) {
                    Log.i("MessageHandler", "handleMessage MSG_ID_TIMY_HEARTBEAT_CLIENT");
                }
            } else if (i == 3) {
                e(bVar);
            }
        }
    }

    public void c() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.android.pushservice.message.c.a(short, byte[]):byte[]
     arg types: [int, ?[OBJECT, ARRAY]]
     candidates:
      com.baidu.android.pushservice.message.c.a(com.baidu.android.pushservice.message.PublicMsg, java.lang.String):void
      com.baidu.android.pushservice.message.c.a(java.lang.String, int):byte[]
      com.baidu.android.pushservice.message.c.a(short, byte[]):byte[] */
    public void d() {
        if (b.a()) {
            Log.i("MessageHandler", "sendHeartbeatMessage ");
        }
        byte[] a = a((short) 5, (byte[]) null);
        b bVar = new b();
        bVar.c = a;
        bVar.d = true;
        a(bVar);
    }
}
