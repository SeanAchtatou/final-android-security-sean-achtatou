package com.baidu.android.pushservice.util;

import android.content.Context;
import android.content.Intent;

public final class Internal implements NoProGuard {
    private Internal() {
    }

    public static Intent createBdussInent(Context context) {
        return n.g(context);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.android.pushservice.util.n.b(android.content.Context, boolean):void
     arg types: [android.content.Context, int]
     candidates:
      com.baidu.android.pushservice.util.n.b(android.content.Context, java.lang.String):android.content.Intent
      com.baidu.android.pushservice.util.n.b(android.content.Context, boolean):void */
    public static void disablePushConnection(Context context) {
        n.b(context, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.android.pushservice.util.n.a(android.content.Context, boolean):void
     arg types: [android.content.Context, int]
     candidates:
      com.baidu.android.pushservice.util.n.a(android.content.Context, java.lang.String):android.content.pm.PackageInfo
      com.baidu.android.pushservice.util.n.a(android.content.Context, long):void
      com.baidu.android.pushservice.util.n.a(java.lang.String, java.util.List):boolean
      com.baidu.android.pushservice.util.n.a(java.lang.String, java.lang.String[]):boolean
      com.baidu.android.pushservice.util.n.a(android.content.Context, boolean):void */
    public static void disablePushService(Context context) {
        n.a(context, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.android.pushservice.util.n.b(android.content.Context, boolean):void
     arg types: [android.content.Context, int]
     candidates:
      com.baidu.android.pushservice.util.n.b(android.content.Context, java.lang.String):android.content.Intent
      com.baidu.android.pushservice.util.n.b(android.content.Context, boolean):void */
    public static void enablePushConnection(Context context) {
        n.b(context, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.android.pushservice.util.n.a(android.content.Context, boolean):void
     arg types: [android.content.Context, int]
     candidates:
      com.baidu.android.pushservice.util.n.a(android.content.Context, java.lang.String):android.content.pm.PackageInfo
      com.baidu.android.pushservice.util.n.a(android.content.Context, long):void
      com.baidu.android.pushservice.util.n.a(java.lang.String, java.util.List):boolean
      com.baidu.android.pushservice.util.n.a(java.lang.String, java.lang.String[]):boolean
      com.baidu.android.pushservice.util.n.a(android.content.Context, boolean):void */
    public static void enablePushService(Context context) {
        n.a(context, true);
    }
}
