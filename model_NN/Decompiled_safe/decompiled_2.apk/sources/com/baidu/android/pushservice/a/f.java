package com.baidu.android.pushservice.a;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.text.TextUtils;
import com.baidu.android.common.logging.Log;
import com.baidu.android.pushservice.PushConstants;
import com.baidu.android.pushservice.b;
import java.util.Iterator;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

public class f extends d {
    protected int f = 0;
    private String g;
    private int h;

    public f(k kVar, Context context, int i, String str, int i2) {
        super(kVar, context);
        this.f = i;
        this.g = str;
        this.h = i2;
        if (this.f == 0) {
            this.e = true;
        }
    }

    /* access modifiers changed from: protected */
    public void a(Intent intent) {
        intent.putExtra(PushConstants.EXTRA_BIND_STATUS, this.f);
    }

    /* access modifiers changed from: protected */
    public void a(List list) {
        super.a(list);
        list.add(new BasicNameValuePair(PushConstants.EXTRA_METHOD, "bind"));
        list.add(new BasicNameValuePair(PushConstants.EXTRA_BIND_NAME, TextUtils.isEmpty(this.g) ? Build.MODEL : this.g));
        list.add(new BasicNameValuePair(PushConstants.EXTRA_BIND_STATUS, this.f + ""));
        list.add(new BasicNameValuePair(PushConstants.EXTRA_PUSH_SDK_VERSION, this.h + ""));
        if (b.a()) {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                Log.d("Bind", "BIND param -- " + ((NameValuePair) it.next()).toString());
            }
        }
    }
}
