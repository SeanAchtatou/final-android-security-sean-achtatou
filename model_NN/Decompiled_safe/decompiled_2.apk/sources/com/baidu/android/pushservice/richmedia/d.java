package com.baidu.android.pushservice.richmedia;

import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.AdapterView;
import com.baidu.android.pushservice.util.e;
import com.baidu.android.pushservice.util.i;
import java.io.File;
import java.util.HashMap;

class d implements AdapterView.OnItemClickListener {
    final /* synthetic */ MediaListActivity a;

    d(MediaListActivity mediaListActivity) {
        this.a = mediaListActivity;
    }

    public void onItemClick(AdapterView adapterView, View view, int i, long j) {
        i a2 = e.a(e.a(this.a), (String) ((HashMap) adapterView.getItemAtPosition(i)).get(MediaListActivity.r));
        if (a2 == null) {
            return;
        }
        if (a2.i == b.f) {
            String str = a2.e;
            String str2 = a2.f;
            String str3 = str + "/" + str2.substring(0, str2.lastIndexOf(".")) + "/index.html";
            Intent intent = new Intent();
            intent.setClass(this.a, MediaViewActivity.class);
            intent.setData(Uri.fromFile(new File(str3)));
            intent.addFlags(268435456);
            this.a.startActivity(intent);
            return;
        }
        this.a.a(a2.b, a2.c, a2.d);
    }
}
