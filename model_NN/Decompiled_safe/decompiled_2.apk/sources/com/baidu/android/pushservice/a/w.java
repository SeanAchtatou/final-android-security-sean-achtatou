package com.baidu.android.pushservice.a;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import com.baidu.android.common.logging.Log;
import com.baidu.android.common.security.Base64;
import com.baidu.android.common.security.RSAUtil;
import com.baidu.android.common.util.DeviceId;
import com.baidu.android.pushservice.PushConstants;
import com.baidu.android.pushservice.PushSDK;
import com.baidu.android.pushservice.b;
import com.baidu.android.pushservice.util.n;
import com.baidu.android.pushservice.z;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

public class w implements Runnable {
    private Context a;
    private int b = 5;
    private int c = 0;
    private boolean d = false;

    public w(Context context) {
        this.a = context.getApplicationContext();
    }

    /* JADX WARNING: Removed duplicated region for block: B:34:0x018f A[Catch:{ all -> 0x01cc }] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x01ac A[Catch:{ all -> 0x01cc }] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:39:0x01a6=Splitter:B:39:0x01a6, B:31:0x0189=Splitter:B:31:0x0189} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean b() {
        /*
            r11 = this;
            r0 = 1
            r1 = 0
            java.lang.String r2 = com.baidu.android.pushservice.x.f
            boolean r3 = com.baidu.android.pushservice.b.a()
            if (r3 == 0) goto L_0x0022
            java.lang.String r3 = "TokenRequester"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = ">>> token request:"
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r4 = r4.append(r2)
            java.lang.String r4 = r4.toString()
            com.baidu.android.common.logging.Log.d(r3, r4)
        L_0x0022:
            com.baidu.android.common.net.ProxyHttpClient r3 = new com.baidu.android.common.net.ProxyHttpClient
            android.content.Context r4 = r11.a
            r3.<init>(r4)
            org.apache.http.client.methods.HttpPost r4 = new org.apache.http.client.methods.HttpPost     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            r4.<init>(r2)     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            java.lang.String r2 = "Content-Type"
            java.lang.String r5 = "application/x-www-form-urlencoded"
            r4.addHeader(r2, r5)     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            java.util.List r2 = r11.c()     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            org.apache.http.client.entity.UrlEncodedFormEntity r5 = new org.apache.http.client.entity.UrlEncodedFormEntity     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            java.lang.String r6 = "UTF-8"
            r5.<init>(r2, r6)     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            r4.setEntity(r5)     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            org.apache.http.HttpResponse r2 = r3.execute(r4)     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            org.apache.http.StatusLine r4 = r2.getStatusLine()     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            int r4 = r4.getStatusCode()     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            r5 = 200(0xc8, float:2.8E-43)
            if (r4 != r5) goto L_0x0142
            org.apache.http.HttpEntity r2 = r2.getEntity()     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            java.lang.String r2 = org.apache.http.util.EntityUtils.toString(r2)     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            boolean r4 = com.baidu.android.pushservice.b.a()     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            if (r4 == 0) goto L_0x0079
            java.lang.String r4 = "TokenRequester"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            r5.<init>()     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            java.lang.String r6 = "<<< RequestToken return string :  "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            java.lang.StringBuilder r5 = r5.append(r2)     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            java.lang.String r5 = r5.toString()     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            com.baidu.android.common.logging.Log.i(r4, r5)     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
        L_0x0079:
            org.json.JSONObject r4 = new org.json.JSONObject     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            r4.<init>(r2)     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            java.lang.String r5 = "response_params"
            org.json.JSONObject r4 = r4.getJSONObject(r5)     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            if (r4 == 0) goto L_0x0122
            java.lang.String r2 = "channel_id"
            java.lang.String r2 = r4.getString(r2)     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            java.lang.String r5 = "rsa_channel_token"
            java.lang.String r5 = r4.getString(r5)     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            byte[] r6 = r5.getBytes()     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            byte[] r6 = com.baidu.android.common.security.Base64.decode(r6)     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            java.lang.String r7 = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC/7VlVn9LIrZ71PL2RZMbK/Yxc\r\ndb046w/cXVylxS7ouPY06namZUFVhdbUnNRJzmGUZlzs3jUbvMO3l+4c9cw/n9aQ\r\nrm/brgaRDeZbeSrQYRZv60xzJIimuFFxsRM+ku6/dAyYmXiQXlRbgvFQ0MsVng4j\r\nv+cXhtTis2Kbwb8mQwIDAQAB\r\n"
            byte[] r6 = com.baidu.android.common.security.RSAUtil.decryptByPublicKey(r6, r7)     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            java.lang.String r7 = new java.lang.String     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            r7.<init>(r6)     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            java.lang.String r6 = "expires_time"
            java.lang.String r4 = r4.getString(r6)     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            boolean r6 = com.baidu.android.pushservice.b.a()     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            if (r6 == 0) goto L_0x0111
            java.lang.String r6 = "TokenRequester"
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            r8.<init>()     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            java.lang.String r9 = "RequestToken channelId :  "
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            java.lang.StringBuilder r8 = r8.append(r2)     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            java.lang.String r8 = r8.toString()     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            com.baidu.android.common.logging.Log.i(r6, r8)     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            java.lang.String r6 = "TokenRequester"
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            r8.<init>()     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            java.lang.String r9 = "RequestToken rsaChannelToken :  "
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            java.lang.StringBuilder r5 = r8.append(r5)     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            java.lang.String r5 = r5.toString()     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            com.baidu.android.common.logging.Log.i(r6, r5)     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            java.lang.String r5 = "TokenRequester"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            r6.<init>()     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            java.lang.String r8 = "RequestToken realChannelToken :  "
            java.lang.StringBuilder r6 = r6.append(r8)     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            java.lang.String r6 = r6.toString()     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            com.baidu.android.common.logging.Log.i(r5, r6)     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            java.lang.String r5 = "TokenRequester"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            r6.<init>()     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            java.lang.String r8 = "RequestToken expiresTime :  "
            java.lang.StringBuilder r6 = r6.append(r8)     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            java.lang.StringBuilder r4 = r6.append(r4)     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            java.lang.String r4 = r4.toString()     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            com.baidu.android.common.logging.Log.i(r5, r4)     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
        L_0x0111:
            com.baidu.android.pushservice.z r4 = com.baidu.android.pushservice.z.a()     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            r4.a(r2, r7)     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
        L_0x0118:
            r1 = 0
            r11.c = r1     // Catch:{ IOException -> 0x01d3, Exception -> 0x01d1 }
            r1 = 0
            r11.d = r1     // Catch:{ IOException -> 0x01d3, Exception -> 0x01d1 }
            r3.close()
        L_0x0121:
            return r0
        L_0x0122:
            boolean r0 = com.baidu.android.pushservice.b.a()     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            if (r0 == 0) goto L_0x0140
            java.lang.String r0 = "TokenRequester"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            r4.<init>()     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            java.lang.String r5 = "RequestToken failed :  "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            java.lang.StringBuilder r2 = r4.append(r2)     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            java.lang.String r2 = r2.toString()     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            com.baidu.android.common.logging.Log.i(r0, r2)     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
        L_0x0140:
            r0 = r1
            goto L_0x0118
        L_0x0142:
            boolean r0 = com.baidu.android.pushservice.b.a()     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            if (r0 == 0) goto L_0x0140
            java.lang.String r0 = "TokenRequester"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            r4.<init>()     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            java.lang.String r5 = "RequestToken request failed  "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            org.apache.http.StatusLine r5 = r2.getStatusLine()     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            java.lang.String r4 = r4.toString()     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            com.baidu.android.common.logging.Log.i(r0, r4)     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            org.apache.http.HttpEntity r0 = r2.getEntity()     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            java.lang.String r0 = org.apache.http.util.EntityUtils.toString(r0)     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            java.lang.String r2 = "TokenRequester"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            r4.<init>()     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            java.lang.String r5 = "<<< RequestToken return string :  "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            java.lang.StringBuilder r0 = r4.append(r0)     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            java.lang.String r0 = r0.toString()     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            com.baidu.android.common.logging.Log.i(r2, r0)     // Catch:{ IOException -> 0x0185, Exception -> 0x01a2 }
            goto L_0x0140
        L_0x0185:
            r0 = move-exception
            r10 = r0
            r0 = r1
            r1 = r10
        L_0x0189:
            boolean r2 = com.baidu.android.pushservice.b.a()     // Catch:{ all -> 0x01cc }
            if (r2 == 0) goto L_0x019b
            java.lang.String r2 = "TokenRequester"
            com.baidu.android.common.logging.Log.e(r2, r1)     // Catch:{ all -> 0x01cc }
            java.lang.String r1 = "TokenRequester"
            java.lang.String r2 = "io exception, schedule retry"
            com.baidu.android.common.logging.Log.i(r1, r2)     // Catch:{ all -> 0x01cc }
        L_0x019b:
            r1 = 1
            r11.d = r1     // Catch:{ all -> 0x01cc }
            r3.close()
            goto L_0x0121
        L_0x01a2:
            r0 = move-exception
            r10 = r0
            r0 = r1
            r1 = r10
        L_0x01a6:
            boolean r2 = com.baidu.android.pushservice.b.a()     // Catch:{ all -> 0x01cc }
            if (r2 == 0) goto L_0x01c4
            java.lang.String r2 = "TokenRequester"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x01cc }
            r4.<init>()     // Catch:{ all -> 0x01cc }
            java.lang.String r5 = "Connect Exception:"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x01cc }
            java.lang.StringBuilder r1 = r4.append(r1)     // Catch:{ all -> 0x01cc }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x01cc }
            com.baidu.android.common.logging.Log.e(r2, r1)     // Catch:{ all -> 0x01cc }
        L_0x01c4:
            r1 = 0
            r11.d = r1     // Catch:{ all -> 0x01cc }
            r3.close()
            goto L_0x0121
        L_0x01cc:
            r0 = move-exception
            r3.close()
            throw r0
        L_0x01d1:
            r1 = move-exception
            goto L_0x01a6
        L_0x01d3:
            r1 = move-exception
            goto L_0x0189
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.android.pushservice.a.w.b():boolean");
    }

    private List c() {
        int i = 1;
        ArrayList<NameValuePair> arrayList = new ArrayList<>();
        arrayList.add(new BasicNameValuePair(PushConstants.EXTRA_METHOD, "token"));
        b.a(arrayList);
        arrayList.add(new BasicNameValuePair("device_type", "3"));
        arrayList.add(new BasicNameValuePair("rsa_device_id", Base64.encode(RSAUtil.encryptByPublicKey(DeviceId.getDeviceID(this.a).getBytes(), "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC/7VlVn9LIrZ71PL2RZMbK/Yxc\r\ndb046w/cXVylxS7ouPY06namZUFVhdbUnNRJzmGUZlzs3jUbvMO3l+4c9cw/n9aQ\r\nrm/brgaRDeZbeSrQYRZv60xzJIimuFFxsRM+ku6/dAyYmXiQXlRbgvFQ0MsVng4j\r\nv+cXhtTis2Kbwb8mQwIDAQAB\r\n"), "utf-8")));
        arrayList.add(new BasicNameValuePair("device_name", Build.MODEL));
        SharedPreferences sharedPreferences = this.a.getSharedPreferences(this.a.getPackageName(), 1);
        int i2 = sharedPreferences.getInt("com.baidu.android.pushservice.PushManager.LOGIN_TYPE", -1);
        String string = sharedPreferences.getString("com.baidu.android.pushservice.PushManager.LONGIN_VALUE", "");
        if (i2 == 2) {
            arrayList.add(new BasicNameValuePair("rsa_bduss", PushConstants.rsaEncrypt(sharedPreferences.getString("com.baidu.android.pushservice.PushManager.BDUSS", ""))));
            arrayList.add(new BasicNameValuePair("appid", string));
        } else if (i2 == 0) {
            arrayList.add(new BasicNameValuePair("rsa_access_token", PushConstants.rsaEncrypt(string)));
        } else {
            arrayList.add(new BasicNameValuePair("apikey", string));
        }
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("api_level", Build.VERSION.SDK_INT);
        int[] b2 = n.b(this.a);
        jSONObject.put("screen_height", b2[0]);
        jSONObject.put("screen_width", b2[1]);
        jSONObject.put("model", Build.MODEL);
        jSONObject.put("isroot", n.a(this.a) ? 1 : 0);
        if (!n.e(this.a, this.a.getPackageName())) {
            i = 0;
        }
        jSONObject.put("is_baidu_app", i);
        jSONObject.put(PushConstants.EXTRA_PUSH_SDK_VERSION, 9);
        arrayList.add(new BasicNameValuePair("info", jSONObject.toString()));
        if (b.a()) {
            for (NameValuePair obj : arrayList) {
                Log.d("TokenRequester", "TOKEN param -- " + obj.toString());
            }
        }
        return arrayList;
    }

    private void d() {
        this.c++;
        if (this.c < this.b) {
            int i = (1 << (this.c - 1)) * 5 * 1000;
            if (b.a()) {
                Log.i("TokenRequester", "schedule retry-- retry times: " + this.c + "time delay: " + i);
            }
            try {
                Thread.sleep((long) i);
            } catch (InterruptedException e) {
                if (b.a()) {
                    Log.e("TokenRequester", e);
                }
            }
        } else {
            if (b.a()) {
                Log.i("TokenRequester", "hava reconnect " + this.b + " times, all failed.");
            }
            this.d = false;
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
        boolean b2;
        do {
            b2 = b();
            if (this.d) {
                d();
            }
            if (this.b <= 0) {
                break;
            }
        } while (this.d);
        if (b.a()) {
            Log.i("TokenRequester", "RequestTokenThread connectResult: " + b2);
        }
        if (!b2) {
            n.d(this.a);
            return;
        }
        b.b(this.a);
        if (PushSDK.mPushConnection != null) {
            if (b.a()) {
                Log.i("TokenRequester", "TokenRequester start PushService after Request finish.");
            }
            b.a(this.a);
        }
    }

    public void a(int i) {
        this.b = i;
    }

    public void run() {
        a();
        synchronized (z.a()) {
            z.a().notifyAll();
        }
    }
}
