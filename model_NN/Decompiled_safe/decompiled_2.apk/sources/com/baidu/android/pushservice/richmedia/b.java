package com.baidu.android.pushservice.richmedia;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import com.baidu.android.pushservice.util.e;
import com.baidu.android.pushservice.util.i;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashSet;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import org.apache.http.HttpResponse;

public class b extends AsyncTask implements Comparable {
    public static int e = 1;
    public static int f = 2;
    private static HashSet h = new HashSet();
    protected s a;
    public WeakReference b;
    protected long c;
    public n d;
    private l g = l.a(b.class.getName());
    private boolean i = false;
    private boolean j = false;

    public b(Context context, s sVar) {
        this.a = sVar;
        this.b = new WeakReference(context);
        this.c = System.currentTimeMillis();
    }

    private int a(String str) {
        try {
            return ((HttpURLConnection) new URL(str).openConnection()).getContentLength();
        } catch (MalformedURLException e2) {
            e2.printStackTrace();
        } catch (IOException e3) {
            e3.printStackTrace();
        }
        return 0;
    }

    private i a(Context context, String str) {
        List b2 = e.b(e.a(context));
        if (b2 != null) {
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 >= b2.size()) {
                    break;
                } else if (((i) b2.get(i3)).b.equalsIgnoreCase(str)) {
                    return (i) b2.get(i3);
                } else {
                    i2 = i3 + 1;
                }
            }
        }
        return null;
    }

    private static void a(File file, String str) {
        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            ZipInputStream zipInputStream = new ZipInputStream(new BufferedInputStream(fileInputStream));
            while (true) {
                ZipEntry nextEntry = zipInputStream.getNextEntry();
                if (nextEntry != null) {
                    try {
                        Log.i("DownloadCompleteReceiver: ", "unzip----=" + nextEntry);
                        byte[] bArr = new byte[4096];
                        String[] split = nextEntry.getName().split("/");
                        File file2 = new File(str + "/" + split[split.length - 1]);
                        if (!nextEntry.isDirectory()) {
                            File file3 = new File(file2.getParent());
                            if (!file3.exists()) {
                                file3.mkdirs();
                            }
                            if (!file2.exists()) {
                                file2.createNewFile();
                            }
                            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(file2), 4096);
                            while (true) {
                                int read = zipInputStream.read(bArr, 0, 4096);
                                if (read == -1) {
                                    break;
                                }
                                bufferedOutputStream.write(bArr, 0, read);
                            }
                            bufferedOutputStream.flush();
                            bufferedOutputStream.close();
                        }
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                } else {
                    fileInputStream.close();
                    zipInputStream.close();
                    return;
                }
            }
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }

    private static synchronized boolean a(n nVar) {
        boolean add;
        synchronized (b.class) {
            add = h.add(nVar);
        }
        return add;
    }

    private static synchronized boolean b(n nVar) {
        boolean remove;
        synchronized (b.class) {
            remove = h.remove(nVar);
        }
        return remove;
    }

    /* renamed from: a */
    public int compareTo(b bVar) {
        if (bVar == null) {
            return -1;
        }
        long a2 = bVar.a();
        if (this.c <= a2) {
            return this.c < a2 ? 1 : 0;
        }
        return -1;
    }

    public long a() {
        return this.c;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public r doInBackground(n... nVarArr) {
        i iVar;
        r rVar = new r();
        this.d = nVarArr[0];
        rVar.d = this.d;
        if (this.d != null) {
            rVar.a = this.d.b();
            if (this.d.b == null) {
                if (com.baidu.android.pushservice.b.a()) {
                    Log.d("HttpTask", "download file Request error: " + this.d);
                }
                rVar.c = 3;
            } else if (!a(this.d)) {
                this.g.c("Request url: " + this.d.d() + " failed, already in queue");
                this.a = null;
                this.d = null;
                return null;
            } else {
                i a2 = a((Context) this.b.get(), this.d.d());
                if (a2 == null) {
                    iVar = new i();
                    iVar.b = this.d.d();
                    iVar.a = this.d.a;
                    iVar.c = this.d.c;
                    iVar.d = this.d.d;
                    iVar.g = 0;
                    iVar.h = a(iVar.b);
                    iVar.i = e;
                    iVar.f = iVar.b.substring(iVar.b.lastIndexOf(47) + 1);
                    iVar.e = this.d.b;
                    e.a(e.a((Context) this.b.get()), iVar);
                } else {
                    a2.h = a(a2.b);
                    iVar = a2;
                }
                if (iVar.i == f) {
                    rVar.c = 0;
                    rVar.d = this.d;
                    rVar.e = iVar.e + "/" + iVar.f;
                    return rVar;
                }
                this.g.b("Request url: " + this.d.d() + " success");
                if (this.a != null) {
                    this.a.a(this);
                }
                try {
                    HttpResponse a3 = new a().a(this.d.c(), this.d.d(), this.d.a(), this.d.f);
                    if (a3.getStatusLine().getStatusCode() == 200) {
                        InputStream content = a3.getEntity().getContent();
                        File file = new File(iVar.e);
                        if (!file.exists()) {
                            file.mkdirs();
                        }
                        File file2 = new File(iVar.e + "/" + iVar.f);
                        if (!file2.exists()) {
                            file2.createNewFile();
                        }
                        RandomAccessFile randomAccessFile = new RandomAccessFile(file2, "rw");
                        randomAccessFile.seek((long) iVar.g);
                        byte[] bArr = new byte[102400];
                        int i2 = iVar.g;
                        m mVar = new m();
                        mVar.b = (long) iVar.h;
                        mVar.a = (long) i2;
                        publishProgress(mVar);
                        while (true) {
                            if (this.i) {
                                break;
                            }
                            while (true == this.j) {
                                Thread.sleep(500);
                            }
                            int read = content.read(bArr);
                            if (read == -1) {
                                break;
                            }
                            randomAccessFile.write(bArr, 0, read);
                            int i3 = read + i2;
                            m mVar2 = new m();
                            mVar2.b = (long) iVar.h;
                            mVar2.a = (long) i3;
                            publishProgress(mVar2);
                            if (i3 == iVar.h) {
                                i2 = i3;
                                break;
                            }
                            Thread.sleep(500);
                            iVar.g = i3;
                            e.a(e.a((Context) this.b.get()), iVar.b, iVar);
                            i2 = i3;
                        }
                        content.close();
                        randomAccessFile.close();
                        if (!this.i) {
                            iVar.g = i2;
                            iVar.i = f;
                            e.a(e.a((Context) this.b.get()), iVar.b, iVar);
                            rVar.c = 0;
                            rVar.e = file2.getAbsolutePath();
                        } else {
                            e.b(e.a((Context) this.b.get()), iVar.b);
                            rVar.c = 2;
                            file2.delete();
                        }
                    } else {
                        rVar.c = 1;
                        rVar.b = a3.getStatusLine().getStatusCode();
                    }
                } catch (Exception e2) {
                    if (com.baidu.android.pushservice.b.a()) {
                        Log.d("HttpTask", "download file Exception:" + e2.getMessage());
                    }
                    rVar.c = -1;
                }
            }
        }
        return rVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(r rVar) {
        try {
            if (this.a != null && rVar != null) {
                if (rVar.c == 0) {
                    String str = rVar.e;
                    if (rVar.a == o.REQ_TYPE_GET_ZIP && str != null) {
                        String substring = str.substring(0, str.lastIndexOf("."));
                        File file = new File(str);
                        a(file, substring);
                        file.delete();
                        rVar.e = substring;
                    }
                    this.a.a(this, rVar);
                } else if (rVar.c == 1) {
                    this.a.a(this, new Throwable("error: response http error errorCode=" + rVar.b));
                } else if (rVar.c == 3) {
                    this.a.a(this, new Throwable("error: request error,request is null or fileName is null."));
                } else if (rVar.c == 2) {
                    this.a.b(this);
                } else if (rVar.c == -1) {
                    this.a.a(this, new Throwable("IOException"));
                }
                b(this.d);
            }
        } finally {
            b(this.d);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onProgressUpdate(m... mVarArr) {
        if (this.a != null) {
            this.a.a(this, mVarArr[0]);
        }
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        if (this.a != null) {
            this.a.b(this);
        }
        b(this.d);
        this.i = true;
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
    }
}
