package com.baidu.android.pushservice;

import android.content.Context;
import android.content.Intent;
import android.net.LocalServerSocket;
import com.baidu.android.common.logging.Log;
import com.baidu.android.common.util.DeviceId;
import com.baidu.android.common.util.Util;
import com.baidu.android.pushservice.a.a;
import com.baidu.android.pushservice.a.g;
import com.baidu.android.pushservice.a.h;
import com.baidu.android.pushservice.a.i;
import com.baidu.android.pushservice.a.j;
import com.baidu.android.pushservice.a.k;
import com.baidu.android.pushservice.a.l;
import com.baidu.android.pushservice.a.m;
import com.baidu.android.pushservice.a.n;
import com.baidu.android.pushservice.a.o;
import com.baidu.android.pushservice.a.p;
import com.baidu.android.pushservice.a.q;
import com.baidu.android.pushservice.a.r;
import com.baidu.android.pushservice.a.s;
import com.baidu.android.pushservice.a.t;
import com.baidu.android.pushservice.a.u;
import com.baidu.android.pushservice.a.v;
import com.baidu.android.pushservice.a.x;
import com.baidu.android.pushservice.b.f;
import com.baidu.android.pushservice.message.PublicMsg;
import com.baidu.android.pushservice.util.d;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class y {
    private Context a;
    private f b;
    private ExecutorService c = Executors.newFixedThreadPool(5, new d("PushService-ApiThreadPool"));

    y(Context context) {
        this.a = context;
        this.b = new f(context);
        a.a(context);
        z.a();
    }

    private String a() {
        return Util.toMd5(("com.baidu.pushservice.singelinstancev1" + DeviceId.getDeviceID(this.a)).getBytes(), false);
    }

    private void a(a aVar) {
        this.c.submit(aVar);
    }

    private void b(Intent intent) {
        k kVar = new k(intent);
        String stringExtra = intent.getStringExtra(PushConstants.EXTRA_BIND_NAME);
        int intExtra = intent.getIntExtra(PushConstants.EXTRA_BIND_STATUS, 0);
        int intExtra2 = intent.getIntExtra(PushConstants.EXTRA_PUSH_SDK_VERSION, 0);
        if (b.a()) {
            Log.d("RegistrationService", "<<< METHOD_BIND ");
            Log.d("RegistrationService", "packageName:" + kVar.e + ", bindName:" + stringExtra + ", bindStatus:" + intExtra);
            Log.d("RegistrationService", "accessToken:" + kVar.d);
            Log.d("RegistrationService", "apiKey:" + kVar.i);
        }
        a(new com.baidu.android.pushservice.a.f(kVar, this.a, intExtra, stringExtra, intExtra2));
    }

    private void c(Intent intent) {
        k kVar = new k(intent);
        if (b.a()) {
            Log.d("RegistrationService", "<<< METHOD_UNBIND ");
            Log.d("RegistrationService", "packageName:" + kVar.e);
            Log.d("RegistrationService", "accessToken:" + kVar.d);
            Log.d("RegistrationService", "apiKey:" + kVar.i);
        }
        a(new x(kVar, this.a));
    }

    private void d(Intent intent) {
        d a2;
        String stringExtra = intent.getStringExtra("package_name");
        String stringExtra2 = intent.getStringExtra(PushConstants.EXTRA_APP_ID);
        if ((stringExtra2 == null || stringExtra2.length() == 0) && (a2 = a.a(this.a).a(stringExtra)) != null) {
            stringExtra2 = a2.b;
        }
        String stringExtra3 = intent.getStringExtra(PushConstants.EXTRA_USER_ID);
        if (b.a()) {
            Log.d("RegistrationService", "<<< METHOD_UNBIND_APP ");
            Log.d("RegistrationService", "packageName:" + stringExtra);
            Log.d("RegistrationService", "appid:" + stringExtra2);
            Log.d("RegistrationService", "userid:" + stringExtra3);
        }
        k kVar = new k();
        kVar.a = "com.baidu.android.pushservice.action.UNBINDAPP";
        kVar.e = stringExtra;
        kVar.f = stringExtra2;
        kVar.g = stringExtra3;
        a(new com.baidu.android.pushservice.a.y(kVar, this.a));
    }

    private void e(Intent intent) {
        k kVar = new k(intent);
        int intExtra = intent.getIntExtra(PushConstants.EXTRA_FETCH_TYPE, 1);
        int intExtra2 = intent.getIntExtra(PushConstants.EXTRA_FETCH_NUM, 1);
        if (b.a()) {
            Log.d("RegistrationService", "<<< METHOD_FETCH ");
            Log.d("RegistrationService", "packageName:" + kVar.e);
            Log.d("RegistrationService", "accessToken:" + kVar.d);
        }
        a(new l(kVar, this.a, intExtra, intExtra2));
    }

    private void f(Intent intent) {
        k kVar = new k(intent);
        if (b.a()) {
            Log.d("RegistrationService", "<<< METHOD_COUNT ");
            Log.d("RegistrationService", "packageName:" + kVar.e);
            Log.d("RegistrationService", "accessToken:" + kVar.d);
        }
        a(new g(kVar, this.a));
    }

    private void g(Intent intent) {
        k kVar = new k(intent);
        String[] stringArrayExtra = intent.getStringArrayExtra(PushConstants.EXTRA_MSG_IDS);
        if (b.a()) {
            Log.d("RegistrationService", "<<< METHOD_DELETE ");
            Log.d("RegistrationService", "packageName:" + kVar.e);
            Log.d("RegistrationService", "accessToken:" + kVar.d);
        }
        a(new j(kVar, this.a, stringArrayExtra));
    }

    private void h(Intent intent) {
        k kVar = new k(intent);
        String stringExtra = intent.getStringExtra(PushConstants.EXTRA_GID);
        if (b.a()) {
            Log.d("RegistrationService", "<<< ACTION_GBIND ");
            Log.d("RegistrationService", "packageName:" + kVar.e + ", gid:" + stringExtra);
            Log.d("RegistrationService", "accessToken:" + kVar.d);
        }
        a(new n(kVar, this.a, stringExtra));
    }

    private void i(Intent intent) {
        k kVar = new k(intent);
        String stringExtra = intent.getStringExtra(PushConstants.EXTRA_TAGS);
        if (b.a()) {
            Log.d("RegistrationService", "<<< ACTION_GBIND ");
            Log.d("RegistrationService", "packageName:" + kVar.e + ", gid:" + stringExtra);
            Log.d("RegistrationService", "accessToken:" + kVar.d);
        }
        a(new v(kVar, this.a, stringExtra));
    }

    private void j(Intent intent) {
        k kVar = new k(intent);
        String stringExtra = intent.getStringExtra(PushConstants.EXTRA_TAGS);
        if (b.a()) {
            Log.d("RegistrationService", "<<< ACTION_GBIND ");
            Log.d("RegistrationService", "packageName:" + kVar.e + ", gid:" + stringExtra);
            Log.d("RegistrationService", "accessToken:" + kVar.d);
        }
        a(new i(kVar, this.a, stringExtra));
    }

    private void k(Intent intent) {
        k kVar = new k(intent);
        String stringExtra = intent.getStringExtra(PushConstants.EXTRA_GID);
        if (b.a()) {
            Log.d("RegistrationService", "<<< ACTION_GUNBIND ");
            Log.d("RegistrationService", "packageName:" + kVar.e + ", gid:" + stringExtra);
            Log.d("RegistrationService", "accessToken:" + kVar.d);
        }
        a(new q(kVar, this.a, stringExtra));
    }

    private void l(Intent intent) {
        k kVar = new k(intent);
        String stringExtra = intent.getStringExtra(PushConstants.EXTRA_GID);
        if (b.a()) {
            Log.d("RegistrationService", "<<< METHOD_GINFO ");
            Log.d("RegistrationService", "packageName:" + kVar.e + ", gid:" + stringExtra);
            Log.d("RegistrationService", "accessToken:" + kVar.d);
        }
        a(new o(kVar, this.a, stringExtra));
    }

    private void m(Intent intent) {
        k kVar = new k(intent);
        if (b.a()) {
            Log.d("RegistrationService", "<<< METHOD_GLIST ");
            Log.d("RegistrationService", "packageName:" + kVar.e);
            Log.d("RegistrationService", "accessToken:" + kVar.d);
        }
        a(new p(kVar, this.a));
    }

    private void n(Intent intent) {
        k kVar = new k(intent);
        String stringExtra = intent.getStringExtra(PushConstants.EXTRA_GID);
        int intExtra = intent.getIntExtra(PushConstants.EXTRA_GROUP_FETCH_TYPE, 1);
        int intExtra2 = intent.getIntExtra(PushConstants.EXTRA_GROUP_FETCH_NUM, 1);
        if (b.a()) {
            Log.d("RegistrationService", "<<< METHOD_FETCHGMSG ");
            Log.d("RegistrationService", "packageName:" + kVar.e);
            Log.d("RegistrationService", "accessToken:" + kVar.d);
            Log.d("RegistrationService", "gid:" + stringExtra);
            Log.d("RegistrationService", "fetchType:" + intExtra);
            Log.d("RegistrationService", "fetchNum:" + intExtra2);
        }
        a(new m(kVar, this.a, stringExtra, intExtra, intExtra2));
    }

    private void o(Intent intent) {
        k kVar = new k(intent);
        String stringExtra = intent.getStringExtra(PushConstants.EXTRA_GID);
        if (b.a()) {
            Log.d("RegistrationService", "<<< METHOD_COUNTGMSG ");
            Log.d("RegistrationService", "packageName:" + kVar.e);
            Log.d("RegistrationService", "accessToken:" + kVar.d);
            Log.d("RegistrationService", "gid:" + stringExtra);
        }
        a(new h(kVar, this.a, stringExtra));
    }

    private void p(Intent intent) {
        k kVar = new k(intent);
        if (b.a()) {
            Log.d("RegistrationService", "<<< METHOD_ONLINE ");
            Log.d("RegistrationService", "packageName:" + kVar.e);
            Log.d("RegistrationService", "accessToken:" + kVar.d);
        }
        a(new r(kVar, this.a));
    }

    private void q(Intent intent) {
        k kVar = new k(intent);
        if (b.a()) {
            Log.d("RegistrationService", "<<< METHOD_SEND ");
            Log.d("RegistrationService", "packageName:" + kVar.e);
            Log.d("RegistrationService", "accessToken:" + kVar.d);
        }
        a(new s(kVar, this.a, intent.getStringExtra(PushConstants.EXTRA_MSG)));
    }

    private void r(Intent intent) {
        k kVar = new k(intent);
        if (b.a()) {
            Log.d("RegistrationService", "<<< METHOD_SEND_MSG_TO_SERVER ");
            Log.d("RegistrationService", "packageName:" + kVar.e);
            Log.d("RegistrationService", "accessToken:" + kVar.d);
        }
        a(new t(kVar, this.a, intent.getStringExtra(PushConstants.EXTRA_APP_ID), intent.getStringExtra(PushConstants.EXTRA_CB_URL), intent.getStringExtra(PushConstants.EXTRA_MSG)));
    }

    private void s(Intent intent) {
        k kVar = new k(intent);
        Log.d("RegistrationService", "<<< METHOD_SEND_MSG_TO_USER ");
        Log.d("RegistrationService", "packageName:" + kVar.e);
        Log.d("RegistrationService", "accessToken:" + kVar.d);
        a(new u(kVar, this.a, intent.getStringExtra(PushConstants.EXTRA_APP_ID), intent.getStringExtra(PushConstants.EXTRA_USER_ID), intent.getStringExtra(PushConstants.EXTRA_MSG_KEY), intent.getStringExtra(PushConstants.EXTRA_MSG)));
    }

    private void t(Intent intent) {
        this.b.a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.android.pushservice.z.a(android.content.Context, boolean):void
     arg types: [android.content.Context, int]
     candidates:
      com.baidu.android.pushservice.z.a(java.lang.String, java.lang.String):void
      com.baidu.android.pushservice.z.a(android.content.Context, boolean):void */
    public boolean a(Intent intent) {
        boolean z = false;
        if (intent == null) {
            return false;
        }
        if (b.a()) {
            Log.d("RegistrationService", "RegistrationSerice handleIntent : " + intent);
        }
        String action = intent.getAction();
        if ("com.baidu.pushservice.action.publicmsg.CLICK_V2".equals(action) || "com.baidu.pushservice.action.publicmsg.DELETE_V2".equals(action)) {
            ((PublicMsg) intent.getParcelableExtra("public_msg")).a(this.a, action, intent.getData().getHost());
            return true;
        } else if ("com.baidu.android.pushservice.action.privatenotification.CLICK".equals(action) || "com.baidu.android.pushservice.action.privatenotification.DELETE".equals(action)) {
            ((PublicMsg) intent.getParcelableExtra("public_msg")).a(this.a, action, intent.getStringExtra("msg_id"), intent.getStringExtra(PushConstants.EXTRA_APP_ID));
            return true;
        } else if ("com.baidu.android.pushservice.action.media.CLICK".equals(action) || "com.baidu.android.pushservice.action.media.DELETE".equals(action)) {
            ((PublicMsg) intent.getParcelableExtra("public_msg")).a(this.a, action);
            return true;
        } else if ("com.baidu.pushservice.action.TOKEN".equals(action)) {
            if (b.a()) {
                Log.d("RegistrationService", "<<< ACTION_TOKEN ");
            }
            if (!z.a().d()) {
                z.a().a(this.a, true);
            }
            return true;
        } else if (!PushConstants.ACTION_METHOD.equals(action)) {
            return false;
        } else {
            String stringExtra = intent.getStringExtra("method_version");
            LocalServerSocket localServerSocket = null;
            if (stringExtra != null && !"V2".equals(stringExtra) && stringExtra.equals("V1") && !com.baidu.android.pushservice.util.n.r(this.a)) {
                try {
                    localServerSocket = new LocalServerSocket(a());
                } catch (Exception e) {
                    if (b.a()) {
                        Log.d("RegistrationService", "---V1 Socket Adress (" + a() + ") in use --- @ " + this.a.getPackageName());
                    }
                }
                if (localServerSocket == null) {
                    Intent b2 = com.baidu.android.pushservice.util.n.b(this.a, "com.baidu.pushservice.action.start.SERVICEINFO");
                    Intent b3 = com.baidu.android.pushservice.util.n.b(this.a, "com.baidu.moplus.action.start.SERVICEINFO");
                    if (b2 == null && b3 == null) {
                        return false;
                    }
                    if (b2 != null) {
                        String stringExtra2 = b2.getStringExtra("method_version");
                        if ("V1".equals(stringExtra2)) {
                            if (b.a()) {
                                Log.d("RegistrationService", "Method Version : " + stringExtra2);
                            }
                            return false;
                        }
                    }
                    if (b3 != null) {
                        String stringExtra3 = b3.getStringExtra("method_version");
                        if ("V1".equals(stringExtra3)) {
                            if (b.a()) {
                                Log.d("RegistrationService", "Method Version : " + stringExtra3);
                            }
                            return false;
                        }
                    }
                }
            }
            String stringExtra4 = intent.getStringExtra(PushConstants.EXTRA_METHOD);
            if (PushConstants.METHOD_BIND.equals(stringExtra4)) {
                b(intent);
                z = true;
            } else if (PushConstants.METHOD_UNBIND.equals(stringExtra4)) {
                c(intent);
                z = true;
            } else if ("com.baidu.android.pushservice.action.UNBINDAPP".equals(stringExtra4)) {
                d(intent);
                z = true;
            } else if (PushConstants.METHOD_FETCH.equals(stringExtra4)) {
                e(intent);
                z = true;
            } else if (PushConstants.METHOD_COUNT.equals(stringExtra4)) {
                f(intent);
                z = true;
            } else if (PushConstants.METHOD_DELETE.equals(stringExtra4)) {
                g(intent);
                z = true;
            } else if (PushConstants.METHOD_GBIND.equals(stringExtra4)) {
                h(intent);
                z = true;
            } else if (PushConstants.METHOD_SET_TAGS.equals(stringExtra4)) {
                i(intent);
                z = true;
            } else if (PushConstants.METHOD_DEL_TAGS.equals(stringExtra4)) {
                j(intent);
                z = true;
            } else if (PushConstants.METHOD_GUNBIND.equals(stringExtra4)) {
                k(intent);
                z = true;
            } else if (PushConstants.METHOD_GINFO.equals(stringExtra4)) {
                l(intent);
                z = true;
            } else if (PushConstants.METHOD_GLIST.equals(stringExtra4)) {
                m(intent);
                z = true;
            } else if (PushConstants.METHOD_FETCHGMSG.equals(stringExtra4)) {
                n(intent);
                z = true;
            } else if (PushConstants.METHOD_COUNTGMSG.equals(stringExtra4)) {
                o(intent);
                z = true;
            } else if (PushConstants.METHOD_ONLINE.equals(stringExtra4)) {
                p(intent);
                z = true;
            } else if (PushConstants.METHOD_SEND.equals(stringExtra4)) {
                q(intent);
                z = true;
            } else if ("com.baidu.android.pushservice.action.SEND_APPSTAT".equals(stringExtra4)) {
                t(intent);
                z = true;
            } else if (PushConstants.METHOD_SEND_MSG_TO_SERVER.equals(stringExtra4)) {
                r(intent);
                z = true;
            } else if (PushConstants.METHOD_SEND_MSG_TO_USER.equals(stringExtra4)) {
                s(intent);
                z = true;
            }
            if (localServerSocket != null) {
                try {
                    localServerSocket.close();
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
            }
            return z;
        }
    }
}
