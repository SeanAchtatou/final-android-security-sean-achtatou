package com.baidu.android.pushservice;

import com.baidu.android.common.logging.Log;
import com.baidu.android.pushservice.message.c;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

class f implements Runnable {
    final /* synthetic */ e a;

    f(e eVar) {
        this.a = eVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.android.pushservice.e.a(com.baidu.android.pushservice.e, boolean):boolean
     arg types: [com.baidu.android.pushservice.e, int]
     candidates:
      com.baidu.android.pushservice.e.a(com.baidu.android.pushservice.e, int):int
      com.baidu.android.pushservice.e.a(com.baidu.android.pushservice.e, com.baidu.android.pushservice.i):com.baidu.android.pushservice.i
      com.baidu.android.pushservice.e.a(com.baidu.android.pushservice.e, com.baidu.android.pushservice.j):com.baidu.android.pushservice.j
      com.baidu.android.pushservice.e.a(com.baidu.android.pushservice.e, java.io.InputStream):java.io.InputStream
      com.baidu.android.pushservice.e.a(com.baidu.android.pushservice.e, java.io.OutputStream):java.io.OutputStream
      com.baidu.android.pushservice.e.a(com.baidu.android.pushservice.e, java.net.Socket):java.net.Socket
      com.baidu.android.pushservice.e.a(com.baidu.android.pushservice.e, boolean):boolean */
    public void run() {
        try {
            if (b.a()) {
                Log.i("PushConnection", "connect begin ： Host=" + this.a.c + ", Port=" + this.a.d);
            }
            Socket unused = this.a.i = new Socket(this.a.c, this.a.d);
            InputStream unused2 = this.a.j = this.a.i.getInputStream();
            OutputStream unused3 = this.a.k = this.a.i.getOutputStream();
            this.a.b = new c(this.a.p.getApplicationContext(), this.a, this.a.j, this.a.k);
            if (this.a.n != null) {
                this.a.n.interrupt();
            }
            i unused4 = this.a.n = new i(this.a);
            this.a.n.start();
            if (this.a.m != null) {
                this.a.m.interrupt();
            }
            j unused5 = this.a.m = new j(this.a, this.a.k);
            this.a.m.start();
            boolean unused6 = this.a.e = true;
            this.a.a(true);
            this.a.b.b();
            if (b.a()) {
                Log.i("PushConnection", "connect success");
            }
        } catch (Throwable th) {
            Log.e("PushConnection", "Connecting exception: " + th);
            this.a.f();
        }
        boolean unused7 = this.a.f = false;
    }
}
