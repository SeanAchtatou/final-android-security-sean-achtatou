package com.baidu.android.pushservice.richmedia;

import android.content.DialogInterface;
import android.content.Intent;
import com.baidu.android.pushservice.util.e;
import com.baidu.android.pushservice.util.i;
import java.io.File;
import java.util.Map;

class g implements DialogInterface.OnClickListener {
    final /* synthetic */ long a;
    final /* synthetic */ e b;

    g(e eVar, long j) {
        this.b = eVar;
        this.a = j;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        String str = (String) ((Map) this.b.a.a.get((int) this.a)).get(MediaListActivity.r);
        i a2 = e.a(e.a(this.b.a), str);
        if (a2 != null) {
            new File(a2.e).delete();
        }
        e.b(e.a(this.b.a), str);
        Intent intent = new Intent();
        intent.setClass(this.b.a, MediaListActivity.class);
        this.b.a.startActivity(intent);
    }
}
