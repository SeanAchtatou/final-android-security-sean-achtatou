package com.baidu.android.pushservice.b;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import android.util.Log;
import com.baidu.android.common.net.ConnectManager;
import com.baidu.android.pushservice.PushConstants;
import com.baidu.android.pushservice.b;
import com.baidu.android.pushservice.util.e;
import com.baidu.android.pushservice.util.j;
import com.baidu.android.pushservice.z;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class f {
    /* access modifiers changed from: private */
    public Context a;
    private c b;

    public f(Context context) {
        this.a = context;
        this.b = c.a(context);
    }

    private void a(Context context) {
        String b2 = z.a().b();
        String c = z.a().c();
        if (!TextUtils.isEmpty(b2) && !TextUtils.isEmpty(c)) {
            SQLiteDatabase a2 = e.a(context);
            if (a2 != null) {
                List a3 = e.a(a2);
                if (ConnectManager.isNetworkConnected(this.a) && a3.size() > 0) {
                    JSONArray jSONArray = new JSONArray();
                    int i = 0;
                    while (true) {
                        int i2 = i;
                        if (i2 < a3.size()) {
                            JSONObject jSONObject = new JSONObject();
                            try {
                                jSONObject.put(PushConstants.EXTRA_MSGID, ((j) a3.get(i2)).b);
                                jSONObject.put("appid", ((j) a3.get(i2)).c);
                                jSONObject.put("resultCode", ((j) a3.get(i2)).d);
                                jSONArray.put(jSONObject);
                            } catch (JSONException e) {
                            }
                            i = i2 + 1;
                        } else {
                            Thread thread = new Thread(new g(this, context, b2, c, jSONArray.toString()));
                            thread.setName("PushService-feedback");
                            thread.start();
                            return;
                        }
                    }
                }
            }
        } else if (b.a(this.a)) {
            Log.e("StatisticsInfoManager", "Fail Send Msg result info. Token invalid!");
        }
    }

    public void a() {
        a(this.a);
        if (this.b == null) {
            this.b = c.a(this.a);
        }
        this.b.e();
    }
}
