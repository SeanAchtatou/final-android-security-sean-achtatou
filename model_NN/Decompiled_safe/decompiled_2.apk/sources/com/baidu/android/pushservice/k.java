package com.baidu.android.pushservice;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.baidu.android.common.logging.Log;

class k extends BroadcastReceiver {
    final /* synthetic */ PushSDK a;

    k(PushSDK pushSDK) {
        this.a = pushSDK;
    }

    public void onReceive(Context context, Intent intent) {
        if (b.a()) {
            Log.d(PushSDK.TAG, "ACTION_SCREEN_ON");
        }
        if (!this.a.tryConnect()) {
            b.a(PushSDK.mContext, new Intent("com.baidu.pushservice.action.STOP"));
        }
    }
}
