package com.baidu.android.pushservice;

import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.LocalServerSocket;
import android.os.Handler;
import android.os.SystemClock;
import com.baidu.android.common.logging.Log;
import com.baidu.android.common.net.ConnectManager;
import com.baidu.android.pushservice.util.NoProGuard;
import com.baidu.android.pushservice.util.e;
import com.baidu.android.pushservice.util.n;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PushSDK implements NoProGuard {
    private static int ALARM_TIMEOUT = 600000;
    public static final String LOCAL_SOCKET_ADDRESS = "com.baidu.pushservice.singelinstance";
    /* access modifiers changed from: private */
    public static String TAG = "PushSDK";
    /* access modifiers changed from: private */
    public static Context mContext;
    private static Handler mHandler;
    private static LocalServerSocket mLocalSocket;
    /* access modifiers changed from: private */
    public static Object mPushConnLock = new Object();
    public static e mPushConnection;
    private static PushSDK mPushSDK = null;
    private Runnable mConnectRunnable = new n(this);
    private Boolean mIsAlive = false;
    private Runnable mRegisterRunnable = new m(this);
    private y mRegistrationService;
    private final BroadcastReceiver mScreenOnReceiver = new k(this);
    private Runnable mStartRunnable = new l(this);

    private PushSDK(Context context) {
        mHandler = new Handler();
        mContext = context.getApplicationContext();
        PushSettings.a = context.getApplicationContext();
    }

    private void cancelAlarmRepeat() {
        Intent intent = new Intent();
        intent.putExtra("AlarmAlert", "OK");
        try {
            Class.forName("com.baidu.android.moplus.MoPlusService");
            intent.setClassName(mContext.getPackageName(), "com.baidu.android.moplus.MoPlusService");
        } catch (ClassNotFoundException e) {
            intent.setClass(mContext, PushService.class);
        }
        ((AlarmManager) mContext.getSystemService("alarm")).cancel(PendingIntent.getService(mContext, 0, intent, 268435456));
    }

    public static void destory() {
        if (mPushSDK != null) {
            mPushSDK.doDestory();
        }
    }

    private void doDestory() {
        if (b.a()) {
            Log.d(TAG, "destory");
        }
        synchronized (this.mIsAlive) {
            try {
                if (mLocalSocket != null) {
                    mLocalSocket.close();
                    mLocalSocket = null;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (mPushConnection != null) {
                synchronized (mPushConnLock) {
                    mPushConnection.c();
                    mPushConnection = null;
                }
            }
            try {
                mContext.unregisterReceiver(this.mScreenOnReceiver);
            } catch (IllegalArgumentException e2) {
                if (b.a()) {
                    Log.i(TAG, e2.getMessage());
                }
            }
            e.a();
            this.mIsAlive = false;
            mPushSDK = null;
        }
    }

    public static synchronized PushSDK getInstantce(Context context) {
        PushSDK pushSDK;
        synchronized (PushSDK.class) {
            if (mPushSDK == null) {
                mPushSDK = new PushSDK(context);
            }
            pushSDK = mPushSDK;
        }
        return pushSDK;
    }

    public static boolean isAlive() {
        if (mPushSDK != null) {
            return mPushSDK.mIsAlive.booleanValue();
        }
        return false;
    }

    private void newPushConnection() {
        synchronized (mPushConnLock) {
            mPushConnection = e.a(mContext);
        }
    }

    private void scheduleConnect() {
        mHandler.removeCallbacks(this.mConnectRunnable);
        mHandler.postDelayed(this.mConnectRunnable, 1000);
    }

    private void scheduleRegister() {
        mHandler.removeCallbacks(this.mRegisterRunnable);
        mHandler.postDelayed(this.mRegisterRunnable, 500);
    }

    private void setAlarmRepeat() {
        cancelAlarmRepeat();
        Intent intent = new Intent();
        intent.putExtra("AlarmAlert", "OK");
        try {
            Class.forName("com.baidu.android.moplus.MoPlusService");
            intent.setClassName(mContext.getPackageName(), "com.baidu.android.moplus.MoPlusService");
        } catch (ClassNotFoundException e) {
            intent.setClass(mContext, PushService.class);
        }
        PendingIntent service = PendingIntent.getService(mContext.getApplicationContext(), 0, intent, 268435456);
        long elapsedRealtime = SystemClock.elapsedRealtime();
        AlarmManager alarmManager = (AlarmManager) mContext.getSystemService("alarm");
        alarmManager.cancel(service);
        alarmManager.setRepeating(0, elapsedRealtime, (long) ALARM_TIMEOUT, service);
    }

    private boolean shouldReConnect(Context context) {
        SharedPreferences sharedPreferences;
        if (n.n(context.getApplicationContext()).size() <= 1) {
            if (b.a()) {
                Log.i(TAG, "Only one push app : " + context.getPackageName());
            }
            return false;
        }
        List<ActivityManager.RunningServiceInfo> runningServices = ((ActivityManager) context.getSystemService("activity")).getRunningServices(1000);
        ArrayList arrayList = new ArrayList();
        for (ActivityManager.RunningServiceInfo next : runningServices) {
            if (PushService.class.getName().equals(next.service.getClassName())) {
                arrayList.add(next.service.getPackageName());
            }
        }
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            String str = (String) it.next();
            if (!context.getPackageName().equals(str)) {
                try {
                    sharedPreferences = context.createPackageContext(str, 2).getSharedPreferences(str + ".push_sync", 1);
                } catch (PackageManager.NameNotFoundException e) {
                    if (b.a()) {
                        Log.e(TAG, e.getMessage());
                    }
                    sharedPreferences = null;
                }
                if (sharedPreferences == null) {
                    if (b.a()) {
                        Log.w(TAG, "App:" + str + " doesn't init Version!");
                    }
                } else if (sharedPreferences.getInt("version2", 0) == 0) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean shouldStopSelf(Context context) {
        boolean z;
        SharedPreferences sharedPreferences;
        List n = n.n(context.getApplicationContext());
        if (n.size() > 1) {
            long j = context.getSharedPreferences(context.getPackageName() + ".push_sync", 1).getLong("priority2", 0);
            Iterator it = n.iterator();
            while (true) {
                if (!it.hasNext()) {
                    z = false;
                    break;
                }
                String str = ((ResolveInfo) it.next()).activityInfo.packageName;
                if (!context.getPackageName().equals(str)) {
                    try {
                        sharedPreferences = context.createPackageContext(str, 2).getSharedPreferences(str + ".push_sync", 1);
                    } catch (PackageManager.NameNotFoundException e) {
                        if (b.a()) {
                            Log.e(TAG, e.getMessage());
                        }
                        sharedPreferences = null;
                    }
                    if (sharedPreferences != null) {
                        long j2 = sharedPreferences.getLong("priority2", 0);
                        if (j2 > j) {
                            if (b.a()) {
                                Log.d(TAG, "shouldStopSelf-------localPriority = " + j + ";  other packageName = " + str + "--priority =" + j2);
                            }
                            z = true;
                        }
                    } else if (b.a()) {
                        Log.w(TAG, "App:" + str + " doesn't init Version!");
                    }
                }
            }
            return z;
        } else if (!b.a()) {
            return false;
        } else {
            Log.i(TAG, "Only one push app : " + context.getPackageName());
            return false;
        }
    }

    /* access modifiers changed from: private */
    public boolean tryConnect() {
        if (b.a() && mPushConnection != null) {
            Log.i(TAG, "tryConnect mPushConnection.isConnected() :" + mPushConnection.a());
        }
        boolean isNetworkConnected = ConnectManager.isNetworkConnected(mContext);
        if (b.a()) {
            Log.i(TAG, "tryConnect networkConnected :" + isNetworkConnected);
        }
        if (!isNetworkConnected || mPushConnection == null) {
            return false;
        }
        if (mPushConnection.a()) {
            mPushConnection.d();
        } else if (!z.a().d()) {
            if (b.a()) {
                Log.i(TAG, "host is not available, start NETWORK REGISTER SERVICE .");
            }
            scheduleRegister();
        } else {
            scheduleConnect();
        }
        return true;
    }

    public boolean handleOnStart(Intent intent) {
        if (b.a()) {
            Log.d(TAG, "handleOnStart intent action = " + (intent != null ? intent.getAction() : ""));
        }
        synchronized (this.mIsAlive) {
            if (!this.mIsAlive.booleanValue()) {
                return false;
            }
            mHandler.removeCallbacks(this.mStartRunnable);
            if (b.a()) {
                Log.i(TAG, "-- handleOnStart -- " + intent);
            }
            if (mLocalSocket == null) {
                return false;
            }
            if (intent != null && "com.baidu.pushservice.action.STOP".equals(intent.getAction())) {
                return false;
            }
            if (intent != null) {
                if ("pushservice_restart".equals(intent.getStringExtra(PushConstants.EXTRA_METHOD))) {
                    n.a(mContext, 1000);
                    return false;
                } else if (this.mRegistrationService.a(intent)) {
                    if (b.a()) {
                        Log.i(TAG, "-- handleOnStart -- intent handled  by mRegistrationService ");
                    }
                    return true;
                }
            }
            boolean tryConnect = tryConnect();
            return tryConnect;
        }
    }

    public boolean initPushSDK() {
        if (b.a()) {
            Log.d(TAG, "Create PushSDK from : " + mContext.getPackageName());
        }
        n.i(mContext.getApplicationContext());
        mContext.getApplicationContext().registerReceiver(this.mScreenOnReceiver, new IntentFilter("android.intent.action.SCREEN_ON"));
        if (!n.c(mContext.getApplicationContext()) && !shouldStopSelf(mContext)) {
            synchronized (this.mIsAlive) {
                if (mLocalSocket == null) {
                    try {
                        mLocalSocket = new LocalServerSocket(n.p(mContext));
                    } catch (Exception e) {
                        if (b.a()) {
                            Log.d(TAG, "--- Socket Adress (" + n.p(mContext) + ") in use --- @ " + mContext.getPackageName());
                        }
                    }
                }
                if (mLocalSocket == null) {
                    return false;
                }
                newPushConnection();
                this.mRegistrationService = new y(mContext);
                setAlarmRepeat();
                mHandler.postDelayed(this.mStartRunnable, 500);
                this.mIsAlive = true;
                return true;
            }
        } else if (!b.a()) {
            return false;
        } else {
            Log.d(TAG, "onCreate shouldStopSelf");
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public void sendRequestTokenIntent() {
        if (b.a()) {
            Log.d(TAG, ">> sendRequestTokenIntent");
        }
        b.a(mContext, new Intent("com.baidu.pushservice.action.TOKEN"));
    }
}
