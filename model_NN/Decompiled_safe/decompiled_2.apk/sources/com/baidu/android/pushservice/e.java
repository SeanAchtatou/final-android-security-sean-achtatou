package com.baidu.android.pushservice;

import android.content.Context;
import android.os.Handler;
import android.provider.Settings;
import com.baidu.android.common.logging.Log;
import com.baidu.android.pushservice.message.a;
import com.baidu.android.pushservice.message.b;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.HashSet;
import java.util.LinkedList;

public final class e {
    private static volatile e q;
    Handler a = new Handler();
    a b;
    /* access modifiers changed from: private */
    public final String c = x.b;
    /* access modifiers changed from: private */
    public final int d = x.c;
    /* access modifiers changed from: private */
    public boolean e = false;
    /* access modifiers changed from: private */
    public boolean f = false;
    /* access modifiers changed from: private */
    public boolean g = false;
    private boolean h = false;
    /* access modifiers changed from: private */
    public Socket i;
    /* access modifiers changed from: private */
    public InputStream j;
    /* access modifiers changed from: private */
    public OutputStream k;
    /* access modifiers changed from: private */
    public LinkedList l = new LinkedList();
    /* access modifiers changed from: private */
    public j m;
    /* access modifiers changed from: private */
    public i n;
    /* access modifiers changed from: private */
    public int o = 0;
    /* access modifiers changed from: private */
    public Context p;
    private HashSet r = new HashSet();
    private Runnable s = new g(this);
    /* access modifiers changed from: private */
    public Runnable t = new h(this);
    private long u = 0;

    private e(Context context) {
        this.p = context;
    }

    public static e a(Context context) {
        if (q == null) {
            q = new e(context);
        }
        return q;
    }

    /* access modifiers changed from: private */
    public void e() {
        if (this.e || this.f) {
            if (b.a()) {
                Log.i("PushConnection", "connect return. mConnected:" + this.e + " mConnectting:" + this.f);
            }
        } else if (!z.a().d()) {
            PushSDK.getInstantce(this.p).sendRequestTokenIntent();
        } else {
            this.f = true;
            this.g = false;
            Thread thread = new Thread(new f(this));
            thread.setName("PushService-PushService-connect");
            thread.start();
        }
    }

    /* access modifiers changed from: private */
    public void f() {
        this.g = true;
        g();
        if (!this.h) {
            this.o++;
            if (this.o < 5) {
                int i2 = (1 << (this.o - 1)) * 5 * 1000;
                this.a.postDelayed(this.s, (long) i2);
                if (b.a()) {
                    Log.i("PushConnection", "schedule retry-- retry times: " + this.o + "time delay: " + i2);
                }
            }
        } else if (b.a()) {
            Log.i("PushConnection", "disconnectedByPeer, mStoped == true, return.");
        }
    }

    private void g() {
        if (b.a()) {
            Log.i("PushConnection", "disconnected");
        }
        this.a.removeCallbacks(this.t);
        this.g = true;
        this.e = false;
        a(false);
        synchronized (this.l) {
            this.l.notifyAll();
        }
        try {
            if (this.i != null) {
                this.i.close();
            }
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        try {
            if (this.j != null) {
                this.j.close();
            }
        } catch (IOException e3) {
            e3.printStackTrace();
        }
        try {
            if (this.k != null) {
                this.k.close();
            }
        } catch (IOException e4) {
            e4.printStackTrace();
        }
        if (this.b != null) {
            this.b.c();
        }
    }

    public void a(b bVar) {
        synchronized (this.l) {
            this.l.add(bVar);
            this.l.notify();
        }
    }

    public void a(boolean z) {
        Log.d("PushConnection", "setConnectState: " + z);
        if (this.p == null) {
            Log.e("PushConnection", "setConnectState, mContext == null");
            return;
        }
        int i2 = 0;
        if (z) {
            i2 = 1;
        }
        Settings.System.putInt(this.p.getContentResolver(), "com.baidu.pushservice.PushSettings.connect_state", i2);
    }

    public boolean a() {
        return this.e;
    }

    public boolean a(String str) {
        boolean z = false;
        if (this.r.contains(str)) {
            this.r.remove(str);
            z = true;
        }
        if (this.r.size() >= 100) {
            this.r.clear();
        }
        this.r.add(str);
        return z;
    }

    public void b() {
        this.o = 0;
        this.h = false;
        e();
    }

    public void c() {
        if (b.a()) {
            Log.i("PushConnection", "---stop()---");
        }
        this.g = true;
        this.h = true;
        this.a.removeCallbacks(this.s);
        g();
    }

    public void d() {
        if (this.b != null) {
            long currentTimeMillis = System.currentTimeMillis();
            if (currentTimeMillis - this.u > 60000) {
                this.b.d();
                this.u = currentTimeMillis;
                if (b.a()) {
                    Log.i("PushConnection", "sendHeartbeatMessage");
                }
            } else if (b.a()) {
                Log.i("PushConnection", "sendHeartbeatMessage ingnored， because too frequent.");
            }
        }
    }
}
