package com.baidu.android.pushservice;

import android.content.Context;
import android.text.TextUtils;
import com.baidu.android.pushservice.a.w;

public final class z {
    private static z a;
    private String b;
    private String c;
    private Thread d;

    private z() {
        this.b = null;
        this.c = null;
        this.d = null;
        this.c = PushSettings.b();
        this.b = PushSettings.a();
    }

    public static z a() {
        if (a == null) {
            a = new z();
        }
        return a;
    }

    public void a(Context context, boolean z) {
        if (this.d == null || !this.d.isAlive()) {
            w wVar = new w(context);
            if (!z) {
                wVar.a(0);
            }
            this.d = new Thread(wVar);
            this.d.start();
        }
    }

    public synchronized void a(String str, String str2) {
        this.b = str;
        this.c = str2;
        PushSettings.a(str);
        PushSettings.b(str2);
    }

    public String b() {
        return this.b;
    }

    public String c() {
        return this.c;
    }

    public boolean d() {
        return !TextUtils.isEmpty(this.b) && !TextUtils.isEmpty(this.c);
    }
}
