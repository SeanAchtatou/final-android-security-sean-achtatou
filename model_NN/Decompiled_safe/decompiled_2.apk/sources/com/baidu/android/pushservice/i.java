package com.baidu.android.pushservice;

import com.baidu.android.common.logging.Log;
import com.baidu.android.pushservice.message.b;
import com.baidu.android.pushservice.message.d;

class i extends Thread {
    final /* synthetic */ e a;

    i(e eVar) {
        this.a = eVar;
        setName("PushService-PushConnection-readThread");
    }

    public void run() {
        while (!this.a.g) {
            try {
                b a2 = this.a.b.a();
                this.a.a.removeCallbacks(this.a.t);
                if (a2 != null) {
                    if (b.a()) {
                        Log.d("PushConnection", "ReadThread receive msg :" + a2.toString());
                    }
                    try {
                        this.a.b.b(a2);
                        int unused = this.a.o = 0;
                    } catch (d e) {
                        Log.e("PushConnection", "handleMessage exception.");
                        Log.e("PushConnection", e);
                        this.a.f();
                    }
                }
            } catch (Exception e2) {
                Log.e("PushConnection", "ReadThread exception: " + e2);
                this.a.f();
            }
        }
    }
}
