package com.baidu.android.pushservice.a;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.baidu.android.common.logging.Log;
import com.baidu.android.common.net.ConnectManager;
import com.baidu.android.common.net.ProxyHttpClient;
import com.baidu.android.pushservice.PushConstants;
import com.baidu.android.pushservice.b;
import com.baidu.android.pushservice.x;
import com.baidu.android.pushservice.z;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.HttpResponse;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class a implements Runnable {
    protected Context a;
    protected k b;
    protected String c;
    protected boolean d = true;

    public a(k kVar, Context context) {
        this.b = kVar;
        this.a = context.getApplicationContext();
        this.c = x.g;
    }

    private void b(int i, byte[] bArr) {
        Intent intent = new Intent("com.baidu.android.pushservice.action.internal.RECEIVE");
        intent.putExtra(PushConstants.EXTRA_METHOD, this.b.a);
        intent.putExtra(PushConstants.EXTRA_ERROR_CODE, i);
        intent.putExtra(PushConstants.EXTRA_CONTENT, bArr);
        intent.putExtra("appid", this.b.f);
        intent.setFlags(32);
        a(intent);
        if (b.a()) {
            Log.d("BaseBaseApiProcessor", "> sendInternalMethodResult  ,method:" + this.b.a + " ,errorCode : " + i + " ,content : " + new String(bArr));
        }
        this.a.sendBroadcast(intent);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.android.pushservice.z.a(android.content.Context, boolean):void
     arg types: [android.content.Context, int]
     candidates:
      com.baidu.android.pushservice.z.a(java.lang.String, java.lang.String):void
      com.baidu.android.pushservice.z.a(android.content.Context, boolean):void */
    /* access modifiers changed from: protected */
    public void a() {
        if (this.b != null && !TextUtils.isEmpty(this.b.e) && !TextUtils.isEmpty(this.b.a)) {
            if (!ConnectManager.isNetworkConnected(this.a)) {
                if (b.a()) {
                    Log.e("BaseBaseApiProcessor", "Network is not useful!");
                }
                a((int) PushConstants.ERROR_NETWORK_ERROR);
                b.a(this.a);
                if (b.a()) {
                    Log.i("BaseBaseApiProcessor", "startPushService BaseApiProcess");
                    return;
                }
                return;
            }
            z a2 = z.a();
            synchronized (a2) {
                if (this.d && !z.a().d()) {
                    a2.a(this.a, false);
                    this.d = false;
                    try {
                        a2.wait();
                    } catch (InterruptedException e) {
                        if (b.a()) {
                            Log.e("BaseBaseApiProcessor", e.getMessage());
                        }
                    }
                }
            }
            if (!z.a().d()) {
                a((int) PushConstants.ERROR_SERVICE_NOT_AVAILABLE);
                return;
            }
            boolean b2 = b();
            if (b.a()) {
                Log.i("BaseBaseApiProcessor", "netWorkConnect connectResult: " + b2);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(int i) {
        a(i, PushConstants.getErrorMsg(i).getBytes());
    }

    /* access modifiers changed from: protected */
    public void a(int i, byte[] bArr) {
        if (TextUtils.isEmpty(this.b.b) || !this.b.b.equals("internal")) {
            Intent intent = new Intent(PushConstants.ACTION_RECEIVE);
            intent.putExtra(PushConstants.EXTRA_METHOD, this.b.a);
            intent.putExtra(PushConstants.EXTRA_ERROR_CODE, i);
            intent.putExtra(PushConstants.EXTRA_CONTENT, bArr);
            intent.setFlags(32);
            a(intent);
            if (!TextUtils.isEmpty(this.b.e)) {
                intent.setPackage(this.b.e);
                if (b.a()) {
                    Log.d("BaseBaseApiProcessor", "> sendResult to " + this.b.e + " ,method:" + this.b.a + " ,errorCode : " + i + " ,content : " + new String(bArr));
                }
                this.a.sendBroadcast(intent);
                return;
            }
            return;
        }
        b(i, bArr);
    }

    /* access modifiers changed from: protected */
    public void a(Intent intent) {
    }

    /* access modifiers changed from: protected */
    public void a(String str) {
        if (str != null) {
            if (!str.startsWith("{\"")) {
                str = str.substring(str.indexOf("{\""));
            }
            try {
                JSONObject jSONObject = new JSONObject(str);
                int i = jSONObject.getInt("error_code");
                String string = jSONObject.getString(PushConstants.EXTRA_ERROR_CODE);
                String string2 = jSONObject.getString("request_id");
                JSONObject jSONObject2 = new JSONObject();
                jSONObject2.put(PushConstants.EXTRA_ERROR_CODE, string);
                jSONObject2.put("requestId", string2);
                a(i, jSONObject2.toString().getBytes());
            } catch (JSONException e) {
                Log.e("BaseBaseApiProcessor", e.getMessage());
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(List list) {
        b.a(list);
        if (!TextUtils.isEmpty(this.b.h)) {
            list.add(new BasicNameValuePair("rsa_bduss", this.b.h));
            list.add(new BasicNameValuePair("appid", this.b.f));
        } else if (!TextUtils.isEmpty(this.b.d)) {
            list.add(new BasicNameValuePair("rsa_access_token", this.b.d));
        } else {
            list.add(new BasicNameValuePair("apikey", this.b.i));
        }
    }

    /* access modifiers changed from: protected */
    public String b(String str) {
        return str;
    }

    public boolean b() {
        boolean z = false;
        Log.i("BaseBaseApiProcessor", "networkConnect");
        if (!TextUtils.isEmpty(this.c)) {
            if (b.a()) {
                Log.d("BaseBaseApiProcessor", "Request Url = " + this.c);
            }
            ProxyHttpClient proxyHttpClient = new ProxyHttpClient(this.a);
            try {
                HttpPost httpPost = new HttpPost(this.c);
                httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded");
                ArrayList arrayList = new ArrayList();
                a(arrayList);
                httpPost.setEntity(new UrlEncodedFormEntity(arrayList, "UTF-8"));
                HttpResponse execute = proxyHttpClient.execute(httpPost);
                if (execute.getStatusLine().getStatusCode() == 200) {
                    String entityUtils = EntityUtils.toString(execute.getEntity());
                    if (b.a()) {
                        Log.i("BaseBaseApiProcessor", "<<< networkRegister return string :  " + entityUtils);
                    }
                    a(0, b(entityUtils).getBytes());
                    z = true;
                } else {
                    Log.i("BaseBaseApiProcessor", "networkRegister request failed  " + execute.getStatusLine());
                    String entityUtils2 = EntityUtils.toString(execute.getEntity());
                    if (b.a()) {
                        Log.i("BaseBaseApiProcessor", "<<< networkRegister return string :  " + entityUtils2);
                    }
                    a(entityUtils2);
                }
            } catch (IOException e) {
                if (b.a()) {
                    Log.e("BaseBaseApiProcessor", e.getMessage());
                    Log.i("BaseBaseApiProcessor", "io exception do something ? ");
                }
                a((int) PushConstants.ERROR_SERVICE_NOT_AVAILABLE);
            } catch (Exception e2) {
                if (b.a()) {
                    Log.e("BaseBaseApiProcessor", e2.getMessage());
                }
                a((int) PushConstants.ERROR_UNKNOWN);
            } finally {
                proxyHttpClient.close();
            }
        } else if (b.a()) {
            Log.e("BaseBaseApiProcessor", "mUrl is null");
        }
        return z;
    }

    public void run() {
        a();
    }
}
