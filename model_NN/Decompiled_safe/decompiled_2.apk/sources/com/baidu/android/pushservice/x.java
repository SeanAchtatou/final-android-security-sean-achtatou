package com.baidu.android.pushservice;

public final class x {
    public static final String a = i;
    public static final String b = j;
    public static final int c = k;
    public static final String d = l;
    public static final long e = m;
    public static final String f = (a + "/rest/2.0/channel/channel");
    public static final String g = (a + "/rest/2.0/channel/");
    public static final String h = (d + "/searchbox?action=publicsrv&type=issuedcode");
    private static String i = "http://channel.api.duapp.com";
    private static String j = "agentchannel.api.duapp.com";
    private static int k = 5287;
    private static String l = "http://m.baidu.com";
    private static long m = 86400000;

    static {
        a();
    }

    /* JADX WARNING: Removed duplicated region for block: B:41:0x008f A[Catch:{ all -> 0x00b0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x009a A[SYNTHETIC, Splitter:B:43:0x009a] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x00a7 A[SYNTHETIC, Splitter:B:50:0x00a7] */
    /* JADX WARNING: Removed duplicated region for block: B:61:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void a() {
        /*
            java.io.File r0 = new java.io.File
            java.io.File r1 = android.os.Environment.getExternalStorageDirectory()
            java.lang.String r2 = "pushservice.cfg"
            r0.<init>(r1, r2)
            boolean r1 = r0.exists()
            if (r1 == 0) goto L_0x0081
            java.util.Properties r3 = new java.util.Properties
            r3.<init>()
            r2 = 0
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0087, all -> 0x00a3 }
            r1.<init>(r0)     // Catch:{ Exception -> 0x0087, all -> 0x00a3 }
            r3.load(r1)     // Catch:{ Exception -> 0x00b2 }
            java.lang.String r0 = "http_server"
            java.lang.String r0 = r3.getProperty(r0)     // Catch:{ Exception -> 0x00b2 }
            if (r0 == 0) goto L_0x002f
            int r2 = r0.length()     // Catch:{ Exception -> 0x00b2 }
            if (r2 <= 0) goto L_0x002f
            com.baidu.android.pushservice.x.i = r0     // Catch:{ Exception -> 0x00b2 }
        L_0x002f:
            java.lang.String r0 = "socket_server"
            java.lang.String r0 = r3.getProperty(r0)     // Catch:{ Exception -> 0x00b2 }
            if (r0 == 0) goto L_0x003f
            int r2 = r0.length()     // Catch:{ Exception -> 0x00b2 }
            if (r2 <= 0) goto L_0x003f
            com.baidu.android.pushservice.x.j = r0     // Catch:{ Exception -> 0x00b2 }
        L_0x003f:
            java.lang.String r0 = "socket_server_port"
            java.lang.String r0 = r3.getProperty(r0)     // Catch:{ Exception -> 0x00b2 }
            if (r0 == 0) goto L_0x0053
            int r2 = r0.length()     // Catch:{ Exception -> 0x00b2 }
            if (r2 <= 0) goto L_0x0053
            int r0 = java.lang.Integer.parseInt(r0)     // Catch:{ Exception -> 0x00b2 }
            com.baidu.android.pushservice.x.k = r0     // Catch:{ Exception -> 0x00b2 }
        L_0x0053:
            java.lang.String r0 = "config_server"
            java.lang.String r0 = r3.getProperty(r0)     // Catch:{ Exception -> 0x00b2 }
            if (r0 == 0) goto L_0x0063
            int r2 = r0.length()     // Catch:{ Exception -> 0x00b2 }
            if (r2 <= 0) goto L_0x0063
            com.baidu.android.pushservice.x.l = r0     // Catch:{ Exception -> 0x00b2 }
        L_0x0063:
            java.lang.String r0 = "socket_interval"
            java.lang.String r0 = r3.getProperty(r0)     // Catch:{ Exception -> 0x00b2 }
            if (r0 == 0) goto L_0x007c
            int r2 = r0.length()     // Catch:{ Exception -> 0x00b2 }
            if (r2 <= 0) goto L_0x007c
            int r0 = java.lang.Integer.parseInt(r0)     // Catch:{ Exception -> 0x00b2 }
            long r2 = (long) r0     // Catch:{ Exception -> 0x00b2 }
            r4 = 60000(0xea60, double:2.9644E-319)
            long r2 = r2 * r4
            com.baidu.android.pushservice.x.m = r2     // Catch:{ Exception -> 0x00b2 }
        L_0x007c:
            if (r1 == 0) goto L_0x0081
            r1.close()     // Catch:{ IOException -> 0x0082 }
        L_0x0081:
            return
        L_0x0082:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0081
        L_0x0087:
            r0 = move-exception
            r1 = r2
        L_0x0089:
            boolean r2 = com.baidu.android.pushservice.b.a()     // Catch:{ all -> 0x00b0 }
            if (r2 == 0) goto L_0x0098
            java.io.PrintStream r2 = java.lang.System.out     // Catch:{ all -> 0x00b0 }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x00b0 }
            r2.println(r0)     // Catch:{ all -> 0x00b0 }
        L_0x0098:
            if (r1 == 0) goto L_0x0081
            r1.close()     // Catch:{ IOException -> 0x009e }
            goto L_0x0081
        L_0x009e:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0081
        L_0x00a3:
            r0 = move-exception
            r1 = r2
        L_0x00a5:
            if (r1 == 0) goto L_0x00aa
            r1.close()     // Catch:{ IOException -> 0x00ab }
        L_0x00aa:
            throw r0
        L_0x00ab:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00aa
        L_0x00b0:
            r0 = move-exception
            goto L_0x00a5
        L_0x00b2:
            r0 = move-exception
            goto L_0x0089
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.android.pushservice.x.a():void");
    }
}
