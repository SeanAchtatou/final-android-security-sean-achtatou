package com.baidu.android.pushservice.message;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.baidu.android.common.logging.Log;
import com.baidu.android.pushservice.PushConstants;
import com.baidu.android.pushservice.b;
import com.baidu.android.pushservice.z;
import java.net.URISyntaxException;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PublicMsg implements Parcelable {
    public static final Parcelable.Creator CREATOR = new i();
    public String a;
    public String b;
    public String c;
    public String d;
    public String e;
    public String f;
    public int g = 0;
    public String h;
    public int i = 0;
    public int j = 0;
    public int k = 0;
    public int l = 0;
    public int m = 7;
    public String n;
    public String o;
    public boolean p = true;

    PublicMsg() {
    }

    PublicMsg(Parcel parcel) {
        this.a = parcel.readString();
        this.b = parcel.readString();
        this.c = parcel.readString();
        this.d = parcel.readString();
        this.e = parcel.readString();
        this.f = parcel.readString();
        this.g = parcel.readInt();
        this.j = parcel.readInt();
        this.m = parcel.readInt();
        this.k = parcel.readInt();
        this.l = parcel.readInt();
        this.n = parcel.readString();
        this.h = parcel.readString();
    }

    private void a(Intent intent) {
        if (this.n != null) {
            try {
                JSONObject jSONObject = new JSONObject(this.n);
                Iterator<String> keys = jSONObject.keys();
                while (keys.hasNext()) {
                    String next = keys.next();
                    intent.putExtra(next, jSONObject.getString(next));
                }
                intent.putExtra(PushConstants.EXTRA_EXTRA, this.n);
            } catch (JSONException e2) {
                Log.w("PublicMsg", "Custom content to JSONObject exception::" + e2.getMessage());
            }
        }
    }

    private String b(Context context, String str) {
        Intent intent = new Intent();
        intent.setAction("android.intent.action.MAIN");
        intent.addCategory("android.intent.category.LAUNCHER");
        intent.setPackage(str);
        for (ResolveInfo next : context.getPackageManager().queryIntentActivities(intent, 0)) {
            if (next.activityInfo != null) {
                return next.activityInfo.name;
            }
        }
        return null;
    }

    private void b(Context context, String str, String str2) {
        try {
            Intent parseUri = this.h != null ? Intent.parseUri(this.h, 0) : new Intent();
            String b2 = b(context, str);
            if (b2 != null) {
                parseUri.setClassName(str, b2);
                parseUri.setFlags(parseUri.getFlags() | 268435456);
                parseUri.putExtra(PushConstants.EXTRA_OPENTYPE, 1);
                parseUri.putExtra(PushConstants.EXTRA_MSGID, str2);
                context.startActivity(parseUri);
            }
        } catch (URISyntaxException e2) {
            e2.printStackTrace();
        }
    }

    public void a(Context context, String str) {
        if (b.a()) {
            Log.d("PublicMsg", "=== Handle rich media notification: " + str + " title = " + this.c);
        }
        if ("com.baidu.android.pushservice.action.media.DELETE".equals(str)) {
            a(context, this.a, this.b, 2);
            return;
        }
        Intent intent = new Intent("com.baidu.android.pushservice.action.media.CLICK");
        intent.setPackage(this.f);
        intent.putExtra("public_msg", this);
        context.sendBroadcast(intent);
        a(context, this.a, this.b, 1);
    }

    /* access modifiers changed from: package-private */
    public void a(Context context, String str, int i2) {
        String b2 = z.a().b();
        String c2 = z.a().c();
        if (!TextUtils.isEmpty(b2) && !TextUtils.isEmpty(c2)) {
            if (b.a()) {
                Log.d("PublicMsg", "Send Linkhit, msgId = " + str + ", resultCode = " + i2);
            }
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put(PushConstants.EXTRA_MSGID, str);
                jSONObject.put("result_code", i2);
            } catch (JSONException e2) {
                if (b.a()) {
                    Log.d("PublicMsg", e2.getMessage());
                }
            }
            Thread thread = new Thread(new g(this, context, b2, c2, jSONObject.toString()));
            thread.setName("PushService-linkhit");
            thread.start();
        } else if (b.a()) {
            Log.e("PublicMsg", "Fail Send Public msg result. Token invalid!");
        }
    }

    public void a(Context context, String str, String str2) {
        int i2 = 1;
        int i3 = 0;
        if (b.a()) {
            Log.d("PublicMsg", "=== Handle msg: " + toString());
        }
        if ("com.baidu.pushservice.action.publicmsg.DELETE_V2".equals(str)) {
            if (b.a()) {
                Log.d("PublicMsg", "Public msg deleted by user, title = " + this.c);
            }
            a(context, str2, 2);
            return;
        }
        PackageManager packageManager = context.getPackageManager();
        try {
            int i4 = packageManager.getPackageInfo(this.f, 0).versionCode;
            if (i4 >= this.g) {
                Intent parseUri = Intent.parseUri(this.h, 0);
                parseUri.setPackage(this.f);
                if (packageManager.queryBroadcastReceivers(parseUri, 0).size() > 0) {
                    if (b.a()) {
                        Log.d("PublicMsg", "Intent broadcasted to app! ===> " + parseUri.toURI());
                    }
                    context.sendBroadcast(parseUri);
                } else if (packageManager.queryIntentActivities(parseUri, 0).size() > 0) {
                    if (b.a()) {
                        Log.d("PublicMsg", "Intent sent to actvity! ===> " + parseUri.toURI());
                    }
                    parseUri.addFlags(268435456);
                    context.startActivity(parseUri);
                } else {
                    if (b.a()) {
                        Log.d("PublicMsg", "No app component can deal, so start " + this.f + " launcher activity!");
                    }
                    i2 = 0;
                }
                i3 = i2;
            } else if (b.a()) {
                Log.d("PublicMsg", "Version code is too low! ===> app ver: " + i4 + ", request ver:" + this.g);
            }
        } catch (PackageManager.NameNotFoundException e2) {
            if (b.a()) {
                Log.e("PublicMsg", "package not exist \r\n" + e2.getMessage());
            }
        } catch (URISyntaxException e3) {
            if (b.a()) {
                Log.e("PublicMsg", "uri to intent fail \r\n" + e3.getMessage());
            }
        } catch (Exception e4) {
            Log.e("PublicMsg", "parse customize action error\r\n" + e4.getMessage());
        }
        if (i3 == 0) {
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setData(Uri.parse(this.e));
            intent.addFlags(268435456);
            try {
                context.startActivity(intent);
            } catch (ActivityNotFoundException e5) {
                if (b.a()) {
                    Log.e("PublicMsg", ">>> Url cann't be deal! \r\n" + e5.getMessage());
                }
            }
        }
        a(context, str2, i3);
    }

    /* access modifiers changed from: package-private */
    public void a(Context context, String str, String str2, int i2) {
        String b2 = z.a().b();
        String c2 = z.a().c();
        if (!TextUtils.isEmpty(b2) && !TextUtils.isEmpty(c2)) {
            if (b.a()) {
                Log.d("PublicMsg", "Send feedback, msgId = " + str + ", resultCode = " + i2);
            }
            JSONArray jSONArray = new JSONArray();
            JSONObject jSONObject = new JSONObject();
            jSONArray.put(jSONObject);
            try {
                jSONObject.put(PushConstants.EXTRA_MSGID, str);
                jSONObject.put("appid", str2);
                jSONObject.put("resultCode", i2);
            } catch (JSONException e2) {
                if (b.a()) {
                    Log.d("PublicMsg", e2.getMessage());
                }
            }
            Thread thread = new Thread(new h(this, context, b2, c2, jSONArray.toString(), str2, str, i2));
            thread.setName("PushService-feedback");
            thread.start();
        } else if (b.a()) {
            Log.e("PublicMsg", "Fail Send Private Notification msg result. Token invalid!");
        }
    }

    public void a(Context context, String str, String str2, String str3) {
        if (b.a()) {
            Log.d("PublicMsg", "=== Handle private notification: " + str);
        }
        if ("com.baidu.android.pushservice.action.privatenotification.DELETE".equals(str)) {
            if (b.a()) {
                Log.d("PublicMsg", "private notification deleted by user, title = " + this.c);
            }
            a(context, str2, str3, 2);
            return;
        }
        PackageManager packageManager = context.getPackageManager();
        try {
            PackageInfo packageInfo = packageManager.getPackageInfo(this.f, 0);
            int i2 = packageInfo.versionCode;
            if (i2 >= this.g) {
                Intent intent = new Intent(PushConstants.ACTION_RECEIVER_NOTIFICATION_CLICK);
                intent.setPackage(this.f);
                intent.putExtra(PushConstants.EXTRA_NOTIFICATION_TITLE, this.c);
                intent.putExtra(PushConstants.EXTRA_NOTIFICATION_CONTENT, this.d);
                a(intent);
                context.sendBroadcast(intent);
                if (this.k != 1 || this.e == null) {
                    if (this.k == 2) {
                        if (this.h != null) {
                            Intent parseUri = Intent.parseUri(this.h, 0);
                            parseUri.setPackage(this.f);
                            if (packageManager.queryBroadcastReceivers(parseUri, 0).size() > 0) {
                                if (b.a()) {
                                    Log.d("PublicMsg", "Intent broadcasted to app! ===> " + parseUri.toURI());
                                }
                                context.sendBroadcast(parseUri);
                            } else if (packageManager.queryIntentActivities(parseUri, 0).size() > 0) {
                                if (b.a()) {
                                    Log.d("PublicMsg", "Intent sent to actvity! ===> " + parseUri.toURI());
                                }
                                parseUri.addFlags(268435456);
                                parseUri.putExtra(PushConstants.EXTRA_OPENTYPE, 1);
                                parseUri.putExtra(PushConstants.EXTRA_MSGID, str2);
                                context.startActivity(parseUri);
                            }
                        } else {
                            b(context, this.f, str2);
                        }
                    }
                } else if (this.l == 1) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setTitle("提示");
                    builder.setMessage(((Object) packageInfo.applicationInfo.loadLabel(packageManager)) + "请求访问:" + this.e + " ?");
                    builder.setPositiveButton("允许", new e(this, context));
                    builder.setNeutralButton("拒绝", new f(this));
                    AlertDialog create = builder.create();
                    create.getWindow().setType(2003);
                    create.setCanceledOnTouchOutside(false);
                    create.show();
                } else {
                    Intent intent2 = new Intent();
                    intent2.setAction("android.intent.action.VIEW");
                    intent2.setData(Uri.parse(this.e));
                    intent2.addFlags(268435456);
                    context.startActivity(intent2);
                }
            } else if (b.a()) {
                Log.d("PublicMsg", "Version code is too low! ===> app ver: " + i2 + ", request ver:" + this.g);
            }
        } catch (PackageManager.NameNotFoundException e2) {
            if (b.a()) {
                Log.e("PublicMsg", "package not exist \r\n" + e2.getMessage());
            }
        } catch (URISyntaxException e3) {
            if (b.a()) {
                Log.e("PublicMsg", "uri to intent fail \r\n" + e3.getMessage());
            }
        }
        a(context, str2, str3, 1);
    }

    public int describeContents() {
        return 0;
    }

    public String toString() {
        return "\r\n mMsgId = " + this.a + "\r\n mAppId = " + this.b + "\r\n mTitle = " + this.c + "\r\n mDescription = " + this.d + "\r\n mUrl = " + this.e + "\r\n mNetType = " + this.i + "\r\n mSupportAppname = " + this.o + "\r\n mIsSupportApp = " + this.p + "\r\n mPkgName = " + this.f + "\r\n mPlgVercode = " + this.g + "\r\n mNotificationBuilder = " + this.j + "\r\n mNotificationBasicStyle = " + this.m + "\r\n mOpenType = " + this.k + "\r\n mUserConfirm = " + this.l + "\r\n mCustomContent = " + this.n + "\r\n mIntent = " + this.h;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        parcel.writeString(this.a);
        parcel.writeString(this.b);
        parcel.writeString(this.c);
        parcel.writeString(this.d);
        parcel.writeString(this.e);
        parcel.writeString(this.f);
        parcel.writeInt(this.g);
        parcel.writeInt(this.j);
        parcel.writeInt(this.m);
        parcel.writeInt(this.k);
        parcel.writeInt(this.l);
        parcel.writeString(this.n);
        parcel.writeString(this.h);
    }
}
