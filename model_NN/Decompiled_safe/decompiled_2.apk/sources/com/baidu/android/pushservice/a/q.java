package com.baidu.android.pushservice.a;

import android.content.Context;
import com.baidu.android.common.logging.Log;
import com.baidu.android.pushservice.PushConstants;
import com.baidu.android.pushservice.b;
import java.util.Iterator;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

public class q extends c {
    protected String e;

    public q(k kVar, Context context, String str) {
        super(kVar, context);
        this.e = str;
    }

    /* access modifiers changed from: protected */
    public void a(List list) {
        super.a(list);
        list.add(new BasicNameValuePair(PushConstants.EXTRA_METHOD, "gunbind"));
        list.add(new BasicNameValuePair(PushConstants.EXTRA_GID, this.e));
        if (b.a()) {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                Log.d("Gunbind", "Gunbind param -- " + ((NameValuePair) it.next()).toString());
            }
        }
    }
}
