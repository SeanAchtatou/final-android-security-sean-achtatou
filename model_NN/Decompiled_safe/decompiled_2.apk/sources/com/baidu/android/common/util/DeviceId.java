package com.baidu.android.common.util;

import android.content.Context;
import android.os.Environment;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.baidu.android.common.security.AESUtil;
import com.baidu.android.common.security.Base64;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public final class DeviceId {
    private static final String AES_KEY = "30212102dicudiab";
    private static final boolean DEBUG = false;
    private static final String EXT_FILE = "baidu/.cuid";
    private static final String KEY_DEVICE_ID = "com.baidu.deviceid";
    private static final String KEY_IMEI = "bd_setting_i";
    private static final String TAG = "DeviceId";

    private DeviceId() {
    }

    private static void checkPermission(Context context, String str) {
        if (!(context.checkCallingOrSelfPermission(str) == 0 ? true : DEBUG)) {
            throw new SecurityException("Permission Denial: requires permission " + str);
        }
    }

    public static String getAndroidId(Context context) {
        String string = Settings.Secure.getString(context.getContentResolver(), "android_id");
        return TextUtils.isEmpty(string) ? "" : string;
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x005c  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0034  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String getDeviceID(android.content.Context r7) {
        /*
            r2 = 1
            java.lang.String r0 = "android.permission.WRITE_SETTINGS"
            checkPermission(r7, r0)
            java.lang.String r0 = "android.permission.READ_PHONE_STATE"
            checkPermission(r7, r0)
            java.lang.String r0 = "android.permission.WRITE_EXTERNAL_STORAGE"
            checkPermission(r7, r0)
            r1 = 0
            java.lang.String r3 = ""
            android.content.ContentResolver r0 = r7.getContentResolver()     // Catch:{ Exception -> 0x0050 }
            java.lang.String r4 = "bd_setting_i"
            java.lang.String r3 = android.provider.Settings.System.getString(r0, r4)     // Catch:{ Exception -> 0x0050 }
            if (r3 != 0) goto L_0x0109
            java.lang.String r0 = getIMEI(r7)     // Catch:{ Exception -> 0x0050 }
        L_0x0023:
            android.content.ContentResolver r3 = r7.getContentResolver()     // Catch:{ Exception -> 0x0106 }
            java.lang.String r4 = "bd_setting_i"
            android.provider.Settings.System.putString(r3, r4, r0)     // Catch:{ Exception -> 0x0106 }
        L_0x002c:
            java.lang.String r4 = getAndroidId(r7)
            java.lang.String r3 = ""
            if (r1 == 0) goto L_0x005c
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "com.baidu"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r4)
            java.lang.String r0 = r0.toString()
            byte[] r0 = r0.getBytes()
            java.lang.String r3 = com.baidu.android.common.util.Util.toMd5(r0, r2)
        L_0x004f:
            return r3
        L_0x0050:
            r0 = move-exception
            r1 = r0
            r0 = r3
        L_0x0053:
            java.lang.String r3 = "DeviceId"
            java.lang.String r4 = "Settings.System.getString or putString failed"
            com.baidu.android.common.logging.Log.e(r3, r4, r1)
            r1 = r2
            goto L_0x002c
        L_0x005c:
            r1 = 0
            android.content.ContentResolver r3 = r7.getContentResolver()
            java.lang.String r5 = "com.baidu.deviceid"
            java.lang.String r3 = android.provider.Settings.System.getString(r3, r5)
            boolean r5 = android.text.TextUtils.isEmpty(r3)
            if (r5 == 0) goto L_0x00a6
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r3 = "com.baidu"
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.StringBuilder r1 = r1.append(r0)
            java.lang.StringBuilder r1 = r1.append(r4)
            java.lang.String r1 = r1.toString()
            byte[] r1 = r1.getBytes()
            java.lang.String r1 = com.baidu.android.common.util.Util.toMd5(r1, r2)
            android.content.ContentResolver r3 = r7.getContentResolver()
            java.lang.String r3 = android.provider.Settings.System.getString(r3, r1)
            boolean r5 = android.text.TextUtils.isEmpty(r3)
            if (r5 != 0) goto L_0x00a6
            android.content.ContentResolver r5 = r7.getContentResolver()
            java.lang.String r6 = "com.baidu.deviceid"
            android.provider.Settings.System.putString(r5, r6, r3)
            setExternalDeviceId(r0, r3)
        L_0x00a6:
            boolean r5 = android.text.TextUtils.isEmpty(r3)
            if (r5 == 0) goto L_0x00c6
            java.lang.String r3 = getExternalDeviceId(r0)
            boolean r5 = android.text.TextUtils.isEmpty(r3)
            if (r5 != 0) goto L_0x00c6
            android.content.ContentResolver r5 = r7.getContentResolver()
            android.provider.Settings.System.putString(r5, r1, r3)
            android.content.ContentResolver r5 = r7.getContentResolver()
            java.lang.String r6 = "com.baidu.deviceid"
            android.provider.Settings.System.putString(r5, r6, r3)
        L_0x00c6:
            boolean r5 = android.text.TextUtils.isEmpty(r3)
            if (r5 == 0) goto L_0x004f
            java.util.UUID r3 = java.util.UUID.randomUUID()
            java.lang.String r3 = r3.toString()
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.StringBuilder r5 = r5.append(r0)
            java.lang.StringBuilder r4 = r5.append(r4)
            java.lang.StringBuilder r3 = r4.append(r3)
            java.lang.String r3 = r3.toString()
            byte[] r3 = r3.getBytes()
            java.lang.String r3 = com.baidu.android.common.util.Util.toMd5(r3, r2)
            android.content.ContentResolver r2 = r7.getContentResolver()
            android.provider.Settings.System.putString(r2, r1, r3)
            android.content.ContentResolver r1 = r7.getContentResolver()
            java.lang.String r2 = "com.baidu.deviceid"
            android.provider.Settings.System.putString(r1, r2, r3)
            setExternalDeviceId(r0, r3)
            goto L_0x004f
        L_0x0106:
            r1 = move-exception
            goto L_0x0053
        L_0x0109:
            r0 = r3
            goto L_0x0023
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.android.common.util.DeviceId.getDeviceID(android.content.Context):java.lang.String");
    }

    private static String getExternalDeviceId(String str) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(new File(Environment.getExternalStorageDirectory(), EXT_FILE)));
            StringBuilder sb = new StringBuilder();
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    break;
                }
                sb.append(readLine);
                sb.append("\r\n");
            }
            bufferedReader.close();
            String[] split = new String(AESUtil.decrypt(AES_KEY, AES_KEY, Base64.decode(sb.toString().getBytes()))).split("=");
            return (split == null || split.length != 2 || !str.equals(split[0])) ? "" : split[1];
        } catch (FileNotFoundException e) {
            return "";
        } catch (IOException | Exception e2) {
            return "";
        }
    }

    public static String getIMEI(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        if (telephonyManager == null) {
            return "";
        }
        String deviceId = telephonyManager.getDeviceId();
        return TextUtils.isEmpty(deviceId) ? "" : deviceId;
    }

    private static void setExternalDeviceId(String str, String str2) {
        if (!TextUtils.isEmpty(str)) {
            File file = new File(Environment.getExternalStorageDirectory(), EXT_FILE);
            try {
                new File(file.getParent()).mkdirs();
                FileWriter fileWriter = new FileWriter(file, (boolean) DEBUG);
                fileWriter.write(Base64.encode(AESUtil.encrypt(AES_KEY, AES_KEY, (str + "=" + str2).getBytes()), "utf-8"));
                fileWriter.flush();
                fileWriter.close();
            } catch (IOException | Exception e) {
            }
        }
    }
}
