package com.baidu.android.common.logging;

import android.os.Process;
import android.text.TextUtils;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class Log {
    private static final boolean DEBUG = Configuration.isLogEnabled();
    public static final int FILE_LIMETE = 10485760;
    public static final int FILE_NUMBER = 2;
    private static final boolean LOG_TO_FILE = Configuration.shouldLog2Fie();
    private static Logger sFilelogger;

    static {
        if (LOG_TO_FILE && DEBUG) {
            String logFileName = getLogFileName();
            try {
                FileHandler fileHandler = new FileHandler(("/sdcard/" + logFileName) + "_%g.log", FILE_LIMETE, 2, true);
                fileHandler.setFormatter(new SimpleFormatter());
                sFilelogger = Logger.getLogger(logFileName);
                sFilelogger.setLevel(Level.ALL);
                sFilelogger.addHandler(fileHandler);
            } catch (SecurityException e) {
                e.printStackTrace();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
    }

    private Log() {
    }

    public static void d(String str, String str2) {
        if (!DEBUG) {
            return;
        }
        if (!LOG_TO_FILE || sFilelogger == null) {
            android.util.Log.d(str, str2);
        } else {
            sFilelogger.log(Level.INFO, str + ": " + str2);
        }
    }

    public static void d(String str, String str2, Throwable th) {
        d(str, str2 + 10 + getStackTraceString(th));
    }

    public static void e(String str, String str2) {
        if (!DEBUG) {
            return;
        }
        if (!LOG_TO_FILE || sFilelogger == null) {
            android.util.Log.e(str, str2);
        } else {
            sFilelogger.log(Level.SEVERE, str + ": " + str2);
        }
    }

    public static void e(String str, String str2, Throwable th) {
        e(str, str2 + 10 + getStackTraceString(th));
    }

    public static void e(String str, Throwable th) {
        e(str, getStackTraceString(th));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    private static String getLogFileName() {
        String processNameForPid = getProcessNameForPid(Process.myPid());
        if (TextUtils.isEmpty(processNameForPid)) {
            processNameForPid = "BaiduFileLog";
        }
        return processNameForPid.replace(':', '_');
    }

    private static String getProcessNameForPid(int i) {
        Exception e;
        String str;
        BufferedReader bufferedReader;
        String str2 = "/proc/" + i + "/status";
        try {
            BufferedReader bufferedReader2 = new BufferedReader(new FileReader(new File("/proc/" + i + "/cmdline")));
            String readLine = bufferedReader2.readLine();
            if (!TextUtils.isEmpty(readLine)) {
                BufferedReader bufferedReader3 = bufferedReader2;
                str = readLine.substring(0, readLine.indexOf(0));
                bufferedReader = bufferedReader3;
            } else {
                BufferedReader bufferedReader4 = new BufferedReader(new FileReader(new File(str2)));
                String readLine2 = bufferedReader4.readLine();
                while (true) {
                    if (readLine2 == null) {
                        break;
                    } else if (readLine2.startsWith("Name:")) {
                        int indexOf = readLine2.indexOf("\t");
                        if (indexOf >= 0) {
                            BufferedReader bufferedReader5 = bufferedReader4;
                            str = readLine2.substring(indexOf + 1);
                            bufferedReader = bufferedReader5;
                        }
                    } else {
                        readLine2 = bufferedReader4.readLine();
                    }
                }
                BufferedReader bufferedReader6 = bufferedReader4;
                str = "";
                bufferedReader = bufferedReader6;
            }
            try {
                bufferedReader.close();
            } catch (Exception e2) {
                e = e2;
                e.printStackTrace();
                return str;
            }
        } catch (Exception e3) {
            Exception exc = e3;
            str = "";
            e = exc;
            e.printStackTrace();
            return str;
        }
        return str;
    }

    public static String getStackTraceString(Throwable th) {
        if (th == null) {
            return "";
        }
        StringWriter stringWriter = new StringWriter();
        th.printStackTrace(new PrintWriter(stringWriter));
        return stringWriter.toString();
    }

    public static void i(String str, String str2) {
        if (!DEBUG) {
            return;
        }
        if (!LOG_TO_FILE || sFilelogger == null) {
            android.util.Log.i(str, str2);
        } else {
            sFilelogger.log(Level.INFO, str + ": " + str2);
        }
    }

    public static void i(String str, String str2, Throwable th) {
        i(str, str2 + 10 + getStackTraceString(th));
    }

    public static void v(String str, String str2) {
        if (!DEBUG) {
            return;
        }
        if (!LOG_TO_FILE || sFilelogger == null) {
            android.util.Log.v(str, str2);
        } else {
            sFilelogger.log(Level.INFO, str + ": " + str2);
        }
    }

    public static void v(String str, String str2, Throwable th) {
        v(str, str2 + 10 + getStackTraceString(th));
    }

    public static void w(String str, String str2) {
        if (!DEBUG) {
            return;
        }
        if (!LOG_TO_FILE || sFilelogger == null) {
            android.util.Log.w(str, str2);
        } else {
            sFilelogger.log(Level.WARNING, str + ": " + str2);
        }
    }

    public static void w(String str, String str2, Throwable th) {
        w(str, str2 + 10 + getStackTraceString(th));
    }
}
