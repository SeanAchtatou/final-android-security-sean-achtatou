package com.monkeyfly.android.bubble.BeachTwinkleP2;

import android.content.Context;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.util.Log;
import java.util.HashMap;
import java.util.Random;

class SoundEffect {
    private int[] MUSIC_RID = {R.raw.bg_g1, R.raw.bg_g2, R.raw.bg_g3, R.raw.bg_g4};
    private Context mContext;
    private int mCurMusicIdx;
    private MediaPlayer mMusicMP;
    private Random mRand = new Random();
    private HashMap<Integer, Integer> mSoundMap;
    private boolean mSoundOn = false;
    private SoundPool mSoundPool;

    public SoundEffect(Context c) {
        this.mContext = c;
        this.mCurMusicIdx = 0;
        init();
    }

    private void init() {
        this.mSoundPool = new SoundPool(30, 3, 100);
        this.mSoundMap = new HashMap<>();
        for (int i = 1; i < 11; i++) {
            this.mSoundMap.put(Integer.valueOf(i), Integer.valueOf(this.mSoundPool.load(this.mContext, getSoundResIdByType(i), 0)));
        }
    }

    public void play(int soundId) {
        int resId;
        if (soundId > 0 && soundId < 11 && this.mSoundOn) {
            if (soundId > 6) {
                resId = this.mSoundMap.get(Integer.valueOf(soundId)).intValue();
            } else {
                resId = this.mSoundMap.get(Integer.valueOf(this.mRand.nextInt(6) + 1)).intValue();
            }
            this.mSoundPool.play(resId, (float) 1, (float) 1, 0, 0, 1.0f);
        }
    }

    public void free() {
        if (this.mSoundPool != null) {
            this.mSoundPool.release();
            this.mSoundPool = null;
        }
        if (this.mSoundMap != null) {
            this.mSoundMap.clear();
            this.mSoundMap = null;
        }
        musicStop();
    }

    private int getSoundResIdByType(int st) {
        switch (st) {
            case 1:
                return R.raw.drip1;
            case 2:
                return R.raw.drip2;
            case 3:
                return R.raw.drip3;
            case 4:
                return R.raw.drip4;
            case 5:
                return R.raw.drip5;
            case 6:
                return R.raw.drip6;
            case 7:
                return R.raw.game_over;
            case BBConstants.SOUND_ID_WELLDONE:
                return R.raw.well_done;
            case BBConstants.SOUND_ID_PRESS_BALL:
                return R.raw.press;
            case BBConstants.SOUND_ID_PRESS_BTN:
                return R.raw.press_btn;
            default:
                return -1;
        }
    }

    public void setSoundOn(boolean on) {
        this.mSoundOn = on;
    }

    public void playMusic(int musicId) {
        if (this.mSoundOn) {
            musicPlay(musicId);
        }
    }

    public void pauseMusic() {
        if (this.mMusicMP != null) {
            this.mMusicMP.pause();
        }
    }

    public void resumeMusic(int musicId) {
        if (!this.mSoundOn) {
            Log.v("SoundEffect", "resumeMusic(), mSoundOn = false");
            musicStop();
            return;
        }
        Log.v("SoundEffect", "resumeMusic(), mSoundOn = true");
        if (this.mMusicMP != null) {
            this.mMusicMP.start();
        } else {
            musicPlay(musicId);
        }
    }

    private void musicPlay(int musicId) {
        int resId;
        musicStop();
        if (musicId == 1) {
            resId = R.raw.bg_menu;
        } else {
            if (this.mCurMusicIdx >= this.MUSIC_RID.length) {
                this.mCurMusicIdx = 0;
            }
            resId = this.MUSIC_RID[this.mCurMusicIdx];
            this.mCurMusicIdx++;
        }
        if (this.mMusicMP == null) {
            this.mMusicMP = MediaPlayer.create(this.mContext, resId);
        }
        this.mMusicMP.start();
        this.mMusicMP.setLooping(true);
    }

    public void musicStop() {
        if (this.mMusicMP != null) {
            if (this.mMusicMP.isPlaying()) {
                this.mMusicMP.stop();
            }
            this.mMusicMP.release();
            this.mMusicMP = null;
        }
    }
}
