package com.monkeyfly.android.bubble.BeachTwinkleP2;

import android.os.Handler;
import android.os.Message;

public class MyTimer extends Handler {
    private static int TIMERID = 0;
    private CallBack mCallBack;
    private final int mInterval;

    public interface CallBack {
        void timerCallBack();
    }

    public MyTimer(int interval, CallBack callback) {
        this.mInterval = interval;
        this.mCallBack = callback;
        TIMERID++;
    }

    public void handleMessage(Message msg) {
        super.handleMessage(msg);
        if (msg.what == TIMERID) {
            sendMessageDelayed(obtainMessage(TIMERID), (long) this.mInterval);
            this.mCallBack.timerCallBack();
        }
    }

    public void startTimer() {
        sendMessage(obtainMessage(TIMERID));
    }

    public void stopTimer() {
        removeMessages(TIMERID);
    }
}
