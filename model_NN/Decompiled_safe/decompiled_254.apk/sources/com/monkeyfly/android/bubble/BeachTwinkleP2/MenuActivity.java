package com.monkeyfly.android.bubble.BeachTwinkleP2;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Process;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import com.scoreloop.android.coreui.HighscoresActivity;
import com.scoreloop.android.coreui.ScoreloopManager;

public class MenuActivity extends Activity implements View.OnClickListener {
    private ImageView mImgGameLogo = null;
    private boolean mIsSoundOn;
    /* access modifiers changed from: private */
    public SoundEffect mSoundEffect;
    private Window window;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        this.window = getWindow();
        this.window.setFlags(1024, 1024);
        setContentView((int) R.layout.splash);
        ((Button) findViewById(R.id.start_game)).setOnClickListener(this);
        ((Button) findViewById(R.id.score_board)).setOnClickListener(this);
        ((Button) findViewById(R.id.resume)).setOnClickListener(this);
        ((Button) findViewById(R.id.setting)).setOnClickListener(this);
        ((Button) findViewById(R.id.exit)).setOnClickListener(this);
        initScoreLoop();
        loadGameSetting();
        this.mSoundEffect = new SoundEffect(getBaseContext());
        this.mSoundEffect.setSoundOn(this.mIsSoundOn);
        this.mSoundEffect.playMusic(1);
        this.mImgGameLogo = (ImageView) findViewById(R.id.game_logo);
        startGameLogoMove();
    }

    public void onStop() {
        super.onStop();
    }

    public void onPause() {
        super.onPause();
        this.mSoundEffect.pauseMusic();
    }

    public void onResume() {
        super.onResume();
        loadGameSetting();
        this.mSoundEffect.setSoundOn(this.mIsSoundOn);
        this.mSoundEffect.resumeMusic(1);
    }

    private void startGameLogoMove() {
        if (this.mImgGameLogo != null) {
            this.mImgGameLogo.startAnimation(AnimationUtils.loadAnimation(this, R.anim.logo_move));
        }
    }

    private void exitConfirm() {
        new AlertDialog.Builder(this).setMessage("Do you want to quit?").setTitle("Exit").setIcon((int) R.drawable.icon).setPositiveButton("More Game", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                MenuActivity.this.showMore();
            }
        }).setNeutralButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                MenuActivity.this.mSoundEffect.free();
                MenuActivity.this.finish();
                Process.killProcess(Process.myPid());
            }
        }).create().show();
    }

    /* access modifiers changed from: private */
    public void showMore() {
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://market.android.com/search?q=pub:\"GameStores\"")));
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        exitConfirm();
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void onClick(View v) {
        Class<Bubble> cls = Bubble.class;
        Intent it = null;
        switch (v.getId()) {
            case R.id.start_game:
                this.mSoundEffect.play(10);
                Class<Bubble> cls2 = Bubble.class;
                it = new Intent(this, cls);
                it.putExtra("resume", false);
                finish();
                break;
            case R.id.resume:
                this.mSoundEffect.play(10);
                Class<Bubble> cls3 = Bubble.class;
                it = new Intent(this, cls);
                it.putExtra("resume", true);
                finish();
                break;
            case R.id.setting:
                this.mSoundEffect.play(10);
                it = new Intent(this, Prefs.class);
                break;
            case R.id.score_board:
                this.mSoundEffect.play(10);
                it = new Intent(this, HighscoresActivity.class);
                break;
            case R.id.exit:
                this.mSoundEffect.play(10);
                exitConfirm();
                break;
        }
        if (it != null) {
            startActivity(it);
        }
    }

    private void initScoreLoop() {
        ScoreloopManager.init(this, BBConstants.SL_GAME_ID, BBConstants.SL_GAME_SECRET);
        ScoreloopManager.setNumberOfModes(1);
    }

    private void loadGameSetting() {
        this.mIsSoundOn = getSharedPreferences(BBConstants.KEY_CFG_BASE, 0).getBoolean(BBConstants.KEY_CFG_SOUND, true);
    }
}
