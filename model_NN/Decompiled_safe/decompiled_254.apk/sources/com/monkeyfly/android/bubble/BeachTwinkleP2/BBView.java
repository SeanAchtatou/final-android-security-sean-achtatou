package com.monkeyfly.android.bubble.BeachTwinkleP2;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.SoundPool;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import com.monkeyfly.android.bubble.BeachTwinkleP2.MyTimer;
import java.util.Timer;
import java.util.TimerTask;

public class BBView extends View implements MyTimer.CallBack {
    private static final int MAX_GAME_TIME = 90;
    public static final int mAdsHeight = 45;
    public static final int mScoreBoardHeight = 43;
    public static long mStartTime;
    public static int mViewHeight;
    public static int mViewWidth;
    public static int mXOffset;
    public static int mYOffset;
    public boolean bSound;
    private int bubbleShowIdx;
    private Typeface fontDefault;
    private Typeface fontGame;
    Handler handler;
    public int hitOkSfx;
    public int longhit;
    /* access modifiers changed from: private */
    public boolean mAlive;
    public BBMatrix mBBMatrix;
    public Bitmap mBitmap;
    public Bitmap mBitmap1;
    public Bitmap mBitmap2;
    public Bitmap[] mBmpBG;
    private Bitmap mBmpMask;
    private Bitmap mBmpTimebar;
    private Bitmap mBmpTimebarFill;
    public Bitmap[] mBmpTvWave;
    private Bitmap mBmpWellDone;
    public Bitmap[] mBubble2Array;
    public Bitmap[] mBubble3Array;
    public Bitmap[] mBubbleArray;
    public Canvas mCanvas;
    private boolean mIsResumeNewLevel;
    public int mLevel;
    public int mMode;
    public View mOverView;
    public final Paint mPaint;
    public Bubble mRefMain;
    private int mScaleIdx;
    private long mScaleLastTime;
    public int mScore;
    private ScoreBubble mScoreBubble;
    public Bitmap mSelectmp;
    TimerTask mTask;
    private int mTaskPassTime;
    public MyTimer mTimerDraw;
    Timer mTimerJob;
    public int mTotalScore;
    private long mWaveChangeTime;
    private int mWaveImgIdx;
    public Paint mpt;
    public SoundPool snd;
    public int streamVolume;

    /* access modifiers changed from: private */
    public void doJob() {
        if (this.mMode == 2) {
            int i = this.mTaskPassTime;
            this.mTaskPassTime = i + 1;
            if (i > MAX_GAME_TIME) {
                this.mTaskPassTime = MAX_GAME_TIME;
                setMode(5);
            }
        }
    }

    public void timerCallBack() {
        if (this.mAlive) {
            if (this.mMode == 2) {
                refreshPlayground();
            } else if (this.mMode == 4) {
                drawUpgradeImg(this.mCanvas);
            }
            invalidate();
        }
    }

    public void RefeshScoreAndLevel(Canvas pCan) {
        pCan.drawBitmap(this.mBitmap1, 0.0f, 0.0f, this.mPaint);
        this.mpt.setColor(-1);
        this.mpt.setTextSize(14.0f);
        pCan.drawText(new StringBuilder().append(this.mTotalScore).toString(), 17.0f, 37.0f, this.mpt);
        pCan.drawText(Integer.toString(this.mLevel), 278.0f, 37.0f, this.mpt);
    }

    public void RefeshDrawTime() {
        if (2 == this.mMode) {
            this.mBitmap1 = Bitmap.createBitmap(mViewWidth, 43, Bitmap.Config.ARGB_8888);
            this.mCanvas.setBitmap(this.mBitmap1);
            if (this.mBitmap1 != null) {
                this.mCanvas.drawBitmap(this.mBitmap1, 0.0f, 0.0f, (Paint) null);
            }
            Paint pt = new Paint();
            pt.setColor(-65536);
            pt.setTextSize(14.0f);
            this.mCanvas.drawText("Time:" + ((System.currentTimeMillis() - mStartTime) / 1000) + " second", 5.0f, 40.0f, pt);
            invalidate();
        }
    }

    public void RefeshDrawPush(Canvas pCan) {
        this.mpt.setARGB(77, 77, 128, 179);
        pCan.drawRect(1.0f, (float) ((mViewHeight - 45) + 10), (float) mViewWidth, (float) mViewHeight, this.mpt);
    }

    private void drawTimeBar(Canvas c) {
        BitmapDrawable d1 = new BitmapDrawable(this.mBmpTimebar);
        d1.setBounds(70, 12, 250, 12 + 11);
        d1.draw(c);
        BitmapDrawable d2 = new BitmapDrawable(this.mBmpTimebarFill);
        d2.setBounds(70 + 1, 12 + 1, (int) (70.0f + ((180.0f * ((float) (MAX_GAME_TIME - this.mTaskPassTime))) / 90.0f)), 12 + 10);
        d2.draw(c);
    }

    public BBView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mMode = 1;
        this.mPaint = new Paint();
        this.mIsResumeNewLevel = false;
        this.snd = null;
        this.bSound = true;
        this.mWaveChangeTime = 0;
        this.mWaveImgIdx = 0;
        this.mAlive = true;
        this.mTimerJob = new Timer();
        this.handler = new Handler() {
            public void handleMessage(Message msg) {
                if (msg.what == 1 && BBView.this.mAlive) {
                    BBView.this.doJob();
                }
                super.handleMessage(msg);
            }
        };
        this.mTask = new TimerTask() {
            public void run() {
                Message message = new Message();
                message.what = 1;
                BBView.this.handler.sendMessage(message);
            }
        };
        this.mScaleIdx = -1;
        this.mScaleLastTime = -1;
        this.bubbleShowIdx = 0;
        this.mTimerDraw = null;
        this.mAlive = true;
        this.mCanvas = new Canvas();
        Display display = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
        mViewWidth = display.getWidth();
        mViewHeight = display.getHeight();
    }

    public void initBBView(boolean isResume) {
        setFocusable(true);
        loadResImage();
        this.snd = null;
        this.mpt = new Paint();
        this.mpt.setFlags(1);
        this.mpt.setTextSize(20.0f);
        this.mpt.setColor(-16711936);
        this.mTimerDraw = new MyTimer(30, this);
        this.mTimerDraw.startTimer();
        this.mTimerJob.schedule(this.mTask, 1000, 1000);
        mXOffset = 1;
        mYOffset = 44;
        this.mBitmap = null;
        this.mBitmap1 = null;
        this.mBitmap2 = null;
        this.fontGame = Typeface.createFromAsset(getContext().getAssets(), "fonts/ARCENA.ttf");
        this.mScoreBubble = new ScoreBubble(320, 480);
        this.mScoreBubble.setFont(this.fontGame);
        boolean isLoadData = false;
        if (!isResume) {
            this.mTotalScore = 0;
            this.mLevel = 1;
        } else if (!loadLevelData()) {
            this.mTotalScore = 0;
            this.mLevel = 1;
        } else if (!this.mIsResumeNewLevel) {
            isLoadData = true;
        }
        this.mBBMatrix = new BBMatrix(mViewWidth, mViewHeight - 88);
        if (isLoadData) {
            loadBBMatrixData();
            newGame(true);
            return;
        }
        newGame(false);
    }

    public void clearsound() {
        if (this.snd != null) {
            this.snd.release();
            this.snd = null;
        }
    }

    private void updateLevel() {
        this.mLevel++;
    }

    public void setMode(int newMode) {
        int oldMode = this.mMode;
        this.mMode = newMode;
        if ((newMode == 2) && (oldMode != 2)) {
            this.mRefMain.showAd(false);
            Log.v("BBView", "setMode():newMode = RUNNING");
            return;
        }
        Resources resources = getContext().getResources();
        if (newMode != 0 && newMode != 1) {
            if (newMode == 4) {
                this.mRefMain.showAd(true);
                this.mRefMain.mSoundEffect.musicStop();
                this.mRefMain.mSoundEffect.play(8);
            } else if (newMode == 5) {
                this.mRefMain.mSoundEffect.musicStop();
                this.mRefMain.mSoundEffect.play(7);
                this.mRefMain.showSubmitScoreDlg("Game Over", "Do you want to submit your score (" + this.mTotalScore + ") to the global high scores list?");
            }
        }
    }

    private void freeAlloc() {
        if (!this.mBitmap2.isRecycled()) {
            this.mBitmap2.recycle();
            this.mBitmap2 = null;
        }
        if (!this.mBitmap.isRecycled()) {
            this.mBitmap.recycle();
            this.mBitmap = null;
        }
        if (!this.mBitmap1.isRecycled()) {
            this.mBitmap1.recycle();
            this.mBitmap1 = null;
        }
        if (!this.mBmpWellDone.isRecycled()) {
            this.mBmpWellDone.recycle();
            this.mBmpWellDone = null;
        }
        if (this.mBmpBG != null) {
            for (int i = 0; i < this.mBmpBG.length; i++) {
                if (this.mBmpBG[i].isRecycled()) {
                    this.mBmpBG[i].recycle();
                    this.mBmpBG[i] = null;
                }
            }
        }
        System.gc();
    }

    public void newGame(boolean isResume) {
        mStartTime = System.currentTimeMillis();
        if (!isResume) {
            this.mScore = 0;
            gameTimeInit(this.mLevel);
            this.mBBMatrix.clearBubbles();
            this.mBBMatrix.setTypeCountByLevel(this.mLevel);
            this.mBBMatrix.fillBBMatrix();
        }
        int idx = (this.mLevel - 1) % this.mBmpBG.length;
        if (idx < 0 || idx >= this.mBmpBG.length) {
            idx = 0;
        }
        this.mBitmap2 = this.mBmpBG[idx];
        if (this.mBitmap == null) {
            this.mBitmap = Bitmap.createBitmap(mViewWidth, mViewHeight, Bitmap.Config.ARGB_8888);
        }
        if (this.mBitmap1 == null) {
            Drawable bubble = getContext().getResources().getDrawable(R.drawable.top);
            Bitmap bitmap = Bitmap.createBitmap(mViewWidth, 43, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            bubble.setBounds(0, 0, mViewWidth, 43);
            bubble.draw(canvas);
            this.mBitmap1 = bitmap;
        }
        this.mCanvas.setBitmap(this.mBitmap);
        refreshPlayground();
        this.mRefMain.mSoundEffect.playMusic(2);
        this.mWaveChangeTime = 0;
        setMode(2);
    }

    private void gameUpgrade() {
        this.mLevel++;
        newGame(false);
    }

    private void gameTimeInit(int level) {
        this.mTaskPassTime = 0;
    }

    private void gameTimeAdd(int sameCount) {
        this.mTaskPassTime -= sameCount * 5;
        if (this.mTaskPassTime < 0) {
            this.mTaskPassTime = 0;
        }
    }

    private float getScaleSize(boolean change) {
        float r = 1.0f;
        if (this.mScaleIdx == -1) {
            return 1.0f;
        }
        if (change) {
            this.mScaleIdx++;
        }
        switch (this.mScaleIdx) {
            case 1:
            case 7:
                r = 0.9f;
                break;
            case 2:
            case 6:
                r = 0.8f;
                break;
            case 3:
            case 5:
                r = 0.7f;
                break;
            case 4:
                r = 0.5f;
                break;
            case BBConstants.SOUND_ID_WELLDONE:
                r = 1.1f;
                this.mScaleIdx = 0;
                break;
        }
        return r;
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() != 1 || this.mMode == 5) {
            return true;
        }
        if (this.mMode == 4) {
            gameUpgrade();
            return true;
        }
        int curX = (int) Math.floor((double) event.getX());
        int curY = (int) Math.floor((double) event.getY());
        int bubbleX = (curX - mXOffset) / 40;
        int bubbleY = (curY - mYOffset) / 40;
        if (!this.mBBMatrix.isMarked(bubbleX, bubbleY) || this.mBBMatrix.mSameBubbleCount <= 1) {
            this.mBBMatrix.removeMark();
            this.mBBMatrix.mSameBubbleCount = 0;
            this.mBBMatrix.findSameBubble(bubbleX, bubbleY);
            if (this.mBBMatrix.mSameBubbleCount > 1) {
                this.mScaleIdx = 0;
                this.mRefMain.mSoundEffect.play(1);
                this.mRefMain.playVibrate();
            } else {
                this.mScaleIdx = -1;
            }
            return true;
        }
        this.mRefMain.mSoundEffect.play(9);
        int hitCount = this.mBBMatrix.mSameBubbleCount;
        int nowScore = calculateScore(hitCount);
        this.mScoreBubble.Add(curX, curY, hitCount, nowScore);
        Log.v("BBView", "Add Score Bubble");
        gameTimeAdd(hitCount);
        this.mScore += nowScore;
        this.mTotalScore += nowScore;
        this.mBBMatrix.removeMarkedBubbles();
        refreshPlayground();
        this.mBBMatrix.removeMark();
        invalidate();
        if (!this.mBBMatrix.isBBMatrixSolvable()) {
            Log.v("BBView", "isBBMatrixSolvable() is false, Next Level now");
            setMode(4);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.mAlive && this.mBitmap != null) {
            canvas.drawBitmap(this.mBitmap, 0.0f, 0.0f, (Paint) null);
        }
    }

    private void drawTvWave(Canvas c) {
        if (this.mWaveChangeTime == 0) {
            this.mWaveImgIdx = 0;
            this.mWaveChangeTime = System.currentTimeMillis();
        } else if (System.currentTimeMillis() - this.mWaveChangeTime >= 200) {
            int i = this.mWaveImgIdx + 1;
            this.mWaveImgIdx = i;
            if (i >= 4) {
                this.mWaveImgIdx = 0;
            }
            this.mWaveChangeTime = System.currentTimeMillis();
        }
        c.drawBitmap(this.mBmpTvWave[this.mWaveImgIdx], 120.0f, 40.0f, (Paint) null);
    }

    private void drawUpgradeImg(Canvas c) {
        c.drawBitmap(this.mBmpWellDone, 0.0f, 0.0f, (Paint) null);
        drawTvWave(c);
        this.mpt.setColor(-1);
        this.mpt.setTextSize(18.0f);
        String s = Integer.toString(this.mLevel);
        c.drawText(s, 20.0f + ((320.0f - this.mpt.measureText(s)) / 2.0f), 219.0f, this.mpt);
        String s2 = Integer.toString(this.mScore);
        c.drawText(s2, 20.0f + ((320.0f - this.mpt.measureText(s2)) / 2.0f), 260.0f, this.mpt);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    private Bitmap getScaleBmp(Bitmap bmp, float f) {
        Matrix matrix = new Matrix();
        matrix.postScale(f, f);
        return Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), matrix, true);
    }

    private Bitmap getCurBmp(int bubbleKey, int idx) {
        Bitmap bmp = this.mBubbleArray[bubbleKey];
        if (idx == 1) {
            return this.mBubble2Array[bubbleKey];
        }
        if (idx == 2) {
            return this.mBubble3Array[bubbleKey];
        }
        return bmp;
    }

    public void refreshPlayground() {
        Bitmap bmp;
        this.mCanvas.drawBitmap(this.mBitmap2, 0.0f, 0.0f, this.mPaint);
        RefeshScoreAndLevel(this.mCanvas);
        this.mCanvas.drawBitmap(this.mBmpMask, 0.0f, 43.0f, this.mPaint);
        if (this.mBBMatrix.mSameBubbleCount <= 1) {
            this.bubbleShowIdx = 0;
        } else if (this.mScaleLastTime == -1 || System.currentTimeMillis() - this.mScaleLastTime >= 130) {
            this.mScaleLastTime = System.currentTimeMillis();
            if (this.bubbleShowIdx == 0) {
                this.bubbleShowIdx = 1;
            } else if (this.bubbleShowIdx == 1) {
                this.bubbleShowIdx = 2;
            } else {
                this.bubbleShowIdx = 0;
            }
        }
        for (int i = 0; i < this.mBBMatrix.mXBubbleCount; i++) {
            for (int j = 0; j < this.mBBMatrix.mYBubbleCount; j++) {
                if (this.mBBMatrix.mBubbleGrid[i][j] != -1) {
                    float left = (float) (mXOffset + (i * 40));
                    float top = (float) (mYOffset + (j * 40));
                    if (!this.mBBMatrix.isMarked(i, j) || this.mBBMatrix.mSameBubbleCount <= 1) {
                        bmp = this.mBubbleArray[this.mBBMatrix.mBubbleGrid[i][j]];
                    } else {
                        bmp = getCurBmp(this.mBBMatrix.mBubbleGrid[i][j], this.bubbleShowIdx);
                    }
                    this.mCanvas.drawBitmap(bmp, left, top, this.mPaint);
                }
            }
        }
        this.mScoreBubble.show(this.mCanvas, this.mPaint);
        drawTimeBar(this.mCanvas);
    }

    private int getBonus() {
        if (this.mLevel == 1) {
            return 1;
        }
        return (this.mLevel - 1) * 5;
    }

    public int calculateScore(int pSameBubbleCount) {
        if (pSameBubbleCount <= 1) {
            return 0;
        }
        return getBonus() * pSameBubbleCount;
    }

    public void setMain(Bubble newMain) {
        this.mRefMain = newMain;
    }

    public void exitView() {
        this.mAlive = false;
        this.mTimerDraw.stopTimer();
        this.mTimerJob.cancel();
        saveResumeData();
        clearsound();
        freeAlloc();
    }

    private void loadResImage() {
        this.mBubbleArray = new Bitmap[7];
        this.mBubble2Array = new Bitmap[7];
        this.mBubble3Array = new Bitmap[7];
        Resources res = getContext().getResources();
        Log.v("BBView", "initResImg(): init....");
        this.mBmpWellDone = Bitmap.createBitmap(BitmapFactory.decodeResource(res, R.drawable.bg_welldone));
        this.mBubbleArray[0] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(res, R.drawable.a1), 38, 38, true);
        this.mBubbleArray[1] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(res, R.drawable.a2), 38, 38, true);
        this.mBubbleArray[2] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(res, R.drawable.a3), 38, 38, true);
        this.mBubbleArray[3] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(res, R.drawable.a4), 38, 38, true);
        this.mBubbleArray[4] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(res, R.drawable.a5), 38, 38, true);
        this.mBubbleArray[5] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(res, R.drawable.a6), 38, 38, true);
        this.mBubbleArray[6] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(res, R.drawable.a7), 38, 38, true);
        this.mBubble2Array[0] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(res, R.drawable.a1_1), 38, 38, true);
        this.mBubble2Array[1] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(res, R.drawable.a2_1), 38, 38, true);
        this.mBubble2Array[2] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(res, R.drawable.a3_1), 38, 38, true);
        this.mBubble2Array[3] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(res, R.drawable.a4_1), 38, 38, true);
        this.mBubble2Array[4] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(res, R.drawable.a5_1), 38, 38, true);
        this.mBubble2Array[5] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(res, R.drawable.a6_1), 38, 38, true);
        this.mBubble2Array[6] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(res, R.drawable.a7_1), 38, 38, true);
        this.mBubble3Array[0] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(res, R.drawable.a1_2), 38, 38, true);
        this.mBubble3Array[1] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(res, R.drawable.a2_2), 38, 38, true);
        this.mBubble3Array[2] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(res, R.drawable.a3_2), 38, 38, true);
        this.mBubble3Array[3] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(res, R.drawable.a4_2), 38, 38, true);
        this.mBubble3Array[4] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(res, R.drawable.a5_2), 38, 38, true);
        this.mBubble3Array[5] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(res, R.drawable.a6_2), 38, 38, true);
        this.mBubble3Array[6] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(res, R.drawable.a7_2), 38, 38, true);
        this.mBmpTimebar = Bitmap.createBitmap(BitmapFactory.decodeResource(res, R.drawable.timebar));
        this.mBmpTimebarFill = Bitmap.createBitmap(BitmapFactory.decodeResource(res, R.drawable.timebar_fill));
        this.mBmpBG = new Bitmap[3];
        this.mBmpBG[0] = Bitmap.createBitmap(BitmapFactory.decodeResource(res, R.drawable.bg1));
        this.mBmpBG[1] = Bitmap.createBitmap(BitmapFactory.decodeResource(res, R.drawable.bg2));
        this.mBmpBG[2] = Bitmap.createBitmap(BitmapFactory.decodeResource(res, R.drawable.bg3));
        this.mBmpTvWave = new Bitmap[4];
        this.mBmpTvWave[0] = Bitmap.createBitmap(BitmapFactory.decodeResource(res, R.drawable.tv_wave_1));
        this.mBmpTvWave[1] = Bitmap.createBitmap(BitmapFactory.decodeResource(res, R.drawable.tv_wave_2));
        this.mBmpTvWave[2] = Bitmap.createBitmap(BitmapFactory.decodeResource(res, R.drawable.tv_wave_3));
        this.mBmpTvWave[3] = Bitmap.createBitmap(BitmapFactory.decodeResource(res, R.drawable.tv_wave_4));
        int resId = R.drawable.bg_mask;
        if (mViewHeight > 480) {
            resId = R.drawable.bg_mask2;
        }
        this.mBmpMask = Bitmap.createBitmap(BitmapFactory.decodeResource(res, resId));
    }

    private void saveResumeData() {
        if (this.mBBMatrix != null) {
            SharedPreferences.Editor editor = this.mRefMain.getSharedPreferences(BBConstants.KEY_CFG_BASE, 1).edit();
            editor.putInt("TotalScore", this.mTotalScore);
            if (!this.mBBMatrix.isBBMatrixSolvable()) {
                editor.putBoolean("IsNewLevel", true);
                editor.putInt("Level", this.mLevel + 1);
                editor.putInt("CurScore", 0);
                editor.putInt("Time", 0);
            } else {
                editor.putBoolean("IsNewLevel", false);
                editor.putInt("Level", this.mLevel);
                editor.putInt("CurScore", this.mScore);
                editor.putInt("Time", this.mTaskPassTime);
            }
            new String("");
            for (int i = 0; i < this.mBBMatrix.mXBubbleCount; i++) {
                for (int j = 0; j < this.mBBMatrix.mYBubbleCount; j++) {
                    editor.putInt("matrix_" + i + "_" + j, this.mBBMatrix.mBubbleGrid[i][j]);
                }
            }
            editor.commit();
            Log.v("BBView", "saveResumeData(): save finish.");
        }
    }

    public boolean loadLevelData() {
        SharedPreferences gamePreferences = this.mRefMain.getSharedPreferences(BBConstants.KEY_CFG_BASE, 1);
        int totalScore = gamePreferences.getInt("TotalScore", -1);
        int curScore = gamePreferences.getInt("CurScore", -1);
        int curLevel = gamePreferences.getInt("Level", -1);
        boolean isNewLevel = gamePreferences.getBoolean("IsNewLevel", true);
        int passTime = gamePreferences.getInt("Time", -1);
        if (totalScore == -1 || curScore == -1 || curLevel == -1 || passTime == -1) {
            return false;
        }
        this.mTotalScore = totalScore;
        this.mScore = curScore;
        this.mLevel = curLevel;
        this.mIsResumeNewLevel = isNewLevel;
        this.mTaskPassTime = passTime;
        Log.v("BBView", "loadLevelData(): passTime=" + this.mTaskPassTime);
        return true;
    }

    public boolean loadBBMatrixData() {
        if (this.mBBMatrix == null) {
            return false;
        }
        Log.v("BBView", "loadBBMatrixData()....");
        SharedPreferences gamePreferences = this.mRefMain.getSharedPreferences(BBConstants.KEY_CFG_BASE, 1);
        new String("");
        for (int i = 0; i < this.mBBMatrix.mXBubbleCount; i++) {
            for (int j = 0; j < this.mBBMatrix.mYBubbleCount; j++) {
                int bId = gamePreferences.getInt("matrix_" + i + "_" + j, -1);
                if (bId < -1 || bId >= 7) {
                    bId = -1;
                }
                this.mBBMatrix.mBubbleGrid[i][j] = bId;
            }
        }
        return true;
    }
}
