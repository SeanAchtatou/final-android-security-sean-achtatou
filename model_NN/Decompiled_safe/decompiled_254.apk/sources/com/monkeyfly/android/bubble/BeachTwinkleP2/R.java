package com.monkeyfly.android.bubble.BeachTwinkleP2;

public final class R {

    public static final class anim {
        public static final int logo_move = 2130968576;
    }

    public static final class array {
        public static final int game_modes = 2131099648;
        public static final int sl_game_modes = 2131099649;
    }

    public static final class attr {
        public static final int backgroundColor = 2130771968;
        public static final int keywords = 2130771971;
        public static final int primaryTextColor = 2130771969;
        public static final int refreshInterval = 2130771972;
        public static final int secondaryTextColor = 2130771970;
        public static final int textColor = 2130771973;
    }

    public static final class color {
        public static final int default_error_text = 2131165186;
        public static final int default_highlight_background = 2131165184;
        public static final int sl_selector_color = 2131165187;
        public static final int user_info_background = 2131165185;
    }

    public static final class drawable {
        public static final int a1 = 2130837504;
        public static final int a1_1 = 2130837505;
        public static final int a1_2 = 2130837506;
        public static final int a2 = 2130837507;
        public static final int a2_1 = 2130837508;
        public static final int a2_2 = 2130837509;
        public static final int a3 = 2130837510;
        public static final int a3_1 = 2130837511;
        public static final int a3_2 = 2130837512;
        public static final int a4 = 2130837513;
        public static final int a4_1 = 2130837514;
        public static final int a4_2 = 2130837515;
        public static final int a5 = 2130837516;
        public static final int a5_1 = 2130837517;
        public static final int a5_2 = 2130837518;
        public static final int a6 = 2130837519;
        public static final int a6_1 = 2130837520;
        public static final int a6_2 = 2130837521;
        public static final int a7 = 2130837522;
        public static final int a7_1 = 2130837523;
        public static final int a7_2 = 2130837524;
        public static final int bg1 = 2130837525;
        public static final int bg2 = 2130837526;
        public static final int bg3 = 2130837527;
        public static final int bg_mask = 2130837528;
        public static final int bg_mask2 = 2130837529;
        public static final int bg_menu = 2130837530;
        public static final int bg_welldone = 2130837531;
        public static final int button_background_focus = 2130837532;
        public static final int button_background_focus2 = 2130837533;
        public static final int button_background_normal = 2130837534;
        public static final int button_background_normal2 = 2130837535;
        public static final int button_background_normal_old = 2130837536;
        public static final int button_background_pressed = 2130837537;
        public static final int button_background_pressed2 = 2130837538;
        public static final int button_background_pressed_old = 2130837539;
        public static final int dialog_divider_horizontal_light = 2130837540;
        public static final int game_logo = 2130837541;
        public static final int icon = 2130837542;
        public static final int menu_my = 2130837543;
        public static final int select = 2130837544;
        public static final int sl_bg_btn = 2130837545;
        public static final int sl_bg_btn_pre = 2130837546;
        public static final int sl_bg_dropdown = 2130837547;
        public static final int sl_bg_dropdown_pre = 2130837548;
        public static final int sl_bg_h1 = 2130837549;
        public static final int sl_bg_list = 2130837550;
        public static final int sl_bg_list_pre = 2130837551;
        public static final int sl_divider = 2130837552;
        public static final int sl_divider_list = 2130837553;
        public static final int sl_logo = 2130837554;
        public static final int sl_menu_highscores = 2130837555;
        public static final int sl_menu_profile = 2130837556;
        public static final int sl_selector_btn = 2130837557;
        public static final int sl_selector_dropdown = 2130837558;
        public static final int sl_selector_list = 2130837559;
        public static final int timebar = 2130837560;
        public static final int timebar_fill = 2130837561;
        public static final int top = 2130837562;
        public static final int tv_wave_1 = 2130837563;
        public static final int tv_wave_2 = 2130837564;
        public static final int tv_wave_3 = 2130837565;
        public static final int tv_wave_4 = 2130837566;
    }

    public static final class id {
        public static final int Button01 = 2131361794;
        public static final int Button02 = 2131361795;
        public static final int TextView02 = 2131361813;
        public static final int ad1 = 2131361802;
        public static final int ad2 = 2131361797;
        public static final int ad_bottom = 2131361793;
        public static final int btn_back = 2131361801;
        public static final int bubblebreaker = 2131361792;
        public static final int email = 2131361812;
        public static final int exit = 2131361821;
        public static final int game_logo = 2131361816;
        public static final int game_mode_spinner = 2131361805;
        public static final int highscores_list_item = 2131361808;
        public static final int list_view = 2131361806;
        public static final int login = 2131361810;
        public static final int menu = 2131361796;
        public static final int myscore_view = 2131361807;
        public static final int options_sounds_checkbox = 2131361800;
        public static final int options_vibrate_checkbox = 2131361799;
        public static final int progress_indicator = 2131361804;
        public static final int rank = 2131361809;
        public static final int resume = 2131361818;
        public static final int score = 2131361811;
        public static final int score_board = 2131361820;
        public static final int scrollview2 = 2131361798;
        public static final int setting = 2131361819;
        public static final int spinnerTarget = 2131361815;
        public static final int start_game = 2131361817;
        public static final int title_login = 2131361803;
        public static final int update_button = 2131361814;
    }

    public static final class layout {
        public static final int bb_layout = 2130903040;
        public static final int main = 2130903041;
        public static final int menu = 2130903042;
        public static final int options = 2130903043;
        public static final int sl_highscores = 2130903044;
        public static final int sl_highscores_list_item = 2130903045;
        public static final int sl_profile = 2130903046;
        public static final int sl_spinner_item = 2130903047;
        public static final int splash = 2130903048;
    }

    public static final class raw {
        public static final int bg_g1 = 2131034112;
        public static final int bg_g2 = 2131034113;
        public static final int bg_g3 = 2131034114;
        public static final int bg_g4 = 2131034115;
        public static final int bg_menu = 2131034116;
        public static final int drip1 = 2131034117;
        public static final int drip2 = 2131034118;
        public static final int drip3 = 2131034119;
        public static final int drip4 = 2131034120;
        public static final int drip5 = 2131034121;
        public static final int drip6 = 2131034122;
        public static final int game_over = 2131034123;
        public static final int press = 2131034124;
        public static final int press2 = 2131034125;
        public static final int press_btn = 2131034126;
        public static final int well_done = 2131034127;
    }

    public static final class string {
        public static final int Menu_Close_Bg = 2131230834;
        public static final int Menu_Close_Ck = 2131230836;
        public static final int Menu_Open_Bg = 2131230833;
        public static final int Menu_Open_Ck = 2131230835;
        public static final int app_name = 2131230733;
        public static final int bb_layout_text_text = 2131230828;
        public static final int btn_accept_challenge = 2131230753;
        public static final int btn_check_rank = 2131230745;
        public static final int btn_choose_opponent = 2131230752;
        public static final int btn_directly_challenge_this_user = 2131230750;
        public static final int btn_game_over = 2131230744;
        public static final int btn_load_next = 2131230747;
        public static final int btn_load_prev = 2131230746;
        public static final int btn_open_url = 2131230741;
        public static final int btn_play_challenge = 2131230751;
        public static final int btn_random_score = 2131230743;
        public static final int btn_reject_challenge = 2131230754;
        public static final int btn_show_me = 2131230748;
        public static final int btn_update_profile = 2131230749;
        public static final int bubble_breaker = 2131230827;
        public static final int chalenge_stake_cap = 2131230760;
        public static final int challenge_anyone = 2131230763;
        public static final int challenge_anyone_cap = 2131230762;
        public static final int challenge_assigned = 2131230768;
        public static final int challenge_complete = 2131230769;
        public static final int challenge_confirmation_format = 2131230782;
        public static final int challenge_invalid_cap = 2131230761;
        public static final int challenge_new_menu_item_direct = 2131230759;
        public static final int challenge_new_menu_item_open = 2131230758;
        public static final int challenge_open = 2131230770;
        public static final int challenge_open_cap = 2131230765;
        public static final int challenge_opponent_format = 2131230783;
        public static final int challenge_other_cap = 2131230766;
        public static final int challenge_pending_cap = 2131230767;
        public static final int challenge_rejected_cap = 2131230764;
        public static final int challenge_score_result_format = 2131230774;
        public static final int challenge_stake_lost_format = 2131230773;
        public static final int challenge_stake_won_format = 2131230772;
        public static final int challenge_tab_history = 2131230756;
        public static final int challenge_tab_new = 2131230757;
        public static final int challenge_tab_open = 2131230755;
        public static final int challenge_won_lost_format = 2131230771;
        public static final int error_balance_to_low = 2131230797;
        public static final int error_message_cannot_accept_challenge = 2131230786;
        public static final int error_message_cannot_reject_challenge = 2131230787;
        public static final int error_message_challenge_upload = 2131230789;
        public static final int error_message_email_already_taken = 2131230795;
        public static final int error_message_insufficient_balance = 2131230788;
        public static final int error_message_invalid_email_format = 2131230796;
        public static final int error_message_name_already_taken = 2131230794;
        public static final int error_message_network = 2131230793;
        public static final int error_message_not_on_highscore_list = 2131230792;
        public static final int error_message_request_cancelled = 2131230790;
        public static final int error_message_self_challenge = 2131230785;
        public static final int error_message_user_info_update_failed = 2131230791;
        public static final int facebook_comm_cancelled = 2131230817;
        public static final int facebook_comm_failed = 2131230816;
        public static final int facebook_comm_ok = 2131230815;
        public static final int facebook_invalid_credentials = 2131230818;
        public static final int game_mode_label = 2131230798;
        public static final int game_score_label = 2131230799;
        public static final int history_challenge_info_format = 2131230784;
        public static final int lost = 2131230811;
        public static final int mExit = 2131230838;
        public static final int mScore = 2131230837;
        public static final int menu_item_challenges = 2131230736;
        public static final int menu_item_highscores = 2131230735;
        public static final int menu_item_payment = 2131230740;
        public static final int menu_item_play = 2131230734;
        public static final int menu_item_post_message = 2131230739;
        public static final int menu_item_profile = 2131230738;
        public static final int menu_item_users = 2131230737;
        public static final int message_no_auth = 2131230742;
        public static final int message_post_failed = 2131230814;
        public static final int message_post_ok = 2131230813;
        public static final int mode_lose_prefix = 2131230831;
        public static final int mode_lose_suffix = 2131230832;
        public static final int mode_pause = 2131230830;
        public static final int mode_ready = 2131230829;
        public static final int money_format = 2131230812;
        public static final int myspace_comm_cancelled = 2131230821;
        public static final int myspace_comm_failed = 2131230820;
        public static final int myspace_comm_ok = 2131230819;
        public static final int myspace_invalid_credentials = 2131230822;
        public static final int name_cap = 2131230808;
        public static final int new_challenge_balance = 2131230780;
        public static final int new_challenge_stake_header = 2131230781;
        public static final int new_direct_challenge = 2131230778;
        public static final int new_open_challenge = 2131230779;
        public static final int play_for_random_score = 2131230806;
        public static final int profile_email_label = 2131230777;
        public static final int profile_name_label = 2131230776;
        public static final int progress_message_default = 2131230807;
        public static final int ranking_check_failed = 2131230804;
        public static final int ranking_check_label = 2131230805;
        public static final int score_submit_failed = 2131230803;
        public static final int sl_email = 2131230724;
        public static final int sl_error_message_email_already_taken = 2131230731;
        public static final int sl_error_message_invalid_email_format = 2131230732;
        public static final int sl_error_message_name_already_taken = 2131230730;
        public static final int sl_error_message_network = 2131230729;
        public static final int sl_error_message_not_on_highscore_list = 2131230728;
        public static final int sl_highscores = 2131230721;
        public static final int sl_login = 2131230723;
        public static final int sl_next = 2131230726;
        public static final int sl_prev = 2131230725;
        public static final int sl_profile = 2131230720;
        public static final int sl_top = 2131230727;
        public static final int sl_update_profile = 2131230722;
        public static final int status_cap = 2131230809;
        public static final int submitted_challenge_score_info = 2131230802;
        public static final int submitted_challenge_score_label = 2131230801;
        public static final int submitted_score_label = 2131230800;
        public static final int twitter_comm_cancelled = 2131230825;
        public static final int twitter_comm_failed = 2131230824;
        public static final int twitter_comm_ok = 2131230823;
        public static final int twitter_invalid_credentials = 2131230826;
        public static final int user_info_format = 2131230775;
        public static final int won = 2131230810;
    }

    public static final class style {
        public static final int sl_heading = 2131296256;
        public static final int sl_normal = 2131296258;
        public static final int sl_title_bar = 2131296257;
    }

    public static final class styleable {
        public static final int[] com_admob_android_ads_AdView = {R.attr.backgroundColor, R.attr.primaryTextColor, R.attr.secondaryTextColor, R.attr.keywords, R.attr.refreshInterval, R.attr.textColor};
        public static final int com_admob_android_ads_AdView_backgroundColor = 0;
        public static final int com_admob_android_ads_AdView_keywords = 3;
        public static final int com_admob_android_ads_AdView_primaryTextColor = 1;
        public static final int com_admob_android_ads_AdView_refreshInterval = 4;
        public static final int com_admob_android_ads_AdView_secondaryTextColor = 2;
        public static final int com_admob_android_ads_AdView_textColor = 5;
    }
}
