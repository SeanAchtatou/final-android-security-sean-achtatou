package com.admob.android.ads;

import java.util.Map;
import org.json.JSONObject;

/* compiled from: AdMobConnectorFactory */
public final class e {

    /* renamed from: a  reason: collision with root package name */
    private static boolean f32a = false;

    public static r a(String str, String str2, String str3, h hVar, int i, Map<String, String> map, String str4) {
        return new i(str, str2, str3, hVar, i, null, str4);
    }

    public static r a(String str, String str2, String str3, JSONObject jSONObject, h hVar) {
        r a2 = a(str, str2, str3, hVar, 5000, null, jSONObject == null ? null : jSONObject.toString());
        a2.a("application/json");
        return a2;
    }

    public static r a(String str, String str2, String str3, h hVar) {
        return a(str, str2, str3, hVar, 5000, null, null);
    }

    public static r a(String str, String str2, String str3, h hVar, int i) {
        r a2 = a(str, null, str3, hVar, 5000, null, null);
        if (a2 != null) {
            a2.a(1);
        }
        return a2;
    }

    public static r a(String str, String str2, String str3) {
        return a(str, str2, str3, null);
    }
}
