package com.admob.android.ads;

import android.util.Log;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

/* compiled from: AdMobURLConnector */
final class i extends r {
    private HttpURLConnection m;
    private URL n;

    public i(String str, String str2, String str3, h hVar, int i, Map<String, String> map, String str4) {
        super(str2, str3, hVar, i, map, str4);
        try {
            this.n = new URL(str);
            this.i = this.n;
        } catch (MalformedURLException e) {
            this.n = null;
            this.c = e;
        }
        this.m = null;
        this.e = 0;
    }

    /* JADX WARNING: Removed duplicated region for block: B:41:0x00e8 A[SYNTHETIC, Splitter:B:41:0x00e8] */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x01cf A[SYNTHETIC, Splitter:B:82:0x01cf] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a() {
        /*
            r13 = this;
            r11 = 2
            r10 = 1
            r9 = 0
            java.lang.String r8 = "AdMobSDK"
            java.net.URL r1 = r13.n
            if (r1 != 0) goto L_0x0028
            com.admob.android.ads.h r1 = r13.h
            if (r1 == 0) goto L_0x0019
            com.admob.android.ads.h r1 = r13.h
            java.lang.Exception r2 = new java.lang.Exception
            java.lang.String r3 = "url was null"
            r2.<init>(r3)
            r1.a(r13, r2)
        L_0x0019:
            r1 = r9
        L_0x001a:
            if (r1 != 0) goto L_0x0027
            com.admob.android.ads.h r2 = r13.h
            if (r2 == 0) goto L_0x0027
            com.admob.android.ads.h r2 = r13.h
            java.lang.Exception r3 = r13.c
            r2.a(r13, r3)
        L_0x0027:
            return r1
        L_0x0028:
            java.net.HttpURLConnection.setFollowRedirects(r10)
            r3 = r9
        L_0x002c:
            int r1 = r13.e
            int r2 = r13.f
            if (r1 >= r2) goto L_0x01ea
            if (r3 != 0) goto L_0x01ea
            java.lang.String r1 = "AdMobSDK"
            boolean r1 = android.util.Log.isLoggable(r8, r11)
            if (r1 == 0) goto L_0x0062
            java.lang.String r1 = "AdMobSDK"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "attempt "
            java.lang.StringBuilder r1 = r1.append(r2)
            int r2 = r13.e
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = " to connect to url "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.net.URL r2 = r13.n
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            android.util.Log.v(r8, r1)
        L_0x0062:
            r4 = 0
            r13.h()     // Catch:{ Exception -> 0x00c8, all -> 0x01cb }
            java.net.URL r1 = r13.n     // Catch:{ Exception -> 0x00c8, all -> 0x01cb }
            java.net.URLConnection r1 = r1.openConnection()     // Catch:{ Exception -> 0x00c8, all -> 0x01cb }
            java.net.HttpURLConnection r1 = (java.net.HttpURLConnection) r1     // Catch:{ Exception -> 0x00c8, all -> 0x01cb }
            r13.m = r1     // Catch:{ Exception -> 0x00c8, all -> 0x01cb }
            java.net.HttpURLConnection r1 = r13.m     // Catch:{ Exception -> 0x00c8, all -> 0x01cb }
            if (r1 == 0) goto L_0x01e7
            java.net.HttpURLConnection r1 = r13.m     // Catch:{ Exception -> 0x00c8, all -> 0x01cb }
            java.lang.String r2 = "User-Agent"
            java.lang.String r5 = c()     // Catch:{ Exception -> 0x00c8, all -> 0x01cb }
            r1.setRequestProperty(r2, r5)     // Catch:{ Exception -> 0x00c8, all -> 0x01cb }
            java.lang.String r1 = r13.g     // Catch:{ Exception -> 0x00c8, all -> 0x01cb }
            if (r1 == 0) goto L_0x008c
            java.net.HttpURLConnection r1 = r13.m     // Catch:{ Exception -> 0x00c8, all -> 0x01cb }
            java.lang.String r2 = "X-ADMOB-ISU"
            java.lang.String r5 = r13.g     // Catch:{ Exception -> 0x00c8, all -> 0x01cb }
            r1.setRequestProperty(r2, r5)     // Catch:{ Exception -> 0x00c8, all -> 0x01cb }
        L_0x008c:
            java.net.HttpURLConnection r1 = r13.m     // Catch:{ Exception -> 0x00c8, all -> 0x01cb }
            int r2 = r13.b     // Catch:{ Exception -> 0x00c8, all -> 0x01cb }
            r1.setConnectTimeout(r2)     // Catch:{ Exception -> 0x00c8, all -> 0x01cb }
            java.net.HttpURLConnection r1 = r13.m     // Catch:{ Exception -> 0x00c8, all -> 0x01cb }
            int r2 = r13.b     // Catch:{ Exception -> 0x00c8, all -> 0x01cb }
            r1.setReadTimeout(r2)     // Catch:{ Exception -> 0x00c8, all -> 0x01cb }
            java.util.Map r1 = r13.d     // Catch:{ Exception -> 0x00c8, all -> 0x01cb }
            if (r1 == 0) goto L_0x00f8
            java.util.Map r1 = r13.d     // Catch:{ Exception -> 0x00c8, all -> 0x01cb }
            java.util.Set r1 = r1.keySet()     // Catch:{ Exception -> 0x00c8, all -> 0x01cb }
            java.util.Iterator r5 = r1.iterator()     // Catch:{ Exception -> 0x00c8, all -> 0x01cb }
        L_0x00a8:
            boolean r1 = r5.hasNext()     // Catch:{ Exception -> 0x00c8, all -> 0x01cb }
            if (r1 == 0) goto L_0x00f8
            java.lang.Object r1 = r5.next()     // Catch:{ Exception -> 0x00c8, all -> 0x01cb }
            r0 = r1
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x00c8, all -> 0x01cb }
            r2 = r0
            if (r2 == 0) goto L_0x00a8
            java.util.Map r1 = r13.d     // Catch:{ Exception -> 0x00c8, all -> 0x01cb }
            java.lang.Object r1 = r1.get(r2)     // Catch:{ Exception -> 0x00c8, all -> 0x01cb }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ Exception -> 0x00c8, all -> 0x01cb }
            if (r1 == 0) goto L_0x00a8
            java.net.HttpURLConnection r6 = r13.m     // Catch:{ Exception -> 0x00c8, all -> 0x01cb }
            r6.addRequestProperty(r2, r1)     // Catch:{ Exception -> 0x00c8, all -> 0x01cb }
            goto L_0x00a8
        L_0x00c8:
            r1 = move-exception
            r2 = r4
        L_0x00ca:
            java.lang.String r3 = "AdMobSDK"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x01db }
            r4.<init>()     // Catch:{ all -> 0x01db }
            java.lang.String r5 = "could not open connection to url "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x01db }
            java.net.URL r5 = r13.n     // Catch:{ all -> 0x01db }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x01db }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x01db }
            android.util.Log.w(r3, r4, r1)     // Catch:{ all -> 0x01db }
            r13.c = r1     // Catch:{ all -> 0x01db }
            if (r2 == 0) goto L_0x00eb
            r2.close()     // Catch:{ Exception -> 0x01d6 }
        L_0x00eb:
            r13.h()
            r1 = r9
        L_0x00ef:
            int r2 = r13.e
            int r2 = r2 + 1
            r13.e = r2
            r3 = r1
            goto L_0x002c
        L_0x00f8:
            java.lang.String r1 = r13.l     // Catch:{ Exception -> 0x00c8, all -> 0x01cb }
            if (r1 == 0) goto L_0x01ab
            java.net.HttpURLConnection r1 = r13.m     // Catch:{ Exception -> 0x00c8, all -> 0x01cb }
            java.lang.String r2 = "POST"
            r1.setRequestMethod(r2)     // Catch:{ Exception -> 0x00c8, all -> 0x01cb }
            java.net.HttpURLConnection r1 = r13.m     // Catch:{ Exception -> 0x00c8, all -> 0x01cb }
            r2 = 1
            r1.setDoOutput(r2)     // Catch:{ Exception -> 0x00c8, all -> 0x01cb }
            java.net.HttpURLConnection r1 = r13.m     // Catch:{ Exception -> 0x00c8, all -> 0x01cb }
            java.lang.String r2 = "Content-Type"
            java.lang.String r5 = r13.f43a     // Catch:{ Exception -> 0x00c8, all -> 0x01cb }
            r1.setRequestProperty(r2, r5)     // Catch:{ Exception -> 0x00c8, all -> 0x01cb }
            java.net.HttpURLConnection r1 = r13.m     // Catch:{ Exception -> 0x00c8, all -> 0x01cb }
            java.lang.String r2 = "Content-Length"
            java.lang.String r5 = r13.l     // Catch:{ Exception -> 0x00c8, all -> 0x01cb }
            int r5 = r5.length()     // Catch:{ Exception -> 0x00c8, all -> 0x01cb }
            java.lang.String r5 = java.lang.Integer.toString(r5)     // Catch:{ Exception -> 0x00c8, all -> 0x01cb }
            r1.setRequestProperty(r2, r5)     // Catch:{ Exception -> 0x00c8, all -> 0x01cb }
            java.net.HttpURLConnection r1 = r13.m     // Catch:{ Exception -> 0x00c8, all -> 0x01cb }
            java.io.OutputStream r1 = r1.getOutputStream()     // Catch:{ Exception -> 0x00c8, all -> 0x01cb }
            java.io.BufferedWriter r2 = new java.io.BufferedWriter     // Catch:{ Exception -> 0x00c8, all -> 0x01cb }
            java.io.OutputStreamWriter r5 = new java.io.OutputStreamWriter     // Catch:{ Exception -> 0x00c8, all -> 0x01cb }
            r5.<init>(r1)     // Catch:{ Exception -> 0x00c8, all -> 0x01cb }
            r1 = 4096(0x1000, float:5.74E-42)
            r2.<init>(r5, r1)     // Catch:{ Exception -> 0x00c8, all -> 0x01cb }
            java.lang.String r1 = r13.l     // Catch:{ Exception -> 0x01e2 }
            r2.write(r1)     // Catch:{ Exception -> 0x01e2 }
            r2.close()     // Catch:{ Exception -> 0x01e2 }
            r1 = 0
        L_0x013e:
            java.net.HttpURLConnection r2 = r13.m     // Catch:{ Exception -> 0x01a5, all -> 0x01dd }
            int r2 = r2.getResponseCode()     // Catch:{ Exception -> 0x01a5, all -> 0x01dd }
            java.lang.String r4 = "AdMobSDK"
            r5 = 2
            boolean r4 = android.util.Log.isLoggable(r4, r5)     // Catch:{ Exception -> 0x01a5, all -> 0x01dd }
            if (r4 == 0) goto L_0x016d
            java.net.HttpURLConnection r4 = r13.m     // Catch:{ Exception -> 0x01a5, all -> 0x01dd }
            java.lang.String r5 = "X-AdMob-AdSrc"
            java.lang.String r4 = r4.getHeaderField(r5)     // Catch:{ Exception -> 0x01a5, all -> 0x01dd }
            java.lang.String r5 = "AdMobSDK"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01a5, all -> 0x01dd }
            r6.<init>()     // Catch:{ Exception -> 0x01a5, all -> 0x01dd }
            java.lang.String r7 = "Ad response came from server "
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x01a5, all -> 0x01dd }
            java.lang.StringBuilder r4 = r6.append(r4)     // Catch:{ Exception -> 0x01a5, all -> 0x01dd }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x01a5, all -> 0x01dd }
            android.util.Log.v(r5, r4)     // Catch:{ Exception -> 0x01a5, all -> 0x01dd }
        L_0x016d:
            r4 = 200(0xc8, float:2.8E-43)
            if (r2 < r4) goto L_0x01e5
            r4 = 300(0x12c, float:4.2E-43)
            if (r2 >= r4) goto L_0x01e5
            java.net.HttpURLConnection r2 = r13.m     // Catch:{ Exception -> 0x01a5, all -> 0x01dd }
            java.net.URL r2 = r2.getURL()     // Catch:{ Exception -> 0x01a5, all -> 0x01dd }
            r13.i = r2     // Catch:{ Exception -> 0x01a5, all -> 0x01dd }
            boolean r2 = r13.k     // Catch:{ Exception -> 0x01a5, all -> 0x01dd }
            if (r2 == 0) goto L_0x01b8
            java.io.BufferedInputStream r2 = new java.io.BufferedInputStream     // Catch:{ Exception -> 0x01a5, all -> 0x01dd }
            java.net.HttpURLConnection r3 = r13.m     // Catch:{ Exception -> 0x01a5, all -> 0x01dd }
            java.io.InputStream r3 = r3.getInputStream()     // Catch:{ Exception -> 0x01a5, all -> 0x01dd }
            r4 = 4096(0x1000, float:5.74E-42)
            r2.<init>(r3, r4)     // Catch:{ Exception -> 0x01a5, all -> 0x01dd }
            r3 = 4096(0x1000, float:5.74E-42)
            byte[] r3 = new byte[r3]     // Catch:{ Exception -> 0x01a5, all -> 0x01dd }
            java.io.ByteArrayOutputStream r4 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x01a5, all -> 0x01dd }
            r5 = 4096(0x1000, float:5.74E-42)
            r4.<init>(r5)     // Catch:{ Exception -> 0x01a5, all -> 0x01dd }
        L_0x0199:
            int r5 = r2.read(r3)     // Catch:{ Exception -> 0x01a5, all -> 0x01dd }
            r6 = -1
            if (r5 == r6) goto L_0x01b2
            r6 = 0
            r4.write(r3, r6, r5)     // Catch:{ Exception -> 0x01a5, all -> 0x01dd }
            goto L_0x0199
        L_0x01a5:
            r2 = move-exception
            r12 = r2
            r2 = r1
            r1 = r12
            goto L_0x00ca
        L_0x01ab:
            java.net.HttpURLConnection r1 = r13.m     // Catch:{ Exception -> 0x00c8, all -> 0x01cb }
            r1.connect()     // Catch:{ Exception -> 0x00c8, all -> 0x01cb }
            r1 = r4
            goto L_0x013e
        L_0x01b2:
            byte[] r2 = r4.toByteArray()     // Catch:{ Exception -> 0x01a5, all -> 0x01dd }
            r13.j = r2     // Catch:{ Exception -> 0x01a5, all -> 0x01dd }
        L_0x01b8:
            com.admob.android.ads.h r2 = r13.h     // Catch:{ Exception -> 0x01a5, all -> 0x01dd }
            if (r2 == 0) goto L_0x01c1
            com.admob.android.ads.h r2 = r13.h     // Catch:{ Exception -> 0x01a5, all -> 0x01dd }
            r2.a(r13)     // Catch:{ Exception -> 0x01a5, all -> 0x01dd }
        L_0x01c1:
            r2 = r10
        L_0x01c2:
            r13.h()     // Catch:{ Exception -> 0x01a5, all -> 0x01dd }
            r13.h()
            r1 = r2
            goto L_0x00ef
        L_0x01cb:
            r1 = move-exception
            r2 = r4
        L_0x01cd:
            if (r2 == 0) goto L_0x01d2
            r2.close()     // Catch:{ Exception -> 0x01d9 }
        L_0x01d2:
            r13.h()
            throw r1
        L_0x01d6:
            r1 = move-exception
            goto L_0x00eb
        L_0x01d9:
            r2 = move-exception
            goto L_0x01d2
        L_0x01db:
            r1 = move-exception
            goto L_0x01cd
        L_0x01dd:
            r2 = move-exception
            r12 = r2
            r2 = r1
            r1 = r12
            goto L_0x01cd
        L_0x01e2:
            r1 = move-exception
            goto L_0x00ca
        L_0x01e5:
            r2 = r3
            goto L_0x01c2
        L_0x01e7:
            r1 = r4
            r2 = r3
            goto L_0x01c2
        L_0x01ea:
            r1 = r3
            goto L_0x001a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.admob.android.ads.i.a():boolean");
    }

    private void h() {
        if (this.m != null) {
            this.m.disconnect();
            this.m = null;
        }
    }

    public final void b() {
        h();
        this.h = null;
    }

    public final void run() {
        try {
            a();
        } catch (Exception e) {
            if (Log.isLoggable(AdManager.LOG, 6)) {
                Log.e(AdManager.LOG, "exception caught in AdMobURLConnector.run(), " + e.getMessage());
            }
        }
    }
}
