package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.model.Ranking;
import com.scoreloop.client.android.core.model.Score;
import com.scoreloop.client.android.core.model.SearchList;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.Response;
import java.util.Map;
import org.json.JSONArray;

public class RankingController extends RequestController {
    private Ranking c;
    private SearchList d;

    public RankingController(RequestControllerObserver requestControllerObserver) {
        this(Session.getCurrentSession(), requestControllerObserver);
    }

    public RankingController(Session session, RequestControllerObserver requestControllerObserver) {
        super(session, requestControllerObserver);
    }

    /* access modifiers changed from: package-private */
    public boolean a(Request request, Response response) {
        if (response.f() != 200) {
            throw new Exception("Request failed");
        }
        JSONArray d2 = response.d();
        this.c = new Ranking();
        this.c.a(d2.getJSONObject(0).getJSONObject("ranking"));
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean g() {
        return true;
    }

    public Ranking getRanking() {
        return this.c;
    }

    public SearchList getSearchList() {
        return this.d;
    }

    public void requestRankingForScore(Score score) {
        if (score == null) {
            throw new IllegalArgumentException("score parameter cannot be null");
        } else if (this.d == null) {
            throw new IllegalArgumentException("Search list or user is required for score ranking");
        } else {
            Q q = new Q(d(), b(), this.d, null, score, score.getMode());
            h();
            a(q);
        }
    }

    public void requestRankingForScoreResult(Double d2, Map map) {
        requestRankingForScore(new Score(d2, map, e().getUser()));
    }

    public void requestRankingForUserInGameMode(User user, int i) {
        if (user == null) {
            throw new IllegalArgumentException("user paramter cannot be null");
        }
        Q q = new Q(d(), b(), this.d, user, null, Integer.valueOf(i));
        h();
        a(q);
    }

    public void setSearchList(SearchList searchList) {
        this.d = searchList;
    }
}
