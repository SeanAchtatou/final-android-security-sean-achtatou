package com.scoreloop.client.android.core.ui;

import android.app.Activity;
import com.scoreloop.client.android.core.controller.SocialProviderControllerObserver;

public abstract class AuthViewController {

    /* renamed from: a  reason: collision with root package name */
    private SocialProviderControllerObserver f123a;

    public AuthViewController(SocialProviderControllerObserver socialProviderControllerObserver) {
        this.f123a = socialProviderControllerObserver;
        if (this.f123a == null) {
            this.f123a = new f(this);
        }
    }

    public SocialProviderControllerObserver a() {
        return this.f123a;
    }

    public abstract void a(Activity activity);
}
