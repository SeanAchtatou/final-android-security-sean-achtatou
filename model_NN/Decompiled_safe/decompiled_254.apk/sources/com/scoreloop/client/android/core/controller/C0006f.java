package com.scoreloop.client.android.core.controller;

/* renamed from: com.scoreloop.client.android.core.controller.f  reason: case insensitive filesystem */
class C0006f implements ChallengeControllerObserver {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ScoreController f73a;

    private C0006f(ScoreController scoreController) {
        this.f73a = scoreController;
    }

    public void onCannotAcceptChallenge(ChallengeController challengeController) {
        throw new IllegalStateException();
    }

    public void onCannotRejectChallenge(ChallengeController challengeController) {
        throw new IllegalStateException();
    }

    public void onInsufficientBalance(ChallengeController challengeController) {
        throw new IllegalStateException();
    }

    public void requestControllerDidFail(RequestController requestController, Exception exc) {
        this.f73a.c().requestControllerDidFail(this.f73a, exc);
    }

    public void requestControllerDidReceiveResponse(RequestController requestController) {
        this.f73a.c().requestControllerDidReceiveResponse(this.f73a);
    }
}
