package com.scoreloop.client.android.core.ui;

import android.content.Context;
import android.view.KeyEvent;
import android.webkit.WebView;
import java.net.MalformedURLException;
import java.net.URL;

public class TwitterAuthDialog extends WebViewDialog {

    /* renamed from: a  reason: collision with root package name */
    private TwitterAuthViewController f131a;

    public TwitterAuthDialog(Context context, int i, TwitterAuthViewController twitterAuthViewController) {
        super(context, i);
        this.f131a = twitterAuthViewController;
    }

    private void c() {
        this.f131a.a().userDidCancel();
        a();
        dismiss();
    }

    private void d() {
        this.f131a.a().didFail(new IllegalStateException("unparsable twitter response"));
        a();
        dismiss();
    }

    /* access modifiers changed from: protected */
    public void a(WebView webView, String str) {
        try {
            URL url = new URL(str);
            if (url.getHost().equalsIgnoreCase("www.scoreloop.com") && url.getPath().equalsIgnoreCase("/twitter/oauth")) {
                String[] split = url.getQuery().split("=");
                String str2 = split[0];
                if (str2.equalsIgnoreCase("done")) {
                    c();
                } else if (split.length == 2) {
                    String str3 = split[1];
                    if (!str2.equalsIgnoreCase("oauth_token")) {
                        return;
                    }
                    if (str3.equals(this.f131a.b())) {
                        this.f131a.a().didSucceed();
                        dismiss();
                        return;
                    }
                    d();
                } else {
                    d();
                }
            }
        } catch (MalformedURLException e) {
            throw new IllegalStateException(e);
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 4) {
            c();
        }
        return super.onKeyDown(i, keyEvent);
    }
}
