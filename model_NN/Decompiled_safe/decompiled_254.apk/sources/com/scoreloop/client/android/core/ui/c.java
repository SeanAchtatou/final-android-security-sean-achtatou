package com.scoreloop.client.android.core.ui;

import android.view.View;
import android.widget.EditText;

class c implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ EditText f136a;
    final /* synthetic */ EditText b;
    final /* synthetic */ MyspaceCredentialsDialog c;

    c(MyspaceCredentialsDialog myspaceCredentialsDialog, EditText editText, EditText editText2) {
        this.c = myspaceCredentialsDialog;
        this.f136a = editText;
        this.b = editText2;
    }

    public void onClick(View view) {
        this.c.f129a.a(this.f136a.getText().toString(), this.b.getText().toString());
    }
}
