package com.scoreloop.client.android.core.model;

import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class MessageReceiver {

    /* renamed from: a  reason: collision with root package name */
    private Object f95a;
    private List b;

    public MessageReceiver(Object obj, List list) {
        this.f95a = obj;
        this.b = list;
    }

    public Object a() {
        return this.f95a;
    }

    public JSONObject b() {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("receiver_type", ((SocialProvider) a()).getIdentifier());
        JSONArray jSONArray = new JSONArray();
        if (this.b != null && this.b.size() > 0) {
            for (User d : this.b) {
                jSONArray.put(d.d());
            }
            jSONObject.put("users", jSONArray);
        }
        return jSONObject;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof MessageReceiver)) {
            return super.equals(obj);
        }
        MessageReceiver messageReceiver = (MessageReceiver) obj;
        if (messageReceiver == this) {
            return true;
        }
        return a().equals(messageReceiver.a());
    }

    public int hashCode() {
        return a().hashCode();
    }
}
