package com.scoreloop.client.android.core.controller;

final class V implements UserControllerObserver {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ FacebookSocialProviderController f68a;

    private V(FacebookSocialProviderController facebookSocialProviderController) {
        this.f68a = facebookSocialProviderController;
    }

    public void onEmailAlreadyTaken(UserController userController) {
        this.f68a.e.a(false);
        this.f68a.d().didFail(new IllegalStateException());
    }

    public void onEmailInvalidFormat(UserController userController) {
        this.f68a.e.a(false);
        this.f68a.d().didFail(new IllegalStateException());
    }

    public void onUsernameAlreadyTaken(UserController userController) {
        this.f68a.e.a(false);
        this.f68a.d().didFail(new IllegalStateException());
    }

    public void requestControllerDidFail(RequestController requestController, Exception exc) {
        this.f68a.e.a(false);
        this.f68a.d().didFail(exc);
    }

    public void requestControllerDidReceiveResponse(RequestController requestController) {
        this.f68a.a();
    }
}
