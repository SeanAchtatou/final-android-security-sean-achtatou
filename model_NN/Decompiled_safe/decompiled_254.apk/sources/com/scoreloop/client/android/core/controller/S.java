package com.scoreloop.client.android.core.controller;

enum S {
    ASCENDING("ASC"),
    DESCENDING("DESC");
    
    private final String c;

    private S(String str) {
        this.c = str;
    }

    public String toString() {
        return this.c;
    }
}
