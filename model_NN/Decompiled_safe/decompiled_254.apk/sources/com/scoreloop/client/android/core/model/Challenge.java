package com.scoreloop.client.android.core.model;

import com.scoreloop.client.android.core.utils.Formats;
import com.scoreloop.client.android.core.utils.JSONUtils;
import java.text.ParseException;
import java.util.Date;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class Challenge implements MessageTarget {

    /* renamed from: a  reason: collision with root package name */
    private Date f88a;
    private User b;
    private Score c;
    private Integer d;
    private User e;
    private Score f;
    private Integer g;
    private Map h;
    private Date i;
    private String j;
    private String k;
    private Integer l;
    private User m;
    private Integer n;
    private Money o;
    private Money p;
    private Money q;
    private Money r;
    private Money s;
    private String t;
    private User u;

    public Challenge(Money money) {
        a(money);
        this.t = "created";
    }

    public Challenge(JSONObject jSONObject) {
        a(jSONObject);
    }

    private void a(Money money) {
        this.q = money;
    }

    private String c() {
        return this.t;
    }

    public JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("id", this.k);
        jSONObject.put("state", this.t);
        jSONObject.put("level", this.l);
        jSONObject.put("mode", this.n);
        jSONObject.put("game_id", this.j);
        if (this.b != null) {
            jSONObject.put("contender_id", this.b.getIdentifier());
        }
        if (this.u != null) {
            jSONObject.put("winner_id", this.u.getIdentifier());
        }
        if (this.m != null) {
            jSONObject.put("looser_id", this.m.getIdentifier());
        }
        if (this.q != null) {
            jSONObject.put("stake", this.q.c());
        }
        if (this.e != null) {
            String identifier = this.e.getIdentifier();
            if (identifier != null) {
                jSONObject.put("contestant_id", identifier);
            } else {
                jSONObject.put("contestant", this.e.d());
            }
        }
        if (this.f != null) {
            String identifier2 = this.f.getIdentifier();
            if (identifier2 != null) {
                jSONObject.put("contestant_score_id", identifier2);
            } else {
                jSONObject.put("contestant_score", this.f.a());
            }
        }
        if (this.c != null) {
            String identifier3 = this.c.getIdentifier();
            if (identifier3 != null) {
                jSONObject.put("contender_score_id", identifier3);
            } else {
                jSONObject.put("contender_score", this.c.a());
            }
        }
        if (this.h != null) {
            jSONObject.put("context", JSONUtils.a(this.h));
        }
        return jSONObject;
    }

    public void a(Score score) {
        if (this.b == null || this.b.equals(score.getUser())) {
            if (isOpen()) {
                throw new IllegalStateException("Can not modify a already open challenge");
            } else if (isComplete()) {
                throw new IllegalStateException("Can not modify a already open challenge");
            } else {
                this.b = score.getUser();
                this.c = score;
                this.t = "open";
            }
        } else if (this.e != null && !this.e.equals(score.getUser())) {
            throw new IllegalStateException("Can not change already assigned contender or contestant");
        } else if (isOpen() || isAccepted()) {
            this.e = score.getUser();
            this.f = score;
            this.t = "complete";
        } else {
            throw new IllegalStateException("Can not submit a score for a non-open challenge");
        }
    }

    public void a(User user, boolean z) {
        if (!isOpen() && !isAssigned()) {
            if (isAccepted() && (!isAccepted() || !z)) {
                throw new IllegalStateException("Can not accept a rejected challenge");
            } else if (isRejected() && (!isRejected() || z)) {
                throw new IllegalStateException("Can not reject a accepted challenge");
            }
        }
        setContestant(user);
        a(z ? "accepted" : "rejected");
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        this.t = str;
    }

    public void a(JSONObject jSONObject) {
        if (jSONObject.has("id")) {
            this.k = jSONObject.getString("id");
        }
        if (jSONObject.has("state")) {
            this.t = jSONObject.getString("state");
        }
        if (jSONObject.has("level")) {
            this.l = Integer.valueOf(jSONObject.getInt("level"));
        }
        if (jSONObject.has("mode")) {
            this.n = Integer.valueOf(jSONObject.getInt("mode"));
        }
        if (jSONObject.has("game_id")) {
            this.j = jSONObject.getString("game_id");
        }
        if (jSONObject.has("contender_id")) {
            this.b = new User();
            this.b.d(jSONObject.getString("contender_id"));
        }
        if (jSONObject.has("contender")) {
            this.b = new User(jSONObject.getJSONObject("contender"));
        }
        if (jSONObject.has("contestant_id")) {
            this.e = new User();
            this.e.d(jSONObject.getString("contestant_id"));
        }
        if (jSONObject.has("contestant")) {
            this.e = new User(jSONObject.getJSONObject("contestant"));
        }
        if (jSONObject.has("winner")) {
            if (this.b == null || this.e == null) {
                throw new JSONException("winner present but missing contender or contestant");
            } else if (jSONObject.getJSONObject("winner").getString("id").equals(this.b.getIdentifier())) {
                this.u = this.b;
                this.m = this.e;
            } else {
                this.u = this.e;
                this.m = this.b;
            }
        }
        if (jSONObject.has("contender_score")) {
            this.c = new Score(jSONObject.getJSONObject("contender_score"));
        }
        if (jSONObject.has("contestant_score")) {
            this.f = new Score(jSONObject.getJSONObject("contestant_score"));
        }
        if (jSONObject.has("contender_skill_value")) {
            this.d = Integer.valueOf(jSONObject.getInt("contender_skill_value"));
        }
        if (jSONObject.has("contestant_skill_value")) {
            this.g = Integer.valueOf(jSONObject.getInt("contestant_skill_value"));
        }
        if (jSONObject.has("stake")) {
            this.q = new Money(jSONObject.getJSONObject("stake"));
        }
        if (jSONObject.has("price")) {
            this.o = new Money(jSONObject.getJSONObject("price"));
        }
        if (jSONObject.has("stake_in_local_currency")) {
            this.s = new Money(jSONObject.getJSONObject("stake_in_local_currency"));
        } else if (this.q != null) {
            this.s = this.q.clone();
        } else {
            this.q = null;
        }
        if (jSONObject.has("stake_in_contestant_currency")) {
            this.r = new Money(jSONObject.getJSONObject("stake_in_contestant_currency"));
        }
        if (jSONObject.has("price_in_contestant_currency")) {
            this.p = new Money(jSONObject.getJSONObject("price_in_contestant_currency"));
        }
        if (jSONObject.has("created_at")) {
            try {
                this.i = Formats.f149a.parse(jSONObject.getString("created_at"));
            } catch (ParseException e2) {
                throw new JSONException("Invalid format of creation date");
            }
        }
        if (jSONObject.has("completed_at")) {
            try {
                this.f88a = Formats.f149a.parse(jSONObject.getString("completed_at"));
            } catch (ParseException e3) {
                throw new JSONException("Invalid format of completion date");
            }
        }
        if (jSONObject.has("context") && !jSONObject.isNull("context")) {
            this.h = JSONUtils.a(jSONObject.getJSONObject("context"));
        }
    }

    public boolean a(User user) {
        return getIdentifier() == null || ((isOpen() || isAssigned()) && !getContender().equals(user));
    }

    public String b() {
        return "challenge";
    }

    public Date getCompletedAt() {
        return this.f88a;
    }

    public User getContender() {
        return this.b;
    }

    public Score getContenderScore() {
        return this.c;
    }

    public Integer getContenderSkill() {
        return this.d;
    }

    public User getContestant() {
        return this.e;
    }

    public Score getContestantScore() {
        return this.f;
    }

    public Integer getContestantSkill() {
        return this.g;
    }

    public Map getContext() {
        return this.h;
    }

    public Date getCreatedAt() {
        return this.i;
    }

    public String getIdentifier() {
        return this.k;
    }

    public Integer getLevel() {
        return this.l;
    }

    public User getLoser() {
        return this.m;
    }

    public Integer getMode() {
        return this.n;
    }

    public Money getPrice() {
        return this.o;
    }

    public Money getStake() {
        return this.q;
    }

    public User getWinner() {
        return this.u;
    }

    public boolean isAccepted() {
        return "accepted".equalsIgnoreCase(c());
    }

    public boolean isAssigned() {
        return "assigned".equalsIgnoreCase(c());
    }

    public boolean isCancelled() {
        return "cancelled".equalsIgnoreCase(c());
    }

    public boolean isComplete() {
        return "complete".equalsIgnoreCase(c());
    }

    public boolean isCreated() {
        return "created".equalsIgnoreCase(c());
    }

    public boolean isDone() {
        return "done".equalsIgnoreCase(c());
    }

    public boolean isInvalid() {
        return "invalid".equalsIgnoreCase(c());
    }

    public boolean isInvited() {
        return "invited".equalsIgnoreCase(c());
    }

    public boolean isOpen() {
        return "open".equalsIgnoreCase(c());
    }

    public boolean isRejected() {
        return "rejected".equalsIgnoreCase(c());
    }

    public boolean isWinner(User user) {
        if (user != null) {
            return user.equals(getWinner());
        }
        throw new IllegalArgumentException();
    }

    public void setCompletedAt(Date date) {
        this.f88a = date;
    }

    public void setContender(User user) {
        this.b = user;
    }

    public void setContenderScore(Score score) {
        this.c = score;
    }

    public void setContenderSkill(Integer num) {
        this.d = num;
    }

    public void setContestant(User user) {
        this.e = user;
    }

    public void setContestantScore(Score score) {
        this.f = score;
    }

    public void setContestantSkill(Integer num) {
        this.g = num;
    }

    public void setContext(Map map) {
        this.h = map;
    }

    public void setCreatedAt(Date date) {
        this.i = date;
    }

    public void setLevel(Integer num) {
        this.l = num;
    }

    public void setMode(Integer num) {
        this.n = num;
    }
}
