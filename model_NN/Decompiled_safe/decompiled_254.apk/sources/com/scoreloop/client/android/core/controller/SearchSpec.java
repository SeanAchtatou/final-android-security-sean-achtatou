package com.scoreloop.client.android.core.controller;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;

public class SearchSpec {

    /* renamed from: a  reason: collision with root package name */
    private final List f64a;
    private final int b;
    private final C0009i c;

    public SearchSpec() {
        this.b = 25;
        this.f64a = new ArrayList();
        this.c = null;
    }

    public SearchSpec(C0009i iVar) {
        this.b = 25;
        this.f64a = new ArrayList();
        this.c = iVar;
    }

    public JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("per_page", this.b);
        if (this.c != null) {
            jSONObject.put("order_by", this.c.a());
            jSONObject.put("order_as", this.c.b());
        }
        if (this.f64a != null) {
            JSONObject jSONObject2 = new JSONObject();
            for (C0016p pVar : this.f64a) {
                jSONObject2.put(pVar.a(), pVar.b());
            }
            jSONObject.put("conditions", jSONObject2);
        }
        JSONObject jSONObject3 = new JSONObject();
        jSONObject3.put("search", jSONObject);
        return jSONObject3;
    }

    public void a(C0016p pVar) {
        this.f64a.add(pVar);
    }
}
