package com.scoreloop.client.android.core.controller;

class A implements RequestControllerObserver {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SocialProviderController f48a;

    A(SocialProviderController socialProviderController) {
        this.f48a = socialProviderController;
    }

    public void requestControllerDidFail(RequestController requestController, Exception exc) {
        this.f48a.d().didFail(exc);
    }

    public void requestControllerDidReceiveResponse(RequestController requestController) {
        if (requestController.getClass().isAssignableFrom(U.class)) {
            this.f48a.g();
        }
    }
}
