package com.scoreloop.android.coreui;

import android.app.Activity;
import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import com.monkeyfly.android.bubble.BeachTwinkleP2.R;
import com.scoreloop.client.android.core.controller.RequestControllerObserver;
import com.scoreloop.client.android.core.controller.ScoreController;
import com.scoreloop.client.android.core.model.Client;
import com.scoreloop.client.android.core.model.Range;
import com.scoreloop.client.android.core.model.Score;
import com.scoreloop.client.android.core.model.Session;

public abstract class ScoreloopManager {
    static final int GAME_MODE_MIN = 0;
    static Client client;

    public static Spinner getGameModeChooser(Activity context) {
        ArrayAdapter<?> adapter = ArrayAdapter.createFromResource(context, R.array.sl_game_modes, R.layout.sl_spinner_item);
        adapter.setDropDownViewResource(17367049);
        Spinner spinner = (Spinner) context.findViewById(R.id.game_mode_spinner);
        spinner.setAdapter((SpinnerAdapter) adapter);
        return spinner;
    }

    public static void init(Context context, String gameID, String gameSecret) {
        if (client == null) {
            client = new Client(context, gameID, gameSecret, null);
        }
    }

    public static void setNumberOfModes(int modeCount) {
        if (client != null) {
            client.setGameModes(new Range(0, modeCount));
            return;
        }
        throw new IllegalStateException("client object is null. has ScoreloopManager.init() been called?");
    }

    public static void submitScore(int scoreValue, int gameMode, RequestControllerObserver observer) {
        Score score = new Score(Double.valueOf((double) scoreValue), null, Session.getCurrentSession().getUser());
        score.setMode(Integer.valueOf(gameMode));
        new ScoreController(observer).submitScore(score);
    }

    public static void submitScore(int scoreValue, RequestControllerObserver observer) {
        submitScore(scoreValue, 0, observer);
    }
}
