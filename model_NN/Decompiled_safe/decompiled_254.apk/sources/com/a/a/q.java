package com.a.a;

import a.a;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.view.animation.AlphaAnimation;
import android.view.animation.ScaleAnimation;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.net.URI;
import java.net.URL;
import java.util.Iterator;
import java.util.Map;

public class q extends FrameLayout {
    /* access modifiers changed from: private */
    public static final String d = q.class.getSimpleName();
    /* access modifiers changed from: private */
    public static final int e = a.a(0.42578125f, 0.515625f, 0.703125f, 1.0f);
    /* access modifiers changed from: private */
    public static final int f = a.a(0.3f, 0.3f, 0.3f, 0.8f);
    private static final int g = a.a(0.3f, 0.3f, 0.3f, 1.0f);
    private static final int h = a.a(0.23f, 0.35f, 0.6f, 1.0f);

    /* renamed from: a  reason: collision with root package name */
    protected g f9a;
    protected WebView b;
    protected Context c;
    /* access modifiers changed from: private */
    public p i = null;
    /* access modifiers changed from: private */
    public URL j;
    /* access modifiers changed from: private */
    public TextView k;
    private ImageButton l;
    private int m;
    private boolean n;
    private LinearLayout o;
    private b p;

    public q(Context context, g gVar) {
        super(context);
        this.c = context;
        this.f9a = gVar;
        this.j = null;
        this.m = -1;
        this.n = false;
        setWillNotDraw(false);
        setPadding(20, 20, 20, 20);
        this.o = new LinearLayout(context);
        this.o.setOrientation(1);
        this.o.setBackgroundColor(-1);
        this.o.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
        this.k = new TextView(context);
        this.k.setText("Connect to Facebook");
        this.k.setBackgroundColor(e);
        this.k.setTextColor(-1);
        this.k.setTypeface(Typeface.DEFAULT_BOLD);
        this.k.setPadding(8, 4, 8, 4);
        this.k.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
        Drawable a2 = a.a(getClass(), "com/codecarpet/fbconnect/resources/fbicon.png");
        Drawable a3 = a.a(getClass(), "com/codecarpet/fbconnect/resources/close.png");
        this.k.setCompoundDrawablePadding(5);
        this.k.setCompoundDrawablesWithIntrinsicBounds(a2, (Drawable) null, (Drawable) null, (Drawable) null);
        this.l = new ImageButton(context);
        this.l.setBackgroundColor(0);
        this.l.setImageDrawable(a3);
        this.l.setOnTouchListener(new j(this));
        new RelativeLayout.LayoutParams(-1, -1);
        this.b = new WebView(context);
        this.b.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
        this.p = new b(this, null);
        this.b.setWebViewClient(this.p);
        WebSettings settings = this.b.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setDefaultTextEncodingName("UTF-8");
        this.o.addView(this.b);
        addView(this.o);
    }

    private String a(Map map) {
        StringBuilder sb = new StringBuilder();
        StringBuilder append = new StringBuilder("\r\n--").append("3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f").append("\r\n");
        sb.append("--").append("3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f").append("\r\n");
        for (Map.Entry entry : map.entrySet()) {
            sb.append("Content-Disposition: form-data; name=\"").append((String) entry.getKey()).append("\"\r\n\r\n");
            String str = (String) entry.getValue();
            if ("user_message_prompt".equals(entry.getKey())) {
                sb.append(str);
            } else {
                sb.append(a.a((CharSequence) str));
            }
            sb.append((CharSequence) append);
        }
        return sb.toString();
    }

    private URL a(String str, Map map) {
        StringBuilder sb = new StringBuilder(str);
        Iterator it = map.entrySet().iterator();
        if (it.hasNext()) {
            sb.append('?');
        }
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            sb.append((String) entry.getKey());
            sb.append('=');
            sb.append(a.a((CharSequence) entry.getValue()));
            if (it.hasNext()) {
                sb.append('&');
            }
        }
        return new URL(sb.toString());
    }

    private void a(Canvas canvas, Rect rect, int i2, float f2) {
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(i2);
        paint.setAntiAlias(true);
        if (f2 > 0.0f) {
            canvas.drawRoundRect(new RectF(rect), f2, f2, paint);
        } else {
            canvas.drawRect(rect, paint);
        }
    }

    /* access modifiers changed from: private */
    public void a(boolean z) {
        b();
        this.j = null;
        if (z) {
            AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
            alphaAnimation.setDuration(200);
            h();
            startAnimation(alphaAnimation);
            return;
        }
        h();
    }

    /* access modifiers changed from: private */
    public void g() {
        ScaleAnimation scaleAnimation = new ScaleAnimation(0.9f, 1.0f, 0.9f, 1.0f, 1, 0.5f, 1, 0.5f);
        scaleAnimation.setDuration(100);
        startAnimation(scaleAnimation);
    }

    private void h() {
    }

    /* access modifiers changed from: protected */
    public void a() {
    }

    public void a(p pVar) {
        this.i = pVar;
    }

    public void a(String str) {
        this.k.setText(str);
    }

    /* access modifiers changed from: protected */
    public void a(String str, String str2, Map map, Map map2) {
        a(str, str2, map, map2, true);
    }

    /* JADX WARN: Type inference failed for: r3v14, types: [java.net.URLConnection] */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x01b9  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x01d2 A[SYNTHETIC, Splitter:B:67:0x01d2] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(java.lang.String r17, java.lang.String r18, java.util.Map r19, java.util.Map r20, boolean r21) {
        /*
            r16 = this;
            r0 = r16
            android.content.Context r0 = r0.c
            r3 = r0
            android.webkit.CookieSyncManager r4 = android.webkit.CookieSyncManager.createInstance(r3)
            android.webkit.CookieManager r5 = android.webkit.CookieManager.getInstance()
            r3 = 1
            r5.setAcceptCookie(r3)
            r0 = r16
            r1 = r17
            r2 = r19
            java.net.URL r3 = r0.a(r1, r2)
            r0 = r3
            r1 = r16
            r1.j = r0
            r6 = 0
            r7 = 0
            r8 = 0
            r0 = r16
            java.net.URL r0 = r0.j     // Catch:{ URISyntaxException -> 0x019d, IOException -> 0x01b3, all -> 0x01e8 }
            r3 = r0
            java.net.URLConnection r3 = r3.openConnection()     // Catch:{ URISyntaxException -> 0x019d, IOException -> 0x01b3, all -> 0x01e8 }
            r0 = r3
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ URISyntaxException -> 0x019d, IOException -> 0x01b3, all -> 0x01e8 }
            r9 = r0
            java.lang.String r3 = "login.php"
            r0 = r17
            r1 = r3
            boolean r3 = r0.endsWith(r1)     // Catch:{ URISyntaxException -> 0x021b, IOException -> 0x0208, all -> 0x01ec }
            if (r3 == 0) goto L_0x003f
            r3 = 0
            r9.setInstanceFollowRedirects(r3)     // Catch:{ URISyntaxException -> 0x021b, IOException -> 0x0208, all -> 0x01ec }
        L_0x003f:
            r3 = 1
            r9.setDoInput(r3)     // Catch:{ URISyntaxException -> 0x021b, IOException -> 0x0208, all -> 0x01ec }
            if (r18 == 0) goto L_0x0235
            r0 = r9
            r1 = r18
            r0.setRequestMethod(r1)     // Catch:{ URISyntaxException -> 0x021b, IOException -> 0x0208, all -> 0x01ec }
            java.lang.String r3 = "POST"
            r0 = r3
            r1 = r18
            boolean r3 = r0.equals(r1)     // Catch:{ URISyntaxException -> 0x021b, IOException -> 0x0208, all -> 0x01ec }
            if (r3 == 0) goto L_0x005d
            java.lang.String r3 = "multipart/form-data; boundary=3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f"
            java.lang.String r6 = "Content-Type"
            r9.setRequestProperty(r6, r3)     // Catch:{ URISyntaxException -> 0x021b, IOException -> 0x0208, all -> 0x01ec }
        L_0x005d:
            if (r21 == 0) goto L_0x006b
            java.lang.String r3 = "Cookie"
            r0 = r5
            r1 = r17
            java.lang.String r6 = r0.getCookie(r1)     // Catch:{ URISyntaxException -> 0x021b, IOException -> 0x0208, all -> 0x01ec }
            r9.setRequestProperty(r3, r6)     // Catch:{ URISyntaxException -> 0x021b, IOException -> 0x0208, all -> 0x01ec }
        L_0x006b:
            r9.connect()     // Catch:{ URISyntaxException -> 0x021b, IOException -> 0x0208, all -> 0x01ec }
            java.lang.String r3 = "POST"
            r0 = r3
            r1 = r18
            boolean r3 = r0.equals(r1)     // Catch:{ URISyntaxException -> 0x021b, IOException -> 0x0208, all -> 0x01ec }
            if (r3 == 0) goto L_0x0232
            r3 = 1
            r9.setDoOutput(r3)     // Catch:{ URISyntaxException -> 0x021b, IOException -> 0x0208, all -> 0x01ec }
            java.io.OutputStream r3 = r9.getOutputStream()     // Catch:{ URISyntaxException -> 0x021b, IOException -> 0x0208, all -> 0x01ec }
            r0 = r16
            r1 = r20
            java.lang.String r6 = r0.a(r1)     // Catch:{ URISyntaxException -> 0x0220, IOException -> 0x020d, all -> 0x01f1 }
            if (r6 == 0) goto L_0x0094
            java.lang.String r7 = "UTF-8"
            byte[] r6 = r6.getBytes(r7)     // Catch:{ URISyntaxException -> 0x0220, IOException -> 0x020d, all -> 0x01f1 }
            r3.write(r6)     // Catch:{ URISyntaxException -> 0x0220, IOException -> 0x020d, all -> 0x01f1 }
        L_0x0094:
            r10 = r3
        L_0x0095:
            java.io.InputStream r11 = r9.getInputStream()     // Catch:{ URISyntaxException -> 0x0226, IOException -> 0x0212, all -> 0x01f7 }
            r3 = 0
            r6 = 0
            r15 = r6
            r6 = r3
            r3 = r15
        L_0x009e:
            java.lang.String r7 = r9.getHeaderFieldKey(r3)     // Catch:{ URISyntaxException -> 0x022c, IOException -> 0x0216, all -> 0x01fc }
            java.lang.String r8 = r9.getHeaderField(r3)     // Catch:{ URISyntaxException -> 0x022c, IOException -> 0x0216, all -> 0x01fc }
            if (r7 != 0) goto L_0x00d4
            if (r8 != 0) goto L_0x010c
            r4.sync()     // Catch:{ URISyntaxException -> 0x022c, IOException -> 0x0216, all -> 0x01fc }
            int r3 = r9.getResponseCode()     // Catch:{ URISyntaxException -> 0x022c, IOException -> 0x0216, all -> 0x01fc }
            r4 = 302(0x12e, float:4.23E-43)
            if (r3 != r4) goto L_0x010f
            java.lang.String r3 = "fbconnect:"
            boolean r3 = r6.startsWith(r3)     // Catch:{ URISyntaxException -> 0x022c, IOException -> 0x0216, all -> 0x01fc }
            if (r3 == 0) goto L_0x010f
            r0 = r16
            com.a.a.b r0 = r0.p     // Catch:{ URISyntaxException -> 0x022c, IOException -> 0x0216, all -> 0x01fc }
            r3 = r0
            r0 = r16
            android.webkit.WebView r0 = r0.b     // Catch:{ URISyntaxException -> 0x022c, IOException -> 0x0216, all -> 0x01fc }
            r4 = r0
            r3.shouldOverrideUrlLoading(r4, r6)     // Catch:{ URISyntaxException -> 0x022c, IOException -> 0x0216, all -> 0x01fc }
            a.a.a(r11)
            a.a.a(r10)
            a.a.a(r9)
        L_0x00d3:
            return
        L_0x00d4:
            java.lang.String r12 = com.a.a.q.d     // Catch:{ URISyntaxException -> 0x022c, IOException -> 0x0216, all -> 0x01fc }
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ URISyntaxException -> 0x022c, IOException -> 0x0216, all -> 0x01fc }
            r13.<init>()     // Catch:{ URISyntaxException -> 0x022c, IOException -> 0x0216, all -> 0x01fc }
            java.lang.String r14 = "url header: "
            java.lang.StringBuilder r13 = r13.append(r14)     // Catch:{ URISyntaxException -> 0x022c, IOException -> 0x0216, all -> 0x01fc }
            java.lang.StringBuilder r13 = r13.append(r7)     // Catch:{ URISyntaxException -> 0x022c, IOException -> 0x0216, all -> 0x01fc }
            java.lang.String r14 = "="
            java.lang.StringBuilder r13 = r13.append(r14)     // Catch:{ URISyntaxException -> 0x022c, IOException -> 0x0216, all -> 0x01fc }
            java.lang.StringBuilder r13 = r13.append(r8)     // Catch:{ URISyntaxException -> 0x022c, IOException -> 0x0216, all -> 0x01fc }
            java.lang.String r13 = r13.toString()     // Catch:{ URISyntaxException -> 0x022c, IOException -> 0x0216, all -> 0x01fc }
            com.scoreloop.client.android.core.utils.Logger.a(r12, r13)     // Catch:{ URISyntaxException -> 0x022c, IOException -> 0x0216, all -> 0x01fc }
            java.lang.String r12 = "set-cookie"
            boolean r12 = r7.equalsIgnoreCase(r12)     // Catch:{ URISyntaxException -> 0x022c, IOException -> 0x0216, all -> 0x01fc }
            if (r12 == 0) goto L_0x0103
            java.lang.String r12 = "facebook.com"
            r5.setCookie(r12, r8)     // Catch:{ URISyntaxException -> 0x022c, IOException -> 0x0216, all -> 0x01fc }
        L_0x0103:
            java.lang.String r12 = "location"
            boolean r7 = r7.equalsIgnoreCase(r12)     // Catch:{ URISyntaxException -> 0x022c, IOException -> 0x0216, all -> 0x01fc }
            if (r7 == 0) goto L_0x010c
            r6 = r8
        L_0x010c:
            int r3 = r3 + 1
            goto L_0x009e
        L_0x010f:
            java.lang.StringBuilder r3 = a.a.a(r11)     // Catch:{ URISyntaxException -> 0x022c, IOException -> 0x0216, all -> 0x01fc }
            java.lang.String r3 = r3.toString()     // Catch:{ URISyntaxException -> 0x022c, IOException -> 0x0216, all -> 0x01fc }
            java.lang.String r4 = com.a.a.q.d     // Catch:{ URISyntaxException -> 0x022c, IOException -> 0x0216, all -> 0x01fc }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ URISyntaxException -> 0x022c, IOException -> 0x0216, all -> 0x01fc }
            r5.<init>()     // Catch:{ URISyntaxException -> 0x022c, IOException -> 0x0216, all -> 0x01fc }
            java.lang.String r6 = "whole response: \n"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ URISyntaxException -> 0x022c, IOException -> 0x0216, all -> 0x01fc }
            java.lang.StringBuilder r5 = r5.append(r3)     // Catch:{ URISyntaxException -> 0x022c, IOException -> 0x0216, all -> 0x01fc }
            java.lang.String r5 = r5.toString()     // Catch:{ URISyntaxException -> 0x022c, IOException -> 0x0216, all -> 0x01fc }
            com.scoreloop.client.android.core.utils.Logger.a(r4, r5)     // Catch:{ URISyntaxException -> 0x022c, IOException -> 0x0216, all -> 0x01fc }
            java.lang.String r4 = ""
            if (r3 == r4) goto L_0x018a
            java.lang.String r4 = "http:\\\\/\\\\/www.facebook.com\\\\/fbconnect:"
            java.lang.String r5 = "fbconnect:"
            java.lang.String r5 = r3.replaceAll(r4, r5)     // Catch:{ URISyntaxException -> 0x022c, IOException -> 0x0216, all -> 0x01fc }
            java.net.URI r8 = new java.net.URI     // Catch:{ URISyntaxException -> 0x022c, IOException -> 0x0216, all -> 0x01fc }
            r0 = r8
            r1 = r17
            r0.<init>(r1)     // Catch:{ URISyntaxException -> 0x022c, IOException -> 0x0216, all -> 0x01fc }
            r0 = r16
            android.webkit.WebView r0 = r0.b     // Catch:{ URISyntaxException -> 0x022c, IOException -> 0x0216, all -> 0x01fc }
            r3 = r0
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ URISyntaxException -> 0x022c, IOException -> 0x0216, all -> 0x01fc }
            r4.<init>()     // Catch:{ URISyntaxException -> 0x022c, IOException -> 0x0216, all -> 0x01fc }
            java.lang.String r6 = "http://"
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ URISyntaxException -> 0x022c, IOException -> 0x0216, all -> 0x01fc }
            java.lang.String r6 = r8.getHost()     // Catch:{ URISyntaxException -> 0x022c, IOException -> 0x0216, all -> 0x01fc }
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ URISyntaxException -> 0x022c, IOException -> 0x0216, all -> 0x01fc }
            java.lang.String r4 = r4.toString()     // Catch:{ URISyntaxException -> 0x022c, IOException -> 0x0216, all -> 0x01fc }
            java.lang.String r6 = "text/html"
            java.lang.String r7 = "UTF-8"
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ URISyntaxException -> 0x022c, IOException -> 0x0216, all -> 0x01fc }
            r12.<init>()     // Catch:{ URISyntaxException -> 0x022c, IOException -> 0x0216, all -> 0x01fc }
            java.lang.String r13 = "http://"
            java.lang.StringBuilder r12 = r12.append(r13)     // Catch:{ URISyntaxException -> 0x022c, IOException -> 0x0216, all -> 0x01fc }
            java.lang.String r8 = r8.getHost()     // Catch:{ URISyntaxException -> 0x022c, IOException -> 0x0216, all -> 0x01fc }
            java.lang.StringBuilder r8 = r12.append(r8)     // Catch:{ URISyntaxException -> 0x022c, IOException -> 0x0216, all -> 0x01fc }
            java.lang.String r8 = r8.toString()     // Catch:{ URISyntaxException -> 0x022c, IOException -> 0x0216, all -> 0x01fc }
            r3.loadDataWithBaseURL(r4, r5, r6, r7, r8)     // Catch:{ URISyntaxException -> 0x022c, IOException -> 0x0216, all -> 0x01fc }
            r3 = r11
            r4 = r10
        L_0x017f:
            a.a.a(r3)
            a.a.a(r4)
            a.a.a(r9)
            goto L_0x00d3
        L_0x018a:
            if (r21 == 0) goto L_0x019a
            r8 = 0
            r3 = r16
            r4 = r17
            r5 = r18
            r6 = r19
            r7 = r20
            r3.a(r4, r5, r6, r7, r8)     // Catch:{ URISyntaxException -> 0x022c, IOException -> 0x0216, all -> 0x01fc }
        L_0x019a:
            r3 = r11
            r4 = r10
            goto L_0x017f
        L_0x019d:
            r3 = move-exception
            r3 = r8
            r4 = r7
            r5 = r6
        L_0x01a1:
            java.lang.String r6 = com.a.a.q.d     // Catch:{ all -> 0x0201 }
            java.lang.String r7 = "Error on url format"
            com.scoreloop.client.android.core.utils.Logger.a(r6, r7)     // Catch:{ all -> 0x0201 }
            a.a.a(r3)
            a.a.a(r4)
            a.a.a(r5)
            goto L_0x00d3
        L_0x01b3:
            r3 = move-exception
            r9 = r8
            r10 = r7
            r11 = r6
        L_0x01b7:
            if (r21 == 0) goto L_0x01d2
            r8 = 0
            r3 = r16
            r4 = r17
            r5 = r18
            r6 = r19
            r7 = r20
            r3.a(r4, r5, r6, r7, r8)     // Catch:{ all -> 0x01da }
        L_0x01c7:
            a.a.a(r9)
            a.a.a(r10)
            a.a.a(r11)
            goto L_0x00d3
        L_0x01d2:
            java.lang.String r3 = com.a.a.q.d     // Catch:{ all -> 0x01da }
            java.lang.String r4 = "Error while opening page"
            com.scoreloop.client.android.core.utils.Logger.a(r3, r4)     // Catch:{ all -> 0x01da }
            goto L_0x01c7
        L_0x01da:
            r3 = move-exception
            r4 = r9
            r5 = r10
            r6 = r11
        L_0x01de:
            a.a.a(r4)
            a.a.a(r5)
            a.a.a(r6)
            throw r3
        L_0x01e8:
            r3 = move-exception
            r4 = r8
            r5 = r7
            goto L_0x01de
        L_0x01ec:
            r3 = move-exception
            r4 = r8
            r5 = r7
            r6 = r9
            goto L_0x01de
        L_0x01f1:
            r4 = move-exception
            r5 = r3
            r6 = r9
            r3 = r4
            r4 = r8
            goto L_0x01de
        L_0x01f7:
            r3 = move-exception
            r4 = r8
            r5 = r10
            r6 = r9
            goto L_0x01de
        L_0x01fc:
            r3 = move-exception
            r4 = r11
            r5 = r10
            r6 = r9
            goto L_0x01de
        L_0x0201:
            r6 = move-exception
            r15 = r6
            r6 = r5
            r5 = r4
            r4 = r3
            r3 = r15
            goto L_0x01de
        L_0x0208:
            r3 = move-exception
            r10 = r7
            r11 = r9
            r9 = r8
            goto L_0x01b7
        L_0x020d:
            r4 = move-exception
            r10 = r3
            r11 = r9
            r9 = r8
            goto L_0x01b7
        L_0x0212:
            r3 = move-exception
            r11 = r9
            r9 = r8
            goto L_0x01b7
        L_0x0216:
            r3 = move-exception
            r15 = r11
            r11 = r9
            r9 = r15
            goto L_0x01b7
        L_0x021b:
            r3 = move-exception
            r3 = r8
            r4 = r7
            r5 = r9
            goto L_0x01a1
        L_0x0220:
            r4 = move-exception
            r4 = r3
            r5 = r9
            r3 = r8
            goto L_0x01a1
        L_0x0226:
            r3 = move-exception
            r3 = r8
            r4 = r10
            r5 = r9
            goto L_0x01a1
        L_0x022c:
            r3 = move-exception
            r3 = r11
            r4 = r10
            r5 = r9
            goto L_0x01a1
        L_0x0232:
            r10 = r7
            goto L_0x0095
        L_0x0235:
            r3 = r8
            r4 = r7
            goto L_0x017f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.a.a.q.a(java.lang.String, java.lang.String, java.util.Map, java.util.Map, boolean):void");
    }

    /* access modifiers changed from: protected */
    public void a(Throwable th, boolean z) {
        this.i.a(this, th);
        a(z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.a.q.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.a.a.q.a(java.lang.String, java.util.Map):java.net.URL
      com.a.a.q.a(com.a.a.q, boolean):void
      com.a.a.q.a(java.lang.Throwable, boolean):void
      com.a.a.q.a(boolean, boolean):void */
    /* access modifiers changed from: protected */
    public void a(URI uri) {
        a(true, true);
    }

    /* access modifiers changed from: protected */
    public void a(boolean z, boolean z2) {
        if (this.i != null) {
            if (z) {
                this.i.a(this);
            } else {
                this.i.b(this);
            }
        }
        a(z2);
    }

    /* access modifiers changed from: protected */
    public void b() {
    }

    public void c() {
        a();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Rect rect = new Rect(new Rect(canvas.getClipBounds()));
        rect.inset(10, 10);
        a(canvas, rect, f, 10.0f);
    }
}
