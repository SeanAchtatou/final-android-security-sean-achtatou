package com.a.a;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.scoreloop.client.android.core.utils.Logger;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;

final class b extends WebViewClient {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ q f2a;
    private Dialog b;

    private b(q qVar) {
        this.f2a = qVar;
        this.b = null;
    }

    /* synthetic */ b(q qVar, i iVar) {
        this(qVar);
    }

    public void onPageFinished(WebView webView, String str) {
        super.onPageFinished(webView, str);
        r.a(this.f2a.getContext());
    }

    public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        super.onPageStarted(webView, str, bitmap);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.a.q.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.a.a.q.a(java.lang.String, java.util.Map):java.net.URL
      com.a.a.q.a(com.a.a.q, boolean):void
      com.a.a.q.a(java.lang.Throwable, boolean):void
      com.a.a.q.a(boolean, boolean):void */
    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        try {
            URI uri = new URI(str);
            Logger.a("FBDialog", "Web view URL = " + str);
            if (!uri.isAbsolute()) {
                Logger.a(q.d, "Something went wrong. You probably forgot to specify API key and secret?");
            }
            if (uri.getScheme() != null && str.contains("fbconnect:")) {
                if (str.contains("fbconnect://cancel") || str.contains("fbconnect:cancel")) {
                    this.f2a.a(false, true);
                } else {
                    this.f2a.a(uri);
                }
                return true;
            } else if (this.f2a.j.toExternalForm().equals(str)) {
                return false;
            } else {
                return this.f2a.i != null && !this.f2a.i.a(this.f2a, uri.toURL());
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return false;
        } catch (MalformedURLException e2) {
            e2.printStackTrace();
            return false;
        }
    }
}
