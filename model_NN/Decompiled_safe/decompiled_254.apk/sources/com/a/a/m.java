package com.a.a;

import java.util.Map;

public class m extends Exception {

    /* renamed from: a  reason: collision with root package name */
    private int f8a;
    private String b;
    private Map c;

    public m(int i, String str, Map map) {
        this.f8a = i;
        this.b = str;
        this.c = map;
    }

    public int a() {
        return this.f8a;
    }

    public String getMessage() {
        return this.b;
    }

    public String toString() {
        return "FBRequestError (" + this.f8a + "): " + this.b;
    }
}
