package adon;

import android.content.Context;
import android.location.Location;
import android.webkit.WebChromeClient;
import android.webkit.WebView;

public final class f extends WebView {
    l a;
    Context b;

    public f(Context context) {
        super(context, null, 0);
        getSettings().setJavaScriptEnabled(true);
        getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        getSettings().setSupportZoom(true);
        setWebChromeClient(new WebChromeClient());
        this.b = context;
    }

    /* access modifiers changed from: package-private */
    public String a(Location location) {
        if (location == null || Double.isNaN(this.a.e) || Double.isNaN(this.a.d)) {
            return "<div id=\"distance\"></div>";
        }
        if ((this.a.e == 0.0d && this.a.d == 0.0d) || location == null) {
            return "<div id=\"distance\"></div>";
        }
        double d = 0.017453292519943295d * this.a.e;
        double d2 = 0.017453292519943295d * this.a.d;
        double latitude = 0.017453292519943295d * location.getLatitude();
        double acos = Math.acos((Math.cos(d) * Math.cos(latitude) * Math.cos((0.017453292519943295d * location.getLongitude()) - d2)) + (Math.sin(d) * Math.sin(latitude))) * 6371.0d;
        if (Double.isNaN(acos)) {
            return "<div id=\"distance\"></div>";
        }
        int i = (int) (acos * 1000.0d);
        if (i < 1000) {
            return "<div id=\"distance\">" + String.valueOf(i) + "<br />" + "m" + "</div>";
        }
        double d3 = (double) (i / 1000);
        if (d3 >= 1000.0d) {
            return "<div id=\"distance\"></div>";
        }
        String valueOf = String.valueOf(d3);
        if (valueOf.length() > 3) {
            valueOf = valueOf.substring(0, 3);
        }
        if (valueOf.substring(valueOf.length() - 1, valueOf.length()).equalsIgnoreCase(".")) {
            valueOf = valueOf.substring(0, valueOf.length() - 1);
        }
        return "<div id=\"distance\">" + valueOf + "<br />" + "km" + "</div>";
    }
}
