package adon;

import android.widget.RelativeLayout;
import com.vpon.adon.android.AdView;

final class y implements Runnable {
    private final /* synthetic */ AdView a;
    private final /* synthetic */ l b;

    y(r rVar, AdView adView, l lVar) {
        this.a = adView;
        this.b = lVar;
    }

    public final void run() {
        AdView adView = this.a;
        l lVar = this.b;
        if (lVar == null) {
            adView.b();
            return;
        }
        adView.e = lVar;
        try {
            adView.h = new f(adView.c);
            adView.h.setLayoutParams(new RelativeLayout.LayoutParams(adView.e.b, adView.e.c));
            f fVar = adView.h;
            l lVar2 = adView.e;
            fVar.a = lVar2;
            fVar.setWebViewClient(new i(adView, lVar2, fVar.b));
            "html: " + lVar2.a;
            String str = lVar2.a;
            if (lVar2.h.j != null) {
                str = str.replace("<div id=\"distance\"></div>", fVar.a(lVar2.h.j));
            }
            fVar.loadDataWithBaseURL(null, str, "text/html", "utf-8", null);
        } catch (Exception e) {
            r.a(adView.c).a(adView, e.getMessage());
            adView.b();
        }
    }
}
