package adon;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.util.LinkedList;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SealedObject;
import javax.crypto.spec.SecretKeySpec;

public final class p {
    private static String a = "RSA/NONE/NoPadding";
    private static String b = "BC";

    public static a a(String str, SealedObject sealedObject) {
        try {
            byte[] bArr = new byte[24];
            System.arraycopy(str.getBytes("UTF-8"), 0, bArr, 0, 24);
            SecretKeySpec secretKeySpec = new SecretKeySpec(bArr, "DESede");
            Cipher instance = Cipher.getInstance("DESede");
            instance.init(2, secretKeySpec);
            return (a) sealedObject.getObject(instance);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return new a(new LinkedList());
        } catch (NoSuchAlgorithmException e2) {
            e2.printStackTrace();
            return new a(new LinkedList());
        } catch (NoSuchPaddingException e3) {
            e3.printStackTrace();
            return new a(new LinkedList());
        } catch (InvalidKeyException e4) {
            e4.printStackTrace();
            return new a(new LinkedList());
        } catch (IllegalBlockSizeException e5) {
            e5.printStackTrace();
            return new a(new LinkedList());
        } catch (BadPaddingException e6) {
            e6.printStackTrace();
            return new a(new LinkedList());
        } catch (IOException e7) {
            e7.printStackTrace();
            return new a(new LinkedList());
        } catch (ClassNotFoundException e8) {
            e8.printStackTrace();
            return new a(new LinkedList());
        } catch (Exception e9) {
            e9.printStackTrace();
            return new a(new LinkedList());
        }
    }

    public static String a(String str, String str2) {
        byte[] bArr = new byte[24];
        System.arraycopy(str.getBytes("UTF-8"), 0, bArr, 0, 24);
        SecretKeySpec secretKeySpec = new SecretKeySpec(bArr, "DESede");
        Cipher instance = Cipher.getInstance("DESede");
        instance.init(2, secretKeySpec);
        return new String(instance.doFinal(e.a(str2)), "UTF-8");
    }

    public static PublicKey a(byte[] bArr) {
        return KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(bArr));
    }

    public static Cipher a(PublicKey publicKey) {
        Cipher instance = Cipher.getInstance(a, b);
        instance.init(1, publicKey);
        return instance;
    }

    public static SealedObject a(String str, a aVar) {
        try {
            byte[] bArr = new byte[24];
            System.arraycopy(str.getBytes("UTF-8"), 0, bArr, 0, 24);
            SecretKeySpec secretKeySpec = new SecretKeySpec(bArr, "DESede");
            Cipher instance = Cipher.getInstance("DESede");
            instance.init(1, secretKeySpec);
            return new SealedObject(aVar, instance);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        } catch (NoSuchAlgorithmException e2) {
            e2.printStackTrace();
            return null;
        } catch (NoSuchPaddingException e3) {
            e3.printStackTrace();
            return null;
        } catch (InvalidKeyException e4) {
            e4.printStackTrace();
            return null;
        } catch (IllegalBlockSizeException e5) {
            e5.printStackTrace();
            return null;
        } catch (IOException e6) {
            e6.printStackTrace();
            return null;
        } catch (Exception e7) {
            e7.printStackTrace();
            return null;
        }
    }

    public static byte[] a(byte[] bArr, Cipher cipher) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(53);
        for (int i = 0; i < bArr.length; i += 245) {
            byteArrayOutputStream.write(cipher.doFinal(bArr, i, bArr.length - i <= 245 ? bArr.length - i : 245));
        }
        return byteArrayOutputStream.toByteArray();
    }
}
