package adon;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import com.vpon.adon.android.AdView;

final class t implements LocationListener {
    private /* synthetic */ r a;

    t(r rVar) {
        this.a = rVar;
    }

    public final void onLocationChanged(Location location) {
        this.a.z = location;
        this.a.e = location.getLatitude();
        this.a.f = location.getLongitude();
        for (AdView adView : this.a.s) {
            if (!(adView == null || adView.g == null)) {
                f fVar = adView.g;
                if (!(location == null || fVar.a == null)) {
                    fVar.loadDataWithBaseURL(null, fVar.a.a.replace("<div id=\"distance\"></div>", fVar.a(location)), "text/html", "utf-8", null);
                }
            }
        }
    }

    public final void onProviderDisabled(String str) {
    }

    public final void onProviderEnabled(String str) {
    }

    public final void onStatusChanged(String str, int i, Bundle bundle) {
    }
}
