package adon;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import java.net.URLDecoder;

public final class k extends WebViewClient {
    private Activity a;

    public k(Activity activity) {
        this.a = activity;
    }

    public final void onLoadResource(WebView webView, String str) {
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        Intent intent;
        if (str.startsWith("tel://")) {
            this.a.startActivity(new Intent("android.intent.action.DIAL", Uri.parse(str)));
            return true;
        } else if (str.startsWith("sms://")) {
            Intent intent2 = new Intent("android.intent.action.SENDTO", Uri.parse(str));
            String[] split = str.split("body=");
            if (split.length >= 2) {
                Uri.parse(split[0]);
                String str2 = split[0].split("//")[1];
                Intent intent3 = new Intent("android.intent.action.SENDTO", Uri.parse("smsto:" + str2.substring(0, str2.length() - 1)));
                intent3.putExtra("sms_body", URLDecoder.decode(split[1]));
                intent = intent3;
            } else {
                intent = intent2;
            }
            this.a.startActivity(intent);
            return true;
        } else if (str.startsWith("mailto:")) {
            this.a.startActivity(new Intent("android.intent.action.SENDTO", Uri.parse(str)));
            return true;
        } else if (str.startsWith("market:")) {
            this.a.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
            return true;
        } else if (str.startsWith("http://maps.google")) {
            this.a.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
            return true;
        } else if (!str.startsWith("http://www.youtube.com/")) {
            return false;
        } else {
            this.a.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
            return true;
        }
    }
}
