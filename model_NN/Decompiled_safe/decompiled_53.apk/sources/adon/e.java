package adon;

import java.util.Arrays;

public final class e {
    private static final char[] a = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".toCharArray();
    private static final int[] b;

    static {
        int[] iArr = new int[256];
        b = iArr;
        Arrays.fill(iArr, -1);
        int length = a.length;
        for (int i = 0; i < length; i++) {
            b[a[i]] = i;
        }
        b[61] = 0;
    }

    public static final String a(byte[] bArr, boolean z) {
        return new String(b(bArr, false));
    }

    public static final byte[] a(String str) {
        int length = str != null ? str.length() : 0;
        if (length == 0) {
            return new byte[0];
        }
        int i = 0;
        for (int i2 = 0; i2 < length; i2++) {
            if (b[str.charAt(i2)] < 0) {
                i++;
            }
        }
        if ((length - i) % 4 != 0) {
            return null;
        }
        int i3 = length;
        int i4 = 0;
        while (i3 > 1) {
            i3--;
            if (b[str.charAt(i3)] > 0) {
                break;
            } else if (str.charAt(i3) == '=') {
                i4++;
            }
        }
        int i5 = (((length - i) * 6) >> 3) - i4;
        byte[] bArr = new byte[i5];
        int i6 = 0;
        int i7 = 0;
        while (i6 < i5) {
            int i8 = 0;
            int i9 = i7;
            int i10 = 0;
            while (i10 < 4) {
                int i11 = i9 + 1;
                int i12 = b[str.charAt(i9)];
                if (i12 >= 0) {
                    i8 |= i12 << (18 - (i10 * 6));
                } else {
                    i10--;
                }
                i10++;
                i9 = i11;
            }
            int i13 = i6 + 1;
            bArr[i6] = (byte) (i8 >> 16);
            if (i13 < i5) {
                i6 = i13 + 1;
                bArr[i13] = (byte) (i8 >> 8);
                if (i6 < i5) {
                    i13 = i6 + 1;
                    bArr[i6] = (byte) i8;
                } else {
                    i7 = i9;
                }
            }
            i6 = i13;
            i7 = i9;
        }
        return bArr;
    }

    private static char[] b(byte[] bArr, boolean z) {
        int length = bArr != null ? bArr.length : 0;
        if (length == 0) {
            return new char[0];
        }
        int i = (length / 3) * 3;
        int i2 = (((length - 1) / 3) + 1) << 2;
        int i3 = i2 + (z ? ((i2 - 1) / 76) << 1 : 0);
        char[] cArr = new char[i3];
        int i4 = 0;
        int i5 = 0;
        int i6 = 0;
        while (i5 < i) {
            int i7 = i5 + 1;
            int i8 = i7 + 1;
            byte b2 = ((bArr[i5] & 255) << 16) | ((bArr[i7] & 255) << 8);
            int i9 = i8 + 1;
            byte b3 = b2 | (bArr[i8] & 255);
            int i10 = i4 + 1;
            cArr[i4] = a[(b3 >>> 18) & 63];
            int i11 = i10 + 1;
            cArr[i10] = a[(b3 >>> 12) & 63];
            int i12 = i11 + 1;
            cArr[i11] = a[(b3 >>> 6) & 63];
            i4 = i12 + 1;
            cArr[i12] = a[b3 & 63];
            if (!z || (i6 = i6 + 1) != 19 || i4 >= i3 - 2) {
                i5 = i9;
            } else {
                int i13 = i4 + 1;
                cArr[i4] = 13;
                i4 = i13 + 1;
                cArr[i13] = 10;
                i6 = 0;
                i5 = i9;
            }
        }
        int i14 = length - i;
        if (i14 > 0) {
            int i15 = (i14 == 2 ? (bArr[length - 1] & 255) << 2 : 0) | ((bArr[i] & 255) << 10);
            cArr[i3 - 4] = a[i15 >> 12];
            cArr[i3 - 3] = a[(i15 >>> 6) & 63];
            cArr[i3 - 2] = i14 == 2 ? a[i15 & 63] : '=';
            cArr[i3 - 1] = '=';
        }
        return cArr;
    }
}
