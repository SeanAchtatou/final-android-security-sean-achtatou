package adon;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.util.Log;
import com.vpon.adon.android.AdOnPlatform;
import com.vpon.adon.android.AdView;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

public final class r {
    private static /* synthetic */ int[] A;
    /* access modifiers changed from: private */
    public static r b;
    private static int q = 0;
    private Context a;
    private LocationManager c = null;
    private LocationListener d = null;
    /* access modifiers changed from: private */
    public double e;
    /* access modifiers changed from: private */
    public double f;
    private String g;
    private String h;
    private String i;
    private String j;
    private String k;
    private String l;
    private String m = "Android";
    private String n;
    private q o;
    private String p = "Android";
    private List r;
    /* access modifiers changed from: private */
    public List s = new LinkedList();
    private String t = "http://tw.adon.vpon.com/api/webviewAdReq";
    private String u = "http://tw.adon.vpon.com/api/webviewSdkError";
    private String v = "http://tw.adon.vpon.com/api/webviewAdClick";
    private String w = "http://cn.adon.vpon.com/api/webviewAdReq";
    private String x = "http://cn.adon.vpon.com/api/swebviewSdkError";
    private String y = "http://cn.adon.vpon.com/api/webviewAdClick";
    /* access modifiers changed from: private */
    public Location z;

    private r(Context context) {
        if (s.a == null) {
            s.a = new Handler();
        }
        Handler handler = s.a;
        this.a = context;
        InputStream resourceAsStream = getClass().getResourceAsStream("/server.properties");
        Properties properties = new Properties();
        try {
            properties.load(resourceAsStream);
            this.t = properties.getProperty("adReqUrlTW");
            this.u = properties.getProperty("sdkErrorUrlTW");
            this.v = properties.getProperty("adClickUrlTW");
            this.w = properties.getProperty("adReqUrlCN");
            this.x = properties.getProperty("sdkErrorUrlCN");
            this.y = properties.getProperty("adClickUrlCN");
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        this.k = h.a(context);
        this.l = h.a();
        this.n = h.b();
        this.o = h.b(context);
        try {
            if (this.c == null) {
                this.c = (LocationManager) this.a.getSystemService("location");
                this.d = new t(this);
                if (this.c.isProviderEnabled("gps") || this.c.isProviderEnabled("network")) {
                    try {
                        String bestProvider = this.c.getBestProvider(new Criteria(), true);
                        this.c.getLastKnownLocation(bestProvider);
                        this.c.requestLocationUpdates(bestProvider, 2000, 10.0f, this.d);
                    } catch (Exception e3) {
                    }
                }
            }
        } catch (Exception e4) {
        }
        try {
            TelephonyManager telephonyManager = (TelephonyManager) this.a.getSystemService("phone");
            String networkOperator = telephonyManager.getNetworkOperator();
            if (networkOperator != null) {
                this.j = networkOperator.substring(0, 3);
                this.i = networkOperator.substring(3);
            }
            GsmCellLocation gsmCellLocation = (GsmCellLocation) telephonyManager.getCellLocation();
            this.g = String.valueOf(gsmCellLocation.getCid());
            this.h = String.valueOf(gsmCellLocation.getLac());
        } catch (Exception e5) {
        }
    }

    public static r a(Context context) {
        if (b == null) {
            b = new r(context);
        }
        return b;
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:37:0x0058=Splitter:B:37:0x0058, B:14:0x002f=Splitter:B:14:0x002f} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(adon.a r6) {
        /*
            r5 = this;
            r4 = 0
            java.io.File r0 = new java.io.File
            java.lang.String r1 = r5.c()
            r0.<init>(r1)
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x002c, IOException -> 0x0055, all -> 0x007e }
            r1.<init>(r0)     // Catch:{ FileNotFoundException -> 0x002c, IOException -> 0x0055, all -> 0x007e }
            java.io.ObjectOutputStream r2 = new java.io.ObjectOutputStream     // Catch:{ FileNotFoundException -> 0x00cc, IOException -> 0x00c7, all -> 0x00c2 }
            java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x00cc, IOException -> 0x00c7, all -> 0x00c2 }
            r3.<init>(r0)     // Catch:{ FileNotFoundException -> 0x00cc, IOException -> 0x00c7, all -> 0x00c2 }
            r2.<init>(r3)     // Catch:{ FileNotFoundException -> 0x00cc, IOException -> 0x00c7, all -> 0x00c2 }
            java.lang.String r0 = "http://www.vpon.com/v/index.jsp88623698333service@vpon.com"
            javax.crypto.SealedObject r0 = adon.p.a(r0, r6)     // Catch:{ FileNotFoundException -> 0x00d0, IOException -> 0x00ca }
            r2.writeObject(r0)     // Catch:{ FileNotFoundException -> 0x00d0, IOException -> 0x00ca }
            r2.flush()     // Catch:{ FileNotFoundException -> 0x00d0, IOException -> 0x00ca }
            r2.close()     // Catch:{ IOException -> 0x00a4 }
            r1.close()     // Catch:{ IOException -> 0x00bc }
        L_0x002b:
            return
        L_0x002c:
            r0 = move-exception
            r1 = r4
            r2 = r4
        L_0x002f:
            r0.printStackTrace()     // Catch:{ all -> 0x00c5 }
            r2.close()     // Catch:{ IOException -> 0x003e }
            r1.close()     // Catch:{ IOException -> 0x0039 }
            goto L_0x002b
        L_0x0039:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x002b
        L_0x003e:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x004b }
            r1.close()     // Catch:{ IOException -> 0x0046 }
            goto L_0x002b
        L_0x0046:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x002b
        L_0x004b:
            r0 = move-exception
            r1.close()     // Catch:{ IOException -> 0x0050 }
        L_0x004f:
            throw r0
        L_0x0050:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x004f
        L_0x0055:
            r0 = move-exception
            r1 = r4
            r2 = r4
        L_0x0058:
            r0.printStackTrace()     // Catch:{ all -> 0x00c5 }
            r2.close()     // Catch:{ IOException -> 0x0067 }
            r1.close()     // Catch:{ IOException -> 0x0062 }
            goto L_0x002b
        L_0x0062:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x002b
        L_0x0067:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x0074 }
            r1.close()     // Catch:{ IOException -> 0x006f }
            goto L_0x002b
        L_0x006f:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x002b
        L_0x0074:
            r0 = move-exception
            r1.close()     // Catch:{ IOException -> 0x0079 }
        L_0x0078:
            throw r0
        L_0x0079:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0078
        L_0x007e:
            r0 = move-exception
            r1 = r4
            r2 = r4
        L_0x0081:
            r2.close()     // Catch:{ IOException -> 0x0088 }
            r1.close()     // Catch:{ IOException -> 0x009f }
        L_0x0087:
            throw r0
        L_0x0088:
            r2 = move-exception
            r2.printStackTrace()     // Catch:{ all -> 0x0095 }
            r1.close()     // Catch:{ IOException -> 0x0090 }
            goto L_0x0087
        L_0x0090:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0087
        L_0x0095:
            r0 = move-exception
            r1.close()     // Catch:{ IOException -> 0x009a }
        L_0x0099:
            throw r0
        L_0x009a:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0099
        L_0x009f:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0087
        L_0x00a4:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x00b2 }
            r1.close()     // Catch:{ IOException -> 0x00ac }
            goto L_0x002b
        L_0x00ac:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x002b
        L_0x00b2:
            r0 = move-exception
            r1.close()     // Catch:{ IOException -> 0x00b7 }
        L_0x00b6:
            throw r0
        L_0x00b7:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00b6
        L_0x00bc:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x002b
        L_0x00c2:
            r0 = move-exception
            r2 = r4
            goto L_0x0081
        L_0x00c5:
            r0 = move-exception
            goto L_0x0081
        L_0x00c7:
            r0 = move-exception
            r2 = r4
            goto L_0x0058
        L_0x00ca:
            r0 = move-exception
            goto L_0x0058
        L_0x00cc:
            r0 = move-exception
            r2 = r4
            goto L_0x002f
        L_0x00d0:
            r0 = move-exception
            goto L_0x002f
        */
        throw new UnsupportedOperationException("Method not decompiled: adon.r.a(adon.a):void");
    }

    static /* synthetic */ void a(r rVar, AdView adView, l lVar) {
        x xVar = new x();
        xVar.a = adView.getLicenseKey();
        xVar.b = lVar.f;
        xVar.c = rVar.e;
        xVar.d = rVar.f;
        xVar.e = rVar.g;
        xVar.f = rVar.h;
        xVar.h = rVar.j;
        xVar.g = rVar.i;
        xVar.i = d();
        if (!new File(rVar.c()).exists()) {
            rVar.a(new a(new LinkedList()));
        }
        rVar.r = rVar.b().a;
        rVar.r.add(xVar);
        rVar.b(adView);
    }

    static /* synthetic */ void a(r rVar, AdView adView, Handler handler) {
        String str;
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("method", "webviewAdReq");
            jSONObject.put("imei", rVar.k);
            jSONObject.put("licensekey", adView.getLicenseKey());
            jSONObject.put("screenHeight", String.valueOf(rVar.o.b));
            jSONObject.put("screenWidth", String.valueOf(rVar.o.a));
            jSONObject.put("sdkName", rVar.p);
            jSONObject.put("sdkVersion", "2.0.2");
            jSONObject.put("deviceName", rVar.l);
            jSONObject.put("deviceOsName", rVar.m);
            jSONObject.put("deviceOsVersion", rVar.n);
            jSONObject.put("lat", String.valueOf(rVar.e));
            jSONObject.put("lon", String.valueOf(rVar.f));
            jSONObject.put("cellId", rVar.g);
            jSONObject.put("lac", rVar.h);
            jSONObject.put("mcc", rVar.j);
            jSONObject.put("mnc", rVar.i);
            jSONObject.put("ts", 0);
            switch (e()[adView.getPlatform().ordinal()]) {
                case 1:
                    str = rVar.t;
                    break;
                case 2:
                    str = rVar.w;
                    break;
                default:
                    str = null;
                    break;
            }
            if (str == null) {
                rVar.a(adView, String.valueOf(adView.getLicenseKey()) + " : platform error");
                rVar.a(adView, handler, (l) null);
                return;
            }
            HttpResponse a2 = o.a().a(jSONObject.toString(), str, rVar.p, "2.0.2");
            int intValue = Integer.valueOf(a2.getLastHeader("X-ADON-STATUS").getValue()).intValue();
            if (intValue != 0) {
                switch (intValue) {
                    case -10:
                        Log.d("AdOn SDK error", "application is using wrong license key");
                        rVar.a(adView, handler, (l) null);
                        return;
                    case -9:
                        Log.d("AdOn SDK error", "license key is blocked administratively");
                        rVar.a(adView, handler, (l) null);
                        return;
                    case -8:
                        Log.d("AdOn SDK error", "application is using unknown license key");
                        rVar.a(adView, handler, (l) null);
                        return;
                    case -7:
                        Log.d("AdOn SDK error", "application is not sending the (Lat,Lon) pair or cellId pair to server");
                        rVar.a(adView, handler, (l) null);
                        return;
                    case -6:
                        Log.d("AdOn SDK error", "device not support");
                        rVar.a(adView, handler, (l) null);
                        return;
                    case -5:
                        Log.d("AdOn SDK error", "application is using deprecated version of sdk");
                        rVar.a(adView, handler, (l) null);
                        return;
                    default:
                        rVar.a(adView, handler, (l) null);
                        return;
                }
            } else {
                String entityUtils = EntityUtils.toString(a2.getEntity());
                l lVar = new l();
                lVar.a = entityUtils;
                lVar.f = a2.getLastHeader("X-ADON-AD_ID").getValue();
                lVar.c = Integer.valueOf(a2.getLastHeader("X-ADON-AD_HEIGHT").getValue()).intValue();
                lVar.b = Integer.valueOf(a2.getLastHeader("X-ADON-AD_WIDTH").getValue()).intValue();
                lVar.g = Integer.valueOf(a2.getLastHeader("X-ADON-REFRESH_TIME").getValue()).intValue();
                if (a2.getLastHeader("X-ADON-LAT") != null) {
                    lVar.e = Double.valueOf(a2.getLastHeader("X-ADON-LAT").getValue()).doubleValue();
                }
                if (a2.getLastHeader("X-ADON-LON") != null) {
                    lVar.d = Double.valueOf(a2.getLastHeader("X-ADON-LON").getValue()).doubleValue();
                }
                m mVar = new m();
                mVar.a = lVar.f;
                mVar.i = rVar.g;
                mVar.c = rVar.k;
                mVar.h = rVar.h;
                mVar.d = Double.valueOf(rVar.e);
                mVar.b = adView.getLicenseKey();
                mVar.e = Double.valueOf(rVar.f);
                mVar.f = rVar.j;
                mVar.g = rVar.i;
                mVar.j = rVar.z;
                lVar.h = mVar;
                rVar.a(adView, handler, lVar);
            }
        } catch (JSONException e2) {
            rVar.a(adView, handler, (l) null);
        } catch (InvalidKeyException e3) {
            rVar.a(adView, handler, (l) null);
        } catch (ParseException e4) {
            rVar.a(adView, handler, (l) null);
        } catch (NoSuchAlgorithmException e5) {
            rVar.a(adView, handler, (l) null);
        } catch (NoSuchPaddingException e6) {
            rVar.a(adView, handler, (l) null);
        } catch (InvalidKeySpecException e7) {
            rVar.a(adView, handler, (l) null);
        } catch (IllegalBlockSizeException e8) {
            rVar.a(adView, handler, (l) null);
        } catch (BadPaddingException e9) {
            rVar.a(adView, handler, (l) null);
        } catch (NoSuchProviderException e10) {
            rVar.a(adView, handler, (l) null);
        } catch (IOException e11) {
            rVar.a(adView, handler, (l) null);
        } catch (v e12) {
            rVar.a(adView, handler, (l) null);
        } catch (URISyntaxException e13) {
            rVar.a(adView, handler, (l) null);
        } catch (Exception e14) {
            rVar.a(adView, handler, (l) null);
        }
    }

    static /* synthetic */ void a(r rVar, AdView adView, String str) {
        String str2;
        String str3 = String.valueOf(adView.getLicenseKey()) + " generate error: " + str;
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("method", "sdkError");
            jSONObject.put("imei", rVar.k);
            jSONObject.put("licensekey", adView.getLicenseKey());
            jSONObject.put("screenHeight", String.valueOf(rVar.o.b));
            jSONObject.put("screenWidth", String.valueOf(rVar.o.a));
            jSONObject.put("sdkName", rVar.p);
            jSONObject.put("sdkVersion", "2.0.2");
            jSONObject.put("deviceName", rVar.l);
            jSONObject.put("deviceOsName", rVar.m);
            jSONObject.put("deviceOsVersion", rVar.n);
            jSONObject.put("lat", String.valueOf(rVar.e));
            jSONObject.put("lon", String.valueOf(rVar.f));
            jSONObject.put("cellId", rVar.g);
            jSONObject.put("lac", rVar.h);
            jSONObject.put("mcc", rVar.j);
            jSONObject.put("mnc", rVar.i);
            jSONObject.put("error", str3);
            jSONObject.put("errorAt", d());
            jSONObject.put("ts", 0);
            switch (e()[adView.getPlatform().ordinal()]) {
                case 1:
                    str2 = rVar.u;
                    break;
                case 2:
                    str2 = rVar.x;
                    break;
                default:
                    str2 = null;
                    break;
            }
            if (str2 != null) {
                o.a().a(jSONObject.toString(), str2, rVar.p, "2.0.2");
            }
        } catch (v | IOException | Exception | URISyntaxException | InvalidKeyException | NoSuchAlgorithmException | NoSuchProviderException | InvalidKeySpecException | BadPaddingException | IllegalBlockSizeException | NoSuchPaddingException | ParseException | JSONException e2) {
        }
    }

    private void a(AdView adView, Handler handler, l lVar) {
        handler.post(new y(this, adView, lVar));
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x003d A[SYNTHETIC, Splitter:B:15:0x003d] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0056 A[SYNTHETIC, Splitter:B:23:0x0056] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x006f A[SYNTHETIC, Splitter:B:31:0x006f] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0088 A[SYNTHETIC, Splitter:B:39:0x0088] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0094 A[SYNTHETIC, Splitter:B:45:0x0094] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:28:0x0060=Splitter:B:28:0x0060, B:20:0x0047=Splitter:B:20:0x0047, B:36:0x0079=Splitter:B:36:0x0079, B:12:0x002e=Splitter:B:12:0x002e} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private adon.a b() {
        /*
            r4 = this;
            java.io.File r0 = new java.io.File
            java.lang.String r1 = r4.c()
            r0.<init>(r1)
            r1 = 0
            java.io.ObjectInputStream r2 = new java.io.ObjectInputStream     // Catch:{ StreamCorruptedException -> 0x002d, FileNotFoundException -> 0x0046, IOException -> 0x005f, ClassNotFoundException -> 0x0078 }
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ StreamCorruptedException -> 0x002d, FileNotFoundException -> 0x0046, IOException -> 0x005f, ClassNotFoundException -> 0x0078 }
            r3.<init>(r0)     // Catch:{ StreamCorruptedException -> 0x002d, FileNotFoundException -> 0x0046, IOException -> 0x005f, ClassNotFoundException -> 0x0078 }
            r2.<init>(r3)     // Catch:{ StreamCorruptedException -> 0x002d, FileNotFoundException -> 0x0046, IOException -> 0x005f, ClassNotFoundException -> 0x0078 }
            java.lang.Object r4 = r2.readObject()     // Catch:{ StreamCorruptedException -> 0x00ae, FileNotFoundException -> 0x00ab, IOException -> 0x00a8, ClassNotFoundException -> 0x00a5, all -> 0x00a2 }
            if (r4 != 0) goto L_0x0021
            java.lang.String r0 = "adManger"
            java.lang.String r1 = "sealedObject null"
            android.util.Log.i(r0, r1)     // Catch:{ StreamCorruptedException -> 0x00ae, FileNotFoundException -> 0x00ab, IOException -> 0x00a8, ClassNotFoundException -> 0x00a5, all -> 0x00a2 }
        L_0x0021:
            java.lang.String r0 = "http://www.vpon.com/v/index.jsp88623698333service@vpon.com"
            javax.crypto.SealedObject r4 = (javax.crypto.SealedObject) r4     // Catch:{ StreamCorruptedException -> 0x00ae, FileNotFoundException -> 0x00ab, IOException -> 0x00a8, ClassNotFoundException -> 0x00a5, all -> 0x00a2 }
            adon.a r0 = adon.p.a(r0, r4)     // Catch:{ StreamCorruptedException -> 0x00ae, FileNotFoundException -> 0x00ab, IOException -> 0x00a8, ClassNotFoundException -> 0x00a5, all -> 0x00a2 }
            r2.close()     // Catch:{ IOException -> 0x009d }
        L_0x002c:
            return r0
        L_0x002d:
            r0 = move-exception
        L_0x002e:
            r0.printStackTrace()     // Catch:{ all -> 0x0091 }
            adon.a r0 = new adon.a     // Catch:{ all -> 0x0091 }
            java.util.LinkedList r2 = new java.util.LinkedList     // Catch:{ all -> 0x0091 }
            r2.<init>()     // Catch:{ all -> 0x0091 }
            r0.<init>(r2)     // Catch:{ all -> 0x0091 }
            if (r1 == 0) goto L_0x002c
            r1.close()     // Catch:{ IOException -> 0x0041 }
            goto L_0x002c
        L_0x0041:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x002c
        L_0x0046:
            r0 = move-exception
        L_0x0047:
            r0.printStackTrace()     // Catch:{ all -> 0x0091 }
            adon.a r0 = new adon.a     // Catch:{ all -> 0x0091 }
            java.util.LinkedList r2 = new java.util.LinkedList     // Catch:{ all -> 0x0091 }
            r2.<init>()     // Catch:{ all -> 0x0091 }
            r0.<init>(r2)     // Catch:{ all -> 0x0091 }
            if (r1 == 0) goto L_0x002c
            r1.close()     // Catch:{ IOException -> 0x005a }
            goto L_0x002c
        L_0x005a:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x002c
        L_0x005f:
            r0 = move-exception
        L_0x0060:
            r0.printStackTrace()     // Catch:{ all -> 0x0091 }
            adon.a r0 = new adon.a     // Catch:{ all -> 0x0091 }
            java.util.LinkedList r2 = new java.util.LinkedList     // Catch:{ all -> 0x0091 }
            r2.<init>()     // Catch:{ all -> 0x0091 }
            r0.<init>(r2)     // Catch:{ all -> 0x0091 }
            if (r1 == 0) goto L_0x002c
            r1.close()     // Catch:{ IOException -> 0x0073 }
            goto L_0x002c
        L_0x0073:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x002c
        L_0x0078:
            r0 = move-exception
        L_0x0079:
            r0.printStackTrace()     // Catch:{ all -> 0x0091 }
            adon.a r0 = new adon.a     // Catch:{ all -> 0x0091 }
            java.util.LinkedList r2 = new java.util.LinkedList     // Catch:{ all -> 0x0091 }
            r2.<init>()     // Catch:{ all -> 0x0091 }
            r0.<init>(r2)     // Catch:{ all -> 0x0091 }
            if (r1 == 0) goto L_0x002c
            r1.close()     // Catch:{ IOException -> 0x008c }
            goto L_0x002c
        L_0x008c:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x002c
        L_0x0091:
            r0 = move-exception
        L_0x0092:
            if (r1 == 0) goto L_0x0097
            r1.close()     // Catch:{ IOException -> 0x0098 }
        L_0x0097:
            throw r0
        L_0x0098:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0097
        L_0x009d:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x002c
        L_0x00a2:
            r0 = move-exception
            r1 = r2
            goto L_0x0092
        L_0x00a5:
            r0 = move-exception
            r1 = r2
            goto L_0x0079
        L_0x00a8:
            r0 = move-exception
            r1 = r2
            goto L_0x0060
        L_0x00ab:
            r0 = move-exception
            r1 = r2
            goto L_0x0047
        L_0x00ae:
            r0 = move-exception
            r1 = r2
            goto L_0x002e
        */
        throw new UnsupportedOperationException("Method not decompiled: adon.r.b():adon.a");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
        return;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void b(com.vpon.adon.android.AdView r9) {
        /*
            r8 = this;
            r3 = 0
            org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            r1.<init>()     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            java.lang.String r0 = "method"
            java.lang.String r2 = "adClick"
            r1.put(r0, r2)     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            java.lang.String r0 = "imei"
            java.lang.String r2 = r8.k     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            r1.put(r0, r2)     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            java.lang.String r0 = "screenHeight"
            adon.q r2 = r8.o     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            int r2 = r2.b     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            java.lang.String r2 = java.lang.String.valueOf(r2)     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            r1.put(r0, r2)     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            java.lang.String r0 = "screenWidth"
            adon.q r2 = r8.o     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            int r2 = r2.a     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            java.lang.String r2 = java.lang.String.valueOf(r2)     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            r1.put(r0, r2)     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            java.lang.String r0 = "sdkName"
            java.lang.String r2 = r8.p     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            r1.put(r0, r2)     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            java.lang.String r0 = "sdkVersion"
            java.lang.String r2 = "2.0.2"
            r1.put(r0, r2)     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            java.lang.String r0 = "deviceName"
            java.lang.String r2 = r8.l     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            r1.put(r0, r2)     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            java.lang.String r0 = "deviceOsName"
            java.lang.String r2 = r8.m     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            r1.put(r0, r2)     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            java.lang.String r0 = "deviceOsVersion"
            java.lang.String r2 = r8.n     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            r1.put(r0, r2)     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            java.lang.String r0 = "ts"
            r2 = 0
            r1.put(r0, r2)     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            org.json.JSONArray r2 = new org.json.JSONArray     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            r2.<init>()     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
        L_0x005c:
            java.util.List r0 = r8.r     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            int r0 = r0.size()     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            if (r3 < r0) goto L_0x007e
            java.lang.String r0 = "adClickItemList"
            r1.put(r0, r2)     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            int[] r0 = e()     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            com.vpon.adon.android.AdOnPlatform r2 = r9.getPlatform()     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            int r2 = r2.ordinal()     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            r0 = r0[r2]     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            switch(r0) {
                case 1: goto L_0x011e;
                case 2: goto L_0x011a;
                default: goto L_0x007a;
            }     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
        L_0x007a:
            r0 = 0
        L_0x007b:
            if (r0 != 0) goto L_0x0122
        L_0x007d:
            return
        L_0x007e:
            org.json.JSONObject r4 = new org.json.JSONObject     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            r4.<init>()     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            java.lang.String r5 = "adId"
            java.util.List r0 = r8.r     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            java.lang.Object r0 = r0.get(r3)     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            adon.x r0 = (adon.x) r0     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            java.lang.String r0 = r0.b     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            r4.put(r5, r0)     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            java.lang.String r5 = "licensekey"
            java.util.List r0 = r8.r     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            java.lang.Object r0 = r0.get(r3)     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            adon.x r0 = (adon.x) r0     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            java.lang.String r0 = r0.a     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            r4.put(r5, r0)     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            java.lang.String r5 = "clickAt"
            java.util.List r0 = r8.r     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            java.lang.Object r0 = r0.get(r3)     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            adon.x r0 = (adon.x) r0     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            java.lang.String r0 = r0.i     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            r4.put(r5, r0)     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            java.lang.String r5 = "lat"
            java.util.List r0 = r8.r     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            java.lang.Object r0 = r0.get(r3)     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            adon.x r0 = (adon.x) r0     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            double r6 = r0.c     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            java.lang.String r0 = java.lang.String.valueOf(r6)     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            r4.put(r5, r0)     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            java.lang.String r5 = "lon"
            java.util.List r0 = r8.r     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            java.lang.Object r0 = r0.get(r3)     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            adon.x r0 = (adon.x) r0     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            double r6 = r0.d     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            java.lang.String r0 = java.lang.String.valueOf(r6)     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            r4.put(r5, r0)     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            java.lang.String r5 = "cellId"
            java.util.List r0 = r8.r     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            java.lang.Object r0 = r0.get(r3)     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            adon.x r0 = (adon.x) r0     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            java.lang.String r0 = r0.e     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            r4.put(r5, r0)     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            java.lang.String r5 = "lac"
            java.util.List r0 = r8.r     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            java.lang.Object r0 = r0.get(r3)     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            adon.x r0 = (adon.x) r0     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            java.lang.String r0 = r0.f     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            r4.put(r5, r0)     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            java.lang.String r5 = "mcc"
            java.util.List r0 = r8.r     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            java.lang.Object r0 = r0.get(r3)     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            adon.x r0 = (adon.x) r0     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            java.lang.String r0 = r0.h     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            r4.put(r5, r0)     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            java.lang.String r5 = "mnc"
            java.util.List r0 = r8.r     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            java.lang.Object r0 = r0.get(r3)     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            adon.x r0 = (adon.x) r0     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            java.lang.String r0 = r0.g     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            r4.put(r5, r0)     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            r2.put(r4)     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            int r0 = r3 + 1
            r3 = r0
            goto L_0x005c
        L_0x011a:
            java.lang.String r0 = r8.y     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            goto L_0x007b
        L_0x011e:
            java.lang.String r0 = r8.v     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            goto L_0x007b
        L_0x0122:
            adon.o r2 = adon.o.a()     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            java.lang.String r1 = r1.toString()     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            java.lang.String r3 = r8.p     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            java.lang.String r4 = "2.0.2"
            org.apache.http.HttpResponse r0 = r2.a(r1, r0, r3, r4)     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            java.lang.String r1 = "X-ADON-STATUS"
            org.apache.http.Header r0 = r0.getLastHeader(r1)     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            java.lang.String r0 = r0.getValue()     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            int r0 = r0.intValue()     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            java.lang.String r1 = "AdMAnager"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            java.lang.String r3 = "reStatus: "
            r2.<init>(r3)     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            java.lang.String r3 = java.lang.String.valueOf(r0)     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            java.lang.String r2 = r2.toString()     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            android.util.Log.i(r1, r2)     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            if (r0 < 0) goto L_0x0165
            java.util.LinkedList r0 = new java.util.LinkedList     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            r0.<init>()     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            r8.r = r0     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
        L_0x0165:
            adon.a r0 = new adon.a     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            java.util.List r1 = r8.r     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            r0.<init>(r1)     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            r8.a(r0)     // Catch:{ JSONException -> 0x0171, InvalidKeyException -> 0x0197, ParseException -> 0x0194, NoSuchAlgorithmException -> 0x0191, NoSuchPaddingException -> 0x018e, InvalidKeySpecException -> 0x018b, IllegalBlockSizeException -> 0x0188, BadPaddingException -> 0x0185, NoSuchProviderException -> 0x0182, IOException -> 0x017f, v -> 0x017c, URISyntaxException -> 0x0179, Exception -> 0x0176, all -> 0x0174 }
            goto L_0x007d
        L_0x0171:
            r0 = move-exception
            goto L_0x007d
        L_0x0174:
            r0 = move-exception
            throw r0
        L_0x0176:
            r0 = move-exception
            goto L_0x007d
        L_0x0179:
            r0 = move-exception
            goto L_0x007d
        L_0x017c:
            r0 = move-exception
            goto L_0x007d
        L_0x017f:
            r0 = move-exception
            goto L_0x007d
        L_0x0182:
            r0 = move-exception
            goto L_0x007d
        L_0x0185:
            r0 = move-exception
            goto L_0x007d
        L_0x0188:
            r0 = move-exception
            goto L_0x007d
        L_0x018b:
            r0 = move-exception
            goto L_0x007d
        L_0x018e:
            r0 = move-exception
            goto L_0x007d
        L_0x0191:
            r0 = move-exception
            goto L_0x007d
        L_0x0194:
            r0 = move-exception
            goto L_0x007d
        L_0x0197:
            r0 = move-exception
            goto L_0x007d
        */
        throw new UnsupportedOperationException("Method not decompiled: adon.r.b(com.vpon.adon.android.AdView):void");
    }

    private String c() {
        return String.valueOf(this.a.getFilesDir().getParent()) + File.separator + "appDataFile";
    }

    private static String d() {
        return new SimpleDateFormat("yyyy/MM/dd HH:mm:ss z").format(Calendar.getInstance().getTime());
    }

    private static /* synthetic */ int[] e() {
        int[] iArr = A;
        if (iArr == null) {
            iArr = new int[AdOnPlatform.values().length];
            try {
                iArr[AdOnPlatform.CN.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[AdOnPlatform.TW.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            A = iArr;
        }
        return iArr;
    }

    public final void a(AdView adView) {
        this.s.add(adView);
    }

    public final void a(AdView adView, l lVar) {
        new Thread(new w(this, adView, lVar)).start();
    }

    public final void a(AdView adView, Handler handler) {
        new Thread(new u(this, adView, handler)).start();
    }

    public final void a(AdView adView, String str) {
        new Thread(new z(this, adView, str)).start();
    }
}
