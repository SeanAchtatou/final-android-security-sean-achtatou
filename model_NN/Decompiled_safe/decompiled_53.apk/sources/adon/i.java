package adon;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.vpon.adon.android.AdView;
import com.vpon.adon.android.WebInApp;
import java.io.IOException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

final class i extends WebViewClient {
    private AdView a;
    private Context b;
    private l c;
    private Intent d;
    private boolean e = true;

    public i(AdView adView, l lVar, Context context) {
        this.a = adView;
        this.c = lVar;
        this.b = context;
    }

    public final void onPageFinished(WebView webView, String str) {
        if (this.e) {
            AdView adView = this.a;
            adView.removeView(adView.g);
            adView.f = adView.e;
            adView.g = adView.h;
            adView.addView(adView.g);
            if (adView.j != null) {
                adView.j.onRecevieAd(adView);
            }
            this.e = false;
        }
        super.onPageFinished(webView, str);
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        String str2;
        if (str.startsWith("tel://")) {
            this.d = new Intent("android.intent.action.DIAL", Uri.parse(str));
            this.b.startActivity(this.d);
            this.a.a();
            return true;
        } else if (str.startsWith("sms://")) {
            this.d = new Intent("android.intent.action.SENDTO", Uri.parse(str));
            String[] split = str.split("body=");
            if (split.length >= 2) {
                Uri.parse(split[0]);
                String str3 = split[0].split("//")[1];
                this.d = new Intent("android.intent.action.SENDTO", Uri.parse("smsto:" + str3.substring(0, str3.length() - 1)));
                this.d.putExtra("sms_body", URLDecoder.decode(split[1]));
            }
            this.b.startActivity(this.d);
            this.a.a();
            return true;
        } else if (str.startsWith("mailto:")) {
            this.b.startActivity(new Intent("android.intent.action.SENDTO", Uri.parse(str)));
            this.a.a();
            return true;
        } else if (str.startsWith("market:")) {
            this.b.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
            this.a.a();
            return true;
        } else if (str.startsWith("http://")) {
            try {
                if (str.contains("&t=o")) {
                    this.d = new Intent("android.intent.action.VIEW", Uri.parse(str.split("&t=o")[0]));
                } else {
                    String str4 = str.split("&t=i")[0];
                    this.d = new Intent(this.b, WebInApp.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("url", str4);
                    bundle.putInt("adWidth", this.c.b);
                    this.d.putExtras(bundle);
                }
                this.b.startActivity(this.d);
                this.a.a();
            } catch (ActivityNotFoundException e2) {
                e2.printStackTrace();
                this.a.a("WebInApp Error");
            } catch (Exception e3) {
                e3.printStackTrace();
            }
            return true;
        } else if (!str.startsWith("redir://")) {
            return false;
        } else {
            try {
                boolean z = !str.contains("redir?t=o");
                String str5 = str.split("redir://")[1];
                switch (z) {
                    case true:
                        str2 = str5.split("t=i")[0];
                        break;
                    default:
                        str2 = str5.split("t=o")[0];
                        break;
                }
                StringBuilder append = new StringBuilder(String.valueOf("http://" + str2)).append("data=");
                o.a();
                String sb = append.append(URLEncoder.encode(e.a(p.a(this.c.h.toString().getBytes("UTF-8"), p.a(p.a(e.a(p.a("http://www.vpon.com/v/index.jsp88623698333service@vpon.com", "O4+gWecUjeNBCuwRbLprfpbXPUGl3A/Ac8HJxnr8A85UqiTMIJ50RxEkEp/urCSUrkICRuVt0JVss0komwdQQBz94D5XSKE3zlWSMplh+Al5BR99slEzLIZU7TJ7Upk/ZmZlY3UpXmRMX+zLEqybApfAlLEr67QoJBdbEpjHsnWxx5nZwHAdeYXwtgSFVGgtydkZBR+PGEjriyWhWkSTgFbO8yJTMxirdEtldU+604SemOFub2WqVHC86lrN+pnvfO9pfs+d99QgMno8MRVEX0EqTnf+qiVIJrCOJbv0cj7SWws7LTgWrGssmur08FEyY0mPCqUqY0/AEkTnwg4PkiOwny3y3hPVvRipMDxBUiTeOIC9U8UVGPD/T5bX+Qrrb2CV6DkfucIU0Ie5q8iC8KCVI6Uh4wu0SDavbqaZH5Dhtb2zLnmOF4qwBF8We4gkRJ63bosreSm1uGn3UoC2CRvOJTyyf8CcCijLnEm0V7Ob4GXFCJlnPKVMsJxbnlQV+ik9IKny/BGkE5j6Gqq1HQ=="))))), false))).toString();
                switch (z) {
                    case true:
                        this.d = new Intent(this.b, WebInApp.class);
                        Bundle bundle2 = new Bundle();
                        bundle2.putString("url", sb);
                        bundle2.putInt("adWidth", this.c.b);
                        this.d.putExtras(bundle2);
                        break;
                    default:
                        this.d = new Intent("android.intent.action.VIEW", Uri.parse(sb));
                        break;
                }
                this.b.startActivity(this.d);
                this.a.a();
            } catch (InvalidKeyException e4) {
                e4.printStackTrace();
            } catch (NoSuchAlgorithmException e5) {
                e5.printStackTrace();
            } catch (InvalidKeySpecException e6) {
                e6.printStackTrace();
            } catch (NoSuchPaddingException e7) {
                e7.printStackTrace();
            } catch (IllegalBlockSizeException e8) {
                e8.printStackTrace();
            } catch (BadPaddingException e9) {
                e9.printStackTrace();
            } catch (IOException e10) {
                e10.printStackTrace();
            } catch (NoSuchProviderException e11) {
                e11.printStackTrace();
            } catch (ActivityNotFoundException e12) {
                e12.printStackTrace();
                this.a.a("WebInApp Error");
            } catch (Exception e13) {
                e13.printStackTrace();
            }
            return true;
        }
    }
}
