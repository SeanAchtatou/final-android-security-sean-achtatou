package adon;

import android.graphics.Bitmap;
import android.os.Message;
import android.view.View;
import android.webkit.ConsoleMessage;
import android.webkit.GeolocationPermissions;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebStorage;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.RelativeLayout;

public final class d extends WebChromeClient {
    private static /* synthetic */ int[] g;
    public g a;
    private RelativeLayout b;
    private WebView c;
    private Button d;
    private View e;
    private WebChromeClient.CustomViewCallback f;

    public d(View view, RelativeLayout relativeLayout, WebView webView, Button button) {
        this.e = view;
        this.b = relativeLayout;
        this.c = webView;
        this.d = button;
    }

    public static /* synthetic */ int[] b() {
        int[] iArr = g;
        if (iArr == null) {
            iArr = new int[g.a().length];
            try {
                iArr[g.CUSTOMVIEW.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[g.WEBVIEW.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            g = iArr;
        }
        return iArr;
    }

    public final void a() {
        if (this.f != null) {
            try {
                this.f.onCustomViewHidden();
            } catch (NullPointerException e2) {
            }
            this.f = null;
        }
    }

    public final Bitmap getDefaultVideoPoster() {
        return null;
    }

    public final View getVideoLoadingProgressView() {
        return null;
    }

    public final void getVisitedHistory(ValueCallback valueCallback) {
    }

    public final void onCloseWindow(WebView webView) {
    }

    public final void onConsoleMessage(String str, int i, String str2) {
    }

    public final boolean onConsoleMessage(ConsoleMessage consoleMessage) {
        return true;
    }

    public final boolean onCreateWindow(WebView webView, boolean z, boolean z2, Message message) {
        return false;
    }

    public final void onExceededDatabaseQuota(String str, String str2, long j, long j2, long j3, WebStorage.QuotaUpdater quotaUpdater) {
        quotaUpdater.updateQuota(j);
    }

    public final void onGeolocationPermissionsHidePrompt() {
    }

    public final void onGeolocationPermissionsShowPrompt(String str, GeolocationPermissions.Callback callback) {
    }

    public final void onHideCustomView() {
        this.f.onCustomViewHidden();
    }

    public final boolean onJsAlert(WebView webView, String str, String str2, JsResult jsResult) {
        return false;
    }

    public final boolean onJsBeforeUnload(WebView webView, String str, String str2, JsResult jsResult) {
        return false;
    }

    public final boolean onJsConfirm(WebView webView, String str, String str2, JsResult jsResult) {
        return false;
    }

    public final boolean onJsPrompt(WebView webView, String str, String str2, String str3, JsPromptResult jsPromptResult) {
        return false;
    }

    public final boolean onJsTimeout() {
        return true;
    }

    public final void onProgressChanged(WebView webView, int i) {
    }

    public final void onReachedMaxAppCacheSize(long j, long j2, WebStorage.QuotaUpdater quotaUpdater) {
        quotaUpdater.updateQuota(0);
    }

    public final void onReceivedIcon(WebView webView, Bitmap bitmap) {
    }

    public final void onReceivedTitle(WebView webView, String str) {
    }

    public final void onReceivedTouchIconUrl(WebView webView, String str, boolean z) {
    }

    public final void onRequestFocus(WebView webView) {
    }

    public final void onShowCustomView(View view, WebChromeClient.CustomViewCallback customViewCallback) {
        this.e = view;
        this.b.addView(this.e, new RelativeLayout.LayoutParams(-1, -1));
        this.f = customViewCallback;
        this.a = g.CUSTOMVIEW;
        this.c.setVisibility(4);
        this.d.setVisibility(4);
    }
}
