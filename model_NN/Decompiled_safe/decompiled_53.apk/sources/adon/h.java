package adon;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.WindowManager;

public final class h {
    public static String a() {
        try {
            return b.a("") ? Build.MODEL : "";
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String a(Context context) {
        String str;
        try {
            str = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        } catch (Exception e) {
            e.printStackTrace();
            str = "";
        }
        return b.a(str) ? c(context) : str;
    }

    public static q b(Context context) {
        q qVar = new q();
        try {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
            qVar.a = displayMetrics.widthPixels;
            qVar.b = displayMetrics.heightPixels;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return qVar;
    }

    public static String b() {
        try {
            return Build.VERSION.SDK;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    private static String c(Context context) {
        try {
            return ((WifiManager) context.getSystemService("wifi")).getConnectionInfo().getMacAddress();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }
}
