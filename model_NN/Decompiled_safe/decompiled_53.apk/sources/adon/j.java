package adon;

import com.vpon.adon.android.AdView;

public final class j implements Runnable {
    private /* synthetic */ AdView a;

    public j(AdView adView) {
        this.a = adView;
    }

    public final void run() {
        if (this.a.i && this.a.a && !this.a.b && this.a.f != null && this.a.f.g > 0) {
            int i = this.a.f.g - 1;
            this.a.f.g = i;
            if (i == 0) {
                r.a(this.a.c).a(this.a, this.a.d);
            }
        }
        if (this.a.k != null) {
            this.a.k.postDelayed(this.a.l, 1000);
        }
    }
}
