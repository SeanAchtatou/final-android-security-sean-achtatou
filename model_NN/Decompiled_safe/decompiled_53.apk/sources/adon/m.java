package adon;

import android.location.Location;
import java.io.Serializable;
import org.json.JSONException;
import org.json.JSONObject;

public final class m implements Serializable {
    public String a;
    public String b;
    public String c;
    public Double d;
    public Double e;
    public String f;
    public String g;
    public String h;
    public String i;
    public Location j;

    public final String toString() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("imei", this.c);
            jSONObject.put("licensekey", this.b);
            jSONObject.put("adId", this.a);
            jSONObject.put("lat", this.d);
            jSONObject.put("lon", this.e);
            jSONObject.put("cellId", this.i);
            jSONObject.put("lac", this.h);
            jSONObject.put("mcc", this.f);
            jSONObject.put("mnc", this.g);
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
        return jSONObject.toString();
    }
}
