package com.admob.android.ads;

import android.util.Log;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

/* compiled from: AdMobURLConnector */
final class i extends r {
    private HttpURLConnection m;
    private URL n;

    public i(String str, String str2, String str3, h hVar, int i, Map<String, String> map, String str4) {
        super(str2, str3, hVar, i, map, str4);
        try {
            this.n = new URL(str);
            this.i = this.n;
        } catch (MalformedURLException e) {
            this.n = null;
            this.c = e;
        }
        this.m = null;
        this.e = 0;
    }

    /* JADX WARNING: Removed duplicated region for block: B:40:0x00e6 A[SYNTHETIC, Splitter:B:40:0x00e6] */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x01cd A[SYNTHETIC, Splitter:B:81:0x01cd] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a() {
        /*
            r13 = this;
            r11 = 0
            r10 = 2
            r9 = 1
            r8 = 0
            java.net.URL r1 = r13.n
            if (r1 != 0) goto L_0x0027
            com.admob.android.ads.h r1 = r13.h
            if (r1 == 0) goto L_0x0018
            com.admob.android.ads.h r1 = r13.h
            java.lang.Exception r2 = new java.lang.Exception
            java.lang.String r3 = "url was null"
            r2.<init>(r3)
            r1.a(r13, r2)
        L_0x0018:
            r1 = r8
        L_0x0019:
            if (r1 != 0) goto L_0x0026
            com.admob.android.ads.h r2 = r13.h
            if (r2 == 0) goto L_0x0026
            com.admob.android.ads.h r2 = r13.h
            java.lang.Exception r3 = r13.c
            r2.a(r13, r3)
        L_0x0026:
            return r1
        L_0x0027:
            java.net.HttpURLConnection.setFollowRedirects(r9)
            r3 = r8
        L_0x002b:
            int r1 = r13.e
            int r2 = r13.f
            if (r1 >= r2) goto L_0x01e8
            if (r3 != 0) goto L_0x01e8
            java.lang.String r1 = "AdMobSDK"
            boolean r1 = android.util.Log.isLoggable(r1, r10)
            if (r1 == 0) goto L_0x0061
            java.lang.String r1 = "AdMobSDK"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r4 = "attempt "
            java.lang.StringBuilder r2 = r2.append(r4)
            int r4 = r13.e
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r4 = " to connect to url "
            java.lang.StringBuilder r2 = r2.append(r4)
            java.net.URL r4 = r13.n
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r2 = r2.toString()
            android.util.Log.v(r1, r2)
        L_0x0061:
            r13.h()     // Catch:{ Exception -> 0x00c6, all -> 0x01c9 }
            java.net.URL r1 = r13.n     // Catch:{ Exception -> 0x00c6, all -> 0x01c9 }
            java.net.URLConnection r1 = r1.openConnection()     // Catch:{ Exception -> 0x00c6, all -> 0x01c9 }
            java.net.HttpURLConnection r1 = (java.net.HttpURLConnection) r1     // Catch:{ Exception -> 0x00c6, all -> 0x01c9 }
            r13.m = r1     // Catch:{ Exception -> 0x00c6, all -> 0x01c9 }
            java.net.HttpURLConnection r1 = r13.m     // Catch:{ Exception -> 0x00c6, all -> 0x01c9 }
            if (r1 == 0) goto L_0x01e5
            java.net.HttpURLConnection r1 = r13.m     // Catch:{ Exception -> 0x00c6, all -> 0x01c9 }
            java.lang.String r2 = "User-Agent"
            java.lang.String r4 = c()     // Catch:{ Exception -> 0x00c6, all -> 0x01c9 }
            r1.setRequestProperty(r2, r4)     // Catch:{ Exception -> 0x00c6, all -> 0x01c9 }
            java.lang.String r1 = r13.g     // Catch:{ Exception -> 0x00c6, all -> 0x01c9 }
            if (r1 == 0) goto L_0x008a
            java.net.HttpURLConnection r1 = r13.m     // Catch:{ Exception -> 0x00c6, all -> 0x01c9 }
            java.lang.String r2 = "X-ADMOB-ISU"
            java.lang.String r4 = r13.g     // Catch:{ Exception -> 0x00c6, all -> 0x01c9 }
            r1.setRequestProperty(r2, r4)     // Catch:{ Exception -> 0x00c6, all -> 0x01c9 }
        L_0x008a:
            java.net.HttpURLConnection r1 = r13.m     // Catch:{ Exception -> 0x00c6, all -> 0x01c9 }
            int r2 = r13.b     // Catch:{ Exception -> 0x00c6, all -> 0x01c9 }
            r1.setConnectTimeout(r2)     // Catch:{ Exception -> 0x00c6, all -> 0x01c9 }
            java.net.HttpURLConnection r1 = r13.m     // Catch:{ Exception -> 0x00c6, all -> 0x01c9 }
            int r2 = r13.b     // Catch:{ Exception -> 0x00c6, all -> 0x01c9 }
            r1.setReadTimeout(r2)     // Catch:{ Exception -> 0x00c6, all -> 0x01c9 }
            java.util.Map r1 = r13.d     // Catch:{ Exception -> 0x00c6, all -> 0x01c9 }
            if (r1 == 0) goto L_0x00f6
            java.util.Map r1 = r13.d     // Catch:{ Exception -> 0x00c6, all -> 0x01c9 }
            java.util.Set r1 = r1.keySet()     // Catch:{ Exception -> 0x00c6, all -> 0x01c9 }
            java.util.Iterator r4 = r1.iterator()     // Catch:{ Exception -> 0x00c6, all -> 0x01c9 }
        L_0x00a6:
            boolean r1 = r4.hasNext()     // Catch:{ Exception -> 0x00c6, all -> 0x01c9 }
            if (r1 == 0) goto L_0x00f6
            java.lang.Object r1 = r4.next()     // Catch:{ Exception -> 0x00c6, all -> 0x01c9 }
            r0 = r1
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x00c6, all -> 0x01c9 }
            r2 = r0
            if (r2 == 0) goto L_0x00a6
            java.util.Map r1 = r13.d     // Catch:{ Exception -> 0x00c6, all -> 0x01c9 }
            java.lang.Object r1 = r1.get(r2)     // Catch:{ Exception -> 0x00c6, all -> 0x01c9 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ Exception -> 0x00c6, all -> 0x01c9 }
            if (r1 == 0) goto L_0x00a6
            java.net.HttpURLConnection r5 = r13.m     // Catch:{ Exception -> 0x00c6, all -> 0x01c9 }
            r5.addRequestProperty(r2, r1)     // Catch:{ Exception -> 0x00c6, all -> 0x01c9 }
            goto L_0x00a6
        L_0x00c6:
            r1 = move-exception
            r2 = r11
        L_0x00c8:
            java.lang.String r3 = "AdMobSDK"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x01d9 }
            r4.<init>()     // Catch:{ all -> 0x01d9 }
            java.lang.String r5 = "could not open connection to url "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x01d9 }
            java.net.URL r5 = r13.n     // Catch:{ all -> 0x01d9 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x01d9 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x01d9 }
            android.util.Log.w(r3, r4, r1)     // Catch:{ all -> 0x01d9 }
            r13.c = r1     // Catch:{ all -> 0x01d9 }
            if (r2 == 0) goto L_0x00e9
            r2.close()     // Catch:{ Exception -> 0x01d4 }
        L_0x00e9:
            r13.h()
            r1 = r8
        L_0x00ed:
            int r2 = r13.e
            int r2 = r2 + 1
            r13.e = r2
            r3 = r1
            goto L_0x002b
        L_0x00f6:
            java.lang.String r1 = r13.l     // Catch:{ Exception -> 0x00c6, all -> 0x01c9 }
            if (r1 == 0) goto L_0x01a9
            java.net.HttpURLConnection r1 = r13.m     // Catch:{ Exception -> 0x00c6, all -> 0x01c9 }
            java.lang.String r2 = "POST"
            r1.setRequestMethod(r2)     // Catch:{ Exception -> 0x00c6, all -> 0x01c9 }
            java.net.HttpURLConnection r1 = r13.m     // Catch:{ Exception -> 0x00c6, all -> 0x01c9 }
            r2 = 1
            r1.setDoOutput(r2)     // Catch:{ Exception -> 0x00c6, all -> 0x01c9 }
            java.net.HttpURLConnection r1 = r13.m     // Catch:{ Exception -> 0x00c6, all -> 0x01c9 }
            java.lang.String r2 = "Content-Type"
            java.lang.String r4 = r13.a     // Catch:{ Exception -> 0x00c6, all -> 0x01c9 }
            r1.setRequestProperty(r2, r4)     // Catch:{ Exception -> 0x00c6, all -> 0x01c9 }
            java.net.HttpURLConnection r1 = r13.m     // Catch:{ Exception -> 0x00c6, all -> 0x01c9 }
            java.lang.String r2 = "Content-Length"
            java.lang.String r4 = r13.l     // Catch:{ Exception -> 0x00c6, all -> 0x01c9 }
            int r4 = r4.length()     // Catch:{ Exception -> 0x00c6, all -> 0x01c9 }
            java.lang.String r4 = java.lang.Integer.toString(r4)     // Catch:{ Exception -> 0x00c6, all -> 0x01c9 }
            r1.setRequestProperty(r2, r4)     // Catch:{ Exception -> 0x00c6, all -> 0x01c9 }
            java.net.HttpURLConnection r1 = r13.m     // Catch:{ Exception -> 0x00c6, all -> 0x01c9 }
            java.io.OutputStream r1 = r1.getOutputStream()     // Catch:{ Exception -> 0x00c6, all -> 0x01c9 }
            java.io.BufferedWriter r2 = new java.io.BufferedWriter     // Catch:{ Exception -> 0x00c6, all -> 0x01c9 }
            java.io.OutputStreamWriter r4 = new java.io.OutputStreamWriter     // Catch:{ Exception -> 0x00c6, all -> 0x01c9 }
            r4.<init>(r1)     // Catch:{ Exception -> 0x00c6, all -> 0x01c9 }
            r1 = 4096(0x1000, float:5.74E-42)
            r2.<init>(r4, r1)     // Catch:{ Exception -> 0x00c6, all -> 0x01c9 }
            java.lang.String r1 = r13.l     // Catch:{ Exception -> 0x01e0 }
            r2.write(r1)     // Catch:{ Exception -> 0x01e0 }
            r2.close()     // Catch:{ Exception -> 0x01e0 }
            r1 = r11
        L_0x013c:
            java.net.HttpURLConnection r2 = r13.m     // Catch:{ Exception -> 0x01a3, all -> 0x01db }
            int r2 = r2.getResponseCode()     // Catch:{ Exception -> 0x01a3, all -> 0x01db }
            java.lang.String r4 = "AdMobSDK"
            r5 = 2
            boolean r4 = android.util.Log.isLoggable(r4, r5)     // Catch:{ Exception -> 0x01a3, all -> 0x01db }
            if (r4 == 0) goto L_0x016b
            java.net.HttpURLConnection r4 = r13.m     // Catch:{ Exception -> 0x01a3, all -> 0x01db }
            java.lang.String r5 = "X-AdMob-AdSrc"
            java.lang.String r4 = r4.getHeaderField(r5)     // Catch:{ Exception -> 0x01a3, all -> 0x01db }
            java.lang.String r5 = "AdMobSDK"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01a3, all -> 0x01db }
            r6.<init>()     // Catch:{ Exception -> 0x01a3, all -> 0x01db }
            java.lang.String r7 = "Ad response came from server "
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x01a3, all -> 0x01db }
            java.lang.StringBuilder r4 = r6.append(r4)     // Catch:{ Exception -> 0x01a3, all -> 0x01db }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x01a3, all -> 0x01db }
            android.util.Log.v(r5, r4)     // Catch:{ Exception -> 0x01a3, all -> 0x01db }
        L_0x016b:
            r4 = 200(0xc8, float:2.8E-43)
            if (r2 < r4) goto L_0x01e3
            r4 = 300(0x12c, float:4.2E-43)
            if (r2 >= r4) goto L_0x01e3
            java.net.HttpURLConnection r2 = r13.m     // Catch:{ Exception -> 0x01a3, all -> 0x01db }
            java.net.URL r2 = r2.getURL()     // Catch:{ Exception -> 0x01a3, all -> 0x01db }
            r13.i = r2     // Catch:{ Exception -> 0x01a3, all -> 0x01db }
            boolean r2 = r13.k     // Catch:{ Exception -> 0x01a3, all -> 0x01db }
            if (r2 == 0) goto L_0x01b6
            java.io.BufferedInputStream r2 = new java.io.BufferedInputStream     // Catch:{ Exception -> 0x01a3, all -> 0x01db }
            java.net.HttpURLConnection r3 = r13.m     // Catch:{ Exception -> 0x01a3, all -> 0x01db }
            java.io.InputStream r3 = r3.getInputStream()     // Catch:{ Exception -> 0x01a3, all -> 0x01db }
            r4 = 4096(0x1000, float:5.74E-42)
            r2.<init>(r3, r4)     // Catch:{ Exception -> 0x01a3, all -> 0x01db }
            r3 = 4096(0x1000, float:5.74E-42)
            byte[] r3 = new byte[r3]     // Catch:{ Exception -> 0x01a3, all -> 0x01db }
            java.io.ByteArrayOutputStream r4 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x01a3, all -> 0x01db }
            r5 = 4096(0x1000, float:5.74E-42)
            r4.<init>(r5)     // Catch:{ Exception -> 0x01a3, all -> 0x01db }
        L_0x0197:
            int r5 = r2.read(r3)     // Catch:{ Exception -> 0x01a3, all -> 0x01db }
            r6 = -1
            if (r5 == r6) goto L_0x01b0
            r6 = 0
            r4.write(r3, r6, r5)     // Catch:{ Exception -> 0x01a3, all -> 0x01db }
            goto L_0x0197
        L_0x01a3:
            r2 = move-exception
            r12 = r2
            r2 = r1
            r1 = r12
            goto L_0x00c8
        L_0x01a9:
            java.net.HttpURLConnection r1 = r13.m     // Catch:{ Exception -> 0x00c6, all -> 0x01c9 }
            r1.connect()     // Catch:{ Exception -> 0x00c6, all -> 0x01c9 }
            r1 = r11
            goto L_0x013c
        L_0x01b0:
            byte[] r2 = r4.toByteArray()     // Catch:{ Exception -> 0x01a3, all -> 0x01db }
            r13.j = r2     // Catch:{ Exception -> 0x01a3, all -> 0x01db }
        L_0x01b6:
            com.admob.android.ads.h r2 = r13.h     // Catch:{ Exception -> 0x01a3, all -> 0x01db }
            if (r2 == 0) goto L_0x01bf
            com.admob.android.ads.h r2 = r13.h     // Catch:{ Exception -> 0x01a3, all -> 0x01db }
            r2.a(r13)     // Catch:{ Exception -> 0x01a3, all -> 0x01db }
        L_0x01bf:
            r2 = r9
        L_0x01c0:
            r13.h()     // Catch:{ Exception -> 0x01a3, all -> 0x01db }
            r13.h()
            r1 = r2
            goto L_0x00ed
        L_0x01c9:
            r1 = move-exception
            r2 = r11
        L_0x01cb:
            if (r2 == 0) goto L_0x01d0
            r2.close()     // Catch:{ Exception -> 0x01d7 }
        L_0x01d0:
            r13.h()
            throw r1
        L_0x01d4:
            r1 = move-exception
            goto L_0x00e9
        L_0x01d7:
            r2 = move-exception
            goto L_0x01d0
        L_0x01d9:
            r1 = move-exception
            goto L_0x01cb
        L_0x01db:
            r2 = move-exception
            r12 = r2
            r2 = r1
            r1 = r12
            goto L_0x01cb
        L_0x01e0:
            r1 = move-exception
            goto L_0x00c8
        L_0x01e3:
            r2 = r3
            goto L_0x01c0
        L_0x01e5:
            r1 = r11
            r2 = r3
            goto L_0x01c0
        L_0x01e8:
            r1 = r3
            goto L_0x0019
        */
        throw new UnsupportedOperationException("Method not decompiled: com.admob.android.ads.i.a():boolean");
    }

    private void h() {
        if (this.m != null) {
            this.m.disconnect();
            this.m = null;
        }
    }

    public final void b() {
        h();
        this.h = null;
    }

    public final void run() {
        try {
            a();
        } catch (Exception e) {
            if (Log.isLoggable(AdManager.LOG, 6)) {
                Log.e(AdManager.LOG, "exception caught in AdMobURLConnector.run(), " + e.getMessage());
            }
        }
    }
}
