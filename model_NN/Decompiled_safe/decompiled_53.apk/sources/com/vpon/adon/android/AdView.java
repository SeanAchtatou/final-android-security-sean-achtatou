package com.vpon.adon.android;

import adon.f;
import adon.j;
import adon.l;
import adon.r;
import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import com.adwhirl.util.AdWhirlUtil;
import com.bolfish.TonePickerChinese.R;

public final class AdView extends RelativeLayout {
    public boolean a;
    public boolean b;
    public Context c;
    public Handler d;
    public l e;
    public l f;
    public f g;
    public f h;
    public boolean i;
    public AdListener j;
    public Handler k;
    public Runnable l;
    private String m;
    private AdOnPlatform n;

    public AdView(Context context) {
        this(context, null, 0);
    }

    public AdView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public AdView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.m = null;
        this.a = false;
        this.b = false;
        this.e = null;
        this.f = null;
        this.g = null;
        this.h = null;
        this.i = false;
        this.l = new j(this);
        this.f = null;
        this.e = null;
        this.c = context;
        this.d = new Handler();
        this.g = new f(context);
        addView(this.g);
        r.a(context).a(this);
    }

    public final void a() {
        r.a(this.c).a(this, this.f);
    }

    public final void a(String str) {
        r.a(this.c).a(this, str);
    }

    public void b() {
        if (this.j != null) {
            this.j.onFailedToRecevieAd(this);
        }
        if (this.f != null) {
            this.f.g = 120;
            return;
        }
        l lVar = new l();
        lVar.g = 120;
        this.f = null;
        this.f = lVar;
    }

    public final String getLicenseKey() {
        return this.m;
    }

    public final AdOnPlatform getPlatform() {
        return this.n;
    }

    public final String getVersion() {
        return "2.0.2";
    }

    /* access modifiers changed from: protected */
    public final void onWindowVisibilityChanged(int i2) {
        switch (i2) {
            case R.styleable.com_admob_android_ads_AdView_backgroundColor:
                this.a = true;
                return;
            case 4:
                this.a = false;
                return;
            case AdWhirlUtil.NETWORK_TYPE_QUATTRO:
                this.a = false;
                return;
            default:
                return;
        }
    }

    public final void pauseAdAutoRefresh() {
        this.b = true;
    }

    public final void refreshAd() {
        this.i = false;
        r.a(this.c).a(this, this.d);
    }

    public final void restartAdAutoRefresh() {
        if (this.i) {
            this.b = false;
        }
    }

    public final void setAdListener(AdListener adListener) {
        this.j = adListener;
    }

    public final void setLicenseKey(String str) {
        this.m = str;
    }

    public final void setLicenseKey(String str, AdOnPlatform adOnPlatform, boolean z) {
        this.m = str;
        this.i = z;
        this.n = adOnPlatform;
        r.a(this.c).a(this, this.d);
        if (this.k == null) {
            this.k = new Handler();
            this.k.post(this.l);
        }
        setLayoutParams(new RelativeLayout.LayoutParams(-2, -2));
    }
}
