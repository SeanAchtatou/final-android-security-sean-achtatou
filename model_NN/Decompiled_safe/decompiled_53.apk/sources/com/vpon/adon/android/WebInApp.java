package com.vpon.adon.android;

import adon.b;
import adon.c;
import adon.d;
import adon.g;
import adon.k;
import android.app.Activity;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

public class WebInApp extends Activity {
    public d a;
    private RelativeLayout b;
    private WebView c;
    private Button d;

    public void onCreate(Bundle bundle) {
        RelativeLayout.LayoutParams layoutParams;
        String str;
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().addFlags(1024);
        this.b = new RelativeLayout(this);
        setContentView(this.b);
        Bundle extras = getIntent().getExtras();
        String string = extras.getString("url");
        if (b.a(string)) {
            finish();
        }
        ProgressBar progressBar = new ProgressBar(this);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams2.addRule(13);
        progressBar.setLayoutParams(layoutParams2);
        this.b.addView(progressBar);
        this.c = new WebView(this);
        this.c.getSettings().setJavaScriptEnabled(true);
        this.c.getSettings().setSupportZoom(true);
        this.c.getSettings().setUseWideViewPort(true);
        this.c.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        this.c.loadUrl(string);
        this.b.addView(this.c, new RelativeLayout.LayoutParams(-1, -1));
        if (extras.getInt("adWidth") == 480) {
            layoutParams = new RelativeLayout.LayoutParams(75, 75);
            str = "/close75.png";
        } else {
            layoutParams = new RelativeLayout.LayoutParams(50, 50);
            str = "/close50.png";
        }
        BitmapDrawable bitmapDrawable = new BitmapDrawable(getClass().getResourceAsStream(str));
        this.d = new Button(this);
        layoutParams.addRule(11);
        this.d.setBackgroundDrawable(bitmapDrawable);
        this.d.setLayoutParams(layoutParams);
        this.d.setOnClickListener(new c(this));
        this.b.addView(this.d);
        this.a = new d(null, this.b, this.c, this.d);
        this.a.a = g.WEBVIEW;
        this.c.setWebChromeClient(this.a);
        this.c.setWebViewClient(new k(this));
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        boolean z;
        switch (i) {
            case 4:
                if (this.a != null) {
                    this.a.a();
                }
                switch (d.b()[this.a.a.ordinal()]) {
                    case 1:
                        z = true;
                        break;
                    default:
                        z = false;
                        break;
                }
                if (!z) {
                    finish();
                    break;
                } else {
                    this.a.a = g.WEBVIEW;
                    this.b.removeView(null);
                    this.c.setVisibility(0);
                    this.d.setVisibility(0);
                    break;
                }
        }
        return false;
    }
}
