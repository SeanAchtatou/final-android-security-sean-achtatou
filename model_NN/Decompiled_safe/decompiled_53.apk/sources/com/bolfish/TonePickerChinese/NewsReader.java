package com.bolfish.TonePickerChinese;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TabActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;

public class NewsReader extends TabActivity implements TabHost.OnTabChangeListener {
    private static final int DIALOG1_KEY = 0;
    private static final int DIALOG2_KEY = 1;
    private final int COLOR_BLACK = -16777216;
    private final int COLOR_BLUE = -16741493;
    private final int COLOR_GRAY = -8355712;
    private final int COLOR_ORANGE = -32704;
    private final int COLOR_RED = -65536;
    private final int COLOR_WHITE = -1;
    private final int COLOR_YELLOW = -128;
    TabHost mTabHost;

    /* access modifiers changed from: protected */
    public void onDestroy() {
        HelperUtil.ClearCache(this);
        try {
            new WebView(this).clearCache(true);
        } catch (Exception e) {
        }
        super.onDestroy();
    }

    /* access modifiers changed from: package-private */
    public void UpdateTab() {
        int nColor;
        int nTotal = this.mTabHost.getTabWidget().getChildCount();
        int nIndex = this.mTabHost.getCurrentTab();
        this.mTabHost.getTabWidget().setBackgroundResource(R.drawable.tabwidget_background);
        for (int i = 0; i < nTotal; i++) {
            try {
                View view = this.mTabHost.getTabWidget().getChildAt(i);
                if (i == nIndex) {
                    view.setBackgroundResource(R.drawable.tab_gradient);
                    nColor = -32704;
                } else {
                    view.setBackgroundResource(R.drawable.transparent_all);
                    nColor = -1;
                }
                TextView tabIndicator = (TextView) view.findViewById(((Integer) Class.forName("com.android.internal.R$id").getField("title").get(null)).intValue());
                if (tabIndicator != null) {
                    tabIndicator.setTextColor(nColor);
                }
                view.setPadding(5, 5, 5, 5);
            } catch (Exception e) {
            }
        }
    }

    public void onTabChanged(String arg0) {
        UpdateTab();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        UpdateTab();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 0:
                ProgressDialog dialog = new ProgressDialog(this);
                dialog.setTitle("Indeterminate");
                dialog.setMessage("Please wait while loading...");
                dialog.setIndeterminate(true);
                dialog.setCancelable(true);
                return dialog;
            case 1:
                ProgressDialog dialog2 = new ProgressDialog(this);
                dialog2.setMessage("Please wait while loading...");
                dialog2.setIndeterminate(true);
                dialog2.setCancelable(true);
                return dialog2;
            default:
                return null;
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.news_main);
        String szPhone = getString(R.string.phone_main);
        String szContact = getString(R.string.contact_main);
        this.mTabHost = getTabHost();
        this.mTabHost.addTab(this.mTabHost.newTabSpec("1").setIndicator(szPhone, getResources().getDrawable(R.drawable.phone)).setContent(new Intent(this, TonePickerMain.class)));
        this.mTabHost.addTab(this.mTabHost.newTabSpec("3").setIndicator(szContact, getResources().getDrawable(R.drawable.contact_32)).setContent(new Intent(this, ScrollView_List.class)));
        this.mTabHost.addTab(this.mTabHost.newTabSpec("2").setIndicator(getString(R.string.MP3_main), getResources().getDrawable(R.drawable.music)).setContent(new Intent(this, ScrollView_List_SelectSong.class)));
        this.mTabHost.setOnTabChangedListener(this);
        this.mTabHost.setCurrentTab(0);
        TabWidget tabWidget = this.mTabHost.getTabWidget();
        Tools m_Tools = new Tools(this);
        int bUpgrade = m_Tools.QuickLoadInt("Ring_Free", "ShowPro", 5);
        if (bUpgrade > 4) {
            ShowHelpDialog(R.string.dialog_getpro_title, R.string.dialog_getpro_description);
            m_Tools.QuickSave("Ring_Free", "ShowPro", 0);
            return;
        }
        m_Tools.QuickSave("Ring_Free", "ShowPro", bUpgrade + 1);
    }

    public void ShowHelpDialog(int nStringID, int nStringID1) {
        AlertDialog dlg = new AlertDialog.Builder(this).setIcon((int) R.drawable.help).setTitle(nStringID).setMessage(nStringID1).setNegativeButton(getString(R.string.dialog_getpro_check_yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                NewsReader.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=com.bolfish_paid.TonePickerWidget")));
            }
        }).setPositiveButton(getString(R.string.dialog_getpro_check_no), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        }).create();
        dlg.getWindow().setFlags(1024, 1024);
        dlg.show();
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }
}
