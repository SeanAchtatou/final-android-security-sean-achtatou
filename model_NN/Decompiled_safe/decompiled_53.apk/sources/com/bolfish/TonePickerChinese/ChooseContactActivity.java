package com.bolfish.TonePickerChinese;

import android.app.ListActivity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.Contacts;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class ChooseContactActivity extends ListActivity implements TextWatcher {
    /* access modifiers changed from: private */
    public SimpleCursorAdapter mAdapter;
    private TextView mFilter;
    private Uri mRingtoneUri;
    Uri m_uri = null;

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setTitle((int) R.string.choose_contact_title);
        this.mRingtoneUri = getIntent().getData();
        this.mRingtoneUri = null;
        setContentView((int) R.layout.choose_contact);
        try {
            this.mAdapter = new SimpleCursorAdapter(this, R.layout.contact_row, createCursor(""), new String[]{"custom_ringtone", "starred", "display_name"}, new int[]{R.id.row_ringtone, R.id.row_starred, R.id.row_display_name});
            this.mAdapter.setViewBinder(new SimpleCursorAdapter.ViewBinder() {
                public boolean setViewValue(View view, Cursor cursor, int columnIndex) {
                    String name = cursor.getColumnName(columnIndex);
                    String value = cursor.getString(columnIndex);
                    if (name.equals("custom_ringtone")) {
                        TextView tv = (TextView) view;
                        if (value == null || value.length() <= 0) {
                            view.setVisibility(4);
                        } else {
                            view.setVisibility(0);
                            Uri uri = Uri.parse(value);
                            if (uri != null) {
                                tv.setText(ChooseContactActivity.this.GetRingtoneName(uri));
                            }
                        }
                        return true;
                    } else if (!name.equals("starred")) {
                        return false;
                    } else {
                        if (value == null || !value.equals("1")) {
                            view.setVisibility(4);
                        } else {
                            view.setVisibility(0);
                        }
                        return true;
                    }
                }
            });
            setListAdapter(this.mAdapter);
            getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView parent, View view, int position, long id) {
                    Cursor c = ChooseContactActivity.this.mAdapter.getCursor();
                    String contactId = c.getString(c.getColumnIndexOrThrow("_id"));
                    String string = c.getString(c.getColumnIndexOrThrow("display_name"));
                    ChooseContactActivity.this.m_uri = Uri.withAppendedPath(ChooseContactActivity.this.getContactContentUri(), contactId);
                    ChooseContactActivity.this.ChooseRingTone(0, "", true);
                }
            });
        } catch (SecurityException e) {
            Log.e("Ringdroid", e.toString());
        }
    }

    private boolean isEclairOrLater() {
        return Build.VERSION.SDK_INT >= 5;
    }

    /* access modifiers changed from: private */
    public Uri getContactContentUri() {
        if (isEclairOrLater()) {
            return Uri.parse("content://com.android.contacts/contacts");
        }
        return Contacts.People.CONTENT_URI;
    }

    private void assignRingtoneToContact() {
        Cursor c = this.mAdapter.getCursor();
        String contactId = c.getString(c.getColumnIndexOrThrow("_id"));
        String string = c.getString(c.getColumnIndexOrThrow("display_name"));
        Uri withAppendedPath = Uri.withAppendedPath(getContactContentUri(), contactId);
        Toast.makeText(this, "NULL RingTone", 0).show();
    }

    private Cursor myCursor() {
        return managedQuery(Contacts.People.CONTENT_URI, new String[]{"_id", "custom_ringtone", "display_name", "last_time_contacted", "starred", "times_contacted", "number"}, null, null, "name ASC");
    }

    private Cursor createCursor(String filter) {
        String selection;
        if (filter == null || filter.length() <= 0) {
            selection = null;
        } else {
            selection = "(DISPLAY_NAME LIKE \"%" + filter + "%\")";
        }
        Cursor cursor = managedQuery(getContactContentUri(), new String[]{"_id", "custom_ringtone", "display_name", "last_time_contacted", "starred", "times_contacted"}, selection, null, "STARRED DESC, TIMES_CONTACTED DESC, LAST_TIME_CONTACTED DESC, DISPLAY_NAME ASC");
        while (cursor.moveToNext()) {
            String string = cursor.getString(cursor.getColumnIndexOrThrow("_id"));
            cursor.getString(cursor.getColumnIndexOrThrow("display_name"));
        }
        return cursor;
    }

    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    public void afterTextChanged(Editable s) {
        this.mAdapter.changeCursor(createCursor(this.mFilter.getText().toString()));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: private */
    public void ChooseRingTone(int nRingType, String sTitle, boolean bFile) {
        if (bFile) {
            Intent intent = new Intent("android.intent.action.GET_CONTENT");
            intent.setType("audio/*");
            startActivityForResult(Intent.createChooser(intent, "Select music"), 263);
            return;
        }
        Intent intent2 = new Intent("android.intent.action.RINGTONE_PICKER");
        intent2.putExtra("android.intent.extra.ringtone.TYPE", nRingType);
        intent2.putExtra("android.intent.extra.ringtone.TITLE", sTitle);
        Uri uri1 = RingtoneManager.getActualDefaultRingtoneUri(this, nRingType);
        if (uri1 != null) {
            intent2.putExtra("android.intent.extra.ringtone.EXISTING_URI", uri1);
        } else {
            intent2.putExtra("android.intent.extra.ringtone.EXISTING_URI", (Parcelable) null);
        }
        intent2.putExtra("android.intent.extra.ringtone.SHOW_DEFAULT", false);
        intent2.putExtra("android.intent.extra.ringtone.SHOW_SILENT", false);
        startActivityForResult(intent2, 264);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == -1) {
            switch (requestCode) {
                case 263:
                    this.mRingtoneUri = data.getData();
                    if (this.mRingtoneUri != null) {
                        ContentValues values = new ContentValues();
                        values.put("custom_ringtone", this.mRingtoneUri.toString());
                        getContentResolver().update(this.m_uri, values, null, null);
                        break;
                    }
                    break;
                case 264:
                    try {
                        this.mRingtoneUri = (Uri) data.getParcelableExtra("android.intent.extra.ringtone.PICKED_URI");
                        if (this.mRingtoneUri != null) {
                            assignRingtoneToContact();
                            break;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        break;
                    }
                    break;
            }
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    /* access modifiers changed from: package-private */
    public String GetRingtoneName(Uri uri) {
        String szName = getString(R.string.Unknown);
        if (uri == null) {
            return szName;
        }
        try {
            Cursor cursor = managedQuery(uri, new String[]{"title"}, null, null, null);
            if (cursor == null || cursor.getCount() != 1) {
                return szName;
            }
            cursor.moveToFirst();
            return cursor.getString(0);
        } catch (Exception e) {
            return szName;
        }
    }
}
