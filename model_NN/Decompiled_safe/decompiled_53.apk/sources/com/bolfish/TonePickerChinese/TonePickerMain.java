package com.bolfish.TonePickerChinese;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.Settings;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SlidingDrawer;
import android.widget.TextView;
import com.adwhirl.AdWhirlLayout;
import com.bolfish.TonePickerChinese.RingtoneBookmarks;
import com.vpon.adon.android.AdOnPlatform;
import com.vpon.adon.android.AdView;
import java.util.ArrayList;

public class TonePickerMain extends Activity implements View.OnClickListener, SlidingDrawer.OnDrawerOpenListener, SlidingDrawer.OnDrawerCloseListener, SeekBar.OnSeekBarChangeListener, AdWhirlLayout.AdWhirlInterface {
    final String MySettingFile = "Bolfish_RingTone";
    private Ringtone RingtoneNoti;
    private Ringtone RingtoneRing;
    AdWhirlLayout adWhirlLayout;
    private AudioManager audMgr;
    private RingtoneBookmarks bookmarksNotification;
    private RingtoneBookmarks bookmarksRingtone;
    private Button btn_NotificationNotification;
    private Button btn_NotificationPhone;
    /* access modifiers changed from: private */
    public ArrayList<RingtoneBookmarks.Bookmarks> m_TempBookmark = new ArrayList<>();
    private Tools m_Tools = new Tools(this);
    private boolean m_bInit = true;
    /* access modifiers changed from: private */
    public boolean m_bNotification = false;
    private int m_nToneMode = 0;
    private int nVolumeNotification;
    private int nVolumeRinger;
    private SeekBar seekbar01;
    private SeekBar seekbar02;
    private TextView tv_Notification;
    private TextView tv_Ringtone;

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        StopPlay();
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        ((Button) findViewById(R.id.ButtonRingtonePhone)).setOnClickListener(this);
        ((Button) findViewById(R.id.ButtonRingtoneNotification)).setOnClickListener(this);
        this.btn_NotificationPhone = (Button) findViewById(R.id.ButtonNotificationPhone);
        this.btn_NotificationPhone.setOnClickListener(this);
        this.btn_NotificationNotification = (Button) findViewById(R.id.ButtonNotificationNotification);
        this.btn_NotificationNotification.setOnClickListener(this);
        SlidingDrawer slide = (SlidingDrawer) findViewById(R.id.slide);
        slide.setOnDrawerOpenListener(this);
        slide.setOnDrawerCloseListener(this);
        this.seekbar01 = (SeekBar) findViewById(R.id.SeekBarD01);
        this.seekbar02 = (SeekBar) findViewById(R.id.SeekBarD02);
        int nTemp = this.seekbar01.getThumbOffset() / 2;
        if (nTemp > 5) {
            this.seekbar01.setThumbOffset(nTemp);
            this.seekbar02.setThumbOffset(nTemp);
        }
        this.audMgr = (AudioManager) getSystemService("audio");
        this.nVolumeRinger = this.audMgr.getStreamVolume(2);
        this.nVolumeNotification = this.audMgr.getStreamVolume(5);
        int nVolumeMaxRinger = this.audMgr.getStreamMaxVolume(2);
        int nVolumeMaxNotification = this.audMgr.getStreamMaxVolume(5);
        this.seekbar01.setMax(nVolumeMaxRinger);
        this.seekbar02.setMax(nVolumeMaxNotification);
        this.seekbar01.setProgress(this.nVolumeRinger);
        this.seekbar02.setProgress(this.nVolumeNotification);
        this.seekbar01.setOnSeekBarChangeListener(this);
        this.seekbar02.setOnSeekBarChangeListener(this);
        this.m_bInit = false;
        try {
            this.RingtoneRing = RingtoneManager.getRingtone(this, RingtoneManager.getDefaultUri(1));
            this.RingtoneRing.setStreamType(2);
            this.RingtoneNoti = RingtoneManager.getRingtone(this, RingtoneManager.getDefaultUri(2));
            this.RingtoneNoti.setStreamType(5);
        } catch (Exception e) {
        }
        ((ImageView) findViewById(R.id.ImageViewRingToneBookmark)).setOnClickListener(this);
        ((ImageView) findViewById(R.id.ImageViewRingToneAddBookmark)).setOnClickListener(this);
        ((ImageView) findViewById(R.id.ImageViewNotificationAddBookmark)).setOnClickListener(this);
        ((ImageView) findViewById(R.id.ImageViewNotificationBookmark)).setOnClickListener(this);
        this.bookmarksRingtone = new RingtoneBookmarks(this, "Bolfish_Ringtone_Bookmarks");
        this.bookmarksNotification = new RingtoneBookmarks(this, "Bolfish_Notification_Bookmarks");
        this.tv_Ringtone = (TextView) findViewById(R.id.TextViewRingTone);
        this.tv_Notification = (TextView) findViewById(R.id.TextViewNotification);
        this.tv_Ringtone.setText(GetRingtoneName(false));
        this.tv_Notification.setText(GetRingtoneName(true));
        this.tv_Ringtone.setOnClickListener(this);
        this.tv_Notification.setOnClickListener(this);
        LinearLayout layout = (LinearLayout) findViewById(R.id.ad);
        this.adWhirlLayout = new AdWhirlLayout(this, getString(R.string.Adwhirl_ID));
        this.adWhirlLayout.setAdWhirlInterface(this);
        layout.addView(this.adWhirlLayout, new RelativeLayout.LayoutParams(-1, -2));
        layout.invalidate();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ImageViewRingToneAddBookmark:
                AddBookmark(false);
                return;
            case R.id.TextViewRingtoneTitle:
            case R.id.TextViewRingTone:
                try {
                    if (this.RingtoneRing.isPlaying()) {
                        StopPlay();
                        return;
                    }
                    StopPlay();
                    this.RingtoneRing.play();
                    return;
                } catch (Exception e) {
                    return;
                }
            case R.id.ImageViewRingToneBookmark:
                this.m_bNotification = false;
                ChooseBookmark(false);
                return;
            case R.id.ButtonRingtonePhone:
                this.m_nToneMode = 1;
                ChooseRingTone(1, "Ringtone", false);
                StopPlay();
                return;
            case R.id.ButtonRingtoneNotification:
                this.m_nToneMode = 1;
                ChooseRingTone(1, "Ringtone", true);
                StopPlay();
                return;
            case R.id.Blank1:
            case R.id.LinearLayout02:
            default:
                return;
            case R.id.ImageViewNotificationAddBookmark:
                AddBookmark(true);
                return;
            case R.id.TextViewNotificationTitle:
            case R.id.TextViewNotification:
                try {
                    if (this.RingtoneNoti.isPlaying()) {
                        StopPlay();
                        return;
                    }
                    StopPlay();
                    this.RingtoneNoti.play();
                    return;
                } catch (Exception e2) {
                    return;
                }
            case R.id.ImageViewNotificationBookmark:
                this.m_bNotification = true;
                ChooseBookmark(true);
                return;
            case R.id.ButtonNotificationPhone:
                this.m_nToneMode = 2;
                ChooseRingTone(2, "Ringtone", false);
                StopPlay();
                return;
            case R.id.ButtonNotificationNotification:
                this.m_nToneMode = 2;
                ChooseRingTone(2, "Ringtone", true);
                StopPlay();
                return;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    private void ChooseRingTone(int nRingType, String sTitle, boolean bFile) {
        if (bFile) {
            Intent intent = new Intent("android.intent.action.GET_CONTENT");
            intent.setType("audio/*");
            startActivityForResult(Intent.createChooser(intent, "Select music"), 263);
            return;
        }
        Intent intent2 = new Intent("android.intent.action.RINGTONE_PICKER");
        intent2.putExtra("android.intent.extra.ringtone.TYPE", nRingType);
        intent2.putExtra("android.intent.extra.ringtone.TITLE", sTitle);
        Uri uri1 = RingtoneManager.getActualDefaultRingtoneUri(this, nRingType);
        if (uri1 != null) {
            intent2.putExtra("android.intent.extra.ringtone.EXISTING_URI", uri1);
        } else {
            intent2.putExtra("android.intent.extra.ringtone.EXISTING_URI", (Parcelable) null);
        }
        intent2.putExtra("android.intent.extra.ringtone.SHOW_DEFAULT", false);
        intent2.putExtra("android.intent.extra.ringtone.SHOW_SILENT", false);
        startActivityForResult(intent2, 264);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == -1) {
            switch (requestCode) {
                case 263:
                    int nCount = this.m_Tools.QuickLoadInt("Bolfish_RingTone", "COUNT", 7);
                    if (nCount >= 1) {
                        Uri uri = data.getData();
                        if (uri != null) {
                            try {
                                if (this.m_nToneMode == 1) {
                                    Settings.System.putString(getContentResolver(), "ringtone", uri.toString());
                                    this.tv_Ringtone.setText(GetRingtoneName(false));
                                } else {
                                    Settings.System.putString(getContentResolver(), "notification_sound", uri.toString());
                                    this.tv_Notification.setText(GetRingtoneName(true));
                                }
                                int nCount2 = nCount - 1;
                                this.m_Tools.ShowHelpDialog(String.valueOf(getString(R.string.change_count)) + nCount2);
                                this.m_Tools.QuickSave("Bolfish_RingTone", "COUNT", nCount2);
                                break;
                            } catch (Exception e) {
                                Tools.Toast(this, getString(R.string.ErrorSetting));
                                break;
                            }
                        }
                    } else {
                        this.m_Tools.ShowHelpDialog((int) R.string.change_count_zero);
                        return;
                    }
                    break;
                case 264:
                    try {
                        Uri pickedUri = (Uri) data.getParcelableExtra("android.intent.extra.ringtone.PICKED_URI");
                        if (pickedUri != null) {
                            try {
                                RingtoneManager.setActualDefaultRingtoneUri(this, this.m_nToneMode, pickedUri);
                                if (this.m_nToneMode != 1) {
                                    this.tv_Notification.setText(GetRingtoneName(true));
                                    break;
                                } else {
                                    this.tv_Ringtone.setText(GetRingtoneName(false));
                                    break;
                                }
                            } catch (Exception e2) {
                                Tools.Toast(this, getString(R.string.ErrorSetting));
                                break;
                            }
                        }
                    } catch (Exception e3) {
                        e3.printStackTrace();
                        break;
                    }
                    break;
            }
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void onDrawerClosed() {
        this.btn_NotificationPhone.setEnabled(true);
        this.btn_NotificationNotification.setEnabled(true);
    }

    public void onDrawerOpened() {
        this.btn_NotificationPhone.setEnabled(false);
        this.btn_NotificationNotification.setEnabled(false);
        StopPlay();
    }

    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (!this.m_bInit) {
            switch (seekBar.getId()) {
                case R.id.SeekBarD01:
                    this.audMgr.setStreamVolume(2, progress, 5);
                    return;
                case R.id.TextNotification:
                default:
                    return;
                case R.id.SeekBarD02:
                    this.audMgr.setStreamVolume(5, progress, 5);
                    return;
            }
        }
    }

    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    public void onStopTrackingTouch(SeekBar seekBar) {
    }

    /* access modifiers changed from: package-private */
    public void StopPlay() {
        try {
            this.RingtoneRing.stop();
            this.RingtoneNoti.stop();
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.bookmarksNotification.ReadAll();
        this.bookmarksRingtone.ReadAll();
    }

    /* access modifiers changed from: package-private */
    public void SetRingTone(String szUri, boolean bNotification) {
        if (bNotification) {
            try {
                Settings.System.putString(getContentResolver(), "notification_sound", szUri);
                this.tv_Notification.setText(GetRingtoneName(true));
            } catch (Exception e) {
            }
        } else {
            Settings.System.putString(getContentResolver(), "ringtone", szUri);
            this.tv_Ringtone.setText(GetRingtoneName(false));
        }
    }

    /* access modifiers changed from: package-private */
    public String GetRingtoneName(boolean bNotification) {
        Uri uri;
        String szName = getString(R.string.Unknown);
        if (bNotification) {
            uri = RingtoneManager.getActualDefaultRingtoneUri(this, 2);
        } else {
            uri = RingtoneManager.getActualDefaultRingtoneUri(this, 1);
        }
        if (uri == null) {
            return szName;
        }
        try {
            Cursor cursor = managedQuery(uri, new String[]{"title"}, null, null, null);
            if (cursor == null || cursor.getCount() != 1) {
                return szName;
            }
            cursor.moveToFirst();
            return cursor.getString(0);
        } catch (Exception e) {
            return szName;
        }
    }

    /* access modifiers changed from: package-private */
    public String GetRingtoneUri(boolean bNotification) {
        Uri uri;
        if (bNotification) {
            uri = RingtoneManager.getActualDefaultRingtoneUri(this, 2);
        } else {
            uri = RingtoneManager.getActualDefaultRingtoneUri(this, 1);
        }
        if (uri != null) {
            return uri.toString();
        }
        return "";
    }

    /* access modifiers changed from: package-private */
    public void ChooseBookmark(boolean bNotification) {
        RingtoneBookmarks bookmarks;
        StopPlay();
        String szTitle = getString(R.string.Bookmark);
        String szOK = getString(R.string.BookmarkManager);
        String szCancel = getString(R.string.Cancel);
        if (bNotification) {
            bookmarks = this.bookmarksNotification;
        } else {
            bookmarks = this.bookmarksRingtone;
        }
        this.m_TempBookmark.clear();
        int i = 0;
        while (true) {
            bookmarks.getClass();
            if (i >= 10) {
                break;
            }
            if (bookmarks.bookmarks[i].bSaved) {
                this.m_TempBookmark.add(bookmarks.bookmarks[i]);
            }
            i++;
        }
        int nTotal = this.m_TempBookmark.size();
        String[] days = new String[nTotal];
        for (int i2 = 0; i2 < nTotal; i2++) {
            days[i2] = new String(this.m_TempBookmark.get(i2).Name);
        }
        new AlertDialog.Builder(this).setTitle(szTitle).setSingleChoiceItems(days, -1, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                TonePickerMain.this.SetRingTone(((RingtoneBookmarks.Bookmarks) TonePickerMain.this.m_TempBookmark.get(whichButton)).UriAddress, TonePickerMain.this.m_bNotification);
                dialog.dismiss();
            }
        }).setPositiveButton(szOK, new DialogInterface.OnClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
             arg types: [java.lang.String, int]
             candidates:
              ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
            public void onClick(DialogInterface dialog, int id) {
                Intent intent = new Intent();
                intent.setClass(TonePickerMain.this, BookmarkManager.class);
                if (TonePickerMain.this.m_bNotification) {
                    intent.putExtra("Notification", true);
                } else {
                    intent.putExtra("Notification", false);
                }
                TonePickerMain.this.startActivity(intent);
            }
        }).setNegativeButton(szCancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        }).setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
            }
        }).create().show();
    }

    /* access modifiers changed from: package-private */
    public void AddBookmark(boolean bNotification) {
        int nIndex;
        if (bNotification) {
            nIndex = this.bookmarksNotification.Insert(this.tv_Notification.getText().toString(), this.m_Tools.GetRingtoneUri(true));
        } else {
            nIndex = this.bookmarksRingtone.Insert(this.tv_Ringtone.getText().toString(), this.m_Tools.GetRingtoneUri(false));
        }
        switch (nIndex) {
            case -2:
                Tools.Toast(this, getString(R.string.Duplicate));
                return;
            case -1:
                Tools.Toast(this, getString(R.string.Full));
                return;
            default:
                Tools.Toast(this, getString(R.string.SaveSuccess));
                return;
        }
    }

    public void adWhirlGeneric() {
    }

    public void AdOn() {
        AdView adView = new AdView(this);
        this.adWhirlLayout.addView(adView, new ViewGroup.LayoutParams(320, -2));
        adView.setLicenseKey(getString(R.string.AdOn_ID), AdOnPlatform.TW, true);
    }
}
