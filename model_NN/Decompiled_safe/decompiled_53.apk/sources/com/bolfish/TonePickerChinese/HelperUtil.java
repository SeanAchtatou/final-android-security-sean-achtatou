package com.bolfish.TonePickerChinese;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;
import java.io.File;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.UUID;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.ByteArrayBuffer;
import org.apache.http.util.EntityUtils;

public class HelperUtil {
    public static boolean HasNetConnection(Context ctx) {
        NetworkInfo ni = ((ConnectivityManager) ctx.getSystemService("connectivity")).getActiveNetworkInfo();
        if (ni != null) {
            return ni.isConnectedOrConnecting();
        }
        return false;
    }

    public static String generateGUID() {
        return UUID.randomUUID().toString();
    }

    public static String htmlEscape(String html) {
        return html.replace("#", "%23").replace("%", "%25").replace("\\", "%27").replace("?", "%3f");
    }

    public static String htmlBrackets(String html) {
        return html.replace("&lt;", "<").replace("&gt;", ">");
    }

    public static String htmlTransfer(String html) {
        return html.replace("&#38;", "&").replace("&amp;", "&").replace("&#32;", " ").replace("&#33;", "!").replace("&#34;", "\"").replace("&#35;", "#").replace("&#36;", "$").replace("&#37;", "%").replace("&#39;", "'").replace("&#40;", "(").replace("&#41;", ")").replace("&#42;", "*").replace("&#43;", "+").replace("&#44;", ",").replace("&#45;", "-").replace("&#46;", ".").replace("&#47;", "/").replace("&#58;", ":").replace("&#59;", ";").replace("&#60;", "<").replace("&#61;", "=").replace("&#62;", ">").replace("&#63;", "?").replace("&#64;", "@").replace("&#91;", "[").replace("&#92;", "\\").replace("&#93;", "]").replace("&#94;", "^").replace("&#95;", "_").replace("&#96;", "`").replace("&#123;", "{").replace("&#124;", "|").replace("&#125;", "}").replace("&#126;", "~").replace("&nbsp;", " ").replace("&lt;", "<").replace("&gt;", ">").replace("&laquo;", "<<").replace("&raquo;", ">>").replace("&copy;", "(C)").replace("&reg;", "(R)").replace("&apos;", "'").replace("&quot;", "\"").replace("&#163;", "Euro");
    }

    public static void Toast(Context ctx, String szMessage) {
        Toast.makeText(ctx, szMessage, 0).show();
    }

    public static void Toast(Context ctx, int nRID) {
        Toast.makeText(ctx, ctx.getString(nRID), 0).show();
    }

    public static void ShowErrorDialog(Context ctx, String szError) {
        try {
            new AlertDialog.Builder(ctx).setIcon((int) R.drawable.error).setTitle("Error").setMessage(szError).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                }
            }).create().show();
        } catch (Exception e) {
        }
    }

    /* JADX INFO: Multiple debug info for r0v7 java.lang.String: [D('m_httpGet' org.apache.http.client.methods.HttpGet), D('href' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r6v12 org.apache.http.HttpEntity: [D('httpResponse' org.apache.http.HttpResponse), D('entity' org.apache.http.HttpEntity)] */
    /* JADX INFO: Multiple debug info for r2v2 java.io.InputStream: [D('m_httpGet' org.apache.http.client.methods.HttpGet), D('is' java.io.InputStream)] */
    /* JADX INFO: Multiple debug info for r0v9 byte[]: [D('data' byte[]), D('httpClient' org.apache.http.client.HttpClient)] */
    /* JADX INFO: Multiple debug info for r0v10 int: [D('data' byte[]), D('index' int)] */
    /* JADX INFO: Multiple debug info for r6v27 java.lang.String: [D('end' int), D('href' java.lang.String)] */
    public static String TryToGetBBCImageURL(String szURL) {
        String href;
        HttpGet m_httpGet;
        String response;
        HttpGet m_httpGet2;
        int end;
        HttpClient httpClient = new DefaultHttpClient();
        HttpGet m_httpGet3 = new HttpGet();
        try {
            m_httpGet3.setURI(new URL(szURL.replace("/hi/", "/mobile/")).toURI());
            try {
                try {
                    InputStream is = httpClient.execute(m_httpGet3).getEntity().getContent();
                    byte[] data = new byte[1024];
                    ByteArrayBuffer buf = new ByteArrayBuffer(1024);
                    while (true) {
                        int n = is.read(data);
                        if (n == -1) {
                            break;
                        }
                        buf.append(data, 0, n);
                    }
                    String response2 = new String(buf.toByteArray());
                    int index = response2.indexOf("http://news.bbcimg.co.uk/media/images/");
                    if (index == -1 || (end = response2.indexOf("\"", index)) == -1) {
                        href = null;
                    } else {
                        href = response2.substring(index, end);
                    }
                    try {
                        byte[] bArr = null;
                        response = href;
                        m_httpGet2 = null;
                    } catch (Exception e) {
                        m_httpGet = null;
                        Log.d("News", "Error");
                        response = href;
                        m_httpGet2 = m_httpGet;
                        return response;
                    }
                } catch (Exception e2) {
                    href = null;
                    m_httpGet = null;
                }
            } catch (Exception e3) {
                href = null;
                m_httpGet = m_httpGet3;
                Log.d("News", "Error");
                response = href;
                m_httpGet2 = m_httpGet;
                return response;
            }
            return response;
        } catch (URISyntaxException e4) {
            e4.printStackTrace();
            return null;
        } catch (Exception e5) {
            return null;
        }
    }

    public static Bitmap LoadWebImage(String szURL) {
        try {
            return BitmapFactory.decodeStream(new FilterInputStream(new URL(szURL).openStream()) {
                public long skip(long n) throws IOException {
                    long totalBytesSkipped = 0;
                    while (totalBytesSkipped < n) {
                        long bytesSkipped = this.in.skip(n - totalBytesSkipped);
                        if (bytesSkipped == 0) {
                            if (read() < 0) {
                                break;
                            }
                            bytesSkipped = 1;
                        }
                        totalBytesSkipped += bytesSkipped;
                    }
                    return totalBytesSkipped;
                }
            });
        } catch (Exception e) {
            return null;
        }
    }

    public static void ClearCache(Context context) {
        File dir = context.getCacheDir();
        if (dir != null && dir.isDirectory()) {
            try {
                File[] children = dir.listFiles();
                if (children.length > 0) {
                    for (File listFiles : children) {
                        File[] temp = listFiles.listFiles();
                        for (File delete : temp) {
                            delete.delete();
                        }
                    }
                }
            } catch (Exception e) {
            }
        }
    }

    public static String GetHtml(String szURL, String szDecode) {
        if (szURL == null) {
            return null;
        }
        HttpParams httpParameters = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParameters, 10000);
        HttpConnectionParams.setSoTimeout(httpParameters, 10000);
        HttpClient httpClient = new DefaultHttpClient(httpParameters);
        HttpGet m_httpGet = new HttpGet();
        try {
            m_httpGet.setURI(new URL(szURL).toURI());
            String response = null;
            try {
                HttpResponse httpResponse = httpClient.execute(m_httpGet);
                if (httpResponse != null) {
                    response = szDecode == null ? EntityUtils.toString(httpResponse.getEntity()) : EntityUtils.toString(httpResponse.getEntity(), szDecode);
                }
            } catch (Exception e) {
            }
            return response;
        } catch (Exception e2) {
            Exception exc = e2;
            return null;
        }
    }

    public static String RemoveString(String source, String start, String end) {
        if (source == null) {
            return null;
        }
        int nStart = source.indexOf(start);
        int nEnd = source.indexOf(end);
        return (nStart == -1 || nEnd == -1) ? source : String.valueOf(source.substring(0, nStart)) + source.substring(end.length() + nEnd);
    }
}
