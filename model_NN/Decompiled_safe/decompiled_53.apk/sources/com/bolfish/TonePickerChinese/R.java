package com.bolfish.TonePickerChinese;

public final class R {

    public static final class attr {
        public static final int backgroundColor = 2130771968;
        public static final int keywords = 2130771971;
        public static final int primaryTextColor = 2130771969;
        public static final int refreshInterval = 2130771972;
        public static final int secondaryTextColor = 2130771970;
    }

    public static final class color {
        public static final int color_black = 2130968581;
        public static final int color_blue = 2130968582;
        public static final int color_orange = 2130968585;
        public static final int color_pulse_background = 2130968586;
        public static final int color_red = 2130968584;
        public static final int color_white = 2130968580;
        public static final int color_yellow = 2130968583;
        public static final int list_divider = 2130968579;
        public static final int main_color = 2130968576;
        public static final int second_color = 2130968577;
        public static final int tab_background = 2130968578;
    }

    public static final class drawable {
        public static final int add_bookmark = 2130837504;
        public static final int alert_dialog_icon = 2130837505;
        public static final int app = 2130837506;
        public static final int black = 2130837535;
        public static final int blue = 2130837536;
        public static final int blue_dark = 2130837533;
        public static final int blue_light = 2130837543;
        public static final int bookmarks = 2130837507;
        public static final int browser_background = 2130837508;
        public static final int contact = 2130837509;
        public static final int contact_32 = 2130837510;
        public static final int contact_all = 2130837511;
        public static final int dark_yellow = 2130837549;
        public static final int error = 2130837512;
        public static final int gray = 2130837557;
        public static final int green = 2130837538;
        public static final int green_light = 2130837541;
        public static final int grid_line = 2130837564;
        public static final int half_black = 2130837544;
        public static final int help = 2130837513;
        public static final int innk_ground = 2130837558;
        public static final int music = 2130837514;
        public static final int music_mp3 = 2130837515;
        public static final int orange = 2130837540;
        public static final int phone = 2130837516;
        public static final int playback_indicator = 2130837563;
        public static final int proapp = 2130837517;
        public static final int read_background = 2130837548;
        public static final int red = 2130837534;
        public static final int retangle = 2130837518;
        public static final int retangle1 = 2130837519;
        public static final int ringtone = 2130837520;
        public static final int search = 2130837521;
        public static final int searching = 2130837522;
        public static final int selection_border = 2130837562;
        public static final int slide_draw = 2130837523;
        public static final int star = 2130837524;
        public static final int tab_gradient = 2130837525;
        public static final int tabwidget_background = 2130837526;
        public static final int timecode = 2130837565;
        public static final int timecode_shadow = 2130837566;
        public static final int transparent_all = 2130837550;
        public static final int transparent_all1 = 2130837551;
        public static final int transparent_black = 2130837554;
        public static final int transparent_blue = 2130837546;
        public static final int transparent_green = 2130837545;
        public static final int transparent_grey_light = 2130837547;
        public static final int transparent_newsbar_black = 2130837553;
        public static final int transparent_newsbar_dark = 2130837556;
        public static final int transparent_newsbar_white = 2130837552;
        public static final int transparent_red = 2130837555;
        public static final int transparent_white = 2130837537;
        public static final int type_alarm = 2130837527;
        public static final int type_bkgnd_alarm = 2130837567;
        public static final int type_bkgnd_music = 2130837570;
        public static final int type_bkgnd_notification = 2130837569;
        public static final int type_bkgnd_ringtone = 2130837568;
        public static final int type_bkgnd_unsupported = 2130837571;
        public static final int type_music = 2130837528;
        public static final int type_notification = 2130837529;
        public static final int type_ringtone = 2130837530;
        public static final int waveform_selected = 2130837559;
        public static final int waveform_unselected = 2130837560;
        public static final int waveform_unselected_bkgnd_overlay = 2130837561;
        public static final int white = 2130837531;
        public static final int white_dark1 = 2130837542;
        public static final int yellow = 2130837539;
        public static final int yellow_dark = 2130837532;
    }

    public static final class id {
        public static final int Blank1 = 2131230735;
        public static final int Blank2 = 2131230743;
        public static final int Button01 = 2131230751;
        public static final int ButtonNotificationNotification = 2131230742;
        public static final int ButtonNotificationPhone = 2131230741;
        public static final int ButtonRingtoneNotification = 2131230734;
        public static final int ButtonRingtonePhone = 2131230733;
        public static final int Button_ScrollView_Exit = 2131230757;
        public static final int ET_addbookmark_title = 2131230758;
        public static final int ImageViewNotificationAddBookmark = 2131230737;
        public static final int ImageViewNotificationBookmark = 2131230739;
        public static final int ImageViewRingToneAddBookmark = 2131230729;
        public static final int ImageViewRingToneBookmark = 2131230731;
        public static final int LL_ScrollView = 2131230754;
        public static final int LinearLayout01 = 2131230728;
        public static final int LinearLayout02 = 2131230736;
        public static final int ListView01 = 2131230723;
        public static final int ProgressBar01 = 2131230722;
        public static final int RelativeLayout01 = 2131230727;
        public static final int RelativeLayoutBM01 = 2131230720;
        public static final int ScrollView01 = 2131230752;
        public static final int ScrollView_List = 2131230756;
        public static final int SeekBarD01 = 2131230748;
        public static final int SeekBarD02 = 2131230750;
        public static final int TV_ScrollView = 2131230755;
        public static final int TextNotification = 2131230749;
        public static final int TextRing = 2131230747;
        public static final int TextViewNotification = 2131230740;
        public static final int TextViewNotificationTitle = 2131230738;
        public static final int TextViewRingTone = 2131230732;
        public static final int TextViewRingtoneTitle = 2131230730;
        public static final int ad = 2131230721;
        public static final int content = 2131230745;
        public static final int favoritelist = 2131230753;
        public static final int handle = 2131230744;
        public static final int menu_all = 2131230760;
        public static final int menu_getpro = 2131230761;
        public static final int menu_search = 2131230759;
        public static final int row_display_name = 2131230725;
        public static final int row_ringtone = 2131230726;
        public static final int row_starred = 2131230724;
        public static final int slide = 2131230746;
    }

    public static final class layout {
        public static final int bookmark_manager = 2130903040;
        public static final int choose_contact = 2130903041;
        public static final int contact_row = 2130903042;
        public static final int main = 2130903043;
        public static final int news_main = 2130903044;
        public static final int scrollview_list = 2130903045;
        public static final int scrollview_list_song = 2130903046;
        public static final int youtube_keyword_input = 2130903047;
    }

    public static final class menu {
        public static final int contact = 2131165184;
    }

    public static final class string {
        public static final int AdOn_ID = 2131034113;
        public static final int Adwhirl_ID = 2131034112;
        public static final int Bookmark = 2131034123;
        public static final int BookmarkManager = 2131034124;
        public static final int Cancel = 2131034125;
        public static final int DeleteBookmark = 2131034129;
        public static final int Duplicate = 2131034126;
        public static final int ErrorSetting = 2131034121;
        public static final int Full = 2131034128;
        public static final int HowToPlay = 2131034120;
        public static final int MP3_main = 2131034151;
        public static final int No = 2131034131;
        public static final int Notification = 2131034119;
        public static final int Progress_description = 2131034153;
        public static final int Progress_title = 2131034152;
        public static final int SaveSuccess = 2131034127;
        public static final int TestMode = 2131034114;
        public static final int Unknown = 2131034122;
        public static final int Yes = 2131034130;
        public static final int about_license = 2131034162;
        public static final int app_name = 2131034115;
        public static final int change_count = 2131034145;
        public static final int change_count_zero = 2131034146;
        public static final int choose_contact_title = 2131034132;
        public static final int choose_save_help = 2131034163;
        public static final int contact_help = 2131034135;
        public static final int contact_main = 2131034137;
        public static final int contact_ringtone_change = 2131034141;
        public static final int contact_ringtone_change_mp3 = 2131034155;
        public static final int contact_ringtone_change_phone = 2131034156;
        public static final int contact_ringtone_delete = 2131034139;
        public static final int contact_ringtone_dialogTitle = 2131034154;
        public static final int contact_ringtone_errorplay = 2131034142;
        public static final int contact_ringtone_errorplay_unknow = 2131034165;
        public static final int contact_ringtone_name = 2131034138;
        public static final int contact_ringtone_none = 2131034143;
        public static final int contact_ringtone_play = 2131034140;
        public static final int default_rinetone_manager = 2131034116;
        public static final int delete_failed = 2131034166;
        public static final int dialog_exit_after_save = 2131034164;
        public static final int dialog_getpro_check_no = 2131034149;
        public static final int dialog_getpro_check_yes = 2131034150;
        public static final int dialog_getpro_description = 2131034148;
        public static final int dialog_getpro_title = 2131034147;
        public static final int error_playnone = 2131034157;
        public static final int free_no_support = 2131034144;
        public static final int music_mp3 = 2131034117;
        public static final int phone_main = 2131034136;
        public static final int ringtone = 2131034118;
        public static final int song_edit_delete = 2131034160;
        public static final int song_edit_delete_alert = 2131034161;
        public static final int song_edit_edit = 2131034158;
        public static final int song_edit_play = 2131034159;
        public static final int success_contact_ringtone = 2131034133;
        public static final int upgrade_newsmessage = 2131034134;
    }

    public static final class style {
        public static final int AudioFileInfoOverlayText = 2131099648;
        public static final int ChooseContactBackground = 2131099650;
        public static final int HorizontalDividerBottom = 2131099655;
        public static final int HorizontalDividerTop = 2131099654;
        public static final int ToolbarBackground = 2131099649;
        public static final int VerticalDividerForList = 2131099651;
        public static final int VerticalDividerLeft = 2131099652;
        public static final int VerticalDividerRight = 2131099653;
    }

    public static final class styleable {
        public static final int[] com_admob_android_ads_AdView = {R.attr.backgroundColor, R.attr.primaryTextColor, R.attr.secondaryTextColor, R.attr.keywords, R.attr.refreshInterval};
        public static final int com_admob_android_ads_AdView_backgroundColor = 0;
        public static final int com_admob_android_ads_AdView_keywords = 3;
        public static final int com_admob_android_ads_AdView_primaryTextColor = 1;
        public static final int com_admob_android_ads_AdView_refreshInterval = 4;
        public static final int com_admob_android_ads_AdView_secondaryTextColor = 2;
    }
}
