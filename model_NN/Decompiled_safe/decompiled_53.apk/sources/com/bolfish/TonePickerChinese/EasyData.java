package com.bolfish.TonePickerChinese;

import android.content.Context;
import android.content.SharedPreferences;

public class EasyData {
    private final String Item1 = "Ringtone_Item1_";
    private final String Item2 = "Ringtone_Item2_";
    private final String ItemName = "Ringtone_Enable_";
    public final int MaxStore = 10;
    private final String MySettingFile;
    private SharedPreferences ReadDatabase = null;
    private SharedPreferences.Editor WriteDatabase = null;
    Context m_context;

    EasyData(Context context, String Filename) {
        this.m_context = context;
        this.MySettingFile = Filename;
    }

    /* access modifiers changed from: package-private */
    public void Init() {
        this.ReadDatabase = this.m_context.getSharedPreferences(this.MySettingFile, 0);
        this.WriteDatabase = this.ReadDatabase.edit();
    }

    /* access modifiers changed from: package-private */
    public boolean GetSaved(int nIndex) {
        return this.ReadDatabase.getBoolean("Ringtone_Enable_" + nIndex, false);
    }

    /* access modifiers changed from: package-private */
    public String GetSavedItem1(int nIndex) {
        return this.ReadDatabase.getString("Ringtone_Item1_" + nIndex, "");
    }

    /* access modifiers changed from: package-private */
    public String GetSavedItem2(int nIndex) {
        return this.ReadDatabase.getString("Ringtone_Item2_" + nIndex, "");
    }

    /* access modifiers changed from: package-private */
    public void Delete(int nIndex) {
        this.WriteDatabase.putBoolean("Ringtone_Enable_" + nIndex, false);
        this.WriteDatabase.commit();
    }

    /* access modifiers changed from: package-private */
    public void Saved(int nIndex, String s1, String s2) {
        this.WriteDatabase.putBoolean("Ringtone_Enable_" + nIndex, true);
        this.WriteDatabase.putString("Ringtone_Item1_" + nIndex, s1);
        this.WriteDatabase.putString("Ringtone_Item2_" + nIndex, s2);
        this.WriteDatabase.commit();
    }

    /* access modifiers changed from: package-private */
    public int GetNewIndex() {
        for (int i = 0; i < 10; i++) {
            if (!GetSaved(i)) {
                return i;
            }
        }
        return -1;
    }

    /* access modifiers changed from: package-private */
    public int Insert(String name, String data) {
        int nIndex = GetNewIndex();
        if (nIndex <= -1 || nIndex >= 10) {
            return -1;
        }
        Saved(nIndex, name, data);
        return nIndex;
    }

    /* access modifiers changed from: package-private */
    public void Empty() {
        this.WriteDatabase.clear();
        this.WriteDatabase.commit();
    }
}
