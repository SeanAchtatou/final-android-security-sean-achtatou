package com.bolfish.TonePickerChinese;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import com.adwhirl.AdWhirlLayout;
import com.vpon.adon.android.AdOnPlatform;
import com.vpon.adon.android.AdView;
import java.util.ArrayList;

public class BookmarkManager extends Activity implements AdapterView.OnItemClickListener, AdWhirlLayout.AdWhirlInterface {
    AdWhirlLayout adWhirlLayout;
    private ArrayAdapter<String> m_Adapter;
    /* access modifiers changed from: private */
    public int m_CurrentIndex;
    private ArrayList<String> m_Strings = new ArrayList<>();
    private ArrayList<String> m_Uris = new ArrayList<>();
    private boolean m_bNotification;
    private RingtoneBookmarks m_bookmarks;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.bookmark_manager);
        this.m_bNotification = getIntent().getBooleanExtra("Notification", false);
        ListView listview = (ListView) findViewById(R.id.ListView01);
        this.m_Adapter = new ArrayAdapter<>(this, 17367043, this.m_Strings);
        listview.setAdapter((ListAdapter) this.m_Adapter);
        listview.setChoiceMode(1);
        listview.setOnItemClickListener(this);
        if (this.m_bNotification) {
            this.m_bookmarks = new RingtoneBookmarks(this, "Bolfish_Notification_Bookmarks");
        } else {
            this.m_bookmarks = new RingtoneBookmarks(this, "Bolfish_Ringtone_Bookmarks");
        }
        this.m_bookmarks.getClass();
        for (int i = 0; i < 10; i++) {
            if (this.m_bookmarks.bookmarks[i].bSaved) {
                this.m_Uris.add(this.m_bookmarks.bookmarks[i].UriAddress);
                this.m_Adapter.add(this.m_bookmarks.bookmarks[i].Name);
            }
        }
        LinearLayout layout = (LinearLayout) findViewById(R.id.ad);
        this.adWhirlLayout = new AdWhirlLayout(this, getString(R.string.Adwhirl_ID));
        this.adWhirlLayout.setAdWhirlInterface(this);
        layout.addView(this.adWhirlLayout, new RelativeLayout.LayoutParams(-1, -2));
        layout.invalidate();
    }

    public void onItemClick(AdapterView<?> adapterView, View arg1, int position, long id) {
        this.m_CurrentIndex = position;
        String szDeleteMessage = getString(R.string.DeleteBookmark);
        String szYes = getString(R.string.Yes);
        new AlertDialog.Builder(this).setIcon((int) R.drawable.alert_dialog_icon).setTitle(String.valueOf(szDeleteMessage) + "\n" + this.m_Adapter.getItem(position)).setPositiveButton(szYes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                BookmarkManager.this.Delete(BookmarkManager.this.m_CurrentIndex);
            }
        }).setNegativeButton(getString(R.string.No), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        }).create().show();
    }

    /* access modifiers changed from: package-private */
    public void Delete(int nIndex) {
        this.m_Adapter.remove(this.m_Adapter.getItem(nIndex));
        this.m_Uris.remove(nIndex);
        this.m_bookmarks.DeleteByUri(this.m_Uris.get(nIndex));
        this.m_Adapter.notifyDataSetChanged();
    }

    public void adWhirlGeneric() {
    }

    public void AdOn() {
        AdView adView = new AdView(this);
        this.adWhirlLayout.addView(adView, new ViewGroup.LayoutParams(320, -2));
        adView.setLicenseKey(getString(R.string.AdOn_ID), AdOnPlatform.TW, true);
    }
}
