package com.bolfish.TonePickerChinese;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.MergeCursor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.MediaStore;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import com.adwhirl.AdWhirlLayout;
import com.vpon.adon.android.AdOnPlatform;
import com.vpon.adon.android.AdView;
import java.io.File;
import java.util.ArrayList;

public class ScrollView_List_SelectSong extends Activity implements AdWhirlLayout.AdWhirlInterface {
    private static final String[] EXTERNAL_COLUMNS = {"_id", "_data", "title", "artist", "album", "is_ringtone", "is_alarm", "is_notification", "is_music", "\"" + MediaStore.Audio.Media.EXTERNAL_CONTENT_URI + "\""};
    private static final String[] INTERNAL_COLUMNS = {"_id", "_data", "title", "artist", "album", "is_ringtone", "is_alarm", "is_notification", "is_music", "\"" + MediaStore.Audio.Media.INTERNAL_CONTENT_URI + "\""};
    private final int COLOR_BLACK = -16777216;
    private final int COLOR_BLUE = -16741493;
    private final int COLOR_DARKGREEN = -13011031;
    private final int COLOR_GRAY = -8355712;
    private final int COLOR_LIGHTGRAY = -5197648;
    private final int COLOR_ORANGE = -32704;
    private final int COLOR_RED = -65536;
    private final int COLOR_WHITE = -1;
    private final int COLOR_YELLOW = -128;
    private final int EVENT_LoadComplete = 1;
    final String MySettingFile = "Bolfish_RingTone";
    /* access modifiers changed from: private */
    public String Song_Edit_Delete;
    /* access modifiers changed from: private */
    public String Song_Edit_Edit;
    private String Song_Edit_Play;
    AdWhirlLayout adWhirlLayout;
    /* access modifiers changed from: private */
    public boolean mMutex = false;
    /* access modifiers changed from: private */
    public MediaPlayer mPlayer;
    private boolean mShowAll = false;
    /* access modifiers changed from: private */
    public EventHandler m_EventHandler = new EventHandler(Looper.myLooper());
    /* access modifiers changed from: private */
    public ArrayList<LinearLayout> m_List = new ArrayList<>();
    /* access modifiers changed from: private */
    public ArrayList<ImageButton> m_ListPlayBtn = new ArrayList<>();
    /* access modifiers changed from: private */
    public ArrayList<MySong> m_MySong = new ArrayList<>();
    Tools m_Tools = new Tools(this);
    private boolean m_bNeedToRefresh = true;
    LinearLayout m_ll;
    /* access modifiers changed from: private */
    public int m_nCurrent;
    /* access modifiers changed from: private */
    public int m_nCurrentClick;
    ScrollView m_sv;
    String m_szResponse = null;
    public ProgressDialog progressDialog = null;

    /* access modifiers changed from: private */
    public static final boolean CheckDuplicate(MySong song1, MySong song2) {
        if (song1.mFileName.replace("/mnt/sdcard", "/sdcard").equals(song2.mFileName.replace("/mnt/sdcard", "/sdcard"))) {
            return true;
        }
        return false;
    }

    private int GetImageFromCursor(Cursor cursor) {
        if (cursor.getInt(cursor.getColumnIndexOrThrow("is_ringtone")) != 0) {
            return R.drawable.type_ringtone;
        }
        if (cursor.getInt(cursor.getColumnIndexOrThrow("is_alarm")) != 0) {
            return R.drawable.type_alarm;
        }
        if (cursor.getInt(cursor.getColumnIndexOrThrow("is_notification")) != 0) {
            return R.drawable.type_notification;
        }
        if (cursor.getInt(cursor.getColumnIndexOrThrow("is_music")) != 0) {
            return R.drawable.type_music;
        }
        return !CheapSoundFile.isFilenameSupported(cursor.getString(cursor.getColumnIndexOrThrow("_data"))) ? R.drawable.type_bkgnd_unsupported : R.drawable.type_bkgnd_unsupported;
    }

    private class MySong {
        String mAlbum;
        String mArtist;
        String mFileName;
        int mImage;
        String mName;
        Uri mUri;

        private MySong() {
        }

        /* synthetic */ MySong(ScrollView_List_SelectSong scrollView_List_SelectSong, MySong mySong) {
            this();
        }
    }

    /* access modifiers changed from: package-private */
    public void RefreshSong() {
        this.m_List.clear();
        this.m_ListPlayBtn.clear();
        this.m_MySong.clear();
        Cursor cuSounds = createCursor("");
        if (cuSounds.getCount() > 0) {
            while (cuSounds.moveToNext()) {
                MySong song = new MySong(this, null);
                int uriIndex = cuSounds.getColumnIndex("\"" + MediaStore.Audio.Media.INTERNAL_CONTENT_URI + "\"");
                if (uriIndex == -1) {
                    uriIndex = cuSounds.getColumnIndex("\"" + MediaStore.Audio.Media.EXTERNAL_CONTENT_URI + "\"");
                }
                song.mUri = Uri.parse(String.valueOf(cuSounds.getString(uriIndex)) + "/" + cuSounds.getString(cuSounds.getColumnIndexOrThrow("_id")));
                song.mName = cuSounds.getString(cuSounds.getColumnIndex("title"));
                song.mArtist = cuSounds.getString(cuSounds.getColumnIndex("artist"));
                song.mAlbum = cuSounds.getString(cuSounds.getColumnIndex("album"));
                song.mFileName = cuSounds.getString(cuSounds.getColumnIndex("_data"));
                song.mImage = GetImageFromCursor(cuSounds);
                this.m_MySong.add(song);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.scrollview_list_song);
        setVolumeControlStream(3);
        this.m_sv = (ScrollView) findViewById(R.id.ScrollView_List);
        this.m_ll = new LinearLayout(this);
        this.m_ll.setOrientation(1);
        this.m_sv.addView(this.m_ll);
        this.Song_Edit_Edit = getString(R.string.song_edit_edit);
        this.Song_Edit_Play = getString(R.string.song_edit_play);
        this.Song_Edit_Delete = getString(R.string.song_edit_delete);
        LinearLayout layout = (LinearLayout) findViewById(R.id.ad);
        this.adWhirlLayout = new AdWhirlLayout(this, getString(R.string.Adwhirl_ID));
        this.adWhirlLayout.setAdWhirlInterface(this);
        layout.addView(this.adWhirlLayout, new RelativeLayout.LayoutParams(-1, -2));
        layout.invalidate();
    }

    /* access modifiers changed from: package-private */
    public void InsertViewIntoScroll(MySong song) {
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setOrientation(0);
        linearLayout.setBackgroundResource(R.drawable.black);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, 64));
        ImageButton imageButton = new ImageButton(this);
        imageButton.setImageResource(17301540);
        linearLayout.setGravity(16);
        linearLayout.addView(imageButton);
        imageButton.setId(this.m_nCurrent);
        this.m_ListPlayBtn.add(imageButton);
        imageButton.setOnClickListener(new View.OnClickListener() {
            /* Debug info: failed to restart local var, previous not found, register: 10 */
            public void onClick(View v) {
                int nIndex = v.getId();
                int nTotal = ScrollView_List_SelectSong.this.m_List.size();
                for (int i = 0; i < nTotal; i++) {
                    ((LinearLayout) ScrollView_List_SelectSong.this.m_List.get(i)).setEnabled(false);
                    ((ImageButton) ScrollView_List_SelectSong.this.m_ListPlayBtn.get(i)).setEnabled(false);
                }
                ((ImageButton) ScrollView_List_SelectSong.this.m_ListPlayBtn.get(nIndex)).setEnabled(true);
                String sState = (String) v.getTag();
                if (ScrollView_List_SelectSong.this.mPlayer == null) {
                    try {
                        ScrollView_List_SelectSong.this.mPlayer = new MediaPlayer();
                        ScrollView_List_SelectSong.this.mPlayer.setDataSource(((MySong) ScrollView_List_SelectSong.this.m_MySong.get(nIndex)).mFileName);
                        ScrollView_List_SelectSong.this.mPlayer.prepare();
                        ScrollView_List_SelectSong.this.mPlayer.start();
                        ScrollView_List_SelectSong.this.mPlayer.setAudioStreamType(3);
                        ((ImageButton) ScrollView_List_SelectSong.this.m_ListPlayBtn.get(nIndex)).setImageResource(17301539);
                        v.setTag("Playing");
                    } catch (Exception e) {
                        Toast.makeText(ScrollView_List_SelectSong.this, ScrollView_List_SelectSong.this.getString(R.string.contact_ringtone_errorplay_unknow), 1).show();
                        for (int i2 = 0; i2 < nTotal; i2++) {
                            ((LinearLayout) ScrollView_List_SelectSong.this.m_List.get(i2)).setEnabled(true);
                            ((ImageButton) ScrollView_List_SelectSong.this.m_ListPlayBtn.get(i2)).setEnabled(true);
                        }
                        ((ImageButton) ScrollView_List_SelectSong.this.m_ListPlayBtn.get(nIndex)).setImageResource(17301540);
                        v.setTag("Stop");
                    }
                } else if (sState == null || !sState.equalsIgnoreCase("Playing")) {
                    try {
                        ScrollView_List_SelectSong.this.mPlayer.stop();
                        ScrollView_List_SelectSong.this.mPlayer.reset();
                        ScrollView_List_SelectSong.this.mPlayer.setDataSource(((MySong) ScrollView_List_SelectSong.this.m_MySong.get(nIndex)).mFileName);
                        ScrollView_List_SelectSong.this.mPlayer.prepare();
                        ScrollView_List_SelectSong.this.mPlayer.start();
                        ScrollView_List_SelectSong.this.mPlayer.setAudioStreamType(3);
                        ((ImageButton) ScrollView_List_SelectSong.this.m_ListPlayBtn.get(nIndex)).setImageResource(17301539);
                        v.setTag("Playing");
                    } catch (Exception e2) {
                        Toast.makeText(ScrollView_List_SelectSong.this, ScrollView_List_SelectSong.this.getString(R.string.contact_ringtone_errorplay_unknow), 1).show();
                        for (int i3 = 0; i3 < nTotal; i3++) {
                            ((LinearLayout) ScrollView_List_SelectSong.this.m_List.get(i3)).setEnabled(true);
                            ((ImageButton) ScrollView_List_SelectSong.this.m_ListPlayBtn.get(i3)).setEnabled(true);
                        }
                        ((ImageButton) ScrollView_List_SelectSong.this.m_ListPlayBtn.get(nIndex)).setImageResource(17301540);
                        v.setTag("Stop");
                    }
                } else {
                    ScrollView_List_SelectSong.this.mPlayer.stop();
                    for (int i4 = 0; i4 < nTotal; i4++) {
                        ((LinearLayout) ScrollView_List_SelectSong.this.m_List.get(i4)).setEnabled(true);
                        ((ImageButton) ScrollView_List_SelectSong.this.m_ListPlayBtn.get(i4)).setEnabled(true);
                    }
                    ((ImageButton) ScrollView_List_SelectSong.this.m_ListPlayBtn.get(nIndex)).setImageResource(17301540);
                    v.setTag("Stop");
                }
            }
        });
        LinearLayout linearLayout2 = new LinearLayout(this);
        linearLayout2.setOrientation(1);
        linearLayout2.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        LinearLayout linearLayout3 = new LinearLayout(this);
        linearLayout3.setOrientation(0);
        ImageView imageView = new ImageView(this);
        imageView.setImageResource(song.mImage);
        linearLayout.setGravity(16);
        imageView.setLayoutParams(new LinearLayout.LayoutParams(16, 16));
        linearLayout3.addView(imageView);
        TextView textView = new TextView(this);
        textView.setTextColor(-16711681);
        textView.setText(song.mArtist);
        textView.setTextSize(14.0f);
        if (song.mArtist != null && (song.mArtist.endsWith("Created by 'Ringtone Picker'") || song.mArtist.endsWith("由'鈴聲選取'建立"))) {
            textView.setTextColor(-65332);
        } else if (song.mImage == R.drawable.type_ringtone) {
            textView.setTextColor(-7593951);
        }
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
        layoutParams.setMargins(4, 0, 4, 0);
        linearLayout3.addView(textView, layoutParams);
        TextView textView2 = new TextView(this);
        textView2.setTextColor(-128);
        textView2.setText(song.mAlbum);
        textView2.setTextSize(12.0f);
        textView2.setSingleLine(true);
        textView2.setGravity(3);
        linearLayout3.addView(textView2, layoutParams);
        linearLayout2.addView(linearLayout3);
        if (song.mImage == R.drawable.type_ringtone) {
            textView2.setTextColor(-7593951);
        }
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-1, -2);
        layoutParams2.setMargins(10, 5, 0, 5);
        TextView textView3 = new TextView(this);
        textView3.setTextColor(-1);
        textView3.setTypeface(null, 1);
        textView3.setText(song.mName);
        textView3.setTextSize(15.0f);
        linearLayout2.addView(textView3, layoutParams2);
        linearLayout.addView(linearLayout2);
        linearLayout.setId(this.m_nCurrent);
        this.m_ll.addView(linearLayout);
        this.m_List.add(linearLayout);
        linearLayout.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                ScrollView_List_SelectSong.this.m_nCurrentClick = v.getId();
                menu.setHeaderTitle(((MySong) ScrollView_List_SelectSong.this.m_MySong.get(ScrollView_List_SelectSong.this.m_nCurrentClick)).mName);
                menu.add(0, v.getId(), 0, ScrollView_List_SelectSong.this.Song_Edit_Edit);
                menu.add(0, v.getId(), 1, ScrollView_List_SelectSong.this.Song_Edit_Delete);
            }
        });
        InsertLineIntoScroll(2, R.drawable.gray);
        linearLayout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ScrollView_List_SelectSong.this.openContextMenu(v);
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void InsertLineIntoScroll(int nHeight, int resourceID) {
        LinearLayout ll = new LinearLayout(this);
        ll.setOrientation(1);
        ll.setBackgroundResource(resourceID);
        TextView tv = new TextView(this);
        tv.setText("");
        tv.setBackgroundColor(resourceID);
        ll.addView(tv);
        this.m_ll.addView(ll);
        if (nHeight > 0) {
            ViewGroup.LayoutParams param = ll.getLayoutParams();
            param.height = nHeight;
            ll.setLayoutParams(param);
        }
    }

    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        final int item_id = item.getItemId();
        if (item.getTitle().toString().equals(this.Song_Edit_Play)) {
            Uri uri = this.m_MySong.get(item_id).mUri;
            if (uri != null) {
                Intent it = new Intent("android.intent.action.VIEW", uri);
                this.m_bNeedToRefresh = false;
                startActivity(it);
            } else {
                Toast.makeText(this, getString(R.string.contact_ringtone_errorplay), 1).show();
            }
            return true;
        } else if (item.getTitle().toString().equals(this.Song_Edit_Edit)) {
            this.m_Tools.ShowHelpDialog((int) R.string.free_no_support);
            return true;
        } else if (!item.getTitle().toString().equals(this.Song_Edit_Delete)) {
            return true;
        } else {
            final MySong song = this.m_MySong.get(item_id);
            new AlertDialog.Builder(this).setTitle(song.mName).setMessage((int) R.string.song_edit_delete_alert).setPositiveButton((int) R.string.Yes, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    int nTotal = ScrollView_List_SelectSong.this.m_MySong.size();
                    boolean bDuplicate = false;
                    int i = 0;
                    while (true) {
                        if (i < nTotal) {
                            if (i != item_id && ScrollView_List_SelectSong.CheckDuplicate(song, (MySong) ScrollView_List_SelectSong.this.m_MySong.get(i))) {
                                bDuplicate = true;
                                break;
                            }
                            i++;
                        } else {
                            break;
                        }
                    }
                    if (bDuplicate || new File(song.mFileName).delete() || !new File(song.mFileName).exists()) {
                        ScrollView_List_SelectSong.this.getContentResolver().delete(song.mUri, null, null);
                        ScrollView_List_SelectSong.this.LoadSongThread();
                        return;
                    }
                    new Tools(ScrollView_List_SelectSong.this).ShowHelpDialog((int) R.string.delete_failed);
                }
            }).setNegativeButton((int) R.string.No, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                }
            }).setCancelable(true).show();
            return true;
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        return true;
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    /* access modifiers changed from: package-private */
    public void GetContactList() {
    }

    /* access modifiers changed from: package-private */
    public String GetRingtoneName(Uri uri) {
        String szName = getString(R.string.Unknown);
        if (uri == null) {
            return szName;
        }
        try {
            Cursor cursor = managedQuery(uri, new String[]{"title"}, null, null, null);
            if (cursor == null || cursor.getCount() != 1) {
                return szName;
            }
            cursor.moveToFirst();
            return cursor.getString(0);
        } catch (Exception e) {
            return szName;
        }
    }

    /* access modifiers changed from: package-private */
    public Cursor createCursor(String filter) {
        String selection;
        ArrayList<String> args = new ArrayList<>();
        if (this.mShowAll) {
            selection = "(_DATA LIKE ?)";
            args.add("%");
        } else {
            String selection2 = "(";
            String[] supportedExtensions = CheapSoundFile.getSupportedExtensions();
            int length = supportedExtensions.length;
            for (int i = 0; i < length; i++) {
                args.add("%." + supportedExtensions[i]);
                if (selection2.length() > 1) {
                    selection2 = String.valueOf(selection2) + " OR ";
                }
                selection2 = String.valueOf(selection2) + "(_DATA LIKE ?)";
            }
            selection = "(" + (String.valueOf(selection2) + ")") + ") AND (_DATA NOT LIKE ?)";
            args.add("%espeak-data/scratch%");
        }
        if (filter != null && filter.length() > 0) {
            String filter2 = "%" + filter + "%";
            selection = "(" + selection + " AND " + "((TITLE LIKE ?) OR (ARTIST LIKE ?) OR (ALBUM LIKE ?)))";
            args.add(filter2);
            args.add(filter2);
            args.add(filter2);
        }
        String[] argsArray = (String[]) args.toArray(new String[args.size()]);
        Cursor externalAudioCursor = getExternalAudioCursor(selection, argsArray);
        Cursor internalAudioCursor = getInternalAudioCursor(selection, argsArray);
        Cursor c = new MergeCursor(new Cursor[]{getExternalAudioCursor(selection, argsArray), getInternalAudioCursor(selection, argsArray)});
        startManagingCursor(c);
        return c;
    }

    private Cursor getInternalAudioCursor(String selection, String[] selectionArgs) {
        return managedQuery(MediaStore.Audio.Media.INTERNAL_CONTENT_URI, INTERNAL_COLUMNS, selection, selectionArgs, "album");
    }

    private Cursor getExternalAudioCursor(String selection, String[] selectionArgs) {
        return managedQuery(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, EXTERNAL_COLUMNS, selection, selectionArgs, "album");
    }

    /* access modifiers changed from: package-private */
    public void ShowProgressDialog() {
        this.progressDialog = ProgressDialog.show(this, getString(R.string.Progress_title), getString(R.string.Progress_description), true);
        this.progressDialog.setCancelable(false);
    }

    /* access modifiers changed from: package-private */
    public void CloseProgressDialog() {
        if (this.progressDialog != null) {
            this.progressDialog.dismiss();
        }
    }

    public class EventHandler extends Handler {
        public EventHandler(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    ScrollView_List_SelectSong.this.m_ll.removeAllViews();
                    int nTotal = ScrollView_List_SelectSong.this.m_MySong.size();
                    for (int i = 0; i < nTotal; i++) {
                        ScrollView_List_SelectSong.this.m_nCurrent = i;
                        ScrollView_List_SelectSong.this.InsertViewIntoScroll((MySong) ScrollView_List_SelectSong.this.m_MySong.get(i));
                    }
                    ScrollView_List_SelectSong.this.CloseProgressDialog();
                    return;
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void LoadSongThread() {
        ShowProgressDialog();
        new Thread() {
            public void run() {
                ScrollView_List_SelectSong.this.mMutex = true;
                ScrollView_List_SelectSong.this.RefreshSong();
                ScrollView_List_SelectSong.this.m_EventHandler.sendMessage(ScrollView_List_SelectSong.this.m_EventHandler.obtainMessage(1, 1, 1, null));
                ScrollView_List_SelectSong.this.mMutex = false;
            }
        }.start();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        this.mMutex = false;
        if (this.m_bNeedToRefresh) {
            LoadSongThread();
        }
        this.m_bNeedToRefresh = false;
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.mPlayer != null && this.mPlayer.isPlaying()) {
            this.mPlayer.stop();
            this.mPlayer.release();
        }
        this.mPlayer = null;
        super.onDestroy();
    }

    public void adWhirlGeneric() {
    }

    public void AdOn() {
        AdView adView = new AdView(this);
        this.adWhirlLayout.addView(adView, new ViewGroup.LayoutParams(320, -2));
        adView.setLicenseKey(getString(R.string.AdOn_ID), AdOnPlatform.TW, true);
    }
}
