package com.bolfish.TonePickerChinese;

import android.content.Context;

public class RingtoneBookmarks {
    public final int MaxStore = 10;
    public Bookmarks[] bookmarks = new Bookmarks[10];
    private EasyData m_EasyData = null;
    private Context m_context;

    RingtoneBookmarks(Context context, String Filename) {
        this.m_context = context;
        this.m_EasyData = new EasyData(context, Filename);
        for (int i = 0; i < 10; i++) {
            this.bookmarks[i] = new Bookmarks();
        }
        ReadAll();
    }

    public class Bookmarks {
        String Name;
        String UriAddress;
        boolean bSaved;

        Bookmarks() {
            this.bSaved = false;
            this.Name = "";
            this.UriAddress = "";
        }

        Bookmarks(Bookmarks b) {
            this.bSaved = b.bSaved;
            this.Name = b.Name;
            this.UriAddress = b.UriAddress;
        }

        Bookmarks(int index, boolean bS, String szN, String szU) {
            this.bSaved = bS;
            this.Name = szN;
            this.UriAddress = szU;
        }
    }

    /* access modifiers changed from: package-private */
    public void ReadAll() {
        this.m_EasyData.Init();
        for (int i = 0; i < 10; i++) {
            this.bookmarks[i].bSaved = this.m_EasyData.GetSaved(i);
            this.bookmarks[i].Name = this.m_EasyData.GetSavedItem1(i);
            this.bookmarks[i].UriAddress = this.m_EasyData.GetSavedItem2(i);
        }
    }

    /* access modifiers changed from: package-private */
    public void Delete(int i) {
        this.bookmarks[i].bSaved = false;
        this.m_EasyData.Delete(i);
    }

    /* access modifiers changed from: package-private */
    public void DeleteByUri(String szUri) {
        int i = 0;
        while (i < 10) {
            if (!this.bookmarks[i].bSaved || !this.bookmarks[i].UriAddress.equals(szUri)) {
                i++;
            } else {
                this.bookmarks[i].bSaved = false;
                this.m_EasyData.Delete(i);
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void Empty() {
        this.m_EasyData.Empty();
        for (int i = 0; i < 10; i++) {
            this.bookmarks[i].bSaved = false;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean AlreadySaved(String data) {
        for (int i = 0; i < 10; i++) {
            if (this.bookmarks[i].bSaved && this.bookmarks[i].UriAddress.equals(data)) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public int Insert(String name, String data) {
        if (AlreadySaved(data)) {
            return -2;
        }
        int nIndex = this.m_EasyData.Insert(name, data);
        if (nIndex > -1) {
            this.bookmarks[nIndex].bSaved = true;
            this.bookmarks[nIndex].Name = name;
            this.bookmarks[nIndex].UriAddress = data;
        }
        return nIndex;
    }
}
