package com.bolfish.TonePickerChinese;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.Contacts;
import android.provider.ContactsContract;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import com.adwhirl.AdWhirlLayout;
import com.vpon.adon.android.AdOnPlatform;
import com.vpon.adon.android.AdView;
import java.util.ArrayList;

public class ScrollView_List extends Activity implements AdWhirlLayout.AdWhirlInterface {
    private final int COLOR_BLACK = -16777216;
    private final int COLOR_BLUE = -16741493;
    private final int COLOR_DARKGREEN = -13011031;
    private final int COLOR_GRAY = -8355712;
    private final int COLOR_LIGHTGRAY = -5197648;
    private final int COLOR_ORANGE = -32704;
    private final int COLOR_RED = -65536;
    private final int COLOR_WHITE = -1;
    private final int COLOR_YELLOW = -128;
    /* access modifiers changed from: private */
    public String Contact_Ringtone_change;
    /* access modifiers changed from: private */
    public String Contact_Ringtone_delete;
    private String Contact_Ringtone_name;
    private String Contact_Ringtone_none;
    /* access modifiers changed from: private */
    public String Contact_Ringtone_play;
    final String MySettingFile = "Bolfish_RingTone";
    AdWhirlLayout adWhirlLayout;
    private String mSearchKeyword;
    /* access modifiers changed from: private */
    public ArrayList<MyPeople> m_ContactList = new ArrayList<>();
    private ArrayList<LinearLayout> m_Line = new ArrayList<>();
    private ArrayList<LinearLayout> m_List = new ArrayList<>();
    private ArrayList<OsPeople> m_OsContactList = new ArrayList<>();
    private ArrayList<TextView> m_Ringtone = new ArrayList<>();
    Tools m_Tools = new Tools(this);
    LinearLayout m_ll;
    private int m_nCurrent;
    /* access modifiers changed from: private */
    public int m_nCurrentClick;
    ScrollView m_sv;

    private void InitOSContactList(String filter) {
        String selection;
        this.m_OsContactList.clear();
        if (filter == null || filter.length() <= 0) {
            selection = null;
        } else {
            selection = "(DISPLAY_NAME LIKE \"%" + filter + "%\")";
        }
        Cursor cursor = managedQuery(getContactContentUri(), new String[]{"_id", "custom_ringtone", "display_name", "last_time_contacted", "starred", "times_contacted"}, selection, null, "STARRED DESC, TIMES_CONTACTED DESC, LAST_TIME_CONTACTED DESC, DISPLAY_NAME ASC");
        while (cursor.moveToNext()) {
            OsPeople people = new OsPeople();
            people.Id = cursor.getString(cursor.getColumnIndexOrThrow("_id"));
            people.Name = cursor.getString(cursor.getColumnIndexOrThrow("display_name"));
            this.m_OsContactList.add(people);
        }
    }

    /* access modifiers changed from: package-private */
    public String MyToOS(String szName) {
        int nTotal = this.m_OsContactList.size();
        for (int i = 0; i < nTotal; i++) {
            OsPeople people = this.m_OsContactList.get(i);
            if (people.Name.equals(szName)) {
                return people.Id;
            }
        }
        return null;
    }

    public class MyPeople {
        String Id;
        String Name;
        String RingToneName;
        Uri RingToneUri;

        public MyPeople() {
        }
    }

    public class OsPeople {
        String Id;
        String Name;

        public OsPeople() {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bolfish.TonePickerChinese.Tools.QuickSave(java.lang.String, java.lang.String, boolean):void
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.bolfish.TonePickerChinese.Tools.QuickSave(java.lang.String, java.lang.String, int):void
      com.bolfish.TonePickerChinese.Tools.QuickSave(java.lang.String, java.lang.String, java.lang.String):void
      com.bolfish.TonePickerChinese.Tools.QuickSave(java.lang.String, java.lang.String, boolean):void */
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.scrollview_list);
        setVolumeControlStream(0);
        if (this.m_Tools.QuickLoadBoolean("Bolfish_RingTone", "CONTACT_HELP", true)) {
            this.m_Tools.QuickSave("Bolfish_RingTone", "CONTACT_HELP", false);
            this.m_Tools.ShowHelpDialog((int) R.string.contact_help);
        }
        this.m_sv = (ScrollView) findViewById(R.id.ScrollView_List);
        this.m_ll = new LinearLayout(this);
        this.m_ll.setOrientation(1);
        this.m_sv.addView(this.m_ll);
        this.Contact_Ringtone_name = getString(R.string.contact_ringtone_name);
        this.Contact_Ringtone_delete = getString(R.string.contact_ringtone_delete);
        this.Contact_Ringtone_none = getString(R.string.contact_ringtone_none);
        this.Contact_Ringtone_play = getString(R.string.contact_ringtone_play);
        this.Contact_Ringtone_change = getString(R.string.contact_ringtone_change);
        InitOSContactList("");
        GetContactListAfterSDK_5();
        int nTotal = this.m_ContactList.size();
        for (int i = 0; i < nTotal; i++) {
            this.m_nCurrent = i;
            InsertContactScroll(this.m_ContactList.get(i));
        }
        LinearLayout layout = (LinearLayout) findViewById(R.id.ad);
        this.adWhirlLayout = new AdWhirlLayout(this, getString(R.string.Adwhirl_ID));
        this.adWhirlLayout.setAdWhirlInterface(this);
        layout.addView(this.adWhirlLayout, new RelativeLayout.LayoutParams(-1, -2));
        layout.invalidate();
    }

    /* access modifiers changed from: package-private */
    public void InsertContactScroll(MyPeople people) {
        LinearLayout ll = new LinearLayout(this);
        ll.setOrientation(0);
        ll.setBackgroundResource(R.drawable.black);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
        layoutParams.setMargins(10, 10, 10, 10);
        ImageView iv_title = new ImageView(this);
        iv_title.setImageResource(R.drawable.contact);
        ll.setGravity(16);
        ll.addView(iv_title, layoutParams);
        LinearLayout.LayoutParams layoutParams1 = new LinearLayout.LayoutParams(-1, -2);
        LinearLayout ll_V = new LinearLayout(this);
        ll_V.setOrientation(1);
        ll_V.setBackgroundResource(R.drawable.black);
        ll_V.setLayoutParams(layoutParams1);
        TextView tv_title = new TextView(this);
        tv_title.setTextColor(-1);
        tv_title.setTypeface(null, 1);
        tv_title.setText(people.Name);
        tv_title.setTextSize(20.0f);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-1, -2);
        layoutParams2.setMargins(10, 0, 0, 0);
        ll_V.addView(tv_title, layoutParams2);
        TextView tv_ring = new TextView(this);
        tv_ring.setTextColor(-128);
        tv_ring.setTypeface(null, 1);
        if (people.RingToneName == null) {
            tv_ring.setText(String.valueOf(this.Contact_Ringtone_name) + this.Contact_Ringtone_none);
            tv_ring.setTextColor(-11513776);
        } else {
            tv_ring.setText(String.valueOf(this.Contact_Ringtone_name) + people.RingToneName);
        }
        this.m_Ringtone.add(tv_ring);
        tv_ring.setTextSize(12.0f);
        ll_V.addView(tv_ring, layoutParams2);
        ll.addView(ll_V);
        this.m_ll.addView(ll);
        this.m_List.add(ll);
        ll.setId(this.m_nCurrent);
        ll.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                ScrollView_List.this.m_nCurrentClick = v.getId();
                if (((MyPeople) ScrollView_List.this.m_ContactList.get(v.getId())).RingToneUri != null) {
                    menu.setHeaderTitle("Menu");
                    menu.add(0, v.getId(), 0, ScrollView_List.this.Contact_Ringtone_change);
                    menu.add(0, v.getId(), 1, ScrollView_List.this.Contact_Ringtone_delete);
                    menu.add(0, v.getId(), 2, ScrollView_List.this.Contact_Ringtone_play);
                    menu.add(0, v.getId(), 3, ScrollView_List.this.getString(R.string.Cancel));
                    return;
                }
                ScrollView_List.this.ChooseRingTone(0, "", true);
            }
        });
        InsertLineIntoScroll(2, R.drawable.gray);
        ll.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ScrollView_List.this.openContextMenu(v);
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void InsertLineIntoScroll(int nHeight, int resourceID) {
        LinearLayout ll = new LinearLayout(this);
        ll.setOrientation(1);
        ll.setBackgroundResource(resourceID);
        TextView tv = new TextView(this);
        tv.setText("");
        tv.setBackgroundColor(resourceID);
        this.m_ll.addView(ll);
        this.m_Line.add(ll);
        if (nHeight > 0) {
            ViewGroup.LayoutParams param = ll.getLayoutParams();
            param.height = nHeight;
            ll.setLayoutParams(param);
        }
    }

    /* access modifiers changed from: package-private */
    public void Input() {
        View textEntryView = LayoutInflater.from(this).inflate((int) R.layout.youtube_keyword_input, (ViewGroup) null);
        if (textEntryView != null) {
            textEntryView.setMinimumWidth(300);
            final EditText et_title = (EditText) textEntryView.findViewById(R.id.ET_addbookmark_title);
            if (et_title != null) {
                et_title.setText("");
                AlertDialog dlg = new AlertDialog.Builder(this).setIcon((int) R.drawable.alert_dialog_icon).setView(textEntryView).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        ScrollView_List.this.Search(et_title.getText().toString());
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                }).create();
                dlg.getWindow().setFlags(1024, 1024);
                dlg.show();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void Search(String szSearch) {
        if (szSearch == null) {
            int nTotal = this.m_List.size();
            for (int i = 0; i < nTotal; i++) {
                this.m_List.get(i).setVisibility(0);
                this.m_Line.get(i).setVisibility(0);
            }
            return;
        }
        String szSearch2 = szSearch.toLowerCase();
        int nTotal2 = this.m_List.size();
        for (int i2 = 0; i2 < nTotal2; i2++) {
            if (this.m_ContactList.get(i2).Name.toLowerCase().contains(szSearch2)) {
                this.m_List.get(i2).setVisibility(0);
                this.m_Line.get(i2).setVisibility(0);
            } else {
                this.m_List.get(i2).setVisibility(8);
                this.m_Line.get(i2).setVisibility(8);
            }
        }
    }

    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        if (item.getTitle().toString().equals(this.Contact_Ringtone_change)) {
            ChooseRingTone(0, "", true);
            return true;
        } else if (item.getTitle().toString().equals(this.Contact_Ringtone_delete)) {
            int item_id = item.getItemId();
            TextView tv = this.m_Ringtone.get(item_id);
            tv.setText(String.valueOf(this.Contact_Ringtone_name) + this.Contact_Ringtone_none);
            tv.setTextColor(-11513776);
            MyPeople people = this.m_ContactList.get(item_id);
            String sID = MyToOS(people.Name);
            if (sID == null) {
                return false;
            }
            Uri uriPeople = Uri.withAppendedPath(getContactContentUri(), sID);
            ContentValues values = new ContentValues();
            values.put("custom_ringtone", (String) null);
            getContentResolver().update(uriPeople, values, null, null);
            people.RingToneName = null;
            people.RingToneUri = null;
            return true;
        } else if (item.getTitle().toString().equals(this.Contact_Ringtone_play)) {
            Uri uri = this.m_ContactList.get(item.getItemId()).RingToneUri;
            if (uri != null) {
                startActivity(new Intent("android.intent.action.VIEW", uri));
            } else {
                Toast.makeText(this, getString(R.string.contact_ringtone_errorplay), 1).show();
            }
            return true;
        } else {
            switch (item.getItemId()) {
                case R.id.menu_search:
                    Input();
                    break;
                case R.id.menu_all:
                    Search(null);
                    break;
                case R.id.menu_getpro:
                    startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=com.bolfish_paid.TonePickerWidget")));
                    break;
            }
            return true;
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.contact, menu);
        return true;
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    /* access modifiers changed from: package-private */
    public void GetContactList() {
        this.m_ContactList.clear();
        Cursor cur = getContentResolver().query(Contacts.People.CONTENT_URI, null, null, null, "DISPLAY_NAME ASC");
        if (cur.getCount() > 0) {
            while (cur.moveToNext()) {
                String id = cur.getString(cur.getColumnIndex("_id"));
                String name = cur.getString(cur.getColumnIndex("display_name"));
                String ringtonename = cur.getString(cur.getColumnIndex("custom_ringtone"));
                String PhoneID = cur.getString(cur.getColumnIndex("primary_phone"));
                if (PhoneID != null && Integer.parseInt(PhoneID) > 0) {
                    MyPeople people = new MyPeople();
                    people.Id = id;
                    people.Name = name;
                    if (ringtonename == null) {
                        people.RingToneName = null;
                    } else {
                        people.RingToneName = GetRingtoneName(Uri.parse(ringtonename));
                    }
                    this.m_ContactList.add(people);
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void GetContactListAfterSDK_5() {
        this.m_ContactList.clear();
        Cursor cur = getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, "DISPLAY_NAME ASC");
        if (cur.getCount() > 0) {
            while (cur.moveToNext()) {
                String id = cur.getString(cur.getColumnIndex("_id"));
                String name = cur.getString(cur.getColumnIndex("display_name"));
                String ringtonename = cur.getString(cur.getColumnIndex("custom_ringtone"));
                String PhoneID = cur.getString(cur.getColumnIndex("has_phone_number"));
                if (PhoneID != null && Integer.parseInt(PhoneID) > 0) {
                    MyPeople people = new MyPeople();
                    people.Id = id;
                    people.Name = name;
                    if (ringtonename == null) {
                        people.RingToneName = null;
                        people.RingToneUri = null;
                    } else {
                        Uri uri = Uri.parse(ringtonename);
                        people.RingToneUri = uri;
                        people.RingToneName = GetRingtoneName(uri);
                    }
                    this.m_ContactList.add(people);
                }
            }
        }
    }

    private boolean isEclairOrLater() {
        return Build.VERSION.SDK_INT >= 5;
    }

    private Uri getContactContentUri() {
        if (isEclairOrLater()) {
            return Uri.parse("content://com.android.contacts/contacts");
        }
        return Contacts.People.CONTENT_URI;
    }

    /* access modifiers changed from: package-private */
    public String GetRingtoneName(Uri uri) {
        String szName = getString(R.string.Unknown);
        if (uri == null) {
            return szName;
        }
        try {
            Cursor cursor = managedQuery(uri, new String[]{"title"}, null, null, null);
            if (cursor == null || cursor.getCount() != 1) {
                return szName;
            }
            cursor.moveToFirst();
            return cursor.getString(0);
        } catch (Exception e) {
            return szName;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: private */
    public void ChooseRingTone(int nRingType, String sTitle, boolean bFile) {
        if (bFile) {
            Intent intent = new Intent("android.intent.action.GET_CONTENT");
            intent.setType("audio/*");
            startActivityForResult(Intent.createChooser(intent, "Select music"), 263);
            return;
        }
        Intent intent2 = new Intent("android.intent.action.RINGTONE_PICKER");
        intent2.putExtra("android.intent.extra.ringtone.TYPE", nRingType);
        intent2.putExtra("android.intent.extra.ringtone.TITLE", sTitle);
        Uri uri1 = RingtoneManager.getActualDefaultRingtoneUri(this, nRingType);
        if (uri1 != null) {
            intent2.putExtra("android.intent.extra.ringtone.EXISTING_URI", uri1);
        } else {
            intent2.putExtra("android.intent.extra.ringtone.EXISTING_URI", (Parcelable) null);
        }
        intent2.putExtra("android.intent.extra.ringtone.SHOW_DEFAULT", false);
        intent2.putExtra("android.intent.extra.ringtone.SHOW_SILENT", false);
        startActivityForResult(intent2, 264);
    }

    private boolean ChangeRingTone(String szName, Uri ringtone) {
        String sID = MyToOS(szName);
        if (sID == null) {
            return false;
        }
        Uri uriPeople = Uri.withAppendedPath(getContactContentUri(), sID);
        if (ringtone == null) {
            return false;
        }
        ContentValues values = new ContentValues();
        values.put("custom_ringtone", ringtone.toString());
        getContentResolver().update(uriPeople, values, null, null);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == -1) {
            switch (requestCode) {
                case 263:
                    this.m_Tools.ShowHelpDialog((int) R.string.free_no_support);
                    break;
            }
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void adWhirlGeneric() {
    }

    public void AdOn() {
        AdView adView = new AdView(this);
        this.adWhirlLayout.addView(adView, new ViewGroup.LayoutParams(320, -2));
        adView.setLicenseKey(getString(R.string.AdOn_ID), AdOnPlatform.TW, true);
    }
}
