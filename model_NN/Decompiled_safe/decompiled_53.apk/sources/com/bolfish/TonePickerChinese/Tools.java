package com.bolfish.TonePickerChinese;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.widget.Toast;

public class Tools {
    private final Context m_context;
    Ringtone newTone;

    Tools(Context context) {
        this.m_context = context;
    }

    /* access modifiers changed from: package-private */
    public void Toast(String szMessage) {
        Toast.makeText(this.m_context, szMessage, 0).show();
    }

    /* access modifiers changed from: package-private */
    public void Toast(int nRID) {
        Toast.makeText(this.m_context, this.m_context.getString(nRID), 0).show();
    }

    public static void Toast(Context context, String szMessage) {
        Toast.makeText(context, szMessage, 0).show();
    }

    /* access modifiers changed from: package-private */
    public void QuickSave(String szFilename, String szItem, boolean bValue) {
        SharedPreferences.Editor editor = this.m_context.getSharedPreferences(szFilename, 0).edit();
        editor.putBoolean(szItem, bValue);
        editor.commit();
    }

    /* access modifiers changed from: package-private */
    public void QuickSave(String szFilename, String szItem, int iValue) {
        SharedPreferences.Editor editor = this.m_context.getSharedPreferences(szFilename, 0).edit();
        editor.putInt(szItem, iValue);
        editor.commit();
    }

    /* access modifiers changed from: package-private */
    public void QuickSave(String szFilename, String szItem, String sValue) {
        SharedPreferences.Editor editor = this.m_context.getSharedPreferences(szFilename, 0).edit();
        editor.putString(szItem, sValue);
        editor.commit();
    }

    public boolean QuickLoadBoolean(String szFilename, String szItem, boolean bDefault) {
        return this.m_context.getSharedPreferences(szFilename, 0).getBoolean(szItem, bDefault);
    }

    public int QuickLoadInt(String szFilename, String szItem, int nDefault) {
        return this.m_context.getSharedPreferences(szFilename, 0).getInt(szItem, nDefault);
    }

    public String QuickLoadString(String szFilename, String szItem, String sDefault) {
        return this.m_context.getSharedPreferences(szFilename, 0).getString(szItem, sDefault);
    }

    public void ShowHelpDialog(int nStringID) {
        new AlertDialog.Builder(this.m_context).setMessage(nStringID).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        }).create().show();
    }

    public void ShowHelpDialog(String string) {
        new AlertDialog.Builder(this.m_context).setMessage(string).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        }).create().show();
    }

    /* access modifiers changed from: package-private */
    public String GetRingtoneName(boolean bNotification) {
        Uri uri;
        if (bNotification) {
            uri = RingtoneManager.getActualDefaultRingtoneUri(this.m_context, 2);
        } else {
            uri = RingtoneManager.getActualDefaultRingtoneUri(this.m_context, 1);
        }
        if (uri != null) {
            this.newTone = RingtoneManager.getRingtone(this.m_context, uri);
            if (this.newTone != null) {
                return this.newTone.getTitle(this.m_context);
            }
            Toast(this.m_context, "Error GetName 0");
            return "";
        }
        Toast(this.m_context, "Error GetName");
        return "";
    }

    /* access modifiers changed from: package-private */
    public String GetRingtoneUri(boolean bNotification) {
        Uri uri;
        if (bNotification) {
            uri = RingtoneManager.getActualDefaultRingtoneUri(this.m_context, 2);
        } else {
            uri = RingtoneManager.getActualDefaultRingtoneUri(this.m_context, 1);
        }
        if (uri != null) {
            return uri.toString();
        }
        return "";
    }
}
