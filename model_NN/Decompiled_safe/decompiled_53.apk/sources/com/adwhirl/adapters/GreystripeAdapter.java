package com.adwhirl.adapters;

import android.app.Activity;
import android.util.Log;
import com.adwhirl.AdWhirlLayout;
import com.adwhirl.obj.Ration;
import com.adwhirl.util.AdWhirlUtil;
import com.greystripe.android.sdk.BannerListener;
import com.greystripe.android.sdk.BannerView;
import com.greystripe.android.sdk.GSSDK;

public final class GreystripeAdapter extends AdWhirlAdapter implements BannerListener {
    private static GSSDK sdk;

    public GreystripeAdapter(AdWhirlLayout adWhirlLayout, Ration ration) {
        super(adWhirlLayout, ration);
        Activity activity;
        if (sdk == null && (activity = adWhirlLayout.activityReference.get()) != null) {
            GSSDK.initialize(activity, this.ration.key);
        }
    }

    public void handle() {
        Activity activity;
        AdWhirlLayout adWhirlLayout = (AdWhirlLayout) this.adWhirlLayoutReference.get();
        if (adWhirlLayout != null && (activity = adWhirlLayout.activityReference.get()) != null) {
            BannerView bannerView = new BannerView(activity);
            bannerView.addListener(this);
            bannerView.refresh();
        }
    }

    public void onFailedToReceiveAd(BannerView bannerView) {
        Log.d(AdWhirlUtil.ADWHIRL, "Greystripe banner failed");
        AdWhirlLayout adWhirlLayout = (AdWhirlLayout) this.adWhirlLayoutReference.get();
        if (adWhirlLayout != null) {
            adWhirlLayout.rollover();
        }
    }

    public void onReceivedAd(BannerView bannerView) {
        Log.d(AdWhirlUtil.ADWHIRL, "Greystripe banner successfully returned");
        AdWhirlLayout adWhirlLayout = (AdWhirlLayout) this.adWhirlLayoutReference.get();
        if (adWhirlLayout != null) {
            adWhirlLayout.adWhirlManager.resetRollover();
            adWhirlLayout.handler.post(new AdWhirlLayout.ViewAdRunnable(adWhirlLayout, bannerView));
            adWhirlLayout.rotateThreadedDelayed();
        }
    }
}
