package com.adwhirl.adapters;

import android.util.Log;
import com.adwhirl.AdWhirlLayout;
import com.adwhirl.obj.Ration;
import com.adwhirl.util.AdWhirlUtil;

public class EventAdapter extends AdWhirlAdapter {
    public EventAdapter(AdWhirlLayout adWhirlLayout, Ration ration) {
        super(adWhirlLayout, ration);
    }

    public void handle() {
        Log.d(AdWhirlUtil.ADWHIRL, "Event notification request initiated");
        AdWhirlLayout adWhirlLayout = (AdWhirlLayout) this.adWhirlLayoutReference.get();
        if (adWhirlLayout != null) {
            if (adWhirlLayout.adWhirlInterface != null) {
                String key = this.ration.key;
                if (key == null) {
                    Log.w(AdWhirlUtil.ADWHIRL, "Event key is null");
                }
                int methodIndex = key.indexOf("|;|");
                if (methodIndex < 0) {
                    Log.w(AdWhirlUtil.ADWHIRL, "Event key separator not found");
                }
                try {
                    adWhirlLayout.adWhirlInterface.getClass().getMethod(key.substring(methodIndex + 3), null).invoke(adWhirlLayout.adWhirlInterface, null);
                } catch (Exception e) {
                    Log.e(AdWhirlUtil.ADWHIRL, "Caught exception in handle()", e);
                }
            } else {
                Log.w(AdWhirlUtil.ADWHIRL, "Event notification would be sent, but no interface is listening");
            }
        }
    }
}
