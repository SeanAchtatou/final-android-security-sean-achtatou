package com.adwhirl;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class AdWhirlTargeting {
    private static GregorianCalendar birthDate;
    private static Gender gender;
    private static String keywords;
    private static String postalCode;
    private static boolean testMode;

    public enum Gender {
        UNKNOWN,
        MALE,
        FEMALE
    }

    static {
        resetData();
    }

    public static void resetData() {
        testMode = false;
        gender = Gender.UNKNOWN;
        birthDate = null;
        postalCode = null;
        keywords = null;
    }

    public static boolean getTestMode() {
        return testMode;
    }

    public static void setTestMode(boolean testMode2) {
        testMode = testMode2;
    }

    public static Gender getGender() {
        return gender;
    }

    public static void setGender(Gender gender2) {
        if (gender2 == null) {
            gender2 = Gender.UNKNOWN;
        }
        gender = gender2;
    }

    public static int getAge() {
        if (birthDate != null) {
            return GregorianCalendar.getInstance().get(1) - birthDate.get(1);
        }
        return -1;
    }

    public static GregorianCalendar getBirthDate() {
        return birthDate;
    }

    public static void setBirthDate(GregorianCalendar birthDate2) {
        birthDate = birthDate2;
    }

    public static void setAge(int age) {
        birthDate = new GregorianCalendar(Calendar.getInstance().get(1) - age, 0, 1);
    }

    public static String getPostalCode() {
        return postalCode;
    }

    public static void setPostalCode(String postalCode2) {
        postalCode = postalCode2;
    }

    public static String getKeywords() {
        return keywords;
    }

    public static void setKeywords(String keywords2) {
        keywords = keywords2;
    }
}
