package com.adwhirl.obj;

public class Ration implements Comparable<Ration> {
    public String key = "";
    public String name = "";
    public String nid = "";
    public int priority = 0;
    public int type = 0;
    public double weight = 0.0d;

    public int compareTo(Ration another) {
        int otherPriority = another.priority;
        if (this.priority < otherPriority) {
            return -1;
        }
        if (this.priority > otherPriority) {
            return 1;
        }
        return 0;
    }

    public String toString() {
        return "Ration [key=" + this.key + ", name=" + this.name + ", nid=" + this.nid + ", priority=" + this.priority + ", type=" + this.type + ", weight=" + this.weight + "]";
    }
}
