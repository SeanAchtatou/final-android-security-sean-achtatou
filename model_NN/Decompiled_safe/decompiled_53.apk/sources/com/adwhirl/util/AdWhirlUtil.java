package com.adwhirl.util;

public class AdWhirlUtil {
    public static final String ADWHIRL = "AdWhirl SDK";
    public static final int CUSTOM_TYPE_BANNER = 1;
    public static final int CUSTOM_TYPE_ICON = 2;
    public static final int NETWORK_TYPE_4THSCREEN = 13;
    public static final int NETWORK_TYPE_ADMOB = 1;
    public static final int NETWORK_TYPE_ADSENSE = 14;
    public static final int NETWORK_TYPE_ADWHIRL = 10;
    public static final int NETWORK_TYPE_CUSTOM = 9;
    public static final int NETWORK_TYPE_DOUBLECLICK = 15;
    public static final int NETWORK_TYPE_EVENT = 17;
    public static final int NETWORK_TYPE_GENERIC = 16;
    public static final int NETWORK_TYPE_GREYSTRIPE = 7;
    public static final int NETWORK_TYPE_JUMPTAP = 2;
    public static final int NETWORK_TYPE_LIVERAIL = 5;
    public static final int NETWORK_TYPE_MDOTM = 12;
    public static final int NETWORK_TYPE_MEDIALETS = 4;
    public static final int NETWORK_TYPE_MILLENNIAL = 6;
    public static final int NETWORK_TYPE_MOBCLIX = 11;
    public static final int NETWORK_TYPE_QUATTRO = 8;
    public static final int NETWORK_TYPE_VIDEOEGG = 3;
    public static final int NETWORK_TYPE_ZESTADZ = 20;
    public static final int VERSION = 254;
    public static final String locationString = "&location=%f,%f&location_timestamp=%d";
    public static final String urlClick = "http://met.adwhirl.com/exclick.php?appid=%s&nid=%s&type=%d&uuid=%s&country_code=%s&appver=%d&client=2";
    public static final String urlConfig = "http://mob.adwhirl.com/getInfo.php?appid=%s&appver=%d&client=2";
    public static final String urlCustom = "http://cus.adwhirl.com/custom.php?appid=%s&nid=%s&uuid=%s&country_code=%s%s&appver=%d&client=2";
    public static final String urlImpression = "http://met.adwhirl.com/exmet.php?appid=%s&nid=%s&type=%d&uuid=%s&country_code=%s&appver=%d&client=2";

    public static String convertToHex(byte[] data) {
        StringBuffer buf = new StringBuffer();
        for (int i = 0; i < data.length; i++) {
            int halfbyte = (data[i] >>> 4) & 15;
            int two_halfs = 0;
            while (true) {
                if (halfbyte < 0 || halfbyte > 9) {
                    buf.append((char) ((halfbyte - 10) + 97));
                } else {
                    buf.append((char) (halfbyte + 48));
                }
                halfbyte = data[i] & 15;
                int two_halfs2 = two_halfs + 1;
                if (two_halfs >= 1) {
                    break;
                }
                two_halfs = two_halfs2;
            }
        }
        return buf.toString();
    }
}
