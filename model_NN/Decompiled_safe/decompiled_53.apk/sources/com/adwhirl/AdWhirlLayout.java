package com.adwhirl;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import com.adwhirl.adapters.AdWhirlAdapter;
import com.adwhirl.obj.Custom;
import com.adwhirl.obj.Extra;
import com.adwhirl.obj.Ration;
import com.adwhirl.util.AdWhirlUtil;
import com.bolfish.TonePickerChinese.R;
import com.qwapi.adclient.android.view.QWAdView;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public class AdWhirlLayout extends RelativeLayout {
    public Ration activeRation;
    public final WeakReference<Activity> activityReference;
    public AdWhirlInterface adWhirlInterface;
    public AdWhirlManager adWhirlManager;
    public Custom custom;
    public Extra extra;
    public final Handler handler = new Handler();
    private boolean hasWindow = true;
    private boolean isRotating = true;
    public Ration nextRation;
    public QWAdView quattroView;
    public final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
    public WeakReference<RelativeLayout> superViewReference = new WeakReference<>(this);

    public interface AdWhirlInterface {
        void adWhirlGeneric();
    }

    public AdWhirlLayout(Activity context, String keyAdWhirl) {
        super(context);
        this.activityReference = new WeakReference<>(context);
        this.scheduler.schedule(new InitRunnable(this, keyAdWhirl), 0, TimeUnit.SECONDS);
        setHorizontalScrollBarEnabled(false);
        setVerticalScrollBarEnabled(false);
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int visibility) {
        if (visibility == 0) {
            this.hasWindow = true;
            if (!this.isRotating) {
                this.isRotating = true;
                rotateThreadedNow();
                return;
            }
            return;
        }
        this.hasWindow = false;
    }

    /* access modifiers changed from: private */
    public void rotateAd() {
        if (!this.hasWindow) {
            this.isRotating = false;
            return;
        }
        Log.i(AdWhirlUtil.ADWHIRL, "Rotating Ad");
        this.nextRation = this.adWhirlManager.getRation();
        this.handler.post(new HandleAdRunnable(this));
    }

    /* access modifiers changed from: private */
    public void handleAd() {
        if (this.nextRation == null) {
            Log.e(AdWhirlUtil.ADWHIRL, "nextRation is null!");
            rotateThreadedDelayed();
            return;
        }
        Log.d(AdWhirlUtil.ADWHIRL, this.nextRation.toString());
        try {
            AdWhirlAdapter.handle(this, this.nextRation);
        } catch (Throwable th) {
            Log.w(AdWhirlUtil.ADWHIRL, "Caught an exception in adapter:", th);
            rollover();
        }
    }

    public void rotateThreadedNow() {
        this.scheduler.schedule(new RotateAdRunnable(this), 0, TimeUnit.SECONDS);
    }

    public void rotateThreadedDelayed() {
        Log.d(AdWhirlUtil.ADWHIRL, "Will call rotateAd() in " + this.extra.cycleTime + " seconds");
        this.scheduler.schedule(new RotateAdRunnable(this), (long) this.extra.cycleTime, TimeUnit.SECONDS);
    }

    public void pushSubView(ViewGroup subView) {
        RelativeLayout superView = this.superViewReference.get();
        if (superView != null) {
            superView.removeAllViews();
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams.addRule(13);
            superView.addView(subView, layoutParams);
            Log.d(AdWhirlUtil.ADWHIRL, "Added subview");
            this.activeRation = this.nextRation;
            countImpression();
        }
    }

    public void rollover() {
        this.nextRation = this.adWhirlManager.getRollover();
        this.handler.post(new HandleAdRunnable(this));
    }

    private void countImpression() {
        this.scheduler.schedule(new PingUrlRunnable(String.format(AdWhirlUtil.urlImpression, this.adWhirlManager.keyAdWhirl, this.activeRation.nid, Integer.valueOf(this.activeRation.type), this.adWhirlManager.deviceIDHash, this.adWhirlManager.localeString, Integer.valueOf((int) AdWhirlUtil.VERSION))), 0, TimeUnit.SECONDS);
    }

    private void countClick() {
        this.scheduler.schedule(new PingUrlRunnable(String.format(AdWhirlUtil.urlClick, this.adWhirlManager.keyAdWhirl, this.activeRation.nid, Integer.valueOf(this.activeRation.type), this.adWhirlManager.deviceIDHash, this.adWhirlManager.localeString, Integer.valueOf((int) AdWhirlUtil.VERSION))), 0, TimeUnit.SECONDS);
    }

    public boolean onInterceptTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case R.styleable.com_admob_android_ads_AdView_backgroundColor /*0*/:
                Log.d(AdWhirlUtil.ADWHIRL, "Intercepted ACTION_DOWN event");
                if (this.activeRation != null) {
                    countClick();
                    if (this.activeRation.type == 9) {
                        if (this.custom != null && this.custom.link != null) {
                            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(this.custom.link));
                            intent.addFlags(268435456);
                            try {
                                if (this.activityReference != null) {
                                    Activity activity = this.activityReference.get();
                                    if (activity != null) {
                                        activity.startActivity(intent);
                                        break;
                                    } else {
                                        return false;
                                    }
                                } else {
                                    return false;
                                }
                            } catch (Exception e) {
                                Log.w(AdWhirlUtil.ADWHIRL, "Could not handle click to " + this.custom.link, e);
                                break;
                            }
                        } else {
                            Log.w(AdWhirlUtil.ADWHIRL, "In onInterceptTouchEvent(), but custom or custom.link is null");
                            break;
                        }
                    }
                }
                break;
        }
        return false;
    }

    public void setAdWhirlInterface(AdWhirlInterface i) {
        this.adWhirlInterface = i;
    }

    private static class InitRunnable implements Runnable {
        private WeakReference<AdWhirlLayout> adWhirlLayoutReference;
        private String keyAdWhirl;

        public InitRunnable(AdWhirlLayout adWhirlLayout, String keyAdWhirl2) {
            this.adWhirlLayoutReference = new WeakReference<>(adWhirlLayout);
            this.keyAdWhirl = keyAdWhirl2;
        }

        public void run() {
            Activity activity;
            AdWhirlLayout adWhirlLayout = this.adWhirlLayoutReference.get();
            if (adWhirlLayout != null && (activity = adWhirlLayout.activityReference.get()) != null) {
                adWhirlLayout.adWhirlManager = new AdWhirlManager(new WeakReference(activity.getApplicationContext()), this.keyAdWhirl);
                adWhirlLayout.extra = adWhirlLayout.adWhirlManager.getExtra();
                if (adWhirlLayout.extra == null) {
                    Log.e(AdWhirlUtil.ADWHIRL, "Unable to get configuration info or bad info, exiting AdWhirl");
                } else {
                    adWhirlLayout.rotateAd();
                }
            }
        }
    }

    private static class HandleAdRunnable implements Runnable {
        private WeakReference<AdWhirlLayout> adWhirlLayoutReference;

        public HandleAdRunnable(AdWhirlLayout adWhirlLayout) {
            this.adWhirlLayoutReference = new WeakReference<>(adWhirlLayout);
        }

        public void run() {
            AdWhirlLayout adWhirlLayout = this.adWhirlLayoutReference.get();
            if (adWhirlLayout != null) {
                adWhirlLayout.handleAd();
            }
        }
    }

    public static class ViewAdRunnable implements Runnable {
        private WeakReference<AdWhirlLayout> adWhirlLayoutReference;
        private ViewGroup nextView;

        public ViewAdRunnable(AdWhirlLayout adWhirlLayout, ViewGroup nextView2) {
            this.adWhirlLayoutReference = new WeakReference<>(adWhirlLayout);
            this.nextView = nextView2;
        }

        public void run() {
            AdWhirlLayout adWhirlLayout = this.adWhirlLayoutReference.get();
            if (adWhirlLayout != null) {
                adWhirlLayout.pushSubView(this.nextView);
            }
        }
    }

    private static class RotateAdRunnable implements Runnable {
        private WeakReference<AdWhirlLayout> adWhirlLayoutReference;

        public RotateAdRunnable(AdWhirlLayout adWhirlLayout) {
            this.adWhirlLayoutReference = new WeakReference<>(adWhirlLayout);
        }

        public void run() {
            AdWhirlLayout adWhirlLayout = this.adWhirlLayoutReference.get();
            if (adWhirlLayout != null) {
                adWhirlLayout.rotateAd();
            }
        }
    }

    private static class PingUrlRunnable implements Runnable {
        private String url;

        public PingUrlRunnable(String url2) {
            this.url = url2;
        }

        public void run() {
            Log.d(AdWhirlUtil.ADWHIRL, "Pinging URL: " + this.url);
            try {
                new DefaultHttpClient().execute(new HttpGet(this.url));
            } catch (ClientProtocolException e) {
                Log.e(AdWhirlUtil.ADWHIRL, "Caught ClientProtocolException in PingUrlRunnable", e);
            } catch (IOException e2) {
                Log.e(AdWhirlUtil.ADWHIRL, "Caught IOException in PingUrlRunnable", e2);
            }
        }
    }
}
