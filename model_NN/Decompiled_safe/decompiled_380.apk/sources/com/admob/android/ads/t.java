package com.admob.android.ads;

import android.content.Context;
import android.util.Log;
import com.admob.android.ads.InterstitialAd;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Locale;
import java.util.Properties;

/* compiled from: AdMobLocalizer */
public final class t implements h {
    private static t a = null;
    private static Context b = null;
    private static Thread c = null;
    private static String d = null;
    private Properties e = null;
    private Context f;

    public static void a(Context context) {
        if (b == null && context != null) {
            b = context.getApplicationContext();
        }
        if (a == null) {
            a = new t(b);
        }
    }

    public static String a(String str) {
        a(b);
        t tVar = a;
        tVar.b();
        if (tVar.e == null) {
            return str;
        }
        String property = tVar.e.getProperty(str);
        return (property == null || property.equals("")) ? str : property;
    }

    private t(Context context) {
        this.f = context;
        d = a();
        if (a != null) {
            a.e = null;
        }
        if (!b() && c == null) {
            Thread thread = new Thread(g.a("http://mm.admob.com/static/android/i18n/20101109" + "/" + d + ".properties", (String) null, AdManager.getUserId(this.f), this, 1));
            c = thread;
            thread.start();
        }
    }

    public static String a() {
        if (d == null) {
            String language = Locale.getDefault().getLanguage();
            d = language;
            if (language == null) {
                d = "en";
            }
        }
        return d;
    }

    private boolean b() {
        if (this.e == null) {
            try {
                Properties properties = new Properties();
                File a2 = a(this.f, d);
                if (a2.exists()) {
                    properties.load(new FileInputStream(a2));
                    this.e = properties;
                }
            } catch (IOException e2) {
                this.e = null;
            }
        }
        return this.e != null;
    }

    private static File a(Context context, String str) {
        File file = new File(context.getCacheDir(), "admob_cache");
        if (!file.exists()) {
            file.mkdir();
        }
        File file2 = new File(file, AdManager.SDK_VERSION_DATE);
        if (!file2.exists()) {
            file2.mkdir();
        }
        return new File(file2, str + ".properties");
    }

    public final void a(e eVar, Exception exc) {
        if (InterstitialAd.c.a(AdManager.LOG, 3)) {
            Log.d(AdManager.LOG, "Could not get localized strings from the AdMob servers.");
        }
    }

    public final void a(e eVar) {
        try {
            byte[] a2 = eVar.a();
            if (a2 != null) {
                FileOutputStream fileOutputStream = new FileOutputStream(a(this.f, d));
                fileOutputStream.write(a2);
                fileOutputStream.close();
            }
        } catch (Exception e2) {
            if (InterstitialAd.c.a(AdManager.LOG, 3)) {
                Log.d(AdManager.LOG, "Could not store localized strings to cache file.");
            }
        }
    }
}
