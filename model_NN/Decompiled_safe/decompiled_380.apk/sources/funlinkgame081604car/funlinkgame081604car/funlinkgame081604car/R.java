package funlinkgame081604car.funlinkgame081604car.funlinkgame081604car;

public final class R {

    public static final class attr {
        public static final int backgroundColor = 2130771971;
        public static final int backgroundTransparent = 2130771969;
        public static final int changeAdAnimation = 2130771968;
        public static final int isGoneWithoutAd = 2130771975;
        public static final int keywords = 2130771973;
        public static final int refreshInterval = 2130771974;
        public static final int testing = 2130771970;
        public static final int textColor = 2130771972;
    }

    public static final class color {
        public static final int hilite = 2131034112;
        public static final int light = 2131034113;
        public static final int puzzle_dark = 2131034114;
        public static final int puzzle_foreground = 2131034115;
        public static final int puzzle_hint_0 = 2131034116;
        public static final int puzzle_hint_1 = 2131034117;
        public static final int puzzle_hint_2 = 2131034118;
        public static final int puzzle_selected = 2131034119;
    }

    public static final class drawable {
        public static final int background = 2130837504;
        public static final int icon = 2130837505;
        public static final int p01 = 2130837506;
        public static final int p02 = 2130837507;
        public static final int p03 = 2130837508;
        public static final int p04 = 2130837509;
        public static final int p05 = 2130837510;
        public static final int p06 = 2130837511;
        public static final int p07 = 2130837512;
        public static final int p08 = 2130837513;
        public static final int p09 = 2130837514;
        public static final int p10 = 2130837515;
        public static final int p11 = 2130837516;
        public static final int p12 = 2130837517;
        public static final int p13 = 2130837518;
        public static final int p14 = 2130837519;
        public static final int p15 = 2130837520;
        public static final int p16 = 2130837521;
        public static final int p17 = 2130837522;
        public static final int p18 = 2130837523;
        public static final int p19 = 2130837524;
        public static final int p20 = 2130837525;
    }

    public static final class id {
        public static final int adadmob = 2131165188;
        public static final int advertising_banner_view = 2131165189;
        public static final int cv = 2131165187;
        public static final int pb = 2131165184;
        public static final int relativeLayout01 = 2131165186;
        public static final int show_remainTime = 2131165185;
    }

    public static final class layout {
        public static final int gamemainwin = 2130903040;
    }

    public static final class raw {
        public static final int soundb1o = 2130968576;
        public static final int soundb3o = 2130968577;
        public static final int soundw10o = 2130968578;
        public static final int soundw11o = 2130968579;
        public static final int soundw2o = 2130968580;
        public static final int soundw4o = 2130968581;
        public static final int soundw5o = 2130968582;
    }

    public static final class string {
        public static final int again_challenge = 2131099650;
        public static final int app_name = 2131099654;
        public static final int exit = 2131099656;
        public static final int exit_message = 2131099659;
        public static final int exit_title = 2131099658;
        public static final int failInfo = 2131099651;
        public static final int hello = 2131099648;
        public static final int newgame = 2131099655;
        public static final int next = 2131099653;
        public static final int rearrage = 2131099657;
        public static final int remain_time = 2131099649;
        public static final int str_no = 2131099661;
        public static final int str_ok = 2131099660;
        public static final int succeedInfo = 2131099652;
    }

    public static final class styleable {
        public static final int[] com_admob_android_ads_AdView = {R.attr.testing, R.attr.backgroundColor, R.attr.textColor, R.attr.keywords, R.attr.refreshInterval, R.attr.isGoneWithoutAd};
        public static final int com_admob_android_ads_AdView_backgroundColor = 1;
        public static final int com_admob_android_ads_AdView_isGoneWithoutAd = 5;
        public static final int com_admob_android_ads_AdView_keywords = 3;
        public static final int com_admob_android_ads_AdView_refreshInterval = 4;
        public static final int com_admob_android_ads_AdView_testing = 0;
        public static final int com_admob_android_ads_AdView_textColor = 2;
        public static final int[] com_wooboo_adlib_android_WoobooAdView = new int[0];
        public static final int[] net_youmi_android_AdView = {R.attr.changeAdAnimation, R.attr.backgroundTransparent};
        public static final int net_youmi_android_AdView_backgroundTransparent = 1;
        public static final int net_youmi_android_AdView_changeAdAnimation = 0;
    }
}
