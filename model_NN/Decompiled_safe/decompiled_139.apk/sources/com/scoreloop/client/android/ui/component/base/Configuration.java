package com.scoreloop.client.android.ui.component.base;

import android.content.Context;
import android.util.Log;
import com.scoreloop.client.android.core.controller.AchievementsController;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.controller.RequestControllerObserver;
import com.scoreloop.client.android.core.model.Client;
import com.scoreloop.client.android.core.model.Session;
import java.math.BigDecimal;
import java.util.IllegalFormatException;
import java.util.Properties;

public class Configuration {
    private static final String FORMAT_MONEY_KEY = "ui.format.money";
    private static final String FORMAT_SCORE_RESULT_KEY = "ui.format.score.result";
    private static final String RES_MODES_NAME = "ui.res.modes.name";
    private int _modesResId;
    private String _moneyFormat = "%.2f %s";
    private String _scoreResultFormat = "%.0f";

    static class ConfigurationException extends IllegalStateException {
        private static final String SCORELOOP_UI = "ScoreloopUI";
        private static final long serialVersionUID = 1;

        ConfigurationException(String message) {
            super(message);
            Log.e("ScoreloopUI", "=====================================================================================");
            Log.e("ScoreloopUI", "scoreloop.properties file verification error. Please resolve any issues first!");
            Log.e("ScoreloopUI", message);
        }
    }

    public enum Feature {
        ACHIEVEMENT("ui.feature.achievement", false),
        ADDRESS_BOOK("ui.feature.address_book", true),
        CHALLENGE("ui.feature.challenge", false),
        NEWS("ui.feature.news", false);
        
        private boolean _isEnabled = true;
        private String _propertyName;

        private Feature(String propertyName, boolean preset) {
            this._propertyName = propertyName;
            this._isEnabled = preset;
        }

        /* access modifiers changed from: package-private */
        public String getPropertyName() {
            return this._propertyName;
        }

        /* access modifiers changed from: package-private */
        public boolean isEnabled() {
            return this._isEnabled;
        }

        /* access modifiers changed from: package-private */
        public void setEnabled(boolean value) {
            this._isEnabled = value;
        }
    }

    public Configuration(Context context, Session session) {
        Properties properties = Client.getProperties(context);
        Feature[] features = Feature.values();
        for (Feature feature : features) {
            String property = feature.getPropertyName();
            String value = properties.getProperty(property);
            if (value != null) {
                feature.setEnabled(verifyBooleanProperty(value.trim(), property));
            }
        }
        this._scoreResultFormat = properties.getProperty(FORMAT_SCORE_RESULT_KEY, this._scoreResultFormat).trim();
        this._moneyFormat = properties.getProperty(FORMAT_MONEY_KEY, this._moneyFormat).trim();
        String modesResName = properties.getProperty(RES_MODES_NAME);
        if (modesResName != null) {
            this._modesResId = context.getResources().getIdentifier(modesResName.trim(), "array", context.getPackageName());
        }
        verifyConfiguration(context, session);
    }

    public int getModesResId() {
        return this._modesResId;
    }

    public String getMoneyFormat() {
        return this._moneyFormat;
    }

    public String getScoreResultFormat() {
        return this._scoreResultFormat;
    }

    public boolean isFeatureEnabled(Feature feature) {
        return feature.isEnabled();
    }

    private boolean verifyBooleanProperty(String value, String property) {
        if (value.equalsIgnoreCase("false")) {
            return false;
        }
        if (value.equalsIgnoreCase("true")) {
            return true;
        }
        throw new ConfigurationException("property " + property + " must be either 'true' or 'false'");
    }

    /* access modifiers changed from: protected */
    public void verifyConfiguration(Context context, Session session) {
        if (!Feature.access$2(Feature.ACHIEVEMENT) || new AchievementsController(new RequestControllerObserver() {
            public void requestControllerDidFail(RequestController aRequestController, Exception anException) {
            }

            public void requestControllerDidReceiveResponse(RequestController aRequestController) {
            }
        }).getAwardList() != null) {
            int modeCount = session.getGame().getModeCount().intValue();
            if (modeCount > 1) {
                if (this._modesResId == 0) {
                    throw new ConfigurationException("when your game has modes, you have to provide the following property: ui.res.modes.name");
                }
                String[] modeStrings = context.getResources().getStringArray(this._modesResId);
                if (modeStrings == null || modeStrings.length != modeCount) {
                    throw new ConfigurationException("your modes string array must have exactily " + modeCount + " entries!");
                }
            }
            try {
                String.format(this._moneyFormat, BigDecimal.ONE, "$");
                try {
                    String.format(this._scoreResultFormat, Double.valueOf(1.0d));
                } catch (IllegalFormatException e) {
                    throw new ConfigurationException("invalid ui.format.score.result value: must contain one valid %f specifier. " + e.getLocalizedMessage());
                }
            } catch (IllegalFormatException e2) {
                throw new ConfigurationException("invalid ui.format.money value: must contain valid %f and %s specifiers in that order. " + e2.getLocalizedMessage());
            }
        } else {
            throw new ConfigurationException("when you enable the achievement feature you also have to provide an SLAwards.bundle in the assets folder");
        }
    }
}
