package com.scoreloop.client.android.ui.framework;

import android.content.Intent;

public class ActivityDescription {
    public static final String EXTRA_IDENTIFIER = "activityIdentifier";
    private static int _instanceCounter = 0;
    private final ValueStore _arguments = new ValueStore();
    private boolean _enabledWantsClearTop = true;
    private final String _identifier;
    private final Intent _intent;
    private final int _tabId;
    private boolean _wantsClearTop = false;

    ActivityDescription(int tabId, Intent intent) {
        StringBuilder sb = new StringBuilder("pane-");
        int i = _instanceCounter + 1;
        _instanceCounter = i;
        this._identifier = sb.append(i).toString();
        this._tabId = tabId;
        this._intent = intent;
    }

    public ValueStore getArguments() {
        return this._arguments;
    }

    public String getIdentifier() {
        return this._identifier;
    }

    public Intent getIntent() {
        this._intent.putExtra(EXTRA_IDENTIFIER, this._identifier);
        if (!this._enabledWantsClearTop || !this._wantsClearTop) {
            this._intent.setFlags(-67108865 & this._intent.getFlags());
        } else {
            this._intent.addFlags(67108864);
        }
        return this._intent;
    }

    public int getTabId() {
        return this._tabId;
    }

    /* access modifiers changed from: package-private */
    public boolean hasIdentfier(String paneId) {
        return this._identifier.equals(paneId);
    }

    /* access modifiers changed from: package-private */
    public void setEnabledWantsClearTop(boolean enabled) {
        this._enabledWantsClearTop = enabled;
    }

    /* access modifiers changed from: package-private */
    public void setWantsClearTop(boolean flag) {
        this._wantsClearTop = flag;
    }
}
