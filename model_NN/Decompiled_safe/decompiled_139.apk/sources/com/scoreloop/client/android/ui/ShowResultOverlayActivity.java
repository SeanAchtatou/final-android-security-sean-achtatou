package com.scoreloop.client.android.ui;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.scoreloop.client.android.core.model.Challenge;
import com.scoreloop.client.android.core.model.Session;
import com.skyd.bestpuzzle.n1672.R;

public class ShowResultOverlayActivity extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.sl_result);
        StandardScoreloopManager manager = StandardScoreloopManager.getFactory(ScoreloopManagerSingleton.get());
        String text = "";
        switch (manager.getLastStatus()) {
            case 1:
                text = getResources().getString(R.string.sl_status_success_score);
                break;
            case 2:
                Challenge challenge = manager.getLastChallenge();
                if (!challenge.isOpen() && !challenge.isAssigned()) {
                    if (challenge.isComplete()) {
                        if (!Session.getCurrentSession().getUser().equals(challenge.getWinner())) {
                            text = getResources().getString(R.string.sl_status_success_challenge_lost);
                            break;
                        } else {
                            text = getResources().getString(R.string.sl_status_success_challenge_won);
                            break;
                        }
                    }
                } else {
                    text = getResources().getString(R.string.sl_status_success_challenge_created);
                    break;
                }
                break;
            case 3:
                text = getResources().getString(R.string.sl_status_error_network);
                break;
            case 4:
                text = getResources().getString(R.string.sl_status_error_balance);
                break;
            default:
                throw new IllegalStateException("this should not happen - make sure to start ShowResultOverlayActivity only after onScoreSubmit() was called");
        }
        ((TextView) findViewById(R.id.sl_text)).setText(text);
        ((Button) findViewById(R.id.sl_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ShowResultOverlayActivity.this.finish();
            }
        });
    }
}
