package com.scoreloop.client.android.ui;

import android.os.Bundle;
import com.scoreloop.client.android.ui.framework.ScreenActivity;

public class SocialMarketScreenActivity extends ScreenActivity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        display(StandardScoreloopManager.getFactory(ScoreloopManagerSingleton.get()).createMarketScreenDescription(null), savedInstanceState);
    }
}
