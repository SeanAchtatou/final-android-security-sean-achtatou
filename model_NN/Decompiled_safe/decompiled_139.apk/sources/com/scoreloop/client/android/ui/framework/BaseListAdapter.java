package com.scoreloop.client.android.ui.framework;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import com.scoreloop.client.android.ui.framework.BaseListItem;

public class BaseListAdapter<T extends BaseListItem> extends ArrayAdapter<T> {
    private static int _viewTypeCount = 1;
    protected OnListItemClickListener<T> _listItemClickListener;

    public interface OnListItemClickListener<T extends BaseListItem> {
        void onListItemClick(BaseListItem baseListItem);
    }

    public static void setViewTypeCount(int count) {
        _viewTypeCount = count;
    }

    public BaseListAdapter(Context context) {
        super(context, 0);
    }

    public int getItemViewType(int position) {
        return ((BaseListItem) getItem(position)).getType();
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        return ((BaseListItem) getItem(position)).getView(convertView, parent);
    }

    public int getViewTypeCount() {
        return _viewTypeCount;
    }

    public boolean isEnabled(int position) {
        return ((BaseListItem) getItem(position)).isEnabled();
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        if (this._listItemClickListener != null) {
            this._listItemClickListener.onListItemClick((BaseListItem) getItem(position));
        }
    }

    public void setOnListItemClickListener(OnListItemClickListener<T> listener) {
        this._listItemClickListener = listener;
    }
}
