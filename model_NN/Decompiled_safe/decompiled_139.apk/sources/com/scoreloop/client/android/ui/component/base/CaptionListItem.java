package com.scoreloop.client.android.ui.component.base;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.scoreloop.client.android.ui.framework.BaseListItem;
import com.skyd.bestpuzzle.n1672.R;

public class CaptionListItem extends BaseListItem {
    public CaptionListItem(Context context, Drawable drawable, String title) {
        super(context, drawable, title);
    }

    public int getType() {
        return 2;
    }

    public View getView(View view, ViewGroup parent) {
        if (view == null) {
            view = getLayoutInflater().inflate((int) R.layout.sl_list_item_caption, (ViewGroup) null);
        }
        ((TextView) view.findViewById(R.id.sl_list_item_caption_title)).setText(getTitle());
        return view;
    }

    public boolean isEnabled() {
        return false;
    }
}
