package com.scoreloop.client.android.ui.component.agent;

import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.controller.UserController;
import com.scoreloop.client.android.core.model.Entity;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.ui.component.base.Constant;
import com.scoreloop.client.android.ui.framework.ValueStore;

public class UserDetailsAgent extends BaseAgent {
    public static final String[] SUPPORTED_KEYS = {Constant.NUMBER_CHALLENGES_WON, Constant.NUMBER_CHALLENGES_PLAYED};
    private UserController _userController;

    public UserDetailsAgent() {
        super(SUPPORTED_KEYS);
    }

    /* access modifiers changed from: protected */
    public void onFinishRetrieve(RequestController aRequestController, ValueStore valueStore) {
        User.Details details = this._userController.getUser().getDetail();
        Integer numberWon = details.getChallengesWon();
        valueStore.putValue(Constant.NUMBER_CHALLENGES_PLAYED, Integer.valueOf(numberWon.intValue() + details.getChallengesLost().intValue()));
        valueStore.putValue(Constant.NUMBER_CHALLENGES_WON, numberWon);
    }

    /* access modifiers changed from: protected */
    public void onStartRetrieve(ValueStore valueStore) {
        this._userController = new UserController(this);
        this._userController.setUser((Entity) valueStore.getValue(Constant.USER));
        this._userController.loadUserDetail();
    }
}
