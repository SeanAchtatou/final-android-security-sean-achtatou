package com.skyd.core.android;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.view.Menu;
import android.view.MenuItem;

public class CommonActivity extends BaseActivity {
    int openUrlDialogUrlRes;
    int setGetFullVersionMenuUrlResID;
    int setMoreAppMenuAuthorNameResID;
    int setViewDemoMenuUrlResID;

    public void setIsRegistered(String sharedPreferencesKey, String valueKey, boolean value) {
        Android.getSharedPreferences(this, sharedPreferencesKey, 1).edit().putBoolean(valueKey, value).commit();
    }

    public Boolean getIsRegistered(String targetPackage, String sharedPreferencesKey, String valueKey) {
        try {
            return Boolean.valueOf(createPackageContext(targetPackage, 0).getSharedPreferences(sharedPreferencesKey, 0).getBoolean(valueKey, false));
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public void openUrlDialog(int titleResID, int msgResID, int yesResID, int noResID, int urlResID) {
        this.openUrlDialogUrlRes = urlResID;
        new AlertDialog.Builder(this).setIcon(17301514).setTitle(titleResID).setMessage(msgResID).setPositiveButton(yesResID, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Android.openUrl(CommonActivity.this, CommonActivity.this.openUrlDialogUrlRes);
            }
        }).setNegativeButton(noResID, (DialogInterface.OnClickListener) null).create().show();
    }

    public void setViewDemoMenu(Menu menu, int order, String title, int urlResID) {
        this.setViewDemoMenuUrlResID = urlResID;
        menu.add(0, 2147483646, order, title);
    }

    public void setViewDemoMenu(Menu menu, int order, int titleResID, int urlResID) {
        this.setViewDemoMenuUrlResID = urlResID;
        menu.add(0, 2147483646, order, titleResID);
    }

    public void setMoreAppMenu(Menu menu, int order, int titleResID, int authorNameResID) {
        this.setMoreAppMenuAuthorNameResID = authorNameResID;
        menu.add(0, 2147483645, order, titleResID);
    }

    public void setMoreAppMenu(Menu menu, int order, String title, int authorNameResID) {
        this.setMoreAppMenuAuthorNameResID = authorNameResID;
        menu.add(0, 2147483645, order, title);
    }

    public void setGetFullVersionMenu(Menu menu, int order, String title, int urlResID) {
        this.setGetFullVersionMenuUrlResID = urlResID;
        menu.add(0, 2147483644, order, title);
    }

    public void setGetFullVersionMenu(Menu menu, int order, int titleResID, int urlResID) {
        this.setGetFullVersionMenuUrlResID = urlResID;
        menu.add(0, 2147483644, order, titleResID);
    }

    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        int id = item.getItemId();
        if (id == 2147483646) {
            Android.openUrl(this, this.setViewDemoMenuUrlResID);
        } else if (id == 2147483645) {
            Android.openUrl(this, "market://search?q=pub:" + getResources().getString(this.setMoreAppMenuAuthorNameResID));
        } else if (id == 2147483644) {
            Android.openUrl(this, this.setGetFullVersionMenuUrlResID);
        }
        return super.onMenuItemSelected(featureId, item);
    }

    public void openActivity(Class<? extends Activity> targetclass) {
        openActivity(new Intent(), targetclass);
    }

    public void openActivity(Intent intent, Class<? extends Activity> targetclass) {
        intent.setClass(this, targetclass);
        startActivity(intent);
    }
}
