package com.skyd.core.android.game;

public class GameRotationStepMotion extends GameNumberStepMotion<Float> {
    public GameRotationStepMotion(Float targetValue, Float stepLength, Float tolerance) {
        super(targetValue, stepLength, tolerance);
    }

    /* access modifiers changed from: protected */
    public Float getCurrentValue(GameObject obj) {
        return Float.valueOf(obj.getRotation());
    }

    /* access modifiers changed from: protected */
    public Float plus(Float v1, Float v2) {
        return Float.valueOf(v1.floatValue() + v2.floatValue());
    }

    /* access modifiers changed from: protected */
    public void setCurrentValue(GameObject obj, Float value) {
        obj.setRotation(value.floatValue());
    }
}
