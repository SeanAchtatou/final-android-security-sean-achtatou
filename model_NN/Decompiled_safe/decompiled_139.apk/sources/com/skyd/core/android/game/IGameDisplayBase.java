package com.skyd.core.android.game;

public interface IGameDisplayBase {
    int getFPS();

    Boolean getIsFirstShow();

    Boolean getIsInitialized();

    boolean getRunnable();

    void init(Boolean bool);

    void setFPS(int i);

    void setIsInitialized(Boolean bool);

    void setRunnable(boolean z);
}
