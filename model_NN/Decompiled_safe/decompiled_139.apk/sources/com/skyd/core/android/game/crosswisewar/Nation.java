package com.skyd.core.android.game.crosswisewar;

import com.skyd.core.game.crosswisewar.IBase;
import com.skyd.core.game.crosswisewar.INation;

public abstract class Nation implements INation {
    private IBase _Base = null;
    private int _Mana = 0;
    private int _Money = 0;

    public IBase getBase() {
        return this._Base;
    }

    public void setBase(IBase value) {
        this._Base = value;
    }

    public void setBaseToDefault() {
        setBase(null);
    }

    public int getMana() {
        return this._Mana;
    }

    public void setMana(int value) {
        this._Mana = value;
    }

    public void setManaToDefault() {
        setMana(0);
    }

    public int getMoney() {
        return this._Money;
    }

    public void setMoney(int value) {
        this._Money = value;
    }

    public void setMoneyToDefault() {
        setMoney(0);
    }
}
