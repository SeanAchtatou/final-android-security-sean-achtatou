package com.skyd.core.android.game;

public interface IGameReadonlyValueDuct<TargetObjectClass, ValueClass> {
    ValueClass getValueFrom(TargetObjectClass targetobjectclass);
}
