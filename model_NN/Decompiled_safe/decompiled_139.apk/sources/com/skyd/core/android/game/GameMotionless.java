package com.skyd.core.android.game;

public class GameMotionless extends GameMotion {
    public GameMotionless(int duration) {
        super(duration);
    }

    /* access modifiers changed from: protected */
    public void restart(GameObject obj) {
    }

    /* access modifiers changed from: protected */
    public void updateSelf(GameObject obj) {
    }

    /* access modifiers changed from: protected */
    public void updateTarget(GameObject obj) {
    }
}
