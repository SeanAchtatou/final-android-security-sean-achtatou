package com.skyd.core.vector;

import android.graphics.Rect;
import android.graphics.RectF;

public class VectorRect2DF {
    private Vector2DF _Position = null;
    private Vector2DF _Size = null;

    public VectorRect2DF(Vector2DF position, Vector2DF size) {
        setPosition(position);
        setSize(size);
    }

    public VectorRect2DF(float left, float top, float right, float bottom) {
        setPosition(new Vector2DF(left, top));
        setSize(new Vector2DF(right - left, bottom - top));
    }

    public VectorRect2DF(RectF rect) {
        setPosition(new Vector2DF(rect.left, rect.top));
        setSize(new Vector2DF(rect.right - rect.left, rect.bottom - rect.top));
    }

    public Vector2DF getPosition() {
        return this._Position;
    }

    public void setPosition(Vector2DF value) {
        this._Position = value;
    }

    public void setPositionToDefault() {
        setPosition(null);
    }

    public Vector2DF getSize() {
        return this._Size;
    }

    public void setSize(Vector2DF value) {
        this._Size = value;
    }

    public void setSizeToDefault() {
        setSize(null);
    }

    public float getLeft() {
        return getPosition().getX();
    }

    public float getTop() {
        return getPosition().getY();
    }

    public float getRight() {
        return getPosition().getX() + getSize().getX();
    }

    public float getBottom() {
        return getPosition().getY() + getSize().getY();
    }

    public float getWidth() {
        return getSize().getX();
    }

    public float getHeight() {
        return getSize().getY();
    }

    public RectF getRectF() {
        return new RectF(getLeft(), getTop(), getRight(), getBottom());
    }

    public RectF getFixedRectF() {
        Vector2DF pointa = this._Position;
        Vector2DF pointb = this._Position.plusNew(this._Size);
        return new RectF(Math.min(pointa.getX(), pointb.getX()), Math.min(pointa.getY(), pointb.getY()), Math.max(pointa.getX(), pointb.getX()), Math.max(pointa.getY(), pointb.getY()));
    }

    public Rect getRect() {
        return new Rect(Math.round(getLeft()), Math.round(getTop()), Math.round(getRight()), Math.round(getBottom()));
    }

    public VectorRect2DF getIntersectRect(VectorRect2DF target) {
        float px = Math.max(getLeft(), target.getLeft());
        float py = Math.max(getTop(), target.getTop());
        float sx = Math.min(getRight(), target.getRight());
        float sy = Math.min(getBottom(), target.getBottom());
        if (px > sx || py > sy) {
            return null;
        }
        return new VectorRect2DF(px, py, sx, sy);
    }

    public VectorRect2DF getCombinationRect(VectorRect2DF target) {
        return new VectorRect2DF(Math.min(getLeft(), target.getLeft()), Math.min(getTop(), target.getTop()), Math.max(getRight(), target.getRight()), Math.max(getBottom(), target.getBottom()));
    }
}
