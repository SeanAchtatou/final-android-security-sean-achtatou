package com.skyd.bestpuzzle;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.widget.Toast;
import com.scoreloop.client.android.core.model.Score;
import com.scoreloop.client.android.ui.ScoreloopManagerSingleton;
import com.skyd.bestpuzzle.n1672.R;
import com.skyd.core.android.game.GameImageSpirit;
import com.skyd.core.android.game.GameMaster;
import com.skyd.core.android.game.GameObject;
import com.skyd.core.android.game.GameScene;
import com.skyd.core.android.game.GameSpirit;
import com.skyd.core.draw.DrawHelper;
import com.skyd.core.vector.Vector2DF;
import com.skyd.core.vector.VectorRect2DF;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

public class PuzzleScene extends GameScene {
    public Controller Controller;
    public Desktop Desktop;
    boolean IsDrawingPuzzle;
    boolean IsDrawnSelectRect;
    public GameImageSpirit MoveButton;
    public UI MoveButtonSlot;
    public OriginalImage OriginalImage;
    public long PastTime;
    public long StartTime;
    public UI SubmitScoreButton;
    /* access modifiers changed from: private */
    public RectF SubmitScoreButtonRect;
    public GameSpirit Time;
    public UI ViewFullButton;
    /* access modifiers changed from: private */
    public RectF ViewFullButtonRect;
    public UI ZoomInButton;
    public UI ZoomOutButton;
    private boolean _IsFinished = false;
    private ArrayList<OnIsFinishedChangedListener> _IsFinishedChangedListenerList = null;
    long savetime = Long.MIN_VALUE;
    RectF selectRect;

    public interface OnIsFinishedChangedListener {
        void OnIsFinishedChangedEvent(Object obj, boolean z);
    }

    public PuzzleScene() {
        setMaxDrawCacheLayer(25.0f);
        this.StartTime = new Date().getTime();
        loadGameState();
    }

    public void loadGameState() {
        Center c = (Center) GameMaster.getContext().getApplicationContext();
        this.PastTime = c.getPastTime().longValue();
        this._IsFinished = c.getIsFinished().booleanValue();
    }

    public void saveGameState() {
        Center c = (Center) GameMaster.getContext().getApplicationContext();
        if (getIsFinished()) {
            c.setPastTime(Long.valueOf(this.PastTime));
        } else {
            c.setPastTime(Long.valueOf(this.PastTime + (new Date().getTime() - this.StartTime)));
        }
        c.setIsFinished(Boolean.valueOf(getIsFinished()));
    }

    public void setFinished() {
        if (!getIsFinished()) {
            this.SubmitScoreButton.show();
        }
        setIsFinished(true);
        this.PastTime += new Date().getTime() - this.StartTime;
    }

    public boolean getIsFinished() {
        return this._IsFinished;
    }

    private void setIsFinished(boolean value) {
        onIsFinishedChanged(value);
        this._IsFinished = value;
    }

    private void setIsFinishedToDefault() {
        setIsFinished(false);
    }

    public boolean addOnIsFinishedChangedListener(OnIsFinishedChangedListener listener) {
        if (this._IsFinishedChangedListenerList == null) {
            this._IsFinishedChangedListenerList = new ArrayList<>();
        } else if (this._IsFinishedChangedListenerList.contains(listener)) {
            return false;
        }
        this._IsFinishedChangedListenerList.add(listener);
        return true;
    }

    public boolean removeOnIsFinishedChangedListener(OnIsFinishedChangedListener listener) {
        if (this._IsFinishedChangedListenerList == null || !this._IsFinishedChangedListenerList.contains(listener)) {
            return false;
        }
        this._IsFinishedChangedListenerList.remove(listener);
        return true;
    }

    public void clearOnIsFinishedChangedListeners() {
        if (this._IsFinishedChangedListenerList != null) {
            this._IsFinishedChangedListenerList.clear();
        }
    }

    /* access modifiers changed from: protected */
    public void onIsFinishedChanged(boolean newValue) {
        if (this._IsFinishedChangedListenerList != null) {
            Iterator<OnIsFinishedChangedListener> it = this._IsFinishedChangedListenerList.iterator();
            while (it.hasNext()) {
                it.next().OnIsFinishedChangedEvent(this, newValue);
            }
        }
    }

    public int getTargetCacheID() {
        return 1;
    }

    /* access modifiers changed from: protected */
    public void drawChilds(Canvas c, Rect drawArea) {
        this.IsDrawingPuzzle = false;
        this.IsDrawnSelectRect = false;
        super.drawChilds(c, drawArea);
    }

    /* access modifiers changed from: protected */
    public boolean onDrawingChild(GameObject child, Canvas c, Rect drawArea) {
        float lvl = ((GameSpirit) child).getLevel();
        if (!this.IsDrawingPuzzle && lvl <= 10.0f) {
            this.IsDrawingPuzzle = true;
            c.setMatrix(this.Desktop.getMatrix());
        } else if (lvl > 10.0f && this.IsDrawingPuzzle) {
            this.IsDrawingPuzzle = false;
            c.setMatrix(new Matrix());
        }
        if (lvl > 25.0f && this.selectRect != null && !this.IsDrawnSelectRect) {
            this.IsDrawnSelectRect = true;
            Paint p = new Paint();
            p.setARGB(100, 180, 225, 255);
            c.drawRect(this.selectRect, p);
        }
        return super.onDrawingChild(child, c, drawArea);
    }

    public void load(Context c) {
        loadOriginalImage(c);
        loadDesktop(c);
        loadInterface(c);
        loadPuzzle(c);
        loadPuzzleState();
        this.Desktop.updateCurrentObserveAreaRectF();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.skyd.core.draw.DrawHelper.calculateScaleSize(float, float, float, float, boolean):com.skyd.core.vector.Vector2DF
     arg types: [int, int, float, float, int]
     candidates:
      com.skyd.core.draw.DrawHelper.calculateScaleSize(double, double, double, double, boolean):com.skyd.core.vector.Vector2D
      com.skyd.core.draw.DrawHelper.calculateScaleSize(float, float, float, float, boolean):com.skyd.core.vector.Vector2DF */
    public void loadOriginalImage(Context c) {
        this.OriginalImage = new OriginalImage() {
            /* access modifiers changed from: protected */
            public boolean onDrawing(Canvas c, Rect drawArea) {
                c.drawARGB(180, 0, 0, 0);
                return super.onDrawing(c, drawArea);
            }
        };
        this.OriginalImage.setLevel(50.0f);
        this.OriginalImage.setName("OriginalImage");
        this.OriginalImage.getSize().resetWith(DrawHelper.calculateScaleSize(320.0f, 240.0f, (float) (GameMaster.getScreenWidth() - 40), (float) (GameMaster.getScreenHeight() - 40), true));
        this.OriginalImage.setTotalSeparateColumn(8);
        this.OriginalImage.setTotalSeparateRow(6);
        this.OriginalImage.getOriginalSize().reset(800.0f, 600.0f);
        this.OriginalImage.hide();
        this.OriginalImage.setIsUseAbsolutePosition(true);
        this.OriginalImage.setIsUseAbsoluteSize(true);
        this.OriginalImage.getImage().setIsUseAbsolutePosition(true);
        this.OriginalImage.getImage().setIsUseAbsoluteSize(true);
        this.OriginalImage.getPosition().reset(((float) (GameMaster.getScreenWidth() / 2)) - (this.OriginalImage.getSize().getX() / 2.0f), ((float) (GameMaster.getScreenHeight() / 2)) - (this.OriginalImage.getSize().getY() / 2.0f));
        getSpiritList().add(this.OriginalImage);
    }

    public void loadDesktop(Context c) {
        this.Desktop = new Desktop();
        this.Desktop.setLevel(-9999.0f);
        this.Desktop.setName("Desktop");
        this.Desktop.getSize().reset(1600.0f, 1200.0f);
        this.Desktop.getCurrentObservePosition().reset(800.0f, 600.0f);
        this.Desktop.setOriginalImage(this.OriginalImage);
        this.Desktop.setIsUseAbsolutePosition(true);
        this.Desktop.setIsUseAbsoluteSize(true);
        this.Desktop.ColorAPaint = new Paint();
        this.Desktop.ColorAPaint.setColor(Color.argb(255, 32, 30, 43));
        this.Desktop.ColorBPaint = new Paint();
        this.Desktop.ColorBPaint.setColor(Color.argb(255, 26, 24, 37));
        getSpiritList().add(this.Desktop);
    }

    public void loadPuzzle(Context c) {
        this.Controller = new Controller();
        this.Controller.setLevel(10.0f);
        this.Controller.setName("Controller");
        this.Controller.setDesktop(this.Desktop);
        this.Controller.setIsUseAbsolutePosition(true);
        this.Controller.setIsUseAbsoluteSize(true);
        getSpiritList().add(this.Controller);
        Puzzle.getRealitySize().reset(100.0f, 100.0f);
        Puzzle.getMaxRadius().reset(76.66666f, 76.66666f);
        c1796319979(c);
        c1406110428(c);
        c61509587(c);
        c1222309312(c);
        c131847975(c);
        c1839764557(c);
        c266356599(c);
        c1087616773(c);
        c270975088(c);
        c324514135(c);
        c941126592(c);
        c1403772162(c);
        c1842291423(c);
        c1672876109(c);
        c1916936305(c);
        c1634497233(c);
        c1282863324(c);
        c886908192(c);
        c169907163(c);
        c1858273219(c);
        c1899137468(c);
        c400987088(c);
        c1361130064(c);
        c1043421245(c);
        c1596068033(c);
        c936835558(c);
        c284125710(c);
        c571702272(c);
        c1752727094(c);
        c798806197(c);
        c775124271(c);
        c681132208(c);
        c928291381(c);
        c1656769618(c);
        c1044734777(c);
        c1644878244(c);
        c1816556930(c);
        c268814011(c);
        c72983769(c);
        c909185754(c);
        c147288878(c);
        c1871956295(c);
        c1625653041(c);
        c1381966028(c);
        c1206277634(c);
        c645839239(c);
        c1842559882(c);
        c1738048464(c);
    }

    public void loadInterface(Context c) {
        this.MoveButton = new GameImageSpirit();
        this.MoveButton.setLevel(22.0f);
        this.MoveButton.getImage().loadImageFromResource(c, R.drawable.movebutton);
        this.MoveButton.getSize().reset(155.0f, 155.0f);
        this.MoveButton.setIsUseAbsolutePosition(true);
        getSpiritList().add(this.MoveButton);
        final Vector2DF mvds = this.MoveButton.getDisplaySize();
        mvds.scale(0.5f);
        this.MoveButton.getPosition().reset(5.0f + mvds.getX(), ((float) (GameMaster.getScreenHeight() - 5)) - mvds.getY());
        this.MoveButton.getPositionOffset().resetWith(mvds.negateNew());
        this.MoveButtonSlot = new UI() {
            Vector2DF movevector;
            boolean needmove;

            public void executive(Vector2DF point) {
                this.movevector = point.minusNew(getPosition()).restrainLength(mvds.getX() * 0.4f);
                this.needmove = true;
            }

            /* access modifiers changed from: protected */
            public void updateSelf() {
                if (this.needmove) {
                    PuzzleScene.this.MoveButton.getPosition().resetWith(getPosition().plusNew(this.movevector));
                    PuzzleScene.this.Desktop.getCurrentObservePosition().plus(this.movevector);
                    PuzzleScene.this.refreshDrawCacheBitmap();
                }
                super.updateSelf();
            }

            public boolean isInArea(Vector2DF point) {
                return point.minusNew(getPosition()).getLength() < mvds.getX() + 5.0f;
            }

            public void reset() {
                this.needmove = false;
                PuzzleScene.this.MoveButton.getPosition().resetWith(getPosition());
                PuzzleScene.this.refreshDrawCacheBitmap();
            }
        };
        this.MoveButtonSlot.setLevel(21.0f);
        this.MoveButtonSlot.getImage().loadImageFromResource(c, R.drawable.movebuttonslot);
        this.MoveButtonSlot.getSize().reset(155.0f, 155.0f);
        this.MoveButtonSlot.setIsUseAbsolutePosition(true);
        getSpiritList().add(this.MoveButtonSlot);
        this.MoveButtonSlot.getPosition().resetWith(this.MoveButton.getPosition());
        this.MoveButtonSlot.getPositionOffset().resetWith(this.MoveButton.getPositionOffset());
        this.ZoomInButton = new UI() {
            boolean zoom;

            public void executive(Vector2DF point) {
                this.zoom = true;
            }

            public boolean isInArea(Vector2DF point) {
                return point.minusNew(getPosition()).getLength() < getDisplaySize().getX() / 2.0f;
            }

            public void reset() {
                this.zoom = false;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{java.lang.Math.min(float, float):float}
             arg types: [int, float]
             candidates:
              ClspMth{java.lang.Math.min(double, double):double}
              ClspMth{java.lang.Math.min(long, long):long}
              ClspMth{java.lang.Math.min(int, int):int}
              ClspMth{java.lang.Math.min(float, float):float} */
            /* access modifiers changed from: protected */
            public void updateSelf() {
                if (this.zoom) {
                    PuzzleScene.this.Desktop.setZoom(Math.min(1.3f, PuzzleScene.this.Desktop.getZoom() * 1.05f));
                    PuzzleScene.this.refreshDrawCacheBitmap();
                }
                super.updateSelf();
            }
        };
        this.ZoomInButton.setLevel(21.0f);
        this.ZoomInButton.getImage().loadImageFromResource(c, R.drawable.zoombutton1);
        this.ZoomInButton.getSize().reset(77.0f, 77.0f);
        this.ZoomInButton.setIsUseAbsolutePosition(true);
        getSpiritList().add(this.ZoomInButton);
        Vector2DF zibds = this.ZoomInButton.getDisplaySize().scale(0.5f);
        this.ZoomInButton.getPosition().reset(((float) (GameMaster.getScreenWidth() - 50)) - zibds.getX(), ((float) (GameMaster.getScreenHeight() - 10)) - zibds.getY());
        this.ZoomInButton.getPositionOffset().resetWith(zibds.negateNew());
        this.ZoomOutButton = new UI() {
            boolean zoom;

            public void executive(Vector2DF point) {
                this.zoom = true;
            }

            public boolean isInArea(Vector2DF point) {
                return point.minusNew(getPosition()).getLength() < getDisplaySize().getX() / 2.0f;
            }

            public void reset() {
                this.zoom = false;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{java.lang.Math.max(float, float):float}
             arg types: [int, float]
             candidates:
              ClspMth{java.lang.Math.max(double, double):double}
              ClspMth{java.lang.Math.max(int, int):int}
              ClspMth{java.lang.Math.max(long, long):long}
              ClspMth{java.lang.Math.max(float, float):float} */
            /* access modifiers changed from: protected */
            public void updateSelf() {
                if (this.zoom) {
                    PuzzleScene.this.Desktop.setZoom(Math.max(0.3f, PuzzleScene.this.Desktop.getZoom() * 0.95f));
                    PuzzleScene.this.refreshDrawCacheBitmap();
                    GameMaster.log(this, Float.valueOf(PuzzleScene.this.Desktop.getCurrentObserveAreaRectF().width()));
                }
                super.updateSelf();
            }
        };
        this.ZoomOutButton.setLevel(21.0f);
        this.ZoomOutButton.getImage().loadImageFromResource(c, R.drawable.zoombutton2);
        this.ZoomOutButton.getSize().reset(77.0f, 77.0f);
        this.ZoomOutButton.setIsUseAbsolutePosition(true);
        getSpiritList().add(this.ZoomOutButton);
        Vector2DF zobds = this.ZoomOutButton.getDisplaySize().scale(0.5f);
        this.ZoomOutButton.getPosition().reset((((float) (GameMaster.getScreenWidth() - 100)) - zobds.getX()) - (zibds.getX() * 2.0f), ((float) (GameMaster.getScreenHeight() - 10)) - zobds.getY());
        this.ZoomOutButton.getPositionOffset().resetWith(zobds.negateNew());
        this.ViewFullButton = new UI() {
            public void executive(Vector2DF point) {
                if (PuzzleScene.this.OriginalImage.getImage().getImage() == null) {
                    PuzzleScene.this.OriginalImage.getImage().loadImageFromResource(GameMaster.getContext(), R.drawable.oim);
                }
                PuzzleScene.this.OriginalImage.show();
            }

            public boolean isInArea(Vector2DF point) {
                return point.isIn(PuzzleScene.this.ViewFullButtonRect);
            }

            public void reset() {
                PuzzleScene.this.OriginalImage.hide();
            }
        };
        this.ViewFullButton.getImage().loadImageFromResource(c, R.drawable.viewfull);
        this.ViewFullButton.setLevel(21.0f);
        this.ViewFullButton.setIsUseAbsolutePosition(true);
        this.ViewFullButton.getSize().reset(83.0f, 38.0f);
        getSpiritList().add(this.ViewFullButton);
        this.ViewFullButton.getPosition().reset(((float) (GameMaster.getScreenWidth() - 15)) - this.ViewFullButton.getDisplaySize().getX(), 15.0f);
        this.ViewFullButtonRect = new VectorRect2DF(this.ViewFullButton.getPosition(), this.ViewFullButton.getDisplaySize()).getRectF();
        this.SubmitScoreButton = new UI() {
            public void executive(Vector2DF point) {
                Score score = new Score(Double.valueOf(Math.ceil((double) (PuzzleScene.this.PastTime / 1000))), null);
                score.setMode(21);
                ScoreloopManagerSingleton.get().onGamePlayEnded(score);
                hide();
                Toast.makeText(GameMaster.getContext(), (int) R.string.Submitting, 1).show();
            }

            public boolean isInArea(Vector2DF point) {
                return getVisibleOriginalValue() && point.isIn(PuzzleScene.this.SubmitScoreButtonRect);
            }

            public void reset() {
            }
        };
        this.SubmitScoreButton.getImage().loadImageFromResource(c, R.drawable.submitscore);
        this.SubmitScoreButton.setLevel(21.0f);
        this.SubmitScoreButton.setIsUseAbsolutePosition(true);
        this.SubmitScoreButton.getSize().reset(114.0f, 40.0f);
        getSpiritList().add(this.SubmitScoreButton);
        this.SubmitScoreButton.getPosition().reset(15.0f, 60.0f);
        this.SubmitScoreButtonRect = new VectorRect2DF(this.SubmitScoreButton.getPosition(), this.SubmitScoreButton.getDisplaySize()).getRectF();
        this.SubmitScoreButton.hide();
        this.Time = new GameSpirit() {
            DecimalFormat FMT = new DecimalFormat("00");
            Paint p1;
            Paint p2;

            public GameObject getDisplayContentChild() {
                return null;
            }

            /* access modifiers changed from: protected */
            public void drawSelf(Canvas c, Rect drawArea) {
                if (this.p1 == null) {
                    this.p1 = new Paint();
                    this.p1.setARGB(160, 255, 255, 255);
                    this.p1.setAntiAlias(true);
                    this.p1.setTextSize(28.0f);
                    this.p1.setTypeface(Typeface.createFromAsset(GameMaster.getContext().getAssets(), "fonts/font.ttf"));
                }
                if (this.p2 == null) {
                    this.p2 = new Paint();
                    this.p2.setARGB(100, 0, 0, 0);
                    this.p2.setAntiAlias(true);
                    this.p2.setTextSize(28.0f);
                    this.p2.setTypeface(Typeface.createFromAsset(GameMaster.getContext().getAssets(), "fonts/font.ttf"));
                }
                long t = PuzzleScene.this.getIsFinished() ? PuzzleScene.this.PastTime : PuzzleScene.this.PastTime + (new Date().getTime() - PuzzleScene.this.StartTime);
                long h = t / 3600000;
                long m = (t - (((1000 * h) * 60) * 60)) / 60000;
                String str = String.valueOf(this.FMT.format(h)) + ":" + this.FMT.format(m) + ":" + this.FMT.format(((t - (((1000 * h) * 60) * 60)) - ((1000 * m) * 60)) / 1000) + (PuzzleScene.this.getIsFinished() ? " Finished" : "");
                c.drawText(str, 16.0f, 44.0f, this.p2);
                c.drawText(str, 15.0f, 43.0f, this.p1);
                super.drawSelf(c, drawArea);
            }
        };
        this.Time.setLevel(40.0f);
        getSpiritList().add(this.Time);
    }

    public float getMaxPuzzleLevel() {
        float maxlvl = 0.0f;
        Iterator<Puzzle> it = this.Desktop.getPuzzleList().iterator();
        while (it.hasNext()) {
            Puzzle f = it.next();
            if (f.getLevel() <= 10.0f) {
                maxlvl = Math.max(f.getLevel(), maxlvl);
            }
        }
        return maxlvl;
    }

    public Puzzle getTouchPuzzle(Vector2DF v) {
        Vector2DF rv = this.Desktop.reMapPoint(v);
        Puzzle o = null;
        float olen = 99999.0f;
        for (int i = getSpiritList().size() - 1; i >= 0; i--) {
            if (getSpiritList().get(i) instanceof Puzzle) {
                Puzzle p = (Puzzle) getSpiritList().get(i);
                float len = p.getPositionInDesktop().minusNew(rv).getLength();
                if (len <= Puzzle.getRealitySize().getX() * 0.4f) {
                    return p;
                }
                if (len <= Puzzle.getRealitySize().getX() && olen > len) {
                    o = p;
                    olen = len;
                }
            }
        }
        return o;
    }

    public void startDrawSelectRect(Vector2DF startDragPoint, Vector2DF v) {
        this.selectRect = new VectorRect2DF(startDragPoint, v.minusNew(startDragPoint)).getFixedRectF();
    }

    public void stopDrawSelectRect() {
        this.selectRect = null;
    }

    public ArrayList<Puzzle> getSelectPuzzle(Vector2DF startDragPoint, Vector2DF v) {
        Vector2DF rs = this.Desktop.reMapPoint(startDragPoint);
        ArrayList<Puzzle> l = new ArrayList<>();
        RectF rect = new VectorRect2DF(rs, this.Desktop.reMapPoint(v).minusNew(rs)).getFixedRectF();
        float xo = Puzzle.getRealitySize().getX() / 3.0f;
        float yo = Puzzle.getRealitySize().getY() / 3.0f;
        RectF sr = new RectF(rect.left - xo, rect.top - yo, rect.right + xo, rect.bottom + yo);
        Iterator<Puzzle> it = this.Desktop.getPuzzleList().iterator();
        while (it.hasNext()) {
            Puzzle f = it.next();
            if (f.getPositionInDesktop().isIn(sr)) {
                l.add(f);
            }
        }
        return l;
    }

    public void loadPuzzleState() {
        Center c = (Center) GameMaster.getContext().getApplicationContext();
        SharedPreferences s = c.getSharedPreferences();
        Iterator<Puzzle> it = this.Desktop.getPuzzleList().iterator();
        while (it.hasNext()) {
            it.next().load(c, s);
        }
    }

    public void savePuzzleState() {
        Center c = (Center) GameMaster.getContext().getApplicationContext();
        SharedPreferences.Editor e = c.getSharedPreferences().edit();
        Iterator<Puzzle> it = this.Desktop.getPuzzleList().iterator();
        while (it.hasNext()) {
            it.next().save(c, e);
        }
        e.commit();
    }

    public void reset() {
        this.Controller.reset();
        Iterator<Puzzle> it = this.Desktop.getPuzzleList().iterator();
        while (it.hasNext()) {
            Puzzle f = it.next();
            f.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
            this.Desktop.RandomlyPlaced(f);
        }
        refreshDrawCacheBitmap();
        setIsFinishedToDefault();
        this.StartTime = new Date().getTime();
        this.PastTime = 0;
        this.SubmitScoreButton.hide();
        saveGameState();
        savePuzzleState();
    }

    /* access modifiers changed from: package-private */
    public void c1796319979(Context c) {
        Puzzle p1796319979 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1796319979(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1796319979(editor, this);
            }
        };
        p1796319979.setID(1796319979);
        p1796319979.setName("1796319979");
        p1796319979.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1796319979);
        this.Desktop.RandomlyPlaced(p1796319979);
        p1796319979.setTopEdgeType(EdgeType.Flat);
        p1796319979.setBottomEdgeType(EdgeType.Convex);
        p1796319979.setLeftEdgeType(EdgeType.Flat);
        p1796319979.setRightEdgeType(EdgeType.Convex);
        p1796319979.setTopExactPuzzleID(-1);
        p1796319979.setBottomExactPuzzleID(1406110428);
        p1796319979.setLeftExactPuzzleID(-1);
        p1796319979.setRightExactPuzzleID(266356599);
        p1796319979.getDisplayImage().loadImageFromResource(c, R.drawable.p1796319979h);
        p1796319979.setExactRow(0);
        p1796319979.setExactColumn(0);
        p1796319979.getSize().reset(126.6667f, 126.6667f);
        p1796319979.getPositionOffset().reset(-50.0f, -50.0f);
        p1796319979.setIsUseAbsolutePosition(true);
        p1796319979.setIsUseAbsoluteSize(true);
        p1796319979.getImage().setIsUseAbsolutePosition(true);
        p1796319979.getImage().setIsUseAbsoluteSize(true);
        p1796319979.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1796319979.resetPosition();
        getSpiritList().add(p1796319979);
    }

    /* access modifiers changed from: package-private */
    public void c1406110428(Context c) {
        Puzzle p1406110428 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1406110428(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1406110428(editor, this);
            }
        };
        p1406110428.setID(1406110428);
        p1406110428.setName("1406110428");
        p1406110428.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1406110428);
        this.Desktop.RandomlyPlaced(p1406110428);
        p1406110428.setTopEdgeType(EdgeType.Concave);
        p1406110428.setBottomEdgeType(EdgeType.Convex);
        p1406110428.setLeftEdgeType(EdgeType.Flat);
        p1406110428.setRightEdgeType(EdgeType.Concave);
        p1406110428.setTopExactPuzzleID(1796319979);
        p1406110428.setBottomExactPuzzleID(61509587);
        p1406110428.setLeftExactPuzzleID(-1);
        p1406110428.setRightExactPuzzleID(1087616773);
        p1406110428.getDisplayImage().loadImageFromResource(c, R.drawable.p1406110428h);
        p1406110428.setExactRow(1);
        p1406110428.setExactColumn(0);
        p1406110428.getSize().reset(100.0f, 126.6667f);
        p1406110428.getPositionOffset().reset(-50.0f, -50.0f);
        p1406110428.setIsUseAbsolutePosition(true);
        p1406110428.setIsUseAbsoluteSize(true);
        p1406110428.getImage().setIsUseAbsolutePosition(true);
        p1406110428.getImage().setIsUseAbsoluteSize(true);
        p1406110428.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1406110428.resetPosition();
        getSpiritList().add(p1406110428);
    }

    /* access modifiers changed from: package-private */
    public void c61509587(Context c) {
        Puzzle p61509587 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load61509587(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save61509587(editor, this);
            }
        };
        p61509587.setID(61509587);
        p61509587.setName("61509587");
        p61509587.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p61509587);
        this.Desktop.RandomlyPlaced(p61509587);
        p61509587.setTopEdgeType(EdgeType.Concave);
        p61509587.setBottomEdgeType(EdgeType.Convex);
        p61509587.setLeftEdgeType(EdgeType.Flat);
        p61509587.setRightEdgeType(EdgeType.Convex);
        p61509587.setTopExactPuzzleID(1406110428);
        p61509587.setBottomExactPuzzleID(1222309312);
        p61509587.setLeftExactPuzzleID(-1);
        p61509587.setRightExactPuzzleID(270975088);
        p61509587.getDisplayImage().loadImageFromResource(c, R.drawable.p61509587h);
        p61509587.setExactRow(2);
        p61509587.setExactColumn(0);
        p61509587.getSize().reset(126.6667f, 126.6667f);
        p61509587.getPositionOffset().reset(-50.0f, -50.0f);
        p61509587.setIsUseAbsolutePosition(true);
        p61509587.setIsUseAbsoluteSize(true);
        p61509587.getImage().setIsUseAbsolutePosition(true);
        p61509587.getImage().setIsUseAbsoluteSize(true);
        p61509587.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p61509587.resetPosition();
        getSpiritList().add(p61509587);
    }

    /* access modifiers changed from: package-private */
    public void c1222309312(Context c) {
        Puzzle p1222309312 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1222309312(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1222309312(editor, this);
            }
        };
        p1222309312.setID(1222309312);
        p1222309312.setName("1222309312");
        p1222309312.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1222309312);
        this.Desktop.RandomlyPlaced(p1222309312);
        p1222309312.setTopEdgeType(EdgeType.Concave);
        p1222309312.setBottomEdgeType(EdgeType.Convex);
        p1222309312.setLeftEdgeType(EdgeType.Flat);
        p1222309312.setRightEdgeType(EdgeType.Convex);
        p1222309312.setTopExactPuzzleID(61509587);
        p1222309312.setBottomExactPuzzleID(131847975);
        p1222309312.setLeftExactPuzzleID(-1);
        p1222309312.setRightExactPuzzleID(324514135);
        p1222309312.getDisplayImage().loadImageFromResource(c, R.drawable.p1222309312h);
        p1222309312.setExactRow(3);
        p1222309312.setExactColumn(0);
        p1222309312.getSize().reset(126.6667f, 126.6667f);
        p1222309312.getPositionOffset().reset(-50.0f, -50.0f);
        p1222309312.setIsUseAbsolutePosition(true);
        p1222309312.setIsUseAbsoluteSize(true);
        p1222309312.getImage().setIsUseAbsolutePosition(true);
        p1222309312.getImage().setIsUseAbsoluteSize(true);
        p1222309312.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1222309312.resetPosition();
        getSpiritList().add(p1222309312);
    }

    /* access modifiers changed from: package-private */
    public void c131847975(Context c) {
        Puzzle p131847975 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load131847975(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save131847975(editor, this);
            }
        };
        p131847975.setID(131847975);
        p131847975.setName("131847975");
        p131847975.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p131847975);
        this.Desktop.RandomlyPlaced(p131847975);
        p131847975.setTopEdgeType(EdgeType.Concave);
        p131847975.setBottomEdgeType(EdgeType.Convex);
        p131847975.setLeftEdgeType(EdgeType.Flat);
        p131847975.setRightEdgeType(EdgeType.Convex);
        p131847975.setTopExactPuzzleID(1222309312);
        p131847975.setBottomExactPuzzleID(1839764557);
        p131847975.setLeftExactPuzzleID(-1);
        p131847975.setRightExactPuzzleID(941126592);
        p131847975.getDisplayImage().loadImageFromResource(c, R.drawable.p131847975h);
        p131847975.setExactRow(4);
        p131847975.setExactColumn(0);
        p131847975.getSize().reset(126.6667f, 126.6667f);
        p131847975.getPositionOffset().reset(-50.0f, -50.0f);
        p131847975.setIsUseAbsolutePosition(true);
        p131847975.setIsUseAbsoluteSize(true);
        p131847975.getImage().setIsUseAbsolutePosition(true);
        p131847975.getImage().setIsUseAbsoluteSize(true);
        p131847975.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p131847975.resetPosition();
        getSpiritList().add(p131847975);
    }

    /* access modifiers changed from: package-private */
    public void c1839764557(Context c) {
        Puzzle p1839764557 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1839764557(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1839764557(editor, this);
            }
        };
        p1839764557.setID(1839764557);
        p1839764557.setName("1839764557");
        p1839764557.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1839764557);
        this.Desktop.RandomlyPlaced(p1839764557);
        p1839764557.setTopEdgeType(EdgeType.Concave);
        p1839764557.setBottomEdgeType(EdgeType.Flat);
        p1839764557.setLeftEdgeType(EdgeType.Flat);
        p1839764557.setRightEdgeType(EdgeType.Concave);
        p1839764557.setTopExactPuzzleID(131847975);
        p1839764557.setBottomExactPuzzleID(-1);
        p1839764557.setLeftExactPuzzleID(-1);
        p1839764557.setRightExactPuzzleID(1403772162);
        p1839764557.getDisplayImage().loadImageFromResource(c, R.drawable.p1839764557h);
        p1839764557.setExactRow(5);
        p1839764557.setExactColumn(0);
        p1839764557.getSize().reset(100.0f, 100.0f);
        p1839764557.getPositionOffset().reset(-50.0f, -50.0f);
        p1839764557.setIsUseAbsolutePosition(true);
        p1839764557.setIsUseAbsoluteSize(true);
        p1839764557.getImage().setIsUseAbsolutePosition(true);
        p1839764557.getImage().setIsUseAbsoluteSize(true);
        p1839764557.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1839764557.resetPosition();
        getSpiritList().add(p1839764557);
    }

    /* access modifiers changed from: package-private */
    public void c266356599(Context c) {
        Puzzle p266356599 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load266356599(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save266356599(editor, this);
            }
        };
        p266356599.setID(266356599);
        p266356599.setName("266356599");
        p266356599.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p266356599);
        this.Desktop.RandomlyPlaced(p266356599);
        p266356599.setTopEdgeType(EdgeType.Flat);
        p266356599.setBottomEdgeType(EdgeType.Convex);
        p266356599.setLeftEdgeType(EdgeType.Concave);
        p266356599.setRightEdgeType(EdgeType.Concave);
        p266356599.setTopExactPuzzleID(-1);
        p266356599.setBottomExactPuzzleID(1087616773);
        p266356599.setLeftExactPuzzleID(1796319979);
        p266356599.setRightExactPuzzleID(1842291423);
        p266356599.getDisplayImage().loadImageFromResource(c, R.drawable.p266356599h);
        p266356599.setExactRow(0);
        p266356599.setExactColumn(1);
        p266356599.getSize().reset(100.0f, 126.6667f);
        p266356599.getPositionOffset().reset(-50.0f, -50.0f);
        p266356599.setIsUseAbsolutePosition(true);
        p266356599.setIsUseAbsoluteSize(true);
        p266356599.getImage().setIsUseAbsolutePosition(true);
        p266356599.getImage().setIsUseAbsoluteSize(true);
        p266356599.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p266356599.resetPosition();
        getSpiritList().add(p266356599);
    }

    /* access modifiers changed from: package-private */
    public void c1087616773(Context c) {
        Puzzle p1087616773 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1087616773(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1087616773(editor, this);
            }
        };
        p1087616773.setID(1087616773);
        p1087616773.setName("1087616773");
        p1087616773.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1087616773);
        this.Desktop.RandomlyPlaced(p1087616773);
        p1087616773.setTopEdgeType(EdgeType.Concave);
        p1087616773.setBottomEdgeType(EdgeType.Convex);
        p1087616773.setLeftEdgeType(EdgeType.Convex);
        p1087616773.setRightEdgeType(EdgeType.Convex);
        p1087616773.setTopExactPuzzleID(266356599);
        p1087616773.setBottomExactPuzzleID(270975088);
        p1087616773.setLeftExactPuzzleID(1406110428);
        p1087616773.setRightExactPuzzleID(1672876109);
        p1087616773.getDisplayImage().loadImageFromResource(c, R.drawable.p1087616773h);
        p1087616773.setExactRow(1);
        p1087616773.setExactColumn(1);
        p1087616773.getSize().reset(153.3333f, 126.6667f);
        p1087616773.getPositionOffset().reset(-76.66666f, -50.0f);
        p1087616773.setIsUseAbsolutePosition(true);
        p1087616773.setIsUseAbsoluteSize(true);
        p1087616773.getImage().setIsUseAbsolutePosition(true);
        p1087616773.getImage().setIsUseAbsoluteSize(true);
        p1087616773.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1087616773.resetPosition();
        getSpiritList().add(p1087616773);
    }

    /* access modifiers changed from: package-private */
    public void c270975088(Context c) {
        Puzzle p270975088 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load270975088(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save270975088(editor, this);
            }
        };
        p270975088.setID(270975088);
        p270975088.setName("270975088");
        p270975088.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p270975088);
        this.Desktop.RandomlyPlaced(p270975088);
        p270975088.setTopEdgeType(EdgeType.Concave);
        p270975088.setBottomEdgeType(EdgeType.Convex);
        p270975088.setLeftEdgeType(EdgeType.Concave);
        p270975088.setRightEdgeType(EdgeType.Concave);
        p270975088.setTopExactPuzzleID(1087616773);
        p270975088.setBottomExactPuzzleID(324514135);
        p270975088.setLeftExactPuzzleID(61509587);
        p270975088.setRightExactPuzzleID(1916936305);
        p270975088.getDisplayImage().loadImageFromResource(c, R.drawable.p270975088h);
        p270975088.setExactRow(2);
        p270975088.setExactColumn(1);
        p270975088.getSize().reset(100.0f, 126.6667f);
        p270975088.getPositionOffset().reset(-50.0f, -50.0f);
        p270975088.setIsUseAbsolutePosition(true);
        p270975088.setIsUseAbsoluteSize(true);
        p270975088.getImage().setIsUseAbsolutePosition(true);
        p270975088.getImage().setIsUseAbsoluteSize(true);
        p270975088.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p270975088.resetPosition();
        getSpiritList().add(p270975088);
    }

    /* access modifiers changed from: package-private */
    public void c324514135(Context c) {
        Puzzle p324514135 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load324514135(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save324514135(editor, this);
            }
        };
        p324514135.setID(324514135);
        p324514135.setName("324514135");
        p324514135.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p324514135);
        this.Desktop.RandomlyPlaced(p324514135);
        p324514135.setTopEdgeType(EdgeType.Concave);
        p324514135.setBottomEdgeType(EdgeType.Convex);
        p324514135.setLeftEdgeType(EdgeType.Concave);
        p324514135.setRightEdgeType(EdgeType.Convex);
        p324514135.setTopExactPuzzleID(270975088);
        p324514135.setBottomExactPuzzleID(941126592);
        p324514135.setLeftExactPuzzleID(1222309312);
        p324514135.setRightExactPuzzleID(1634497233);
        p324514135.getDisplayImage().loadImageFromResource(c, R.drawable.p324514135h);
        p324514135.setExactRow(3);
        p324514135.setExactColumn(1);
        p324514135.getSize().reset(126.6667f, 126.6667f);
        p324514135.getPositionOffset().reset(-50.0f, -50.0f);
        p324514135.setIsUseAbsolutePosition(true);
        p324514135.setIsUseAbsoluteSize(true);
        p324514135.getImage().setIsUseAbsolutePosition(true);
        p324514135.getImage().setIsUseAbsoluteSize(true);
        p324514135.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p324514135.resetPosition();
        getSpiritList().add(p324514135);
    }

    /* access modifiers changed from: package-private */
    public void c941126592(Context c) {
        Puzzle p941126592 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load941126592(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save941126592(editor, this);
            }
        };
        p941126592.setID(941126592);
        p941126592.setName("941126592");
        p941126592.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p941126592);
        this.Desktop.RandomlyPlaced(p941126592);
        p941126592.setTopEdgeType(EdgeType.Concave);
        p941126592.setBottomEdgeType(EdgeType.Concave);
        p941126592.setLeftEdgeType(EdgeType.Concave);
        p941126592.setRightEdgeType(EdgeType.Concave);
        p941126592.setTopExactPuzzleID(324514135);
        p941126592.setBottomExactPuzzleID(1403772162);
        p941126592.setLeftExactPuzzleID(131847975);
        p941126592.setRightExactPuzzleID(1282863324);
        p941126592.getDisplayImage().loadImageFromResource(c, R.drawable.p941126592h);
        p941126592.setExactRow(4);
        p941126592.setExactColumn(1);
        p941126592.getSize().reset(100.0f, 100.0f);
        p941126592.getPositionOffset().reset(-50.0f, -50.0f);
        p941126592.setIsUseAbsolutePosition(true);
        p941126592.setIsUseAbsoluteSize(true);
        p941126592.getImage().setIsUseAbsolutePosition(true);
        p941126592.getImage().setIsUseAbsoluteSize(true);
        p941126592.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p941126592.resetPosition();
        getSpiritList().add(p941126592);
    }

    /* access modifiers changed from: package-private */
    public void c1403772162(Context c) {
        Puzzle p1403772162 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1403772162(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1403772162(editor, this);
            }
        };
        p1403772162.setID(1403772162);
        p1403772162.setName("1403772162");
        p1403772162.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1403772162);
        this.Desktop.RandomlyPlaced(p1403772162);
        p1403772162.setTopEdgeType(EdgeType.Convex);
        p1403772162.setBottomEdgeType(EdgeType.Flat);
        p1403772162.setLeftEdgeType(EdgeType.Convex);
        p1403772162.setRightEdgeType(EdgeType.Convex);
        p1403772162.setTopExactPuzzleID(941126592);
        p1403772162.setBottomExactPuzzleID(-1);
        p1403772162.setLeftExactPuzzleID(1839764557);
        p1403772162.setRightExactPuzzleID(886908192);
        p1403772162.getDisplayImage().loadImageFromResource(c, R.drawable.p1403772162h);
        p1403772162.setExactRow(5);
        p1403772162.setExactColumn(1);
        p1403772162.getSize().reset(153.3333f, 126.6667f);
        p1403772162.getPositionOffset().reset(-76.66666f, -76.66666f);
        p1403772162.setIsUseAbsolutePosition(true);
        p1403772162.setIsUseAbsoluteSize(true);
        p1403772162.getImage().setIsUseAbsolutePosition(true);
        p1403772162.getImage().setIsUseAbsoluteSize(true);
        p1403772162.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1403772162.resetPosition();
        getSpiritList().add(p1403772162);
    }

    /* access modifiers changed from: package-private */
    public void c1842291423(Context c) {
        Puzzle p1842291423 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1842291423(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1842291423(editor, this);
            }
        };
        p1842291423.setID(1842291423);
        p1842291423.setName("1842291423");
        p1842291423.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1842291423);
        this.Desktop.RandomlyPlaced(p1842291423);
        p1842291423.setTopEdgeType(EdgeType.Flat);
        p1842291423.setBottomEdgeType(EdgeType.Convex);
        p1842291423.setLeftEdgeType(EdgeType.Convex);
        p1842291423.setRightEdgeType(EdgeType.Concave);
        p1842291423.setTopExactPuzzleID(-1);
        p1842291423.setBottomExactPuzzleID(1672876109);
        p1842291423.setLeftExactPuzzleID(266356599);
        p1842291423.setRightExactPuzzleID(169907163);
        p1842291423.getDisplayImage().loadImageFromResource(c, R.drawable.p1842291423h);
        p1842291423.setExactRow(0);
        p1842291423.setExactColumn(2);
        p1842291423.getSize().reset(126.6667f, 126.6667f);
        p1842291423.getPositionOffset().reset(-76.66666f, -50.0f);
        p1842291423.setIsUseAbsolutePosition(true);
        p1842291423.setIsUseAbsoluteSize(true);
        p1842291423.getImage().setIsUseAbsolutePosition(true);
        p1842291423.getImage().setIsUseAbsoluteSize(true);
        p1842291423.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1842291423.resetPosition();
        getSpiritList().add(p1842291423);
    }

    /* access modifiers changed from: package-private */
    public void c1672876109(Context c) {
        Puzzle p1672876109 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1672876109(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1672876109(editor, this);
            }
        };
        p1672876109.setID(1672876109);
        p1672876109.setName("1672876109");
        p1672876109.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1672876109);
        this.Desktop.RandomlyPlaced(p1672876109);
        p1672876109.setTopEdgeType(EdgeType.Concave);
        p1672876109.setBottomEdgeType(EdgeType.Convex);
        p1672876109.setLeftEdgeType(EdgeType.Concave);
        p1672876109.setRightEdgeType(EdgeType.Concave);
        p1672876109.setTopExactPuzzleID(1842291423);
        p1672876109.setBottomExactPuzzleID(1916936305);
        p1672876109.setLeftExactPuzzleID(1087616773);
        p1672876109.setRightExactPuzzleID(1858273219);
        p1672876109.getDisplayImage().loadImageFromResource(c, R.drawable.p1672876109h);
        p1672876109.setExactRow(1);
        p1672876109.setExactColumn(2);
        p1672876109.getSize().reset(100.0f, 126.6667f);
        p1672876109.getPositionOffset().reset(-50.0f, -50.0f);
        p1672876109.setIsUseAbsolutePosition(true);
        p1672876109.setIsUseAbsoluteSize(true);
        p1672876109.getImage().setIsUseAbsolutePosition(true);
        p1672876109.getImage().setIsUseAbsoluteSize(true);
        p1672876109.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1672876109.resetPosition();
        getSpiritList().add(p1672876109);
    }

    /* access modifiers changed from: package-private */
    public void c1916936305(Context c) {
        Puzzle p1916936305 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1916936305(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1916936305(editor, this);
            }
        };
        p1916936305.setID(1916936305);
        p1916936305.setName("1916936305");
        p1916936305.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1916936305);
        this.Desktop.RandomlyPlaced(p1916936305);
        p1916936305.setTopEdgeType(EdgeType.Concave);
        p1916936305.setBottomEdgeType(EdgeType.Concave);
        p1916936305.setLeftEdgeType(EdgeType.Convex);
        p1916936305.setRightEdgeType(EdgeType.Concave);
        p1916936305.setTopExactPuzzleID(1672876109);
        p1916936305.setBottomExactPuzzleID(1634497233);
        p1916936305.setLeftExactPuzzleID(270975088);
        p1916936305.setRightExactPuzzleID(1899137468);
        p1916936305.getDisplayImage().loadImageFromResource(c, R.drawable.p1916936305h);
        p1916936305.setExactRow(2);
        p1916936305.setExactColumn(2);
        p1916936305.getSize().reset(126.6667f, 100.0f);
        p1916936305.getPositionOffset().reset(-76.66666f, -50.0f);
        p1916936305.setIsUseAbsolutePosition(true);
        p1916936305.setIsUseAbsoluteSize(true);
        p1916936305.getImage().setIsUseAbsolutePosition(true);
        p1916936305.getImage().setIsUseAbsoluteSize(true);
        p1916936305.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1916936305.resetPosition();
        getSpiritList().add(p1916936305);
    }

    /* access modifiers changed from: package-private */
    public void c1634497233(Context c) {
        Puzzle p1634497233 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1634497233(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1634497233(editor, this);
            }
        };
        p1634497233.setID(1634497233);
        p1634497233.setName("1634497233");
        p1634497233.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1634497233);
        this.Desktop.RandomlyPlaced(p1634497233);
        p1634497233.setTopEdgeType(EdgeType.Convex);
        p1634497233.setBottomEdgeType(EdgeType.Convex);
        p1634497233.setLeftEdgeType(EdgeType.Concave);
        p1634497233.setRightEdgeType(EdgeType.Concave);
        p1634497233.setTopExactPuzzleID(1916936305);
        p1634497233.setBottomExactPuzzleID(1282863324);
        p1634497233.setLeftExactPuzzleID(324514135);
        p1634497233.setRightExactPuzzleID(400987088);
        p1634497233.getDisplayImage().loadImageFromResource(c, R.drawable.p1634497233h);
        p1634497233.setExactRow(3);
        p1634497233.setExactColumn(2);
        p1634497233.getSize().reset(100.0f, 153.3333f);
        p1634497233.getPositionOffset().reset(-50.0f, -76.66666f);
        p1634497233.setIsUseAbsolutePosition(true);
        p1634497233.setIsUseAbsoluteSize(true);
        p1634497233.getImage().setIsUseAbsolutePosition(true);
        p1634497233.getImage().setIsUseAbsoluteSize(true);
        p1634497233.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1634497233.resetPosition();
        getSpiritList().add(p1634497233);
    }

    /* access modifiers changed from: package-private */
    public void c1282863324(Context c) {
        Puzzle p1282863324 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1282863324(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1282863324(editor, this);
            }
        };
        p1282863324.setID(1282863324);
        p1282863324.setName("1282863324");
        p1282863324.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1282863324);
        this.Desktop.RandomlyPlaced(p1282863324);
        p1282863324.setTopEdgeType(EdgeType.Concave);
        p1282863324.setBottomEdgeType(EdgeType.Convex);
        p1282863324.setLeftEdgeType(EdgeType.Convex);
        p1282863324.setRightEdgeType(EdgeType.Convex);
        p1282863324.setTopExactPuzzleID(1634497233);
        p1282863324.setBottomExactPuzzleID(886908192);
        p1282863324.setLeftExactPuzzleID(941126592);
        p1282863324.setRightExactPuzzleID(1361130064);
        p1282863324.getDisplayImage().loadImageFromResource(c, R.drawable.p1282863324h);
        p1282863324.setExactRow(4);
        p1282863324.setExactColumn(2);
        p1282863324.getSize().reset(153.3333f, 126.6667f);
        p1282863324.getPositionOffset().reset(-76.66666f, -50.0f);
        p1282863324.setIsUseAbsolutePosition(true);
        p1282863324.setIsUseAbsoluteSize(true);
        p1282863324.getImage().setIsUseAbsolutePosition(true);
        p1282863324.getImage().setIsUseAbsoluteSize(true);
        p1282863324.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1282863324.resetPosition();
        getSpiritList().add(p1282863324);
    }

    /* access modifiers changed from: package-private */
    public void c886908192(Context c) {
        Puzzle p886908192 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load886908192(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save886908192(editor, this);
            }
        };
        p886908192.setID(886908192);
        p886908192.setName("886908192");
        p886908192.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p886908192);
        this.Desktop.RandomlyPlaced(p886908192);
        p886908192.setTopEdgeType(EdgeType.Concave);
        p886908192.setBottomEdgeType(EdgeType.Flat);
        p886908192.setLeftEdgeType(EdgeType.Concave);
        p886908192.setRightEdgeType(EdgeType.Concave);
        p886908192.setTopExactPuzzleID(1282863324);
        p886908192.setBottomExactPuzzleID(-1);
        p886908192.setLeftExactPuzzleID(1403772162);
        p886908192.setRightExactPuzzleID(1043421245);
        p886908192.getDisplayImage().loadImageFromResource(c, R.drawable.p886908192h);
        p886908192.setExactRow(5);
        p886908192.setExactColumn(2);
        p886908192.getSize().reset(100.0f, 100.0f);
        p886908192.getPositionOffset().reset(-50.0f, -50.0f);
        p886908192.setIsUseAbsolutePosition(true);
        p886908192.setIsUseAbsoluteSize(true);
        p886908192.getImage().setIsUseAbsolutePosition(true);
        p886908192.getImage().setIsUseAbsoluteSize(true);
        p886908192.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p886908192.resetPosition();
        getSpiritList().add(p886908192);
    }

    /* access modifiers changed from: package-private */
    public void c169907163(Context c) {
        Puzzle p169907163 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load169907163(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save169907163(editor, this);
            }
        };
        p169907163.setID(169907163);
        p169907163.setName("169907163");
        p169907163.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p169907163);
        this.Desktop.RandomlyPlaced(p169907163);
        p169907163.setTopEdgeType(EdgeType.Flat);
        p169907163.setBottomEdgeType(EdgeType.Concave);
        p169907163.setLeftEdgeType(EdgeType.Convex);
        p169907163.setRightEdgeType(EdgeType.Convex);
        p169907163.setTopExactPuzzleID(-1);
        p169907163.setBottomExactPuzzleID(1858273219);
        p169907163.setLeftExactPuzzleID(1842291423);
        p169907163.setRightExactPuzzleID(1596068033);
        p169907163.getDisplayImage().loadImageFromResource(c, R.drawable.p169907163h);
        p169907163.setExactRow(0);
        p169907163.setExactColumn(3);
        p169907163.getSize().reset(153.3333f, 100.0f);
        p169907163.getPositionOffset().reset(-76.66666f, -50.0f);
        p169907163.setIsUseAbsolutePosition(true);
        p169907163.setIsUseAbsoluteSize(true);
        p169907163.getImage().setIsUseAbsolutePosition(true);
        p169907163.getImage().setIsUseAbsoluteSize(true);
        p169907163.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p169907163.resetPosition();
        getSpiritList().add(p169907163);
    }

    /* access modifiers changed from: package-private */
    public void c1858273219(Context c) {
        Puzzle p1858273219 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1858273219(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1858273219(editor, this);
            }
        };
        p1858273219.setID(1858273219);
        p1858273219.setName("1858273219");
        p1858273219.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1858273219);
        this.Desktop.RandomlyPlaced(p1858273219);
        p1858273219.setTopEdgeType(EdgeType.Convex);
        p1858273219.setBottomEdgeType(EdgeType.Convex);
        p1858273219.setLeftEdgeType(EdgeType.Convex);
        p1858273219.setRightEdgeType(EdgeType.Concave);
        p1858273219.setTopExactPuzzleID(169907163);
        p1858273219.setBottomExactPuzzleID(1899137468);
        p1858273219.setLeftExactPuzzleID(1672876109);
        p1858273219.setRightExactPuzzleID(936835558);
        p1858273219.getDisplayImage().loadImageFromResource(c, R.drawable.p1858273219h);
        p1858273219.setExactRow(1);
        p1858273219.setExactColumn(3);
        p1858273219.getSize().reset(126.6667f, 153.3333f);
        p1858273219.getPositionOffset().reset(-76.66666f, -76.66666f);
        p1858273219.setIsUseAbsolutePosition(true);
        p1858273219.setIsUseAbsoluteSize(true);
        p1858273219.getImage().setIsUseAbsolutePosition(true);
        p1858273219.getImage().setIsUseAbsoluteSize(true);
        p1858273219.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1858273219.resetPosition();
        getSpiritList().add(p1858273219);
    }

    /* access modifiers changed from: package-private */
    public void c1899137468(Context c) {
        Puzzle p1899137468 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1899137468(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1899137468(editor, this);
            }
        };
        p1899137468.setID(1899137468);
        p1899137468.setName("1899137468");
        p1899137468.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1899137468);
        this.Desktop.RandomlyPlaced(p1899137468);
        p1899137468.setTopEdgeType(EdgeType.Concave);
        p1899137468.setBottomEdgeType(EdgeType.Concave);
        p1899137468.setLeftEdgeType(EdgeType.Convex);
        p1899137468.setRightEdgeType(EdgeType.Convex);
        p1899137468.setTopExactPuzzleID(1858273219);
        p1899137468.setBottomExactPuzzleID(400987088);
        p1899137468.setLeftExactPuzzleID(1916936305);
        p1899137468.setRightExactPuzzleID(284125710);
        p1899137468.getDisplayImage().loadImageFromResource(c, R.drawable.p1899137468h);
        p1899137468.setExactRow(2);
        p1899137468.setExactColumn(3);
        p1899137468.getSize().reset(153.3333f, 100.0f);
        p1899137468.getPositionOffset().reset(-76.66666f, -50.0f);
        p1899137468.setIsUseAbsolutePosition(true);
        p1899137468.setIsUseAbsoluteSize(true);
        p1899137468.getImage().setIsUseAbsolutePosition(true);
        p1899137468.getImage().setIsUseAbsoluteSize(true);
        p1899137468.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1899137468.resetPosition();
        getSpiritList().add(p1899137468);
    }

    /* access modifiers changed from: package-private */
    public void c400987088(Context c) {
        Puzzle p400987088 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load400987088(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save400987088(editor, this);
            }
        };
        p400987088.setID(400987088);
        p400987088.setName("400987088");
        p400987088.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p400987088);
        this.Desktop.RandomlyPlaced(p400987088);
        p400987088.setTopEdgeType(EdgeType.Convex);
        p400987088.setBottomEdgeType(EdgeType.Concave);
        p400987088.setLeftEdgeType(EdgeType.Convex);
        p400987088.setRightEdgeType(EdgeType.Convex);
        p400987088.setTopExactPuzzleID(1899137468);
        p400987088.setBottomExactPuzzleID(1361130064);
        p400987088.setLeftExactPuzzleID(1634497233);
        p400987088.setRightExactPuzzleID(571702272);
        p400987088.getDisplayImage().loadImageFromResource(c, R.drawable.p400987088h);
        p400987088.setExactRow(3);
        p400987088.setExactColumn(3);
        p400987088.getSize().reset(153.3333f, 126.6667f);
        p400987088.getPositionOffset().reset(-76.66666f, -76.66666f);
        p400987088.setIsUseAbsolutePosition(true);
        p400987088.setIsUseAbsoluteSize(true);
        p400987088.getImage().setIsUseAbsolutePosition(true);
        p400987088.getImage().setIsUseAbsoluteSize(true);
        p400987088.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p400987088.resetPosition();
        getSpiritList().add(p400987088);
    }

    /* access modifiers changed from: package-private */
    public void c1361130064(Context c) {
        Puzzle p1361130064 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1361130064(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1361130064(editor, this);
            }
        };
        p1361130064.setID(1361130064);
        p1361130064.setName("1361130064");
        p1361130064.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1361130064);
        this.Desktop.RandomlyPlaced(p1361130064);
        p1361130064.setTopEdgeType(EdgeType.Convex);
        p1361130064.setBottomEdgeType(EdgeType.Concave);
        p1361130064.setLeftEdgeType(EdgeType.Concave);
        p1361130064.setRightEdgeType(EdgeType.Convex);
        p1361130064.setTopExactPuzzleID(400987088);
        p1361130064.setBottomExactPuzzleID(1043421245);
        p1361130064.setLeftExactPuzzleID(1282863324);
        p1361130064.setRightExactPuzzleID(1752727094);
        p1361130064.getDisplayImage().loadImageFromResource(c, R.drawable.p1361130064h);
        p1361130064.setExactRow(4);
        p1361130064.setExactColumn(3);
        p1361130064.getSize().reset(126.6667f, 126.6667f);
        p1361130064.getPositionOffset().reset(-50.0f, -76.66666f);
        p1361130064.setIsUseAbsolutePosition(true);
        p1361130064.setIsUseAbsoluteSize(true);
        p1361130064.getImage().setIsUseAbsolutePosition(true);
        p1361130064.getImage().setIsUseAbsoluteSize(true);
        p1361130064.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1361130064.resetPosition();
        getSpiritList().add(p1361130064);
    }

    /* access modifiers changed from: package-private */
    public void c1043421245(Context c) {
        Puzzle p1043421245 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1043421245(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1043421245(editor, this);
            }
        };
        p1043421245.setID(1043421245);
        p1043421245.setName("1043421245");
        p1043421245.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1043421245);
        this.Desktop.RandomlyPlaced(p1043421245);
        p1043421245.setTopEdgeType(EdgeType.Convex);
        p1043421245.setBottomEdgeType(EdgeType.Flat);
        p1043421245.setLeftEdgeType(EdgeType.Convex);
        p1043421245.setRightEdgeType(EdgeType.Convex);
        p1043421245.setTopExactPuzzleID(1361130064);
        p1043421245.setBottomExactPuzzleID(-1);
        p1043421245.setLeftExactPuzzleID(886908192);
        p1043421245.setRightExactPuzzleID(798806197);
        p1043421245.getDisplayImage().loadImageFromResource(c, R.drawable.p1043421245h);
        p1043421245.setExactRow(5);
        p1043421245.setExactColumn(3);
        p1043421245.getSize().reset(153.3333f, 126.6667f);
        p1043421245.getPositionOffset().reset(-76.66666f, -76.66666f);
        p1043421245.setIsUseAbsolutePosition(true);
        p1043421245.setIsUseAbsoluteSize(true);
        p1043421245.getImage().setIsUseAbsolutePosition(true);
        p1043421245.getImage().setIsUseAbsoluteSize(true);
        p1043421245.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1043421245.resetPosition();
        getSpiritList().add(p1043421245);
    }

    /* access modifiers changed from: package-private */
    public void c1596068033(Context c) {
        Puzzle p1596068033 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1596068033(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1596068033(editor, this);
            }
        };
        p1596068033.setID(1596068033);
        p1596068033.setName("1596068033");
        p1596068033.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1596068033);
        this.Desktop.RandomlyPlaced(p1596068033);
        p1596068033.setTopEdgeType(EdgeType.Flat);
        p1596068033.setBottomEdgeType(EdgeType.Concave);
        p1596068033.setLeftEdgeType(EdgeType.Concave);
        p1596068033.setRightEdgeType(EdgeType.Convex);
        p1596068033.setTopExactPuzzleID(-1);
        p1596068033.setBottomExactPuzzleID(936835558);
        p1596068033.setLeftExactPuzzleID(169907163);
        p1596068033.setRightExactPuzzleID(775124271);
        p1596068033.getDisplayImage().loadImageFromResource(c, R.drawable.p1596068033h);
        p1596068033.setExactRow(0);
        p1596068033.setExactColumn(4);
        p1596068033.getSize().reset(126.6667f, 100.0f);
        p1596068033.getPositionOffset().reset(-50.0f, -50.0f);
        p1596068033.setIsUseAbsolutePosition(true);
        p1596068033.setIsUseAbsoluteSize(true);
        p1596068033.getImage().setIsUseAbsolutePosition(true);
        p1596068033.getImage().setIsUseAbsoluteSize(true);
        p1596068033.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1596068033.resetPosition();
        getSpiritList().add(p1596068033);
    }

    /* access modifiers changed from: package-private */
    public void c936835558(Context c) {
        Puzzle p936835558 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load936835558(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save936835558(editor, this);
            }
        };
        p936835558.setID(936835558);
        p936835558.setName("936835558");
        p936835558.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p936835558);
        this.Desktop.RandomlyPlaced(p936835558);
        p936835558.setTopEdgeType(EdgeType.Convex);
        p936835558.setBottomEdgeType(EdgeType.Convex);
        p936835558.setLeftEdgeType(EdgeType.Convex);
        p936835558.setRightEdgeType(EdgeType.Convex);
        p936835558.setTopExactPuzzleID(1596068033);
        p936835558.setBottomExactPuzzleID(284125710);
        p936835558.setLeftExactPuzzleID(1858273219);
        p936835558.setRightExactPuzzleID(681132208);
        p936835558.getDisplayImage().loadImageFromResource(c, R.drawable.p936835558h);
        p936835558.setExactRow(1);
        p936835558.setExactColumn(4);
        p936835558.getSize().reset(153.3333f, 153.3333f);
        p936835558.getPositionOffset().reset(-76.66666f, -76.66666f);
        p936835558.setIsUseAbsolutePosition(true);
        p936835558.setIsUseAbsoluteSize(true);
        p936835558.getImage().setIsUseAbsolutePosition(true);
        p936835558.getImage().setIsUseAbsoluteSize(true);
        p936835558.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p936835558.resetPosition();
        getSpiritList().add(p936835558);
    }

    /* access modifiers changed from: package-private */
    public void c284125710(Context c) {
        Puzzle p284125710 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load284125710(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save284125710(editor, this);
            }
        };
        p284125710.setID(284125710);
        p284125710.setName("284125710");
        p284125710.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p284125710);
        this.Desktop.RandomlyPlaced(p284125710);
        p284125710.setTopEdgeType(EdgeType.Concave);
        p284125710.setBottomEdgeType(EdgeType.Convex);
        p284125710.setLeftEdgeType(EdgeType.Concave);
        p284125710.setRightEdgeType(EdgeType.Concave);
        p284125710.setTopExactPuzzleID(936835558);
        p284125710.setBottomExactPuzzleID(571702272);
        p284125710.setLeftExactPuzzleID(1899137468);
        p284125710.setRightExactPuzzleID(928291381);
        p284125710.getDisplayImage().loadImageFromResource(c, R.drawable.p284125710h);
        p284125710.setExactRow(2);
        p284125710.setExactColumn(4);
        p284125710.getSize().reset(100.0f, 126.6667f);
        p284125710.getPositionOffset().reset(-50.0f, -50.0f);
        p284125710.setIsUseAbsolutePosition(true);
        p284125710.setIsUseAbsoluteSize(true);
        p284125710.getImage().setIsUseAbsolutePosition(true);
        p284125710.getImage().setIsUseAbsoluteSize(true);
        p284125710.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p284125710.resetPosition();
        getSpiritList().add(p284125710);
    }

    /* access modifiers changed from: package-private */
    public void c571702272(Context c) {
        Puzzle p571702272 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load571702272(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save571702272(editor, this);
            }
        };
        p571702272.setID(571702272);
        p571702272.setName("571702272");
        p571702272.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p571702272);
        this.Desktop.RandomlyPlaced(p571702272);
        p571702272.setTopEdgeType(EdgeType.Concave);
        p571702272.setBottomEdgeType(EdgeType.Concave);
        p571702272.setLeftEdgeType(EdgeType.Concave);
        p571702272.setRightEdgeType(EdgeType.Concave);
        p571702272.setTopExactPuzzleID(284125710);
        p571702272.setBottomExactPuzzleID(1752727094);
        p571702272.setLeftExactPuzzleID(400987088);
        p571702272.setRightExactPuzzleID(1656769618);
        p571702272.getDisplayImage().loadImageFromResource(c, R.drawable.p571702272h);
        p571702272.setExactRow(3);
        p571702272.setExactColumn(4);
        p571702272.getSize().reset(100.0f, 100.0f);
        p571702272.getPositionOffset().reset(-50.0f, -50.0f);
        p571702272.setIsUseAbsolutePosition(true);
        p571702272.setIsUseAbsoluteSize(true);
        p571702272.getImage().setIsUseAbsolutePosition(true);
        p571702272.getImage().setIsUseAbsoluteSize(true);
        p571702272.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p571702272.resetPosition();
        getSpiritList().add(p571702272);
    }

    /* access modifiers changed from: package-private */
    public void c1752727094(Context c) {
        Puzzle p1752727094 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1752727094(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1752727094(editor, this);
            }
        };
        p1752727094.setID(1752727094);
        p1752727094.setName("1752727094");
        p1752727094.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1752727094);
        this.Desktop.RandomlyPlaced(p1752727094);
        p1752727094.setTopEdgeType(EdgeType.Convex);
        p1752727094.setBottomEdgeType(EdgeType.Convex);
        p1752727094.setLeftEdgeType(EdgeType.Concave);
        p1752727094.setRightEdgeType(EdgeType.Concave);
        p1752727094.setTopExactPuzzleID(571702272);
        p1752727094.setBottomExactPuzzleID(798806197);
        p1752727094.setLeftExactPuzzleID(1361130064);
        p1752727094.setRightExactPuzzleID(1044734777);
        p1752727094.getDisplayImage().loadImageFromResource(c, R.drawable.p1752727094h);
        p1752727094.setExactRow(4);
        p1752727094.setExactColumn(4);
        p1752727094.getSize().reset(100.0f, 153.3333f);
        p1752727094.getPositionOffset().reset(-50.0f, -76.66666f);
        p1752727094.setIsUseAbsolutePosition(true);
        p1752727094.setIsUseAbsoluteSize(true);
        p1752727094.getImage().setIsUseAbsolutePosition(true);
        p1752727094.getImage().setIsUseAbsoluteSize(true);
        p1752727094.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1752727094.resetPosition();
        getSpiritList().add(p1752727094);
    }

    /* access modifiers changed from: package-private */
    public void c798806197(Context c) {
        Puzzle p798806197 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load798806197(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save798806197(editor, this);
            }
        };
        p798806197.setID(798806197);
        p798806197.setName("798806197");
        p798806197.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p798806197);
        this.Desktop.RandomlyPlaced(p798806197);
        p798806197.setTopEdgeType(EdgeType.Concave);
        p798806197.setBottomEdgeType(EdgeType.Flat);
        p798806197.setLeftEdgeType(EdgeType.Concave);
        p798806197.setRightEdgeType(EdgeType.Concave);
        p798806197.setTopExactPuzzleID(1752727094);
        p798806197.setBottomExactPuzzleID(-1);
        p798806197.setLeftExactPuzzleID(1043421245);
        p798806197.setRightExactPuzzleID(1644878244);
        p798806197.getDisplayImage().loadImageFromResource(c, R.drawable.p798806197h);
        p798806197.setExactRow(5);
        p798806197.setExactColumn(4);
        p798806197.getSize().reset(100.0f, 100.0f);
        p798806197.getPositionOffset().reset(-50.0f, -50.0f);
        p798806197.setIsUseAbsolutePosition(true);
        p798806197.setIsUseAbsoluteSize(true);
        p798806197.getImage().setIsUseAbsolutePosition(true);
        p798806197.getImage().setIsUseAbsoluteSize(true);
        p798806197.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p798806197.resetPosition();
        getSpiritList().add(p798806197);
    }

    /* access modifiers changed from: package-private */
    public void c775124271(Context c) {
        Puzzle p775124271 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load775124271(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save775124271(editor, this);
            }
        };
        p775124271.setID(775124271);
        p775124271.setName("775124271");
        p775124271.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p775124271);
        this.Desktop.RandomlyPlaced(p775124271);
        p775124271.setTopEdgeType(EdgeType.Flat);
        p775124271.setBottomEdgeType(EdgeType.Concave);
        p775124271.setLeftEdgeType(EdgeType.Concave);
        p775124271.setRightEdgeType(EdgeType.Convex);
        p775124271.setTopExactPuzzleID(-1);
        p775124271.setBottomExactPuzzleID(681132208);
        p775124271.setLeftExactPuzzleID(1596068033);
        p775124271.setRightExactPuzzleID(1816556930);
        p775124271.getDisplayImage().loadImageFromResource(c, R.drawable.p775124271h);
        p775124271.setExactRow(0);
        p775124271.setExactColumn(5);
        p775124271.getSize().reset(126.6667f, 100.0f);
        p775124271.getPositionOffset().reset(-50.0f, -50.0f);
        p775124271.setIsUseAbsolutePosition(true);
        p775124271.setIsUseAbsoluteSize(true);
        p775124271.getImage().setIsUseAbsolutePosition(true);
        p775124271.getImage().setIsUseAbsoluteSize(true);
        p775124271.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p775124271.resetPosition();
        getSpiritList().add(p775124271);
    }

    /* access modifiers changed from: package-private */
    public void c681132208(Context c) {
        Puzzle p681132208 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load681132208(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save681132208(editor, this);
            }
        };
        p681132208.setID(681132208);
        p681132208.setName("681132208");
        p681132208.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p681132208);
        this.Desktop.RandomlyPlaced(p681132208);
        p681132208.setTopEdgeType(EdgeType.Convex);
        p681132208.setBottomEdgeType(EdgeType.Convex);
        p681132208.setLeftEdgeType(EdgeType.Concave);
        p681132208.setRightEdgeType(EdgeType.Concave);
        p681132208.setTopExactPuzzleID(775124271);
        p681132208.setBottomExactPuzzleID(928291381);
        p681132208.setLeftExactPuzzleID(936835558);
        p681132208.setRightExactPuzzleID(268814011);
        p681132208.getDisplayImage().loadImageFromResource(c, R.drawable.p681132208h);
        p681132208.setExactRow(1);
        p681132208.setExactColumn(5);
        p681132208.getSize().reset(100.0f, 153.3333f);
        p681132208.getPositionOffset().reset(-50.0f, -76.66666f);
        p681132208.setIsUseAbsolutePosition(true);
        p681132208.setIsUseAbsoluteSize(true);
        p681132208.getImage().setIsUseAbsolutePosition(true);
        p681132208.getImage().setIsUseAbsoluteSize(true);
        p681132208.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p681132208.resetPosition();
        getSpiritList().add(p681132208);
    }

    /* access modifiers changed from: package-private */
    public void c928291381(Context c) {
        Puzzle p928291381 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load928291381(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save928291381(editor, this);
            }
        };
        p928291381.setID(928291381);
        p928291381.setName("928291381");
        p928291381.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p928291381);
        this.Desktop.RandomlyPlaced(p928291381);
        p928291381.setTopEdgeType(EdgeType.Concave);
        p928291381.setBottomEdgeType(EdgeType.Concave);
        p928291381.setLeftEdgeType(EdgeType.Convex);
        p928291381.setRightEdgeType(EdgeType.Concave);
        p928291381.setTopExactPuzzleID(681132208);
        p928291381.setBottomExactPuzzleID(1656769618);
        p928291381.setLeftExactPuzzleID(284125710);
        p928291381.setRightExactPuzzleID(72983769);
        p928291381.getDisplayImage().loadImageFromResource(c, R.drawable.p928291381h);
        p928291381.setExactRow(2);
        p928291381.setExactColumn(5);
        p928291381.getSize().reset(126.6667f, 100.0f);
        p928291381.getPositionOffset().reset(-76.66666f, -50.0f);
        p928291381.setIsUseAbsolutePosition(true);
        p928291381.setIsUseAbsoluteSize(true);
        p928291381.getImage().setIsUseAbsolutePosition(true);
        p928291381.getImage().setIsUseAbsoluteSize(true);
        p928291381.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p928291381.resetPosition();
        getSpiritList().add(p928291381);
    }

    /* access modifiers changed from: package-private */
    public void c1656769618(Context c) {
        Puzzle p1656769618 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1656769618(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1656769618(editor, this);
            }
        };
        p1656769618.setID(1656769618);
        p1656769618.setName("1656769618");
        p1656769618.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1656769618);
        this.Desktop.RandomlyPlaced(p1656769618);
        p1656769618.setTopEdgeType(EdgeType.Convex);
        p1656769618.setBottomEdgeType(EdgeType.Concave);
        p1656769618.setLeftEdgeType(EdgeType.Convex);
        p1656769618.setRightEdgeType(EdgeType.Concave);
        p1656769618.setTopExactPuzzleID(928291381);
        p1656769618.setBottomExactPuzzleID(1044734777);
        p1656769618.setLeftExactPuzzleID(571702272);
        p1656769618.setRightExactPuzzleID(909185754);
        p1656769618.getDisplayImage().loadImageFromResource(c, R.drawable.p1656769618h);
        p1656769618.setExactRow(3);
        p1656769618.setExactColumn(5);
        p1656769618.getSize().reset(126.6667f, 126.6667f);
        p1656769618.getPositionOffset().reset(-76.66666f, -76.66666f);
        p1656769618.setIsUseAbsolutePosition(true);
        p1656769618.setIsUseAbsoluteSize(true);
        p1656769618.getImage().setIsUseAbsolutePosition(true);
        p1656769618.getImage().setIsUseAbsoluteSize(true);
        p1656769618.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1656769618.resetPosition();
        getSpiritList().add(p1656769618);
    }

    /* access modifiers changed from: package-private */
    public void c1044734777(Context c) {
        Puzzle p1044734777 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1044734777(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1044734777(editor, this);
            }
        };
        p1044734777.setID(1044734777);
        p1044734777.setName("1044734777");
        p1044734777.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1044734777);
        this.Desktop.RandomlyPlaced(p1044734777);
        p1044734777.setTopEdgeType(EdgeType.Convex);
        p1044734777.setBottomEdgeType(EdgeType.Convex);
        p1044734777.setLeftEdgeType(EdgeType.Convex);
        p1044734777.setRightEdgeType(EdgeType.Concave);
        p1044734777.setTopExactPuzzleID(1656769618);
        p1044734777.setBottomExactPuzzleID(1644878244);
        p1044734777.setLeftExactPuzzleID(1752727094);
        p1044734777.setRightExactPuzzleID(147288878);
        p1044734777.getDisplayImage().loadImageFromResource(c, R.drawable.p1044734777h);
        p1044734777.setExactRow(4);
        p1044734777.setExactColumn(5);
        p1044734777.getSize().reset(126.6667f, 153.3333f);
        p1044734777.getPositionOffset().reset(-76.66666f, -76.66666f);
        p1044734777.setIsUseAbsolutePosition(true);
        p1044734777.setIsUseAbsoluteSize(true);
        p1044734777.getImage().setIsUseAbsolutePosition(true);
        p1044734777.getImage().setIsUseAbsoluteSize(true);
        p1044734777.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1044734777.resetPosition();
        getSpiritList().add(p1044734777);
    }

    /* access modifiers changed from: package-private */
    public void c1644878244(Context c) {
        Puzzle p1644878244 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1644878244(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1644878244(editor, this);
            }
        };
        p1644878244.setID(1644878244);
        p1644878244.setName("1644878244");
        p1644878244.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1644878244);
        this.Desktop.RandomlyPlaced(p1644878244);
        p1644878244.setTopEdgeType(EdgeType.Concave);
        p1644878244.setBottomEdgeType(EdgeType.Flat);
        p1644878244.setLeftEdgeType(EdgeType.Convex);
        p1644878244.setRightEdgeType(EdgeType.Concave);
        p1644878244.setTopExactPuzzleID(1044734777);
        p1644878244.setBottomExactPuzzleID(-1);
        p1644878244.setLeftExactPuzzleID(798806197);
        p1644878244.setRightExactPuzzleID(1871956295);
        p1644878244.getDisplayImage().loadImageFromResource(c, R.drawable.p1644878244h);
        p1644878244.setExactRow(5);
        p1644878244.setExactColumn(5);
        p1644878244.getSize().reset(126.6667f, 100.0f);
        p1644878244.getPositionOffset().reset(-76.66666f, -50.0f);
        p1644878244.setIsUseAbsolutePosition(true);
        p1644878244.setIsUseAbsoluteSize(true);
        p1644878244.getImage().setIsUseAbsolutePosition(true);
        p1644878244.getImage().setIsUseAbsoluteSize(true);
        p1644878244.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1644878244.resetPosition();
        getSpiritList().add(p1644878244);
    }

    /* access modifiers changed from: package-private */
    public void c1816556930(Context c) {
        Puzzle p1816556930 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1816556930(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1816556930(editor, this);
            }
        };
        p1816556930.setID(1816556930);
        p1816556930.setName("1816556930");
        p1816556930.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1816556930);
        this.Desktop.RandomlyPlaced(p1816556930);
        p1816556930.setTopEdgeType(EdgeType.Flat);
        p1816556930.setBottomEdgeType(EdgeType.Concave);
        p1816556930.setLeftEdgeType(EdgeType.Concave);
        p1816556930.setRightEdgeType(EdgeType.Concave);
        p1816556930.setTopExactPuzzleID(-1);
        p1816556930.setBottomExactPuzzleID(268814011);
        p1816556930.setLeftExactPuzzleID(775124271);
        p1816556930.setRightExactPuzzleID(1625653041);
        p1816556930.getDisplayImage().loadImageFromResource(c, R.drawable.p1816556930h);
        p1816556930.setExactRow(0);
        p1816556930.setExactColumn(6);
        p1816556930.getSize().reset(100.0f, 100.0f);
        p1816556930.getPositionOffset().reset(-50.0f, -50.0f);
        p1816556930.setIsUseAbsolutePosition(true);
        p1816556930.setIsUseAbsoluteSize(true);
        p1816556930.getImage().setIsUseAbsolutePosition(true);
        p1816556930.getImage().setIsUseAbsoluteSize(true);
        p1816556930.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1816556930.resetPosition();
        getSpiritList().add(p1816556930);
    }

    /* access modifiers changed from: package-private */
    public void c268814011(Context c) {
        Puzzle p268814011 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load268814011(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save268814011(editor, this);
            }
        };
        p268814011.setID(268814011);
        p268814011.setName("268814011");
        p268814011.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p268814011);
        this.Desktop.RandomlyPlaced(p268814011);
        p268814011.setTopEdgeType(EdgeType.Convex);
        p268814011.setBottomEdgeType(EdgeType.Concave);
        p268814011.setLeftEdgeType(EdgeType.Convex);
        p268814011.setRightEdgeType(EdgeType.Convex);
        p268814011.setTopExactPuzzleID(1816556930);
        p268814011.setBottomExactPuzzleID(72983769);
        p268814011.setLeftExactPuzzleID(681132208);
        p268814011.setRightExactPuzzleID(1381966028);
        p268814011.getDisplayImage().loadImageFromResource(c, R.drawable.p268814011h);
        p268814011.setExactRow(1);
        p268814011.setExactColumn(6);
        p268814011.getSize().reset(153.3333f, 126.6667f);
        p268814011.getPositionOffset().reset(-76.66666f, -76.66666f);
        p268814011.setIsUseAbsolutePosition(true);
        p268814011.setIsUseAbsoluteSize(true);
        p268814011.getImage().setIsUseAbsolutePosition(true);
        p268814011.getImage().setIsUseAbsoluteSize(true);
        p268814011.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p268814011.resetPosition();
        getSpiritList().add(p268814011);
    }

    /* access modifiers changed from: package-private */
    public void c72983769(Context c) {
        Puzzle p72983769 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load72983769(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save72983769(editor, this);
            }
        };
        p72983769.setID(72983769);
        p72983769.setName("72983769");
        p72983769.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p72983769);
        this.Desktop.RandomlyPlaced(p72983769);
        p72983769.setTopEdgeType(EdgeType.Convex);
        p72983769.setBottomEdgeType(EdgeType.Convex);
        p72983769.setLeftEdgeType(EdgeType.Convex);
        p72983769.setRightEdgeType(EdgeType.Concave);
        p72983769.setTopExactPuzzleID(268814011);
        p72983769.setBottomExactPuzzleID(909185754);
        p72983769.setLeftExactPuzzleID(928291381);
        p72983769.setRightExactPuzzleID(1206277634);
        p72983769.getDisplayImage().loadImageFromResource(c, R.drawable.p72983769h);
        p72983769.setExactRow(2);
        p72983769.setExactColumn(6);
        p72983769.getSize().reset(126.6667f, 153.3333f);
        p72983769.getPositionOffset().reset(-76.66666f, -76.66666f);
        p72983769.setIsUseAbsolutePosition(true);
        p72983769.setIsUseAbsoluteSize(true);
        p72983769.getImage().setIsUseAbsolutePosition(true);
        p72983769.getImage().setIsUseAbsoluteSize(true);
        p72983769.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p72983769.resetPosition();
        getSpiritList().add(p72983769);
    }

    /* access modifiers changed from: package-private */
    public void c909185754(Context c) {
        Puzzle p909185754 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load909185754(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save909185754(editor, this);
            }
        };
        p909185754.setID(909185754);
        p909185754.setName("909185754");
        p909185754.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p909185754);
        this.Desktop.RandomlyPlaced(p909185754);
        p909185754.setTopEdgeType(EdgeType.Concave);
        p909185754.setBottomEdgeType(EdgeType.Concave);
        p909185754.setLeftEdgeType(EdgeType.Convex);
        p909185754.setRightEdgeType(EdgeType.Convex);
        p909185754.setTopExactPuzzleID(72983769);
        p909185754.setBottomExactPuzzleID(147288878);
        p909185754.setLeftExactPuzzleID(1656769618);
        p909185754.setRightExactPuzzleID(645839239);
        p909185754.getDisplayImage().loadImageFromResource(c, R.drawable.p909185754h);
        p909185754.setExactRow(3);
        p909185754.setExactColumn(6);
        p909185754.getSize().reset(153.3333f, 100.0f);
        p909185754.getPositionOffset().reset(-76.66666f, -50.0f);
        p909185754.setIsUseAbsolutePosition(true);
        p909185754.setIsUseAbsoluteSize(true);
        p909185754.getImage().setIsUseAbsolutePosition(true);
        p909185754.getImage().setIsUseAbsoluteSize(true);
        p909185754.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p909185754.resetPosition();
        getSpiritList().add(p909185754);
    }

    /* access modifiers changed from: package-private */
    public void c147288878(Context c) {
        Puzzle p147288878 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load147288878(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save147288878(editor, this);
            }
        };
        p147288878.setID(147288878);
        p147288878.setName("147288878");
        p147288878.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p147288878);
        this.Desktop.RandomlyPlaced(p147288878);
        p147288878.setTopEdgeType(EdgeType.Convex);
        p147288878.setBottomEdgeType(EdgeType.Convex);
        p147288878.setLeftEdgeType(EdgeType.Convex);
        p147288878.setRightEdgeType(EdgeType.Convex);
        p147288878.setTopExactPuzzleID(909185754);
        p147288878.setBottomExactPuzzleID(1871956295);
        p147288878.setLeftExactPuzzleID(1044734777);
        p147288878.setRightExactPuzzleID(1842559882);
        p147288878.getDisplayImage().loadImageFromResource(c, R.drawable.p147288878h);
        p147288878.setExactRow(4);
        p147288878.setExactColumn(6);
        p147288878.getSize().reset(153.3333f, 153.3333f);
        p147288878.getPositionOffset().reset(-76.66666f, -76.66666f);
        p147288878.setIsUseAbsolutePosition(true);
        p147288878.setIsUseAbsoluteSize(true);
        p147288878.getImage().setIsUseAbsolutePosition(true);
        p147288878.getImage().setIsUseAbsoluteSize(true);
        p147288878.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p147288878.resetPosition();
        getSpiritList().add(p147288878);
    }

    /* access modifiers changed from: package-private */
    public void c1871956295(Context c) {
        Puzzle p1871956295 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1871956295(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1871956295(editor, this);
            }
        };
        p1871956295.setID(1871956295);
        p1871956295.setName("1871956295");
        p1871956295.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1871956295);
        this.Desktop.RandomlyPlaced(p1871956295);
        p1871956295.setTopEdgeType(EdgeType.Concave);
        p1871956295.setBottomEdgeType(EdgeType.Flat);
        p1871956295.setLeftEdgeType(EdgeType.Convex);
        p1871956295.setRightEdgeType(EdgeType.Concave);
        p1871956295.setTopExactPuzzleID(147288878);
        p1871956295.setBottomExactPuzzleID(-1);
        p1871956295.setLeftExactPuzzleID(1644878244);
        p1871956295.setRightExactPuzzleID(1738048464);
        p1871956295.getDisplayImage().loadImageFromResource(c, R.drawable.p1871956295h);
        p1871956295.setExactRow(5);
        p1871956295.setExactColumn(6);
        p1871956295.getSize().reset(126.6667f, 100.0f);
        p1871956295.getPositionOffset().reset(-76.66666f, -50.0f);
        p1871956295.setIsUseAbsolutePosition(true);
        p1871956295.setIsUseAbsoluteSize(true);
        p1871956295.getImage().setIsUseAbsolutePosition(true);
        p1871956295.getImage().setIsUseAbsoluteSize(true);
        p1871956295.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1871956295.resetPosition();
        getSpiritList().add(p1871956295);
    }

    /* access modifiers changed from: package-private */
    public void c1625653041(Context c) {
        Puzzle p1625653041 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1625653041(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1625653041(editor, this);
            }
        };
        p1625653041.setID(1625653041);
        p1625653041.setName("1625653041");
        p1625653041.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1625653041);
        this.Desktop.RandomlyPlaced(p1625653041);
        p1625653041.setTopEdgeType(EdgeType.Flat);
        p1625653041.setBottomEdgeType(EdgeType.Concave);
        p1625653041.setLeftEdgeType(EdgeType.Convex);
        p1625653041.setRightEdgeType(EdgeType.Flat);
        p1625653041.setTopExactPuzzleID(-1);
        p1625653041.setBottomExactPuzzleID(1381966028);
        p1625653041.setLeftExactPuzzleID(1816556930);
        p1625653041.setRightExactPuzzleID(-1);
        p1625653041.getDisplayImage().loadImageFromResource(c, R.drawable.p1625653041h);
        p1625653041.setExactRow(0);
        p1625653041.setExactColumn(7);
        p1625653041.getSize().reset(126.6667f, 100.0f);
        p1625653041.getPositionOffset().reset(-76.66666f, -50.0f);
        p1625653041.setIsUseAbsolutePosition(true);
        p1625653041.setIsUseAbsoluteSize(true);
        p1625653041.getImage().setIsUseAbsolutePosition(true);
        p1625653041.getImage().setIsUseAbsoluteSize(true);
        p1625653041.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1625653041.resetPosition();
        getSpiritList().add(p1625653041);
    }

    /* access modifiers changed from: package-private */
    public void c1381966028(Context c) {
        Puzzle p1381966028 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1381966028(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1381966028(editor, this);
            }
        };
        p1381966028.setID(1381966028);
        p1381966028.setName("1381966028");
        p1381966028.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1381966028);
        this.Desktop.RandomlyPlaced(p1381966028);
        p1381966028.setTopEdgeType(EdgeType.Convex);
        p1381966028.setBottomEdgeType(EdgeType.Concave);
        p1381966028.setLeftEdgeType(EdgeType.Concave);
        p1381966028.setRightEdgeType(EdgeType.Flat);
        p1381966028.setTopExactPuzzleID(1625653041);
        p1381966028.setBottomExactPuzzleID(1206277634);
        p1381966028.setLeftExactPuzzleID(268814011);
        p1381966028.setRightExactPuzzleID(-1);
        p1381966028.getDisplayImage().loadImageFromResource(c, R.drawable.p1381966028h);
        p1381966028.setExactRow(1);
        p1381966028.setExactColumn(7);
        p1381966028.getSize().reset(100.0f, 126.6667f);
        p1381966028.getPositionOffset().reset(-50.0f, -76.66666f);
        p1381966028.setIsUseAbsolutePosition(true);
        p1381966028.setIsUseAbsoluteSize(true);
        p1381966028.getImage().setIsUseAbsolutePosition(true);
        p1381966028.getImage().setIsUseAbsoluteSize(true);
        p1381966028.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1381966028.resetPosition();
        getSpiritList().add(p1381966028);
    }

    /* access modifiers changed from: package-private */
    public void c1206277634(Context c) {
        Puzzle p1206277634 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1206277634(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1206277634(editor, this);
            }
        };
        p1206277634.setID(1206277634);
        p1206277634.setName("1206277634");
        p1206277634.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1206277634);
        this.Desktop.RandomlyPlaced(p1206277634);
        p1206277634.setTopEdgeType(EdgeType.Convex);
        p1206277634.setBottomEdgeType(EdgeType.Concave);
        p1206277634.setLeftEdgeType(EdgeType.Convex);
        p1206277634.setRightEdgeType(EdgeType.Flat);
        p1206277634.setTopExactPuzzleID(1381966028);
        p1206277634.setBottomExactPuzzleID(645839239);
        p1206277634.setLeftExactPuzzleID(72983769);
        p1206277634.setRightExactPuzzleID(-1);
        p1206277634.getDisplayImage().loadImageFromResource(c, R.drawable.p1206277634h);
        p1206277634.setExactRow(2);
        p1206277634.setExactColumn(7);
        p1206277634.getSize().reset(126.6667f, 126.6667f);
        p1206277634.getPositionOffset().reset(-76.66666f, -76.66666f);
        p1206277634.setIsUseAbsolutePosition(true);
        p1206277634.setIsUseAbsoluteSize(true);
        p1206277634.getImage().setIsUseAbsolutePosition(true);
        p1206277634.getImage().setIsUseAbsoluteSize(true);
        p1206277634.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1206277634.resetPosition();
        getSpiritList().add(p1206277634);
    }

    /* access modifiers changed from: package-private */
    public void c645839239(Context c) {
        Puzzle p645839239 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load645839239(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save645839239(editor, this);
            }
        };
        p645839239.setID(645839239);
        p645839239.setName("645839239");
        p645839239.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p645839239);
        this.Desktop.RandomlyPlaced(p645839239);
        p645839239.setTopEdgeType(EdgeType.Convex);
        p645839239.setBottomEdgeType(EdgeType.Convex);
        p645839239.setLeftEdgeType(EdgeType.Concave);
        p645839239.setRightEdgeType(EdgeType.Flat);
        p645839239.setTopExactPuzzleID(1206277634);
        p645839239.setBottomExactPuzzleID(1842559882);
        p645839239.setLeftExactPuzzleID(909185754);
        p645839239.setRightExactPuzzleID(-1);
        p645839239.getDisplayImage().loadImageFromResource(c, R.drawable.p645839239h);
        p645839239.setExactRow(3);
        p645839239.setExactColumn(7);
        p645839239.getSize().reset(100.0f, 153.3333f);
        p645839239.getPositionOffset().reset(-50.0f, -76.66666f);
        p645839239.setIsUseAbsolutePosition(true);
        p645839239.setIsUseAbsoluteSize(true);
        p645839239.getImage().setIsUseAbsolutePosition(true);
        p645839239.getImage().setIsUseAbsoluteSize(true);
        p645839239.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p645839239.resetPosition();
        getSpiritList().add(p645839239);
    }

    /* access modifiers changed from: package-private */
    public void c1842559882(Context c) {
        Puzzle p1842559882 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1842559882(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1842559882(editor, this);
            }
        };
        p1842559882.setID(1842559882);
        p1842559882.setName("1842559882");
        p1842559882.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1842559882);
        this.Desktop.RandomlyPlaced(p1842559882);
        p1842559882.setTopEdgeType(EdgeType.Concave);
        p1842559882.setBottomEdgeType(EdgeType.Convex);
        p1842559882.setLeftEdgeType(EdgeType.Concave);
        p1842559882.setRightEdgeType(EdgeType.Flat);
        p1842559882.setTopExactPuzzleID(645839239);
        p1842559882.setBottomExactPuzzleID(1738048464);
        p1842559882.setLeftExactPuzzleID(147288878);
        p1842559882.setRightExactPuzzleID(-1);
        p1842559882.getDisplayImage().loadImageFromResource(c, R.drawable.p1842559882h);
        p1842559882.setExactRow(4);
        p1842559882.setExactColumn(7);
        p1842559882.getSize().reset(100.0f, 126.6667f);
        p1842559882.getPositionOffset().reset(-50.0f, -50.0f);
        p1842559882.setIsUseAbsolutePosition(true);
        p1842559882.setIsUseAbsoluteSize(true);
        p1842559882.getImage().setIsUseAbsolutePosition(true);
        p1842559882.getImage().setIsUseAbsoluteSize(true);
        p1842559882.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1842559882.resetPosition();
        getSpiritList().add(p1842559882);
    }

    /* access modifiers changed from: package-private */
    public void c1738048464(Context c) {
        Puzzle p1738048464 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1738048464(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1738048464(editor, this);
            }
        };
        p1738048464.setID(1738048464);
        p1738048464.setName("1738048464");
        p1738048464.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1738048464);
        this.Desktop.RandomlyPlaced(p1738048464);
        p1738048464.setTopEdgeType(EdgeType.Concave);
        p1738048464.setBottomEdgeType(EdgeType.Flat);
        p1738048464.setLeftEdgeType(EdgeType.Convex);
        p1738048464.setRightEdgeType(EdgeType.Flat);
        p1738048464.setTopExactPuzzleID(1842559882);
        p1738048464.setBottomExactPuzzleID(-1);
        p1738048464.setLeftExactPuzzleID(1871956295);
        p1738048464.setRightExactPuzzleID(-1);
        p1738048464.getDisplayImage().loadImageFromResource(c, R.drawable.p1738048464h);
        p1738048464.setExactRow(5);
        p1738048464.setExactColumn(7);
        p1738048464.getSize().reset(126.6667f, 100.0f);
        p1738048464.getPositionOffset().reset(-76.66666f, -50.0f);
        p1738048464.setIsUseAbsolutePosition(true);
        p1738048464.setIsUseAbsoluteSize(true);
        p1738048464.getImage().setIsUseAbsolutePosition(true);
        p1738048464.getImage().setIsUseAbsoluteSize(true);
        p1738048464.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1738048464.resetPosition();
        getSpiritList().add(p1738048464);
    }
}
