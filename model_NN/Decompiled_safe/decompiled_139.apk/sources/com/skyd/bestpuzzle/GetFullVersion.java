package com.skyd.bestpuzzle;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.skyd.bestpuzzle.n1672.R;
import com.skyd.core.android.Android;
import com.skyd.core.android.CommonActivity;

public class GetFullVersion extends CommonActivity {
    private Button _button1 = null;
    private Button _button2 = null;

    public Button getbutton1() {
        if (this._button1 == null) {
            this._button1 = (Button) findViewById(R.id.button1);
        }
        return this._button1;
    }

    public Button getbutton2() {
        if (this._button2 == null) {
            this._button2 = (Button) findViewById(R.id.button2);
        }
        return this._button2;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.getfullversion);
        getbutton1().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Android.openUrl(GetFullVersion.this, (int) R.string.fullVersionKeyAppUrl);
            }
        });
        getbutton2().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                GetFullVersion.this.finish();
            }
        });
    }
}
