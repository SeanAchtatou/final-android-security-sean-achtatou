package mediba.ad.sdk.android;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.net.Uri;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationSet;
import android.widget.RelativeLayout;
import java.lang.ref.WeakReference;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;
import org.json.JSONException;
import org.json.JSONObject;

public class AdProxy implements View.OnClickListener, AdProxyConnectorListener {
    private static HashSet<AdProxyConnector> Connection = null;
    static final String _TAG = "AdProxy";
    private static String icon1_path;
    private static String icon2_path;
    private static String image_path;
    public static Intent intent;
    private static String link_path;
    private static String link_text_bottom;
    private static String link_text_upper;
    private static String view_mode;
    private Hashtable<String, byte[]> A = new Hashtable<>();
    private Vector<Bitmap> C = new Vector<>();
    private double E = -1.0d;
    private double F = -1.0d;
    protected View a;
    private int containerheight = 48;
    private int containerwidth = -1;
    private boolean l;
    protected AdContainer mAdcontainer;
    private int mBackgroundColor = -16777216;
    private int mPrimaryTextColor = -1;
    private int mSecondaryTextColor = -1;
    private a v = null;

    public interface b {
        void a();
    }

    protected AdProxy() {
    }

    public static AdProxy a(a a1, String s, String[] response, int primaryTextColor, int secondaryTextColor, int backgroundColor, AdContainer adcontainer) throws JSONException {
        AdProxy adproxy = new AdProxy();
        adproxy.v = a1;
        adproxy.mPrimaryTextColor = primaryTextColor;
        adproxy.mSecondaryTextColor = secondaryTextColor;
        adproxy.mBackgroundColor = backgroundColor;
        adproxy.mAdcontainer = adcontainer;
        JSONObject jObject = new JSONObject(s);
        if (jObject.length() != 6 && jObject.length() != 3) {
            Log.w(_TAG, "不正なADを所得しました。");
            return null;
        } else if (adproxy.json_parse_response(jObject)) {
            return adproxy;
        } else {
            return null;
        }
    }

    private boolean json_parse_response(JSONObject jObject) throws JSONException {
        view_mode = jObject.getString("type").toString();
        this.F = (double) AdContainer.getDensity();
        link_path = null;
        link_text_upper = null;
        link_text_bottom = null;
        image_path = null;
        icon1_path = null;
        icon2_path = null;
        if (view_mode.equals("1") || view_mode.equals("3")) {
            image_path = jObject.getString("imageurl").toString();
            link_path = jObject.getString("clickurl").toString();
        } else if (view_mode.equals("2") || view_mode.equals("4")) {
            icon1_path = jObject.getString("icon1url").toString();
            icon2_path = jObject.getString("icon2url").toString();
            link_text_upper = jObject.getString("text1").toString();
            link_text_bottom = jObject.getString("text2").toString();
            link_path = jObject.getString("clickurl").toString();
        } else {
            Log.w(_TAG, "不正なADを所得しました。");
            return false;
        }
        constructView();
        return true;
    }

    private void constructView() {
        try {
            if (AdUtil.isLogEnable()) {
                Log.d(_TAG, "constructView");
            }
            Viewadd view = new Viewadd(this.mAdcontainer, this);
            if (!createInnerView(view) && this.v != null) {
                this.v.a();
            }
            MasAdView.handler.post(view);
            if (Connection != null) {
                synchronized (Connection) {
                    Connection.clear();
                    Connection = null;
                }
            }
            if (this.A != null) {
                this.A.clear();
                this.A = null;
            }
        } catch (Exception e2) {
            AdUtil.onFailedToReceiveAd(AdContainer.a(getAdContainer()));
            Log.e(_TAG, "Handler Illeguler in AdProxy.constructView ");
        }
    }

    private boolean createInnerView(Viewadd view) {
        if (AdUtil.isLogEnable()) {
            Log.d(_TAG, "AdProxy CreateInnerView");
        }
        Rect init_rect = init_rect();
        try {
            Context context = this.mAdcontainer.getContext();
            if (view_mode.equals("1")) {
                this.mAdcontainer.setImageView(image_path);
                this.mAdcontainer.setImageLink(link_path, "nomal");
                return true;
            } else if (view_mode.equals("2")) {
                this.mAdcontainer.setIconView(link_text_upper, link_text_bottom, icon1_path, icon2_path);
                this.mAdcontainer.setIconLink(link_path, "nomal");
                return true;
            } else if (view_mode.equals("3")) {
                this.mAdcontainer.setImageView(image_path);
                this.mAdcontainer.setImageLink(link_path, "overlay");
                return true;
            } else if (!view_mode.equals("4")) {
                return true;
            } else {
                this.mAdcontainer.setIconView(link_text_upper, link_text_bottom, icon1_path, icon2_path);
                this.mAdcontainer.setIconLink(link_path, "overlay");
                return true;
            }
        } catch (Exception e2) {
            Log.e(_TAG, "Handler Illeguler in AdProxy.createInnerView " + e2.getMessage());
            AdUtil.onFailedToReceiveAd(AdContainer.a(getAdContainer()));
            return true;
        }
    }

    private static Rect init_rect() {
        return new Rect(50, 50, 50, 50);
    }

    private static void CacheConnection() {
        if (Connection != null) {
            synchronized (Connection) {
                Iterator iterator = Connection.iterator();
                while (iterator.hasNext()) {
                    iterator.next().g();
                }
            }
        }
    }

    private static boolean checkConnectionCache() {
        return Connection == null || Connection.size() == 0;
    }

    private static void form_intent(String url) {
        if (AdUtil.isLogEnable()) {
            Log.d(_TAG, "Createing Intent:" + url);
        }
        intent = new Intent("android.intent.action.VIEW", Uri.parse(url));
    }

    private static class Viewadd implements Runnable {
        private AdContainer ladcontainer;
        private WeakReference ladproxy;

        public Viewadd(AdContainer adcontainer, AdProxy adproxy) {
            this.ladcontainer = adcontainer;
            this.ladproxy = new WeakReference(adproxy);
        }

        public final void run() {
            if (AdUtil.isLogEnable()) {
                Log.d(AdProxy._TAG, "Start Viewadd.run");
            }
            try {
                if (this.ladcontainer != null) {
                    this.ladcontainer.setPadding(0, 0, 0, 0);
                    this.ladcontainer.invalidate();
                    this.ladcontainer.requestLayout();
                } else {
                    Log.e(AdProxy._TAG, "コンテナデータがありません");
                }
                AdProxy adproxy = (AdProxy) this.ladproxy.get();
                if (adproxy != null) {
                    AdProxy.a(adproxy);
                }
            } catch (Exception e) {
                Log.e(AdProxy._TAG, "exception caught in AdProxy Viewadd, " + e.getMessage());
            }
        }
    }

    private static class ViewContainer {
        public AnimationSet animationset;
        public RelativeLayout.LayoutParams layoutParam;
        public View view;

        private ViewContainer(byte paramByte) {
        }

        public ViewContainer() {
            this((byte) 0);
        }
    }

    static void a(AdProxy adproxy) {
        if (adproxy.v != null) {
            adproxy.v.a(adproxy);
        }
    }

    protected static class a {
        WeakReference<MasAdView> a;

        public void a() {
            MasAdView localAdView = this.a.get();
            if (localAdView != null) {
                MasAdView.c(localAdView);
            }
        }

        /* access modifiers changed from: package-private */
        public void a(AdProxy adproxy) {
            MasAdView adview = this.a.get();
            if (adview != null) {
                synchronized (adview) {
                    try {
                        if (MasAdView.getAdContainer(adview) == null || !adproxy.equals(MasAdView.getAdContainer(adview).getAdProxyInstance())) {
                            if (AdUtil.isLogEnable()) {
                                Log.i(AdProxy._TAG, "Ad Response (" + (SystemClock.uptimeMillis() - MasAdView.getLastRequestTime(adview)) + " ms)");
                            }
                            adview.getContext();
                            adview.a(adproxy, adproxy.getAdContainer());
                        } else if (AdUtil.isLogEnable()) {
                            Log.d(AdProxy._TAG, "Same AD SKIP");
                        }
                    } catch (Exception e) {
                        Exception e2 = e;
                        AdUtil.onFailedToReceiveAd(adview);
                        Log.e(AdProxy._TAG, "Handler Illeguler in AdProxy.a(AdProxy adproxy) " + e2.getMessage());
                    }
                }
                return;
            }
            return;
        }

        public a(MasAdView paramAdView) {
            this.a = new WeakReference<>(paramAdView);
        }
    }

    /* access modifiers changed from: package-private */
    public final double a() {
        return this.E;
    }

    private static class e {
        public String a;
        public boolean b;

        public e(String paramString, boolean paramBoolean) {
            this.a = paramString;
            this.b = paramBoolean;
        }
    }

    public final int e() {
        return this.containerwidth;
    }

    public final void setAdContainer(AdContainer adcontainer) {
        this.mAdcontainer = adcontainer;
    }

    /* access modifiers changed from: package-private */
    public final int a(int paramInt) {
        double d1 = (double) paramInt;
        return (int) (this.F > 0.0d ? this.F * d1 : d1);
    }

    public final int getContainerHeight() {
        return this.containerheight;
    }

    public final void onClick(View view) {
        if (AdUtil.isLogEnable()) {
            Log.d(_TAG, "View Clicked");
        }
    }

    public final AdContainer getAdContainer() {
        return this.mAdcontainer;
    }

    public void a(AdProxyConnector r) {
        String obj = r.e();
        byte[] abyte0 = r.d();
        if (abyte0 != null) {
            this.A.put(obj, abyte0);
            if (Connection != null) {
                synchronized (Connection) {
                    Connection.remove(r);
                }
            }
            if (checkConnectionCache()) {
                constructView();
                return;
            }
            return;
        }
        if (AdUtil.isLogEnable()) {
            Log.d(_TAG, "Failed reading asset(" + obj + ") for ad");
        }
        p();
    }

    public void a(AdProxyConnector connector, Exception exception) {
        p();
    }

    private void p() {
        if (Connection != null) {
            synchronized (Connection) {
                Iterator iterator = Connection.iterator();
                while (iterator.hasNext()) {
                    iterator.next().disconnect();
                }
                Connection.clear();
                Connection = null;
            }
        }
        if (this.A != null) {
            this.A.clear();
            this.A = null;
        }
        if (this.v != null) {
            this.v.a();
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean d() {
        return this.l;
    }
}
