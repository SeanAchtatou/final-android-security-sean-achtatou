package mediba.ad.sdk.android;

public interface AdProxyListener {
    void onFailedToReceiveAd(MasAdView masAdView);

    void onFailedToReceiveRefreshedAd(MasAdView masAdView);

    void onReceiveAd(MasAdView masAdView);

    void onReceiveRefreshedAd(MasAdView masAdView);
}
