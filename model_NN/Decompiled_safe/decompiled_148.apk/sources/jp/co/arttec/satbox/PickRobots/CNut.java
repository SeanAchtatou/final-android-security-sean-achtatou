package jp.co.arttec.satbox.PickRobots;

/* compiled from: Nut */
class CNut extends CDangerBase {
    final int NUT_DMG = 40;
    final int NUT_SCORE = 15;
    final int NUT_SPEED = 3;

    public CNut(MoguraView pView, int nPosX, int nSpeed, int nTexIdx, int nSnd, int nAdventX) {
        super(pView, nPosX, nTexIdx, nSnd);
        CHitPoint HP = this.m_pView.GetHP();
        this.m_nDmgLen = (HP.GetMetaW() / HP.GetMaxHp()) * 40;
        this.m_nDmg = 40;
        this.m_rcRect.left = nAdventX;
        this.m_nSpeed = nSpeed + 3;
        this.m_nScore = 15;
    }
}
