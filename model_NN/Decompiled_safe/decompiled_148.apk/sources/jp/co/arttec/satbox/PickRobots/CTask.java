package jp.co.arttec.satbox.PickRobots;

import android.graphics.Canvas;
import android.graphics.Rect;

/* compiled from: Task */
class CTask {
    protected MoguraView m_pView;
    protected Rect m_rcRect = new Rect(0, 0, 0, 0);

    public CTask(MoguraView pView) {
        this.m_pView = pView;
    }

    public boolean Exec(Canvas canvas) {
        return true;
    }

    public void Draw(Canvas canvas) {
    }

    public Rect GetRect() {
        return this.m_rcRect;
    }
}
