package jp.co.arttec.satbox.PickRobots;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

/* compiled from: Fall */
class CFall extends CTask {
    static final int START_ADVENT_POSY = 20;
    final int BOMB_TOUCH_SCOPE = 30;
    protected float m_fAddRot;
    protected float m_fRot;
    protected int m_nAlpha;
    protected int m_nAnimCnt;
    protected int m_nHP;
    protected int m_nScore;
    protected int m_nSnd;
    protected int m_nSpeed = 0;
    protected int m_nTouchScope;
    protected Bitmap m_pTex;

    public CFall(MoguraView pView, int nPosX, int nTexIdx, int nSnd) {
        super(pView);
        this.m_pTex = this.m_pView.GetTex(nTexIdx);
        this.m_nSnd = nSnd;
        this.m_rcRect = new Rect(nPosX, START_ADVENT_POSY, (int) (((float) this.m_pTex.getWidth()) * this.m_pView.re_size_width), (int) (((float) this.m_pTex.getWidth()) * this.m_pView.re_size_height));
        this.m_nAlpha = 255;
        this.m_fRot = 0.0f;
        this.m_fAddRot = (((float) Math.random()) * 6.0f) - 3.0f;
        this.m_nHP = 1;
        this.m_nTouchScope = 30;
        this.m_nAnimCnt = 0;
        this.m_nScore = 0;
    }

    public void Move() {
        float f = this.m_fRot + this.m_fAddRot;
        this.m_fRot = f;
        if (f >= 360.0f) {
            this.m_fRot -= 360.0f;
        }
        this.m_rcRect.top += this.m_nSpeed;
        this.m_rcRect.top = Math.min(this.m_pView.GetDispY(), Math.max(0, this.m_rcRect.top));
    }

    public void Draw(Canvas canvas) {
        Paint paint = new Paint();
        paint.setAlpha(this.m_nAlpha);
        canvas.save();
        canvas.rotate(this.m_fRot, (float) (this.m_rcRect.left + (this.m_rcRect.right / 2)), (float) (this.m_rcRect.top + (this.m_rcRect.bottom / 2)));
        canvas.drawBitmap(this.m_pTex, (float) this.m_rcRect.left, (float) this.m_rcRect.top, paint);
        canvas.restore();
    }

    public boolean Touch(int nX, int nY) {
        if (!HitCheckDot(nX, nY)) {
            return false;
        }
        int i = this.m_nHP - 1;
        this.m_nHP = i;
        if (i <= 0) {
            this.m_nAnimCnt++;
            this.m_pView.GetScore().AddScore(this.m_nScore);
        }
        this.m_pView.GetSndPool().play(this.m_pView.GetSnd(this.m_nSnd), 1.0f, 1.0f, 0, 0, 1.0f);
        return true;
    }

    public boolean HitCheckDot(int nX, int nY) {
        return this.m_rcRect.left - this.m_nTouchScope <= nX && nX < (this.m_rcRect.left + this.m_rcRect.right) + this.m_nTouchScope && this.m_rcRect.top - this.m_nTouchScope <= nY && nY < (this.m_rcRect.top + this.m_rcRect.bottom) + this.m_nTouchScope;
    }

    public boolean HitCheckBox(Rect rcRect) {
        return this.m_rcRect.left <= rcRect.left + rcRect.right && rcRect.left < this.m_rcRect.left + this.m_rcRect.right && this.m_rcRect.top <= rcRect.top + rcRect.bottom && rcRect.top < this.m_rcRect.top + this.m_rcRect.bottom;
    }

    public boolean HitCheckBox(int nX, int nY, int nW, int nH) {
        return this.m_rcRect.left <= nX + nW && nX < this.m_rcRect.left + this.m_rcRect.right && this.m_rcRect.top <= nY + nH && nY < this.m_rcRect.top + this.m_rcRect.bottom;
    }

    public Rect GetPos() {
        return this.m_rcRect;
    }

    public int GetAnimCnt() {
        return this.m_nAnimCnt;
    }

    public Bitmap GetTex() {
        return this.m_pTex;
    }
}
