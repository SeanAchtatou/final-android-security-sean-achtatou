package jp.co.arttec.satbox.scoreranklib;

import android.content.Context;

public interface GAEController {
    void getHighScore();

    void registScore();

    void setActivity(Context context);

    void setOnFinishListener(HttpCommunicationListener httpCommunicationListener);
}
