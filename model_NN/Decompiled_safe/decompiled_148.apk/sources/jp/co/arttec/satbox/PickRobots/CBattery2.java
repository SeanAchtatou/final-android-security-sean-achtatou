package jp.co.arttec.satbox.PickRobots;

/* compiled from: Battery2 */
class CBattery2 extends CDangerBase {
    final int BATTERY2_DMG = 30;
    final int BATTERY2_SCORE = 10;
    final int BATTERY2_SPEED = 6;
    final int BATTERY2_TOUCH_SCOPE = 8;

    public CBattery2(MoguraView pView, int nPosX, int nSpeed, int nTexIdx, int nSnd) {
        super(pView, nPosX, nTexIdx, nSnd);
        CHitPoint HP = this.m_pView.GetHP();
        this.m_nDmgLen = (HP.GetMetaW() / HP.GetMaxHp()) * 30;
        this.m_nDmg = 30;
        this.m_nSpeed = nSpeed + 6;
        this.m_nScore = 10;
        this.m_nTouchScope = 8;
    }
}
