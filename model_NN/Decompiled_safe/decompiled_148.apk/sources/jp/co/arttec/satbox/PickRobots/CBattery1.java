package jp.co.arttec.satbox.PickRobots;

/* compiled from: Battery1 */
class CBattery1 extends CItemBase {
    final int BATTERY1_MISS_SCORE = -30;
    final int BATTERY1_SCORE = 10;
    final int BATTERY1_SPEED = 5;

    public CBattery1(MoguraView pView, int nPosX, int nSpeed, int nTexIdx, int nSnd) {
        super(pView, nPosX, nTexIdx, nSnd);
        this.m_nSpeed = nSpeed + 5;
        this.m_nScore = 10;
        this.m_nMissScore = -30;
    }
}
