package jp.co.arttec.satbox.PickRobots;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import jp.co.arttec.satbox.scoreranklib.GAECommonData;

/* compiled from: Score */
class CScore {
    final int SCORE_MAX = 999999;
    private int m_nAddScore;
    private int m_nScore;
    private MoguraView m_pView;

    public CScore(MoguraView pView) {
        this.m_pView = pView;
        this.m_nScore = 0;
        this.m_nAddScore = 0;
    }

    public void Exec(Canvas canvas) {
        if (this.m_nAddScore >= this.m_nScore) {
            this.m_nAddScore = this.m_nScore;
        } else if (this.m_nScore - this.m_nAddScore >= 3000) {
            this.m_nAddScore += 120;
        } else if (this.m_nScore - this.m_nAddScore >= 1000) {
            this.m_nAddScore += 40;
        } else if (this.m_nScore - this.m_nAddScore >= 500) {
            this.m_nAddScore += 20;
        } else if (this.m_nScore - this.m_nAddScore >= 100) {
            this.m_nAddScore += 4;
        } else if (this.m_nScore - this.m_nAddScore >= 50) {
            this.m_nAddScore += 2;
        } else {
            this.m_nAddScore++;
        }
        if (this.m_nAddScore < 0) {
            this.m_nAddScore = 0;
            this.m_nScore = 0;
        }
        if (this.m_nScore >= 999999) {
            this.m_nScore = 999999;
        }
        Draw(canvas);
    }

    public void Draw(Canvas canvas) {
        int GPosY = this.m_pView.game_rect.top;
        MoguraView moguraView = this.m_pView;
        this.m_pView.getClass();
        Bitmap Tex = moguraView.GetTex(77);
        int nTexW = Tex.getWidth();
        int nTexH = Tex.getHeight();
        canvas.drawBitmap(Tex, new Rect(0, 0, nTexW, nTexH), new Rect((int) (300.0f * this.m_pView.re_size_width), (int) (((float) (GPosY + 15)) * this.m_pView.re_size_height), ((int) (300.0f * this.m_pView.re_size_width)) + ((int) (((float) nTexW) * this.m_pView.re_size_width)), ((int) (((float) (GPosY + 15)) * this.m_pView.re_size_height)) + ((int) (((float) nTexH) * this.m_pView.re_size_height))), (Paint) null);
        int[] nScore = {(this.m_nAddScore % 1000000) / 100000, (this.m_nAddScore % 100000) / GAECommonData.SO_TIMEOUT, (this.m_nAddScore % GAECommonData.SO_TIMEOUT) / 1000, (this.m_nAddScore % 1000) / 100, (this.m_nAddScore % 100) / 10, this.m_nAddScore % 10};
        for (int i = 0; i < nScore.length; i++) {
            MoguraView moguraView2 = this.m_pView;
            this.m_pView.getClass();
            Bitmap Tex2 = moguraView2.GetTex(nScore[i] + 66);
            int nTexW2 = Tex2.getWidth();
            int nTexH2 = Tex2.getHeight();
            int nPosX2 = (i * 20) + 350;
            canvas.drawBitmap(Tex2, new Rect(0, 0, nTexW2, nTexH2), new Rect((int) (((float) nPosX2) * this.m_pView.re_size_width), (int) (((float) (GPosY + 42)) * this.m_pView.re_size_height), ((int) (((float) nPosX2) * this.m_pView.re_size_width)) + ((int) (((float) nTexW2) * this.m_pView.re_size_width)), ((int) (((float) (GPosY + 42)) * this.m_pView.re_size_height)) + ((int) (((float) nTexH2) * this.m_pView.re_size_height))), (Paint) null);
        }
    }

    public void Debug(Canvas canvas) {
        Paint paint = new Paint();
        paint.setColor(-1);
        paint.setTextSize(26.0f);
        canvas.drawText("SCORE : " + this.m_nScore, 0.0f, 150.0f, paint);
    }

    public void AddScore(int nVal) {
        this.m_nScore += nVal;
    }

    public int GetScore() {
        return this.m_nScore;
    }
}
