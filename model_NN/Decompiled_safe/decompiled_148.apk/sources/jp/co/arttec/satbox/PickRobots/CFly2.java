package jp.co.arttec.satbox.PickRobots;

/* compiled from: Fly2 */
class CFly2 extends CFlyBase {
    public CFly2(MoguraView pView, int nPosX, int nPosY, int nTexID) {
        super(pView, nPosX, nPosY, nTexID);
        if (this.m_nSize >= 0) {
            this.m_nSpd = -3;
        } else {
            this.m_nSpd = -2;
        }
    }
}
