package jp.co.arttec.satbox.PickRobots;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

/* compiled from: FlyBase */
class CFlyBase extends CTask {
    final int FLY_SIZE_WIDTH = 50;
    protected int m_nSize;
    protected int m_nSpd;
    protected Bitmap m_pTex;

    public CFlyBase(MoguraView pView, int nPosX, int nPosY, int nTexID) {
        super(pView);
        this.m_pTex = this.m_pView.GetTex(nTexID);
        this.m_rcRect = new Rect(nPosX, nPosY, (int) (((float) this.m_pTex.getWidth()) * this.m_pView.re_size_width), (int) (((float) this.m_pTex.getHeight()) * this.m_pView.re_size_height));
        this.m_nSpd = -1;
        this.m_nSize = 0;
    }

    public boolean Exec(Canvas canvas) {
        if (!Move()) {
            return false;
        }
        Draw(canvas);
        return true;
    }

    public boolean Move() {
        if (this.m_rcRect.left <= (-this.m_rcRect.right)) {
            return false;
        }
        this.m_rcRect.left += this.m_nSpd;
        return true;
    }

    public void Draw(Canvas canvas) {
        int nPosRight = this.m_rcRect.left + this.m_rcRect.right + this.m_nSize;
        int nPosBottom = this.m_rcRect.top + this.m_rcRect.bottom + this.m_nSize;
        canvas.drawBitmap(this.m_pTex, new Rect(0, 0, this.m_rcRect.right, this.m_rcRect.bottom), new Rect(this.m_rcRect.left, this.m_rcRect.top, nPosRight, nPosBottom), (Paint) null);
    }

    public void Debug(Canvas canvas) {
        Paint paint = new Paint();
        paint.setColor(-1);
        this.m_pView.getClass();
        paint.setTextSize(20.0f);
        canvas.drawText("<FLY Debug>", 0.0f, 230.0f, paint);
        canvas.drawText("m_rcRect.left : " + this.m_rcRect.left, 0.0f, 250.0f, paint);
    }
}
