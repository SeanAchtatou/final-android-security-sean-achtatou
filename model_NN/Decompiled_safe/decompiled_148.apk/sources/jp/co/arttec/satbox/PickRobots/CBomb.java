package jp.co.arttec.satbox.PickRobots;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.media.SoundPool;

/* compiled from: Bomb */
class CBomb extends CItemBase {
    final int BOMB_MISS_SCORE = -100;
    final int BOMB_SCORE = 100;
    final int BOMB_SPEED = 7;

    public CBomb(MoguraView pView, int nPosX, int nSpeed, int nTexIdx, int nSnd) {
        super(pView, nPosX, nTexIdx, nSnd);
        this.m_nSpeed = nSpeed + 7;
        this.m_nScore = 100;
        this.m_nMissScore = -100;
    }

    public boolean Exec(Canvas canvas) {
        float f = this.m_fRot + this.m_fAddRot;
        this.m_fRot = f;
        if (f >= 360.0f) {
            this.m_fRot -= 360.0f;
        }
        if (this.m_rcRect.top + this.m_rcRect.bottom >= this.m_pView.ground_y) {
            SoundPool GetSndPool = this.m_pView.GetSndPool();
            MoguraView moguraView = this.m_pView;
            this.m_pView.getClass();
            GetSndPool.play(moguraView.GetSnd(0), 1.0f, 1.0f, 0, 0, 1.0f);
            return false;
        } else if (HitCheckBox(this.m_pView.GetChar().GetRect())) {
            SoundPool GetSndPool2 = this.m_pView.GetSndPool();
            MoguraView moguraView2 = this.m_pView;
            this.m_pView.getClass();
            GetSndPool2.play(moguraView2.GetSnd(3), 1.0f, 1.0f, 0, 0, 1.0f);
            this.m_pView.GetScore().AddScore(this.m_nScore);
            this.m_pView.GetChar().AddAppleNum();
            this.m_pView.GetChar().AddFlashNum();
            return false;
        } else {
            if (this.m_nAnimCnt > 0) {
                if (this.m_nAnimCnt >= 10) {
                    return false;
                }
                if (this.m_nAnimCnt == 1) {
                    MoguraView moguraView3 = this.m_pView;
                    this.m_pView.getClass();
                    canvas.drawBitmap(moguraView3.GetTex(6), (float) this.m_rcRect.left, (float) this.m_rcRect.top, (Paint) null);
                }
                this.m_nAnimCnt++;
            } else if (this.m_nAnimCnt == 0) {
                Move();
                Draw(canvas);
            }
            return true;
        }
    }
}
