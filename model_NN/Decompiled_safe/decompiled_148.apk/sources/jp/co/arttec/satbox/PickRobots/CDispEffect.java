package jp.co.arttec.satbox.PickRobots;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

/* compiled from: DispEffect */
class CDispEffect {
    static final int DE_TYPE_FLASH = 1;
    static final int DE_TYPE_WIPE = 0;
    final int RECT_NUM = 30;
    private int m_nCnt;
    private int m_nType;
    private MoguraView m_pView;
    private Rect m_rcRect;

    public CDispEffect(MoguraView pView, int nType) {
        this.m_pView = pView;
        this.m_nType = nType;
        switch (this.m_nType) {
            case 0:
                this.m_rcRect = new Rect(0, 0, this.m_pView.game_rect.right, this.m_pView.game_rect.bottom / 30);
                break;
            case 1:
                this.m_rcRect = new Rect(0, 0, this.m_pView.game_rect.right, this.m_pView.game_rect.bottom);
                break;
        }
        this.m_nCnt = 0;
    }

    public boolean In(Canvas canvas) {
        Draw(canvas);
        Rect rect = this.m_rcRect;
        int i = rect.top + 3;
        rect.top = i;
        if (i < this.m_rcRect.bottom) {
            return true;
        }
        this.m_pView.SetState(eGAME_STATE.MAIN);
        return false;
    }

    public boolean Out(Canvas canvas) {
        Draw(canvas);
        Rect rect = this.m_rcRect;
        int i = rect.top - 3;
        rect.top = i;
        if (i <= this.m_rcRect.bottom - 75) {
            return false;
        }
        return true;
    }

    public boolean Flash(Canvas canvas) {
        int i = this.m_nCnt;
        this.m_nCnt = i + 1;
        if (i < 6) {
            if (this.m_nCnt % 2 == 0) {
                Draw(canvas);
            }
            return true;
        }
        this.m_nCnt = 0;
        return false;
    }

    public void Draw(Canvas canvas) {
        Paint paint = new Paint();
        switch (this.m_nType) {
            case 0:
                paint.setColor(-16777216);
                for (int i = 0; i < 30; i++) {
                    canvas.drawRect((float) this.m_rcRect.left, (float) (this.m_rcRect.top + (i * 30)), (float) this.m_rcRect.right, (float) (this.m_rcRect.bottom + (i * 30)), paint);
                }
                return;
            case 1:
                paint.setColor(-1);
                canvas.drawRect(this.m_rcRect, paint);
                return;
            default:
                return;
        }
    }
}
