package jp.co.arttec.satbox.PickRobots;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import jp.co.arttec.satbox.PickRobots.MoguraView;

public class Ninjya extends BaseActivity {
    private boolean flg_vibrator;
    /* access modifiers changed from: private */
    public int no_tmp = 0;
    private MoguraView.OnSettingListener onSettingListener = new MoguraView.OnSettingListener() {
        public void onSetting() {
            Ninjya.this.score = ((MoguraView) Ninjya.this.findViewById(R.id.play_view)).GetScore().GetScore();
            Intent intent = new Intent(Ninjya.this, GameOver.class);
            intent.putExtra("SCORE", Ninjya.this.score);
            intent.putExtra("CharNo", Ninjya.this.no_tmp);
            Ninjya.this.startActivity(intent);
            Ninjya.this.finish();
        }
    };
    /* access modifiers changed from: private */
    public int score = 0;
    private Vibrator vibrator;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.game);
        int char_no = getIntent().getExtras().getInt("CharNo");
        this.no_tmp = char_no;
        MoguraView view = (MoguraView) findViewById(R.id.play_view);
        view.setOnSettingListener(this.onSettingListener);
        view.setCharNo(char_no);
        this.vibrator = (Vibrator) getSystemService("vibrator");
        this.flg_vibrator = getSharedPreferences("prefkey", 0).getBoolean("vibflg", true);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        ((MoguraView) findViewById(R.id.play_view)).pauseSound();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        ((MoguraView) findViewById(R.id.play_view)).resumeSound();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        startActivity(new Intent(this, StageSelect.class));
        finish();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        MoguraView view = (MoguraView) findViewById(R.id.play_view);
        view.stopSound();
        view.destroyBGM();
        view.destroyBITMAP();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        this.flg_vibrator = getSharedPreferences("prefkey", 0).getBoolean("vibflg", true);
        if (this.flg_vibrator) {
            menu.findItem(R.id.menu_vib_switch).setTitle("Vibrator(Off)");
        } else {
            menu.findItem(R.id.menu_vib_switch).setTitle("Vibrator(On)");
        }
        return super.onPrepareOptionsMenu(menu);
    }

    public synchronized boolean onMenuItemSelected(int featureId, MenuItem item) {
        boolean result;
        result = super.onMenuItemSelected(featureId, item);
        if (item.getItemId() == R.id.menu_vib_switch) {
            this.flg_vibrator = !this.flg_vibrator;
            SharedPreferences.Editor editor = getSharedPreferences("prefkey", 0).edit();
            editor.putBoolean("vibflg", this.flg_vibrator);
            editor.commit();
            if (this.flg_vibrator) {
                this.vibrator.vibrate(100);
            }
        } else if (item.getItemId() == R.id.menu_apk) {
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=pub:SAT-BOX")));
        } else if (item.getItemId() == R.id.menu_hp) {
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://www.arttec.co.jp/sat-box/")));
        } else if (item.getItemId() == R.id.menu_end) {
            finish();
        }
        return result;
    }
}
