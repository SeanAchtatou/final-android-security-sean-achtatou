package jp.co.arttec.satbox.PickRobots;

/* compiled from: Fly1 */
class CFly1 extends CFlyBase {
    public CFly1(MoguraView pView, int nPosX, int nPosY, int nTexID) {
        super(pView, nPosX, nPosY, nTexID);
        if (this.m_nSize >= 0) {
            this.m_nSpd = -2;
        } else {
            this.m_nSpd = -1;
        }
    }
}
