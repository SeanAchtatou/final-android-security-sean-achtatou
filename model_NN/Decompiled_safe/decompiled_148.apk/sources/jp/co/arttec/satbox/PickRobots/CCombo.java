package jp.co.arttec.satbox.PickRobots;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

/* compiled from: Combo */
class CCombo {
    private static Boolean[] bCmbFlg;
    private static int m_nCmbCnt;
    final int CMB_ALPHA_ADD = 10;
    final int CMB_ALPHA_START = 64;
    final int CMB_BNS_TEXT_SIZE = 18;
    final int CMB_SIZE_ADD = 3;
    final int CMB_TEXT_SIZE = 26;
    final int CMB_VAL_SIZE = 40;
    private boolean m_bBnsFlg;
    private int m_nAlpha;
    private int m_nAlphaCnt;
    private int m_nCmbBns;
    private int m_nSize;
    private MoguraView m_pView;

    public CCombo(MoguraView pView) {
        this.m_pView = pView;
        this.m_nSize = 40;
        this.m_nAlpha = 64;
        this.m_nAlphaCnt = 0;
        this.m_nCmbBns = 0;
        this.m_bBnsFlg = false;
        bCmbFlg = new Boolean[10];
        for (int i = 0; i < bCmbFlg.length; i++) {
            bCmbFlg[i] = false;
        }
    }

    public void Exec(Canvas canvas) {
        int[] nCnt = {10, 30, 50, 80, 100, 120, 150, 180, 200, 300};
        for (int i = 0; i < bCmbFlg.length; i++) {
            if (m_nCmbCnt >= nCnt[i] && !bCmbFlg[i].booleanValue()) {
                this.m_nCmbBns = nCnt[i];
                this.m_pView.GetScore().AddScore(this.m_nCmbBns);
                bCmbFlg[i] = true;
                this.m_bBnsFlg = true;
            }
        }
        if (m_nCmbCnt < 10) {
            for (int i2 = 0; i2 < bCmbFlg.length; i2++) {
                bCmbFlg[i2] = false;
            }
        }
        int i3 = this.m_nAlpha + 10;
        this.m_nAlpha = i3;
        if (i3 >= 510) {
            this.m_nAlpha = 510;
            this.m_pView.SetCmbFlg(false);
            this.m_bBnsFlg = false;
            this.m_nAlphaCnt = 0;
        }
        int i4 = this.m_nSize - 3;
        this.m_nSize = i4;
        if (i4 <= 0) {
            this.m_nSize = 0;
        }
        Draw(canvas);
    }

    public void Draw(Canvas canvas) {
        int i;
        int i2;
        int GPosY = this.m_pView.game_rect.top;
        MoguraView moguraView = this.m_pView;
        this.m_pView.getClass();
        Bitmap pTex = moguraView.GetTex(76);
        int nTexW = pTex.getWidth();
        int nTexH = pTex.getHeight();
        int nPosY = 80;
        Rect rect = new Rect(0, 0, nTexW, nTexH);
        Rect rect2 = new Rect((int) (((float) 65) * this.m_pView.re_size_width), (int) (((float) (80 + GPosY)) * this.m_pView.re_size_height), ((int) (((float) 65) * this.m_pView.re_size_width)) + ((int) (((float) nTexW) * this.m_pView.re_size_width)), ((int) (((float) (80 + GPosY)) * this.m_pView.re_size_height)) + ((int) (((float) nTexH) * this.m_pView.re_size_height)));
        Paint paint = new Paint();
        if (this.m_nAlpha <= 255) {
            i = this.m_nAlpha;
        } else {
            i = 255 - (this.m_nAlpha - 255);
        }
        paint.setAlpha(i);
        canvas.drawBitmap(pTex, rect, rect2, paint);
        int[] nCmb = {(m_nCmbCnt % 1000) / 100, (m_nCmbCnt % 100) / 10, m_nCmbCnt % 10};
        boolean bDraw = false;
        for (int i3 = 0; i3 < nCmb.length; i3++) {
            MoguraView moguraView2 = this.m_pView;
            this.m_pView.getClass();
            Bitmap pTex2 = moguraView2.GetTex(nCmb[i3] + 66);
            int nTexW2 = pTex2.getWidth();
            int nTexH2 = pTex2.getHeight();
            int nPosX2 = i3 * 20;
            nPosY = 80;
            Rect rect3 = new Rect(0, 0, nTexW2, nTexH2);
            Rect rect4 = new Rect((int) (((float) nPosX2) * this.m_pView.re_size_width), (int) (((float) (80 + GPosY)) * this.m_pView.re_size_height), ((int) (((float) nPosX2) * this.m_pView.re_size_width)) + ((int) (((float) (this.m_nSize + nTexW2)) * this.m_pView.re_size_width)), ((int) (((float) (80 + GPosY)) * this.m_pView.re_size_height)) + ((int) (((float) (this.m_nSize + nTexH2)) * this.m_pView.re_size_height)));
            if (this.m_nAlpha <= 255) {
                i2 = this.m_nAlpha;
            } else {
                i2 = 255 - (this.m_nAlpha - 255);
            }
            paint.setAlpha(i2);
            if (nCmb[i3] > 0) {
                bDraw = true;
            }
            if (bDraw) {
                canvas.drawBitmap(pTex2, rect3, rect4, paint);
            }
        }
        if (this.m_bBnsFlg) {
            int i4 = this.m_nAlphaCnt;
            this.m_nAlphaCnt = i4 + 1;
            if (i4 % 4 < 2) {
                MoguraView moguraView3 = this.m_pView;
                this.m_pView.getClass();
                Bitmap pTex3 = moguraView3.GetTex(77);
                int nTexW3 = pTex3.getWidth();
                int nTexH3 = pTex3.getHeight();
                int nPosX = 65 - 25;
                canvas.drawBitmap(pTex3, new Rect(0, 0, nTexW3, nTexH3), new Rect((int) (((float) nPosX) * this.m_pView.re_size_width), (int) (((float) (GPosY + 120)) * this.m_pView.re_size_height), ((int) (((float) nPosX) * this.m_pView.re_size_width)) + ((int) (((float) nTexW3) * this.m_pView.re_size_width)), ((int) (((float) (GPosY + 120)) * this.m_pView.re_size_height)) + ((int) (((float) nTexH3) * this.m_pView.re_size_height))), (Paint) null);
                int nPosX3 = nTexW3 + 10 + 40;
                MoguraView moguraView4 = this.m_pView;
                this.m_pView.getClass();
                Bitmap pTex4 = moguraView4.GetTex(78);
                int nTexW4 = pTex4.getWidth();
                int nTexH4 = pTex4.getHeight();
                canvas.drawBitmap(pTex4, new Rect(0, 0, nTexW4, nTexH4), new Rect((int) (((float) nPosX3) * this.m_pView.re_size_width), (int) (((float) (GPosY + 115 + 10)) * this.m_pView.re_size_height), ((int) (((float) nPosX3) * this.m_pView.re_size_width)) + ((int) (((float) nTexW4) * this.m_pView.re_size_width)), ((int) (((float) (GPosY + 115 + 10)) * this.m_pView.re_size_height)) + ((int) (((float) nTexH4) * this.m_pView.re_size_height))), (Paint) null);
                int[] nCmbBns = {(this.m_nCmbBns % 1000) / 100, (this.m_nCmbBns % 100) / 10, this.m_nCmbBns % 10};
                boolean bDraw2 = false;
                int nPosY2 = ((nPosY + 40) - 5) + 30;
                for (int i5 = 0; i5 < nCmbBns.length; i5++) {
                    MoguraView moguraView5 = this.m_pView;
                    this.m_pView.getClass();
                    Bitmap pTex5 = moguraView5.GetTex(nCmbBns[i5] + 66);
                    int nTexW5 = pTex5.getWidth();
                    int nTexH5 = pTex5.getHeight();
                    nPosX3 += nTexW5;
                    Rect rect5 = new Rect(0, 0, nTexW5, nTexH5);
                    Rect rect6 = new Rect((int) (((float) nPosX3) * this.m_pView.re_size_width), (int) (((float) ((GPosY + 145) - 20)) * this.m_pView.re_size_height), ((int) (((float) nPosX3) * this.m_pView.re_size_width)) + ((int) (((float) nTexW5) * this.m_pView.re_size_width)), ((int) (((float) ((GPosY + 145) - 20)) * this.m_pView.re_size_height)) + ((int) (((float) nTexH5) * this.m_pView.re_size_height)));
                    if (nCmbBns[i5] > 0) {
                        bDraw2 = true;
                    }
                    if (bDraw2) {
                        canvas.drawBitmap(pTex5, rect5, rect6, (Paint) null);
                    }
                }
            }
        }
    }

    public void Debug(Canvas canvas) {
        Paint paint = new Paint();
        paint.setColor(-1);
        this.m_pView.getClass();
        paint.setTextSize(20.0f);
        canvas.drawText("<Combo Debug>", 0.0f, 230.0f, paint);
        canvas.drawText("m_nAlpha : " + this.m_nAlpha, 0.0f, 250.0f, paint);
    }

    public static void SetCmbCnt(int nVal) {
        m_nCmbCnt = nVal;
    }

    public static int GetCmbCnt() {
        return m_nCmbCnt;
    }
}
