package jp.co.arttec.satbox.PickRobots;

/* compiled from: Doril */
class CDoril extends CDangerBase {
    final int DORIL_DMG = 40;
    final int DORIL_SCORE = 15;
    final int DORIL_SPEED = 3;

    public CDoril(MoguraView pView, int nPosX, int nSpeed, int nTexIdx, int nSnd) {
        super(pView, nPosX, nTexIdx, nSnd);
        CHitPoint HP = this.m_pView.GetHP();
        this.m_nDmgLen = (HP.GetMetaW() / HP.GetMaxHp()) * 40;
        this.m_nDmg = 40;
        this.m_nSpeed = nSpeed + 3;
        this.m_nScore = 15;
    }
}
