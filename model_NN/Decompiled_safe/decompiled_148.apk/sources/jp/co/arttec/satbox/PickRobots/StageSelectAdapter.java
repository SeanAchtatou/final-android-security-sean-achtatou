package jp.co.arttec.satbox.PickRobots;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;

public class StageSelectAdapter extends BaseAdapter {
    private static final int DIP_X = 200;
    private static final int DIP_Y = 200;
    private static Bitmap[] _bitmap;
    private static final int[] _imageID = {R.drawable.select_robo3, R.drawable.select_robo1, R.drawable.select_robo2};
    private Context _context;
    private int _imageBackground;
    private int _pixel_X;
    private int _pixel_Y;

    public StageSelectAdapter(Context context) {
        this._context = context;
        float scale = context.getResources().getDisplayMetrics().density;
        this._pixel_X = (int) ((200.0f * scale) + 0.5f);
        this._pixel_Y = (int) ((200.0f * scale) + 0.5f);
        loadBitmap(context);
        TypedArray typArray = context.obtainStyledAttributes(R.styleable.default_gallery);
        this._imageBackground = typArray.getResourceId(0, 0);
        typArray.recycle();
    }

    public int getCount() {
        return _imageID.length;
    }

    public Object getItem(int position) {
        return Integer.valueOf(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imgView = new ImageView(this._context);
        imgView.setImageBitmap(_bitmap[position]);
        imgView.setLayoutParams(new Gallery.LayoutParams(this._pixel_X, this._pixel_Y));
        imgView.setScaleType(ImageView.ScaleType.FIT_XY);
        imgView.setBackgroundResource(this._imageBackground);
        return imgView;
    }

    private void loadBitmap(Context context) {
        Resources res = context.getResources();
        if (_bitmap == null) {
            _bitmap = new Bitmap[_imageID.length];
        }
        for (int i = 0; i < _bitmap.length; i++) {
            BitmapFactory.Options option = new BitmapFactory.Options();
            option.inJustDecodeBounds = true;
            BitmapFactory.decodeResource(res, _imageID[i], option);
            int scale = Math.max((option.outWidth / this._pixel_X) + 1, (option.outHeight / this._pixel_Y) + 1);
            option.inJustDecodeBounds = false;
            option.inSampleSize = scale;
            _bitmap[i] = BitmapFactory.decodeResource(context.getResources(), _imageID[i], option);
        }
    }

    public void recycle() {
        for (int i = 0; i < _bitmap.length; i++) {
            if (!_bitmap[i].isRecycled()) {
                _bitmap[i].recycle();
            }
        }
        System.gc();
    }
}
