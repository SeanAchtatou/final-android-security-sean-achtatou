package jp.co.arttec.satbox.PickRobots;

import android.app.Activity;
import android.content.Intent;
import android.media.SoundPool;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;
import jp.co.arttec.satbox.scoreranklib.GetScoreController;
import jp.co.arttec.satbox.scoreranklib.HttpCommunicationListener;
import jp.co.arttec.satbox.scoreranklib.Score;

public class Rank2 extends Activity {
    /* access modifiers changed from: private */
    public GetScoreController controller;
    /* access modifiers changed from: private */
    public boolean flg_end = false;
    private int intGetLevel = 48;
    /* access modifiers changed from: private */
    public int intRankno;
    private ListView mListView;
    /* access modifiers changed from: private */
    public int soundIds;
    /* access modifiers changed from: private */
    public SoundPool soundPool;
    /* access modifiers changed from: private */
    public float spVolume = 0.5f;
    /* access modifiers changed from: private */
    public String strItem;
    /* access modifiers changed from: private */
    public TextView txtRankName;
    private TextView txtRankTouch;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.rank);
        this.mListView = (ListView) findViewById(R.id.ranklistview);
        this.mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                if (!Rank2.this.flg_end) {
                    Rank2.this.flg_end = true;
                    Rank2.this.soundPool.play(Rank2.this.soundIds, Rank2.this.spVolume, Rank2.this.spVolume, 1, 0, 1.0f);
                    Rank2.this.startActivity(new Intent(Rank2.this, Title.class));
                    Rank2.this.finish();
                }
            }
        });
        this.txtRankTouch = new TextView(getApplication());
        this.txtRankTouch = (TextView) findViewById(R.id.rank_tx_touch);
        this.txtRankTouch.setVisibility(8);
        this.soundPool = new SoundPool(1, 3, 0);
        this.soundIds = this.soundPool.load(this, R.raw.se_title, 1);
        onScoreEntry();
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.soundPool != null) {
            this.soundPool.stop(this.soundIds);
            this.soundPool.release();
        }
    }

    private synchronized void onScoreEntry() {
        this.controller = new GetScoreController(this.intGetLevel);
        this.controller.setActivity(this);
        this.controller.setRank(99);
        this.controller.setReverse(false);
        this.controller.setOnFinishListener(new HttpCommunicationListener() {
            public void onFinish(boolean result) {
                Rank2.this.txtRankName = (TextView) Rank2.this.findViewById(R.id.rankname);
                Rank2.this.txtRankName.setText("World Ranking");
                if (result) {
                    Toast toast = Toast.makeText(Rank2.this, "High score acquisition completion.", 1);
                    List<Score> _score = Rank2.this.controller.getHighScoreList();
                    if (_score != null) {
                        List<RankStrDelivery> dataList = new ArrayList<>();
                        int j = 0;
                        for (Score data : _score) {
                            Rank2.this.intRankno = j + 1;
                            Rank2.this.strItem = String.valueOf(data.getScore());
                            dataList.add(new RankStrDelivery(Rank2.this.intRankno, data.getName(), Rank2.this.strItem, ""));
                            j++;
                        }
                        ((ListView) Rank2.this.findViewById(R.id.ranklistview)).setAdapter((ListAdapter) new RankAdapter(Rank2.this, R.layout.rankrow2, dataList));
                    }
                    toast.show();
                    return;
                }
                Rank2.this.finish();
            }
        });
        this.controller.getHighScore();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        this.soundPool.play(this.soundIds, this.spVolume, this.spVolume, 1, 0, 1.0f);
        startActivity(new Intent(this, Rank.class));
        finish();
        return true;
    }

    public synchronized boolean onTouchEvent(MotionEvent event) {
        boolean z;
        if (!this.flg_end) {
            this.flg_end = true;
            if (event.getAction() != 0) {
                z = false;
            } else {
                this.soundPool.play(this.soundIds, this.spVolume, this.spVolume, 1, 0, 1.0f);
                startActivity(new Intent(this, Title.class));
                finish();
                z = true;
            }
        } else {
            z = false;
        }
        return z;
    }
}
