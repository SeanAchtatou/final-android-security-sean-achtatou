package jp.co.arttec.satbox.PickRobots;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.media.SoundPool;

/* compiled from: ItemBase */
class CItemBase extends CFall {
    protected int m_nMissScore = 0;

    public CItemBase(MoguraView pView, int nPosX, int nTexIdx, int nSnd) {
        super(pView, nPosX, nTexIdx, nSnd);
        this.m_nAnimCnt = 0;
        this.m_nScore = 0;
    }

    public boolean Exec(Canvas canvas) {
        float f = this.m_fRot + this.m_fAddRot;
        this.m_fRot = f;
        if (f >= 360.0f) {
            this.m_fRot -= 360.0f;
        }
        if (this.m_rcRect.top + this.m_rcRect.bottom >= this.m_pView.ground_y) {
            SoundPool GetSndPool = this.m_pView.GetSndPool();
            MoguraView moguraView = this.m_pView;
            this.m_pView.getClass();
            GetSndPool.play(moguraView.GetSnd(5), 1.0f, 1.0f, 0, 0, 1.0f);
            return false;
        }
        Rect rcChar = new Rect(this.m_pView.GetChar().GetRect());
        rcChar.top -= 50;
        rcChar.bottom += 50;
        if (HitCheckBox(rcChar)) {
            this.m_pView.GetChar().EatStart();
        }
        if (HitCheckBox(this.m_pView.GetChar().GetRect())) {
            SoundPool GetSndPool2 = this.m_pView.GetSndPool();
            MoguraView moguraView2 = this.m_pView;
            this.m_pView.getClass();
            GetSndPool2.play(moguraView2.GetSnd(3), 1.0f, 1.0f, 0, 0, 1.0f);
            this.m_pView.GetScore().AddScore(this.m_nScore);
            this.m_pView.GetChar().AddAppleNum();
            this.m_pView.GetChar().EatStart();
            return false;
        }
        if (this.m_nAnimCnt > 0) {
            if (this.m_nAnimCnt >= 10) {
                return false;
            }
            if (this.m_nAnimCnt == 1) {
                MoguraView moguraView3 = this.m_pView;
                this.m_pView.getClass();
                canvas.drawBitmap(moguraView3.GetTex(6), (float) this.m_rcRect.left, (float) this.m_rcRect.top, (Paint) null);
            }
            this.m_nAnimCnt++;
        } else if (this.m_nAnimCnt == 0) {
            Move();
            Draw(canvas);
        }
        return true;
    }

    public boolean Touch(int nX, int nY) {
        if (!HitCheckDot(nX, nY)) {
            return false;
        }
        this.m_pView.GetSndPool().play(this.m_pView.GetSnd(this.m_nSnd), 1.0f, 1.0f, 0, 0, 1.0f);
        this.m_pView.GetScore().AddScore(this.m_nMissScore);
        CCombo.SetCmbCnt(0);
        this.m_pView.SetCmbFlg(false);
        this.m_nAnimCnt++;
        return true;
    }
}
