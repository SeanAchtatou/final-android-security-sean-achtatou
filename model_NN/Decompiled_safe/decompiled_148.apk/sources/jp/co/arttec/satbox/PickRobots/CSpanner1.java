package jp.co.arttec.satbox.PickRobots;

/* compiled from: Spanner1 */
class CSpanner1 extends CDangerBase {
    final int SPANNER1_ALPHA = 128;
    int SPANNER1_DMG = 40;
    final int SPANNER1_SCORE = 5;
    final int SPANNER1_SPEED = 5;

    public CSpanner1(MoguraView pView, int nPosX, int nSpeed, int nTexIdx, int nSnd) {
        super(pView, nPosX, nTexIdx, nSnd);
        CHitPoint HP = this.m_pView.GetHP();
        this.m_nDmgLen = (HP.GetMetaW() / HP.GetMaxHp()) * this.SPANNER1_DMG;
        if (this.m_pView.GetLevel() >= 5) {
            this.m_nAlpha = Math.random() % 2.0d == 0.0d ? 255 : 128;
        }
        switch (this.m_pView.char_no) {
            case 0:
                this.SPANNER1_DMG = 20;
                break;
            case R.styleable.mediba_ad_sdk_android_MasAdView_requestInterval /*1*/:
                this.SPANNER1_DMG = 30;
                break;
            case R.styleable.mediba_ad_sdk_android_MasAdView_refreshAnimation /*2*/:
                this.SPANNER1_DMG = 40;
                break;
        }
        this.m_nDmg = this.SPANNER1_DMG;
        this.m_nSpeed = nSpeed + 5;
        this.m_nScore = 5;
    }
}
