package jp.co.arttec.satbox.PickRobots;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

/* compiled from: HitPoint */
class CHitPoint {
    private static /* synthetic */ int[] $SWITCH_TABLE$jp$co$arttec$satbox$PickRobots$eHP_STATE;
    int HP_MAX = 0;
    int HP_META_POSX = 25;
    int HP_META_POSY = 30;
    int HP_VAL_SIZE = 15;
    private Rect back_rect;
    private CDmgBar m_DB;
    private eHP_STATE m_eState;
    private int m_nHP;
    private int m_nHpDmgLen;
    private int m_nMetaH;
    private int m_nMetaW;
    private MoguraView m_pView;
    private Rect m_rcRect;

    static /* synthetic */ int[] $SWITCH_TABLE$jp$co$arttec$satbox$PickRobots$eHP_STATE() {
        int[] iArr = $SWITCH_TABLE$jp$co$arttec$satbox$PickRobots$eHP_STATE;
        if (iArr == null) {
            iArr = new int[eHP_STATE.values().length];
            try {
                iArr[eHP_STATE.DMG.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[eHP_STATE.NORMAL.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            $SWITCH_TABLE$jp$co$arttec$satbox$PickRobots$eHP_STATE = iArr;
        }
        return iArr;
    }

    /* compiled from: HitPoint */
    class CDmgBar {
        private double m_dwRad = 3.141592653589793d;
        private int m_nDmg;
        private int m_nStart;
        private CHitPoint m_pHP;
        private Rect m_rcRect;

        public CDmgBar(CHitPoint pHP, int nStart, int nDmg) {
            this.m_pHP = pHP;
            this.m_rcRect = new Rect(nStart - nDmg, pHP.GetPos().top, nStart, pHP.GetPos().bottom);
            this.m_nStart = nStart;
            this.m_nDmg = nDmg;
        }

        public void Exec(Canvas canvas) {
            this.m_rcRect.right = this.m_nStart + ((int) (Math.sin(this.m_dwRad) * ((double) this.m_nDmg)));
            double d = this.m_dwRad + 0.20000000298023224d;
            this.m_dwRad = d;
            if (d >= 4.71238898038469d) {
                this.m_dwRad = 4.71238898038469d;
                this.m_pHP.SetState(eHP_STATE.NORMAL);
            }
            Draw(canvas);
        }

        public void Draw(Canvas canvas) {
            Paint paint = new Paint();
            paint.setColor(-65536);
            canvas.drawRect(this.m_rcRect, paint);
        }
    }

    public CHitPoint(MoguraView pView) {
        this.m_pView = pView;
        this.m_eState = eHP_STATE.NORMAL;
        MoguraView moguraView = this.m_pView;
        this.m_pView.getClass();
        this.m_nMetaW = (int) (((float) moguraView.GetTex(14).getWidth()) * this.m_pView.re_size_width);
        MoguraView moguraView2 = this.m_pView;
        this.m_pView.getClass();
        this.m_nMetaH = (int) (((float) moguraView2.GetTex(14).getHeight()) * this.m_pView.re_size_height);
        this.m_nHP = this.m_nMetaW;
        this.HP_META_POSX = (int) (25.0f * this.m_pView.re_size_width);
        this.HP_META_POSY = (int) (101.0f * this.m_pView.re_size_height);
        this.HP_MAX = this.m_nHP;
        this.m_rcRect = new Rect(this.HP_META_POSX, this.HP_META_POSY, this.m_nMetaW + this.HP_META_POSX, this.m_nMetaH + this.HP_META_POSY);
        MoguraView moguraView3 = this.m_pView;
        this.m_pView.getClass();
        MoguraView moguraView4 = this.m_pView;
        this.m_pView.getClass();
        this.back_rect = new Rect((int) (this.m_pView.re_size_width * 10.0f), (int) (this.m_pView.re_size_height * 82.0f), (int) ((this.m_pView.re_size_width * 10.0f) + (((float) moguraView3.GetTex(13).getWidth()) * this.m_pView.re_size_width)), (int) ((this.m_pView.re_size_height * 82.0f) + (((float) moguraView4.GetTex(13).getHeight()) * this.m_pView.re_size_height)));
        this.m_nHpDmgLen = 0;
    }

    public void Exec(Canvas canvas) {
        Draw(canvas);
        switch ($SWITCH_TABLE$jp$co$arttec$satbox$PickRobots$eHP_STATE()[this.m_eState.ordinal()]) {
            case R.styleable.mediba_ad_sdk_android_MasAdView_requestInterval /*1*/:
            default:
                return;
            case R.styleable.mediba_ad_sdk_android_MasAdView_refreshAnimation /*2*/:
                this.m_DB.Exec(canvas);
                return;
        }
    }

    public void Draw(Canvas canvas) {
        MoguraView moguraView = this.m_pView;
        this.m_pView.getClass();
        int width = moguraView.GetTex(13).getWidth();
        MoguraView moguraView2 = this.m_pView;
        this.m_pView.getClass();
        Rect meta1_src = new Rect(0, 0, width, moguraView2.GetTex(13).getHeight());
        MoguraView moguraView3 = this.m_pView;
        this.m_pView.getClass();
        canvas.drawBitmap(moguraView3.GetTex(13), meta1_src, this.back_rect, (Paint) null);
        MoguraView moguraView4 = this.m_pView;
        this.m_pView.getClass();
        int width2 = moguraView4.GetTex(14).getWidth();
        MoguraView moguraView5 = this.m_pView;
        this.m_pView.getClass();
        Rect src = new Rect(0, 0, width2, moguraView5.GetTex(14).getHeight());
        Rect dst = new Rect(this.m_rcRect.left, this.m_rcRect.top, this.m_rcRect.right - this.m_nHpDmgLen, this.m_rcRect.bottom);
        MoguraView moguraView6 = this.m_pView;
        this.m_pView.getClass();
        canvas.drawBitmap(moguraView6.GetTex(14), src, dst, (Paint) null);
    }

    public void Debug(Canvas canvas) {
        Paint paint = new Paint();
        paint.setColor(-1);
        this.m_pView.getClass();
        paint.setTextSize(20.0f);
        canvas.drawText("m_nMetaW : " + this.m_nMetaW, 0.0f, 250.0f, paint);
        canvas.drawText("m_eState : " + this.m_eState, 0.0f, 280.0f, paint);
    }

    public void Dmg(int nDmg) {
        if (this.m_nHP < nDmg) {
            nDmg -= this.m_nHP;
        }
        this.m_DB = new CDmgBar(this, this.m_rcRect.right - this.m_nHpDmgLen, nDmg);
        this.m_nHpDmgLen += nDmg;
        this.m_nHP -= nDmg;
        if (this.m_nHP <= 0) {
            this.m_nHP = 0;
        }
        this.m_eState = eHP_STATE.DMG;
    }

    public void SetState(eHP_STATE nVal) {
        this.m_eState = nVal;
    }

    public void SetHP(int nVal) {
        this.m_nHP = nVal;
    }

    public void SetHpDmgLen(int nVal) {
        this.m_nHpDmgLen = nVal;
    }

    public int GetHP() {
        return this.m_nHP;
    }

    public int GetMaxHp() {
        return this.HP_MAX;
    }

    public int GetHpDmgLen() {
        return this.m_nHpDmgLen;
    }

    public Rect GetPos() {
        return this.m_rcRect;
    }

    public int GetMetaW() {
        return this.m_nMetaW;
    }

    public CDmgBar GetDmgBar() {
        return this.m_DB;
    }
}
