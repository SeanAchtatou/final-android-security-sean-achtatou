package jp.co.arttec.satbox.PickRobots;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

public class Title extends BaseActivity {
    private static final int GAME_CNT = 1;
    private MediaPlayer _mediaPlayer;
    private Button btn_howto;
    /* access modifiers changed from: private */
    public Button btn_rank;
    private Button[] btn_start = new Button[1];
    private boolean flg_vibrator;
    /* access modifiers changed from: private */
    public int soundIds;
    /* access modifiers changed from: private */
    public SoundPool soundPool;
    private Vibrator vibrator;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.title);
        this.soundPool = new SoundPool(1, 3, 0);
        this.soundIds = this.soundPool.load(this, R.raw.start, 1);
        int[] button_id = {R.id.title_ranking};
        final int[] button_alpha_id = {R.drawable.ranking1};
        for (int i = 0; i < 1; i++) {
            this.btn_start[i] = new Button(this);
            this.btn_start[i] = (Button) findViewById(button_id[i]);
        }
        this.btn_rank = new Button(this);
        this.btn_rank = (Button) findViewById(R.id.title_ranking);
        Log.d("create", "");
        this.btn_rank.setOnClickListener(new View.OnClickListener() {
            public synchronized void onClick(View v) {
                Title.this.btn_rank.setBackgroundResource(button_alpha_id[0]);
                Title.this.soundPool.play(Title.this.soundIds, 0.99f, 0.99f, 1, 0, 1.0f);
                Title.this.startActivity(new Intent(Title.this, Rank.class));
                Title.this.stopSound();
                Title.this.finish();
            }
        });
        this.vibrator = (Vibrator) getSystemService("vibrator");
        this.flg_vibrator = getSharedPreferences("prefkey", 0).getBoolean("vibflg", true);
        this._mediaPlayer = null;
        playSound();
        this.btn_howto = (Button) findViewById(R.id.title_howto);
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle((int) R.string.howto_button);
        builder.setMessage((int) R.string.howto_str);
        builder.setPositiveButton((int) R.string.close, (DialogInterface.OnClickListener) null);
        this.btn_howto.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                builder.create();
                builder.show();
            }
        });
    }

    public synchronized boolean onTouchEvent(MotionEvent event) {
        boolean z;
        if (event.getAction() != 0) {
            z = false;
        } else {
            this.soundPool.play(this.soundIds, 0.99f, 0.99f, 1, 0, 1.0f);
            startActivity(new Intent(this, StageSelect.class));
            finish();
            z = true;
        }
        return z;
    }

    public void onClickGamestart(View v) {
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=pub:SAT-BOX")));
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this._mediaPlayer != null && this._mediaPlayer.isPlaying()) {
            this._mediaPlayer.pause();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this._mediaPlayer != null) {
            this._mediaPlayer.start();
        } else if (this._mediaPlayer == null) {
            playSound();
        }
    }

    /* access modifiers changed from: private */
    public void stopSound() {
        if (this._mediaPlayer != null) {
            if (this._mediaPlayer.isPlaying()) {
                this._mediaPlayer.stop();
            }
            this._mediaPlayer.release();
            this._mediaPlayer = null;
        }
    }

    private void playSound() {
        stopSound();
        this._mediaPlayer = MediaPlayer.create(this, (int) R.raw.main);
        this._mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
                mp.seekTo(0);
                mp.start();
            }
        });
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        this.flg_vibrator = getSharedPreferences("prefkey", 0).getBoolean("vibflg", true);
        if (this.flg_vibrator) {
            menu.findItem(R.id.menu_vib_switch).setTitle("Vibrator(Off)");
        } else {
            menu.findItem(R.id.menu_vib_switch).setTitle("Vibrator(On)");
        }
        return super.onPrepareOptionsMenu(menu);
    }

    public synchronized boolean onMenuItemSelected(int featureId, MenuItem item) {
        boolean result;
        result = super.onMenuItemSelected(featureId, item);
        if (item.getItemId() == R.id.menu_vib_switch) {
            this.flg_vibrator = !this.flg_vibrator;
            SharedPreferences.Editor editor = getSharedPreferences("prefkey", 0).edit();
            editor.putBoolean("vibflg", this.flg_vibrator);
            editor.commit();
            if (this.flg_vibrator) {
                this.vibrator.vibrate(100);
            }
        } else if (item.getItemId() == R.id.menu_apk) {
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=pub:SAT-BOX")));
        } else if (item.getItemId() == R.id.menu_hp) {
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://www.arttec.co.jp/sat-box/")));
        } else if (item.getItemId() == R.id.menu_end) {
            finish();
        }
        return result;
    }
}
