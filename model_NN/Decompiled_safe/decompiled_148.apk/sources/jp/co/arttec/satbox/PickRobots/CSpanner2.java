package jp.co.arttec.satbox.PickRobots;

/* compiled from: Spanner2 */
class CSpanner2 extends CDangerBase {
    int SPANNER2_DMG = 40;
    final int SPANNER2_HP = 2;
    final int SPANNER2_SCORE = 30;
    final int SPANNER2_SPEED = 2;
    private int m_nVecX;

    public CSpanner2(MoguraView pView, int nPosX, int nSpeed, int nTexIdx, int nSnd) {
        super(pView, nPosX, nTexIdx, nSnd);
        int nRnd;
        CHitPoint HP = this.m_pView.GetHP();
        this.m_nDmgLen = (HP.GetMetaW() / HP.GetMaxHp()) * this.SPANNER2_DMG;
        switch (this.m_pView.char_no) {
            case 0:
                this.SPANNER2_DMG = 20;
                break;
            case R.styleable.mediba_ad_sdk_android_MasAdView_requestInterval /*1*/:
                this.SPANNER2_DMG = 30;
                break;
            case R.styleable.mediba_ad_sdk_android_MasAdView_refreshAnimation /*2*/:
                this.SPANNER2_DMG = 40;
                break;
        }
        this.m_nDmg = this.SPANNER2_DMG;
        this.m_nSpeed = nSpeed + 2;
        this.m_nScore = 30;
        this.m_fAddRot = (((float) Math.random()) * 6.0f) + 6.0f;
        if (((int) Math.random()) == 0) {
            nRnd = 1;
        } else {
            nRnd = -1;
        }
        this.m_fAddRot *= (float) nRnd;
        this.m_nHP = 2;
        this.m_nVecX = ((int) (Math.random() * 3.0d)) + 6;
    }

    public void Move() {
        if (this.m_rcRect.left < 0 || this.m_rcRect.left + this.m_rcRect.right >= this.m_pView.GetDispX()) {
            this.m_nVecX = -this.m_nVecX;
        }
        this.m_rcRect.left += this.m_nVecX;
        this.m_rcRect.top += this.m_nSpeed;
        this.m_rcRect.top = Math.min(this.m_pView.GetDispY(), Math.max(0, this.m_rcRect.top));
    }
}
