package jp.co.arttec.satbox.PickRobots;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.media.SoundPool;
import android.util.AttributeSet;
import android.view.View;

public class SelectView extends View {
    private int DEF_HEIGHT;
    private int DEF_WIDTH;
    private Bitmap bmp_gauge;
    private Bitmap bmp_guard;
    private Bitmap bmp_range;
    private Bitmap bmp_speed;
    private int char_num;
    private int count;
    private int draw_count;
    public Rect game_rect;
    private Rect gauge_dst;
    private int gauge_guard;
    private int gauge_range;
    private int gauge_speed;
    private Rect gauge_src;
    private Rect guard_dst;
    RedrawHandler handler;
    private boolean init_flg;
    SoundPool m_SndPool;
    public Resources r;
    private Rect range_dst;
    public float re_size_height;
    public float re_size_width;
    private Rect speed_dst;
    private Rect state_src;

    public SelectView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.init_flg = false;
    }

    public SelectView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.init_flg = false;
        this.handler = new RedrawHandler(this, 30);
        this.handler.start();
        this.bmp_range = BitmapFactory.decodeResource(context.getResources(), R.drawable.select_range);
        this.bmp_guard = BitmapFactory.decodeResource(context.getResources(), R.drawable.select_guard);
        this.bmp_speed = BitmapFactory.decodeResource(context.getResources(), R.drawable.select_speed);
        this.bmp_gauge = BitmapFactory.decodeResource(context.getResources(), R.drawable.select_gauge);
        this.char_num = 0;
        this.gauge_range = 0;
        this.gauge_guard = 0;
        this.gauge_speed = 0;
        this.count = 0;
        this.draw_count = 0;
    }

    public SelectView(Context c) {
        super(c);
        this.init_flg = false;
        setFocusable(true);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        if (!this.init_flg) {
            Rect view_size = new Rect(getLeft(), getTop(), getRight(), getBottom());
            this.DEF_WIDTH = getRight();
            this.DEF_HEIGHT = getBottom();
            if (this.re_size_width >= ((float) view_size.right)) {
                this.re_size_width = ((float) this.DEF_WIDTH) / ((float) view_size.right);
            } else {
                this.re_size_width = ((float) view_size.right) / ((float) this.DEF_WIDTH);
            }
            if (this.re_size_height >= ((float) view_size.bottom)) {
                this.re_size_height = ((float) this.DEF_HEIGHT) / ((float) view_size.bottom);
            } else {
                this.re_size_height = ((float) view_size.bottom) / ((float) this.DEF_HEIGHT);
            }
            this.state_src = new Rect(0, 0, this.bmp_range.getWidth(), this.bmp_range.getHeight());
            this.range_dst = new Rect((int) (this.re_size_width * 20.0f), (int) (this.re_size_height * 0.0f), (int) ((this.re_size_width * 20.0f) + (((float) this.state_src.right) * this.re_size_width)), (int) ((this.re_size_height * 0.0f) + (((float) this.state_src.bottom) * this.re_size_height)));
            this.speed_dst = new Rect((int) (this.re_size_width * 20.0f), (int) (this.re_size_height * 80.0f), (int) ((this.re_size_width * 20.0f) + (((float) this.state_src.right) * this.re_size_width)), (int) ((this.re_size_height * 80.0f) + (((float) this.state_src.bottom) * this.re_size_height)));
            this.guard_dst = new Rect((int) (this.re_size_width * 20.0f), (int) (160.0f * this.re_size_height), (int) ((this.re_size_width * 20.0f) + (((float) this.state_src.right) * this.re_size_width)), (int) ((160.0f * this.re_size_height) + (((float) this.state_src.bottom) * this.re_size_height)));
            this.gauge_src = new Rect(0, 0, this.bmp_gauge.getWidth(), this.bmp_gauge.getHeight());
            this.gauge_dst = new Rect(0, 0, 0, 0);
            this.init_flg = true;
        }
        canvas.drawBitmap(this.bmp_range, this.state_src, this.range_dst, (Paint) null);
        canvas.drawBitmap(this.bmp_guard, this.state_src, this.guard_dst, (Paint) null);
        canvas.drawBitmap(this.bmp_speed, this.state_src, this.speed_dst, (Paint) null);
        stateDraw(canvas);
        drawCount();
    }

    private void drawCount() {
        this.count++;
        if (this.count >= 1) {
            if (this.draw_count <= 10) {
                this.draw_count++;
            }
            this.count = 0;
        }
    }

    private void stateDraw(Canvas canvas) {
        switch (this.char_num) {
            case 0:
                this.gauge_range = 7;
                this.gauge_speed = 3;
                this.gauge_guard = 6;
                break;
            case R.styleable.mediba_ad_sdk_android_MasAdView_requestInterval:
                this.gauge_range = 5;
                this.gauge_speed = 5;
                this.gauge_guard = 5;
                break;
            case R.styleable.mediba_ad_sdk_android_MasAdView_refreshAnimation:
                this.gauge_range = 3;
                this.gauge_speed = 7;
                this.gauge_guard = 4;
                break;
        }
        for (int i = 0; i < this.gauge_range; i++) {
            if (i <= this.draw_count) {
                this.gauge_dst.set((int) (((float) ((i * 30) + 220)) * this.re_size_width), (int) (this.re_size_height * 25.0f), (int) ((((float) ((i * 30) + 220)) * this.re_size_width) + (((float) this.gauge_src.right) * this.re_size_width)), (int) ((this.re_size_height * 25.0f) + (((float) this.gauge_src.bottom) * this.re_size_height)));
                canvas.drawBitmap(this.bmp_gauge, this.gauge_src, this.gauge_dst, (Paint) null);
            }
        }
        for (int i2 = 0; i2 < this.gauge_speed; i2++) {
            if (i2 <= this.draw_count) {
                this.gauge_dst.set((int) (((float) ((i2 * 30) + 220)) * this.re_size_width), (int) (this.re_size_height * 105.0f), (int) ((((float) ((i2 * 30) + 220)) * this.re_size_width) + (((float) this.gauge_src.right) * this.re_size_width)), (int) ((this.re_size_height * 105.0f) + (((float) this.gauge_src.bottom) * this.re_size_height)));
                canvas.drawBitmap(this.bmp_gauge, this.gauge_src, this.gauge_dst, (Paint) null);
            }
        }
        for (int i3 = 0; i3 < this.gauge_guard; i3++) {
            if (i3 <= this.draw_count) {
                this.gauge_dst.set((int) (((float) ((i3 * 30) + 220)) * this.re_size_width), (int) (this.re_size_height * 185.0f), (int) ((((float) ((i3 * 30) + 220)) * this.re_size_width) + (((float) this.gauge_src.right) * this.re_size_width)), (int) ((this.re_size_height * 185.0f) + (((float) this.gauge_src.bottom) * this.re_size_height)));
                canvas.drawBitmap(this.bmp_gauge, this.gauge_src, this.gauge_dst, (Paint) null);
            }
        }
    }

    public void setType(int _type) {
        this.char_num = _type;
        this.count = 0;
        this.draw_count = 0;
    }
}
