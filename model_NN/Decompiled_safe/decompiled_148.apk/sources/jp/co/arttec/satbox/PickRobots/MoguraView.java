package jp.co.arttec.satbox.PickRobots;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Vibrator;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import java.util.ArrayList;
import jp.co.arttec.satbox.scoreranklib.GAECommonData;

public class MoguraView extends View {
    private static /* synthetic */ int[] $SWITCH_TABLE$jp$co$arttec$satbox$PickRobots$eGAME_STATE;
    final int APPLE1_ADVENT_FRM;
    final int APPLE2_ADVENT_NUM;
    final int BMP_APPLE1;
    final int BMP_APPLE2;
    final int BMP_BAKUHATU;
    final int BMP_BATU;
    final int BMP_BG1;
    final int BMP_BG2;
    final int BMP_BG3;
    final int BMP_BG4;
    final int BMP_BG5;
    final int BMP_BOMB1;
    final int BMP_BOMB2;
    final int BMP_BOMB3;
    final int BMP_BOMB4;
    final int BMP_BOMB5;
    final int BMP_BOMB6;
    final int BMP_CHAR;
    final int BMP_CHAR2;
    final int BMP_CHAR2EAT1;
    final int BMP_CHAR2EAT2;
    final int BMP_CHAR2EAT3;
    final int BMP_CHAR2LEFT;
    final int BMP_CHAR2RIGHT;
    final int BMP_CHAR2_;
    final int BMP_CHAR2_2;
    final int BMP_CHAR2_2EAT1;
    final int BMP_CHAR2_2EAT2;
    final int BMP_CHAR2_2EAT3;
    final int BMP_CHAR2_2LEFT;
    final int BMP_CHAR2_2RIGHT;
    final int BMP_CHAR2_EAT1;
    final int BMP_CHAR2_EAT2;
    final int BMP_CHAR2_EAT3;
    final int BMP_CHAR2_LEFT;
    final int BMP_CHAR2_RIGHT;
    final int BMP_CHAR3;
    final int BMP_CHAR3_2;
    final int BMP_CHAR3_2EAT1;
    final int BMP_CHAR3_2EAT2;
    final int BMP_CHAR3_2EAT3;
    final int BMP_CHAR3_2LEFT;
    final int BMP_CHAR3_2RIGHT;
    final int BMP_CHAR3_EAT1;
    final int BMP_CHAR3_EAT2;
    final int BMP_CHAR3_EAT3;
    final int BMP_CHAR3_LEFT;
    final int BMP_CHAR3_RIGHT;
    final int BMP_CHAREAT1;
    final int BMP_CHAREAT2;
    final int BMP_CHAREAT3;
    final int BMP_CHARLEFT;
    final int BMP_CHARRIGHT;
    final int BMP_COMBO;
    final int BMP_FLY1;
    final int BMP_FLY1R;
    final int BMP_FLY2;
    final int BMP_FLY2R;
    final int BMP_HPMETA1;
    final int BMP_HPMETA2;
    final int BMP_ICON;
    final int BMP_LV1;
    final int BMP_LV2;
    final int BMP_LV3;
    final int BMP_LV4;
    final int BMP_LV5;
    final int BMP_LVUP;
    final int BMP_MAX;
    final int BMP_NUM;
    final int BMP_NUM0;
    final int BMP_NUM1;
    final int BMP_NUM2;
    final int BMP_NUM3;
    final int BMP_NUM4;
    final int BMP_NUM5;
    final int BMP_NUM6;
    final int BMP_NUM7;
    final int BMP_NUM8;
    final int BMP_NUM9;
    final int BMP_PLUS;
    final int BMP_SCORE;
    final int BMP_UFO;
    final int BOMB1_ADVENT_FRM;
    final int BOMB2_ADVENT_FRM;
    final int BOMB3_ADVENT_FRM;
    final int BOMB4_ADVENT_FRM;
    final int BOMB5_ADVENT_FRM;
    final int DEBUG_CHAR_SIZE;
    private int DEF_HEIGHT;
    private int DEF_WIDTH;
    final int DISP_HEIGHT;
    final int DISP_WIDTH;
    final int LEVEL2_SCORE;
    final int LEVEL3_SCORE;
    final int LEVEL4_SCORE;
    final int LEVEL5_SCORE;
    final int MISS_POS_Y;
    final int SND_BAKU;
    final int SND_FLASH;
    final int SND_GET;
    final int SND_LEVELUP;
    final int SND_MAX;
    final int SND_MISS;
    final int SND_TOUCH;
    final int TASK_APPLE1;
    final int TASK_APPLE2;
    final int TASK_BOMB1;
    final int TASK_BOMB2;
    final int TASK_BOMB3;
    final int TASK_BOMB4;
    final int TASK_BOMB5;
    final int TASK_MAX;
    private Rect ad_dst;
    private Bitmap ad_line;
    private Rect ad_src;
    public int char_no;
    public Rect game_rect;
    public int ground_y;
    RedrawHandler handler;
    private boolean init_flg;
    private ArrayList<CBattery1> m_Battery1;
    private ArrayList<CBattery2> m_Battery2;
    private CBackGround m_Bg;
    private ArrayList<CBomb> m_Bomb;
    private CChar m_Char;
    private CCombo m_Cmb;
    private ArrayList<CDoril> m_Doril;
    private CDispEffect m_Flash;
    private CHitPoint m_Hp;
    private MediaPlayer m_MdaPlayer;
    private CMsg m_Msg;
    private ArrayList<CNut> m_Nut;
    private CScore m_Score;
    SoundPool m_SndPool;
    private ArrayList<CSpanner1> m_Spanner1;
    private ArrayList<CSpanner2> m_Spanner2;
    private ArrayList[] m_TaskList;
    private Bitmap[] m_Tex;
    private Vibrator m_Vib;
    private CDispEffect m_Wipe;
    private boolean m_bCmbFlg;
    private boolean m_bFlsFlg;
    private boolean m_bHandler;
    private boolean[] m_bSpeedUp;
    private boolean m_bVibFlg;
    private eGAME_STATE m_eState;
    private int m_nBomb1FrmCnt;
    private int m_nBombSpd;
    private int m_nDispX;
    private int m_nDispY;
    private int m_nFrmCnt;
    private int m_nLevel;
    private int[] m_nSnd;
    private int m_nTouchFlg;
    private OnSettingListener onSettingListener;
    public Resources r;
    public float re_size_height;
    public float re_size_width;

    public interface OnSettingListener {
        void onSetting();
    }

    static /* synthetic */ int[] $SWITCH_TABLE$jp$co$arttec$satbox$PickRobots$eGAME_STATE() {
        int[] iArr = $SWITCH_TABLE$jp$co$arttec$satbox$PickRobots$eGAME_STATE;
        if (iArr == null) {
            iArr = new int[eGAME_STATE.values().length];
            try {
                iArr[eGAME_STATE.LEVELUP.ordinal()] = 3;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[eGAME_STATE.MAIN.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[eGAME_STATE.WIPEIN.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[eGAME_STATE.WIPEOUT.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            $SWITCH_TABLE$jp$co$arttec$satbox$PickRobots$eGAME_STATE = iArr;
        }
        return iArr;
    }

    public MoguraView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.DISP_WIDTH = 480;
        this.DISP_HEIGHT = 854;
        this.MISS_POS_Y = 600;
        this.DEBUG_CHAR_SIZE = 20;
        this.APPLE2_ADVENT_NUM = 20;
        this.LEVEL2_SCORE = 1000;
        this.LEVEL3_SCORE = GAECommonData.CONNECTION_TIMEOUT;
        this.LEVEL4_SCORE = 12000;
        this.LEVEL5_SCORE = 25000;
        this.APPLE1_ADVENT_FRM = 30;
        this.BOMB1_ADVENT_FRM = 40;
        this.BOMB2_ADVENT_FRM = 165;
        this.BOMB3_ADVENT_FRM = 65;
        this.BOMB4_ADVENT_FRM = 115;
        this.BOMB5_ADVENT_FRM = 100;
        this.BMP_APPLE1 = 0;
        this.BMP_APPLE2 = 1;
        this.BMP_BOMB1 = 2;
        this.BMP_BOMB3 = 3;
        this.BMP_BOMB4 = 4;
        this.BMP_BOMB5 = 5;
        this.BMP_BOMB2 = 6;
        this.BMP_BAKUHATU = 7;
        this.BMP_BG1 = 8;
        this.BMP_BG2 = 9;
        this.BMP_BG3 = 10;
        this.BMP_BG4 = 11;
        this.BMP_BG5 = 12;
        this.BMP_HPMETA1 = 13;
        this.BMP_HPMETA2 = 14;
        this.BMP_CHAR = 15;
        this.BMP_CHAREAT1 = 16;
        this.BMP_CHAREAT2 = 17;
        this.BMP_CHAREAT3 = 18;
        this.BMP_CHARRIGHT = 19;
        this.BMP_CHARLEFT = 20;
        this.BMP_CHAR2 = 21;
        this.BMP_CHAR2EAT1 = 22;
        this.BMP_CHAR2EAT2 = 23;
        this.BMP_CHAR2EAT3 = 24;
        this.BMP_CHAR2RIGHT = 25;
        this.BMP_CHAR2LEFT = 26;
        this.BMP_UFO = 27;
        this.BMP_FLY1 = 28;
        this.BMP_FLY2 = 29;
        this.BMP_ICON = 30;
        this.BMP_BATU = 31;
        this.BMP_NUM = 32;
        this.BMP_LVUP = 33;
        this.BMP_LV1 = 34;
        this.BMP_LV2 = 35;
        this.BMP_LV3 = 36;
        this.BMP_LV4 = 37;
        this.BMP_LV5 = 38;
        this.BMP_BOMB6 = 39;
        this.BMP_FLY1R = 40;
        this.BMP_FLY2R = 41;
        this.BMP_CHAR2_ = 42;
        this.BMP_CHAR2_EAT1 = 43;
        this.BMP_CHAR2_EAT2 = 44;
        this.BMP_CHAR2_EAT3 = 45;
        this.BMP_CHAR2_RIGHT = 46;
        this.BMP_CHAR2_LEFT = 47;
        this.BMP_CHAR2_2 = 48;
        this.BMP_CHAR2_2EAT1 = 49;
        this.BMP_CHAR2_2EAT2 = 50;
        this.BMP_CHAR2_2EAT3 = 51;
        this.BMP_CHAR2_2RIGHT = 52;
        this.BMP_CHAR2_2LEFT = 53;
        this.BMP_CHAR3 = 54;
        this.BMP_CHAR3_EAT1 = 55;
        this.BMP_CHAR3_EAT2 = 56;
        this.BMP_CHAR3_EAT3 = 57;
        this.BMP_CHAR3_RIGHT = 58;
        this.BMP_CHAR3_LEFT = 59;
        this.BMP_CHAR3_2 = 60;
        this.BMP_CHAR3_2EAT1 = 61;
        this.BMP_CHAR3_2EAT2 = 62;
        this.BMP_CHAR3_2EAT3 = 63;
        this.BMP_CHAR3_2RIGHT = 64;
        this.BMP_CHAR3_2LEFT = 65;
        this.BMP_NUM0 = 66;
        this.BMP_NUM1 = 67;
        this.BMP_NUM2 = 68;
        this.BMP_NUM3 = 69;
        this.BMP_NUM4 = 70;
        this.BMP_NUM5 = 71;
        this.BMP_NUM6 = 72;
        this.BMP_NUM7 = 73;
        this.BMP_NUM8 = 74;
        this.BMP_NUM9 = 75;
        this.BMP_COMBO = 76;
        this.BMP_SCORE = 77;
        this.BMP_PLUS = 78;
        this.BMP_MAX = 79;
        this.SND_BAKU = 0;
        this.SND_FLASH = 1;
        this.SND_TOUCH = 2;
        this.SND_GET = 3;
        this.SND_LEVELUP = 4;
        this.SND_MISS = 5;
        this.SND_MAX = 6;
        this.TASK_APPLE1 = 0;
        this.TASK_APPLE2 = 1;
        this.TASK_BOMB1 = 2;
        this.TASK_BOMB2 = 3;
        this.TASK_BOMB3 = 4;
        this.TASK_BOMB4 = 5;
        this.TASK_BOMB5 = 6;
        this.TASK_MAX = 7;
        this.DEF_WIDTH = 480;
        this.DEF_HEIGHT = 800;
        this.init_flg = false;
        this.char_no = 0;
    }

    public MoguraView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.DISP_WIDTH = 480;
        this.DISP_HEIGHT = 854;
        this.MISS_POS_Y = 600;
        this.DEBUG_CHAR_SIZE = 20;
        this.APPLE2_ADVENT_NUM = 20;
        this.LEVEL2_SCORE = 1000;
        this.LEVEL3_SCORE = GAECommonData.CONNECTION_TIMEOUT;
        this.LEVEL4_SCORE = 12000;
        this.LEVEL5_SCORE = 25000;
        this.APPLE1_ADVENT_FRM = 30;
        this.BOMB1_ADVENT_FRM = 40;
        this.BOMB2_ADVENT_FRM = 165;
        this.BOMB3_ADVENT_FRM = 65;
        this.BOMB4_ADVENT_FRM = 115;
        this.BOMB5_ADVENT_FRM = 100;
        this.BMP_APPLE1 = 0;
        this.BMP_APPLE2 = 1;
        this.BMP_BOMB1 = 2;
        this.BMP_BOMB3 = 3;
        this.BMP_BOMB4 = 4;
        this.BMP_BOMB5 = 5;
        this.BMP_BOMB2 = 6;
        this.BMP_BAKUHATU = 7;
        this.BMP_BG1 = 8;
        this.BMP_BG2 = 9;
        this.BMP_BG3 = 10;
        this.BMP_BG4 = 11;
        this.BMP_BG5 = 12;
        this.BMP_HPMETA1 = 13;
        this.BMP_HPMETA2 = 14;
        this.BMP_CHAR = 15;
        this.BMP_CHAREAT1 = 16;
        this.BMP_CHAREAT2 = 17;
        this.BMP_CHAREAT3 = 18;
        this.BMP_CHARRIGHT = 19;
        this.BMP_CHARLEFT = 20;
        this.BMP_CHAR2 = 21;
        this.BMP_CHAR2EAT1 = 22;
        this.BMP_CHAR2EAT2 = 23;
        this.BMP_CHAR2EAT3 = 24;
        this.BMP_CHAR2RIGHT = 25;
        this.BMP_CHAR2LEFT = 26;
        this.BMP_UFO = 27;
        this.BMP_FLY1 = 28;
        this.BMP_FLY2 = 29;
        this.BMP_ICON = 30;
        this.BMP_BATU = 31;
        this.BMP_NUM = 32;
        this.BMP_LVUP = 33;
        this.BMP_LV1 = 34;
        this.BMP_LV2 = 35;
        this.BMP_LV3 = 36;
        this.BMP_LV4 = 37;
        this.BMP_LV5 = 38;
        this.BMP_BOMB6 = 39;
        this.BMP_FLY1R = 40;
        this.BMP_FLY2R = 41;
        this.BMP_CHAR2_ = 42;
        this.BMP_CHAR2_EAT1 = 43;
        this.BMP_CHAR2_EAT2 = 44;
        this.BMP_CHAR2_EAT3 = 45;
        this.BMP_CHAR2_RIGHT = 46;
        this.BMP_CHAR2_LEFT = 47;
        this.BMP_CHAR2_2 = 48;
        this.BMP_CHAR2_2EAT1 = 49;
        this.BMP_CHAR2_2EAT2 = 50;
        this.BMP_CHAR2_2EAT3 = 51;
        this.BMP_CHAR2_2RIGHT = 52;
        this.BMP_CHAR2_2LEFT = 53;
        this.BMP_CHAR3 = 54;
        this.BMP_CHAR3_EAT1 = 55;
        this.BMP_CHAR3_EAT2 = 56;
        this.BMP_CHAR3_EAT3 = 57;
        this.BMP_CHAR3_RIGHT = 58;
        this.BMP_CHAR3_LEFT = 59;
        this.BMP_CHAR3_2 = 60;
        this.BMP_CHAR3_2EAT1 = 61;
        this.BMP_CHAR3_2EAT2 = 62;
        this.BMP_CHAR3_2EAT3 = 63;
        this.BMP_CHAR3_2RIGHT = 64;
        this.BMP_CHAR3_2LEFT = 65;
        this.BMP_NUM0 = 66;
        this.BMP_NUM1 = 67;
        this.BMP_NUM2 = 68;
        this.BMP_NUM3 = 69;
        this.BMP_NUM4 = 70;
        this.BMP_NUM5 = 71;
        this.BMP_NUM6 = 72;
        this.BMP_NUM7 = 73;
        this.BMP_NUM8 = 74;
        this.BMP_NUM9 = 75;
        this.BMP_COMBO = 76;
        this.BMP_SCORE = 77;
        this.BMP_PLUS = 78;
        this.BMP_MAX = 79;
        this.SND_BAKU = 0;
        this.SND_FLASH = 1;
        this.SND_TOUCH = 2;
        this.SND_GET = 3;
        this.SND_LEVELUP = 4;
        this.SND_MISS = 5;
        this.SND_MAX = 6;
        this.TASK_APPLE1 = 0;
        this.TASK_APPLE2 = 1;
        this.TASK_BOMB1 = 2;
        this.TASK_BOMB2 = 3;
        this.TASK_BOMB3 = 4;
        this.TASK_BOMB4 = 5;
        this.TASK_BOMB5 = 6;
        this.TASK_MAX = 7;
        this.DEF_WIDTH = 480;
        this.DEF_HEIGHT = 800;
        this.init_flg = false;
        this.char_no = 0;
        this.m_Vib = (Vibrator) getContext().getSystemService("vibrator");
        this.m_eState = eGAME_STATE.WIPEIN;
        this.m_nDispX = 480;
        this.m_nDispY = 854;
        this.m_nFrmCnt = 0;
        this.m_nLevel = 1;
        this.m_nTouchFlg = 0;
        this.m_nBomb1FrmCnt = 40;
        this.m_nBombSpd = 0;
        this.m_bHandler = false;
        this.m_bCmbFlg = false;
        this.m_bFlsFlg = false;
        this.m_bSpeedUp = new boolean[4];
        for (int i = 0; i < this.m_bSpeedUp.length; i++) {
            this.m_bSpeedUp[i] = false;
        }
        CCombo.SetCmbCnt(0);
        Resources r2 = context.getResources();
        this.m_Battery1 = new ArrayList<>();
        this.m_Bomb = new ArrayList<>();
        this.m_Spanner1 = new ArrayList<>();
        this.m_Spanner2 = new ArrayList<>();
        this.m_Battery2 = new ArrayList<>();
        this.m_Nut = new ArrayList<>();
        this.m_Doril = new ArrayList<>();
        this.m_Tex = new Bitmap[79];
        this.m_nSnd = new int[6];
        this.m_TaskList = new ArrayList[7];
        this.m_TaskList[0] = this.m_Battery1;
        this.m_TaskList[1] = this.m_Bomb;
        this.m_TaskList[2] = this.m_Spanner1;
        this.m_TaskList[3] = this.m_Spanner2;
        this.m_TaskList[4] = this.m_Battery2;
        this.m_TaskList[5] = this.m_Nut;
        this.m_TaskList[6] = this.m_Doril;
        this.handler = new RedrawHandler(this, 30);
        this.handler.start();
        int[] nTexID = {R.drawable.apple1, R.drawable.apple2, R.drawable.bomb1, R.drawable.bomb2, R.drawable.bomb3, R.drawable.bomb4, R.drawable.star2, R.drawable.bakuhatu, R.drawable.bg1, R.drawable.bg2, R.drawable.bg3, R.drawable.bg4, R.drawable.bg5, R.drawable.hp_meta1, R.drawable.hp_meta2, R.drawable.char2, R.drawable.chareat1, R.drawable.chareat2, R.drawable.chareat3, R.drawable.charright, R.drawable.charleft, R.drawable.char22, R.drawable.chareat12, R.drawable.chareat22, R.drawable.chareat32, R.drawable.charright2, R.drawable.charleft2, R.drawable.ufo, R.drawable.fly1, R.drawable.fly2, R.drawable.itemicon, R.drawable.batu, R.drawable.num, R.drawable.level, R.drawable.level1, R.drawable.level2, R.drawable.level3, R.drawable.level4, R.drawable.level5, R.drawable.bomb5, R.drawable.fly1_r, R.drawable.fly2_r, R.drawable.char2_, R.drawable.char2eat1, R.drawable.char2eat2, R.drawable.char2eat3, R.drawable.char2right, R.drawable.char2left, R.drawable.char2_2, R.drawable.char2eat12, R.drawable.char2eat22, R.drawable.char2eat32, R.drawable.char2right2, R.drawable.char2left2, R.drawable.char3, R.drawable.char3eat1, R.drawable.char3eat2, R.drawable.char3eat3, R.drawable.char3right, R.drawable.char3left, R.drawable.char32, R.drawable.char3eat12, R.drawable.char3eat22, R.drawable.char3eat32, R.drawable.char3right2, R.drawable.char3left2, R.drawable.num_0, R.drawable.num_1, R.drawable.num_2, R.drawable.num_3, R.drawable.num_4, R.drawable.num_5, R.drawable.num_6, R.drawable.num_7, R.drawable.num_8, R.drawable.num_9, R.drawable.combo, R.drawable.score, R.drawable.plus};
        for (int i2 = 0; i2 < 79; i2++) {
            this.m_Tex[i2] = BitmapFactory.decodeResource(r2, nTexID[i2]);
        }
        this.ad_line = BitmapFactory.decodeResource(context.getResources(), R.drawable.ad_back);
        int[] nSoundID = {R.raw.baku, R.raw.flash, R.raw.touch, R.raw.get, R.raw.levelup, R.raw.miss};
        this.m_SndPool = new SoundPool(6, 3, 0);
        for (int i3 = 0; i3 < 6; i3++) {
            this.m_nSnd[i3] = this.m_SndPool.load(context, nSoundID[i3], 1);
        }
        this.m_Bg = new CBackGround(this);
        this.m_Score = new CScore(this);
        this.m_bVibFlg = context.getSharedPreferences("prefkey", 0).getBoolean("vibflg", true);
        playSound();
    }

    public MoguraView(Context c) {
        super(c);
        this.DISP_WIDTH = 480;
        this.DISP_HEIGHT = 854;
        this.MISS_POS_Y = 600;
        this.DEBUG_CHAR_SIZE = 20;
        this.APPLE2_ADVENT_NUM = 20;
        this.LEVEL2_SCORE = 1000;
        this.LEVEL3_SCORE = GAECommonData.CONNECTION_TIMEOUT;
        this.LEVEL4_SCORE = 12000;
        this.LEVEL5_SCORE = 25000;
        this.APPLE1_ADVENT_FRM = 30;
        this.BOMB1_ADVENT_FRM = 40;
        this.BOMB2_ADVENT_FRM = 165;
        this.BOMB3_ADVENT_FRM = 65;
        this.BOMB4_ADVENT_FRM = 115;
        this.BOMB5_ADVENT_FRM = 100;
        this.BMP_APPLE1 = 0;
        this.BMP_APPLE2 = 1;
        this.BMP_BOMB1 = 2;
        this.BMP_BOMB3 = 3;
        this.BMP_BOMB4 = 4;
        this.BMP_BOMB5 = 5;
        this.BMP_BOMB2 = 6;
        this.BMP_BAKUHATU = 7;
        this.BMP_BG1 = 8;
        this.BMP_BG2 = 9;
        this.BMP_BG3 = 10;
        this.BMP_BG4 = 11;
        this.BMP_BG5 = 12;
        this.BMP_HPMETA1 = 13;
        this.BMP_HPMETA2 = 14;
        this.BMP_CHAR = 15;
        this.BMP_CHAREAT1 = 16;
        this.BMP_CHAREAT2 = 17;
        this.BMP_CHAREAT3 = 18;
        this.BMP_CHARRIGHT = 19;
        this.BMP_CHARLEFT = 20;
        this.BMP_CHAR2 = 21;
        this.BMP_CHAR2EAT1 = 22;
        this.BMP_CHAR2EAT2 = 23;
        this.BMP_CHAR2EAT3 = 24;
        this.BMP_CHAR2RIGHT = 25;
        this.BMP_CHAR2LEFT = 26;
        this.BMP_UFO = 27;
        this.BMP_FLY1 = 28;
        this.BMP_FLY2 = 29;
        this.BMP_ICON = 30;
        this.BMP_BATU = 31;
        this.BMP_NUM = 32;
        this.BMP_LVUP = 33;
        this.BMP_LV1 = 34;
        this.BMP_LV2 = 35;
        this.BMP_LV3 = 36;
        this.BMP_LV4 = 37;
        this.BMP_LV5 = 38;
        this.BMP_BOMB6 = 39;
        this.BMP_FLY1R = 40;
        this.BMP_FLY2R = 41;
        this.BMP_CHAR2_ = 42;
        this.BMP_CHAR2_EAT1 = 43;
        this.BMP_CHAR2_EAT2 = 44;
        this.BMP_CHAR2_EAT3 = 45;
        this.BMP_CHAR2_RIGHT = 46;
        this.BMP_CHAR2_LEFT = 47;
        this.BMP_CHAR2_2 = 48;
        this.BMP_CHAR2_2EAT1 = 49;
        this.BMP_CHAR2_2EAT2 = 50;
        this.BMP_CHAR2_2EAT3 = 51;
        this.BMP_CHAR2_2RIGHT = 52;
        this.BMP_CHAR2_2LEFT = 53;
        this.BMP_CHAR3 = 54;
        this.BMP_CHAR3_EAT1 = 55;
        this.BMP_CHAR3_EAT2 = 56;
        this.BMP_CHAR3_EAT3 = 57;
        this.BMP_CHAR3_RIGHT = 58;
        this.BMP_CHAR3_LEFT = 59;
        this.BMP_CHAR3_2 = 60;
        this.BMP_CHAR3_2EAT1 = 61;
        this.BMP_CHAR3_2EAT2 = 62;
        this.BMP_CHAR3_2EAT3 = 63;
        this.BMP_CHAR3_2RIGHT = 64;
        this.BMP_CHAR3_2LEFT = 65;
        this.BMP_NUM0 = 66;
        this.BMP_NUM1 = 67;
        this.BMP_NUM2 = 68;
        this.BMP_NUM3 = 69;
        this.BMP_NUM4 = 70;
        this.BMP_NUM5 = 71;
        this.BMP_NUM6 = 72;
        this.BMP_NUM7 = 73;
        this.BMP_NUM8 = 74;
        this.BMP_NUM9 = 75;
        this.BMP_COMBO = 76;
        this.BMP_SCORE = 77;
        this.BMP_PLUS = 78;
        this.BMP_MAX = 79;
        this.SND_BAKU = 0;
        this.SND_FLASH = 1;
        this.SND_TOUCH = 2;
        this.SND_GET = 3;
        this.SND_LEVELUP = 4;
        this.SND_MISS = 5;
        this.SND_MAX = 6;
        this.TASK_APPLE1 = 0;
        this.TASK_APPLE2 = 1;
        this.TASK_BOMB1 = 2;
        this.TASK_BOMB2 = 3;
        this.TASK_BOMB3 = 4;
        this.TASK_BOMB4 = 5;
        this.TASK_BOMB5 = 6;
        this.TASK_MAX = 7;
        this.DEF_WIDTH = 480;
        this.DEF_HEIGHT = 800;
        this.init_flg = false;
        this.char_no = 0;
        setFocusable(true);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        this.m_nDispX = w - 60;
        this.m_nDispY = h;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        if (!this.init_flg) {
            Rect view_size = new Rect(getLeft(), getTop(), getRight(), getBottom());
            if (this.re_size_width >= ((float) view_size.right)) {
                this.re_size_width = ((float) this.DEF_WIDTH) / ((float) view_size.right);
            } else {
                this.re_size_width = ((float) view_size.right) / ((float) this.DEF_WIDTH);
            }
            if (this.re_size_height >= ((float) view_size.bottom)) {
                this.re_size_height = ((float) this.DEF_HEIGHT) / ((float) view_size.bottom);
            } else {
                this.re_size_height = ((float) view_size.bottom) / ((float) this.DEF_HEIGHT);
            }
            this.ad_src = new Rect(0, 0, this.ad_line.getWidth(), this.ad_line.getHeight());
            this.ad_dst = new Rect(getLeft(), getTop(), getRight(), getTop() + ((int) (((float) this.ad_line.getHeight()) * this.re_size_height)));
            this.game_rect = new Rect(getLeft(), getTop() + ((int) (((float) this.ad_line.getHeight()) * this.re_size_height)), getRight(), getBottom());
            this.m_Bg.setSrcToResource();
            this.m_Bg.setDst(this.game_rect);
            this.ground_y = (int) (((float) getBottom()) - (40.0f * this.re_size_height));
            this.init_flg = true;
            this.m_Msg = new CMsg(this);
            this.m_Char = new CChar(this, this.char_no);
            this.m_Hp = new CHitPoint(this);
            this.m_Wipe = new CDispEffect(this, 0);
            this.m_Flash = new CDispEffect(this, 1);
        }
        Main(canvas);
        switch ($SWITCH_TABLE$jp$co$arttec$satbox$PickRobots$eGAME_STATE()[this.m_eState.ordinal()]) {
            case R.styleable.mediba_ad_sdk_android_MasAdView_requestInterval:
                this.m_Wipe.In(canvas);
                break;
            case R.styleable.mediba_ad_sdk_android_MasAdView_visibility:
                this.m_nBombSpd++;
                this.m_Char.AddSpeed();
                this.m_nBomb1FrmCnt--;
                this.m_nFrmCnt = 0;
                this.m_SndPool.play(this.m_nSnd[4], 1.0f, 1.0f, 0, 0, 1.0f);
                this.m_Msg = new CMsg(this);
                this.m_eState = eGAME_STATE.MAIN;
                break;
            case R.styleable.mediba_ad_sdk_android_MasAdView_testMode:
                if (!this.m_Wipe.Out(canvas) && !this.m_bHandler) {
                    this.handler.stop();
                    OnSettingEvent();
                    this.m_bHandler = true;
                    break;
                }
        }
        this.m_nFrmCnt++;
    }

    public void Main(Canvas canvas) {
        int nX = 0;
        this.m_Bg.Exec(canvas);
        if (this.m_Hp.GetHP() <= 0) {
            this.m_nFrmCnt = 0;
            this.m_Battery1.clear();
            this.m_Bomb.clear();
            this.m_Spanner1.clear();
            this.m_Spanner2.clear();
            this.m_Battery2.clear();
            this.m_Nut.clear();
            this.m_Doril.clear();
            stopSound();
            this.m_eState = eGAME_STATE.WIPEOUT;
        } else {
            nX = FallExec(canvas);
        }
        canvas.drawBitmap(this.ad_line, this.ad_src, this.ad_dst, (Paint) null);
        this.m_Hp.Exec(canvas);
        this.m_Score.Exec(canvas);
        this.m_Char.Exec(canvas, nX);
        if (this.m_bFlsFlg && !this.m_Flash.Flash(canvas)) {
            this.m_bFlsFlg = false;
        }
        if (this.m_bCmbFlg) {
            this.m_Cmb.Exec(canvas);
        }
        this.m_Msg.Exec(canvas);
    }

    public void Debug(Canvas canvas) {
        Paint paint = new Paint();
        paint.setColor(-1);
        paint.setTextSize(20.0f);
        canvas.drawText("BOMB1_SIZE : " + this.m_Spanner1.size(), 0.0f, 250.0f, paint);
    }

    public int FallExec(Canvas canvas) {
        int nPosX;
        int nPosX2;
        int nPosX3;
        int nPosX4;
        int nPosX5;
        int nPosX6;
        int nPosX7;
        int nX = 0;
        int nY = 0;
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < this.m_TaskList[i].size(); j++) {
                CTask pTask = (CTask) this.m_TaskList[i].get(j);
                if (pTask.GetRect().top > nY) {
                    nX = pTask.GetRect().left + (pTask.GetRect().right / 2);
                    nY = pTask.GetRect().top;
                }
                if (!pTask.Exec(canvas)) {
                    this.m_TaskList[i].remove(j);
                }
            }
        }
        int nScore = this.m_Score.GetScore();
        if (nScore >= 25000) {
            this.m_nLevel = 5;
        } else if (nScore >= 12000) {
            this.m_nLevel = 4;
        } else if (nScore >= 5000) {
            this.m_nLevel = 3;
        } else if (nScore >= 1000) {
            this.m_nLevel = 2;
        } else {
            this.m_nLevel = 1;
        }
        for (int i2 = 2; i2 <= 5; i2++) {
            if (this.m_nLevel >= i2 && !this.m_bSpeedUp[i2 - 2]) {
                this.m_eState = eGAME_STATE.LEVELUP;
                this.m_bSpeedUp[i2 - 2] = true;
            }
        }
        if (this.m_nLevel >= 1 && this.m_nFrmCnt % 30 == 0 && (nPosX7 = CheckAdvent()) != -1) {
            this.m_Battery1.add(new CBattery1(this, nPosX7, this.m_nBombSpd, 0, 2));
        }
        if (this.m_nLevel >= 1 && this.m_Char.GetAppleNum() >= 20 && (nPosX6 = CheckAdvent()) != -1) {
            this.m_Bomb.add(new CBomb(this, nPosX6, this.m_nBombSpd, 1, 2));
            this.m_Char.SetAppleNum(0);
        }
        if (this.m_nLevel >= 1 && this.m_nFrmCnt % this.m_nBomb1FrmCnt == 0 && (nPosX5 = CheckAdvent()) != -1) {
            this.m_Spanner1.add(new CSpanner1(this, nPosX5, this.m_nBombSpd, 2, 2));
        }
        if (this.m_nLevel >= 2 && this.m_nFrmCnt % 165 == 0 && (nPosX4 = CheckAdvent()) != -1) {
            this.m_Spanner2.add(new CSpanner2(this, nPosX4, this.m_nBombSpd, 3, 2));
        }
        if (this.m_nLevel >= 3 && this.m_nFrmCnt % 65 == 0 && (nPosX3 = CheckAdvent()) != -1) {
            this.m_Battery2.add(new CBattery2(this, nPosX3, this.m_nBombSpd, 4, 2));
        }
        if (this.m_nLevel >= 4) {
            int nTexW = (int) (((float) this.m_Tex[5].getWidth()) * this.re_size_width);
            int nAdventX = (int) Math.floor(Math.random() * ((double) (this.m_nDispX - (nTexW * 2))));
            for (int i3 = 0; i3 < 3; i3++) {
                if (this.m_nFrmCnt % 115 == 0 && (nPosX2 = CheckAdvent()) != -1) {
                    this.m_Nut.add(new CNut(this, nPosX2, this.m_nBombSpd, 5, 2, nAdventX + (i3 * nTexW)));
                }
            }
        }
        if (this.m_nLevel >= 5 && this.m_nFrmCnt % 100 == 0 && (nPosX = CheckAdvent()) != -1) {
            this.m_Doril.add(new CDoril(this, nPosX, this.m_nBombSpd, 39, 2));
        }
        return nX;
    }

    public int CheckAdvent() {
        int nPosX = (int) (Math.random() * ((double) (this.m_nDispX - 50)));
        if (this.m_Battery1.size() == 0 && this.m_Bomb.size() == 0 && this.m_Spanner1.size() == 0 && this.m_Spanner2.size() == 0 && this.m_Battery2.size() == 0 && this.m_Nut.size() == 0) {
            return nPosX;
        }
        int i = 0;
        while (i < 3) {
            double dwRnd = Math.random();
            boolean bHit = false;
            Rect rcRect = new Rect(0, 20, 0, 0);
            for (int j = 0; j < 7; j++) {
                int k = 0;
                while (true) {
                    if (k >= this.m_TaskList[j].size()) {
                        break;
                    }
                    CFall pFall = (CFall) this.m_TaskList[j].get(k);
                    nPosX = (int) (((double) (this.m_nDispX - pFall.GetTex().getWidth())) * dwRnd);
                    rcRect.left = nPosX;
                    rcRect.right = pFall.GetTex().getWidth();
                    rcRect.bottom = pFall.GetTex().getHeight();
                    if (pFall.HitCheckBox(rcRect)) {
                        bHit = true;
                        break;
                    }
                    k++;
                }
            }
            if (!bHit) {
                break;
            }
            i++;
        }
        if (i == 3) {
            return -1;
        }
        return nPosX;
    }

    public boolean onTouchEvent(MotionEvent event) {
        int tx = (int) event.getX();
        int ty = (int) event.getY();
        if (event.getAction() == 0) {
            if (this.m_nTouchFlg == 0) {
                if (this.m_Char.Touch(tx, ty) && this.m_Char.GetFlashNum() > 0) {
                    this.m_Spanner1.clear();
                    this.m_Spanner2.clear();
                    this.m_Battery2.clear();
                    this.m_Nut.clear();
                    this.m_Doril.clear();
                    this.m_SndPool.play(this.m_nSnd[1], 1.0f, 1.0f, 0, 0, 1.0f);
                    this.m_bFlsFlg = true;
                    this.m_Char.SubFlashNum();
                }
                for (int i = 0; i < this.m_Battery1.size(); i++) {
                    CBattery1 bm = this.m_Battery1.get(i);
                    if (bm.GetAnimCnt() == 0) {
                        bm.Touch(tx, ty);
                    }
                }
                for (int i2 = 0; i2 < this.m_Bomb.size(); i2++) {
                    CBomb bm2 = this.m_Bomb.get(i2);
                    if (bm2.GetAnimCnt() == 0) {
                        bm2.Touch(tx, ty);
                    }
                }
                for (int i3 = 0; i3 < this.m_Spanner1.size(); i3++) {
                    CSpanner1 bm3 = this.m_Spanner1.get(i3);
                    if (bm3.GetAnimCnt() == 0 && bm3.Touch(tx, ty)) {
                        this.m_Cmb = new CCombo(this);
                        this.m_bCmbFlg = true;
                    }
                }
                for (int i4 = 0; i4 < this.m_Spanner2.size(); i4++) {
                    CSpanner2 bm4 = this.m_Spanner2.get(i4);
                    if (bm4.GetAnimCnt() == 0 && bm4.Touch(tx, ty)) {
                        this.m_Cmb = new CCombo(this);
                        this.m_bCmbFlg = true;
                    }
                }
                for (int i5 = 0; i5 < this.m_Battery2.size(); i5++) {
                    CBattery2 bm5 = this.m_Battery2.get(i5);
                    if (bm5.GetAnimCnt() == 0 && bm5.Touch(tx, ty)) {
                        this.m_Cmb = new CCombo(this);
                        this.m_bCmbFlg = true;
                    }
                }
                for (int i6 = 0; i6 < this.m_Nut.size(); i6++) {
                    CNut bm6 = this.m_Nut.get(i6);
                    if (bm6.GetAnimCnt() == 0 && bm6.Touch(tx, ty)) {
                        this.m_Cmb = new CCombo(this);
                        this.m_bCmbFlg = true;
                    }
                }
                for (int i7 = 0; i7 < this.m_Doril.size(); i7++) {
                    CDoril bm7 = this.m_Doril.get(i7);
                    if (bm7.GetAnimCnt() == 0 && bm7.Touch(tx, ty)) {
                        this.m_Cmb = new CCombo(this);
                        this.m_bCmbFlg = true;
                    }
                }
            }
            this.m_nTouchFlg = 1;
            return true;
        }
        if (event.getAction() == 1) {
            this.m_nTouchFlg = 0;
        }
        return true;
    }

    private void playSound() {
        stopSound();
        this.m_MdaPlayer = MediaPlayer.create(getContext(), (int) R.raw.main);
        this.m_MdaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
                mp.seekTo(0);
                mp.start();
            }
        });
    }

    public void pauseSound() {
        if (this.m_MdaPlayer != null && this.m_MdaPlayer.isPlaying()) {
            this.m_MdaPlayer.pause();
        }
    }

    public void resumeSound() {
        if (this.m_MdaPlayer != null) {
            this.m_MdaPlayer.start();
        }
    }

    public void stopSound() {
        if (this.m_MdaPlayer != null) {
            if (this.m_MdaPlayer.isPlaying()) {
                this.m_MdaPlayer.stop();
            }
            this.m_MdaPlayer.release();
            this.m_MdaPlayer = null;
        }
    }

    public void destroyBGM() {
        if (this.m_SndPool != null) {
            this.m_SndPool.release();
        }
    }

    public void destroyBITMAP() {
        this.m_Msg.destroyBMP();
        for (int i = 0; i < 79; i++) {
            if (this.m_Tex[i] != null) {
                this.m_Tex[i].recycle();
            }
        }
    }

    public void setOnSettingListener(OnSettingListener listener) {
        this.onSettingListener = listener;
    }

    private void OnSettingEvent() {
        if (this.onSettingListener != null) {
            this.onSettingListener.onSetting();
        }
    }

    public void setCharNo(int _char_no) {
        this.char_no = _char_no;
    }

    public void SetState(eGAME_STATE eState) {
        this.m_eState = eState;
    }

    public void SetCmbFlg(boolean bFlg) {
        this.m_bCmbFlg = bFlg;
    }

    public Bitmap GetTex(int nIdx) {
        return this.m_Tex[nIdx];
    }

    public int GetSnd(int nIdx) {
        return this.m_nSnd[nIdx];
    }

    public SoundPool GetSndPool() {
        return this.m_SndPool;
    }

    public Boolean GetVibFlg() {
        return Boolean.valueOf(this.m_bVibFlg);
    }

    public Vibrator GetVib() {
        return this.m_Vib;
    }

    public CBackGround GetBG() {
        return this.m_Bg;
    }

    public CScore GetScore() {
        return this.m_Score;
    }

    public CCombo GetCmb() {
        return this.m_Cmb;
    }

    public CHitPoint GetHP() {
        return this.m_Hp;
    }

    public CChar GetChar() {
        return this.m_Char;
    }

    public int GetDispX() {
        return this.m_nDispX;
    }

    public int GetDispY() {
        return this.m_nDispY;
    }

    public int GetLevel() {
        return this.m_nLevel;
    }
}
