package jp.co.arttec.satbox.PickRobots;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

/* compiled from: Char */
class CChar {
    final int CHAR_EATANIM_SPEED = 15;
    final int CHAR_FLASH_MAX = 3;
    final int CHAR_WALKANIM_SPEED = 15;
    private boolean m_bEat;
    private boolean m_bWalk;
    private int m_nAnimCnt;
    private int m_nAppleNum;
    private int m_nFlashNum;
    private int m_nNo;
    private float m_nSpeed;
    private Bitmap m_pTex;
    private MoguraView m_pView;
    private Rect m_rcRect;

    public CChar(MoguraView pView, int nNo) {
        this.m_pView = pView;
        switch (nNo) {
            case 0:
                MoguraView moguraView = this.m_pView;
                this.m_pView.getClass();
                this.m_pTex = moguraView.GetTex(54);
                this.m_nSpeed = 1.0f;
                break;
            case R.styleable.mediba_ad_sdk_android_MasAdView_requestInterval /*1*/:
                MoguraView moguraView2 = this.m_pView;
                this.m_pView.getClass();
                this.m_pTex = moguraView2.GetTex(15);
                this.m_nSpeed = 1.5f;
                break;
            case R.styleable.mediba_ad_sdk_android_MasAdView_refreshAnimation /*2*/:
                MoguraView moguraView3 = this.m_pView;
                this.m_pView.getClass();
                this.m_pTex = moguraView3.GetTex(42);
                this.m_nSpeed = 2.0f;
                break;
        }
        int nTexW = this.m_pTex.getWidth();
        int nTexH = this.m_pTex.getHeight();
        this.m_rcRect = new Rect((this.m_pView.game_rect.right / 2) - (nTexW / 2), this.m_pView.ground_y - nTexH, nTexW, nTexH);
        this.m_nAppleNum = 0;
        this.m_nFlashNum = 0;
        this.m_nAnimCnt = 0;
        this.m_bWalk = false;
        this.m_bEat = false;
        this.m_nNo = nNo;
    }

    public void Exec(Canvas canvas, int nX) {
        int nTexCenX = this.m_rcRect.left + (this.m_rcRect.right / 2);
        if (nTexCenX > nX) {
            if (((float) (nTexCenX - nX)) > this.m_nSpeed) {
                this.m_rcRect.left -= (int) this.m_nSpeed;
                this.m_bWalk = true;
            } else {
                this.m_rcRect.left = nX - (this.m_rcRect.right / 2);
                this.m_bWalk = false;
            }
        } else if (nTexCenX < nX) {
            if (((float) (nX - nTexCenX)) > this.m_nSpeed) {
                this.m_rcRect.left += (int) this.m_nSpeed;
                this.m_bWalk = true;
            } else {
                this.m_rcRect.left = nX - (this.m_rcRect.right / 2);
                this.m_bWalk = false;
            }
        }
        if (this.m_nFlashNum >= 3) {
            this.m_nFlashNum = 3;
        }
        Draw(canvas);
    }

    public void Draw(Canvas canvas) {
        Bitmap GetTex;
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        Bitmap GetTex2;
        int i7;
        int i8;
        int i9;
        int i10;
        int i11;
        int i12;
        Bitmap bitmap;
        int i13;
        int i14;
        int i15;
        int i16;
        int i17;
        int i18;
        switch (this.m_nNo) {
            case 0:
                if (!this.m_bEat) {
                    if (!this.m_bWalk) {
                        if (this.m_nFlashNum > 0) {
                            MoguraView moguraView = this.m_pView;
                            this.m_pView.getClass();
                            GetTex = moguraView.GetTex(60);
                        } else {
                            MoguraView moguraView2 = this.m_pView;
                            this.m_pView.getClass();
                            GetTex = moguraView2.GetTex(54);
                        }
                        canvas.drawBitmap(GetTex, (float) this.m_rcRect.left, (float) this.m_rcRect.top, (Paint) null);
                        this.m_nAnimCnt = 0;
                        break;
                    } else {
                        int i19 = this.m_nAnimCnt;
                        this.m_nAnimCnt = i19 + 1;
                        if (i19 % 15 > 7) {
                            MoguraView moguraView3 = this.m_pView;
                            if (this.m_nFlashNum > 0) {
                                this.m_pView.getClass();
                                i = 65;
                            } else {
                                this.m_pView.getClass();
                                i = 59;
                            }
                            canvas.drawBitmap(moguraView3.GetTex(i), (float) this.m_rcRect.left, (float) this.m_rcRect.top, (Paint) null);
                            break;
                        } else {
                            MoguraView moguraView4 = this.m_pView;
                            if (this.m_nFlashNum > 0) {
                                this.m_pView.getClass();
                                i2 = 64;
                            } else {
                                this.m_pView.getClass();
                                i2 = 58;
                            }
                            canvas.drawBitmap(moguraView4.GetTex(i2), (float) this.m_rcRect.left, (float) this.m_rcRect.top, (Paint) null);
                            break;
                        }
                    }
                } else {
                    if (this.m_nAnimCnt % 15 <= 0) {
                        MoguraView moguraView5 = this.m_pView;
                        if (this.m_nFlashNum > 0) {
                            this.m_pView.getClass();
                            i6 = 61;
                        } else {
                            this.m_pView.getClass();
                            i6 = 55;
                        }
                        canvas.drawBitmap(moguraView5.GetTex(i6), (float) this.m_rcRect.left, (float) this.m_rcRect.top, (Paint) null);
                    } else if (this.m_nAnimCnt % 15 <= 5) {
                        MoguraView moguraView6 = this.m_pView;
                        if (this.m_nFlashNum > 0) {
                            this.m_pView.getClass();
                            i5 = 62;
                        } else {
                            this.m_pView.getClass();
                            i5 = 56;
                        }
                        canvas.drawBitmap(moguraView6.GetTex(i5), (float) this.m_rcRect.left, (float) this.m_rcRect.top, (Paint) null);
                    } else if (this.m_nAnimCnt % 15 <= 10) {
                        MoguraView moguraView7 = this.m_pView;
                        if (this.m_nFlashNum > 0) {
                            this.m_pView.getClass();
                            i4 = 63;
                        } else {
                            this.m_pView.getClass();
                            i4 = 57;
                        }
                        canvas.drawBitmap(moguraView7.GetTex(i4), (float) this.m_rcRect.left, (float) this.m_rcRect.top, (Paint) null);
                    } else {
                        this.m_bEat = false;
                        this.m_nAnimCnt = 0;
                        MoguraView moguraView8 = this.m_pView;
                        if (this.m_nFlashNum > 0) {
                            this.m_pView.getClass();
                            i3 = 63;
                        } else {
                            this.m_pView.getClass();
                            i3 = 57;
                        }
                        canvas.drawBitmap(moguraView8.GetTex(i3), (float) this.m_rcRect.left, (float) this.m_rcRect.top, (Paint) null);
                    }
                    this.m_nAnimCnt++;
                    break;
                }
            case R.styleable.mediba_ad_sdk_android_MasAdView_requestInterval /*1*/:
                if (!this.m_bEat) {
                    if (!this.m_bWalk) {
                        if (this.m_nFlashNum > 0) {
                            MoguraView moguraView9 = this.m_pView;
                            this.m_pView.getClass();
                            bitmap = moguraView9.GetTex(21);
                        } else {
                            bitmap = this.m_pTex;
                        }
                        canvas.drawBitmap(bitmap, (float) this.m_rcRect.left, (float) this.m_rcRect.top, (Paint) null);
                        this.m_nAnimCnt = 0;
                        break;
                    } else {
                        int i20 = this.m_nAnimCnt;
                        this.m_nAnimCnt = i20 + 1;
                        if (i20 % 15 > 7) {
                            MoguraView moguraView10 = this.m_pView;
                            if (this.m_nFlashNum > 0) {
                                this.m_pView.getClass();
                                i13 = 26;
                            } else {
                                this.m_pView.getClass();
                                i13 = 20;
                            }
                            canvas.drawBitmap(moguraView10.GetTex(i13), (float) this.m_rcRect.left, (float) this.m_rcRect.top, (Paint) null);
                            break;
                        } else {
                            MoguraView moguraView11 = this.m_pView;
                            if (this.m_nFlashNum > 0) {
                                this.m_pView.getClass();
                                i14 = 25;
                            } else {
                                this.m_pView.getClass();
                                i14 = 19;
                            }
                            canvas.drawBitmap(moguraView11.GetTex(i14), (float) this.m_rcRect.left, (float) this.m_rcRect.top, (Paint) null);
                            break;
                        }
                    }
                } else {
                    if (this.m_nAnimCnt % 15 <= 0) {
                        MoguraView moguraView12 = this.m_pView;
                        if (this.m_nFlashNum > 0) {
                            this.m_pView.getClass();
                            i18 = 22;
                        } else {
                            this.m_pView.getClass();
                            i18 = 16;
                        }
                        canvas.drawBitmap(moguraView12.GetTex(i18), (float) this.m_rcRect.left, (float) this.m_rcRect.top, (Paint) null);
                    } else if (this.m_nAnimCnt % 15 <= 5) {
                        MoguraView moguraView13 = this.m_pView;
                        if (this.m_nFlashNum > 0) {
                            this.m_pView.getClass();
                            i17 = 23;
                        } else {
                            this.m_pView.getClass();
                            i17 = 17;
                        }
                        canvas.drawBitmap(moguraView13.GetTex(i17), (float) this.m_rcRect.left, (float) this.m_rcRect.top, (Paint) null);
                    } else if (this.m_nAnimCnt % 15 <= 10) {
                        MoguraView moguraView14 = this.m_pView;
                        if (this.m_nFlashNum > 0) {
                            this.m_pView.getClass();
                            i16 = 24;
                        } else {
                            this.m_pView.getClass();
                            i16 = 18;
                        }
                        canvas.drawBitmap(moguraView14.GetTex(i16), (float) this.m_rcRect.left, (float) this.m_rcRect.top, (Paint) null);
                    } else {
                        this.m_bEat = false;
                        this.m_nAnimCnt = 0;
                        MoguraView moguraView15 = this.m_pView;
                        if (this.m_nFlashNum > 0) {
                            this.m_pView.getClass();
                            i15 = 24;
                        } else {
                            this.m_pView.getClass();
                            i15 = 18;
                        }
                        canvas.drawBitmap(moguraView15.GetTex(i15), (float) this.m_rcRect.left, (float) this.m_rcRect.top, (Paint) null);
                    }
                    this.m_nAnimCnt++;
                    break;
                }
            case R.styleable.mediba_ad_sdk_android_MasAdView_refreshAnimation /*2*/:
                if (!this.m_bEat) {
                    if (!this.m_bWalk) {
                        if (this.m_nFlashNum > 0) {
                            MoguraView moguraView16 = this.m_pView;
                            this.m_pView.getClass();
                            GetTex2 = moguraView16.GetTex(48);
                        } else {
                            MoguraView moguraView17 = this.m_pView;
                            this.m_pView.getClass();
                            GetTex2 = moguraView17.GetTex(42);
                        }
                        canvas.drawBitmap(GetTex2, (float) this.m_rcRect.left, (float) this.m_rcRect.top, (Paint) null);
                        this.m_nAnimCnt = 0;
                        break;
                    } else {
                        int i21 = this.m_nAnimCnt;
                        this.m_nAnimCnt = i21 + 1;
                        if (i21 % 15 > 7) {
                            MoguraView moguraView18 = this.m_pView;
                            if (this.m_nFlashNum > 0) {
                                this.m_pView.getClass();
                                i7 = 53;
                            } else {
                                this.m_pView.getClass();
                                i7 = 47;
                            }
                            canvas.drawBitmap(moguraView18.GetTex(i7), (float) this.m_rcRect.left, (float) this.m_rcRect.top, (Paint) null);
                            break;
                        } else {
                            MoguraView moguraView19 = this.m_pView;
                            if (this.m_nFlashNum > 0) {
                                this.m_pView.getClass();
                                i8 = 52;
                            } else {
                                this.m_pView.getClass();
                                i8 = 46;
                            }
                            canvas.drawBitmap(moguraView19.GetTex(i8), (float) this.m_rcRect.left, (float) this.m_rcRect.top, (Paint) null);
                            break;
                        }
                    }
                } else {
                    if (this.m_nAnimCnt % 15 <= 0) {
                        MoguraView moguraView20 = this.m_pView;
                        if (this.m_nFlashNum > 0) {
                            this.m_pView.getClass();
                            i12 = 49;
                        } else {
                            this.m_pView.getClass();
                            i12 = 43;
                        }
                        canvas.drawBitmap(moguraView20.GetTex(i12), (float) this.m_rcRect.left, (float) this.m_rcRect.top, (Paint) null);
                    } else if (this.m_nAnimCnt % 15 <= 5) {
                        MoguraView moguraView21 = this.m_pView;
                        if (this.m_nFlashNum > 0) {
                            this.m_pView.getClass();
                            i11 = 50;
                        } else {
                            this.m_pView.getClass();
                            i11 = 44;
                        }
                        canvas.drawBitmap(moguraView21.GetTex(i11), (float) this.m_rcRect.left, (float) this.m_rcRect.top, (Paint) null);
                    } else if (this.m_nAnimCnt % 15 <= 10) {
                        MoguraView moguraView22 = this.m_pView;
                        if (this.m_nFlashNum > 0) {
                            this.m_pView.getClass();
                            i10 = 51;
                        } else {
                            this.m_pView.getClass();
                            i10 = 45;
                        }
                        canvas.drawBitmap(moguraView22.GetTex(i10), (float) this.m_rcRect.left, (float) this.m_rcRect.top, (Paint) null);
                    } else {
                        this.m_bEat = false;
                        this.m_nAnimCnt = 0;
                        MoguraView moguraView23 = this.m_pView;
                        if (this.m_nFlashNum > 0) {
                            this.m_pView.getClass();
                            i9 = 51;
                        } else {
                            this.m_pView.getClass();
                            i9 = 45;
                        }
                        canvas.drawBitmap(moguraView23.GetTex(i9), (float) this.m_rcRect.left, (float) this.m_rcRect.top, (Paint) null);
                    }
                    this.m_nAnimCnt++;
                    break;
                }
        }
        int GPosY = this.m_pView.game_rect.top;
        MoguraView moguraView24 = this.m_pView;
        this.m_pView.getClass();
        Bitmap pTex = moguraView24.GetTex(30);
        int nPosY = GPosY + 100;
        int nTexW = pTex.getWidth();
        int nTexH = pTex.getHeight();
        Rect rcScrRect = new Rect(0, 0, nTexW, nTexH);
        Rect rcDesRect = new Rect((int) (((float) 390) * this.m_pView.re_size_width), (int) (((float) nPosY) * this.m_pView.re_size_height), ((int) (((float) 390) * this.m_pView.re_size_width)) + ((int) (((float) nTexW) * this.m_pView.re_size_width)), ((int) (((float) nPosY) * this.m_pView.re_size_height)) + ((int) (((float) nTexH) * this.m_pView.re_size_height)));
        canvas.drawBitmap(pTex, rcScrRect, rcDesRect, (Paint) null);
        int nPosY2 = GPosY + 100;
        MoguraView moguraView25 = this.m_pView;
        this.m_pView.getClass();
        Bitmap pTex2 = moguraView25.GetTex(31);
        int nTexW2 = pTex2.getWidth();
        int nTexH2 = pTex2.getHeight();
        rcScrRect.set(0, 0, nTexW2, nTexH2);
        rcDesRect.set((int) (((float) 420) * this.m_pView.re_size_width), (int) (((float) nPosY2) * this.m_pView.re_size_height), ((int) (((float) 420) * this.m_pView.re_size_width)) + ((int) (((float) nTexW2) * this.m_pView.re_size_width)), ((int) (((float) nPosY2) * this.m_pView.re_size_height)) + ((int) (((float) nTexH2) * this.m_pView.re_size_height)));
        canvas.drawBitmap(pTex2, rcScrRect, rcDesRect, (Paint) null);
        int nPosY3 = GPosY + 95;
        MoguraView moguraView26 = this.m_pView;
        this.m_pView.getClass();
        Bitmap pTex3 = moguraView26.GetTex(this.m_nFlashNum + 66);
        int nTexW3 = pTex3.getWidth();
        int nTexH3 = pTex3.getHeight();
        rcScrRect.set(0, 0, nTexW3, nTexH3);
        rcDesRect.set((int) (((float) 440) * this.m_pView.re_size_width), (int) (((float) nPosY3) * this.m_pView.re_size_height), ((int) (((float) 440) * this.m_pView.re_size_width)) + ((int) (((float) (nTexW3 + 10)) * this.m_pView.re_size_width)), ((int) (((float) nPosY3) * this.m_pView.re_size_height)) + ((int) (((float) (nTexH3 + 10)) * this.m_pView.re_size_height)));
        canvas.drawBitmap(pTex3, rcScrRect, rcDesRect, (Paint) null);
    }

    public void Debug(Canvas canvas) {
        Paint paint = new Paint();
        paint.setColor(-1);
        paint.setTextSize(26.0f);
        canvas.drawText("APPLE_NUM : " + this.m_nAppleNum, 0.0f, 250.0f, paint);
    }

    public boolean Touch(int nX, int nY) {
        if (HitCheckDot(nX, nY)) {
            return true;
        }
        return false;
    }

    public boolean HitCheckDot(int nX, int nY) {
        return this.m_rcRect.left <= nX && nX < this.m_rcRect.left + this.m_rcRect.right && this.m_rcRect.top <= nY && nY < this.m_rcRect.top + this.m_rcRect.bottom;
    }

    public void AddSpeed() {
        this.m_nSpeed += 0.5f;
    }

    public void AddAppleNum() {
        this.m_nAppleNum++;
    }

    public void AddFlashNum() {
        this.m_nFlashNum++;
    }

    public void SubFlashNum() {
        int i = this.m_nFlashNum - 1;
        this.m_nFlashNum = i;
        if (i < 0) {
            this.m_nFlashNum = 0;
        }
    }

    public void EatStart() {
        this.m_bEat = true;
        this.m_nAnimCnt = 0;
    }

    public void SetAppleNum(int nVal) {
        this.m_nAppleNum = nVal;
    }

    public Rect GetRect() {
        Rect reRect = new Rect(this.m_rcRect);
        switch (this.m_nNo) {
            case R.styleable.mediba_ad_sdk_android_MasAdView_refreshAnimation /*2*/:
                reRect.right -= 15;
                break;
        }
        return reRect;
    }

    public int GetAppleNum() {
        return this.m_nAppleNum;
    }

    public int GetFlashNum() {
        return this.m_nFlashNum;
    }
}
