package jp.co.arttec.satbox.PickRobots;

import android.content.Intent;
import android.content.SharedPreferences;
import android.media.SoundPool;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Gallery;
import android.widget.SpinnerAdapter;
import android.widget.Toast;

public class StageSelect extends BaseActivity implements AdapterView.OnItemClickListener {
    private static final int DIP_SPACE = 2;
    private StageSelectAdapter _adapter;
    private boolean _clickFlag = false;
    private Gallery _gallery;
    private int _gallery_space;
    private boolean flg_vibrator;
    private int soundIds;
    private SoundPool soundPool;
    private Vibrator vibrator;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.soundPool = new SoundPool(1, 3, 0);
        this.soundIds = this.soundPool.load(this, R.raw.start, 1);
        this.vibrator = (Vibrator) getSystemService("vibrator");
        this.flg_vibrator = getSharedPreferences("prefkey", 0).getBoolean("vibflg", true);
        setContentView((int) R.layout.select);
        setVolumeControlStream(3);
        this._gallery = (Gallery) findViewById(R.id.Gallery);
        this._gallery_space = (int) ((2.0f * getResources().getDisplayMetrics().density) + 0.5f);
        this._gallery.setSpacing(this._gallery_space);
        this._adapter = new StageSelectAdapter(this);
        this._gallery.setAdapter((SpinnerAdapter) this._adapter);
        this._gallery.setOnItemClickListener(this);
        this._gallery.setSelection(1);
        this._gallery.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                ((SelectView) StageSelect.this.findViewById(R.id.select_view)).setType(position);
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        try {
            Intent intent = new Intent(getApplicationContext(), Title.class);
            intent.addFlags(268435456);
            startActivity(intent);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Error", 1).show();
        }
        finish();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this._adapter.recycle();
    }

    public synchronized void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        if (!this._clickFlag) {
            this._clickFlag = true;
            this.soundPool.play(this.soundIds, 0.99f, 0.99f, 1, 0, 1.0f);
            try {
                Intent intent = new Intent(getApplicationContext(), Ninjya.class);
                intent.addFlags(268435456);
                intent.putExtra("CharNo", position);
                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), "Error", 1).show();
            }
            finish();
        }
        return;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        finish();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        this.flg_vibrator = getSharedPreferences("prefkey", 0).getBoolean("vibflg", true);
        if (this.flg_vibrator) {
            menu.findItem(R.id.menu_vib_switch).setTitle("Vibrator(Off)");
        } else {
            menu.findItem(R.id.menu_vib_switch).setTitle("Vibrator(On)");
        }
        return super.onPrepareOptionsMenu(menu);
    }

    public synchronized boolean onMenuItemSelected(int featureId, MenuItem item) {
        boolean result;
        result = super.onMenuItemSelected(featureId, item);
        if (item.getItemId() == R.id.menu_vib_switch) {
            this.flg_vibrator = !this.flg_vibrator;
            SharedPreferences.Editor editor = getSharedPreferences("prefkey", 0).edit();
            editor.putBoolean("vibflg", this.flg_vibrator);
            editor.commit();
            if (this.flg_vibrator) {
                this.vibrator.vibrate(100);
            }
        } else if (item.getItemId() == R.id.menu_apk) {
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=pub:SAT-BOX")));
        } else if (item.getItemId() == R.id.menu_hp) {
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://www.arttec.co.jp/sat-box/")));
        } else if (item.getItemId() == R.id.menu_end) {
            finish();
        }
        return result;
    }
}
