package jp.co.arttec.satbox.PickRobots;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

/* compiled from: BackGround */
class CBackGround {
    Rect dst;
    private CFly1 m_Fly1;
    private CFly2 m_Fly2;
    private CParam[] m_Prm = new CParam[3];
    private CUfo m_Ufo;
    private MoguraView m_pView;
    Rect src;

    /* compiled from: BackGround */
    private class CParam {
        public int m_nMaxCnt;
        public int m_nWaitCnt = 0;

        public CParam(int nMaxCnt) {
            this.m_nMaxCnt = nMaxCnt;
        }
    }

    public CBackGround(MoguraView pView) {
        this.m_pView = pView;
        int nRndY = ((int) (Math.random() * 200.0d)) + 100;
        MoguraView moguraView = this.m_pView;
        this.m_pView.getClass();
        this.m_Fly1 = new CFly1(moguraView, -1000, nRndY, 28);
        MoguraView moguraView2 = this.m_pView;
        this.m_pView.getClass();
        this.m_Fly2 = new CFly2(moguraView2, -1000, nRndY, 29);
        MoguraView moguraView3 = this.m_pView;
        this.m_pView.getClass();
        this.m_Ufo = new CUfo(moguraView3, -1000, nRndY, 27);
        this.m_Prm[0] = new CParam((int) (Math.random() * 500.0d));
        this.m_Prm[1] = new CParam(((int) (Math.random() * 1000.0d)) + 500);
        this.m_Prm[2] = new CParam(((int) (Math.random() * 1000.0d)) + 1000);
        this.src = new Rect(0, 0, 0, 0);
        this.dst = new Rect(0, 0, 0, 0);
    }

    public void Exec(Canvas canvas) {
        this.m_pView.getClass();
        this.m_pView.getClass();
        this.m_pView.getClass();
        this.m_pView.getClass();
        this.m_pView.getClass();
        int[] nTex = {8, 9, 10, 11, 12};
        int i = nTex.length;
        while (true) {
            if (i <= 0) {
                break;
            } else if (this.m_pView.GetLevel() == i) {
                canvas.drawBitmap(this.m_pView.GetTex(nTex[i - 1]), this.src, this.dst, (Paint) null);
                break;
            } else {
                i--;
            }
        }
        CFlyBase[] Fly = {this.m_Fly1, this.m_Fly2, this.m_Ufo};
        for (int i2 = 0; i2 < Fly.length; i2++) {
            if (!Fly[i2].Exec(canvas)) {
                this.m_Prm[i2].m_nWaitCnt++;
            }
            if (this.m_Prm[i2].m_nWaitCnt >= this.m_Prm[i2].m_nMaxCnt) {
                int nRndY = ((int) (Math.random() * 200.0d)) + 100;
                switch (i2) {
                    case 0:
                        MoguraView moguraView = this.m_pView;
                        int GetDispX = this.m_pView.GetDispX();
                        this.m_pView.getClass();
                        this.m_Fly1 = new CFly1(moguraView, GetDispX, nRndY, 28);
                        break;
                    case R.styleable.mediba_ad_sdk_android_MasAdView_requestInterval /*1*/:
                        MoguraView moguraView2 = this.m_pView;
                        int GetDispX2 = this.m_pView.GetDispX();
                        this.m_pView.getClass();
                        this.m_Fly2 = new CFly2(moguraView2, GetDispX2, nRndY, 29);
                        break;
                    case R.styleable.mediba_ad_sdk_android_MasAdView_refreshAnimation /*2*/:
                        MoguraView moguraView3 = this.m_pView;
                        int GetDispX3 = this.m_pView.GetDispX();
                        this.m_pView.getClass();
                        this.m_Ufo = new CUfo(moguraView3, GetDispX3, nRndY, 27);
                        break;
                }
                this.m_Prm[i2] = new CParam(((int) (Math.random() * 1000.0d)) + 500);
            }
        }
    }

    public void Debug(Canvas canvas) {
        Paint paint = new Paint();
        paint.setColor(-1);
        this.m_pView.getClass();
        paint.setTextSize(20.0f);
        canvas.drawText("<BackGround Debug>", 0.0f, 230.0f, paint);
        for (int i = 0; i < this.m_Prm.length; i++) {
            canvas.drawText("FLY" + i + " : " + this.m_Prm[i].m_nWaitCnt + " / " + this.m_Prm[i].m_nMaxCnt, 0.0f, (float) ((i * 20) + 250), paint);
        }
    }

    public void setSrcToResource() {
        Rect rect = this.src;
        MoguraView moguraView = this.m_pView;
        this.m_pView.getClass();
        int width = moguraView.GetTex(8).getWidth();
        MoguraView moguraView2 = this.m_pView;
        this.m_pView.getClass();
        rect.set(0, 0, width, moguraView2.GetTex(8).getHeight());
    }

    public void setDst(Rect dst2) {
        this.dst = dst2;
    }

    public Rect GetRect() {
        return this.dst;
    }
}
