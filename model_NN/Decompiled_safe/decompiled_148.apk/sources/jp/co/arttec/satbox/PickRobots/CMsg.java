package jp.co.arttec.satbox.PickRobots;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.media.SoundPool;

/* compiled from: Msg */
class CMsg {
    private Rect dst;
    private Bitmap l_num_1;
    private Bitmap l_num_2;
    private Bitmap l_num_3;
    private Bitmap l_num_4;
    private Bitmap l_num_5;
    private Rect level_dst;
    private Rect level_src;
    private int m_nAlpha = 0;
    private Bitmap m_pTex;
    private MoguraView m_pView;
    private Rect m_rcRect;
    private Rect src;

    public CMsg(MoguraView pView) {
        this.m_pView = pView;
        MoguraView moguraView = this.m_pView;
        this.m_pView.getClass();
        this.m_pTex = moguraView.GetTex(33);
        int nTexW = (int) (((float) this.m_pTex.getWidth()) * this.m_pView.re_size_width);
        int nTexH = (int) (((float) this.m_pTex.getHeight()) * this.m_pView.re_size_width);
        this.m_rcRect = new Rect(this.m_pView.game_rect.right, (this.m_pView.game_rect.bottom / 2) - (nTexH / 2), this.m_pView.game_rect.right + nTexW, ((this.m_pView.game_rect.bottom / 2) - (nTexH / 2)) + nTexH);
        MoguraView moguraView2 = this.m_pView;
        this.m_pView.getClass();
        int backW = (int) (((float) moguraView2.GetTex(34).getWidth()) * this.m_pView.re_size_width);
        MoguraView moguraView3 = this.m_pView;
        this.m_pView.getClass();
        int backH = (int) (((float) moguraView3.GetTex(34).getHeight()) * this.m_pView.re_size_height);
        MoguraView moguraView4 = this.m_pView;
        this.m_pView.getClass();
        int width = moguraView4.GetTex(34).getWidth();
        MoguraView moguraView5 = this.m_pView;
        this.m_pView.getClass();
        this.src = new Rect(0, 0, width, moguraView5.GetTex(34).getHeight());
        this.dst = new Rect(this.m_pView.getLeft(), (this.m_pView.game_rect.bottom / 2) - (backH / 2), this.m_pView.getLeft() + backW, ((this.m_pView.game_rect.bottom / 2) - (backH / 2)) + backH);
        SoundPool GetSndPool = this.m_pView.GetSndPool();
        MoguraView moguraView6 = this.m_pView;
        this.m_pView.getClass();
        GetSndPool.play(moguraView6.GetSnd(4), 1.0f, 1.0f, 0, 0, 1.0f);
        this.level_src = new Rect(0, 0, this.m_pTex.getWidth(), this.m_pTex.getHeight());
        this.level_dst = new Rect(0, 0, 0, 0);
        this.l_num_1 = BitmapFactory.decodeResource(this.m_pView.getResources(), R.drawable.l_num_1);
        this.l_num_2 = BitmapFactory.decodeResource(this.m_pView.getResources(), R.drawable.l_num_2);
        this.l_num_3 = BitmapFactory.decodeResource(this.m_pView.getResources(), R.drawable.l_num_3);
        this.l_num_4 = BitmapFactory.decodeResource(this.m_pView.getResources(), R.drawable.l_num_4);
        this.l_num_5 = BitmapFactory.decodeResource(this.m_pView.getResources(), R.drawable.l_num_5);
    }

    public void Exec(Canvas canvas) {
        this.m_rcRect.left -= 4;
        if (this.m_rcRect.left >= -250) {
            if (this.m_nAlpha >= 180) {
                this.m_nAlpha = 180;
            } else {
                this.m_nAlpha += 15;
            }
        } else if (this.m_nAlpha <= 0) {
            this.m_nAlpha = 0;
        } else {
            this.m_nAlpha -= 15;
        }
        Draw(canvas);
    }

    public void Draw(Canvas canvas) {
        Paint paint = new Paint();
        MoguraView moguraView = this.m_pView;
        this.m_pView.getClass();
        int height = (int) (((float) moguraView.GetTex(34).getHeight()) * this.m_pView.re_size_height);
        paint.setAlpha(this.m_nAlpha);
        if (this.m_pView.GetLevel() == 1) {
            MoguraView moguraView2 = this.m_pView;
            this.m_pView.getClass();
            canvas.drawBitmap(moguraView2.GetTex(34), this.src, this.dst, paint);
        } else if (this.m_pView.GetLevel() == 2) {
            MoguraView moguraView3 = this.m_pView;
            this.m_pView.getClass();
            canvas.drawBitmap(moguraView3.GetTex(35), this.src, this.dst, paint);
        } else if (this.m_pView.GetLevel() == 3) {
            MoguraView moguraView4 = this.m_pView;
            this.m_pView.getClass();
            canvas.drawBitmap(moguraView4.GetTex(36), this.src, this.dst, paint);
        } else if (this.m_pView.GetLevel() == 4) {
            MoguraView moguraView5 = this.m_pView;
            this.m_pView.getClass();
            canvas.drawBitmap(moguraView5.GetTex(37), this.src, this.dst, paint);
        } else if (this.m_pView.GetLevel() == 5) {
            MoguraView moguraView6 = this.m_pView;
            this.m_pView.getClass();
            canvas.drawBitmap(moguraView6.GetTex(38), this.src, this.dst, paint);
        }
        int nTexH = (int) (((float) this.m_pTex.getHeight()) * this.m_pView.re_size_height);
        this.level_dst.set(this.m_rcRect.left, (this.m_pView.game_rect.bottom / 2) - (nTexH / 2), this.m_rcRect.left + ((int) (((float) this.m_pTex.getWidth()) * this.m_pView.re_size_width)), ((this.m_pView.game_rect.bottom / 2) - (nTexH / 2)) + nTexH);
        paint.setAlpha(this.m_nAlpha);
        canvas.drawBitmap(this.m_pTex, this.level_src, this.level_dst, paint);
        int nW = (int) (((float) this.l_num_1.getWidth()) * this.m_pView.re_size_width);
        int nH = (int) (((float) this.l_num_1.getHeight()) * this.m_pView.re_size_height);
        Rect rcScrRect = new Rect(0, 0, this.l_num_1.getWidth(), this.l_num_1.getHeight());
        Rect rcDesRect = new Rect((int) (((float) this.m_rcRect.left) + (120.0f * this.m_pView.re_size_width)), (this.m_pView.game_rect.bottom / 2) - (nH / 2), (int) (((float) this.m_rcRect.left) + (120.0f * this.m_pView.re_size_width) + ((float) nW)), ((this.m_pView.game_rect.bottom / 2) - (nH / 2)) + nH);
        paint.setAlpha(this.m_nAlpha);
        if (this.m_pView.GetLevel() == 1) {
            canvas.drawBitmap(this.l_num_1, rcScrRect, rcDesRect, paint);
        } else if (this.m_pView.GetLevel() == 2) {
            canvas.drawBitmap(this.l_num_2, rcScrRect, rcDesRect, paint);
        } else if (this.m_pView.GetLevel() == 3) {
            canvas.drawBitmap(this.l_num_3, rcScrRect, rcDesRect, paint);
        } else if (this.m_pView.GetLevel() == 4) {
            canvas.drawBitmap(this.l_num_4, rcScrRect, rcDesRect, paint);
        } else if (this.m_pView.GetLevel() == 5) {
            canvas.drawBitmap(this.l_num_5, rcScrRect, rcDesRect, paint);
        }
    }

    public void destroyBMP() {
        if (this.l_num_1 != null) {
            this.l_num_1.recycle();
        }
        if (this.l_num_2 != null) {
            this.l_num_2.recycle();
        }
        if (this.l_num_3 != null) {
            this.l_num_3.recycle();
        }
        if (this.l_num_4 != null) {
            this.l_num_4.recycle();
        }
        if (this.l_num_5 != null) {
            this.l_num_5.recycle();
        }
    }
}
