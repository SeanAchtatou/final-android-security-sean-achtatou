package jp.co.arttec.satbox.PickRobots;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.media.SoundPool;
import android.os.Vibrator;

/* compiled from: DangerBase */
class CDangerBase extends CFall {
    int DANGER_DMG = 20;
    final int DANGER_EXP_TIME = 10;
    final int DANGER_HIT_TIME = 10;
    protected int m_ExpCnt;
    protected int m_nDmg;
    protected int m_nDmgLen;

    public CDangerBase(MoguraView pView, int nPosX, int nTexIdx, int nSnd) {
        super(pView, nPosX, nTexIdx, nSnd);
        CHitPoint HP = this.m_pView.GetHP();
        this.m_nDmgLen = (HP.GetMetaW() / HP.GetMaxHp()) * this.DANGER_DMG;
        switch (this.m_pView.char_no) {
            case 0:
                this.DANGER_DMG = 0;
                break;
            case R.styleable.mediba_ad_sdk_android_MasAdView_requestInterval /*1*/:
                this.DANGER_DMG = 20;
                break;
            case R.styleable.mediba_ad_sdk_android_MasAdView_refreshAnimation /*2*/:
                this.DANGER_DMG = 30;
                break;
        }
        this.m_nDmg = this.DANGER_DMG;
        this.m_nAnimCnt = 0;
        this.m_ExpCnt = 0;
        this.m_nScore = 0;
    }

    public boolean Exec(Canvas canvas) {
        SoundPool SP = this.m_pView.GetSndPool();
        Vibrator V = this.m_pView.GetVib();
        float f = this.m_fRot + this.m_fAddRot;
        this.m_fRot = f;
        if (f >= 360.0f) {
            this.m_fRot -= 360.0f;
        }
        if (this.m_ExpCnt == 0) {
            if (this.m_rcRect.top + this.m_rcRect.bottom >= this.m_pView.ground_y) {
                MoguraView moguraView = this.m_pView;
                this.m_pView.getClass();
                SP.play(moguraView.GetSnd(0), 1.0f, 1.0f, 0, 0, 1.0f);
                CCombo.SetCmbCnt(0);
                if (this.m_pView.GetVibFlg().booleanValue()) {
                    V.vibrate(100);
                }
                this.m_pView.SetCmbFlg(false);
                this.m_ExpCnt++;
            }
            if (HitCheckBox(this.m_pView.GetChar().GetRect())) {
                this.m_pView.GetHP().Dmg(this.m_nDmg);
                MoguraView moguraView2 = this.m_pView;
                this.m_pView.getClass();
                SP.play(moguraView2.GetSnd(0), 1.0f, 1.0f, 0, 0, 1.0f);
                CCombo.SetCmbCnt(0);
                if (this.m_pView.GetVibFlg().booleanValue()) {
                    V.vibrate(100);
                }
                this.m_pView.SetCmbFlg(false);
                this.m_ExpCnt++;
            }
        } else if (this.m_ExpCnt > 0) {
            int i = this.m_ExpCnt;
            this.m_ExpCnt = i + 1;
            if (i >= 10) {
                return false;
            }
            MoguraView moguraView3 = this.m_pView;
            this.m_pView.getClass();
            canvas.drawBitmap(moguraView3.GetTex(7), (float) this.m_rcRect.left, (float) this.m_rcRect.top, (Paint) null);
        }
        if (this.m_nAnimCnt > 0) {
            if (this.m_nAnimCnt >= 10) {
                return false;
            }
            if (this.m_nAnimCnt == 1) {
                CCombo.SetCmbCnt(CCombo.GetCmbCnt() + 1);
                MoguraView moguraView4 = this.m_pView;
                this.m_pView.getClass();
                canvas.drawBitmap(moguraView4.GetTex(6), (float) this.m_rcRect.left, (float) this.m_rcRect.top, (Paint) null);
            }
            this.m_nAnimCnt++;
        } else if (this.m_ExpCnt == 0) {
            Move();
            Draw(canvas);
        }
        return true;
    }
}
