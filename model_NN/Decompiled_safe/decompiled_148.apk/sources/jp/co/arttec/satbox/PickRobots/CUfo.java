package jp.co.arttec.satbox.PickRobots;

/* compiled from: Ufo */
class CUfo extends CFlyBase {
    final int UFO_TOSS_HEIGHT = 20;
    private double m_dwAddRot;
    private double m_dwRot;
    private int m_nCenterPosY;

    public CUfo(MoguraView pView, int nPosX, int nPosY, int nTexID) {
        super(pView, nPosX, nPosY, nTexID);
        if (this.m_nSize >= 0) {
            this.m_nSpd = -2;
        } else {
            this.m_nSpd = -1;
        }
        this.m_nCenterPosY = this.m_rcRect.top + (this.m_rcRect.bottom / 2);
        this.m_dwRot = 0.0d;
        this.m_dwAddRot = 0.20000000298023224d;
    }

    public boolean Move() {
        double d = this.m_dwRot + this.m_dwAddRot;
        this.m_dwRot = d;
        if (d >= 180.0d) {
            this.m_dwRot = 0.0d;
        }
        this.m_rcRect.top = ((int) (Math.sin(this.m_dwRot) * 20.0d)) + this.m_nCenterPosY;
        if (this.m_rcRect.left <= (-this.m_pTex.getWidth())) {
            return false;
        }
        this.m_rcRect.left += this.m_nSpd;
        return true;
    }
}
