package jp.co.arttec.satbox.PickRobots;

public final class R {

    public static final class attr {
        public static final int backgroundColor = 2130771968;
        public static final int refreshAnimation = 2130771970;
        public static final int requestInterval = 2130771969;
        public static final int testMode = 2130771972;
        public static final int visibility = 2130771971;
    }

    public static final class color {
        public static final int pick_back_color = 2131034112;
    }

    public static final class drawable {
        public static final int ad_back = 2130837504;
        public static final int apple1 = 2130837505;
        public static final int apple2 = 2130837506;
        public static final int bakuhatu = 2130837507;
        public static final int batu = 2130837508;
        public static final int bg1 = 2130837509;
        public static final int bg2 = 2130837510;
        public static final int bg3 = 2130837511;
        public static final int bg4 = 2130837512;
        public static final int bg5 = 2130837513;
        public static final int bomb1 = 2130837514;
        public static final int bomb2 = 2130837515;
        public static final int bomb3 = 2130837516;
        public static final int bomb4 = 2130837517;
        public static final int bomb5 = 2130837518;
        public static final int button1 = 2130837519;
        public static final int char2 = 2130837520;
        public static final int char22 = 2130837521;
        public static final int char2_ = 2130837522;
        public static final int char2_2 = 2130837523;
        public static final int char2eat1 = 2130837524;
        public static final int char2eat12 = 2130837525;
        public static final int char2eat2 = 2130837526;
        public static final int char2eat22 = 2130837527;
        public static final int char2eat3 = 2130837528;
        public static final int char2eat32 = 2130837529;
        public static final int char2left = 2130837530;
        public static final int char2left2 = 2130837531;
        public static final int char2right = 2130837532;
        public static final int char2right2 = 2130837533;
        public static final int char3 = 2130837534;
        public static final int char32 = 2130837535;
        public static final int char3eat1 = 2130837536;
        public static final int char3eat12 = 2130837537;
        public static final int char3eat2 = 2130837538;
        public static final int char3eat22 = 2130837539;
        public static final int char3eat3 = 2130837540;
        public static final int char3eat32 = 2130837541;
        public static final int char3left = 2130837542;
        public static final int char3left2 = 2130837543;
        public static final int char3right = 2130837544;
        public static final int char3right2 = 2130837545;
        public static final int chareat1 = 2130837546;
        public static final int chareat12 = 2130837547;
        public static final int chareat2 = 2130837548;
        public static final int chareat22 = 2130837549;
        public static final int chareat3 = 2130837550;
        public static final int chareat32 = 2130837551;
        public static final int charleft = 2130837552;
        public static final int charleft2 = 2130837553;
        public static final int charright = 2130837554;
        public static final int charright2 = 2130837555;
        public static final int combo = 2130837556;
        public static final int fly1 = 2130837557;
        public static final int fly1_r = 2130837558;
        public static final int fly2 = 2130837559;
        public static final int fly2_r = 2130837560;
        public static final int fly3 = 2130837561;
        public static final int fly3_r = 2130837562;
        public static final int fly4 = 2130837563;
        public static final int fly4_r = 2130837564;
        public static final int gameover = 2130837565;
        public static final int gameover2 = 2130837566;
        public static final int gameover3 = 2130837567;
        public static final int gameover4 = 2130837568;
        public static final int haikei = 2130837569;
        public static final int howto = 2130837570;
        public static final int howto1 = 2130837571;
        public static final int hp_meta1 = 2130837572;
        public static final int hp_meta2 = 2130837573;
        public static final int icon = 2130837574;
        public static final int itemicon = 2130837575;
        public static final int l_num_1 = 2130837576;
        public static final int l_num_2 = 2130837577;
        public static final int l_num_3 = 2130837578;
        public static final int l_num_4 = 2130837579;
        public static final int l_num_5 = 2130837580;
        public static final int level = 2130837581;
        public static final int level1 = 2130837582;
        public static final int level2 = 2130837583;
        public static final int level3 = 2130837584;
        public static final int level4 = 2130837585;
        public static final int level5 = 2130837586;
        public static final int num = 2130837587;
        public static final int num_0 = 2130837588;
        public static final int num_1 = 2130837589;
        public static final int num_2 = 2130837590;
        public static final int num_3 = 2130837591;
        public static final int num_4 = 2130837592;
        public static final int num_5 = 2130837593;
        public static final int num_6 = 2130837594;
        public static final int num_7 = 2130837595;
        public static final int num_8 = 2130837596;
        public static final int num_9 = 2130837597;
        public static final int pc_satbox = 2130837598;
        public static final int plus = 2130837599;
        public static final int ranking = 2130837600;
        public static final int ranking1 = 2130837601;
        public static final int sat_logo = 2130837602;
        public static final int score = 2130837603;
        public static final int select_back = 2130837604;
        public static final int select_charcter = 2130837605;
        public static final int select_gauge = 2130837606;
        public static final int select_guard = 2130837607;
        public static final int select_range = 2130837608;
        public static final int select_robo1 = 2130837609;
        public static final int select_robo2 = 2130837610;
        public static final int select_robo3 = 2130837611;
        public static final int select_speed = 2130837612;
        public static final int star2 = 2130837613;
        public static final int ufo = 2130837614;
    }

    public static final class id {
        public static final int Gallery = 2131296292;
        public static final int adproxy = 2131296258;
        public static final int credit = 2131296256;
        public static final int end_bt_end = 2131296262;
        public static final int end_bt_score = 2131296261;
        public static final int footer = 2131296286;
        public static final int header = 2131296283;
        public static final int menu_apk = 2131296297;
        public static final int menu_end = 2131296299;
        public static final int menu_hp = 2131296298;
        public static final int menu_vib_switch = 2131296296;
        public static final int no10name = 2131296281;
        public static final int no10score = 2131296282;
        public static final int no1name = 2131296263;
        public static final int no1score = 2131296264;
        public static final int no2name = 2131296265;
        public static final int no2score = 2131296266;
        public static final int no3name = 2131296267;
        public static final int no3score = 2131296268;
        public static final int no4name = 2131296269;
        public static final int no4score = 2131296270;
        public static final int no5name = 2131296271;
        public static final int no5score = 2131296272;
        public static final int no6name = 2131296273;
        public static final int no6score = 2131296274;
        public static final int no7name = 2131296275;
        public static final int no7score = 2131296276;
        public static final int no8name = 2131296277;
        public static final int no8score = 2131296278;
        public static final int no9name = 2131296279;
        public static final int no9score = 2131296280;
        public static final int over_char = 2131296259;
        public static final int play_view = 2131296257;
        public static final int rank_tx_touch = 2131296287;
        public static final int ranklistview = 2131296285;
        public static final int rankname = 2131296284;
        public static final int result = 2131296260;
        public static final int select_view = 2131296293;
        public static final int title_howto = 2131296295;
        public static final int title_ranking = 2131296294;
        public static final int txtItem1 = 2131296289;
        public static final int txtItem21 = 2131296290;
        public static final int txtItem22 = 2131296291;
        public static final int txtRankNo = 2131296288;
    }

    public static final class layout {
        public static final int credit = 2130903040;
        public static final int game = 2130903041;
        public static final int gameover = 2130903042;
        public static final int hiscore = 2130903043;
        public static final int main = 2130903044;
        public static final int rank = 2130903045;
        public static final int rankrow = 2130903046;
        public static final int rankrow2 = 2130903047;
        public static final int select = 2130903048;
        public static final int title = 2130903049;
    }

    public static final class menu {
        public static final int menu = 2131230720;
    }

    public static final class raw {
        public static final int baku = 2130968576;
        public static final int bakuha = 2130968577;
        public static final int flash = 2130968578;
        public static final int get = 2130968579;
        public static final int kumo_se = 2130968580;
        public static final int levelup = 2130968581;
        public static final int main = 2130968582;
        public static final int miss = 2130968583;
        public static final int se_title = 2130968584;
        public static final int start = 2130968585;
        public static final int tawara_se = 2130968586;
        public static final int touch = 2130968587;
        public static final int yarare = 2130968588;
    }

    public static final class string {
        public static final int app_name = 2131099649;
        public static final int close = 2131099653;
        public static final int hello = 2131099648;
        public static final int howto_button = 2131099650;
        public static final int howto_str = 2131099652;
        public static final int rank_button = 2131099651;
    }

    public static final class style {
        public static final int TableNameStyle = 2131165185;
        public static final int TableNoStyle = 2131165184;
        public static final int TableScoreStyle = 2131165186;
    }

    public static final class styleable {
        public static final int[] default_gallery = {16842828};
        public static final int default_gallery_android_galleryItemBackground = 0;
        public static final int[] mediba_ad_sdk_android_MasAdView = {R.attr.backgroundColor, R.attr.requestInterval, R.attr.refreshAnimation, R.attr.visibility, R.attr.testMode};
        public static final int mediba_ad_sdk_android_MasAdView_backgroundColor = 0;
        public static final int mediba_ad_sdk_android_MasAdView_refreshAnimation = 2;
        public static final int mediba_ad_sdk_android_MasAdView_requestInterval = 1;
        public static final int mediba_ad_sdk_android_MasAdView_testMode = 4;
        public static final int mediba_ad_sdk_android_MasAdView_visibility = 3;
    }
}
