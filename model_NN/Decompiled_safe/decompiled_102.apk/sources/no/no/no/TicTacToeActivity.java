package no.no.no;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;

public class TicTacToeActivity extends Activity {
    public static final int REQUEST_CODE_PREFERENCES = 1;
    ImageView[] aImageViews = new ImageView[9];
    Logic aLogic;
    private AdView adView;
    Context baseContext;
    Drawable imCircle;
    Drawable imCross;
    Drawable imEmpty;
    Drawable[] imLines = new Drawable[8];
    public View.OnClickListener imageListener = new View.OnClickListener() {
        public void onClick(View v) {
            TicTacToeActivity.this.aLogic.setMove(Integer.parseInt((String) ((ImageView) v).getContentDescription()));
        }
    };
    private int mode;
    private SharedPreferences preferences;
    Resources res;
    public View.OnClickListener resetButtonListener = new View.OnClickListener() {
        public void onClick(View v) {
            TicTacToeActivity.this.reset();
        }
    };
    ImageView viewLine;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        this.baseContext = getBaseContext();
        this.res = getResources();
        this.imCross = this.res.getDrawable(R.drawable.cross);
        this.imCircle = this.res.getDrawable(R.drawable.circle);
        this.imEmpty = this.res.getDrawable(R.drawable.empty);
        this.aImageViews[0] = (ImageView) findViewById(R.id.imageView1);
        this.aImageViews[1] = (ImageView) findViewById(R.id.imageView2);
        this.aImageViews[2] = (ImageView) findViewById(R.id.imageView3);
        this.aImageViews[3] = (ImageView) findViewById(R.id.imageView4);
        this.aImageViews[4] = (ImageView) findViewById(R.id.imageView5);
        this.aImageViews[5] = (ImageView) findViewById(R.id.imageView6);
        this.aImageViews[6] = (ImageView) findViewById(R.id.imageView7);
        this.aImageViews[7] = (ImageView) findViewById(R.id.imageView8);
        this.aImageViews[8] = (ImageView) findViewById(R.id.imageView9);
        for (ImageView a : this.aImageViews) {
            a.setOnClickListener(this.imageListener);
        }
        this.imLines[0] = this.res.getDrawable(R.drawable.line0);
        this.imLines[1] = this.res.getDrawable(R.drawable.line1);
        this.imLines[2] = this.res.getDrawable(R.drawable.line2);
        this.imLines[3] = this.res.getDrawable(R.drawable.line3);
        this.imLines[4] = this.res.getDrawable(R.drawable.line4);
        this.imLines[5] = this.res.getDrawable(R.drawable.line5);
        this.imLines[6] = this.res.getDrawable(R.drawable.line6);
        this.imLines[7] = this.res.getDrawable(R.drawable.line7);
        this.viewLine = (ImageView) findViewById(R.id.imageView10);
        ((ImageButton) findViewById(R.id.reset)).setOnClickListener(this.resetButtonListener);
        this.preferences = PreferenceManager.getDefaultSharedPreferences(this);
        this.mode = Integer.parseInt(this.preferences.getString(getString(R.string.settings_mode_id), "1"));
        this.aLogic = new Logic(this, this.mode);
        this.adView = new AdView(this, AdSize.BANNER, "a14e4b71421cd06");
        ((LinearLayout) findViewById(R.id.linearLayout1)).addView(this.adView);
        this.adView.loadAd(new AdRequest());
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_settings:
                startActivityForResult(new Intent(this.baseContext, Preferences.class), 1);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            this.mode = Integer.parseInt(this.preferences.getString(getString(R.string.settings_mode_id), "1"));
            this.aLogic.setMode(this.mode);
        }
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    public void reset() {
        this.aLogic.reset();
        for (ImageView a : this.aImageViews) {
            a.setImageDrawable(this.imEmpty);
        }
        this.viewLine.setVisibility(8);
    }

    public void drawSign(int id, int sign) {
        if (sign == 1) {
            this.aImageViews[id].setImageDrawable(this.imCross);
        } else if (sign == 10) {
            this.aImageViews[id].setImageDrawable(this.imCircle);
        }
    }

    public void setVictory(int line, int VictorySign) {
        drawLine(line);
        showDialog(VictorySign);
    }

    public void setDraw() {
        showDialog(0);
    }

    private void drawLine(int line) {
        this.viewLine.setImageDrawable(this.imLines[line]);
        this.viewLine.setVisibility(0);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        int signVin = 0;
        if (id == 10) {
            signVin = R.string.dialog_losing;
        } else if (id == 1) {
            signVin = R.string.dialog_viner;
        } else if (id == 0) {
            signVin = R.string.dialog_draw;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(signVin);
        builder.setPositiveButton((int) R.string.dialog_button, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                TicTacToeActivity.this.reset();
            }
        });
        return builder.create();
    }

    public void onDestroy() {
        this.adView.destroy();
        super.onDestroy();
    }
}
