package no.no.no;

public final class R {

    public static final class array {
        public static final int mode_titl = 2131034112;
        public static final int mode_values = 2131034113;
    }

    public static final class attr {
    }

    public static final class drawable {
        public static final int area = 2130837504;
        public static final int bg = 2130837505;
        public static final int button_res = 2130837506;
        public static final int circle = 2130837507;
        public static final int cross = 2130837508;
        public static final int empty = 2130837509;
        public static final int icon = 2130837510;
        public static final int line0 = 2130837511;
        public static final int line1 = 2130837512;
        public static final int line2 = 2130837513;
        public static final int line3 = 2130837514;
        public static final int line4 = 2130837515;
        public static final int line5 = 2130837516;
        public static final int line6 = 2130837517;
        public static final int line7 = 2130837518;
    }

    public static final class id {
        public static final int imageArea = 2131230720;
        public static final int imageView1 = 2131230723;
        public static final int imageView10 = 2131230734;
        public static final int imageView2 = 2131230724;
        public static final int imageView3 = 2131230725;
        public static final int imageView4 = 2131230727;
        public static final int imageView5 = 2131230728;
        public static final int imageView6 = 2131230729;
        public static final int imageView7 = 2131230731;
        public static final int imageView8 = 2131230732;
        public static final int imageView9 = 2131230733;
        public static final int linearLayout1 = 2131230736;
        public static final int menu_settings = 2131230737;
        public static final int reset = 2131230735;
        public static final int tableLayout1 = 2131230721;
        public static final int tableRow1 = 2131230722;
        public static final int tableRow2 = 2131230726;
        public static final int tableRow3 = 2131230730;
    }

    public static final class layout {
        public static final int main = 2130903040;
    }

    public static final class menu {
        public static final int main_menu = 2131165184;
    }

    public static final class string {
        public static final int app_name = 2131099648;
        public static final int dialog_button = 2131099653;
        public static final int dialog_draw = 2131099652;
        public static final int dialog_losing = 2131099650;
        public static final int dialog_viner = 2131099651;
        public static final int menu_settings = 2131099654;
        public static final int reset = 2131099649;
        public static final int settings_mode = 2131099655;
        public static final int settings_mode_id = 2131099656;
    }

    public static final class xml {
        public static final int preference = 2130968576;
    }
}
