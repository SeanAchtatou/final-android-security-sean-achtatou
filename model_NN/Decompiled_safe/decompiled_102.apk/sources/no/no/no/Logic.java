package no.no.no;

import java.util.Random;

public class Logic {
    public static final int CIRCLE = 10;
    public static final int CROSS = 1;
    public static final int EMPTY = 0;
    public static final int MODE_EASY = 0;
    public static final int MODE_HARD = 1;
    private Pole aPole = new Pole();
    private int[] mass;
    private int mode;
    private TicTacToeActivity ticTacToeClass;

    public Logic(TicTacToeActivity l, int mode2) {
        this.ticTacToeClass = l;
        this.mode = mode2;
    }

    public void setMove(int id) {
        if (!this.aPole.getIsNoSign(id)) {
            this.ticTacToeClass.drawSign(id, 1);
            this.aPole.setSign(id, 1);
            doMove();
        }
    }

    private void doMove() {
        int randid1;
        int randid;
        this.mass = this.aPole.getMass();
        int EndLine = testEnd();
        if (EndLine != 9) {
            this.ticTacToeClass.setVictory(EndLine, this.aPole.getSignLine(EndLine));
        } else if (this.aPole.getSumSign() == 9) {
            this.ticTacToeClass.setDraw();
        } else {
            int line = testVictory(10);
            if (line != 9) {
                int id = this.aPole.getEmpty(line);
                this.ticTacToeClass.drawSign(id, 10);
                this.aPole.setSign(id, 10);
                doMove();
                return;
            }
            int line2 = testVictory(1);
            if (line2 != 9) {
                int id2 = this.aPole.getEmpty(line2);
                this.ticTacToeClass.drawSign(id2, 10);
                this.aPole.setSign(id2, 10);
                return;
            }
            switch (this.mode) {
                case 0:
                    Random r = new Random();
                    do {
                        randid = r.nextInt(9);
                    } while (this.aPole.getIsNoSign(randid));
                    this.ticTacToeClass.drawSign(randid, 10);
                    this.aPole.setSign(randid, 10);
                    return;
                case 1:
                    if (this.aPole.getSumSign() == 3 && this.aPole.getSign(4) != 1) {
                        boolean sign = false;
                        for (int i = 1; i < 8 && !sign; i += 2) {
                            if (!this.aPole.getIsNoSign(i)) {
                                this.ticTacToeClass.drawSign(i, 10);
                                this.aPole.setSign(i, 10);
                                sign = true;
                            }
                        }
                        return;
                    } else if (!this.aPole.getIsNoSign(4)) {
                        this.ticTacToeClass.drawSign(4, 10);
                        this.aPole.setSign(4, 10);
                        return;
                    } else if (!this.aPole.getIsNoSign(0)) {
                        this.ticTacToeClass.drawSign(0, 10);
                        this.aPole.setSign(0, 10);
                        return;
                    } else if (!this.aPole.getIsNoSign(2)) {
                        this.ticTacToeClass.drawSign(2, 10);
                        this.aPole.setSign(2, 10);
                        return;
                    } else if (!this.aPole.getIsNoSign(6)) {
                        this.ticTacToeClass.drawSign(6, 10);
                        this.aPole.setSign(6, 10);
                        return;
                    } else if (!this.aPole.getIsNoSign(8)) {
                        this.ticTacToeClass.drawSign(8, 10);
                        this.aPole.setSign(8, 10);
                        return;
                    } else {
                        Random r1 = new Random();
                        do {
                            randid1 = r1.nextInt(9);
                        } while (this.aPole.getIsNoSign(randid1));
                        this.ticTacToeClass.drawSign(randid1, 10);
                        this.aPole.setSign(randid1, 10);
                        return;
                    }
                default:
                    return;
            }
        }
    }

    private int testEnd() {
        int k = 9;
        for (int i = 0; i < 8; i++) {
            if (this.mass[i] == 3 || this.mass[i] == 30) {
                k = i;
            }
        }
        return k;
    }

    private int testVictory(int moveCELL) {
        int k = 9;
        for (int i = 0; i < 8; i++) {
            if (this.mass[i] == moveCELL * 2) {
                k = i;
            }
        }
        return k;
    }

    public void reset() {
        this.aPole.reset();
    }

    public void setMode(int parseInt) {
        if (parseInt != this.mode) {
            this.mode = parseInt;
            this.ticTacToeClass.reset();
        }
    }
}
