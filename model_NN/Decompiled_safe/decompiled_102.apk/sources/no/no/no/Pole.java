package no.no.no;

import java.lang.reflect.Array;

public class Pole {
    private int[][] cell = ((int[][]) Array.newInstance(Integer.TYPE, 3, 3));
    private int sumSign = 0;

    public Pole() {
        reset();
    }

    public boolean getIsNoSign(int id) {
        if (id < 3) {
            if (this.cell[0][id] != 0) {
                return true;
            }
            return false;
        } else if (id < 6) {
            if (this.cell[1][id - 3] != 0) {
                return true;
            }
            return false;
        } else if (id >= 9 || this.cell[2][id - 6] == 0) {
            return false;
        } else {
            return true;
        }
    }

    public int getSign(int id) {
        if (id < 3) {
            if (this.cell[0][id] != 0) {
                return this.cell[0][id];
            }
            return 0;
        } else if (id < 6) {
            if (this.cell[1][id - 3] != 0) {
                return this.cell[1][id - 3];
            }
            return 0;
        } else if (id >= 9 || this.cell[2][id - 6] == 0) {
            return 0;
        } else {
            return this.cell[2][id - 6];
        }
    }

    public void setSign(int id, int sign) {
        this.sumSign++;
        if (id < 3) {
            this.cell[0][id] = sign;
        } else if (id < 6) {
            this.cell[1][id - 3] = sign;
        } else if (id < 9) {
            this.cell[2][id - 6] = sign;
        }
    }

    public int[] getMass() {
        int[] mass = new int[8];
        for (int i : mass) {
        }
        for (int i2 = 0; i2 < 3; i2++) {
            for (int j = 0; j < 3; j++) {
                mass[i2] = mass[i2] + this.cell[i2][j];
                int i3 = j + 3;
                mass[i3] = mass[i3] + this.cell[i2][j];
            }
        }
        int i4 = 0;
        int j2 = 2;
        while (i4 < 3) {
            mass[6] = mass[6] + this.cell[i4][i4];
            mass[7] = mass[7] + this.cell[i4][j2];
            i4++;
            j2--;
        }
        return mass;
    }

    public int getEmpty(int line) {
        int k = 0;
        if (line < 3) {
            for (int j = 0; j < 3; j++) {
                if (this.cell[line][j] == 0) {
                    k = (line * 3) + j;
                }
            }
        } else if (line < 6) {
            for (int i = 0; i < 3; i++) {
                if (this.cell[i][line - 3] == 0) {
                    k = (line - 3) + (i * 3);
                }
            }
        } else if (line == 6) {
            for (int i2 = 0; i2 < 3; i2++) {
                if (this.cell[i2][i2] == 0) {
                    k = i2 * 4;
                }
            }
        } else if (line == 7) {
            int i3 = 0;
            int j2 = 2;
            while (i3 < 3) {
                if (this.cell[i3][j2] == 0) {
                    k = (i3 * 2) + 2;
                }
                i3++;
                j2--;
            }
        }
        return k;
    }

    public int getSignLine(int line) {
        if (line < 3) {
            return this.cell[line][0];
        }
        if (line < 6) {
            return this.cell[0][line - 3];
        }
        if (line == 6) {
            return this.cell[0][0];
        }
        return this.cell[2][2];
    }

    public int getSumSign() {
        return this.sumSign;
    }

    public void reset() {
        this.sumSign = 0;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                this.cell[i][j] = 0;
            }
        }
    }
}
