package com.machaon.quadratum;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Matrix4;
import com.machaon.quadratum.parts.Geom;
import com.machaon.quadratum.parts.Profile;
import com.machaon.quadratum.parts.Tournaments;
import java.lang.reflect.Array;
import java.util.Random;

public class States {
    public static final String BLACK_RECTANGLE = "br.png";
    public static final int BLIND = 5;
    public static final int BOMB = 4;
    public static final byte CREDITS = 14;
    public static final String FONT_1 = "font1";
    public static final String FONT_2 = "font2";
    public static final String FONT_3 = "font3";
    public static final String FONT_4 = "font4";
    public static final String FONT_GAME_STORE = "storefont";
    public static final int FREEZE = 1;
    public static final byte GAMELOOP = 9;
    public static final byte GAMETYPE = 4;
    public static final byte GAMETYPE_CLASSIC = 2;
    public static final byte GAMETYPE_WITH_BONUSES = 1;
    public static final byte HELP = 16;
    public static final String ICON_BACK = "back.png";
    public static final String ICON_MAIN_MENU = "menu.png";
    public static final String ICON_PROPERTIES = "prop.png";
    public static final byte LOGO = 2;
    public static final byte L_ENGLISH = 0;
    public static final byte L_RUSSIAN = 1;
    public static final byte MAINMENU = 3;
    public static final int MIXER = 2;
    public static final byte MULTIPLAYER_SETUP = 7;
    public static final byte MULTI_TOUR_SELECT = 8;
    public static final String[][] NAME_AI = {new String[]{"R. Solz", "R. Uggo", "R. Kai", "R. Dick", "R. Riz", "R. Atos", "R.Kobo", "R.Ferz", "R. Rita", "R. Hans", "R. Ada", "R. Axel", "R. Will", "R. Kurt"}, new String[]{"R. Vizar", "R. Armin", "R. Tiery", "R. Chase", "R. Oliss", "R. Olivo", "R. Frodo", "R. Dylan", "R. Felix", "R. Eagle", "R. Lando", "R. Oscar", "R. Stone"}, new String[]{"R. Francus", "R. Gregory", "R. Stan-lee", "R. da Vinci", "R. Dezmond", "R. Narciss", "R. Butterfly", "R. Sonata", "R. Zlatan", "R. Mathias", "R. Murdoc", "R. Tobias", "R. Adelaida", "R. Xavier", "R. Crispin", "R. Marius", "R. Baltasar", "R. Quentin"}};
    public static final byte NETWORK = 15;
    public static final byte NONE = 100;
    public static final int N_BONUSES = 6;
    public static final byte N_SHOWING_FINDED_BONUS = 8;
    public static final String PATH_GRAPH = "data/Graph/";
    public static final byte PAUSE = 98;
    public static final byte PREEXIT = 99;
    public static final byte PROFILE = 12;
    public static final byte ROUND_RESULTS = 10;
    public static final byte SETTINGS = 5;
    public static final String SHADOW_SCREEN = "shadowscr.png";
    public static final int SHOWALL = 0;
    public static final byte SINGLE_TOUR_SELECT = 6;
    public static final byte STATISTIC = 13;
    public static final int TELEPORT = 3;
    public static final String[] TEXT_ABANDONED = {"Games abandoned: ", "Незавершенные игры: "};
    public static final String[] TEXT_ACHIEVMENTS = {"Achievments", "Достижения"};
    public static final String[] TEXT_ANDREY = {"Andrey Safronov", "Андрей Сафронов"};
    public static final String[] TEXT_DIFFICULTY = {"Difficulty", "Сложность"};
    public static final String[] TEXT_FIRSTMOVE = {"first move", "первый ход"};
    public static final String[] TEXT_FOUND_BONUSES = {"Found bonuses: ", "Найдено бонусов: "};
    public static final String[] TEXT_GOLD_CUPS = {"Gold cups: ", "Количество золотых кубков: "};
    public static final String[] TEXT_HELP1 = {"1. Choose color", "1. Выбор цвета"};
    public static final String[] TEXT_HELP2 = {"2. Found bonus - dynamite", "2. Найден бонус - динамит"};
    public static final String[] TEXT_HELP3 = {"3. Tap name to open storehouse", "3. Кликните имя, чтобы открыть склад"};
    public static final String[] TEXT_LEVEL = {"Level", "Уровень"};
    public static final String[] TEXT_LIMIT = {"Max 2 players by Lite version", "Максимум 2 игрока в Lite версии"};
    public static final String[] TEXT_LITE = {"Will be available in full version", "Будет доступно в полной версии"};
    public static final String[] TEXT_LITE_ACHIEV = {"Achievments available in full version", "Достижения доступны в полной версии"};
    public static final String[] TEXT_MAX_SCORES_PER_R = {"Max scores per round on difficulty", "Максимум очков за раунд на сложности"};
    public static final String[] TEXT_MAX_SCORES_PER_T = {"Max scores per tournament on difficulty", "Максимум очков за турнир на сложности"};
    public static final String[] TEXT_MULTI_MODE = {"Multiplayer mode", "Многопользовательский режим"};
    public static final String[] TEXT_NEWNAME = {"- new name -", "- новое имя -"};
    public static final String[] TEXT_NEWPLAYER = {"- new player -", "- новый игрок -"};
    public static final String[] TEXT_PLACE = {" place", " место"};
    public static final String[] TEXT_PLAYED_ROUNDS = {"Rounds played: ", "Сыграно раундов: "};
    public static final String[] TEXT_PLAYED_TOURS = {"Tournaments played: ", "Сыграно турниров: "};
    public static final String[] TEXT_QUIT = {"Tap again to quit", "Нажмите кнопку еще раз для выхода в меню"};
    public static final String[] TEXT_RESULT = {"Results", "Итоги"};
    public static final String[] TEXT_SCORING = {"scoring...", "подсчет очков..."};
    public static final String[] TEXT_SERGEY = {"Sergey Safronov", "Сергей Сафронов"};
    public static final String[] TEXT_SINGLE_MODE = {"Single mode", "Одиночный режим"};
    public static final String[] TEXT_SKINS = {"Available in full version", "Доступны в полной версии"};
    public static final String[] TEXT_START = {"Tap to start", "Нажмите для старта"};
    public static final String[] TEXT_STATISTICS = {"Statistics", "Статистика"};
    public static final String[] TEXT_TOURNAMENT = {"Tournaments", "Турниры"};
    public static final String[] TEXT_TOURNAMENTS = {"Available in full version", "Доступны в полной версии"};
    public static final String[] TEXT_USED_BLIND = {"- blinding: ", "- ослепление: "};
    public static final String[] TEXT_USED_BONUSES = {"Used bonuses: ", "Использовано бонусов: "};
    public static final String[] TEXT_USED_EXPLOSIVES = {"- dynamite: ", "- динамит: "};
    public static final String[] TEXT_USED_EYE = {"- eye: ", "- глаз: "};
    public static final String[] TEXT_USED_FROST = {"- freezing: ", "- заморозка: "};
    public static final String[] TEXT_USED_MIXINGS = {"- Rubik's cube: ", "- кубик Рубика: "};
    public static final String[] TEXT_USED_TELEPORTS = {"- portal: ", "- портал: "};
    public static final String[] TEXT_WIN_PERCENTAGE = {"Win percentage: ", "Процент побед: "};
    public static final String[] TEXT_WON_PEOPLES = {"Peoples won: ", "Выиграно человек: "};
    public static final String[] TEXT_WON_ROUNDS = {"Rounds won: ", "Выиграно раундов: "};
    public static final String[] TEXT_WON_TOURS = {"Tournaments won: ", "Выиграно турниров: "};
    public static final String TEX_BOMB = "bon_bomb.png";
    public static final String TEX_BOMB_ANIM = "bon_bomb_anim.png";
    public static final String TEX_BOMB_BIG = "bon_bomb_big.png";
    public static final String TEX_BUTTON_MIDDLE_A = "but_2_a.png";
    public static final String TEX_BUTTON_MIDDLE_NA = "but_2_na.png";
    public static final String TEX_FLAGS = "flags.png";
    public static final String TEX_FREEZE = "bon_freeze.png";
    public static final String TEX_FREEZE_BIG = "bon_freeze_big.png";
    public static final String TEX_LOGO = "logo.png";
    public static final String TEX_MIXER = "bon_mixer.png";
    public static final String TEX_MIXER_BIG = "bon_mixer_big.png";
    public static final String TEX_SHOWALL = "bon_showall.png";
    public static final String TEX_SHOWALL_BIG = "bon_showall_big.png";
    public static final String TEX_TELEPORT = "bon_teleport.png";
    public static final String TEX_TELEPORT_BIG = "bon_teleport_big.png";
    public static float aspectX;
    public static float aspectY;
    public static int cameraOffsetX;
    public static int cameraOffsetY;
    public static Sound click;
    public static byte currentProfile = 0;
    public static byte difficulty;
    public static int displayHeight;
    public static int displayWidth;
    public static BitmapFont fontBig;
    public static BitmapFont fontSmall;
    private static Geom geomClock;
    public static boolean isAntialiasing;
    public static byte language;
    public static Music music;
    public static int numberOfColors = 7;
    public static byte numberOfPlayers;
    public static String path = "";
    public static String path_bigfont;
    public static String path_smallfont;
    public static byte[][] playerBonus;
    public static String[] playerName = {"", "", "", ""};
    public static int[] playerOverallScore = new int[4];
    public static int[] playerScore = new int[4];
    public static String resolution;
    public static int screenHeight;
    public static int screenWidth;
    public static int sizeFieldX = 35;
    public static int sizeFieldY = 23;
    public static int[] skillAI;
    public static String skin;
    public static byte state;
    public static Texture texBackground;
    public static Texture texClock;
    public static byte typeOfGame = 1;
    public static final Matrix4 viewMatrix = new Matrix4();
    public int bombRadius = 5;
    public int pauseAnimation = 2;
    public Profile profile;

    static {
        int[] iArr = new int[4];
        iArr[1] = 2;
        iArr[2] = 2;
        iArr[3] = 2;
        skillAI = iArr;
    }

    public States() {
        numberOfPlayers = 4;
        playerBonus = (byte[][]) Array.newInstance(Byte.TYPE, 4, 6);
        language = 0;
        this.profile = new Profile();
        difficulty = 2;
        click = Gdx.audio.newSound(Gdx.files.internal("data/Sounds/click.ogg"));
        language = Profile.profile.language;
        for (byte s = 0; s < numberOfPlayers; s = (byte) (s + 1)) {
            if (skillAI[s] != 0) {
                skillAI[s] = difficulty;
            }
        }
        namingAI();
        skin = "native";
        Init();
        music = Gdx.audio.newMusic(Gdx.files.internal("data/Sounds/mus.ogg"));
        music.setVolume(0.8f);
        music.setLooping(true);
    }

    public void UpdateCount() {
        for (byte i = 0; i < 4; i = (byte) (i + 1)) {
            int[] iArr = playerOverallScore;
            iArr[i] = iArr[i] + playerScore[i];
        }
        for (byte p = 0; p < 4; p = (byte) (p + 1)) {
            if (Profile.isActiveProfile[p]) {
                boolean isWin = true;
                for (byte i2 = 0; i2 < 4; i2 = (byte) (i2 + 1)) {
                    if (playerScore[p] < playerScore[i2]) {
                        isWin = false;
                    }
                }
                if (isWin) {
                    if (Tournaments.getType() == Tournaments.TYPE_SINGLE) {
                        Profile.pp[p].nWonRounds++;
                    } else {
                        Profile.pp[p].multi_nWonRounds++;
                    }
                }
            }
        }
        if (Tournaments.getType() == Tournaments.TYPE_SINGLE && playerScore[0] > Profile.pp[0].maxScoresOnRound[difficulty - 1]) {
            Profile.pp[0].maxScoresOnRound[difficulty - 1] = playerScore[0];
        }
        for (byte i3 = 0; i3 < 4; i3 = (byte) (i3 + 1)) {
            if (Profile.isActiveProfile[i3]) {
                if (Tournaments.getType() == Tournaments.TYPE_SINGLE) {
                    Profile.pp[i3].nPlayedRounds++;
                } else {
                    Profile.pp[i3].multi_nPlayedRounds++;
                }
                Profile.pushProfile(Profile.pp[i3].numberOfProfile, Profile.pp[i3]);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.files.FileHandle, boolean):void
     arg types: [com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.files.FileHandle, int]
     candidates:
      com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.graphics.g2d.TextureRegion, boolean):void
      com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.graphics.g2d.BitmapFont$BitmapFontData, com.badlogic.gdx.graphics.g2d.TextureRegion, boolean):void
      com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.files.FileHandle, boolean):void */
    public static void Init() {
        displayHeight = Gdx.graphics.getHeight();
        displayWidth = Gdx.graphics.getWidth();
        screenHeight = Gdx.graphics.getHeight();
        screenWidth = Gdx.graphics.getWidth();
        resetBonuses();
        for (byte i = 0; i < 4; i = (byte) (i + 1)) {
            playerOverallScore[i] = 0;
            playerScore[i] = 0;
        }
        if (screenHeight > 480) {
            screenHeight = 480;
            if (((float) (displayWidth / displayHeight)) > 1.66f) {
                screenWidth = 800;
            } else {
                screenWidth = 800;
            }
            cameraOffsetY = 0;
            cameraOffsetX = (displayWidth - ((screenWidth * displayHeight) / screenHeight)) / 2;
            if (cameraOffsetX < 0) {
                cameraOffsetX = 0;
                cameraOffsetY = (displayHeight - ((screenHeight * displayWidth) / screenWidth)) / 2;
            }
            aspectX = ((float) displayWidth) / ((float) (screenWidth + (cameraOffsetX * 2)));
            aspectY = ((float) displayHeight) / ((float) (screenHeight + (cameraOffsetY * 2)));
            resolution = "480";
            isAntialiasing = true;
        }
        if (screenHeight == 480) {
            if (displayHeight == 480) {
                cameraOffsetY = 0;
                isAntialiasing = false;
                aspectX = 1.0f;
                aspectY = 1.0f;
            }
            if (screenWidth != 800) {
                screenWidth = 854;
            }
            if (displayWidth == 800 || displayWidth == 854) {
                screenWidth = displayWidth;
            }
            resolution = "480";
            geomClock = new Geom(0, 0, 256, 256);
        }
        if (screenHeight == 240) {
            cameraOffsetY = 0;
            isAntialiasing = false;
            aspectX = 1.0f;
            aspectY = 1.0f;
            resolution = "240";
            geomClock = new Geom(0, 0, Input.Keys.META_SHIFT_RIGHT_ON, Input.Keys.META_SHIFT_RIGHT_ON);
        }
        if (screenHeight == 320) {
            cameraOffsetY = 0;
            isAntialiasing = false;
            aspectX = 1.0f;
            aspectY = 1.0f;
            if (displayWidth == 480) {
                cameraOffsetX = 0;
            }
            geomClock = new Geom(0, 0, Input.Keys.META_SHIFT_RIGHT_ON, Input.Keys.META_SHIFT_RIGHT_ON);
            resolution = "320";
        }
        path = PATH_GRAPH + resolution + "/";
        texClock = new Texture(Gdx.files.internal(String.valueOf(path) + "clock.png"));
        texBackground = new Texture(Gdx.files.internal("data/Graph/shadowscr.png"));
        if (isAntialiasing) {
            texBackground.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
            texClock.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        }
        if (screenHeight == 240) {
            path_smallfont = "data/Graph/font1";
            path_bigfont = "data/Graph/font2";
        }
        if (screenHeight == 320) {
            path_smallfont = "data/Graph/font2";
            path_bigfont = "data/Graph/font3";
        }
        if (screenHeight == 480) {
            path_smallfont = "data/Graph/font3";
            path_bigfont = "data/Graph/font4";
        }
        fontBig = new BitmapFont(Gdx.files.internal(String.valueOf(path_bigfont) + ".fnt"), Gdx.files.internal(String.valueOf(path_bigfont) + ".png"), false);
        fontSmall = new BitmapFont(Gdx.files.internal(String.valueOf(path_smallfont) + ".fnt"), Gdx.files.internal(String.valueOf(path_smallfont) + ".png"), false);
        viewMatrix.setToOrtho2D((float) (-cameraOffsetX), (float) (-cameraOffsetY), (float) (screenWidth + (cameraOffsetX * 2)), (float) (screenHeight + (cameraOffsetY * 2)));
    }

    public static void namingAI() {
        Random rand = new Random();
        String[] n = {"123", "234", "345", "567"};
        for (byte i = 0; i < numberOfPlayers; i = (byte) (i + 1)) {
            if (skillAI[i] != 0) {
                while (true) {
                    playerName[i] = NAME_AI[skillAI[i] - 1][rand.nextInt(NAME_AI[skillAI[i] - 1].length)];
                    n[i] = playerName[i];
                    if (n[0] != n[1] && n[0] != n[2] && n[0] != n[3] && n[1] != n[2] && n[1] != n[3] && n[2] != n[3]) {
                        break;
                    }
                }
            }
        }
    }

    public static void namingAI(byte player) {
        Random rand = new Random();
        String[] n = {"123", "234", "345", "567"};
        for (int i = 0; i < 4; i++) {
            n[i] = playerName[i];
        }
        while (true) {
            playerName[player] = NAME_AI[skillAI[player] - 1][rand.nextInt(NAME_AI[skillAI[player] - 1].length)];
            n[player] = playerName[player];
            if (n[0] != n[1] && n[0] != n[2] && n[0] != n[3] && n[1] != n[2] && n[1] != n[3] && n[2] != n[3]) {
                return;
            }
        }
    }

    public static void resetBonuses() {
        for (byte i = 0; i < 4; i = (byte) (i + 1)) {
            for (byte b = 0; b < 6; b = (byte) (b + 1)) {
                playerBonus[i][b] = 0;
            }
        }
    }

    public static void renderClock(SpriteBatch spriteBatch) {
        spriteBatch.draw(texClock, (float) ((screenWidth - geomClock.width) / 2), (float) ((screenHeight - geomClock.height) / 2), 0, 0, geomClock.width, geomClock.height);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.g2d.SpriteBatch.draw(com.badlogic.gdx.graphics.Texture, float, float, float, float, int, int, int, int, boolean, boolean):void
     arg types: [com.badlogic.gdx.graphics.Texture, float, float, float, float, int, int, int, int, int, int]
     candidates:
      com.badlogic.gdx.graphics.g2d.SpriteBatch.draw(com.badlogic.gdx.graphics.g2d.TextureRegion, float, float, float, float, float, float, float, float, float, boolean):void
      com.badlogic.gdx.graphics.g2d.SpriteBatch.draw(com.badlogic.gdx.graphics.Texture, float, float, float, float, int, int, int, int, boolean, boolean):void */
    public static void renderBackground(SpriteBatch spriteBatch) {
        if (isAntialiasing) {
            spriteBatch.draw(texBackground, aspectX * ((float) (-cameraOffsetX)), aspectY * ((float) (-cameraOffsetY)), (float) displayWidth, (float) displayHeight, 0, 0, texBackground.getWidth(), texBackground.getHeight(), false, false);
            return;
        }
        spriteBatch.draw(texBackground, ((float) (-cameraOffsetX)) * aspectX, 0.0f, 0, 0, displayWidth, screenHeight);
    }

    public static Texture reduceImageToTexture(String path2, int width, int height) {
        int w = 0;
        int h = 0;
        for (int i = 12; i > 0; i--) {
            if (((double) width) <= Math.pow(2.0d, (double) i)) {
                w = (int) Math.pow(2.0d, (double) i);
            }
            if (((double) height) <= Math.pow(2.0d, (double) i)) {
                h = (int) Math.pow(2.0d, (double) i);
            }
        }
        Pixmap pixSrc = new Pixmap(Gdx.files.internal(path2));
        Texture texture = new Texture(w, h, Pixmap.Format.RGBA8888);
        Pixmap pixReduce = new Pixmap(width, height, Pixmap.Format.RGBA8888);
        Pixmap.setFilter(Pixmap.Filter.BiLinear);
        pixReduce.drawPixmap(pixSrc, 0, 0, pixSrc.getWidth(), pixSrc.getHeight(), 0, 0, width, height);
        texture.draw(pixReduce, 0, 0);
        pixSrc.dispose();
        pixReduce.dispose();
        return texture;
    }
}
