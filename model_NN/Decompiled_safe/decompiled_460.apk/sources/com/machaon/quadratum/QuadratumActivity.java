package com.machaon.quadratum;

import android.os.Bundle;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.android.AndroidApplication;

public class QuadratumActivity extends AndroidApplication {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.backends.android.AndroidApplication.initialize(com.badlogic.gdx.ApplicationListener, boolean):void
     arg types: [com.machaon.quadratum.Quadratum, int]
     candidates:
      com.badlogic.gdx.backends.android.AndroidApplication.initialize(com.badlogic.gdx.ApplicationListener, com.badlogic.gdx.backends.android.AndroidApplicationConfiguration):void
      com.badlogic.gdx.backends.android.AndroidApplication.initialize(com.badlogic.gdx.ApplicationListener, boolean):void */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(0);
        initialize((ApplicationListener) new Quadratum(), false);
    }
}
