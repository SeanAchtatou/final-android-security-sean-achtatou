package com.machaon.quadratum;

import com.badlogic.gdx.backends.jogl.JoglApplication;

public class QudratumDesktop {
    public static void main(String[] argv) {
        new JoglApplication(new Quadratum(), "Quadratum", 480, 320, false);
    }
}
