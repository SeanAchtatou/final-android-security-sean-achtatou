package com.machaon.quadratum.input;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.machaon.quadratum.States;

public class SingleSetInput implements InputProcessor {
    public int x;
    public int y;

    public boolean keyDown(int keycode) {
        if (keycode != 4 && keycode != 67) {
            return false;
        }
        States.state = 3;
        return false;
    }

    public boolean keyTyped(char arg0) {
        return false;
    }

    public boolean keyUp(int arg0) {
        return false;
    }

    public boolean scrolled(int arg0) {
        return false;
    }

    public boolean touchDown(int x2, int y2, int arg2, int arg3) {
        Gdx.app.log("X и Y", String.valueOf(String.valueOf(x2)) + "  " + String.valueOf(y2));
        this.x = x2;
        this.y = y2;
        return false;
    }

    public boolean touchDragged(int arg0, int arg1, int arg2) {
        return false;
    }

    public boolean touchMoved(int arg0, int arg1) {
        return false;
    }

    public boolean touchUp(int arg0, int arg1, int arg2, int arg3) {
        return false;
    }
}
