package com.machaon.quadratum.input;

import com.badlogic.gdx.InputProcessor;
import com.machaon.quadratum.States;
import com.machaon.quadratum.parts.Keyboard;

public class SelectProfileInput implements InputProcessor {
    public final byte BUTTON_DOWN = 10;
    public final byte BUTTON_NONE = States.NONE;
    public final byte BUTTON_SOME = 50;
    public final byte BUTTON_YET_DOWN = Keyboard.STATE_NUMBER;
    public byte buttonDown = States.NONE;
    public byte buttonUp = States.NONE;
    public boolean isBack = false;
    public int x;
    public int y;

    public boolean keyDown(int keycode) {
        if (keycode != 4 && keycode != 67) {
            return false;
        }
        this.isBack = true;
        return false;
    }

    public boolean keyTyped(char arg0) {
        return false;
    }

    public boolean keyUp(int arg0) {
        return false;
    }

    public boolean scrolled(int arg0) {
        return false;
    }

    public boolean touchDown(int x2, int y2, int arg2, int arg3) {
        this.buttonUp = States.NONE;
        this.buttonDown = 50;
        this.x = x2;
        this.y = y2;
        return false;
    }

    public boolean touchDragged(int x2, int y2, int arg2) {
        this.x = x2;
        this.y = y2;
        return false;
    }

    public boolean touchMoved(int x2, int y2) {
        return false;
    }

    public boolean touchUp(int x2, int y2, int arg2, int arg3) {
        this.buttonUp = 50;
        this.buttonDown = States.NONE;
        this.x = x2;
        this.y = y2;
        return false;
    }
}
