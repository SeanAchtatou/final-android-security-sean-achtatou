package com.machaon.quadratum.input;

import com.badlogic.gdx.InputProcessor;
import com.machaon.quadratum.States;
import com.machaon.quadratum.parts.Keyboard;

public class SlideInput implements InputProcessor {
    public final byte BUTTON_DOWN = 10;
    public final byte BUTTON_NONE = States.NONE;
    public final byte BUTTON_SOME = 50;
    public final byte BUTTON_YET_DOWN = Keyboard.STATE_NUMBER;
    public byte buttonDown = States.NONE;
    public byte buttonUp = States.NONE;
    public float delta = 0.0f;
    public float deltaForSpeed = 0.0f;
    public boolean isBack = false;
    public float speed = 0.0f;
    public int x;
    public int y;
    private float y2 = 0.0f;

    public boolean keyDown(int keycode) {
        if (keycode != 4 && keycode != 67) {
            return false;
        }
        this.isBack = true;
        return false;
    }

    public boolean keyTyped(char arg0) {
        return false;
    }

    public boolean keyUp(int arg0) {
        return false;
    }

    public boolean scrolled(int arg0) {
        return false;
    }

    public boolean touchDown(int x2, int y3, int arg2, int arg3) {
        this.buttonUp = States.NONE;
        this.buttonDown = 50;
        this.x = x2;
        this.y = y3;
        this.speed = 0.0f;
        this.delta = 0.0f;
        this.y2 = (float) y3;
        return false;
    }

    public boolean touchDragged(int x2, int y3, int arg2) {
        this.x = x2;
        this.y = y3;
        if (((float) y3) < (20.0f * States.aspectY) + ((float) States.cameraOffsetY) || ((float) y3) > (((float) (States.screenHeight - 20)) * States.aspectY) + ((float) States.cameraOffsetY)) {
            return false;
        }
        this.delta += ((float) y3) - this.y2;
        this.deltaForSpeed = ((float) y3) - this.y2;
        this.y2 = (float) y3;
        return false;
    }

    public boolean touchMoved(int arg0, int arg1) {
        return false;
    }

    public boolean touchUp(int x2, int y3, int arg2, int arg3) {
        this.buttonUp = 50;
        this.buttonDown = States.NONE;
        this.x = x2;
        this.y = y3;
        this.speed = this.deltaForSpeed;
        return false;
    }
}
