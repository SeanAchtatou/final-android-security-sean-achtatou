package com.machaon.quadratum.screens;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.machaon.quadratum.IScreen;
import com.machaon.quadratum.States;

public class Logo implements IScreen {
    private int logoHeight;
    private int logoWidth;
    private final SpriteBatch spriteBatch;
    private final Texture texLogo;
    private float timer;

    public Logo() {
        this.logoWidth = 0;
        this.logoHeight = 0;
        this.spriteBatch = new SpriteBatch();
        this.logoWidth = (int) (((float) States.displayWidth) / 1.2f);
        this.logoHeight = (this.logoWidth * 300) / 670;
        this.texLogo = States.reduceImageToTexture("data/Graph/logo.png", this.logoWidth, this.logoHeight);
        setTimer(4.0f);
    }

    public void update() {
        if (isTimerEnd()) {
            States.state = 3;
        }
    }

    public void render(Application app) {
        app.getGraphics().getGL10().glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
        app.getGraphics().getGL10().glClear(16384);
        this.spriteBatch.begin();
        this.spriteBatch.enableBlending();
        this.spriteBatch.draw(this.texLogo, (float) ((States.displayWidth - this.logoWidth) / 2), (float) ((States.displayHeight - this.logoHeight) / 2), 0, 0, this.logoWidth, this.logoHeight);
        this.spriteBatch.end();
    }

    public void dispose() {
        this.spriteBatch.dispose();
        this.texLogo.dispose();
    }

    public void resume() {
    }

    private void setTimer(float timeInSec) {
        this.timer = timeInSec;
    }

    private boolean isTimerEnd() {
        if (this.timer < 0.0f) {
            return true;
        }
        this.timer -= Gdx.graphics.getDeltaTime();
        return false;
    }
}
