package com.machaon.quadratum.screens;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.machaon.quadratum.IScreen;
import com.machaon.quadratum.States;
import com.machaon.quadratum.input.MainMenuInput;
import com.machaon.quadratum.parts.ButtonPic;
import com.machaon.quadratum.parts.Geom;
import com.machaon.quadratum.parts.Profile;

public class MainMenu implements IScreen {
    private ButtonPic[] buttonMain = new ButtonPic[3];
    private ButtonPic buttonName;
    private Geom geomFlag;
    private Geom geomQ;
    private Geom geomQuestion;
    private MainMenuInput inp = new MainMenuInput();
    private int offsetBackX;
    private final SpriteBatch spriteBatch = new SpriteBatch();
    private Texture texBack = new Texture(Gdx.files.internal(String.valueOf(States.path) + "bg.jpg"));
    private Texture texButtonAct = new Texture(Gdx.files.internal(String.valueOf(States.path) + "but_a.png"));
    private Texture texButtonNameAct = new Texture(Gdx.files.internal(String.valueOf(States.path) + "but_name_a.png"));
    private Texture texButtonNameNonAct = new Texture(Gdx.files.internal(String.valueOf(States.path) + "but_name_na.png"));
    private Texture texButtonNonAct = new Texture(Gdx.files.internal(String.valueOf(States.path) + "but_na.png"));
    private final Texture texFlags;
    private final Texture texQuest;
    private float timer;

    public MainMenu(States states) {
        if (Profile.isSD) {
            Profile.SaveProfiles();
        }
        Profile.setCurrentProfile(Profile.profile.numberOfProfile);
        this.offsetBackX = 0;
        if (States.screenHeight == 240 && States.screenWidth == 320) {
            this.geomFlag = new Geom(285, 9, 29, 30);
            this.buttonMain[0] = new ButtonPic(24, 47, 84, 84, 7, -7);
            this.buttonMain[1] = new ButtonPic(118, 47, 84, 84, 5, -7);
            this.buttonMain[2] = new ButtonPic(212, 47, 84, 84, 7, -8);
            this.buttonName = new ButtonPic(37, 10, (int) Input.Keys.F2, 30, 0, 23);
            this.geomQ = new Geom(16, 139, 360, 90);
            this.geomQuestion = new Geom(12, 9, 16, 30);
            this.offsetBackX = 40;
        }
        if (States.screenHeight == 240 && States.screenWidth == 400) {
            this.geomFlag = new Geom(354, 10, 29, 30);
            this.buttonMain[0] = new ButtonPic(64, 47, 84, 84, 7, -7);
            this.buttonMain[1] = new ButtonPic(158, 47, 84, 84, 5, -7);
            this.buttonMain[2] = new ButtonPic((int) Input.Keys.F9, 47, 84, 84, 7, -8);
            this.buttonName = new ButtonPic(77, 10, (int) Input.Keys.F2, 30, 0, 23);
            this.geomQ = new Geom(16, 139, 360, 90);
            this.geomQuestion = new Geom(20, 10, 16, 30);
        }
        if (States.screenHeight == 320) {
            this.geomFlag = new Geom(425, 15, 39, 39);
            this.buttonMain[0] = new ButtonPic(64, 70, (int) Input.Keys.BUTTON_START, (int) Input.Keys.BUTTON_START, 7, -7);
            this.buttonMain[1] = new ButtonPic(185, 70, (int) Input.Keys.BUTTON_START, (int) Input.Keys.BUTTON_START, 6, -8);
            this.buttonMain[2] = new ButtonPic(307, 70, (int) Input.Keys.BUTTON_START, (int) Input.Keys.BUTTON_START, 7, -8);
            this.buttonName = new ButtonPic(77, 15, 326, 40, -2, 30);
            this.geomQ = new Geom(11, 210, 460, 100);
            this.geomQuestion = new Geom(29, 14, 32, 40);
        }
        if (States.screenHeight == 480) {
            if (States.screenWidth == 800) {
                this.geomFlag = new Geom(708, 23, 56, 59);
                this.buttonMain[0] = new ButtonPic(139, 100, 162, 162, 11, -11);
                this.buttonMain[1] = new ButtonPic(319, 100, 162, 162, 8, -11);
                this.buttonMain[2] = new ButtonPic(499, 100, 162, 162, 10, -12);
                this.buttonName = new ButtonPic(155, 22, 490, 58, -2, 39);
                this.geomQ = new Geom(34, 280, 730, 176);
                this.geomQuestion = new Geom(34, 28, 32, 50);
            } else {
                this.geomFlag = new Geom(756, 23, 56, 59);
                this.buttonMain[0] = new ButtonPic(166, 100, 162, 162, 11, -11);
                this.buttonMain[1] = new ButtonPic(346, 100, 162, 162, 8, -11);
                this.buttonMain[2] = new ButtonPic(526, 100, 162, 162, 10, -12);
                this.buttonName = new ButtonPic(182, 22, 490, 58, -2, 39);
                this.geomQ = new Geom(60, 280, 730, 176);
                this.geomQuestion = new Geom(37, 28, 32, 50);
            }
            if (States.screenWidth == 800) {
                this.offsetBackX = 27;
            }
        }
        this.buttonMain[0].SetParams(this.texButtonNonAct, this.texButtonAct, new Texture(Gdx.files.internal(String.valueOf(States.path) + "play.png")), (byte) 4);
        this.buttonMain[1].SetParams(this.texButtonNonAct, this.texButtonAct, new Texture(Gdx.files.internal(String.valueOf(States.path) + "settings.png")), (byte) 5);
        this.buttonMain[2].SetParams(this.texButtonNonAct, this.texButtonAct, new Texture(Gdx.files.internal(String.valueOf(States.path) + "achiev.png")), States.STATISTIC);
        this.buttonName.SetParams(this.texButtonNameNonAct, this.texButtonNameAct, (byte) 12);
        this.texFlags = new Texture(Gdx.files.internal(String.valueOf(States.path) + States.TEX_FLAGS));
        this.texQuest = new Texture(Gdx.files.internal(String.valueOf(States.path) + "question.png"));
        setTimer(0.5f);
        this.spriteBatch.setProjectionMatrix(States.viewMatrix);
        if (States.isAntialiasing) {
            for (ButtonPic b : this.buttonMain) {
                b.texBackAct.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
                b.texBackNonAct.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
                b.texFront.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
            }
            this.buttonName.texBackAct.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
            this.buttonName.texBackNonAct.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
            this.texFlags.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
            this.texBack.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
            this.texQuest.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        }
        Gdx.app.getInput().setInputProcessor(this.inp);
        if (Profile.profile.name == "") {
            Profile.profile.settingSounds = true;
            Profile.profile.settingMusic = true;
            States.music.play();
        }
        if (!Profile.profile.settingMusic) {
            States.music.stop();
        } else {
            States.music.play();
        }
    }

    public void update() {
        if (isTimerEnd()) {
            updateMenu();
            return;
        }
        MainMenuInput mainMenuInput = this.inp;
        this.inp.getClass();
        mainMenuInput.buttonUp = States.NONE;
    }

    private void updateMenu() {
        byte b = this.inp.buttonUp;
        this.inp.getClass();
        if (b == 50) {
            if (isInputCoordsIn(this.geomFlag)) {
                if (Profile.profile.settingSounds) {
                    States.click.play();
                }
                byte b2 = (byte) (States.language + 1);
                States.language = b2;
                if (b2 > 1) {
                    States.language = 0;
                }
                Profile.profile.language = States.language;
                MainMenuInput mainMenuInput = this.inp;
                this.inp.getClass();
                mainMenuInput.buttonUp = States.NONE;
            }
            for (ButtonPic b3 : this.buttonMain) {
                if (isInputCoordsIn(b3)) {
                    if (Profile.profile.settingSounds) {
                        States.click.play();
                    }
                    if (Profile.profile.name == "") {
                        States.state = 12;
                    } else {
                        States.state = b3.getState();
                    }
                }
            }
            if (isInputCoordsIn(this.buttonName)) {
                if (Profile.profile.settingSounds) {
                    States.click.play();
                }
                States.state = this.buttonName.getState();
            }
        }
        byte b4 = this.inp.buttonDown;
        this.inp.getClass();
        if (b4 == 50) {
            for (ButtonPic b5 : this.buttonMain) {
                b5.setBackActive(isInputCoordsIn(b5));
            }
            this.buttonName.setBackActive(isInputCoordsIn(this.buttonName));
            if (isInputCoordsIn(this.geomQ)) {
                States.state = States.CREDITS;
            }
            if (isInputCoordsIn(this.geomQuestion)) {
                States.state = States.HELP;
                if (Profile.profile.settingSounds) {
                    States.click.play();
                }
            }
        }
    }

    public void render(Application app) {
        app.getGraphics().getGL10().glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        app.getGraphics().getGL10().glClear(16384);
        this.spriteBatch.begin();
        this.spriteBatch.enableBlending();
        this.spriteBatch.draw(this.texBack, 0.0f, 0.0f, this.offsetBackX, 0, States.screenWidth, States.screenHeight);
        States.fontBig.setColor(1.0f, 1.0f, 1.0f, 1.0f);
        for (ButtonPic b : this.buttonMain) {
            this.spriteBatch.draw(b.getBackTexture(), (float) b.x, (float) b.y, 0, 0, b.width, b.height);
            this.spriteBatch.draw(b.texFront, (float) (b.x + b.frontX), (float) (b.y + b.frontY), 0, 0, b.width, b.height);
        }
        this.spriteBatch.draw(this.texFlags, (float) this.geomFlag.x, (float) this.geomFlag.y, this.geomFlag.width * States.language, 0, this.geomFlag.width, this.geomFlag.height);
        this.spriteBatch.draw(this.texQuest, (float) this.geomQuestion.x, (float) this.geomQuestion.y, 0, 0, this.geomQuestion.width, this.geomQuestion.height);
        this.spriteBatch.draw(this.buttonName.getBackTexture(), (float) this.buttonName.x, (float) this.buttonName.y, 0, 0, this.buttonName.width, this.buttonName.height);
        if (Profile.profile.name == "") {
            States.fontBig.drawMultiLine(this.spriteBatch, States.TEXT_NEWPLAYER[States.language], (float) this.buttonName.x, (float) (this.buttonName.y + this.buttonName.frontY), (float) this.buttonName.width, BitmapFont.HAlignment.CENTER);
        } else {
            States.fontBig.drawMultiLine(this.spriteBatch, Profile.profile.name, (float) this.buttonName.x, (float) (this.buttonName.y + this.buttonName.frontY), (float) this.buttonName.width, BitmapFont.HAlignment.CENTER);
        }
        this.spriteBatch.end();
    }

    public void dispose() {
        this.spriteBatch.dispose();
        this.texBack.dispose();
        this.texButtonNonAct.dispose();
        this.texButtonAct.dispose();
        this.texButtonNameNonAct.dispose();
        this.texButtonNameAct.dispose();
        this.texFlags.dispose();
        this.texQuest.dispose();
        for (ButtonPic b : this.buttonMain) {
            b.dispose();
        }
        try {
            finalize();
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public void resume() {
    }

    private boolean isInputCoordsIn(Geom geom) {
        if (((float) this.inp.x) <= ((float) (geom.x + States.cameraOffsetX)) * States.aspectX || ((float) this.inp.x) >= ((float) (geom.x + geom.width + States.cameraOffsetX)) * States.aspectX || ((float) (States.displayHeight - this.inp.y)) <= ((float) (geom.y + States.cameraOffsetY)) * States.aspectY || ((float) (States.displayHeight - this.inp.y)) >= ((float) (geom.y + geom.height + States.cameraOffsetY)) * States.aspectY) {
            return false;
        }
        return true;
    }

    private void setTimer(float timeInSec) {
        this.timer = timeInSec;
    }

    private boolean isTimerEnd() {
        if (this.timer < 0.0f) {
            return true;
        }
        this.timer -= Gdx.graphics.getDeltaTime();
        return false;
    }
}
