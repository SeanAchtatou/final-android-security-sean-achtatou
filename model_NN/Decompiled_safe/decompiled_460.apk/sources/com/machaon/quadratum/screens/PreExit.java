package com.machaon.quadratum.screens;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.machaon.quadratum.IScreen;
import com.machaon.quadratum.States;
import com.machaon.quadratum.input.CommonInput;
import com.machaon.quadratum.parts.ButtonPic;
import com.machaon.quadratum.parts.Geom;
import com.machaon.quadratum.parts.Keyboard;
import com.machaon.quadratum.parts.Profile;

public class PreExit implements IScreen {
    private ButtonPic buttonGetFullVersion;
    private ButtonPic buttonRate;
    private ButtonPic buttonRecommend;
    private CommonInput inp;
    private int line = 0;
    private byte offsetX;
    private final SpriteBatch spriteBatch = new SpriteBatch();
    private Texture texIcon = new Texture(Gdx.files.internal(String.valueOf(States.path) + "icon.png"));
    private Texture texLogo = new Texture(Gdx.files.internal(String.valueOf(States.path) + "minilogo.png"));

    public PreExit() {
        if (States.screenHeight == 240 && States.screenWidth == 320) {
            this.offsetX = States.NETWORK;
            this.buttonRate = new ButtonPic(15, 5, 72, 32, 1, 0);
            this.buttonRecommend = new ButtonPic(93, 5, 72, 32, 0, 0);
            this.buttonGetFullVersion = new ButtonPic(171, 5, 72, 32, 0, 0);
        }
        if (States.screenHeight == 240 && States.screenWidth == 400) {
            this.offsetX = Keyboard.STATE_NUMBER;
            this.buttonRate = new ButtonPic(20, 5, 72, 32, 1, 0);
            this.buttonRecommend = new ButtonPic(98, 5, 72, 32, 0, 0);
            this.buttonGetFullVersion = new ButtonPic(176, 5, 72, 32, 0, 0);
        }
        if (States.screenHeight == 320) {
            this.offsetX = 30;
            this.buttonRate = new ButtonPic(30, 5, 96, 48, 0, -1);
            this.buttonRecommend = new ButtonPic((int) Input.Keys.END, 5, 96, 48, 0, -1);
            this.buttonGetFullVersion = new ButtonPic(234, 5, 96, 48, 0, -1);
        }
        if (States.screenHeight == 480) {
            this.offsetX = 45;
            this.buttonRate = new ButtonPic(45, 10, 144, 72, 0, 0);
            this.buttonRecommend = new ButtonPic(200, 10, 144, 72, 0, 0);
            this.buttonGetFullVersion = new ButtonPic(355, 10, 144, 72, 0, 0);
        }
        String sRec = "recommend_e.png";
        String sRate = "rate_e.png";
        String sBuy = "buy_e.png";
        if (States.language == 1) {
            sRec = "recommend.png";
            sRate = "rate.png";
            sBuy = "buy.png";
        }
        this.buttonRate.SetParams(new Texture(Gdx.files.internal(String.valueOf(States.path) + "but_rate_a.png")), new Texture(Gdx.files.internal(String.valueOf(States.path) + "but_rate_na.png")), new Texture(Gdx.files.internal(String.valueOf(States.path) + sRate)), (byte) 0);
        this.buttonRecommend.SetParams(new Texture(Gdx.files.internal(String.valueOf(States.path) + "but_rate_a.png")), new Texture(Gdx.files.internal(String.valueOf(States.path) + "but_rate_na.png")), new Texture(Gdx.files.internal(String.valueOf(States.path) + sRec)), (byte) 0);
        this.buttonGetFullVersion.SetParams(new Texture(Gdx.files.internal(String.valueOf(States.path) + "but_rate_a.png")), new Texture(Gdx.files.internal(String.valueOf(States.path) + "but_rate_na.png")), new Texture(Gdx.files.internal(String.valueOf(States.path) + sBuy)), (byte) 0);
        this.inp = new CommonInput();
        Gdx.app.getInput().setInputProcessor(this.inp);
        if (States.isAntialiasing) {
            this.buttonRate.texBackAct.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
            this.buttonRate.texBackNonAct.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
            this.buttonRate.texFront.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
            this.buttonRecommend.texBackAct.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
            this.buttonRecommend.texBackNonAct.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
            this.buttonRecommend.texFront.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
            this.texIcon.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
            this.texLogo.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        }
        this.spriteBatch.setProjectionMatrix(States.viewMatrix);
    }

    public void update() {
        byte b = this.inp.buttonUp;
        this.inp.getClass();
        if (b == 50) {
            if (isInputCoordsIn(this.buttonRate)) {
                if (Profile.profile.settingSounds) {
                    States.click.play();
                }
                this.buttonRate.getState();
                Gdx.app.gotoMarket((byte) 1);
            }
            if (isInputCoordsIn(this.buttonGetFullVersion)) {
                if (Profile.profile.settingSounds) {
                    States.click.play();
                }
                this.buttonGetFullVersion.getState();
                Gdx.app.gotoMarket((byte) 2);
            }
            if (isInputCoordsIn(this.buttonRecommend)) {
                if (Profile.profile.settingSounds) {
                    States.click.play();
                }
                this.buttonRecommend.getState();
                if (States.language == 0) {
                    Gdx.app.recommendEng();
                } else {
                    Gdx.app.recommendRus();
                }
            }
            if (((float) this.inp.x) > ((float) (States.screenWidth - this.texLogo.getWidth())) * States.aspectX && ((float) (States.displayHeight - this.inp.y)) < ((float) this.texLogo.getHeight()) * States.aspectY && States.screenWidth != 320) {
                Gdx.app.gotoHttp();
            }
            this.inp.x = 0;
        }
        byte b2 = this.inp.buttonDown;
        this.inp.getClass();
        if (b2 == 50) {
            this.buttonRate.setBackActive(isInputCoordsIn(this.buttonRate));
            this.buttonRecommend.setBackActive(isInputCoordsIn(this.buttonRecommend));
            this.buttonGetFullVersion.setBackActive(isInputCoordsIn(this.buttonGetFullVersion));
        }
    }

    public void render(Application app) {
        app.getGraphics().getGL10().glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
        app.getGraphics().getGL10().glClear(16384);
        this.spriteBatch.begin();
        this.spriteBatch.enableBlending();
        this.line = (int) (States.fontSmall.getCapHeight() * 1.1f);
        this.spriteBatch.draw(this.texIcon, (float) this.offsetX, (float) (getHeight() - this.texIcon.getHeight()), 0, 0, this.texIcon.getWidth(), this.texIcon.getHeight());
        if (States.language == 1) {
            States.fontBig.setColor(0.0f, 0.0f, 0.0f, 1.0f);
            States.fontBig.drawMultiLine(this.spriteBatch, "Полная версия 'Quadratum'", (float) ((this.offsetX * 2) + this.texIcon.getWidth()), (float) getHeight(), (float) States.screenWidth, BitmapFont.HAlignment.LEFT);
            incLine(1.3f);
            States.fontBig.drawMultiLine(this.spriteBatch, "уже в Android Market!", (float) ((this.offsetX * 2) + this.texIcon.getWidth()), (float) getHeight(), (float) States.screenWidth, BitmapFont.HAlignment.LEFT);
            States.fontSmall.setColor(0.2f, 0.2f, 0.2f, 1.0f);
            writeText("В полной версии Вас ждут:", 2.5f);
            writeText("- режим игры без бонусов (филлер)", 1.0f);
            writeText("- система достижений, расширенная статистика", 1.0f);
            writeText("- различные цветовые схемы", 1.0f);
            writeText("- возможность записи/загрузки игры", 1.0f);
            writeText("- возможность одновременной игры до 4 человек", 1.0f);
            writeText("В планах на ближайшее будущее:", 1.0f);
            writeText("- сетевая игра (примерно в сентябре)", 1.0f);
        } else {
            States.fontBig.setColor(0.0f, 0.0f, 0.0f, 1.0f);
            States.fontBig.drawMultiLine(this.spriteBatch, "The full version of 'Quadratum'", (float) ((this.offsetX * 2) + this.texIcon.getWidth()), (float) getHeight(), (float) States.screenWidth, BitmapFont.HAlignment.LEFT);
            incLine(1.3f);
            States.fontBig.drawMultiLine(this.spriteBatch, "is already in Android Market!", (float) ((this.offsetX * 2) + this.texIcon.getWidth()), (float) getHeight(), (float) States.screenWidth, BitmapFont.HAlignment.LEFT);
            States.fontSmall.setColor(0.2f, 0.2f, 0.2f, 1.0f);
            writeText("In the full version you can find:", 2.5f);
            writeText("- mode without bonuses (filler)", 1.0f);
            writeText("- achievements, extended statistics", 1.0f);
            writeText("- various skins", 1.0f);
            writeText("- possibility to save/load the game", 1.0f);
            writeText("- possibility of synchronous game up to 4 people", 1.0f);
            writeText("In the near future we plan to create:", 1.0f);
            writeText("- network game (around September)", 1.0f);
        }
        this.spriteBatch.draw(this.buttonRate.getBackTexture(), (float) this.buttonRate.x, (float) this.buttonRate.y, 0, 0, this.buttonRate.width, this.buttonRate.height);
        this.spriteBatch.draw(this.buttonRate.texFront, (float) (this.buttonRate.x + this.buttonRate.frontX), (float) (this.buttonRate.y + this.buttonRate.frontY), 0, 0, this.buttonRate.width, this.buttonRate.height);
        this.spriteBatch.draw(this.buttonRecommend.getBackTexture(), (float) this.buttonRecommend.x, (float) this.buttonRecommend.y, 0, 0, this.buttonRecommend.width, this.buttonRecommend.height);
        this.spriteBatch.draw(this.buttonRecommend.texFront, (float) (this.buttonRecommend.x + this.buttonRecommend.frontX), (float) (this.buttonRecommend.y + this.buttonRecommend.frontY), 0, 0, this.buttonRecommend.width, this.buttonRecommend.height);
        this.spriteBatch.draw(this.buttonGetFullVersion.getBackTexture(), (float) this.buttonGetFullVersion.x, (float) this.buttonGetFullVersion.y, 0, 0, this.buttonGetFullVersion.width, this.buttonGetFullVersion.height);
        this.spriteBatch.draw(this.buttonGetFullVersion.texFront, (float) (this.buttonGetFullVersion.x + this.buttonGetFullVersion.frontX), (float) (this.buttonGetFullVersion.y + this.buttonGetFullVersion.frontY), 0, 0, this.buttonGetFullVersion.width, this.buttonGetFullVersion.height);
        if (States.screenWidth != 320) {
            this.spriteBatch.draw(this.texLogo, (float) (States.screenWidth - this.texLogo.getWidth()), 0.0f, 0, 0, this.texLogo.getWidth(), this.texLogo.getHeight());
        }
        this.spriteBatch.end();
    }

    public void dispose() {
        Profile.SaveProfiles();
        this.spriteBatch.dispose();
        this.texIcon.dispose();
        this.texLogo.dispose();
        this.buttonRate.dispose();
        this.buttonRecommend.dispose();
        this.buttonGetFullVersion.dispose();
    }

    public void resume() {
    }

    private void writeText(String text, float height) {
        incLine(height);
        States.fontSmall.draw(this.spriteBatch, text, (float) this.offsetX, (float) getHeight());
    }

    private int getHeight() {
        return States.screenHeight - this.line;
    }

    private void incLine(float n) {
        this.line += (int) (States.fontSmall.getCapHeight() * 1.5f * n);
    }

    private boolean isInputCoordsIn(Geom geom) {
        if (((float) this.inp.x) <= ((float) (geom.x + States.cameraOffsetX)) * States.aspectX || ((float) this.inp.x) >= ((float) (geom.x + geom.width + States.cameraOffsetX)) * States.aspectX || ((float) (States.displayHeight - this.inp.y)) <= ((float) (geom.y + States.cameraOffsetY)) * States.aspectY || ((float) (States.displayHeight - this.inp.y)) >= ((float) (geom.y + geom.height + States.cameraOffsetY)) * States.aspectY) {
            return false;
        }
        return true;
    }
}
