package com.machaon.quadratum.screens;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.machaon.quadratum.IScreen;
import com.machaon.quadratum.States;
import com.machaon.quadratum.input.CommonInput;
import com.machaon.quadratum.parts.Geom;
import com.machaon.quadratum.parts.Profile;

public class Network implements IScreen {
    private BitmapFont font;
    private Geom geomBackPic;
    private Geom geomBoth;
    private Geom geomServer;
    private CommonInput inp;
    private boolean isActiveEmail = false;
    private boolean isActiveHttp = false;
    private int logoHeight = 0;
    private int logoWidth = 0;
    private final SpriteBatch spriteBatch = new SpriteBatch();
    private final Texture texBackPic;
    private final Texture texBackground;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.files.FileHandle, boolean):void
     arg types: [com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.files.FileHandle, int]
     candidates:
      com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.graphics.g2d.TextureRegion, boolean):void
      com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.graphics.g2d.BitmapFont$BitmapFontData, com.badlogic.gdx.graphics.g2d.TextureRegion, boolean):void
      com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.files.FileHandle, boolean):void */
    public Network() {
        String str = States.PATH_GRAPH + States.resolution + "/";
        this.texBackground = new Texture(Gdx.files.internal("data/Graph/shadowscr.png"));
        this.texBackPic = new Texture(Gdx.files.internal("data/Graph/q.png"));
        this.font = new BitmapFont(Gdx.files.internal(String.valueOf(States.path_smallfont) + ".fnt"), Gdx.files.internal(String.valueOf(States.path_smallfont) + ".png"), false);
        this.geomBackPic = new Geom(1, 1, Input.Keys.F11, Input.Keys.F11);
        if (States.screenHeight == 320) {
            this.geomServer = new Geom(Input.Keys.BUTTON_MODE, 62, Input.Keys.F7, 20);
            this.geomBoth = new Geom(Input.Keys.BUTTON_MODE, 32, Input.Keys.F7, 20);
        }
        this.inp = new CommonInput();
        Gdx.app.getInput().setInputProcessor(this.inp);
        this.spriteBatch.setProjectionMatrix(States.viewMatrix);
        this.texBackPic.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
    }

    public void update() {
        if (this.inp.isBack) {
            if (Profile.profile.settingSounds) {
                States.click.play();
            }
            States.state = 7;
        }
        byte b = this.inp.buttonUp;
        this.inp.getClass();
        if (b == 50) {
            this.isActiveHttp = false;
            this.isActiveEmail = false;
            isInputCoordsIn(this.geomServer);
            isInputCoordsIn(this.geomBoth);
            this.inp.x = 0;
        }
        byte b2 = this.inp.buttonDown;
        this.inp.getClass();
        if (b2 == 50) {
            this.isActiveHttp = isInputCoordsIn(this.geomServer);
            this.isActiveEmail = isInputCoordsIn(this.geomBoth);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.g2d.SpriteBatch.draw(com.badlogic.gdx.graphics.Texture, float, float, float, float, int, int, int, int, boolean, boolean):void
     arg types: [com.badlogic.gdx.graphics.Texture, float, int, float, float, int, int, int, int, int, int]
     candidates:
      com.badlogic.gdx.graphics.g2d.SpriteBatch.draw(com.badlogic.gdx.graphics.g2d.TextureRegion, float, float, float, float, float, float, float, float, float, boolean):void
      com.badlogic.gdx.graphics.g2d.SpriteBatch.draw(com.badlogic.gdx.graphics.Texture, float, float, float, float, int, int, int, int, boolean, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.g2d.SpriteBatch.draw(com.badlogic.gdx.graphics.Texture, float, float, float, float, int, int, int, int, boolean, boolean):void
     arg types: [com.badlogic.gdx.graphics.Texture, float, float, float, float, int, int, int, int, int, int]
     candidates:
      com.badlogic.gdx.graphics.g2d.SpriteBatch.draw(com.badlogic.gdx.graphics.g2d.TextureRegion, float, float, float, float, float, float, float, float, float, boolean):void
      com.badlogic.gdx.graphics.g2d.SpriteBatch.draw(com.badlogic.gdx.graphics.Texture, float, float, float, float, int, int, int, int, boolean, boolean):void */
    public void render(Application app) {
        app.getGraphics().getGL10().glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
        app.getGraphics().getGL10().glClear(16384);
        this.spriteBatch.begin();
        this.spriteBatch.enableBlending();
        this.spriteBatch.draw(this.texBackPic, (float) ((States.screenWidth / 2) - (States.screenHeight / 2)), 0.0f, (float) States.screenHeight, (float) States.screenHeight, this.geomBackPic.x, this.geomBackPic.y, this.geomBackPic.width, this.geomBackPic.height, false, false);
        if (States.isAntialiasing) {
            this.spriteBatch.draw(this.texBackground, ((float) (-States.cameraOffsetX)) * States.aspectX, ((float) (-States.cameraOffsetY)) * States.aspectY, (float) States.displayWidth, (float) States.displayHeight, 0, 0, this.texBackground.getWidth(), this.texBackground.getHeight(), false, false);
        } else {
            this.spriteBatch.draw(this.texBackground, ((float) (-States.cameraOffsetX)) * States.aspectX, 0.0f, 0, 0, States.displayWidth, States.screenHeight);
        }
        if (this.isActiveHttp) {
            this.font.setColor(0.5f, 0.5f, 1.0f, 1.0f);
        } else {
            this.font.setColor(0.8f, 0.8f, 1.0f, 1.0f);
        }
        this.font.drawMultiLine(this.spriteBatch, "Server", 0.0f, (float) (States.screenHeight / 4), (float) States.screenWidth, BitmapFont.HAlignment.CENTER);
        if (this.isActiveEmail) {
            this.font.setColor(0.5f, 0.5f, 1.0f, 1.0f);
        } else {
            this.font.setColor(0.8f, 0.8f, 1.0f, 1.0f);
        }
        this.font.drawMultiLine(this.spriteBatch, "Both", 0.0f, (float) ((States.screenHeight / 4) - ((States.screenHeight * 30) / 320)), (float) States.screenWidth, BitmapFont.HAlignment.CENTER);
        this.font.setColor(0.3f, 0.3f, 0.3f, 1.0f);
        this.font.drawMultiLine(this.spriteBatch, "2011,  0.92 beta  ", ((((float) States.displayWidth) / States.aspectX) - ((float) States.screenWidth)) / 2.0f, this.font.getCapHeight() * 2.0f, (float) States.screenWidth, BitmapFont.HAlignment.RIGHT);
        this.spriteBatch.end();
    }

    public void dispose() {
        this.spriteBatch.dispose();
        this.texBackground.dispose();
        this.texBackPic.dispose();
        this.font.dispose();
    }

    public void resume() {
    }

    private boolean isInputCoordsIn(Geom geom) {
        if (((float) this.inp.x) <= ((float) (geom.x + States.cameraOffsetX)) * States.aspectX || ((float) this.inp.x) >= ((float) (geom.x + geom.width + States.cameraOffsetX)) * States.aspectX || ((float) (States.displayHeight - this.inp.y)) <= ((float) (geom.y + States.cameraOffsetY)) * States.aspectY || ((float) (States.displayHeight - this.inp.y)) >= ((float) (geom.y + geom.height + States.cameraOffsetY)) * States.aspectY) {
            return false;
        }
        return true;
    }
}
