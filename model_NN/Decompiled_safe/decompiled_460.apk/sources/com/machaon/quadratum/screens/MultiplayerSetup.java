package com.machaon.quadratum.screens;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.machaon.quadratum.IScreen;
import com.machaon.quadratum.States;
import com.machaon.quadratum.input.CommonInput;
import com.machaon.quadratum.parts.ButtonChar;
import com.machaon.quadratum.parts.ButtonPic;
import com.machaon.quadratum.parts.ButtonText;
import com.machaon.quadratum.parts.Geom;
import com.machaon.quadratum.parts.Keyboard;
import com.machaon.quadratum.parts.Profile;

public class MultiplayerSetup implements IScreen {
    static final byte AI1 = 3;
    static final byte AI2 = 4;
    static final byte AI3 = 5;
    static final byte BUSY = 1;
    static final byte FREE = 2;
    private final byte POS_KEYBOARD = 1;
    private final byte POS_SETUP = 0;
    private char bukva;
    private ButtonPic buttonBonusMode;
    private ButtonPic buttonNameEdit;
    private ButtonText[] buttonNames = new ButtonText[4];
    private ButtonPic buttonNetwork;
    private ButtonPic buttonPlay;
    private ButtonPic[] buttonQuant = new ButtonPic[2];
    private byte currentWindow = 0;
    private Geom gInfo;
    private Geom gList;
    private Geom gRect;
    private Geom geomBackPic;
    private Geom geomFlag;
    private CommonInput inp = new CommonInput();
    private boolean isLimit;
    private boolean isLite;
    private boolean isShowWindow = false;
    private Keyboard keyboard;
    private ButtonText[] list = new ButtonText[20];
    private String name;
    private byte namesInList;
    private Pixmap pixmap;
    private byte pos;
    private Sound soundClick;
    private final SpriteBatch spriteBatch = new SpriteBatch();
    private Texture texBackPic;
    private Texture texBonusOff;
    private Texture texButtonA;
    private Texture texButtonNA;
    private Texture texButtonNameA;
    private Texture texButtonNameNA;
    private Texture texFlags;
    private Texture texStar;
    private Texture texture;
    private float timer;

    /* JADX INFO: Multiple debug info for r0v0 com.machaon.quadratum.parts.ButtonText: [D('b' com.machaon.quadratum.parts.ButtonText), D('b' com.machaon.quadratum.parts.ButtonPic)] */
    public MultiplayerSetup() {
        States.namingAI();
        this.keyboard = new Keyboard();
        this.name = "";
        this.bukva = Keyboard.CHAR_SPACE;
        this.pos = 0;
        this.soundClick = Gdx.audio.newSound(Gdx.files.internal("data/Sounds/color_click.ogg"));
        Profile.isActiveProfile[0] = true;
        for (byte p = 1; p < 4; p = (byte) (p + 1)) {
            Profile.isActiveProfile[p] = false;
        }
        Profile.popProfile(Profile.profile.numberOfProfile, Profile.pp[0]);
        States.playerName[0] = Profile.pp[0].name;
        for (byte i = 1; i < 4; i = (byte) (i + 1)) {
            States.skillAI[i] = 2;
        }
        States.namingAI();
        if (States.numberOfPlayers == 4) {
            States.numberOfColors = 7;
        } else {
            States.numberOfColors = 5;
        }
        Init();
        this.spriteBatch.setProjectionMatrix(States.viewMatrix);
        if (States.isAntialiasing) {
            for (ButtonPic b : this.buttonQuant) {
                b.texBackAct.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
                b.texBackNonAct.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
                b.texFront.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
            }
            for (ButtonText b2 : this.buttonNames) {
                b2.texBackAct.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
                b2.texBackNonAct.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
            }
            this.texFlags.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
            this.texStar.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        }
        this.texBackPic.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
    }

    private void Init() {
        int i;
        int i2 = States.screenWidth > 512 ? 1024 : 512;
        if (States.screenHeight > 512) {
            i = 1024;
        } else {
            i = 512;
        }
        this.texture = new Texture(i2, i, Pixmap.Format.RGBA8888);
        String path = States.PATH_GRAPH + States.resolution + "/";
        this.texBackPic = new Texture(Gdx.files.internal("data/Graph/480/but_multi.png"));
        this.texStar = new Texture(Gdx.files.internal(String.valueOf(path) + "starlist.png"));
        this.texFlags = new Texture(Gdx.files.internal(String.valueOf(path) + States.TEX_FLAGS));
        this.geomBackPic = new Geom(0, 0, 142, 142);
        if (States.screenHeight == 240 && States.screenWidth == 320) {
            for (byte i3 = 0; i3 < 2; i3 = (byte) (i3 + 1)) {
                this.buttonQuant[i3] = new ButtonPic((i3 * 64) + Input.Keys.BUTTON_Z, 137, 54, 54, 3, -4);
            }
            this.buttonNames[0] = new ButtonText(4, 213, 152, 21, 18);
            this.buttonNames[1] = new ButtonText(164, 213, 152, 21, 18);
            this.buttonNames[2] = new ButtonText(164, 6, 152, 21, 18);
            this.buttonNames[3] = new ButtonText(4, 6, 152, 21, 18);
            this.buttonBonusMode = new ButtonPic(37, 97, 54, 54, 2, -2);
            this.buttonNetwork = new ButtonPic(229, 97, 54, 54, 3, -3);
            this.buttonPlay = new ButtonPic(118, 45, 84, 84, 7, -7);
            this.gList = new Geom(0, 0, (this.buttonNames[0].width + (this.buttonNames[0].x * 2)) - 4, ((States.screenHeight - this.buttonNames[0].height) - this.buttonNames[0].x) - 4);
            for (byte i4 = 0; i4 < this.list.length; i4 = (byte) (i4 + 1)) {
                this.list[i4] = new ButtonText(0, 0, 226, 18, 19);
            }
            this.geomFlag = new Geom(285, 3, 29, 30);
            this.buttonNameEdit = new ButtonPic(37, 161, (int) Input.Keys.F2, 30, 12, 22);
        }
        if (States.screenHeight == 240 && States.screenWidth == 400) {
            for (byte i5 = 0; i5 < 2; i5 = (byte) (i5 + 1)) {
                this.buttonQuant[i5] = new ButtonPic((i5 * 64) + 141, 137, 54, 54, 3, -4);
            }
            this.buttonNames[0] = new ButtonText(6, 213, 170, 21, 18);
            this.buttonNames[1] = new ButtonText(224, 213, 170, 21, 18);
            this.buttonNames[2] = new ButtonText(224, 6, 170, 21, 18);
            this.buttonNames[3] = new ButtonText(6, 6, 170, 21, 18);
            this.buttonBonusMode = new ButtonPic(77, 97, 54, 54, 1, -1);
            this.buttonNetwork = new ButtonPic(269, 97, 54, 54, 3, -3);
            this.buttonPlay = new ButtonPic(158, 45, 84, 84, 7, -7);
            this.gList = new Geom(0, 0, (this.buttonNames[0].width + (this.buttonNames[0].x * 2)) - 4, ((States.screenHeight - this.buttonNames[0].height) - this.buttonNames[0].x) - 4);
            for (byte i6 = 0; i6 < this.list.length; i6 = (byte) (i6 + 1)) {
                this.list[i6] = new ButtonText(0, 0, 226, 18, 19);
            }
            this.geomFlag = new Geom(354, 11, 29, 30);
            this.buttonNameEdit = new ButtonPic(77, 161, (int) Input.Keys.F2, 30, 12, 22);
        }
        if (States.screenHeight == 320) {
            for (byte i7 = 0; i7 < 2; i7 = (byte) (i7 + 1)) {
                this.buttonQuant[i7] = new ButtonPic((i7 * 84) + 161, 182, 74, 74, 5, -5);
            }
            this.buttonNames[0] = new ButtonText(7, 285, 226, 28, 24);
            this.buttonNames[1] = new ButtonText(Input.Keys.F4, 285, 226, 28, 24);
            this.buttonNames[2] = new ButtonText(Input.Keys.F4, 7, 226, 28, 24);
            this.buttonNames[3] = new ButtonText(7, 7, 226, 28, 24);
            this.buttonBonusMode = new ButtonPic(77, (int) Input.Keys.CONTROL_RIGHT, 74, 74, 4, -4);
            this.buttonNetwork = new ButtonPic(329, (int) Input.Keys.CONTROL_RIGHT, 74, 74, 4, -4);
            this.buttonPlay = new ButtonPic(183, 60, (int) Input.Keys.FORWARD_DEL, (int) Input.Keys.FORWARD_DEL, 7, -7);
            this.gList = new Geom(0, 0, (this.buttonNames[0].width + (this.buttonNames[0].x * 2)) - 4, ((States.screenHeight - this.buttonNames[0].height) - this.buttonNames[0].x) - 4);
            for (byte i8 = 0; i8 < this.list.length; i8 = (byte) (i8 + 1)) {
                this.list[i8] = new ButtonText(0, 0, 226, 24, 24);
            }
            this.geomFlag = new Geom(430, 8, 39, 39);
            this.buttonNameEdit = new ButtonPic(77, 215, 326, 40, 15, 30);
        }
        if (States.screenHeight == 480) {
            if (States.screenWidth == 800) {
                for (byte i9 = 0; i9 < 2; i9 = (byte) (i9 + 1)) {
                    this.buttonQuant[i9] = new ButtonPic((i9 * 128) + 282, 273, (int) Input.Keys.BUTTON_START, (int) Input.Keys.BUTTON_START, 6, -6);
                }
                this.buttonNames[0] = new ButtonText(11, 427, 350, 42, 32);
                this.buttonNames[1] = new ButtonText(439, 427, 350, 42, 32);
                this.buttonNames[2] = new ButtonText(439, 11, 350, 42, 32);
                this.buttonNames[3] = new ButtonText(11, 11, 350, 42, 32);
                this.buttonBonusMode = new ButtonPic(154, 190, (int) Input.Keys.BUTTON_START, (int) Input.Keys.BUTTON_START, 5, -5);
                this.buttonNetwork = new ButtonPic(538, 190, (int) Input.Keys.BUTTON_START, (int) Input.Keys.BUTTON_START, 6, -5);
                this.buttonPlay = new ButtonPic(319, 90, 162, 162, 11, -11);
                this.gList = new Geom(0, 0, (this.buttonNames[0].width + (this.buttonNames[0].x * 2)) - 4, ((States.screenHeight - this.buttonNames[0].height) - this.buttonNames[0].x) - 4);
                for (byte i10 = 0; i10 < this.list.length; i10 = (byte) (i10 + 1)) {
                    this.list[i10] = new ButtonText(0, 0, 226, 36, 37);
                }
                this.geomFlag = new Geom(708, 23, 56, 59);
                this.buttonNameEdit = new ButtonPic(155, 322, 490, 58, 24, 39);
            } else {
                for (byte i11 = 0; i11 < 2; i11 = (byte) (i11 + 1)) {
                    this.buttonQuant[i11] = new ButtonPic((i11 * 128) + 309, 273, (int) Input.Keys.BUTTON_START, (int) Input.Keys.BUTTON_START, 6, -6);
                }
                this.buttonNames[0] = new ButtonText(11, 427, 350, 42, 32);
                this.buttonNames[1] = new ButtonText(493, 427, 350, 42, 32);
                this.buttonNames[2] = new ButtonText(493, 11, 350, 42, 32);
                this.buttonNames[3] = new ButtonText(11, 11, 350, 42, 32);
                this.buttonBonusMode = new ButtonPic(181, 190, (int) Input.Keys.BUTTON_START, (int) Input.Keys.BUTTON_START, 5, -5);
                this.buttonNetwork = new ButtonPic(565, 190, (int) Input.Keys.BUTTON_START, (int) Input.Keys.BUTTON_START, 6, -5);
                this.buttonPlay = new ButtonPic(346, 90, 162, 162, 11, -11);
                this.gList = new Geom(0, 0, (this.buttonNames[0].width + (this.buttonNames[0].x * 2)) - 4, ((States.screenHeight - this.buttonNames[0].height) - this.buttonNames[0].x) - 4);
                for (byte i12 = 0; i12 < this.list.length; i12 = (byte) (i12 + 1)) {
                    this.list[i12] = new ButtonText(0, 0, 226, 36, 37);
                }
                this.geomFlag = new Geom(735, 23, 56, 59);
                this.buttonNameEdit = new ButtonPic(182, 322, 490, 58, 24, 39);
            }
        }
        this.texButtonNA = new Texture(Gdx.files.internal(String.valueOf(path) + States.TEX_BUTTON_MIDDLE_NA));
        this.texButtonA = new Texture(Gdx.files.internal(String.valueOf(path) + States.TEX_BUTTON_MIDDLE_A));
        this.buttonQuant[0].SetParams(this.texButtonNA, this.texButtonA, new Texture(Gdx.files.internal(String.valueOf(path) + "2players.png")), (byte) 2);
        this.buttonQuant[1].SetParams(this.texButtonNA, this.texButtonA, new Texture(Gdx.files.internal(String.valueOf(path) + "4players.png")), (byte) 4);
        this.buttonNetwork.SetParams(this.texButtonNA, this.texButtonA, new Texture(Gdx.files.internal(String.valueOf(path) + "Wi-Fi.png")), States.NETWORK);
        this.buttonBonusMode.SetParams(this.texButtonNA, this.texButtonA, new Texture(Gdx.files.internal(String.valueOf(path) + "bonus_mode.png")), States.NETWORK);
        this.buttonPlay.SetParams(new Texture(Gdx.files.internal(String.valueOf(path) + "but_na.png")), new Texture(Gdx.files.internal(String.valueOf(path) + "but_a.png")), new Texture(Gdx.files.internal(String.valueOf(path) + "play.png")), (byte) 8);
        this.texBonusOff = new Texture(Gdx.files.internal(String.valueOf(path) + "bonus_off.png"));
        this.buttonNameEdit.SetParams(new Texture(Gdx.files.internal(String.valueOf(path) + "but_name_na.png")), new Texture(Gdx.files.internal(String.valueOf(path) + "but_name_a.png")), (byte) 1);
        if (States.screenWidth == 320) {
            path = "data/Graph/240_320/";
        }
        this.texButtonNameNA = new Texture(Gdx.files.internal(String.valueOf(path) + "but_name_2_na.png"));
        this.texButtonNameA = new Texture(Gdx.files.internal(String.valueOf(path) + "but_name_2_a.png"));
        this.buttonNames[0].SetParams(States.playerName[0], this.texButtonNameNA, this.texButtonNameA, (byte) 0);
        this.buttonNames[1].SetParams(States.playerName[1], this.texButtonNameNA, this.texButtonNameA, (byte) 1);
        this.buttonNames[2].SetParams(States.playerName[2], this.texButtonNameNA, this.texButtonNameA, (byte) 2);
        this.buttonNames[3].SetParams(States.playerName[3], this.texButtonNameNA, this.texButtonNameA, (byte) 3);
        InitObjects();
        Gdx.app.getInput().setInputProcessor(this.inp);
    }

    private void InitObjects() {
        if (States.numberOfPlayers == 4) {
            for (byte i = 0; i < 4; i = (byte) (i + 1)) {
                if (i != 1 && States.playerName[1] == States.playerName[i]) {
                    States.skillAI[i] = 2;
                    States.namingAI(i);
                    this.buttonNames[i].setString(States.playerName[i]);
                }
            }
        }
        this.pixmap = new Pixmap(States.screenWidth, States.screenHeight, Pixmap.Format.RGBA8888);
        if (States.screenHeight == 240 && States.screenWidth == 320) {
            this.gRect = new Geom(20, 30, 280, 180);
            this.gInfo = new Geom(8, 211, 18, 31);
        }
        if (States.screenHeight == 240 && States.screenWidth == 400) {
            this.gRect = new Geom(50, 30, 300, 180);
            this.gInfo = new Geom(8, 211, 18, 31);
        }
        if (States.screenHeight == 320) {
            this.gRect = new Geom(40, 40, 400, 240);
            this.gInfo = new Geom(10, 280, 26, 44);
        }
        if (States.screenHeight == 480) {
            if (States.screenWidth == 800) {
                this.gRect = new Geom(100, 60, 600, 360);
                this.gInfo = new Geom(15, 422, 41, 53);
            } else {
                this.gRect = new Geom(120, 60, 614, 360);
                this.gInfo = new Geom(15, 422, 41, 53);
            }
        }
        this.pixmap.setColor(0.4f, 0.4f, 0.4f, 0.7f);
        if (States.numberOfPlayers == 4) {
            this.pixmap.drawLine(States.screenWidth / 2, 0, States.screenWidth / 2, this.gRect.y - 1);
            this.pixmap.drawLine(States.screenWidth / 2, this.gRect.y + this.gRect.height, States.screenWidth / 2, States.screenHeight);
        }
        this.pixmap.drawLine(0, States.screenHeight / 2, this.gRect.x - 1, States.screenHeight / 2);
        this.pixmap.drawLine(this.gRect.x + this.gRect.width, States.screenHeight / 2, States.screenWidth, States.screenHeight / 2);
        if (this.isShowWindow) {
            if (this.currentWindow == 0) {
                this.pixmap.setColor(0.0f, 0.0f, 0.0f, 0.8f);
                this.pixmap.fillRectangle(2, this.buttonNames[0].x + this.buttonNames[0].height + 2, (States.screenWidth / 2) - 4, ((States.screenHeight - this.buttonNames[0].height) - this.buttonNames[0].x) - 4);
                this.pixmap.setColor(0.4f, 0.4f, 0.4f, 0.9f);
                this.pixmap.drawRectangle(2, this.buttonNames[0].x + this.buttonNames[0].height + 2, (States.screenWidth / 2) - 4, ((States.screenHeight - this.buttonNames[0].height) - this.buttonNames[0].x) - 4);
                this.gList.x = 2;
                this.gList.y = this.buttonNames[0].x + this.buttonNames[0].height + 2;
            }
            if (this.currentWindow == 3) {
                this.pixmap.setColor(0.0f, 0.0f, 0.0f, 0.8f);
                this.pixmap.fillRectangle(2, 2, (States.screenWidth / 2) - 4, ((States.screenHeight - this.buttonNames[0].height) - this.buttonNames[0].x) - 4);
                this.pixmap.setColor(0.4f, 0.4f, 0.4f, 0.9f);
                this.pixmap.drawRectangle(2, 2, (States.screenWidth / 2) - 4, ((States.screenHeight - this.buttonNames[0].height) - this.buttonNames[0].x) - 4);
                this.gList.x = 2;
                this.gList.y = 2;
            }
            if (this.currentWindow == 1) {
                this.pixmap.setColor(0.0f, 0.0f, 0.0f, 0.8f);
                this.pixmap.fillRectangle((States.screenWidth / 2) + 2, this.buttonNames[0].x + this.buttonNames[0].height + 2, (States.screenWidth / 2) - 4, ((States.screenHeight - this.buttonNames[0].height) - this.buttonNames[0].x) - 4);
                this.pixmap.setColor(0.4f, 0.4f, 0.4f, 0.9f);
                this.pixmap.drawRectangle((States.screenWidth / 2) + 2, this.buttonNames[0].x + this.buttonNames[0].height + 2, (States.screenWidth / 2) - 4, ((States.screenHeight - this.buttonNames[0].height) - this.buttonNames[0].x) - 4);
                this.gList.x = (States.screenWidth / 2) + 2;
                this.gList.y = this.buttonNames[0].x + this.buttonNames[0].height + 2;
            }
            if (this.currentWindow == 2) {
                this.pixmap.setColor(0.0f, 0.0f, 0.0f, 0.8f);
                this.pixmap.fillRectangle((States.screenWidth / 2) + 2, 2, (States.screenWidth / 2) - 4, ((States.screenHeight - this.buttonNames[0].height) - this.buttonNames[0].x) - 4);
                this.pixmap.setColor(0.4f, 0.4f, 0.4f, 0.9f);
                this.pixmap.drawRectangle((States.screenWidth / 2) + 2, 2, (States.screenWidth / 2) - 4, ((States.screenHeight - this.buttonNames[0].height) - this.buttonNames[0].x) - 4);
                this.gList.x = (States.screenWidth / 2) + 2;
                this.gList.y = 2;
            }
        }
        this.texture.draw(this.pixmap, 0, 0);
        if (States.numberOfPlayers == 2) {
            this.buttonNames[2].setString(States.playerName[1]);
        } else {
            this.buttonNames[2].setString(States.playerName[2]);
        }
        this.list[0].setString(States.TEXT_NEWNAME[States.language]);
        this.list[0].state = 2;
        this.list[0].x = this.gList.x;
        int i2 = 0 + 1;
        this.list[0].y = ((States.screenHeight - this.gList.y) - (this.list[0].height / 2)) - (this.list[0].height * 1);
        byte count = (byte) (0 + 1);
        for (byte i3 = 0; i3 < 6; i3 = (byte) (i3 + 1)) {
            this.list[count].setString(Profile.getProfileName((byte) (5 - i3)));
            if (this.list[count].getString() != "") {
                this.list[count].state = 2;
                for (byte p = 0; p < States.numberOfPlayers; p = (byte) (p + 1)) {
                    if (States.playerName[p] == this.list[count].getString()) {
                        this.list[count].state = 1;
                    }
                }
                this.list[count].x = this.gList.x;
                this.list[count].y = ((States.screenHeight - this.gList.y) - (this.list[count].height / 2)) - (this.list[count].height * (count + 1));
                count = (byte) (count + 1);
            }
        }
        for (byte i4 = 0; i4 < 3; i4 = (byte) (i4 + 1)) {
            this.list[count].setString("AI");
            this.list[count].state = (byte) (i4 + 3);
            this.list[count].x = this.gList.x;
            this.list[count].y = ((States.screenHeight - this.gList.y) - (this.list[count].height / 2)) - (this.list[count].height * (count + 1));
            count = (byte) (count + 1);
        }
        this.namesInList = count;
        this.pixmap.dispose();
    }

    public void update() {
        if (isTimerEnd()) {
            this.isLimit = false;
            this.isLite = false;
        }
        if (this.inp.isBack) {
            if (Profile.profile.settingSounds) {
                States.click.play();
            }
            States.state = 4;
        }
        if (this.pos == 0) {
            updateSetup();
        } else {
            updateKeyboard();
        }
    }

    private void updateSetup() {
        byte b = this.inp.buttonUp;
        this.inp.getClass();
        if (b == 50) {
            if (this.isShowWindow) {
                for (byte c = 0; c < this.namesInList; c = (byte) (c + 1)) {
                    if (isInputCoordsIn(this.list[c])) {
                        if (this.list[c].state != 1) {
                            if (Profile.profile.settingSounds) {
                                this.soundClick.play();
                            }
                            if (!(this.list[c].state == 3 || this.list[c].state == 4 || this.list[c].state == 5 || States.numberOfPlayers != 4)) {
                                byte numbers = 0;
                                for (byte p = 0; p < States.numberOfPlayers; p = (byte) (p + 1)) {
                                    if (States.skillAI[p] == 0) {
                                        numbers = (byte) (numbers + 1);
                                    }
                                }
                                if (numbers == 2 && States.skillAI[this.currentWindow] != 0) {
                                    this.isShowWindow = false;
                                    this.inp.x = 0;
                                    InitObjects();
                                    this.isLimit = true;
                                    this.isLite = false;
                                    setTimer(4.0f);
                                    return;
                                }
                            }
                            for (byte i = 0; i < 6; i = (byte) (i + 1)) {
                                if (this.list[c].getString() == Profile.getProfileName(i)) {
                                    if (this.currentWindow == 2 && States.numberOfPlayers == 2) {
                                        States.skillAI[1] = 0;
                                        States.playerName[1] = this.list[c].getString();
                                        this.buttonNames[1].setString(this.list[c].getString());
                                        Profile.isActiveProfile[1] = true;
                                        Profile.popProfile(i, Profile.pp[1]);
                                    } else {
                                        States.skillAI[this.currentWindow] = 0;
                                        States.playerName[this.currentWindow] = this.list[c].getString();
                                        this.buttonNames[this.currentWindow].setString(this.list[c].getString());
                                        Profile.isActiveProfile[this.currentWindow] = true;
                                        Profile.popProfile(i, Profile.pp[this.currentWindow]);
                                    }
                                    this.isShowWindow = false;
                                    InitObjects();
                                    this.inp.x = 0;
                                }
                            }
                            if (this.list[c].state == 3 || this.list[c].state == 4 || this.list[c].state == 5) {
                                if (this.currentWindow == 2 && States.numberOfPlayers == 2) {
                                    States.skillAI[1] = (this.list[c].state - 3) + 1;
                                    States.namingAI((byte) 1);
                                    this.buttonNames[1].setString(States.playerName[1]);
                                    Profile.isActiveProfile[1] = false;
                                } else {
                                    States.skillAI[this.currentWindow] = (this.list[c].state - 3) + 1;
                                    States.namingAI(this.currentWindow);
                                    this.buttonNames[this.currentWindow].setString(States.playerName[this.currentWindow]);
                                    Profile.isActiveProfile[this.currentWindow] = false;
                                }
                                this.isShowWindow = false;
                                InitObjects();
                                this.inp.x = 0;
                            }
                            if (this.list[c].getString() == States.TEXT_NEWNAME[States.language]) {
                                this.pos = 1;
                                this.isShowWindow = false;
                                this.inp.x = 0;
                                InitObjects();
                                return;
                            }
                        }
                        this.list[c].setBackActive(false);
                    }
                }
                if (isInputCoordsIn(this.buttonNames[this.currentWindow])) {
                    this.isShowWindow = false;
                    this.inp.x = 0;
                    InitObjects();
                    return;
                }
            } else {
                if (isInputCoordsIn(this.buttonNetwork)) {
                    if (Profile.profile.settingSounds) {
                        States.click.play();
                    }
                    this.isLite = true;
                    this.isLimit = false;
                    setTimer(3.0f);
                    this.inp.x = 0;
                }
                if (isInputCoordsIn(this.buttonBonusMode)) {
                    if (Profile.profile.settingSounds) {
                        States.click.play();
                    }
                    this.isLite = true;
                    this.isLimit = false;
                    setTimer(3.0f);
                    this.inp.x = 0;
                }
                for (int i2 = 0; i2 < 2; i2++) {
                    if (isInputCoordsIn(this.buttonQuant[i2])) {
                        if (Profile.profile.settingSounds) {
                            States.click.play();
                        }
                        States.numberOfPlayers = this.buttonQuant[i2].getState();
                        if (States.numberOfPlayers == 2) {
                            States.numberOfColors = 5;
                        } else {
                            States.numberOfColors = 7;
                        }
                        if (States.numberOfPlayers == 4) {
                            byte numbers2 = 0;
                            for (byte p2 = 0; p2 < States.numberOfPlayers; p2 = (byte) (p2 + 1)) {
                                if (States.skillAI[p2] == 0 && (numbers2 = (byte) (numbers2 + 1)) > 2) {
                                    States.skillAI[p2] = 2;
                                    States.namingAI(p2);
                                    this.buttonNames[p2].setString(States.playerName[p2]);
                                    Profile.isActiveProfile[p2] = false;
                                }
                            }
                        }
                        this.buttonQuant[0].setBackActive(false);
                        this.buttonQuant[1].setBackActive(false);
                        InitObjects();
                        CommonInput commonInput = this.inp;
                        this.inp.getClass();
                        commonInput.buttonUp = States.NONE;
                    }
                }
                if (isInputCoordsIn(this.buttonPlay)) {
                    if (Profile.profile.settingSounds) {
                        States.click.play();
                    }
                    States.state = this.buttonPlay.getState();
                }
                for (ButtonText b2 : this.buttonNames) {
                    if (isInputCoordsIn(b2)) {
                        this.currentWindow = b2.getState();
                        this.isShowWindow = true;
                        InitObjects();
                        CommonInput commonInput2 = this.inp;
                        this.inp.getClass();
                        commonInput2.buttonUp = States.NONE;
                    }
                }
            }
        }
        byte b3 = this.inp.buttonDown;
        this.inp.getClass();
        if (b3 == 50) {
            if (this.isShowWindow) {
                if (this.currentWindow == 0 || this.currentWindow == 3) {
                    if (this.inp.x > States.screenWidth / 2) {
                        this.isShowWindow = false;
                        InitObjects();
                    }
                } else if (this.inp.x < States.screenWidth / 2) {
                    this.isShowWindow = false;
                    InitObjects();
                }
                for (byte i3 = 0; i3 < this.namesInList; i3 = (byte) (i3 + 1)) {
                    this.list[i3].setBackActive(isInputCoordsIn(this.list[i3]));
                }
            } else {
                for (int i4 = 0; i4 < 2; i4++) {
                    this.buttonQuant[i4].setBackActive(isInputCoordsIn(this.buttonQuant[i4]));
                }
                this.buttonPlay.setBackActive(isInputCoordsIn(this.buttonPlay));
                this.buttonNetwork.setBackActive(isInputCoordsIn(this.buttonNetwork));
                this.buttonBonusMode.setBackActive(isInputCoordsIn(this.buttonBonusMode));
                for (ButtonText b4 : this.buttonNames) {
                    b4.setBackActive(isInputCoordsIn(b4));
                }
            }
        }
        this.buttonQuant[States.numberOfPlayers >> 2].setBackActive(true);
    }

    public void updateKeyboard() {
        byte b = this.inp.buttonUp;
        this.inp.getClass();
        if (b == 50) {
            if (isInputCoordsIn(this.geomFlag)) {
                if (Profile.profile.settingSounds) {
                    States.click.play();
                }
                Keyboard keyboard2 = this.keyboard;
                keyboard2.language = (byte) (keyboard2.language + 1);
                if (this.keyboard.language > 1) {
                    this.keyboard.language = 0;
                }
                this.keyboard.setLanguage(this.keyboard.language);
                this.keyboard.setState((byte) 0);
            }
            if (isInputCoordsIn(this.buttonNameEdit) || isInputCoordsIn(this.keyboard.buttonControl[4])) {
                this.pos = 0;
                boolean isName = false;
                for (byte i = 0; i < this.name.length(); i = (byte) (i + 1)) {
                    if (this.name.charAt(i) != ' ') {
                        isName = true;
                    }
                }
                if (isName) {
                    if (States.numberOfPlayers == 2 && this.currentWindow == 2) {
                        States.playerName[1] = this.name;
                        this.buttonNames[1].setString(this.name);
                        States.skillAI[1] = 0;
                        Profile.isActiveProfile[1] = false;
                    } else {
                        States.playerName[this.currentWindow] = this.name;
                        this.buttonNames[this.currentWindow].setString(this.name);
                        States.skillAI[this.currentWindow] = 0;
                        Profile.isActiveProfile[this.currentWindow] = false;
                    }
                    this.isShowWindow = false;
                    this.name = "";
                    InitObjects();
                } else {
                    return;
                }
            }
            for (ButtonChar b2 : this.keyboard.buttonKb) {
                if (isInputCoordsIn(b2) && States.fontBig.getBounds(this.name).width < ((float) ((States.screenWidth * 125) / 480))) {
                    this.name = String.valueOf(this.name) + this.keyboard.getChar(b2);
                }
            }
            for (ButtonChar b3 : this.keyboard.buttonControl) {
                if (isInputCoordsIn(b3)) {
                    if (this.keyboard.getChar(b3) == '~') {
                        this.keyboard.setState((byte) 3);
                    }
                    if (this.keyboard.getChar(b3) == '_') {
                        this.keyboard.setState(Keyboard.STATE_NUMBER);
                    }
                    if (this.keyboard.getChar(b3) == '`' && this.name.length() > 0) {
                        this.name = String.copyValueOf(this.name.toCharArray(), 0, this.name.length() - 1);
                    }
                    if (this.keyboard.getChar(b3) == ' ') {
                        this.name = String.valueOf(this.name) + this.keyboard.getChar(b3);
                    }
                }
            }
            this.bukva = Keyboard.CHAR_SPACE;
            CommonInput commonInput = this.inp;
            this.inp.getClass();
            commonInput.buttonUp = States.NONE;
        }
        byte b4 = this.inp.buttonDown;
        this.inp.getClass();
        if (b4 == 50) {
            this.buttonNameEdit.setBackActive(isInputCoordsIn(this.buttonNameEdit));
            for (ButtonChar b5 : this.keyboard.buttonKb) {
                if (isInputCoordsIn(b5)) {
                    this.bukva = this.keyboard.getChar(b5);
                    b5.setBackActive(true);
                }
            }
            for (ButtonChar b6 : this.keyboard.buttonControl) {
                b6.setBackActive(isInputCoordsIn(b6));
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.g2d.SpriteBatch.draw(com.badlogic.gdx.graphics.Texture, float, float, float, float, int, int, int, int, boolean, boolean):void
     arg types: [com.badlogic.gdx.graphics.Texture, float, int, float, float, int, int, int, int, int, int]
     candidates:
      com.badlogic.gdx.graphics.g2d.SpriteBatch.draw(com.badlogic.gdx.graphics.g2d.TextureRegion, float, float, float, float, float, float, float, float, float, boolean):void
      com.badlogic.gdx.graphics.g2d.SpriteBatch.draw(com.badlogic.gdx.graphics.Texture, float, float, float, float, int, int, int, int, boolean, boolean):void */
    public void render(Application app) {
        app.getGraphics().getGL10().glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
        app.getGraphics().getGL10().glClear(16384);
        this.spriteBatch.begin();
        this.spriteBatch.enableBlending();
        if (this.pos == 0) {
            this.spriteBatch.draw(this.texBackPic, (float) ((States.screenWidth / 2) - ((States.screenHeight * 1) / 2)), 0.0f, (float) States.screenHeight, (float) States.screenHeight, this.geomBackPic.x, this.geomBackPic.y, this.geomBackPic.width, this.geomBackPic.height, false, false);
            States.renderBackground(this.spriteBatch);
            States.fontBig.setColor(1.0f, 1.0f, 1.0f, 1.0f);
            if (States.numberOfPlayers == 4) {
                for (ButtonText b : this.buttonNames) {
                    this.spriteBatch.draw(b.getBackTexture(), (float) b.x, (float) b.y, 0, 0, b.width, b.height);
                    States.fontBig.drawMultiLine(this.spriteBatch, b.getString(), (float) b.x, (float) (b.y + b.getTextOffsetY()), (float) b.width, BitmapFont.HAlignment.CENTER);
                }
            } else {
                this.spriteBatch.draw(this.buttonNames[0].getBackTexture(), (float) this.buttonNames[0].x, (float) this.buttonNames[0].y, 0, 0, this.buttonNames[0].width, this.buttonNames[0].height);
                States.fontBig.drawMultiLine(this.spriteBatch, this.buttonNames[0].getString(), (float) this.buttonNames[0].x, (float) (this.buttonNames[0].y + this.buttonNames[0].getTextOffsetY()), (float) this.buttonNames[0].width, BitmapFont.HAlignment.CENTER);
                this.spriteBatch.draw(this.buttonNames[2].getBackTexture(), (float) this.buttonNames[2].x, (float) this.buttonNames[2].y, 0, 0, this.buttonNames[2].width, this.buttonNames[2].height);
                States.fontBig.drawMultiLine(this.spriteBatch, this.buttonNames[2].getString(), (float) this.buttonNames[2].x, (float) (this.buttonNames[2].y + this.buttonNames[2].getTextOffsetY()), (float) this.buttonNames[2].width, BitmapFont.HAlignment.CENTER);
            }
            for (ButtonPic b2 : this.buttonQuant) {
                this.spriteBatch.draw(b2.getBackTexture(), (float) b2.x, (float) b2.y, 0, 0, b2.width, b2.height);
                this.spriteBatch.draw(b2.texFront, (float) (b2.x + b2.frontX), (float) (b2.y + b2.frontY), 0, 0, b2.width, b2.height);
            }
            this.spriteBatch.draw(this.buttonPlay.getBackTexture(), (float) this.buttonPlay.x, (float) this.buttonPlay.y, 0, 0, this.buttonPlay.width, this.buttonPlay.height);
            this.spriteBatch.draw(this.buttonPlay.texFront, (float) (this.buttonPlay.x + this.buttonPlay.frontX), (float) (this.buttonPlay.y + this.buttonPlay.frontY), 0, 0, this.buttonPlay.width, this.buttonPlay.height);
            this.spriteBatch.draw(this.buttonNetwork.getBackTexture(), (float) this.buttonNetwork.x, (float) this.buttonNetwork.y, 0, 0, this.buttonNetwork.width, this.buttonNetwork.height);
            this.spriteBatch.draw(this.buttonNetwork.texFront, (float) (this.buttonNetwork.x + this.buttonNetwork.frontX), (float) (this.buttonNetwork.y + this.buttonNetwork.frontY), 0, 0, this.buttonNetwork.width, this.buttonNetwork.height);
            this.spriteBatch.draw(this.buttonBonusMode.getBackTexture(), (float) this.buttonBonusMode.x, (float) this.buttonBonusMode.y, 0, 0, this.buttonBonusMode.width, this.buttonBonusMode.height);
            this.spriteBatch.draw(this.buttonBonusMode.texFront, (float) (this.buttonBonusMode.x + this.buttonBonusMode.frontX), (float) (this.buttonBonusMode.y + this.buttonBonusMode.frontY), 0, 0, this.buttonBonusMode.width, this.buttonBonusMode.height);
            if (States.typeOfGame == 2) {
                this.spriteBatch.draw(this.texBonusOff, (float) (this.buttonBonusMode.x + this.buttonBonusMode.frontX), (float) (this.buttonBonusMode.y + this.buttonBonusMode.frontY), 0, 0, this.texBonusOff.getWidth(), this.texBonusOff.getHeight());
            }
            if (States.skillAI[0] != 0) {
                for (byte s = 0; s < States.skillAI[0]; s = (byte) (s + 1)) {
                    this.spriteBatch.draw(this.texStar, (float) (this.gInfo.x + (this.gInfo.width * s)), (float) (this.gInfo.y - this.gInfo.width), 0, 0, this.gInfo.width, this.gInfo.width);
                }
            }
            if (States.skillAI[3] != 0 && States.numberOfPlayers == 4) {
                for (byte s2 = 0; s2 < States.skillAI[3]; s2 = (byte) (s2 + 1)) {
                    this.spriteBatch.draw(this.texStar, (float) (this.gInfo.x + (this.gInfo.width * s2)), (float) this.gInfo.height, 0, 0, this.gInfo.width, this.gInfo.width);
                }
            }
            if (States.skillAI[1] != 0 && States.numberOfPlayers == 4) {
                for (byte s3 = 0; s3 < States.skillAI[1]; s3 = (byte) (s3 + 1)) {
                    this.spriteBatch.draw(this.texStar, (float) (((States.screenWidth - this.gInfo.x) - (this.gInfo.width * s3)) - this.gInfo.width), (float) (this.gInfo.y - this.gInfo.width), 0, 0, this.gInfo.width, this.gInfo.width);
                }
            }
            if (States.skillAI[1] != 0 && States.numberOfPlayers == 2) {
                for (byte s4 = 0; s4 < States.skillAI[1]; s4 = (byte) (s4 + 1)) {
                    this.spriteBatch.draw(this.texStar, (float) (((States.screenWidth - this.gInfo.x) - (this.gInfo.width * s4)) - this.gInfo.width), (float) this.gInfo.height, 0, 0, this.gInfo.width, this.gInfo.width);
                }
            }
            if (States.skillAI[2] != 0 && States.numberOfPlayers == 4) {
                for (byte s5 = 0; s5 < States.skillAI[2]; s5 = (byte) (s5 + 1)) {
                    this.spriteBatch.draw(this.texStar, (float) (((States.screenWidth - this.gInfo.x) - (this.gInfo.width * s5)) - this.gInfo.width), (float) this.gInfo.height, 0, 0, this.gInfo.width, this.gInfo.width);
                }
            }
            this.spriteBatch.draw(this.texture, 0.0f, 0.0f, 0, 0, States.screenWidth, States.screenHeight);
            if (this.isShowWindow) {
                for (byte i = 0; i < this.namesInList; i = (byte) (i + 1)) {
                    if (this.list[i].state == 1) {
                        States.fontBig.setColor(1.0f, 1.0f, 1.0f, 0.5f);
                    } else if (this.list[i].isBackActive()) {
                        States.fontBig.setColor(0.5f, 0.5f, 1.0f, 1.0f);
                    } else {
                        States.fontBig.setColor(1.0f, 1.0f, 1.0f, 1.0f);
                    }
                    byte stars = 0;
                    if (this.list[i].state == 3) {
                        stars = 1;
                    }
                    if (this.list[i].state == 4) {
                        stars = 2;
                    }
                    if (this.list[i].state == 5) {
                        stars = 3;
                    }
                    for (byte s6 = 0; s6 < stars; s6 = (byte) (s6 + 1)) {
                        this.spriteBatch.draw(this.texStar, (float) (this.list[i].x + (this.list[i].height / 4) + (this.gInfo.width * s6)), (float) (this.list[i].y + 4), 0, 0, this.gInfo.width, this.gInfo.width);
                    }
                    States.fontBig.drawMultiLine(this.spriteBatch, this.list[i].getString(), (float) (this.list[i].x + (this.list[i].height / 2) + (this.gInfo.width * stars)), (float) (this.list[i].y + this.list[i].getTextOffsetY()), (float) this.gList.width, BitmapFont.HAlignment.LEFT);
                }
            }
        }
        if (this.pos == 1) {
            States.renderBackground(this.spriteBatch);
            this.spriteBatch.draw(this.buttonNameEdit.getBackTexture(), (float) this.buttonNameEdit.x, (float) this.buttonNameEdit.y, 0, 0, this.buttonNameEdit.width, this.buttonNameEdit.height);
            States.fontBig.setColor(1.0f, 1.0f, 1.0f, 1.0f);
            for (ButtonChar b3 : this.keyboard.buttonKb) {
                this.spriteBatch.draw(this.keyboard.getBackTexture(b3), (float) b3.x, (float) b3.y, 0, 0, b3.width, b3.height);
                States.fontBig.drawMultiLine(this.spriteBatch, String.valueOf(this.keyboard.getChar(b3)), (float) b3.x, (float) (b3.y + this.keyboard.fontY), (float) b3.width, BitmapFont.HAlignment.CENTER);
            }
            States.fontBig.draw(this.spriteBatch, this.name, (float) (this.buttonNameEdit.x + this.buttonNameEdit.frontX), (float) (this.buttonNameEdit.y + this.buttonNameEdit.frontY));
            BitmapFont.TextBounds bounds = States.fontBig.getBounds(this.name);
            States.fontBig.setColor(1.0f, 0.0f, 0.0f, 1.0f);
            States.fontBig.draw(this.spriteBatch, String.valueOf(this.bukva), ((float) (this.buttonNameEdit.x + this.buttonNameEdit.frontX)) + bounds.width, (float) (this.buttonNameEdit.y + this.buttonNameEdit.frontY));
            States.fontBig.setColor(1.0f, 1.0f, 1.0f, 1.0f);
            for (ButtonChar b4 : this.keyboard.buttonControl) {
                this.spriteBatch.draw(this.keyboard.getBackTexture(b4), (float) b4.x, (float) b4.y, 0, 0, b4.width, b4.height);
                States.fontBig.drawMultiLine(this.spriteBatch, String.valueOf(this.keyboard.getChar(b4)), (float) b4.x, (float) (b4.y + this.keyboard.fontY), (float) b4.width, BitmapFont.HAlignment.CENTER);
            }
            this.spriteBatch.draw(this.texFlags, (float) this.geomFlag.x, (float) this.geomFlag.y, this.geomFlag.width * this.keyboard.language, 0, this.geomFlag.width, this.geomFlag.height);
        }
        if (this.isLimit) {
            States.fontBig.setColor(1.0f, 1.0f, 1.0f, 1.0f);
            States.fontBig.drawMultiLine(this.spriteBatch, States.TEXT_LIMIT[States.language], 0.0f, (float) ((int) ((((float) States.screenHeight) + States.fontBig.getCapHeight()) / 2.0f)), (float) States.screenWidth, BitmapFont.HAlignment.CENTER);
        }
        if (this.isLite) {
            States.fontBig.setColor(1.0f, 1.0f, 1.0f, 1.0f);
            States.fontBig.drawMultiLine(this.spriteBatch, States.TEXT_LITE[States.language], 0.0f, (float) ((int) ((((float) States.screenHeight) + States.fontBig.getCapHeight()) / 2.0f)), (float) States.screenWidth, BitmapFont.HAlignment.CENTER);
        }
        this.spriteBatch.end();
    }

    /* JADX INFO: Multiple debug info for r0v0 com.machaon.quadratum.parts.ButtonText: [D('b' com.machaon.quadratum.parts.ButtonText), D('b' com.machaon.quadratum.parts.ButtonPic)] */
    public void dispose() {
        this.spriteBatch.dispose();
        this.texture.dispose();
        this.texBackPic.dispose();
        for (ButtonPic b : this.buttonQuant) {
            b.dispose();
        }
        for (ButtonText b2 : this.buttonNames) {
            b2.dispose();
        }
        this.buttonPlay.dispose();
        this.buttonNetwork.dispose();
        this.buttonBonusMode.dispose();
        this.texStar.dispose();
        this.texFlags.dispose();
        this.soundClick.dispose();
        this.texBonusOff.dispose();
        this.texButtonNA.dispose();
        this.texButtonA.dispose();
        this.texButtonNameNA.dispose();
        this.texButtonNameA.dispose();
    }

    public void resume() {
        dispose();
        Init();
        InitObjects();
    }

    private boolean isInputCoordsIn(Geom geom) {
        if (((float) this.inp.x) <= ((float) (geom.x + States.cameraOffsetX)) * States.aspectX || ((float) this.inp.x) >= ((float) (geom.x + geom.width + States.cameraOffsetX)) * States.aspectX || ((float) (States.displayHeight - this.inp.y)) <= ((float) (geom.y + States.cameraOffsetY)) * States.aspectY || ((float) (States.displayHeight - this.inp.y)) >= ((float) (geom.y + geom.height + States.cameraOffsetY)) * States.aspectY) {
            return false;
        }
        return true;
    }

    private void setTimer(float timeInSec) {
        this.timer = timeInSec;
    }

    private boolean isTimerEnd() {
        if (this.timer < 0.0f) {
            return true;
        }
        this.timer -= Gdx.graphics.getDeltaTime();
        return false;
    }
}
