package com.machaon.quadratum.screens;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.machaon.quadratum.IScreen;
import com.machaon.quadratum.States;
import com.machaon.quadratum.input.CommonInput;
import com.machaon.quadratum.parts.ButtonPic;
import com.machaon.quadratum.parts.Geom;
import com.machaon.quadratum.parts.Profile;
import com.machaon.quadratum.parts.Tournaments;

public class SingleTournamentSelect implements IScreen {
    private ButtonPic[] button = new ButtonPic[4];
    private Geom geomBackPic;
    private Geom[] geomKubok = new Geom[4];
    private Geom geomTextDifficulty;
    private Geom geomTextTournament;
    private CommonInput inp = new CommonInput();
    private boolean isToast = false;
    private final SpriteBatch spriteBatch = new SpriteBatch();
    private ButtonPic[] star = new ButtonPic[3];
    private final Texture texBackPic;
    private final Texture texKubok;
    private ButtonPic[] zone = new ButtonPic[4];

    public SingleTournamentSelect() {
        if (Profile.profile.difficulty == 0) {
            Profile.profile.difficulty = 2;
        }
        States.difficulty = Profile.profile.difficulty;
        Profile.popProfile(Profile.profile.numberOfProfile, Profile.profile);
        this.geomBackPic = new Geom(0, 0, 142, 142);
        if (States.screenHeight == 240 && States.screenWidth == 320) {
            this.geomTextTournament = new Geom(0, 225, 0, 0);
            this.geomTextDifficulty = new Geom(0, 57, 0, 0);
            for (byte i = 0; i < 4; i = (byte) (i + 1)) {
                this.button[i] = new ButtonPic((i * 78) + 16, 134, 54, 54, 8, -8);
                this.zone[i] = new ButtonPic((i * 78) + 6, 66, 74, 134);
                this.geomKubok[i] = new Geom((i * 78) + 29, 76, 28, 48);
            }
            for (byte i2 = 0; i2 < 3; i2 = (byte) (i2 + 1)) {
                this.star[i2] = new ButtonPic((i2 * 64) + 80, 7, 32, 32);
            }
        }
        if (States.screenHeight == 240 && States.screenWidth == 400) {
            this.geomTextTournament = new Geom(0, 225, 0, 0);
            this.geomTextDifficulty = new Geom(0, 57, 0, 0);
            for (byte i3 = 0; i3 < 4; i3 = (byte) (i3 + 1)) {
                this.button[i3] = new ButtonPic((i3 * 90) + 38, 134, 54, 54, 8, -8);
                this.zone[i3] = new ButtonPic((i3 * 90) + 25, 66, 80, 134);
                this.geomKubok[i3] = new Geom((i3 * 90) + 51, 76, 28, 48);
            }
            for (byte i4 = 0; i4 < 3; i4 = (byte) (i4 + 1)) {
                this.star[i4] = new ButtonPic((i4 * 64) + 120, 7, 32, 32);
            }
        }
        if (States.screenHeight == 320) {
            this.geomTextTournament = new Geom(0, 303, 0, 0);
            this.geomTextDifficulty = new Geom(0, 76, 0, 0);
            for (byte i5 = 0; i5 < 4; i5 = (byte) (i5 + 1)) {
                this.button[i5] = new ButtonPic((i5 * 114) + 33, 182, 74, 74, 11, -11);
                this.zone[i5] = new ButtonPic((i5 * 114) + 15, 90, Input.Keys.BUTTON_START, 180);
                this.geomKubok[i5] = new Geom((i5 * 114) + 52, Input.Keys.BUTTON_R2, 44, 64);
            }
            for (byte i6 = 0; i6 < 3; i6 = (byte) (i6 + 1)) {
                this.star[i6] = new ButtonPic((i6 * 80) + 140, 9, 42, 42);
            }
        }
        if (States.screenHeight == 480) {
            this.geomTextTournament = new Geom(0, 450, 0, 0);
            this.geomTextDifficulty = new Geom(0, 114, 0, 0);
            if (States.screenWidth == 800) {
                for (byte i7 = 0; i7 < 4; i7 = (byte) (i7 + 1)) {
                    this.button[i7] = new ButtonPic((i7 * 180) + 76, 272, (int) Input.Keys.BUTTON_START, (int) Input.Keys.BUTTON_START, 16, -16);
                    this.zone[i7] = new ButtonPic((i7 * 180) + 49, 135, 162, 270);
                    this.geomKubok[i7] = new Geom((i7 * 180) + Input.Keys.BUTTON_Z, 152, 58, 96);
                }
                for (byte i8 = 0; i8 < 3; i8 = (byte) (i8 + 1)) {
                    this.star[i8] = new ButtonPic((i8 * 120) + Input.Keys.F6, 14, 62, 62);
                }
            } else {
                for (byte i9 = 0; i9 < 4; i9 = (byte) (i9 + 1)) {
                    this.button[i9] = new ButtonPic((i9 * 184) + 97, 272, (int) Input.Keys.BUTTON_START, (int) Input.Keys.BUTTON_START, 16, -16);
                    this.zone[i9] = new ButtonPic((i9 * 184) + 70, 135, 162, 270);
                    this.geomKubok[i9] = new Geom((i9 * 184) + 122, 152, 58, 96);
                }
                for (byte i10 = 0; i10 < 3; i10 = (byte) (i10 + 1)) {
                    this.star[i10] = new ButtonPic((i10 * 120) + 276, 14, 62, 62);
                }
            }
        }
        String path = States.PATH_GRAPH + States.resolution + "/";
        this.texBackPic = new Texture(Gdx.files.internal("data/Graph/480/but_single.png"));
        this.texKubok = new Texture(Gdx.files.internal(String.valueOf(path) + "kubki.png"));
        this.button[0].SetParams(new Texture(Gdx.files.internal(String.valueOf(path) + States.TEX_BUTTON_MIDDLE_NA)), new Texture(Gdx.files.internal(String.valueOf(path) + States.TEX_BUTTON_MIDDLE_A)), new Texture(Gdx.files.internal(String.valueOf(path) + "but_I.png")), (byte) 10);
        this.button[1].SetParams(new Texture(Gdx.files.internal(String.valueOf(path) + States.TEX_BUTTON_MIDDLE_NA)), new Texture(Gdx.files.internal(String.valueOf(path) + States.TEX_BUTTON_MIDDLE_A)), new Texture(Gdx.files.internal(String.valueOf(path) + "but_II.png")), (byte) 10);
        this.button[2].SetParams(new Texture(Gdx.files.internal(String.valueOf(path) + States.TEX_BUTTON_MIDDLE_NA)), new Texture(Gdx.files.internal(String.valueOf(path) + States.TEX_BUTTON_MIDDLE_A)), new Texture(Gdx.files.internal(String.valueOf(path) + "but_III.png")), (byte) 10);
        this.button[3].SetParams(new Texture(Gdx.files.internal(String.valueOf(path) + States.TEX_BUTTON_MIDDLE_NA)), new Texture(Gdx.files.internal(String.valueOf(path) + States.TEX_BUTTON_MIDDLE_A)), new Texture(Gdx.files.internal(String.valueOf(path) + "but_IV.png")), (byte) 10);
        for (ButtonPic b : this.star) {
            b.texFront = new Texture(Gdx.files.internal(String.valueOf(path) + "star.png"));
        }
        path = States.screenWidth == 320 ? "data/Graph/240_320/" : path;
        for (ButtonPic b2 : this.zone) {
            b2.SetParams(new Texture(Gdx.files.internal(String.valueOf(path) + "zone_s_grid.png")), new Texture(Gdx.files.internal(String.valueOf(path) + "zone_s.png")), (byte) 0);
        }
        this.zone[0].setBackActive(true);
        Gdx.app.getInput().setInputProcessor(this.inp);
        this.spriteBatch.setProjectionMatrix(States.viewMatrix);
        if (States.isAntialiasing) {
            for (ButtonPic b3 : this.button) {
                b3.texBackAct.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
                b3.texBackNonAct.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
                b3.texFront.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
            }
            for (ButtonPic b4 : this.zone) {
                b4.texBackAct.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
                b4.texBackNonAct.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
            }
            for (ButtonPic b5 : this.star) {
                b5.texFront.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
            }
            this.texKubok.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        }
        this.texBackPic.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        States.numberOfColors = 7;
    }

    public void update() {
        if (this.inp.isBack) {
            if (Profile.profile.settingSounds) {
                States.click.play();
            }
            States.state = 4;
        }
        byte b = this.inp.buttonUp;
        this.inp.getClass();
        if (b == 50) {
            for (int i = 0; i < 4; i++) {
                if (this.zone[i].isBackActive() && isInputCoordsIn(this.button[i])) {
                    States.numberOfPlayers = 4;
                    for (byte s = 0; s < States.numberOfPlayers; s = (byte) (s + 1)) {
                        States.skillAI[s] = States.difficulty;
                    }
                    States.skillAI[0] = 0;
                    States.playerName[0] = Profile.getProfileName(Profile.profile.numberOfProfile);
                    Profile.isActiveProfile[0] = true;
                    for (byte p = 1; p < 4; p = (byte) (p + 1)) {
                        Profile.isActiveProfile[p] = false;
                    }
                    Tournaments.setTournament((byte) (i + 1));
                    if (Profile.profile.settingSounds) {
                        States.click.play();
                    }
                    Profile.profile.difficulty = States.difficulty;
                    Profile.pushProfile(Profile.profile.numberOfProfile, Profile.profile);
                    Profile.popProfile(Profile.profile.numberOfProfile, Profile.pp[0]);
                    States.namingAI();
                    States.resetBonuses();
                    States.typeOfGame = 1;
                    States.state = this.button[i].getState();
                }
            }
        }
        byte b2 = this.inp.buttonDown;
        this.inp.getClass();
        if (b2 == 50) {
            for (byte i2 = 0; i2 < 3; i2 = (byte) (i2 + 1)) {
                if (isInputCoordsIn(this.star[i2])) {
                    States.difficulty = (byte) (i2 + 1);
                    for (byte s2 = 0; s2 < States.numberOfPlayers; s2 = (byte) (s2 + 1)) {
                        if (States.skillAI[s2] != 0) {
                            States.skillAI[s2] = States.difficulty;
                        }
                    }
                }
            }
            for (int i3 = 0; i3 < 4; i3++) {
                if (this.zone[i3].isBackActive()) {
                    this.button[i3].setBackActive(isInputCoordsIn(this.button[i3]));
                } else if (isInputCoordsIn(this.zone[i3])) {
                    this.isToast = true;
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.g2d.SpriteBatch.draw(com.badlogic.gdx.graphics.Texture, float, float, float, float, int, int, int, int, boolean, boolean):void
     arg types: [com.badlogic.gdx.graphics.Texture, float, int, float, float, int, int, int, int, int, int]
     candidates:
      com.badlogic.gdx.graphics.g2d.SpriteBatch.draw(com.badlogic.gdx.graphics.g2d.TextureRegion, float, float, float, float, float, float, float, float, float, boolean):void
      com.badlogic.gdx.graphics.g2d.SpriteBatch.draw(com.badlogic.gdx.graphics.Texture, float, float, float, float, int, int, int, int, boolean, boolean):void */
    public void render(Application app) {
        app.getGraphics().getGL10().glClearColor(0.85f, 0.85f, 0.85f, 1.0f);
        app.getGraphics().getGL10().glClear(16384);
        this.spriteBatch.begin();
        this.spriteBatch.enableBlending();
        States.fontBig.setColor(1.0f, 1.0f, 1.0f, 1.0f);
        this.spriteBatch.draw(this.texBackPic, (float) ((States.screenWidth / 2) - ((States.screenHeight * 1) / 2)), 0.0f, (float) States.screenHeight, (float) States.screenHeight, this.geomBackPic.x, this.geomBackPic.y, this.geomBackPic.width, this.geomBackPic.height, false, false);
        States.renderBackground(this.spriteBatch);
        for (ButtonPic b : this.button) {
            this.spriteBatch.draw(b.getBackTexture(), (float) b.x, (float) b.y, 0, 0, b.width, b.height);
            this.spriteBatch.draw(b.texFront, (float) (b.x + b.frontX), (float) (b.y + b.frontY), 0, 0, b.width, b.height);
        }
        for (byte i = 0; i < 4; i = (byte) (i + 1)) {
            this.spriteBatch.draw(this.texKubok, (float) this.geomKubok[i].x, (float) this.geomKubok[i].y, this.geomKubok[i].width * Profile.profile.resultTournament[i], 0, this.geomKubok[i].width, this.geomKubok[i].height);
        }
        for (ButtonPic b2 : this.zone) {
            this.spriteBatch.draw(b2.getBackTexture(), (float) b2.x, (float) b2.y, 0, 0, b2.width, b2.height);
        }
        States.fontBig.drawMultiLine(this.spriteBatch, States.TEXT_TOURNAMENT[States.language], 0.0f, (float) this.geomTextTournament.y, (float) States.screenWidth, BitmapFont.HAlignment.CENTER);
        States.fontBig.drawMultiLine(this.spriteBatch, States.TEXT_DIFFICULTY[States.language], 0.0f, (float) this.geomTextDifficulty.y, (float) States.screenWidth, BitmapFont.HAlignment.CENTER);
        byte i2 = 0;
        while (i2 < 3) {
            this.spriteBatch.draw(this.star[i2].texFront, (float) this.star[i2].x, (float) this.star[i2].y, (this.star[i2].width * (i2 < States.difficulty ? 0 : 1)) + 0, 0, this.star[i2].width, this.star[i2].height);
            i2 = (byte) (i2 + 1);
        }
        if (this.isToast) {
            States.fontSmall.setColor(1.0f, 1.0f, 1.0f, 1.0f);
            States.fontSmall.drawMultiLine(this.spriteBatch, States.TEXT_TOURNAMENTS[States.language], (float) this.zone[1].x, ((float) (this.zone[0].y + (this.zone[0].height / 2))) + States.fontBig.getCapHeight(), (float) ((this.zone[3].x + this.zone[3].width) - this.zone[1].x), BitmapFont.HAlignment.CENTER);
        }
        this.spriteBatch.end();
    }

    public void dispose() {
        this.spriteBatch.dispose();
        for (ButtonPic b : this.button) {
            b.dispose();
        }
        for (ButtonPic b2 : this.zone) {
            b2.dispose();
        }
        this.texBackPic.dispose();
        for (ButtonPic b3 : this.star) {
            b3.dispose();
        }
        this.texKubok.dispose();
    }

    public void resume() {
    }

    private boolean isInputCoordsIn(Geom geom) {
        if (((float) this.inp.x) <= ((float) (geom.x + States.cameraOffsetX)) * States.aspectX || ((float) this.inp.x) >= ((float) (geom.x + geom.width + States.cameraOffsetX)) * States.aspectX || ((float) (States.displayHeight - this.inp.y)) <= ((float) (geom.y + States.cameraOffsetY)) * States.aspectY || ((float) (States.displayHeight - this.inp.y)) >= ((float) (geom.y + geom.height + States.cameraOffsetY)) * States.aspectY) {
            return false;
        }
        return true;
    }
}
