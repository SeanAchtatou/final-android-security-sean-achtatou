package com.machaon.quadratum.screens;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.machaon.quadratum.IScreen;
import com.machaon.quadratum.States;
import com.machaon.quadratum.input.CommonInput;
import com.machaon.quadratum.parts.Geom;
import com.machaon.quadratum.parts.Profile;

public class Credits implements IScreen {
    private Geom geomBackPic;
    private Geom geomEmail;
    private Geom geomHttp;
    private CommonInput inp;
    private boolean isActiveEmail;
    private boolean isActiveHttp;
    private int logoHeight;
    private int logoWidth;
    private final SpriteBatch spriteBatch;
    private final Texture texBackPic;
    private Texture texLogo;

    public Credits() {
        this.isActiveHttp = false;
        this.isActiveEmail = false;
        this.logoWidth = 0;
        this.logoHeight = 0;
        this.spriteBatch = new SpriteBatch();
        this.texBackPic = new Texture(Gdx.files.internal("data/Graph/q.png"));
        this.logoHeight = (int) (((float) States.screenHeight) / 3.3f);
        this.logoWidth = (this.logoHeight * 300) / 134;
        this.texLogo = States.reduceImageToTexture("data/Graph/cr_logo.png", this.logoWidth, this.logoHeight);
        this.geomBackPic = new Geom(1, 1, Input.Keys.F11, Input.Keys.F11);
        if (States.screenHeight == 240) {
            this.geomHttp = new Geom(60, 48, 280, 16);
            this.geomEmail = new Geom(60, 27, 280, 16);
        }
        if (States.screenHeight == 320) {
            this.geomHttp = new Geom(Input.Keys.BUTTON_MODE, 62, Input.Keys.F7, 20);
            this.geomEmail = new Geom(Input.Keys.BUTTON_MODE, 32, Input.Keys.F7, 20);
        }
        if (States.screenHeight == 480) {
            this.geomHttp = new Geom(160, 98, 500, 25);
            this.geomEmail = new Geom(160, 50, 500, 25);
        }
        this.inp = new CommonInput();
        Gdx.app.getInput().setInputProcessor(this.inp);
        this.spriteBatch.setProjectionMatrix(States.viewMatrix);
        if (States.isAntialiasing) {
            this.texLogo.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        }
        this.texBackPic.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
    }

    public void update() {
        if (this.inp.isBack) {
            if (Profile.profile.settingSounds) {
                States.click.play();
            }
            States.state = 3;
        }
        byte b = this.inp.buttonUp;
        this.inp.getClass();
        if (b == 50) {
            this.isActiveHttp = false;
            this.isActiveEmail = false;
            if (isInputCoordsIn(this.geomHttp)) {
                Gdx.app.gotoHttp();
            }
            if (isInputCoordsIn(this.geomEmail)) {
                Gdx.app.sendEmail();
            }
            this.inp.x = 0;
        }
        byte b2 = this.inp.buttonDown;
        this.inp.getClass();
        if (b2 == 50) {
            this.isActiveHttp = isInputCoordsIn(this.geomHttp);
            this.isActiveEmail = isInputCoordsIn(this.geomEmail);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.g2d.SpriteBatch.draw(com.badlogic.gdx.graphics.Texture, float, float, float, float, int, int, int, int, boolean, boolean):void
     arg types: [com.badlogic.gdx.graphics.Texture, float, int, float, float, int, int, int, int, int, int]
     candidates:
      com.badlogic.gdx.graphics.g2d.SpriteBatch.draw(com.badlogic.gdx.graphics.g2d.TextureRegion, float, float, float, float, float, float, float, float, float, boolean):void
      com.badlogic.gdx.graphics.g2d.SpriteBatch.draw(com.badlogic.gdx.graphics.Texture, float, float, float, float, int, int, int, int, boolean, boolean):void */
    public void render(Application app) {
        app.getGraphics().getGL10().glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
        app.getGraphics().getGL10().glClear(16384);
        this.spriteBatch.begin();
        this.spriteBatch.enableBlending();
        this.spriteBatch.draw(this.texBackPic, (float) ((States.screenWidth / 2) - (States.screenHeight / 2)), 0.0f, (float) States.screenHeight, (float) States.screenHeight, this.geomBackPic.x, this.geomBackPic.y, this.geomBackPic.width, this.geomBackPic.height, false, false);
        States.renderBackground(this.spriteBatch);
        this.spriteBatch.draw(this.texLogo, (float) ((States.screenWidth - this.logoWidth) / 2), (float) ((States.screenHeight / 2) + (this.logoHeight / 3)), 0, 0, this.logoWidth, this.logoHeight);
        States.fontSmall.setColor(1.0f, 1.0f, 1.0f, 1.0f);
        States.fontSmall.drawMultiLine(this.spriteBatch, States.TEXT_SERGEY[States.language], 0.0f, (float) ((States.screenHeight / 2) + ((States.screenHeight * 15) / 320)), (float) States.screenWidth, BitmapFont.HAlignment.CENTER);
        States.fontSmall.drawMultiLine(this.spriteBatch, States.TEXT_ANDREY[States.language], 0.0f, (float) ((States.screenHeight / 2) - ((States.screenHeight * 15) / 320)), (float) States.screenWidth, BitmapFont.HAlignment.CENTER);
        if (this.isActiveHttp) {
            States.fontSmall.setColor(0.5f, 0.5f, 1.0f, 1.0f);
        } else {
            States.fontSmall.setColor(0.8f, 0.8f, 1.0f, 1.0f);
        }
        States.fontSmall.drawMultiLine(this.spriteBatch, "http://machaonstudio.blogspot.com", 0.0f, (float) (States.screenHeight / 4), (float) States.screenWidth, BitmapFont.HAlignment.CENTER);
        if (this.isActiveEmail) {
            States.fontSmall.setColor(0.5f, 0.5f, 1.0f, 1.0f);
        } else {
            States.fontSmall.setColor(0.8f, 0.8f, 1.0f, 1.0f);
        }
        States.fontSmall.drawMultiLine(this.spriteBatch, "email: machaon.studio@gmail.com", 0.0f, (float) ((States.screenHeight / 4) - ((States.screenHeight * 30) / 320)), (float) States.screenWidth, BitmapFont.HAlignment.CENTER);
        States.fontSmall.setColor(0.3f, 0.3f, 0.3f, 1.0f);
        States.fontSmall.drawMultiLine(this.spriteBatch, "2011,  0.9862 beta ", ((((float) States.displayWidth) / States.aspectX) - ((float) States.screenWidth)) / 2.0f, States.fontSmall.getCapHeight() * 2.0f, (float) States.screenWidth, BitmapFont.HAlignment.RIGHT);
        this.spriteBatch.end();
    }

    public void dispose() {
        this.spriteBatch.dispose();
        this.texBackPic.dispose();
        this.texLogo.dispose();
    }

    public void resume() {
        this.texLogo.dispose();
        this.texLogo = States.reduceImageToTexture("data/Graph/cr_logo.png", this.logoWidth, this.logoHeight);
    }

    private boolean isInputCoordsIn(Geom geom) {
        if (((float) this.inp.x) <= ((float) (geom.x + States.cameraOffsetX)) * States.aspectX || ((float) this.inp.x) >= ((float) (geom.x + geom.width + States.cameraOffsetX)) * States.aspectX || ((float) (States.displayHeight - this.inp.y)) <= ((float) (geom.y + States.cameraOffsetY)) * States.aspectY || ((float) (States.displayHeight - this.inp.y)) >= ((float) (geom.y + geom.height + States.cameraOffsetY)) * States.aspectY) {
            return false;
        }
        return true;
    }
}
