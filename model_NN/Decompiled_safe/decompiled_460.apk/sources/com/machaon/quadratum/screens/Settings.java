package com.machaon.quadratum.screens;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.machaon.quadratum.IScreen;
import com.machaon.quadratum.States;
import com.machaon.quadratum.input.CommonInput;
import com.machaon.quadratum.parts.ButtonPic;
import com.machaon.quadratum.parts.Geom;
import com.machaon.quadratum.parts.Profile;

public class Settings implements IScreen {
    public static final byte SETTING_MUSIC = 2;
    public static final byte SETTING_SKIN = 4;
    public static final byte SETTING_SOUNDS = 1;
    public static final byte SETTING_VIBRATE = 3;
    private ButtonPic[] button = new ButtonPic[5];
    private Geom geomBackPic = new Geom(0, 0, 146, 146);
    private CommonInput inp = new CommonInput();
    boolean isSkipCadr = false;
    private boolean isToast;
    private final SpriteBatch spriteBatch = new SpriteBatch();
    private final Texture texBackPic;
    private final Texture texOff;
    private float timer;

    public Settings(byte sceneToReturn) {
        if (States.screenHeight == 240 && States.screenWidth == 320) {
            this.button[0] = new ButtonPic(14, (int) Input.Keys.CONTROL_RIGHT, 84, 84, 7, -5);
            this.button[1] = new ButtonPic(118, (int) Input.Keys.CONTROL_RIGHT, 84, 84, 5, -5);
            this.button[2] = new ButtonPic(14, 26, 84, 84, 6, -5);
            this.button[4] = new ButtonPic(118, 26, 84, 84, 6, -6);
            this.button[3] = new ButtonPic(222, 26, 84, 84, 6, -6);
        }
        if (States.screenHeight == 240 && States.screenWidth == 400) {
            this.button[0] = new ButtonPic(50, (int) Input.Keys.END, 84, 84, 7, -5);
            this.button[1] = new ButtonPic(158, (int) Input.Keys.END, 84, 84, 5, -5);
            this.button[2] = new ButtonPic(50, 24, 84, 84, 6, -5);
            this.button[4] = new ButtonPic(158, 24, 84, 84, 6, -6);
            this.button[3] = new ButtonPic(266, 24, 84, 84, 6, -6);
        }
        if (States.screenHeight == 320) {
            this.button[0] = new ButtonPic(42, 175, (int) Input.Keys.FORWARD_DEL, (int) Input.Keys.FORWARD_DEL, 9, -8);
            this.button[1] = new ButtonPic(184, 175, (int) Input.Keys.FORWARD_DEL, (int) Input.Keys.FORWARD_DEL, 8, -6);
            this.button[2] = new ButtonPic(42, 33, (int) Input.Keys.FORWARD_DEL, (int) Input.Keys.FORWARD_DEL, 8, -6);
            this.button[4] = new ButtonPic(184, 33, (int) Input.Keys.FORWARD_DEL, (int) Input.Keys.FORWARD_DEL, 8, -8);
            this.button[3] = new ButtonPic(326, 33, (int) Input.Keys.FORWARD_DEL, (int) Input.Keys.FORWARD_DEL, 8, -9);
        }
        if (States.screenHeight == 480) {
            if (States.screenWidth == 800) {
                this.button[0] = new ButtonPic((int) Input.Keys.BUTTON_THUMBR, 259, 162, 162, 10, -8);
                this.button[1] = new ButtonPic(319, 259, 162, 162, 10, -8);
                this.button[2] = new ButtonPic((int) Input.Keys.BUTTON_THUMBR, 47, 162, 162, 10, -8);
                this.button[4] = new ButtonPic(319, 47, 162, 162, 11, -10);
                this.button[3] = new ButtonPic(531, 47, 162, 162, 10, -10);
            } else {
                this.button[0] = new ButtonPic(134, 259, 162, 162, 10, -8);
                this.button[1] = new ButtonPic(348, 259, 162, 162, 10, -8);
                this.button[2] = new ButtonPic(134, 47, 162, 162, 10, -8);
                this.button[4] = new ButtonPic(348, 47, 162, 162, 11, -10);
                this.button[3] = new ButtonPic(558, 47, 162, 162, 10, -10);
            }
        }
        this.texBackPic = new Texture(Gdx.files.internal("data/Graph/480/settings.png"));
        this.texOff = new Texture(Gdx.files.internal(String.valueOf(States.path) + "off.png"));
        this.button[0].SetParams(new Texture(Gdx.files.internal(String.valueOf(States.path) + "but_na.png")), new Texture(Gdx.files.internal(String.valueOf(States.path) + "but_a.png")), new Texture(Gdx.files.internal(String.valueOf(States.path) + "sounds.png")), (byte) 1);
        this.button[1].SetParams(new Texture(Gdx.files.internal(String.valueOf(States.path) + "but_na.png")), new Texture(Gdx.files.internal(String.valueOf(States.path) + "but_a.png")), new Texture(Gdx.files.internal(String.valueOf(States.path) + "music.png")), (byte) 2);
        this.button[2].SetParams(new Texture(Gdx.files.internal(String.valueOf(States.path) + "but_na.png")), new Texture(Gdx.files.internal(String.valueOf(States.path) + "but_a.png")), new Texture(Gdx.files.internal(String.valueOf(States.path) + "vibro.png")), (byte) 3);
        this.button[3].SetParams(new Texture(Gdx.files.internal(String.valueOf(States.path) + "but_na.png")), new Texture(Gdx.files.internal(String.valueOf(States.path) + "but_a.png")), new Texture(Gdx.files.internal(String.valueOf(States.path) + "skins.png")), (byte) 4);
        this.button[4].SetParams(new Texture(Gdx.files.internal(String.valueOf(States.path) + "but_na.png")), new Texture(Gdx.files.internal(String.valueOf(States.path) + "but_a.png")), new Texture(Gdx.files.internal(String.valueOf(States.path) + "turbo.png")), (byte) 4);
        Gdx.app.getInput().setInputProcessor(this.inp);
        this.button[0].isOff = !Profile.profile.settingSounds;
        this.button[1].isOff = !Profile.profile.settingMusic;
        this.button[2].isOff = !Profile.profile.settingVibrate;
        this.spriteBatch.setProjectionMatrix(States.viewMatrix);
        if (States.isAntialiasing) {
            for (ButtonPic b : this.button) {
                b.texBackAct.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
                b.texBackNonAct.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
                b.texFront.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
            }
            this.texOff.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        }
        this.texBackPic.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
    }

    public void update() {
        boolean b;
        boolean b2;
        if (isTimerEnd()) {
            this.isToast = false;
        }
        if (this.inp.isBack) {
            if (this.isSkipCadr && States.state != 3) {
                if (Profile.profile.settingSounds) {
                    States.click.play();
                }
                Profile.pushProfile(Profile.profile.numberOfProfile, Profile.profile);
                States.state = 3;
            }
            this.isSkipCadr = true;
            return;
        }
        byte b3 = this.inp.buttonUp;
        this.inp.getClass();
        if (b3 == 50) {
            for (int i = 0; i < 5; i++) {
                if (isInputCoordsIn(this.button[i])) {
                    boolean b4 = true;
                    if (this.button[i].getState() == 1) {
                        if (Profile.profile.settingSounds) {
                            b4 = false;
                        } else {
                            b4 = true;
                        }
                        Profile.profile.settingSounds = b4;
                    }
                    if (this.button[i].getState() == 2) {
                        if (Profile.profile.settingMusic) {
                            b2 = false;
                        } else {
                            b2 = true;
                        }
                        Profile.profile.settingMusic = b4;
                        if (Profile.profile.settingMusic) {
                            States.music.play();
                        } else {
                            States.music.pause();
                        }
                    }
                    if (this.button[i].getState() == 3) {
                        if (Profile.profile.settingVibrate) {
                            b = false;
                        } else {
                            b = true;
                        }
                        Profile.profile.settingVibrate = b4;
                        if (Profile.profile.settingVibrate) {
                            Gdx.input.vibrate(80);
                        }
                    }
                    if (this.button[i].getState() == 4) {
                        this.isToast = true;
                        setTimer(4.0f);
                    }
                    this.button[i].isOff = !b4;
                    if (Profile.profile.settingSounds) {
                        States.click.play();
                    }
                    this.inp.x = 0;
                }
            }
        }
        byte b5 = this.inp.buttonDown;
        this.inp.getClass();
        if (b5 == 50) {
            for (int i2 = 0; i2 < 5; i2++) {
                this.button[i2].setBackActive(isInputCoordsIn(this.button[i2]));
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.g2d.SpriteBatch.draw(com.badlogic.gdx.graphics.Texture, float, float, float, float, int, int, int, int, boolean, boolean):void
     arg types: [com.badlogic.gdx.graphics.Texture, float, int, float, float, int, int, int, int, int, int]
     candidates:
      com.badlogic.gdx.graphics.g2d.SpriteBatch.draw(com.badlogic.gdx.graphics.g2d.TextureRegion, float, float, float, float, float, float, float, float, float, boolean):void
      com.badlogic.gdx.graphics.g2d.SpriteBatch.draw(com.badlogic.gdx.graphics.Texture, float, float, float, float, int, int, int, int, boolean, boolean):void */
    public void render(Application app) {
        app.getGraphics().getGL10().glClearColor(0.85f, 0.85f, 0.85f, 1.0f);
        app.getGraphics().getGL10().glClear(16384);
        app.getGraphics().getGL10().glEnable(GL10.GL_MULTISAMPLE);
        this.spriteBatch.begin();
        this.spriteBatch.enableBlending();
        this.spriteBatch.draw(this.texBackPic, (float) ((States.screenWidth / 2) - ((States.screenHeight * 1) / 2)), 0.0f, (float) States.screenHeight, (float) States.screenHeight, this.geomBackPic.x, this.geomBackPic.y, this.geomBackPic.width, this.geomBackPic.height, false, false);
        States.renderBackground(this.spriteBatch);
        for (ButtonPic b : this.button) {
            this.spriteBatch.draw(b.getBackTexture(), (float) b.x, (float) b.y, 0, 0, b.width, b.height);
            this.spriteBatch.draw(b.texFront, (float) (b.x + b.frontX), (float) (b.y + b.frontY), 0, 0, b.width, b.height);
            if (b.isOff) {
                this.spriteBatch.draw(this.texOff, (float) b.x, (float) b.y, 0, 0, b.width, b.height);
            }
        }
        if (this.isToast) {
            States.fontSmall.setColor(1.0f, 1.0f, 1.0f, 1.0f);
            States.fontSmall.drawMultiLine(this.spriteBatch, States.TEXT_SKINS[States.language], 0.0f, States.fontSmall.getCapHeight() * 1.7f, (float) States.screenWidth, BitmapFont.HAlignment.CENTER);
        }
        if (this.inp.isBack) {
            States.renderClock(this.spriteBatch);
        }
        this.spriteBatch.end();
    }

    public void dispose() {
        this.spriteBatch.dispose();
        for (ButtonPic b : this.button) {
            b.dispose();
        }
        this.texBackPic.dispose();
        this.texOff.dispose();
    }

    public void resume() {
    }

    private boolean isInputCoordsIn(Geom geom) {
        if (((float) this.inp.x) <= ((float) (geom.x + States.cameraOffsetX)) * States.aspectX || ((float) this.inp.x) >= ((float) (geom.x + geom.width + States.cameraOffsetX)) * States.aspectX || ((float) (States.displayHeight - this.inp.y)) <= ((float) (geom.y + States.cameraOffsetY)) * States.aspectY || ((float) (States.displayHeight - this.inp.y)) >= ((float) (geom.y + geom.height + States.cameraOffsetY)) * States.aspectY) {
            return false;
        }
        return true;
    }

    private void setTimer(float timeInSec) {
        this.timer = timeInSec;
    }

    private boolean isTimerEnd() {
        if (this.timer < 0.0f) {
            return true;
        }
        this.timer -= Gdx.graphics.getDeltaTime();
        return false;
    }
}
