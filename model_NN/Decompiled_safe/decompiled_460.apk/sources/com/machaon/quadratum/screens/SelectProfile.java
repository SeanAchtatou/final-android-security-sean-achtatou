package com.machaon.quadratum.screens;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.machaon.quadratum.IScreen;
import com.machaon.quadratum.States;
import com.machaon.quadratum.input.SelectProfileInput;
import com.machaon.quadratum.parts.ButtonChar;
import com.machaon.quadratum.parts.ButtonPic;
import com.machaon.quadratum.parts.ButtonText;
import com.machaon.quadratum.parts.Geom;
import com.machaon.quadratum.parts.Keyboard;
import com.machaon.quadratum.parts.Profile;

public class SelectProfile implements IScreen {
    private final byte POS_KEYBOARD;
    private final byte POS_PROFILE;
    private char bukva;
    private ButtonPic buttonDelete;
    private ButtonText[] buttonName;
    private ButtonPic buttonNameEdit;
    private byte editedName;
    private Geom geomFlag;
    private SelectProfileInput inp;
    private boolean isEnd;
    private boolean isSelectedName;
    private Keyboard keyboard;
    private String name;
    private byte pos;
    private final SpriteBatch spriteBatch;
    private final Texture texFlags;
    private float timer;
    private float timer_delete;

    public SelectProfile(States states) {
        this.POS_PROFILE = 0;
        this.POS_KEYBOARD = 1;
        this.timer_delete = 0.0f;
        this.inp = new SelectProfileInput();
        this.isEnd = false;
        this.spriteBatch = new SpriteBatch();
        this.buttonName = new ButtonText[6];
        this.keyboard = new Keyboard();
        this.timer_delete = 0.0f;
        this.pos = 0;
        this.bukva = Keyboard.CHAR_SPACE;
        this.name = "";
        this.isSelectedName = false;
        Profile.pushProfile(Profile.profile.numberOfProfile, Profile.profile);
        if (States.screenHeight == 240 && States.screenWidth == 320) {
            for (int i = 0; i < 6; i++) {
                this.buttonName[i] = new ButtonText(37, (i * 37) + 11, Input.Keys.F2, 30, 23);
            }
            this.buttonDelete = new ButtonPic(0, 0, 26, 26, 1, 0);
            this.buttonNameEdit = new ButtonPic(37, 161, (int) Input.Keys.F2, 30, 12, 22);
            this.geomFlag = new Geom(285, 3, 29, 30);
        }
        if (States.screenHeight == 240 && States.screenWidth == 400) {
            for (int i2 = 0; i2 < 6; i2++) {
                this.buttonName[i2] = new ButtonText(77, (i2 * 37) + 11, Input.Keys.F2, 30, 23);
            }
            this.buttonDelete = new ButtonPic(0, 0, 26, 26, 1, 0);
            this.buttonNameEdit = new ButtonPic(77, 161, (int) Input.Keys.F2, 30, 12, 22);
            this.geomFlag = new Geom(354, 11, 29, 30);
        }
        if (States.screenHeight == 320) {
            for (int i3 = 0; i3 < 6; i3++) {
                this.buttonName[i3] = new ButtonText(77, (i3 * 50) + 15, 326, 40, 28);
            }
            this.buttonDelete = new ButtonPic(0, 0, 35, 35, 1, 0);
            this.buttonNameEdit = new ButtonPic(77, 215, 326, 40, 15, 30);
            this.geomFlag = new Geom(430, 8, 39, 39);
        }
        if (States.screenHeight == 480) {
            if (States.screenWidth == 800) {
                for (int i4 = 0; i4 < 6; i4++) {
                    this.buttonName[i4] = new ButtonText(155, (i4 * 75) + 22, 490, 58, 39);
                }
                this.buttonDelete = new ButtonPic(0, 1, 52, 52, 1, 0);
                this.buttonNameEdit = new ButtonPic(155, 322, 490, 58, 24, 39);
                this.geomFlag = new Geom(708, 23, 56, 59);
            } else {
                for (int i5 = 0; i5 < 6; i5++) {
                    this.buttonName[i5] = new ButtonText(182, (i5 * 75) + 22, 490, 58, 39);
                }
                this.buttonDelete = new ButtonPic(0, 1, 52, 52, 1, 0);
                this.buttonNameEdit = new ButtonPic(182, 322, 490, 58, 24, 39);
                this.geomFlag = new Geom(756, 23, 56, 59);
            }
        }
        String path = States.PATH_GRAPH + States.resolution + "/";
        this.texFlags = new Texture(Gdx.files.internal(String.valueOf(path) + States.TEX_FLAGS));
        for (ButtonText b : this.buttonName) {
            b.SetParams("", new Texture(Gdx.files.internal(String.valueOf(path) + "but_name_na.png")), new Texture(Gdx.files.internal(String.valueOf(path) + "but_name_a.png")));
        }
        this.buttonNameEdit.SetParams(new Texture(Gdx.files.internal(String.valueOf(path) + "but_name_na.png")), new Texture(Gdx.files.internal(String.valueOf(path) + "but_name_a.png")), (byte) 1);
        this.buttonDelete.SetParams(new Texture(Gdx.files.internal(String.valueOf(path) + "but_kb_na.png")), new Texture(Gdx.files.internal(String.valueOf(path) + "but_kb_a.png")), new Texture(Gdx.files.internal(String.valueOf(path) + "delete.png")), (byte) 1);
        Gdx.app.getInput().setInputProcessor(this.inp);
        for (byte i6 = 0; i6 < 6; i6 = (byte) (i6 + 1)) {
            this.buttonName[i6].setString(Profile.getProfileName(i6));
        }
        this.spriteBatch.setProjectionMatrix(States.viewMatrix);
        if (States.isAntialiasing) {
            this.buttonDelete.texBackAct.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
            this.buttonDelete.texBackNonAct.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
            this.buttonDelete.texFront.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
            this.buttonNameEdit.texBackAct.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
            this.buttonNameEdit.texBackNonAct.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
            this.texFlags.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        }
    }

    public void update() {
        if (this.isEnd) {
            if (Profile.profile.settingSounds) {
                States.click.play();
            }
            States.state = 3;
            return;
        }
        if (this.inp.isBack) {
            Profile.setCurrentProfile(Profile.profile.numberOfProfile);
            this.isEnd = true;
        }
        if (!isTimerEnd()) {
            this.inp.x = 0;
        } else if (this.pos == 0) {
            updateProfile();
        } else {
            updateKeyboard();
        }
    }

    public void updateProfile() {
        byte b = this.inp.buttonDown;
        this.inp.getClass();
        if (b == 50) {
            for (byte b2 = 0; b2 < 6; b2 = (byte) (b2 + 1)) {
                if (isInputCoordsIn(this.buttonName[b2]) && this.buttonName[b2].isBackActive() && this.buttonName[b2].getString() != "" && this.isSelectedName && this.buttonDelete.y == this.buttonName[b2].y) {
                    States.playerName[0] = this.buttonName[b2].getString();
                    Profile.setCurrentProfile(b2);
                    States.language = Profile.profile.language;
                    this.isEnd = true;
                }
                this.buttonName[b2].setBackActive(isInputCoordsIn(this.buttonName[b2]));
            }
            this.buttonDelete.setBackActive(isInputCoordsIn(this.buttonDelete));
            if (isInputCoordsIn(this.buttonDelete)) {
                this.timer_delete += Gdx.graphics.getDeltaTime() / 1.2f;
            } else {
                this.timer_delete = 0.0f;
            }
        }
        byte b3 = this.inp.buttonUp;
        this.inp.getClass();
        if (b3 == 50) {
            byte i = 0;
            while (i < 6) {
                if (!isInputCoordsIn(this.buttonName[i]) || this.buttonName[i].getString() == "") {
                    if (isInputCoordsIn(this.buttonName[i]) && this.buttonName[i].getString() == "") {
                        this.buttonDelete.x = 0;
                        this.editedName = i;
                        setTimer(0.5f);
                        this.pos = 1;
                        this.name = "";
                    }
                    if (!isInputCoordsIn(this.buttonName[i])) {
                        this.buttonDelete.x = 0;
                        this.timer_delete = 0.0f;
                        this.isSelectedName = false;
                    }
                    i = (byte) (i + 1);
                } else {
                    this.buttonDelete.x = States.screenWidth - ((this.buttonName[i].x + this.buttonDelete.width) / 2);
                    this.buttonDelete.y = this.buttonName[i].y;
                    this.isSelectedName = true;
                    return;
                }
            }
        } else if (this.timer_delete > 1.0f) {
            for (byte b4 = 0; b4 < 6; b4 = (byte) (b4 + 1)) {
                if (this.buttonDelete.y == this.buttonName[b4].y) {
                    this.buttonName[b4].setString("");
                    Profile.setProfileName(b4, "");
                    Profile.ResetProfile(b4);
                    FileHandle fin = Gdx.files.external("Quadratum/" + ((int) b4) + ".sav");
                    if (fin.exists()) {
                        fin.delete();
                    }
                }
            }
            this.buttonDelete.x = 0;
        }
    }

    public void updateKeyboard() {
        byte b = this.inp.buttonUp;
        this.inp.getClass();
        if (b == 50) {
            if (isInputCoordsIn(this.geomFlag)) {
                if (Profile.profile.settingSounds) {
                    States.click.play();
                }
                Keyboard keyboard2 = this.keyboard;
                keyboard2.language = (byte) (keyboard2.language + 1);
                if (this.keyboard.language > 1) {
                    this.keyboard.language = 0;
                }
                this.keyboard.setLanguage(this.keyboard.language);
                this.keyboard.setState((byte) 0);
            }
            if (isInputCoordsIn(this.buttonNameEdit) || isInputCoordsIn(this.keyboard.buttonControl[4])) {
                this.pos = 0;
                this.isSelectedName = true;
                this.buttonName[this.editedName].setBackActive(true);
                this.buttonDelete.y = this.buttonName[this.editedName].y;
                this.buttonDelete.x = States.screenWidth - ((this.buttonName[this.editedName].x + this.buttonDelete.width) / 2);
                boolean isName = false;
                for (byte i = 0; i < this.name.length(); i = (byte) (i + 1)) {
                    if (this.name.charAt(i) != ' ') {
                        isName = true;
                    }
                }
                if (!isName) {
                    Profile.setProfileName(this.editedName, "");
                    this.name = "";
                    this.buttonDelete.x = 0;
                } else {
                    Profile.setProfileName(this.editedName, this.name);
                }
                this.buttonName[this.editedName].setString(this.name);
                setTimer(0.5f);
            }
            for (ButtonChar b2 : this.keyboard.buttonKb) {
                if (isInputCoordsIn(b2) && States.fontBig.getBounds(this.name).width < ((float) ((States.screenWidth * 125) / 480))) {
                    this.name = String.valueOf(this.name) + this.keyboard.getChar(b2);
                }
            }
            for (ButtonChar b3 : this.keyboard.buttonControl) {
                if (isInputCoordsIn(b3)) {
                    if (this.keyboard.getChar(b3) == '~') {
                        this.keyboard.setState((byte) 3);
                    }
                    if (this.keyboard.getChar(b3) == '_') {
                        this.keyboard.setState(Keyboard.STATE_NUMBER);
                    }
                    if (this.keyboard.getChar(b3) == '`' && this.name.length() > 0) {
                        this.name = String.copyValueOf(this.name.toCharArray(), 0, this.name.length() - 1);
                    }
                    if (this.keyboard.getChar(b3) == ' ') {
                        this.name = String.valueOf(this.name) + this.keyboard.getChar(b3);
                    }
                }
            }
            this.bukva = Keyboard.CHAR_SPACE;
            SelectProfileInput selectProfileInput = this.inp;
            this.inp.getClass();
            selectProfileInput.buttonUp = States.NONE;
        }
        byte b4 = this.inp.buttonDown;
        this.inp.getClass();
        if (b4 == 50) {
            this.buttonNameEdit.setBackActive(isInputCoordsIn(this.buttonNameEdit));
            for (ButtonChar b5 : this.keyboard.buttonKb) {
                if (isInputCoordsIn(b5)) {
                    this.bukva = this.keyboard.getChar(b5);
                    b5.setBackActive(true);
                }
            }
            for (ButtonChar b6 : this.keyboard.buttonControl) {
                b6.setBackActive(isInputCoordsIn(b6));
            }
        }
    }

    /* JADX INFO: Multiple debug info for r8v1 com.machaon.quadratum.parts.ButtonChar: [D('b' com.machaon.quadratum.parts.ButtonText), D('b' com.machaon.quadratum.parts.ButtonChar)] */
    public void render(Application app) {
        app.getGraphics().getGL10().glClearColor(0.85f, 0.85f, 0.85f, 1.0f);
        app.getGraphics().getGL10().glClear(16384);
        this.spriteBatch.begin();
        this.spriteBatch.enableBlending();
        States.renderBackground(this.spriteBatch);
        if (this.pos == 0) {
            for (ButtonText b : this.buttonName) {
                this.spriteBatch.draw(b.getBackTexture(), (float) b.x, (float) b.y, 0, 0, b.width, b.height);
                if (this.buttonDelete.y == b.y) {
                    States.fontBig.setColor(1.0f, 1.0f, 1.0f, 1.0f - this.timer_delete);
                } else {
                    States.fontBig.setColor(1.0f, 1.0f, 1.0f, 1.0f);
                }
                if (b.getString() == "") {
                    States.fontBig.setColor(0.5f, 0.5f, 0.8f, 1.0f);
                }
                if (b.getString() == "") {
                    States.fontBig.drawMultiLine(this.spriteBatch, States.TEXT_NEWPLAYER[States.language], (float) b.x, (float) (b.y + b.getTextOffsetY()), (float) b.width, BitmapFont.HAlignment.CENTER);
                } else {
                    States.fontBig.drawMultiLine(this.spriteBatch, b.getString(), (float) b.x, (float) (b.y + b.getTextOffsetY()), (float) b.width, BitmapFont.HAlignment.CENTER);
                }
                if (this.buttonDelete.y == b.y) {
                    States.fontBig.setColor(0.5f, 0.5f, 0.8f, this.timer_delete);
                    States.fontBig.drawMultiLine(this.spriteBatch, States.TEXT_NEWPLAYER[States.language], (float) b.x, (float) (b.y + b.getTextOffsetY()), (float) b.width, BitmapFont.HAlignment.CENTER);
                }
            }
            if (this.buttonDelete.x != 0) {
                this.spriteBatch.draw(this.buttonDelete.getBackTexture(), (float) this.buttonDelete.x, (float) this.buttonDelete.y, 0, 0, this.buttonDelete.width, this.buttonDelete.height);
                this.spriteBatch.draw(this.buttonDelete.texFront, (float) (this.buttonDelete.x + this.buttonDelete.frontX), (float) (this.buttonDelete.y + this.buttonDelete.frontY), 0, 0, this.buttonDelete.width, this.buttonDelete.height);
            }
        }
        if (this.pos == 1) {
            this.spriteBatch.draw(this.buttonNameEdit.getBackTexture(), (float) this.buttonNameEdit.x, (float) this.buttonNameEdit.y, 0, 0, this.buttonNameEdit.width, this.buttonNameEdit.height);
            States.fontBig.setColor(1.0f, 1.0f, 1.0f, 1.0f);
            for (ButtonChar b2 : this.keyboard.buttonKb) {
                this.spriteBatch.draw(this.keyboard.getBackTexture(b2), (float) b2.x, (float) b2.y, 0, 0, b2.width, b2.height);
                States.fontBig.drawMultiLine(this.spriteBatch, String.valueOf(this.keyboard.getChar(b2)), (float) b2.x, (float) (b2.y + this.keyboard.fontY), (float) b2.width, BitmapFont.HAlignment.CENTER);
            }
            States.fontBig.draw(this.spriteBatch, this.name, (float) (this.buttonNameEdit.x + this.buttonNameEdit.frontX), (float) (this.buttonNameEdit.y + this.buttonNameEdit.frontY));
            BitmapFont.TextBounds bounds = States.fontBig.getBounds(this.name);
            States.fontBig.setColor(1.0f, 0.0f, 0.0f, 1.0f);
            States.fontBig.draw(this.spriteBatch, String.valueOf(this.bukva), ((float) (this.buttonNameEdit.x + this.buttonNameEdit.frontX)) + bounds.width, (float) (this.buttonNameEdit.y + this.buttonNameEdit.frontY));
            States.fontBig.setColor(1.0f, 1.0f, 1.0f, 1.0f);
            for (ButtonChar b3 : this.keyboard.buttonControl) {
                this.spriteBatch.draw(this.keyboard.getBackTexture(b3), (float) b3.x, (float) b3.y, 0, 0, b3.width, b3.height);
                States.fontBig.drawMultiLine(this.spriteBatch, String.valueOf(this.keyboard.getChar(b3)), (float) b3.x, (float) (b3.y + this.keyboard.fontY), (float) b3.width, BitmapFont.HAlignment.CENTER);
            }
            this.spriteBatch.draw(this.texFlags, (float) this.geomFlag.x, (float) this.geomFlag.y, this.geomFlag.width * this.keyboard.language, 0, this.geomFlag.width, this.geomFlag.height);
        }
        if (this.isEnd) {
            States.renderClock(this.spriteBatch);
        }
        this.spriteBatch.end();
    }

    public void dispose() {
        Profile.SaveProfiles();
        this.spriteBatch.dispose();
        this.texFlags.dispose();
        for (ButtonText b : this.buttonName) {
            b.dispose();
        }
        this.buttonDelete.dispose();
        this.buttonNameEdit.dispose();
        this.keyboard.dispose();
    }

    public void resume() {
    }

    private boolean isInputCoordsIn(Geom geom) {
        if (((float) this.inp.x) <= ((float) (geom.x + States.cameraOffsetX)) * States.aspectX || ((float) this.inp.x) >= ((float) (geom.x + geom.width + States.cameraOffsetX)) * States.aspectX || ((float) (States.displayHeight - this.inp.y)) <= ((float) (geom.y + States.cameraOffsetY)) * States.aspectY || ((float) (States.displayHeight - this.inp.y)) >= ((float) (geom.y + geom.height + States.cameraOffsetY)) * States.aspectY) {
            return false;
        }
        return true;
    }

    private void setTimer(float timeInSec) {
        this.timer = timeInSec;
    }

    private boolean isTimerEnd() {
        if (this.timer < 0.0f) {
            return true;
        }
        this.timer -= Gdx.graphics.getDeltaTime();
        return false;
    }
}
