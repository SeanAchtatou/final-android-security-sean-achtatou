package com.machaon.quadratum.screens;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.machaon.quadratum.IScreen;
import com.machaon.quadratum.States;
import com.machaon.quadratum.input.CommonInput;
import com.machaon.quadratum.parts.Geom;
import com.machaon.quadratum.parts.Profile;
import com.machaon.quadratum.parts.Tournaments;
import java.io.IOException;
import java.io.InputStream;
import java.util.Random;

public class RoundResults implements IScreen {
    public static boolean isCount;
    private int a_height;
    private int a_width;
    private Geom[] gAchiev = new Geom[4];
    private Geom gInfo;
    private Geom[] gKubok = new Geom[4];
    private Geom gName;
    private Geom gRect;
    private Geom gScore;
    private String[] info = new String[4];
    private CommonInput inp = new CommonInput();
    private boolean isEnd = false;
    private boolean isRefresh = false;
    private boolean isRefreshLevel = false;
    private boolean isSavedProfiles = false;
    private boolean isStartSound = false;
    private boolean isToast = false;
    private byte levelModify = 1;
    private int modifyKubokX = 0;
    private ParticleEffect[] peAchiev;
    private Pixmap pixmap;
    private int[] playerAllScore = new int[4];
    private ShowKubok showKubok = new ShowKubok(this, null);
    private Sound soundScoring;
    private Sound soundTada;
    private final SpriteBatch spriteBatch = new SpriteBatch();
    private Texture texField = new Texture(Gdx.files.internal(String.valueOf(States.PATH_GRAPH + States.resolution + "/") + "field.png"));
    private Texture texKubok = null;
    private Texture texture;
    private float timer;
    private float timerToast;

    private class ShowKubok {
        byte player;
        byte type;

        private ShowKubok() {
        }

        /* synthetic */ ShowKubok(RoundResults roundResults, ShowKubok showKubok) {
            this();
        }
    }

    public RoundResults() {
        this.showKubok.type = 0;
        InitObjects();
        for (byte i = 0; i < 4; i = (byte) (i + 1)) {
            this.playerAllScore[i] = States.playerOverallScore[i];
            int[] iArr = States.playerOverallScore;
            iArr[i] = iArr[i] - States.playerScore[i];
        }
        GameLoop.currentPlayer = (byte) new Random().nextInt(States.numberOfPlayers);
        setTimer(1.0f);
        setTimerToast(1.5f);
        this.spriteBatch.setProjectionMatrix(States.viewMatrix);
        if (States.isAntialiasing) {
            this.texField.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        }
        this.soundTada = Gdx.audio.newSound(Gdx.files.internal("data/Sounds/tada.ogg"));
        this.soundScoring = Gdx.audio.newSound(Gdx.files.internal("data/Sounds/scoring.ogg"));
        Gdx.app.getInput().setInputProcessor(this.inp);
    }

    private void InitTextureKubok() {
        this.texKubok = new Texture(Gdx.files.internal(String.valueOf(States.PATH_GRAPH + States.resolution + "/") + "kubki.png"));
        if (States.isAntialiasing) {
            this.texKubok.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        }
    }

    private void InitObjects() {
        byte widthBlackCell = 0;
        this.pixmap = new Pixmap(States.screenWidth, States.screenHeight, Pixmap.Format.RGBA8888);
        int w = 0;
        int h = 0;
        for (int i = 12; i > 0; i--) {
            if (((double) States.screenWidth) <= Math.pow(2.0d, (double) i)) {
                w = (int) Math.pow(2.0d, (double) i);
            }
            if (((double) States.screenHeight) <= Math.pow(2.0d, (double) i)) {
                h = (int) Math.pow(2.0d, (double) i);
            }
        }
        this.texture = new Texture(w, h, Pixmap.Format.RGBA8888);
        if (States.screenHeight == 240 && States.screenWidth == 320) {
            this.gRect = new Geom(69, 59, 181, 121);
            this.gScore = new Geom(169, Input.Keys.BUTTON_MODE, 0, 26);
            this.gName = new Geom(13, 228, 6, 26);
            this.gInfo = new Geom(13, 206, 0, 49);
            this.gKubok[0] = new Geom(77, 150, 28, 48);
            this.gKubok[1] = new Geom(215, 150, 28, 48);
            this.gKubok[2] = new Geom(215, 36, 28, 48);
            this.gKubok[3] = new Geom(77, 36, 28, 48);
            this.gAchiev[0] = new Geom(0, 122, 1, 0);
            this.gAchiev[1] = new Geom(256, 122, -1, 0);
            this.gAchiev[2] = new Geom(256, 54, -1, 0);
            this.gAchiev[3] = new Geom(0, 54, 1, 0);
            this.a_width = 64;
            this.a_height = 64;
            widthBlackCell = 5;
        }
        if (States.screenHeight == 240 && States.screenWidth == 400) {
            this.gRect = new Geom(Input.Keys.BUTTON_SELECT, 59, 181, 121);
            this.gScore = new Geom(169, Input.Keys.BUTTON_MODE, 0, 26);
            this.gName = new Geom(13, 228, 6, 26);
            this.gInfo = new Geom(13, 206, 0, 49);
            this.gKubok[0] = new Geom(117, 150, 28, 48);
            this.gKubok[1] = new Geom(255, 150, 28, 48);
            this.gKubok[2] = new Geom(255, 36, 28, 48);
            this.gKubok[3] = new Geom(117, 36, 28, 48);
            this.gAchiev[0] = new Geom(0, 122, 1, 0);
            this.gAchiev[1] = new Geom(336, 122, -1, 0);
            this.gAchiev[2] = new Geom(336, 54, -1, 0);
            this.gAchiev[3] = new Geom(0, 54, 1, 0);
            this.a_width = 64;
            this.a_height = 64;
            widthBlackCell = 5;
        }
        if (States.screenHeight == 320) {
            this.gRect = new Geom(Input.Keys.END, 88, 216, 144);
            this.gScore = new Geom(225, 147, 0, 35);
            this.gName = new Geom(15, 305, 7, 30);
            this.gInfo = new Geom(15, 275, 0, 60);
            this.gKubok[0] = new Geom(150, 200, 44, 64);
            this.gKubok[1] = new Geom(286, 200, 44, 64);
            this.gKubok[2] = new Geom(286, 48, 44, 64);
            this.gKubok[3] = new Geom(150, 48, 44, 64);
            this.gAchiev[0] = new Geom(-5, 162, 1, 0);
            this.gAchiev[1] = new Geom(389, 162, -1, 0);
            this.gAchiev[2] = new Geom(389, 60, -1, 0);
            this.gAchiev[3] = new Geom(-5, 60, 1, 0);
            this.a_width = 96;
            this.a_height = 96;
            this.modifyKubokX = 3;
            widthBlackCell = 6;
        }
        if (States.screenHeight == 480) {
            if (States.screenWidth == 800) {
                this.gRect = new Geom(236, Input.Keys.INSERT, 321, 213);
            } else {
                this.gRect = new Geom(263, Input.Keys.INSERT, 321, 213);
            }
            this.gScore = new Geom(337, 220, 0, 50);
            this.gName = new Geom(29, 452, 10, 45);
            this.gInfo = new Geom(29, 412, 0, 90);
            this.a_width = 96;
            this.a_height = 96;
            if (States.screenWidth == 800) {
                this.gKubok[0] = new Geom(255, 300, 58, 96);
                this.gKubok[1] = new Geom(482, 300, 58, 96);
                this.gKubok[2] = new Geom(482, 72, 58, 96);
                this.gKubok[3] = new Geom(255, 72, 58, 96);
                this.gAchiev[0] = new Geom(0, Input.Keys.F7, 1, 0);
                this.gAchiev[1] = new Geom(704, Input.Keys.F7, -1, 0);
                this.gAchiev[2] = new Geom(704, 134, -1, 0);
                this.gAchiev[3] = new Geom(0, 134, 1, 0);
            } else {
                this.gKubok[0] = new Geom(282, 300, 58, 96);
                this.gKubok[1] = new Geom(509, 300, 58, 96);
                this.gKubok[2] = new Geom(509, 72, 58, 96);
                this.gKubok[3] = new Geom(282, 72, 58, 96);
                this.gAchiev[0] = new Geom(0, Input.Keys.F7, 1, 0);
                this.gAchiev[1] = new Geom(758, Input.Keys.F7, -1, 0);
                this.gAchiev[2] = new Geom(758, 134, -1, 0);
                this.gAchiev[3] = new Geom(0, 134, 1, 0);
            }
            widthBlackCell = 9;
        }
        this.pixmap.setColor(0.4f, 0.4f, 0.4f, 0.7f);
        if (!Tournaments.isEnd()) {
            this.pixmap.drawLine(States.screenWidth / 2, 0, States.screenWidth / 2, this.gRect.y - 1);
            this.pixmap.drawLine(States.screenWidth / 2, this.gRect.y + this.gRect.height, States.screenWidth / 2, States.screenHeight);
            this.pixmap.drawLine(0, States.screenHeight / 2, this.gRect.x - 1, States.screenHeight / 2);
            this.pixmap.drawLine(this.gRect.x + this.gRect.width, States.screenHeight / 2, States.screenWidth, States.screenHeight / 2);
            this.pixmap.drawRectangle(this.gRect.x + 2, this.gRect.y + 2, this.gRect.width - 4, this.gRect.height - 4);
            this.pixmap.setColor(0.4f, 0.4f, 0.4f, 0.8f);
            this.pixmap.drawRectangle(this.gRect.x + 1, this.gRect.y + 1, this.gRect.width - 2, this.gRect.height - 2);
            this.pixmap.setColor(0.4f, 0.4f, 0.4f, 0.7f);
            this.pixmap.drawRectangle(this.gRect.x, this.gRect.y, this.gRect.width, this.gRect.height);
        } else {
            this.pixmap.drawLine(States.screenWidth / 2, 0, States.screenWidth / 2, States.screenHeight);
            this.pixmap.drawLine(0, States.screenHeight / 2, States.screenWidth, States.screenHeight / 2);
        }
        this.pixmap.setColor(0.4f, 0.4f, 0.4f, 0.8f);
        if (States.numberOfPlayers == 2) {
            this.pixmap.drawRectangle(this.gName.x - this.gName.width, this.gName.x - this.gName.width, (int) (States.fontBig.getBounds(States.playerName[0]).width + ((float) (this.gName.width * 2))), (int) (States.fontBig.getBounds(States.playerName[0]).height + ((float) (this.gName.width * 2))));
            this.pixmap.drawRectangle((States.screenWidth - this.gName.x) + this.gName.width, (States.screenHeight - this.gName.width) - this.gName.height, (int) (-(States.fontBig.getBounds(States.playerName[1]).width + ((float) (this.gName.width * 2)))), (int) (States.fontBig.getBounds(States.playerName[1]).height + ((float) (this.gName.width * 2))));
        } else {
            this.pixmap.drawRectangle(this.gName.x - this.gName.width, this.gName.x - this.gName.width, (int) (States.fontBig.getBounds(States.playerName[0]).width + ((float) (this.gName.width * 2))), (int) (States.fontBig.getBounds(States.playerName[0]).height + ((float) (this.gName.width * 2))));
            this.pixmap.drawRectangle((States.screenWidth - this.gName.x) + this.gName.width, this.gName.x - this.gName.width, (int) (-(States.fontBig.getBounds(States.playerName[1]).width + ((float) (this.gName.width * 2)))), (int) (States.fontBig.getBounds(States.playerName[1]).height + ((float) (this.gName.width * 2))));
            this.pixmap.drawRectangle((States.screenWidth - this.gName.x) + this.gName.width, (States.screenHeight - this.gName.width) - this.gName.height, (int) (-(States.fontBig.getBounds(States.playerName[2]).width + ((float) (this.gName.width * 2)))), (int) (States.fontBig.getBounds(States.playerName[2]).height + ((float) (this.gName.width * 2))));
            this.pixmap.drawRectangle(this.gName.x - this.gName.width, (States.screenHeight - this.gName.width) - this.gName.height, (int) (States.fontBig.getBounds(States.playerName[3]).width + ((float) (this.gName.width * 2))), (int) (States.fontBig.getBounds(States.playerName[3]).height + ((float) (this.gName.width * 2))));
        }
        if (!Tournaments.isEnd()) {
            this.pixmap.setColor(0.0f, 0.0f, 0.0f, 1.0f);
            InputStream in = Gdx.files.internal("data/Levels/" + Tournaments.getNextLevel()).read();
            try {
                int nBlackCells = in.read();
                for (int i2 = 0; i2 < nBlackCells; i2++) {
                    this.pixmap.fillRectangle(this.gRect.x + 3 + (in.read() * widthBlackCell), this.gRect.y + 3 + (in.read() * widthBlackCell), widthBlackCell, widthBlackCell);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                in.close();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
        this.pixmap.setColor(0.0f, 0.0f, 0.0f, 0.7f);
        int w2 = (int) States.fontBig.getBounds(String.valueOf(States.TEXT_LEVEL[States.language]) + " " + (Tournaments.getNumberOfCurrentLevel() + this.levelModify)).width;
        this.pixmap.fillRectangle(((States.screenWidth - w2) / 2) - this.gName.width, (int) ((((((float) States.screenHeight) - States.fontBig.getBounds("1").height) / 2.0f) - ((float) this.gName.width)) + 1.0f), (this.gName.width * 2) + w2, (int) (States.fontBig.getBounds("1").height + ((float) (this.gName.width * 2))));
        this.pixmap.setColor(0.4f, 0.4f, 0.4f, 0.9f);
        this.pixmap.drawRectangle(((States.screenWidth - w2) / 2) - this.gName.width, (int) ((((((float) States.screenHeight) - States.fontBig.getBounds("1").height) / 2.0f) - ((float) this.gName.width)) + 1.0f), (this.gName.width * 2) + w2, (int) (States.fontBig.getBounds("1").height + ((float) (this.gName.width * 2))));
        if (this.showKubok.type != 0) {
            byte p = this.showKubok.player;
            this.pixmap.setColor(0.0f, 0.0f, 0.0f, 0.8f);
            this.pixmap.fillRectangle(this.gKubok[p].x - (this.gKubok[p].width / 2), ((States.screenHeight - this.gKubok[p].y) - this.gKubok[p].height) - (this.gKubok[p].width / 2), this.gKubok[p].width * 2, this.gKubok[p].height + this.gKubok[p].width);
            this.pixmap.setColor(0.4f, 0.4f, 0.4f, 0.9f);
            this.pixmap.drawRectangle(this.gKubok[p].x - (this.gKubok[p].width / 2), ((States.screenHeight - this.gKubok[p].y) - this.gKubok[p].height) - (this.gKubok[p].width / 2), this.gKubok[p].width * 2, this.gKubok[p].height + this.gKubok[p].width);
        }
        this.texture.draw(this.pixmap, 0, 0);
        this.pixmap.dispose();
    }

    public void update() {
        if (!this.inp.isBack || Tournaments.getNumberOfCurrentLevel() != 0) {
            if (this.isEnd) {
                if (Profile.profile.settingSounds) {
                    States.click.play();
                }
                if (Tournaments.isEnd()) {
                    if (Tournaments.getType() == Tournaments.TYPE_SINGLE) {
                        States.state = 6;
                    } else {
                        States.state = 7;
                    }
                    States.Init();
                    return;
                }
                States.state = 9;
                return;
            }
            if (isTimerToastEnd() && !isCount && !Tournaments.isEnd()) {
                setTimerToast(0.5f);
                if (this.isToast) {
                    this.isToast = false;
                } else {
                    this.isToast = true;
                }
            }
            if (Tournaments.isEnd()) {
                this.isToast = false;
            }
            if (!isCount) {
                isDone();
            }
            if (isCount) {
                int modifySpeed = 1;
                if (Gdx.input.isTouched()) {
                    modifySpeed = 100;
                }
                this.isRefreshLevel = true;
                this.levelModify = 1;
                States.fontSmall.setColor(0.7f, 0.7f, 0.7f, 1.0f);
                for (byte i = 0; i < 4; i = (byte) (i + 1)) {
                    this.info[i] = States.TEXT_SCORING[States.language];
                }
                isCount = false;
                for (byte i2 = 0; i2 < 4; i2 = (byte) (i2 + 1)) {
                    if (States.playerScore[i2] > 0) {
                        isCount = true;
                    }
                }
                if (!isCount) {
                    this.soundScoring.stop();
                    if (!this.isSavedProfiles) {
                        for (byte i3 = 0; i3 < 4; i3 = (byte) (i3 + 1)) {
                            if (Profile.isActiveProfile[i3]) {
                                Profile.pushProfile(Profile.pp[i3].numberOfProfile, Profile.pp[i3]);
                            }
                        }
                        this.isSavedProfiles = true;
                    }
                    setTimer(1.0f);
                } else if (isTimerEnd()) {
                    if (!this.isStartSound) {
                        if (Profile.profile.settingSounds) {
                            this.soundScoring.play();
                        }
                        this.isStartSound = true;
                    }
                    for (byte i4 = 0; i4 < 4; i4 = (byte) (i4 + 1)) {
                        if (States.playerScore[i4] > 0) {
                            float c = ((float) (modifySpeed * 100)) * Gdx.graphics.getDeltaTime();
                            int delta = States.playerScore[i4];
                            int[] iArr = States.playerScore;
                            iArr[i4] = (int) (((float) iArr[i4]) - c);
                            int delta2 = delta - States.playerScore[i4];
                            int[] iArr2 = States.playerOverallScore;
                            iArr2[i4] = iArr2[i4] + delta2;
                        }
                        if (States.playerScore[i4] <= 0) {
                            States.playerOverallScore[i4] = this.playerAllScore[i4];
                            States.playerScore[i4] = 0;
                        }
                    }
                }
            } else if (!this.isRefresh) {
                if (this.isRefreshLevel) {
                    Tournaments.setToNextLevel();
                    resume();
                    this.isRefreshLevel = false;
                }
                this.levelModify = 1;
                States.fontSmall.setColor(0.8f, 0.8f, 0.8f, 1.0f);
                for (byte i5 = 0; i5 < 4; i5 = (byte) (i5 + 1)) {
                    this.info[i5] = "";
                }
                this.info[GameLoop.currentPlayer] = States.TEXT_FIRSTMOVE[States.language];
                if (Tournaments.isEnd()) {
                    States.fontSmall.setColor(1.0f, 1.0f, 0.0f, 1.0f);
                    for (byte i6 = 0; i6 < 4; i6 = (byte) (i6 + 1)) {
                        this.playerAllScore[i6] = States.playerOverallScore[i6];
                    }
                    if (Tournaments.getType() == Tournaments.TYPE_SINGLE && this.playerAllScore[0] > Profile.pp[0].maxScoresOnTour[States.difficulty - 1]) {
                        Profile.pp[0].maxScoresOnTour[States.difficulty - 1] = this.playerAllScore[0];
                    }
                    int max = 0;
                    byte player = 0;
                    for (byte place = 0; place < States.numberOfPlayers; place = (byte) (place + 1)) {
                        for (byte p = 0; p < States.numberOfPlayers; p = (byte) (p + 1)) {
                            if (max <= this.playerAllScore[p]) {
                                max = this.playerAllScore[p];
                                player = p;
                            }
                        }
                        if (place == 0) {
                            if (player == 0 && Tournaments.getType() == Tournaments.TYPE_SINGLE) {
                                if (Profile.pp[0].resultTournament[Tournaments.getTournament() - 1] == 0 || Profile.pp[0].resultTournament[Tournaments.getTournament() - 1] > 4 - States.difficulty) {
                                    Profile.pp[0].resultTournament[Tournaments.getTournament() - 1] = 4 - States.difficulty;
                                }
                                this.showKubok.type = (byte) (4 - States.difficulty);
                                Profile.pp[0].nWinTours++;
                                if (States.difficulty == 3) {
                                    Profile.pp[0].nGoldCups++;
                                }
                            } else {
                                this.showKubok.type = 1;
                            }
                            this.showKubok.player = player;
                            if (States.numberOfPlayers == 2 && player == 1) {
                                this.showKubok.player = 2;
                            }
                        }
                        if (Tournaments.getType() == Tournaments.TYPE_MULTY && Profile.isActiveProfile[player]) {
                            if (place == 0) {
                                Profile.pp[player].multi_nWinTours++;
                                for (int pl = 0; pl < States.numberOfPlayers; pl++) {
                                    if (pl != player && States.skillAI[pl] == 0) {
                                        Profile.pp[player].multi_nWonMans++;
                                    }
                                }
                            }
                            if (States.typeOfGame != 1) {
                                Profile.pp[player].multi_nToursWithoutBonuses++;
                            }
                        }
                        this.playerAllScore[player] = 0;
                        this.info[player] = String.valueOf(place + 1) + States.TEXT_PLACE[States.language];
                        max = 0;
                        resume();
                    }
                    this.isRefresh = true;
                    for (byte i7 = 0; i7 < 4; i7 = (byte) (i7 + 1)) {
                        if (Profile.isActiveProfile[i7]) {
                            if (Tournaments.getType() == Tournaments.TYPE_SINGLE) {
                                Profile.pp[i7].nPlayedTours++;
                            } else {
                                Profile.pp[i7].multi_nPlayedTours++;
                            }
                        }
                    }
                    for (byte i8 = 0; i8 < 4; i8 = (byte) (i8 + 1)) {
                        if (Profile.isActiveProfile[i8]) {
                            int j = 0;
                            for (byte u = 0; u < 5; u = (byte) (u + 1)) {
                                j += States.playerBonus[i8][u];
                            }
                            if (j > Profile.pp[i8].maxBonusesAtEnd) {
                                Profile.pp[i8].maxBonusesAtEnd = j;
                            }
                        }
                    }
                    for (byte i9 = 0; i9 < 4; i9 = (byte) (i9 + 1)) {
                        if (Profile.isActiveProfile[i9]) {
                            Profile.pushProfile(Profile.pp[i9].numberOfProfile, Profile.pp[i9]);
                        }
                    }
                    Profile.SaveProfiles();
                    if (Profile.profile.settingSounds) {
                        this.soundTada.play();
                    }
                }
            }
        } else if (Tournaments.getType() == Tournaments.TYPE_SINGLE) {
            States.state = 6;
        } else {
            States.state = 8;
        }
    }

    public void render(Application app) {
        app.getGraphics().getGL10().glClearColor(0.85f, 0.85f, 0.85f, 1.0f);
        app.getGraphics().getGL10().glClear(16384);
        this.spriteBatch.begin();
        this.spriteBatch.enableBlending();
        States.renderBackground(this.spriteBatch);
        States.fontBig.setColor(1.0f, 1.0f, 1.0f, 1.0f);
        if (States.numberOfPlayers == 2) {
            States.fontBig.drawMultiLine(this.spriteBatch, States.playerName[0], (float) this.gName.x, (float) this.gName.y);
            States.fontBig.drawMultiLine(this.spriteBatch, States.playerName[1], (float) (States.screenWidth >> 1), (float) this.gName.height, (float) ((States.screenWidth >> 1) - this.gName.x), BitmapFont.HAlignment.RIGHT);
        } else {
            States.fontBig.drawMultiLine(this.spriteBatch, States.playerName[0], (float) this.gName.x, (float) this.gName.y);
            States.fontBig.drawMultiLine(this.spriteBatch, States.playerName[1], (float) (States.screenWidth >> 1), (float) this.gName.y, (float) ((States.screenWidth >> 1) - this.gName.x), BitmapFont.HAlignment.RIGHT);
            States.fontBig.drawMultiLine(this.spriteBatch, States.playerName[2], (float) (States.screenWidth >> 1), (float) this.gName.height, (float) ((States.screenWidth >> 1) - this.gName.x), BitmapFont.HAlignment.RIGHT);
            States.fontBig.drawMultiLine(this.spriteBatch, States.playerName[3], (float) this.gName.x, (float) this.gName.height);
        }
        if (States.numberOfPlayers == 2) {
            States.fontBig.drawMultiLine(this.spriteBatch, String.valueOf(States.playerScore[0]), 0.0f, (float) this.gScore.x, (float) (this.gRect.x - 1), BitmapFont.HAlignment.CENTER);
            States.fontBig.drawMultiLine(this.spriteBatch, String.valueOf(States.playerScore[1]), (float) (this.gRect.x + this.gRect.width), (float) this.gScore.y, (float) (this.gRect.x - 1), BitmapFont.HAlignment.CENTER);
        } else {
            States.fontBig.drawMultiLine(this.spriteBatch, String.valueOf(States.playerScore[0]), 0.0f, (float) this.gScore.x, (float) (this.gRect.x - 1), BitmapFont.HAlignment.CENTER);
            States.fontBig.drawMultiLine(this.spriteBatch, String.valueOf(States.playerScore[1]), (float) (this.gRect.x + this.gRect.width), (float) this.gScore.x, (float) (this.gRect.x - 1), BitmapFont.HAlignment.CENTER);
            States.fontBig.drawMultiLine(this.spriteBatch, String.valueOf(States.playerScore[3]), 0.0f, (float) this.gScore.y, (float) (this.gRect.x - 1), BitmapFont.HAlignment.CENTER);
            States.fontBig.drawMultiLine(this.spriteBatch, String.valueOf(States.playerScore[2]), (float) (this.gRect.x + this.gRect.width), (float) this.gScore.y, (float) (this.gRect.x - 1), BitmapFont.HAlignment.CENTER);
        }
        States.fontBig.setColor(1.0f, 0.0f, 0.0f, 1.0f);
        if (States.numberOfPlayers == 2) {
            States.fontBig.drawMultiLine(this.spriteBatch, String.valueOf(States.playerOverallScore[0]), 0.0f, (float) (this.gScore.x - this.gScore.height), (float) (this.gRect.x - 1), BitmapFont.HAlignment.CENTER);
            States.fontBig.drawMultiLine(this.spriteBatch, String.valueOf(States.playerOverallScore[1]), (float) (this.gRect.x + this.gRect.width), (float) (this.gScore.y - this.gScore.height), (float) (this.gRect.x - 1), BitmapFont.HAlignment.CENTER);
        } else {
            States.fontBig.drawMultiLine(this.spriteBatch, String.valueOf(States.playerOverallScore[0]), 0.0f, (float) (this.gScore.x - this.gScore.height), (float) (this.gRect.x - 1), BitmapFont.HAlignment.CENTER);
            States.fontBig.drawMultiLine(this.spriteBatch, String.valueOf(States.playerOverallScore[1]), (float) (this.gRect.x + this.gRect.width), (float) (this.gScore.x - this.gScore.height), (float) (this.gRect.x - 1), BitmapFont.HAlignment.CENTER);
            States.fontBig.drawMultiLine(this.spriteBatch, String.valueOf(States.playerOverallScore[3]), 0.0f, (float) (this.gScore.y - this.gScore.height), (float) (this.gRect.x - 1), BitmapFont.HAlignment.CENTER);
            States.fontBig.drawMultiLine(this.spriteBatch, String.valueOf(States.playerOverallScore[2]), (float) (this.gRect.x + this.gRect.width), (float) (this.gScore.y - this.gScore.height), (float) (this.gRect.x - 1), BitmapFont.HAlignment.CENTER);
        }
        if (!Tournaments.isEnd()) {
            this.spriteBatch.draw(this.texField, (float) (this.gRect.x + 3), (float) (this.gRect.y + 3), 0, 0, this.gRect.width - 6, this.gRect.height - 6);
        }
        this.spriteBatch.draw(this.texture, 0.0f, 0.0f, 0, 0, States.screenWidth, States.screenHeight);
        States.fontBig.setColor(1.0f, 1.0f, 1.0f, 1.0f);
        if (Tournaments.isEnd()) {
            States.fontBig.drawMultiLine(this.spriteBatch, States.TEXT_RESULT[States.language], 0.0f, (float) ((int) ((((float) States.screenHeight) + States.fontBig.getBounds("1").height) / 2.0f)), (float) States.screenWidth, BitmapFont.HAlignment.CENTER);
        } else {
            States.fontBig.drawMultiLine(this.spriteBatch, String.valueOf(States.TEXT_LEVEL[States.language]) + " " + (Tournaments.getNumberOfCurrentLevel() + this.levelModify), 0.0f, (float) ((int) ((((float) States.screenHeight) + States.fontBig.getBounds("1").height) / 2.0f)), (float) States.screenWidth, BitmapFont.HAlignment.CENTER);
        }
        renderInfo();
        if (this.showKubok.type != 0) {
            renderKubok();
        }
        if (this.isToast) {
            States.fontSmall.setColor(0.0f, 1.0f, 0.0f, 1.0f);
            States.fontSmall.drawMultiLine(this.spriteBatch, States.TEXT_START[States.language], 0.0f, (States.fontBig.getCapHeight() * 1.7f) + ((float) (this.gRect.y + this.gRect.height)), (float) States.screenWidth, BitmapFont.HAlignment.CENTER);
        }
        if (this.isEnd) {
            States.renderClock(this.spriteBatch);
        }
        this.spriteBatch.end();
    }

    private void renderInfo() {
        if (States.numberOfPlayers == 2) {
            States.fontSmall.drawMultiLine(this.spriteBatch, this.info[0], (float) this.gInfo.x, (float) this.gInfo.y);
            States.fontSmall.drawMultiLine(this.spriteBatch, this.info[1], (float) (States.screenWidth / 2), (float) this.gInfo.height, (float) ((States.screenWidth / 2) - this.gInfo.x), BitmapFont.HAlignment.RIGHT);
            return;
        }
        States.fontSmall.drawMultiLine(this.spriteBatch, this.info[0], (float) this.gInfo.x, (float) this.gInfo.y);
        States.fontSmall.drawMultiLine(this.spriteBatch, this.info[1], (float) (States.screenWidth / 2), (float) this.gInfo.y, (float) ((States.screenWidth / 2) - this.gInfo.x), BitmapFont.HAlignment.RIGHT);
        States.fontSmall.drawMultiLine(this.spriteBatch, this.info[2], (float) (States.screenWidth / 2), (float) this.gInfo.height, (float) ((States.screenWidth / 2) - this.gInfo.x), BitmapFont.HAlignment.RIGHT);
        States.fontSmall.drawMultiLine(this.spriteBatch, this.info[3], (float) this.gInfo.x, (float) this.gInfo.height);
    }

    private void renderKubok() {
        if (this.texKubok == null) {
            InitTextureKubok();
        }
        this.spriteBatch.draw(this.texKubok, (float) (this.gKubok[this.showKubok.player].x + this.modifyKubokX), (float) this.gKubok[this.showKubok.player].y, this.gKubok[this.showKubok.player].width * this.showKubok.type, 0, this.gKubok[this.showKubok.player].width, this.gKubok[this.showKubok.player].height);
    }

    private void isDone() {
        if (isTimerEnd()) {
            if (Gdx.input.isTouched()) {
                this.isEnd = true;
            } else {
                States.state = States.NONE;
            }
        }
    }

    public void dispose() {
        this.spriteBatch.dispose();
        if (this.texture != null) {
            this.texture.dispose();
        }
        if (this.texField != null) {
            this.texField.dispose();
        }
        if (this.texKubok != null) {
            this.texKubok.dispose();
        }
        if (this.soundTada != null) {
            this.soundTada.dispose();
        }
        if (this.soundScoring != null) {
            this.soundScoring.dispose();
        }
        if (this.peAchiev != null) {
            for (byte i = 0; i < this.peAchiev.length; i = (byte) (i + 1)) {
                if (this.peAchiev[i] != null) {
                    this.peAchiev[i].dispose();
                }
            }
        }
    }

    public void resume() {
        if (this.texture != null) {
            this.texture.dispose();
        }
        InitObjects();
    }

    private void setTimer(float timeInSec) {
        this.timer = timeInSec;
    }

    private boolean isTimerEnd() {
        if (this.timer < 0.0f) {
            return true;
        }
        this.timer -= Gdx.graphics.getDeltaTime();
        return false;
    }

    private void setTimerToast(float timeInSec) {
        this.timerToast = timeInSec;
    }

    private boolean isTimerToastEnd() {
        if (this.timerToast < 0.0f) {
            return true;
        }
        this.timerToast -= Gdx.graphics.getDeltaTime();
        return false;
    }
}
