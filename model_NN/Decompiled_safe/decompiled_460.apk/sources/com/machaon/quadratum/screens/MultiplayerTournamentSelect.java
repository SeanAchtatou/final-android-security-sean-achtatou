package com.machaon.quadratum.screens;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.machaon.quadratum.IScreen;
import com.machaon.quadratum.States;
import com.machaon.quadratum.input.CommonInput;
import com.machaon.quadratum.parts.ButtonPic;
import com.machaon.quadratum.parts.Geom;
import com.machaon.quadratum.parts.Profile;
import com.machaon.quadratum.parts.Tournaments;
import java.io.IOException;
import java.io.InputStream;

public class MultiplayerTournamentSelect implements IScreen {
    private ButtonPic[] button = new ButtonPic[4];
    private Geom geomBackPic;
    private Geom[] geomField = new Geom[4];
    private Geom geomTextTournament;
    private CommonInput inp = new CommonInput();
    private boolean isToast = false;
    private final SpriteBatch spriteBatch = new SpriteBatch();
    private final Texture texBackPic;
    private final Texture texField;
    private Texture texture;
    private int yOffsetField;
    private ButtonPic[] zone = new ButtonPic[4];

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.files.FileHandle, boolean):void
     arg types: [com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.files.FileHandle, int]
     candidates:
      com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.graphics.g2d.TextureRegion, boolean):void
      com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.graphics.g2d.BitmapFont$BitmapFontData, com.badlogic.gdx.graphics.g2d.TextureRegion, boolean):void
      com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.files.FileHandle, boolean):void */
    public MultiplayerTournamentSelect() {
        States.difficulty = Profile.profile.difficulty;
        this.geomBackPic = new Geom(0, 0, 142, 142);
        if (States.screenHeight == 240 && States.screenWidth == 320) {
            this.geomTextTournament = new Geom(0, 225, 0, 0);
            for (byte i = 0; i < 4; i = (byte) (i + 1)) {
                this.button[i] = new ButtonPic((i * 77) + 17, 134, 54, 54, 8, -8);
                this.zone[i] = new ButtonPic((i * 77) + 7, 8, 74, 192);
                this.geomField[i] = new Geom((i * 77) + 27, 93, 35, 23);
            }
            this.yOffsetField = 35;
        }
        if (States.screenHeight == 240 && States.screenWidth == 400) {
            this.geomTextTournament = new Geom(0, 225, 0, 0);
            for (byte i2 = 0; i2 < 4; i2 = (byte) (i2 + 1)) {
                this.button[i2] = new ButtonPic((i2 * 92) + 35, 134, 54, 54, 8, -8);
                this.zone[i2] = new ButtonPic((i2 * 92) + 22, 8, 80, 192);
                this.geomField[i2] = new Geom((i2 * 92) + 44, 93, 35, 23);
            }
            this.yOffsetField = 35;
        }
        if (States.screenHeight == 320) {
            this.geomTextTournament = new Geom(0, 303, 0, 0);
            for (byte i3 = 0; i3 < 4; i3 = (byte) (i3 + 1)) {
                this.button[i3] = new ButtonPic((i3 * 114) + 32, 182, 74, 74, 12, -12);
                this.zone[i3] = new ButtonPic((i3 * 114) + 15, 10, Input.Keys.BUTTON_START, 256);
                this.geomField[i3] = new Geom((i3 * 114) + 34, 125, 70, 46);
            }
            this.yOffsetField = 53;
        }
        if (States.screenHeight == 480) {
            this.geomTextTournament = new Geom(0, 450, 0, 0);
            if (States.screenWidth == 800) {
                for (byte i4 = 0; i4 < 4; i4 = (byte) (i4 + 1)) {
                    this.button[i4] = new ButtonPic((i4 * 180) + 76, 272, (int) Input.Keys.BUTTON_START, (int) Input.Keys.BUTTON_START, 16, -16);
                    this.zone[i4] = new ButtonPic((i4 * 180) + 49, 15, 162, 384);
                    this.geomField[i4] = new Geom((i4 * 180) + 77, 187, Input.Keys.BUTTON_R2, 69);
                }
            } else {
                for (byte i5 = 0; i5 < 4; i5 = (byte) (i5 + 1)) {
                    this.button[i5] = new ButtonPic((i5 * 180) + Input.Keys.BUTTON_R1, 272, (int) Input.Keys.BUTTON_START, (int) Input.Keys.BUTTON_START, 16, -16);
                    this.zone[i5] = new ButtonPic((i5 * 180) + 76, 15, 162, 384);
                    this.geomField[i5] = new Geom((i5 * 180) + Input.Keys.BUTTON_L2, 187, Input.Keys.BUTTON_R2, 69);
                }
            }
            this.yOffsetField = 79;
        }
        this.texBackPic = new Texture(Gdx.files.internal("data/Graph/480/but_multi.png"));
        this.texField = new Texture(Gdx.files.internal(String.valueOf(States.path) + "field_m.png"));
        this.button[0].SetParams(new Texture(Gdx.files.internal(String.valueOf(States.path) + States.TEX_BUTTON_MIDDLE_NA)), new Texture(Gdx.files.internal(String.valueOf(States.path) + States.TEX_BUTTON_MIDDLE_A)), new Texture(Gdx.files.internal(String.valueOf(States.path) + "but_I.png")), (byte) 10);
        this.button[1].SetParams(new Texture(Gdx.files.internal(String.valueOf(States.path) + States.TEX_BUTTON_MIDDLE_NA)), new Texture(Gdx.files.internal(String.valueOf(States.path) + States.TEX_BUTTON_MIDDLE_A)), new Texture(Gdx.files.internal(String.valueOf(States.path) + "but_II.png")), (byte) 10);
        this.button[2].SetParams(new Texture(Gdx.files.internal(String.valueOf(States.path) + States.TEX_BUTTON_MIDDLE_NA)), new Texture(Gdx.files.internal(String.valueOf(States.path) + States.TEX_BUTTON_MIDDLE_A)), new Texture(Gdx.files.internal(String.valueOf(States.path) + "but_III.png")), (byte) 10);
        this.button[3].SetParams(new Texture(Gdx.files.internal(String.valueOf(States.path) + States.TEX_BUTTON_MIDDLE_NA)), new Texture(Gdx.files.internal(String.valueOf(States.path) + States.TEX_BUTTON_MIDDLE_A)), new Texture(Gdx.files.internal(String.valueOf(States.path) + "but_IV.png")), (byte) 10);
        String path = States.screenWidth == 320 ? "data/Graph/240_320/" : States.path;
        for (ButtonPic b : this.zone) {
            b.SetParams(new Texture(Gdx.files.internal(String.valueOf(path) + "zone_m_grid.png")), new Texture(Gdx.files.internal(String.valueOf(path) + "zone_m.png")), (byte) 0);
        }
        this.zone[0].setBackActive(true);
        States.fontBig = new BitmapFont(Gdx.files.internal(String.valueOf(States.path_bigfont) + ".fnt"), Gdx.files.internal(String.valueOf(States.path_bigfont) + ".png"), false);
        States.fontSmall = new BitmapFont(Gdx.files.internal(String.valueOf(States.path_smallfont) + ".fnt"), Gdx.files.internal(String.valueOf(States.path_smallfont) + ".png"), false);
        Gdx.app.getInput().setInputProcessor(this.inp);
        InitObjects();
        this.spriteBatch.setProjectionMatrix(States.viewMatrix);
        if (States.isAntialiasing) {
            for (ButtonPic b2 : this.zone) {
                b2.texBackAct.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
                b2.texBackNonAct.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
            }
            for (ButtonPic b3 : this.button) {
                b3.texBackAct.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
                b3.texBackNonAct.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
                b3.texFront.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
            }
            this.texField.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        }
        this.texBackPic.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
    }

    private void InitObjects() {
        int i;
        byte widthBlackCell = 0;
        Pixmap pixmap = new Pixmap(States.screenWidth, States.screenHeight, Pixmap.Format.RGBA8888);
        int i2 = States.screenWidth > 512 ? 1024 : 512;
        if (States.screenHeight > 512) {
            i = 1024;
        } else {
            i = 512;
        }
        this.texture = new Texture(i2, i, Pixmap.Format.RGBA8888);
        if (States.screenHeight == 240) {
            widthBlackCell = 1;
        }
        if (States.screenHeight == 320) {
            widthBlackCell = 2;
        }
        if (States.screenHeight == 480) {
            widthBlackCell = 3;
        }
        pixmap.setColor(0.0f, 0.0f, 0.0f, 1.0f);
        for (int level = 0; level < 3; level++) {
            InputStream in = Gdx.files.internal("data/Levels/" + Tournaments.getNameLevel((byte) 1, (byte) level)).read();
            try {
                int nBlackCells = in.read();
                int sh = States.screenHeight;
                for (int i3 = 0; i3 < nBlackCells; i3++) {
                    pixmap.fillRectangle(this.geomField[0].x + (in.read() * widthBlackCell), sh - (((this.geomField[0].y + widthBlackCell) + (in.read() * widthBlackCell)) - (this.yOffsetField * level)), widthBlackCell, widthBlackCell);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                in.close();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
        this.texture.draw(pixmap, 0, 0);
        pixmap.dispose();
    }

    public void update() {
        if (this.inp.isBack) {
            if (Profile.profile.settingSounds) {
                States.click.play();
            }
            States.state = 7;
        }
        byte b = this.inp.buttonUp;
        this.inp.getClass();
        if (b == 50) {
            for (int i = 0; i < 4; i++) {
                if (this.zone[i].isBackActive() && isInputCoordsIn(this.button[i])) {
                    States.resetBonuses();
                    Tournaments.setTournament((byte) (i + 1));
                    if (Profile.profile.settingSounds) {
                        States.click.play();
                    }
                    States.state = this.button[i].getState();
                }
            }
        }
        byte b2 = this.inp.buttonDown;
        this.inp.getClass();
        if (b2 == 50) {
            for (int i2 = 0; i2 < 4; i2++) {
                if (this.zone[i2].isBackActive()) {
                    this.button[i2].setBackActive(isInputCoordsIn(this.button[i2]));
                } else if (isInputCoordsIn(this.zone[i2])) {
                    this.isToast = true;
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.g2d.SpriteBatch.draw(com.badlogic.gdx.graphics.Texture, float, float, float, float, int, int, int, int, boolean, boolean):void
     arg types: [com.badlogic.gdx.graphics.Texture, float, int, float, float, int, int, int, int, int, int]
     candidates:
      com.badlogic.gdx.graphics.g2d.SpriteBatch.draw(com.badlogic.gdx.graphics.g2d.TextureRegion, float, float, float, float, float, float, float, float, float, boolean):void
      com.badlogic.gdx.graphics.g2d.SpriteBatch.draw(com.badlogic.gdx.graphics.Texture, float, float, float, float, int, int, int, int, boolean, boolean):void */
    public void render(Application app) {
        app.getGraphics().getGL10().glClearColor(0.85f, 0.85f, 0.85f, 1.0f);
        app.getGraphics().getGL10().glClear(16384);
        this.spriteBatch.begin();
        this.spriteBatch.enableBlending();
        States.fontBig.setColor(1.0f, 1.0f, 1.0f, 1.0f);
        this.spriteBatch.draw(this.texBackPic, (float) ((States.screenWidth / 2) - ((States.screenHeight * 1) / 2)), 0.0f, (float) States.screenHeight, (float) States.screenHeight, this.geomBackPic.x, this.geomBackPic.y, this.geomBackPic.width, this.geomBackPic.height, false, false);
        States.renderBackground(this.spriteBatch);
        for (ButtonPic b : this.button) {
            this.spriteBatch.draw(b.getBackTexture(), (float) b.x, (float) b.y, 0, 0, b.width, b.height);
            this.spriteBatch.draw(b.texFront, (float) (b.x + b.frontX), (float) (b.y + b.frontY), 0, 0, b.width, b.height);
        }
        for (byte i = 0; i < 4; i = (byte) (i + 1)) {
            for (byte l = 0; l < 3; l = (byte) (l + 1)) {
                this.spriteBatch.draw(this.texField, (float) this.geomField[i].x, (float) (this.geomField[i].y - (this.yOffsetField * l)), 0, 0, this.geomField[i].width, this.geomField[i].height);
            }
        }
        this.spriteBatch.draw(this.texture, 0.0f, 0.0f, 0, 0, States.screenWidth, States.screenHeight);
        for (ButtonPic b2 : this.zone) {
            this.spriteBatch.draw(b2.getBackTexture(), (float) b2.x, (float) b2.y, 0, 0, b2.width, b2.height);
        }
        States.fontBig.drawMultiLine(this.spriteBatch, States.TEXT_TOURNAMENT[States.language], 0.0f, (float) this.geomTextTournament.y, (float) States.screenWidth, BitmapFont.HAlignment.CENTER);
        if (this.isToast) {
            States.fontSmall.setColor(1.0f, 1.0f, 1.0f, 1.0f);
            States.fontSmall.drawMultiLine(this.spriteBatch, States.TEXT_TOURNAMENTS[States.language], (float) this.zone[1].x, ((float) (this.zone[0].y + (this.zone[0].height / 2))) + States.fontBig.getCapHeight(), (float) ((this.zone[3].x + this.zone[3].width) - this.zone[1].x), BitmapFont.HAlignment.CENTER);
        }
        this.spriteBatch.end();
    }

    public void dispose() {
        this.spriteBatch.dispose();
        for (ButtonPic b : this.button) {
            b.dispose();
        }
        for (ButtonPic b2 : this.zone) {
            b2.dispose();
        }
        this.texBackPic.dispose();
        this.texture.dispose();
    }

    public void resume() {
        this.texture.dispose();
        InitObjects();
    }

    private boolean isInputCoordsIn(Geom geom) {
        if (((float) this.inp.x) <= ((float) (geom.x + States.cameraOffsetX)) * States.aspectX || ((float) this.inp.x) >= ((float) (geom.x + geom.width + States.cameraOffsetX)) * States.aspectX || ((float) (States.displayHeight - this.inp.y)) <= ((float) (geom.y + States.cameraOffsetY)) * States.aspectY || ((float) (States.displayHeight - this.inp.y)) >= ((float) (geom.y + geom.height + States.cameraOffsetY)) * States.aspectY) {
            return false;
        }
        return true;
    }
}
