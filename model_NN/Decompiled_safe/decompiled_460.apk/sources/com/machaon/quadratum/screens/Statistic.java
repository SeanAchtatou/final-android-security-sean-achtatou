package com.machaon.quadratum.screens;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.machaon.quadratum.IScreen;
import com.machaon.quadratum.States;
import com.machaon.quadratum.input.SlideInput;
import com.machaon.quadratum.parts.ButtonText;
import com.machaon.quadratum.parts.Geom;
import com.machaon.quadratum.parts.Keyboard;
import com.machaon.quadratum.parts.Profile;

public class Statistic implements IScreen {
    public static final byte A_BORN_WIN = 2;
    public static final byte A_BRONZE_WIN = 4;
    public static final byte A_CAP_THINK = 0;
    public static final byte A_CAP_WIN = 1;
    public static final byte A_COLLECTOR = 7;
    public static final byte A_FREEZER = 9;
    public static final byte A_GOLD_WIN = 6;
    public static final byte A_JUMPER = 11;
    public static final byte A_SHARPER = 10;
    public static final byte A_SHOT_FIRER = 8;
    public static final byte A_SILVER_WIN = 5;
    public static final byte A_SQ_HEAD = 3;
    public static final String[][] NAME = {new String[]{"Capable of thinking", "Capable of winning", "Born to win", "Square head", "Bronze Winner", "Silver Winner", "Gold Winner", "Collector", "Shot-firer", "Freezer", "Sharper", "Jumper"}, new String[]{"Способный думать", "Способный побеждать", "Рожденный побеждать", "Квадратоголовый", "Бронзовый Победитель", "Серебряный Победитель", "Золотой Победитель", "Коллекционер", "Подрывник", "Замораживатель", "Шулер", "Прыгун"}};
    public static final byte NUMBER_OF_ACHIEV = 12;
    public static final byte POS_ACHIEV = 1;
    public static final byte POS_STATS = 2;
    public static final String[][] USL = {new String[]{"Win 1 round", "Win 10 rounds", "Win 100 rounds", "Win 500 rounds", "Win 10 tournaments on easy difficulty", "Win 10 tournaments on normal difficulty", "Win 10 tournaments on hard difficulty", "At the end of tournament more than 10 bonuses on warehouse", "Explode the dynamites!", "Freeze your rivals!", "Shuffle the field!", "Teleport!"}, new String[]{"Выиграйте 1 раунд", "Выиграйте 10 раундов", "Выиграйте 100 раундов", "Выиграйте 500 раундов", "Выиграйте 10 турниров на легкой сложности", "Выиграйте 10 турниров на нормальной сложности", "Выиграйте 10 турниров на высокой сложности", "В конце турнира на складе больше 10 бонусов", "Взрывайте динамиты!", "Замораживайте соперников!", "Перемешивайте поле!", "Телепортируйтесь!"}};
    private int a_height;
    private float a_offsetY;
    private int a_width;
    private ButtonText[] buttonPos = new ButtonText[2];
    private SlideInput inp = new SlideInput();
    private boolean isBegin = false;
    private boolean isEnd = false;
    private int line = 0;
    private int lineWidth;
    private byte offsetX;
    private float offsetY;
    private byte pos;
    private float s_offsetY;
    private final SpriteBatch spriteBatch = new SpriteBatch();
    private int stopY = 10000;
    private final Texture texAchiev;
    private final Texture texLine;

    public Statistic(States states) {
        String path = States.PATH_GRAPH + States.resolution + "/";
        this.texAchiev = new Texture(Gdx.files.internal(String.valueOf(path) + "achiev_no.png"));
        this.texLine = new Texture(Gdx.files.internal(String.valueOf(path) + "line.png"));
        Profile.setCurrentProfile(Profile.profile.numberOfProfile);
        if (States.screenHeight == 240 && States.screenWidth == 320) {
            this.offsetX = 10;
            this.a_width = 54;
            this.a_height = 54;
            this.lineWidth = 300;
            this.buttonPos[0] = new ButtonText(4, 213, 152, 21, 16);
            this.buttonPos[1] = new ButtonText(164, 213, 152, 21, 16);
        }
        if (States.screenHeight == 240 && States.screenWidth == 400) {
            this.offsetX = Keyboard.STATE_NUMBER;
            this.a_width = 54;
            this.a_height = 54;
            this.lineWidth = 300;
            this.buttonPos[0] = new ButtonText(26, 213, 170, 21, 16);
            this.buttonPos[1] = new ButtonText(204, 213, 170, 21, 16);
        }
        if (States.screenHeight == 320) {
            this.offsetX = 30;
            this.a_width = 96;
            this.a_height = 96;
            this.lineWidth = 440;
            this.buttonPos[0] = new ButtonText(10, 285, 226, 28, 21);
            this.buttonPos[1] = new ButtonText(Input.Keys.F1, 285, 226, 28, 21);
        }
        if (States.screenHeight == 480) {
            this.offsetX = 45;
            this.a_width = 96;
            this.a_height = 96;
            this.lineWidth = 700;
            if (States.screenWidth == 800) {
                this.buttonPos[0] = new ButtonText(41, 427, 350, 42, 30);
                this.buttonPos[1] = new ButtonText(409, 427, 350, 42, 30);
            } else {
                this.buttonPos[0] = new ButtonText(61, 427, 350, 42, 30);
                this.buttonPos[1] = new ButtonText(443, 427, 350, 42, 30);
            }
        }
        path = States.screenWidth == 320 ? "data/Graph/240_320/" : path;
        this.buttonPos[0].SetParams(States.TEXT_ACHIEVMENTS[States.language], new Texture(Gdx.files.internal(String.valueOf(path) + "but_name_2_na.png")), new Texture(Gdx.files.internal(String.valueOf(path) + "but_name_2_a.png")), (byte) 1);
        this.buttonPos[1].SetParams(States.TEXT_STATISTICS[States.language], new Texture(Gdx.files.internal(String.valueOf(path) + "but_name_2_na.png")), new Texture(Gdx.files.internal(String.valueOf(path) + "but_name_2_a.png")), (byte) 2);
        Gdx.app.getInput().setInputProcessor(this.inp);
        this.spriteBatch.setProjectionMatrix(States.viewMatrix);
        this.pos = 2;
    }

    public void update() {
        if (this.inp.isBack) {
            if (Profile.profile.settingSounds) {
                States.click.play();
            }
            States.state = 3;
        }
        byte b = this.inp.buttonUp;
        this.inp.getClass();
        if (b == 50) {
            for (ButtonText b2 : this.buttonPos) {
                if (isInputCoordsInSpec(b2)) {
                    this.pos = b2.state;
                    for (ButtonText b22 : this.buttonPos) {
                        b22.setBackActive(false);
                    }
                    this.a_offsetY = 0.0f;
                    this.s_offsetY = 0.0f;
                    this.inp.speed = 0.0f;
                    this.inp.delta = 0.0f;
                }
            }
        }
        byte b3 = this.inp.buttonDown;
        this.inp.getClass();
        if (b3 == 50) {
            for (ButtonText b4 : this.buttonPos) {
                b4.setBackActive(isInputCoordsInSpec(b4));
            }
        }
        if (this.pos == 2) {
            updateStats();
        }
        if (this.pos == 1) {
            updateAchiev();
        }
    }

    public void updateStats() {
        this.buttonPos[1].setBackActive(true);
        this.s_offsetY -= this.inp.delta;
        this.inp.delta = 0.0f;
        if (this.inp.speed != 0.0f) {
            this.inp.speed /= 1.05f;
            if (((double) Math.abs(this.inp.speed)) < 0.01d) {
                this.inp.speed = 0.0f;
            }
            this.s_offsetY -= this.inp.speed;
        }
        if (this.s_offsetY < 0.0f) {
            if (!this.isBegin) {
                if (Profile.profile.settingVibrate) {
                    Gdx.input.vibrate(20);
                }
                this.isBegin = true;
            }
            this.inp.speed = 0.0f;
            this.s_offsetY = 0.0f;
        } else {
            this.isBegin = false;
        }
        if (this.s_offsetY > ((float) this.stopY)) {
            if (!this.isEnd) {
                if (Profile.profile.settingVibrate) {
                    Gdx.input.vibrate(20);
                }
                this.isEnd = true;
            }
            this.inp.speed = 0.0f;
            this.s_offsetY = (float) this.stopY;
        } else {
            this.isEnd = false;
        }
        this.offsetY = this.s_offsetY;
    }

    public void updateAchiev() {
        this.buttonPos[0].setBackActive(true);
        this.a_offsetY -= this.inp.delta;
        this.inp.delta = 0.0f;
        if (this.inp.speed != 0.0f) {
            this.inp.speed /= 1.05f;
            if (((double) Math.abs(this.inp.speed)) < 0.01d) {
                this.inp.speed = 0.0f;
            }
            this.a_offsetY -= this.inp.speed;
        }
        if (this.a_offsetY < 0.0f) {
            if (!this.isBegin) {
                if (Profile.profile.settingVibrate) {
                    Gdx.input.vibrate(20);
                }
                this.isBegin = true;
            }
            this.inp.speed = 0.0f;
            this.a_offsetY = 0.0f;
        } else {
            this.isBegin = false;
        }
        if (this.a_offsetY > ((float) this.stopY)) {
            if (!this.isEnd) {
                if (Profile.profile.settingVibrate) {
                    Gdx.input.vibrate(20);
                }
                this.isEnd = true;
            }
            this.inp.speed = 0.0f;
            this.a_offsetY = (float) this.stopY;
        } else {
            this.isEnd = false;
        }
        this.offsetY = this.a_offsetY;
    }

    public void render(Application app) {
        app.getGraphics().getGL10().glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
        app.getGraphics().getGL10().glClear(16384);
        this.spriteBatch.begin();
        this.spriteBatch.enableBlending();
        States.renderBackground(this.spriteBatch);
        this.line = (int) (States.fontSmall.getCapHeight() * 1.4f);
        States.fontBig.setColor(0.5f, 0.5f, 1.0f, 1.0f);
        States.fontBig.drawMultiLine(this.spriteBatch, Profile.profile.name, 0.0f, (float) getHeight(), (float) States.screenWidth, BitmapFont.HAlignment.CENTER);
        this.line += (int) (States.fontSmall.getCapHeight() * 5.0f);
        this.spriteBatch.draw(this.buttonPos[0].getBackTexture(), (float) this.buttonPos[0].x, (float) getHeight(), 0, 0, this.buttonPos[0].width, this.buttonPos[0].height);
        States.fontSmall.setColor(1.0f, 1.0f, 1.0f, 1.0f);
        States.fontSmall.drawMultiLine(this.spriteBatch, this.buttonPos[0].getString(), (float) this.buttonPos[0].x, (float) (this.buttonPos[0].getTextOffsetY() + getHeight()), (float) this.buttonPos[0].width, BitmapFont.HAlignment.CENTER);
        this.spriteBatch.draw(this.buttonPos[1].getBackTexture(), (float) this.buttonPos[1].x, (float) getHeight(), 0, 0, this.buttonPos[1].width, this.buttonPos[1].height);
        States.fontSmall.drawMultiLine(this.spriteBatch, this.buttonPos[1].getString(), (float) this.buttonPos[1].x, (float) (this.buttonPos[1].getTextOffsetY() + getHeight()), (float) this.buttonPos[1].width, BitmapFont.HAlignment.CENTER);
        if (this.pos == 2) {
            renderStats();
        }
        if (this.pos == 1) {
            renderAchiev();
        }
        this.spriteBatch.end();
    }

    private void renderAchiev() {
        incLine(1.0f);
        States.fontSmall.setColor(1.0f, 0.0f, 0.0f, 1.0f);
        States.fontSmall.draw(this.spriteBatch, States.TEXT_LITE_ACHIEV[States.language], (float) this.offsetX, (float) ((int) (((float) getHeight()) - (States.fontSmall.getCapHeight() / 2.0f))));
        incLine(2.0f);
        int lineTemp = this.line;
        for (int i = 0; i < 12; i++) {
            if (((float) (this.line + this.a_height)) > this.offsetY) {
                if (this.offsetY + ((float) States.screenHeight) <= ((float) this.line)) {
                    break;
                }
                this.spriteBatch.draw(this.texAchiev, (float) this.offsetX, (float) (getHeight() - this.a_height), 0, 0, this.a_width, this.a_height);
                this.line = (int) (((float) this.line) + ((float) this.a_height) + (States.fontSmall.getCapHeight() * 2.0f));
            } else {
                this.line = (int) (((float) this.line) + ((float) this.a_height) + (States.fontSmall.getCapHeight() * 2.0f));
            }
        }
        this.line = lineTemp;
        for (int i2 = 0; i2 < 12; i2++) {
            if (((float) (this.line + this.a_height)) > this.offsetY) {
                if (this.offsetY + ((float) States.screenHeight) <= ((float) this.line)) {
                    break;
                }
                States.fontSmall.setColor(1.0f, 1.0f, 1.0f, 1.0f);
                States.fontSmall.draw(this.spriteBatch, NAME[States.language][i2], (float) ((this.offsetX * 2) + this.a_width), (float) ((int) (((float) getHeight()) - (States.fontSmall.getCapHeight() / 2.0f))));
                States.fontSmall.setColor(0.7f, 0.7f, 0.7f, 1.0f);
                States.fontSmall.drawWrapped(this.spriteBatch, USL[States.language][i2], (float) ((this.offsetX * 2) + this.a_width), (float) ((int) (((float) getHeight()) - (States.fontSmall.getCapHeight() * 2.5f))), (float) ((States.screenWidth - (this.offsetX * 3)) - this.a_width));
                this.line = (int) (((float) this.line) + ((float) this.a_height) + (States.fontSmall.getCapHeight() * 2.0f));
            } else {
                this.line = (int) (((float) this.line) + ((float) this.a_height) + (States.fontSmall.getCapHeight() * 2.0f));
            }
        }
        this.line = lineTemp;
        int i3 = 0;
        while (true) {
            if (i3 >= 12) {
                break;
            }
            this.line += this.a_height;
            if (((float) (this.line + this.a_height)) <= this.offsetY) {
                this.line = (int) (((float) this.line) + (States.fontSmall.getCapHeight() * 2.0f));
            } else if (this.offsetY + ((float) States.screenHeight) <= ((float) this.line)) {
                this.line = (int) (((float) this.line) + ((((float) this.a_height) + (States.fontSmall.getCapHeight() * 2.0f)) * ((float) (12 - i3))));
                break;
            } else {
                if (i3 != 11) {
                    this.spriteBatch.draw(this.texLine, (float) ((States.screenWidth - this.lineWidth) / 2), (float) ((int) (((float) getHeight()) - (States.fontSmall.getCapHeight() * 1.5f))), 0, 0, this.lineWidth, this.texLine.getHeight());
                }
                this.line = (int) (((float) this.line) + (States.fontSmall.getCapHeight() * 2.0f));
            }
            i3++;
        }
        this.stopY = this.line - States.screenHeight;
    }

    private void renderStats() {
        this.line = (int) (States.fontSmall.getCapHeight() * 5.0f);
        States.fontSmall.setColor(0.7f, 0.7f, 1.0f, 1.0f);
        writeText(States.TEXT_SINGLE_MODE[States.language], 2.0f);
        States.fontSmall.setColor(1.0f, 1.0f, 1.0f, 1.0f);
        writeText(String.valueOf(States.TEXT_PLAYED_ROUNDS[States.language]) + Profile.profile.nPlayedRounds, 1.5f);
        writeText(String.valueOf(States.TEXT_WON_ROUNDS[States.language]) + Profile.profile.nWonRounds, 1.0f);
        if (Profile.profile.nPlayedRounds == 0) {
            writeText(String.valueOf(States.TEXT_WIN_PERCENTAGE[States.language]) + "0", 1.0f);
        } else {
            writeText(String.valueOf(States.TEXT_WIN_PERCENTAGE[States.language]) + ((Profile.profile.nWonRounds * 100) / Profile.profile.nPlayedRounds) + "%", 1.0f);
        }
        writeText(String.valueOf(States.TEXT_ABANDONED[States.language]) + Profile.profile.nGamesAbandoned, 1.0f);
        writeText(String.valueOf(States.TEXT_MAX_SCORES_PER_R[States.language]) + " 1:  " + Profile.profile.maxScoresOnRound[0], 1.0f);
        writeText(String.valueOf(States.TEXT_MAX_SCORES_PER_R[States.language]) + " 2:  " + Profile.profile.maxScoresOnRound[1], 1.0f);
        writeText(String.valueOf(States.TEXT_MAX_SCORES_PER_R[States.language]) + " 3:  " + Profile.profile.maxScoresOnRound[2], 1.0f);
        writeText(String.valueOf(States.TEXT_PLAYED_TOURS[States.language]) + Profile.profile.nPlayedTours, 1.5f);
        writeText(String.valueOf(States.TEXT_WON_TOURS[States.language]) + Profile.profile.nWinTours, 1.0f);
        writeText(String.valueOf(States.TEXT_MAX_SCORES_PER_T[States.language]) + " 1:  " + Profile.profile.maxScoresOnTour[0], 1.0f);
        writeText(String.valueOf(States.TEXT_MAX_SCORES_PER_T[States.language]) + " 2:  " + Profile.profile.maxScoresOnTour[1], 1.0f);
        writeText(String.valueOf(States.TEXT_MAX_SCORES_PER_T[States.language]) + " 3:  " + Profile.profile.maxScoresOnTour[2], 1.0f);
        writeText(String.valueOf(States.TEXT_GOLD_CUPS[States.language]) + Profile.profile.nGoldCups, 1.0f);
        writeText(String.valueOf(States.TEXT_FOUND_BONUSES[States.language]) + Profile.profile.nFindedBonuses, 1.5f);
        writeText(States.TEXT_USED_BONUSES[States.language], 1.0f);
        writeText(String.valueOf(States.TEXT_USED_EYE[States.language]) + Profile.profile.nEye, 1.0f);
        writeText(String.valueOf(States.TEXT_USED_FROST[States.language]) + Profile.profile.nFreezes, 1.0f);
        writeText(String.valueOf(States.TEXT_USED_MIXINGS[States.language]) + Profile.profile.nMixer, 1.0f);
        writeText(String.valueOf(States.TEXT_USED_BLIND[States.language]) + Profile.profile.nBlind, 1.0f);
        writeText(String.valueOf(States.TEXT_USED_TELEPORTS[States.language]) + Profile.profile.nTeleport, 1.0f);
        writeText(String.valueOf(States.TEXT_USED_EXPLOSIVES[States.language]) + Profile.profile.nBlowedBombs, 1.0f);
        States.fontSmall.setColor(0.7f, 0.7f, 1.0f, 1.0f);
        writeText(States.TEXT_MULTI_MODE[States.language], 2.0f);
        States.fontSmall.setColor(1.0f, 1.0f, 1.0f, 1.0f);
        writeText(String.valueOf(States.TEXT_PLAYED_ROUNDS[States.language]) + Profile.profile.multi_nPlayedRounds, 1.5f);
        writeText(String.valueOf(States.TEXT_WON_ROUNDS[States.language]) + Profile.profile.multi_nWonRounds, 1.0f);
        if (Profile.profile.multi_nPlayedRounds == 0) {
            writeText(String.valueOf(States.TEXT_WIN_PERCENTAGE[States.language]) + "0", 1.0f);
        } else {
            writeText(String.valueOf(States.TEXT_WIN_PERCENTAGE[States.language]) + ((Profile.profile.multi_nWonRounds * 100) / Profile.profile.multi_nPlayedRounds) + "%", 1.0f);
        }
        writeText(String.valueOf(States.TEXT_ABANDONED[States.language]) + Profile.profile.multi_nGamesAbandoned, 1.0f);
        writeText(String.valueOf(States.TEXT_PLAYED_TOURS[States.language]) + Profile.profile.multi_nPlayedTours, 1.5f);
        writeText(String.valueOf(States.TEXT_WON_TOURS[States.language]) + Profile.profile.multi_nWinTours, 1.0f);
        writeText(String.valueOf(States.TEXT_WON_PEOPLES[States.language]) + Profile.profile.multi_nWonMans, 1.0f);
        writeText(String.valueOf(States.TEXT_FOUND_BONUSES[States.language]) + Profile.profile.multi_nFindedBonuses, 1.5f);
        writeText(States.TEXT_USED_BONUSES[States.language], 1.0f);
        writeText(String.valueOf(States.TEXT_USED_EYE[States.language]) + Profile.profile.multi_nEye, 1.0f);
        writeText(String.valueOf(States.TEXT_USED_FROST[States.language]) + Profile.profile.multi_nFreezes, 1.0f);
        writeText(String.valueOf(States.TEXT_USED_MIXINGS[States.language]) + Profile.profile.multi_nMixer, 1.0f);
        writeText(String.valueOf(States.TEXT_USED_BLIND[States.language]) + Profile.profile.multi_nBlind, 1.0f);
        writeText(String.valueOf(States.TEXT_USED_TELEPORTS[States.language]) + Profile.profile.multi_nTeleport, 1.0f);
        writeText(String.valueOf(States.TEXT_USED_EXPLOSIVES[States.language]) + Profile.profile.multi_nBlowedBombs, 1.0f);
        incLine(3.0f);
        this.stopY = this.line - States.screenHeight;
    }

    public void dispose() {
        this.spriteBatch.dispose();
        this.texLine.dispose();
        this.texAchiev.dispose();
        for (ButtonText b : this.buttonPos) {
            b.dispose();
        }
    }

    public void resume() {
    }

    private void writeText(String text, float height) {
        incLine(height);
        if (((float) this.line) + (States.fontSmall.getCapHeight() * 1.5f) >= this.offsetY && this.offsetY + ((float) States.screenHeight) >= ((float) this.line)) {
            States.fontSmall.draw(this.spriteBatch, text, (float) this.offsetX, (float) getHeight());
        }
    }

    private int getHeight() {
        return (int) ((this.offsetY + ((float) States.screenHeight)) - ((float) this.line));
    }

    private void incLine(float n) {
        this.line += (int) (States.fontSmall.getCapHeight() * 1.6f * n);
    }

    private boolean isInputCoordsInSpec(Geom geom) {
        if (((float) this.inp.x) <= ((float) (geom.x + States.cameraOffsetX)) * States.aspectX || ((float) this.inp.x) >= ((float) (geom.x + geom.width + States.cameraOffsetX)) * States.aspectX || ((float) this.inp.y) <= (((States.fontSmall.getCapHeight() * 6.4f) - ((float) geom.height)) - this.offsetY) * States.aspectY || ((float) this.inp.y) >= ((States.fontSmall.getCapHeight() * 6.4f) - this.offsetY) * States.aspectY) {
            return false;
        }
        this.inp.speed = 0.0f;
        this.inp.delta = 0.0f;
        return true;
    }
}
