package com.machaon.quadratum.screens;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFontCache;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.machaon.quadratum.IScreen;
import com.machaon.quadratum.States;
import com.machaon.quadratum.input.SlideInput;
import com.machaon.quadratum.parts.Profile;

public class Help implements IScreen {
    private static String[] TEXT_1 = {"      Quadratum is a puzzle game, the goal is to occupy more squares than your rival. The occupation of squares is realized per consecutive joining of squares which are next to the player. At the beginning of round the players are in the corners of the field. Color selection is realized by using of the buttons which are on the right of the field. Colors which are unavailable at the moment are painted in a dark color.", "      Quadratum - логическая игра. Цель игры захватить больше квадратиков на поле, чем соперник. Захват поля осуществляется последовательным поглощением соседних с игроком квадратиков. В начале раунда игроки расположены в углах поля. Выбор цвета осуществляется посредством кнопок справа от поля. Недоступные на текущий момент цвета окрашены в темный цвет."};
    private static String[] TEXT_10 = {"      To start to play Quadratum you should make profile. Profile is necessary to accumulate statistics of game. Statistics is accumulated over a period of all games. You can make 6 independent profiles.", "      Чтобы начать играть в Quadratum, создайте сначала себе профиль. Профиль нужен, чтобы накапливать статистику проведенных игр. Статистика накапливается на протяжении всех игр. Всего можно создать 6 независимых профилей."};
    private static String[] TEXT_11 = {"      There are 2 modes: one player and multiplayer.\n      To start game in one player mode press in main menu the button 'play', then button with one little man. You pass to menu 'setting of tournament'. Choose the difficulty level of artificial intelligence, then available tournament (in the Lite version only one tournament is available). After that you pass to score window, there you can find names of players, their points for one round and for whole tournament. In this window you can see, who makes the first move and miniature of the next round field. To start the game click on window. After the whole field will be occupied, the score of round will be shown, you can start the new round. After three rounds in the score window the winner will be called and he will be present with a cup.", "      В игре существует 2 режима игры: одиночный режим и многопользовательский.\n      Чтобы начать играть в одиночном режиме, выберите свой профиль, нажмите в главном меню кнопку 'Играть', затем кнопку с одним человечком. Попадете в меню настройки турнира. Выберите сложность искусственного интеллекта, затем доступный для игры турнир (в Lite версии доступен только один турнир). После этого попадете в окно просмотра результатов игры, в котором можно увидеть имена играющих, их очки за отдельный раунд и турнир в целом. Также в этом окне сообщается, кто будет ходить первым и показывается миниатюра игрового поля следующего раунда. Кликнув по окну начнется игра. \n"};
    private static String[] TEXT_12 = {"      By one player mode the player can get a cup too: for wining a victory over the first difficulty level artificial intelligence you get a bronze cup, the second – silver cup, the third – gold cup. The next tournament opens independent of by which difficulty level you wined the tournament (unavailable in Lite version).", "      После того как будет захвачено всё игровое поле, будут показаны результаты раунда и можно начинать новый раунд. По истечении трех раундов в окне результатов будет объявлен победитель и вручен ему кубок. Игроку тоже присваивается кубок, зависящий от того, на какой сложности он играл.\n       За победу ИИ 1-ого уровня дается бронзовый кубок, 2-ого уровня - серебряный, 3-его уровня - высшая награда - золотой кубок. Следующий турнир открывается независимо от того, какой сложности был выигран турнир (недоступно в Lite версии)."};
    private static String[] TEXT_13 = {"      To start the game in multiplayer mode it’s not necessary to make a profile. Press the button 'play' in the main menu, then the button with four little men.\tYou pass to menu 'setting of tournament'. You can choose the number of players and who will play in the round. To choose the players click on names in the corner as the result a window opens. In this window you can choose one of profiles or a bot with any difficulty level or make a new player with new name but a new profile will not be created.", "      Для игры в многопользовательском режиме выбирать свой профиль необязательно. нажмите в главном меню кнопку 'Играть', затем кнопку с четырьмя человечками. Попадете в меню настройки турнира. Вы можете выбрать количество игроков, а также кто будет играть в турнире. Для выбора игроков, кликайте по именам в углу, при этом откроется окно. В этом окне вы можете выбрать один из существующих профилей или же выбрать бота с любым уровнем сложности или создать игрока с новым именем, но профиль при этом не создастся."};
    private static String[] TEXT_14 = {"      In the full version you can choose mode without bonuses.\n      After you finish setting click 'play' and you pass to window 'choose tournament'. Choose any tournament (in the Lite version only one tournament is available).\t\n      Playing process is analogous to one player mode. The winner gets a gold cup.", "      В полной версии игры Вы можете выбрать режим игры без бонусов.\n      После того, как закончите настройку, нажмите 'Играть' и попадете в окно выбора турнира. Выберете любой понравившийся Вам турнир (в Lite версии доступен 1 турнир). \n      Игровой процесс аналогичен игровому процессу одиночного режима. Победителю достанется золотой кубок."};
    private static String[] TEXT_2 = {"      By choosing of color the neighboring squares of the selected color are occupied. Black squares are obstacles, their occupation is impossible.\n      The bonuses are scattered under free squares on the field. After occupation of this square the bonus will be transferred to the player’s warehouse, the place, where the bonus was hidden, will be highlighted. Warehouse with bonuses can be accessed by clicking on the button with three stars, or by clicking 'menu' of apparatus.", "      При выборе цвета захватываются соседние квадраты выбранного цвета. Черные клетки на поле это препятствия, их захватить невозможно. \n      На поле раскиданы бонусы, находящиеся под свободными квадратиками. Как только будет захвачен такой квадратик, бонус перенесется в склад игрока, а место, где был бонус подсветится. Склад с бонусами можно открыть, щелкнув по кнопке с тремя звездочками, или нажав кнопку 'menu' аппарата."};
    private static String[] TEXT_3 = {"      Description of bonuses:", "      Описание бонусов:"};
    private static String[] TEXT_4 = {"'Eye': shows player for 5 seconds where are which bonuses. The move keeps by player.", "'Глаз': показывает игроку на протяжении 5 секунд где и какие бонусы находятся на поле. Ход сохраняется у игрока."};
    private static String[] TEXT_5 = {"'Freezing': freezes all rivals for one move, lets player make 2 moves, after that the move passes to the other player.", "'Заморозка': замораживает всех соперников на 1 ход, позволяя игроку сделать 2 хода. После чего ход переходит другому."};
    private static String[] TEXT_6 = {"'Rubik's cube': shuffles the colors of free squares. The move keeps by player.", "'Кубик Рубика': меняет цвета свободных ячеек на поле случайным образом. Ход сохраняется у игрока."};
    private static String[] TEXT_61 = {"'Blindness': all free cells are closed by an opaque mask on 3 moves for each player, complicating a color choice. At 'blindness' it is possible to apply any bonus in addition. The move keeps by player.", "'Слепота': все свободные ячейки закрываются непрозрачной маской на 3 хода для каждого игрока, затрудняя выбор цвета. При 'слепоте' можно дополнительно применять любой бонус. Ход сохраняется у игрока."};
    private static String[] TEXT_7 = {"'Portal': teleports the player to any square. After that the move passes to the other player.", "'Портал': телепортирует игрока в любую ячейку на поле. После применения ход переходит другому."};
    private static String[] TEXT_8 = {"'Dynamite': after putting on necessary place it explodes in 3 seconds with 5 squares blasting radius. On this place appears free squares of random colors. After that the move passes to the other player. Note: The installation location of dynamite can be adjusted, while there is a countdown timer.", "'Динамит': после установки динамита в нужном месте он взрывается через 3 секунды с радиусом в 5 ячеек. На месте взрыва появляются свободные ячейки случайного цвета. После применения ход переходит другому. Примечание: место установки динамита можно корректировать, пока идет обратный отсчет таймера."};
    private static String[] TEXT_9 = {"      Press anywhere on the field to see the boundaries of the players. At the same time free squares darken, so you can see, for example, where the player teleported.", "      Чтобы увидеть границы игроков, нажмите в любое место на поле. При этом свободные ячейки затемнятся, тем самым легко будет видно, например, куда телепортировался игрок."};
    private static String[] TEXT_MANUAL = {"Manual", "Руководство"};
    private final byte POS_HELP = 2;
    private final byte POS_START = 1;
    private BitmapFontCache bfc11;
    private BitmapFontCache bfc12;
    private BitmapFontCache bfc13;
    private BitmapFontCache bfc14;
    private int bonusWidth;
    private int font_cap_height;
    private int font_cap_height_x_1_2;
    private SlideInput inp;
    private boolean isBegin = false;
    private boolean isEnd = false;
    private boolean isStart = false;
    private int line;
    private int offsetX;
    private int offsetXx2;
    private int offsetXx3;
    private float offsetY = 0.0f;
    private int picHeight1;
    private int picHeight2;
    private int picWidth1;
    private int picWidth2;
    private byte pos = 1;
    private final SpriteBatch spriteBatch = new SpriteBatch();
    private int stopY = 10000;
    private int tb1;
    private int tb10;
    private int tb11;
    private int tb12;
    private int tb13;
    private int tb14;
    private int tb2;
    private int tb8;
    private int tb9;
    private Texture[] texManual = new Texture[3];

    public Help() {
        if (States.screenHeight == 240) {
            this.bonusWidth = 48;
        }
        if (States.screenHeight == 320) {
            this.bonusWidth = 64;
        }
        if (States.screenHeight == 480) {
            this.bonusWidth = 96;
        }
        this.inp = new SlideInput();
        Gdx.app.getInput().setInputProcessor(this.inp);
        this.spriteBatch.setProjectionMatrix(States.viewMatrix);
        InitObjects();
        this.offsetX = (int) (States.fontSmall.getSpaceWidth() * 2.0f);
        this.inp.delta = 0.0f;
        this.inp.speed = 0.0f;
        this.offsetXx2 = this.offsetX * 2;
        this.offsetXx3 = this.offsetX * 3;
        this.font_cap_height = (int) States.fontSmall.getCapHeight();
        this.font_cap_height_x_1_2 = (int) (((float) this.font_cap_height) * 1.2f);
        this.tb14 = (int) States.fontSmall.getWrappedBounds(TEXT_14[States.language], (float) (States.screenWidth - this.offsetXx2)).height;
        this.tb13 = (int) States.fontSmall.getWrappedBounds(TEXT_13[States.language], (float) (States.screenWidth - this.offsetXx2)).height;
        this.tb12 = (int) States.fontSmall.getWrappedBounds(TEXT_12[States.language], (float) (States.screenWidth - this.offsetXx2)).height;
        this.tb11 = (int) States.fontSmall.getWrappedBounds(TEXT_11[States.language], (float) (States.screenWidth - this.offsetXx2)).height;
        this.tb10 = (int) States.fontSmall.getWrappedBounds(TEXT_10[States.language], (float) (States.screenWidth - this.offsetXx2)).height;
        this.tb9 = (int) States.fontSmall.getWrappedBounds(TEXT_9[States.language], (float) (States.screenWidth - this.offsetXx2)).height;
        this.tb8 = (int) States.fontSmall.getWrappedBounds(TEXT_8[States.language], (float) ((States.screenWidth - (this.offsetX * 4)) - this.bonusWidth)).height;
        this.tb2 = (int) States.fontSmall.getWrappedBounds(TEXT_2[States.language], (float) (States.screenWidth - this.offsetXx2)).height;
        this.tb1 = (int) States.fontSmall.getWrappedBounds(TEXT_1[States.language], (float) (States.screenWidth - this.offsetXx2)).height;
        this.bfc14 = new BitmapFontCache(States.fontSmall);
        this.bfc14.setWrappedText(TEXT_14[States.language], (float) this.offsetX, (float) getHeight(), (float) (States.screenWidth - this.offsetXx2), BitmapFont.HAlignment.LEFT);
        this.bfc13 = new BitmapFontCache(States.fontSmall);
        this.bfc13.setWrappedText(TEXT_13[States.language], (float) this.offsetX, (float) getHeight(), (float) (States.screenWidth - this.offsetXx2), BitmapFont.HAlignment.LEFT);
        this.bfc12 = new BitmapFontCache(States.fontSmall);
        this.bfc12.setWrappedText(TEXT_12[States.language], (float) this.offsetX, (float) getHeight(), (float) (States.screenWidth - this.offsetXx2), BitmapFont.HAlignment.LEFT);
        this.bfc11 = new BitmapFontCache(States.fontSmall);
        this.bfc11.setWrappedText(TEXT_11[States.language], (float) this.offsetX, (float) getHeight(), (float) (States.screenWidth - this.offsetXx2), BitmapFont.HAlignment.LEFT);
        this.texManual[0] = new Texture(Gdx.files.internal("data/Graph/help_1.png"));
        this.texManual[1] = new Texture(Gdx.files.internal("data/Graph/help_2.png"));
        this.texManual[0].setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        this.texManual[1].setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
    }

    private void InitObjects() {
        this.picWidth1 = (int) (((float) States.screenWidth) / 1.4f);
        this.picHeight1 = (this.picWidth1 * 288) / 480;
    }

    private void InitPost() {
        this.picWidth2 = (int) (((float) States.screenWidth) / 1.4f);
        this.picHeight2 = (this.picWidth1 * 300) / 480;
        this.texManual[2] = new Texture(Gdx.files.internal(String.valueOf(States.PATH_GRAPH + States.resolution + "/") + "bons.png"));
    }

    public void update() {
        if (this.pos == 1) {
            if (!this.isStart) {
                this.isStart = true;
            } else {
                InitPost();
                this.pos = 2;
            }
        }
        this.offsetY -= this.inp.delta;
        this.inp.delta = 0.0f;
        if (this.inp.speed != 0.0f) {
            this.inp.speed /= 1.05f;
            if (((double) Math.abs(this.inp.speed)) < 0.01d) {
                this.inp.speed = 0.0f;
            }
            this.offsetY -= this.inp.speed;
        }
        if (this.offsetY < 0.0f) {
            if (!this.isBegin) {
                if (Profile.profile.settingVibrate) {
                    Gdx.input.vibrate(20);
                }
                this.isBegin = true;
            }
            this.inp.speed = 0.0f;
            this.offsetY = 0.0f;
        } else {
            this.isBegin = false;
        }
        if (this.offsetY > ((float) this.stopY)) {
            if (!this.isEnd) {
                if (Profile.profile.settingVibrate) {
                    Gdx.input.vibrate(20);
                }
                this.isEnd = true;
            }
            this.inp.speed = 0.0f;
            this.offsetY = (float) this.stopY;
        } else {
            this.isEnd = false;
        }
        if (this.inp.isBack) {
            if (Profile.profile.settingSounds) {
                States.click.play();
            }
            States.state = 3;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.g2d.SpriteBatch.draw(com.badlogic.gdx.graphics.Texture, float, float, float, float, int, int, int, int, boolean, boolean):void
     arg types: [com.badlogic.gdx.graphics.Texture, float, float, float, float, int, int, int, int, int, int]
     candidates:
      com.badlogic.gdx.graphics.g2d.SpriteBatch.draw(com.badlogic.gdx.graphics.g2d.TextureRegion, float, float, float, float, float, float, float, float, float, boolean):void
      com.badlogic.gdx.graphics.g2d.SpriteBatch.draw(com.badlogic.gdx.graphics.Texture, float, float, float, float, int, int, int, int, boolean, boolean):void */
    public void render(Application app) {
        app.getGraphics().getGL10().glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
        app.getGraphics().getGL10().glClear(16384);
        this.spriteBatch.begin();
        this.spriteBatch.enableBlending();
        States.renderBackground(this.spriteBatch);
        this.line = this.font_cap_height_x_1_2;
        if (((float) (this.tb1 + (this.line * 4))) > this.offsetY) {
            States.fontBig.setColor(0.0f, 1.0f, 0.0f, 1.0f);
            States.fontBig.drawMultiLine(this.spriteBatch, TEXT_MANUAL[States.language], 0.0f, (float) getHeight(), (float) States.screenWidth, BitmapFont.HAlignment.CENTER);
            this.line += this.font_cap_height_x_1_2 * 2;
            States.fontSmall.setColor(1.0f, 1.0f, 1.0f, 1.0f);
            States.fontSmall.drawWrapped(this.spriteBatch, TEXT_1[States.language], (float) this.offsetX, (float) getHeight(), (float) (States.screenWidth - this.offsetXx2), BitmapFont.HAlignment.LEFT);
        } else {
            this.line += this.font_cap_height_x_1_2 * 2;
        }
        this.line += this.tb1 + (this.font_cap_height_x_1_2 * 2);
        if (((float) (this.line + this.picHeight1)) > this.offsetY) {
            this.spriteBatch.draw(this.texManual[0], (float) ((States.screenWidth - this.picWidth1) / 2), (float) (getHeight() - this.picHeight1), (float) this.picWidth1, (float) this.picHeight1, 0, 0, 480, 288, false, false);
        }
        this.line += this.picHeight1 + (this.font_cap_height_x_1_2 * 2);
        if (((float) (this.line + this.tb2)) > this.offsetY && this.offsetY + ((float) States.screenHeight) > ((float) this.line)) {
            States.fontSmall.drawWrapped(this.spriteBatch, TEXT_2[States.language], (float) this.offsetX, (float) getHeight(), (float) (States.screenWidth - this.offsetXx2), BitmapFont.HAlignment.LEFT);
        }
        this.line += this.tb2 + (this.font_cap_height_x_1_2 * 2);
        if (((float) (this.line + this.picHeight2)) > this.offsetY && this.offsetY + ((float) States.screenHeight) > ((float) this.line)) {
            this.spriteBatch.draw(this.texManual[1], (float) ((States.screenWidth - this.picWidth2) / 2), (float) (getHeight() - this.picHeight2), (float) this.picWidth2, (float) this.picHeight2, 0, 0, 480, 300, false, false);
        }
        this.line += this.picHeight2 + (this.font_cap_height_x_1_2 * 2);
        if (((float) (this.line + this.font_cap_height_x_1_2)) > this.offsetY && this.offsetY + ((float) States.screenHeight) > ((float) this.line)) {
            States.fontSmall.drawWrapped(this.spriteBatch, TEXT_3[States.language], (float) this.offsetX, (float) getHeight(), (float) (States.screenWidth - this.offsetXx2), BitmapFont.HAlignment.LEFT);
        }
        if (this.pos == 1) {
            this.spriteBatch.end();
            return;
        }
        this.line += this.font_cap_height_x_1_2 << 1;
        if (((float) (this.line + this.bonusWidth)) > this.offsetY && this.offsetY + ((float) States.screenHeight) > ((float) this.line)) {
            this.spriteBatch.draw(this.texManual[2], (float) this.offsetX, (float) (getHeight() - this.bonusWidth), 0, 0, this.bonusWidth, this.bonusWidth);
            States.fontSmall.drawWrapped(this.spriteBatch, TEXT_4[States.language], (float) (this.offsetXx2 + this.bonusWidth), (float) (getHeight() - (this.font_cap_height >> 1)), (float) ((States.screenWidth - this.offsetXx3) - this.bonusWidth), BitmapFont.HAlignment.LEFT);
        }
        this.line += this.bonusWidth + this.font_cap_height;
        if (((float) (this.line + this.bonusWidth)) > this.offsetY && this.offsetY + ((float) States.screenHeight) > ((float) this.line)) {
            this.spriteBatch.draw(this.texManual[2], (float) this.offsetX, (float) (getHeight() - this.bonusWidth), this.bonusWidth, 0, this.bonusWidth, this.bonusWidth);
            States.fontSmall.drawWrapped(this.spriteBatch, TEXT_5[States.language], (float) (this.offsetXx2 + this.bonusWidth), (float) (getHeight() - (this.font_cap_height >> 1)), (float) ((States.screenWidth - this.offsetXx3) - this.bonusWidth), BitmapFont.HAlignment.LEFT);
        }
        this.line += this.bonusWidth + this.font_cap_height;
        if (((float) (this.line + this.bonusWidth)) > this.offsetY && this.offsetY + ((float) States.screenHeight) > ((float) this.line)) {
            this.spriteBatch.draw(this.texManual[2], (float) this.offsetX, (float) (getHeight() - this.bonusWidth), this.bonusWidth << 1, 0, this.bonusWidth, this.bonusWidth);
            States.fontSmall.drawWrapped(this.spriteBatch, TEXT_6[States.language], (float) (this.offsetXx2 + this.bonusWidth), (float) (getHeight() - (this.font_cap_height >> 1)), (float) ((States.screenWidth - this.offsetXx3) - this.bonusWidth), BitmapFont.HAlignment.LEFT);
        }
        this.line += this.bonusWidth + this.font_cap_height;
        if (((float) (this.line + (this.bonusWidth * 2))) > this.offsetY && this.offsetY + ((float) States.screenHeight) > ((float) this.line)) {
            this.spriteBatch.draw(this.texManual[2], (float) this.offsetX, (float) (getHeight() - this.bonusWidth), this.bonusWidth * 5, 0, this.bonusWidth, this.bonusWidth);
            States.fontSmall.drawWrapped(this.spriteBatch, TEXT_61[States.language], (float) (this.offsetXx2 + this.bonusWidth), (float) (getHeight() - (this.font_cap_height >> 1)), (float) ((States.screenWidth - this.offsetXx3) - this.bonusWidth), BitmapFont.HAlignment.LEFT);
        }
        this.line = (int) (((float) this.line) + States.fontSmall.getWrappedBounds(TEXT_61[States.language], (float) ((States.screenWidth - this.offsetXx3) - this.bonusWidth)).height + ((float) this.font_cap_height));
        if (((float) (this.line + this.bonusWidth)) > this.offsetY && this.offsetY + ((float) States.screenHeight) > ((float) this.line)) {
            this.spriteBatch.draw(this.texManual[2], (float) this.offsetX, (float) (getHeight() - this.bonusWidth), this.bonusWidth * 3, 0, this.bonusWidth, this.bonusWidth);
            States.fontSmall.drawWrapped(this.spriteBatch, TEXT_7[States.language], (float) (this.offsetXx2 + this.bonusWidth), (float) (getHeight() - (this.font_cap_height >> 1)), (float) ((States.screenWidth - this.offsetXx3) - this.bonusWidth), BitmapFont.HAlignment.LEFT);
        }
        this.line += this.bonusWidth + this.font_cap_height;
        if (((float) (this.line + (this.bonusWidth * 2))) > this.offsetY && this.offsetY + ((float) States.screenHeight) > ((float) this.line)) {
            this.spriteBatch.draw(this.texManual[2], (float) this.offsetX, (float) (getHeight() - this.bonusWidth), this.bonusWidth << 2, 0, this.bonusWidth, this.bonusWidth);
            States.fontSmall.drawWrapped(this.spriteBatch, TEXT_8[States.language], (float) (this.offsetXx2 + this.bonusWidth), (float) (getHeight() - (this.font_cap_height >> 1)), (float) ((States.screenWidth - this.offsetXx3) - this.bonusWidth), BitmapFont.HAlignment.LEFT);
        }
        this.line += this.tb8 + (this.font_cap_height * 2);
        if (((float) (this.line + this.tb9)) > this.offsetY && this.offsetY + ((float) States.screenHeight) > ((float) this.line)) {
            States.fontSmall.drawWrapped(this.spriteBatch, TEXT_9[States.language], (float) this.offsetX, (float) getHeight(), (float) (States.screenWidth - this.offsetXx2), BitmapFont.HAlignment.LEFT);
        }
        this.line += this.tb9 + this.font_cap_height;
        if (((float) (this.line + this.tb10)) > this.offsetY && this.offsetY + ((float) States.screenHeight) > ((float) this.line)) {
            States.fontSmall.drawWrapped(this.spriteBatch, TEXT_10[States.language], (float) this.offsetX, (float) getHeight(), (float) (States.screenWidth - this.offsetXx2), BitmapFont.HAlignment.LEFT);
        }
        this.line += this.tb10 + this.font_cap_height;
        if (((float) (this.line + this.tb11)) > this.offsetY && this.offsetY + ((float) States.screenHeight) > ((float) this.line)) {
            this.bfc11.setPosition(0.0f, (float) (getHeight() - States.screenHeight));
            this.bfc11.draw(this.spriteBatch);
        }
        this.line += this.tb11 + this.font_cap_height;
        if (((float) (this.line + this.tb12)) > this.offsetY && this.offsetY + ((float) States.screenHeight) > ((float) this.line)) {
            this.bfc12.setPosition(0.0f, (float) (getHeight() - States.screenHeight));
            this.bfc12.draw(this.spriteBatch);
        }
        this.line += this.tb12 + this.font_cap_height;
        if (((float) (this.line + this.tb13)) > this.offsetY && this.offsetY + ((float) States.screenHeight) > ((float) this.line)) {
            this.bfc13.setPosition(0.0f, (float) (getHeight() - States.screenHeight));
            this.bfc13.draw(this.spriteBatch);
        }
        this.line += this.tb13 + this.font_cap_height;
        if (((float) (this.line + this.tb14)) > this.offsetY && this.offsetY + ((float) States.screenHeight) > ((float) this.line)) {
            this.bfc14.setPosition(0.0f, (float) (getHeight() - States.screenHeight));
            this.bfc14.draw(this.spriteBatch);
        }
        this.line += this.tb14 + (this.font_cap_height * 2);
        this.stopY = this.line - States.screenHeight;
        this.spriteBatch.end();
    }

    private int getHeight() {
        return (int) ((this.offsetY + ((float) States.screenHeight)) - ((float) this.line));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.files.FileHandle, boolean):void
     arg types: [com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.files.FileHandle, int]
     candidates:
      com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.graphics.g2d.TextureRegion, boolean):void
      com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.graphics.g2d.BitmapFont$BitmapFontData, com.badlogic.gdx.graphics.g2d.TextureRegion, boolean):void
      com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.files.FileHandle, boolean):void */
    public void dispose() {
        this.bfc11.dispose();
        this.bfc12.dispose();
        this.bfc13.dispose();
        this.bfc14.dispose();
        this.spriteBatch.dispose();
        for (Texture t : this.texManual) {
            t.dispose();
        }
        States.fontBig = new BitmapFont(Gdx.files.internal(String.valueOf(States.path_bigfont) + ".fnt"), Gdx.files.internal(String.valueOf(States.path_bigfont) + ".png"), false);
        States.fontSmall = new BitmapFont(Gdx.files.internal(String.valueOf(States.path_smallfont) + ".fnt"), Gdx.files.internal(String.valueOf(States.path_smallfont) + ".png"), false);
    }

    public void resume() {
    }
}
