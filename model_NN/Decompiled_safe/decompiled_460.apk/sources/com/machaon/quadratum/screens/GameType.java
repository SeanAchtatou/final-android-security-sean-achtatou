package com.machaon.quadratum.screens;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.machaon.quadratum.IScreen;
import com.machaon.quadratum.States;
import com.machaon.quadratum.input.CommonInput;
import com.machaon.quadratum.parts.ButtonPic;
import com.machaon.quadratum.parts.Geom;
import com.machaon.quadratum.parts.Profile;
import com.machaon.quadratum.parts.Tournaments;

public class GameType implements IScreen {
    private ButtonPic[] button = new ButtonPic[2];
    private Geom geomBackPic = new Geom(0, 0, 140, 140);
    private CommonInput inp = new CommonInput();
    private final SpriteBatch spriteBatch = new SpriteBatch();
    private final Texture texBackPic;

    public GameType() {
        if (States.screenHeight == 240 && States.screenWidth == 320) {
            this.button[0] = new ButtonPic(61, 78, 84, 84, 6, -5);
            this.button[1] = new ButtonPic(175, 78, 84, 84, 5, -6);
        }
        if (States.screenHeight == 240 && States.screenWidth == 400) {
            this.button[0] = new ButtonPic((int) Input.Keys.BUTTON_Z, 78, 84, 84, 6, -5);
            this.button[1] = new ButtonPic(215, 78, 84, 84, 5, -6);
        }
        if (States.screenHeight == 320) {
            this.button[0] = new ButtonPic((int) Input.Keys.BUTTON_START, (int) Input.Keys.BUTTON_L2, (int) Input.Keys.FORWARD_DEL, (int) Input.Keys.FORWARD_DEL, 6, -5);
            this.button[1] = new ButtonPic((int) GL10.GL_ADD, (int) Input.Keys.BUTTON_L2, (int) Input.Keys.FORWARD_DEL, (int) Input.Keys.FORWARD_DEL, 5, -5);
        }
        if (States.screenHeight == 480) {
            if (States.screenWidth == 800) {
                this.button[0] = new ButtonPic(208, 159, 162, 162, 8, -8);
                this.button[1] = new ButtonPic(430, 159, 162, 162, 9, -9);
            } else {
                this.button[0] = new ButtonPic(235, 159, 162, 162, 8, -8);
                this.button[1] = new ButtonPic(457, 159, 162, 162, 9, -9);
            }
        }
        this.texBackPic = new Texture(Gdx.files.internal("data/Graph/480/play.png"));
        this.button[0].SetParams(new Texture(Gdx.files.internal(String.valueOf(States.path) + "but_na.png")), new Texture(Gdx.files.internal(String.valueOf(States.path) + "but_a.png")), new Texture(Gdx.files.internal(String.valueOf(States.path) + "but_single.png")), (byte) 6);
        this.button[1].SetParams(new Texture(Gdx.files.internal(String.valueOf(States.path) + "but_na.png")), new Texture(Gdx.files.internal(String.valueOf(States.path) + "but_a.png")), new Texture(Gdx.files.internal(String.valueOf(States.path) + "but_multi.png")), (byte) 7);
        Gdx.app.getInput().setInputProcessor(this.inp);
        this.spriteBatch.setProjectionMatrix(States.viewMatrix);
        if (States.isAntialiasing) {
            for (ButtonPic b : this.button) {
                b.texBackAct.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
                b.texBackNonAct.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
                b.texFront.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
            }
        }
        this.texBackPic.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
    }

    /* JADX INFO: Multiple debug info for r0v0 com.machaon.quadratum.parts.ButtonPic: [D('b' byte), D('b' com.machaon.quadratum.parts.ButtonPic)] */
    public void update() {
        if (this.inp.isBack) {
            if (Profile.profile.settingSounds) {
                States.click.play();
            }
            States.state = 3;
        }
        byte b = this.inp.buttonUp;
        this.inp.getClass();
        if (b == 50) {
            byte b2 = 0;
            while (b2 < 2) {
                if (isInputCoordsIn(this.button[b2])) {
                    if (Profile.profile.settingSounds) {
                        States.click.play();
                    }
                    Tournaments.setType(b2 == 0 ? Tournaments.TYPE_SINGLE : Tournaments.TYPE_MULTY);
                    States.state = this.button[b2].getState();
                }
                b2 = (byte) (b2 + 1);
            }
        }
        byte b3 = this.inp.buttonDown;
        this.inp.getClass();
        if (b3 == 50) {
            for (ButtonPic b4 : this.button) {
                b4.setBackActive(isInputCoordsIn(b4));
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.g2d.SpriteBatch.draw(com.badlogic.gdx.graphics.Texture, float, float, float, float, int, int, int, int, boolean, boolean):void
     arg types: [com.badlogic.gdx.graphics.Texture, float, int, float, float, int, int, int, int, int, int]
     candidates:
      com.badlogic.gdx.graphics.g2d.SpriteBatch.draw(com.badlogic.gdx.graphics.g2d.TextureRegion, float, float, float, float, float, float, float, float, float, boolean):void
      com.badlogic.gdx.graphics.g2d.SpriteBatch.draw(com.badlogic.gdx.graphics.Texture, float, float, float, float, int, int, int, int, boolean, boolean):void */
    public void render(Application app) {
        app.getGraphics().getGL10().glClearColor(0.85f, 0.85f, 0.85f, 1.0f);
        app.getGraphics().getGL10().glClear(16384);
        this.spriteBatch.begin();
        this.spriteBatch.enableBlending();
        this.spriteBatch.draw(this.texBackPic, (float) ((States.screenWidth / 2) - ((States.screenHeight * 1) / 2)), 0.0f, (float) States.screenHeight, (float) States.screenHeight, this.geomBackPic.x, this.geomBackPic.y, this.geomBackPic.width, this.geomBackPic.height, false, false);
        States.renderBackground(this.spriteBatch);
        for (ButtonPic b : this.button) {
            this.spriteBatch.draw(b.getBackTexture(), (float) b.x, (float) b.y, 0, 0, b.width, b.height);
            this.spriteBatch.draw(b.texFront, (float) (b.x + b.frontX), (float) (b.y + b.frontY), 0, 0, b.width, b.height);
        }
        this.spriteBatch.end();
    }

    public void dispose() {
        this.spriteBatch.dispose();
        for (ButtonPic b : this.button) {
            b.dispose();
        }
        this.texBackPic.dispose();
    }

    public void resume() {
    }

    private boolean isInputCoordsIn(Geom geom) {
        if (((float) this.inp.x) <= ((float) (geom.x + States.cameraOffsetX)) * States.aspectX || ((float) this.inp.x) >= ((float) (geom.x + geom.width + States.cameraOffsetX)) * States.aspectX || ((float) (States.displayHeight - this.inp.y)) <= ((float) (geom.y + States.cameraOffsetY)) * States.aspectY || ((float) (States.displayHeight - this.inp.y)) >= ((float) (geom.y + geom.height + States.cameraOffsetY)) * States.aspectY) {
            return false;
        }
        return true;
    }
}
