package com.machaon.quadratum.screens;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEmitter;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Matrix4;
import com.machaon.quadratum.IScreen;
import com.machaon.quadratum.States;
import com.machaon.quadratum.input.GameLoopInput;
import com.machaon.quadratum.parts.ButtonPic;
import com.machaon.quadratum.parts.Field;
import com.machaon.quadratum.parts.Geom;
import com.machaon.quadratum.parts.Keyboard;
import com.machaon.quadratum.parts.Profile;
import com.machaon.quadratum.parts.Tournaments;
import java.lang.reflect.Array;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Random;

public class GameLoop implements IScreen {
    public static final byte SETTING_MUSIC = 2;
    public static final byte SETTING_SKIN = 4;
    public static final byte SETTING_SOUNDS = 1;
    public static final byte SETTING_VIBRATE = 3;
    static byte[] activ = new byte[7];
    public static byte currentPlayer;
    private int ANIM_LIGHT_DURATION = 25;
    private final int BUT_ANIM_END = 2;
    private final int BUT_ANIM_START = 1;
    private final int CELL_ANIM_END = 1;
    private final int CELL_ANIM_START = 4;
    private final int N_BUTTONS_IN_PAUSE = 3;
    private final byte POS_BLIND = 5;
    private final byte POS_BOMB = 4;
    private final byte POS_BOMB_BLOW = Statistic.A_JUMPER;
    private final byte POS_FREEZE = 1;
    private final byte POS_GAME = 6;
    private final byte POS_MIXER = 2;
    private final byte POS_SETTINGS = 8;
    private final byte POS_SHOWALL = 0;
    private final byte POS_SHOW_BORDERS = 9;
    private final byte POS_START = 10;
    private final byte POS_TELEPORT = 3;
    private int animAfterBomb;
    private int animBlind = 0;
    private int animButton;
    private byte animCell;
    private int animLight;
    private int bombAnimWidth;
    private int bombBlow;
    private int bombX;
    private int bombY;
    private int bonusSmallWidth;
    private int bonusStoreWidth;
    private int boost_cellSize_div_2;
    private float boost_cellSize_div_6;
    private float boost_cellSize_mul1_4;
    private int boost_geomBlackRect_aspectX;
    private int boost_geomBlackRect_aspectY;
    private int boost_geomBlackRect_offset_aspectX;
    private int boost_geomBlackRect_offset_aspectY;
    private float boost_geomButtonX_height_div_2;
    private float boost_geomButton_height_mul_15;
    private int boost_geomButton_height_mul_3;
    private int boost_geomClock_font_x_2;
    private int boost_geomClock_font_x_3;
    private ButtonPic[] butAuto;
    private ButtonPic[] butInPause;
    private ButtonPic butStore;
    private ButtonPic[] buttonBonusInStore;
    private ButtonPic[] buttonSettings;
    byte cadr = 0;
    int cdate = 100;
    private int cellSize;
    private int cellSize_1;
    float deltaTimeForAnimBlind = 0.0f;
    private final Field field;
    private BitmapFont fontStore;
    private Geom geomBackPic;
    private Geom geomBlackRect;
    private Geom geomBonusBig;
    private Geom geomButStore;
    private Geom[] geomButton;
    private Geom geomClock;
    private Geom geomField;
    private Geom geomForBorders;
    private Geom[] geomInfo;
    private Geom[] geomInfoCell;
    private Geom[] geomInfoPlayer;
    private Geom geomStore;
    private Geom geomTextStore;
    private GameLoopInput inp = new GameLoopInput();
    private boolean[] isAuto;
    private boolean isBlind = false;
    private boolean isBombClockTick;
    private boolean isBonusFinded = false;
    private boolean[] isButtonRight;
    boolean isCalcBlowingTerrain = false;
    private boolean isClock = true;
    private boolean isQuitPress = false;
    private boolean isRefreshPlayerInfo;
    private boolean isResume = false;
    private boolean isResumePause = false;
    private boolean isResumeSettings = false;
    private boolean isShowBonus = false;
    boolean isSoundExpPlay = false;
    private boolean isStart = false;
    private boolean isStoreShow = false;
    private boolean isToast;
    private boolean isUnBlind = false;
    private boolean isUpdatePixField;
    private byte numberOfPlayers;
    private byte partBonusAnim;
    private ParticleEffect peBomb;
    private ParticleEffect peBonus;
    private ParticleEffect[] peFindBonus;
    private ParticleEffect peTeleport;
    private Pixmap pixField;
    private byte pos;
    private byte posBeforePause;
    private byte posBeforeResume;
    String sdate;
    private Sound soundBlindness;
    private Sound soundBombClock;
    private Sound soundBonus;
    private Sound soundClick;
    private Sound soundExp;
    private Sound soundFreeze;
    private Sound soundMixer;
    private Sound soundShowAll;
    private Sound soundTeleport;
    private final SpriteBatch spriteBatch;
    private States states;
    private int stepBlind = 0;
    private byte stepFreeze;
    private int stepParticleOfTeleport;
    private String[] strPlayerInfo = new String[4];
    private byte[] tempPeFind = new byte[8];
    private Texture texBackPic;
    private Texture texBlackRect;
    private Texture texBonus;
    private Texture texBonusBig;
    private Texture texBonusesSmall;
    private Texture texBut2A;
    private Texture texBut2NA;
    private Texture texButA;
    private Texture texButNA;
    private final Texture[][] texButton;
    private Texture texButtons;
    private Texture texCells;
    private Texture texField;
    private Texture texOff;
    private Texture texStore;
    private float timer;
    private float timer_afterPause;
    private float timer_afterResume;
    private float timer_countdownBomb;
    private float timer_mixer;
    private float timer_showAll;
    private float timer_showFindedBonus;
    private float timer_timeoutBeforeNextStep;
    private float timer_useBonus;
    private final Matrix4 viewMatrix = new Matrix4();

    public GameLoop(String sLevel, States states2) {
        this.states = states2;
        this.numberOfPlayers = States.numberOfPlayers;
        this.animCell = 1;
        this.animButton = 1;
        this.animLight = 0;
        this.pos = 10;
        this.partBonusAnim = 0;
        this.stepParticleOfTeleport = 0;
        this.isButtonRight = new boolean[this.numberOfPlayers];
        for (byte i = 0; i < this.numberOfPlayers; i = (byte) (i + 1)) {
            this.isButtonRight[i] = true;
        }
        this.isClock = Profile.profile.isClock;
        for (byte i2 = 0; i2 < 8; i2 = (byte) (i2 + 1)) {
            this.tempPeFind[i2] = 0;
        }
        this.field = new Field(this.numberOfPlayers, States.sizeFieldX, States.sizeFieldY, States.numberOfColors, sLevel, states2);
        this.spriteBatch = new SpriteBatch();
        this.butInPause = new ButtonPic[3];
        this.texButton = (Texture[][]) Array.newInstance(Texture.class, this.field.numberOfColors, 3);
        this.isAuto = new boolean[this.numberOfPlayers];
        this.geomButton = new Geom[this.field.numberOfColors];
        this.geomInfoCell = new Geom[this.numberOfPlayers];
        this.geomInfoPlayer = new Geom[this.numberOfPlayers];
        this.geomInfo = new Geom[this.numberOfPlayers];
        this.butAuto = new ButtonPic[this.numberOfPlayers];
        this.buttonBonusInStore = new ButtonPic[6];
        this.buttonSettings = new ButtonPic[5];
        this.geomBackPic = new Geom(0, 0, 146, 146);
        this.ANIM_LIGHT_DURATION = 25;
        if (States.screenHeight == 240 && States.screenWidth == 320) {
            if (this.field.numberOfColors == 7) {
                for (byte i3 = 0; i3 < 7; i3 = (byte) (i3 + 1)) {
                    this.geomButton[i3] = new Geom(285, 205 - (i3 * 34), 33, 33);
                }
            }
            if (this.field.numberOfColors == 5) {
                for (byte i4 = 0; i4 < 5; i4 = (byte) (i4 + 1)) {
                    this.geomButton[i4] = new Geom(285, 192 - (i4 * 45), 33, 33);
                }
            }
            this.cellSize = 8;
            this.cellSize_1 = this.cellSize - 1;
            this.geomField = new Geom(4, 28, this.cellSize * this.field.sizeFieldX, this.cellSize * this.field.sizeFieldY);
            this.geomForBorders = new Geom(4, 40, this.cellSize * this.field.sizeFieldX, (this.cellSize * this.field.sizeFieldY) - 30);
            this.geomBlackRect = new Geom(0, 25, 285, 191);
            this.geomInfoCell[0] = new Geom(4, 222, this.cellSize_1, this.cellSize_1);
            this.geomInfoPlayer[0] = new Geom(17, 232, 0, 0);
            if (this.numberOfPlayers == 2) {
                this.geomInfoCell[1] = new Geom(276, 10, this.cellSize_1, this.cellSize_1);
                this.geomInfoPlayer[1] = new Geom(164, 20, Input.Keys.BUTTON_THUMBL, 0);
            } else {
                this.geomInfoCell[1] = new Geom(276, 222, this.cellSize_1, this.cellSize_1);
                this.geomInfoPlayer[1] = new Geom(164, 232, Input.Keys.BUTTON_THUMBL, 0);
                this.geomInfoCell[2] = new Geom(276, 10, this.cellSize_1, this.cellSize_1);
                this.geomInfoPlayer[2] = new Geom(164, 20, Input.Keys.BUTTON_THUMBL, 0);
                this.geomInfoCell[3] = new Geom(4, 10, this.cellSize_1, this.cellSize_1);
                this.geomInfoPlayer[3] = new Geom(17, 20, 0, 0);
            }
            this.butInPause[0] = new ButtonPic(24, 78, 84, 84, 7, -7);
            this.butInPause[1] = new ButtonPic(118, 78, 84, 84, 5, -7);
            this.butInPause[2] = new ButtonPic(212, 78, 84, 84, 6, -7);
            this.butStore = new ButtonPic(Input.Keys.META_SHIFT_RIGHT_ON, 220, 29, 12);
            this.geomButStore = new Geom(120, 210, 49, 30);
            this.geomClock = new Geom(Input.Keys.CONTROL_LEFT, 11, 25, 25);
            this.geomInfo[0] = new Geom(0, 198, 126, 42);
            if (this.numberOfPlayers == 2) {
                this.geomInfo[1] = new Geom(194, 0, 126, 42);
            } else {
                this.geomInfo[1] = new Geom(194, 198, 126, 42);
                this.geomInfo[2] = new Geom(194, 0, 126, 42);
                this.geomInfo[3] = new Geom(0, 0, 126, 42);
            }
            this.butAuto[0] = new ButtonPic(8, 206, 26, 26, 8, 8);
            if (this.numberOfPlayers == 2) {
                this.butAuto[1] = new ButtonPic(202, 8, 26, 26, 8, 22);
            } else {
                this.butAuto[1] = new ButtonPic(202, 206, 26, 26, 8, 22);
                this.butAuto[2] = new ButtonPic(202, 8, 26, 26, 8, 0);
                this.butAuto[3] = new ButtonPic(8, 8, 26, 26, 8, 0);
            }
            this.geomStore = new Geom(56, 45, 210, 150);
            this.buttonBonusInStore[0] = new ButtonPic(74, 123, 54, 54, 77, 120);
            this.buttonBonusInStore[1] = new ButtonPic(74, 63, 54, 54, 77, 60);
            this.buttonBonusInStore[2] = new ButtonPic((int) Input.Keys.INSERT, 123, 54, 54, 136, 120);
            this.buttonBonusInStore[3] = new ButtonPic(192, 123, 54, 54, 195, 121);
            this.buttonBonusInStore[4] = new ButtonPic(192, 63, 54, 54, 195, 59);
            this.buttonBonusInStore[5] = new ButtonPic((int) Input.Keys.INSERT, 63, 54, 54, 136, 58);
            this.geomTextStore = new Geom(4, 6, 0, 0);
            this.bonusSmallWidth = 20;
            this.geomBonusBig = new Geom(0, 0, Input.Keys.META_SHIFT_RIGHT_ON, Input.Keys.META_SHIFT_RIGHT_ON);
            this.bombAnimWidth = 48;
            this.bonusStoreWidth = 48;
            this.buttonSettings[0] = new ButtonPic(14, (int) Input.Keys.CONTROL_RIGHT, 84, 84, 7, -5);
            this.buttonSettings[1] = new ButtonPic(118, (int) Input.Keys.CONTROL_RIGHT, 84, 84, 5, -5);
            this.buttonSettings[2] = new ButtonPic(14, 26, 84, 84, 6, -5);
            this.buttonSettings[4] = new ButtonPic(118, 26, 84, 84, 6, -6);
            this.buttonSettings[3] = new ButtonPic(222, 26, 84, 84, 6, -6);
        }
        if (States.screenHeight == 240 && States.screenWidth == 400) {
            if (this.field.numberOfColors == 7) {
                for (byte i5 = 0; i5 < 7; i5 = (byte) (i5 + 1)) {
                    this.geomButton[i5] = new Geom(345, 205 - (i5 * 34), 33, 33);
                }
            }
            if (this.field.numberOfColors == 5) {
                for (byte i6 = 0; i6 < 5; i6 = (byte) (i6 + 1)) {
                    this.geomButton[i6] = new Geom(345, 192 - (i6 * 45), 33, 33);
                }
            }
            this.cellSize = 9;
            this.cellSize_1 = this.cellSize - 1;
            this.geomField = new Geom(8, 17, this.cellSize * this.field.sizeFieldX, this.cellSize * this.field.sizeFieldY);
            this.geomForBorders = new Geom(8, 30, this.cellSize * this.field.sizeFieldX, (this.cellSize * this.field.sizeFieldY) - 30);
            this.geomBlackRect = new Geom(4, 12, 323, 215);
            this.geomInfoCell[0] = new Geom(8, 228, this.cellSize_1, this.cellSize_1);
            this.geomInfoPlayer[0] = new Geom(26, 238, 0, 0);
            if (this.numberOfPlayers == 2) {
                this.geomInfoCell[1] = new Geom(314, 4, this.cellSize_1, this.cellSize_1);
                this.geomInfoPlayer[1] = new Geom(198, 14, Input.Keys.BUTTON_THUMBL, 0);
            } else {
                this.geomInfoCell[1] = new Geom(314, 228, this.cellSize_1, this.cellSize_1);
                this.geomInfoPlayer[1] = new Geom(198, 238, Input.Keys.BUTTON_THUMBL, 0);
                this.geomInfoCell[2] = new Geom(314, 4, this.cellSize_1, this.cellSize_1);
                this.geomInfoPlayer[2] = new Geom(198, 14, Input.Keys.BUTTON_THUMBL, 0);
                this.geomInfoCell[3] = new Geom(8, 4, this.cellSize_1, this.cellSize_1);
                this.geomInfoPlayer[3] = new Geom(26, 14, 0, 0);
            }
            this.butInPause[0] = new ButtonPic(66, 78, 84, 84, 7, -7);
            this.butInPause[1] = new ButtonPic(158, 78, 84, 84, 5, -7);
            this.butInPause[2] = new ButtonPic((int) Input.Keys.F7, 78, 84, 84, 6, -7);
            this.butStore = new ButtonPic(150, 225, 29, 12);
            this.geomButStore = new Geom(Input.Keys.CONTROL_RIGHT, 210, 69, 30);
            this.geomClock = new Geom(152, 5, 25, 25);
            this.geomInfo[0] = new Geom(0, 198, 136, 42);
            if (this.numberOfPlayers == 2) {
                this.geomInfo[1] = new Geom(264, 0, 136, 42);
            } else {
                this.geomInfo[1] = new Geom(264, 198, 136, 42);
                this.geomInfo[2] = new Geom(264, 0, 136, 42);
                this.geomInfo[3] = new Geom(0, 0, 136, 42);
            }
            this.butAuto[0] = new ButtonPic(8, 206, 26, 26, 8, 8);
            if (this.numberOfPlayers == 2) {
                this.butAuto[1] = new ButtonPic(272, 8, 26, 26, 8, 22);
            } else {
                this.butAuto[1] = new ButtonPic(272, 206, 26, 26, 8, 22);
                this.butAuto[2] = new ButtonPic(272, 8, 26, 26, 8, 0);
                this.butAuto[3] = new ButtonPic(8, 8, 26, 26, 8, 0);
            }
            this.geomStore = new Geom(96, 45, 210, 150);
            this.buttonBonusInStore[0] = new ButtonPic(114, 123, 54, 54, 117, 120);
            this.buttonBonusInStore[1] = new ButtonPic(114, 63, 54, 54, 117, 60);
            this.buttonBonusInStore[2] = new ButtonPic(173, 123, 54, 54, 176, 120);
            this.buttonBonusInStore[3] = new ButtonPic(232, 123, 54, 54, 235, 121);
            this.buttonBonusInStore[4] = new ButtonPic(232, 63, 54, 54, 235, 59);
            this.buttonBonusInStore[5] = new ButtonPic(173, 63, 54, 54, 176, 58);
            this.geomTextStore = new Geom(4, 6, 0, 0);
            this.bonusSmallWidth = 20;
            this.geomBonusBig = new Geom(0, 0, Input.Keys.META_SHIFT_RIGHT_ON, Input.Keys.META_SHIFT_RIGHT_ON);
            this.bombAnimWidth = 48;
            this.bonusStoreWidth = 48;
            this.buttonSettings[0] = new ButtonPic(50, (int) Input.Keys.END, 84, 84, 7, -5);
            this.buttonSettings[1] = new ButtonPic(158, (int) Input.Keys.END, 84, 84, 5, -5);
            this.buttonSettings[2] = new ButtonPic(50, 24, 84, 84, 6, -5);
            this.buttonSettings[4] = new ButtonPic(158, 24, 84, 84, 6, -6);
            this.buttonSettings[3] = new ButtonPic(266, 24, 84, 84, 6, -6);
        }
        if (States.screenHeight == 320) {
            this.cellSize = 12;
            this.cellSize_1 = this.cellSize - 1;
            this.butInPause[0] = new ButtonPic(62, (int) Input.Keys.BUTTON_MODE, (int) Input.Keys.FORWARD_DEL, (int) Input.Keys.FORWARD_DEL, 7, -6);
            this.butInPause[1] = new ButtonPic(183, (int) Input.Keys.BUTTON_MODE, (int) Input.Keys.FORWARD_DEL, (int) Input.Keys.FORWARD_DEL, 6, -8);
            this.butInPause[2] = new ButtonPic(305, (int) Input.Keys.BUTTON_MODE, (int) Input.Keys.FORWARD_DEL, (int) Input.Keys.FORWARD_DEL, 6, -6);
            this.geomStore = new Geom(100, 60, 280, 200);
            this.buttonBonusInStore[0] = new ButtonPic(123, 163, 74, 74, 127, 158);
            this.buttonBonusInStore[1] = new ButtonPic(123, 83, 74, 74, 127, 78);
            this.buttonBonusInStore[2] = new ButtonPic(203, 163, 74, 74, 207, 158);
            this.buttonBonusInStore[3] = new ButtonPic(283, 163, 74, 74, 287, 159);
            this.buttonBonusInStore[4] = new ButtonPic(283, 83, 74, 74, 286, 78);
            this.buttonBonusInStore[5] = new ButtonPic(203, 83, 74, 74, 206, 75);
            this.geomTextStore = new Geom(4, 8, 0, 0);
            this.bonusSmallWidth = 20;
            this.geomBonusBig = new Geom(0, 0, Input.Keys.META_SHIFT_RIGHT_ON, Input.Keys.META_SHIFT_RIGHT_ON);
            this.bombAnimWidth = 64;
            this.bonusStoreWidth = 64;
            this.buttonSettings[0] = new ButtonPic(42, 175, (int) Input.Keys.FORWARD_DEL, (int) Input.Keys.FORWARD_DEL, 9, -8);
            this.buttonSettings[1] = new ButtonPic(184, 175, (int) Input.Keys.FORWARD_DEL, (int) Input.Keys.FORWARD_DEL, 8, -6);
            this.buttonSettings[2] = new ButtonPic(42, 33, (int) Input.Keys.FORWARD_DEL, (int) Input.Keys.FORWARD_DEL, 8, -6);
            this.buttonSettings[4] = new ButtonPic(184, 33, (int) Input.Keys.FORWARD_DEL, (int) Input.Keys.FORWARD_DEL, 8, -8);
            this.buttonSettings[3] = new ButtonPic(326, 33, (int) Input.Keys.FORWARD_DEL, (int) Input.Keys.FORWARD_DEL, 8, -9);
            this.geomInfo[0] = new Geom(0, 266, 189, 54);
            if (this.numberOfPlayers == 2) {
                this.geomInfo[1] = new Geom(292, 0, 189, 54);
            } else {
                this.geomInfo[1] = new Geom(292, 266, 189, 54);
                this.geomInfo[2] = new Geom(292, 0, 189, 54);
                this.geomInfo[3] = new Geom(0, 0, 189, 54);
            }
            this.butAuto[0] = new ButtonPic(10, 276, 35, 35, 10, 11);
            if (this.numberOfPlayers == 2) {
                this.butAuto[1] = new ButtonPic(302, 10, 35, 35, 10, 29);
            } else {
                this.butAuto[1] = new ButtonPic(302, 276, 35, 35, 10, 29);
                this.butAuto[2] = new ButtonPic(302, 10, 35, 35, 10, 0);
                this.butAuto[3] = new ButtonPic(10, 10, 35, 35, 10, 0);
            }
            if (this.field.numberOfColors == 7) {
                for (byte i7 = 0; i7 < 7; i7 = (byte) (i7 + 1)) {
                    this.geomButton[i7] = new Geom(434, 272 - (i7 * 45), 44, 44);
                }
            }
            if (this.field.numberOfColors == 5) {
                for (byte i8 = 0; i8 < 5; i8 = (byte) (i8 + 1)) {
                    this.geomButton[i8] = new Geom(434, 256 - (i8 * 60), 44, 44);
                }
            }
            this.geomField = new Geom(11, 22, this.cellSize * this.field.sizeFieldX, this.cellSize * this.field.sizeFieldY);
            this.geomForBorders = new Geom(11, 40, this.cellSize * this.field.sizeFieldX, (this.cellSize * this.field.sizeFieldY) - 40);
            this.geomBlackRect = new Geom(5, 0, 432, 303);
            this.geomInfoCell[0] = new Geom(11, 303, this.cellSize_1, this.cellSize_1);
            this.geomInfoPlayer[0] = new Geom(35, 317, 0, 0);
            if (this.numberOfPlayers == 2) {
                this.geomInfoCell[1] = new Geom(419, 5, this.cellSize_1, this.cellSize_1);
                this.geomInfoPlayer[1] = new Geom(300, 18, Input.Keys.BUTTON_THUMBL, 0);
            } else {
                this.geomInfoCell[1] = new Geom(419, 303, this.cellSize_1, this.cellSize_1);
                this.geomInfoPlayer[1] = new Geom(300, 317, Input.Keys.BUTTON_THUMBL, 0);
                this.geomInfoCell[2] = new Geom(419, 5, this.cellSize_1, this.cellSize_1);
                this.geomInfoPlayer[2] = new Geom(300, 18, Input.Keys.BUTTON_THUMBL, 0);
                this.geomInfoCell[3] = new Geom(11, 5, this.cellSize_1, this.cellSize_1);
                this.geomInfoPlayer[3] = new Geom(35, 18, 0, 0);
            }
            this.butStore = new ButtonPic(201, 301, 38, 16);
            this.geomButStore = new Geom(180, 280, 78, 40);
            this.geomClock = new Geom(207, 7, 30, 30);
        }
        if (States.screenHeight == 480) {
            if (States.screenWidth == 800) {
                if (this.field.numberOfColors == 7) {
                    for (byte i9 = 0; i9 < 7; i9 = (byte) (i9 + 1)) {
                        this.geomButton[i9] = new Geom(705, 411 - (i9 * 68), 68, 68);
                    }
                }
                if (this.field.numberOfColors == 5) {
                    for (byte i10 = 0; i10 < 5; i10 = (byte) (i10 + 1)) {
                        this.geomButton[i10] = new Geom(705, 387 - (i10 * 91), 68, 68);
                    }
                }
                this.cellSize = 18;
                this.cellSize_1 = this.cellSize - 1;
                this.geomField = new Geom(43, 33, this.cellSize * this.field.sizeFieldX, this.cellSize * this.field.sizeFieldY);
                this.geomForBorders = new Geom(43, 60, this.cellSize * this.field.sizeFieldX, (this.cellSize * this.field.sizeFieldY) - 60);
                this.geomBlackRect = new Geom(33, 23, 649, 433);
                this.geomInfoCell[0] = new Geom(43, 454, this.cellSize_1, this.cellSize_1);
                this.geomInfoPlayer[0] = new Geom(79, 472, 0, 0);
                if (this.numberOfPlayers == 2) {
                    this.geomInfoCell[1] = new Geom(655, 7, this.cellSize_1, this.cellSize_1);
                    this.geomInfoPlayer[1] = new Geom(527, 25, Input.Keys.BUTTON_THUMBL, 0);
                } else {
                    this.geomInfoCell[1] = new Geom(655, 454, this.cellSize_1, this.cellSize_1);
                    this.geomInfoPlayer[1] = new Geom(527, 472, Input.Keys.BUTTON_THUMBL, 0);
                    this.geomInfoCell[2] = new Geom(655, 7, this.cellSize_1, this.cellSize_1);
                    this.geomInfoPlayer[2] = new Geom(527, 25, Input.Keys.BUTTON_THUMBL, 0);
                    this.geomInfoCell[3] = new Geom(43, 7, this.cellSize_1, this.cellSize_1);
                    this.geomInfoPlayer[3] = new Geom(79, 25, 0, 0);
                }
                this.butInPause[0] = new ButtonPic(137, 159, 162, 162, 9, -10);
                this.butInPause[1] = new ButtonPic(319, 159, 162, 162, 8, -11);
                this.butInPause[2] = new ButtonPic(501, 159, 162, 162, 12, -13);
                this.butStore = new ButtonPic(330, 451, 56, 24);
                this.geomButStore = new Geom(310, 420, 96, 60);
                this.geomClock = new Geom(340, 10, 50, 40);
                this.geomInfo[0] = new Geom(0, 396, 281, 84);
                if (this.numberOfPlayers == 2) {
                    this.geomInfo[1] = new Geom(519, 0, 281, 84);
                } else {
                    this.geomInfo[1] = new Geom(519, 396, 281, 84);
                    this.geomInfo[2] = new Geom(519, 0, 281, 84);
                    this.geomInfo[3] = new Geom(0, 0, 281, 84);
                }
                this.butAuto[0] = new ButtonPic(15, 411, 56, 56, 15, 19);
                if (this.numberOfPlayers == 2) {
                    this.butAuto[1] = new ButtonPic(534, 15, 56, 56, 15, 47);
                } else {
                    this.butAuto[1] = new ButtonPic(534, 411, 56, 56, 15, 47);
                    this.butAuto[2] = new ButtonPic(534, 15, 56, 56, 15, 0);
                    this.butAuto[3] = new ButtonPic(15, 15, 56, 56, 15, 0);
                }
                this.geomStore = new Geom(190, 89, 420, 302);
                this.buttonBonusInStore[0] = new ButtonPic(228, (int) Input.Keys.F2, (int) Input.Keys.BUTTON_START, (int) Input.Keys.BUTTON_START, 235, 231);
                this.buttonBonusInStore[1] = new ButtonPic(228, 127, (int) Input.Keys.BUTTON_START, (int) Input.Keys.BUTTON_START, 233, 118);
                this.buttonBonusInStore[2] = new ButtonPic(346, (int) Input.Keys.F2, (int) Input.Keys.BUTTON_START, (int) Input.Keys.BUTTON_START, 352, 235);
                this.buttonBonusInStore[3] = new ButtonPic(464, (int) Input.Keys.F2, (int) Input.Keys.BUTTON_START, (int) Input.Keys.BUTTON_START, 470, 238);
                this.buttonBonusInStore[4] = new ButtonPic(464, 127, (int) Input.Keys.BUTTON_START, (int) Input.Keys.BUTTON_START, 468, 115);
                this.buttonBonusInStore[5] = new ButtonPic(346, 127, (int) Input.Keys.BUTTON_START, (int) Input.Keys.BUTTON_START, 354, 114);
                this.geomTextStore = new Geom(6, 8, 0, 0);
            } else {
                if (this.field.numberOfColors == 7) {
                    for (byte i11 = 0; i11 < 7; i11 = (byte) (i11 + 1)) {
                        this.geomButton[i11] = new Geom(736, 411 - (i11 * 68), 68, 68);
                    }
                }
                if (this.field.numberOfColors == 5) {
                    for (byte i12 = 0; i12 < 5; i12 = (byte) (i12 + 1)) {
                        this.geomButton[i12] = new Geom(736, 387 - (i12 * 91), 68, 68);
                    }
                }
                this.cellSize = 18;
                this.cellSize_1 = this.cellSize - 1;
                this.geomField = new Geom(52, 33, this.cellSize * this.field.sizeFieldX, this.cellSize * this.field.sizeFieldY);
                this.geomForBorders = new Geom(52, 60, this.cellSize * this.field.sizeFieldX, (this.cellSize * this.field.sizeFieldY) - 60);
                this.geomBlackRect = new Geom(42, 23, 649, 433);
                this.geomInfoCell[0] = new Geom(52, 454, this.cellSize_1, this.cellSize_1);
                this.geomInfoPlayer[0] = new Geom(88, 472, 0, 0);
                if (this.numberOfPlayers == 2) {
                    this.geomInfoCell[1] = new Geom(664, 7, this.cellSize_1, this.cellSize_1);
                    this.geomInfoPlayer[1] = new Geom(526, 25, Input.Keys.BUTTON_THUMBL, 0);
                } else {
                    this.geomInfoCell[1] = new Geom(664, 454, this.cellSize_1, this.cellSize_1);
                    this.geomInfoPlayer[1] = new Geom(526, 472, Input.Keys.BUTTON_THUMBL, 0);
                    this.geomInfoCell[2] = new Geom(664, 7, this.cellSize_1, this.cellSize_1);
                    this.geomInfoPlayer[2] = new Geom(526, 25, Input.Keys.BUTTON_THUMBL, 0);
                    this.geomInfoCell[3] = new Geom(52, 7, this.cellSize_1, this.cellSize_1);
                    this.geomInfoPlayer[3] = new Geom(98, 25, 0, 0);
                }
                this.butInPause[0] = new ButtonPic(164, 159, 162, 162, 9, -10);
                this.butInPause[1] = new ButtonPic(346, 159, 162, 162, 8, -11);
                this.butInPause[2] = new ButtonPic(528, 159, 162, 162, 12, -13);
                this.butStore = new ButtonPic(338, 451, 56, 24);
                this.geomButStore = new Geom(318, 420, 96, 60);
                this.geomClock = new Geom(348, 10, 50, 40);
                this.geomInfo[0] = new Geom(20, 396, 281, 84);
                if (this.numberOfPlayers == 2) {
                    this.geomInfo[1] = new Geom(553, 0, 281, 84);
                } else {
                    this.geomInfo[1] = new Geom(553, 396, 281, 84);
                    this.geomInfo[2] = new Geom(553, 0, 281, 84);
                    this.geomInfo[3] = new Geom(20, 0, 281, 84);
                }
                this.butAuto[0] = new ButtonPic(35, 411, 56, 56, 15, 19);
                if (this.numberOfPlayers == 2) {
                    this.butAuto[1] = new ButtonPic(568, 15, 56, 56, 15, 47);
                } else {
                    this.butAuto[1] = new ButtonPic(568, 411, 56, 56, 15, 47);
                    this.butAuto[2] = new ButtonPic(568, 15, 56, 56, 15, 0);
                    this.butAuto[3] = new ButtonPic(35, 15, 56, 56, 15, 0);
                }
                this.geomStore = new Geom(217, 89, 420, 302);
                this.buttonBonusInStore[0] = new ButtonPic(255, (int) Input.Keys.F2, (int) Input.Keys.BUTTON_START, (int) Input.Keys.BUTTON_START, 262, 231);
                this.buttonBonusInStore[1] = new ButtonPic(255, 127, (int) Input.Keys.BUTTON_START, (int) Input.Keys.BUTTON_START, (int) GL10.GL_ADD, 118);
                this.buttonBonusInStore[2] = new ButtonPic(373, (int) Input.Keys.F2, (int) Input.Keys.BUTTON_START, (int) Input.Keys.BUTTON_START, 379, 235);
                this.buttonBonusInStore[3] = new ButtonPic(491, (int) Input.Keys.F2, (int) Input.Keys.BUTTON_START, (int) Input.Keys.BUTTON_START, 497, 238);
                this.buttonBonusInStore[4] = new ButtonPic(491, 127, (int) Input.Keys.BUTTON_START, (int) Input.Keys.BUTTON_START, 495, 115);
                this.buttonBonusInStore[5] = new ButtonPic(373, 127, (int) Input.Keys.BUTTON_START, (int) Input.Keys.BUTTON_START, 381, 114);
                this.geomTextStore = new Geom(6, 8, 0, 0);
            }
            this.bonusSmallWidth = 32;
            this.geomBonusBig = new Geom(0, 0, 196, 196);
            this.bombAnimWidth = 96;
            this.bonusStoreWidth = 96;
            if (States.screenWidth == 800) {
                this.buttonSettings[0] = new ButtonPic((int) Input.Keys.BUTTON_THUMBR, 259, 162, 162, 10, -8);
                this.buttonSettings[1] = new ButtonPic(319, 259, 162, 162, 10, -8);
                this.buttonSettings[2] = new ButtonPic((int) Input.Keys.BUTTON_THUMBR, 47, 162, 162, 10, -8);
                this.buttonSettings[4] = new ButtonPic(319, 47, 162, 162, 11, -10);
                this.buttonSettings[3] = new ButtonPic(531, 47, 162, 162, 10, -10);
            } else {
                this.buttonSettings[0] = new ButtonPic(134, 259, 162, 162, 10, -8);
                this.buttonSettings[1] = new ButtonPic(348, 259, 162, 162, 10, -8);
                this.buttonSettings[2] = new ButtonPic(134, 47, 162, 162, 10, -8);
                this.buttonSettings[4] = new ButtonPic(348, 47, 162, 162, 11, -10);
                this.buttonSettings[3] = new ButtonPic(558, 47, 162, 162, 10, -10);
            }
        }
        refreshPlayerInfo();
        InitGeom(true);
        InitObjects();
        Gdx.app.getInput().setInputProcessor(this.inp);
        this.timer_afterPause = 0.0f;
        this.spriteBatch.setProjectionMatrix(States.viewMatrix);
        if (States.isAntialiasing) {
            for (ButtonPic b : this.buttonBonusInStore) {
                b.texBackAct.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
                b.texBackNonAct.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
                b.texFront.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
            }
            this.texBonus.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
            this.texButtons.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
            this.butStore.texBackAct.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
            this.butStore.texBackNonAct.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        }
        InitBoost();
    }

    private void InitGeom(boolean is) {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        int i11;
        int i12;
        int i13;
        int i14;
        int i15;
        int i16;
        int i17;
        if (States.screenHeight == 240 && States.screenWidth == 320) {
            for (byte i18 = 0; i18 < this.field.numberOfColors; i18 = (byte) (i18 + 1)) {
                Geom geom = this.geomButton[i18];
                if (is) {
                    i17 = 285;
                } else {
                    i17 = 2;
                }
                geom.x = i17;
            }
            this.geomField.x = is ? 4 : 36;
            this.geomForBorders.x = is ? 4 : 36;
            this.geomBlackRect.x = is ? 0 : 32;
            this.geomInfoCell[0].x = is ? 4 : 36;
            Geom geom2 = this.geomInfoPlayer[0];
            if (is) {
                i14 = 17;
            } else {
                i14 = 49;
            }
            geom2.x = i14;
            if (this.numberOfPlayers == 2) {
                this.geomInfoCell[1].x = is ? 276 : 308;
                Geom geom3 = this.geomInfoPlayer[1];
                if (is) {
                    i16 = 164;
                } else {
                    i16 = 196;
                }
                geom3.x = i16;
            } else {
                this.geomInfoCell[1].x = is ? 276 : 308;
                this.geomInfoPlayer[1].x = is ? 164 : 196;
                this.geomInfoCell[2].x = is ? 276 : 308;
                this.geomInfoPlayer[2].x = is ? 164 : 196;
                this.geomInfoCell[3].x = is ? 4 : 36;
                Geom geom4 = this.geomInfoPlayer[3];
                if (is) {
                    i15 = 17;
                } else {
                    i15 = 49;
                }
                geom4.x = i15;
            }
            this.butStore.x = is ? Input.Keys.META_SHIFT_RIGHT_ON : 160;
            this.geomButStore.x = is ? Input.Keys.BUTTON_MODE : 142;
            this.geomClock.x = is ? Input.Keys.CONTROL_LEFT : 161;
        }
        if (States.screenHeight == 240 && States.screenWidth == 400) {
            for (byte i19 = 0; i19 < this.field.numberOfColors; i19 = (byte) (i19 + 1)) {
                this.geomButton[i19].x = is ? 345 : 22;
            }
            this.geomField.x = is ? 8 : 77;
            this.geomForBorders.x = is ? 8 : 77;
            this.geomBlackRect.x = is ? 4 : 73;
            this.geomInfoCell[0].x = is ? 8 : 77;
            this.geomInfoPlayer[0].x = is ? 26 : 95;
            if (this.numberOfPlayers == 2) {
                this.geomInfoCell[1].x = is ? 314 : 383;
                Geom geom5 = this.geomInfoPlayer[1];
                if (is) {
                    i13 = 198;
                } else {
                    i13 = 267;
                }
                geom5.x = i13;
            } else {
                this.geomInfoCell[1].x = is ? 314 : 383;
                this.geomInfoPlayer[1].x = is ? 198 : 267;
                this.geomInfoCell[2].x = is ? 314 : 383;
                this.geomInfoPlayer[2].x = is ? 198 : 267;
                this.geomInfoCell[3].x = is ? 8 : 77;
                Geom geom6 = this.geomInfoPlayer[3];
                if (is) {
                    i12 = 26;
                } else {
                    i12 = 95;
                }
                geom6.x = i12;
            }
            this.butStore.x = is ? 150 : 219;
            this.geomButStore.x = is ? Input.Keys.CONTROL_RIGHT : 199;
            this.geomClock.x = is ? 152 : 221;
        }
        if (States.screenHeight == 320) {
            for (byte i20 = 0; i20 < this.field.numberOfColors; i20 = (byte) (i20 + 1)) {
                Geom geom7 = this.geomButton[i20];
                if (is) {
                    i11 = 434;
                } else {
                    i11 = 2;
                }
                geom7.x = i11;
            }
            Geom geom8 = this.geomField;
            if (is) {
                i5 = 11;
            } else {
                i5 = 49;
            }
            geom8.x = i5;
            Geom geom9 = this.geomForBorders;
            if (is) {
                i6 = 11;
            } else {
                i6 = 49;
            }
            geom9.x = i6;
            this.geomBlackRect.x = is ? 5 : 43;
            Geom geom10 = this.geomInfoCell[0];
            if (is) {
                i7 = 11;
            } else {
                i7 = 49;
            }
            geom10.x = i7;
            this.geomInfoPlayer[0].x = is ? 35 : 73;
            if (this.numberOfPlayers == 2) {
                this.geomInfoCell[1].x = is ? 419 : 457;
                Geom geom11 = this.geomInfoPlayer[1];
                if (is) {
                    i10 = 300;
                } else {
                    i10 = 338;
                }
                geom11.x = i10;
            } else {
                this.geomInfoCell[1].x = is ? 419 : 457;
                this.geomInfoPlayer[1].x = is ? 300 : 338;
                this.geomInfoCell[2].x = is ? 419 : 457;
                this.geomInfoPlayer[2].x = is ? 300 : 338;
                Geom geom12 = this.geomInfoCell[3];
                if (is) {
                    i8 = 11;
                } else {
                    i8 = 49;
                }
                geom12.x = i8;
                Geom geom13 = this.geomInfoPlayer[3];
                if (is) {
                    i9 = 35;
                } else {
                    i9 = 73;
                }
                geom13.x = i9;
            }
            this.butStore.x = is ? 201 : 239;
            this.geomButStore.x = is ? 180 : 218;
            this.geomClock.x = is ? 207 : Input.Keys.F2;
        }
        if (States.screenHeight == 480 && States.screenWidth == 800) {
            for (byte i21 = 0; i21 < this.field.numberOfColors; i21 = (byte) (i21 + 1)) {
                this.geomButton[i21].x = is ? 705 : 27;
            }
            this.geomField.x = is ? 43 : 127;
            this.geomForBorders.x = is ? 43 : 127;
            this.geomBlackRect.x = is ? 33 : 117;
            this.geomInfoCell[0].x = is ? 43 : 127;
            this.geomInfoPlayer[0].x = is ? 79 : 163;
            if (this.numberOfPlayers == 2) {
                this.geomInfoCell[1].x = is ? 655 : 739;
                Geom geom14 = this.geomInfoPlayer[1];
                if (is) {
                    i4 = 527;
                } else {
                    i4 = 611;
                }
                geom14.x = i4;
            } else {
                this.geomInfoCell[1].x = is ? 655 : 739;
                this.geomInfoPlayer[1].x = is ? 527 : 611;
                this.geomInfoCell[2].x = is ? 655 : 739;
                this.geomInfoPlayer[2].x = is ? 527 : 611;
                this.geomInfoCell[3].x = is ? 43 : 127;
                Geom geom15 = this.geomInfoPlayer[3];
                if (is) {
                    i3 = 79;
                } else {
                    i3 = 163;
                }
                geom15.x = i3;
            }
            this.butStore.x = is ? 330 : 414;
            this.geomButStore.x = is ? 310 : 394;
            this.geomClock.x = is ? 340 : 424;
        }
        if (States.screenHeight == 480 && States.screenWidth == 854) {
            for (byte i22 = 0; i22 < this.field.numberOfColors; i22 = (byte) (i22 + 1)) {
                this.geomButton[i22].x = is ? 736 : 50;
            }
            this.geomField.x = is ? 52 : 172;
            this.geomForBorders.x = is ? 52 : 172;
            this.geomBlackRect.x = is ? 42 : 162;
            this.geomInfoCell[0].x = is ? 52 : 172;
            this.geomInfoPlayer[0].x = is ? 98 : 218;
            if (this.numberOfPlayers == 2) {
                this.geomInfoCell[1].x = is ? 664 : 784;
                Geom geom16 = this.geomInfoPlayer[1];
                if (is) {
                    i2 = 526;
                } else {
                    i2 = 646;
                }
                geom16.x = i2;
            } else {
                this.geomInfoCell[1].x = is ? 664 : 784;
                this.geomInfoPlayer[1].x = is ? 526 : 646;
                this.geomInfoCell[2].x = is ? 664 : 784;
                this.geomInfoPlayer[2].x = is ? 526 : 646;
                this.geomInfoCell[3].x = is ? 52 : 172;
                Geom geom17 = this.geomInfoPlayer[3];
                if (is) {
                    i = 98;
                } else {
                    i = 218;
                }
                geom17.x = i;
            }
            this.butStore.x = is ? 338 : 458;
            this.geomButStore.x = is ? 318 : 438;
            this.geomClock.x = is ? 348 : 468;
        }
        this.isUpdatePixField = true;
    }

    private void InitBoost() {
        this.boost_geomClock_font_x_2 = (int) (((float) this.geomClock.x) - (this.fontStore.getSpaceWidth() * 2.0f));
        this.boost_geomClock_font_x_3 = (int) (((float) this.geomClock.x) + (this.fontStore.getSpaceWidth() * 3.0f));
        this.boost_cellSize_div_2 = this.cellSize / 2;
        this.boost_cellSize_mul1_4 = ((float) this.cellSize) * 1.4f;
        this.boost_cellSize_div_6 = (float) (this.cellSize / 6);
        this.boost_geomButtonX_height_div_2 = (float) (this.geomButton[0].x + (this.geomButton[0].height / 2));
        this.boost_geomButton_height_mul_15 = ((float) this.geomButton[0].height) * 1.5f;
        this.boost_geomButton_height_mul_3 = this.geomButton[0].height * 3;
        this.boost_geomBlackRect_aspectX = Math.round(((float) this.geomBlackRect.width) * States.aspectX);
        this.boost_geomBlackRect_aspectY = Math.round(((float) this.geomBlackRect.height) * States.aspectY);
        this.boost_geomBlackRect_offset_aspectX = Math.round(((float) (this.geomBlackRect.x + States.cameraOffsetX)) * States.aspectX);
        this.boost_geomBlackRect_offset_aspectY = Math.round(((float) (this.geomBlackRect.y + States.cameraOffsetY)) * States.aspectY);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.files.FileHandle, boolean):void
     arg types: [com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.files.FileHandle, int]
     candidates:
      com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.graphics.g2d.TextureRegion, boolean):void
      com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.graphics.g2d.BitmapFont$BitmapFontData, com.badlogic.gdx.graphics.g2d.TextureRegion, boolean):void
      com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.files.FileHandle, boolean):void */
    private void InitObjects() {
        String pathBonusBig;
        this.soundExp = null;
        this.soundClick = Gdx.audio.newSound(Gdx.files.internal("data/Sounds/color_click.ogg"));
        this.soundFreeze = Gdx.audio.newSound(Gdx.files.internal("data/Sounds/freeze.ogg"));
        this.soundTeleport = Gdx.audio.newSound(Gdx.files.internal("data/Sounds/teleport.ogg"));
        this.soundMixer = Gdx.audio.newSound(Gdx.files.internal("data/Sounds/mixer.ogg"));
        this.soundShowAll = Gdx.audio.newSound(Gdx.files.internal("data/Sounds/showall.ogg"));
        this.soundBombClock = Gdx.audio.newSound(Gdx.files.internal("data/Sounds/bombclock.ogg"));
        this.soundBonus = Gdx.audio.newSound(Gdx.files.internal("data/Sounds/bonus.ogg"));
        this.soundBlindness = Gdx.audio.newSound(Gdx.files.internal("data/Sounds/blindness.ogg"));
        this.isUpdatePixField = true;
        this.boost_geomBlackRect_aspectX = Math.round(((float) this.geomBlackRect.width) * States.aspectX);
        this.boost_geomBlackRect_aspectY = Math.round(((float) this.geomBlackRect.height) * States.aspectY);
        this.boost_geomBlackRect_offset_aspectX = Math.round(((float) (this.geomBlackRect.x + States.cameraOffsetX)) * States.aspectX);
        this.boost_geomBlackRect_offset_aspectY = Math.round(((float) (this.geomBlackRect.y + States.cameraOffsetY)) * States.aspectY);
        this.pixField = new Pixmap(this.boost_geomBlackRect_aspectX, this.boost_geomBlackRect_aspectY, Pixmap.Format.RGBA8888);
        int w = 0;
        int h = 0;
        for (int i = 12; i > 0; i--) {
            if (((double) States.displayWidth) <= Math.pow(2.0d, (double) i)) {
                w = (int) Math.pow(2.0d, (double) i);
            }
            if (((double) States.displayHeight) <= Math.pow(2.0d, (double) i)) {
                h = (int) Math.pow(2.0d, (double) i);
            }
        }
        this.texField = new Texture(w, h, Pixmap.Format.RGBA8888);
        String path = States.PATH_GRAPH + States.resolution + "/";
        this.texBlackRect = new Texture(Gdx.files.internal(String.valueOf(path) + States.BLACK_RECTANGLE));
        InitSkin();
        this.butStore.SetParams(new Texture(Gdx.files.internal(String.valueOf(path) + "store_na.png")), new Texture(Gdx.files.internal(String.valueOf(path) + "store_a.png")), new Texture(Gdx.files.internal(String.valueOf(path) + States.ICON_BACK)), States.NONE);
        this.fontStore = new BitmapFont(Gdx.files.internal(String.valueOf(path) + States.FONT_GAME_STORE + ".fnt"), Gdx.files.internal(String.valueOf(path) + States.FONT_GAME_STORE + ".png"), false);
        this.texBonus = new Texture(Gdx.files.internal(String.valueOf(path) + "bons.png"));
        this.texBonusBig = null;
        this.texBonusesSmall = null;
        if (States.screenHeight == 240) {
            pathBonusBig = "data/Graph/320/";
        } else {
            pathBonusBig = path;
        }
        this.texBonusBig = new Texture(Gdx.files.internal(String.valueOf(pathBonusBig) + "bons_big.png"));
        this.texBut2NA = new Texture(Gdx.files.internal(String.valueOf(path) + States.TEX_BUTTON_MIDDLE_NA));
        this.texBut2A = new Texture(Gdx.files.internal(String.valueOf(path) + States.TEX_BUTTON_MIDDLE_A));
        for (byte i2 = 0; i2 < 6; i2 = (byte) (i2 + 1)) {
            this.buttonBonusInStore[i2].SetParams(this.texBut2NA, this.texBut2A, this.texBonus, i2);
        }
        this.peTeleport = null;
        this.peBonus = new ParticleEffect();
        this.peFindBonus = new ParticleEffect[8];
        this.peBonus.load(Gdx.files.internal("data/Particles/bonus"), Gdx.files.internal("data/Particles/"));
        for (int i3 = 0; i3 < 8; i3++) {
            this.peFindBonus[i3] = new ParticleEffect();
            this.peFindBonus[i3].load(Gdx.files.internal("data/Particles/find_bonus"), Gdx.files.internal("data/Particles/"));
        }
        for (byte b = 0; b < 6; b = (byte) (b + 1)) {
            ParticleEmitter emitter = this.peBonus.findEmitter(String.valueOf((int) b));
            emitter.setSprite(new Sprite(this.texBonus, this.bonusStoreWidth * b, 0, this.bonusStoreWidth, this.bonusStoreWidth));
            while (!emitter.isComplete()) {
                emitter.allowCompletion();
            }
        }
        Iterator<ParticleEmitter> it = this.peBonus.getEmitters().iterator();
        while (it.hasNext()) {
            ParticleEmitter emitter2 = it.next();
            emitter2.getScale().setHighMax((emitter2.getScale().getHighMax() * ((float) States.screenHeight)) / 320.0f);
            emitter2.getVelocity().setHighMax((emitter2.getVelocity().getHighMax() * ((float) States.screenHeight)) / 320.0f);
        }
        for (int i4 = 0; i4 < 8; i4++) {
            Iterator<ParticleEmitter> it2 = this.peFindBonus[i4].getEmitters().iterator();
            while (it2.hasNext()) {
                ParticleEmitter emitter3 = it2.next();
                emitter3.getScale().setHighMax((emitter3.getScale().getHighMax() * ((float) States.screenHeight)) / 320.0f);
                emitter3.getVelocity().setHighMax((emitter3.getVelocity().getHighMax() * ((float) States.screenHeight)) / 320.0f);
            }
        }
    }

    private void InitSkin() {
        String path_skin = States.PATH_GRAPH + States.skin + "/" + States.resolution + "/";
        String path_skin2 = path_skin;
        if (States.screenWidth == 320) {
            path_skin2 = States.PATH_GRAPH + States.skin + "/" + "240_320/";
        }
        String strAdd = "";
        if (this.field.numberOfColors == 5) {
            strAdd = "5";
        }
        this.texButtons = new Texture(Gdx.files.internal(String.valueOf(path_skin) + "but0" + strAdd + ".png"));
        this.texCells = new Texture(Gdx.files.internal(String.valueOf(path_skin2) + "cells" + strAdd + ".png"));
        this.peBomb = null;
    }

    private void InitPeTeleport() {
        this.peTeleport = new ParticleEffect();
        this.peTeleport.load(Gdx.files.internal("data/Particles/teleport"), Gdx.files.internal("data/Particles/"));
        Iterator<ParticleEmitter> it = this.peTeleport.getEmitters().iterator();
        while (it.hasNext()) {
            ParticleEmitter emitter = it.next();
            emitter.getScale().setHighMax((emitter.getScale().getHighMax() * ((float) States.screenHeight)) / 320.0f);
            emitter.getVelocity().setHighMax((emitter.getVelocity().getHighMax() * ((float) States.screenHeight)) / 320.0f);
        }
    }

    private void InitPeBomb() {
        this.peBomb = new ParticleEffect();
        this.peBomb.load(Gdx.files.internal("data/Particles/bomb"), Gdx.files.internal(States.PATH_GRAPH + States.skin + "/" + "particles/"));
        Iterator<ParticleEmitter> it = this.peBomb.getEmitters().iterator();
        while (it.hasNext()) {
            ParticleEmitter emitter = it.next();
            emitter.getScale().setHighMin((emitter.getScale().getHighMin() * ((float) States.screenHeight)) / 320.0f);
            emitter.getScale().setHighMax((emitter.getScale().getHighMax() * ((float) States.screenHeight)) / 320.0f);
            emitter.getVelocity().setHighMax((emitter.getVelocity().getHighMax() * ((float) States.screenHeight)) / 320.0f);
        }
        this.soundExp = Gdx.audio.newSound(Gdx.files.internal("data/Sounds/exp.ogg"));
    }

    private void InitTextureBonusSmall() {
        this.texBonusesSmall = new Texture(Gdx.files.internal(String.valueOf(States.PATH_GRAPH + States.resolution + "/") + "bons_small.png"));
        if (States.isAntialiasing) {
            this.texBonusesSmall.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        }
    }

    private void InitPost() {
        Pixmap pixmap1 = new Pixmap(Gdx.files.internal("data/Graph/light.png"));
        Pixmap pixmap2 = new Pixmap((int) Input.Keys.META_SHIFT_RIGHT_ON, (int) Input.Keys.META_SHIFT_RIGHT_ON, Pixmap.Format.RGBA8888);
        Pixmap pixmap3 = new Pixmap(Gdx.files.internal(States.PATH_GRAPH + States.skin + "/c.png"));
        ByteBuffer b1 = pixmap1.getPixels();
        ByteBuffer b2 = pixmap2.getPixels();
        byte ind = 0;
        byte color = 0;
        while (color < 7) {
            if (this.field.numberOfColors == 5) {
                if (States.skin == "red-blue") {
                    if (color == 1 || color == 5) {
                        color = (byte) (color + 1);
                    }
                } else if (color == 1 || color == 4) {
                    color = (byte) (color + 1);
                }
            }
            int c = pixmap3.getPixel(color, 0);
            for (int i = 0; i < 65536; i += 4) {
                b2.putInt(i, c);
                b2.put(i + 3, b1.get(i + 3));
            }
            this.texButton[ind][2] = new Texture(pixmap2);
            color = (byte) (color + 1);
            ind = (byte) (ind + 1);
        }
        pixmap1.dispose();
        pixmap2.dispose();
        pixmap3.dispose();
        this.timer_timeoutBeforeNextStep = 0.2f;
    }

    private void loadTexturesPause() {
        this.isResumePause = false;
        String path = States.PATH_GRAPH + States.resolution + "/";
        this.texStore = new Texture(Gdx.files.internal(String.valueOf(path) + "store.png"));
        this.texButNA = new Texture(Gdx.files.internal(String.valueOf(path) + "but_na.png"));
        this.texButA = new Texture(Gdx.files.internal(String.valueOf(path) + "but_a.png"));
        this.butInPause[0].SetParams(this.texButNA, this.texButA, new Texture(Gdx.files.internal(String.valueOf(path) + States.ICON_MAIN_MENU)), (byte) 3);
        this.butInPause[1].SetParams(this.texButNA, this.texButA, new Texture(Gdx.files.internal(String.valueOf(path) + "settings.png")), States.NONE);
        this.butInPause[2].SetParams(this.texButNA, this.texButA, new Texture(Gdx.files.internal(String.valueOf(path) + States.ICON_BACK)), States.NONE);
        if (States.isAntialiasing) {
            for (ButtonPic b : this.butInPause) {
                b.texBackAct.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
                b.texBackNonAct.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
                b.texFront.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
            }
        }
    }

    public void update() {
        if (this.pos == 6 && this.partBonusAnim == 0 && this.stepBlind < (this.numberOfPlayers * 3) - 1 && States.skillAI[currentPlayer] == 0 && (isInputCoordsIn(this.geomButStore) || this.inp.isMenu)) {
            byte b = this.inp.down;
            this.inp.getClass();
            if (b == 10 || this.inp.isMenu) {
                this.isStoreShow = !this.isStoreShow;
                this.butStore.setBackActive(this.isStoreShow);
                if (this.inp.isMenu) {
                    GameLoopInput gameLoopInput = this.inp;
                    this.inp.getClass();
                    gameLoopInput.down = States.NONE;
                } else {
                    GameLoopInput gameLoopInput2 = this.inp;
                    this.inp.getClass();
                    gameLoopInput2.down = Keyboard.STATE_NUMBER;
                }
                this.inp.isMenu = false;
                GameLoopInput gameLoopInput3 = this.inp;
                this.inp.getClass();
                gameLoopInput3.buttonDown = States.NONE;
                GameLoopInput gameLoopInput4 = this.inp;
                this.inp.getClass();
                gameLoopInput4.buttonUp = States.NONE;
            }
        }
        if (this.timer_timeoutBeforeNextStep > 0.0f) {
            this.timer_timeoutBeforeNextStep = countdownTimer(this.timer_timeoutBeforeNextStep);
        } else if (this.pos != 10) {
            if (this.inp.isBack) {
                this.inp.x = 0;
                this.inp.y = 0;
                GameLoopInput gameLoopInput5 = this.inp;
                this.inp.getClass();
                gameLoopInput5.buttonUp = States.NONE;
                GameLoopInput gameLoopInput6 = this.inp;
                this.inp.getClass();
                gameLoopInput6.buttonDown = States.NONE;
                this.inp.isBack = false;
                if (States.state != 98) {
                    this.posBeforePause = this.pos;
                    if (this.soundShowAll != null) {
                        this.soundShowAll.stop();
                    }
                    this.soundTeleport.stop();
                    if (this.soundExp != null) {
                        this.soundExp.stop();
                    }
                    this.soundBombClock.stop();
                    States.state = States.PAUSE;
                } else if (this.pos == 8) {
                    for (int i = 0; i < this.field.numberOfColors; i++) {
                        for (int u = 2; u < 3; u++) {
                            this.texButton[i][u].dispose();
                        }
                    }
                    this.texButtons.dispose();
                    this.texCells.dispose();
                    if (this.peBomb != null) {
                        this.peBomb.dispose();
                    }
                    InitSkin();
                    InitPost();
                    this.pos = 6;
                } else {
                    this.pos = this.posBeforePause;
                    GameLoopInput gameLoopInput7 = this.inp;
                    this.inp.getClass();
                    gameLoopInput7.buttonUp = 50;
                    States.state = States.NONE;
                }
            }
            if (this.isShowBonus) {
                this.timer_showFindedBonus = countdownTimer(this.timer_showFindedBonus);
                if (this.timer_showFindedBonus < 0.0f) {
                    this.isShowBonus = false;
                    return;
                }
                return;
            }
            if (States.state != 98) {
                if (this.timer_afterPause > 0.0f) {
                    this.timer_afterPause = countdownTimer(this.timer_afterPause);
                    GameLoopInput gameLoopInput8 = this.inp;
                    this.inp.getClass();
                    gameLoopInput8.buttonUp = States.NONE;
                    this.inp.x = 0;
                    return;
                } else if (this.pos == 9) {
                    updateShowBorders();
                    return;
                } else if (this.isStoreShow) {
                    updateStore();
                } else if (!this.isBonusFinded) {
                    updateGame();
                }
            } else if (this.pos == 8) {
                updateSettings();
                return;
            } else {
                updatePause();
            }
            isDone();
        } else if (!this.isStart) {
            this.isStart = true;
        } else {
            InitPost();
            this.pos = 6;
        }
    }

    private void updateGame() {
        if (this.isRefreshPlayerInfo) {
            refreshPlayerInfo();
        }
        if (this.isBlind && this.partBonusAnim == 0 && this.animBlind < 10) {
            this.animBlind++;
            if (this.animBlind == 10) {
                this.isUpdatePixField = true;
            }
        }
        if (this.isUnBlind) {
            int i = this.animBlind - 1;
            this.animBlind = i;
            if (i == 0) {
                this.isUnBlind = false;
                this.isUpdatePixField = true;
                return;
            }
            return;
        }
        if ((this.pos == 6 || this.pos == 1) && States.skillAI[currentPlayer] == 0 && !this.isBlind && isInputCoordsIn(this.geomForBorders)) {
            byte b = this.inp.down;
            this.inp.getClass();
            if (b == 10) {
                GameLoopInput gameLoopInput = this.inp;
                this.inp.getClass();
                gameLoopInput.buttonDown = States.NONE;
                this.posBeforeResume = this.pos;
                this.pos = 9;
                this.isUpdatePixField = true;
                return;
            }
        }
        if (States.skillAI[currentPlayer] == 0 && this.animLight == 0) {
            if ((this.pos == 6 || this.pos == 1) && ((float) this.inp.x) > ((float) ((this.geomButton[0].x - this.cellSize) + States.cameraOffsetX)) * States.aspectX && ((float) this.inp.x) < ((float) (this.geomButton[0].x + this.geomButton[0].width + 4)) * States.aspectX) {
                for (byte i2 = 0; i2 < this.field.numberOfColors; i2 = (byte) (i2 + 1)) {
                    if (((float) (States.displayHeight - this.inp.y)) > ((float) (this.geomButton[i2].y + States.cameraOffsetY)) * States.aspectY && ((float) (States.displayHeight - this.inp.y)) < ((float) (this.geomButton[i2].y + this.geomButton[i2].height + States.cameraOffsetY)) * States.aspectY) {
                        byte b2 = this.inp.buttonUp;
                        this.inp.getClass();
                        if (b2 == 100) {
                            this.inp.buttonDown = i2;
                        } else {
                            this.inp.buttonUp = i2;
                        }
                    }
                }
            }
            if (isInputCoordsIn(this.geomClock)) {
                byte b3 = this.inp.down;
                this.inp.getClass();
                if (b3 == 10) {
                    this.isClock = !this.isClock;
                    Profile.profile.isClock = this.isClock;
                    for (byte p = 0; p < 4; p = (byte) (p + 1)) {
                        if (Profile.isActiveProfile[p] && Profile.profile.numberOfProfile == Profile.pp[p].numberOfProfile) {
                            Profile.pp[p].isClock = Profile.profile.isClock;
                        }
                    }
                    GameLoopInput gameLoopInput2 = this.inp;
                    this.inp.getClass();
                    gameLoopInput2.down = Keyboard.STATE_NUMBER;
                    this.isUpdatePixField = true;
                }
            }
        }
        if (this.pos == 11) {
            this.timer_countdownBomb = countdownTimer(this.timer_countdownBomb);
        }
        if (this.pos == 4) {
            if (this.peBomb == null) {
                InitPeBomb();
            }
            this.timer_useBonus = countdownTimer(this.timer_useBonus);
            if (this.timer_useBonus < 0.0f) {
                this.timer_countdownBomb = countdownTimer(this.timer_countdownBomb);
                byte b4 = this.inp.down;
                this.inp.getClass();
                if (b4 == 10 && States.skillAI[currentPlayer] == 0 && isInputCoordsIn(this.geomField)) {
                    this.soundBombClock.stop();
                    this.isBombClockTick = false;
                    this.bombX = (int) ((((float) this.inp.x) / States.aspectX) - ((float) States.cameraOffsetX));
                    this.bombY = (int) ((((float) (States.displayHeight - this.inp.y)) / States.aspectY) - ((float) States.cameraOffsetY));
                    this.timer_countdownBomb = 3.5f;
                }
                byte b5 = this.inp.buttonUp;
                this.inp.getClass();
                if ((b5 == 50 && this.bombX != 0) || States.skillAI[currentPlayer] != 0) {
                    if (Profile.profile.settingSounds && !this.isBombClockTick) {
                        this.soundBombClock.play();
                        this.isBombClockTick = true;
                    }
                    if (((double) this.timer_countdownBomb) < 0.4d) {
                        this.field.StartBonusBomb((this.bombX - this.geomField.x) / this.cellSize, (this.bombY - this.geomField.y) / this.cellSize, this.states.bombRadius);
                        byte color = 0;
                        for (int i3 = 0; i3 < 7; i3++) {
                            int s = (int) (((double) this.field.blownColors[color]) * 0.4d);
                            if (this.field.numberOfColors == 5) {
                                if (States.skin == "red-blue") {
                                    if (i3 == 1 || i3 == 5) {
                                        s = 0;
                                        color = (byte) (color - 1);
                                    }
                                } else if (i3 == 1 || i3 == 4) {
                                    s = 0;
                                    color = (byte) (color - 1);
                                }
                            }
                            ParticleEmitter emitter = this.peBomb.findEmitter(String.valueOf(i3));
                            if (s > 0) {
                                s++;
                            }
                            emitter.setMaxParticleCount(s);
                            this.peBomb.findEmitter("1" + String.valueOf(i3)).setMaxParticleCount(Math.round((float) s));
                            this.peBomb.findEmitter("2" + String.valueOf(i3)).setMaxParticleCount(Math.round((float) (s / 2)));
                            color = (byte) (color + 1);
                        }
                        this.pos = Statistic.A_JUMPER;
                        this.animAfterBomb = 0;
                    }
                }
            }
        }
        if ((this.pos == 6 || this.pos == 1) && this.animCell == 1 && this.animLight == 0) {
            boolean isFindedBonus = false;
            this.isRefreshPlayerInfo = true;
            if (States.skillAI[currentPlayer] != 0) {
                GameLoopInput gameLoopInput3 = this.inp;
                this.inp.getClass();
                gameLoopInput3.buttonDown = States.NONE;
                GameLoopInput gameLoopInput4 = this.inp;
                this.inp.getClass();
                gameLoopInput4.buttonUp = 50;
                this.inp.x = 200;
                byte bonus = States.NONE;
                if (this.pos != 1) {
                    bonus = (byte) this.field.UseBonusAI(currentPlayer, this.isBlind);
                }
                if (bonus == 100) {
                    if (this.pos != 1) {
                        isFindedBonus = this.field.updatePlayerField(currentPlayer, this.field.AI(currentPlayer, States.skillAI[currentPlayer], this.isBlind));
                    } else {
                        this.timer_useBonus = countdownTimer(this.timer_useBonus);
                        if (this.timer_useBonus >= 0.0f) {
                            return;
                        }
                        if (this.stepFreeze == 2) {
                            isFindedBonus = this.field.updatePlayerField(currentPlayer, this.field.freezeStepAI1);
                            this.stepFreeze = (byte) (this.stepFreeze - 1);
                            this.timer_useBonus = 0.5f;
                        } else if (this.partBonusAnim == 0) {
                            isFindedBonus = this.field.updatePlayerField(currentPlayer, this.field.freezeStepAI2);
                            this.pos = 6;
                        }
                    }
                    if (isFindedBonus) {
                        if (Profile.profile.settingSounds) {
                            this.soundBonus.play();
                        }
                        if (Profile.profile.settingVibrate) {
                            Gdx.input.vibrate(30);
                        }
                        this.isBonusFinded = true;
                    }
                    this.animCell = 4;
                    this.animLight = this.ANIM_LIGHT_DURATION;
                } else {
                    byte[] bArr = States.playerBonus[currentPlayer];
                    bArr[bonus] = (byte) (bArr[bonus] - 1);
                    this.partBonusAnim = 2;
                    GameLoopInput gameLoopInput5 = this.inp;
                    this.inp.getClass();
                    gameLoopInput5.down = Keyboard.STATE_NUMBER;
                    this.pos = bonus;
                    byte b6 = 0;
                    while (b6 < 6) {
                        ParticleEmitter emitter2 = this.peBonus.findEmitter(String.valueOf((int) b6));
                        emitter2.setMaxParticleCount(b6 == bonus ? 1 : 0);
                        while (!emitter2.isComplete()) {
                            emitter2.allowCompletion();
                        }
                        b6 = (byte) (b6 + 1);
                    }
                    if (bonus == 2) {
                        if (Profile.profile.settingSounds) {
                            this.soundMixer.play();
                        }
                        this.field.StartBonusMixer();
                        this.animCell = 4;
                        this.field.playerWhichBlind = 5;
                    }
                    if (bonus == 1) {
                        if (Profile.profile.settingSounds) {
                            this.soundFreeze.play();
                        }
                        this.isUpdatePixField = true;
                        this.stepFreeze = 2;
                        this.timer_useBonus = 1.5f;
                    }
                    if (bonus == 3) {
                        if (Profile.profile.settingSounds) {
                            this.soundTeleport.play();
                        }
                        this.field.StartBonusTeleport(currentPlayer);
                        this.isUpdatePixField = true;
                        this.stepParticleOfTeleport = 0;
                    }
                    if (bonus == 4) {
                        this.timer_countdownBomb = 3.5f;
                        this.isUpdatePixField = true;
                        this.bombX = (this.field.bombAIX * this.cellSize) + this.geomField.x;
                        this.bombY = (this.field.bombAIY * this.cellSize) + this.geomField.y;
                        this.timer_useBonus = 2.0f;
                        this.isBombClockTick = false;
                    }
                    if (bonus == 5) {
                        this.pos = 6;
                        this.isBlind = true;
                        this.stepBlind = (this.numberOfPlayers * 3) - 1;
                        if (Profile.profile.settingSounds) {
                            this.soundBlindness.play();
                        }
                        this.field.playerWhichBlind = currentPlayer;
                    }
                }
            } else {
                byte b7 = this.inp.buttonUp;
                this.inp.getClass();
                if (b7 != 100) {
                    byte b8 = this.inp.buttonUp;
                    this.inp.getClass();
                    if (b8 != 50) {
                        boolean isContinue = true;
                        for (int p2 = 0; p2 < this.numberOfPlayers; p2++) {
                            if (this.inp.buttonUp == this.field.getPlayerColor(p2)) {
                                isContinue = false;
                            }
                        }
                        if (isContinue) {
                            if (Profile.profile.settingSounds) {
                                this.soundClick.play();
                            }
                            boolean isFindedBonus2 = this.field.updatePlayerField(currentPlayer, this.inp.buttonUp);
                            this.isRefreshPlayerInfo = true;
                            if (isFindedBonus2) {
                                if (Profile.profile.settingSounds) {
                                    this.soundBonus.play();
                                }
                                if (Profile.profile.settingVibrate) {
                                    Gdx.input.vibrate(30);
                                }
                                for (int i4 = 0; i4 < 8; i4++) {
                                    if (this.field.findedBonus[i4] != 100 && Profile.isActiveProfile[currentPlayer]) {
                                        if (Tournaments.getType() == Tournaments.TYPE_SINGLE) {
                                            Profile.pp[currentPlayer].nFindedBonuses++;
                                        } else {
                                            Profile.pp[currentPlayer].multi_nFindedBonuses++;
                                        }
                                    }
                                }
                                this.isBonusFinded = true;
                                this.timer_showFindedBonus = 1.0f;
                                this.isShowBonus = true;
                            }
                            if (this.pos == 1) {
                                byte b9 = this.stepFreeze;
                                this.stepFreeze = (byte) (b9 - 1);
                                if (b9 == 0) {
                                    this.pos = 6;
                                }
                            }
                            this.animCell = 4;
                            GameLoopInput gameLoopInput6 = this.inp;
                            this.inp.getClass();
                            gameLoopInput6.buttonUp = States.NONE;
                            this.animLight = this.ANIM_LIGHT_DURATION;
                        }
                    }
                }
            }
        }
        if (this.pos == 0) {
            this.timer_showAll = countdownTimer(this.timer_showAll);
            if (this.timer_showAll < 0.0f) {
                this.pos = 6;
                this.isUpdatePixField = true;
            }
        }
        if (this.pos == 2 && this.partBonusAnim == 0) {
            this.pos = 6;
            this.animCell = 1;
        }
        if (this.animLight != 1 || this.partBonusAnim != 0) {
            return;
        }
        if (this.animBlind == 10 || this.animBlind == 0) {
            this.animLight--;
            if (this.pos != 1) {
                nextPlayer();
                this.timer_timeoutBeforeNextStep = 0.2f;
            }
        }
    }

    private void updateShowBorders() {
        byte b = this.inp.buttonUp;
        this.inp.getClass();
        if (b == 50 || !isInputCoordsIn(this.geomForBorders)) {
            this.pos = this.posBeforeResume;
            this.isUpdatePixField = true;
        }
    }

    private void updateStore() {
        int i;
        boolean z;
        byte b = this.inp.down;
        this.inp.getClass();
        if (b == 10) {
            for (ButtonPic b2 : this.buttonBonusInStore) {
                b2.setBackActive(isInputCoordsIn(b2));
            }
        }
        byte b3 = this.inp.buttonUp;
        this.inp.getClass();
        if (b3 == 50) {
            for (byte i2 = 0; i2 < this.numberOfPlayers; i2 = (byte) (i2 + 1)) {
                if (States.skillAI[i2] == 0 && isInputCoordsIn(this.butAuto[i2])) {
                    boolean[] zArr = this.isAuto;
                    if (this.isAuto[i2]) {
                        z = false;
                    } else {
                        z = true;
                    }
                    zArr[i2] = z;
                    this.butAuto[i2].setBackActive(false);
                }
            }
            if (States.typeOfGame == 1 && States.skillAI[currentPlayer] == 0 && !this.isAuto[currentPlayer]) {
                for (byte i3 = 0; i3 < 6; i3 = (byte) (i3 + 1)) {
                    if (isInputCoordsIn(this.buttonBonusInStore[i3])) {
                        if (States.playerBonus[currentPlayer][i3] > 0) {
                            this.isStoreShow = false;
                            this.butStore.setBackActive(false);
                            byte[] bArr = States.playerBonus[currentPlayer];
                            bArr[i3] = (byte) (bArr[i3] - 1);
                            this.partBonusAnim = 2;
                            GameLoopInput gameLoopInput = this.inp;
                            this.inp.getClass();
                            gameLoopInput.down = Keyboard.STATE_NUMBER;
                            this.pos = i3;
                            for (byte b4 = 0; b4 < 6; b4 = (byte) (b4 + 1)) {
                                ParticleEmitter emitter = this.peBonus.findEmitter(String.valueOf((int) b4));
                                if (b4 == i3) {
                                    i = 1;
                                } else {
                                    i = 0;
                                }
                                emitter.setMaxParticleCount(i);
                                while (!emitter.isComplete()) {
                                    emitter.allowCompletion();
                                }
                            }
                            if (this.pos == 2) {
                                if (Profile.profile.settingSounds) {
                                    this.soundMixer.play();
                                }
                                if (Profile.isActiveProfile[currentPlayer]) {
                                    if (Tournaments.getType() == Tournaments.TYPE_SINGLE) {
                                        Profile.pp[currentPlayer].nMixer++;
                                    } else {
                                        Profile.pp[currentPlayer].multi_nMixer++;
                                    }
                                }
                                this.field.StartBonusMixer();
                                this.animCell = 4;
                                if (this.isBlind) {
                                    this.field.playerWhichBlind = 5;
                                    this.field.isMixerOnBlind = true;
                                }
                            }
                            if (this.pos == 1) {
                                if (Profile.profile.settingSounds) {
                                    this.soundFreeze.play();
                                }
                                if (Profile.isActiveProfile[currentPlayer]) {
                                    if (Tournaments.getType() == Tournaments.TYPE_SINGLE) {
                                        Profile.pp[currentPlayer].nFreezes++;
                                    } else {
                                        Profile.pp[currentPlayer].multi_nFreezes++;
                                    }
                                }
                                this.isUpdatePixField = true;
                                this.stepFreeze = 1;
                            }
                            if (this.pos == 3) {
                                if (Profile.profile.settingSounds) {
                                    this.soundTeleport.play();
                                }
                                if (Profile.isActiveProfile[currentPlayer]) {
                                    if (Tournaments.getType() == Tournaments.TYPE_SINGLE) {
                                        Profile.pp[currentPlayer].nTeleport++;
                                    } else {
                                        Profile.pp[currentPlayer].multi_nTeleport++;
                                    }
                                }
                                this.field.StartBonusTeleport(currentPlayer);
                                this.isUpdatePixField = true;
                                this.stepParticleOfTeleport = 0;
                            }
                            if (this.pos == 4) {
                                if (Profile.isActiveProfile[currentPlayer]) {
                                    if (Tournaments.getType() == Tournaments.TYPE_SINGLE) {
                                        Profile.pp[currentPlayer].nBlowedBombs++;
                                    } else {
                                        Profile.pp[currentPlayer].multi_nBlowedBombs++;
                                    }
                                }
                                this.timer_countdownBomb = 3.5f;
                                this.isUpdatePixField = true;
                            }
                            if (this.pos == 0) {
                                if (Profile.profile.settingSounds) {
                                    this.soundShowAll.play();
                                }
                                if (Profile.isActiveProfile[currentPlayer]) {
                                    if (Tournaments.getType() == Tournaments.TYPE_SINGLE) {
                                        Profile.pp[currentPlayer].nEye++;
                                    } else {
                                        Profile.pp[currentPlayer].multi_nEye++;
                                    }
                                }
                                this.timer_showAll = 5.0f;
                                this.isUpdatePixField = true;
                            }
                            if (this.pos == 5) {
                                this.pos = 6;
                                this.isBlind = true;
                                this.stepBlind = (this.numberOfPlayers * 3) - 1;
                                if (Profile.profile.settingSounds) {
                                    this.soundBlindness.play();
                                }
                                if (Profile.isActiveProfile[currentPlayer]) {
                                    if (Tournaments.getType() == Tournaments.TYPE_SINGLE) {
                                        Profile.pp[currentPlayer].nBlind++;
                                    } else {
                                        Profile.pp[currentPlayer].multi_nBlind++;
                                    }
                                }
                                this.field.playerWhichBlind = currentPlayer;
                            }
                        }
                        GameLoopInput gameLoopInput2 = this.inp;
                        this.inp.getClass();
                        gameLoopInput2.down = States.NONE;
                    }
                }
            }
            this.inp.x = 0;
        }
        byte b5 = this.inp.down;
        this.inp.getClass();
        if (b5 == 10 || this.inp.isMenu) {
            for (byte i4 = 0; i4 < this.numberOfPlayers; i4 = (byte) (i4 + 1)) {
                if (States.skillAI[i4] == 0) {
                    this.butAuto[i4].setBackActive(isInputCoordsIn(this.butAuto[i4]));
                }
            }
            if (this.field.numberOfPlayers == 2 && !this.inp.isMenu && (isInputCoordsIn(this.geomStore) || isInputCoordsIn(this.geomInfo[0]) || isInputCoordsIn(this.geomInfo[1]))) {
                return;
            }
            if (this.field.numberOfPlayers != 4 || this.inp.isMenu || ((!isInputCoordsIn(this.geomStore) || this.isAuto[currentPlayer] || States.skillAI[currentPlayer] != 0) && !isInputCoordsIn(this.geomInfo[0]) && !isInputCoordsIn(this.geomInfo[1]) && !isInputCoordsIn(this.geomInfo[2]) && !isInputCoordsIn(this.geomInfo[3]))) {
                this.inp.isMenu = false;
                this.isStoreShow = false;
                this.butStore.setBackActive(false);
                GameLoopInput gameLoopInput3 = this.inp;
                this.inp.getClass();
                gameLoopInput3.down = Keyboard.STATE_NUMBER;
            }
        }
    }

    private void updatePause() {
        if (!this.isResume) {
            byte b = this.inp.down;
            this.inp.getClass();
            if (b == 10) {
                for (ButtonPic b2 : this.butInPause) {
                    b2.setBackActive(isInputCoordsIn(b2));
                }
            }
            byte b3 = this.inp.buttonUp;
            this.inp.getClass();
            if (b3 == 50) {
                if (isInputCoordsIn(this.butInPause[0])) {
                    if (this.isQuitPress) {
                        if (Profile.profile.settingSounds) {
                            States.click.play();
                        }
                        if (this.animLight != 0) {
                            byte b4 = (byte) (currentPlayer + 1);
                            currentPlayer = b4;
                            if (b4 == this.numberOfPlayers) {
                                currentPlayer = 0;
                            }
                        }
                        for (byte i = 0; i < 4; i = (byte) (i + 1)) {
                            if (Profile.isActiveProfile[i]) {
                                if (Tournaments.getType() == Tournaments.TYPE_SINGLE) {
                                    Profile.pT[Profile.pp[i].numberOfProfile].nGamesAbandoned++;
                                }
                                if (Tournaments.getType() == Tournaments.TYPE_MULTY) {
                                    Profile.pT[Profile.pp[i].numberOfProfile].multi_nGamesAbandoned++;
                                }
                            }
                        }
                        Profile.pT[Profile.profile.numberOfProfile].isClock = Profile.profile.isClock;
                        Profile.pT[Profile.profile.numberOfProfile].settingMusic = Profile.profile.settingMusic;
                        Profile.pT[Profile.profile.numberOfProfile].settingSkin = Profile.profile.settingSkin;
                        Profile.pT[Profile.profile.numberOfProfile].settingSounds = Profile.profile.settingSounds;
                        Profile.pT[Profile.profile.numberOfProfile].settingVibrate = Profile.profile.settingVibrate;
                        States.Init();
                        States.state = 3;
                    } else {
                        this.isQuitPress = true;
                    }
                    this.inp.x = 0;
                    GameLoopInput gameLoopInput = this.inp;
                    this.inp.getClass();
                    gameLoopInput.buttonUp = States.NONE;
                } else {
                    this.isQuitPress = false;
                }
                if (isInputCoordsIn(this.butInPause[1])) {
                    if (Profile.profile.settingSounds) {
                        States.click.play();
                    }
                    this.pos = 8;
                    this.inp.x = 0;
                } else if (isInputCoordsIn(this.butInPause[2])) {
                    if (Profile.profile.settingSounds) {
                        States.click.play();
                    }
                    States.state = this.butInPause[2].getState();
                    this.pos = this.posBeforePause;
                    this.timer_afterPause = 1.0f;
                    GameLoopInput gameLoopInput2 = this.inp;
                    this.inp.getClass();
                    gameLoopInput2.buttonUp = 50;
                }
            }
        } else {
            this.timer_afterResume = countdownTimer(this.timer_afterResume);
            if (countdownTimer(this.timer_afterResume) < 0.0f) {
                this.isResume = false;
                this.pos = this.posBeforeResume;
            }
        }
    }

    /* JADX WARN: Type inference failed for: r0v177, types: [int] */
    /* JADX WARN: Type inference failed for: r0v185, types: [int] */
    /* JADX WARN: Type inference failed for: r0v317, types: [int] */
    /* JADX WARN: Type inference failed for: r0v337, types: [int] */
    /* JADX WARN: Type inference failed for: r0v379, types: [int] */
    /* JADX WARN: Type inference failed for: r0v401, types: [int] */
    /* JADX WARN: Type inference failed for: r0v434, types: [int] */
    /* JADX WARN: Type inference failed for: r0v493, types: [int] */
    /* JADX WARN: Type inference failed for: r0v523, types: [int] */
    /* JADX WARN: Type inference failed for: r0v586, types: [int] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.g2d.SpriteBatch.draw(com.badlogic.gdx.graphics.Texture, float, float, float, float, int, int, int, int, boolean, boolean):void
     arg types: [com.badlogic.gdx.graphics.Texture, float, float, float, float, int, int, ?, ?, int, int]
     candidates:
      com.badlogic.gdx.graphics.g2d.SpriteBatch.draw(com.badlogic.gdx.graphics.g2d.TextureRegion, float, float, float, float, float, float, float, float, float, boolean):void
      com.badlogic.gdx.graphics.g2d.SpriteBatch.draw(com.badlogic.gdx.graphics.Texture, float, float, float, float, int, int, int, int, boolean, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.g2d.SpriteBatch.draw(com.badlogic.gdx.graphics.Texture, float, float, float, float, int, int, int, int, boolean, boolean):void
     arg types: [com.badlogic.gdx.graphics.Texture, float, float, float, float, int, int, int, int, int, int]
     candidates:
      com.badlogic.gdx.graphics.g2d.SpriteBatch.draw(com.badlogic.gdx.graphics.g2d.TextureRegion, float, float, float, float, float, float, float, float, float, boolean):void
      com.badlogic.gdx.graphics.g2d.SpriteBatch.draw(com.badlogic.gdx.graphics.Texture, float, float, float, float, int, int, int, int, boolean, boolean):void */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void render(com.badlogic.gdx.Application r31) {
        /*
            r30 = this;
            r0 = r30
            byte r0 = r0.pos
            r3 = r0
            r4 = 8
            if (r3 != r4) goto L_0x000d
            r30.renderSettings(r31)
        L_0x000c:
            return
        L_0x000d:
            r24 = 0
            r0 = r30
            byte r0 = r0.pos
            r3 = r0
            r4 = 9
            if (r3 != r4) goto L_0x003c
            r0 = r30
            boolean r0 = r0.isUpdatePixField
            r3 = r0
            if (r3 != 0) goto L_0x003c
            byte r3 = com.machaon.quadratum.States.state
            r4 = 98
            if (r3 == r4) goto L_0x003c
            r0 = r30
            byte r0 = r0.cadr
            r3 = r0
            r4 = 1
            if (r3 == r4) goto L_0x000c
            r24 = 1
            r0 = r30
            byte r0 = r0.cadr
            r3 = r0
            int r3 = r3 + 1
            byte r3 = (byte) r3
            r0 = r3
            r1 = r30
            r1.cadr = r0
        L_0x003c:
            r0 = r30
            byte r0 = r0.pos
            r3 = r0
            r4 = 6
            if (r3 != r4) goto L_0x00a7
            r0 = r30
            boolean r0 = r0.isStoreShow
            r3 = r0
            if (r3 != 0) goto L_0x00a7
            r0 = r30
            byte r0 = r0.animCell
            r3 = r0
            r4 = 1
            if (r3 != r4) goto L_0x00a7
            r0 = r30
            int r0 = r0.animLight
            r3 = r0
            r4 = 1
            if (r3 >= r4) goto L_0x00a7
            r0 = r30
            com.machaon.quadratum.input.GameLoopInput r0 = r0.inp
            r3 = r0
            byte r3 = r3.buttonDown
            r0 = r30
            com.machaon.quadratum.input.GameLoopInput r0 = r0.inp
            r4 = r0
            r4.getClass()
            r4 = 100
            if (r3 != r4) goto L_0x00a7
            r0 = r30
            boolean r0 = r0.isUpdatePixField
            r3 = r0
            if (r3 != 0) goto L_0x00a7
            byte r3 = com.machaon.quadratum.States.state
            r4 = 98
            if (r3 == r4) goto L_0x00a7
            r0 = r30
            byte r0 = r0.partBonusAnim
            r3 = r0
            if (r3 != 0) goto L_0x00a7
            r0 = r30
            boolean r0 = r0.isBlind
            r3 = r0
            if (r3 != 0) goto L_0x00a7
            r0 = r30
            boolean r0 = r0.isUnBlind
            r3 = r0
            if (r3 != 0) goto L_0x00a7
            r0 = r30
            byte r0 = r0.cadr
            r3 = r0
            r4 = 2
            if (r3 == r4) goto L_0x000c
            r24 = 1
            r0 = r30
            byte r0 = r0.cadr
            r3 = r0
            int r3 = r3 + 1
            byte r3 = (byte) r3
            r0 = r3
            r1 = r30
            r1.cadr = r0
        L_0x00a7:
            if (r24 != 0) goto L_0x00af
            r3 = 0
            r0 = r3
            r1 = r30
            r1.cadr = r0
        L_0x00af:
            r0 = r30
            byte r0 = r0.pos
            r3 = r0
            r4 = 11
            if (r3 != r4) goto L_0x0169
            byte r3 = com.machaon.quadratum.States.state
            r4 = 98
            if (r3 == r4) goto L_0x0169
            r0 = r30
            int r0 = r0.bombBlow
            r3 = r0
            int r3 = r3 + 1
            r0 = r3
            r1 = r30
            r1.bombBlow = r0
            r4 = 1
            if (r3 <= r4) goto L_0x00d3
            r3 = -1
            r0 = r3
            r1 = r30
            r1.bombBlow = r0
        L_0x00d3:
            r0 = r30
            float r0 = r0.timer_countdownBomb
            r3 = r0
            double r3 = (double) r3
            r5 = -4631501856787818086(0xbfb999999999999a, double:-0.1)
            int r3 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r3 <= 0) goto L_0x0529
            r0 = r30
            float r0 = r0.timer_countdownBomb
            r3 = r0
            double r3 = (double) r3
            r5 = 4599075939470750515(0x3fd3333333333333, double:0.3)
            int r3 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r3 >= 0) goto L_0x0529
            r0 = r30
            float r0 = r0.timer_countdownBomb
            r3 = r0
            double r3 = (double) r3
            r5 = 4596373779694328218(0x3fc999999999999a, double:0.2)
            int r3 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r3 <= 0) goto L_0x011c
            r0 = r30
            float r0 = r0.timer_countdownBomb
            r3 = r0
            double r3 = (double) r3
            r5 = 4599976659396224614(0x3fd6666666666666, double:0.35)
            int r3 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r3 >= 0) goto L_0x011c
            com.machaon.quadratum.parts.ProfileData r3 = com.machaon.quadratum.parts.Profile.profile
            boolean r3 = r3.settingVibrate
            if (r3 == 0) goto L_0x011c
            com.badlogic.gdx.Input r3 = com.badlogic.gdx.Gdx.input
            r4 = 180(0xb4, float:2.52E-43)
            r3.vibrate(r4)
        L_0x011c:
            r0 = r30
            com.badlogic.gdx.math.Matrix4 r0 = r0.viewMatrix
            r3 = r0
            int r4 = com.machaon.quadratum.States.cameraOffsetX
            int r4 = -r4
            r0 = r30
            int r0 = r0.bombBlow
            r5 = r0
            int r5 = r5 * 3
            int r4 = r4 - r5
            float r4 = (float) r4
            int r5 = com.machaon.quadratum.States.cameraOffsetY
            int r5 = -r5
            r0 = r30
            int r0 = r0.bombBlow
            r6 = r0
            int r6 = r6 * 2
            int r5 = r5 - r6
            float r5 = (float) r5
            int r6 = com.machaon.quadratum.States.screenWidth
            int r7 = com.machaon.quadratum.States.cameraOffsetX
            int r7 = r7 * 2
            int r6 = r6 + r7
            r0 = r30
            int r0 = r0.bombBlow
            r7 = r0
            int r7 = r7 * 5
            int r6 = r6 + r7
            float r6 = (float) r6
            int r7 = com.machaon.quadratum.States.screenHeight
            int r8 = com.machaon.quadratum.States.cameraOffsetY
            int r8 = r8 * 2
            int r7 = r7 + r8
            r0 = r30
            int r0 = r0.bombBlow
            r8 = r0
            int r8 = r8 * 6
            int r7 = r7 + r8
            float r7 = (float) r7
            r3.setToOrtho2D(r4, r5, r6, r7)
        L_0x015c:
            r0 = r30
            com.badlogic.gdx.graphics.g2d.SpriteBatch r0 = r0.spriteBatch
            r3 = r0
            r0 = r30
            com.badlogic.gdx.math.Matrix4 r0 = r0.viewMatrix
            r4 = r0
            r3.setProjectionMatrix(r4)
        L_0x0169:
            java.lang.String r3 = com.machaon.quadratum.States.skin
            java.lang.String r4 = "red-blue"
            if (r3 == r4) goto L_0x0175
            java.lang.String r3 = com.machaon.quadratum.States.skin
            java.lang.String r4 = "figure"
            if (r3 != r4) goto L_0x054b
        L_0x0175:
            com.badlogic.gdx.Graphics r3 = r31.getGraphics()
            com.badlogic.gdx.graphics.GL10 r3 = r3.getGL10()
            r4 = 1041865114(0x3e19999a, float:0.15)
            r5 = 1041865114(0x3e19999a, float:0.15)
            r6 = 1041865114(0x3e19999a, float:0.15)
            r7 = 1065353216(0x3f800000, float:1.0)
            r3.glClearColor(r4, r5, r6, r7)
        L_0x018b:
            com.badlogic.gdx.Graphics r3 = r31.getGraphics()
            com.badlogic.gdx.graphics.GL10 r3 = r3.getGL10()
            r4 = 16384(0x4000, float:2.2959E-41)
            r3.glClear(r4)
            r0 = r30
            com.badlogic.gdx.graphics.g2d.SpriteBatch r0 = r0.spriteBatch
            r3 = r0
            r3.begin()
            r0 = r30
            com.badlogic.gdx.graphics.g2d.SpriteBatch r0 = r0.spriteBatch
            r3 = r0
            r3.enableBlending()
            r0 = r30
            boolean r0 = r0.isUpdatePixField
            r3 = r0
            if (r3 != 0) goto L_0x01bc
            byte r3 = com.machaon.quadratum.States.state
            r4 = 98
            if (r3 == r4) goto L_0x01bc
            r0 = r30
            boolean r0 = r0.isResume
            r3 = r0
            if (r3 == 0) goto L_0x08eb
        L_0x01bc:
            r3 = 0
            r0 = r3
            r1 = r30
            r1.isUpdatePixField = r0
            r0 = r30
            com.badlogic.gdx.graphics.g2d.SpriteBatch r0 = r0.spriteBatch
            r3 = r0
            r0 = r30
            com.badlogic.gdx.graphics.Texture r0 = r0.texBlackRect
            r4 = r0
            r0 = r30
            com.machaon.quadratum.parts.Geom r0 = r0.geomBlackRect
            r5 = r0
            int r5 = r5.x
            float r5 = (float) r5
            r0 = r30
            com.machaon.quadratum.parts.Geom r0 = r0.geomBlackRect
            r6 = r0
            int r6 = r6.y
            float r6 = (float) r6
            r7 = 0
            r8 = 0
            r0 = r30
            com.machaon.quadratum.parts.Geom r0 = r0.geomBlackRect
            r9 = r0
            int r9 = r9.width
            r0 = r30
            com.machaon.quadratum.parts.Geom r0 = r0.geomBlackRect
            r10 = r0
            int r10 = r10.height
            r3.draw(r4, r5, r6, r7, r8, r9, r10)
            r0 = r30
            byte r0 = r0.pos
            r3 = r0
            r4 = 1
            if (r3 == r4) goto L_0x0215
            r0 = r30
            byte r0 = r0.pos
            r3 = r0
            r4 = 9
            if (r3 == r4) goto L_0x0215
            r0 = r30
            byte r0 = r0.pos
            r3 = r0
            if (r3 == 0) goto L_0x0215
            r28 = 0
        L_0x0209:
            r0 = r30
            com.machaon.quadratum.parts.Field r0 = r0.field
            r3 = r0
            int r3 = r3.sizeFieldX
            r0 = r28
            r1 = r3
            if (r0 < r1) goto L_0x0563
        L_0x0215:
            r0 = r30
            boolean r0 = r0.isBlind
            r3 = r0
            if (r3 == 0) goto L_0x022c
            r22 = 0
            r28 = 0
        L_0x0220:
            r0 = r30
            com.machaon.quadratum.parts.Field r0 = r0.field
            r3 = r0
            int r3 = r3.sizeFieldX
            r0 = r28
            r1 = r3
            if (r0 < r1) goto L_0x05c1
        L_0x022c:
            r0 = r30
            byte r0 = r0.pos
            r3 = r0
            if (r3 != 0) goto L_0x0241
            r28 = 0
        L_0x0235:
            r0 = r30
            com.machaon.quadratum.parts.Field r0 = r0.field
            r3 = r0
            int r3 = r3.sizeFieldX
            r0 = r28
            r1 = r3
            if (r0 < r1) goto L_0x06ad
        L_0x0241:
            r0 = r30
            byte r0 = r0.pos
            r3 = r0
            r4 = 1
            if (r3 != r4) goto L_0x0271
            r0 = r30
            byte r0 = r0.numberOfPlayers
            r3 = r0
            r4 = 1
            int r3 = r3 - r4
            r0 = r3
            byte[] r0 = new byte[r0]
            r19 = r0
            r25 = 0
            r26 = 0
        L_0x0259:
            r0 = r30
            byte r0 = r0.numberOfPlayers
            r3 = r0
            r0 = r25
            r1 = r3
            if (r0 < r1) goto L_0x0711
            r28 = 0
        L_0x0265:
            r0 = r30
            com.machaon.quadratum.parts.Field r0 = r0.field
            r3 = r0
            int r3 = r3.sizeFieldX
            r0 = r28
            r1 = r3
            if (r0 < r1) goto L_0x072a
        L_0x0271:
            r0 = r30
            byte r0 = r0.pos
            r3 = r0
            r4 = 9
            if (r3 != r4) goto L_0x028a
            r22 = 0
            r28 = 0
        L_0x027e:
            r0 = r30
            com.machaon.quadratum.parts.Field r0 = r0.field
            r3 = r0
            int r3 = r3.sizeFieldX
            r0 = r28
            r1 = r3
            if (r0 < r1) goto L_0x080f
        L_0x028a:
            r3 = 0
            r0 = r30
            r1 = r3
            r0.renderButtons(r1)
            com.badlogic.gdx.Graphics r3 = r31.getGraphics()
            com.badlogic.gdx.graphics.GL10 r3 = r3.getGL10()
            r0 = r30
            int r0 = r0.boost_geomBlackRect_offset_aspectX
            r4 = r0
            r0 = r30
            int r0 = r0.boost_geomBlackRect_offset_aspectY
            r5 = r0
            r0 = r30
            int r0 = r0.boost_geomBlackRect_aspectX
            r6 = r0
            r0 = r30
            int r0 = r0.boost_geomBlackRect_aspectY
            r7 = r0
            r8 = 6408(0x1908, float:8.98E-42)
            r9 = 5121(0x1401, float:7.176E-42)
            r0 = r30
            com.badlogic.gdx.graphics.Pixmap r0 = r0.pixField
            r10 = r0
            java.nio.ByteBuffer r10 = r10.getPixels()
            r3.glReadPixels(r4, r5, r6, r7, r8, r9, r10)
            r0 = r30
            com.badlogic.gdx.graphics.Texture r0 = r0.texField
            r3 = r0
            r0 = r30
            com.badlogic.gdx.graphics.Pixmap r0 = r0.pixField
            r4 = r0
            r5 = 0
            r6 = 0
            r3.draw(r4, r5, r6)
        L_0x02cc:
            r0 = r30
            boolean r0 = r0.isUnBlind
            r3 = r0
            if (r3 == 0) goto L_0x02e3
            r22 = 0
            r28 = 0
        L_0x02d7:
            r0 = r30
            com.machaon.quadratum.parts.Field r0 = r0.field
            r3 = r0
            int r3 = r3.sizeFieldX
            r0 = r28
            r1 = r3
            if (r0 < r1) goto L_0x0aaa
        L_0x02e3:
            r0 = r30
            boolean r0 = r0.isClock
            r3 = r0
            if (r3 == 0) goto L_0x0b4e
            r0 = r30
            com.badlogic.gdx.graphics.g2d.BitmapFont r0 = r0.fontStore
            r3 = r0
            r4 = 1056964608(0x3f000000, float:0.5)
            r5 = 1056964608(0x3f000000, float:0.5)
            r6 = 1056964608(0x3f000000, float:0.5)
            r7 = 1065353216(0x3f800000, float:1.0)
            r3.setColor(r4, r5, r6, r7)
            r0 = r30
            com.badlogic.gdx.graphics.g2d.BitmapFont r0 = r0.fontStore
            r3 = r0
            r0 = r30
            com.badlogic.gdx.graphics.g2d.SpriteBatch r0 = r0.spriteBatch
            r4 = r0
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            java.lang.String r6 = "<"
            r5.<init>(r6)
            java.lang.String r6 = r30.getDateTime()
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r5 = r5.toString()
            r0 = r30
            int r0 = r0.boost_geomClock_font_x_2
            r6 = r0
            float r6 = (float) r6
            r0 = r30
            com.machaon.quadratum.parts.Geom r0 = r0.geomClock
            r7 = r0
            int r7 = r7.y
            float r7 = (float) r7
            r3.draw(r4, r5, r6, r7)
        L_0x0328:
            r0 = r30
            com.badlogic.gdx.graphics.g2d.SpriteBatch r0 = r0.spriteBatch
            r3 = r0
            r0 = r30
            com.machaon.quadratum.parts.ButtonPic r0 = r0.butStore
            r4 = r0
            com.badlogic.gdx.graphics.Texture r4 = r4.getBackTexture()
            r0 = r30
            com.machaon.quadratum.parts.ButtonPic r0 = r0.butStore
            r5 = r0
            int r5 = r5.x
            float r5 = (float) r5
            r0 = r30
            com.machaon.quadratum.parts.ButtonPic r0 = r0.butStore
            r6 = r0
            int r6 = r6.y
            float r6 = (float) r6
            r7 = 0
            r8 = 0
            r0 = r30
            com.machaon.quadratum.parts.ButtonPic r0 = r0.butStore
            r9 = r0
            int r9 = r9.width
            r0 = r30
            com.machaon.quadratum.parts.ButtonPic r0 = r0.butStore
            r10 = r0
            int r10 = r10.height
            r3.draw(r4, r5, r6, r7, r8, r9, r10)
            r30.renderInfo()
            r0 = r30
            boolean r0 = r0.isStoreShow
            r3 = r0
            if (r3 == 0) goto L_0x0366
            r30.renderStore()
        L_0x0366:
            byte r3 = com.machaon.quadratum.States.state
            r4 = 98
            if (r3 == r4) goto L_0x0516
            r0 = r30
            byte r0 = r0.pos
            r3 = r0
            r4 = 4
            if (r3 != r4) goto L_0x0390
            r0 = r30
            int r0 = r0.bombX
            r3 = r0
            if (r3 != 0) goto L_0x0383
            int[] r3 = com.machaon.quadratum.States.skillAI
            byte r4 = com.machaon.quadratum.screens.GameLoop.currentPlayer
            r3 = r3[r4]
            if (r3 == 0) goto L_0x0390
        L_0x0383:
            r0 = r30
            float r0 = r0.timer_useBonus
            r3 = r0
            r4 = 0
            int r3 = (r3 > r4 ? 1 : (r3 == r4 ? 0 : -1))
            if (r3 > 0) goto L_0x0390
            r30.renderBomb()
        L_0x0390:
            r0 = r30
            byte r0 = r0.pos
            r3 = r0
            r4 = 11
            if (r3 != r4) goto L_0x039c
            r30.renderBombBlow()
        L_0x039c:
            r0 = r30
            byte r0 = r0.pos
            r3 = r0
            r4 = 2
            if (r3 != r4) goto L_0x03b5
            r0 = r30
            boolean r0 = r0.isBlind
            r3 = r0
            if (r3 != 0) goto L_0x03b5
            r0 = r30
            byte r0 = r0.animCell
            r3 = r0
            if (r3 <= 0) goto L_0x03b5
            r30.renderMixer()
        L_0x03b5:
            r0 = r30
            byte r0 = r0.pos
            r3 = r0
            r4 = 3
            if (r3 != r4) goto L_0x03c0
            r30.renderTeleport()
        L_0x03c0:
            r0 = r30
            byte r0 = r0.pos
            r3 = r0
            if (r3 != 0) goto L_0x03ca
            r30.renderShowAll()
        L_0x03ca:
            r0 = r30
            boolean r0 = r0.isBonusFinded
            r3 = r0
            if (r3 == 0) goto L_0x03e1
            r3 = 8
            r0 = r3
            boolean[] r0 = new boolean[r0]
            r17 = r0
            r25 = 0
        L_0x03da:
            r3 = 8
            r0 = r25
            r1 = r3
            if (r0 < r1) goto L_0x0b7e
        L_0x03e1:
            r0 = r30
            boolean r0 = r0.isShowBonus
            r3 = r0
            if (r3 == 0) goto L_0x03fa
            r21 = 0
            r20 = 0
        L_0x03ec:
            r3 = 3
            r0 = r20
            r1 = r3
            if (r0 < r1) goto L_0x0c52
            r20 = 0
        L_0x03f4:
            r0 = r20
            r1 = r21
            if (r0 < r1) goto L_0x0c6d
        L_0x03fa:
            r0 = r30
            com.machaon.quadratum.input.GameLoopInput r0 = r0.inp
            r3 = r0
            byte r3 = r3.buttonDown
            r0 = r30
            com.machaon.quadratum.input.GameLoopInput r0 = r0.inp
            r4 = r0
            r4.getClass()
            r4 = 100
            if (r3 == r4) goto L_0x0427
            r0 = r30
            com.machaon.quadratum.input.GameLoopInput r0 = r0.inp
            r3 = r0
            byte r3 = r3.buttonDown
            r0 = r30
            com.machaon.quadratum.input.GameLoopInput r0 = r0.inp
            r4 = r0
            r4.getClass()
            r4 = 50
            if (r3 == r4) goto L_0x0427
            r3 = 1
            r0 = r30
            r1 = r3
            r0.renderButtons(r1)
        L_0x0427:
            r0 = r30
            byte r0 = r0.pos
            r3 = r0
            r4 = 2
            if (r3 == r4) goto L_0x0455
            r0 = r30
            byte r0 = r0.pos
            r3 = r0
            r4 = 4
            if (r3 == r4) goto L_0x0455
            r0 = r30
            byte r0 = r0.pos
            r3 = r0
            r4 = 3
            if (r3 == r4) goto L_0x0455
            r0 = r30
            byte r0 = r0.pos
            r3 = r0
            if (r3 == 0) goto L_0x0455
            r0 = r30
            byte r0 = r0.pos
            r3 = r0
            r4 = 1
            if (r3 == r4) goto L_0x0455
            r0 = r30
            boolean r0 = r0.isBlind
            r3 = r0
            if (r3 == 0) goto L_0x04a0
        L_0x0455:
            r17 = 1
            r0 = r30
            com.badlogic.gdx.graphics.g2d.ParticleEffect r0 = r0.peBonus
            r3 = r0
            com.badlogic.gdx.utils.Array r3 = r3.getEmitters()
            java.util.Iterator r3 = r3.iterator()
        L_0x0464:
            boolean r4 = r3.hasNext()
            if (r4 != 0) goto L_0x0cc8
            if (r17 == 0) goto L_0x0481
            r0 = r30
            byte r0 = r0.partBonusAnim
            r3 = r0
            r4 = 1
            if (r3 != r4) goto L_0x0481
            r0 = r30
            byte r0 = r0.partBonusAnim
            r3 = r0
            r4 = 1
            int r3 = r3 - r4
            byte r3 = (byte) r3
            r0 = r3
            r1 = r30
            r1.partBonusAnim = r0
        L_0x0481:
            if (r17 == 0) goto L_0x04a0
            r0 = r30
            byte r0 = r0.partBonusAnim
            r3 = r0
            r4 = 2
            if (r3 != r4) goto L_0x04a0
            r0 = r30
            com.badlogic.gdx.graphics.g2d.ParticleEffect r0 = r0.peBonus
            r3 = r0
            r3.start()
            r0 = r30
            byte r0 = r0.partBonusAnim
            r3 = r0
            r4 = 1
            int r3 = r3 - r4
            byte r3 = (byte) r3
            r0 = r3
            r1 = r30
            r1.partBonusAnim = r0
        L_0x04a0:
            r0 = r30
            com.machaon.quadratum.parts.Geom[] r0 = r0.geomButton
            r3 = r0
            r4 = 0
            r3 = r3[r4]
            int r15 = r3.height
            r0 = r30
            int r0 = r0.animLight
            r3 = r0
            r4 = 1
            if (r3 <= r4) goto L_0x0516
            r0 = r30
            com.machaon.quadratum.parts.Field r0 = r0.field
            r3 = r0
            byte r4 = com.machaon.quadratum.screens.GameLoop.currentPlayer
            byte r16 = r3.getPlayerColor(r4)
            r0 = r30
            com.badlogic.gdx.graphics.g2d.SpriteBatch r0 = r0.spriteBatch
            r3 = r0
            r0 = r30
            com.badlogic.gdx.graphics.Texture[][] r0 = r0.texButton
            r4 = r0
            r4 = r4[r16]
            r5 = 2
            r4 = r4[r5]
            r0 = r30
            float r0 = r0.boost_geomButtonX_height_div_2
            r5 = r0
            r0 = r30
            int r0 = r0.animLight
            r6 = r0
            float r6 = (float) r6
            float r5 = r5 - r6
            float r6 = (float) r15
            float r5 = r5 - r6
            r0 = r30
            com.machaon.quadratum.parts.Geom[] r0 = r0.geomButton
            r6 = r0
            r6 = r6[r16]
            int r6 = r6.y
            int r7 = r15 >> 1
            int r6 = r6 - r7
            r0 = r30
            int r0 = r0.animLight
            r7 = r0
            int r6 = r6 - r7
            float r6 = (float) r6
            r0 = r30
            int r0 = r0.animLight
            r7 = r0
            int r7 = r7 + r15
            int r7 = r7 << 1
            float r7 = (float) r7
            r0 = r30
            int r0 = r0.animLight
            r8 = r0
            int r8 = r8 + r15
            int r8 = r8 << 1
            float r8 = (float) r8
            r9 = 0
            r10 = 0
            r11 = 128(0x80, float:1.794E-43)
            r12 = 128(0x80, float:1.794E-43)
            r13 = 0
            r14 = 0
            r3.draw(r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14)
            r0 = r30
            int r0 = r0.animLight
            r3 = r0
            r4 = 1
            int r3 = r3 - r4
            r0 = r3
            r1 = r30
            r1.animLight = r0
        L_0x0516:
            byte r3 = com.machaon.quadratum.States.state
            r4 = 98
            if (r3 != r4) goto L_0x051f
            r30.renderPauseWindow()
        L_0x051f:
            r0 = r30
            com.badlogic.gdx.graphics.g2d.SpriteBatch r0 = r0.spriteBatch
            r3 = r0
            r3.end()
            goto L_0x000c
        L_0x0529:
            r0 = r30
            com.badlogic.gdx.math.Matrix4 r0 = r0.viewMatrix
            r3 = r0
            int r4 = com.machaon.quadratum.States.cameraOffsetX
            int r4 = -r4
            float r4 = (float) r4
            int r5 = com.machaon.quadratum.States.cameraOffsetY
            int r5 = -r5
            float r5 = (float) r5
            int r6 = com.machaon.quadratum.States.screenWidth
            int r7 = com.machaon.quadratum.States.cameraOffsetX
            int r7 = r7 * 2
            int r6 = r6 + r7
            float r6 = (float) r6
            int r7 = com.machaon.quadratum.States.screenHeight
            int r8 = com.machaon.quadratum.States.cameraOffsetY
            int r8 = r8 * 2
            int r7 = r7 + r8
            float r7 = (float) r7
            r3.setToOrtho2D(r4, r5, r6, r7)
            goto L_0x015c
        L_0x054b:
            com.badlogic.gdx.Graphics r3 = r31.getGraphics()
            com.badlogic.gdx.graphics.GL10 r3 = r3.getGL10()
            r4 = 1045220557(0x3e4ccccd, float:0.2)
            r5 = 1045220557(0x3e4ccccd, float:0.2)
            r6 = 1045220557(0x3e4ccccd, float:0.2)
            r7 = 1065353216(0x3f800000, float:1.0)
            r3.glClearColor(r4, r5, r6, r7)
            goto L_0x018b
        L_0x0563:
            r29 = 0
        L_0x0565:
            r0 = r30
            com.machaon.quadratum.parts.Field r0 = r0.field
            r3 = r0
            int r3 = r3.sizeFieldY
            r0 = r29
            r1 = r3
            if (r0 < r1) goto L_0x0575
            int r28 = r28 + 1
            goto L_0x0209
        L_0x0575:
            r0 = r30
            com.badlogic.gdx.graphics.g2d.SpriteBatch r0 = r0.spriteBatch
            r3 = r0
            r0 = r30
            com.badlogic.gdx.graphics.Texture r0 = r0.texCells
            r4 = r0
            r0 = r30
            com.machaon.quadratum.parts.Geom r0 = r0.geomField
            r5 = r0
            int r5 = r5.x
            r0 = r30
            int r0 = r0.cellSize
            r6 = r0
            int r6 = r6 * r28
            int r5 = r5 + r6
            float r5 = (float) r5
            r0 = r30
            com.machaon.quadratum.parts.Geom r0 = r0.geomField
            r6 = r0
            int r6 = r6.y
            r0 = r30
            int r0 = r0.cellSize
            r7 = r0
            int r7 = r7 * r29
            int r6 = r6 + r7
            float r6 = (float) r6
            r7 = 0
            r0 = r30
            int r0 = r0.cellSize_1
            r8 = r0
            r0 = r30
            com.machaon.quadratum.parts.Field r0 = r0.field
            r9 = r0
            byte[][] r9 = r9.cell
            r9 = r9[r28]
            byte r9 = r9[r29]
            int r8 = r8 * r9
            r0 = r30
            int r0 = r0.cellSize_1
            r9 = r0
            r0 = r30
            int r0 = r0.cellSize_1
            r10 = r0
            r3.draw(r4, r5, r6, r7, r8, r9, r10)
            int r29 = r29 + 1
            goto L_0x0565
        L_0x05c1:
            r29 = 0
        L_0x05c3:
            r0 = r30
            com.machaon.quadratum.parts.Field r0 = r0.field
            r3 = r0
            int r3 = r3.sizeFieldY
            r0 = r29
            r1 = r3
            if (r0 < r1) goto L_0x05d3
            int r28 = r28 + 1
            goto L_0x0220
        L_0x05d3:
            r22 = 0
            r25 = 0
        L_0x05d7:
            r0 = r30
            byte r0 = r0.numberOfPlayers
            r3 = r0
            r0 = r25
            r1 = r3
            if (r0 < r1) goto L_0x062f
        L_0x05e1:
            if (r22 == 0) goto L_0x064d
            r0 = r30
            com.badlogic.gdx.graphics.g2d.SpriteBatch r0 = r0.spriteBatch
            r3 = r0
            r0 = r30
            com.badlogic.gdx.graphics.Texture r0 = r0.texCells
            r4 = r0
            r0 = r30
            com.machaon.quadratum.parts.Geom r0 = r0.geomField
            r5 = r0
            int r5 = r5.x
            r0 = r30
            int r0 = r0.cellSize
            r6 = r0
            int r6 = r6 * r28
            int r5 = r5 + r6
            float r5 = (float) r5
            r0 = r30
            com.machaon.quadratum.parts.Geom r0 = r0.geomField
            r6 = r0
            int r6 = r6.y
            r0 = r30
            int r0 = r0.cellSize
            r7 = r0
            int r7 = r7 * r29
            int r6 = r6 + r7
            float r6 = (float) r6
            r7 = 0
            r0 = r30
            int r0 = r0.cellSize_1
            r8 = r0
            r0 = r30
            com.machaon.quadratum.parts.Field r0 = r0.field
            r9 = r0
            byte[][] r9 = r9.cell
            r9 = r9[r28]
            byte r9 = r9[r29]
            int r8 = r8 * r9
            r0 = r30
            int r0 = r0.cellSize_1
            r9 = r0
            r0 = r30
            int r0 = r0.cellSize_1
            r10 = r0
            r3.draw(r4, r5, r6, r7, r8, r9, r10)
        L_0x062c:
            int r29 = r29 + 1
            goto L_0x05c3
        L_0x062f:
            r0 = r30
            com.machaon.quadratum.parts.Field r0 = r0.field
            r3 = r0
            byte[][][] r3 = r3.playerField
            r3 = r3[r28]
            r3 = r3[r29]
            byte r3 = r3[r25]
            r0 = r30
            com.machaon.quadratum.parts.Field r0 = r0.field
            r4 = r0
            r4.getClass()
            r4 = 3
            if (r3 != r4) goto L_0x064a
            r22 = 1
            goto L_0x05e1
        L_0x064a:
            int r25 = r25 + 1
            goto L_0x05d7
        L_0x064d:
            r0 = r30
            com.machaon.quadratum.parts.Field r0 = r0.field
            r3 = r0
            byte[][] r3 = r3.cell
            r3 = r3[r28]
            byte r3 = r3[r29]
            r0 = r30
            com.machaon.quadratum.parts.Field r0 = r0.field
            r4 = r0
            r4.getClass()
            r4 = 7
            if (r3 == r4) goto L_0x062c
            r0 = r30
            com.badlogic.gdx.graphics.g2d.SpriteBatch r0 = r0.spriteBatch
            r3 = r0
            r0 = r30
            com.badlogic.gdx.graphics.Texture r0 = r0.texCells
            r4 = r0
            r0 = r30
            com.machaon.quadratum.parts.Geom r0 = r0.geomField
            r5 = r0
            int r5 = r5.x
            r0 = r30
            int r0 = r0.cellSize
            r6 = r0
            int r6 = r6 * r28
            int r5 = r5 + r6
            float r5 = (float) r5
            r0 = r30
            com.machaon.quadratum.parts.Geom r0 = r0.geomField
            r6 = r0
            int r6 = r6.y
            r0 = r30
            int r0 = r0.cellSize
            r7 = r0
            int r7 = r7 * r29
            int r6 = r6 + r7
            float r6 = (float) r6
            r0 = r30
            int r0 = r0.cellSize_1
            r7 = r0
            int r7 = r7 << 2
            r0 = r30
            int r0 = r0.cellSize_1
            r8 = r0
            r0 = r30
            int r0 = r0.animBlind
            r9 = r0
            int r8 = r8 * r9
            r0 = r30
            int r0 = r0.cellSize_1
            r9 = r0
            r0 = r30
            int r0 = r0.cellSize_1
            r10 = r0
            r3.draw(r4, r5, r6, r7, r8, r9, r10)
            goto L_0x062c
        L_0x06ad:
            r29 = 0
        L_0x06af:
            r0 = r30
            com.machaon.quadratum.parts.Field r0 = r0.field
            r3 = r0
            int r3 = r3.sizeFieldY
            r0 = r29
            r1 = r3
            if (r0 < r1) goto L_0x06bf
            int r28 = r28 + 1
            goto L_0x0235
        L_0x06bf:
            r0 = r30
            com.badlogic.gdx.graphics.g2d.SpriteBatch r0 = r0.spriteBatch
            r3 = r0
            r0 = r30
            com.badlogic.gdx.graphics.Texture r0 = r0.texCells
            r4 = r0
            r0 = r30
            com.machaon.quadratum.parts.Geom r0 = r0.geomField
            r5 = r0
            int r5 = r5.x
            r0 = r30
            int r0 = r0.cellSize
            r6 = r0
            int r6 = r6 * r28
            int r5 = r5 + r6
            float r5 = (float) r5
            r0 = r30
            com.machaon.quadratum.parts.Geom r0 = r0.geomField
            r6 = r0
            int r6 = r6.y
            r0 = r30
            int r0 = r0.cellSize
            r7 = r0
            int r7 = r7 * r29
            int r6 = r6 + r7
            float r6 = (float) r6
            r0 = r30
            int r0 = r0.cellSize_1
            r7 = r0
            int r7 = r7 * 3
            r0 = r30
            int r0 = r0.cellSize_1
            r8 = r0
            r0 = r30
            com.machaon.quadratum.parts.Field r0 = r0.field
            r9 = r0
            byte[][] r9 = r9.cell
            r9 = r9[r28]
            byte r9 = r9[r29]
            int r8 = r8 * r9
            r0 = r30
            int r0 = r0.cellSize_1
            r9 = r0
            r0 = r30
            int r0 = r0.cellSize_1
            r10 = r0
            r3.draw(r4, r5, r6, r7, r8, r9, r10)
            int r29 = r29 + 1
            goto L_0x06af
        L_0x0711:
            byte r3 = com.machaon.quadratum.screens.GameLoop.currentPlayer
            r0 = r25
            r1 = r3
            if (r0 == r1) goto L_0x0722
            int r3 = r26 + 1
            r0 = r3
            byte r0 = (byte) r0
            r27 = r0
            r19[r26] = r25
            r26 = r27
        L_0x0722:
            int r3 = r25 + 1
            r0 = r3
            byte r0 = (byte) r0
            r25 = r0
            goto L_0x0259
        L_0x072a:
            r29 = 0
        L_0x072c:
            r0 = r30
            com.machaon.quadratum.parts.Field r0 = r0.field
            r3 = r0
            int r3 = r3.sizeFieldY
            r0 = r29
            r1 = r3
            if (r0 < r1) goto L_0x073c
            int r28 = r28 + 1
            goto L_0x0265
        L_0x073c:
            r23 = 0
            r25 = 0
        L_0x0740:
            r0 = r30
            byte r0 = r0.numberOfPlayers
            r3 = r0
            r4 = 1
            int r3 = r3 - r4
            r0 = r25
            r1 = r3
            if (r0 < r1) goto L_0x079e
        L_0x074c:
            if (r23 == 0) goto L_0x07be
            r0 = r30
            com.badlogic.gdx.graphics.g2d.SpriteBatch r0 = r0.spriteBatch
            r3 = r0
            r0 = r30
            com.badlogic.gdx.graphics.Texture r0 = r0.texCells
            r4 = r0
            r0 = r30
            com.machaon.quadratum.parts.Geom r0 = r0.geomField
            r5 = r0
            int r5 = r5.x
            r0 = r30
            int r0 = r0.cellSize
            r6 = r0
            int r6 = r6 * r28
            int r5 = r5 + r6
            float r5 = (float) r5
            r0 = r30
            com.machaon.quadratum.parts.Geom r0 = r0.geomField
            r6 = r0
            int r6 = r6.y
            r0 = r30
            int r0 = r0.cellSize
            r7 = r0
            int r7 = r7 * r29
            int r6 = r6 + r7
            float r6 = (float) r6
            r0 = r30
            int r0 = r0.cellSize_1
            r7 = r0
            r0 = r30
            int r0 = r0.cellSize_1
            r8 = r0
            r0 = r30
            com.machaon.quadratum.parts.Field r0 = r0.field
            r9 = r0
            byte[][] r9 = r9.cell
            r9 = r9[r28]
            byte r9 = r9[r29]
            int r8 = r8 * r9
            r0 = r30
            int r0 = r0.cellSize_1
            r9 = r0
            r0 = r30
            int r0 = r0.cellSize_1
            r10 = r0
            r3.draw(r4, r5, r6, r7, r8, r9, r10)
        L_0x079b:
            int r29 = r29 + 1
            goto L_0x072c
        L_0x079e:
            r0 = r30
            com.machaon.quadratum.parts.Field r0 = r0.field
            r3 = r0
            byte[][][] r3 = r3.playerField
            r3 = r3[r28]
            r3 = r3[r29]
            byte r4 = r19[r25]
            byte r3 = r3[r4]
            r0 = r30
            com.machaon.quadratum.parts.Field r0 = r0.field
            r4 = r0
            r4.getClass()
            r4 = 3
            if (r3 != r4) goto L_0x07bb
            r23 = 1
            goto L_0x074c
        L_0x07bb:
            int r25 = r25 + 1
            goto L_0x0740
        L_0x07be:
            r0 = r30
            boolean r0 = r0.isBlind
            r3 = r0
            if (r3 != 0) goto L_0x079b
            r0 = r30
            com.badlogic.gdx.graphics.g2d.SpriteBatch r0 = r0.spriteBatch
            r3 = r0
            r0 = r30
            com.badlogic.gdx.graphics.Texture r0 = r0.texCells
            r4 = r0
            r0 = r30
            com.machaon.quadratum.parts.Geom r0 = r0.geomField
            r5 = r0
            int r5 = r5.x
            r0 = r30
            int r0 = r0.cellSize
            r6 = r0
            int r6 = r6 * r28
            int r5 = r5 + r6
            float r5 = (float) r5
            r0 = r30
            com.machaon.quadratum.parts.Geom r0 = r0.geomField
            r6 = r0
            int r6 = r6.y
            r0 = r30
            int r0 = r0.cellSize
            r7 = r0
            int r7 = r7 * r29
            int r6 = r6 + r7
            float r6 = (float) r6
            r7 = 0
            r0 = r30
            int r0 = r0.cellSize_1
            r8 = r0
            r0 = r30
            com.machaon.quadratum.parts.Field r0 = r0.field
            r9 = r0
            byte[][] r9 = r9.cell
            r9 = r9[r28]
            byte r9 = r9[r29]
            int r8 = r8 * r9
            r0 = r30
            int r0 = r0.cellSize_1
            r9 = r0
            r0 = r30
            int r0 = r0.cellSize_1
            r10 = r0
            r3.draw(r4, r5, r6, r7, r8, r9, r10)
            goto L_0x079b
        L_0x080f:
            r29 = 0
        L_0x0811:
            r0 = r30
            com.machaon.quadratum.parts.Field r0 = r0.field
            r3 = r0
            int r3 = r3.sizeFieldY
            r0 = r29
            r1 = r3
            if (r0 < r1) goto L_0x0821
            int r28 = r28 + 1
            goto L_0x027e
        L_0x0821:
            r22 = 0
            r25 = 0
        L_0x0825:
            r0 = r30
            byte r0 = r0.numberOfPlayers
            r3 = r0
            r0 = r25
            r1 = r3
            if (r0 < r1) goto L_0x087d
        L_0x082f:
            if (r22 == 0) goto L_0x089b
            r0 = r30
            com.badlogic.gdx.graphics.g2d.SpriteBatch r0 = r0.spriteBatch
            r3 = r0
            r0 = r30
            com.badlogic.gdx.graphics.Texture r0 = r0.texCells
            r4 = r0
            r0 = r30
            com.machaon.quadratum.parts.Geom r0 = r0.geomField
            r5 = r0
            int r5 = r5.x
            r0 = r30
            int r0 = r0.cellSize
            r6 = r0
            int r6 = r6 * r28
            int r5 = r5 + r6
            float r5 = (float) r5
            r0 = r30
            com.machaon.quadratum.parts.Geom r0 = r0.geomField
            r6 = r0
            int r6 = r6.y
            r0 = r30
            int r0 = r0.cellSize
            r7 = r0
            int r7 = r7 * r29
            int r6 = r6 + r7
            float r6 = (float) r6
            r7 = 0
            r0 = r30
            int r0 = r0.cellSize_1
            r8 = r0
            r0 = r30
            com.machaon.quadratum.parts.Field r0 = r0.field
            r9 = r0
            byte[][] r9 = r9.cell
            r9 = r9[r28]
            byte r9 = r9[r29]
            int r8 = r8 * r9
            r0 = r30
            int r0 = r0.cellSize_1
            r9 = r0
            r0 = r30
            int r0 = r0.cellSize_1
            r10 = r0
            r3.draw(r4, r5, r6, r7, r8, r9, r10)
        L_0x087a:
            int r29 = r29 + 1
            goto L_0x0811
        L_0x087d:
            r0 = r30
            com.machaon.quadratum.parts.Field r0 = r0.field
            r3 = r0
            byte[][][] r3 = r3.playerField
            r3 = r3[r28]
            r3 = r3[r29]
            byte r3 = r3[r25]
            r0 = r30
            com.machaon.quadratum.parts.Field r0 = r0.field
            r4 = r0
            r4.getClass()
            r4 = 3
            if (r3 != r4) goto L_0x0898
            r22 = 1
            goto L_0x082f
        L_0x0898:
            int r25 = r25 + 1
            goto L_0x0825
        L_0x089b:
            r0 = r30
            com.badlogic.gdx.graphics.g2d.SpriteBatch r0 = r0.spriteBatch
            r3 = r0
            r0 = r30
            com.badlogic.gdx.graphics.Texture r0 = r0.texCells
            r4 = r0
            r0 = r30
            com.machaon.quadratum.parts.Geom r0 = r0.geomField
            r5 = r0
            int r5 = r5.x
            r0 = r30
            int r0 = r0.cellSize
            r6 = r0
            int r6 = r6 * r28
            int r5 = r5 + r6
            float r5 = (float) r5
            r0 = r30
            com.machaon.quadratum.parts.Geom r0 = r0.geomField
            r6 = r0
            int r6 = r6.y
            r0 = r30
            int r0 = r0.cellSize
            r7 = r0
            int r7 = r7 * r29
            int r6 = r6 + r7
            float r6 = (float) r6
            r0 = r30
            int r0 = r0.cellSize_1
            r7 = r0
            int r7 = r7 * 3
            r0 = r30
            int r0 = r0.cellSize_1
            r8 = r0
            r0 = r30
            com.machaon.quadratum.parts.Field r0 = r0.field
            r9 = r0
            byte[][] r9 = r9.cell
            r9 = r9[r28]
            byte r9 = r9[r29]
            int r8 = r8 * r9
            r0 = r30
            int r0 = r0.cellSize_1
            r9 = r0
            r0 = r30
            int r0 = r0.cellSize_1
            r10 = r0
            r3.draw(r4, r5, r6, r7, r8, r9, r10)
            goto L_0x087a
        L_0x08eb:
            r3 = 0
            r0 = r30
            r1 = r3
            r0.renderButtons(r1)
            r0 = r30
            com.badlogic.gdx.graphics.g2d.SpriteBatch r0 = r0.spriteBatch
            r3 = r0
            r0 = r30
            com.badlogic.gdx.graphics.Texture r0 = r0.texField
            r4 = r0
            r0 = r30
            com.machaon.quadratum.parts.Geom r0 = r0.geomBlackRect
            r5 = r0
            int r5 = r5.x
            float r5 = (float) r5
            r0 = r30
            com.machaon.quadratum.parts.Geom r0 = r0.geomBlackRect
            r6 = r0
            int r6 = r6.y
            float r6 = (float) r6
            r0 = r30
            com.machaon.quadratum.parts.Geom r0 = r0.geomBlackRect
            r7 = r0
            int r7 = r7.width
            float r7 = (float) r7
            r0 = r30
            com.machaon.quadratum.parts.Geom r0 = r0.geomBlackRect
            r8 = r0
            int r8 = r8.height
            float r8 = (float) r8
            r9 = 0
            r10 = 0
            r0 = r30
            int r0 = r0.boost_geomBlackRect_aspectX
            r11 = r0
            r0 = r30
            int r0 = r0.boost_geomBlackRect_aspectY
            r12 = r0
            r13 = 0
            r14 = 1
            r3.draw(r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14)
            r0 = r30
            boolean r0 = r0.isBlind
            r3 = r0
            if (r3 == 0) goto L_0x094d
            r0 = r30
            int r0 = r0.animBlind
            r3 = r0
            r4 = 10
            if (r3 >= r4) goto L_0x094d
            r22 = 0
            r28 = 0
        L_0x0941:
            r0 = r30
            com.machaon.quadratum.parts.Field r0 = r0.field
            r3 = r0
            int r3 = r3.sizeFieldX
            r0 = r28
            r1 = r3
            if (r0 < r1) goto L_0x0983
        L_0x094d:
            r0 = r30
            byte r0 = r0.animCell
            r3 = r0
            r4 = 1
            if (r3 <= r4) goto L_0x02cc
            r0 = r30
            byte r0 = r0.pos
            r3 = r0
            r4 = 2
            if (r3 == r4) goto L_0x02cc
            r28 = 0
        L_0x095f:
            r0 = r30
            com.machaon.quadratum.parts.Field r0 = r0.field
            r3 = r0
            int r3 = r3.sizeFieldX
            r0 = r28
            r1 = r3
            if (r0 < r1) goto L_0x0a25
            r0 = r30
            byte r0 = r0.animCell
            r3 = r0
            r4 = 1
            int r3 = r3 - r4
            byte r3 = (byte) r3
            r0 = r3
            r1 = r30
            r1.animCell = r0
            r4 = 1
            if (r3 != r4) goto L_0x02cc
            r3 = 1
            r0 = r3
            r1 = r30
            r1.isUpdatePixField = r0
            goto L_0x02cc
        L_0x0983:
            r29 = 0
        L_0x0985:
            r0 = r30
            com.machaon.quadratum.parts.Field r0 = r0.field
            r3 = r0
            int r3 = r3.sizeFieldY
            r0 = r29
            r1 = r3
            if (r0 < r1) goto L_0x0994
            int r28 = r28 + 1
            goto L_0x0941
        L_0x0994:
            r22 = 0
            r25 = 0
        L_0x0998:
            r0 = r30
            byte r0 = r0.numberOfPlayers
            r3 = r0
            r0 = r25
            r1 = r3
            if (r0 < r1) goto L_0x0a06
        L_0x09a2:
            if (r22 != 0) goto L_0x0a03
            r0 = r30
            com.machaon.quadratum.parts.Field r0 = r0.field
            r3 = r0
            byte[][] r3 = r3.cell
            r3 = r3[r28]
            byte r3 = r3[r29]
            r0 = r30
            com.machaon.quadratum.parts.Field r0 = r0.field
            r4 = r0
            r4.getClass()
            r4 = 7
            if (r3 == r4) goto L_0x0a03
            r0 = r30
            com.badlogic.gdx.graphics.g2d.SpriteBatch r0 = r0.spriteBatch
            r3 = r0
            r0 = r30
            com.badlogic.gdx.graphics.Texture r0 = r0.texCells
            r4 = r0
            r0 = r30
            com.machaon.quadratum.parts.Geom r0 = r0.geomField
            r5 = r0
            int r5 = r5.x
            r0 = r30
            int r0 = r0.cellSize
            r6 = r0
            int r6 = r6 * r28
            int r5 = r5 + r6
            float r5 = (float) r5
            r0 = r30
            com.machaon.quadratum.parts.Geom r0 = r0.geomField
            r6 = r0
            int r6 = r6.y
            r0 = r30
            int r0 = r0.cellSize
            r7 = r0
            int r7 = r7 * r29
            int r6 = r6 + r7
            float r6 = (float) r6
            r0 = r30
            int r0 = r0.cellSize_1
            r7 = r0
            int r7 = r7 << 2
            r0 = r30
            int r0 = r0.cellSize_1
            r8 = r0
            r0 = r30
            int r0 = r0.animBlind
            r9 = r0
            int r8 = r8 * r9
            r0 = r30
            int r0 = r0.cellSize_1
            r9 = r0
            r0 = r30
            int r0 = r0.cellSize_1
            r10 = r0
            r3.draw(r4, r5, r6, r7, r8, r9, r10)
        L_0x0a03:
            int r29 = r29 + 1
            goto L_0x0985
        L_0x0a06:
            r0 = r30
            com.machaon.quadratum.parts.Field r0 = r0.field
            r3 = r0
            byte[][][] r3 = r3.playerField
            r3 = r3[r28]
            r3 = r3[r29]
            byte r3 = r3[r25]
            r0 = r30
            com.machaon.quadratum.parts.Field r0 = r0.field
            r4 = r0
            r4.getClass()
            r4 = 3
            if (r3 != r4) goto L_0x0a21
            r22 = 1
            goto L_0x09a2
        L_0x0a21:
            int r25 = r25 + 1
            goto L_0x0998
        L_0x0a25:
            r29 = 0
        L_0x0a27:
            r0 = r30
            com.machaon.quadratum.parts.Field r0 = r0.field
            r3 = r0
            int r3 = r3.sizeFieldY
            r0 = r29
            r1 = r3
            if (r0 < r1) goto L_0x0a37
            int r28 = r28 + 1
            goto L_0x095f
        L_0x0a37:
            r0 = r30
            com.machaon.quadratum.parts.Field r0 = r0.field
            r3 = r0
            byte[][][] r3 = r3.playerField
            r3 = r3[r28]
            r3 = r3[r29]
            byte r4 = com.machaon.quadratum.screens.GameLoop.currentPlayer
            byte r3 = r3[r4]
            r0 = r30
            com.machaon.quadratum.parts.Field r0 = r0.field
            r4 = r0
            r4.getClass()
            r4 = 3
            if (r3 != r4) goto L_0x0aa6
            r0 = r30
            com.badlogic.gdx.graphics.g2d.SpriteBatch r0 = r0.spriteBatch
            r3 = r0
            r0 = r30
            com.badlogic.gdx.graphics.Texture r0 = r0.texCells
            r4 = r0
            r0 = r30
            com.machaon.quadratum.parts.Geom r0 = r0.geomField
            r5 = r0
            int r5 = r5.x
            r0 = r30
            int r0 = r0.cellSize
            r6 = r0
            int r6 = r6 * r28
            int r5 = r5 + r6
            float r5 = (float) r5
            r0 = r30
            com.machaon.quadratum.parts.Geom r0 = r0.geomField
            r6 = r0
            int r6 = r6.y
            r0 = r30
            int r0 = r0.cellSize
            r7 = r0
            int r7 = r7 * r29
            int r6 = r6 + r7
            float r6 = (float) r6
            r0 = r30
            int r0 = r0.cellSize_1
            r7 = r0
            r0 = r30
            byte r0 = r0.animCell
            r8 = r0
            r9 = 1
            int r8 = r8 - r9
            int r7 = r7 * r8
            r0 = r30
            int r0 = r0.cellSize_1
            r8 = r0
            r0 = r30
            com.machaon.quadratum.parts.Field r0 = r0.field
            r9 = r0
            byte[][] r9 = r9.cell
            r9 = r9[r28]
            byte r9 = r9[r29]
            int r8 = r8 * r9
            r0 = r30
            int r0 = r0.cellSize_1
            r9 = r0
            r0 = r30
            int r0 = r0.cellSize_1
            r10 = r0
            r3.draw(r4, r5, r6, r7, r8, r9, r10)
        L_0x0aa6:
            int r29 = r29 + 1
            goto L_0x0a27
        L_0x0aaa:
            r29 = 0
        L_0x0aac:
            r0 = r30
            com.machaon.quadratum.parts.Field r0 = r0.field
            r3 = r0
            int r3 = r3.sizeFieldY
            r0 = r29
            r1 = r3
            if (r0 < r1) goto L_0x0abc
            int r28 = r28 + 1
            goto L_0x02d7
        L_0x0abc:
            r22 = 0
            r25 = 0
        L_0x0ac0:
            r0 = r30
            byte r0 = r0.numberOfPlayers
            r3 = r0
            r0 = r25
            r1 = r3
            if (r0 < r1) goto L_0x0b2f
        L_0x0aca:
            if (r22 != 0) goto L_0x0b2b
            r0 = r30
            com.machaon.quadratum.parts.Field r0 = r0.field
            r3 = r0
            byte[][] r3 = r3.cell
            r3 = r3[r28]
            byte r3 = r3[r29]
            r0 = r30
            com.machaon.quadratum.parts.Field r0 = r0.field
            r4 = r0
            r4.getClass()
            r4 = 7
            if (r3 == r4) goto L_0x0b2b
            r0 = r30
            com.badlogic.gdx.graphics.g2d.SpriteBatch r0 = r0.spriteBatch
            r3 = r0
            r0 = r30
            com.badlogic.gdx.graphics.Texture r0 = r0.texCells
            r4 = r0
            r0 = r30
            com.machaon.quadratum.parts.Geom r0 = r0.geomField
            r5 = r0
            int r5 = r5.x
            r0 = r30
            int r0 = r0.cellSize
            r6 = r0
            int r6 = r6 * r28
            int r5 = r5 + r6
            float r5 = (float) r5
            r0 = r30
            com.machaon.quadratum.parts.Geom r0 = r0.geomField
            r6 = r0
            int r6 = r6.y
            r0 = r30
            int r0 = r0.cellSize
            r7 = r0
            int r7 = r7 * r29
            int r6 = r6 + r7
            float r6 = (float) r6
            r0 = r30
            int r0 = r0.cellSize_1
            r7 = r0
            int r7 = r7 << 2
            r0 = r30
            int r0 = r0.cellSize_1
            r8 = r0
            r0 = r30
            int r0 = r0.animBlind
            r9 = r0
            int r8 = r8 * r9
            r0 = r30
            int r0 = r0.cellSize_1
            r9 = r0
            r0 = r30
            int r0 = r0.cellSize_1
            r10 = r0
            r3.draw(r4, r5, r6, r7, r8, r9, r10)
        L_0x0b2b:
            int r29 = r29 + 1
            goto L_0x0aac
        L_0x0b2f:
            r0 = r30
            com.machaon.quadratum.parts.Field r0 = r0.field
            r3 = r0
            byte[][][] r3 = r3.playerField
            r3 = r3[r28]
            r3 = r3[r29]
            byte r3 = r3[r25]
            r0 = r30
            com.machaon.quadratum.parts.Field r0 = r0.field
            r4 = r0
            r4.getClass()
            r4 = 3
            if (r3 != r4) goto L_0x0b4a
            r22 = 1
            goto L_0x0aca
        L_0x0b4a:
            int r25 = r25 + 1
            goto L_0x0ac0
        L_0x0b4e:
            r0 = r30
            com.badlogic.gdx.graphics.g2d.BitmapFont r0 = r0.fontStore
            r3 = r0
            r4 = 1056964608(0x3f000000, float:0.5)
            r5 = 1056964608(0x3f000000, float:0.5)
            r6 = 1056964608(0x3f000000, float:0.5)
            r7 = 1053609165(0x3ecccccd, float:0.4)
            r3.setColor(r4, r5, r6, r7)
            r0 = r30
            com.badlogic.gdx.graphics.g2d.BitmapFont r0 = r0.fontStore
            r3 = r0
            r0 = r30
            com.badlogic.gdx.graphics.g2d.SpriteBatch r0 = r0.spriteBatch
            r4 = r0
            java.lang.String r5 = "<"
            r0 = r30
            int r0 = r0.boost_geomClock_font_x_3
            r6 = r0
            float r6 = (float) r6
            r0 = r30
            com.machaon.quadratum.parts.Geom r0 = r0.geomClock
            r7 = r0
            int r7 = r7.y
            float r7 = (float) r7
            r3.draw(r4, r5, r6, r7)
            goto L_0x0328
        L_0x0b7e:
            r0 = r30
            com.machaon.quadratum.parts.Field r0 = r0.field
            r3 = r0
            byte[] r3 = r3.findedBonus
            byte r3 = r3[r25]
            r4 = 100
            if (r3 == r4) goto L_0x0be2
            r3 = 1
            r17[r25] = r3
            r0 = r30
            com.badlogic.gdx.graphics.g2d.ParticleEffect[] r0 = r0.peFindBonus
            r3 = r0
            r3 = r3[r25]
            com.badlogic.gdx.utils.Array r3 = r3.getEmitters()
            java.util.Iterator r3 = r3.iterator()
        L_0x0b9d:
            boolean r4 = r3.hasNext()
            if (r4 != 0) goto L_0x0be6
            boolean r3 = r17[r25]
            if (r3 == 0) goto L_0x0bbf
            r0 = r30
            byte[] r0 = r0.tempPeFind
            r3 = r0
            byte r3 = r3[r25]
            r4 = 1
            if (r3 != r4) goto L_0x0bbf
            r3 = 0
            r0 = r3
            r1 = r30
            r1.isBonusFinded = r0
            r0 = r30
            byte[] r0 = r0.tempPeFind
            r3 = r0
            r4 = 0
            r3[r25] = r4
        L_0x0bbf:
            boolean r3 = r17[r25]
            if (r3 == 0) goto L_0x0be2
            r0 = r30
            byte[] r0 = r0.tempPeFind
            r3 = r0
            byte r3 = r3[r25]
            if (r3 != 0) goto L_0x0be2
            r0 = r30
            com.badlogic.gdx.graphics.g2d.ParticleEffect[] r0 = r0.peFindBonus
            r3 = r0
            r3 = r3[r25]
            r3.start()
            r0 = r30
            byte[] r0 = r0.tempPeFind
            r3 = r0
            byte r4 = r3[r25]
            int r4 = r4 + 1
            byte r4 = (byte) r4
            r3[r25] = r4
        L_0x0be2:
            int r25 = r25 + 1
            goto L_0x03da
        L_0x0be6:
            java.lang.Object r18 = r3.next()
            com.badlogic.gdx.graphics.g2d.ParticleEmitter r18 = (com.badlogic.gdx.graphics.g2d.ParticleEmitter) r18
            r0 = r30
            com.machaon.quadratum.parts.Field r0 = r0.field
            r4 = r0
            byte[] r4 = r4.findedBonusX
            byte r4 = r4[r25]
            r0 = r30
            int r0 = r0.cellSize
            r5 = r0
            int r4 = r4 * r5
            r0 = r30
            com.machaon.quadratum.parts.Geom r0 = r0.geomField
            r5 = r0
            int r5 = r5.x
            int r4 = r4 + r5
            r0 = r30
            int r0 = r0.boost_cellSize_div_2
            r5 = r0
            int r4 = r4 + r5
            float r4 = (float) r4
            r0 = r30
            com.machaon.quadratum.parts.Field r0 = r0.field
            r5 = r0
            byte[] r5 = r5.findedBonusY
            byte r5 = r5[r25]
            r0 = r30
            int r0 = r0.cellSize
            r6 = r0
            int r5 = r5 * r6
            r0 = r30
            com.machaon.quadratum.parts.Geom r0 = r0.geomField
            r6 = r0
            int r6 = r6.y
            int r5 = r5 + r6
            r0 = r30
            int r0 = r0.boost_cellSize_div_2
            r6 = r0
            int r5 = r5 + r6
            float r5 = (float) r5
            r0 = r18
            r1 = r4
            r2 = r5
            r0.setPosition(r1, r2)
            r0 = r30
            com.badlogic.gdx.graphics.g2d.SpriteBatch r0 = r0.spriteBatch
            r4 = r0
            com.badlogic.gdx.Graphics r5 = com.badlogic.gdx.Gdx.graphics
            float r5 = r5.getDeltaTime()
            r0 = r18
            r1 = r4
            r2 = r5
            r0.draw(r1, r2)
            boolean r4 = r18.isContinuous()
            if (r4 != 0) goto L_0x0c4d
            boolean r4 = r18.isComplete()
            if (r4 != 0) goto L_0x0b9d
        L_0x0c4d:
            r4 = 0
            r17[r25] = r4
            goto L_0x0b9d
        L_0x0c52:
            r0 = r30
            com.machaon.quadratum.parts.Field r0 = r0.field
            r3 = r0
            byte[] r3 = r3.findedBonus
            byte r3 = r3[r20]
            r4 = 100
            if (r3 == r4) goto L_0x0c65
            int r3 = r21 + 1
            r0 = r3
            byte r0 = (byte) r0
            r21 = r0
        L_0x0c65:
            int r3 = r20 + 1
            r0 = r3
            byte r0 = (byte) r0
            r20 = r0
            goto L_0x03ec
        L_0x0c6d:
            r0 = r30
            com.badlogic.gdx.graphics.g2d.SpriteBatch r0 = r0.spriteBatch
            r3 = r0
            r0 = r30
            com.badlogic.gdx.graphics.Texture r0 = r0.texBonusBig
            r4 = r0
            int r5 = com.machaon.quadratum.States.screenWidth
            int r6 = r21 * 2
            int r5 = r5 / r6
            int r6 = com.machaon.quadratum.States.screenWidth
            int r6 = r6 * r20
            int r6 = r6 / r21
            int r5 = r5 + r6
            r0 = r30
            com.machaon.quadratum.parts.Geom r0 = r0.geomBonusBig
            r6 = r0
            int r6 = r6.width
            int r6 = r6 / 2
            int r5 = r5 - r6
            float r5 = (float) r5
            int r6 = com.machaon.quadratum.States.screenHeight
            int r6 = r6 / 2
            r0 = r30
            com.machaon.quadratum.parts.Geom r0 = r0.geomBonusBig
            r7 = r0
            int r7 = r7.height
            int r7 = r7 / 2
            int r6 = r6 - r7
            float r6 = (float) r6
            r0 = r30
            com.machaon.quadratum.parts.Field r0 = r0.field
            r7 = r0
            byte[] r7 = r7.findedBonus
            byte r7 = r7[r20]
            r0 = r30
            com.machaon.quadratum.parts.Geom r0 = r0.geomBonusBig
            r8 = r0
            int r8 = r8.width
            int r7 = r7 * r8
            r8 = 0
            r0 = r30
            com.machaon.quadratum.parts.Geom r0 = r0.geomBonusBig
            r9 = r0
            int r9 = r9.width
            r0 = r30
            com.machaon.quadratum.parts.Geom r0 = r0.geomBonusBig
            r10 = r0
            int r10 = r10.height
            r3.draw(r4, r5, r6, r7, r8, r9, r10)
            int r3 = r20 + 1
            r0 = r3
            byte r0 = (byte) r0
            r20 = r0
            goto L_0x03f4
        L_0x0cc8:
            java.lang.Object r18 = r3.next()
            com.badlogic.gdx.graphics.g2d.ParticleEmitter r18 = (com.badlogic.gdx.graphics.g2d.ParticleEmitter) r18
            r0 = r30
            com.machaon.quadratum.parts.Geom[] r0 = r0.geomInfoCell
            r4 = r0
            byte r5 = com.machaon.quadratum.screens.GameLoop.currentPlayer
            r4 = r4[r5]
            int r4 = r4.x
            float r4 = (float) r4
            r0 = r30
            float r0 = r0.boost_cellSize_div_6
            r5 = r0
            float r4 = r4 - r5
            r0 = r30
            com.machaon.quadratum.parts.Geom[] r0 = r0.geomInfoCell
            r5 = r0
            byte r6 = com.machaon.quadratum.screens.GameLoop.currentPlayer
            r5 = r5[r6]
            int r5 = r5.y
            float r5 = (float) r5
            r0 = r30
            float r0 = r0.boost_cellSize_div_6
            r6 = r0
            float r5 = r5 - r6
            r0 = r18
            r1 = r4
            r2 = r5
            r0.setPosition(r1, r2)
            r0 = r30
            com.badlogic.gdx.graphics.g2d.SpriteBatch r0 = r0.spriteBatch
            r4 = r0
            com.badlogic.gdx.Graphics r5 = com.badlogic.gdx.Gdx.graphics
            float r5 = r5.getDeltaTime()
            r0 = r18
            r1 = r4
            r2 = r5
            r0.draw(r1, r2)
            boolean r4 = r18.isContinuous()
            if (r4 != 0) goto L_0x0d17
            boolean r4 = r18.isComplete()
            if (r4 != 0) goto L_0x0464
        L_0x0d17:
            r17 = 0
            goto L_0x0464
        */
        throw new UnsupportedOperationException("Method not decompiled: com.machaon.quadratum.screens.GameLoop.render(com.badlogic.gdx.Application):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.g2d.SpriteBatch.draw(com.badlogic.gdx.graphics.Texture, float, float, float, float, int, int, int, int, boolean, boolean):void
     arg types: [com.badlogic.gdx.graphics.Texture, float, float, float, float, int, int, ?, ?, int, int]
     candidates:
      com.badlogic.gdx.graphics.g2d.SpriteBatch.draw(com.badlogic.gdx.graphics.g2d.TextureRegion, float, float, float, float, float, float, float, float, float, boolean):void
      com.badlogic.gdx.graphics.g2d.SpriteBatch.draw(com.badlogic.gdx.graphics.Texture, float, float, float, float, int, int, int, int, boolean, boolean):void */
    private void renderButtons(int kind) {
        int boost_height = this.geomButton[0].height;
        if (kind == 0) {
            for (int i = 0; i < this.field.numberOfColors; i++) {
                this.spriteBatch.draw(this.texButtons, (float) this.geomButton[0].x, (float) this.geomButton[i].y, boost_height * i, activ[i] * boost_height, boost_height, boost_height);
            }
        }
        if (kind == 1 && activ[this.inp.buttonDown] == 0) {
            this.spriteBatch.draw(this.texButton[this.inp.buttonDown][2], this.boost_geomButtonX_height_div_2 - (this.boost_geomButton_height_mul_15 * ((float) this.animButton)), ((float) (this.geomButton[this.inp.buttonDown].y + (boost_height >> 1))) - (this.boost_geomButton_height_mul_15 * ((float) this.animButton)), (float) (this.boost_geomButton_height_mul_3 * this.animButton), (float) (this.boost_geomButton_height_mul_3 * this.animButton), 0, 0, (int) Input.Keys.META_SHIFT_RIGHT_ON, (int) Input.Keys.META_SHIFT_RIGHT_ON, false, false);
            if (this.animButton != 2) {
                this.animButton = this.animButton + 1;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.g2d.SpriteBatch.draw(com.badlogic.gdx.graphics.Texture, float, float, float, float, int, int, int, int, boolean, boolean):void
     arg types: [com.badlogic.gdx.graphics.Texture, float, float, float, float, int, int, int, int, int, int]
     candidates:
      com.badlogic.gdx.graphics.g2d.SpriteBatch.draw(com.badlogic.gdx.graphics.g2d.TextureRegion, float, float, float, float, float, float, float, float, float, boolean):void
      com.badlogic.gdx.graphics.g2d.SpriteBatch.draw(com.badlogic.gdx.graphics.Texture, float, float, float, float, int, int, int, int, boolean, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.g2d.SpriteBatch.draw(com.badlogic.gdx.graphics.Texture, float, float, float, float, int, int, int, int, boolean, boolean):void
     arg types: [com.badlogic.gdx.graphics.Texture, float, float, int, int, int, int, int, int, int, int]
     candidates:
      com.badlogic.gdx.graphics.g2d.SpriteBatch.draw(com.badlogic.gdx.graphics.g2d.TextureRegion, float, float, float, float, float, float, float, float, float, boolean):void
      com.badlogic.gdx.graphics.g2d.SpriteBatch.draw(com.badlogic.gdx.graphics.Texture, float, float, float, float, int, int, int, int, boolean, boolean):void */
    private void renderInfo() {
        int i = 0;
        while (i < this.numberOfPlayers) {
            States.fontSmall.setColor(1.0f, 1.0f, 1.0f, currentPlayer == i ? 1.0f : 0.5f);
            if (i == 0 || i == 3) {
                States.fontSmall.draw(this.spriteBatch, this.strPlayerInfo[i], (float) this.geomInfoPlayer[0].x, (float) this.geomInfoPlayer[i].y);
            } else {
                States.fontSmall.drawMultiLine(this.spriteBatch, this.strPlayerInfo[i], (float) this.geomInfoPlayer[1].x, (float) this.geomInfoPlayer[i].y, (float) this.geomInfoPlayer[i].width, BitmapFont.HAlignment.RIGHT);
            }
            if (currentPlayer != i) {
                this.spriteBatch.draw(this.texCells, (float) this.geomInfoCell[i].x, (float) this.geomInfoCell[i].y, 0, this.cellSize_1 * this.field.getPlayerColor(i), this.cellSize_1, this.cellSize_1);
            } else if (States.screenHeight != 240) {
                this.spriteBatch.draw(this.texCells, ((float) this.geomInfoCell[i].x) - this.boost_cellSize_div_6, ((float) this.geomInfoCell[i].y) - this.boost_cellSize_div_6, this.boost_cellSize_mul1_4, this.boost_cellSize_mul1_4, 0, this.cellSize_1 * this.field.getPlayerColor(i), this.cellSize_1, this.cellSize_1, false, false);
            } else {
                this.spriteBatch.draw(this.texCells, (float) (this.geomInfoCell[i].x - 2), (float) (this.geomInfoCell[i].y - 2), 12.0f, 12.0f, 0, this.cellSize_1 * this.field.getPlayerColor(i), this.cellSize_1, this.cellSize_1, false, false);
            }
            i++;
        }
    }

    private void renderStore() {
        if (this.texStore == null || this.isResumePause) {
            loadTexturesPause();
        }
        if (States.typeOfGame == 1 && States.skillAI[currentPlayer] == 0 && !this.isAuto[currentPlayer]) {
            this.spriteBatch.draw(this.texStore, (float) this.geomStore.x, (float) this.geomStore.y, 0, 0, this.geomStore.width, this.geomStore.height);
            for (ButtonPic b : this.buttonBonusInStore) {
                this.spriteBatch.draw(b.getBackTexture(), (float) b.x, (float) b.y, 0, 0, b.width, b.height);
                this.spriteBatch.draw(b.texFront, (float) b.frontX, (float) b.frontY, this.bonusStoreWidth * b.getState(), 0, this.bonusStoreWidth, b.height);
            }
            this.fontStore.setColor(1.0f, 1.0f, 1.0f, 1.0f);
            for (int i = 0; i < 6; i++) {
                this.fontStore.draw(this.spriteBatch, String.valueOf((int) States.playerBonus[currentPlayer][i]), (float) (this.buttonBonusInStore[i].x + this.geomTextStore.x), (float) (this.buttonBonusInStore[i].y + this.geomTextStore.y));
            }
        }
    }

    private void renderMixer() {
        for (int i = 0; i < this.field.numberOfColors; i++) {
            for (int x = 0; x < this.field.sizeFieldX; x++) {
                for (int y = 0; y < this.field.sizeFieldY; y++) {
                    if (this.field.cellTemp[x][y] == i) {
                        this.spriteBatch.draw(this.texCells, (float) (this.geomField.x + (this.cellSize * x)), (float) (this.geomField.y + (this.cellSize * y)), this.cellSize_1 * (this.animCell - 1), this.cellSize_1 * this.field.cellTemp[x][y], this.cellSize_1, this.cellSize_1);
                        if (this.animCell - 1 == 0) {
                            this.field.cell[x][y] = this.field.cellTemp[x][y];
                        }
                    }
                }
            }
        }
        this.timer_mixer = countdownTimer(this.timer_mixer);
        if (this.timer_mixer < 0.0f) {
            this.timer_mixer = 0.05f;
            this.animCell = (byte) (this.animCell - 1);
        }
        if (this.animCell == 0) {
            this.isUpdatePixField = true;
        }
    }

    private void renderBomb() {
        byte b = this.inp.buttonUp;
        this.inp.getClass();
        if (b == 50 || States.skillAI[currentPlayer] != 0) {
            if (Tournaments.getType() == Tournaments.TYPE_SINGLE && currentPlayer == 0 && States.difficulty == 1) {
                if (!this.isCalcBlowingTerrain) {
                    this.field.CalcBlowingTerrain((this.bombX - this.geomField.x) / this.cellSize, (this.bombY - this.geomField.y) / this.cellSize);
                    this.isCalcBlowingTerrain = true;
                }
                for (int x = 0; x < this.field.sizeFieldX; x++) {
                    for (int y = 0; y < this.field.sizeFieldY; y++) {
                        byte b2 = this.field.cellTemp[x][y];
                        this.field.getClass();
                        if (b2 == 7) {
                            this.spriteBatch.draw(this.texCells, (float) (this.geomField.x + (this.cellSize * x)), (float) (this.geomField.y + (this.cellSize * y)), 0, this.cellSize_1 * this.field.numberOfColors, this.cellSize_1, this.cellSize_1);
                            this.spriteBatch.draw(this.texCells, (float) (this.geomField.x + (this.cellSize * x)), (float) (this.geomField.y + (this.cellSize * y)), this.cellSize_1 * 2, this.cellSize_1 * this.field.cell[x][y], this.cellSize_1, this.cellSize_1);
                        }
                    }
                }
            }
            this.spriteBatch.draw(this.texBonus, (float) (this.bombX - (this.bombAnimWidth >> 1)), (float) (this.bombY - (this.bombAnimWidth >> 1)), (7 - ((int) (this.timer_countdownBomb * 2.0f))) * this.bombAnimWidth, this.bombAnimWidth, this.bombAnimWidth, this.bombAnimWidth);
            return;
        }
        this.isCalcBlowingTerrain = false;
        this.spriteBatch.draw(this.texBonusBig, (float) (this.bombX - (this.geomBonusBig.width >> 1)), (float) (this.bombY - (this.geomBonusBig.width >> 1)), this.geomBonusBig.width * 4, 0, this.geomBonusBig.width, this.geomBonusBig.width);
    }

    private void renderBombBlow() {
        int color;
        if (this.timer_countdownBomb > 0.0f) {
            this.spriteBatch.draw(this.texBonus, (float) (this.bombX - (this.bombAnimWidth >> 1)), (float) (this.bombY - (this.bombAnimWidth >> 1)), (7 - ((int) (this.timer_countdownBomb * 2.0f))) * this.bombAnimWidth, this.bombAnimWidth, this.bombAnimWidth, this.bombAnimWidth);
        }
        if (((double) this.timer_countdownBomb) < 0.35d && Profile.profile.settingSounds && !this.isSoundExpPlay) {
            this.isSoundExpPlay = true;
            this.soundExp.play();
        }
        if (this.timer_countdownBomb < 0.0f && this.animAfterBomb == 0) {
            if (States.numberOfColors == 5) {
                color = 5;
            } else {
                color = 7;
            }
            for (int x = 0; x < this.field.sizeFieldX; x++) {
                for (int y = 0; y < this.field.sizeFieldY; y++) {
                    byte b = this.field.cellTemp[x][y];
                    this.field.getClass();
                    if (b != 20) {
                        this.spriteBatch.draw(this.texCells, (float) (this.geomField.x + (this.cellSize * x)), (float) (this.geomField.y + (this.cellSize * y)), 0, this.cellSize_1 * color, this.cellSize_1, this.cellSize_1);
                    }
                }
            }
        }
        if (((double) this.timer_countdownBomb) < 0.5d) {
            boolean complete = true;
            Iterator<ParticleEmitter> it = this.peBomb.getEmitters().iterator();
            while (it.hasNext()) {
                ParticleEmitter emitter = it.next();
                emitter.setPosition((float) this.bombX, (float) this.bombY);
                emitter.draw(this.spriteBatch, Gdx.graphics.getDeltaTime());
                if (emitter.isContinuous() || !emitter.isComplete()) {
                    complete = false;
                }
            }
            if (complete && this.timer_countdownBomb > 0.0f) {
                this.peBomb.start();
            }
            if (((double) this.timer_countdownBomb) < -0.1d && complete) {
                if (this.animAfterBomb < Gdx.graphics.getFramesPerSecond() / 3 || this.field.temp != 0) {
                    this.animAfterBomb++;
                    for (int x2 = 0; x2 < this.field.sizeFieldX; x2++) {
                        for (int y2 = 0; y2 < this.field.sizeFieldY; y2++) {
                            byte b2 = this.field.cellTemp[x2][y2];
                            this.field.getClass();
                            if (b2 != 20) {
                                Random rand = new Random();
                                if (this.field.cellTemp2[x2][y2] == 0) {
                                    this.field.cellTemp[x2][y2] = this.field.cell[x2][y2];
                                    rand.nextInt(this.field.numberOfColors * 2);
                                    this.field.cell[x2][y2] = (byte) rand.nextInt(this.field.numberOfColors);
                                    this.field.cellTemp2[x2][y2] = 4;
                                } else {
                                    Field field2 = this.field;
                                    byte[] bArr = this.field.cellTemp2[x2];
                                    byte b3 = (byte) (bArr[y2] - 1);
                                    bArr[y2] = b3;
                                    field2.temp = b3;
                                }
                                this.spriteBatch.draw(this.texCells, (float) (this.geomField.x + (this.cellSize * x2)), (float) (this.geomField.y + (this.cellSize * y2)), 0, this.cellSize_1 * this.field.cellTemp[x2][y2], this.cellSize_1, this.cellSize_1);
                                this.spriteBatch.draw(this.texCells, (float) (this.geomField.x + (this.cellSize * x2)), (float) (this.geomField.y + (this.cellSize * y2)), this.cellSize_1 * this.field.cellTemp2[x2][y2], this.cellSize_1 * this.field.cell[x2][y2], this.cellSize_1, this.cellSize_1);
                                for (int i = 0; i < this.numberOfPlayers; i++) {
                                    this.field.setPlayerColor(i, this.field.playerColors[i]);
                                }
                            }
                        }
                    }
                    return;
                }
                for (int x3 = 0; x3 < this.field.sizeFieldX; x3++) {
                    for (int y3 = 0; y3 < this.field.sizeFieldY; y3++) {
                        byte b4 = this.field.cellTemp[x3][y3];
                        this.field.getClass();
                        if (b4 != 20) {
                            this.spriteBatch.draw(this.texCells, (float) (this.geomField.x + (this.cellSize * x3)), (float) (this.geomField.y + (this.cellSize * y3)), 0, this.cellSize_1 * this.field.cell[x3][y3], this.cellSize_1, this.cellSize_1);
                        }
                    }
                }
                this.field.EndBonusBomb();
                this.pos = 6;
                GameLoopInput gameLoopInput = this.inp;
                this.inp.getClass();
                gameLoopInput.buttonUp = States.NONE;
                this.inp.x = 0;
                this.inp.y = 0;
                this.bombX = 0;
                this.bombY = 0;
                this.isUpdatePixField = true;
                this.animCell = 1;
                this.partBonusAnim = 0;
                this.isSoundExpPlay = false;
                nextPlayer();
            }
        }
    }

    private void renderTeleport() {
        if (this.peTeleport == null) {
            InitPeTeleport();
        }
        boolean complete = true;
        Iterator<ParticleEmitter> it = this.peTeleport.getEmitters().iterator();
        while (it.hasNext()) {
            ParticleEmitter emitter = it.next();
            emitter.setPosition((float) ((this.field.teleportX * this.cellSize) + this.geomField.x + this.boost_cellSize_div_2), (float) ((this.field.teleportY * this.cellSize) + this.geomField.y + this.boost_cellSize_div_2));
            emitter.draw(this.spriteBatch, Gdx.graphics.getDeltaTime());
            if (emitter.isContinuous() || !emitter.isComplete()) {
                complete = false;
            }
        }
        if (complete && this.stepParticleOfTeleport == 1 && this.partBonusAnim == 0) {
            this.pos = 6;
            nextPlayer();
        }
        if (complete && this.stepParticleOfTeleport == 0) {
            this.peTeleport.start();
            this.stepParticleOfTeleport++;
        }
    }

    private void renderShowAll() {
        if (this.texBonusesSmall == null) {
            InitTextureBonusSmall();
        }
        for (int x = 0; x < this.field.sizeFieldX; x++) {
            for (int y = 0; y < this.field.sizeFieldY; y++) {
                byte b = this.field.cellBonus[x][y];
                this.field.getClass();
                if (b != 110) {
                    this.spriteBatch.draw(this.texBonusesSmall, (float) (((this.cellSize * x) + this.geomField.x) - this.boost_cellSize_div_2), (float) (((this.cellSize * y) + this.geomField.y) - this.boost_cellSize_div_2), this.field.cellBonus[x][y] * this.bonusSmallWidth, 0, this.bonusSmallWidth, this.bonusSmallWidth);
                }
            }
        }
    }

    private void renderPauseWindow() {
        if (this.texButNA == null || this.isResumePause) {
            loadTexturesPause();
        }
        States.renderBackground(this.spriteBatch);
        for (ButtonPic b : this.butInPause) {
            this.spriteBatch.draw(b.getBackTexture(), (float) b.x, (float) b.y, 0, 0, b.width, b.height);
            this.spriteBatch.draw(b.texFront, (float) (b.x + b.frontX), (float) (b.y + b.frontY), 0, 0, b.width, b.height);
        }
        if (this.isQuitPress) {
            States.fontSmall.setColor(0.0f, 1.0f, 0.0f, 1.0f);
            States.fontSmall.drawMultiLine(this.spriteBatch, States.TEXT_QUIT[States.language], 0.0f, (States.fontSmall.getCapHeight() * 2.0f) + ((float) (this.butInPause[0].y + this.butInPause[0].height)), (float) States.screenWidth, BitmapFont.HAlignment.CENTER);
        }
    }

    private void refreshPlayerInfo() {
        this.isRefreshPlayerInfo = false;
        for (int i = 0; i < this.numberOfPlayers; i++) {
            this.strPlayerInfo[i] = "";
            if (this.field.numberOfCells[i] < 100) {
                this.strPlayerInfo[i] = "0";
            }
            if (this.field.numberOfCells[i] < 10) {
                this.strPlayerInfo[i] = "00";
            }
            if (i == 0 || i == 3) {
                this.strPlayerInfo[i] = String.valueOf(States.playerName[i]) + "    " + this.strPlayerInfo[i] + this.field.numberOfCells[i];
            } else {
                String[] strArr = this.strPlayerInfo;
                strArr[i] = String.valueOf(strArr[i]) + this.field.numberOfCells[i] + "    " + States.playerName[i];
            }
        }
        for (byte i2 = 0; i2 < 7; i2 = (byte) (i2 + 1)) {
            activ[i2] = 0;
        }
        for (int p = 0; p < this.numberOfPlayers; p++) {
            activ[this.field.getPlayerColor(p)] = 1;
        }
    }

    private void isDone() {
        if (this.field.isThisEnd() && this.animLight == 0) {
            for (int i = 0; i < this.field.numberOfPlayers; i++) {
                States.playerScore[i] = this.field.numberOfCells[i];
            }
            this.states.UpdateCount();
            RoundResults.isCount = true;
            States.state = 10;
        }
    }

    public void resume() {
        dispose();
        if (States.state == 98) {
            this.posBeforeResume = this.posBeforePause;
        } else {
            this.posBeforeResume = this.pos;
            this.posBeforePause = this.pos;
        }
        States.state = States.PAUSE;
        this.isResume = true;
        this.isResumeSettings = true;
        this.isResumePause = true;
        this.timer_afterResume = 1.0f;
        InitObjects();
        InitPost();
    }

    public void dispose() {
        this.spriteBatch.dispose();
        if (this.fontStore != null) {
            this.fontStore.dispose();
        }
        if (this.texBlackRect != null) {
            this.texBlackRect.dispose();
        }
        for (ButtonPic b : this.butInPause) {
            if (b != null) {
                b.dispose();
            }
        }
        for (int i = 0; i < this.field.numberOfColors; i++) {
            for (int u = 2; u < 3; u++) {
                if (this.texButton[i][u] != null) {
                    this.texButton[i][u].dispose();
                }
            }
        }
        if (this.texBonus != null) {
            this.texBonus.dispose();
        }
        if (this.texBonusBig != null) {
            this.texBonusBig.dispose();
        }
        for (ButtonPic b2 : this.buttonBonusInStore) {
            b2.dispose();
        }
        for (ButtonPic b3 : this.butAuto) {
            if (b3 != null) {
                b3.dispose();
            }
        }
        if (this.texBonusesSmall != null) {
            this.texBonusesSmall.dispose();
        }
        if (this.texField != null) {
            this.texField.dispose();
        }
        if (this.pixField != null) {
            this.pixField.dispose();
        }
        if (this.texButtons != null) {
            this.texButtons.dispose();
        }
        if (this.texCells != null) {
            this.texCells.dispose();
        }
        if (this.texStore != null) {
            this.texStore.dispose();
        }
        if (this.soundExp != null) {
            this.soundExp.dispose();
        }
        if (this.soundClick != null) {
            this.soundClick.dispose();
        }
        if (this.soundFreeze != null) {
            this.soundFreeze.dispose();
        }
        if (this.soundTeleport != null) {
            this.soundTeleport.dispose();
        }
        if (this.soundMixer != null) {
            this.soundMixer.dispose();
        }
        if (this.soundShowAll != null) {
            this.soundShowAll.dispose();
        }
        if (this.soundBonus != null) {
            this.soundBonus.dispose();
        }
        if (this.soundBombClock != null) {
            this.soundBombClock.dispose();
        }
        if (this.soundBlindness != null) {
            this.soundBlindness.dispose();
        }
        if (this.butStore != null) {
            this.butStore.dispose();
        }
        if (this.peBomb != null) {
            this.peBomb.dispose();
        }
        if (this.peBonus != null) {
            this.peBonus.dispose();
        }
        if (this.peTeleport != null) {
            this.peTeleport.dispose();
        }
        for (ParticleEffect pe : this.peFindBonus) {
            if (pe != null) {
                pe.dispose();
            }
        }
        for (ButtonPic b4 : this.buttonSettings) {
            if (b4 != null) {
                b4.dispose();
            }
        }
        if (this.texBackPic != null) {
            this.texBackPic.dispose();
        }
        if (this.texOff != null) {
            this.texOff.dispose();
        }
        if (this.texButNA != null) {
            this.texButNA.dispose();
        }
        if (this.texButA != null) {
            this.texButA.dispose();
        }
        if (this.texBut2NA != null) {
            this.texBut2NA.dispose();
        }
        if (this.texBut2A != null) {
            this.texBut2A.dispose();
        }
    }

    private void nextPlayer() {
        byte b = (byte) (currentPlayer + 1);
        currentPlayer = b;
        if (b == this.numberOfPlayers) {
            currentPlayer = 0;
            for (byte i = 0; i < this.field.numberOfPlayers; i = (byte) (i + 1)) {
                if (this.field.stepAfterTeleported[i] < 5) {
                    byte[] bArr = this.field.stepAfterTeleported;
                    bArr[i] = (byte) (bArr[i] + 1);
                } else {
                    this.field.isAnybodyTeleported[i] = false;
                }
            }
        }
        GameLoopInput gameLoopInput = this.inp;
        this.inp.getClass();
        gameLoopInput.buttonDown = States.NONE;
        GameLoopInput gameLoopInput2 = this.inp;
        this.inp.getClass();
        gameLoopInput2.buttonUp = States.NONE;
        if (this.isBlind) {
            int i2 = this.stepBlind - 1;
            this.stepBlind = i2;
            if (i2 == 0) {
                this.field.isMixerOnBlind = false;
                this.isBlind = false;
                this.isUnBlind = true;
            }
        }
    }

    private boolean isInputCoordsIn(Geom geom) {
        if (((float) this.inp.x) <= ((float) (geom.x + States.cameraOffsetX)) * States.aspectX || ((float) this.inp.x) >= ((float) (geom.x + geom.width + States.cameraOffsetX)) * States.aspectX || ((float) (States.displayHeight - this.inp.y)) <= ((float) (geom.y + States.cameraOffsetY)) * States.aspectY || ((float) (States.displayHeight - this.inp.y)) >= ((float) (geom.y + geom.height + States.cameraOffsetY)) * States.aspectY) {
            return false;
        }
        return true;
    }

    private float countdownTimer(float timer2) {
        return timer2 - Gdx.graphics.getDeltaTime();
    }

    private void updateSettings() {
        boolean b;
        boolean b2;
        if (this.texBackPic == null || this.isResumeSettings) {
            loadTexturesSettings();
        }
        if (isTimerEnd()) {
            this.isToast = false;
        }
        byte b3 = this.inp.buttonUp;
        this.inp.getClass();
        if (b3 == 50) {
            for (int i = 0; i < 5; i++) {
                if (isInputCoordsIn(this.buttonSettings[i])) {
                    boolean b4 = true;
                    if (this.buttonSettings[i].getState() == 1) {
                        if (Profile.profile.settingSounds) {
                            b4 = false;
                        } else {
                            b4 = true;
                        }
                        Profile.profile.settingSounds = b4;
                    }
                    if (this.buttonSettings[i].getState() == 2) {
                        if (Profile.profile.settingMusic) {
                            b2 = false;
                        } else {
                            b2 = true;
                        }
                        Profile.profile.settingMusic = b4;
                    }
                    if (this.buttonSettings[i].getState() == 3) {
                        if (Profile.profile.settingVibrate) {
                            b = false;
                        } else {
                            b = true;
                        }
                        Profile.profile.settingVibrate = b4;
                        if (Profile.profile.settingVibrate) {
                            Gdx.input.vibrate(80);
                        }
                    }
                    if (this.buttonSettings[i].getState() == 4) {
                        this.isToast = true;
                        setTimer(4.0f);
                    }
                    this.buttonSettings[i].isOff = !b4;
                    if (Profile.profile.settingSounds) {
                        States.click.play();
                    }
                    boolean isSave = false;
                    for (byte p = 0; p < 4; p = (byte) (p + 1)) {
                        if (Profile.isActiveProfile[p] && Profile.profile.numberOfProfile == Profile.pp[p].numberOfProfile) {
                            Profile.pp[p].settingMusic = Profile.profile.settingMusic;
                            Profile.pp[p].settingSkin = Profile.profile.settingSkin;
                            Profile.pp[p].settingSounds = Profile.profile.settingSounds;
                            Profile.pp[p].settingVibrate = Profile.profile.settingVibrate;
                            isSave = true;
                        }
                    }
                    if (!isSave) {
                        Profile.pushProfile(Profile.profile.numberOfProfile, Profile.profile);
                    }
                    this.inp.x = 0;
                }
            }
        }
        byte b5 = this.inp.down;
        this.inp.getClass();
        if (b5 == 10) {
            for (int i2 = 0; i2 < 5; i2++) {
                this.buttonSettings[i2].setBackActive(isInputCoordsIn(this.buttonSettings[i2]));
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.g2d.SpriteBatch.draw(com.badlogic.gdx.graphics.Texture, float, float, float, float, int, int, int, int, boolean, boolean):void
     arg types: [com.badlogic.gdx.graphics.Texture, float, int, float, float, int, int, int, int, int, int]
     candidates:
      com.badlogic.gdx.graphics.g2d.SpriteBatch.draw(com.badlogic.gdx.graphics.g2d.TextureRegion, float, float, float, float, float, float, float, float, float, boolean):void
      com.badlogic.gdx.graphics.g2d.SpriteBatch.draw(com.badlogic.gdx.graphics.Texture, float, float, float, float, int, int, int, int, boolean, boolean):void */
    private void renderSettings(Application app) {
        app.getGraphics().getGL10().glClearColor(0.85f, 0.85f, 0.85f, 1.0f);
        app.getGraphics().getGL10().glClear(16384);
        if (this.texBackPic == null || this.isResumeSettings) {
            loadTexturesSettings();
        }
        this.spriteBatch.begin();
        this.spriteBatch.enableBlending();
        this.spriteBatch.draw(this.texBackPic, (float) ((States.screenWidth / 2) - ((States.screenHeight * 1) / 2)), 0.0f, (float) States.screenHeight, (float) States.screenHeight, this.geomBackPic.x, this.geomBackPic.y, this.geomBackPic.width, this.geomBackPic.height, false, false);
        States.renderBackground(this.spriteBatch);
        for (ButtonPic b : this.buttonSettings) {
            this.spriteBatch.draw(b.getBackTexture(), (float) b.x, (float) b.y, 0, 0, b.width, b.height);
            this.spriteBatch.draw(b.texFront, (float) (b.x + b.frontX), (float) (b.y + b.frontY), 0, 0, b.width, b.height);
            if (b.isOff) {
                this.spriteBatch.draw(this.texOff, (float) b.x, (float) b.y, 0, 0, b.width, b.height);
            }
        }
        if (this.isToast) {
            States.fontSmall.setColor(1.0f, 1.0f, 1.0f, 1.0f);
            States.fontSmall.drawMultiLine(this.spriteBatch, States.TEXT_SKINS[States.language], 0.0f, States.fontSmall.getCapHeight() * 1.7f, (float) States.screenWidth, BitmapFont.HAlignment.CENTER);
        }
        this.spriteBatch.end();
    }

    private void loadTexturesSettings() {
        boolean z;
        boolean z2;
        this.isResumeSettings = false;
        String path = States.PATH_GRAPH + States.resolution + "/";
        this.texBackPic = new Texture(Gdx.files.internal("data/Graph/480/settings.png"));
        this.texOff = new Texture(Gdx.files.internal(String.valueOf(path) + "off.png"));
        this.buttonSettings[0].SetParams(this.texButNA, this.texButA, new Texture(Gdx.files.internal(String.valueOf(path) + "sounds.png")), (byte) 1);
        this.buttonSettings[1].SetParams(this.texButNA, this.texButA, new Texture(Gdx.files.internal(String.valueOf(path) + "music.png")), (byte) 2);
        this.buttonSettings[2].SetParams(this.texButNA, this.texButA, new Texture(Gdx.files.internal(String.valueOf(path) + "vibro.png")), (byte) 3);
        this.buttonSettings[3].SetParams(this.texButNA, this.texButA, new Texture(Gdx.files.internal(String.valueOf(path) + "skins.png")), (byte) 4);
        this.buttonSettings[4].SetParams(new Texture(Gdx.files.internal(String.valueOf(States.path) + "but_na.png")), new Texture(Gdx.files.internal(String.valueOf(States.path) + "but_a.png")), new Texture(Gdx.files.internal(String.valueOf(States.path) + "turbo.png")), (byte) 4);
        this.buttonSettings[0].isOff = !Profile.profile.settingSounds;
        ButtonPic buttonPic = this.buttonSettings[1];
        if (Profile.profile.settingMusic) {
            z = false;
        } else {
            z = true;
        }
        buttonPic.isOff = z;
        ButtonPic buttonPic2 = this.buttonSettings[2];
        if (Profile.profile.settingVibrate) {
            z2 = false;
        } else {
            z2 = true;
        }
        buttonPic2.isOff = z2;
        this.texBackPic.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
    }

    private void setTimer(float timeInSec) {
        this.timer = timeInSec;
    }

    private boolean isTimerEnd() {
        if (this.timer < 0.0f) {
            return true;
        }
        this.timer -= Gdx.graphics.getDeltaTime();
        return false;
    }

    private String getDateTime() {
        int i = this.cdate + 1;
        this.cdate = i;
        if (i > 100) {
            this.cdate = 0;
            this.sdate = new SimpleDateFormat("HH:mm").format(new Date());
        }
        return this.sdate;
    }
}
