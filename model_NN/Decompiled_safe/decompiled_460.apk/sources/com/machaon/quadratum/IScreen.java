package com.machaon.quadratum;

import com.badlogic.gdx.Application;

public interface IScreen {
    void dispose();

    void render(Application application);

    void resume();

    void update();
}
