package com.machaon.quadratum;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.machaon.quadratum.parts.Profile;
import com.machaon.quadratum.parts.Tournaments;
import com.machaon.quadratum.screens.Credits;
import com.machaon.quadratum.screens.GameLoop;
import com.machaon.quadratum.screens.GameType;
import com.machaon.quadratum.screens.Help;
import com.machaon.quadratum.screens.Logo;
import com.machaon.quadratum.screens.MainMenu;
import com.machaon.quadratum.screens.MultiplayerSetup;
import com.machaon.quadratum.screens.MultiplayerTournamentSelect;
import com.machaon.quadratum.screens.Network;
import com.machaon.quadratum.screens.PreExit;
import com.machaon.quadratum.screens.RoundResults;
import com.machaon.quadratum.screens.SelectProfile;
import com.machaon.quadratum.screens.Settings;
import com.machaon.quadratum.screens.SingleTournamentSelect;
import com.machaon.quadratum.screens.Statistic;

public class Quadratum implements ApplicationListener {
    private boolean isInitialized = false;
    private IScreen screen;
    States states;

    public void create() {
        this.states = new States();
        if (!this.isInitialized) {
            this.screen = new Logo();
            this.isInitialized = true;
        }
        States.state = States.NONE;
    }

    public void dispose() {
        Profile.SaveProfiles();
        this.screen.dispose();
        if (States.texClock != null) {
            States.texClock.dispose();
        }
        if (States.texBackground != null) {
            States.texBackground.dispose();
        }
        if (States.fontBig != null) {
            States.fontBig.dispose();
        }
        if (States.fontSmall != null) {
            States.fontSmall.dispose();
        }
        States.click.dispose();
        States.music.dispose();
    }

    public void pause() {
    }

    public void render() {
        Application app = Gdx.app;
        this.screen.update();
        this.screen.render(app);
        if (States.state != 100 && States.state != 98) {
            Gdx.app.getInput().setCatchBackKey(true);
            this.screen.dispose();
            if (States.state == 99) {
                if (Profile.profile.settingMusic) {
                    States.music.stop();
                }
                Gdx.app.getInput().setCatchBackKey(false);
                this.screen = new PreExit();
                States.state = States.NONE;
            }
            if (States.state == 3) {
                if (Profile.profile.settingMusic) {
                    States.music.play();
                }
                this.screen = new MainMenu(this.states);
                States.state = States.NONE;
            }
            if (States.state == 4) {
                this.screen = new GameType();
                States.state = States.NONE;
            }
            if (States.state == 5) {
                this.screen = new Settings((byte) 3);
                States.state = States.NONE;
            }
            if (States.state == 6) {
                this.screen = new SingleTournamentSelect();
                States.state = States.NONE;
            }
            if (States.state == 7) {
                this.screen = new MultiplayerSetup();
                States.state = States.NONE;
            }
            if (States.state == 8) {
                this.screen = new MultiplayerTournamentSelect();
                States.state = States.NONE;
            }
            if (States.state == 15) {
                this.screen = new Network();
                States.state = States.NONE;
            }
            if (States.state == 16) {
                this.screen = new Help();
                States.state = States.NONE;
            }
            if (States.state == 9) {
                States.music.stop();
                this.screen = new GameLoop(Tournaments.getNextLevel(), this.states);
                States.state = States.NONE;
            }
            if (States.state == 10) {
                this.screen = new RoundResults();
                States.state = States.NONE;
            }
            if (States.state == 14) {
                this.screen = new Credits();
                States.state = States.NONE;
            }
            if (States.state == 12) {
                this.screen = new SelectProfile(this.states);
                States.state = States.NONE;
            }
            if (States.state == 13) {
                this.screen = new Statistic(this.states);
                States.state = States.NONE;
            }
        }
    }

    public void resize(int arg0, int arg1) {
    }

    public void resume() {
        this.screen.resume();
    }
}
