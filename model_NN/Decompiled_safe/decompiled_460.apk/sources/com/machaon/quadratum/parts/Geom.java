package com.machaon.quadratum.parts;

public class Geom {
    public int height;
    public int width;
    public int x;
    public int y;

    public Geom(int x2, int y2) {
        this.x = x2;
        this.y = y2;
    }

    public Geom(int x2, int y2, int width2, int height2) {
        this.x = x2;
        this.y = y2;
        this.height = height2;
        this.width = width2;
    }
}
