package com.machaon.quadratum.parts;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.files.FileHandle;
import com.machaon.quadratum.States;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Random;

public class Profile {
    public static final byte NUMBER_PROFILES = 6;
    private static byte currentProfile;
    static InputStream in;
    public static boolean[] isActiveProfile;
    public static boolean isSD;
    static int nByte;
    static OutputStream out;
    public static ProfileData[] pT;
    public static ProfileData[] pp;
    private static Preferences prefs;
    public static ProfileData profile;
    static byte rCode;
    static Random rand = new Random();

    static {
        boolean[] zArr = new boolean[4];
        zArr[0] = true;
        isActiveProfile = zArr;
    }

    public Profile() {
        profile = new ProfileData();
        pp = new ProfileData[4];
        pT = new ProfileData[6];
        for (int i = 0; i < 6; i++) {
            pT[i] = new ProfileData();
        }
        for (int i2 = 0; i2 < 4; i2++) {
            pp[i2] = new ProfileData();
        }
        prefs = Gdx.app.getPreferences(".prof2");
        isSD = Gdx.files.isExternalStorageAvailable();
        if (isSD) {
            if (!Gdx.files.external("Quadratum/prof_lite").exists()) {
                if (!prefs.getBoolean("created")) {
                    CreateProfiles();
                    currentProfile = 0;
                } else {
                    CopyProfilesFromPhoneToSD();
                }
            }
        } else if (!prefs.getBoolean("created")) {
            CreateProfiles();
        }
        LoadProfiles();
        if (currentProfile > 5 || currentProfile < 0) {
            fixBug();
        }
        setCurrentProfile(currentProfile);
        if (profile.language > 1 || profile.language < 0) {
            fixBug();
            setCurrentProfile(currentProfile);
        }
    }

    private void fixBug() {
        if (!prefs.getBoolean("created")) {
            CreateProfiles();
        } else {
            CopyProfilesFromPhoneToSD();
        }
        LoadProfiles();
        currentProfile = 0;
        setCurrentProfile(currentProfile);
    }

    private static void CreateProfiles() {
        for (byte i = 0; i < 6; i = (byte) (i + 1)) {
            ResetProfile(i);
        }
        SaveProfiles();
    }

    public static String getProfileName(byte i) {
        return pT[i].name;
    }

    public static void setProfileName(byte i, String name) {
        pT[i].name = name;
    }

    public static void setCurrentProfile(byte cp) {
        currentProfile = cp;
        popProfile(cp, profile);
    }

    public static void ResetProfile(byte i) {
        pT[i] = new ProfileData();
        pT[i].name = "";
        pT[i].numberOfProfile = i;
        pT[i].settingMusic = true;
        pT[i].settingSounds = true;
        pT[i].settingVibrate = true;
        pT[i].isClock = true;
    }

    public static void popProfile(byte i, ProfileData p) {
        CopyProfile(pT[i], p);
    }

    public static void pushProfile(byte i, ProfileData p) {
        CopyProfile(p, pT[i]);
    }

    public static void LoadProfiles() {
        if (isSD) {
            LoadProfilesSD();
        } else {
            LoadProfilesPhone();
        }
    }

    public static void SaveProfiles() {
        if (isSD) {
            SaveProfilesSD();
        } else {
            SaveProfilesPhone();
        }
    }

    private static void LoadProfilesPhone() {
        currentProfile = (byte) prefs.getInteger("cp");
        for (byte i = 0; i < 6; i = (byte) (i + 1)) {
            pT[i].name = prefs.getString("n" + ((int) i));
            pT[i].numberOfProfile = i;
            pT[i].language = (byte) prefs.getInteger("l" + ((int) i), 0);
            pT[i].nPlayedRounds = prefs.getInteger("pr" + ((int) i), 0);
            pT[i].nWonRounds = prefs.getInteger("wr" + ((int) i), 0);
            pT[i].nPlayedTours = prefs.getInteger("pt" + ((int) i), 0);
            pT[i].nWinTours = prefs.getInteger("wt" + ((int) i), 0);
            pT[i].maxScoresOnRound[0] = prefs.getInteger("ms0" + ((int) i), 0);
            pT[i].maxScoresOnRound[1] = prefs.getInteger("ms1" + ((int) i), 0);
            pT[i].maxScoresOnRound[2] = prefs.getInteger("ms2" + ((int) i), 0);
            pT[i].maxScoresOnTour[0] = prefs.getInteger("m0" + ((int) i), 0);
            pT[i].maxScoresOnTour[1] = prefs.getInteger("m1" + ((int) i), 0);
            pT[i].maxScoresOnTour[2] = prefs.getInteger("m2" + ((int) i), 0);
            pT[i].nBlowedBombs = prefs.getInteger("bb" + ((int) i), 0);
            pT[i].nFindedBonuses = prefs.getInteger("fb" + ((int) i), 0);
            pT[i].nFindedMines = prefs.getInteger("fm" + ((int) i), 0);
            pT[i].nFreezes = prefs.getInteger("f" + ((int) i), 0);
            pT[i].nTeleport = prefs.getInteger("t" + ((int) i), 0);
            pT[i].nMixer = prefs.getInteger("m" + ((int) i), 0);
            pT[i].nEye = prefs.getInteger("e" + ((int) i), 0);
            pT[i].nBlind = prefs.getInteger("b" + ((int) i), 0);
            pT[i].difficulty = (byte) prefs.getInteger("d" + ((int) i), 2);
            pT[i].settingMusic = prefs.getBoolean("sm" + ((int) i), true);
            pT[i].settingSounds = prefs.getBoolean("ss" + ((int) i), true);
            pT[i].settingVibrate = prefs.getBoolean("sv" + ((int) i), true);
            pT[i].isClock = prefs.getBoolean("c" + ((int) i), true);
            pT[i].settingSkin = (byte) prefs.getInteger("sk" + ((int) i), 0);
            pT[i].nGoldCups = prefs.getInteger("gk" + ((int) i), 0);
            pT[i].nGamesAbandoned = prefs.getInteger("ga" + ((int) i), 0);
            pT[i].multi_nBlowedBombs = prefs.getInteger("mbb" + ((int) i), 0);
            pT[i].multi_nFindedBonuses = prefs.getInteger("mfb" + ((int) i), 0);
            pT[i].multi_nFindedMines = prefs.getInteger("mfm" + ((int) i), 0);
            pT[i].multi_nFreezes = prefs.getInteger("mf" + ((int) i), 0);
            pT[i].multi_nMixer = prefs.getInteger("mm" + ((int) i), 0);
            pT[i].multi_nEye = prefs.getInteger("me" + ((int) i), 0);
            pT[i].multi_nBlind = prefs.getInteger("mb" + ((int) i), 0);
            pT[i].multi_nPlayedRounds = prefs.getInteger("mpr" + ((int) i), 0);
            pT[i].multi_nPlayedTours = prefs.getInteger("mpt" + ((int) i), 0);
            pT[i].multi_nTeleport = prefs.getInteger("mt" + ((int) i), 0);
            pT[i].multi_nToursWithoutBonuses = prefs.getInteger("mwb" + ((int) i), 0);
            pT[i].multi_nWinTours = prefs.getInteger("mwt" + ((int) i), 0);
            pT[i].multi_nWonMans = prefs.getInteger("mwm" + ((int) i), 0);
            pT[i].multi_nWonRounds = prefs.getInteger("mwr" + ((int) i), 0);
            pT[i].multi_nGamesAbandoned = prefs.getInteger("mga" + ((int) i), 0);
            pT[i].maxBonusesAtEnd = prefs.getInteger("mbe" + ((int) i), 0);
            for (int t = 0; t < 4; t++) {
                pT[i].resultTournament[t] = prefs.getInteger("rt" + t + ((int) i), 0);
            }
        }
    }

    public static void Reserve() {
        SaveProfilesPhone();
    }

    private static void SaveProfilesPhone() {
        prefs.clear();
        prefs.putBoolean("created", true);
        prefs.putInteger("cp", currentProfile);
        for (byte i = 0; i < 6; i = (byte) (i + 1)) {
            if (pT[i].name == States.TEXT_NEWPLAYER[0] || pT[i].name == States.TEXT_NEWPLAYER[1]) {
                prefs.putString("n" + ((int) i), "");
                return;
            }
            prefs.putString("n" + ((int) i), pT[i].name);
            prefs.putInteger("l" + ((int) i), pT[i].language);
            prefs.putInteger("pr" + ((int) i), pT[i].nPlayedRounds);
            prefs.putInteger("wr" + ((int) i), pT[i].nWonRounds);
            prefs.putInteger("pt" + ((int) i), pT[i].nPlayedTours);
            prefs.putInteger("wt" + ((int) i), pT[i].nWinTours);
            prefs.putInteger("ms0" + ((int) i), pT[i].maxScoresOnRound[0]);
            prefs.putInteger("ms1" + ((int) i), pT[i].maxScoresOnRound[1]);
            prefs.putInteger("ms2" + ((int) i), pT[i].maxScoresOnRound[2]);
            prefs.putInteger("m0" + ((int) i), pT[i].maxScoresOnTour[0]);
            prefs.putInteger("m1" + ((int) i), pT[i].maxScoresOnTour[1]);
            prefs.putInteger("m2" + ((int) i), pT[i].maxScoresOnTour[2]);
            prefs.putInteger("bb" + ((int) i), pT[i].nBlowedBombs);
            prefs.putInteger("fb" + ((int) i), pT[i].nFindedBonuses);
            prefs.putInteger("fm" + ((int) i), pT[i].nFindedMines);
            prefs.putInteger("f" + ((int) i), pT[i].nFreezes);
            prefs.putInteger("t" + ((int) i), pT[i].nTeleport);
            prefs.putInteger("m" + ((int) i), pT[i].nMixer);
            prefs.putInteger("e" + ((int) i), pT[i].nEye);
            prefs.putInteger("b" + ((int) i), pT[i].nBlind);
            prefs.putInteger("d" + ((int) i), pT[i].difficulty);
            prefs.putBoolean("sm" + ((int) i), pT[i].settingMusic);
            prefs.putBoolean("ss" + ((int) i), pT[i].settingSounds);
            prefs.putBoolean("sv" + ((int) i), pT[i].settingVibrate);
            prefs.putBoolean("c" + ((int) i), pT[i].isClock);
            prefs.putInteger("sk" + ((int) i), pT[i].settingSkin);
            prefs.putInteger("gk" + ((int) i), pT[i].nGoldCups);
            prefs.putInteger("ga" + ((int) i), pT[i].nGamesAbandoned);
            prefs.putInteger("mbb" + ((int) i), pT[i].multi_nBlowedBombs);
            prefs.putInteger("mfb" + ((int) i), pT[i].multi_nFindedBonuses);
            prefs.putInteger("mfm" + ((int) i), pT[i].multi_nFindedMines);
            prefs.putInteger("mf" + ((int) i), pT[i].multi_nFreezes);
            prefs.putInteger("mm" + ((int) i), pT[i].multi_nMixer);
            prefs.putInteger("me" + ((int) i), pT[i].multi_nEye);
            prefs.putInteger("mb" + ((int) i), pT[i].multi_nBlind);
            prefs.putInteger("mpr" + ((int) i), pT[i].multi_nPlayedRounds);
            prefs.putInteger("mpt" + ((int) i), pT[i].multi_nPlayedTours);
            prefs.putInteger("mt" + ((int) i), pT[i].multi_nTeleport);
            prefs.putInteger("mwb" + ((int) i), pT[i].multi_nToursWithoutBonuses);
            prefs.putInteger("mwt" + ((int) i), pT[i].multi_nWinTours);
            prefs.putInteger("mwm" + ((int) i), pT[i].multi_nWonMans);
            prefs.putInteger("mwr" + ((int) i), pT[i].multi_nWonRounds);
            prefs.putInteger("mga" + ((int) i), pT[i].multi_nGamesAbandoned);
            prefs.putInteger("mbe" + ((int) i), pT[i].maxBonusesAtEnd);
            for (int t = 0; t < 4; t++) {
                prefs.putInteger("rt" + t + ((int) i), pT[i].resultTournament[t]);
            }
        }
    }

    public static void CopyProfile(ProfileData pSource, ProfileData pTarget) {
        pTarget.numberOfProfile = pSource.numberOfProfile;
        pTarget.name = pSource.name;
        pTarget.language = pSource.language;
        pTarget.nPlayedRounds = pSource.nPlayedRounds;
        pTarget.nWonRounds = pSource.nWonRounds;
        pTarget.nWinTours = pSource.nWinTours;
        pTarget.nPlayedTours = pSource.nPlayedTours;
        pTarget.maxScoresOnRound[0] = pSource.maxScoresOnRound[0];
        pTarget.maxScoresOnRound[1] = pSource.maxScoresOnRound[1];
        pTarget.maxScoresOnRound[2] = pSource.maxScoresOnRound[2];
        pTarget.maxScoresOnTour[0] = pSource.maxScoresOnTour[0];
        pTarget.maxScoresOnTour[1] = pSource.maxScoresOnTour[1];
        pTarget.maxScoresOnTour[2] = pSource.maxScoresOnTour[2];
        pTarget.nBlowedBombs = pSource.nBlowedBombs;
        pTarget.nFindedBonuses = pSource.nFindedBonuses;
        pTarget.nFreezes = pSource.nFreezes;
        pTarget.nTeleport = pSource.nTeleport;
        pTarget.nMixer = pSource.nMixer;
        pTarget.nEye = pSource.nEye;
        pTarget.nBlind = pSource.nBlind;
        pTarget.difficulty = pSource.difficulty;
        pTarget.settingMusic = pSource.settingMusic;
        pTarget.settingSounds = pSource.settingSounds;
        pTarget.settingVibrate = pSource.settingVibrate;
        pTarget.isClock = pSource.isClock;
        pTarget.settingSkin = pSource.settingSkin;
        pTarget.nGoldCups = pSource.nGoldCups;
        pTarget.nGamesAbandoned = pSource.nGamesAbandoned;
        pTarget.multi_nBlowedBombs = pSource.multi_nBlowedBombs;
        pTarget.multi_nFindedBonuses = pSource.multi_nFindedBonuses;
        pTarget.multi_nFreezes = pSource.multi_nFreezes;
        pTarget.multi_nMixer = pSource.multi_nMixer;
        pTarget.multi_nEye = pSource.multi_nEye;
        pTarget.multi_nBlind = pSource.multi_nBlind;
        pTarget.multi_nPlayedRounds = pSource.multi_nPlayedRounds;
        pTarget.multi_nPlayedTours = pSource.multi_nPlayedTours;
        pTarget.multi_nTeleport = pSource.multi_nTeleport;
        pTarget.multi_nToursWithoutBonuses = pSource.multi_nToursWithoutBonuses;
        pTarget.multi_nWinTours = pSource.multi_nWinTours;
        pTarget.multi_nWonMans = pSource.multi_nWonMans;
        pTarget.multi_nWonRounds = pSource.multi_nWonRounds;
        pTarget.multi_nGamesAbandoned = pSource.multi_nGamesAbandoned;
        pTarget.maxBonusesAtEnd = pSource.maxBonusesAtEnd;
        pTarget.resultTournament[0] = pSource.resultTournament[0];
        pTarget.resultTournament[1] = pSource.resultTournament[1];
        pTarget.resultTournament[2] = pSource.resultTournament[2];
        pTarget.resultTournament[3] = pSource.resultTournament[3];
    }

    public static void CopyProfilesFromPhoneToSD() {
        LoadProfilesPhone();
        SaveProfilesSD();
    }

    private static void SaveProfilesSD() {
        FileHandle fdir = Gdx.files.external("Quadratum");
        if (!fdir.exists()) {
            fdir.mkdirs();
        }
        FileHandle fout = Gdx.files.external("Quadratum/prof_lite");
        out = null;
        try {
            out = fout.write(false);
            rCode = 0;
            out.write(rCode);
            nByte = 1;
            SaveByte(currentProfile);
            for (ProfileData p : pT) {
                SaveString(p.name);
            }
            for (ProfileData p2 : pT) {
                SaveByte(p2.language);
            }
            for (ProfileData p3 : pT) {
                SaveInteger(p3.nPlayedRounds);
            }
            for (ProfileData p4 : pT) {
                SaveInteger(p4.nWonRounds);
            }
            for (ProfileData p5 : pT) {
                SaveInteger(p5.nPlayedTours);
            }
            for (ProfileData p6 : pT) {
                SaveInteger(p6.nWinTours);
            }
            for (ProfileData p7 : pT) {
                SaveInteger(p7.maxScoresOnRound[0]);
            }
            for (ProfileData p8 : pT) {
                SaveInteger(p8.maxScoresOnRound[1]);
            }
            for (ProfileData p9 : pT) {
                SaveInteger(p9.maxScoresOnRound[2]);
            }
            for (ProfileData p10 : pT) {
                SaveInteger(p10.maxScoresOnTour[0]);
            }
            for (ProfileData p11 : pT) {
                SaveInteger(p11.maxScoresOnTour[1]);
            }
            for (ProfileData p12 : pT) {
                SaveInteger(p12.maxScoresOnTour[2]);
            }
            for (ProfileData p13 : pT) {
                SaveInteger(p13.nBlowedBombs);
            }
            for (ProfileData p14 : pT) {
                SaveInteger(p14.nFindedBonuses);
            }
            for (ProfileData p15 : pT) {
                SaveInteger(p15.nFreezes);
            }
            for (ProfileData p16 : pT) {
                SaveInteger(p16.nTeleport);
            }
            for (ProfileData p17 : pT) {
                SaveInteger(p17.nMixer);
            }
            for (ProfileData p18 : pT) {
                SaveInteger(p18.nEye);
            }
            for (ProfileData p19 : pT) {
                SaveByte(p19.difficulty);
            }
            for (ProfileData p20 : pT) {
                SaveBoolean(p20.settingMusic);
            }
            for (ProfileData p21 : pT) {
                SaveBoolean(p21.settingSounds);
            }
            for (ProfileData p22 : pT) {
                SaveBoolean(p22.settingVibrate);
            }
            for (ProfileData p23 : pT) {
                SaveBoolean(p23.isClock);
            }
            for (ProfileData p24 : pT) {
                SaveByte(p24.settingSkin);
            }
            for (ProfileData p25 : pT) {
                SaveInteger(p25.nGoldCups);
            }
            for (ProfileData p26 : pT) {
                SaveInteger(p26.nGamesAbandoned);
            }
            for (ProfileData p27 : pT) {
                SaveInteger(p27.multi_nBlowedBombs);
            }
            for (ProfileData p28 : pT) {
                SaveInteger(p28.multi_nFindedBonuses);
            }
            for (ProfileData p29 : pT) {
                SaveInteger(p29.multi_nFreezes);
            }
            for (ProfileData p30 : pT) {
                SaveInteger(p30.multi_nMixer);
            }
            for (ProfileData p31 : pT) {
                SaveInteger(p31.multi_nEye);
            }
            for (ProfileData p32 : pT) {
                SaveInteger(p32.multi_nPlayedRounds);
            }
            for (ProfileData p33 : pT) {
                SaveInteger(p33.multi_nPlayedTours);
            }
            for (ProfileData p34 : pT) {
                SaveInteger(p34.multi_nTeleport);
            }
            for (ProfileData p35 : pT) {
                SaveInteger(p35.multi_nToursWithoutBonuses);
            }
            for (ProfileData p36 : pT) {
                SaveInteger(p36.multi_nWinTours);
            }
            for (ProfileData p37 : pT) {
                SaveInteger(p37.multi_nWonMans);
            }
            for (ProfileData p38 : pT) {
                SaveInteger(p38.multi_nWonRounds);
            }
            for (ProfileData p39 : pT) {
                SaveInteger(p39.multi_nGamesAbandoned);
            }
            for (ProfileData p40 : pT) {
                SaveInteger(p40.maxBonusesAtEnd);
            }
            for (ProfileData p41 : pT) {
                SaveInteger(p41.resultTournament[0]);
            }
            for (ProfileData p42 : pT) {
                SaveInteger(p42.resultTournament[1]);
            }
            for (ProfileData p43 : pT) {
                SaveInteger(p43.resultTournament[2]);
            }
            for (ProfileData p44 : pT) {
                SaveInteger(p44.resultTournament[3]);
            }
            for (ProfileData p45 : pT) {
                SaveInteger(p45.nBlind);
            }
            for (ProfileData p46 : pT) {
                SaveInteger(p46.multi_nBlind);
            }
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e2) {
            e2.printStackTrace();
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e3) {
                    e3.printStackTrace();
                }
            }
        } catch (Throwable th) {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e4) {
                    e4.printStackTrace();
                }
            }
            throw th;
        }
    }

    /* JADX INFO: finally extract failed */
    private static boolean LoadProfilesSD() {
        FileHandle fin = Gdx.files.external("Quadratum/prof_lite");
        in = null;
        try {
            in = fin.read();
            rCode = (byte) in.read();
            nByte = 1;
            currentProfile = LoadByte();
            for (byte i = 0; i < 6; i = (byte) (i + 1)) {
                pT[i].numberOfProfile = i;
            }
            for (ProfileData p : pT) {
                p.name = LoadString();
            }
            for (ProfileData p2 : pT) {
                p2.language = LoadByte();
            }
            for (ProfileData p3 : pT) {
                p3.nPlayedRounds = LoadInteger();
            }
            for (ProfileData p4 : pT) {
                p4.nWonRounds = LoadInteger();
            }
            for (ProfileData p5 : pT) {
                p5.nPlayedTours = LoadInteger();
            }
            for (ProfileData p6 : pT) {
                p6.nWinTours = LoadInteger();
            }
            for (ProfileData p7 : pT) {
                p7.maxScoresOnRound[0] = LoadInteger();
            }
            for (ProfileData p8 : pT) {
                p8.maxScoresOnRound[1] = LoadInteger();
            }
            for (ProfileData p9 : pT) {
                p9.maxScoresOnRound[2] = LoadInteger();
            }
            for (ProfileData p10 : pT) {
                p10.maxScoresOnTour[0] = LoadInteger();
            }
            for (ProfileData p11 : pT) {
                p11.maxScoresOnTour[1] = LoadInteger();
            }
            for (ProfileData p12 : pT) {
                p12.maxScoresOnTour[2] = LoadInteger();
            }
            for (ProfileData p13 : pT) {
                p13.nBlowedBombs = LoadInteger();
            }
            for (ProfileData p14 : pT) {
                p14.nFindedBonuses = LoadInteger();
            }
            for (ProfileData p15 : pT) {
                p15.nFreezes = LoadInteger();
            }
            for (ProfileData p16 : pT) {
                p16.nTeleport = LoadInteger();
            }
            for (ProfileData p17 : pT) {
                p17.nMixer = LoadInteger();
            }
            for (ProfileData p18 : pT) {
                p18.nEye = LoadInteger();
            }
            for (ProfileData p19 : pT) {
                p19.difficulty = LoadByte();
            }
            for (ProfileData p20 : pT) {
                p20.settingMusic = LoadBoolean();
            }
            for (ProfileData p21 : pT) {
                p21.settingSounds = LoadBoolean();
            }
            for (ProfileData p22 : pT) {
                p22.settingVibrate = LoadBoolean();
            }
            for (ProfileData p23 : pT) {
                p23.isClock = LoadBoolean();
            }
            for (ProfileData p24 : pT) {
                p24.settingSkin = LoadByte();
            }
            for (ProfileData p25 : pT) {
                p25.nGoldCups = LoadInteger();
            }
            for (ProfileData p26 : pT) {
                p26.nGamesAbandoned = LoadInteger();
            }
            for (ProfileData p27 : pT) {
                p27.multi_nBlowedBombs = LoadInteger();
            }
            for (ProfileData p28 : pT) {
                p28.multi_nFindedBonuses = LoadInteger();
            }
            for (ProfileData p29 : pT) {
                p29.multi_nFreezes = LoadInteger();
            }
            for (ProfileData p30 : pT) {
                p30.multi_nMixer = LoadInteger();
            }
            for (ProfileData p31 : pT) {
                p31.multi_nEye = LoadInteger();
            }
            for (ProfileData p32 : pT) {
                p32.multi_nPlayedRounds = LoadInteger();
            }
            for (ProfileData p33 : pT) {
                p33.multi_nPlayedTours = LoadInteger();
            }
            for (ProfileData p34 : pT) {
                p34.multi_nTeleport = LoadInteger();
            }
            for (ProfileData p35 : pT) {
                p35.multi_nToursWithoutBonuses = LoadInteger();
            }
            for (ProfileData p36 : pT) {
                p36.multi_nWinTours = LoadInteger();
            }
            for (ProfileData p37 : pT) {
                p37.multi_nWonMans = LoadInteger();
            }
            for (ProfileData p38 : pT) {
                p38.multi_nWonRounds = LoadInteger();
            }
            for (ProfileData p39 : pT) {
                p39.multi_nGamesAbandoned = LoadInteger();
            }
            for (ProfileData p40 : pT) {
                p40.maxBonusesAtEnd = LoadInteger();
            }
            for (ProfileData p41 : pT) {
                p41.resultTournament[0] = LoadInteger();
            }
            for (ProfileData p42 : pT) {
                p42.resultTournament[1] = LoadInteger();
            }
            for (ProfileData p43 : pT) {
                p43.resultTournament[2] = LoadInteger();
            }
            for (ProfileData p44 : pT) {
                p44.resultTournament[3] = LoadInteger();
            }
            for (ProfileData p45 : pT) {
                p45.nBlind = LoadInteger();
            }
            for (ProfileData p46 : pT) {
                p46.multi_nBlind = LoadInteger();
            }
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e2) {
            e2.printStackTrace();
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e3) {
                    e3.printStackTrace();
                }
            }
        } catch (Throwable th) {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e4) {
                    e4.printStackTrace();
                }
            }
            throw th;
        }
        return true;
    }

    private static void SaveByte(byte b) throws IOException {
        out.write(b);
    }

    private static void SaveBoolean(boolean b) throws IOException {
        if (b) {
            SaveByte((byte) 30);
        } else {
            SaveByte(Keyboard.STATE_NUMBER);
        }
    }

    private static void SaveBytes(byte[] bs) throws IOException {
        SaveByte((byte) bs.length);
        for (byte SaveByte : bs) {
            SaveByte(SaveByte);
        }
    }

    private static void SaveString(String str) throws IOException {
        SaveBytes(str.getBytes("cp1251"));
    }

    private static void SaveInteger(int integer) throws IOException {
        SaveByte((byte) (integer & 255));
        SaveByte((byte) ((integer >> 8) & 255));
    }

    private static void SaveInteger3(int integer) throws IOException {
        SaveByte((byte) (integer & 255));
        SaveByte((byte) ((integer >> 8) & 255));
        SaveByte((byte) ((integer >> 16) & 255));
    }

    private static byte LoadByte() throws IOException {
        nByte += 37;
        int c = in.read();
        if (c == -1) {
            return -1;
        }
        if (rCode == 0) {
            return (byte) c;
        }
        return (byte) ((255 - ((byte) c)) + rCode + nByte);
    }

    private static String LoadString() throws IOException {
        int length = LoadByte();
        if (length <= 0) {
            return "";
        }
        byte[] bs = new byte[length];
        for (int i = 0; i < length; i++) {
            bs[i] = LoadByte();
        }
        return new String(bs, "cp1251");
    }

    private static int LoadInteger3() throws IOException {
        int s1 = LoadByte();
        if (s1 < 0) {
            s1 += 256;
        }
        int s2 = LoadByte();
        if (s2 < 0) {
            s2 += 256;
        }
        int s3 = LoadByte();
        if (s3 < 0) {
            s3 += 256;
        }
        return (s2 << 8) | s1 | (s3 << 16);
    }

    private static int LoadInteger() throws IOException {
        int s1 = LoadByte();
        if (s1 < 0) {
            s1 += 256;
        }
        int s2 = LoadByte();
        if (s2 < 0) {
            s2 += 256;
        }
        if (s1 == 255 && s2 == 255) {
            s2 = 0;
            s1 = 0;
        }
        return (s2 << 8) | s1;
    }

    private static boolean LoadBoolean() throws IOException {
        if (LoadByte() == 30) {
            return true;
        }
        return false;
    }
}
