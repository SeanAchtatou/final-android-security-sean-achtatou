package com.machaon.quadratum.parts;

public class Bonus extends Geom {
    public int kind;

    public Bonus(int x, int y, int width, int height, int kind2) {
        super(x, y, width, height);
        this.kind = kind2;
    }
}
