package com.machaon.quadratum.parts;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.machaon.quadratum.States;

public class Keyboard {
    public static final char CHAR_DELETE = '`';
    public static final char CHAR_ENTER = '|';
    public static final char CHAR_SPACE = ' ';
    public static final byte STATE_NEXT = 3;
    public static final byte STATE_NORMAL = 0;
    public static final byte STATE_NUMBER = 20;
    public static final byte STATE_SHIFT = 1;
    public final byte SPACE = 2;
    public ButtonChar[] buttonControl = new ButtonChar[5];
    public ButtonChar[] buttonKb;
    private final char[][] charsEngNormal = {new char[]{'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p'}, new char[]{'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l'}, new char[]{'z', 'x', 'c', 'v', 'b', 'n', 'm'}};
    private final char[][] charsEngShift = {new char[]{'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P'}, new char[]{'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L'}, new char[]{'Z', 'X', 'C', 'V', 'B', 'N', 'M'}};
    private final char[][] charsNumber = {new char[]{'1', '2', '3', '4', '5', '6', '7', '8', '9', '0'}, new char[]{'!', '@', '#', '$', '%', '^', '&', '*', '(', ')'}, new char[]{'-', '+', '?', '=', ':', '\'', '\"', ',', '.'}};
    private final char[][] charsRusNormal = {new char[]{1081, 1094, 1091, 1082, 1077, 1085, 1075, 1096, 1097, 1079, 1093, 1098}, new char[]{1092, 1099, 1074, 1072, 1087, 1088, 1086, 1083, 1076, 1078, 1101}, new char[]{1103, 1095, 1089, 1084, 1080, 1090, 1100, 1073, 1102}};
    private final char[][] charsRusShift = {new char[]{1049, 1062, 1059, 1050, 1045, 1053, 1043, 1064, 1065, 1047, 1061, 1066}, new char[]{1060, 1067, 1042, 1040, 1055, 1056, 1054, 1051, 1044, 1046, 1069}, new char[]{1071, 1063, 1057, 1052, 1048, 1058, 1068, 1041, 1070}};
    public int fontX;
    public int fontY;
    public byte language;
    public int[] line;
    private byte state = 0;
    private Texture texBackA;
    private Texture texBackNA;
    private Texture texBackSpaceA;
    private Texture texBackSpaceNA;

    public Keyboard() {
        setLanguage(States.language);
        String path = States.screenWidth == 320 ? "data/Graph/240_320/" : States.PATH_GRAPH + States.resolution + "/";
        setTextures(new Texture(Gdx.files.internal(String.valueOf(path) + "but_kb_na.png")), new Texture(Gdx.files.internal(String.valueOf(path) + "but_kb_a.png")), new Texture(Gdx.files.internal(String.valueOf(path) + "but_kb_sp_na.png")), new Texture(Gdx.files.internal(String.valueOf(path) + "but_kb_sp_a.png")));
    }

    public void setTextures(Texture texBackNA2, Texture texBackA2, Texture texBackSpaceNA2, Texture texBackSpaceA2) {
        this.texBackNA = texBackNA2;
        this.texBackA = texBackA2;
        this.texBackSpaceA = texBackSpaceA2;
        this.texBackSpaceNA = texBackSpaceNA2;
    }

    public void setState(byte state2) {
        if (state2 == 3) {
            if (this.state == 0) {
                this.state = 1;
            } else {
                this.state = 0;
            }
        } else if (state2 != 20) {
            this.state = state2;
        } else if (this.state == 20) {
            this.state = 0;
            setLanguage(this.language);
        } else {
            this.state = STATE_NUMBER;
            setLanguage(STATE_NUMBER);
        }
    }

    public Texture getBackTexture(ButtonChar b) {
        if (b.getNormalChar() == ' ') {
            if (b.isBackActive) {
                return this.texBackSpaceA;
            }
            return this.texBackSpaceNA;
        } else if (b.isBackActive) {
            return this.texBackA;
        } else {
            return this.texBackNA;
        }
    }

    public void setLanguage(byte language2) {
        if (language2 != 20) {
            this.language = language2;
        }
        this.line = new int[3];
        char[][] charsNormal = null;
        char[][] charsShift = null;
        if (language2 == 20) {
            for (int i = 0; i < 3; i++) {
                this.line[i] = this.charsNumber[i].length;
            }
            charsNormal = this.charsNumber;
            charsShift = this.charsNumber;
        }
        if (language2 == 0) {
            for (int i2 = 0; i2 < 3; i2++) {
                this.line[i2] = this.charsEngNormal[i2].length;
            }
            charsNormal = this.charsEngNormal;
            charsShift = this.charsEngShift;
        }
        if (language2 == 1) {
            for (int i3 = 0; i3 < 3; i3++) {
                this.line[i3] = this.charsRusNormal[i3].length;
            }
            charsNormal = this.charsRusNormal;
            charsShift = this.charsRusShift;
        }
        this.buttonKb = new ButtonChar[(this.line[0] + this.line[1] + this.line[2])];
        int i4 = 0;
        for (int p = 0; p < 3; p++) {
            int m = 0;
            while (m < this.line[p]) {
                this.buttonKb[i4] = new ButtonChar(0, 0, 0, 0);
                this.buttonKb[i4].setChars(charsNormal[p][m], charsShift[p][m]);
                m++;
                i4++;
            }
        }
        for (int p2 = 0; p2 < 5; p2++) {
            this.buttonControl[p2] = new ButtonChar(0, 0, 0, 0);
        }
        this.buttonControl[0].setChars('~', '~');
        this.buttonControl[1].setChars('_', '_');
        this.buttonControl[2].setChars(CHAR_SPACE, CHAR_SPACE);
        this.buttonControl[3].setChars(CHAR_DELETE, CHAR_DELETE);
        this.buttonControl[4].setChars(CHAR_ENTER, CHAR_ENTER);
        int w = 0;
        int s = 0;
        int fX = 0;
        int fY = 0;
        int butY = 0;
        if (States.screenHeight == 240 && States.screenWidth == 320) {
            w = 24;
            s = 2;
            fX = 10;
            fY = 20;
            butY = 96;
        }
        if (States.screenHeight == 240 && States.screenWidth == 400) {
            w = 26;
            s = 5;
            fX = 10;
            fY = 20;
            butY = 96;
        }
        if (States.screenHeight == 320) {
            w = 35;
            s = 4;
            fX = 13;
            fY = 26;
            butY = Input.Keys.META_SHIFT_RIGHT_ON;
        }
        if (States.screenHeight == 480) {
            w = 52;
            s = 8;
            fX = 13;
            fY = 37;
            butY = 192;
        }
        int i5 = 0;
        for (int p3 = 0; p3 < 3; p3++) {
            int m2 = 0;
            while (m2 < this.line[p3]) {
                this.buttonKb[i5].setGeom((((States.screenWidth - (this.line[p3] * w)) - ((this.line[p3] - 1) * s)) / 2) + ((w + s) * m2), butY - ((w + s) * p3), w, w);
                m2++;
                i5++;
            }
        }
        this.fontX = fX;
        this.fontY = fY;
        for (int i6 = 0; i6 < 2; i6++) {
            this.buttonControl[i6].setGeom((((States.screenWidth - (w * 7)) - (s * 6)) / 2) + ((w + s) * i6), (butY - (w * 3)) - (s * 3), w, w);
            this.buttonControl[i6 + 3].setGeom((((States.screenWidth - (w * 7)) - (s * 6)) / 2) + ((w + s) * (i6 + 5)), (butY - (w * 3)) - (s * 3), w, w);
        }
        this.buttonControl[2].setGeom((((States.screenWidth - (w * 7)) - (s * 6)) / 2) + ((w + s) * 2), (butY - (w * 3)) - (s * 3), (w * 3) + (s * 2), w);
    }

    public char getChar(ButtonChar button) {
        button.isBackActive = false;
        if (this.state == 0 || this.state == 20) {
            return button.getNormalChar();
        }
        if (this.state == 1) {
            return button.getShiftChar();
        }
        return 0;
    }

    public void dispose() {
        this.texBackNA.dispose();
        this.texBackA.dispose();
        this.texBackSpaceA.dispose();
        this.texBackSpaceNA.dispose();
    }
}
