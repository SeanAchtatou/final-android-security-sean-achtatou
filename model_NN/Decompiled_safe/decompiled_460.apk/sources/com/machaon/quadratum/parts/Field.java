package com.machaon.quadratum.parts;

import com.badlogic.gdx.Gdx;
import com.machaon.quadratum.States;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.util.Random;

public class Field {
    public byte[] AnybodyTeleportedX = new byte[4];
    public byte[] AnybodyTeleportedY = new byte[4];
    public final byte BLACK_COLOR = 7;
    public final byte CHECKED = 3;
    final byte FINDED = 1;
    public final byte NONE = 110;
    public final byte NO_COLOR = Keyboard.STATE_NUMBER;
    final byte ZERO = 0;
    public int[] blownColors;
    public byte bombAIX;
    public byte bombAIY;
    public final byte[][] cell;
    public final byte[][] cellBonus;
    public final byte[][] cellTemp;
    public final byte[][] cellTemp2;
    public byte[] findedBonus = new byte[8];
    public byte[] findedBonusX = new byte[8];
    public byte[] findedBonusY = new byte[8];
    public byte freezeStepAI1;
    public byte freezeStepAI2;
    public boolean[] isAnybodyTeleported = new boolean[4];
    public boolean isMixerOnBlind = false;
    private final int maxPoints;
    private int nBlackCells = 0;
    private byte[] numberOfBonuses;
    public int[] numberOfCells;
    public final int numberOfColors;
    private String numberOfLvl;
    public final int numberOfPlayers;
    public byte[] playerColors;
    public byte[][][] playerField;
    public byte playerWhichBlind = 0;
    public final int sizeFieldX;
    public final int sizeFieldY;
    States states;
    public byte[] stepAfterTeleported = new byte[4];
    public byte teleportX;
    public byte teleportY;
    public int temp;

    public Field(int numberOfPlayers2, int sizeFieldX2, int sizeFieldY2, int numberOfColors2, String numberOfLvl2, States states2) {
        this.numberOfPlayers = numberOfPlayers2;
        this.sizeFieldX = sizeFieldX2;
        this.sizeFieldY = sizeFieldY2;
        this.numberOfColors = numberOfColors2;
        this.numberOfLvl = numberOfLvl2;
        this.states = states2;
        this.cell = (byte[][]) Array.newInstance(Byte.TYPE, sizeFieldX2, sizeFieldY2);
        this.cellBonus = (byte[][]) Array.newInstance(Byte.TYPE, sizeFieldX2, sizeFieldY2);
        this.cellTemp = (byte[][]) Array.newInstance(Byte.TYPE, sizeFieldX2, sizeFieldY2);
        this.cellTemp2 = (byte[][]) Array.newInstance(Byte.TYPE, sizeFieldX2, sizeFieldY2);
        this.playerField = (byte[][][]) Array.newInstance(Byte.TYPE, sizeFieldX2, sizeFieldY2, numberOfPlayers2);
        this.numberOfCells = new int[numberOfPlayers2];
        this.playerColors = new byte[numberOfPlayers2];
        this.blownColors = new int[7];
        this.numberOfBonuses = new byte[6];
        Init();
        this.maxPoints = (sizeFieldX2 * sizeFieldY2) - this.nBlackCells;
    }

    public void Init() {
        for (int i = 0; i < this.numberOfPlayers; i++) {
            this.numberOfCells[i] = 1;
            for (int x = 0; x < this.sizeFieldX; x++) {
                for (int y = 0; y < this.sizeFieldY; y++) {
                    this.playerField[x][y][i] = 0;
                }
            }
        }
        this.playerField[0][this.sizeFieldY - 1][0] = 3;
        if (this.numberOfPlayers == 2) {
            this.playerField[this.sizeFieldX - 1][0][1] = 3;
        } else {
            this.playerField[this.sizeFieldX - 1][this.sizeFieldY - 1][1] = 3;
            this.playerField[this.sizeFieldX - 1][0][2] = 3;
            this.playerField[0][0][3] = 3;
        }
        Random rand = new Random();
        for (int x2 = 0; x2 < this.sizeFieldX; x2++) {
            for (int y2 = 0; y2 < this.sizeFieldY; y2++) {
                this.cell[x2][y2] = (byte) rand.nextInt(this.numberOfColors);
            }
        }
        InputStream in = Gdx.files.internal("data/Levels/" + this.numberOfLvl).read();
        try {
            this.nBlackCells = in.read();
            for (int i2 = 0; i2 < this.nBlackCells; i2++) {
                this.cell[in.read()][in.read()] = 7;
            }
            this.numberOfBonuses[0] = (byte) in.read();
            this.numberOfBonuses[1] = (byte) in.read();
            this.numberOfBonuses[2] = (byte) in.read();
            this.numberOfBonuses[3] = (byte) in.read();
            this.numberOfBonuses[4] = (byte) in.read();
            this.numberOfBonuses[5] = (byte) in.read();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            in.close();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        arrangeBonuses();
        if (this.numberOfPlayers != 2) {
            while (true) {
                if (getPlayerColor(0) != getPlayerColor(1) && getPlayerColor(0) != getPlayerColor(2) && getPlayerColor(0) != getPlayerColor(3) && getPlayerColor(1) != getPlayerColor(2) && getPlayerColor(1) != getPlayerColor(3) && getPlayerColor(2) != getPlayerColor(3)) {
                    break;
                }
                setPlayerColor(1, (byte) rand.nextInt(this.numberOfColors));
                setPlayerColor(2, (byte) rand.nextInt(this.numberOfColors));
                setPlayerColor(3, (byte) rand.nextInt(this.numberOfColors));
            }
        } else {
            while (getPlayerColor(0) == getPlayerColor(1)) {
                setPlayerColor(1, (byte) rand.nextInt(this.numberOfColors));
            }
        }
        for (int i3 = 0; i3 < this.numberOfPlayers; i3++) {
            updatePlayerField(i3, getPlayerColor(i3));
        }
    }

    public void arrangeBonuses() {
        boolean isPlaced;
        byte newX;
        byte newY;
        Random rand = new Random();
        if (States.typeOfGame == 2) {
            this.numberOfBonuses[0] = 0;
            this.numberOfBonuses[1] = 0;
            this.numberOfBonuses[2] = 0;
            this.numberOfBonuses[3] = 0;
            this.numberOfBonuses[4] = 0;
            this.numberOfBonuses[5] = 0;
        }
        for (int x = 0; x < this.sizeFieldX; x++) {
            for (int y = 0; y < this.sizeFieldY; y++) {
                this.cellBonus[x][y] = 110;
            }
        }
        for (byte b = 0; b < 6; b = (byte) (b + 1)) {
            for (int k = 0; k < this.numberOfBonuses[b]; k++) {
                do {
                    isPlaced = true;
                    newX = (byte) rand.nextInt(this.sizeFieldX);
                    newY = (byte) rand.nextInt(this.sizeFieldY);
                    if ((newX < 4 && newY < 4) || ((newX < 4 && newY >= this.sizeFieldY - 4) || ((newX >= this.sizeFieldX - 4 && newY < 4) || (newX >= this.sizeFieldX - 4 && newY >= this.sizeFieldY - 4)))) {
                        isPlaced = false;
                        continue;
                    } else if (this.cell[newX][newY] == 7 || this.cellBonus[newX][newY] != 110) {
                        isPlaced = false;
                        continue;
                    }
                } while (!isPlaced);
                this.cellBonus[newX][newY] = b;
            }
        }
    }

    public boolean updatePlayerField(int player, byte selectedColor) {
        boolean isDone;
        for (int x = 0; x < this.sizeFieldX; x++) {
            for (int y = 0; y < this.sizeFieldY; y++) {
                if (this.playerField[x][y][player] == 3) {
                    this.playerField[x][y][player] = 1;
                    this.cell[x][y] = selectedColor;
                }
            }
        }
        int nCells = 0;
        do {
            isDone = true;
            int x2 = 0;
            while (x2 < this.sizeFieldX) {
                int y2 = this.sizeFieldY - 1;
                if (this.playerField[x2][y2][player] == 1) {
                    isDone = false;
                    this.playerField[x2][y2][player] = 3;
                    nCells++;
                    int x3 = x2 + 1;
                    if (x3 < this.sizeFieldX && this.cell[x3][y2] == selectedColor) {
                        byte[] bArr = this.playerField[x3][y2];
                        bArr[player] = (byte) (bArr[player] | 1);
                    }
                    int x4 = x3 - 1;
                    int y3 = y2 - 1;
                    if (this.cell[x4][y3] == selectedColor) {
                        byte[] bArr2 = this.playerField[x4][y3];
                        bArr2[player] = (byte) (bArr2[player] | 1);
                    }
                    int x5 = x4 - 1;
                    int y4 = y3 + 1;
                    if (x5 >= 0 && this.cell[x5][y4] == selectedColor) {
                        byte[] bArr3 = this.playerField[x5][y4];
                        bArr3[player] = (byte) (bArr3[player] | 1);
                    }
                    x2 = x5 + 1;
                }
                if (this.playerField[x2][0][player] == 1) {
                    isDone = false;
                    this.playerField[x2][0][player] = 3;
                    nCells++;
                    int x6 = x2 + 1;
                    if (x6 < this.sizeFieldX && this.cell[x6][0] == selectedColor) {
                        byte[] bArr4 = this.playerField[x6][0];
                        bArr4[player] = (byte) (bArr4[player] | 1);
                    }
                    int x7 = x6 - 1;
                    int y5 = 0 + 1;
                    if (this.cell[x7][y5] == selectedColor) {
                        byte[] bArr5 = this.playerField[x7][y5];
                        bArr5[player] = (byte) (bArr5[player] | 1);
                    }
                    int x8 = x7 - 1;
                    int y6 = y5 - 1;
                    if (x8 >= 0 && this.cell[x8][y6] == selectedColor) {
                        byte[] bArr6 = this.playerField[x8][y6];
                        bArr6[player] = (byte) (bArr6[player] | 1);
                    }
                    x2 = x8 + 1;
                }
                x2++;
            }
            int y7 = 1;
            while (y7 < this.sizeFieldY - 1) {
                int x9 = this.sizeFieldX - 1;
                if (this.playerField[x9][y7][player] == 1) {
                    isDone = false;
                    this.playerField[x9][y7][player] = 3;
                    nCells++;
                    int y8 = y7 + 1;
                    if (this.cell[x9][y8] == selectedColor) {
                        byte[] bArr7 = this.playerField[x9][y8];
                        bArr7[player] = (byte) (bArr7[player] | 1);
                    }
                    int x10 = x9 - 1;
                    int y9 = y8 - 1;
                    if (this.cell[x10][y9] == selectedColor) {
                        byte[] bArr8 = this.playerField[x10][y9];
                        bArr8[player] = (byte) (bArr8[player] | 1);
                    }
                    int x11 = x10 + 1;
                    int y10 = y9 - 1;
                    if (this.cell[x11][y10] == selectedColor) {
                        byte[] bArr9 = this.playerField[x11][y10];
                        bArr9[player] = (byte) (bArr9[player] | 1);
                    }
                    y7 = y10 + 1;
                }
                if (this.playerField[0][y7][player] == 1) {
                    isDone = false;
                    this.playerField[0][y7][player] = 3;
                    nCells++;
                    int y11 = y7 + 1;
                    if (this.cell[0][y11] == selectedColor) {
                        byte[] bArr10 = this.playerField[0][y11];
                        bArr10[player] = (byte) (bArr10[player] | 1);
                    }
                    int x12 = 0 + 1;
                    int y12 = y11 - 1;
                    if (this.cell[x12][y12] == selectedColor) {
                        byte[] bArr11 = this.playerField[x12][y12];
                        bArr11[player] = (byte) (bArr11[player] | 1);
                    }
                    int x13 = x12 - 1;
                    int y13 = y12 - 1;
                    if (this.cell[x13][y13] == selectedColor) {
                        byte[] bArr12 = this.playerField[x13][y13];
                        bArr12[player] = (byte) (bArr12[player] | 1);
                    }
                    y7 = y13 + 1;
                }
                y7++;
            }
            int x14 = 1;
            while (x14 < this.sizeFieldX - 1) {
                int y14 = 1;
                while (y14 < this.sizeFieldY - 1) {
                    if (this.playerField[x14][y14][player] == 1) {
                        isDone = false;
                        this.playerField[x14][y14][player] = 3;
                        nCells++;
                        int y15 = y14 + 1;
                        if (this.cell[x14][y15] == selectedColor) {
                            byte[] bArr13 = this.playerField[x14][y15];
                            bArr13[player] = (byte) (bArr13[player] | 1);
                        }
                        int x15 = x14 + 1;
                        int y16 = y15 - 1;
                        if (this.cell[x15][y16] == selectedColor) {
                            byte[] bArr14 = this.playerField[x15][y16];
                            bArr14[player] = (byte) (bArr14[player] | 1);
                        }
                        int x16 = x15 - 1;
                        int y17 = y16 - 1;
                        if (this.cell[x16][y17] == selectedColor) {
                            byte[] bArr15 = this.playerField[x16][y17];
                            bArr15[player] = (byte) (bArr15[player] | 1);
                        }
                        int x17 = x16 - 1;
                        y14 = y17 + 1;
                        if (this.cell[x17][y14] == selectedColor) {
                            byte[] bArr16 = this.playerField[x17][y14];
                            bArr16[player] = (byte) (bArr16[player] | 1);
                        }
                        x14 = x17 + 1;
                    }
                    y14++;
                }
                x14++;
            }
        } while (!isDone);
        this.numberOfCells[player] = nCells;
        for (int i = 0; i < 8; i++) {
            this.findedBonus[i] = States.NONE;
        }
        byte index = 0;
        boolean isFindedBonus = false;
        for (byte x18 = 0; x18 < this.sizeFieldX; x18 = (byte) (x18 + 1)) {
            for (byte y18 = 0; y18 < this.sizeFieldY; y18 = (byte) (y18 + 1)) {
                if (this.playerField[x18][y18][player] == 3 && this.cellBonus[x18][y18] != 110) {
                    this.findedBonusX[index] = x18;
                    this.findedBonusY[index] = y18;
                    byte[] bArr17 = States.playerBonus[player];
                    byte b = this.cellBonus[x18][y18];
                    bArr17[b] = (byte) (bArr17[b] + 1);
                    byte index2 = (byte) (index + 1);
                    this.findedBonus[index] = this.cellBonus[x18][y18];
                    if (index2 == 8) {
                        index = (byte) (index2 - 1);
                    } else {
                        index = index2;
                    }
                    this.cellBonus[x18][y18] = 110;
                    isFindedBonus = true;
                }
            }
        }
        return isFindedBonus;
    }

    public byte getPlayerColor(int player) {
        switch (player) {
            case 0:
                return this.cell[0][this.sizeFieldY - 1];
            case 1:
                return this.cell[this.sizeFieldX - 1][this.numberOfPlayers == 2 ? 0 : this.sizeFieldY - 1];
            case 2:
                return this.cell[this.sizeFieldX - 1][0];
            case 3:
                return this.cell[0][0];
            default:
                return States.NONE;
        }
    }

    public void setPlayerColor(int player, byte color) {
        switch (player) {
            case 0:
                this.cell[0][this.sizeFieldY - 1] = color;
                return;
            case 1:
                this.cell[this.sizeFieldX - 1][this.numberOfPlayers == 2 ? 0 : this.sizeFieldY - 1] = color;
                return;
            case 2:
                this.cell[this.sizeFieldX - 1][0] = color;
                return;
            case 3:
                this.cell[0][0] = color;
                return;
            default:
                return;
        }
    }

    public boolean isThisEnd() {
        int n = 0;
        for (int i = 0; i < this.numberOfPlayers; i++) {
            n += this.numberOfCells[i];
        }
        return this.maxPoints == n;
    }

    /* JADX WARN: Type inference failed for: r0v39, types: [int] */
    /* JADX WARN: Type inference failed for: r0v94, types: [int] */
    /* JADX WARN: Type inference failed for: r0v97, types: [int] */
    /* JADX WARN: Type inference failed for: r0v181, types: [int] */
    /* JADX WARN: Type inference failed for: r0v249, types: [int] */
    /* JADX WARNING: Incorrect type for immutable var: ssa=byte, code=int, for r15v11, types: [byte] */
    /* JADX WARNING: Incorrect type for immutable var: ssa=byte, code=int, for r15v15, types: [byte] */
    /* JADX WARNING: Incorrect type for immutable var: ssa=byte, code=int, for r19v5, types: [byte] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public byte AI(int r43, int r44, boolean r45) {
        /*
            r42 = this;
            com.machaon.quadratum.parts.Table r33 = new com.machaon.quadratum.parts.Table
            r0 = r42
            int r0 = r0.numberOfColors
            r39 = r0
            r0 = r33
            r1 = r39
            r0.<init>(r1)
            r7 = 0
        L_0x0010:
            r0 = r42
            int r0 = r0.numberOfColors
            r39 = r0
            r0 = r7
            r1 = r39
            if (r0 < r1) goto L_0x01e0
            r7 = 0
        L_0x001c:
            r0 = r42
            int r0 = r0.numberOfColors
            r39 = r0
            r0 = r7
            r1 = r39
            if (r0 < r1) goto L_0x01f7
            r0 = r42
            int r0 = r0.sizeFieldX
            r39 = r0
            r0 = r42
            int r0 = r0.sizeFieldY
            r40 = r0
            int[] r39 = new int[]{r39, r40}
            java.lang.Class r40 = java.lang.Byte.TYPE
            r0 = r40
            r1 = r39
            java.lang.Object r3 = java.lang.reflect.Array.newInstance(r0, r1)
            byte[][] r3 = (byte[][]) r3
            r0 = r42
            int r0 = r0.sizeFieldX
            r39 = r0
            r0 = r42
            int r0 = r0.sizeFieldY
            r40 = r0
            int[] r39 = new int[]{r39, r40}
            java.lang.Class r40 = java.lang.Byte.TYPE
            r0 = r40
            r1 = r39
            java.lang.Object r25 = java.lang.reflect.Array.newInstance(r0, r1)
            byte[][] r25 = (byte[][]) r25
            r37 = 0
        L_0x0061:
            r0 = r42
            int r0 = r0.sizeFieldX
            r39 = r0
            r0 = r37
            r1 = r39
            if (r0 < r1) goto L_0x0227
            r7 = 0
        L_0x006e:
            r0 = r42
            int r0 = r0.numberOfPlayers
            r39 = r0
            r0 = r7
            r1 = r39
            if (r0 < r1) goto L_0x025a
            r0 = r42
            int r0 = r0.sizeFieldX
            r39 = r0
            r0 = r42
            int r0 = r0.sizeFieldY
            r40 = r0
            int[] r39 = new int[]{r39, r40}
            java.lang.Class r40 = java.lang.Byte.TYPE
            r0 = r40
            r1 = r39
            java.lang.Object r4 = java.lang.reflect.Array.newInstance(r0, r1)
            byte[][] r4 = (byte[][]) r4
            r0 = r42
            int r0 = r0.sizeFieldX
            r39 = r0
            r0 = r42
            int r0 = r0.sizeFieldY
            r40 = r0
            int[] r39 = new int[]{r39, r40}
            java.lang.Class r40 = java.lang.Byte.TYPE
            r0 = r40
            r1 = r39
            java.lang.Object r26 = java.lang.reflect.Array.newInstance(r0, r1)
            byte[][] r26 = (byte[][]) r26
            r37 = 0
        L_0x00b3:
            r0 = r42
            int r0 = r0.sizeFieldX
            r39 = r0
            r0 = r37
            r1 = r39
            if (r0 < r1) goto L_0x029f
            r30 = 0
        L_0x00c1:
            r0 = r42
            int r0 = r0.numberOfColors
            r39 = r0
            r0 = r30
            r1 = r39
            if (r0 < r1) goto L_0x02c4
            r18 = 0
            r17 = 0
            r15 = 0
            r16 = 0
            r34 = 0
        L_0x00d6:
            r0 = r42
            int r0 = r0.numberOfColors
            r39 = r0
            r0 = r34
            r1 = r39
            if (r0 < r1) goto L_0x08cf
            r20 = 0
            r19 = 0
            java.util.Random r29 = new java.util.Random
            r29.<init>()
            r39 = 100
            r0 = r29
            r1 = r39
            int r39 = r0.nextInt(r1)
            r0 = r39
            byte r0 = (byte) r0
            r14 = r0
            if (r15 == 0) goto L_0x010a
            r39 = 3
            r0 = r44
            r1 = r39
            if (r0 != r1) goto L_0x012a
            r39 = 50
            r0 = r14
            r1 = r39
            if (r0 <= r1) goto L_0x012a
        L_0x010a:
            r34 = 0
        L_0x010c:
            r0 = r42
            int r0 = r0.numberOfColors
            r39 = r0
            r0 = r34
            r1 = r39
            if (r0 < r1) goto L_0x093f
            r19 = r16
            r20 = r17
            r34 = 0
        L_0x011e:
            r0 = r42
            int r0 = r0.numberOfColors
            r39 = r0
            r0 = r34
            r1 = r39
            if (r0 < r1) goto L_0x0963
        L_0x012a:
            r39 = 100
            r0 = r29
            r1 = r39
            int r39 = r0.nextInt(r1)
            r0 = r39
            byte r0 = (byte) r0
            r14 = r0
            if (r16 == 0) goto L_0x0157
            r39 = 1
            r0 = r44
            r1 = r39
            if (r0 != r1) goto L_0x0157
            r39 = 50
            r0 = r14
            r1 = r39
            if (r0 <= r1) goto L_0x098e
            int r39 = r15 / r16
            r40 = 4
            r0 = r39
            r1 = r40
            if (r0 >= r1) goto L_0x0157
            r18 = r17
            r15 = r16
        L_0x0157:
            if (r16 == 0) goto L_0x018f
            r39 = 2
            r0 = r44
            r1 = r39
            if (r0 != r1) goto L_0x018f
            r39 = 30
            r0 = r14
            r1 = r39
            if (r0 <= r1) goto L_0x018f
            int r39 = r15 / r16
            r40 = 2
            r0 = r39
            r1 = r40
            if (r0 >= r1) goto L_0x018f
            r18 = r17
            r15 = r16
            r39 = 100
            r0 = r29
            r1 = r39
            int r39 = r0.nextInt(r1)
            r0 = r39
            byte r0 = (byte) r0
            r14 = r0
            r39 = 70
            r0 = r14
            r1 = r39
            if (r0 <= r1) goto L_0x018f
            r15 = r19
            r18 = r20
        L_0x018f:
            if (r15 == 0) goto L_0x019f
            if (r45 == 0) goto L_0x01df
            r0 = r42
            byte r0 = r0.playerWhichBlind
            r39 = r0
            r0 = r39
            r1 = r43
            if (r0 == r1) goto L_0x01df
        L_0x019f:
            r0 = r42
            int r0 = r0.numberOfColors
            r39 = r0
            r0 = r29
            r1 = r39
            int r39 = r0.nextInt(r1)
            r0 = r39
            byte r0 = (byte) r0
            r2 = r0
            r0 = r42
            int r0 = r0.numberOfPlayers
            r39 = r0
            r40 = 2
            r0 = r39
            r1 = r40
            if (r0 != r1) goto L_0x09ba
        L_0x01bf:
            r39 = 0
            r0 = r42
            r1 = r39
            byte r39 = r0.getPlayerColor(r1)
            r0 = r39
            r1 = r2
            if (r0 == r1) goto L_0x0994
            r39 = 1
            r0 = r42
            r1 = r39
            byte r39 = r0.getPlayerColor(r1)
            r0 = r39
            r1 = r2
            if (r0 == r1) goto L_0x0994
        L_0x01dd:
            r18 = r2
        L_0x01df:
            return r18
        L_0x01e0:
            r0 = r33
            com.machaon.quadratum.parts.Table[] r0 = r0.table
            r39 = r0
            com.machaon.quadratum.parts.Table r40 = new com.machaon.quadratum.parts.Table
            r0 = r42
            int r0 = r0.numberOfColors
            r41 = r0
            r40.<init>(r41)
            r39[r7] = r40
            int r7 = r7 + 1
            goto L_0x0010
        L_0x01f7:
            r34 = 0
        L_0x01f9:
            r0 = r42
            int r0 = r0.numberOfColors
            r39 = r0
            r0 = r34
            r1 = r39
            if (r0 < r1) goto L_0x0209
            int r7 = r7 + 1
            goto L_0x001c
        L_0x0209:
            r0 = r33
            com.machaon.quadratum.parts.Table[] r0 = r0.table
            r39 = r0
            r39 = r39[r7]
            r0 = r39
            com.machaon.quadratum.parts.Table[] r0 = r0.table
            r39 = r0
            com.machaon.quadratum.parts.Table r40 = new com.machaon.quadratum.parts.Table
            r0 = r42
            int r0 = r0.numberOfColors
            r41 = r0
            r40.<init>(r41)
            r39[r34] = r40
            int r34 = r34 + 1
            goto L_0x01f9
        L_0x0227:
            r38 = 0
        L_0x0229:
            r0 = r42
            int r0 = r0.sizeFieldY
            r39 = r0
            r0 = r38
            r1 = r39
            if (r0 < r1) goto L_0x0239
            int r37 = r37 + 1
            goto L_0x0061
        L_0x0239:
            r39 = r3[r37]
            r0 = r42
            byte[][] r0 = r0.cell
            r40 = r0
            r40 = r40[r37]
            byte r40 = r40[r38]
            r39[r38] = r40
            r39 = r25[r37]
            r0 = r42
            byte[][][] r0 = r0.playerField
            r40 = r0
            r40 = r40[r37]
            r40 = r40[r38]
            byte r40 = r40[r43]
            r39[r38] = r40
            int r38 = r38 + 1
            goto L_0x0229
        L_0x025a:
            r0 = r7
            r1 = r43
            if (r0 == r1) goto L_0x026d
            r37 = 0
        L_0x0261:
            r0 = r42
            int r0 = r0.sizeFieldX
            r39 = r0
            r0 = r37
            r1 = r39
            if (r0 < r1) goto L_0x0271
        L_0x026d:
            int r7 = r7 + 1
            goto L_0x006e
        L_0x0271:
            r38 = 0
        L_0x0273:
            r0 = r42
            int r0 = r0.sizeFieldY
            r39 = r0
            r0 = r38
            r1 = r39
            if (r0 < r1) goto L_0x0282
            int r37 = r37 + 1
            goto L_0x0261
        L_0x0282:
            r0 = r42
            byte[][][] r0 = r0.playerField
            r39 = r0
            r39 = r39[r37]
            r39 = r39[r38]
            byte r39 = r39[r7]
            r40 = 3
            r0 = r39
            r1 = r40
            if (r0 != r1) goto L_0x029c
            r39 = r3[r37]
            r40 = 20
            r39[r38] = r40
        L_0x029c:
            int r38 = r38 + 1
            goto L_0x0273
        L_0x029f:
            r38 = 0
        L_0x02a1:
            r0 = r42
            int r0 = r0.sizeFieldY
            r39 = r0
            r0 = r38
            r1 = r39
            if (r0 < r1) goto L_0x02b1
            int r37 = r37 + 1
            goto L_0x00b3
        L_0x02b1:
            r39 = r4[r37]
            r40 = r3[r37]
            byte r40 = r40[r38]
            r39[r38] = r40
            r39 = r26[r37]
            r40 = r25[r37]
            byte r40 = r40[r38]
            r39[r38] = r40
            int r38 = r38 + 1
            goto L_0x02a1
        L_0x02c4:
            r8 = 1
            r24 = 0
        L_0x02c7:
            r0 = r42
            int r0 = r0.numberOfPlayers
            r39 = r0
            r0 = r24
            r1 = r39
            if (r0 < r1) goto L_0x038a
            if (r8 == 0) goto L_0x08c3
            r37 = 0
        L_0x02d7:
            r0 = r42
            int r0 = r0.sizeFieldX
            r39 = r0
            r0 = r37
            r1 = r39
            if (r0 < r1) goto L_0x039d
            r37 = 0
        L_0x02e5:
            r0 = r42
            int r0 = r0.sizeFieldX
            r39 = r0
            r0 = r37
            r1 = r39
            if (r0 < r1) goto L_0x03c2
            r21 = 0
        L_0x02f3:
            r11 = 1
            r37 = 0
        L_0x02f6:
            r0 = r42
            int r0 = r0.sizeFieldX
            r39 = r0
            r0 = r37
            r1 = r39
            if (r0 < r1) goto L_0x03ed
            if (r11 == 0) goto L_0x02f3
            r0 = r33
            byte[] r0 = r0.t
            r39 = r0
            r0 = r42
            int[] r0 = r0.numberOfCells
            r40 = r0
            r40 = r40[r43]
            int r40 = r21 - r40
            r0 = r40
            byte r0 = (byte) r0
            r40 = r0
            r39[r30] = r40
            r0 = r33
            byte[] r0 = r0.t
            r39 = r0
            byte r39 = r39[r30]
            if (r39 <= 0) goto L_0x0381
            r39 = 1
            r0 = r44
            r1 = r39
            if (r0 <= r1) goto L_0x0381
            r0 = r42
            int r0 = r0.sizeFieldX
            r39 = r0
            r0 = r42
            int r0 = r0.sizeFieldY
            r40 = r0
            int[] r39 = new int[]{r39, r40}
            java.lang.Class r40 = java.lang.Byte.TYPE
            r0 = r40
            r1 = r39
            java.lang.Object r5 = java.lang.reflect.Array.newInstance(r0, r1)
            byte[][] r5 = (byte[][]) r5
            r0 = r42
            int r0 = r0.sizeFieldX
            r39 = r0
            r0 = r42
            int r0 = r0.sizeFieldY
            r40 = r0
            int[] r39 = new int[]{r39, r40}
            java.lang.Class r40 = java.lang.Byte.TYPE
            r0 = r40
            r1 = r39
            java.lang.Object r27 = java.lang.reflect.Array.newInstance(r0, r1)
            byte[][] r27 = (byte[][]) r27
            r37 = 0
        L_0x0367:
            r0 = r42
            int r0 = r0.sizeFieldX
            r39 = r0
            r0 = r37
            r1 = r39
            if (r0 < r1) goto L_0x04a0
            r31 = 0
        L_0x0375:
            r0 = r42
            int r0 = r0.numberOfColors
            r39 = r0
            r0 = r31
            r1 = r39
            if (r0 < r1) goto L_0x04c5
        L_0x0381:
            int r39 = r30 + 1
            r0 = r39
            byte r0 = (byte) r0
            r30 = r0
            goto L_0x00c1
        L_0x038a:
            r0 = r42
            r1 = r24
            byte r39 = r0.getPlayerColor(r1)
            r0 = r30
            r1 = r39
            if (r0 != r1) goto L_0x0399
            r8 = 0
        L_0x0399:
            int r24 = r24 + 1
            goto L_0x02c7
        L_0x039d:
            r38 = 0
        L_0x039f:
            r0 = r42
            int r0 = r0.sizeFieldY
            r39 = r0
            r0 = r38
            r1 = r39
            if (r0 < r1) goto L_0x03af
            int r37 = r37 + 1
            goto L_0x02d7
        L_0x03af:
            r39 = r3[r37]
            r40 = r4[r37]
            byte r40 = r40[r38]
            r39[r38] = r40
            r39 = r25[r37]
            r40 = r26[r37]
            byte r40 = r40[r38]
            r39[r38] = r40
            int r38 = r38 + 1
            goto L_0x039f
        L_0x03c2:
            r38 = 0
        L_0x03c4:
            r0 = r42
            int r0 = r0.sizeFieldY
            r39 = r0
            r0 = r38
            r1 = r39
            if (r0 < r1) goto L_0x03d4
            int r37 = r37 + 1
            goto L_0x02e5
        L_0x03d4:
            r39 = r25[r37]
            byte r39 = r39[r38]
            r40 = 3
            r0 = r39
            r1 = r40
            if (r0 != r1) goto L_0x03ea
            r39 = r25[r37]
            r40 = 1
            r39[r38] = r40
            r39 = r3[r37]
            r39[r38] = r30
        L_0x03ea:
            int r38 = r38 + 1
            goto L_0x03c4
        L_0x03ed:
            r38 = 0
        L_0x03ef:
            r0 = r42
            int r0 = r0.sizeFieldY
            r39 = r0
            r0 = r38
            r1 = r39
            if (r0 < r1) goto L_0x03ff
            int r37 = r37 + 1
            goto L_0x02f6
        L_0x03ff:
            r39 = r25[r37]
            byte r39 = r39[r38]
            r40 = 1
            r0 = r39
            r1 = r40
            if (r0 != r1) goto L_0x049c
            r11 = 0
            r39 = r25[r37]
            r40 = 3
            r39[r38] = r40
            int r21 = r21 + 1
            int r38 = r38 + 1
            r0 = r42
            int r0 = r0.sizeFieldY
            r39 = r0
            r0 = r38
            r1 = r39
            if (r0 >= r1) goto L_0x0439
            r39 = r3[r37]
            byte r39 = r39[r38]
            r0 = r39
            r1 = r30
            if (r0 != r1) goto L_0x0439
            r39 = r25[r37]
            byte r40 = r39[r38]
            r40 = r40 | 1
            r0 = r40
            byte r0 = (byte) r0
            r40 = r0
            r39[r38] = r40
        L_0x0439:
            int r37 = r37 + 1
            int r38 = r38 + -1
            r0 = r42
            int r0 = r0.sizeFieldX
            r39 = r0
            r0 = r37
            r1 = r39
            if (r0 >= r1) goto L_0x0460
            r39 = r3[r37]
            byte r39 = r39[r38]
            r0 = r39
            r1 = r30
            if (r0 != r1) goto L_0x0460
            r39 = r25[r37]
            byte r40 = r39[r38]
            r40 = r40 | 1
            r0 = r40
            byte r0 = (byte) r0
            r40 = r0
            r39[r38] = r40
        L_0x0460:
            int r37 = r37 + -1
            int r38 = r38 + -1
            if (r38 < 0) goto L_0x047d
            r39 = r3[r37]
            byte r39 = r39[r38]
            r0 = r39
            r1 = r30
            if (r0 != r1) goto L_0x047d
            r39 = r25[r37]
            byte r40 = r39[r38]
            r40 = r40 | 1
            r0 = r40
            byte r0 = (byte) r0
            r40 = r0
            r39[r38] = r40
        L_0x047d:
            int r37 = r37 + -1
            int r38 = r38 + 1
            if (r37 < 0) goto L_0x049a
            r39 = r3[r37]
            byte r39 = r39[r38]
            r0 = r39
            r1 = r30
            if (r0 != r1) goto L_0x049a
            r39 = r25[r37]
            byte r40 = r39[r38]
            r40 = r40 | 1
            r0 = r40
            byte r0 = (byte) r0
            r40 = r0
            r39[r38] = r40
        L_0x049a:
            int r37 = r37 + 1
        L_0x049c:
            int r38 = r38 + 1
            goto L_0x03ef
        L_0x04a0:
            r38 = 0
        L_0x04a2:
            r0 = r42
            int r0 = r0.sizeFieldY
            r39 = r0
            r0 = r38
            r1 = r39
            if (r0 < r1) goto L_0x04b2
            int r37 = r37 + 1
            goto L_0x0367
        L_0x04b2:
            r39 = r5[r37]
            r40 = r3[r37]
            byte r40 = r40[r38]
            r39[r38] = r40
            r39 = r27[r37]
            r40 = r25[r37]
            byte r40 = r40[r38]
            r39[r38] = r40
            int r38 = r38 + 1
            goto L_0x04a2
        L_0x04c5:
            r9 = 1
            r0 = r31
            r1 = r30
            if (r0 != r1) goto L_0x04cd
            r9 = 0
        L_0x04cd:
            if (r9 == 0) goto L_0x08a9
            r37 = 0
        L_0x04d1:
            r0 = r42
            int r0 = r0.sizeFieldX
            r39 = r0
            r0 = r37
            r1 = r39
            if (r0 < r1) goto L_0x05ae
            r37 = 0
        L_0x04df:
            r0 = r42
            int r0 = r0.sizeFieldX
            r39 = r0
            r0 = r37
            r1 = r39
            if (r0 < r1) goto L_0x05d3
            r22 = 0
        L_0x04ed:
            r12 = 1
            r37 = 0
        L_0x04f0:
            r0 = r42
            int r0 = r0.sizeFieldX
            r39 = r0
            r0 = r37
            r1 = r39
            if (r0 < r1) goto L_0x05fe
            if (r12 == 0) goto L_0x04ed
            r0 = r33
            com.machaon.quadratum.parts.Table[] r0 = r0.table
            r39 = r0
            r39 = r39[r30]
            r0 = r39
            byte[] r0 = r0.t
            r39 = r0
            r0 = r42
            int[] r0 = r0.numberOfCells
            r40 = r0
            r40 = r40[r43]
            int r40 = r22 - r40
            r0 = r40
            byte r0 = (byte) r0
            r40 = r0
            r39[r31] = r40
            r0 = r33
            byte[] r0 = r0.t
            r39 = r0
            byte r39 = r39[r30]
            r0 = r33
            com.machaon.quadratum.parts.Table[] r0 = r0.table
            r40 = r0
            r40 = r40[r30]
            r0 = r40
            byte[] r0 = r0.t
            r40 = r0
            byte r40 = r40[r31]
            r0 = r39
            r1 = r40
            if (r0 >= r1) goto L_0x086b
            r39 = 2
            r0 = r44
            r1 = r39
            if (r0 <= r1) goto L_0x086b
            r0 = r42
            int r0 = r0.numberOfPlayers
            r39 = r0
            r40 = 2
            r0 = r39
            r1 = r40
            if (r0 != r1) goto L_0x086b
            r0 = r42
            int r0 = r0.sizeFieldX
            r39 = r0
            r0 = r42
            int r0 = r0.sizeFieldY
            r40 = r0
            int[] r39 = new int[]{r39, r40}
            java.lang.Class r40 = java.lang.Byte.TYPE
            r0 = r40
            r1 = r39
            java.lang.Object r6 = java.lang.reflect.Array.newInstance(r0, r1)
            byte[][] r6 = (byte[][]) r6
            r0 = r42
            int r0 = r0.sizeFieldX
            r39 = r0
            r0 = r42
            int r0 = r0.sizeFieldY
            r40 = r0
            int[] r39 = new int[]{r39, r40}
            java.lang.Class r40 = java.lang.Byte.TYPE
            r0 = r40
            r1 = r39
            java.lang.Object r28 = java.lang.reflect.Array.newInstance(r0, r1)
            byte[][] r28 = (byte[][]) r28
            r37 = 0
        L_0x058b:
            r0 = r42
            int r0 = r0.sizeFieldX
            r39 = r0
            r0 = r37
            r1 = r39
            if (r0 < r1) goto L_0x06b1
            r32 = 0
        L_0x0599:
            r0 = r42
            int r0 = r0.numberOfColors
            r39 = r0
            r0 = r32
            r1 = r39
            if (r0 < r1) goto L_0x06d6
        L_0x05a5:
            int r39 = r31 + 1
            r0 = r39
            byte r0 = (byte) r0
            r31 = r0
            goto L_0x0375
        L_0x05ae:
            r38 = 0
        L_0x05b0:
            r0 = r42
            int r0 = r0.sizeFieldY
            r39 = r0
            r0 = r38
            r1 = r39
            if (r0 < r1) goto L_0x05c0
            int r37 = r37 + 1
            goto L_0x04d1
        L_0x05c0:
            r39 = r3[r37]
            r40 = r5[r37]
            byte r40 = r40[r38]
            r39[r38] = r40
            r39 = r25[r37]
            r40 = r27[r37]
            byte r40 = r40[r38]
            r39[r38] = r40
            int r38 = r38 + 1
            goto L_0x05b0
        L_0x05d3:
            r38 = 0
        L_0x05d5:
            r0 = r42
            int r0 = r0.sizeFieldY
            r39 = r0
            r0 = r38
            r1 = r39
            if (r0 < r1) goto L_0x05e5
            int r37 = r37 + 1
            goto L_0x04df
        L_0x05e5:
            r39 = r25[r37]
            byte r39 = r39[r38]
            r40 = 3
            r0 = r39
            r1 = r40
            if (r0 != r1) goto L_0x05fb
            r39 = r25[r37]
            r40 = 1
            r39[r38] = r40
            r39 = r3[r37]
            r39[r38] = r31
        L_0x05fb:
            int r38 = r38 + 1
            goto L_0x05d5
        L_0x05fe:
            r38 = 0
        L_0x0600:
            r0 = r42
            int r0 = r0.sizeFieldY
            r39 = r0
            r0 = r38
            r1 = r39
            if (r0 < r1) goto L_0x0610
            int r37 = r37 + 1
            goto L_0x04f0
        L_0x0610:
            r39 = r25[r37]
            byte r39 = r39[r38]
            r40 = 1
            r0 = r39
            r1 = r40
            if (r0 != r1) goto L_0x06ad
            r12 = 0
            r39 = r25[r37]
            r40 = 3
            r39[r38] = r40
            int r22 = r22 + 1
            int r38 = r38 + 1
            r0 = r42
            int r0 = r0.sizeFieldY
            r39 = r0
            r0 = r38
            r1 = r39
            if (r0 >= r1) goto L_0x064a
            r39 = r3[r37]
            byte r39 = r39[r38]
            r0 = r39
            r1 = r31
            if (r0 != r1) goto L_0x064a
            r39 = r25[r37]
            byte r40 = r39[r38]
            r40 = r40 | 1
            r0 = r40
            byte r0 = (byte) r0
            r40 = r0
            r39[r38] = r40
        L_0x064a:
            int r37 = r37 + 1
            int r38 = r38 + -1
            r0 = r42
            int r0 = r0.sizeFieldX
            r39 = r0
            r0 = r37
            r1 = r39
            if (r0 >= r1) goto L_0x0671
            r39 = r3[r37]
            byte r39 = r39[r38]
            r0 = r39
            r1 = r31
            if (r0 != r1) goto L_0x0671
            r39 = r25[r37]
            byte r40 = r39[r38]
            r40 = r40 | 1
            r0 = r40
            byte r0 = (byte) r0
            r40 = r0
            r39[r38] = r40
        L_0x0671:
            int r37 = r37 + -1
            int r38 = r38 + -1
            if (r38 < 0) goto L_0x068e
            r39 = r3[r37]
            byte r39 = r39[r38]
            r0 = r39
            r1 = r31
            if (r0 != r1) goto L_0x068e
            r39 = r25[r37]
            byte r40 = r39[r38]
            r40 = r40 | 1
            r0 = r40
            byte r0 = (byte) r0
            r40 = r0
            r39[r38] = r40
        L_0x068e:
            int r37 = r37 + -1
            int r38 = r38 + 1
            if (r37 < 0) goto L_0x06ab
            r39 = r3[r37]
            byte r39 = r39[r38]
            r0 = r39
            r1 = r31
            if (r0 != r1) goto L_0x06ab
            r39 = r25[r37]
            byte r40 = r39[r38]
            r40 = r40 | 1
            r0 = r40
            byte r0 = (byte) r0
            r40 = r0
            r39[r38] = r40
        L_0x06ab:
            int r37 = r37 + 1
        L_0x06ad:
            int r38 = r38 + 1
            goto L_0x0600
        L_0x06b1:
            r38 = 0
        L_0x06b3:
            r0 = r42
            int r0 = r0.sizeFieldY
            r39 = r0
            r0 = r38
            r1 = r39
            if (r0 < r1) goto L_0x06c3
            int r37 = r37 + 1
            goto L_0x058b
        L_0x06c3:
            r39 = r6[r37]
            r40 = r3[r37]
            byte r40 = r40[r38]
            r39[r38] = r40
            r39 = r28[r37]
            r40 = r25[r37]
            byte r40 = r40[r38]
            r39[r38] = r40
            int r38 = r38 + 1
            goto L_0x06b3
        L_0x06d6:
            r10 = 1
            r0 = r32
            r1 = r30
            if (r0 != r1) goto L_0x06de
            r10 = 0
        L_0x06de:
            if (r10 == 0) goto L_0x0841
            r37 = 0
        L_0x06e2:
            r0 = r42
            int r0 = r0.sizeFieldX
            r39 = r0
            r0 = r37
            r1 = r39
            if (r0 < r1) goto L_0x073f
            r37 = 0
        L_0x06f0:
            r0 = r42
            int r0 = r0.sizeFieldX
            r39 = r0
            r0 = r37
            r1 = r39
            if (r0 < r1) goto L_0x0763
            r23 = 0
        L_0x06fe:
            r13 = 1
            r37 = 0
        L_0x0701:
            r0 = r42
            int r0 = r0.sizeFieldX
            r39 = r0
            r0 = r37
            r1 = r39
            if (r0 < r1) goto L_0x078e
            if (r13 == 0) goto L_0x06fe
            r0 = r33
            com.machaon.quadratum.parts.Table[] r0 = r0.table
            r39 = r0
            r39 = r39[r30]
            r0 = r39
            com.machaon.quadratum.parts.Table[] r0 = r0.table
            r39 = r0
            r39 = r39[r31]
            r0 = r39
            byte[] r0 = r0.t
            r39 = r0
            r0 = r42
            int[] r0 = r0.numberOfCells
            r40 = r0
            r40 = r40[r43]
            int r40 = r23 - r40
            r0 = r40
            byte r0 = (byte) r0
            r40 = r0
            r39[r32] = r40
        L_0x0736:
            int r39 = r32 + 1
            r0 = r39
            byte r0 = (byte) r0
            r32 = r0
            goto L_0x0599
        L_0x073f:
            r38 = 0
        L_0x0741:
            r0 = r42
            int r0 = r0.sizeFieldY
            r39 = r0
            r0 = r38
            r1 = r39
            if (r0 < r1) goto L_0x0750
            int r37 = r37 + 1
            goto L_0x06e2
        L_0x0750:
            r39 = r3[r37]
            r40 = r6[r37]
            byte r40 = r40[r38]
            r39[r38] = r40
            r39 = r25[r37]
            r40 = r28[r37]
            byte r40 = r40[r38]
            r39[r38] = r40
            int r38 = r38 + 1
            goto L_0x0741
        L_0x0763:
            r38 = 0
        L_0x0765:
            r0 = r42
            int r0 = r0.sizeFieldY
            r39 = r0
            r0 = r38
            r1 = r39
            if (r0 < r1) goto L_0x0775
            int r37 = r37 + 1
            goto L_0x06f0
        L_0x0775:
            r39 = r25[r37]
            byte r39 = r39[r38]
            r40 = 3
            r0 = r39
            r1 = r40
            if (r0 != r1) goto L_0x078b
            r39 = r25[r37]
            r40 = 1
            r39[r38] = r40
            r39 = r3[r37]
            r39[r38] = r32
        L_0x078b:
            int r38 = r38 + 1
            goto L_0x0765
        L_0x078e:
            r38 = 0
        L_0x0790:
            r0 = r42
            int r0 = r0.sizeFieldY
            r39 = r0
            r0 = r38
            r1 = r39
            if (r0 < r1) goto L_0x07a0
            int r37 = r37 + 1
            goto L_0x0701
        L_0x07a0:
            r39 = r25[r37]
            byte r39 = r39[r38]
            r40 = 1
            r0 = r39
            r1 = r40
            if (r0 != r1) goto L_0x083d
            r13 = 0
            r39 = r25[r37]
            r40 = 3
            r39[r38] = r40
            int r23 = r23 + 1
            int r38 = r38 + 1
            r0 = r42
            int r0 = r0.sizeFieldY
            r39 = r0
            r0 = r38
            r1 = r39
            if (r0 >= r1) goto L_0x07da
            r39 = r3[r37]
            byte r39 = r39[r38]
            r0 = r39
            r1 = r32
            if (r0 != r1) goto L_0x07da
            r39 = r25[r37]
            byte r40 = r39[r38]
            r40 = r40 | 1
            r0 = r40
            byte r0 = (byte) r0
            r40 = r0
            r39[r38] = r40
        L_0x07da:
            int r37 = r37 + 1
            int r38 = r38 + -1
            r0 = r42
            int r0 = r0.sizeFieldX
            r39 = r0
            r0 = r37
            r1 = r39
            if (r0 >= r1) goto L_0x0801
            r39 = r3[r37]
            byte r39 = r39[r38]
            r0 = r39
            r1 = r32
            if (r0 != r1) goto L_0x0801
            r39 = r25[r37]
            byte r40 = r39[r38]
            r40 = r40 | 1
            r0 = r40
            byte r0 = (byte) r0
            r40 = r0
            r39[r38] = r40
        L_0x0801:
            int r37 = r37 + -1
            int r38 = r38 + -1
            if (r38 < 0) goto L_0x081e
            r39 = r3[r37]
            byte r39 = r39[r38]
            r0 = r39
            r1 = r32
            if (r0 != r1) goto L_0x081e
            r39 = r25[r37]
            byte r40 = r39[r38]
            r40 = r40 | 1
            r0 = r40
            byte r0 = (byte) r0
            r40 = r0
            r39[r38] = r40
        L_0x081e:
            int r37 = r37 + -1
            int r38 = r38 + 1
            if (r37 < 0) goto L_0x083b
            r39 = r3[r37]
            byte r39 = r39[r38]
            r0 = r39
            r1 = r32
            if (r0 != r1) goto L_0x083b
            r39 = r25[r37]
            byte r40 = r39[r38]
            r40 = r40 | 1
            r0 = r40
            byte r0 = (byte) r0
            r40 = r0
            r39[r38] = r40
        L_0x083b:
            int r37 = r37 + 1
        L_0x083d:
            int r38 = r38 + 1
            goto L_0x0790
        L_0x0841:
            r0 = r33
            com.machaon.quadratum.parts.Table[] r0 = r0.table
            r39 = r0
            r39 = r39[r30]
            r0 = r39
            com.machaon.quadratum.parts.Table[] r0 = r0.table
            r39 = r0
            r39 = r39[r31]
            r0 = r39
            byte[] r0 = r0.t
            r39 = r0
            r0 = r33
            com.machaon.quadratum.parts.Table[] r0 = r0.table
            r40 = r0
            r40 = r40[r30]
            r0 = r40
            byte[] r0 = r0.t
            r40 = r0
            byte r40 = r40[r31]
            r39[r32] = r40
            goto L_0x0736
        L_0x086b:
            r32 = 0
        L_0x086d:
            r0 = r42
            int r0 = r0.numberOfColors
            r39 = r0
            r0 = r32
            r1 = r39
            if (r0 >= r1) goto L_0x05a5
            r0 = r33
            com.machaon.quadratum.parts.Table[] r0 = r0.table
            r39 = r0
            r39 = r39[r30]
            r0 = r39
            com.machaon.quadratum.parts.Table[] r0 = r0.table
            r39 = r0
            r39 = r39[r31]
            r0 = r39
            byte[] r0 = r0.t
            r39 = r0
            r0 = r33
            com.machaon.quadratum.parts.Table[] r0 = r0.table
            r40 = r0
            r40 = r40[r30]
            r0 = r40
            byte[] r0 = r0.t
            r40 = r0
            byte r40 = r40[r31]
            r39[r32] = r40
            int r39 = r32 + 1
            r0 = r39
            byte r0 = (byte) r0
            r32 = r0
            goto L_0x086d
        L_0x08a9:
            r0 = r33
            com.machaon.quadratum.parts.Table[] r0 = r0.table
            r39 = r0
            r39 = r39[r30]
            r0 = r39
            byte[] r0 = r0.t
            r39 = r0
            r0 = r33
            byte[] r0 = r0.t
            r40 = r0
            byte r40 = r40[r30]
            r39[r31] = r40
            goto L_0x05a5
        L_0x08c3:
            r0 = r33
            byte[] r0 = r0.t
            r39 = r0
            r40 = 0
            r39[r30] = r40
            goto L_0x0381
        L_0x08cf:
            r35 = 0
        L_0x08d1:
            r0 = r42
            int r0 = r0.numberOfColors
            r39 = r0
            r0 = r35
            r1 = r39
            if (r0 < r1) goto L_0x08e6
            int r39 = r34 + 1
            r0 = r39
            byte r0 = (byte) r0
            r34 = r0
            goto L_0x00d6
        L_0x08e6:
            r36 = 0
        L_0x08e8:
            r0 = r42
            int r0 = r0.numberOfColors
            r39 = r0
            r0 = r36
            r1 = r39
            if (r0 < r1) goto L_0x08fc
            int r39 = r35 + 1
            r0 = r39
            byte r0 = (byte) r0
            r35 = r0
            goto L_0x08d1
        L_0x08fc:
            r0 = r33
            com.machaon.quadratum.parts.Table[] r0 = r0.table
            r39 = r0
            r39 = r39[r34]
            r0 = r39
            com.machaon.quadratum.parts.Table[] r0 = r0.table
            r39 = r0
            r39 = r39[r35]
            r0 = r39
            byte[] r0 = r0.t
            r39 = r0
            byte r39 = r39[r36]
            r0 = r39
            r1 = r15
            if (r0 <= r1) goto L_0x0937
            r17 = r18
            r18 = r34
            r16 = r15
            r0 = r33
            com.machaon.quadratum.parts.Table[] r0 = r0.table
            r39 = r0
            r39 = r39[r34]
            r0 = r39
            com.machaon.quadratum.parts.Table[] r0 = r0.table
            r39 = r0
            r39 = r39[r35]
            r0 = r39
            byte[] r0 = r0.t
            r39 = r0
            byte r15 = r39[r36]
        L_0x0937:
            int r39 = r36 + 1
            r0 = r39
            byte r0 = (byte) r0
            r36 = r0
            goto L_0x08e8
        L_0x093f:
            r0 = r33
            byte[] r0 = r0.t
            r39 = r0
            byte r39 = r39[r34]
            r0 = r39
            r1 = r15
            if (r0 <= r1) goto L_0x095a
            r17 = r18
            r18 = r34
            r16 = r15
            r0 = r33
            byte[] r0 = r0.t
            r39 = r0
            byte r15 = r39[r34]
        L_0x095a:
            int r39 = r34 + 1
            r0 = r39
            byte r0 = (byte) r0
            r34 = r0
            goto L_0x010c
        L_0x0963:
            r0 = r33
            byte[] r0 = r0.t
            r39 = r0
            byte r39 = r39[r34]
            r0 = r39
            r1 = r19
            if (r0 >= r1) goto L_0x0985
            r0 = r33
            byte[] r0 = r0.t
            r39 = r0
            byte r39 = r39[r34]
            if (r39 == 0) goto L_0x0985
            r20 = r34
            r0 = r33
            byte[] r0 = r0.t
            r39 = r0
            byte r19 = r39[r34]
        L_0x0985:
            int r39 = r34 + 1
            r0 = r39
            byte r0 = (byte) r0
            r34 = r0
            goto L_0x011e
        L_0x098e:
            r15 = r19
            r18 = r20
            goto L_0x0157
        L_0x0994:
            r0 = r42
            int r0 = r0.numberOfColors
            r39 = r0
            r0 = r29
            r1 = r39
            int r39 = r0.nextInt(r1)
            r0 = r39
            byte r0 = (byte) r0
            r2 = r0
            goto L_0x01bf
        L_0x09a8:
            r0 = r42
            int r0 = r0.numberOfColors
            r39 = r0
            r0 = r29
            r1 = r39
            int r39 = r0.nextInt(r1)
            r0 = r39
            byte r0 = (byte) r0
            r2 = r0
        L_0x09ba:
            r39 = 0
            r0 = r42
            r1 = r39
            byte r39 = r0.getPlayerColor(r1)
            r0 = r39
            r1 = r2
            if (r0 == r1) goto L_0x09a8
            r39 = 1
            r0 = r42
            r1 = r39
            byte r39 = r0.getPlayerColor(r1)
            r0 = r39
            r1 = r2
            if (r0 == r1) goto L_0x09a8
            r39 = 2
            r0 = r42
            r1 = r39
            byte r39 = r0.getPlayerColor(r1)
            r0 = r39
            r1 = r2
            if (r0 == r1) goto L_0x09a8
            r39 = 3
            r0 = r42
            r1 = r39
            byte r39 = r0.getPlayerColor(r1)
            r0 = r39
            r1 = r2
            if (r0 == r1) goto L_0x09a8
            goto L_0x01dd
        */
        throw new UnsupportedOperationException("Method not decompiled: com.machaon.quadratum.parts.Field.AI(int, int, boolean):byte");
    }

    public byte autoMove() {
        Random rand = new Random();
        byte c = (byte) rand.nextInt(this.numberOfColors);
        if (this.numberOfPlayers != 2) {
            while (true) {
                if (getPlayerColor(0) != c && getPlayerColor(1) != c && getPlayerColor(2) != c && getPlayerColor(3) != c) {
                    break;
                }
                c = (byte) rand.nextInt(this.numberOfColors);
            }
        } else {
            while (true) {
                if (getPlayerColor(0) != c && getPlayerColor(1) != c) {
                    break;
                }
                c = (byte) rand.nextInt(this.numberOfColors);
            }
        }
        return c;
    }

    public void StartBonusMixer() {
        byte[] colors = new byte[this.numberOfPlayers];
        for (int i = 0; i < this.numberOfPlayers; i++) {
            colors[i] = getPlayerColor(i);
        }
        Random rand = new Random();
        for (int x = 0; x < this.sizeFieldX; x++) {
            for (int y = 0; y < this.sizeFieldY; y++) {
                this.cell[x][y] = (byte) rand.nextInt(this.numberOfColors);
            }
        }
        for (int i2 = 0; i2 < this.numberOfPlayers; i2++) {
            for (int x2 = 0; x2 < this.sizeFieldX; x2++) {
                for (int y2 = 0; y2 < this.sizeFieldY; y2++) {
                    if (this.playerField[x2][y2][i2] == 3) {
                        this.cell[x2][y2] = colors[i2];
                    }
                }
            }
        }
        InputStream in = Gdx.files.internal("data/Levels/" + this.numberOfLvl).read();
        try {
            this.nBlackCells = in.read();
            for (int i3 = 0; i3 < this.nBlackCells; i3++) {
                this.cell[in.read()][in.read()] = 7;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            in.close();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        this.temp = 0;
        for (int i4 = 0; i4 < this.numberOfPlayers; i4++) {
            for (int x3 = 0; x3 < this.sizeFieldX; x3++) {
                for (int y3 = 0; y3 < this.sizeFieldY; y3++) {
                    if (this.playerField[x3][y3][i4] == 3 || this.cellTemp[x3][y3] == 7) {
                        this.cellTemp[x3][y3] = 7;
                    } else {
                        this.cellTemp[x3][y3] = this.cell[x3][y3];
                    }
                }
            }
        }
        for (int x4 = 0; x4 < this.sizeFieldX; x4++) {
            for (int y4 = 0; y4 < this.sizeFieldY; y4++) {
                this.cellTemp2[x4][y4] = 7;
                if (this.cellTemp[x4][y4] != 7) {
                    this.temp++;
                }
            }
        }
        for (int i5 = 0; i5 < this.numberOfPlayers; i5++) {
            updatePlayerField(i5, getPlayerColor(i5));
        }
    }

    public void StartBonusTeleport(int player) {
        boolean isTeleported;
        byte newX;
        byte newY;
        Random rand = new Random();
        do {
            isTeleported = true;
            newX = (byte) rand.nextInt(this.sizeFieldX);
            newY = (byte) rand.nextInt(this.sizeFieldY);
            if ((newX >= 8 || newY >= 8) && ((newX >= 8 || newY < this.sizeFieldY - 8) && ((newX < this.sizeFieldX - 8 || newY >= 8) && (newX < this.sizeFieldX - 8 || newY < this.sizeFieldY - 8)))) {
                for (int i = 0; i < this.numberOfPlayers; i++) {
                    if (this.playerField[newX][newY][i] == 3) {
                        isTeleported = false;
                    }
                }
                if (this.cell[newX][newY] == 7) {
                    isTeleported = false;
                    continue;
                } else {
                    continue;
                }
            } else {
                isTeleported = false;
                continue;
            }
        } while (!isTeleported);
        this.playerField[newX][newY][player] = 3;
        this.cell[newX][newY] = getPlayerColor(player);
        updatePlayerField(player, getPlayerColor(player));
        this.teleportX = newX;
        this.teleportY = newY;
        this.isAnybodyTeleported[player] = true;
        this.AnybodyTeleportedX[player] = this.teleportX;
        this.AnybodyTeleportedY[player] = this.teleportY;
        this.stepAfterTeleported[player] = 0;
    }

    public void CalcBlowingTerrain(int x, int y) {
        for (int xx = 0; xx < this.sizeFieldX; xx++) {
            for (int yy = 0; yy < this.sizeFieldY; yy++) {
                this.cellTemp[xx][yy] = Keyboard.STATE_NUMBER;
            }
        }
        int[] q = new int[(5 + 1)];
        for (int i = 0; i <= 5; i++) {
            q[i] = (int) Math.round(Math.sqrt((double) ((5 * 5) - (i * i))));
        }
        for (int player = 0; player < this.numberOfPlayers; player++) {
            for (int i2 = -5; i2 <= 5; i2++) {
                for (int u = 0; u <= q[Math.abs(i2)]; u++) {
                    if (x + i2 < this.sizeFieldX && x + i2 >= 0 && y + u < this.sizeFieldY && y + u >= 0) {
                        this.cellTemp[x + i2][y + u] = 7;
                    }
                    if (x + i2 < this.sizeFieldX && x + i2 >= 0 && y - u >= 0 && y - u < this.sizeFieldY) {
                        this.cellTemp[x + i2][y - u] = 7;
                    }
                    if (x + u < this.sizeFieldX && x + u >= 0 && y + i2 < this.sizeFieldY && y + i2 >= 0) {
                        this.cellTemp[x + u][y + i2] = 7;
                    }
                    if (x - u >= 0 && x - u < this.sizeFieldX && y + i2 < this.sizeFieldY && y + i2 >= 0) {
                        this.cellTemp[x - u][y + i2] = 7;
                    }
                }
            }
        }
    }

    public void StartBonusBomb(int x, int y, int zone) {
        for (int i = 0; i < this.numberOfPlayers; i++) {
            this.playerColors[i] = getPlayerColor(i);
        }
        for (int xx = 0; xx < this.sizeFieldX; xx++) {
            for (int yy = 0; yy < this.sizeFieldY; yy++) {
                this.cellTemp[xx][yy] = Keyboard.STATE_NUMBER;
                this.cellTemp2[xx][yy] = 0;
            }
        }
        for (int i2 = 0; i2 < 7; i2++) {
            this.blownColors[i2] = 0;
        }
        int[] q = new int[(zone + 1)];
        for (int i3 = 0; i3 <= zone; i3++) {
            q[i3] = (int) Math.round(Math.sqrt((double) ((zone * zone) - (i3 * i3))));
        }
        Random rand = new Random();
        for (int player = 0; player < this.numberOfPlayers; player++) {
            for (int i4 = -zone; i4 <= zone; i4++) {
                for (int u = 0; u <= q[Math.abs(i4)]; u++) {
                    if (x + i4 < this.sizeFieldX && x + i4 >= 0 && y + u < this.sizeFieldY && y + u >= 0) {
                        this.playerField[x + i4][y + u][player] = 0;
                        if (this.cell[x + i4][y + u] != 7) {
                            int[] iArr = this.blownColors;
                            byte b = this.cell[x + i4][y + u];
                            iArr[b] = iArr[b] + 1;
                        }
                        this.cell[x + i4][y + u] = 7;
                        this.cellTemp[x + i4][y + u] = (byte) rand.nextInt(this.numberOfColors);
                    }
                    if (x + i4 < this.sizeFieldX && x + i4 >= 0 && y - u >= 0 && y - u < this.sizeFieldY) {
                        this.playerField[x + i4][y - u][player] = 0;
                        if (this.cell[x + i4][y - u] != 7) {
                            int[] iArr2 = this.blownColors;
                            byte b2 = this.cell[x + i4][y - u];
                            iArr2[b2] = iArr2[b2] + 1;
                        }
                        this.cell[x + i4][y - u] = 7;
                        this.cellTemp[x + i4][y - u] = (byte) rand.nextInt(this.numberOfColors);
                    }
                    if (x + u < this.sizeFieldX && x + u >= 0 && y + i4 < this.sizeFieldY && y + i4 >= 0) {
                        this.playerField[x + u][y + i4][player] = 0;
                        if (this.cell[x + u][y + i4] != 7) {
                            int[] iArr3 = this.blownColors;
                            byte b3 = this.cell[x + u][y + i4];
                            iArr3[b3] = iArr3[b3] + 1;
                        }
                        this.cell[x + u][y + i4] = 7;
                        this.cellTemp[x + u][y + i4] = (byte) rand.nextInt(this.numberOfColors);
                    }
                    if (x - u >= 0 && x - u < this.sizeFieldX && y + i4 < this.sizeFieldY && y + i4 >= 0) {
                        this.playerField[x - u][y + i4][player] = 0;
                        if (this.cell[x - u][y + i4] != 7) {
                            int[] iArr4 = this.blownColors;
                            byte b4 = this.cell[x - u][y + i4];
                            iArr4[b4] = iArr4[b4] + 1;
                        }
                        this.cell[x - u][y + i4] = 7;
                        this.cellTemp[x - u][y + i4] = (byte) rand.nextInt(this.numberOfColors);
                    }
                }
            }
        }
        for (int i5 = 0; i5 < this.numberOfPlayers; i5++) {
            setPlayerColor(i5, this.playerColors[i5]);
        }
    }

    public void EndBonusBomb() {
        InputStream in = Gdx.files.internal("data/Levels/" + this.numberOfLvl).read();
        try {
            this.nBlackCells = in.read();
            for (int i = 0; i < this.nBlackCells; i++) {
                this.cell[in.read()][in.read()] = 7;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            in.close();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        for (int i2 = 0; i2 < this.numberOfPlayers; i2++) {
            setPlayerColor(i2, this.playerColors[i2]);
        }
        this.playerField[0][this.sizeFieldY - 1][0] = 3;
        if (this.numberOfPlayers == 2) {
            this.playerField[this.sizeFieldX - 1][0][1] = 3;
        } else {
            this.playerField[this.sizeFieldX - 1][this.sizeFieldY - 1][1] = 3;
            this.playerField[this.sizeFieldX - 1][0][2] = 3;
            this.playerField[0][0][3] = 3;
        }
        for (int i3 = 0; i3 < this.numberOfPlayers; i3++) {
            updatePlayerField(i3, getPlayerColor(i3));
        }
    }

    public int UseBonusAI(byte player, boolean isBlind) {
        if (isBlind) {
            if (States.playerBonus[player][2] > 0) {
                return SolveAIMixerWhileBlind(player);
            }
            if (States.playerBonus[player][3] <= 0 || SolveAITeleport(player) == 100) {
                return 100;
            }
            return 3;
        } else if (States.playerBonus[player][4] > 0 && SolveAIBomb(player) != 100) {
            return 4;
        } else {
            if (States.playerBonus[player][3] > 0 && SolveAITeleport(player) != 100) {
                return 3;
            }
            if (States.playerBonus[player][1] > 0 && SolveAIFreeze(player) != 100) {
                return 1;
            }
            if (States.playerBonus[player][5] > 0 && SolveAIBlind(player) != 100) {
                return 5;
            }
            if (States.playerBonus[player][2] <= 0 || SolveAIMixer(player) == 100) {
                return 100;
            }
            return 2;
        }
    }

    private int SolveAIMixerWhileBlind(byte player) {
        if (player == this.playerWhichBlind || this.isMixerOnBlind) {
            return 100;
        }
        if (States.skillAI[player] == 1) {
            return 100;
        }
        byte m = (byte) new Random().nextInt(100);
        if (States.skillAI[player] == 2 && m > 10) {
            return 100;
        }
        if (States.skillAI[player] == 3 && m > 20) {
            return 100;
        }
        this.isMixerOnBlind = true;
        return 2;
    }

    private int SolveAIBlind(byte player) {
        byte m = (byte) new Random().nextInt(100);
        if (States.skillAI[player] == 1 && m > 10) {
            return 100;
        }
        if (States.skillAI[player] == 2 && m > 30) {
            return 100;
        }
        int remainingCells = 0;
        for (byte i = 0; i < this.numberOfPlayers; i = (byte) (i + 1)) {
            remainingCells += this.numberOfCells[i];
        }
        if (remainingCells < this.maxPoints / 3) {
            return 100;
        }
        if (remainingCells > (this.maxPoints * 6) / 7) {
            return 100;
        }
        int[] cells = new int[this.numberOfPlayers];
        int max = 0;
        for (byte i2 = 0; i2 < this.numberOfPlayers; i2 = (byte) (i2 + 1)) {
            cells[i2] = FindMaxCells(i2);
            if (max < cells[i2]) {
                max = cells[i2];
            }
        }
        if (max > cells[player] + 1) {
            return 100;
        }
        if (States.skillAI[player] == 1 && cells[player] < 10) {
            return 100;
        }
        if (States.skillAI[player] == 2 && cells[player] < 13) {
            return 100;
        }
        if (States.skillAI[player] == 3 && cells[player] < 17) {
            return 100;
        }
        int max2 = 0;
        for (byte i3 = 0; i3 < this.numberOfPlayers; i3 = (byte) (i3 + 1)) {
            if (i3 != player) {
                cells[i3] = States.playerOverallScore[i3] + this.numberOfCells[i3];
                if (max2 < cells[i3]) {
                    max2 = cells[i3];
                }
            }
        }
        if (max2 < (States.playerOverallScore[player] + this.numberOfCells[player]) - 100) {
            return 100;
        }
        return 5;
    }

    /* JADX WARN: Type inference failed for: r0v17, types: [int] */
    /* JADX WARN: Type inference failed for: r0v91, types: [int] */
    /* JADX WARN: Type inference failed for: r0v103, types: [int] */
    /* JADX WARN: Type inference failed for: r0v115, types: [int] */
    /* JADX WARN: Type inference failed for: r0v127, types: [int] */
    /* JADX WARN: Type inference failed for: r0v150, types: [int] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int SolveAIBomb(byte r34) {
        /*
            r33 = this;
            java.util.Random r19 = new java.util.Random
            r19.<init>()
            r30 = 100
            r0 = r19
            r1 = r30
            int r30 = r0.nextInt(r1)
            r0 = r30
            byte r0 = (byte) r0
            r11 = r0
            int[] r30 = com.machaon.quadratum.States.skillAI
            r30 = r30[r34]
            r31 = 1
            r0 = r30
            r1 = r31
            if (r0 != r1) goto L_0x0029
            r30 = 3
            r0 = r11
            r1 = r30
            if (r0 <= r1) goto L_0x0029
            r30 = 100
        L_0x0028:
            return r30
        L_0x0029:
            int[] r30 = com.machaon.quadratum.States.skillAI
            r30 = r30[r34]
            r31 = 2
            r0 = r30
            r1 = r31
            if (r0 != r1) goto L_0x003f
            r30 = 30
            r0 = r11
            r1 = r30
            if (r0 <= r1) goto L_0x003f
            r30 = 100
            goto L_0x0028
        L_0x003f:
            int[] r30 = com.machaon.quadratum.States.skillAI
            r30 = r30[r34]
            r31 = 2
            r0 = r30
            r1 = r31
            if (r0 != r1) goto L_0x0055
            r30 = 10
            r0 = r11
            r1 = r30
            if (r0 <= r1) goto L_0x0055
            r30 = 100
            goto L_0x0028
        L_0x0055:
            r20 = 0
            r21 = 0
        L_0x0059:
            r0 = r33
            int r0 = r0.numberOfPlayers
            r30 = r0
            r0 = r21
            r1 = r30
            if (r0 < r1) goto L_0x00b4
            r0 = r33
            int r0 = r0.maxPoints
            r30 = r0
            int r30 = r30 * 1
            int r30 = r30 / 9
            r0 = r20
            r1 = r30
            if (r0 >= r1) goto L_0x0081
            r9 = 0
        L_0x0076:
            r0 = r33
            int r0 = r0.numberOfPlayers
            r30 = r0
            r0 = r9
            r1 = r30
            if (r0 < r1) goto L_0x00c6
        L_0x0081:
            r12 = 0
            r0 = r33
            int r0 = r0.numberOfPlayers
            r30 = r0
            r0 = r30
            int[] r0 = new int[r0]
            r7 = r0
            r9 = 0
        L_0x008e:
            r0 = r33
            int r0 = r0.numberOfPlayers
            r30 = r0
            r0 = r9
            r1 = r30
            if (r0 < r1) goto L_0x015f
            int[] r30 = com.machaon.quadratum.States.playerOverallScore
            r30 = r30[r34]
            r0 = r33
            int[] r0 = r0.numberOfCells
            r31 = r0
            r31 = r31[r34]
            r32 = 140(0x8c, float:1.96E-43)
            int r31 = r31 - r32
            int r30 = r30 + r31
            r0 = r12
            r1 = r30
            if (r0 >= r1) goto L_0x0185
            r30 = 100
            goto L_0x0028
        L_0x00b4:
            r0 = r33
            int[] r0 = r0.numberOfCells
            r30 = r0
            r30 = r30[r21]
            int r20 = r20 + r30
            int r30 = r21 + 1
            r0 = r30
            byte r0 = (byte) r0
            r21 = r0
            goto L_0x0059
        L_0x00c6:
            r0 = r9
            r1 = r34
            if (r0 == r1) goto L_0x0157
            r0 = r33
            boolean[] r0 = r0.isAnybodyTeleported
            r30 = r0
            boolean r30 = r30[r9]
            if (r30 == 0) goto L_0x0157
            int[] r30 = com.machaon.quadratum.States.skillAI
            r30 = r30[r34]
            r31 = 1
            r0 = r30
            r1 = r31
            if (r0 == r1) goto L_0x0157
            r0 = r33
            byte[] r0 = r0.AnybodyTeleportedX
            r30 = r0
            byte r30 = r30[r9]
            r0 = r33
            byte[] r0 = r0.AnybodyTeleportedY
            r31 = r0
            byte r31 = r31[r9]
            r0 = r33
            r1 = r34
            r2 = r30
            r3 = r31
            boolean r30 = r0.isEnemyCloser(r1, r2, r3)
            if (r30 == 0) goto L_0x0157
            r0 = r33
            byte[][][] r0 = r0.playerField
            r30 = r0
            r0 = r33
            byte[] r0 = r0.AnybodyTeleportedX
            r31 = r0
            byte r31 = r31[r9]
            r30 = r30[r31]
            r0 = r33
            byte[] r0 = r0.AnybodyTeleportedY
            r31 = r0
            byte r31 = r31[r9]
            r30 = r30[r31]
            byte r30 = r30[r9]
            r31 = 3
            r0 = r30
            r1 = r31
            if (r0 != r1) goto L_0x014d
            r0 = r33
            byte[] r0 = r0.AnybodyTeleportedX
            r30 = r0
            byte r30 = r30[r9]
            r0 = r30
            r1 = r33
            r1.bombAIX = r0
            r0 = r33
            byte[] r0 = r0.AnybodyTeleportedY
            r30 = r0
            byte r30 = r30[r9]
            r0 = r30
            r1 = r33
            r1.bombAIY = r0
            r0 = r33
            boolean[] r0 = r0.isAnybodyTeleported
            r30 = r0
            r31 = 0
            r30[r9] = r31
            r30 = 4
            goto L_0x0028
        L_0x014d:
            r0 = r33
            boolean[] r0 = r0.isAnybodyTeleported
            r30 = r0
            r31 = 0
            r30[r9] = r31
        L_0x0157:
            int r30 = r9 + 1
            r0 = r30
            byte r0 = (byte) r0
            r9 = r0
            goto L_0x0076
        L_0x015f:
            r0 = r9
            r1 = r34
            if (r0 == r1) goto L_0x017d
            int[] r30 = com.machaon.quadratum.States.playerOverallScore
            r30 = r30[r9]
            r0 = r33
            int[] r0 = r0.numberOfCells
            r31 = r0
            r31 = r31[r9]
            int r30 = r30 + r31
            r7[r9] = r30
            r30 = r7[r9]
            r0 = r12
            r1 = r30
            if (r0 >= r1) goto L_0x017d
            r12 = r7[r9]
        L_0x017d:
            int r30 = r9 + 1
            r0 = r30
            byte r0 = (byte) r0
            r9 = r0
            goto L_0x008e
        L_0x0185:
            r30 = 100
            r0 = r12
            r1 = r30
            if (r0 >= r1) goto L_0x019c
            int[] r30 = com.machaon.quadratum.States.skillAI
            r30 = r30[r34]
            r31 = 3
            r0 = r30
            r1 = r31
            if (r0 != r1) goto L_0x019c
            r30 = 100
            goto L_0x0028
        L_0x019c:
            r0 = r33
            int r0 = r0.sizeFieldX
            r30 = r0
            r0 = r33
            int r0 = r0.sizeFieldY
            r31 = r0
            int[] r30 = new int[]{r30, r31}
            java.lang.Class r31 = java.lang.Byte.TYPE
            r0 = r31
            r1 = r30
            java.lang.Object r23 = java.lang.reflect.Array.newInstance(r0, r1)
            byte[][] r23 = (byte[][]) r23
            r15 = 400(0x190, float:5.6E-43)
            int[] r30 = com.machaon.quadratum.States.skillAI
            r30 = r30[r34]
            r31 = 1
            r0 = r30
            r1 = r31
            if (r0 != r1) goto L_0x01c8
            r15 = 80
        L_0x01c8:
            r0 = r15
            int[] r0 = new int[r0]
            r25 = r0
            r0 = r15
            int[] r0 = new int[r0]
            r26 = r0
            r24 = 5
            int[] r8 = new int[r15]
            r0 = r15
            int[] r0 = new int[r0]
            r16 = r0
            int[] r14 = new int[r15]
            int r30 = r24 + 1
            r0 = r30
            int[] r0 = new int[r0]
            r18 = r0
            r9 = 0
        L_0x01e6:
            r0 = r9
            r1 = r24
            if (r0 <= r1) goto L_0x0222
            r5 = 1
            r4 = 3
            r6 = 5
            r22 = 0
        L_0x01f0:
            r0 = r22
            r1 = r15
            if (r0 < r1) goto L_0x023f
            r12 = 0
            r13 = 50
            int[] r30 = com.machaon.quadratum.States.skillAI
            r30 = r30[r34]
            r31 = 1
            r0 = r30
            r1 = r31
            if (r0 != r1) goto L_0x0206
            r13 = 30
        L_0x0206:
            int[] r30 = com.machaon.quadratum.States.skillAI
            r30 = r30[r34]
            r31 = 1
            r0 = r30
            r1 = r31
            if (r0 != r1) goto L_0x0214
            r13 = 40
        L_0x0214:
            r10 = 0
            r22 = 0
        L_0x0217:
            r0 = r22
            r1 = r15
            if (r0 < r1) goto L_0x053a
            if (r10 != 0) goto L_0x057d
            r30 = 100
            goto L_0x0028
        L_0x0222:
            int r30 = r24 * r24
            int r31 = r9 * r9
            int r30 = r30 - r31
            r0 = r30
            double r0 = (double) r0
            r30 = r0
            double r30 = java.lang.Math.sqrt(r30)
            long r30 = java.lang.Math.round(r30)
            r0 = r30
            int r0 = (int) r0
            r30 = r0
            r18[r9] = r30
            int r9 = r9 + 1
            goto L_0x01e6
        L_0x023f:
            r0 = r33
            int r0 = r0.sizeFieldX
            r30 = r0
            r0 = r19
            r1 = r30
            int r30 = r0.nextInt(r1)
            r25[r22] = r30
            r0 = r33
            int r0 = r0.sizeFieldY
            r30 = r0
            r0 = r19
            r1 = r30
            int r30 = r0.nextInt(r1)
            r26[r22] = r30
            r28 = 0
        L_0x0261:
            r0 = r33
            int r0 = r0.sizeFieldX
            r30 = r0
            r0 = r28
            r1 = r30
            if (r0 < r1) goto L_0x0288
            r0 = r24
            int r0 = -r0
            r9 = r0
        L_0x0271:
            r0 = r9
            r1 = r24
            if (r0 <= r1) goto L_0x02a2
            r28 = 0
        L_0x0278:
            r0 = r33
            int r0 = r0.sizeFieldX
            r30 = r0
            r0 = r28
            r1 = r30
            if (r0 < r1) goto L_0x04ff
            int r22 = r22 + 1
            goto L_0x01f0
        L_0x0288:
            r29 = 0
        L_0x028a:
            r0 = r33
            int r0 = r0.sizeFieldY
            r30 = r0
            r0 = r29
            r1 = r30
            if (r0 < r1) goto L_0x0299
            int r28 = r28 + 1
            goto L_0x0261
        L_0x0299:
            r30 = r23[r28]
            r31 = 0
            r30[r29] = r31
            int r29 = r29 + 1
            goto L_0x028a
        L_0x02a2:
            r27 = 0
        L_0x02a4:
            int r30 = java.lang.Math.abs(r9)
            r30 = r18[r30]
            r0 = r27
            r1 = r30
            if (r0 <= r1) goto L_0x02b3
            int r9 = r9 + 1
            goto L_0x0271
        L_0x02b3:
            r30 = r25[r22]
            int r30 = r30 + r9
            r0 = r33
            int r0 = r0.sizeFieldX
            r31 = r0
            r0 = r30
            r1 = r31
            if (r0 >= r1) goto L_0x0309
            r30 = r25[r22]
            int r30 = r30 + r9
            if (r30 < 0) goto L_0x0309
            r30 = r26[r22]
            int r30 = r30 + r27
            r0 = r33
            int r0 = r0.sizeFieldY
            r31 = r0
            r0 = r30
            r1 = r31
            if (r0 >= r1) goto L_0x0309
            r30 = r26[r22]
            int r30 = r30 + r27
            if (r30 < 0) goto L_0x0309
            r30 = r25[r22]
            int r30 = r30 + r9
            r30 = r23[r30]
            r31 = r26[r22]
            int r31 = r31 + r27
            byte r30 = r30[r31]
            if (r30 != 0) goto L_0x0309
            r30 = r25[r22]
            int r30 = r30 + r9
            r30 = r23[r30]
            r31 = r26[r22]
            int r31 = r31 + r27
            r32 = 1
            r30[r31] = r32
            r17 = 0
        L_0x02fd:
            r0 = r33
            int r0 = r0.numberOfPlayers
            r30 = r0
            r0 = r17
            r1 = r30
            if (r0 < r1) goto L_0x040f
        L_0x0309:
            r30 = r25[r22]
            int r30 = r30 + r9
            r0 = r33
            int r0 = r0.sizeFieldX
            r31 = r0
            r0 = r30
            r1 = r31
            if (r0 >= r1) goto L_0x035f
            r30 = r25[r22]
            int r30 = r30 + r9
            if (r30 < 0) goto L_0x035f
            r30 = r26[r22]
            int r30 = r30 - r27
            if (r30 < 0) goto L_0x035f
            r30 = r26[r22]
            int r30 = r30 - r27
            r0 = r33
            int r0 = r0.sizeFieldY
            r31 = r0
            r0 = r30
            r1 = r31
            if (r0 >= r1) goto L_0x035f
            r30 = r25[r22]
            int r30 = r30 + r9
            r30 = r23[r30]
            r31 = r26[r22]
            int r31 = r31 - r27
            byte r30 = r30[r31]
            if (r30 != 0) goto L_0x035f
            r30 = r25[r22]
            int r30 = r30 + r9
            r30 = r23[r30]
            r31 = r26[r22]
            int r31 = r31 - r27
            r32 = 1
            r30[r31] = r32
            r17 = 0
        L_0x0353:
            r0 = r33
            int r0 = r0.numberOfPlayers
            r30 = r0
            r0 = r17
            r1 = r30
            if (r0 < r1) goto L_0x044b
        L_0x035f:
            r30 = r25[r22]
            int r30 = r30 + r27
            r0 = r33
            int r0 = r0.sizeFieldX
            r31 = r0
            r0 = r30
            r1 = r31
            if (r0 >= r1) goto L_0x03b5
            r30 = r25[r22]
            int r30 = r30 + r27
            if (r30 < 0) goto L_0x03b5
            r30 = r26[r22]
            int r30 = r30 + r9
            r0 = r33
            int r0 = r0.sizeFieldY
            r31 = r0
            r0 = r30
            r1 = r31
            if (r0 >= r1) goto L_0x03b5
            r30 = r26[r22]
            int r30 = r30 + r9
            if (r30 < 0) goto L_0x03b5
            r30 = r25[r22]
            int r30 = r30 + r27
            r30 = r23[r30]
            r31 = r26[r22]
            int r31 = r31 + r9
            byte r30 = r30[r31]
            if (r30 != 0) goto L_0x03b5
            r30 = r25[r22]
            int r30 = r30 + r27
            r30 = r23[r30]
            r31 = r26[r22]
            int r31 = r31 + r9
            r32 = 1
            r30[r31] = r32
            r17 = 0
        L_0x03a9:
            r0 = r33
            int r0 = r0.numberOfPlayers
            r30 = r0
            r0 = r17
            r1 = r30
            if (r0 < r1) goto L_0x0487
        L_0x03b5:
            r30 = r25[r22]
            int r30 = r30 - r27
            if (r30 < 0) goto L_0x040b
            r30 = r25[r22]
            int r30 = r30 - r27
            r0 = r33
            int r0 = r0.sizeFieldX
            r31 = r0
            r0 = r30
            r1 = r31
            if (r0 >= r1) goto L_0x040b
            r30 = r26[r22]
            int r30 = r30 + r9
            r0 = r33
            int r0 = r0.sizeFieldY
            r31 = r0
            r0 = r30
            r1 = r31
            if (r0 >= r1) goto L_0x040b
            r30 = r26[r22]
            int r30 = r30 + r9
            if (r30 < 0) goto L_0x040b
            r30 = r25[r22]
            int r30 = r30 - r27
            r30 = r23[r30]
            r31 = r26[r22]
            int r31 = r31 + r9
            byte r30 = r30[r31]
            if (r30 != 0) goto L_0x040b
            r30 = r25[r22]
            int r30 = r30 - r27
            r30 = r23[r30]
            r31 = r26[r22]
            int r31 = r31 + r9
            r32 = 1
            r30[r31] = r32
            r17 = 0
        L_0x03ff:
            r0 = r33
            int r0 = r0.numberOfPlayers
            r30 = r0
            r0 = r17
            r1 = r30
            if (r0 < r1) goto L_0x04c3
        L_0x040b:
            int r27 = r27 + 1
            goto L_0x02a4
        L_0x040f:
            r0 = r33
            byte[][][] r0 = r0.playerField
            r30 = r0
            r31 = r25[r22]
            int r31 = r31 + r9
            r30 = r30[r31]
            r31 = r26[r22]
            int r31 = r31 + r27
            r30 = r30[r31]
            byte r30 = r30[r17]
            r31 = 3
            r0 = r30
            r1 = r31
            if (r0 != r1) goto L_0x043f
            r30 = r25[r22]
            int r30 = r30 + r9
            r30 = r23[r30]
            r31 = r26[r22]
            int r31 = r31 + r27
            r0 = r17
            r1 = r34
            if (r0 == r1) goto L_0x0448
            r32 = 3
        L_0x043d:
            r30[r31] = r32
        L_0x043f:
            int r30 = r17 + 1
            r0 = r30
            byte r0 = (byte) r0
            r17 = r0
            goto L_0x02fd
        L_0x0448:
            r32 = 5
            goto L_0x043d
        L_0x044b:
            r0 = r33
            byte[][][] r0 = r0.playerField
            r30 = r0
            r31 = r25[r22]
            int r31 = r31 + r9
            r30 = r30[r31]
            r31 = r26[r22]
            int r31 = r31 - r27
            r30 = r30[r31]
            byte r30 = r30[r17]
            r31 = 3
            r0 = r30
            r1 = r31
            if (r0 != r1) goto L_0x047b
            r30 = r25[r22]
            int r30 = r30 + r9
            r30 = r23[r30]
            r31 = r26[r22]
            int r31 = r31 - r27
            r0 = r17
            r1 = r34
            if (r0 == r1) goto L_0x0484
            r32 = 3
        L_0x0479:
            r30[r31] = r32
        L_0x047b:
            int r30 = r17 + 1
            r0 = r30
            byte r0 = (byte) r0
            r17 = r0
            goto L_0x0353
        L_0x0484:
            r32 = 5
            goto L_0x0479
        L_0x0487:
            r0 = r33
            byte[][][] r0 = r0.playerField
            r30 = r0
            r31 = r25[r22]
            int r31 = r31 + r27
            r30 = r30[r31]
            r31 = r26[r22]
            int r31 = r31 + r9
            r30 = r30[r31]
            byte r30 = r30[r17]
            r31 = 3
            r0 = r30
            r1 = r31
            if (r0 != r1) goto L_0x04b7
            r30 = r25[r22]
            int r30 = r30 + r27
            r30 = r23[r30]
            r31 = r26[r22]
            int r31 = r31 + r9
            r0 = r17
            r1 = r34
            if (r0 == r1) goto L_0x04c0
            r32 = 3
        L_0x04b5:
            r30[r31] = r32
        L_0x04b7:
            int r30 = r17 + 1
            r0 = r30
            byte r0 = (byte) r0
            r17 = r0
            goto L_0x03a9
        L_0x04c0:
            r32 = 5
            goto L_0x04b5
        L_0x04c3:
            r0 = r33
            byte[][][] r0 = r0.playerField
            r30 = r0
            r31 = r25[r22]
            int r31 = r31 - r27
            r30 = r30[r31]
            r31 = r26[r22]
            int r31 = r31 + r9
            r30 = r30[r31]
            byte r30 = r30[r17]
            r31 = 3
            r0 = r30
            r1 = r31
            if (r0 != r1) goto L_0x04f3
            r30 = r25[r22]
            int r30 = r30 - r27
            r30 = r23[r30]
            r31 = r26[r22]
            int r31 = r31 + r9
            r0 = r17
            r1 = r34
            if (r0 == r1) goto L_0x04fc
            r32 = 3
        L_0x04f1:
            r30[r31] = r32
        L_0x04f3:
            int r30 = r17 + 1
            r0 = r30
            byte r0 = (byte) r0
            r17 = r0
            goto L_0x03ff
        L_0x04fc:
            r32 = 5
            goto L_0x04f1
        L_0x04ff:
            r29 = 0
        L_0x0501:
            r0 = r33
            int r0 = r0.sizeFieldY
            r30 = r0
            r0 = r29
            r1 = r30
            if (r0 < r1) goto L_0x0516
            int r30 = r28 + 1
            r0 = r30
            byte r0 = (byte) r0
            r28 = r0
            goto L_0x0278
        L_0x0516:
            r30 = r23[r28]
            byte r30 = r30[r29]
            switch(r30) {
                case 1: goto L_0x0533;
                case 2: goto L_0x051d;
                case 3: goto L_0x0525;
                case 4: goto L_0x051d;
                case 5: goto L_0x052c;
                default: goto L_0x051d;
            }
        L_0x051d:
            int r30 = r29 + 1
            r0 = r30
            byte r0 = (byte) r0
            r29 = r0
            goto L_0x0501
        L_0x0525:
            r30 = r8[r22]
            int r30 = r30 + 1
            r8[r22] = r30
            goto L_0x051d
        L_0x052c:
            r30 = r16[r22]
            int r30 = r30 + 1
            r16[r22] = r30
            goto L_0x051d
        L_0x0533:
            r30 = r14[r22]
            int r30 = r30 + 1
            r14[r22] = r30
            goto L_0x051d
        L_0x053a:
            r30 = r16[r22]
            r31 = 3
            r0 = r30
            r1 = r31
            if (r0 <= r1) goto L_0x0579
            r30 = r16[r22]
            r31 = 8
            r0 = r30
            r1 = r31
            if (r0 >= r1) goto L_0x0579
            r30 = r8[r22]
            r0 = r30
            r1 = r13
            if (r0 <= r1) goto L_0x0579
            r30 = r8[r22]
            r0 = r12
            r1 = r30
            if (r0 >= r1) goto L_0x0579
            r12 = r8[r22]
            r30 = r25[r22]
            r0 = r30
            byte r0 = (byte) r0
            r30 = r0
            r0 = r30
            r1 = r33
            r1.bombAIX = r0
            r30 = r26[r22]
            r0 = r30
            byte r0 = (byte) r0
            r30 = r0
            r0 = r30
            r1 = r33
            r1.bombAIY = r0
            r10 = 1
        L_0x0579:
            int r22 = r22 + 1
            goto L_0x0217
        L_0x057d:
            r30 = 4
            goto L_0x0028
        */
        throw new UnsupportedOperationException("Method not decompiled: com.machaon.quadratum.parts.Field.SolveAIBomb(byte):int");
    }

    private boolean isEnemyCloser(byte player, byte x, byte y) {
        int minDeltaX = 40;
        int minDeltaY = 40;
        for (int xx = 0; xx < this.sizeFieldX; xx++) {
            for (int yy = 0; yy < this.sizeFieldY; yy++) {
                if (this.playerField[xx][yy][player] == 3) {
                    int deltaX = Math.abs(xx - x);
                    int deltaY = Math.abs(yy - y);
                    if (deltaX < minDeltaX && deltaY < minDeltaY) {
                        minDeltaX = deltaX;
                        minDeltaY = deltaY;
                    }
                }
            }
        }
        if (minDeltaX >= 17 || minDeltaY >= 12) {
            return false;
        }
        return true;
    }

    private int SolveAITeleport(byte player) {
        byte m = (byte) new Random().nextInt(100);
        if (States.skillAI[player] == 1 && m > 10) {
            return 100;
        }
        if (States.skillAI[player] == 2 && m > 40) {
            return 100;
        }
        if (States.skillAI[player] == 3 && m > 80) {
            return 100;
        }
        int remainingCells = 0;
        for (byte i = 0; i < this.numberOfPlayers; i = (byte) (i + 1)) {
            remainingCells += this.numberOfCells[i];
        }
        if (remainingCells > (this.maxPoints * 6) / 7) {
            return 100;
        }
        if (States.skillAI[player] == 1 && remainingCells < (this.maxPoints * 3) / 5) {
            return 100;
        }
        if (States.skillAI[player] == 2 && remainingCells < (this.maxPoints * 6) / 7) {
            return 100;
        }
        if (States.skillAI[player] != 3 || remainingCells <= (this.maxPoints * 2) / 3) {
            return 3;
        }
        return 100;
    }

    /* JADX WARN: Type inference failed for: r0v16, types: [int] */
    /* JADX WARN: Type inference failed for: r0v57, types: [int] */
    /* JADX WARN: Type inference failed for: r0v131, types: [int] */
    /* JADX WARN: Type inference failed for: r0v135, types: [int] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int SolveAIFreeze(byte r33) {
        /*
            r32 = this;
            java.util.Random r20 = new java.util.Random
            r20.<init>()
            r29 = 100
            r0 = r20
            r1 = r29
            int r29 = r0.nextInt(r1)
            r0 = r29
            byte r0 = (byte) r0
            r11 = r0
            int[] r29 = com.machaon.quadratum.States.skillAI
            r29 = r29[r33]
            r30 = 1
            r0 = r29
            r1 = r30
            if (r0 != r1) goto L_0x0029
            r29 = 40
            r0 = r11
            r1 = r29
            if (r0 <= r1) goto L_0x0029
            r29 = 100
        L_0x0028:
            return r29
        L_0x0029:
            int[] r29 = com.machaon.quadratum.States.skillAI
            r29 = r29[r33]
            r30 = 2
            r0 = r29
            r1 = r30
            if (r0 != r1) goto L_0x003f
            r29 = 60
            r0 = r11
            r1 = r29
            if (r0 <= r1) goto L_0x003f
            r29 = 100
            goto L_0x0028
        L_0x003f:
            r21 = 0
            r6 = 0
        L_0x0042:
            r0 = r32
            int r0 = r0.numberOfPlayers
            r29 = r0
            r0 = r6
            r1 = r29
            if (r0 < r1) goto L_0x005e
            r0 = r32
            int r0 = r0.maxPoints
            r29 = r0
            int r29 = r29 / 4
            r0 = r21
            r1 = r29
            if (r0 >= r1) goto L_0x006f
            r29 = 100
            goto L_0x0028
        L_0x005e:
            r0 = r32
            int[] r0 = r0.numberOfCells
            r29 = r0
            r29 = r29[r6]
            int r21 = r21 + r29
            int r29 = r6 + 1
            r0 = r29
            byte r0 = (byte) r0
            r6 = r0
            goto L_0x0042
        L_0x006f:
            r12 = 0
            r0 = r32
            int r0 = r0.numberOfPlayers
            r29 = r0
            r0 = r29
            int[] r0 = new int[r0]
            r5 = r0
            r6 = 0
        L_0x007c:
            r0 = r32
            int r0 = r0.numberOfPlayers
            r29 = r0
            r0 = r6
            r1 = r29
            if (r0 < r1) goto L_0x00a1
            int[] r29 = com.machaon.quadratum.States.playerOverallScore
            r29 = r29[r33]
            r0 = r32
            int[] r0 = r0.numberOfCells
            r30 = r0
            r30 = r30[r33]
            int r29 = r29 + r30
            r30 = 40
            int r29 = r29 - r30
            r0 = r12
            r1 = r29
            if (r0 >= r1) goto L_0x00c6
            r29 = 100
            goto L_0x0028
        L_0x00a1:
            r0 = r6
            r1 = r33
            if (r0 == r1) goto L_0x00bf
            int[] r29 = com.machaon.quadratum.States.playerOverallScore
            r29 = r29[r6]
            r0 = r32
            int[] r0 = r0.numberOfCells
            r30 = r0
            r30 = r30[r6]
            int r29 = r29 + r30
            r5[r6] = r29
            r29 = r5[r6]
            r0 = r12
            r1 = r29
            if (r0 >= r1) goto L_0x00bf
            r12 = r5[r6]
        L_0x00bf:
            int r29 = r6 + 1
            r0 = r29
            byte r0 = (byte) r0
            r6 = r0
            goto L_0x007c
        L_0x00c6:
            com.machaon.quadratum.parts.Table r24 = new com.machaon.quadratum.parts.Table
            r0 = r32
            int r0 = r0.numberOfColors
            r29 = r0
            r0 = r24
            r1 = r29
            r0.<init>(r1)
            r6 = 0
        L_0x00d6:
            r0 = r32
            int r0 = r0.numberOfColors
            r29 = r0
            r0 = r6
            r1 = r29
            if (r0 < r1) goto L_0x01a1
            r0 = r32
            int r0 = r0.sizeFieldX
            r29 = r0
            r0 = r32
            int r0 = r0.sizeFieldY
            r30 = r0
            int[] r29 = new int[]{r29, r30}
            java.lang.Class r30 = java.lang.Byte.TYPE
            r0 = r30
            r1 = r29
            java.lang.Object r2 = java.lang.reflect.Array.newInstance(r0, r1)
            byte[][] r2 = (byte[][]) r2
            r0 = r32
            int r0 = r0.sizeFieldX
            r29 = r0
            r0 = r32
            int r0 = r0.sizeFieldY
            r30 = r0
            int[] r29 = new int[]{r29, r30}
            java.lang.Class r30 = java.lang.Byte.TYPE
            r0 = r30
            r1 = r29
            java.lang.Object r17 = java.lang.reflect.Array.newInstance(r0, r1)
            byte[][] r17 = (byte[][]) r17
            r27 = 0
        L_0x011b:
            r0 = r32
            int r0 = r0.sizeFieldX
            r29 = r0
            r0 = r27
            r1 = r29
            if (r0 < r1) goto L_0x01b8
            r6 = 0
        L_0x0128:
            r0 = r32
            int r0 = r0.numberOfPlayers
            r29 = r0
            r0 = r6
            r1 = r29
            if (r0 < r1) goto L_0x01eb
            r0 = r32
            int r0 = r0.sizeFieldX
            r29 = r0
            r0 = r32
            int r0 = r0.sizeFieldY
            r30 = r0
            int[] r29 = new int[]{r29, r30}
            java.lang.Class r30 = java.lang.Byte.TYPE
            r0 = r30
            r1 = r29
            java.lang.Object r3 = java.lang.reflect.Array.newInstance(r0, r1)
            byte[][] r3 = (byte[][]) r3
            r0 = r32
            int r0 = r0.sizeFieldX
            r29 = r0
            r0 = r32
            int r0 = r0.sizeFieldY
            r30 = r0
            int[] r29 = new int[]{r29, r30}
            java.lang.Class r30 = java.lang.Byte.TYPE
            r0 = r30
            r1 = r29
            java.lang.Object r18 = java.lang.reflect.Array.newInstance(r0, r1)
            byte[][] r18 = (byte[][]) r18
            r27 = 0
        L_0x016d:
            r0 = r32
            int r0 = r0.sizeFieldX
            r29 = r0
            r0 = r27
            r1 = r29
            if (r0 < r1) goto L_0x0230
            r22 = 0
        L_0x017b:
            r0 = r32
            int r0 = r0.numberOfColors
            r29 = r0
            r0 = r22
            r1 = r29
            if (r0 < r1) goto L_0x0255
            r12 = 0
            r25 = 0
        L_0x018a:
            r0 = r32
            int r0 = r0.numberOfColors
            r29 = r0
            r0 = r25
            r1 = r29
            if (r0 < r1) goto L_0x0601
            r29 = 20
            r0 = r12
            r1 = r29
            if (r0 >= r1) goto L_0x0651
            r29 = 100
            goto L_0x0028
        L_0x01a1:
            r0 = r24
            com.machaon.quadratum.parts.Table[] r0 = r0.table
            r29 = r0
            com.machaon.quadratum.parts.Table r30 = new com.machaon.quadratum.parts.Table
            r0 = r32
            int r0 = r0.numberOfColors
            r31 = r0
            r30.<init>(r31)
            r29[r6] = r30
            int r6 = r6 + 1
            goto L_0x00d6
        L_0x01b8:
            r28 = 0
        L_0x01ba:
            r0 = r32
            int r0 = r0.sizeFieldY
            r29 = r0
            r0 = r28
            r1 = r29
            if (r0 < r1) goto L_0x01ca
            int r27 = r27 + 1
            goto L_0x011b
        L_0x01ca:
            r29 = r2[r27]
            r0 = r32
            byte[][] r0 = r0.cell
            r30 = r0
            r30 = r30[r27]
            byte r30 = r30[r28]
            r29[r28] = r30
            r29 = r17[r27]
            r0 = r32
            byte[][][] r0 = r0.playerField
            r30 = r0
            r30 = r30[r27]
            r30 = r30[r28]
            byte r30 = r30[r33]
            r29[r28] = r30
            int r28 = r28 + 1
            goto L_0x01ba
        L_0x01eb:
            r0 = r6
            r1 = r33
            if (r0 == r1) goto L_0x01fe
            r27 = 0
        L_0x01f2:
            r0 = r32
            int r0 = r0.sizeFieldX
            r29 = r0
            r0 = r27
            r1 = r29
            if (r0 < r1) goto L_0x0202
        L_0x01fe:
            int r6 = r6 + 1
            goto L_0x0128
        L_0x0202:
            r28 = 0
        L_0x0204:
            r0 = r32
            int r0 = r0.sizeFieldY
            r29 = r0
            r0 = r28
            r1 = r29
            if (r0 < r1) goto L_0x0213
            int r27 = r27 + 1
            goto L_0x01f2
        L_0x0213:
            r0 = r32
            byte[][][] r0 = r0.playerField
            r29 = r0
            r29 = r29[r27]
            r29 = r29[r28]
            byte r29 = r29[r6]
            r30 = 3
            r0 = r29
            r1 = r30
            if (r0 != r1) goto L_0x022d
            r29 = r2[r27]
            r30 = 20
            r29[r28] = r30
        L_0x022d:
            int r28 = r28 + 1
            goto L_0x0204
        L_0x0230:
            r28 = 0
        L_0x0232:
            r0 = r32
            int r0 = r0.sizeFieldY
            r29 = r0
            r0 = r28
            r1 = r29
            if (r0 < r1) goto L_0x0242
            int r27 = r27 + 1
            goto L_0x016d
        L_0x0242:
            r29 = r3[r27]
            r30 = r2[r27]
            byte r30 = r30[r28]
            r29[r28] = r30
            r29 = r18[r27]
            r30 = r17[r27]
            byte r30 = r30[r28]
            r29[r28] = r30
            int r28 = r28 + 1
            goto L_0x0232
        L_0x0255:
            r7 = 1
            r16 = 0
        L_0x0258:
            r0 = r32
            int r0 = r0.numberOfPlayers
            r29 = r0
            r0 = r16
            r1 = r29
            if (r0 < r1) goto L_0x0312
            if (r7 == 0) goto L_0x05f5
            r27 = 0
        L_0x0268:
            r0 = r32
            int r0 = r0.sizeFieldX
            r29 = r0
            r0 = r27
            r1 = r29
            if (r0 < r1) goto L_0x0325
            r27 = 0
        L_0x0276:
            r0 = r32
            int r0 = r0.sizeFieldX
            r29 = r0
            r0 = r27
            r1 = r29
            if (r0 < r1) goto L_0x034a
            r14 = 0
        L_0x0283:
            r9 = 1
            r27 = 0
        L_0x0286:
            r0 = r32
            int r0 = r0.sizeFieldX
            r29 = r0
            r0 = r27
            r1 = r29
            if (r0 < r1) goto L_0x0375
            if (r9 == 0) goto L_0x0283
            r0 = r24
            byte[] r0 = r0.t
            r29 = r0
            r0 = r32
            int[] r0 = r0.numberOfCells
            r30 = r0
            r30 = r30[r33]
            int r30 = r14 - r30
            r0 = r30
            byte r0 = (byte) r0
            r30 = r0
            r29[r22] = r30
            r0 = r24
            byte[] r0 = r0.t
            r29 = r0
            byte r29 = r29[r22]
            if (r29 <= 0) goto L_0x0309
            r0 = r32
            int r0 = r0.sizeFieldX
            r29 = r0
            r0 = r32
            int r0 = r0.sizeFieldY
            r30 = r0
            int[] r29 = new int[]{r29, r30}
            java.lang.Class r30 = java.lang.Byte.TYPE
            r0 = r30
            r1 = r29
            java.lang.Object r4 = java.lang.reflect.Array.newInstance(r0, r1)
            byte[][] r4 = (byte[][]) r4
            r0 = r32
            int r0 = r0.sizeFieldX
            r29 = r0
            r0 = r32
            int r0 = r0.sizeFieldY
            r30 = r0
            int[] r29 = new int[]{r29, r30}
            java.lang.Class r30 = java.lang.Byte.TYPE
            r0 = r30
            r1 = r29
            java.lang.Object r19 = java.lang.reflect.Array.newInstance(r0, r1)
            byte[][] r19 = (byte[][]) r19
            r27 = 0
        L_0x02ef:
            r0 = r32
            int r0 = r0.sizeFieldX
            r29 = r0
            r0 = r27
            r1 = r29
            if (r0 < r1) goto L_0x0428
            r23 = 0
        L_0x02fd:
            r0 = r32
            int r0 = r0.numberOfColors
            r29 = r0
            r0 = r23
            r1 = r29
            if (r0 < r1) goto L_0x044d
        L_0x0309:
            int r29 = r22 + 1
            r0 = r29
            byte r0 = (byte) r0
            r22 = r0
            goto L_0x017b
        L_0x0312:
            r0 = r32
            r1 = r16
            byte r29 = r0.getPlayerColor(r1)
            r0 = r22
            r1 = r29
            if (r0 != r1) goto L_0x0321
            r7 = 0
        L_0x0321:
            int r16 = r16 + 1
            goto L_0x0258
        L_0x0325:
            r28 = 0
        L_0x0327:
            r0 = r32
            int r0 = r0.sizeFieldY
            r29 = r0
            r0 = r28
            r1 = r29
            if (r0 < r1) goto L_0x0337
            int r27 = r27 + 1
            goto L_0x0268
        L_0x0337:
            r29 = r2[r27]
            r30 = r3[r27]
            byte r30 = r30[r28]
            r29[r28] = r30
            r29 = r17[r27]
            r30 = r18[r27]
            byte r30 = r30[r28]
            r29[r28] = r30
            int r28 = r28 + 1
            goto L_0x0327
        L_0x034a:
            r28 = 0
        L_0x034c:
            r0 = r32
            int r0 = r0.sizeFieldY
            r29 = r0
            r0 = r28
            r1 = r29
            if (r0 < r1) goto L_0x035c
            int r27 = r27 + 1
            goto L_0x0276
        L_0x035c:
            r29 = r17[r27]
            byte r29 = r29[r28]
            r30 = 3
            r0 = r29
            r1 = r30
            if (r0 != r1) goto L_0x0372
            r29 = r17[r27]
            r30 = 1
            r29[r28] = r30
            r29 = r2[r27]
            r29[r28] = r22
        L_0x0372:
            int r28 = r28 + 1
            goto L_0x034c
        L_0x0375:
            r28 = 0
        L_0x0377:
            r0 = r32
            int r0 = r0.sizeFieldY
            r29 = r0
            r0 = r28
            r1 = r29
            if (r0 < r1) goto L_0x0387
            int r27 = r27 + 1
            goto L_0x0286
        L_0x0387:
            r29 = r17[r27]
            byte r29 = r29[r28]
            r30 = 1
            r0 = r29
            r1 = r30
            if (r0 != r1) goto L_0x0424
            r9 = 0
            r29 = r17[r27]
            r30 = 3
            r29[r28] = r30
            int r14 = r14 + 1
            int r28 = r28 + 1
            r0 = r32
            int r0 = r0.sizeFieldY
            r29 = r0
            r0 = r28
            r1 = r29
            if (r0 >= r1) goto L_0x03c1
            r29 = r2[r27]
            byte r29 = r29[r28]
            r0 = r29
            r1 = r22
            if (r0 != r1) goto L_0x03c1
            r29 = r17[r27]
            byte r30 = r29[r28]
            r30 = r30 | 1
            r0 = r30
            byte r0 = (byte) r0
            r30 = r0
            r29[r28] = r30
        L_0x03c1:
            int r27 = r27 + 1
            int r28 = r28 + -1
            r0 = r32
            int r0 = r0.sizeFieldX
            r29 = r0
            r0 = r27
            r1 = r29
            if (r0 >= r1) goto L_0x03e8
            r29 = r2[r27]
            byte r29 = r29[r28]
            r0 = r29
            r1 = r22
            if (r0 != r1) goto L_0x03e8
            r29 = r17[r27]
            byte r30 = r29[r28]
            r30 = r30 | 1
            r0 = r30
            byte r0 = (byte) r0
            r30 = r0
            r29[r28] = r30
        L_0x03e8:
            int r27 = r27 + -1
            int r28 = r28 + -1
            if (r28 < 0) goto L_0x0405
            r29 = r2[r27]
            byte r29 = r29[r28]
            r0 = r29
            r1 = r22
            if (r0 != r1) goto L_0x0405
            r29 = r17[r27]
            byte r30 = r29[r28]
            r30 = r30 | 1
            r0 = r30
            byte r0 = (byte) r0
            r30 = r0
            r29[r28] = r30
        L_0x0405:
            int r27 = r27 + -1
            int r28 = r28 + 1
            if (r27 < 0) goto L_0x0422
            r29 = r2[r27]
            byte r29 = r29[r28]
            r0 = r29
            r1 = r22
            if (r0 != r1) goto L_0x0422
            r29 = r17[r27]
            byte r30 = r29[r28]
            r30 = r30 | 1
            r0 = r30
            byte r0 = (byte) r0
            r30 = r0
            r29[r28] = r30
        L_0x0422:
            int r27 = r27 + 1
        L_0x0424:
            int r28 = r28 + 1
            goto L_0x0377
        L_0x0428:
            r28 = 0
        L_0x042a:
            r0 = r32
            int r0 = r0.sizeFieldY
            r29 = r0
            r0 = r28
            r1 = r29
            if (r0 < r1) goto L_0x043a
            int r27 = r27 + 1
            goto L_0x02ef
        L_0x043a:
            r29 = r4[r27]
            r30 = r2[r27]
            byte r30 = r30[r28]
            r29[r28] = r30
            r29 = r19[r27]
            r30 = r17[r27]
            byte r30 = r30[r28]
            r29[r28] = r30
            int r28 = r28 + 1
            goto L_0x042a
        L_0x044d:
            r8 = 1
            r0 = r23
            r1 = r22
            if (r0 != r1) goto L_0x0455
            r8 = 0
        L_0x0455:
            r16 = 0
        L_0x0457:
            r0 = r32
            int r0 = r0.numberOfPlayers
            r29 = r0
            r0 = r16
            r1 = r29
            if (r0 < r1) goto L_0x04bb
            if (r8 == 0) goto L_0x05db
            r27 = 0
        L_0x0467:
            r0 = r32
            int r0 = r0.sizeFieldX
            r29 = r0
            r0 = r27
            r1 = r29
            if (r0 < r1) goto L_0x04d8
            r27 = 0
        L_0x0475:
            r0 = r32
            int r0 = r0.sizeFieldX
            r29 = r0
            r0 = r27
            r1 = r29
            if (r0 < r1) goto L_0x04fd
            r15 = 0
        L_0x0482:
            r10 = 1
            r27 = 0
        L_0x0485:
            r0 = r32
            int r0 = r0.sizeFieldX
            r29 = r0
            r0 = r27
            r1 = r29
            if (r0 < r1) goto L_0x0528
            if (r10 == 0) goto L_0x0482
            r0 = r24
            com.machaon.quadratum.parts.Table[] r0 = r0.table
            r29 = r0
            r29 = r29[r22]
            r0 = r29
            byte[] r0 = r0.t
            r29 = r0
            r0 = r32
            int[] r0 = r0.numberOfCells
            r30 = r0
            r30 = r30[r33]
            int r30 = r15 - r30
            r0 = r30
            byte r0 = (byte) r0
            r30 = r0
            r29[r23] = r30
        L_0x04b2:
            int r29 = r23 + 1
            r0 = r29
            byte r0 = (byte) r0
            r23 = r0
            goto L_0x02fd
        L_0x04bb:
            r0 = r16
            r1 = r33
            if (r0 == r1) goto L_0x04d0
            r0 = r32
            r1 = r16
            byte r29 = r0.getPlayerColor(r1)
            r0 = r23
            r1 = r29
            if (r0 != r1) goto L_0x04d0
            r8 = 0
        L_0x04d0:
            int r29 = r16 + 1
            r0 = r29
            byte r0 = (byte) r0
            r16 = r0
            goto L_0x0457
        L_0x04d8:
            r28 = 0
        L_0x04da:
            r0 = r32
            int r0 = r0.sizeFieldY
            r29 = r0
            r0 = r28
            r1 = r29
            if (r0 < r1) goto L_0x04ea
            int r27 = r27 + 1
            goto L_0x0467
        L_0x04ea:
            r29 = r2[r27]
            r30 = r4[r27]
            byte r30 = r30[r28]
            r29[r28] = r30
            r29 = r17[r27]
            r30 = r19[r27]
            byte r30 = r30[r28]
            r29[r28] = r30
            int r28 = r28 + 1
            goto L_0x04da
        L_0x04fd:
            r28 = 0
        L_0x04ff:
            r0 = r32
            int r0 = r0.sizeFieldY
            r29 = r0
            r0 = r28
            r1 = r29
            if (r0 < r1) goto L_0x050f
            int r27 = r27 + 1
            goto L_0x0475
        L_0x050f:
            r29 = r17[r27]
            byte r29 = r29[r28]
            r30 = 3
            r0 = r29
            r1 = r30
            if (r0 != r1) goto L_0x0525
            r29 = r17[r27]
            r30 = 1
            r29[r28] = r30
            r29 = r2[r27]
            r29[r28] = r23
        L_0x0525:
            int r28 = r28 + 1
            goto L_0x04ff
        L_0x0528:
            r28 = 0
        L_0x052a:
            r0 = r32
            int r0 = r0.sizeFieldY
            r29 = r0
            r0 = r28
            r1 = r29
            if (r0 < r1) goto L_0x053a
            int r27 = r27 + 1
            goto L_0x0485
        L_0x053a:
            r29 = r17[r27]
            byte r29 = r29[r28]
            r30 = 1
            r0 = r29
            r1 = r30
            if (r0 != r1) goto L_0x05d7
            r10 = 0
            r29 = r17[r27]
            r30 = 3
            r29[r28] = r30
            int r15 = r15 + 1
            int r28 = r28 + 1
            r0 = r32
            int r0 = r0.sizeFieldY
            r29 = r0
            r0 = r28
            r1 = r29
            if (r0 >= r1) goto L_0x0574
            r29 = r2[r27]
            byte r29 = r29[r28]
            r0 = r29
            r1 = r23
            if (r0 != r1) goto L_0x0574
            r29 = r17[r27]
            byte r30 = r29[r28]
            r30 = r30 | 1
            r0 = r30
            byte r0 = (byte) r0
            r30 = r0
            r29[r28] = r30
        L_0x0574:
            int r27 = r27 + 1
            int r28 = r28 + -1
            r0 = r32
            int r0 = r0.sizeFieldX
            r29 = r0
            r0 = r27
            r1 = r29
            if (r0 >= r1) goto L_0x059b
            r29 = r2[r27]
            byte r29 = r29[r28]
            r0 = r29
            r1 = r23
            if (r0 != r1) goto L_0x059b
            r29 = r17[r27]
            byte r30 = r29[r28]
            r30 = r30 | 1
            r0 = r30
            byte r0 = (byte) r0
            r30 = r0
            r29[r28] = r30
        L_0x059b:
            int r27 = r27 + -1
            int r28 = r28 + -1
            if (r28 < 0) goto L_0x05b8
            r29 = r2[r27]
            byte r29 = r29[r28]
            r0 = r29
            r1 = r23
            if (r0 != r1) goto L_0x05b8
            r29 = r17[r27]
            byte r30 = r29[r28]
            r30 = r30 | 1
            r0 = r30
            byte r0 = (byte) r0
            r30 = r0
            r29[r28] = r30
        L_0x05b8:
            int r27 = r27 + -1
            int r28 = r28 + 1
            if (r27 < 0) goto L_0x05d5
            r29 = r2[r27]
            byte r29 = r29[r28]
            r0 = r29
            r1 = r23
            if (r0 != r1) goto L_0x05d5
            r29 = r17[r27]
            byte r30 = r29[r28]
            r30 = r30 | 1
            r0 = r30
            byte r0 = (byte) r0
            r30 = r0
            r29[r28] = r30
        L_0x05d5:
            int r27 = r27 + 1
        L_0x05d7:
            int r28 = r28 + 1
            goto L_0x052a
        L_0x05db:
            r0 = r24
            com.machaon.quadratum.parts.Table[] r0 = r0.table
            r29 = r0
            r29 = r29[r22]
            r0 = r29
            byte[] r0 = r0.t
            r29 = r0
            r0 = r24
            byte[] r0 = r0.t
            r30 = r0
            byte r30 = r30[r22]
            r29[r23] = r30
            goto L_0x04b2
        L_0x05f5:
            r0 = r24
            byte[] r0 = r0.t
            r29 = r0
            r30 = 0
            r29[r22] = r30
            goto L_0x0309
        L_0x0601:
            r26 = 0
        L_0x0603:
            r0 = r32
            int r0 = r0.numberOfColors
            r29 = r0
            r0 = r26
            r1 = r29
            if (r0 < r1) goto L_0x0618
            int r29 = r25 + 1
            r0 = r29
            byte r0 = (byte) r0
            r25 = r0
            goto L_0x018a
        L_0x0618:
            r0 = r24
            com.machaon.quadratum.parts.Table[] r0 = r0.table
            r29 = r0
            r29 = r29[r25]
            r0 = r29
            byte[] r0 = r0.t
            r29 = r0
            byte r29 = r29[r26]
            r0 = r29
            r1 = r12
            if (r0 <= r1) goto L_0x0649
            r0 = r24
            com.machaon.quadratum.parts.Table[] r0 = r0.table
            r29 = r0
            r29 = r29[r25]
            r0 = r29
            byte[] r0 = r0.t
            r29 = r0
            byte r12 = r29[r26]
            r0 = r25
            r1 = r32
            r1.freezeStepAI1 = r0
            r0 = r26
            r1 = r32
            r1.freezeStepAI2 = r0
        L_0x0649:
            int r29 = r26 + 1
            r0 = r29
            byte r0 = (byte) r0
            r26 = r0
            goto L_0x0603
        L_0x0651:
            r13 = 0
            r6 = 0
        L_0x0653:
            r0 = r32
            int r0 = r0.numberOfPlayers
            r29 = r0
            r0 = r6
            r1 = r29
            if (r0 < r1) goto L_0x067c
            r29 = 50
            int r29 = r13 - r29
            int[] r30 = com.machaon.quadratum.States.playerOverallScore
            r30 = r30[r33]
            r0 = r32
            int[] r0 = r0.numberOfCells
            r31 = r0
            r31 = r31[r33]
            int r30 = r30 + r31
            int r30 = r30 + r12
            r0 = r29
            r1 = r30
            if (r0 <= r1) goto L_0x06a1
            r29 = 100
            goto L_0x0028
        L_0x067c:
            r0 = r6
            r1 = r33
            if (r0 == r1) goto L_0x069a
            int[] r29 = com.machaon.quadratum.States.playerOverallScore
            r29 = r29[r6]
            r0 = r32
            int[] r0 = r0.numberOfCells
            r30 = r0
            r30 = r30[r6]
            int r29 = r29 + r30
            r5[r6] = r29
            r29 = r5[r6]
            r0 = r13
            r1 = r29
            if (r0 >= r1) goto L_0x069a
            r13 = r5[r6]
        L_0x069a:
            int r29 = r6 + 1
            r0 = r29
            byte r0 = (byte) r0
            r6 = r0
            goto L_0x0653
        L_0x06a1:
            r29 = 1
            goto L_0x0028
        */
        throw new UnsupportedOperationException("Method not decompiled: com.machaon.quadratum.parts.Field.SolveAIFreeze(byte):int");
    }

    private int SolveAIMixer(byte player) {
        byte m = (byte) new Random().nextInt(100);
        if (States.skillAI[player] == 1 && m > 40) {
            return 100;
        }
        if (States.skillAI[player] == 2 && m > 60) {
            return 100;
        }
        int remainingCells = 0;
        for (byte i = 0; i < this.numberOfPlayers; i = (byte) (i + 1)) {
            remainingCells += this.numberOfCells[i];
        }
        if (remainingCells < this.maxPoints / 3) {
            return 100;
        }
        if (remainingCells > (this.maxPoints * 6) / 7) {
            return 100;
        }
        int[] cells = new int[this.numberOfPlayers];
        int max = 0;
        for (byte i2 = 0; i2 < this.numberOfPlayers; i2 = (byte) (i2 + 1)) {
            cells[i2] = FindMaxCells(i2);
            if (max < cells[i2]) {
                max = cells[i2];
            }
        }
        if (max < cells[player] + 4) {
            return 100;
        }
        if (cells[player] < 4) {
            return 100;
        }
        int max2 = 0;
        for (byte i3 = 0; i3 < this.numberOfPlayers; i3 = (byte) (i3 + 1)) {
            if (i3 != player) {
                cells[i3] = States.playerOverallScore[i3] + this.numberOfCells[i3];
                if (max2 < cells[i3]) {
                    max2 = cells[i3];
                }
            }
        }
        if (max2 < (States.playerOverallScore[player] + this.numberOfCells[player]) - 50) {
            return 100;
        }
        return 2;
    }

    /* JADX WARN: Type inference failed for: r0v15, types: [int] */
    /* JADX WARN: Type inference failed for: r0v29, types: [int] */
    /* JADX WARN: Type inference failed for: r0v31, types: [int] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int FindMaxCells(byte r18) {
        /*
            r17 = this;
            com.machaon.quadratum.parts.Table r11 = new com.machaon.quadratum.parts.Table
            r0 = r17
            int r0 = r0.numberOfColors
            r15 = r0
            r11.<init>(r15)
            r0 = r17
            int r0 = r0.sizeFieldX
            r15 = r0
            r0 = r17
            int r0 = r0.sizeFieldY
            r16 = r0
            int[] r15 = new int[]{r15, r16}
            java.lang.Class r16 = java.lang.Byte.TYPE
            r0 = r16
            r1 = r15
            java.lang.Object r2 = java.lang.reflect.Array.newInstance(r0, r1)
            byte[][] r2 = (byte[][]) r2
            r0 = r17
            int r0 = r0.sizeFieldX
            r15 = r0
            r0 = r17
            int r0 = r0.sizeFieldY
            r16 = r0
            int[] r15 = new int[]{r15, r16}
            java.lang.Class r16 = java.lang.Byte.TYPE
            r0 = r16
            r1 = r15
            java.lang.Object r8 = java.lang.reflect.Array.newInstance(r0, r1)
            byte[][] r8 = (byte[][]) r8
            r13 = 0
        L_0x003f:
            r0 = r17
            int r0 = r0.sizeFieldX
            r15 = r0
            if (r13 < r15) goto L_0x009c
            r4 = 0
        L_0x0047:
            r0 = r17
            int r0 = r0.numberOfPlayers
            r15 = r0
            if (r4 < r15) goto L_0x00c8
            r0 = r17
            int r0 = r0.sizeFieldX
            r15 = r0
            r0 = r17
            int r0 = r0.sizeFieldY
            r16 = r0
            int[] r15 = new int[]{r15, r16}
            java.lang.Class r16 = java.lang.Byte.TYPE
            r0 = r16
            r1 = r15
            java.lang.Object r3 = java.lang.reflect.Array.newInstance(r0, r1)
            byte[][] r3 = (byte[][]) r3
            r0 = r17
            int r0 = r0.sizeFieldX
            r15 = r0
            r0 = r17
            int r0 = r0.sizeFieldY
            r16 = r0
            int[] r15 = new int[]{r15, r16}
            java.lang.Class r16 = java.lang.Byte.TYPE
            r0 = r16
            r1 = r15
            java.lang.Object r9 = java.lang.reflect.Array.newInstance(r0, r1)
            byte[][] r9 = (byte[][]) r9
            r13 = 0
        L_0x0083:
            r0 = r17
            int r0 = r0.sizeFieldX
            r15 = r0
            if (r13 < r15) goto L_0x0100
            r10 = 0
        L_0x008b:
            r0 = r17
            int r0 = r0.numberOfColors
            r15 = r0
            if (r10 < r15) goto L_0x011f
            r6 = 0
            r12 = 0
        L_0x0094:
            r0 = r17
            int r0 = r0.numberOfColors
            r15 = r0
            if (r12 < r15) goto L_0x0225
            return r6
        L_0x009c:
            r14 = 0
        L_0x009d:
            r0 = r17
            int r0 = r0.sizeFieldY
            r15 = r0
            if (r14 < r15) goto L_0x00a7
            int r13 = r13 + 1
            goto L_0x003f
        L_0x00a7:
            r15 = r2[r13]
            r0 = r17
            byte[][] r0 = r0.cell
            r16 = r0
            r16 = r16[r13]
            byte r16 = r16[r14]
            r15[r14] = r16
            r15 = r8[r13]
            r0 = r17
            byte[][][] r0 = r0.playerField
            r16 = r0
            r16 = r16[r13]
            r16 = r16[r14]
            byte r16 = r16[r18]
            r15[r14] = r16
            int r14 = r14 + 1
            goto L_0x009d
        L_0x00c8:
            r0 = r4
            r1 = r18
            if (r0 == r1) goto L_0x00d5
            r13 = 0
        L_0x00ce:
            r0 = r17
            int r0 = r0.sizeFieldX
            r15 = r0
            if (r13 < r15) goto L_0x00da
        L_0x00d5:
            int r15 = r4 + 1
            byte r4 = (byte) r15
            goto L_0x0047
        L_0x00da:
            r14 = 0
        L_0x00db:
            r0 = r17
            int r0 = r0.sizeFieldY
            r15 = r0
            if (r14 < r15) goto L_0x00e5
            int r13 = r13 + 1
            goto L_0x00ce
        L_0x00e5:
            r0 = r17
            byte[][][] r0 = r0.playerField
            r15 = r0
            r15 = r15[r13]
            r15 = r15[r14]
            byte r15 = r15[r4]
            r16 = 3
            r0 = r15
            r1 = r16
            if (r0 != r1) goto L_0x00fd
            r15 = r2[r13]
            r16 = 20
            r15[r14] = r16
        L_0x00fd:
            int r14 = r14 + 1
            goto L_0x00db
        L_0x0100:
            r14 = 0
        L_0x0101:
            r0 = r17
            int r0 = r0.sizeFieldY
            r15 = r0
            if (r14 < r15) goto L_0x010c
            int r13 = r13 + 1
            goto L_0x0083
        L_0x010c:
            r15 = r3[r13]
            r16 = r2[r13]
            byte r16 = r16[r14]
            r15[r14] = r16
            r15 = r9[r13]
            r16 = r8[r13]
            byte r16 = r16[r14]
            r15[r14] = r16
            int r14 = r14 + 1
            goto L_0x0101
        L_0x011f:
            r13 = 0
        L_0x0120:
            r0 = r17
            int r0 = r0.sizeFieldX
            r15 = r0
            if (r13 < r15) goto L_0x0153
            r13 = 0
        L_0x0128:
            r0 = r17
            int r0 = r0.sizeFieldX
            r15 = r0
            if (r13 < r15) goto L_0x0171
            r7 = 0
        L_0x0130:
            r5 = 1
            r13 = 0
        L_0x0132:
            r0 = r17
            int r0 = r0.sizeFieldX
            r15 = r0
            if (r13 < r15) goto L_0x0194
            if (r5 == 0) goto L_0x0130
            byte[] r15 = r11.t
            r0 = r17
            int[] r0 = r0.numberOfCells
            r16 = r0
            r16 = r16[r18]
            int r16 = r7 - r16
            r0 = r16
            byte r0 = (byte) r0
            r16 = r0
            r15[r10] = r16
            int r15 = r10 + 1
            byte r10 = (byte) r15
            goto L_0x008b
        L_0x0153:
            r14 = 0
        L_0x0154:
            r0 = r17
            int r0 = r0.sizeFieldY
            r15 = r0
            if (r14 < r15) goto L_0x015e
            int r13 = r13 + 1
            goto L_0x0120
        L_0x015e:
            r15 = r2[r13]
            r16 = r3[r13]
            byte r16 = r16[r14]
            r15[r14] = r16
            r15 = r8[r13]
            r16 = r9[r13]
            byte r16 = r16[r14]
            r15[r14] = r16
            int r14 = r14 + 1
            goto L_0x0154
        L_0x0171:
            r14 = 0
        L_0x0172:
            r0 = r17
            int r0 = r0.sizeFieldY
            r15 = r0
            if (r14 < r15) goto L_0x017c
            int r13 = r13 + 1
            goto L_0x0128
        L_0x017c:
            r15 = r8[r13]
            byte r15 = r15[r14]
            r16 = 3
            r0 = r15
            r1 = r16
            if (r0 != r1) goto L_0x0191
            r15 = r8[r13]
            r16 = 1
            r15[r14] = r16
            r15 = r2[r13]
            r15[r14] = r10
        L_0x0191:
            int r14 = r14 + 1
            goto L_0x0172
        L_0x0194:
            r14 = 0
        L_0x0195:
            r0 = r17
            int r0 = r0.sizeFieldY
            r15 = r0
            if (r14 < r15) goto L_0x019f
            int r13 = r13 + 1
            goto L_0x0132
        L_0x019f:
            r15 = r8[r13]
            byte r15 = r15[r14]
            r16 = 1
            r0 = r15
            r1 = r16
            if (r0 != r1) goto L_0x0221
            r5 = 0
            r15 = r8[r13]
            r16 = 3
            r15[r14] = r16
            int r7 = r7 + 1
            int r14 = r14 + 1
            r0 = r17
            int r0 = r0.sizeFieldY
            r15 = r0
            if (r14 >= r15) goto L_0x01cf
            r15 = r2[r13]
            byte r15 = r15[r14]
            if (r15 != r10) goto L_0x01cf
            r15 = r8[r13]
            byte r16 = r15[r14]
            r16 = r16 | 1
            r0 = r16
            byte r0 = (byte) r0
            r16 = r0
            r15[r14] = r16
        L_0x01cf:
            int r13 = r13 + 1
            int r14 = r14 + -1
            r0 = r17
            int r0 = r0.sizeFieldX
            r15 = r0
            if (r13 >= r15) goto L_0x01ed
            r15 = r2[r13]
            byte r15 = r15[r14]
            if (r15 != r10) goto L_0x01ed
            r15 = r8[r13]
            byte r16 = r15[r14]
            r16 = r16 | 1
            r0 = r16
            byte r0 = (byte) r0
            r16 = r0
            r15[r14] = r16
        L_0x01ed:
            int r13 = r13 + -1
            int r14 = r14 + -1
            if (r14 < 0) goto L_0x0206
            r15 = r2[r13]
            byte r15 = r15[r14]
            if (r15 != r10) goto L_0x0206
            r15 = r8[r13]
            byte r16 = r15[r14]
            r16 = r16 | 1
            r0 = r16
            byte r0 = (byte) r0
            r16 = r0
            r15[r14] = r16
        L_0x0206:
            int r13 = r13 + -1
            int r14 = r14 + 1
            if (r13 < 0) goto L_0x021f
            r15 = r2[r13]
            byte r15 = r15[r14]
            if (r15 != r10) goto L_0x021f
            r15 = r8[r13]
            byte r16 = r15[r14]
            r16 = r16 | 1
            r0 = r16
            byte r0 = (byte) r0
            r16 = r0
            r15[r14] = r16
        L_0x021f:
            int r13 = r13 + 1
        L_0x0221:
            int r14 = r14 + 1
            goto L_0x0195
        L_0x0225:
            byte[] r15 = r11.t
            byte r15 = r15[r12]
            if (r15 <= r6) goto L_0x022f
            byte[] r15 = r11.t
            byte r6 = r15[r12]
        L_0x022f:
            int r15 = r12 + 1
            byte r12 = (byte) r15
            goto L_0x0094
        */
        throw new UnsupportedOperationException("Method not decompiled: com.machaon.quadratum.parts.Field.FindMaxCells(byte):int");
    }

    public void startMine(byte x, byte y) {
        StartBonusBomb(x, y, 4);
    }

    public void EndMine() {
        EndBonusBomb();
    }
}
