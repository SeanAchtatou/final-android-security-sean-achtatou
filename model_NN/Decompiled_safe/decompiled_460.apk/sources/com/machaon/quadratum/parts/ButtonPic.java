package com.machaon.quadratum.parts;

import com.badlogic.gdx.graphics.Texture;

public class ButtonPic extends Geom {
    public int frontX;
    public int frontY;
    boolean isBackAct = false;
    public boolean isOff;
    private byte state;
    public Texture texBackAct;
    public Texture texBackNonAct;
    public Texture texFront;

    public ButtonPic(int x, int y, int width, int height) {
        super(x, y, width, height);
    }

    public ButtonPic(int x, int y, int width, int height, int frontX2, int frontY2) {
        super(x, y, width, height);
        this.frontX = frontX2;
        this.frontY = frontY2;
    }

    public ButtonPic(Texture texBackNonAct2, Texture texBackAct2, int x, int y, int width, int height) {
        super(x, y, width, height);
        this.texBackNonAct = texBackNonAct2;
        this.texBackAct = texBackAct2;
    }

    public ButtonPic(Texture texBackNonAct2, Texture texBackAct2, byte state2, int x, int y, int width, int height) {
        super(x, y, width, height);
        this.texBackNonAct = texBackNonAct2;
        this.texBackAct = texBackAct2;
        this.state = state2;
    }

    public ButtonPic(Texture texBackNonAct2, Texture texBackAct2, Texture texFront2, int x, int y, int width, int height) {
        super(x, y, width, height);
        this.texBackNonAct = texBackNonAct2;
        this.texBackAct = texBackAct2;
        this.texFront = texFront2;
    }

    public ButtonPic(Texture texBackNonAct2, Texture texBackAct2, Texture texFront2, byte state2, int x, int y, int width, int height) {
        super(x, y, width, height);
        this.texBackNonAct = texBackNonAct2;
        this.texBackAct = texBackAct2;
        this.texFront = texFront2;
        this.state = state2;
    }

    public void SetParams(Texture texBackNonAct2, Texture texBackAct2, Texture texFront2, byte state2) {
        this.texBackNonAct = texBackNonAct2;
        this.texBackAct = texBackAct2;
        this.texFront = texFront2;
        this.state = state2;
    }

    public void SetParams(Texture texBackNonAct2, Texture texBackAct2, byte state2) {
        this.texBackNonAct = texBackNonAct2;
        this.texBackAct = texBackAct2;
        this.state = state2;
    }

    public byte getState() {
        this.isBackAct = false;
        return this.state;
    }

    public void setBackActive(boolean act) {
        this.isBackAct = act;
    }

    public Texture getBackTexture() {
        if (this.isBackAct) {
            return this.texBackAct;
        }
        return this.texBackNonAct;
    }

    public boolean isBackActive() {
        return this.isBackAct;
    }

    public void dispose() {
        if (this.texBackNonAct != null) {
            this.texBackNonAct.dispose();
        }
        if (this.texBackAct != null) {
            this.texBackAct.dispose();
        }
        if (this.texFront != null) {
            this.texFront.dispose();
        }
    }
}
