package com.machaon.quadratum.parts;

import com.badlogic.gdx.Gdx;
import java.io.IOException;
import java.io.InputStream;

public class Tournaments {
    public static String TYPE_MULTY = "m";
    public static String TYPE_SINGLE = "s";
    private static byte level;
    private static byte nLevels = 1;
    private static byte tournament;
    private static String type;

    public static void setType(String atype) {
        type = atype;
    }

    public static String getType() {
        return type;
    }

    public static void setTournament(byte atournament) {
        tournament = atournament;
        level = 0;
    }

    public static byte getTournament() {
        return tournament;
    }

    public static boolean isEnd() {
        if (level == nLevels) {
            return true;
        }
        return false;
    }

    public static String getNextLevel() {
        String strLevel = "";
        InputStream in = Gdx.files.internal("data/Levels/t" + type + ((int) tournament)).read();
        try {
            nLevels = (byte) in.read();
            for (byte i = 0; i < level; i = (byte) (i + 1)) {
                in.read();
            }
            strLevel = String.valueOf(String.valueOf(in.read())) + ".lvl";
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            in.close();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        return strLevel;
    }

    public static String getNameLevel(byte tournament2, byte level2) {
        String strLevel = "";
        InputStream in = Gdx.files.internal("data/Levels/t" + type + ((int) tournament2)).read();
        try {
            nLevels = (byte) in.read();
            for (byte i = 0; i < level2; i = (byte) (i + 1)) {
                in.read();
            }
            strLevel = String.valueOf(String.valueOf(in.read())) + ".lvl";
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            in.close();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        return strLevel;
    }

    public static void setToNextLevel() {
        level = (byte) (level + 1);
    }

    public static byte getNumberOfCurrentLevel() {
        return level;
    }
}
