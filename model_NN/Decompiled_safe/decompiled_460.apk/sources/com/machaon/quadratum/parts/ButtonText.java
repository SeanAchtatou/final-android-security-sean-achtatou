package com.machaon.quadratum.parts;

import com.badlogic.gdx.graphics.Texture;

public class ButtonText extends Geom {
    private boolean isBackAct = false;
    public byte state;
    private String string;
    public Texture texBackAct;
    public Texture texBackNonAct;
    private int textOffsetY;

    public ButtonText(int x, int y, int width, int height) {
        super(x, y, width, height);
    }

    public ButtonText(int x, int y, int width, int height, int textY) {
        super(x, y, width, height);
        this.textOffsetY = textY;
    }

    public void SetParams(String string2, Texture texBackNonAct2, Texture texBackAct2) {
        this.texBackNonAct = texBackNonAct2;
        this.texBackAct = texBackAct2;
        this.string = string2;
    }

    public void SetParams(String string2, Texture texBackNonAct2, Texture texBackAct2, byte state2) {
        this.texBackNonAct = texBackNonAct2;
        this.texBackAct = texBackAct2;
        this.string = string2;
        this.state = state2;
    }

    public void setString(String string2) {
        this.string = string2;
    }

    public byte getState() {
        this.isBackAct = false;
        return this.state;
    }

    public String getString() {
        return this.string;
    }

    public int getTextOffsetY() {
        return this.textOffsetY;
    }

    public boolean isBackActive() {
        return this.isBackAct;
    }

    public void setBackActive(boolean act) {
        this.isBackAct = act;
    }

    public Texture getBackTexture() {
        if (this.isBackAct) {
            return this.texBackAct;
        }
        return this.texBackNonAct;
    }

    public void dispose() {
        if (this.texBackNonAct != null) {
            this.texBackNonAct.dispose();
        }
        if (this.texBackAct != null) {
            this.texBackAct.dispose();
        }
    }
}
