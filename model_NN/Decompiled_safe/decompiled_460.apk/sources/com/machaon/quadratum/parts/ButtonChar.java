package com.machaon.quadratum.parts;

public class ButtonChar extends Geom {
    public boolean isBackActive = false;
    private char normal;
    private char shift;

    public ButtonChar(int x, int y, int width, int height) {
        super(x, y, width, height);
    }

    public void setGeom(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.height = height;
        this.width = width;
    }

    public void setChars(char normal2, char shift2) {
        this.normal = normal2;
        this.shift = shift2;
    }

    public char getNormalChar() {
        return this.normal;
    }

    public char getShiftChar() {
        return this.shift;
    }

    public void setBackActive(boolean act) {
        this.isBackActive = act;
    }
}
