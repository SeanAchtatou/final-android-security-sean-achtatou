package com.machaon.quadratum.parts;

public class Table {
    public byte[] t;
    public Table[] table;

    public Table(int numberOfColors) {
        this.t = new byte[numberOfColors];
        this.table = new Table[numberOfColors];
    }
}
