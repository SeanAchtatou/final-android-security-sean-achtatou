package com.badlogic.gdx.utils;

import com.badlogic.gdx.math.MathUtils;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class ObjectMap<K, V> {
    private static final int PRIME1 = -1105259343;
    private static final int PRIME2 = -1262997959;
    private static final int PRIME3 = -825114047;
    int capacity;
    private Entries entries;
    private int hashShift;
    K[] keyTable;
    private Keys keys;
    private float loadFactor;
    private int mask;
    private int pushIterations;
    public int size;
    private int stashCapacity;
    int stashSize;
    private int threshold;
    V[] valueTable;
    private Values values;

    public ObjectMap() {
        this(32, 0.8f);
    }

    public ObjectMap(int initialCapacity) {
        this(initialCapacity, 0.8f);
    }

    public ObjectMap(int initialCapacity, float loadFactor2) {
        if (initialCapacity < 0) {
            throw new IllegalArgumentException("initialCapacity must be >= 0: " + initialCapacity);
        } else if (this.capacity > 1073741824) {
            throw new IllegalArgumentException("initialCapacity is too large: " + initialCapacity);
        } else {
            this.capacity = MathUtils.nextPowerOfTwo(initialCapacity);
            if (loadFactor2 <= 0.0f) {
                throw new IllegalArgumentException("loadFactor must be > 0: " + loadFactor2);
            }
            this.loadFactor = loadFactor2;
            this.threshold = (int) (((float) this.capacity) * loadFactor2);
            this.mask = this.capacity - 1;
            this.hashShift = 31 - Integer.numberOfTrailingZeros(this.capacity);
            this.stashCapacity = Math.max(3, ((int) Math.ceil(Math.log((double) this.capacity))) + 1);
            this.pushIterations = Math.max(Math.min(this.capacity, 32), ((int) Math.sqrt((double) this.capacity)) / 4);
            this.keyTable = new Object[(this.capacity + this.stashCapacity)];
            this.valueTable = new Object[this.keyTable.length];
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: K
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public V put(K r13, V r14) {
        /*
            r12 = this;
            r11 = 0
            if (r13 != 0) goto L_0x000b
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "key cannot be null."
            r0.<init>(r1)
            throw r0
        L_0x000b:
            int r9 = r13.hashCode()
            int r0 = r12.mask
            r3 = r9 & r0
            K[] r0 = r12.keyTable
            r4 = r0[r3]
            boolean r0 = r13.equals(r4)
            if (r0 == 0) goto L_0x0027
            V[] r0 = r12.valueTable
            r10 = r0[r3]
            V[] r0 = r12.valueTable
            r0[r3] = r14
            r0 = r10
        L_0x0026:
            return r0
        L_0x0027:
            long r0 = (long) r9
            int r5 = r12.hash2(r0)
            K[] r0 = r12.keyTable
            r6 = r0[r5]
            boolean r0 = r13.equals(r6)
            if (r0 == 0) goto L_0x0040
            V[] r0 = r12.valueTable
            r10 = r0[r5]
            V[] r0 = r12.valueTable
            r0[r5] = r14
            r0 = r10
            goto L_0x0026
        L_0x0040:
            long r0 = (long) r9
            int r7 = r12.hash3(r0)
            K[] r0 = r12.keyTable
            r8 = r0[r7]
            boolean r0 = r13.equals(r8)
            if (r0 == 0) goto L_0x0059
            V[] r0 = r12.valueTable
            r10 = r0[r7]
            V[] r0 = r12.valueTable
            r0[r7] = r14
            r0 = r10
            goto L_0x0026
        L_0x0059:
            if (r4 != 0) goto L_0x0076
            K[] r0 = r12.keyTable
            r0[r3] = r13
            V[] r0 = r12.valueTable
            r0[r3] = r14
            int r0 = r12.size
            int r1 = r0 + 1
            r12.size = r1
            int r1 = r12.threshold
            if (r0 < r1) goto L_0x0074
            int r0 = r12.capacity
            int r0 = r0 << 1
            r12.resize(r0)
        L_0x0074:
            r0 = r11
            goto L_0x0026
        L_0x0076:
            if (r6 != 0) goto L_0x0093
            K[] r0 = r12.keyTable
            r0[r5] = r13
            V[] r0 = r12.valueTable
            r0[r5] = r14
            int r0 = r12.size
            int r1 = r0 + 1
            r12.size = r1
            int r1 = r12.threshold
            if (r0 < r1) goto L_0x0091
            int r0 = r12.capacity
            int r0 = r0 << 1
            r12.resize(r0)
        L_0x0091:
            r0 = r11
            goto L_0x0026
        L_0x0093:
            if (r8 != 0) goto L_0x00b1
            K[] r0 = r12.keyTable
            r0[r7] = r13
            V[] r0 = r12.valueTable
            r0[r7] = r14
            int r0 = r12.size
            int r1 = r0 + 1
            r12.size = r1
            int r1 = r12.threshold
            if (r0 < r1) goto L_0x00ae
            int r0 = r12.capacity
            int r0 = r0 << 1
            r12.resize(r0)
        L_0x00ae:
            r0 = r11
            goto L_0x0026
        L_0x00b1:
            r0 = r12
            r1 = r13
            r2 = r14
            r0.push(r1, r2, r3, r4, r5, r6, r7, r8)
            r0 = r11
            goto L_0x0026
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.ObjectMap.put(java.lang.Object, java.lang.Object):java.lang.Object");
    }

    public void putAll(ObjectMap<K, V> map) {
        Iterator<Entry<K, V>> it = map.entries().iterator();
        while (it.hasNext()) {
            Entry<K, V> entry = it.next();
            put(entry.key, entry.value);
        }
    }

    private void putResize(K key, V value) {
        int hashCode = key.hashCode();
        int index1 = hashCode & this.mask;
        K key1 = this.keyTable[index1];
        if (key1 == null) {
            this.keyTable[index1] = key;
            this.valueTable[index1] = value;
            int i = this.size;
            this.size = i + 1;
            if (i >= this.threshold) {
                resize(this.capacity << 1);
                return;
            }
            return;
        }
        int index2 = hash2((long) hashCode);
        K key2 = this.keyTable[index2];
        if (key2 == null) {
            this.keyTable[index2] = key;
            this.valueTable[index2] = value;
            int i2 = this.size;
            this.size = i2 + 1;
            if (i2 >= this.threshold) {
                resize(this.capacity << 1);
                return;
            }
            return;
        }
        int index3 = hash3((long) hashCode);
        K key3 = this.keyTable[index3];
        if (key3 == null) {
            this.keyTable[index3] = key;
            this.valueTable[index3] = value;
            int i3 = this.size;
            this.size = i3 + 1;
            if (i3 >= this.threshold) {
                resize(this.capacity << 1);
                return;
            }
            return;
        }
        push(key, value, index1, key1, index2, key2, index3, key3);
    }

    private void push(K insertKey, V insertValue, int index1, K key1, int index2, K key2, int index3, K key3) {
        K evictedKey;
        V evictedValue;
        K[] kArr = this.keyTable;
        V[] vArr = this.valueTable;
        int mask2 = this.mask;
        int i = 0;
        int pushIterations2 = this.pushIterations;
        while (true) {
            switch (MathUtils.random(2)) {
                case 0:
                    evictedKey = key1;
                    evictedValue = vArr[index1];
                    kArr[index1] = insertKey;
                    vArr[index1] = insertValue;
                    break;
                case 1:
                    evictedKey = key2;
                    evictedValue = vArr[index2];
                    kArr[index2] = insertKey;
                    vArr[index2] = insertValue;
                    break;
                default:
                    evictedKey = key3;
                    evictedValue = vArr[index3];
                    kArr[index3] = insertKey;
                    vArr[index3] = insertValue;
                    break;
            }
            int hashCode = evictedKey.hashCode();
            index1 = hashCode & mask2;
            key1 = kArr[index1];
            if (key1 == null) {
                kArr[index1] = evictedKey;
                vArr[index1] = evictedValue;
                int i2 = this.size;
                this.size = i2 + 1;
                if (i2 >= this.threshold) {
                    resize(this.capacity << 1);
                    return;
                }
                return;
            }
            index2 = hash2((long) hashCode);
            key2 = kArr[index2];
            if (key2 == null) {
                kArr[index2] = evictedKey;
                vArr[index2] = evictedValue;
                int i3 = this.size;
                this.size = i3 + 1;
                if (i3 >= this.threshold) {
                    resize(this.capacity << 1);
                    return;
                }
                return;
            }
            index3 = hash3((long) hashCode);
            key3 = kArr[index3];
            if (key3 == null) {
                kArr[index3] = evictedKey;
                vArr[index3] = evictedValue;
                int i4 = this.size;
                this.size = i4 + 1;
                if (i4 >= this.threshold) {
                    resize(this.capacity << 1);
                    return;
                }
                return;
            }
            i++;
            if (i == pushIterations2) {
                putStash(evictedKey, evictedValue);
                return;
            } else {
                insertKey = evictedKey;
                insertValue = evictedValue;
            }
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: K
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private void putStash(K r7, V r8) {
        /*
            r6 = this;
            int r4 = r6.stashSize
            int r5 = r6.stashCapacity
            if (r4 != r5) goto L_0x0011
            int r4 = r6.capacity
            int r4 = r4 << 1
            r6.resize(r4)
            r6.put(r7, r8)
        L_0x0010:
            return
        L_0x0011:
            K[] r2 = r6.keyTable
            int r0 = r6.capacity
            int r4 = r6.stashSize
            int r3 = r0 + r4
        L_0x0019:
            if (r0 < r3) goto L_0x002e
            int r4 = r6.capacity
            int r5 = r6.stashSize
            int r1 = r4 + r5
            r2[r1] = r7
            V[] r4 = r6.valueTable
            r4[r1] = r8
            int r4 = r6.stashSize
            int r4 = r4 + 1
            r6.stashSize = r4
            goto L_0x0010
        L_0x002e:
            r4 = r2[r0]
            boolean r4 = r7.equals(r4)
            if (r4 == 0) goto L_0x003b
            V[] r4 = r6.valueTable
            r4[r0] = r8
            goto L_0x0010
        L_0x003b:
            int r0 = r0 + 1
            goto L_0x0019
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.ObjectMap.putStash(java.lang.Object, java.lang.Object):void");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: K
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public V get(K r5) {
        /*
            r4 = this;
            int r0 = r5.hashCode()
            int r2 = r4.mask
            r1 = r0 & r2
            K[] r2 = r4.keyTable
            r2 = r2[r1]
            boolean r2 = r5.equals(r2)
            if (r2 != 0) goto L_0x0035
            long r2 = (long) r0
            int r1 = r4.hash2(r2)
            K[] r2 = r4.keyTable
            r2 = r2[r1]
            boolean r2 = r5.equals(r2)
            if (r2 != 0) goto L_0x0035
            long r2 = (long) r0
            int r1 = r4.hash3(r2)
            K[] r2 = r4.keyTable
            r2 = r2[r1]
            boolean r2 = r5.equals(r2)
            if (r2 != 0) goto L_0x0035
            java.lang.Object r2 = r4.getStash(r5)
        L_0x0034:
            return r2
        L_0x0035:
            V[] r2 = r4.valueTable
            r2 = r2[r1]
            goto L_0x0034
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.ObjectMap.get(java.lang.Object):java.lang.Object");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: K
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private V getStash(K r5) {
        /*
            r4 = this;
            K[] r1 = r4.keyTable
            int r0 = r4.capacity
            int r3 = r4.stashSize
            int r2 = r0 + r3
        L_0x0008:
            if (r0 < r2) goto L_0x000c
            r3 = 0
        L_0x000b:
            return r3
        L_0x000c:
            r3 = r1[r0]
            boolean r3 = r5.equals(r3)
            if (r3 == 0) goto L_0x0019
            V[] r3 = r4.valueTable
            r3 = r3[r0]
            goto L_0x000b
        L_0x0019:
            int r0 = r0 + 1
            goto L_0x0008
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.ObjectMap.getStash(java.lang.Object):java.lang.Object");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: K
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public V remove(K r8) {
        /*
            r7 = this;
            r6 = 1
            r5 = 0
            int r0 = r8.hashCode()
            int r3 = r7.mask
            r1 = r0 & r3
            K[] r3 = r7.keyTable
            r3 = r3[r1]
            boolean r3 = r8.equals(r3)
            if (r3 == 0) goto L_0x0027
            K[] r3 = r7.keyTable
            r3[r1] = r5
            V[] r3 = r7.valueTable
            r2 = r3[r1]
            V[] r3 = r7.valueTable
            r3[r1] = r5
            int r3 = r7.size
            int r3 = r3 - r6
            r7.size = r3
            r3 = r2
        L_0x0026:
            return r3
        L_0x0027:
            long r3 = (long) r0
            int r1 = r7.hash2(r3)
            K[] r3 = r7.keyTable
            r3 = r3[r1]
            boolean r3 = r8.equals(r3)
            if (r3 == 0) goto L_0x0049
            K[] r3 = r7.keyTable
            r3[r1] = r5
            V[] r3 = r7.valueTable
            r2 = r3[r1]
            V[] r3 = r7.valueTable
            r3[r1] = r5
            int r3 = r7.size
            int r3 = r3 - r6
            r7.size = r3
            r3 = r2
            goto L_0x0026
        L_0x0049:
            long r3 = (long) r0
            int r1 = r7.hash3(r3)
            K[] r3 = r7.keyTable
            r3 = r3[r1]
            boolean r3 = r8.equals(r3)
            if (r3 == 0) goto L_0x006b
            K[] r3 = r7.keyTable
            r3[r1] = r5
            V[] r3 = r7.valueTable
            r2 = r3[r1]
            V[] r3 = r7.valueTable
            r3[r1] = r5
            int r3 = r7.size
            int r3 = r3 - r6
            r7.size = r3
            r3 = r2
            goto L_0x0026
        L_0x006b:
            java.lang.Object r3 = r7.removeStash(r8)
            goto L_0x0026
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.ObjectMap.remove(java.lang.Object):java.lang.Object");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: K
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    V removeStash(K r7) {
        /*
            r6 = this;
            K[] r1 = r6.keyTable
            int r0 = r6.capacity
            int r4 = r6.stashSize
            int r2 = r0 + r4
        L_0x0008:
            if (r0 < r2) goto L_0x000c
            r4 = 0
        L_0x000b:
            return r4
        L_0x000c:
            r4 = r1[r0]
            boolean r4 = r7.equals(r4)
            if (r4 == 0) goto L_0x0023
            V[] r4 = r6.valueTable
            r3 = r4[r0]
            r6.removeStashIndex(r0)
            int r4 = r6.size
            r5 = 1
            int r4 = r4 - r5
            r6.size = r4
            r4 = r3
            goto L_0x000b
        L_0x0023:
            int r0 = r0 + 1
            goto L_0x0008
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.ObjectMap.removeStash(java.lang.Object):java.lang.Object");
    }

    /* access modifiers changed from: package-private */
    public void removeStashIndex(int index) {
        this.stashSize--;
        int lastIndex = this.capacity + this.stashSize;
        if (index < lastIndex) {
            this.keyTable[index] = this.keyTable[lastIndex];
            this.valueTable[index] = this.valueTable[lastIndex];
            this.valueTable[lastIndex] = null;
            return;
        }
        this.valueTable[index] = null;
    }

    public void clear() {
        Object[] keyTable2 = this.keyTable;
        Object[] valueTable2 = this.valueTable;
        int i = this.capacity + this.stashSize;
        while (true) {
            int i2 = i;
            i = i2 - 1;
            if (i2 <= 0) {
                this.size = 0;
                this.stashSize = 0;
                return;
            }
            keyTable2[i] = null;
            valueTable2[i] = null;
        }
    }

    public boolean containsValue(Object value, boolean identity) {
        Object[] valueTable2 = this.valueTable;
        if (value != null) {
            if (!identity) {
                int i = this.capacity + this.stashSize;
                while (true) {
                    int i2 = i;
                    i = i2 - 1;
                    if (i2 <= 0) {
                        break;
                    } else if (value.equals(valueTable2[i])) {
                        return true;
                    }
                }
            } else {
                int i3 = this.capacity + this.stashSize;
                while (true) {
                    int i4 = i3;
                    i3 = i4 - 1;
                    if (i4 <= 0) {
                        break;
                    } else if (valueTable2[i3] == value) {
                        return true;
                    }
                }
            }
        } else {
            Object[] keyTable2 = this.keyTable;
            int i5 = this.capacity + this.stashSize;
            while (true) {
                int i6 = i5;
                i5 = i6 - 1;
                if (i6 <= 0) {
                    break;
                } else if (keyTable2[i5] != null && valueTable2[i5] == null) {
                    return true;
                }
            }
        }
        return false;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: K
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public boolean containsKey(K r5) {
        /*
            r4 = this;
            int r0 = r5.hashCode()
            int r2 = r4.mask
            r1 = r0 & r2
            K[] r2 = r4.keyTable
            r2 = r2[r1]
            boolean r2 = r5.equals(r2)
            if (r2 != 0) goto L_0x0035
            long r2 = (long) r0
            int r1 = r4.hash2(r2)
            K[] r2 = r4.keyTable
            r2 = r2[r1]
            boolean r2 = r5.equals(r2)
            if (r2 != 0) goto L_0x0035
            long r2 = (long) r0
            int r1 = r4.hash3(r2)
            K[] r2 = r4.keyTable
            r2 = r2[r1]
            boolean r2 = r5.equals(r2)
            if (r2 != 0) goto L_0x0035
            boolean r2 = r4.containsKeyStash(r5)
        L_0x0034:
            return r2
        L_0x0035:
            r2 = 1
            goto L_0x0034
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.ObjectMap.containsKey(java.lang.Object):boolean");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: K
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private boolean containsKeyStash(K r5) {
        /*
            r4 = this;
            K[] r1 = r4.keyTable
            int r0 = r4.capacity
            int r3 = r4.stashSize
            int r2 = r0 + r3
        L_0x0008:
            if (r0 < r2) goto L_0x000c
            r3 = 0
        L_0x000b:
            return r3
        L_0x000c:
            r3 = r1[r0]
            boolean r3 = r5.equals(r3)
            if (r3 == 0) goto L_0x0016
            r3 = 1
            goto L_0x000b
        L_0x0016:
            int r0 = r0 + 1
            goto L_0x0008
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.ObjectMap.containsKeyStash(java.lang.Object):boolean");
    }

    public void ensureCapacity(int additionalCapacity) {
        int sizeNeeded = this.size + additionalCapacity;
        if (sizeNeeded >= this.threshold) {
            resize(MathUtils.nextPowerOfTwo((int) (((float) sizeNeeded) / this.loadFactor)));
        }
    }

    private void resize(int newSize) {
        int oldEndIndex = this.capacity + this.stashSize;
        this.capacity = newSize;
        this.threshold = (int) (((float) newSize) * this.loadFactor);
        this.mask = newSize - 1;
        this.hashShift = 31 - Integer.numberOfTrailingZeros(newSize);
        this.stashCapacity = Math.max(3, (int) Math.ceil(Math.log((double) newSize)));
        this.pushIterations = Math.max(Math.min(this.capacity, 32), ((int) Math.sqrt((double) this.capacity)) / 4);
        K[] oldKeyTable = this.keyTable;
        Object[] oldValueTable = this.valueTable;
        this.keyTable = new Object[(this.stashCapacity + newSize)];
        this.valueTable = new Object[(this.stashCapacity + newSize)];
        this.size = 0;
        this.stashSize = 0;
        for (int i = 0; i < oldEndIndex; i++) {
            K key = oldKeyTable[i];
            if (key != null) {
                putResize(key, oldValueTable[i]);
            }
        }
    }

    private int hash2(long h) {
        long h2 = h * -1262997959;
        return (int) (((h2 >>> this.hashShift) ^ h2) & ((long) this.mask));
    }

    private int hash3(long h) {
        long h2 = h * -825114047;
        return (int) (((h2 >>> this.hashShift) ^ h2) & ((long) this.mask));
    }

    public String toString() {
        if (this.size == 0) {
            return "[]";
        }
        StringBuilder buffer = new StringBuilder(32);
        buffer.append('[');
        K[] keyTable2 = this.keyTable;
        Object[] valueTable2 = this.valueTable;
        int i = keyTable2.length;
        while (true) {
            int i2 = i;
            i = i2 - 1;
            if (i2 <= 0) {
                break;
            }
            K key = keyTable2[i];
            if (key != null) {
                buffer.append((Object) key);
                buffer.append('=');
                buffer.append(valueTable2[i]);
                break;
            }
        }
        while (true) {
            int i3 = i;
            i = i3 - 1;
            if (i3 <= 0) {
                buffer.append(']');
                return buffer.toString();
            }
            K key2 = keyTable2[i];
            if (key2 != null) {
                buffer.append(", ");
                buffer.append((Object) key2);
                buffer.append('=');
                buffer.append(valueTable2[i]);
            }
        }
    }

    public Entries<K, V> entries() {
        if (this.entries == null) {
            this.entries = new Entries(this);
        } else {
            this.entries.reset();
        }
        return this.entries;
    }

    public Values<V> values() {
        if (this.values == null) {
            this.values = new Values(this);
        } else {
            this.values.reset();
        }
        return this.values;
    }

    public Keys<K> keys() {
        if (this.keys == null) {
            this.keys = new Keys(this);
        } else {
            this.keys.reset();
        }
        return this.keys;
    }

    public static class Entry<K, V> {
        public K key;
        public V value;

        public String toString() {
            return ((Object) this.key) + "=" + ((Object) this.value);
        }
    }

    private static class MapIterator<K, V> {
        int currentIndex;
        public boolean hasNext;
        final ObjectMap<K, V> map;
        int nextIndex;

        public MapIterator(ObjectMap<K, V> map2) {
            this.map = map2;
            reset();
        }

        public void reset() {
            this.currentIndex = -1;
            this.nextIndex = -1;
            findNextIndex();
        }

        /* access modifiers changed from: package-private */
        public void findNextIndex() {
            this.hasNext = false;
            Object[] keyTable = this.map.keyTable;
            int n = this.map.capacity + this.map.stashSize;
            do {
                int i = this.nextIndex + 1;
                this.nextIndex = i;
                if (i >= n) {
                    return;
                }
            } while (keyTable[this.nextIndex] == null);
            this.hasNext = true;
        }

        public void remove() {
            if (this.currentIndex < 0) {
                throw new IllegalStateException("next must be called before remove.");
            }
            if (this.currentIndex >= this.map.capacity) {
                this.map.removeStashIndex(this.currentIndex);
            } else {
                this.map.keyTable[this.currentIndex] = null;
                this.map.valueTable[this.currentIndex] = null;
            }
            this.currentIndex = -1;
            this.map.size--;
        }
    }

    public static class Entries<K, V> extends MapIterator<K, V> implements Iterable<Entry<K, V>>, Iterator<Entry<K, V>> {
        private Entry<K, V> entry = new Entry<>();

        public /* bridge */ /* synthetic */ void remove() {
            super.remove();
        }

        public /* bridge */ /* synthetic */ void reset() {
            super.reset();
        }

        public Entries(ObjectMap<K, V> map) {
            super(map);
        }

        public Entry<K, V> next() {
            if (!this.hasNext) {
                throw new NoSuchElementException();
            }
            Object[] keyTable = this.map.keyTable;
            this.entry.key = keyTable[this.nextIndex];
            this.entry.value = this.map.valueTable[this.nextIndex];
            this.currentIndex = this.nextIndex;
            findNextIndex();
            return this.entry;
        }

        public boolean hasNext() {
            return this.hasNext;
        }

        public Iterator<Entry<K, V>> iterator() {
            return this;
        }
    }

    public static class Values<V> extends MapIterator<Object, V> implements Iterable<V>, Iterator<V> {
        public /* bridge */ /* synthetic */ void remove() {
            super.remove();
        }

        public /* bridge */ /* synthetic */ void reset() {
            super.reset();
        }

        public Values(ObjectMap<?, V> map) {
            super(map);
        }

        public boolean hasNext() {
            return this.hasNext;
        }

        public V next() {
            V value = this.map.valueTable[this.nextIndex];
            this.currentIndex = this.nextIndex;
            findNextIndex();
            return value;
        }

        public Iterator<V> iterator() {
            return this;
        }

        public Array<V> toArray() {
            Array array = new Array(true, this.map.size);
            while (this.hasNext) {
                array.add(next());
            }
            return array;
        }
    }

    public static class Keys<K> extends MapIterator<K, Object> implements Iterable<K>, Iterator<K> {
        public /* bridge */ /* synthetic */ void remove() {
            super.remove();
        }

        public /* bridge */ /* synthetic */ void reset() {
            super.reset();
        }

        public Keys(ObjectMap<K, ?> map) {
            super(map);
        }

        public boolean hasNext() {
            return this.hasNext;
        }

        public K next() {
            K key = this.map.keyTable[this.nextIndex];
            this.currentIndex = this.nextIndex;
            findNextIndex();
            return key;
        }

        public Iterator<K> iterator() {
            return this;
        }

        public Array<K> toArray() {
            Array array = new Array(true, this.map.size);
            while (this.hasNext) {
                array.add(next());
            }
            return array;
        }
    }
}
