package com.badlogic.gdx.assets;

public class AssetDescriptor {
    final String fileName;
    final AssetLoaderParameters params;
    final Class type;

    public AssetDescriptor(String fileName2, Class assetType, AssetLoaderParameters params2) {
        this.fileName = fileName2;
        this.type = assetType;
        this.params = params2;
    }
}
