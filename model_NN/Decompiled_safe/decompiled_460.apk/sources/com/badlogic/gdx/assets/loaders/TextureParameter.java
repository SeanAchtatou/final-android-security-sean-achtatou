package com.badlogic.gdx.assets.loaders;

import com.badlogic.gdx.assets.AssetLoaderParameters;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;

public class TextureParameter implements AssetLoaderParameters<Texture> {
    public Pixmap.Format format = null;
    public boolean genMipMaps = false;
    public Texture texture = null;
}
