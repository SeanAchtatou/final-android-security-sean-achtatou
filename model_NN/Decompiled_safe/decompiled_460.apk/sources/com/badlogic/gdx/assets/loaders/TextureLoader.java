package com.badlogic.gdx.assets.loaders;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.TextureData;
import com.badlogic.gdx.graphics.glutils.FileTextureData;
import com.badlogic.gdx.utils.Array;

public class TextureLoader implements AsynchronousAssetLoader<Texture, TextureParameter> {
    TextureData data;
    Texture texture;

    public void loadAsync(AssetManager manager, String fileName, TextureParameter parameter) {
        FileHandle handle = Gdx.files.internal(fileName);
        Pixmap pixmap = new Pixmap(handle);
        Pixmap.Format format = null;
        boolean genMipMaps = false;
        this.texture = null;
        if (parameter != null) {
            format = parameter.format;
            genMipMaps = parameter.genMipMaps;
            this.texture = parameter.texture;
        }
        this.data = new FileTextureData(handle, pixmap, format, genMipMaps);
    }

    public Texture loadSync() {
        if (this.texture == null) {
            return new Texture(this.data);
        }
        this.texture.load(this.data);
        return this.texture;
    }

    public Array<AssetDescriptor> getDependencies(String fileName, TextureParameter parameter) {
        return null;
    }
}
