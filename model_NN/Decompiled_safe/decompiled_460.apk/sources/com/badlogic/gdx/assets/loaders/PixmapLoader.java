package com.badlogic.gdx.assets.loaders;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.utils.Array;

public class PixmapLoader implements AsynchronousAssetLoader<Pixmap, PixmapParameter> {
    Pixmap pixmap;

    public void loadAsync(AssetManager manager, String fileName, PixmapParameter parameter) {
        this.pixmap = null;
        this.pixmap = new Pixmap(Gdx.files.internal(fileName));
    }

    public Pixmap loadSync() {
        return this.pixmap;
    }

    public Array<AssetDescriptor> getDependencies(String fileName, PixmapParameter parameter) {
        return null;
    }
}
