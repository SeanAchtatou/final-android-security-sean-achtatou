package com.badlogic.gdx.assets;

import com.badlogic.gdx.assets.loaders.AssetLoader;
import com.badlogic.gdx.assets.loaders.BitmapFontLoader;
import com.badlogic.gdx.assets.loaders.MusicLoader;
import com.badlogic.gdx.assets.loaders.PixmapLoader;
import com.badlogic.gdx.assets.loaders.SoundLoader;
import com.badlogic.gdx.assets.loaders.TextureLoader;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.ObjectMap;
import java.util.Iterator;
import java.util.Stack;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

public class AssetManager implements Disposable {
    final ObjectMap<String, Class> assetTypes = new ObjectMap<>();
    final ObjectMap<Class, ObjectMap<String, Object>> assets = new ObjectMap<>();
    AssetErrorListener listener = null;
    int loaded = 0;
    final ObjectMap<Class, AssetLoader> loaders = new ObjectMap<>();
    final Array<AssetDescriptor> preloadQueue = new Array<>();
    Stack<AssetLoadingTask> tasks = new Stack<>();
    final ExecutorService threadPool;
    int toLoad = 0;

    public AssetManager() {
        setLoader(BitmapFont.class, new BitmapFontLoader());
        setLoader(Music.class, new MusicLoader());
        setLoader(Pixmap.class, new PixmapLoader());
        setLoader(Sound.class, new SoundLoader());
        setLoader(Texture.class, new TextureLoader());
        this.threadPool = Executors.newFixedThreadPool(1, new ThreadFactory() {
            public Thread newThread(Runnable r) {
                Thread thread = new Thread(r, "AssetManager-Loader-Thread");
                thread.setDaemon(true);
                return thread;
            }
        });
    }

    public synchronized <T> T get(String fileName, Class<T> type) {
        T asset;
        ObjectMap<String, Object> assetsByType = this.assets.get(type);
        if (assetsByType == null) {
            throw new GdxRuntimeException("Asset '" + fileName + "' not loaded");
        }
        asset = assetsByType.get(fileName);
        if (asset == null) {
            throw new GdxRuntimeException("Asset '" + fileName + "' not loaded");
        }
        return asset;
    }

    public synchronized void remove(String fileName) {
        Class type = this.assetTypes.get(fileName);
        if (type == null) {
            throw new GdxRuntimeException("Asset '" + fileName + "' not loaded");
        }
        this.assetTypes.remove(fileName);
        Object asset = this.assets.get(type).remove(fileName);
        if (asset instanceof Disposable) {
            ((Disposable) asset).dispose();
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x001b A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x001e A[SYNTHETIC, Splitter:B:8:0x001e] */
    public synchronized <T> boolean containsAsset(T r6) {
        /*
            r5 = this;
            monitor-enter(r5)
            com.badlogic.gdx.utils.ObjectMap<java.lang.Class, com.badlogic.gdx.utils.ObjectMap<java.lang.String, java.lang.Object>> r3 = r5.assets     // Catch:{ all -> 0x0032 }
            java.lang.Class r4 = r6.getClass()     // Catch:{ all -> 0x0032 }
            java.lang.Object r2 = r3.get(r4)     // Catch:{ all -> 0x0032 }
            com.badlogic.gdx.utils.ObjectMap r2 = (com.badlogic.gdx.utils.ObjectMap) r2     // Catch:{ all -> 0x0032 }
            com.badlogic.gdx.utils.ObjectMap$Keys r3 = r2.keys()     // Catch:{ all -> 0x0032 }
            java.util.Iterator r3 = r3.iterator()     // Catch:{ all -> 0x0032 }
        L_0x0015:
            boolean r4 = r3.hasNext()     // Catch:{ all -> 0x0032 }
            if (r4 != 0) goto L_0x001e
            r3 = 0
        L_0x001c:
            monitor-exit(r5)
            return r3
        L_0x001e:
            java.lang.Object r0 = r3.next()     // Catch:{ all -> 0x0032 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ all -> 0x0032 }
            java.lang.Object r1 = r2.get(r0)     // Catch:{ all -> 0x0032 }
            if (r1 == r6) goto L_0x0030
            boolean r4 = r6.equals(r1)     // Catch:{ all -> 0x0032 }
            if (r4 == 0) goto L_0x0015
        L_0x0030:
            r3 = 1
            goto L_0x001c
        L_0x0032:
            r3 = move-exception
            monitor-exit(r5)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.assets.AssetManager.containsAsset(java.lang.Object):boolean");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x001b A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x001e A[SYNTHETIC, Splitter:B:8:0x001e] */
    public synchronized <T> java.lang.String getAssetFileName(T r6) {
        /*
            r5 = this;
            monitor-enter(r5)
            com.badlogic.gdx.utils.ObjectMap<java.lang.Class, com.badlogic.gdx.utils.ObjectMap<java.lang.String, java.lang.Object>> r3 = r5.assets     // Catch:{ all -> 0x0032 }
            java.lang.Class r4 = r6.getClass()     // Catch:{ all -> 0x0032 }
            java.lang.Object r2 = r3.get(r4)     // Catch:{ all -> 0x0032 }
            com.badlogic.gdx.utils.ObjectMap r2 = (com.badlogic.gdx.utils.ObjectMap) r2     // Catch:{ all -> 0x0032 }
            com.badlogic.gdx.utils.ObjectMap$Keys r3 = r2.keys()     // Catch:{ all -> 0x0032 }
            java.util.Iterator r3 = r3.iterator()     // Catch:{ all -> 0x0032 }
        L_0x0015:
            boolean r4 = r3.hasNext()     // Catch:{ all -> 0x0032 }
            if (r4 != 0) goto L_0x001e
            r3 = 0
        L_0x001c:
            monitor-exit(r5)
            return r3
        L_0x001e:
            java.lang.Object r0 = r3.next()     // Catch:{ all -> 0x0032 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ all -> 0x0032 }
            java.lang.Object r1 = r2.get(r0)     // Catch:{ all -> 0x0032 }
            if (r1 == r6) goto L_0x0030
            boolean r4 = r6.equals(r1)     // Catch:{ all -> 0x0032 }
            if (r4 == 0) goto L_0x0015
        L_0x0030:
            r3 = r0
            goto L_0x001c
        L_0x0032:
            r3 = move-exception
            monitor-exit(r5)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.assets.AssetManager.getAssetFileName(java.lang.Object):java.lang.String");
    }

    public synchronized boolean isLoaded(String fileName) {
        return this.assetTypes.containsKey(fileName);
    }

    public synchronized <T> void preload(String fileName, Class<T> type) {
        preload(fileName, type, null);
    }

    public synchronized <T> void preload(String fileName, Class<T> type, AssetLoaderParameters<T> parameter) {
        if (this.loaders.get(type) == null) {
            throw new GdxRuntimeException("No loader for type '" + type.getSimpleName() + "'");
        }
        if (this.preloadQueue.size == 0) {
            this.loaded = 0;
            this.toLoad = 0;
        }
        this.toLoad++;
        this.preloadQueue.add(new AssetDescriptor(fileName, type, parameter));
    }

    public synchronized boolean update() {
        boolean handleTaskError;
        try {
            if (this.tasks.size() == 0) {
                if (this.preloadQueue.size == 0) {
                    handleTaskError = true;
                } else {
                    nextTask();
                }
            }
            handleTaskError = updateTask() && this.preloadQueue.size == 0;
        } catch (Throwable th) {
            handleTaskError = handleTaskError(th);
        }
        return handleTaskError;
    }

    /* access modifiers changed from: package-private */
    public synchronized void injectTask(AssetDescriptor assetDesc) {
        if (!isLoaded(assetDesc.fileName)) {
            AssetLoader loader = this.loaders.get(assetDesc.type);
            if (loader == null) {
                throw new GdxRuntimeException("No loader for type '" + assetDesc.type.getSimpleName() + "'");
            }
            this.tasks.push(new AssetLoadingTask(this, assetDesc, loader, this.threadPool));
        }
    }

    private void nextTask() {
        AssetDescriptor assetDesc = this.preloadQueue.removeIndex(0);
        AssetLoader loader = this.loaders.get(assetDesc.type);
        if (loader == null) {
            throw new GdxRuntimeException("No loader for type '" + assetDesc.type.getSimpleName() + "'");
        }
        this.tasks.push(new AssetLoadingTask(this, assetDesc, loader, this.threadPool));
    }

    private boolean updateTask() {
        AssetLoadingTask task = this.tasks.peek();
        if (!task.update()) {
            return false;
        }
        this.assetTypes.put(task.assetDesc.fileName, task.assetDesc.type);
        ObjectMap<String, Object> typeToAssets = this.assets.get(task.assetDesc.type);
        if (typeToAssets == null) {
            typeToAssets = new ObjectMap<>();
            this.assets.put(task.assetDesc.type, typeToAssets);
        }
        typeToAssets.put(task.assetDesc.fileName, task.getAsset());
        this.loaded++;
        this.tasks.pop();
        return true;
    }

    private boolean handleTaskError(Throwable t) {
        AssetDescriptor assetDesc = this.tasks.pop().assetDesc;
        if (this.listener != null) {
            this.listener.error(assetDesc.fileName, assetDesc.type, t);
            return this.preloadQueue.size == 0;
        }
        throw new GdxRuntimeException(t);
    }

    public synchronized <T, P> void setLoader(Class<T> type, AssetLoader<T, P> loader) {
        this.loaders.put(type, loader);
    }

    public synchronized int getLoadedAssets() {
        return this.assetTypes.size;
    }

    public synchronized int getQueuedAssets() {
        return this.preloadQueue.size + this.tasks.size();
    }

    public synchronized float getProgress() {
        return ((float) this.loaded) / ((float) this.toLoad);
    }

    public synchronized void setErrorListener(AssetErrorListener listener2) {
        this.listener = listener2;
    }

    public synchronized void dispose() {
        this.threadPool.shutdown();
        Iterator<String> it = this.assetTypes.keys().toArray().iterator();
        while (it.hasNext()) {
            remove(it.next());
        }
    }

    public synchronized void clear() {
        if (this.preloadQueue.size > 0 || this.tasks.size() != 0) {
            do {
                try {
                } catch (Throwable t) {
                    handleTaskError(t);
                }
            } while (!updateTask());
            this.preloadQueue.clear();
            this.tasks.clear();
        }
        Iterator<String> it = this.assetTypes.keys().toArray().iterator();
        while (it.hasNext()) {
            remove(it.next());
        }
        this.assets.clear();
        this.assetTypes.clear();
        this.loaded = 0;
        return;
    }
}
