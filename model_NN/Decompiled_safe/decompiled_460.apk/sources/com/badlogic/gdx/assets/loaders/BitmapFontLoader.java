package com.badlogic.gdx.assets.loaders;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;

public class BitmapFontLoader implements AsynchronousAssetLoader<BitmapFont, BitmapFontParameter> {
    BitmapFont.BitmapFontData data;
    String fileName;
    AssetManager manager;
    BitmapFontParameter parameter;

    public Array<AssetDescriptor> getDependencies(String fileName2, BitmapFontParameter parameter2) {
        this.data = new BitmapFont.BitmapFontData(Gdx.files.internal(fileName2), parameter2 != null ? parameter2.flip : false);
        Array<AssetDescriptor> deps = new Array<>();
        deps.add(new AssetDescriptor(this.data.getImageFile(), Texture.class, null));
        return deps;
    }

    public void loadAsync(AssetManager manager2, String fileName2, BitmapFontParameter parameter2) {
        this.manager = manager2;
        this.fileName = fileName2;
        this.parameter = parameter2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.graphics.g2d.BitmapFont$BitmapFontData, com.badlogic.gdx.graphics.g2d.TextureRegion, boolean):void
     arg types: [com.badlogic.gdx.graphics.g2d.BitmapFont$BitmapFontData, com.badlogic.gdx.graphics.g2d.TextureRegion, int]
     candidates:
      com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.files.FileHandle, boolean):void
      com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.graphics.g2d.TextureRegion, boolean):void
      com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.graphics.g2d.BitmapFont$BitmapFontData, com.badlogic.gdx.graphics.g2d.TextureRegion, boolean):void */
    public BitmapFont loadSync() {
        FileHandle internal = Gdx.files.internal(this.fileName);
        return new BitmapFont(this.data, new TextureRegion((Texture) this.manager.get(this.data.getImageFile(), Texture.class)), true);
    }
}
