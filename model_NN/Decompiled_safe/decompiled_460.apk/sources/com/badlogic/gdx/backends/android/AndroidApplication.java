package com.badlogic.gdx.backends.android;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.opengl.GLSurfaceView;
import android.os.Build;
import android.os.Debug;
import android.os.Handler;
import android.os.PowerManager;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import com.badlogic.gdx.Application;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Audio;
import com.badlogic.gdx.Files;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.backends.android.surfaceview.FillResolutionStrategy;
import com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewCupcake;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.utils.GdxNativesLoader;
import java.util.ArrayList;
import java.util.List;

public class AndroidApplication extends Activity implements Application {
    protected AndroidAudio audio;
    protected AndroidFiles files;
    protected boolean firstResume = true;
    protected AndroidGraphics graphics;
    protected Handler handler;
    protected AndroidInput input;
    protected ApplicationListener listener;
    protected int logLevel = 1;
    protected final List<Runnable> runnables = new ArrayList();
    protected PowerManager.WakeLock wakeLock = null;

    static {
        GdxNativesLoader.load();
    }

    public void initialize(ApplicationListener listener2, boolean useGL2IfAvailable) {
        AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        config.useGL20 = useGL2IfAvailable;
        initialize(listener2, config);
    }

    public void initialize(ApplicationListener listener2, AndroidApplicationConfiguration config) {
        this.graphics = new AndroidGraphics(this, config, config.resolutionStrategy == null ? new FillResolutionStrategy() : config.resolutionStrategy);
        this.input = new AndroidInput(this, this.graphics.view, config);
        this.audio = new AndroidAudio(this);
        this.files = new AndroidFiles(getAssets());
        this.listener = listener2;
        this.handler = new Handler();
        Gdx.app = this;
        Gdx.input = getInput();
        Gdx.audio = getAudio();
        Gdx.files = getFiles();
        Gdx.graphics = getGraphics();
        try {
            requestWindowFeature(1);
        } catch (Exception ex) {
            log("AndroidApplication", "Content already displayed, cannot request FEATURE_NO_TITLE", ex);
        }
        getWindow().setFlags(1024, 1024);
        getWindow().clearFlags(GL10.GL_EXP);
        setContentView(this.graphics.getView(), createLayoutParams());
        createWakeLock(config);
    }

    /* access modifiers changed from: protected */
    public FrameLayout.LayoutParams createLayoutParams() {
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, -1);
        layoutParams.gravity = 17;
        return layoutParams;
    }

    /* access modifiers changed from: protected */
    public void createWakeLock(AndroidApplicationConfiguration config) {
        if (config.useWakelock) {
            this.wakeLock = ((PowerManager) getSystemService("power")).newWakeLock(26, "libgdx wakelock");
        }
    }

    public View initializeForView(ApplicationListener listener2, boolean useGL2IfAvailable) {
        AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        config.useGL20 = useGL2IfAvailable;
        return initializeForView(listener2, config);
    }

    public View initializeForView(ApplicationListener listener2, AndroidApplicationConfiguration config) {
        this.graphics = new AndroidGraphics(this, config, config.resolutionStrategy == null ? new FillResolutionStrategy() : config.resolutionStrategy);
        this.input = new AndroidInput(this, this.graphics.view, config);
        this.audio = new AndroidAudio(this);
        this.files = new AndroidFiles(getAssets());
        this.listener = listener2;
        this.handler = new Handler();
        Gdx.app = this;
        Gdx.input = getInput();
        Gdx.audio = getAudio();
        Gdx.files = getFiles();
        Gdx.graphics = getGraphics();
        createWakeLock(config);
        return this.graphics.getView();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        if (this.wakeLock != null) {
            this.wakeLock.release();
        }
        this.graphics.pause();
        this.input.unregisterSensorListeners();
        int[] realId = this.input.realId;
        for (int i = 0; i < realId.length; i++) {
            realId[i] = -1;
        }
        if (isFinishing()) {
            this.graphics.clearManagedCaches();
            this.graphics.destroy();
        }
        if (!(this.graphics == null || this.graphics.view == null)) {
            if (this.graphics.view instanceof GLSurfaceViewCupcake) {
                ((GLSurfaceViewCupcake) this.graphics.view).onPause();
            }
            if (this.graphics.view instanceof GLSurfaceView) {
                ((GLSurfaceView) this.graphics.view).onPause();
            }
        }
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        if (this.wakeLock != null) {
            this.wakeLock.acquire();
        }
        Gdx.app = this;
        Gdx.input = getInput();
        Gdx.audio = getAudio();
        Gdx.files = getFiles();
        Gdx.graphics = getGraphics();
        ((AndroidInput) getInput()).registerSensorListeners();
        if (this.audio != null) {
            this.audio.resume();
        }
        if (!(this.graphics == null || this.graphics.view == null)) {
            if (this.graphics.view instanceof GLSurfaceViewCupcake) {
                ((GLSurfaceViewCupcake) this.graphics.view).onResume();
            }
            if (this.graphics.view instanceof GLSurfaceView) {
                ((GLSurfaceView) this.graphics.view).onResume();
            }
        }
        if (!this.firstResume) {
            this.graphics.resume();
        } else {
            this.firstResume = false;
        }
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    public Audio getAudio() {
        return this.audio;
    }

    public Files getFiles() {
        return this.files;
    }

    public Graphics getGraphics() {
        return this.graphics;
    }

    public Input getInput() {
        return this.input;
    }

    public Application.ApplicationType getType() {
        return Application.ApplicationType.Android;
    }

    public int getVersion() {
        return Integer.parseInt(Build.VERSION.SDK);
    }

    public long getJavaHeap() {
        return Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
    }

    public long getNativeHeap() {
        return Debug.getNativeHeapAllocatedSize();
    }

    public Preferences getPreferences(String name) {
        return new AndroidPreferences(getSharedPreferences(name, 0));
    }

    public void postRunnable(Runnable runnable) {
        synchronized (this.runnables) {
            this.runnables.add(runnable);
        }
    }

    public void onConfigurationChanged(Configuration config) {
        super.onConfigurationChanged(config);
        boolean keyboardAvailable = false;
        if (config.keyboardHidden == 1) {
            keyboardAvailable = true;
        }
        this.input.keyboardAvailable = keyboardAvailable;
    }

    public void exit() {
        this.handler.post(new Runnable() {
            public void run() {
                AndroidApplication.this.finish();
            }
        });
    }

    public void log(String tag, String message) {
        if (this.logLevel >= 1) {
            Log.i(tag, message);
        }
    }

    public void log(String tag, String message, Exception exception) {
        if (this.logLevel >= 1) {
            Log.i(tag, message, exception);
        }
    }

    public void error(String tag, String message) {
        if (this.logLevel >= 2) {
            Log.e(tag, message);
        }
    }

    public void error(String tag, String message, Exception exception) {
        if (this.logLevel >= 2) {
            Log.e(tag, message, exception);
        }
    }

    public void setLogLevel(int logLevel2) {
        this.logLevel = logLevel2;
    }

    public void gotoHttp() {
        Intent i = new Intent("android.intent.action.VIEW");
        i.setData(Uri.parse("http://machaonstudio.blogspot.com"));
        startActivity(i);
    }

    public void sendEmail() {
        Intent i = new Intent("android.intent.action.SEND");
        i.setType("text/plain");
        i.putExtra("android.intent.extra.EMAIL", new String[]{"machaon.studio@gmail.com"});
        i.putExtra("android.intent.extra.SUBJECT", "");
        i.putExtra("android.intent.extra.TEXT", "");
        try {
            startActivity(Intent.createChooser(i, "Send mail..."));
        } catch (ActivityNotFoundException e) {
        }
    }

    public void recommendEng() {
        Intent i = new Intent("android.intent.action.SEND");
        i.setType("text/plain");
        i.putExtra("android.intent.extra.EMAIL", new String[]{""});
        i.putExtra("android.intent.extra.SUBJECT", "Addictive puzzle game");
        i.putExtra("android.intent.extra.TEXT", "Hi! I have a good game for the android - 'Quadratum'. Look, I think you'll like it. :) \nhttps://market.android.com/details?id=com.machaon.quadratum");
        try {
            startActivity(Intent.createChooser(i, "Send mail..."));
        } catch (ActivityNotFoundException e) {
        }
    }

    public void recommendRus() {
        Intent i = new Intent("android.intent.action.SEND");
        i.setType("text/plain");
        i.putExtra("android.intent.extra.EMAIL", new String[]{""});
        i.putExtra("android.intent.extra.SUBJECT", "Игра-головоломка");
        i.putExtra("android.intent.extra.TEXT", "Привет! Я играю в одну хорошую игру для андроида - Quadratum. Посмотри, думаю тебе понравится. \nhttps://market.android.com/details?id=com.machaon.quadratum");
        try {
            startActivity(Intent.createChooser(i, "Send mail..."));
        } catch (ActivityNotFoundException e) {
        }
    }

    public void gotoMarket(byte version) {
        String url = "market://details?id=com.machaon.quadratum";
        if (version == 2) {
            url = "market://details?id=com.machaon.quadratumfull";
        }
        Intent i = new Intent("android.intent.action.VIEW");
        i.setData(Uri.parse(url));
        startActivity(i);
    }
}
