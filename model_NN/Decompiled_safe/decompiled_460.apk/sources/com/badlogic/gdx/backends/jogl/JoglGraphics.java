package com.badlogic.gdx.backends.jogl;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics;
import com.badlogic.gdx.backends.openal.OpenALAudio;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.utils.GdxRuntimeException;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.DisplayMode;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Toolkit;
import java.awt.Window;
import java.util.List;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCanvas;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLCapabilitiesChooser;
import javax.media.opengl.GLEventListener;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class JoglGraphics extends JoglGraphicsBase implements GLEventListener {
    final JoglApplicationConfiguration config;
    boolean created = false;
    final JoglDisplayMode desktopMode;
    boolean exclusiveMode = false;
    String extensions;
    ApplicationListener listener = null;
    boolean useGL2;

    public JoglGraphics(ApplicationListener listener2, JoglApplicationConfiguration config2) {
        initialize(config2);
        if (listener2 == null) {
            throw new GdxRuntimeException("RenderListener must not be null");
        }
        this.listener = listener2;
        this.config = config2;
        this.desktopMode = (JoglDisplayMode) JoglApplicationConfiguration.getDesktopDisplayMode();
    }

    public void create() {
        super.create();
    }

    public void pause() {
        super.pause();
        this.canvas.getContext().makeCurrent();
        this.listener.pause();
    }

    public void resume() {
        this.canvas.getContext().makeCurrent();
        this.listener.resume();
        super.resume();
    }

    public void init(GLAutoDrawable drawable) {
        initializeGLInstances(drawable);
        setVSync(this.config.vSyncEnabled);
        if (!this.created) {
            this.listener.create();
            synchronized (this) {
                this.paused = false;
            }
            this.created = true;
        }
    }

    public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
        this.listener.resize(width, height);
    }

    public void display(GLAutoDrawable arg0) {
        synchronized (this) {
            if (!this.paused) {
                updateTimes();
                synchronized (((JoglApplication) Gdx.app).runnables) {
                    List<Runnable> runnables = ((JoglApplication) Gdx.app).runnables;
                    for (int i = 0; i < runnables.size(); i++) {
                        runnables.get(i).run();
                    }
                    runnables.clear();
                }
                ((JoglInput) ((JoglApplication) Gdx.app).getInput()).processEvents();
                this.listener.render();
                ((OpenALAudio) Gdx.audio).update();
            }
        }
    }

    public void displayChanged(GLAutoDrawable arg0, boolean arg1, boolean arg2) {
    }

    public void destroy() {
        this.canvas.getContext().makeCurrent();
        this.listener.dispose();
        GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().setFullScreenWindow((Window) null);
    }

    public float getPpiX() {
        return (float) Toolkit.getDefaultToolkit().getScreenResolution();
    }

    public float getPpiY() {
        return (float) Toolkit.getDefaultToolkit().getScreenResolution();
    }

    public float getPpcX() {
        return ((float) Toolkit.getDefaultToolkit().getScreenResolution()) / 2.54f;
    }

    public float getPpcY() {
        return ((float) Toolkit.getDefaultToolkit().getScreenResolution()) / 2.54f;
    }

    public float getDensity() {
        return ((float) Toolkit.getDefaultToolkit().getScreenResolution()) / 160.0f;
    }

    public boolean supportsDisplayModeChange() {
        return GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().isFullScreenSupported() && (Gdx.app instanceof JoglApplication);
    }

    protected static class JoglDisplayMode extends Graphics.DisplayMode {
        final DisplayMode mode;

        protected JoglDisplayMode(int width, int height, int refreshRate, int bitsPerPixel, DisplayMode mode2) {
            super(width, height, refreshRate, bitsPerPixel);
            this.mode = mode2;
        }
    }

    public Graphics.DisplayMode[] getDisplayModes() {
        return JoglApplicationConfiguration.getDisplayModes();
    }

    public void setTitle(String title) {
        for (Container parent = this.canvas.getParent(); parent != null; parent = parent.getParent()) {
            if (parent instanceof JFrame) {
                ((JFrame) parent).setTitle(title);
                return;
            }
        }
    }

    public void setIcon(Pixmap pixmap) {
    }

    public Graphics.DisplayMode getDesktopDisplayMode() {
        return this.desktopMode;
    }

    public boolean setDisplayMode(int width, int height, boolean fullscreen) {
        if (!supportsDisplayModeChange()) {
            return false;
        }
        if (!fullscreen) {
            setWindowedMode(width, height);
        } else {
            Graphics.DisplayMode mode = findBestMatch(width, height);
            if (mode == null) {
                return false;
            }
            setDisplayMode(mode);
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public JoglDisplayMode findBestMatch(int width, int height) {
        Graphics.DisplayMode best = null;
        for (Graphics.DisplayMode mode : getDisplayModes()) {
            if (mode.width == width && mode.height == height && mode.bitsPerPixel == this.desktopMode.bitsPerPixel) {
                int maxBitDepth = mode.bitsPerPixel;
                best = mode;
            }
        }
        return (JoglDisplayMode) best;
    }

    public boolean setDisplayMode(Graphics.DisplayMode displayMode) {
        if (!supportsDisplayModeChange()) {
            return false;
        }
        GraphicsDevice device = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
        final JFrame frame = findJFrame(this.canvas);
        if (frame == null) {
            return false;
        }
        super.pause();
        GLCanvas newCanvas = new GLCanvas(this.canvas.getChosenGLCapabilities(), (GLCapabilitiesChooser) null, this.canvas.getContext(), device);
        newCanvas.addGLEventListener(this);
        JFrame newframe = new JFrame(frame.getTitle());
        newframe.setUndecorated(true);
        newframe.setResizable(false);
        newframe.add(newCanvas, "Center");
        newframe.setDefaultCloseOperation(2);
        newframe.setLocationRelativeTo((Component) null);
        newframe.pack();
        newframe.setVisible(true);
        device.setFullScreenWindow(newframe);
        device.setDisplayMode(((JoglDisplayMode) displayMode).mode);
        initializeGLInstances(this.canvas);
        this.canvas = newCanvas;
        ((JoglInput) Gdx.input).setListeners(this.canvas);
        this.canvas.requestFocus();
        newframe.addWindowListener(((JoglApplication) Gdx.app).windowListener);
        ((JoglApplication) Gdx.app).frame = newframe;
        resume();
        Gdx.app.postRunnable(new Runnable() {
            public void run() {
                final JFrame jFrame = frame;
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        jFrame.dispose();
                    }
                });
            }
        });
        return true;
    }

    private boolean setWindowedMode(int width, int height) {
        GraphicsDevice device = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
        if (device.isDisplayChangeSupported()) {
            device.setDisplayMode(this.desktopMode.mode);
            device.setFullScreenWindow((Window) null);
            final JFrame frame = findJFrame(this.canvas);
            if (frame == null) {
                return false;
            }
            super.pause();
            GLCanvas newCanvas = new GLCanvas(this.canvas.getChosenGLCapabilities(), (GLCapabilitiesChooser) null, this.canvas.getContext(), device);
            newCanvas.setBackground(Color.BLACK);
            newCanvas.setPreferredSize(new Dimension(width, height));
            newCanvas.addGLEventListener(this);
            JFrame newframe = new JFrame(frame.getTitle());
            newframe.setUndecorated(false);
            newframe.setResizable(true);
            newframe.setSize(newframe.getInsets().left + width + newframe.getInsets().right, newframe.getInsets().top + newframe.getInsets().bottom + height);
            newframe.add(newCanvas, "Center");
            newframe.setDefaultCloseOperation(2);
            newframe.setLocationRelativeTo((Component) null);
            newframe.pack();
            newframe.setVisible(true);
            initializeGLInstances(this.canvas);
            this.canvas = newCanvas;
            ((JoglInput) Gdx.input).setListeners(this.canvas);
            this.canvas.requestFocus();
            newframe.addWindowListener(((JoglApplication) Gdx.app).windowListener);
            ((JoglApplication) Gdx.app).frame = newframe;
            resume();
            Gdx.app.postRunnable(new Runnable() {
                public void run() {
                    final JFrame jFrame = frame;
                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            jFrame.dispose();
                        }
                    });
                }
            });
        } else {
            JFrame frame2 = findJFrame(this.canvas);
            if (frame2 == null) {
                return false;
            }
            frame2.setSize(frame2.getInsets().left + width + frame2.getInsets().right, frame2.getInsets().top + frame2.getInsets().bottom + height);
        }
        return true;
    }

    protected static JFrame findJFrame(Component component) {
        for (Container parent = component.getParent(); parent != null; parent = parent.getParent()) {
            if (parent instanceof JFrame) {
                return (JFrame) parent;
            }
        }
        return null;
    }

    public void setVSync(boolean vsync) {
        if (vsync) {
            this.canvas.getGL().setSwapInterval(1);
        } else {
            this.canvas.getGL().setSwapInterval(0);
        }
    }

    public Graphics.BufferFormat getBufferFormat() {
        GLCapabilities caps = this.canvas.getChosenGLCapabilities();
        return new Graphics.BufferFormat(caps.getRedBits(), caps.getGreenBits(), caps.getBlueBits(), caps.getAlphaBits(), caps.getDepthBits(), caps.getStencilBits(), caps.getNumSamples(), false);
    }

    public boolean supportsExtension(String extension) {
        if (this.extensions == null) {
            this.extensions = Gdx.gl.glGetString(7939);
        }
        return this.extensions.contains(extension);
    }
}
