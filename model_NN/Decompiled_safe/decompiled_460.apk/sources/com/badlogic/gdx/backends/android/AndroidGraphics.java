package com.badlogic.gdx.backends.android;

import android.opengl.GLSurfaceView;
import android.os.Build;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics;
import com.badlogic.gdx.backends.android.surfaceview.GLSurfaceView20;
import com.badlogic.gdx.backends.android.surfaceview.GdxEglConfigChooser;
import com.badlogic.gdx.backends.android.surfaceview.ResolutionStrategy;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.GL11;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.GLCommon;
import com.badlogic.gdx.graphics.GLU;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.WindowedMean;
import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;

public final class AndroidGraphics implements Graphics, GLSurfaceView.Renderer {
    AndroidApplication app;
    private Graphics.BufferFormat bufferFormat = new Graphics.BufferFormat(5, 6, 5, 0, 16, 0, 0, false);
    private final AndroidApplicationConfiguration config;
    volatile boolean created = false;
    private float deltaTime = 0.0f;
    private float density = 1.0f;
    volatile boolean destroy = false;
    String extensions;
    private int fps;
    private long frameStart = System.nanoTime();
    private int frames = 0;
    GLCommon gl;
    GL10 gl10;
    GL11 gl11;
    GL20 gl20;
    GLU glu;
    int height;
    private long lastFrameTime = System.nanoTime();
    private WindowedMean mean = new WindowedMean(5);
    volatile boolean pause = false;
    private float ppcX = 0.0f;
    private float ppcY = 0.0f;
    private float ppiX = 0.0f;
    private float ppiY = 0.0f;
    volatile boolean resume = false;
    volatile boolean running = false;
    Object synch = new Object();
    int[] value = new int[1];
    final View view;
    int width;

    public AndroidGraphics(AndroidApplication activity, AndroidApplicationConfiguration config2, ResolutionStrategy resolutionStrategy) {
        this.config = config2;
        this.view = createGLSurfaceView(activity, config2.useGL20, resolutionStrategy);
        this.view.setFocusable(true);
        this.view.setFocusableInTouchMode(true);
        this.app = activity;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v1, resolved type: com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewCupcake} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v2, resolved type: com.badlogic.gdx.backends.android.surfaceview.DefaultGLSurfaceView} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v3, resolved type: com.badlogic.gdx.backends.android.surfaceview.GLSurfaceView20} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v4, resolved type: com.badlogic.gdx.backends.android.surfaceview.DefaultGLSurfaceView} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v5, resolved type: com.badlogic.gdx.backends.android.surfaceview.DefaultGLSurfaceView} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v6, resolved type: com.badlogic.gdx.backends.android.surfaceview.DefaultGLSurfaceView} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private android.view.View createGLSurfaceView(android.app.Activity r9, boolean r10, com.badlogic.gdx.backends.android.surfaceview.ResolutionStrategy r11) {
        /*
            r8 = this;
            android.opengl.GLSurfaceView$EGLConfigChooser r7 = r8.getEglConfigChooser()
            if (r10 == 0) goto L_0x0036
            boolean r1 = r8.checkGL20()
            if (r1 == 0) goto L_0x0036
            com.badlogic.gdx.backends.android.surfaceview.GLSurfaceView20 r0 = new com.badlogic.gdx.backends.android.surfaceview.GLSurfaceView20
            r0.<init>(r9, r11)
            if (r7 == 0) goto L_0x001a
            r0.setEGLConfigChooser(r7)
        L_0x0016:
            r0.setRenderer(r8)
        L_0x0019:
            return r0
        L_0x001a:
            com.badlogic.gdx.backends.android.AndroidApplicationConfiguration r1 = r8.config
            int r1 = r1.r
            com.badlogic.gdx.backends.android.AndroidApplicationConfiguration r2 = r8.config
            int r2 = r2.g
            com.badlogic.gdx.backends.android.AndroidApplicationConfiguration r3 = r8.config
            int r3 = r3.b
            com.badlogic.gdx.backends.android.AndroidApplicationConfiguration r4 = r8.config
            int r4 = r4.a
            com.badlogic.gdx.backends.android.AndroidApplicationConfiguration r5 = r8.config
            int r5 = r5.depth
            com.badlogic.gdx.backends.android.AndroidApplicationConfiguration r6 = r8.config
            int r6 = r6.stencil
            r0.setEGLConfigChooser(r1, r2, r3, r4, r5, r6)
            goto L_0x0016
        L_0x0036:
            java.lang.String r1 = android.os.Build.VERSION.SDK
            int r1 = java.lang.Integer.parseInt(r1)
            r2 = 4
            if (r1 > r2) goto L_0x0069
            com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewCupcake r0 = new com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewCupcake
            r0.<init>(r9, r11)
            if (r7 == 0) goto L_0x004d
            r0.setEGLConfigChooser(r7)
        L_0x0049:
            r0.setRenderer(r8)
            goto L_0x0019
        L_0x004d:
            com.badlogic.gdx.backends.android.AndroidApplicationConfiguration r1 = r8.config
            int r1 = r1.r
            com.badlogic.gdx.backends.android.AndroidApplicationConfiguration r2 = r8.config
            int r2 = r2.g
            com.badlogic.gdx.backends.android.AndroidApplicationConfiguration r3 = r8.config
            int r3 = r3.b
            com.badlogic.gdx.backends.android.AndroidApplicationConfiguration r4 = r8.config
            int r4 = r4.a
            com.badlogic.gdx.backends.android.AndroidApplicationConfiguration r5 = r8.config
            int r5 = r5.depth
            com.badlogic.gdx.backends.android.AndroidApplicationConfiguration r6 = r8.config
            int r6 = r6.stencil
            r0.setEGLConfigChooser(r1, r2, r3, r4, r5, r6)
            goto L_0x0049
        L_0x0069:
            com.badlogic.gdx.backends.android.surfaceview.DefaultGLSurfaceView r0 = new com.badlogic.gdx.backends.android.surfaceview.DefaultGLSurfaceView
            r0.<init>(r9, r11)
            if (r7 == 0) goto L_0x0077
            r0.setEGLConfigChooser(r7)
        L_0x0073:
            r0.setRenderer(r8)
            goto L_0x0019
        L_0x0077:
            com.badlogic.gdx.backends.android.AndroidApplicationConfiguration r1 = r8.config
            int r1 = r1.r
            com.badlogic.gdx.backends.android.AndroidApplicationConfiguration r2 = r8.config
            int r2 = r2.g
            com.badlogic.gdx.backends.android.AndroidApplicationConfiguration r3 = r8.config
            int r3 = r3.b
            com.badlogic.gdx.backends.android.AndroidApplicationConfiguration r4 = r8.config
            int r4 = r4.a
            com.badlogic.gdx.backends.android.AndroidApplicationConfiguration r5 = r8.config
            int r5 = r5.depth
            com.badlogic.gdx.backends.android.AndroidApplicationConfiguration r6 = r8.config
            int r6 = r6.stencil
            r0.setEGLConfigChooser(r1, r2, r3, r4, r5, r6)
            goto L_0x0073
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.backends.android.AndroidGraphics.createGLSurfaceView(android.app.Activity, boolean, com.badlogic.gdx.backends.android.surfaceview.ResolutionStrategy):android.view.View");
    }

    private GLSurfaceView.EGLConfigChooser getEglConfigChooser() {
        return new GdxEglConfigChooser(this.config.r, this.config.g, this.config.b, this.config.a, this.config.depth, this.config.stencil, this.config.numSamples, this.config.useGL20);
    }

    private void updatePpi() {
        DisplayMetrics metrics = new DisplayMetrics();
        this.app.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        this.ppiX = metrics.xdpi;
        this.ppiY = metrics.ydpi;
        this.ppcX = metrics.xdpi / 2.54f;
        this.ppcY = metrics.ydpi / 2.54f;
        this.density = metrics.density;
    }

    private boolean checkGL20() {
        EGL10 egl = (EGL10) EGLContext.getEGL();
        EGLDisplay display = egl.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);
        egl.eglInitialize(display, new int[2]);
        int[] num_config = new int[1];
        egl.eglChooseConfig(display, new int[]{12324, 4, 12323, 4, 12322, 4, 12352, 4, 12344}, new EGLConfig[10], 10, num_config);
        egl.eglTerminate(display);
        return num_config[0] > 0;
    }

    public GL10 getGL10() {
        return this.gl10;
    }

    public GL11 getGL11() {
        return this.gl11;
    }

    public GL20 getGL20() {
        return this.gl20;
    }

    public int getHeight() {
        return this.height;
    }

    public int getWidth() {
        return this.width;
    }

    public boolean isGL11Available() {
        return this.gl11 != null;
    }

    public boolean isGL20Available() {
        return this.gl20 != null;
    }

    private static boolean isPowerOfTwo(int value2) {
        return value2 != 0 && ((value2 - 1) & value2) == 0;
    }

    private void setupGL(javax.microedition.khronos.opengles.GL10 gl2) {
        String renderer;
        if (this.gl10 == null && this.gl20 == null) {
            if (this.view instanceof GLSurfaceView20) {
                this.gl20 = new AndroidGL20();
                this.gl = this.gl20;
            } else {
                this.gl10 = new AndroidGL10(gl2);
                this.gl = this.gl10;
                if ((gl2 instanceof javax.microedition.khronos.opengles.GL11) && (renderer = gl2.glGetString(7937)) != null && !renderer.toLowerCase().contains("pixelflinger") && !Build.MODEL.equals("MB200") && !Build.MODEL.equals("MB220") && !Build.MODEL.contains("Behold")) {
                    this.gl11 = new AndroidGL11((javax.microedition.khronos.opengles.GL11) gl2);
                    this.gl10 = this.gl11;
                }
            }
            this.glu = new AndroidGLU();
            Gdx.gl = this.gl;
            Gdx.gl10 = this.gl10;
            Gdx.gl11 = this.gl11;
            Gdx.gl20 = this.gl20;
            Gdx.glu = this.glu;
            Gdx.app.log("AndroidGraphics", "OGL renderer: " + gl2.glGetString(7937));
            Gdx.app.log("AndroidGraphics", "OGL vendor: " + gl2.glGetString(7936));
            Gdx.app.log("AndroidGraphics", "OGL version: " + gl2.glGetString(7938));
            Gdx.app.log("AndroidGraphics", "OGL extensions: " + gl2.glGetString(7939));
        }
    }

    public void onSurfaceChanged(javax.microedition.khronos.opengles.GL10 gl2, int width2, int height2) {
        this.width = width2;
        this.height = height2;
        updatePpi();
        gl2.glViewport(0, 0, this.width, this.height);
        if (!this.created) {
            this.app.listener.create();
            this.created = true;
            synchronized (this) {
                this.running = true;
            }
        }
        this.app.listener.resize(width2, height2);
    }

    public void onSurfaceCreated(javax.microedition.khronos.opengles.GL10 gl2, EGLConfig config2) {
        setupGL(gl2);
        logConfig(config2);
        updatePpi();
        Mesh.invalidateAllMeshes(this.app);
        Texture.invalidateAllTextures(this.app);
        ShaderProgram.invalidateAllShaderPrograms(this.app);
        FrameBuffer.invalidateAllFrameBuffers(this.app);
        Gdx.app.log("AndroidGraphics", Mesh.getManagedStatus());
        Gdx.app.log("AndroidGraphics", Texture.getManagedStatus());
        Gdx.app.log("AndroidGraphics", ShaderProgram.getManagedStatus());
        Gdx.app.log("AndroidGraphics", FrameBuffer.getManagedStatus());
        Display display = this.app.getWindowManager().getDefaultDisplay();
        this.width = display.getWidth();
        this.height = display.getHeight();
        this.mean = new WindowedMean(5);
        this.lastFrameTime = System.nanoTime();
        gl2.glViewport(0, 0, this.width, this.height);
    }

    private void logConfig(EGLConfig config2) {
        EGL10 egl = (EGL10) EGLContext.getEGL();
        EGLDisplay display = egl.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);
        int r = getAttrib(egl, display, config2, 12324, 0);
        int g = getAttrib(egl, display, config2, 12323, 0);
        int b = getAttrib(egl, display, config2, 12322, 0);
        int a = getAttrib(egl, display, config2, 12321, 0);
        int d = getAttrib(egl, display, config2, 12325, 0);
        int s = getAttrib(egl, display, config2, 12326, 0);
        int samples = Math.max(getAttrib(egl, display, config2, 12337, 0), getAttrib(egl, display, config2, GdxEglConfigChooser.EGL_COVERAGE_SAMPLES_NV, 0));
        boolean coverageSample = getAttrib(egl, display, config2, GdxEglConfigChooser.EGL_COVERAGE_SAMPLES_NV, 0) != 0;
        Gdx.app.log("AndroidGraphics", "framebuffer: (" + r + ", " + g + ", " + b + ", " + a + ")");
        Gdx.app.log("AndroidGraphics", "depthbuffer: (" + d + ")");
        Gdx.app.log("AndroidGraphics", "stencilbuffer: (" + s + ")");
        Gdx.app.log("AndroidGraphics", "samples: (" + samples + ")");
        Gdx.app.log("AndroidGraphics", "coverage sampling: (" + coverageSample + ")");
        this.bufferFormat = new Graphics.BufferFormat(r, g, b, a, d, s, samples, coverageSample);
    }

    private int getAttrib(EGL10 egl, EGLDisplay display, EGLConfig config2, int attrib, int defValue) {
        if (egl.eglGetConfigAttrib(display, config2, attrib, this.value)) {
            return this.value[0];
        }
        return defValue;
    }

    /* access modifiers changed from: package-private */
    public void resume() {
        synchronized (this.synch) {
            this.running = true;
            this.resume = true;
        }
    }

    /* access modifiers changed from: package-private */
    public void pause() {
        synchronized (this.synch) {
            if (this.running) {
                this.running = false;
                this.pause = true;
                while (this.pause) {
                    try {
                        this.synch.wait();
                    } catch (InterruptedException e) {
                    }
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void destroy() {
        synchronized (this.synch) {
            this.running = false;
            this.destroy = true;
            while (this.destroy) {
                try {
                    this.synch.wait();
                } catch (InterruptedException e) {
                }
            }
        }
    }

    public void onDrawFrame(javax.microedition.khronos.opengles.GL10 gl2) {
        boolean lrunning;
        boolean lpause;
        boolean ldestroy;
        boolean lresume;
        long time = System.nanoTime();
        this.deltaTime = ((float) (time - this.lastFrameTime)) / 1.0E9f;
        this.lastFrameTime = time;
        this.mean.addValue(this.deltaTime);
        synchronized (this.synch) {
            lrunning = this.running;
            lpause = this.pause;
            ldestroy = this.destroy;
            lresume = this.resume;
            if (this.resume) {
                this.resume = false;
            }
            if (this.pause) {
                this.pause = false;
                this.synch.notifyAll();
            }
            if (this.destroy) {
                this.destroy = false;
                this.synch.notifyAll();
            }
        }
        if (lresume) {
            this.app.listener.resume();
            Gdx.app.log("AndroidGraphics", "resumed");
        }
        if (lrunning) {
            synchronized (this.app.runnables) {
                for (int i = 0; i < this.app.runnables.size(); i++) {
                    this.app.runnables.get(i).run();
                }
                this.app.runnables.clear();
            }
            this.app.input.processEvents();
            this.app.listener.render();
        }
        if (lpause) {
            this.app.listener.pause();
            this.app.audio.pause();
            Gdx.app.log("AndroidGraphics", "paused");
        }
        if (ldestroy) {
            this.app.listener.dispose();
            this.app.audio.dispose();
            this.app.audio = null;
            Gdx.app.log("AndroidGraphics", "destroyed");
        }
        if (time - this.frameStart > 1000000000) {
            this.fps = this.frames;
            this.frames = 0;
            this.frameStart = time;
        }
        this.frames++;
    }

    public float getDeltaTime() {
        return this.mean.getMean() == 0.0f ? this.deltaTime : this.mean.getMean();
    }

    public Graphics.GraphicsType getType() {
        return Graphics.GraphicsType.AndroidGL;
    }

    public int getFramesPerSecond() {
        return this.fps;
    }

    public void clearManagedCaches() {
        Mesh.clearAllMeshes(this.app);
        Texture.clearAllTextures(this.app);
        ShaderProgram.clearAllShaderPrograms(this.app);
        FrameBuffer.clearAllFrameBuffers(this.app);
        Gdx.app.log("AndroidGraphics", Mesh.getManagedStatus());
        Gdx.app.log("AndroidGraphics", Texture.getManagedStatus());
        Gdx.app.log("AndroidGraphics", ShaderProgram.getManagedStatus());
        Gdx.app.log("AndroidGraphics", FrameBuffer.getManagedStatus());
    }

    public View getView() {
        return this.view;
    }

    public GLCommon getGLCommon() {
        return this.gl;
    }

    public float getPpiX() {
        return this.ppiX;
    }

    public float getPpiY() {
        return this.ppiY;
    }

    public float getPpcX() {
        return this.ppcX;
    }

    public float getPpcY() {
        return this.ppcY;
    }

    public float getDensity() {
        return this.density;
    }

    public GLU getGLU() {
        return this.glu;
    }

    public boolean supportsDisplayModeChange() {
        return false;
    }

    public boolean setDisplayMode(Graphics.DisplayMode displayMode) {
        return false;
    }

    public Graphics.DisplayMode[] getDisplayModes() {
        return new Graphics.DisplayMode[]{getDesktopDisplayMode()};
    }

    public boolean setDisplayMode(int width2, int height2, boolean fullscreen) {
        return false;
    }

    public void setTitle(String title) {
    }

    public void setIcon(Pixmap pixmap) {
    }

    private class AndroidDisplayMode extends Graphics.DisplayMode {
        protected AndroidDisplayMode(int width, int height, int refreshRate, int bitsPerPixel) {
            super(width, height, refreshRate, bitsPerPixel);
        }
    }

    public Graphics.DisplayMode getDesktopDisplayMode() {
        DisplayMetrics metrics = new DisplayMetrics();
        this.app.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        return new AndroidDisplayMode(metrics.widthPixels, metrics.heightPixels, 0, 0);
    }

    public Graphics.BufferFormat getBufferFormat() {
        return this.bufferFormat;
    }

    public void setVSync(boolean vsync) {
    }

    public boolean supportsExtension(String extension) {
        if (this.extensions == null) {
            this.extensions = Gdx.gl.glGetString(7939);
        }
        return this.extensions.contains(extension);
    }
}
