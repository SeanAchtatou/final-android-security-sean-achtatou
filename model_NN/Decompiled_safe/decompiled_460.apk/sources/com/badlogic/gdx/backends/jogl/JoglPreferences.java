package com.badlogic.gdx.backends.jogl;

import com.badlogic.gdx.Preferences;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class JoglPreferences implements Preferences {
    private final String name;
    private final Properties properties = new Properties();

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0037 A[SYNTHETIC, Splitter:B:13:0x0037] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0040 A[SYNTHETIC, Splitter:B:18:0x0040] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    JoglPreferences(java.lang.String r5) {
        /*
            r4 = this;
            r4.<init>()
            java.util.Properties r2 = new java.util.Properties
            r2.<init>()
            r4.properties = r2
            r4.name = r5
            r0 = 0
            java.io.BufferedInputStream r1 = new java.io.BufferedInputStream     // Catch:{ Throwable -> 0x0034, all -> 0x003d }
            com.badlogic.gdx.Files r2 = com.badlogic.gdx.Gdx.files     // Catch:{ Throwable -> 0x0034, all -> 0x003d }
            com.badlogic.gdx.files.FileHandle r2 = r2.external(r5)     // Catch:{ Throwable -> 0x0034, all -> 0x003d }
            java.io.InputStream r2 = r2.read()     // Catch:{ Throwable -> 0x0034, all -> 0x003d }
            r1.<init>(r2)     // Catch:{ Throwable -> 0x0034, all -> 0x003d }
            java.util.Properties r2 = r4.properties     // Catch:{ Throwable -> 0x004c, all -> 0x0049 }
            r2.loadFromXML(r1)     // Catch:{ Throwable -> 0x004c, all -> 0x0049 }
            if (r1 == 0) goto L_0x004f
            r1.close()     // Catch:{ Exception -> 0x0044 }
            r0 = r1
        L_0x0027:
            java.lang.Runtime r2 = java.lang.Runtime.getRuntime()
            com.badlogic.gdx.backends.jogl.JoglPreferences$1 r3 = new com.badlogic.gdx.backends.jogl.JoglPreferences$1
            r3.<init>()
            r2.addShutdownHook(r3)
            return
        L_0x0034:
            r2 = move-exception
        L_0x0035:
            if (r0 == 0) goto L_0x0027
            r0.close()     // Catch:{ Exception -> 0x003b }
            goto L_0x0027
        L_0x003b:
            r2 = move-exception
            goto L_0x0027
        L_0x003d:
            r2 = move-exception
        L_0x003e:
            if (r0 == 0) goto L_0x0043
            r0.close()     // Catch:{ Exception -> 0x0047 }
        L_0x0043:
            throw r2
        L_0x0044:
            r2 = move-exception
            r0 = r1
            goto L_0x0027
        L_0x0047:
            r3 = move-exception
            goto L_0x0043
        L_0x0049:
            r2 = move-exception
            r0 = r1
            goto L_0x003e
        L_0x004c:
            r2 = move-exception
            r0 = r1
            goto L_0x0035
        L_0x004f:
            r0 = r1
            goto L_0x0027
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.backends.jogl.JoglPreferences.<init>(java.lang.String):void");
    }

    public void putBoolean(String key, boolean val) {
        this.properties.put(key, Boolean.toString(val));
    }

    public void putInteger(String key, int val) {
        this.properties.put(key, Integer.toString(val));
    }

    public void putLong(String key, long val) {
        this.properties.put(key, Long.toString(val));
    }

    public void putFloat(String key, float val) {
        this.properties.put(key, Float.toString(val));
    }

    public void putString(String key, String val) {
        this.properties.put(key, val);
    }

    public void put(Map<String, ?> vals) {
        for (Map.Entry<String, ?> val : vals.entrySet()) {
            if (val.getValue() instanceof Boolean) {
                putBoolean((String) val.getKey(), ((Boolean) val.getValue()).booleanValue());
            }
            if (val.getValue() instanceof Integer) {
                putInteger((String) val.getKey(), ((Integer) val.getValue()).intValue());
            }
            if (val.getValue() instanceof Long) {
                putLong((String) val.getKey(), ((Long) val.getValue()).longValue());
            }
            if (val.getValue() instanceof String) {
                putString((String) val.getKey(), (String) val.getValue());
            }
            if (val.getValue() instanceof Float) {
                putFloat((String) val.getKey(), ((Float) val.getValue()).floatValue());
            }
        }
    }

    public boolean getBoolean(String key) {
        return getBoolean(key, false);
    }

    public int getInteger(String key) {
        return getInteger(key, 0);
    }

    public long getLong(String key) {
        return getLong(key, 0);
    }

    public float getFloat(String key) {
        return getFloat(key, 0.0f);
    }

    public String getString(String key) {
        return getString(key, "");
    }

    public boolean getBoolean(String key, boolean defValue) {
        return Boolean.parseBoolean(this.properties.getProperty(key, Boolean.toString(defValue)));
    }

    public int getInteger(String key, int defValue) {
        return Integer.parseInt(this.properties.getProperty(key, Integer.toString(defValue)));
    }

    public long getLong(String key, long defValue) {
        return Long.parseLong(this.properties.getProperty(key, Long.toString(defValue)));
    }

    public float getFloat(String key, float defValue) {
        return Float.parseFloat(this.properties.getProperty(key, Float.toString(defValue)));
    }

    public String getString(String key, String defValue) {
        return this.properties.getProperty(key, defValue);
    }

    public Map<String, ?> get() {
        Map<String, Object> map = new HashMap<>();
        for (Map.Entry<Object, Object> val : this.properties.entrySet()) {
            if (val.getValue() instanceof Boolean) {
                map.put((String) val.getKey(), Boolean.valueOf(Boolean.parseBoolean((String) val.getValue())));
            }
            if (val.getValue() instanceof Integer) {
                map.put((String) val.getKey(), Integer.valueOf(Integer.parseInt((String) val.getValue())));
            }
            if (val.getValue() instanceof Long) {
                map.put((String) val.getKey(), Long.valueOf(Long.parseLong((String) val.getValue())));
            }
            if (val.getValue() instanceof String) {
                map.put((String) val.getKey(), (String) val.getValue());
            }
            if (val.getValue() instanceof Float) {
                map.put((String) val.getKey(), Float.valueOf(Float.parseFloat((String) val.getValue())));
            }
        }
        return map;
    }

    public boolean contains(String key) {
        return this.properties.containsKey(key);
    }

    public void clear() {
        this.properties.clear();
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0023 A[SYNTHETIC, Splitter:B:11:0x0023] */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x002c A[SYNTHETIC, Splitter:B:16:0x002c] */
    /* JADX WARNING: Removed duplicated region for block: B:28:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void flush() {
        /*
            r4 = this;
            r0 = 0
            java.io.BufferedOutputStream r1 = new java.io.BufferedOutputStream     // Catch:{ Throwable -> 0x0020, all -> 0x0029 }
            com.badlogic.gdx.Files r2 = com.badlogic.gdx.Gdx.files     // Catch:{ Throwable -> 0x0020, all -> 0x0029 }
            java.lang.String r3 = r4.name     // Catch:{ Throwable -> 0x0020, all -> 0x0029 }
            com.badlogic.gdx.files.FileHandle r2 = r2.external(r3)     // Catch:{ Throwable -> 0x0020, all -> 0x0029 }
            r3 = 0
            java.io.OutputStream r2 = r2.write(r3)     // Catch:{ Throwable -> 0x0020, all -> 0x0029 }
            r1.<init>(r2)     // Catch:{ Throwable -> 0x0020, all -> 0x0029 }
            java.util.Properties r2 = r4.properties     // Catch:{ Throwable -> 0x0038, all -> 0x0035 }
            r3 = 0
            r2.storeToXML(r1, r3)     // Catch:{ Throwable -> 0x0038, all -> 0x0035 }
            if (r1 == 0) goto L_0x003b
            r1.close()     // Catch:{ Exception -> 0x0030 }
            r0 = r1
        L_0x001f:
            return
        L_0x0020:
            r2 = move-exception
        L_0x0021:
            if (r0 == 0) goto L_0x001f
            r0.close()     // Catch:{ Exception -> 0x0027 }
            goto L_0x001f
        L_0x0027:
            r2 = move-exception
            goto L_0x001f
        L_0x0029:
            r2 = move-exception
        L_0x002a:
            if (r0 == 0) goto L_0x002f
            r0.close()     // Catch:{ Exception -> 0x0033 }
        L_0x002f:
            throw r2
        L_0x0030:
            r2 = move-exception
            r0 = r1
            goto L_0x001f
        L_0x0033:
            r3 = move-exception
            goto L_0x002f
        L_0x0035:
            r2 = move-exception
            r0 = r1
            goto L_0x002a
        L_0x0038:
            r2 = move-exception
            r0 = r1
            goto L_0x0021
        L_0x003b:
            r0 = r1
            goto L_0x001f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.backends.jogl.JoglPreferences.flush():void");
    }
}
