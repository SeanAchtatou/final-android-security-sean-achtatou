package com.badlogic.gdx.backends.jogl;

import com.badlogic.gdx.Graphics;
import com.badlogic.gdx.backends.jogl.JoglGraphics;
import java.awt.DisplayMode;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.util.ArrayList;

public class JoglApplicationConfiguration {
    public int a = 8;
    public int b = 8;
    public int depth = 16;
    public boolean fullscreen = false;
    public int g = 8;
    public int height = 320;
    public int r = 8;
    public int samples = 0;
    public int stencil = 0;
    public String title = "Jogl Application";
    public boolean useGL20 = false;
    public boolean vSyncEnabled = false;
    public int width = 480;

    public void setFromDisplayMode(Graphics.DisplayMode mode) {
        this.width = mode.width;
        this.height = mode.height;
        if (mode.bitsPerPixel == 16) {
            this.r = 5;
            this.g = 6;
            this.b = 5;
            this.a = 0;
        }
        if (mode.bitsPerPixel == 24) {
            this.r = 8;
            this.g = 8;
            this.b = 8;
            this.a = 0;
        }
        if (mode.bitsPerPixel == 32) {
            this.r = 8;
            this.g = 8;
            this.b = 8;
            this.a = 8;
        }
        this.fullscreen = true;
    }

    public static Graphics.DisplayMode getDesktopDisplayMode() {
        DisplayMode mode = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode();
        return new JoglGraphics.JoglDisplayMode(mode.getWidth(), mode.getHeight(), mode.getRefreshRate(), mode.getBitDepth(), mode);
    }

    public static Graphics.DisplayMode[] getDisplayModes() {
        GraphicsDevice device = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
        DisplayMode desktopMode = device.getDisplayMode();
        DisplayMode[] displayModes = device.getDisplayModes();
        ArrayList<Graphics.DisplayMode> modes = new ArrayList<>();
        for (DisplayMode mode : displayModes) {
            boolean duplicate = false;
            int i = 0;
            while (true) {
                if (i < modes.size()) {
                    if (((Graphics.DisplayMode) modes.get(i)).width == mode.getWidth() && ((Graphics.DisplayMode) modes.get(i)).height == mode.getHeight() && ((Graphics.DisplayMode) modes.get(i)).bitsPerPixel == mode.getBitDepth()) {
                        duplicate = true;
                        break;
                    }
                    i++;
                } else {
                    break;
                }
            }
            if (!duplicate && mode.getBitDepth() == desktopMode.getBitDepth()) {
                modes.add(new JoglGraphics.JoglDisplayMode(mode.getWidth(), mode.getHeight(), mode.getRefreshRate(), mode.getBitDepth(), mode));
            }
        }
        return (Graphics.DisplayMode[]) modes.toArray(new Graphics.DisplayMode[modes.size()]);
    }
}
