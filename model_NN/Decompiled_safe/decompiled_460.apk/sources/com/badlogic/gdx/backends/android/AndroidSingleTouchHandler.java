package com.badlogic.gdx.backends.android;

import android.view.MotionEvent;
import com.badlogic.gdx.backends.android.AndroidInput;

public class AndroidSingleTouchHandler implements AndroidTouchHandler {
    public void onTouch(MotionEvent event, AndroidInput input) {
        int x = (int) event.getX();
        int y = (int) event.getY();
        int oldX = input.touchX[0];
        int oldY = input.touchY[0];
        input.touchX[0] = x;
        input.touchY[0] = y;
        if (event.getAction() == 0) {
            postTouchEvent(input, 0, x, y, 0);
            input.touched[0] = true;
            input.deltaX[0] = 0;
            input.deltaY[0] = 0;
        } else if (event.getAction() == 2) {
            postTouchEvent(input, 2, x, y, 0);
            input.touched[0] = true;
            input.deltaX[0] = x - oldX;
            input.deltaY[0] = y - oldY;
        } else if (event.getAction() == 1) {
            postTouchEvent(input, 1, x, y, 0);
            input.touched[0] = false;
            input.deltaX[0] = 0;
            input.deltaY[0] = 0;
        } else if (event.getAction() == 3) {
            postTouchEvent(input, 1, x, y, 0);
            input.touched[0] = false;
            input.deltaX[0] = 0;
            input.deltaY[0] = 0;
        }
    }

    private void postTouchEvent(AndroidInput input, int type, int x, int y, int pointer) {
        long timeStamp = System.nanoTime();
        synchronized (input) {
            AndroidInput.TouchEvent event = input.usedTouchEvents.obtain();
            event.timeStamp = timeStamp;
            event.pointer = 0;
            event.x = x;
            event.y = y;
            event.type = type;
            input.touchEvents.add(event);
        }
    }

    public boolean supportsMultitouch(AndroidApplication activity) {
        return false;
    }
}
