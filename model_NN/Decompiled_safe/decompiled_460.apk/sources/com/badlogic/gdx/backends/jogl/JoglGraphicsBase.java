package com.badlogic.gdx.backends.jogl;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.GL11;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.GLCommon;
import com.badlogic.gdx.graphics.GLU;
import java.awt.Color;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCanvas;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLEventListener;

public abstract class JoglGraphicsBase implements Graphics, GLEventListener {
    static int major;
    static int minor;
    JoglAnimator animator;
    GLCanvas canvas;
    float deltaTime = 0.0f;
    int fps;
    long frameStart = System.nanoTime();
    int frames;
    GLCommon gl;
    GL10 gl10;
    GL11 gl11;
    GL20 gl20;
    GLU glu;
    long lastFrameTime = System.nanoTime();
    boolean paused = true;
    boolean useGL2;

    /* access modifiers changed from: package-private */
    public void initialize(JoglApplicationConfiguration config) {
        GLCapabilities caps = new GLCapabilities();
        caps.setRedBits(config.r);
        caps.setGreenBits(config.g);
        caps.setBlueBits(config.b);
        caps.setAlphaBits(config.a);
        caps.setDepthBits(config.depth);
        caps.setStencilBits(config.stencil);
        caps.setNumSamples(config.samples);
        caps.setSampleBuffers(config.samples > 0);
        caps.setDoubleBuffered(true);
        this.canvas = new GLCanvas(caps);
        this.canvas.setBackground(Color.BLACK);
        this.canvas.addGLEventListener(this);
        this.useGL2 = config.useGL20;
        this.glu = new JoglGLU();
        Gdx.glu = this.glu;
    }

    /* access modifiers changed from: package-private */
    public GLCanvas getCanvas() {
        return this.canvas;
    }

    /* access modifiers changed from: package-private */
    public void create() {
        this.frameStart = System.nanoTime();
        this.lastFrameTime = this.frameStart;
        this.deltaTime = 0.0f;
        this.animator = new JoglAnimator(this.canvas);
        this.animator.start();
    }

    /* access modifiers changed from: package-private */
    public void pause() {
        synchronized (this) {
            this.paused = true;
        }
        this.animator.stop();
    }

    /* access modifiers changed from: package-private */
    public void resume() {
        this.paused = false;
        this.frameStart = System.nanoTime();
        this.lastFrameTime = this.frameStart;
        this.deltaTime = 0.0f;
        this.animator = new JoglAnimator(this.canvas);
        this.animator.setRunAsFastAsPossible(true);
        this.animator.start();
    }

    /* access modifiers changed from: package-private */
    public void initializeGLInstances(GLAutoDrawable drawable) {
        String version = drawable.getGL().glGetString(7938);
        String renderer = drawable.getGL().glGetString(7937);
        major = Integer.parseInt(new StringBuilder().append(version.charAt(0)).toString());
        minor = Integer.parseInt(new StringBuilder().append(version.charAt(2)).toString());
        if (!this.useGL2 || major < 2) {
            if ((major != 1 || minor >= 5) && !renderer.equals("Mirage Graphics3")) {
                this.gl11 = new JoglGL11(drawable.getGL());
                this.gl10 = this.gl11;
            } else {
                this.gl10 = new JoglGL10(drawable.getGL());
            }
            this.gl = this.gl10;
        } else {
            this.gl20 = new JoglGL20(drawable.getGL());
            this.gl = this.gl20;
        }
        Gdx.gl = this.gl;
        Gdx.gl10 = this.gl10;
        Gdx.gl11 = this.gl11;
        Gdx.gl20 = this.gl20;
    }

    /* access modifiers changed from: package-private */
    public void updateTimes() {
        this.deltaTime = ((float) (System.nanoTime() - this.lastFrameTime)) / 1.0E9f;
        this.lastFrameTime = System.nanoTime();
        if (System.nanoTime() - this.frameStart > 1000000000) {
            this.fps = this.frames;
            this.frames = 0;
            this.frameStart = System.nanoTime();
        }
        this.frames++;
    }

    public float getDeltaTime() {
        return this.deltaTime;
    }

    public int getFramesPerSecond() {
        return this.fps;
    }

    public int getHeight() {
        return this.canvas.getHeight();
    }

    public int getWidth() {
        return this.canvas.getWidth();
    }

    public GL10 getGL10() {
        return this.gl10;
    }

    public GL11 getGL11() {
        return this.gl11;
    }

    public GL20 getGL20() {
        return this.gl20;
    }

    public GLCommon getGLCommon() {
        return this.gl;
    }

    public GLU getGLU() {
        return this.glu;
    }

    public boolean isGL11Available() {
        return this.gl11 != null;
    }

    public boolean isGL20Available() {
        return this.gl20 != null;
    }

    public Graphics.GraphicsType getType() {
        return Graphics.GraphicsType.JoglGL;
    }
}
