package com.badlogic.gdx.backends.openal;

import com.badlogic.gdx.Audio;
import com.badlogic.gdx.audio.AudioDevice;
import com.badlogic.gdx.backends.openal.Ogg;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.IntArray;
import com.badlogic.gdx.utils.ObjectMap;
import java.nio.FloatBuffer;
import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;
import org.lwjgl.openal.AL;
import org.lwjgl.openal.AL10;

public class OpenALAudio implements Audio {
    private IntArray allSources;
    private ObjectMap<String, Class<? extends OpenALMusic>> extensionToMusicClass;
    private ObjectMap<String, Class<? extends OpenALSound>> extensionToSoundClass;
    private IntArray idleSources;
    Array<OpenALMusic> music;

    public OpenALAudio() {
        this(16);
    }

    public OpenALAudio(int simultaneousSources) {
        this.extensionToSoundClass = new ObjectMap<>();
        this.extensionToMusicClass = new ObjectMap<>();
        this.music = new Array<>(false, 1, OpenALMusic.class);
        registerSound("ogg", Ogg.Sound.class);
        registerMusic("ogg", Ogg.Music.class);
        try {
            AL.create();
            this.allSources = new IntArray(false, simultaneousSources);
            for (int i = 0; i < simultaneousSources; i++) {
                int sourceID = AL10.alGenSources();
                if (AL10.alGetError() != 0) {
                    break;
                }
                this.allSources.add(sourceID);
            }
            this.idleSources = new IntArray(this.allSources);
            AL10.alListener(4111, (FloatBuffer) BufferUtils.createFloatBuffer(6).put(new float[]{0.0f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f}).flip());
            AL10.alListener(4102, (FloatBuffer) BufferUtils.createFloatBuffer(3).put(new float[]{0.0f, 0.0f, 0.0f}).flip());
            AL10.alListener(4100, (FloatBuffer) BufferUtils.createFloatBuffer(3).put(new float[]{0.0f, 0.0f, 0.0f}).flip());
        } catch (LWJGLException ex) {
            throw new GdxRuntimeException("Error initializing OpenAL.", ex);
        }
    }

    public void registerSound(String extension, Class<? extends OpenALSound> soundClass) {
        if (extension == null) {
            throw new IllegalArgumentException("extension cannot be null.");
        } else if (soundClass == null) {
            throw new IllegalArgumentException("soundClass cannot be null.");
        } else {
            this.extensionToSoundClass.put(extension, soundClass);
        }
    }

    public void registerMusic(String extension, Class<? extends OpenALMusic> musicClass) {
        if (extension == null) {
            throw new IllegalArgumentException("extension cannot be null.");
        } else if (musicClass == null) {
            throw new IllegalArgumentException("musicClass cannot be null.");
        } else {
            this.extensionToMusicClass.put(extension, musicClass);
        }
    }

    public OpenALSound newSound(FileHandle file) {
        if (file == null) {
            throw new IllegalArgumentException("file cannot be null.");
        }
        Class<? extends OpenALSound> soundClass = this.extensionToSoundClass.get(file.extension());
        if (soundClass == null) {
            throw new GdxRuntimeException("Unknown file extension for sound: " + file);
        }
        try {
            return (OpenALSound) soundClass.getConstructor(OpenALAudio.class, FileHandle.class).newInstance(this, file);
        } catch (Exception e) {
            throw new GdxRuntimeException("Error creating sound " + soundClass.getName() + " for file: " + file, e);
        }
    }

    public OpenALMusic newMusic(FileHandle file) {
        if (file == null) {
            throw new IllegalArgumentException("file cannot be null.");
        }
        Class<? extends OpenALMusic> musicClass = this.extensionToMusicClass.get(file.extension());
        if (musicClass == null) {
            throw new GdxRuntimeException("Unknown file extension for music: " + file);
        }
        try {
            return (OpenALMusic) musicClass.getConstructor(OpenALAudio.class, FileHandle.class).newInstance(this, file);
        } catch (Exception e) {
            throw new GdxRuntimeException("Error creating music " + musicClass.getName() + " for file: " + file, e);
        }
    }

    /* access modifiers changed from: package-private */
    public int obtainSource(boolean isMusic) {
        int i = 0;
        int n = this.idleSources.size;
        while (i < n) {
            int sourceID = this.idleSources.get(i);
            int state = AL10.alGetSourcei(sourceID, 4112);
            if (state == 4114 || state == 4115) {
                i++;
            } else {
                if (isMusic) {
                    this.idleSources.removeIndex(i);
                }
                AL10.alSourceStop(sourceID);
                AL10.alSourcei(sourceID, 4105, 0);
                return sourceID;
            }
        }
        return -1;
    }

    /* access modifiers changed from: package-private */
    public void freeSource(int sourceID) {
        AL10.alSourceStop(sourceID);
        AL10.alSourcei(sourceID, 4105, 0);
        this.idleSources.add(sourceID);
    }

    /* access modifiers changed from: package-private */
    public void freeBuffer(int bufferID) {
        int n = this.idleSources.size;
        for (int i = 0; i < n; i++) {
            int sourceID = this.idleSources.get(i);
            if (AL10.alGetSourcei(sourceID, 4105) == bufferID) {
                AL10.alSourceStop(sourceID);
                AL10.alSourcei(sourceID, 4105, 0);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void stopSourcesWithBuffer(int bufferID) {
        int n = this.idleSources.size;
        for (int i = 0; i < n; i++) {
            int sourceID = this.idleSources.get(i);
            if (AL10.alGetSourcei(sourceID, 4105) == bufferID) {
                AL10.alSourceStop(sourceID);
            }
        }
    }

    public void update() {
        for (int i = 0; i < this.music.size; i++) {
            ((OpenALMusic[]) this.music.items)[i].update();
        }
    }

    public void dispose() {
        int n = this.allSources.size;
        for (int i = 0; i < n; i++) {
            int sourceID = this.allSources.get(i);
            if (AL10.alGetSourcei(sourceID, 4112) != 4116) {
                AL10.alSourceStop(sourceID);
            }
            AL10.alDeleteSources(sourceID);
        }
        AL.destroy();
    }

    public AudioDevice newAudioDevice(int samplingRate, boolean isMono) {
        return new JavaSoundAudioDevice(samplingRate, isMono);
    }
}
