package com.badlogic.gdx.backends.openal;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.GdxRuntimeException;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import org.lwjgl.BufferUtils;
import org.lwjgl.openal.AL10;

public abstract class OpenALMusic implements Music {
    private static final int bufferCount = 3;
    private static final int bufferSize = 40960;
    private static final int bytesPerSample = 2;
    private static final ByteBuffer tempBuffer = BufferUtils.createByteBuffer((int) bufferSize);
    private static final byte[] tempBytes = new byte[bufferSize];
    private final OpenALAudio audio;
    private IntBuffer buffers;
    protected final FileHandle file;
    private int format;
    private boolean isLooping;
    private boolean isPlaying;
    private float renderedSeconds;
    private int sampleRate;
    private float secondsPerBuffer;
    private int sourceID = -1;
    private float volume = 1.0f;

    /* access modifiers changed from: protected */
    public abstract int read(byte[] bArr);

    /* access modifiers changed from: protected */
    public abstract void reset();

    public OpenALMusic(OpenALAudio audio2, FileHandle file2) {
        this.audio = audio2;
        this.file = file2;
        this.buffers = BufferUtils.createIntBuffer(3);
        AL10.alGenBuffers(this.buffers);
        if (AL10.alGetError() != 0) {
            throw new GdxRuntimeException("Unabe to allocate audio buffers.");
        }
        audio2.music.add(this);
    }

    /* access modifiers changed from: protected */
    public void setup(int channels, int sampleRate2) {
        this.format = channels > 1 ? 4355 : 4353;
        this.sampleRate = sampleRate2;
        this.secondsPerBuffer = (20480.0f / ((float) channels)) / ((float) sampleRate2);
    }

    public void play() {
        if (this.sourceID == -1) {
            this.sourceID = this.audio.obtainSource(true);
            if (this.sourceID != -1) {
                AL10.alSourcei(this.sourceID, 4103, 0);
                AL10.alSourcef(this.sourceID, 4106, this.volume);
                for (int i = 0; i < 3; i++) {
                    int bufferID = this.buffers.get(i);
                    if (!fill(bufferID)) {
                        break;
                    }
                    AL10.alSourceQueueBuffers(this.sourceID, bufferID);
                }
                if (AL10.alGetError() != 0) {
                    stop();
                    return;
                }
            } else {
                return;
            }
        }
        AL10.alSourcePlay(this.sourceID);
        this.isPlaying = true;
    }

    public void stop() {
        if (this.sourceID != -1) {
            reset();
            this.audio.freeSource(this.sourceID);
            this.sourceID = -1;
            this.renderedSeconds = 0.0f;
            this.isPlaying = false;
        }
    }

    public void pause() {
        if (this.sourceID != -1) {
            AL10.alSourcePause(this.sourceID);
        }
        this.isPlaying = false;
    }

    public boolean isPlaying() {
        if (this.sourceID == -1) {
            return false;
        }
        return this.isPlaying;
    }

    public void setLooping(boolean isLooping2) {
        this.isLooping = isLooping2;
    }

    public boolean isLooping() {
        return this.isLooping;
    }

    public void setVolume(float volume2) {
        this.volume = volume2;
        if (this.sourceID != -1) {
            AL10.alSourcef(this.sourceID, 4106, volume2);
        }
    }

    public float getPosition() {
        if (this.sourceID == -1) {
            return 0.0f;
        }
        return this.renderedSeconds + AL10.alGetSourcef(this.sourceID, 4132);
    }

    public void update() {
        int bufferID;
        if (this.sourceID != -1) {
            boolean end = false;
            int buffers2 = AL10.alGetSourcei(this.sourceID, 4118);
            while (true) {
                int buffers3 = buffers2;
                buffers2 = buffers3 - 1;
                if (buffers3 > 0 && (bufferID = AL10.alSourceUnqueueBuffers(this.sourceID)) != 40963) {
                    this.renderedSeconds += this.secondsPerBuffer;
                    if (!end) {
                        if (fill(bufferID)) {
                            AL10.alSourceQueueBuffers(this.sourceID, bufferID);
                        } else {
                            end = true;
                        }
                    }
                }
            }
            if (end && AL10.alGetSourcei(this.sourceID, 4117) == 0) {
                stop();
            }
            if (this.isPlaying && AL10.alGetSourcei(this.sourceID, 4112) != 4114) {
                AL10.alSourcePlay(this.sourceID);
            }
        }
    }

    private boolean fill(int bufferID) {
        tempBuffer.clear();
        int length = read(tempBytes);
        if (length <= 0) {
            if (!this.isLooping) {
                return false;
            }
            reset();
            this.renderedSeconds = 0.0f;
            length = read(tempBytes);
            if (length <= 0) {
                return false;
            }
        }
        tempBuffer.put(tempBytes, 0, length).flip();
        AL10.alBufferData(bufferID, this.format, tempBuffer, this.sampleRate);
        return true;
    }

    public void dispose() {
        if (this.buffers != null) {
            if (this.sourceID != -1) {
                reset();
                this.audio.music.removeValue(this, true);
                this.audio.freeSource(this.sourceID);
                this.sourceID = -1;
            }
            AL10.alDeleteBuffers(this.buffers);
            this.buffers = null;
        }
    }
}
