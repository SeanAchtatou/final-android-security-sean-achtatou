package com.badlogic.gdx.backends.jogl;

import com.badlogic.gdx.Files;
import com.badlogic.gdx.files.FileHandle;
import java.io.File;

public class JoglFileHandle extends FileHandle {
    JoglFileHandle(String fileName, Files.FileType type) {
        super(fileName, type);
    }

    JoglFileHandle(File file, Files.FileType type) {
        super(file, type);
    }

    public FileHandle child(String name) {
        if (this.file.getPath().length() == 0) {
            return new JoglFileHandle(new File(name), this.type);
        }
        return new JoglFileHandle(new File(this.file, name), this.type);
    }

    public FileHandle parent() {
        File parent = this.file.getParentFile();
        if (parent == null) {
            if (this.type == Files.FileType.Absolute) {
                parent = new File("/");
            } else {
                parent = new File("");
            }
        }
        return new JoglFileHandle(parent, this.type);
    }
}
