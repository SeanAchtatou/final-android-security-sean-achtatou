package com.badlogic.gdx.backends.android;

import android.media.AudioManager;
import android.media.SoundPool;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.utils.IntArray;

final class AndroidSound implements Sound {
    final AudioManager manager;
    final int soundId;
    final SoundPool soundPool;
    final IntArray streamIds = new IntArray(8);

    AndroidSound(SoundPool pool, AudioManager manager2, int soundId2) {
        this.soundPool = pool;
        this.manager = manager2;
        this.soundId = soundId2;
    }

    public void dispose() {
        this.soundPool.unload(this.soundId);
    }

    public void play() {
        play(1.0f);
    }

    public void play(float volume) {
        if (this.streamIds.size == 8) {
            this.streamIds.pop();
        }
        this.streamIds.add(this.soundPool.play(this.soundId, volume, volume, 1, 0, 1.0f));
    }

    public void stop() {
        int n = this.streamIds.size;
        for (int i = 0; i < n; i++) {
            this.soundPool.stop(this.streamIds.get(i));
        }
    }
}
