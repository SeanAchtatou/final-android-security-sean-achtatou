package com.badlogic.gdx.backends.openal;

import com.badlogic.gdx.audio.AudioDevice;
import com.badlogic.gdx.utils.GdxRuntimeException;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.SourceDataLine;

public class JavaSoundAudioDevice implements AudioDevice {
    private byte[] bytes = new byte[176400];
    private final boolean isMono;
    private SourceDataLine line;

    public JavaSoundAudioDevice(int samplingRate, boolean isMono2) {
        int i = 1;
        this.isMono = isMono2;
        try {
            AudioFormat format = new AudioFormat((float) samplingRate, 16, !isMono2 ? 2 : i, true, false);
            this.line = AudioSystem.getSourceDataLine(format);
            this.line.open(format, Math.min(1024, samplingRate / 10) * 2);
            this.line.start();
        } catch (Exception e) {
            throw new GdxRuntimeException("Error creating JavaSoundAudioDevice.", e);
        }
    }

    public void dispose() {
        this.line.drain();
        this.line.close();
    }

    public boolean isMono() {
        return this.isMono;
    }

    public void writeSamples(short[] samples, int offset, int numSamples) {
        if (this.bytes.length < samples.length * 2) {
            this.bytes = new byte[(samples.length * 2)];
        }
        int i = offset;
        int j = 0;
        while (i < offset + numSamples) {
            short value = samples[i];
            this.bytes[j] = (byte) (value & 255);
            this.bytes[j + 1] = (byte) (value >> 8);
            i++;
            j += 2;
        }
        int writtenBytes = this.line.write(this.bytes, 0, numSamples * 2);
        while (writtenBytes != numSamples * 2) {
            writtenBytes += this.line.write(this.bytes, writtenBytes, (numSamples * 2) - writtenBytes);
        }
    }

    public void writeSamples(float[] samples, int offset, int numSamples) {
        if (this.bytes.length < samples.length * 2) {
            this.bytes = new byte[(samples.length * 2)];
        }
        int i = offset;
        int j = 0;
        while (i < offset + numSamples) {
            float fValue = samples[i];
            if (fValue > 1.0f) {
                fValue = 1.0f;
            }
            if (fValue < -1.0f) {
                fValue = -1.0f;
            }
            short value = (short) ((int) (32767.0f * fValue));
            this.bytes[j] = (byte) (value & 255);
            this.bytes[j + 1] = (byte) (value >> 8);
            i++;
            j += 2;
        }
        int writtenBytes = this.line.write(this.bytes, 0, numSamples * 2);
        while (writtenBytes != numSamples * 2) {
            writtenBytes += this.line.write(this.bytes, writtenBytes, (numSamples * 2) - writtenBytes);
        }
    }

    public int getLatency() {
        return 0;
    }
}
