package com.badlogic.gdx.backends.jogl;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.backends.android.AndroidInput;
import com.badlogic.gdx.utils.Pool;
import java.awt.AWTException;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.GraphicsEnvironment;
import java.awt.HeadlessException;
import java.awt.Point;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.media.opengl.GLCanvas;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

public class JoglInput implements Input, MouseMotionListener, MouseListener, MouseWheelListener, KeyListener {
    GLCanvas canvas;
    boolean catched = false;
    int deltaX = 0;
    int deltaY = 0;
    boolean justTouched = false;
    List<KeyEvent> keyEvents = new ArrayList();
    Set<Integer> keys = new HashSet();
    Set<Integer> pressedButtons = new HashSet();
    InputProcessor processor;
    Robot robot = null;
    boolean touchDown = false;
    List<TouchEvent> touchEvents = new ArrayList();
    int touchX = 0;
    int touchY = 0;
    Pool<KeyEvent> usedKeyEvents = new Pool<KeyEvent>(16, 1000) {
        /* access modifiers changed from: protected */
        public KeyEvent newObject() {
            return new KeyEvent();
        }
    };
    Pool<TouchEvent> usedTouchEvents = new Pool<TouchEvent>(16, 1000) {
        /* access modifiers changed from: protected */
        public TouchEvent newObject() {
            return new TouchEvent();
        }
    };

    class KeyEvent {
        static final int KEY_DOWN = 0;
        static final int KEY_TYPED = 2;
        static final int KEY_UP = 1;
        char keyChar;
        int keyCode;
        int type;

        KeyEvent() {
        }
    }

    class TouchEvent {
        static final int TOUCH_DOWN = 0;
        static final int TOUCH_DRAGGED = 2;
        static final int TOUCH_MOVED = 3;
        static final int TOUCH_SCROLLED = 4;
        static final int TOUCH_UP = 1;
        int button;
        int pointer;
        int scrollAmount;
        int type;
        int x;
        int y;

        TouchEvent() {
        }
    }

    public JoglInput(GLCanvas canvas2) {
        setListeners(canvas2);
        try {
            this.robot = new Robot(GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice());
        } catch (AWTException | HeadlessException e) {
        }
    }

    public void setListeners(GLCanvas canvas2) {
        if (this.canvas != null) {
            canvas2.removeMouseListener(this);
            canvas2.removeMouseMotionListener(this);
            canvas2.removeMouseWheelListener(this);
            canvas2.removeKeyListener(this);
            JoglGraphics.findJFrame(canvas2);
        }
        canvas2.addMouseListener(this);
        canvas2.addMouseMotionListener(this);
        canvas2.addMouseWheelListener(this);
        canvas2.addKeyListener(this);
        this.canvas = canvas2;
    }

    public float getAccelerometerX() {
        return 0.0f;
    }

    public float getAccelerometerY() {
        return 0.0f;
    }

    public float getAccelerometerZ() {
        return 0.0f;
    }

    public void getTextInput(final Input.TextInputListener listener, final String title, final String text) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                String output = JOptionPane.showInputDialog((Component) null, title, text);
                if (output != null) {
                    listener.input(output);
                } else {
                    listener.canceled();
                }
            }
        });
    }

    public int getX() {
        return this.touchX;
    }

    public int getX(int pointer) {
        if (pointer == 0) {
            return this.touchX;
        }
        return 0;
    }

    public int getY() {
        return this.touchY;
    }

    public int getY(int pointer) {
        if (pointer == 0) {
            return this.touchY;
        }
        return 0;
    }

    public boolean isKeyPressed(int key) {
        boolean contains;
        synchronized (this) {
            if (key == -1) {
                contains = this.keys.size() > 0;
            } else {
                contains = this.keys.contains(Integer.valueOf(key));
            }
        }
        return contains;
    }

    public boolean isTouched() {
        return this.touchDown;
    }

    public boolean isTouched(int pointer) {
        if (pointer == 0) {
            return this.touchDown;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public void processEvents() {
        synchronized (this) {
            this.justTouched = false;
            if (this.processor != null) {
                InputProcessor processor2 = this.processor;
                int len = this.keyEvents.size();
                for (int i = 0; i < len; i++) {
                    KeyEvent e = this.keyEvents.get(i);
                    switch (e.type) {
                        case 0:
                            processor2.keyDown(e.keyCode);
                            break;
                        case 1:
                            processor2.keyUp(e.keyCode);
                            break;
                        case 2:
                            processor2.keyTyped(e.keyChar);
                            break;
                    }
                    this.usedKeyEvents.free((AndroidInput.KeyEvent) e);
                }
                int len2 = this.touchEvents.size();
                for (int i2 = 0; i2 < len2; i2++) {
                    TouchEvent e2 = this.touchEvents.get(i2);
                    switch (e2.type) {
                        case 0:
                            processor2.touchDown(e2.x, e2.y, e2.pointer, e2.button);
                            this.justTouched = true;
                            break;
                        case 1:
                            processor2.touchUp(e2.x, e2.y, e2.pointer, e2.button);
                            break;
                        case 2:
                            processor2.touchDragged(e2.x, e2.y, e2.pointer);
                            break;
                        case 3:
                            processor2.touchMoved(e2.x, e2.y);
                            break;
                        case 4:
                            processor2.scrolled(e2.scrollAmount);
                            break;
                    }
                    this.usedTouchEvents.free((AndroidInput.KeyEvent) e2);
                }
            } else {
                int len3 = this.touchEvents.size();
                for (int i3 = 0; i3 < len3; i3++) {
                    TouchEvent event = this.touchEvents.get(i3);
                    if (event.type == 0) {
                        this.justTouched = true;
                    }
                    this.usedTouchEvents.free((AndroidInput.KeyEvent) event);
                }
                int len4 = this.keyEvents.size();
                for (int i4 = 0; i4 < len4; i4++) {
                    this.usedKeyEvents.free((AndroidInput.KeyEvent) this.keyEvents.get(i4));
                }
            }
            if (this.touchEvents.size() == 0) {
                this.deltaX = 0;
                this.deltaY = 0;
            }
            this.keyEvents.clear();
            this.touchEvents.clear();
        }
    }

    public void setCatchBackKey(boolean catchBack) {
    }

    public void setOnscreenKeyboardVisible(boolean visible) {
    }

    public void mouseDragged(MouseEvent e) {
        synchronized (this) {
            TouchEvent event = this.usedTouchEvents.obtain();
            event.pointer = 0;
            event.x = e.getX();
            event.y = e.getY();
            event.type = 2;
            this.touchEvents.add(event);
            this.deltaX = event.x - this.touchX;
            this.deltaY = event.y - this.touchY;
            this.touchX = event.x;
            this.touchY = event.y;
            checkCatched(e);
        }
    }

    public void mouseMoved(MouseEvent e) {
        synchronized (this) {
            TouchEvent event = this.usedTouchEvents.obtain();
            event.pointer = 0;
            event.x = e.getX();
            event.y = e.getY();
            event.type = 3;
            this.touchEvents.add(event);
            this.deltaX = event.x - this.touchX;
            this.deltaY = event.y - this.touchY;
            this.touchX = event.x;
            this.touchY = event.y;
            checkCatched(e);
        }
    }

    public void mouseClicked(MouseEvent arg0) {
    }

    public void mouseEntered(MouseEvent e) {
        this.touchX = e.getX();
        this.touchY = e.getY();
        checkCatched(e);
    }

    public void mouseExited(MouseEvent e) {
        checkCatched(e);
    }

    private void checkCatched(MouseEvent e) {
        if (this.catched && this.robot != null && this.canvas.isShowing()) {
            int x = Math.max(0, Math.min(e.getX(), this.canvas.getWidth()) - 1) + this.canvas.getLocationOnScreen().x;
            int y = Math.max(0, Math.min(e.getY(), this.canvas.getHeight()) - 1) + this.canvas.getLocationOnScreen().y;
            this.deltaX = e.getLocationOnScreen().x - x;
            this.deltaY = e.getLocationOnScreen().y - y;
            if (e.getX() < 0 || e.getX() >= this.canvas.getWidth() || e.getY() < 0 || e.getY() >= this.canvas.getHeight()) {
                this.robot.mouseMove(x - this.deltaX, y - this.deltaY);
            }
        }
    }

    private int toGdxButton(int swingButton) {
        if (swingButton == 1) {
            return 0;
        }
        if (swingButton == 2) {
            return 2;
        }
        if (swingButton == 3) {
            return 1;
        }
        return 0;
    }

    public void mousePressed(MouseEvent e) {
        synchronized (this) {
            TouchEvent event = this.usedTouchEvents.obtain();
            event.pointer = 0;
            event.x = e.getX();
            event.y = e.getY();
            event.type = 0;
            event.button = toGdxButton(e.getButton());
            this.touchEvents.add(event);
            this.deltaX = event.x - this.touchX;
            this.deltaY = event.y - this.touchY;
            this.touchX = event.x;
            this.touchY = event.y;
            this.touchDown = true;
            this.pressedButtons.add(Integer.valueOf(event.button));
        }
    }

    public void mouseReleased(MouseEvent e) {
        synchronized (this) {
            TouchEvent event = this.usedTouchEvents.obtain();
            event.pointer = 0;
            event.x = e.getX();
            event.y = e.getY();
            event.button = toGdxButton(e.getButton());
            event.type = 1;
            this.touchEvents.add(event);
            this.deltaX = event.x - this.touchX;
            this.deltaY = event.y - this.touchY;
            this.touchX = event.x;
            this.touchY = event.y;
            this.pressedButtons.remove(Integer.valueOf(event.button));
            if (this.pressedButtons.size() == 0) {
                this.touchDown = false;
            }
        }
    }

    public void mouseWheelMoved(MouseWheelEvent e) {
        synchronized (this) {
            TouchEvent event = this.usedTouchEvents.obtain();
            event.pointer = 0;
            event.type = 4;
            event.scrollAmount = e.getWheelRotation();
            this.touchEvents.add(event);
        }
    }

    public void keyPressed(java.awt.event.KeyEvent e) {
        synchronized (this) {
            KeyEvent event = this.usedKeyEvents.obtain();
            event.keyChar = 0;
            event.keyCode = translateKeyCode(e.getKeyCode());
            event.type = 0;
            this.keyEvents.add(event);
            this.keys.add(Integer.valueOf(event.keyCode));
        }
    }

    public void keyReleased(java.awt.event.KeyEvent e) {
        synchronized (this) {
            KeyEvent event = this.usedKeyEvents.obtain();
            event.keyChar = 0;
            event.keyCode = translateKeyCode(e.getKeyCode());
            event.type = 1;
            this.keyEvents.add(event);
            this.keys.remove(Integer.valueOf(event.keyCode));
        }
    }

    public void keyTyped(java.awt.event.KeyEvent e) {
        synchronized (this) {
            KeyEvent event = this.usedKeyEvents.obtain();
            event.keyChar = e.getKeyChar();
            event.keyCode = 0;
            event.type = 2;
            this.keyEvents.add(event);
        }
    }

    protected static int translateKeyCode(int keyCode) {
        if (keyCode == 107) {
            return 81;
        }
        if (keyCode == 109) {
            return 69;
        }
        if (keyCode == 48) {
            return 7;
        }
        if (keyCode == 49) {
            return 8;
        }
        if (keyCode == 50) {
            return 9;
        }
        if (keyCode == 51) {
            return 10;
        }
        if (keyCode == 52) {
            return 11;
        }
        if (keyCode == 53) {
            return 12;
        }
        if (keyCode == 54) {
            return 13;
        }
        if (keyCode == 55) {
            return 14;
        }
        if (keyCode == 56) {
            return 15;
        }
        if (keyCode == 57) {
            return 16;
        }
        if (keyCode == 65) {
            return 29;
        }
        if (keyCode == 66) {
            return 30;
        }
        if (keyCode == 67) {
            return 31;
        }
        if (keyCode == 68) {
            return 32;
        }
        if (keyCode == 69) {
            return 33;
        }
        if (keyCode == 70) {
            return 34;
        }
        if (keyCode == 71) {
            return 35;
        }
        if (keyCode == 72) {
            return 36;
        }
        if (keyCode == 73) {
            return 37;
        }
        if (keyCode == 74) {
            return 38;
        }
        if (keyCode == 75) {
            return 39;
        }
        if (keyCode == 76) {
            return 40;
        }
        if (keyCode == 77) {
            return 41;
        }
        if (keyCode == 78) {
            return 42;
        }
        if (keyCode == 79) {
            return 43;
        }
        if (keyCode == 80) {
            return 44;
        }
        if (keyCode == 81) {
            return 45;
        }
        if (keyCode == 82) {
            return 46;
        }
        if (keyCode == 83) {
            return 47;
        }
        if (keyCode == 84) {
            return 48;
        }
        if (keyCode == 85) {
            return 49;
        }
        if (keyCode == 86) {
            return 50;
        }
        if (keyCode == 87) {
            return 51;
        }
        if (keyCode == 88) {
            return 52;
        }
        if (keyCode == 89) {
            return 53;
        }
        if (keyCode == 90) {
            return 54;
        }
        if (keyCode == 18) {
            return 57;
        }
        if (keyCode == 65406) {
            return 58;
        }
        if (keyCode == 92) {
            return 73;
        }
        if (keyCode == 44) {
            return 55;
        }
        if (keyCode == 127) {
            return 67;
        }
        if (keyCode == 37) {
            return 21;
        }
        if (keyCode == 39) {
            return 22;
        }
        if (keyCode == 38) {
            return 19;
        }
        if (keyCode == 40) {
            return 20;
        }
        if (keyCode == 10) {
            return 66;
        }
        if (keyCode == 36) {
            return 3;
        }
        if (keyCode == 45) {
            return 69;
        }
        if (keyCode == 46) {
            return 56;
        }
        if (keyCode == 521) {
            return 81;
        }
        if (keyCode == 59) {
            return 74;
        }
        if (keyCode == 16) {
            return 59;
        }
        if (keyCode == 47) {
            return 76;
        }
        if (keyCode == 32) {
            return 62;
        }
        if (keyCode == 9) {
            return 61;
        }
        if (keyCode == 8) {
            return 67;
        }
        if (keyCode == 17) {
            return Input.Keys.CONTROL_LEFT;
        }
        if (keyCode == 27) {
            return Input.Keys.ESCAPE;
        }
        if (keyCode == 35) {
            return Input.Keys.END;
        }
        if (keyCode == 155) {
            return Input.Keys.INSERT;
        }
        if (keyCode == 101) {
            return 23;
        }
        if (keyCode == 33) {
            return 92;
        }
        if (keyCode == 34) {
            return 93;
        }
        if (keyCode == 112) {
            return Input.Keys.F1;
        }
        if (keyCode == 113) {
            return Input.Keys.F2;
        }
        if (keyCode == 114) {
            return Input.Keys.F3;
        }
        if (keyCode == 115) {
            return Input.Keys.F4;
        }
        if (keyCode == 116) {
            return Input.Keys.F5;
        }
        if (keyCode == 117) {
            return Input.Keys.F6;
        }
        if (keyCode == 118) {
            return Input.Keys.F7;
        }
        if (keyCode == 119) {
            return Input.Keys.F8;
        }
        if (keyCode == 120) {
            return Input.Keys.F9;
        }
        if (keyCode == 121) {
            return Input.Keys.F10;
        }
        if (keyCode == 122) {
            return Input.Keys.F11;
        }
        if (keyCode == 123) {
            return 255;
        }
        if (keyCode == 513) {
            return Input.Keys.COLON;
        }
        return 0;
    }

    public void setInputProcessor(InputProcessor processor2) {
        synchronized (this) {
            this.processor = processor2;
        }
    }

    public InputProcessor getInputProcessor() {
        return this.processor;
    }

    public void vibrate(int milliseconds) {
    }

    public boolean justTouched() {
        return this.justTouched;
    }

    public boolean isButtonPressed(int button) {
        return this.pressedButtons.contains(Integer.valueOf(button));
    }

    public void vibrate(long[] pattern, int repeat) {
    }

    public void cancelVibrate() {
    }

    public float getAzimuth() {
        return 0.0f;
    }

    public float getPitch() {
        return 0.0f;
    }

    public float getRoll() {
        return 0.0f;
    }

    public boolean isPeripheralAvailable(Input.Peripheral peripheral) {
        if (peripheral == Input.Peripheral.HardwareKeyboard) {
            return true;
        }
        return false;
    }

    public int getRotation() {
        return 0;
    }

    public Input.Orientation getNativeOrientation() {
        return Input.Orientation.Landscape;
    }

    public void setCursorCatched(boolean catched2) {
        this.catched = catched2;
        showCursor(!catched2);
    }

    private void showCursor(boolean visible) {
        if (!visible) {
            JoglGraphics.findJFrame(this.canvas).setCursor(Toolkit.getDefaultToolkit().createCustomCursor(new BufferedImage(1, 1, 2), new Point(0, 0), "none"));
            return;
        }
        JoglGraphics.findJFrame(this.canvas).setCursor(Cursor.getDefaultCursor());
    }

    public boolean isCursorCatched() {
        return this.catched;
    }

    public int getDeltaX() {
        return this.deltaX;
    }

    public int getDeltaX(int pointer) {
        if (pointer == 0) {
            return this.deltaX;
        }
        return 0;
    }

    public int getDeltaY() {
        return this.deltaY;
    }

    public int getDeltaY(int pointer) {
        if (pointer == 0) {
            return this.deltaY;
        }
        return 0;
    }

    public void setCursorPosition(int x, int y) {
        if (this.robot != null) {
            this.robot.mouseMove(this.canvas.getLocationOnScreen().x + x, this.canvas.getLocationOnScreen().y + y);
        }
    }

    public void setCatchMenuKey(boolean catchMenu) {
    }
}
