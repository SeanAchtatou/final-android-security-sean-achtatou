package com.badlogic.gdx.backends.jogl;

import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.GLU;

public class JoglGLU implements GLU {
    javax.media.opengl.glu.GLU glu = new javax.media.opengl.glu.GLU();
    double[] modeld = new double[16];
    double[] projectd = new double[16];
    double[] wind = new double[3];

    public void gluLookAt(GL10 gl, float eyeX, float eyeY, float eyeZ, float centerX, float centerY, float centerZ, float upX, float upY, float upZ) {
        this.glu.gluLookAt((double) eyeX, (double) eyeY, (double) eyeZ, (double) centerX, (double) centerY, (double) centerZ, (double) upX, (double) upY, (double) upZ);
    }

    public void gluOrtho2D(GL10 gl, float left, float right, float bottom, float top) {
        this.glu.gluOrtho2D((double) left, (double) right, (double) bottom, (double) top);
    }

    public void gluPerspective(GL10 gl, float fovy, float aspect, float zNear, float zFar) {
        this.glu.gluPerspective((double) fovy, (double) aspect, (double) zNear, (double) zFar);
    }

    public boolean gluProject(float objX, float objY, float objZ, float[] model, int modelOffset, float[] project, int projectOffset, int[] view, int viewOffset, float[] win, int winOffset) {
        for (int i = 0; i < 16; i++) {
            this.modeld[i] = (double) model[modelOffset + i];
            this.projectd[i] = (double) project[projectOffset + i];
        }
        boolean result = this.glu.gluProject((double) objX, (double) objY, (double) objZ, this.modeld, 0, this.projectd, 0, view, 0, this.wind, 0);
        win[winOffset] = (float) this.wind[0];
        win[winOffset + 1] = (float) this.wind[1];
        win[winOffset + 2] = (float) this.wind[2];
        return result;
    }

    public boolean gluUnProject(float winX, float winY, float winZ, float[] model, int modelOffset, float[] project, int projectOffset, int[] view, int viewOffset, float[] obj, int objOffset) {
        for (int i = 0; i < 16; i++) {
            this.modeld[i] = (double) model[modelOffset + i];
            this.projectd[i] = (double) project[projectOffset + i];
        }
        boolean result = this.glu.gluUnProject((double) winX, (double) winY, (double) winZ, this.modeld, 0, this.projectd, 0, view, 0, this.wind, 0);
        obj[objOffset] = (float) this.wind[0];
        obj[objOffset + 1] = (float) this.wind[1];
        obj[objOffset + 2] = (float) this.wind[2];
        return result;
    }
}
