package com.badlogic.gdx.backends.jogl;

import com.badlogic.gdx.utils.GdxNativesLoader;
import com.sun.opengl.impl.NativeLibLoader;

public class JoglNativesLoader {
    private static boolean nativesLoaded = false;

    public static void load() {
        GdxNativesLoader.load();
        if (!GdxNativesLoader.disableNativesLoading && !nativesLoaded) {
            NativeLibLoader.disableLoading();
            com.sun.gluegen.runtime.NativeLibLoader.disableLoading();
            if (System.getProperty("os.name", "").contains("Windows") && !System.getProperty("libgdx.nojawtpreloading", "false").contains("true")) {
                try {
                    System.loadLibrary("jawt");
                } catch (Exception e) {
                    System.err.println("WARNING: Unable to load native jawt library: '" + e.getMessage() + "'");
                }
            }
            if (GdxNativesLoader.isWindows) {
                GdxNativesLoader.loadLibrary("gluegen-rt-win32.dll", "gluegen-rt-win64.dll");
                GdxNativesLoader.loadLibrary("jogl_awt-win32.dll", "jogl_awt-win64.dll");
                GdxNativesLoader.loadLibrary("jogl-win32.dll", "jogl-win64.dll");
            } else if (GdxNativesLoader.isMac) {
                GdxNativesLoader.loadLibrary("libgluegen-rt.jnilib", "libgluegen-rt.jnilib");
                GdxNativesLoader.loadLibrary("libjogl_awt.jnilib", "libjogl_awt.jnilib");
                GdxNativesLoader.loadLibrary("libjogl.jnilib", "libjogl.jnilib");
            } else if (GdxNativesLoader.isLinux) {
                GdxNativesLoader.loadLibrary("libgluegen-rt-linux32.so", "libgluegen-rt-linux64.so");
                GdxNativesLoader.loadLibrary("libjogl_awt-linux32.so", "libjogl_awt-linux64.so");
                GdxNativesLoader.loadLibrary("libjogl-linux32.so", "libjogl-linux64.so");
            }
            if (GdxNativesLoader.isWindows) {
                GdxNativesLoader.extractLibrary("OpenAL32.dll", "OpenAL64.dll");
                GdxNativesLoader.extractLibrary("lwjgl.dll", "lwjgl64.dll");
            } else if (GdxNativesLoader.isMac) {
                GdxNativesLoader.extractLibrary("openal.dylib", "openal.dylib");
                GdxNativesLoader.extractLibrary("liblwjgl.jnilib", "liblwjgl.jnilib");
            } else if (GdxNativesLoader.isLinux) {
                GdxNativesLoader.extractLibrary("libopenal.so", "libopenal64.so");
                GdxNativesLoader.extractLibrary("liblwjgl.so", "liblwjgl64.so");
            }
            System.setProperty("org.lwjgl.librarypath", GdxNativesLoader.nativesDir.getAbsolutePath());
            nativesLoaded = true;
        }
    }
}
