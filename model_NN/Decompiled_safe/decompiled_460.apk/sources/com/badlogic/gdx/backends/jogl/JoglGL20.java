package com.badlogic.gdx.backends.jogl;

import com.badlogic.gdx.graphics.GL20;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import javax.media.opengl.GL;

final class JoglGL20 implements GL20 {
    private final GL gl;

    public JoglGL20(GL gl2) {
        this.gl = gl2;
    }

    public void glActiveTexture(int texture) {
        this.gl.glActiveTexture(texture);
    }

    public void glAttachShader(int program, int shader) {
        this.gl.glAttachShader(program, shader);
    }

    public void glBindAttribLocation(int program, int index, String name) {
        this.gl.glBindAttribLocation(program, index, name);
    }

    public void glBindBuffer(int target, int buffer) {
        this.gl.glBindBuffer(target, buffer);
    }

    public void glBindFramebuffer(int target, int framebuffer) {
        this.gl.glBindFramebufferEXT(target, framebuffer);
    }

    public void glBindRenderbuffer(int target, int renderbuffer) {
        this.gl.glBindRenderbufferEXT(target, renderbuffer);
    }

    public void glBindTexture(int target, int texture) {
        this.gl.glBindTexture(target, texture);
    }

    public void glBlendColor(float red, float green, float blue, float alpha) {
        this.gl.glBlendColor(red, green, blue, alpha);
    }

    public void glBlendEquation(int mode) {
        this.gl.glBlendEquation(mode);
    }

    public void glBlendEquationSeparate(int modeRGB, int modeAlpha) {
        this.gl.glBlendEquationSeparate(modeRGB, modeAlpha);
    }

    public void glBlendFunc(int sfactor, int dfactor) {
        this.gl.glBlendFunc(sfactor, dfactor);
    }

    public void glBlendFuncSeparate(int srcRGB, int dstRGB, int srcAlpha, int dstAlpha) {
        this.gl.glBlendFuncSeparate(srcRGB, dstRGB, srcAlpha, dstAlpha);
    }

    public void glBufferData(int target, int size, Buffer data, int usage) {
        this.gl.glBufferData(target, size, data, usage);
    }

    public void glBufferSubData(int target, int offset, int size, Buffer data) {
        this.gl.glBufferSubData(target, offset, size, data);
    }

    public int glCheckFramebufferStatus(int target) {
        return this.gl.glCheckFramebufferStatusEXT(target);
    }

    public void glClear(int mask) {
        this.gl.glClear(mask);
    }

    public void glClearColor(float red, float green, float blue, float alpha) {
        this.gl.glClearColor(red, green, blue, alpha);
    }

    public void glClearDepthf(float depth) {
        this.gl.glClearDepth((double) depth);
    }

    public void glClearStencil(int s) {
        this.gl.glClearStencil(s);
    }

    public void glColorMask(boolean red, boolean green, boolean blue, boolean alpha) {
        this.gl.glColorMask(red, green, blue, alpha);
    }

    public void glCompileShader(int shader) {
        this.gl.glCompileShader(shader);
    }

    public void glCompressedTexImage2D(int target, int level, int internalformat, int width, int height, int border, int imageSize, Buffer data) {
        this.gl.glCompressedTexImage2D(target, level, internalformat, width, height, border, imageSize, data);
    }

    public void glCompressedTexSubImage2D(int target, int level, int xoffset, int yoffset, int width, int height, int format, int imageSize, Buffer data) {
        this.gl.glCompressedTexSubImage2D(target, level, xoffset, yoffset, width, height, format, imageSize, data);
    }

    public void glCopyTexImage2D(int target, int level, int internalformat, int x, int y, int width, int height, int border) {
        this.gl.glCopyTexImage2D(target, level, internalformat, x, y, width, height, border);
    }

    public void glCopyTexSubImage2D(int target, int level, int xoffset, int yoffset, int x, int y, int width, int height) {
        this.gl.glCopyTexSubImage2D(target, level, xoffset, yoffset, x, y, width, height);
    }

    public int glCreateProgram() {
        return this.gl.glCreateProgram();
    }

    public int glCreateShader(int type) {
        return this.gl.glCreateShader(type);
    }

    public void glCullFace(int mode) {
        this.gl.glCullFace(mode);
    }

    public void glDeleteBuffers(int n, IntBuffer buffers) {
        this.gl.glDeleteBuffers(n, buffers);
    }

    public void glDeleteFramebuffers(int n, IntBuffer framebuffers) {
        this.gl.glDeleteFramebuffersEXT(n, framebuffers);
    }

    public void glDeleteProgram(int program) {
        this.gl.glDeleteProgram(program);
    }

    public void glDeleteRenderbuffers(int n, IntBuffer renderbuffers) {
        this.gl.glDeleteRenderbuffersEXT(n, renderbuffers);
    }

    public void glDeleteShader(int shader) {
        this.gl.glDeleteShader(shader);
    }

    public void glDeleteTextures(int n, IntBuffer textures) {
        this.gl.glDeleteTextures(n, textures);
    }

    public void glDepthFunc(int func) {
        this.gl.glDepthFunc(func);
    }

    public void glDepthMask(boolean flag) {
        this.gl.glDepthMask(flag);
    }

    public void glDepthRangef(float zNear, float zFar) {
        this.gl.glDepthRange((double) zNear, (double) zFar);
    }

    public void glDetachShader(int program, int shader) {
        this.gl.glDetachShader(program, shader);
    }

    public void glDisable(int cap) {
        this.gl.glDisable(cap);
    }

    public void glDisableVertexAttribArray(int index) {
        this.gl.glDisableVertexAttribArray(index);
    }

    public void glDrawArrays(int mode, int first, int count) {
        this.gl.glDrawArrays(mode, first, count);
    }

    public void glDrawElements(int mode, int count, int type, Buffer indices) {
        this.gl.glDrawElements(mode, count, type, indices);
    }

    public void glEnable(int cap) {
        this.gl.glEnable(cap);
    }

    public void glEnableVertexAttribArray(int index) {
        this.gl.glEnableVertexAttribArray(index);
    }

    public void glFinish() {
        this.gl.glFinish();
    }

    public void glFlush() {
        this.gl.glFlush();
    }

    public void glFramebufferRenderbuffer(int target, int attachment, int renderbuffertarget, int renderbuffer) {
        this.gl.glFramebufferRenderbufferEXT(target, attachment, renderbuffertarget, renderbuffer);
    }

    public void glFramebufferTexture2D(int target, int attachment, int textarget, int texture, int level) {
        this.gl.glFramebufferTexture2DEXT(target, attachment, textarget, texture, level);
    }

    public void glFrontFace(int mode) {
        this.gl.glFrontFace(mode);
    }

    public void glGenBuffers(int n, IntBuffer buffers) {
        this.gl.glGenBuffers(n, buffers);
    }

    public void glGenFramebuffers(int n, IntBuffer framebuffers) {
        this.gl.glGenFramebuffersEXT(n, framebuffers);
    }

    public void glGenRenderbuffers(int n, IntBuffer renderbuffers) {
        this.gl.glGenRenderbuffersEXT(n, renderbuffers);
    }

    public void glGenTextures(int n, IntBuffer textures) {
        this.gl.glGenTextures(n, textures);
    }

    public void glGenerateMipmap(int target) {
        this.gl.glGenerateMipmapEXT(target);
    }

    public String glGetActiveAttrib(int program, int index, IntBuffer size, Buffer type) {
        int[] length = new int[1];
        int[] sizeTmp = new int[2];
        int[] typeTmp = new int[1];
        byte[] name = new byte[256];
        this.gl.glGetActiveAttrib(program, index, 256, length, 0, sizeTmp, 0, typeTmp, 0, name, 0);
        size.put(sizeTmp[0]);
        if (type instanceof IntBuffer) {
            ((IntBuffer) type).put(typeTmp[0]);
        }
        return new String(name, 0, length[0]);
    }

    public String glGetActiveUniform(int program, int index, IntBuffer size, Buffer type) {
        int[] length = new int[1];
        int[] sizeTmp = new int[2];
        int[] typeTmp = new int[1];
        byte[] name = new byte[256];
        this.gl.glGetActiveUniform(program, index, 256, length, 0, sizeTmp, 0, typeTmp, 0, name, 0);
        size.put(sizeTmp[0]);
        if (type instanceof IntBuffer) {
            ((IntBuffer) type).put(typeTmp[0]);
        }
        return new String(name, 0, length[0]);
    }

    public void glGetAttachedShaders(int program, int maxcount, Buffer count, IntBuffer shaders) {
        this.gl.glGetAttachedShaders(program, maxcount, (IntBuffer) count, shaders);
    }

    public int glGetAttribLocation(int program, String name) {
        return this.gl.glGetAttribLocation(program, name);
    }

    public void glGetBooleanv(int pname, Buffer params) {
        throw new UnsupportedOperationException("not implemented");
    }

    public void glGetBufferParameteriv(int target, int pname, IntBuffer params) {
        this.gl.glGetBufferParameteriv(target, pname, params);
    }

    public int glGetError() {
        return this.gl.glGetError();
    }

    public void glGetFloatv(int pname, FloatBuffer params) {
        this.gl.glGetFloatv(pname, params);
    }

    public void glGetFramebufferAttachmentParameteriv(int target, int attachment, int pname, IntBuffer params) {
        this.gl.glGetFramebufferAttachmentParameterivEXT(target, attachment, pname, params);
    }

    public void glGetIntegerv(int pname, IntBuffer params) {
        this.gl.glGetIntegerv(pname, params);
    }

    public String glGetProgramInfoLog(int program) {
        ByteBuffer buffer = ByteBuffer.allocateDirect(10240);
        buffer.order(ByteOrder.nativeOrder());
        ByteBuffer tmp = ByteBuffer.allocateDirect(4);
        tmp.order(ByteOrder.nativeOrder());
        IntBuffer intBuffer = tmp.asIntBuffer();
        this.gl.glGetProgramInfoLog(program, 10240, intBuffer, buffer);
        byte[] bytes = new byte[intBuffer.get(0)];
        buffer.get(bytes);
        return new String(bytes);
    }

    public void glGetProgramiv(int program, int pname, IntBuffer params) {
        this.gl.glGetProgramiv(program, pname, params);
    }

    public void glGetRenderbufferParameteriv(int target, int pname, IntBuffer params) {
        this.gl.glGetRenderbufferParameterivEXT(target, pname, params);
    }

    public String glGetShaderInfoLog(int shader) {
        ByteBuffer buffer = ByteBuffer.allocateDirect(10240);
        buffer.order(ByteOrder.nativeOrder());
        ByteBuffer tmp = ByteBuffer.allocateDirect(4);
        tmp.order(ByteOrder.nativeOrder());
        IntBuffer intBuffer = tmp.asIntBuffer();
        this.gl.glGetShaderInfoLog(shader, 10240, intBuffer, buffer);
        byte[] bytes = new byte[intBuffer.get(0)];
        buffer.get(bytes);
        return new String(bytes);
    }

    public void glGetShaderPrecisionFormat(int shadertype, int precisiontype, IntBuffer range, IntBuffer precision) {
        throw new UnsupportedOperationException("unsupported, won't implement");
    }

    public void glGetShaderSource(int shader, int bufsize, Buffer length, String source) {
        throw new UnsupportedOperationException("unsupported, won't implement.");
    }

    public void glGetShaderiv(int shader, int pname, IntBuffer params) {
        this.gl.glGetShaderiv(shader, pname, params);
    }

    public String glGetString(int name) {
        return this.gl.glGetString(name);
    }

    public void glGetTexParameterfv(int target, int pname, FloatBuffer params) {
        this.gl.glGetTexParameterfv(target, pname, params);
    }

    public void glGetTexParameteriv(int target, int pname, IntBuffer params) {
        this.gl.glGetTexParameteriv(target, pname, params);
    }

    public int glGetUniformLocation(int program, String name) {
        return this.gl.glGetUniformLocation(program, name);
    }

    public void glGetUniformfv(int program, int location, FloatBuffer params) {
        this.gl.glGetUniformfv(program, location, params);
    }

    public void glGetUniformiv(int program, int location, IntBuffer params) {
        this.gl.glGetUniformiv(program, location, params);
    }

    public void glGetVertexAttribPointerv(int index, int pname, Buffer pointer) {
        throw new UnsupportedOperationException("unsupported, won't implement");
    }

    public void glGetVertexAttribfv(int index, int pname, FloatBuffer params) {
        this.gl.glGetVertexAttribfv(index, pname, params);
    }

    public void glGetVertexAttribiv(int index, int pname, IntBuffer params) {
        this.gl.glGetVertexAttribiv(index, pname, params);
    }

    public void glHint(int target, int mode) {
        this.gl.glHint(target, mode);
    }

    public boolean glIsBuffer(int buffer) {
        return this.gl.glIsBuffer(buffer);
    }

    public boolean glIsEnabled(int cap) {
        return this.gl.glIsEnabled(cap);
    }

    public boolean glIsFramebuffer(int framebuffer) {
        return this.gl.glIsFramebufferEXT(framebuffer);
    }

    public boolean glIsProgram(int program) {
        return this.gl.glIsProgram(program);
    }

    public boolean glIsRenderbuffer(int renderbuffer) {
        return this.gl.glIsRenderbufferEXT(renderbuffer);
    }

    public boolean glIsShader(int shader) {
        return this.gl.glIsShader(shader);
    }

    public boolean glIsTexture(int texture) {
        return this.gl.glIsTexture(texture);
    }

    public void glLineWidth(float width) {
        this.gl.glLineWidth(width);
    }

    public void glLinkProgram(int program) {
        this.gl.glLinkProgram(program);
    }

    public void glPixelStorei(int pname, int param) {
        this.gl.glPixelStorei(pname, param);
    }

    public void glPolygonOffset(float factor, float units) {
        this.gl.glPolygonOffset(factor, units);
    }

    public void glReadPixels(int x, int y, int width, int height, int format, int type, Buffer pixels) {
        this.gl.glReadPixels(x, y, width, height, format, type, pixels);
    }

    public void glReleaseShaderCompiler() {
    }

    public void glRenderbufferStorage(int target, int internalformat, int width, int height) {
        this.gl.glRenderbufferStorageEXT(target, internalformat, width, height);
    }

    public void glSampleCoverage(float value, boolean invert) {
        this.gl.glSampleCoverage(value, invert);
    }

    public void glScissor(int x, int y, int width, int height) {
        this.gl.glScissor(x, y, width, height);
    }

    public void glShaderBinary(int n, IntBuffer shaders, int binaryformat, Buffer binary, int length) {
        throw new UnsupportedOperationException("unsupported, won't implement");
    }

    public void glShaderSource(int shader, String string) {
        this.gl.glShaderSource(shader, 1, new String[]{string}, (int[]) null, 0);
    }

    public void glStencilFunc(int func, int ref, int mask) {
        this.gl.glStencilFunc(func, ref, mask);
    }

    public void glStencilFuncSeparate(int face, int func, int ref, int mask) {
        this.gl.glStencilFuncSeparate(face, func, ref, mask);
    }

    public void glStencilMask(int mask) {
        this.gl.glStencilMask(mask);
    }

    public void glStencilMaskSeparate(int face, int mask) {
        this.gl.glStencilMaskSeparate(face, mask);
    }

    public void glStencilOp(int fail, int zfail, int zpass) {
        this.gl.glStencilOp(fail, zfail, zpass);
    }

    public void glStencilOpSeparate(int face, int fail, int zfail, int zpass) {
        this.gl.glStencilOpSeparate(face, fail, zfail, zpass);
    }

    public void glTexImage2D(int target, int level, int internalformat, int width, int height, int border, int format, int type, Buffer pixels) {
        this.gl.glTexImage2D(target, level, internalformat, width, height, border, format, type, pixels);
    }

    public void glTexParameterf(int target, int pname, float param) {
        this.gl.glTexParameterf(target, pname, param);
    }

    public void glTexParameterfv(int target, int pname, FloatBuffer params) {
        this.gl.glTexParameterfv(target, pname, params);
    }

    public void glTexParameteri(int target, int pname, int param) {
        this.gl.glTexParameteri(target, pname, param);
    }

    public void glTexParameteriv(int target, int pname, IntBuffer params) {
        this.gl.glTexParameteriv(target, pname, params);
    }

    public void glTexSubImage2D(int target, int level, int xoffset, int yoffset, int width, int height, int format, int type, Buffer pixels) {
        this.gl.glTexSubImage2D(target, level, xoffset, yoffset, width, height, format, type, pixels);
    }

    public void glUniform1f(int location, float x) {
        this.gl.glUniform1f(location, x);
    }

    public void glUniform1fv(int location, int count, FloatBuffer v) {
        this.gl.glUniform1fv(location, count, v);
    }

    public void glUniform1i(int location, int x) {
        this.gl.glUniform1i(location, x);
    }

    public void glUniform1iv(int location, int count, IntBuffer v) {
        this.gl.glUniform1iv(location, count, v);
    }

    public void glUniform2f(int location, float x, float y) {
        this.gl.glUniform2f(location, x, y);
    }

    public void glUniform2fv(int location, int count, FloatBuffer v) {
        this.gl.glUniform2fv(location, count, v);
    }

    public void glUniform2i(int location, int x, int y) {
        this.gl.glUniform2i(location, x, y);
    }

    public void glUniform2iv(int location, int count, IntBuffer v) {
        this.gl.glUniform2iv(location, count, v);
    }

    public void glUniform3f(int location, float x, float y, float z) {
        this.gl.glUniform3f(location, x, y, z);
    }

    public void glUniform3fv(int location, int count, FloatBuffer v) {
        this.gl.glUniform3fv(location, count, v);
    }

    public void glUniform3i(int location, int x, int y, int z) {
        this.gl.glUniform3i(location, x, y, z);
    }

    public void glUniform3iv(int location, int count, IntBuffer v) {
        this.gl.glUniform3iv(location, count, v);
    }

    public void glUniform4f(int location, float x, float y, float z, float w) {
        this.gl.glUniform4f(location, x, y, z, w);
    }

    public void glUniform4fv(int location, int count, FloatBuffer v) {
        this.gl.glUniform4fv(location, count, v);
    }

    public void glUniform4i(int location, int x, int y, int z, int w) {
        this.gl.glUniform4i(location, x, y, z, w);
    }

    public void glUniform4iv(int location, int count, IntBuffer v) {
        this.gl.glUniform4iv(location, count, v);
    }

    public void glUniformMatrix2fv(int location, int count, boolean transpose, FloatBuffer value) {
        this.gl.glUniformMatrix2fv(location, count, transpose, value);
    }

    public void glUniformMatrix3fv(int location, int count, boolean transpose, FloatBuffer value) {
        this.gl.glUniformMatrix3fv(location, count, transpose, value);
    }

    public void glUniformMatrix4fv(int location, int count, boolean transpose, FloatBuffer value) {
        this.gl.glUniformMatrix4fv(location, count, transpose, value);
    }

    public void glUseProgram(int program) {
        this.gl.glUseProgram(program);
    }

    public void glValidateProgram(int program) {
        this.gl.glValidateProgram(program);
    }

    public void glVertexAttrib1f(int indx, float x) {
        this.gl.glVertexAttrib1f(indx, x);
    }

    public void glVertexAttrib1fv(int indx, FloatBuffer values) {
        this.gl.glVertexAttrib1fv(indx, values);
    }

    public void glVertexAttrib2f(int indx, float x, float y) {
        this.gl.glVertexAttrib2f(indx, x, y);
    }

    public void glVertexAttrib2fv(int indx, FloatBuffer values) {
        this.gl.glVertexAttrib2fv(indx, values);
    }

    public void glVertexAttrib3f(int indx, float x, float y, float z) {
        this.gl.glVertexAttrib3f(indx, x, y, z);
    }

    public void glVertexAttrib3fv(int indx, FloatBuffer values) {
        this.gl.glVertexAttrib3fv(indx, values);
    }

    public void glVertexAttrib4f(int indx, float x, float y, float z, float w) {
        this.gl.glVertexAttrib4f(indx, x, y, z, w);
    }

    public void glVertexAttrib4fv(int indx, FloatBuffer values) {
        this.gl.glVertexAttrib4fv(indx, values);
    }

    public void glVertexAttribPointer(int indx, int size, int type, boolean normalized, int stride, Buffer ptr) {
        this.gl.glVertexAttribPointer(indx, size, type, normalized, stride, ptr);
    }

    public void glViewport(int x, int y, int width, int height) {
        this.gl.glViewport(x, y, width, height);
    }

    public void glDrawElements(int mode, int count, int type, int indices) {
        this.gl.glDrawElements(mode, count, type, (long) indices);
    }

    public void glVertexAttribPointer(int indx, int size, int type, boolean normalized, int stride, int ptr) {
        this.gl.glVertexAttribPointer(indx, size, type, normalized, stride, (long) ptr);
    }
}
