package com.badlogic.gdx.backends.jogl;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Audio;
import com.badlogic.gdx.Files;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.backends.jogl.JoglGraphics;
import com.badlogic.gdx.backends.openal.OpenALAudio;
import com.badlogic.gdx.utils.GdxRuntimeException;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.DisplayMode;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Window;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.media.opengl.GLCanvas;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public final class JoglApplication implements Application {
    OpenALAudio audio;
    JoglFiles files;
    JFrame frame;
    JoglGraphics graphics;
    JoglInput input;
    int logLevel = 1;
    Map<String, Preferences> preferences = new HashMap();
    List<Runnable> runnables = new ArrayList();
    final WindowAdapter windowListener = new WindowAdapter() {
        public void windowOpened(WindowEvent arg0) {
            JoglApplication.this.graphics.getCanvas().requestFocus();
            JoglApplication.this.graphics.getCanvas().requestFocusInWindow();
        }

        public void windowIconified(WindowEvent arg0) {
        }

        public void windowDeiconified(WindowEvent arg0) {
        }

        public void windowClosing(WindowEvent arg0) {
            JoglApplication.this.graphics.pause();
            JoglApplication.this.graphics.destroy();
            JoglApplication.this.audio.dispose();
            JoglApplication.this.frame.remove(JoglApplication.this.graphics.getCanvas());
        }
    };

    public JoglApplication(final ApplicationListener listener, String title, int width, int height, boolean useGL20IfAvailable) {
        final JoglApplicationConfiguration config = new JoglApplicationConfiguration();
        config.title = title;
        config.width = width;
        config.height = height;
        config.useGL20 = useGL20IfAvailable;
        if (!SwingUtilities.isEventDispatchThread()) {
            try {
                SwingUtilities.invokeAndWait(new Runnable() {
                    public void run() {
                        JoglApplication.this.initialize(listener, config);
                    }
                });
            } catch (Exception e) {
                throw new GdxRuntimeException("Creating window failed", e);
            }
        } else {
            config.useGL20 = useGL20IfAvailable;
            initialize(listener, config);
        }
    }

    public JoglApplication(final ApplicationListener listener, final JoglApplicationConfiguration config) {
        if (!SwingUtilities.isEventDispatchThread()) {
            try {
                SwingUtilities.invokeAndWait(new Runnable() {
                    public void run() {
                        JoglApplication.this.initialize(listener, config);
                    }
                });
            } catch (Exception e) {
                throw new GdxRuntimeException("Creating window failed", e);
            }
        } else {
            initialize(listener, config);
        }
    }

    /* access modifiers changed from: package-private */
    public void initialize(ApplicationListener listener, JoglApplicationConfiguration config) {
        JoglNativesLoader.load();
        this.graphics = new JoglGraphics(listener, config);
        this.input = new JoglInput(this.graphics.getCanvas());
        this.audio = new OpenALAudio();
        this.files = new JoglFiles();
        Gdx.app = this;
        Gdx.graphics = getGraphics();
        Gdx.input = getInput();
        Gdx.audio = getAudio();
        Gdx.files = getFiles();
        if (!config.fullscreen) {
            this.frame = new JFrame(config.title);
            this.graphics.getCanvas().setPreferredSize(new Dimension(config.width, config.height));
            this.frame.setSize(config.width + this.frame.getInsets().left + this.frame.getInsets().right, this.frame.getInsets().top + this.frame.getInsets().bottom + config.height);
            this.frame.add(this.graphics.getCanvas(), "Center");
            this.frame.setDefaultCloseOperation(2);
            this.frame.setLocationRelativeTo((Component) null);
            this.frame.addWindowListener(this.windowListener);
            this.frame.pack();
            this.frame.setVisible(true);
            this.graphics.create();
            return;
        }
        GraphicsDevice device = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
        this.frame = new JFrame(config.title);
        this.graphics.getCanvas().setPreferredSize(new Dimension(config.width, config.height));
        this.frame.setSize(config.width + this.frame.getInsets().left + this.frame.getInsets().right, this.frame.getInsets().top + this.frame.getInsets().bottom + config.height);
        this.frame.add(this.graphics.getCanvas(), "Center");
        this.frame.setDefaultCloseOperation(2);
        this.frame.setLocationRelativeTo((Component) null);
        this.frame.addWindowListener(this.windowListener);
        this.frame.setUndecorated(true);
        this.frame.setResizable(false);
        this.frame.pack();
        this.frame.setVisible(true);
        DisplayMode desktopMode = device.getDisplayMode();
        try {
            device.setFullScreenWindow(this.frame);
            JoglGraphics.JoglDisplayMode mode = this.graphics.findBestMatch(config.width, config.height);
            if (mode == null) {
                throw new GdxRuntimeException("Couldn't set fullscreen mode " + config.width + "x" + config.height);
            }
            device.setDisplayMode(mode.mode);
            this.graphics.create();
        } catch (Throwable th) {
            th.printStackTrace();
            device.setDisplayMode(desktopMode);
            device.setFullScreenWindow((Window) null);
            this.frame.dispose();
            this.audio.dispose();
            System.exit(-1);
        }
    }

    public Audio getAudio() {
        return this.audio;
    }

    public Files getFiles() {
        return this.files;
    }

    public Graphics getGraphics() {
        return this.graphics;
    }

    public Input getInput() {
        return this.input;
    }

    public Application.ApplicationType getType() {
        return Application.ApplicationType.Desktop;
    }

    public int getVersion() {
        return 0;
    }

    public long getJavaHeap() {
        return Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
    }

    public long getNativeHeap() {
        return getJavaHeap();
    }

    public JFrame getJFrame() {
        return this.frame;
    }

    public GLCanvas getGLCanvas() {
        return this.graphics.canvas;
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public Preferences getPreferences(String name) {
        if (this.preferences.containsKey(name)) {
            return this.preferences.get(name);
        }
        Preferences prefs = new JoglPreferences(name);
        this.preferences.put(name, prefs);
        return prefs;
    }

    public void postRunnable(Runnable runnable) {
        synchronized (this.runnables) {
            this.runnables.add(runnable);
        }
    }

    public void log(String tag, String message) {
        if (this.logLevel >= 1) {
            System.out.println(String.valueOf(tag) + ":" + message);
        }
    }

    public void log(String tag, String message, Exception exception) {
        if (this.logLevel >= 1) {
            System.out.println(String.valueOf(tag) + ":" + message);
            exception.printStackTrace(System.out);
        }
    }

    public void error(String tag, String message) {
        if (this.logLevel >= 2) {
            System.err.println(String.valueOf(tag) + ":" + message);
        }
    }

    public void error(String tag, String message, Exception exception) {
        if (this.logLevel >= 2) {
            System.err.println(String.valueOf(tag) + ":" + message);
            exception.printStackTrace(System.err);
        }
    }

    public void setLogLevel(int logLevel2) {
        this.logLevel = logLevel2;
    }

    public void exit() {
        postRunnable(new Runnable() {
            public void run() {
                JoglApplication.this.graphics.listener.pause();
                JoglApplication.this.graphics.listener.dispose();
                System.exit(-1);
            }
        });
    }

    public void gotoHttp() {
    }

    public void sendEmail() {
    }

    public void recommendRus() {
    }

    public void recommendEng() {
    }

    public void gotoMarket(byte version) {
    }
}
