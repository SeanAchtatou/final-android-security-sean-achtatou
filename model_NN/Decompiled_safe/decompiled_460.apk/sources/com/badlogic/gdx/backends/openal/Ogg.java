package com.badlogic.gdx.backends.openal;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL10;
import java.io.ByteArrayOutputStream;

public class Ogg {

    public static class Music extends OpenALMusic {
        private OggInputStream input;

        public Music(OpenALAudio audio, FileHandle file) {
            super(audio, file);
            this.input = new OggInputStream(file.read());
            setup(this.input.getChannels(), this.input.getSampleRate());
        }

        /* access modifiers changed from: protected */
        public int read(byte[] buffer) {
            if (this.input == null) {
                this.input = new OggInputStream(this.file.read());
                setup(this.input.getChannels(), this.input.getSampleRate());
            }
            return this.input.read(buffer);
        }

        /* access modifiers changed from: protected */
        public void reset() {
            if (this.input != null) {
                this.input.close();
                this.input = null;
            }
        }
    }

    public static class Sound extends OpenALSound {
        public Sound(OpenALAudio audio, FileHandle file) {
            super(audio);
            int length;
            OggInputStream input = new OggInputStream(file.read());
            ByteArrayOutputStream output = new ByteArrayOutputStream(4096);
            byte[] buffer = new byte[GL10.GL_EXP];
            while (!input.atEnd() && (length = input.read(buffer)) != -1) {
                output.write(buffer, 0, length);
            }
            setup(output.toByteArray(), input.getChannels(), input.getSampleRate());
        }
    }
}
