package com.badlogic.gdx.backends.openal;

import com.badlogic.gdx.audio.Sound;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.lwjgl.openal.AL10;

public class OpenALSound implements Sound {
    private final OpenALAudio audio;
    private int bufferID = -1;

    public OpenALSound(OpenALAudio audio2) {
        this.audio = audio2;
    }

    /* access modifiers changed from: package-private */
    public void setup(byte[] pcm, int channels, int sampleRate) {
        int bytes = pcm.length - (pcm.length % (channels > 1 ? 4 : 2));
        ByteBuffer buffer = ByteBuffer.allocateDirect(bytes);
        buffer.order(ByteOrder.nativeOrder());
        buffer.put(pcm, 0, bytes);
        buffer.flip();
        if (this.bufferID == -1) {
            this.bufferID = AL10.alGenBuffers();
            AL10.alBufferData(this.bufferID, channels > 1 ? 4355 : 4353, buffer.asShortBuffer(), sampleRate);
        }
    }

    public void play() {
        play(1.0f);
    }

    public void play(float volume) {
        int sourceID = this.audio.obtainSource(false);
        if (sourceID != -1) {
            AL10.alSourcei(sourceID, 4105, this.bufferID);
            AL10.alSourcei(sourceID, 4103, 0);
            AL10.alSourcef(sourceID, 4106, volume);
            AL10.alSourcePlay(sourceID);
        }
    }

    public void loop() {
        int sourceID = this.audio.obtainSource(false);
        if (sourceID != -1) {
            AL10.alSourcei(sourceID, 4105, this.bufferID);
            AL10.alSourcei(sourceID, 4103, 1);
            AL10.alSourcePlay(sourceID);
        }
    }

    public void stop() {
        this.audio.stopSourcesWithBuffer(this.bufferID);
    }

    public void dispose() {
        if (this.bufferID != -1) {
            this.audio.freeBuffer(this.bufferID);
            AL10.alDeleteBuffers(this.bufferID);
            this.bufferID = -1;
        }
    }
}
