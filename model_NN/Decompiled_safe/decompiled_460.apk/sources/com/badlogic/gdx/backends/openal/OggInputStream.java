package com.badlogic.gdx.backends.openal;

import com.badlogic.gdx.utils.GdxRuntimeException;
import com.jcraft.jogg.Packet;
import com.jcraft.jogg.Page;
import com.jcraft.jogg.StreamState;
import com.jcraft.jogg.SyncState;
import com.jcraft.jorbis.Block;
import com.jcraft.jorbis.Comment;
import com.jcraft.jorbis.DspState;
import com.jcraft.jorbis.Info;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.lwjgl.BufferUtils;

public class OggInputStream extends InputStream {
    boolean bigEndian = ByteOrder.nativeOrder().equals(ByteOrder.BIG_ENDIAN);
    byte[] buffer;
    int bytes = 0;
    private Comment comment = new Comment();
    private byte[] convbuffer = new byte[this.convsize];
    private int convsize = 16384;
    private DspState dspState = new DspState();
    boolean endOfBitStream = true;
    private boolean endOfStream;
    boolean inited = false;
    private InputStream input;
    private Info oggInfo = new Info();
    private Packet packet = new Packet();
    private Page page = new Page();
    private ByteBuffer pcmBuffer = BufferUtils.createByteBuffer(2048000);
    private int readIndex;
    private StreamState streamState = new StreamState();
    private SyncState syncState = new SyncState();
    private int total;
    private Block vorbisBlock = new Block(this.dspState);

    public OggInputStream(InputStream input2) {
        this.input = input2;
        try {
            this.total = input2.available();
            init();
        } catch (IOException e) {
            throw new GdxRuntimeException(e);
        }
    }

    public int getLength() {
        return this.total;
    }

    public int getChannels() {
        return this.oggInfo.channels;
    }

    public int getSampleRate() {
        return this.oggInfo.rate;
    }

    private void init() {
        initVorbis();
        readPCM();
    }

    public int available() {
        return this.endOfStream ? 0 : 1;
    }

    private void initVorbis() {
        this.syncState.init();
    }

    /* JADX WARNING: CFG modification limit reached, blocks count: 163 */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00b7  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean getPageAndPacket() {
        /*
            r11 = this;
            r10 = 0
            r9 = 2
            r8 = 1
            r7 = 4096(0x1000, float:5.74E-42)
            com.jcraft.jogg.SyncState r4 = r11.syncState
            int r2 = r4.buffer(r7)
            com.jcraft.jogg.SyncState r4 = r11.syncState
            byte[] r4 = r4.data
            r11.buffer = r4
            byte[] r4 = r11.buffer
            if (r4 != 0) goto L_0x0019
            r11.endOfStream = r8
            r4 = r10
        L_0x0018:
            return r4
        L_0x0019:
            java.io.InputStream r4 = r11.input     // Catch:{ Exception -> 0x003c }
            byte[] r5 = r11.buffer     // Catch:{ Exception -> 0x003c }
            r6 = 4096(0x1000, float:5.74E-42)
            int r4 = r4.read(r5, r2, r6)     // Catch:{ Exception -> 0x003c }
            r11.bytes = r4     // Catch:{ Exception -> 0x003c }
            com.jcraft.jogg.SyncState r4 = r11.syncState
            int r5 = r11.bytes
            r4.wrote(r5)
            com.jcraft.jogg.SyncState r4 = r11.syncState
            com.jcraft.jogg.Page r5 = r11.page
            int r4 = r4.pageout(r5)
            if (r4 == r8) goto L_0x004e
            int r4 = r11.bytes
            if (r4 >= r7) goto L_0x0046
            r4 = r10
            goto L_0x0018
        L_0x003c:
            r4 = move-exception
            r0 = r4
            com.badlogic.gdx.utils.GdxRuntimeException r4 = new com.badlogic.gdx.utils.GdxRuntimeException
            java.lang.String r5 = "Failure reading Vorbis."
            r4.<init>(r5, r0)
            throw r4
        L_0x0046:
            com.badlogic.gdx.utils.GdxRuntimeException r4 = new com.badlogic.gdx.utils.GdxRuntimeException
            java.lang.String r5 = "Input does not appear to be an Ogg bitstream."
            r4.<init>(r5)
            throw r4
        L_0x004e:
            com.jcraft.jogg.StreamState r4 = r11.streamState
            com.jcraft.jogg.Page r5 = r11.page
            int r5 = r5.serialno()
            r4.init(r5)
            com.jcraft.jorbis.Info r4 = r11.oggInfo
            r4.init()
            com.jcraft.jorbis.Comment r4 = r11.comment
            r4.init()
            com.jcraft.jogg.StreamState r4 = r11.streamState
            com.jcraft.jogg.Page r5 = r11.page
            int r4 = r4.pagein(r5)
            if (r4 >= 0) goto L_0x0075
            com.badlogic.gdx.utils.GdxRuntimeException r4 = new com.badlogic.gdx.utils.GdxRuntimeException
            java.lang.String r5 = "Error reading first page of Ogg bitstream."
            r4.<init>(r5)
            throw r4
        L_0x0075:
            com.jcraft.jogg.StreamState r4 = r11.streamState
            com.jcraft.jogg.Packet r5 = r11.packet
            int r4 = r4.packetout(r5)
            if (r4 == r8) goto L_0x0087
            com.badlogic.gdx.utils.GdxRuntimeException r4 = new com.badlogic.gdx.utils.GdxRuntimeException
            java.lang.String r5 = "Error reading initial header packet."
            r4.<init>(r5)
            throw r4
        L_0x0087:
            com.jcraft.jorbis.Info r4 = r11.oggInfo
            com.jcraft.jorbis.Comment r5 = r11.comment
            com.jcraft.jogg.Packet r6 = r11.packet
            int r4 = r4.synthesis_headerin(r5, r6)
            if (r4 >= 0) goto L_0x009b
            com.badlogic.gdx.utils.GdxRuntimeException r4 = new com.badlogic.gdx.utils.GdxRuntimeException
            java.lang.String r5 = "Ogg bitstream does not contain Vorbis audio data."
            r4.<init>(r5)
            throw r4
        L_0x009b:
            r1 = 0
        L_0x009c:
            if (r1 < r9) goto L_0x00f2
            com.jcraft.jorbis.Info r4 = r11.oggInfo
            int r4 = r4.channels
            int r4 = r7 / r4
            r11.convsize = r4
            com.jcraft.jorbis.DspState r4 = r11.dspState
            com.jcraft.jorbis.Info r5 = r11.oggInfo
            r4.synthesis_init(r5)
            com.jcraft.jorbis.Block r4 = r11.vorbisBlock
            com.jcraft.jorbis.DspState r5 = r11.dspState
            r4.init(r5)
            r4 = r8
            goto L_0x0018
        L_0x00b7:
            com.jcraft.jogg.SyncState r4 = r11.syncState
            com.jcraft.jogg.Page r5 = r11.page
            int r3 = r4.pageout(r5)
            if (r3 != 0) goto L_0x00e7
        L_0x00c1:
            com.jcraft.jogg.SyncState r4 = r11.syncState
            int r2 = r4.buffer(r7)
            com.jcraft.jogg.SyncState r4 = r11.syncState
            byte[] r4 = r4.data
            r11.buffer = r4
            java.io.InputStream r4 = r11.input     // Catch:{ Exception -> 0x0116 }
            byte[] r5 = r11.buffer     // Catch:{ Exception -> 0x0116 }
            r6 = 4096(0x1000, float:5.74E-42)
            int r4 = r4.read(r5, r2, r6)     // Catch:{ Exception -> 0x0116 }
            r11.bytes = r4     // Catch:{ Exception -> 0x0116 }
            int r4 = r11.bytes
            if (r4 != 0) goto L_0x0120
            if (r1 >= r9) goto L_0x0120
            com.badlogic.gdx.utils.GdxRuntimeException r4 = new com.badlogic.gdx.utils.GdxRuntimeException
            java.lang.String r5 = "End of file before finding all Vorbis headers."
            r4.<init>(r5)
            throw r4
        L_0x00e7:
            if (r3 != r8) goto L_0x00f2
            com.jcraft.jogg.StreamState r4 = r11.streamState
            com.jcraft.jogg.Page r5 = r11.page
            r4.pagein(r5)
        L_0x00f0:
            if (r1 < r9) goto L_0x00f5
        L_0x00f2:
            if (r1 < r9) goto L_0x00b7
            goto L_0x00c1
        L_0x00f5:
            com.jcraft.jogg.StreamState r4 = r11.streamState
            com.jcraft.jogg.Packet r5 = r11.packet
            int r3 = r4.packetout(r5)
            if (r3 == 0) goto L_0x00f2
            r4 = -1
            if (r3 != r4) goto L_0x010a
            com.badlogic.gdx.utils.GdxRuntimeException r4 = new com.badlogic.gdx.utils.GdxRuntimeException
            java.lang.String r5 = "Corrupt secondary header."
            r4.<init>(r5)
            throw r4
        L_0x010a:
            com.jcraft.jorbis.Info r4 = r11.oggInfo
            com.jcraft.jorbis.Comment r5 = r11.comment
            com.jcraft.jogg.Packet r6 = r11.packet
            r4.synthesis_headerin(r5, r6)
            int r1 = r1 + 1
            goto L_0x00f0
        L_0x0116:
            r4 = move-exception
            r0 = r4
            com.badlogic.gdx.utils.GdxRuntimeException r4 = new com.badlogic.gdx.utils.GdxRuntimeException
            java.lang.String r5 = "Failed to read Vorbis."
            r4.<init>(r5, r0)
            throw r4
        L_0x0120:
            com.jcraft.jogg.SyncState r4 = r11.syncState
            int r5 = r11.bytes
            r4.wrote(r5)
            goto L_0x009c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.backends.openal.OggInputStream.getPageAndPacket():boolean");
    }

    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: CFG modification limit reached, blocks count: 189 */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x013d  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x015d  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0176  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0195  */
    private void readPCM() {
        /*
            r23 = this;
            r18 = 0
        L_0x0002:
            r0 = r23
            boolean r0 = r0.endOfBitStream
            r19 = r0
            if (r19 == 0) goto L_0x002a
            boolean r19 = r23.getPageAndPacket()
            if (r19 != 0) goto L_0x0022
            r0 = r23
            com.jcraft.jogg.SyncState r0 = r0.syncState
            r19 = r0
            r19.clear()
            r19 = 1
            r0 = r19
            r1 = r23
            r1.endOfStream = r0
        L_0x0021:
            return
        L_0x0022:
            r19 = 0
            r0 = r19
            r1 = r23
            r1.endOfBitStream = r0
        L_0x002a:
            r0 = r23
            boolean r0 = r0.inited
            r19 = r0
            if (r19 != 0) goto L_0x003b
            r19 = 1
            r0 = r19
            r1 = r23
            r1.inited = r0
            goto L_0x0021
        L_0x003b:
            r19 = 1
            r0 = r19
            float[][][] r0 = new float[r0][][]
            r5 = r0
            r0 = r23
            com.jcraft.jorbis.Info r0 = r0.oggInfo
            r19 = r0
            r0 = r19
            int r0 = r0.channels
            r19 = r0
            r0 = r19
            int[] r0 = new int[r0]
            r4 = r0
        L_0x0053:
            r0 = r23
            boolean r0 = r0.endOfBitStream
            r19 = r0
            if (r19 == 0) goto L_0x0112
            r0 = r23
            com.jcraft.jogg.StreamState r0 = r0.streamState
            r19 = r0
            r19.clear()
            r0 = r23
            com.jcraft.jorbis.Block r0 = r0.vorbisBlock
            r19 = r0
            r19.clear()
            r0 = r23
            com.jcraft.jorbis.DspState r0 = r0.dspState
            r19 = r0
            r19.clear()
            r0 = r23
            com.jcraft.jorbis.Info r0 = r0.oggInfo
            r19 = r0
            r19.clear()
            goto L_0x0002
        L_0x0080:
            r0 = r23
            com.jcraft.jogg.SyncState r0 = r0.syncState
            r19 = r0
            r0 = r23
            com.jcraft.jogg.Page r0 = r0.page
            r20 = r0
            int r15 = r19.pageout(r20)
            if (r15 != 0) goto L_0x0102
        L_0x0092:
            r0 = r23
            boolean r0 = r0.endOfBitStream
            r19 = r0
            if (r19 != 0) goto L_0x0053
            r19 = 0
            r0 = r19
            r1 = r23
            r1.bytes = r0
            r0 = r23
            com.jcraft.jogg.SyncState r0 = r0.syncState
            r19 = r0
            r20 = 4096(0x1000, float:5.74E-42)
            int r10 = r19.buffer(r20)
            if (r10 < 0) goto L_0x02c5
            r0 = r23
            com.jcraft.jogg.SyncState r0 = r0.syncState
            r19 = r0
            r0 = r19
            byte[] r0 = r0.data
            r19 = r0
            r0 = r19
            r1 = r23
            r1.buffer = r0
            r0 = r23
            java.io.InputStream r0 = r0.input     // Catch:{ Exception -> 0x02b5 }
            r19 = r0
            r0 = r23
            byte[] r0 = r0.buffer     // Catch:{ Exception -> 0x02b5 }
            r20 = r0
            r21 = 4096(0x1000, float:5.74E-42)
            r0 = r19
            r1 = r20
            r2 = r10
            r3 = r21
            int r19 = r0.read(r1, r2, r3)     // Catch:{ Exception -> 0x02b5 }
            r0 = r19
            r1 = r23
            r1.bytes = r0     // Catch:{ Exception -> 0x02b5 }
        L_0x00e1:
            r0 = r23
            com.jcraft.jogg.SyncState r0 = r0.syncState
            r19 = r0
            r0 = r23
            int r0 = r0.bytes
            r20 = r0
            r19.wrote(r20)
            r0 = r23
            int r0 = r0.bytes
            r19 = r0
            if (r19 != 0) goto L_0x0053
            r19 = 1
            r0 = r19
            r1 = r23
            r1.endOfBitStream = r0
            goto L_0x0053
        L_0x0102:
            r19 = -1
            r0 = r15
            r1 = r19
            if (r0 != r1) goto L_0x011c
            com.badlogic.gdx.Application r19 = com.badlogic.gdx.Gdx.app
            java.lang.String r20 = "gdx-audio"
            java.lang.String r21 = "Error reading OGG: Corrupt or missing data in bitstream."
            r19.log(r20, r21)
        L_0x0112:
            r0 = r23
            boolean r0 = r0.endOfBitStream
            r19 = r0
            if (r19 == 0) goto L_0x0080
            goto L_0x0092
        L_0x011c:
            r0 = r23
            com.jcraft.jogg.StreamState r0 = r0.streamState
            r19 = r0
            r0 = r23
            com.jcraft.jogg.Page r0 = r0.page
            r20 = r0
            r19.pagein(r20)
        L_0x012b:
            r0 = r23
            com.jcraft.jogg.StreamState r0 = r0.streamState
            r19 = r0
            r0 = r23
            com.jcraft.jogg.Packet r0 = r0.packet
            r20 = r0
            int r15 = r19.packetout(r20)
            if (r15 != 0) goto L_0x015d
            r0 = r23
            com.jcraft.jogg.Page r0 = r0.page
            r19 = r0
            int r19 = r19.eos()
            if (r19 == 0) goto L_0x0151
            r19 = 1
            r0 = r19
            r1 = r23
            r1.endOfBitStream = r0
        L_0x0151:
            r0 = r23
            boolean r0 = r0.endOfBitStream
            r19 = r0
            if (r19 != 0) goto L_0x0112
            if (r18 == 0) goto L_0x0112
            goto L_0x0021
        L_0x015d:
            r19 = -1
            r0 = r15
            r1 = r19
            if (r0 == r1) goto L_0x012b
            r0 = r23
            com.jcraft.jorbis.Block r0 = r0.vorbisBlock
            r19 = r0
            r0 = r23
            com.jcraft.jogg.Packet r0 = r0.packet
            r20 = r0
            int r19 = r19.synthesis(r20)
            if (r19 != 0) goto L_0x0185
            r0 = r23
            com.jcraft.jorbis.DspState r0 = r0.dspState
            r19 = r0
            r0 = r23
            com.jcraft.jorbis.Block r0 = r0.vorbisBlock
            r20 = r0
            r19.synthesis_blockin(r20)
        L_0x0185:
            r0 = r23
            com.jcraft.jorbis.DspState r0 = r0.dspState
            r19 = r0
            r0 = r19
            r1 = r5
            r2 = r4
            int r16 = r0.synthesis_pcmout(r1, r2)
            if (r16 <= 0) goto L_0x012b
            r19 = 0
            r13 = r5[r19]
            r0 = r23
            int r0 = r0.convsize
            r19 = r0
            r0 = r16
            r1 = r19
            if (r0 >= r1) goto L_0x01f0
            r6 = r16
        L_0x01a7:
            r9 = 0
        L_0x01a8:
            r0 = r23
            com.jcraft.jorbis.Info r0 = r0.oggInfo
            r19 = r0
            r0 = r19
            int r0 = r0.channels
            r19 = r0
            r0 = r9
            r1 = r19
            if (r0 < r1) goto L_0x01f9
            r0 = r23
            com.jcraft.jorbis.Info r0 = r0.oggInfo
            r19 = r0
            r0 = r19
            int r0 = r0.channels
            r19 = r0
            int r19 = r19 * 2
            int r7 = r19 * r6
            r0 = r23
            java.nio.ByteBuffer r0 = r0.pcmBuffer
            r19 = r0
            int r19 = r19.remaining()
            r0 = r7
            r1 = r19
            if (r0 < r1) goto L_0x028d
            com.badlogic.gdx.utils.GdxRuntimeException r19 = new com.badlogic.gdx.utils.GdxRuntimeException
            java.lang.StringBuilder r20 = new java.lang.StringBuilder
            java.lang.String r21 = "Ogg block too big to be buffered: "
            r20.<init>(r21)
            r0 = r20
            r1 = r7
            java.lang.StringBuilder r20 = r0.append(r1)
            java.lang.String r20 = r20.toString()
            r19.<init>(r20)
            throw r19
        L_0x01f0:
            r0 = r23
            int r0 = r0.convsize
            r19 = r0
            r6 = r19
            goto L_0x01a7
        L_0x01f9:
            int r14 = r9 * 2
            r12 = r4[r9]
            r11 = 0
        L_0x01fe:
            if (r11 < r6) goto L_0x0203
            int r9 = r9 + 1
            goto L_0x01a8
        L_0x0203:
            r19 = r13[r9]
            int r20 = r12 + r11
            r19 = r19[r20]
            r0 = r19
            double r0 = (double) r0
            r19 = r0
            r21 = 4674736138332667904(0x40dfffc000000000, double:32767.0)
            double r19 = r19 * r21
            r0 = r19
            int r0 = (int) r0
            r17 = r0
            r19 = 32767(0x7fff, float:4.5916E-41)
            r0 = r17
            r1 = r19
            if (r0 <= r1) goto L_0x0224
            r17 = 32767(0x7fff, float:4.5916E-41)
        L_0x0224:
            r19 = -32768(0xffffffffffff8000, float:NaN)
            r0 = r17
            r1 = r19
            if (r0 >= r1) goto L_0x022e
            r17 = -32768(0xffffffffffff8000, float:NaN)
        L_0x022e:
            if (r17 >= 0) goto L_0x0235
            r19 = 32768(0x8000, float:4.5918E-41)
            r17 = r17 | r19
        L_0x0235:
            r0 = r23
            boolean r0 = r0.bigEndian
            r19 = r0
            if (r19 == 0) goto L_0x026e
            r0 = r23
            byte[] r0 = r0.convbuffer
            r19 = r0
            int r20 = r17 >>> 8
            r0 = r20
            byte r0 = (byte) r0
            r20 = r0
            r19[r14] = r20
            r0 = r23
            byte[] r0 = r0.convbuffer
            r19 = r0
            int r20 = r14 + 1
            r0 = r17
            byte r0 = (byte) r0
            r21 = r0
            r19[r20] = r21
        L_0x025b:
            r0 = r23
            com.jcraft.jorbis.Info r0 = r0.oggInfo
            r19 = r0
            r0 = r19
            int r0 = r0.channels
            r19 = r0
            int r19 = r19 * 2
            int r14 = r14 + r19
            int r11 = r11 + 1
            goto L_0x01fe
        L_0x026e:
            r0 = r23
            byte[] r0 = r0.convbuffer
            r19 = r0
            r0 = r17
            byte r0 = (byte) r0
            r20 = r0
            r19[r14] = r20
            r0 = r23
            byte[] r0 = r0.convbuffer
            r19 = r0
            int r20 = r14 + 1
            int r21 = r17 >>> 8
            r0 = r21
            byte r0 = (byte) r0
            r21 = r0
            r19[r20] = r21
            goto L_0x025b
        L_0x028d:
            r0 = r23
            java.nio.ByteBuffer r0 = r0.pcmBuffer
            r19 = r0
            r0 = r23
            byte[] r0 = r0.convbuffer
            r20 = r0
            r21 = 0
            r0 = r19
            r1 = r20
            r2 = r21
            r3 = r7
            r0.put(r1, r2, r3)
            r18 = 1
            r0 = r23
            com.jcraft.jorbis.DspState r0 = r0.dspState
            r19 = r0
            r0 = r19
            r1 = r6
            r0.synthesis_read(r1)
            goto L_0x0185
        L_0x02b5:
            r19 = move-exception
            r8 = r19
            com.badlogic.gdx.utils.GdxRuntimeException r19 = new com.badlogic.gdx.utils.GdxRuntimeException
            java.lang.String r20 = "Error during Vorbis decoding."
            r0 = r19
            r1 = r20
            r2 = r8
            r0.<init>(r1, r2)
            throw r19
        L_0x02c5:
            r19 = 0
            r0 = r19
            r1 = r23
            r1.bytes = r0
            goto L_0x00e1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.backends.openal.OggInputStream.readPCM():void");
    }

    public int read() {
        if (this.readIndex >= this.pcmBuffer.position()) {
            this.pcmBuffer.clear();
            readPCM();
            this.readIndex = 0;
        }
        if (this.readIndex >= this.pcmBuffer.position()) {
            return -1;
        }
        int value = this.pcmBuffer.get(this.readIndex);
        if (value < 0) {
            value += 256;
        }
        this.readIndex++;
        return value;
    }

    public boolean atEnd() {
        return this.endOfStream && this.readIndex >= this.pcmBuffer.position();
    }

    public int read(byte[] b, int off, int len) {
        int i = 0;
        while (i < len) {
            int value = read();
            if (value >= 0) {
                b[i] = (byte) value;
                i++;
            } else if (i == 0) {
                return -1;
            } else {
                return i;
            }
        }
        return len;
    }

    public int read(byte[] b) {
        return read(b, 0, b.length);
    }

    public void close() {
    }
}
