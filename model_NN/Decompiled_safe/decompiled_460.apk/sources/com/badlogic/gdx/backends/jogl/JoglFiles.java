package com.badlogic.gdx.backends.jogl;

import com.badlogic.gdx.Files;
import com.badlogic.gdx.files.FileHandle;

final class JoglFiles implements Files {
    private final String externalPath = (String.valueOf(System.getProperty("user.home")) + "/");

    JoglFiles() {
    }

    public FileHandle getFileHandle(String fileName, Files.FileType type) {
        return new JoglFileHandle(fileName, type);
    }

    public FileHandle classpath(String path) {
        return new JoglFileHandle(path, Files.FileType.Classpath);
    }

    public FileHandle internal(String path) {
        return new JoglFileHandle(path, Files.FileType.Internal);
    }

    public FileHandle external(String path) {
        return new JoglFileHandle(path, Files.FileType.External);
    }

    public FileHandle absolute(String path) {
        return new JoglFileHandle(path, Files.FileType.Absolute);
    }

    public String getExternalStoragePath() {
        return this.externalPath;
    }

    public boolean isExternalStorageAvailable() {
        return true;
    }
}
