package com.badlogic.gdx.backends.jogl;

import java.awt.Component;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLException;
import javax.swing.JComponent;
import javax.swing.RepaintManager;
import javax.swing.SwingUtilities;

public class JoglAnimator {
    Map dirtyRegions = new IdentityHashMap();
    private Runnable drawWithRepaintManagerRunnable = new Runnable() {
        public void run() {
            for (JComponent comp : JoglAnimator.this.lightweights) {
                RepaintManager rm = RepaintManager.currentManager(comp);
                rm.markCompletelyDirty(comp);
                JoglAnimator.this.repaintManagers.put(rm, rm);
                Rectangle visible = comp.getVisibleRect();
                int x = visible.x;
                int y = visible.y;
                while (comp != null) {
                    x += comp.getX();
                    y += comp.getY();
                    Component c = comp.getParent();
                    if (c == null || !(c instanceof JComponent)) {
                        comp = null;
                    } else {
                        comp = (JComponent) c;
                        if (!comp.isOptimizedDrawingEnabled()) {
                            RepaintManager rm2 = RepaintManager.currentManager(comp);
                            JoglAnimator.this.repaintManagers.put(rm2, rm2);
                            Rectangle dirty = (Rectangle) JoglAnimator.this.dirtyRegions.get(comp);
                            if (dirty == null) {
                                JoglAnimator.this.dirtyRegions.put(comp, new Rectangle(x, y, visible.width, visible.height));
                            } else {
                                dirty.add(new Rectangle(x, y, visible.width, visible.height));
                            }
                        }
                    }
                }
            }
            for (JComponent comp2 : JoglAnimator.this.dirtyRegions.keySet()) {
                Rectangle rect = (Rectangle) JoglAnimator.this.dirtyRegions.get(comp2);
                RepaintManager.currentManager(comp2).addDirtyRegion(comp2, rect.x, rect.y, rect.width, rect.height);
            }
            for (RepaintManager paintDirtyRegions : JoglAnimator.this.repaintManagers.keySet()) {
                paintDirtyRegions.paintDirtyRegions();
            }
            JoglAnimator.this.dirtyRegions.clear();
            JoglAnimator.this.repaintManagers.clear();
        }
    };
    volatile ArrayList drawables = new ArrayList();
    protected boolean ignoreExceptions;
    List lightweights = new ArrayList();
    protected boolean printExceptions;
    Map repaintManagers = new IdentityHashMap();
    boolean runAsFastAsPossible;
    private Runnable runnable;
    volatile boolean shouldStop;
    Thread thread;

    public JoglAnimator() {
    }

    public JoglAnimator(GLAutoDrawable drawable) {
        add(drawable);
    }

    public synchronized void add(GLAutoDrawable drawable) {
        ArrayList newList = (ArrayList) this.drawables.clone();
        newList.add(drawable);
        this.drawables = newList;
        notifyAll();
    }

    public synchronized void remove(GLAutoDrawable drawable) {
        ArrayList newList = (ArrayList) this.drawables.clone();
        newList.remove(drawable);
        this.drawables = newList;
    }

    public Iterator drawableIterator() {
        return this.drawables.iterator();
    }

    public void setIgnoreExceptions(boolean ignoreExceptions2) {
        this.ignoreExceptions = ignoreExceptions2;
    }

    public void setPrintExceptions(boolean printExceptions2) {
        this.printExceptions = printExceptions2;
    }

    public final void setRunAsFastAsPossible(boolean runFast) {
        this.runAsFastAsPossible = runFast;
    }

    /* access modifiers changed from: protected */
    public void display() {
        Iterator iter = drawableIterator();
        while (iter.hasNext()) {
            GLAutoDrawable drawable = (GLAutoDrawable) iter.next();
            if (drawable instanceof JComponent) {
                this.lightweights.add(drawable);
            } else {
                try {
                    drawable.display();
                } catch (RuntimeException e) {
                    if (!this.ignoreExceptions) {
                        throw e;
                    } else if (this.printExceptions) {
                        e.printStackTrace();
                    }
                }
            }
        }
        if (this.lightweights.size() > 0) {
            try {
                SwingUtilities.invokeAndWait(this.drawWithRepaintManagerRunnable);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            this.lightweights.clear();
        }
    }

    class MainLoop implements Runnable {
        MainLoop() {
        }

        public void run() {
            while (!JoglAnimator.this.shouldStop) {
                try {
                    if (JoglAnimator.this.drawables.size() == 0) {
                        synchronized (JoglAnimator.this) {
                            while (JoglAnimator.this.drawables.size() == 0 && !JoglAnimator.this.shouldStop) {
                                try {
                                    JoglAnimator.this.wait();
                                } catch (InterruptedException e) {
                                }
                            }
                        }
                    }
                    JoglAnimator.this.display();
                    if (!JoglAnimator.this.runAsFastAsPossible) {
                        Thread.yield();
                    }
                } catch (Throwable th) {
                    JoglAnimator.this.shouldStop = false;
                    synchronized (JoglAnimator.this) {
                        JoglAnimator.this.thread = null;
                        JoglAnimator.this.notify();
                        throw th;
                    }
                }
            }
            JoglAnimator.this.shouldStop = false;
            synchronized (JoglAnimator.this) {
                JoglAnimator.this.thread = null;
                JoglAnimator.this.notify();
            }
        }
    }

    public synchronized void start() {
        if (this.thread != null) {
            throw new GLException("Already started");
        }
        if (this.runnable == null) {
            this.runnable = new MainLoop();
        }
        this.thread = new Thread(this.runnable);
        this.thread.start();
    }

    public synchronized boolean isAnimating() {
        return this.thread != null;
    }

    /* JADX WARNING: CFG modification limit reached, blocks count: 129 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void stop() {
        /*
            r2 = this;
            monitor-enter(r2)
            r0 = 1
            r2.shouldStop = r0     // Catch:{ all -> 0x0034 }
            r2.notifyAll()     // Catch:{ all -> 0x0034 }
            java.lang.Thread r0 = java.lang.Thread.currentThread()     // Catch:{ all -> 0x0034 }
            java.lang.Thread r1 = r2.thread     // Catch:{ all -> 0x0034 }
            if (r0 == r1) goto L_0x0015
            boolean r0 = java.awt.EventQueue.isDispatchThread()     // Catch:{ all -> 0x0034 }
            if (r0 == 0) goto L_0x001a
        L_0x0015:
            monitor-exit(r2)
            return
        L_0x0017:
            r2.wait()     // Catch:{ InterruptedException -> 0x0032 }
        L_0x001a:
            boolean r0 = r2.shouldStop     // Catch:{ all -> 0x0034 }
            if (r0 == 0) goto L_0x0022
            java.lang.Thread r0 = r2.thread     // Catch:{ all -> 0x0034 }
            if (r0 != 0) goto L_0x0017
        L_0x0022:
            java.lang.Thread r0 = r2.thread     // Catch:{ all -> 0x0034 }
            boolean r0 = r0.isAlive()     // Catch:{ all -> 0x0034 }
            if (r0 == 0) goto L_0x0015
            r0 = 10
            java.lang.Thread.sleep(r0)     // Catch:{ InterruptedException -> 0x0030 }
            goto L_0x0022
        L_0x0030:
            r0 = move-exception
            goto L_0x0022
        L_0x0032:
            r0 = move-exception
            goto L_0x001a
        L_0x0034:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.backends.jogl.JoglAnimator.stop():void");
    }
}
