package com.badlogic.gdx.graphics.g2d;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.NumberUtils;

public class BitmapFontCache implements Disposable {
    private float color = Color.WHITE.toFloatBits();
    private final BitmapFont font;
    private int idx;
    private boolean integer = true;
    private final BitmapFont.TextBounds textBounds = new BitmapFont.TextBounds();
    private final Color tmpColor = new Color(Color.WHITE);
    private float[] vertices = new float[0];
    private float x;
    private float y;

    public BitmapFontCache(BitmapFont font2) {
        this.font = font2;
    }

    public BitmapFontCache(BitmapFont font2, boolean integer2) {
        this.font = font2;
        this.integer = integer2;
    }

    public void setPosition(float x2, float y2) {
        translate(x2 - this.x, y2 - this.y);
    }

    public void translate(float xAmount, float yAmount) {
        if (xAmount != 0.0f || yAmount != 0.0f) {
            if (this.integer) {
                xAmount = (float) ((int) xAmount);
                yAmount = (float) ((int) yAmount);
            }
            this.x += xAmount;
            this.y += yAmount;
            float[] vertices2 = this.vertices;
            int n = this.idx;
            for (int i = 0; i < n; i += 5) {
                vertices2[i] = vertices2[i] + xAmount;
                int i2 = i + 1;
                vertices2[i2] = vertices2[i2] + yAmount;
            }
        }
    }

    public void setColor(Color tint) {
        float color2 = tint.toFloatBits();
        if (color2 != this.color) {
            this.color = color2;
            float[] vertices2 = this.vertices;
            int n = this.idx;
            for (int i = 2; i < n; i += 5) {
                vertices2[i] = color2;
            }
        }
    }

    public void setColor(float r, float g, float b, float a) {
        float color2 = NumberUtils.intBitsToFloat(-16777217 & ((((int) (255.0f * a)) << 24) | (((int) (255.0f * b)) << 16) | (((int) (255.0f * g)) << 8) | ((int) (255.0f * r))));
        if (color2 != this.color) {
            this.color = color2;
            float[] vertices2 = this.vertices;
            int n = this.idx;
            for (int i = 2; i < n; i += 5) {
                vertices2[i] = color2;
            }
        }
    }

    public void draw(SpriteBatch spriteBatch) {
        spriteBatch.draw(this.font.getRegion().getTexture(), this.vertices, 0, this.idx);
    }

    public void draw(SpriteBatch spriteBatch, float alphaModulation) {
        Color color2 = getColor();
        float oldAlpha = color2.a;
        color2.a *= alphaModulation;
        setColor(color2);
        draw(spriteBatch);
        color2.a = oldAlpha;
        setColor(color2);
    }

    public Color getColor() {
        float f = this.color;
        int intBits = NumberUtils.floatToRawIntBits(this.color);
        Color color2 = this.tmpColor;
        color2.r = ((float) (intBits & 255)) / 255.0f;
        color2.g = ((float) ((intBits >>> 8) & 255)) / 255.0f;
        color2.b = ((float) ((intBits >>> 16) & 255)) / 255.0f;
        color2.a = ((float) ((intBits >>> 24) & 255)) / 255.0f;
        return color2;
    }

    private void reset(int glyphCount) {
        this.x = 0.0f;
        this.y = 0.0f;
        this.idx = 0;
        int vertexCount = glyphCount * 20;
        if (this.vertices == null || this.vertices.length < vertexCount) {
            this.vertices = new float[vertexCount];
        }
    }

    private float addToCache(CharSequence str, float x2, float y2, int start, int end) {
        int start2;
        int start3;
        float startX = x2;
        BitmapFont font2 = this.font;
        BitmapFont.Glyph lastGlyph = null;
        if (font2.data.scaleX == 1.0f && font2.data.scaleY == 1.0f) {
            while (true) {
                int start4 = start;
                if (start4 >= end) {
                    break;
                }
                start = start4 + 1;
                lastGlyph = font2.data.getGlyph(str.charAt(start4));
                if (lastGlyph != null) {
                    addGlyph(lastGlyph, x2 + ((float) lastGlyph.xoffset), y2 + ((float) lastGlyph.yoffset), (float) lastGlyph.width, (float) lastGlyph.height);
                    x2 += (float) lastGlyph.xadvance;
                    start4 = start;
                    break;
                }
            }
            while (start3 < end) {
                int start5 = start3 + 1;
                char ch = str.charAt(start3);
                BitmapFont.Glyph g = font2.data.getGlyph(ch);
                if (g != null) {
                    float x3 = x2 + ((float) lastGlyph.getKerning(ch));
                    lastGlyph = g;
                    addGlyph(lastGlyph, x3 + ((float) g.xoffset), y2 + ((float) g.yoffset), (float) g.width, (float) g.height);
                    x2 = x3 + ((float) g.xadvance);
                    start3 = start5;
                } else {
                    start3 = start5;
                }
            }
        } else {
            float scaleX = font2.data.scaleX;
            float scaleY = font2.data.scaleY;
            while (true) {
                int start6 = start;
                if (start6 >= end) {
                    break;
                }
                start = start6 + 1;
                lastGlyph = font2.data.getGlyph(str.charAt(start6));
                if (lastGlyph != null) {
                    addGlyph(lastGlyph, x2 + (((float) lastGlyph.xoffset) * scaleX), y2 + (((float) lastGlyph.yoffset) * scaleY), ((float) lastGlyph.width) * scaleX, ((float) lastGlyph.height) * scaleY);
                    x2 += ((float) lastGlyph.xadvance) * scaleX;
                    start6 = start;
                    break;
                }
            }
            while (start2 < end) {
                int start7 = start2 + 1;
                char ch2 = str.charAt(start2);
                BitmapFont.Glyph g2 = font2.data.getGlyph(ch2);
                if (g2 != null) {
                    float x4 = x2 + (((float) lastGlyph.getKerning(ch2)) * scaleX);
                    lastGlyph = g2;
                    addGlyph(lastGlyph, x4 + (((float) g2.xoffset) * scaleX), y2 + (((float) g2.yoffset) * scaleY), ((float) g2.width) * scaleX, ((float) g2.height) * scaleY);
                    x2 = x4 + (((float) g2.xadvance) * scaleX);
                    start2 = start7;
                } else {
                    start2 = start7;
                }
            }
        }
        return x2 - startX;
    }

    private void addGlyph(BitmapFont.Glyph glyph, float x2, float y2, float width, float height) {
        float x22 = x2 + width;
        float y22 = y2 + height;
        float u = glyph.u;
        float u2 = glyph.u2;
        float v = glyph.v;
        float v2 = glyph.v2;
        float[] vertices2 = this.vertices;
        if (!this.integer) {
            int i = this.idx;
            this.idx = i + 1;
            vertices2[i] = x2;
            int i2 = this.idx;
            this.idx = i2 + 1;
            vertices2[i2] = y2;
            int i3 = this.idx;
            this.idx = i3 + 1;
            vertices2[i3] = this.color;
            int i4 = this.idx;
            this.idx = i4 + 1;
            vertices2[i4] = u;
            int i5 = this.idx;
            this.idx = i5 + 1;
            vertices2[i5] = v;
            int i6 = this.idx;
            this.idx = i6 + 1;
            vertices2[i6] = x2;
            int i7 = this.idx;
            this.idx = i7 + 1;
            vertices2[i7] = y22;
            int i8 = this.idx;
            this.idx = i8 + 1;
            vertices2[i8] = this.color;
            int i9 = this.idx;
            this.idx = i9 + 1;
            vertices2[i9] = u;
            int i10 = this.idx;
            this.idx = i10 + 1;
            vertices2[i10] = v2;
            int i11 = this.idx;
            this.idx = i11 + 1;
            vertices2[i11] = x22;
            int i12 = this.idx;
            this.idx = i12 + 1;
            vertices2[i12] = y22;
            int i13 = this.idx;
            this.idx = i13 + 1;
            vertices2[i13] = this.color;
            int i14 = this.idx;
            this.idx = i14 + 1;
            vertices2[i14] = u2;
            int i15 = this.idx;
            this.idx = i15 + 1;
            vertices2[i15] = v2;
            int i16 = this.idx;
            this.idx = i16 + 1;
            vertices2[i16] = x22;
            int i17 = this.idx;
            this.idx = i17 + 1;
            vertices2[i17] = y2;
            int i18 = this.idx;
            this.idx = i18 + 1;
            vertices2[i18] = this.color;
            int i19 = this.idx;
            this.idx = i19 + 1;
            vertices2[i19] = u2;
            int i20 = this.idx;
            this.idx = i20 + 1;
            vertices2[i20] = v;
            return;
        }
        int i21 = this.idx;
        this.idx = i21 + 1;
        vertices2[i21] = (float) ((int) x2);
        int i22 = this.idx;
        this.idx = i22 + 1;
        vertices2[i22] = (float) ((int) y2);
        int i23 = this.idx;
        this.idx = i23 + 1;
        vertices2[i23] = this.color;
        int i24 = this.idx;
        this.idx = i24 + 1;
        vertices2[i24] = u;
        int i25 = this.idx;
        this.idx = i25 + 1;
        vertices2[i25] = v;
        int i26 = this.idx;
        this.idx = i26 + 1;
        vertices2[i26] = (float) ((int) x2);
        int i27 = this.idx;
        this.idx = i27 + 1;
        vertices2[i27] = (float) ((int) y22);
        int i28 = this.idx;
        this.idx = i28 + 1;
        vertices2[i28] = this.color;
        int i29 = this.idx;
        this.idx = i29 + 1;
        vertices2[i29] = u;
        int i30 = this.idx;
        this.idx = i30 + 1;
        vertices2[i30] = v2;
        int i31 = this.idx;
        this.idx = i31 + 1;
        vertices2[i31] = (float) ((int) x22);
        int i32 = this.idx;
        this.idx = i32 + 1;
        vertices2[i32] = (float) ((int) y22);
        int i33 = this.idx;
        this.idx = i33 + 1;
        vertices2[i33] = this.color;
        int i34 = this.idx;
        this.idx = i34 + 1;
        vertices2[i34] = u2;
        int i35 = this.idx;
        this.idx = i35 + 1;
        vertices2[i35] = v2;
        int i36 = this.idx;
        this.idx = i36 + 1;
        vertices2[i36] = (float) ((int) x22);
        int i37 = this.idx;
        this.idx = i37 + 1;
        vertices2[i37] = (float) ((int) y2);
        int i38 = this.idx;
        this.idx = i38 + 1;
        vertices2[i38] = this.color;
        int i39 = this.idx;
        this.idx = i39 + 1;
        vertices2[i39] = u2;
        int i40 = this.idx;
        this.idx = i40 + 1;
        vertices2[i40] = v;
    }

    public BitmapFont.TextBounds setText(CharSequence str, float x2, float y2) {
        return setText(str, x2, y2, 0, str.length());
    }

    public BitmapFont.TextBounds setText(CharSequence str, float x2, float y2, int start, int end) {
        reset(end - start);
        float y3 = y2 + this.font.data.ascent;
        this.textBounds.width = addToCache(str, x2, y3, start, end);
        this.textBounds.height = this.font.data.capHeight;
        return this.textBounds;
    }

    public BitmapFont.TextBounds setMultiLineText(CharSequence str, float x2, float y2) {
        return setMultiLineText(str, x2, y2, 0.0f, BitmapFont.HAlignment.LEFT);
    }

    public BitmapFont.TextBounds setMultiLineText(CharSequence str, float x2, float y2, float alignmentWidth, BitmapFont.HAlignment alignment) {
        BitmapFont font2 = this.font;
        int length = str.length();
        reset(length);
        float y3 = y2 + font2.data.ascent;
        float down = font2.data.down;
        float maxWidth = 0.0f;
        int start = 0;
        int numLines = 0;
        while (start < length) {
            int lineEnd = BitmapFont.indexOf(str, 10, start);
            float xOffset = 0.0f;
            if (alignment != BitmapFont.HAlignment.LEFT) {
                xOffset = alignmentWidth - font2.getBounds(str, start, lineEnd).width;
                if (alignment == BitmapFont.HAlignment.CENTER) {
                    xOffset /= 2.0f;
                }
            }
            maxWidth = Math.max(maxWidth, addToCache(str, x2 + xOffset, y3, start, lineEnd));
            start = lineEnd + 1;
            y3 += down;
            numLines++;
        }
        this.textBounds.width = maxWidth;
        this.textBounds.height = font2.data.capHeight + (((float) (numLines - 1)) * font2.data.lineHeight);
        return this.textBounds;
    }

    public BitmapFont.TextBounds setWrappedText(CharSequence str, float x2, float y2, float wrapWidth) {
        return setWrappedText(str, x2, y2, wrapWidth, BitmapFont.HAlignment.LEFT);
    }

    public BitmapFont.TextBounds setWrappedText(CharSequence str, float x2, float y2, float wrapWidth, BitmapFont.HAlignment alignment) {
        BitmapFont font2 = this.font;
        int length = str.length();
        reset(length);
        float y3 = y2 + font2.data.ascent;
        float down = font2.data.down;
        float maxWidth = 0.0f;
        int start = 0;
        int numLines = 0;
        while (start < length) {
            int lineEnd = start + font2.computeVisibleGlyphs(str, start, BitmapFont.indexOf(str, 10, start), wrapWidth);
            if (lineEnd < length) {
                while (lineEnd > start) {
                    char ch = str.charAt(lineEnd);
                    if (ch == ' ' || ch == 10) {
                        break;
                    }
                    lineEnd--;
                }
            }
            if (lineEnd == start) {
                lineEnd++;
            }
            float xOffset = 0.0f;
            if (alignment != BitmapFont.HAlignment.LEFT) {
                xOffset = wrapWidth - font2.getBounds(str, start, lineEnd).width;
                if (alignment == BitmapFont.HAlignment.CENTER) {
                    xOffset /= 2.0f;
                }
            }
            maxWidth = Math.max(maxWidth, addToCache(str, x2 + xOffset, y3, start, lineEnd));
            start = lineEnd + 1;
            y3 += down;
            numLines++;
        }
        this.textBounds.width = maxWidth;
        this.textBounds.height = font2.data.capHeight + (((float) (numLines - 1)) * font2.data.lineHeight);
        return this.textBounds;
    }

    public BitmapFont.TextBounds getBounds() {
        return this.textBounds;
    }

    public float getX() {
        return this.x;
    }

    public float getY() {
        return this.y;
    }

    public BitmapFont getFont() {
        return this.font;
    }

    public void dispose() {
        this.font.dispose();
    }

    public void setUseIntegerPositions(boolean use) {
        this.integer = use;
    }

    public boolean usesIntegerPositions() {
        return this.integer;
    }
}
