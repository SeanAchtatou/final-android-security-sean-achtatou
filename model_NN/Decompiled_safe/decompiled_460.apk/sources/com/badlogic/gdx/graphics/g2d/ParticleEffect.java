package com.badlogic.gdx.graphics.g2d;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;
import java.io.File;

public class ParticleEffect implements Disposable {
    private final Array<ParticleEmitter> emitters;

    public ParticleEffect() {
        this.emitters = new Array<>(8);
    }

    public ParticleEffect(ParticleEffect effect) {
        this.emitters = new Array<>(true, effect.emitters.size);
        int n = effect.emitters.size;
        for (int i = 0; i < n; i++) {
            this.emitters.add(new ParticleEmitter(effect.emitters.get(i)));
        }
    }

    public void start() {
        int n = this.emitters.size;
        for (int i = 0; i < n; i++) {
            this.emitters.get(i).start();
        }
    }

    public void update(float delta) {
        int n = this.emitters.size;
        for (int i = 0; i < n; i++) {
            this.emitters.get(i).update(delta);
        }
    }

    public void draw(SpriteBatch spriteBatch) {
        int n = this.emitters.size;
        for (int i = 0; i < n; i++) {
            this.emitters.get(i).draw(spriteBatch);
        }
    }

    public void draw(SpriteBatch spriteBatch, float delta) {
        int n = this.emitters.size;
        for (int i = 0; i < n; i++) {
            this.emitters.get(i).draw(spriteBatch, delta);
        }
    }

    public void allowCompletion() {
        int n = this.emitters.size;
        for (int i = 0; i < n; i++) {
            this.emitters.get(i).allowCompletion();
        }
    }

    public boolean isComplete() {
        int n = this.emitters.size;
        for (int i = 0; i < n; i++) {
            ParticleEmitter emitter = this.emitters.get(i);
            if (emitter.isContinuous()) {
                return false;
            }
            if (!emitter.isComplete()) {
                return false;
            }
        }
        return true;
    }

    public void setDuration(int duration) {
        int n = this.emitters.size;
        for (int i = 0; i < n; i++) {
            ParticleEmitter emitter = this.emitters.get(i);
            emitter.setContinuous(false);
            emitter.duration = (float) duration;
            emitter.durationTimer = 0.0f;
        }
    }

    public void setPosition(float x, float y) {
        int n = this.emitters.size;
        for (int i = 0; i < n; i++) {
            this.emitters.get(i).setPosition(x, y);
        }
    }

    public void setFlip(boolean flipX, boolean flipY) {
        int n = this.emitters.size;
        for (int i = 0; i < n; i++) {
            this.emitters.get(i).setFlip(flipX, flipY);
        }
    }

    public Array<ParticleEmitter> getEmitters() {
        return this.emitters;
    }

    public ParticleEmitter findEmitter(String name) {
        int n = this.emitters.size;
        for (int i = 0; i < n; i++) {
            ParticleEmitter emitter = this.emitters.get(i);
            if (emitter.getName().equals(name)) {
                return emitter;
            }
        }
        return null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x0066 A[SYNTHETIC, Splitter:B:24:0x0066] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void save(java.io.File r12) {
        /*
            r11 = this;
            r6 = 0
            java.io.FileWriter r7 = new java.io.FileWriter     // Catch:{ IOException -> 0x004c }
            r7.<init>(r12)     // Catch:{ IOException -> 0x004c }
            r3 = 0
            r2 = 0
            com.badlogic.gdx.utils.Array<com.badlogic.gdx.graphics.g2d.ParticleEmitter> r8 = r11.emitters     // Catch:{ IOException -> 0x0071, all -> 0x006e }
            int r5 = r8.size     // Catch:{ IOException -> 0x0071, all -> 0x006e }
            r4 = r3
        L_0x000d:
            if (r2 < r5) goto L_0x0015
            if (r7 == 0) goto L_0x0014
            r7.close()     // Catch:{ IOException -> 0x006c }
        L_0x0014:
            return
        L_0x0015:
            com.badlogic.gdx.utils.Array<com.badlogic.gdx.graphics.g2d.ParticleEmitter> r8 = r11.emitters     // Catch:{ IOException -> 0x0071, all -> 0x006e }
            java.lang.Object r0 = r8.get(r2)     // Catch:{ IOException -> 0x0071, all -> 0x006e }
            com.badlogic.gdx.graphics.g2d.ParticleEmitter r0 = (com.badlogic.gdx.graphics.g2d.ParticleEmitter) r0     // Catch:{ IOException -> 0x0071, all -> 0x006e }
            int r3 = r4 + 1
            if (r4 <= 0) goto L_0x0026
            java.lang.String r8 = "\n\n"
            r7.write(r8)     // Catch:{ IOException -> 0x0071, all -> 0x006e }
        L_0x0026:
            r0.save(r7)     // Catch:{ IOException -> 0x0071, all -> 0x006e }
            java.lang.String r8 = "- Image Path -\n"
            r7.write(r8)     // Catch:{ IOException -> 0x0071, all -> 0x006e }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0071, all -> 0x006e }
            java.lang.String r9 = r0.getImagePath()     // Catch:{ IOException -> 0x0071, all -> 0x006e }
            java.lang.String r9 = java.lang.String.valueOf(r9)     // Catch:{ IOException -> 0x0071, all -> 0x006e }
            r8.<init>(r9)     // Catch:{ IOException -> 0x0071, all -> 0x006e }
            java.lang.String r9 = "\n"
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ IOException -> 0x0071, all -> 0x006e }
            java.lang.String r8 = r8.toString()     // Catch:{ IOException -> 0x0071, all -> 0x006e }
            r7.write(r8)     // Catch:{ IOException -> 0x0071, all -> 0x006e }
            int r2 = r2 + 1
            r4 = r3
            goto L_0x000d
        L_0x004c:
            r8 = move-exception
            r1 = r8
        L_0x004e:
            com.badlogic.gdx.utils.GdxRuntimeException r8 = new com.badlogic.gdx.utils.GdxRuntimeException     // Catch:{ all -> 0x0063 }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ all -> 0x0063 }
            java.lang.String r10 = "Error saving effect: "
            r9.<init>(r10)     // Catch:{ all -> 0x0063 }
            java.lang.StringBuilder r9 = r9.append(r12)     // Catch:{ all -> 0x0063 }
            java.lang.String r9 = r9.toString()     // Catch:{ all -> 0x0063 }
            r8.<init>(r9, r1)     // Catch:{ all -> 0x0063 }
            throw r8     // Catch:{ all -> 0x0063 }
        L_0x0063:
            r8 = move-exception
        L_0x0064:
            if (r6 == 0) goto L_0x0069
            r6.close()     // Catch:{ IOException -> 0x006a }
        L_0x0069:
            throw r8
        L_0x006a:
            r9 = move-exception
            goto L_0x0069
        L_0x006c:
            r8 = move-exception
            goto L_0x0014
        L_0x006e:
            r8 = move-exception
            r6 = r7
            goto L_0x0064
        L_0x0071:
            r8 = move-exception
            r1 = r8
            r6 = r7
            goto L_0x004e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.graphics.g2d.ParticleEffect.save(java.io.File):void");
    }

    public void load(FileHandle effectFile, FileHandle imagesDir) {
        loadEmitters(effectFile);
        loadEmitterImages(imagesDir);
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x0057 A[SYNTHETIC, Splitter:B:19:0x0057] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void loadEmitters(com.badlogic.gdx.files.FileHandle r9) {
        /*
            r8 = this;
            java.io.InputStream r2 = r9.read()
            com.badlogic.gdx.utils.Array<com.badlogic.gdx.graphics.g2d.ParticleEmitter> r5 = r8.emitters
            r5.clear()
            r3 = 0
            java.io.BufferedReader r4 = new java.io.BufferedReader     // Catch:{ IOException -> 0x003d }
            java.io.InputStreamReader r5 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x003d }
            r5.<init>(r2)     // Catch:{ IOException -> 0x003d }
            r6 = 512(0x200, float:7.175E-43)
            r4.<init>(r5, r6)     // Catch:{ IOException -> 0x003d }
        L_0x0016:
            com.badlogic.gdx.graphics.g2d.ParticleEmitter r0 = new com.badlogic.gdx.graphics.g2d.ParticleEmitter     // Catch:{ IOException -> 0x0062, all -> 0x005f }
            r0.<init>(r4)     // Catch:{ IOException -> 0x0062, all -> 0x005f }
            r4.readLine()     // Catch:{ IOException -> 0x0062, all -> 0x005f }
            java.lang.String r5 = r4.readLine()     // Catch:{ IOException -> 0x0062, all -> 0x005f }
            r0.setImagePath(r5)     // Catch:{ IOException -> 0x0062, all -> 0x005f }
            com.badlogic.gdx.utils.Array<com.badlogic.gdx.graphics.g2d.ParticleEmitter> r5 = r8.emitters     // Catch:{ IOException -> 0x0062, all -> 0x005f }
            r5.add(r0)     // Catch:{ IOException -> 0x0062, all -> 0x005f }
            java.lang.String r5 = r4.readLine()     // Catch:{ IOException -> 0x0062, all -> 0x005f }
            if (r5 != 0) goto L_0x0036
        L_0x0030:
            if (r4 == 0) goto L_0x0035
            r4.close()     // Catch:{ IOException -> 0x005d }
        L_0x0035:
            return
        L_0x0036:
            java.lang.String r5 = r4.readLine()     // Catch:{ IOException -> 0x0062, all -> 0x005f }
            if (r5 != 0) goto L_0x0016
            goto L_0x0030
        L_0x003d:
            r5 = move-exception
            r1 = r5
        L_0x003f:
            com.badlogic.gdx.utils.GdxRuntimeException r5 = new com.badlogic.gdx.utils.GdxRuntimeException     // Catch:{ all -> 0x0054 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x0054 }
            java.lang.String r7 = "Error loading effect: "
            r6.<init>(r7)     // Catch:{ all -> 0x0054 }
            java.lang.StringBuilder r6 = r6.append(r9)     // Catch:{ all -> 0x0054 }
            java.lang.String r6 = r6.toString()     // Catch:{ all -> 0x0054 }
            r5.<init>(r6, r1)     // Catch:{ all -> 0x0054 }
            throw r5     // Catch:{ all -> 0x0054 }
        L_0x0054:
            r5 = move-exception
        L_0x0055:
            if (r3 == 0) goto L_0x005a
            r3.close()     // Catch:{ IOException -> 0x005b }
        L_0x005a:
            throw r5
        L_0x005b:
            r6 = move-exception
            goto L_0x005a
        L_0x005d:
            r5 = move-exception
            goto L_0x0035
        L_0x005f:
            r5 = move-exception
            r3 = r4
            goto L_0x0055
        L_0x0062:
            r5 = move-exception
            r1 = r5
            r3 = r4
            goto L_0x003f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.graphics.g2d.ParticleEffect.loadEmitters(com.badlogic.gdx.files.FileHandle):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public void loadEmitterImages(FileHandle imagesDir) {
        int n = this.emitters.size;
        for (int i = 0; i < n; i++) {
            ParticleEmitter emitter = this.emitters.get(i);
            String imagePath = emitter.getImagePath();
            if (imagePath != null) {
                emitter.setSprite(new Sprite(loadTexture(imagesDir.child(new File(imagePath.replace('\\', '/')).getName()))));
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.Texture.<init>(com.badlogic.gdx.files.FileHandle, boolean):void
     arg types: [com.badlogic.gdx.files.FileHandle, int]
     candidates:
      com.badlogic.gdx.graphics.Texture.<init>(com.badlogic.gdx.graphics.Pixmap, boolean):void
      com.badlogic.gdx.graphics.Texture.<init>(com.badlogic.gdx.files.FileHandle, boolean):void */
    /* access modifiers changed from: protected */
    public Texture loadTexture(FileHandle file) {
        return new Texture(file, false);
    }

    public void dispose() {
        int n = this.emitters.size;
        for (int i = 0; i < n; i++) {
            this.emitters.get(i).getSprite().getTexture().dispose();
        }
    }
}
