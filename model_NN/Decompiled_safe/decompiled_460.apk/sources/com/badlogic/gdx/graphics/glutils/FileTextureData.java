package com.badlogic.gdx.graphics.glutils;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.TextureData;

public class FileTextureData implements TextureData {
    final FileHandle file;
    Pixmap.Format format;
    int height = 0;
    Pixmap pixmap;
    boolean useMipMaps;
    int width = 0;

    public FileTextureData(FileHandle file2, Pixmap preloadedPixmap, Pixmap.Format format2, boolean useMipMaps2) {
        this.file = file2;
        this.pixmap = preloadedPixmap;
        this.format = format2;
        this.useMipMaps = useMipMaps2;
        if (this.pixmap != null) {
            this.width = this.pixmap.getWidth();
            this.height = this.pixmap.getHeight();
            if (format2 == null) {
                this.format = this.pixmap.getFormat();
            }
        }
    }

    public Pixmap getPixmap() {
        if (this.pixmap != null) {
            Pixmap tmp = this.pixmap;
            this.pixmap = null;
            return tmp;
        }
        Pixmap pixmap2 = new Pixmap(this.file);
        this.width = pixmap2.getWidth();
        this.height = pixmap2.getHeight();
        if (this.format == null) {
            this.format = pixmap2.getFormat();
        }
        return pixmap2;
    }

    public boolean disposePixmap() {
        return true;
    }

    public int getWidth() {
        return this.width;
    }

    public int getHeight() {
        return this.height;
    }

    public Pixmap.Format getFormat() {
        return this.format;
    }

    public boolean useMipMaps() {
        return this.useMipMaps;
    }

    public boolean isManaged() {
        return true;
    }

    public FileHandle getFileHandle() {
        return this.file;
    }
}
