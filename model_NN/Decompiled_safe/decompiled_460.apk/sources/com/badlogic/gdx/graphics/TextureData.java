package com.badlogic.gdx.graphics;

import com.badlogic.gdx.graphics.Pixmap;

public interface TextureData {
    boolean disposePixmap();

    Pixmap.Format getFormat();

    int getHeight();

    Pixmap getPixmap();

    int getWidth();

    boolean isManaged();

    boolean useMipMaps();
}
