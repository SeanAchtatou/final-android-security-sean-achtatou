package com.badlogic.gdx.graphics.glutils;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.TextureData;

public class PixmapTextureData implements TextureData {
    final boolean disposePixmap;
    final Pixmap.Format format;
    final Pixmap pixmap;
    final boolean useMipMaps;

    public PixmapTextureData(Pixmap pixmap2, Pixmap.Format format2, boolean useMipMaps2, boolean disposePixmap2) {
        Pixmap.Format format3;
        this.pixmap = pixmap2;
        if (format2 == null) {
            format3 = pixmap2.getFormat();
        } else {
            format3 = format2;
        }
        this.format = format3;
        this.useMipMaps = useMipMaps2;
        this.disposePixmap = disposePixmap2;
    }

    public boolean disposePixmap() {
        return this.disposePixmap;
    }

    public Pixmap getPixmap() {
        return this.pixmap;
    }

    public int getWidth() {
        return this.pixmap.getWidth();
    }

    public int getHeight() {
        return this.pixmap.getHeight();
    }

    public Pixmap.Format getFormat() {
        return this.format;
    }

    public boolean useMipMaps() {
        return this.useMipMaps;
    }

    public boolean isManaged() {
        return false;
    }
}
