package com.badlogic.gdx;

import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.GL11;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.GLCommon;
import com.badlogic.gdx.graphics.GLU;
import com.badlogic.gdx.graphics.Pixmap;

public interface Graphics {

    public enum GraphicsType {
        AndroidGL,
        JoglGL,
        LWJGL,
        Angle,
        WebGL
    }

    BufferFormat getBufferFormat();

    float getDeltaTime();

    float getDensity();

    DisplayMode getDesktopDisplayMode();

    DisplayMode[] getDisplayModes();

    int getFramesPerSecond();

    GL10 getGL10();

    GL11 getGL11();

    GL20 getGL20();

    GLCommon getGLCommon();

    GLU getGLU();

    int getHeight();

    float getPpcX();

    float getPpcY();

    float getPpiX();

    float getPpiY();

    GraphicsType getType();

    int getWidth();

    boolean isGL11Available();

    boolean isGL20Available();

    boolean setDisplayMode(int i, int i2, boolean z);

    boolean setDisplayMode(DisplayMode displayMode);

    void setIcon(Pixmap pixmap);

    void setTitle(String str);

    void setVSync(boolean z);

    boolean supportsDisplayModeChange();

    boolean supportsExtension(String str);

    public static class DisplayMode {
        public final int bitsPerPixel;
        public final int height;
        public final int refreshRate;
        public final int width;

        protected DisplayMode(int width2, int height2, int refreshRate2, int bitsPerPixel2) {
            this.width = width2;
            this.height = height2;
            this.refreshRate = refreshRate2;
            this.bitsPerPixel = bitsPerPixel2;
        }

        public String toString() {
            return String.valueOf(this.width) + "x" + this.height + ", bpp: " + this.bitsPerPixel + ", hz: " + this.refreshRate;
        }
    }

    public static class BufferFormat {
        public final int a;
        public final int b;
        public final boolean coverageSampling;
        public final int depth;
        public final int g;
        public final int r;
        public final int samples;
        public final int stencil;

        public BufferFormat(int r2, int g2, int b2, int a2, int depth2, int stencil2, int samples2, boolean coverageSampling2) {
            this.r = r2;
            this.g = g2;
            this.b = b2;
            this.a = a2;
            this.depth = depth2;
            this.stencil = stencil2;
            this.samples = samples2;
            this.coverageSampling = coverageSampling2;
        }

        public String toString() {
            return "r: " + this.r + ", g: " + this.g + ", b: " + this.b + ", a: " + this.a + ", depth: " + this.depth + ", stencil: " + this.stencil + ", num samples: " + this.samples + ", coverage sampling: " + this.coverageSampling;
        }
    }
}
