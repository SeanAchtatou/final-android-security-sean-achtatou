package com.badlogic.gdx.math;

import java.io.Serializable;

public class Quaternion implements Serializable {
    private static final float NORMALIZATION_TOLERANCE = 1.0E-5f;
    private static final long serialVersionUID = -7661875440774897168L;
    private static Quaternion tmp1 = new Quaternion(0.0f, 0.0f, 0.0f, 0.0f);
    private static Quaternion tmp2 = new Quaternion(0.0f, 0.0f, 0.0f, 0.0f);
    public float w;
    public float x;
    public float y;
    public float z;

    public Quaternion(float x2, float y2, float z2, float w2) {
        set(x2, y2, z2, w2);
    }

    Quaternion() {
    }

    public Quaternion(Quaternion quaternion) {
        set(quaternion);
    }

    public Quaternion(Vector3 axis, float angle) {
        set(axis, angle);
    }

    public Quaternion set(float x2, float y2, float z2, float w2) {
        this.x = x2;
        this.y = y2;
        this.z = z2;
        this.w = w2;
        return this;
    }

    public Quaternion set(Quaternion quaternion) {
        return set(quaternion.x, quaternion.y, quaternion.z, quaternion.w);
    }

    public Quaternion set(Vector3 axis, float angle) {
        float l_ang = (float) Math.toRadians((double) angle);
        float l_sin = (float) Math.sin((double) (l_ang / 2.0f));
        return set(axis.x * l_sin, axis.y * l_sin, axis.z * l_sin, (float) Math.cos((double) (l_ang / 2.0f))).nor();
    }

    public Quaternion cpy() {
        return new Quaternion(this);
    }

    public float len() {
        return (float) Math.sqrt((double) ((this.x * this.x) + (this.y * this.y) + (this.z * this.z) + (this.w * this.w)));
    }

    public String toString() {
        return "[" + this.x + "|" + this.y + "|" + this.z + "|" + this.w + "]";
    }

    public Quaternion setEulerAngles(float yaw, float pitch, float roll) {
        float yaw2 = (float) Math.toRadians((double) yaw);
        float pitch2 = (float) Math.toRadians((double) pitch);
        float num9 = ((float) Math.toRadians((double) roll)) * 0.5f;
        float num6 = (float) Math.sin((double) num9);
        float num5 = (float) Math.cos((double) num9);
        float num8 = pitch2 * 0.5f;
        float num4 = (float) Math.sin((double) num8);
        float num3 = (float) Math.cos((double) num8);
        float num7 = yaw2 * 0.5f;
        float num2 = (float) Math.sin((double) num7);
        float num = (float) Math.cos((double) num7);
        this.x = (num * num4 * num5) + (num2 * num3 * num6);
        this.y = ((num2 * num3) * num5) - ((num * num4) * num6);
        this.z = ((num * num3) * num6) - ((num2 * num4) * num5);
        this.w = (num * num3 * num5) + (num2 * num4 * num6);
        return this;
    }

    public float len2() {
        return (this.x * this.x) + (this.y * this.y) + (this.z * this.z) + (this.w * this.w);
    }

    public Quaternion nor() {
        float len = len2();
        if (len != 0.0f && Math.abs(len - 1.0f) > NORMALIZATION_TOLERANCE) {
            float len2 = (float) Math.sqrt((double) len);
            this.w /= len2;
            this.x /= len2;
            this.y /= len2;
            this.z /= len2;
        }
        return this;
    }

    public Quaternion conjugate() {
        this.x = -this.x;
        this.y = -this.y;
        this.z = -this.z;
        return this;
    }

    public void transform(Vector3 v) {
        tmp2.set(this);
        tmp2.conjugate();
        tmp2.mulLeft(tmp1.set(v.x, v.y, v.z, 0.0f)).mulLeft(this);
        v.x = tmp2.x;
        v.y = tmp2.y;
        v.z = tmp2.z;
    }

    public Quaternion mul(Quaternion q) {
        float newX = (((this.w * q.x) + (this.x * q.w)) + (this.y * q.z)) - (this.z * q.y);
        float newY = (((this.w * q.y) + (this.y * q.w)) + (this.z * q.x)) - (this.x * q.z);
        float newZ = (((this.w * q.z) + (this.z * q.w)) + (this.x * q.y)) - (this.y * q.x);
        this.x = newX;
        this.y = newY;
        this.z = newZ;
        this.w = (((this.w * q.w) - (this.x * q.x)) - (this.y * q.y)) - (this.z * q.z);
        return this;
    }

    public Quaternion mulLeft(Quaternion q) {
        float newX = (((q.w * this.x) + (q.x * this.w)) + (q.y * this.z)) - (q.z * this.y);
        float newY = (((q.w * this.y) + (q.y * this.w)) + (q.z * this.x)) - (q.x * this.z);
        float newZ = (((q.w * this.z) + (q.z * this.w)) + (q.x * this.y)) - (q.y * this.x);
        this.x = newX;
        this.y = newY;
        this.z = newZ;
        this.w = (((q.w * this.w) - (q.x * this.x)) - (q.y * this.y)) - (q.z * this.z);
        return this;
    }

    public void toMatrix(float[] matrix) {
        float xx = this.x * this.x;
        float xy = this.x * this.y;
        float xz = this.x * this.z;
        float xw = this.x * this.w;
        float yy = this.y * this.y;
        float yz = this.y * this.z;
        float yw = this.y * this.w;
        float zz = this.z * this.z;
        float zw = this.z * this.w;
        matrix[0] = 1.0f - ((yy + zz) * 2.0f);
        matrix[4] = (xy - zw) * 2.0f;
        matrix[8] = (xz + yw) * 2.0f;
        matrix[12] = 0.0f;
        matrix[1] = (xy + zw) * 2.0f;
        matrix[5] = 1.0f - ((xx + zz) * 2.0f);
        matrix[9] = (yz - xw) * 2.0f;
        matrix[13] = 0.0f;
        matrix[2] = (xz - yw) * 2.0f;
        matrix[6] = (yz + xw) * 2.0f;
        matrix[10] = 1.0f - ((xx + yy) * 2.0f);
        matrix[14] = 0.0f;
        matrix[3] = 0.0f;
        matrix[7] = 0.0f;
        matrix[11] = 0.0f;
        matrix[15] = 1.0f;
    }

    public static Quaternion idt() {
        return new Quaternion(0.0f, 0.0f, 0.0f, 1.0f);
    }

    public Quaternion setFromAxis(Vector3 axis, float angle) {
        return setFromAxis(axis.x, axis.y, axis.z, angle);
    }

    public Quaternion setFromAxis(float x2, float y2, float z2, float angle) {
        float l_ang = angle * 0.017453292f;
        float l_sin = MathUtils.sin(l_ang / 2.0f);
        return set(x2 * l_sin, y2 * l_sin, z2 * l_sin, MathUtils.cos(l_ang / 2.0f)).nor();
    }

    public Quaternion setFromMatrix(Matrix4 matrix) {
        return setFromAxes(matrix.val[0], matrix.val[4], matrix.val[8], matrix.val[1], matrix.val[5], matrix.val[9], matrix.val[2], matrix.val[6], matrix.val[10]);
    }

    public Quaternion setFromAxes(float xx, float xy, float xz, float yx, float yy, float yz, float zx, float zy, float zz) {
        double z2;
        double x2;
        double y2;
        double w2;
        float m00 = xx;
        float m01 = yx;
        float m02 = zx;
        float m10 = xy;
        float m11 = yy;
        float m12 = zy;
        float m20 = xz;
        float m21 = yz;
        float m22 = zz;
        float t = m00 + m11 + m22;
        if (t >= 0.0f) {
            double s = Math.sqrt((double) (1.0f + t));
            w2 = 0.5d * s;
            double s2 = 0.5d / s;
            x2 = ((double) (m21 - m12)) * s2;
            y2 = ((double) (m02 - m20)) * s2;
            z2 = ((double) (m10 - m01)) * s2;
        } else if (m00 > m11 && m00 > m22) {
            double s3 = Math.sqrt(((1.0d + ((double) m00)) - ((double) m11)) - ((double) m22));
            x2 = s3 * 0.5d;
            double s4 = 0.5d / s3;
            y2 = ((double) (m10 + m01)) * s4;
            z2 = ((double) (m02 + m20)) * s4;
            w2 = ((double) (m21 - m12)) * s4;
        } else if (m11 > m22) {
            double s5 = Math.sqrt(((1.0d + ((double) m11)) - ((double) m00)) - ((double) m22));
            y2 = s5 * 0.5d;
            double s6 = 0.5d / s5;
            x2 = ((double) (m10 + m01)) * s6;
            z2 = ((double) (m21 + m12)) * s6;
            w2 = ((double) (m02 - m20)) * s6;
        } else {
            double s7 = Math.sqrt(((1.0d + ((double) m22)) - ((double) m00)) - ((double) m11));
            z2 = s7 * 0.5d;
            double s8 = 0.5d / s7;
            x2 = ((double) (m02 + m20)) * s8;
            y2 = ((double) (m21 + m12)) * s8;
            w2 = ((double) (m10 - m01)) * s8;
        }
        return set((float) x2, (float) y2, (float) z2, (float) w2);
    }

    public Quaternion slerp(Quaternion end, float alpha) {
        if (!equals(end)) {
            float result = dot(end);
            if (((double) result) < 0.0d) {
                end.mul(-1.0f);
                result = -result;
            }
            float scale0 = 1.0f - alpha;
            float scale1 = alpha;
            if (((double) (1.0f - result)) > 0.1d) {
                double theta = Math.acos((double) result);
                double invSinTheta = 1.0d / Math.sin(theta);
                scale0 = (float) (Math.sin(((double) (1.0f - alpha)) * theta) * invSinTheta);
                scale1 = (float) (Math.sin(((double) alpha) * theta) * invSinTheta);
            }
            set((this.x * scale0) + (end.x * scale1), (this.y * scale0) + (end.y * scale1), (this.z * scale0) + (end.z * scale1), (this.w * scale0) + (end.w * scale1));
        }
        return this;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Quaternion)) {
            return false;
        }
        Quaternion comp = (Quaternion) o;
        return this.x == comp.x && this.y == comp.y && this.z == comp.z && this.w == comp.w;
    }

    public float dot(Quaternion other) {
        return (this.x * other.x) + (this.y * other.y) + (this.z * other.z) + (this.w * other.w);
    }

    public Quaternion mul(float scalar) {
        this.x *= scalar;
        this.y *= scalar;
        this.z *= scalar;
        this.w *= scalar;
        return this;
    }
}
