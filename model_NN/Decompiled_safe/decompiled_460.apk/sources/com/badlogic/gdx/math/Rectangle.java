package com.badlogic.gdx.math;

import java.io.Serializable;

public class Rectangle implements Serializable {
    private static final long serialVersionUID = 5733252015138115702L;
    public float height;
    public float width;
    public float x;
    public float y;

    public Rectangle() {
    }

    public Rectangle(float x2, float y2, float width2, float height2) {
        this.x = x2;
        this.y = y2;
        this.width = width2;
        this.height = height2;
    }

    public Rectangle(Rectangle rect) {
        this.x = rect.x;
        this.y = rect.y;
        this.width = rect.width;
        this.height = rect.height;
    }

    public float getX() {
        return this.x;
    }

    public void setX(float x2) {
        this.x = x2;
    }

    public float getY() {
        return this.y;
    }

    public void setY(float y2) {
        this.y = y2;
    }

    public float getWidth() {
        return this.width;
    }

    public void setWidth(float width2) {
        this.width = width2;
    }

    public float getHeight() {
        return this.height;
    }

    public void setHeight(float height2) {
        this.height = height2;
    }

    public boolean contains(Rectangle rectangle) {
        float xmin = rectangle.x;
        float xmax = xmin + rectangle.width;
        float ymin = rectangle.y;
        float ymax = ymin + rectangle.height;
        return ((xmin > this.x && xmin < this.x + this.width) || (xmax > this.x && xmax < this.x + this.width)) && ((ymin > this.y && ymin < this.y + this.height) || (ymax > this.y && ymax < this.y + this.height));
    }

    public boolean overlaps(Rectangle rectangle) {
        return this.x <= rectangle.x + rectangle.width && this.x + this.width >= rectangle.x && this.y <= rectangle.y + rectangle.height && this.y + this.height >= rectangle.y;
    }

    public void set(float x2, float y2, float width2, float height2) {
        this.x = x2;
        this.y = y2;
        this.width = width2;
        this.height = height2;
    }

    public boolean contains(float x2, float y2) {
        return this.x < x2 && this.x + this.width > x2 && this.y < y2 && this.y + this.height > y2;
    }

    public void set(Rectangle rect) {
        this.x = rect.x;
        this.y = rect.y;
        this.width = rect.width;
        this.height = rect.height;
    }

    public String toString() {
        return String.valueOf(this.x) + "," + this.y + "," + this.width + "," + this.height;
    }
}
