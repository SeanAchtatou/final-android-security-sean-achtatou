package net.cross.micplayer;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.HashMap;
import net.cross.micplayer.task.CategoryTask;

public class CategoryActivity extends Activity implements AdapterView.OnItemClickListener, SimpleCursorAdapter.ViewBinder, CategoryTask.Listener {
    private final int DIALOG_PROGRESS = 0;
    private StringBuilder localStringBuilder;
    ArrayList<HashMap<String, Object>> mAlbumsList;
    ArrayList<HashMap<String, Object>> mArtistsList;
    int mCategoryCode;
    SimpleCursorAdapter mCategoryListAdapter;
    SimpleAdapter mCategoryListAlbumAdapter;
    SimpleAdapter mCategoryListArtistAdapter;
    SimpleAdapter mCategoryListFolderAdapter;
    ListView mCategoryListView;
    private CategoryTask mCategoryTask = null;
    ArrayList<HashMap<String, Object>> mFoldersList;
    String mPressedPlaylistName;

    public void onCreate(Bundle paramBundle) {
        super.onCreate(paramBundle);
        setContentView((int) R.layout.category);
        AdsView.createAdWhirl(this);
        setTitle(getString(R.string.title_library));
        this.mCategoryListView = (ListView) findViewById(R.id.lv_category);
        this.mCategoryListView.setOnItemClickListener(this);
        if (!CategoryTask.bReady) {
            showDialog(0);
            this.mCategoryTask = new CategoryTask(this);
            this.mCategoryTask.execute(this);
            return;
        }
        this.mArtistsList = CategoryTask.mArtistsList;
        this.mAlbumsList = CategoryTask.mAlbumsList;
        this.mFoldersList = CategoryTask.mFoldersList;
        initList();
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        Log.e("CategoryActivity", "onSaveInstanceState");
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle savedInstanceState) {
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void initList() {
        Cursor localCursor;
        Bundle localBundle = getIntent().getExtras();
        if (localBundle != null) {
            this.mCategoryCode = localBundle.getInt("bundle_category_code", 40);
            Log.e("CategoryActivity", "category is: " + this.mCategoryCode);
            if (this.mCategoryCode == 41) {
                int[] arrayOfInt = {R.id.row_title, R.id.children_number};
                this.localStringBuilder = new StringBuilder(" -> Artists");
                this.mCategoryListArtistAdapter = new SimpleAdapter(this, this.mArtistsList, R.layout.category_row, new String[]{"artist", "count"}, arrayOfInt);
            }
            if (this.mCategoryCode == 42) {
                int[] arrayOfInt2 = {R.id.row_title, R.id.children_number};
                this.localStringBuilder = new StringBuilder(" -> Albums");
                this.mCategoryListAlbumAdapter = new SimpleAdapter(this, this.mAlbumsList, R.layout.category_row, new String[]{"album", "count"}, arrayOfInt2);
            }
            if (this.mCategoryCode == 43) {
                StringBuilder where = new StringBuilder();
                where.append("name != ''");
                localCursor = managedQuery(MediaStore.Audio.Genres.EXTERNAL_CONTENT_URI, new String[]{"_id", "name"}, where.toString(), null, "name");
                int[] arrayOfInt3 = {R.id.row_title, R.id.children_number};
                this.localStringBuilder = new StringBuilder(" -> Genres");
                this.mCategoryListAdapter = new SimpleCursorAdapter(this, R.layout.category_row, localCursor, new String[]{"name", "_id"}, arrayOfInt3);
                this.mCategoryListAdapter.setViewBinder(this);
            } else {
                localCursor = null;
            }
            if (this.mCategoryCode == 44) {
                int[] arrayOfInt4 = {R.id.row_title, R.id.children_number};
                this.localStringBuilder = new StringBuilder(" -> Folders");
                this.mCategoryListFolderAdapter = new SimpleAdapter(this, this.mFoldersList, R.layout.category_row, new String[]{"directory", "count"}, arrayOfInt4);
            }
            if (this.mCategoryCode == 45) {
                StringBuilder where2 = new StringBuilder();
                where2.append("name != ''");
                localCursor = managedQuery(MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI, new String[]{"_id", "name"}, where2.toString(), null, "name");
                int[] arrayOfInt5 = {R.id.row_title, R.id.children_number};
                this.localStringBuilder = new StringBuilder(" -> Playlists");
                this.mCategoryListAdapter = new SimpleCursorAdapter(this, R.layout.category_row, localCursor, new String[]{"name", "_id"}, arrayOfInt5);
                this.mCategoryListAdapter.setViewBinder(this);
            }
            this.localStringBuilder.insert(0, getString(R.string.title_library));
            setTitle(this.localStringBuilder.toString());
            if (localCursor != null) {
                startManagingCursor(localCursor);
            }
            if (this.mCategoryCode == 41) {
                this.mCategoryListView.setAdapter((ListAdapter) this.mCategoryListArtistAdapter);
            } else if (this.mCategoryCode == 42) {
                this.mCategoryListView.setAdapter((ListAdapter) this.mCategoryListAlbumAdapter);
            } else if (this.mCategoryCode == 44) {
                this.mCategoryListView.setAdapter((ListAdapter) this.mCategoryListFolderAdapter);
            } else {
                this.mCategoryListView.setAdapter((ListAdapter) this.mCategoryListAdapter);
            }
        }
    }

    public void onItemClick(AdapterView<?> adapterView, View paramView, int paramInt, long paramLong) {
        int id;
        String str;
        Intent localIntent = new Intent(this, SongsActivity.class);
        if (this.mCategoryCode == 41) {
            id = 0;
            str = this.mArtistsList.get(paramInt).get("artist").toString();
        } else if (this.mCategoryCode == 42) {
            id = 0;
            str = this.mAlbumsList.get(paramInt).get("album").toString();
        } else if (this.mCategoryCode == 44) {
            id = 0;
            str = this.mFoldersList.get(paramInt).get("directory").toString();
        } else {
            id = ((Cursor) this.mCategoryListAdapter.getItem(paramInt)).getInt(0);
            str = ((Cursor) this.mCategoryListAdapter.getItem(paramInt)).getString(1);
        }
        localIntent.putExtra("bundle_category_code", this.mCategoryCode);
        localIntent.putExtra("bundle_row_id", id);
        localIntent.putExtra("bundle_row_title", str);
        startActivity(localIntent);
    }

    public boolean setViewValue(View view, Cursor cursor, int columnIndex) {
        int columnID;
        int columnID2;
        TextView textView = (TextView) view;
        String[] cursorCols = {"_id", "artist_id", "artist"};
        if (this.mCategoryCode == 43 && (columnID2 = cursor.getColumnIndex("_id")) == columnIndex) {
            Cursor cur = getContentResolver().query(MediaStore.Audio.Genres.Members.getContentUri("external", (long) Integer.valueOf(cursor.getInt(columnID2)).intValue()), cursorCols, "duration>60000", null, null);
            int count = 0;
            if (cur != null) {
                count = cur.getCount();
            }
            textView.setText(String.valueOf(count));
            cur.close();
            cur.deactivate();
            return true;
        } else if (this.mCategoryCode != 45 || (columnID = cursor.getColumnIndex("_id")) != columnIndex) {
            return false;
        } else {
            Cursor cur2 = getContentResolver().query(MediaStore.Audio.Playlists.Members.getContentUri("external", (long) Integer.valueOf(cursor.getInt(columnID)).intValue()), cursorCols, "duration>60000", null, null);
            int count2 = 0;
            if (cur2 != null) {
                count2 = cur2.getCount();
            }
            textView.setText(String.valueOf(count2));
            cur2.close();
            cur2.deactivate();
            return true;
        }
    }

    public void CT_OnBegin() {
    }

    public void CT_OnCategoryReady() {
    }

    public void CT_OnEnd(boolean bError) {
        dismissDialog(0);
        this.mArtistsList = CategoryTask.mArtistsList;
        this.mAlbumsList = CategoryTask.mAlbumsList;
        this.mFoldersList = CategoryTask.mFoldersList;
        initList();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 0:
                return ProgressDialog.show(this, "Category", "Reading data...", true);
            default:
                return null;
        }
    }
}
