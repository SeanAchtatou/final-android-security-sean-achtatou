package net.cross.micplayer.data;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.CursorWrapper;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.util.Log;
import com.qwapi.adclient.android.utils.Utils;
import net.cross.micplayer.service.DownloadService;
import net.cross.micplayer.utils.Constants;

public class DownloadQueueProvider extends ContentProvider {
    private static final String DB_NAME = "vdq.db";
    private static final String DB_TABLE = "mdq";
    private static final String DB_URL = "net.cross.micplayer.mdq";
    private static final int DB_VERSION = 1;
    private static final int DB_VERSION_NOP_UPGRADE_FROM = 1;
    private static final int DB_VERSION_NOP_UPGRADE_TO = 1;
    private static final int TABLE = 1;
    private static final int TABLE_ID = 2;
    private static final String TABLE_LIST_TYPE = "nd.android.cursor.dir/vnd.net.cross.micplayer.mdq";
    private static final String TABLE_TYPE = "vnd.android.cursor.item/vnd.net.cross.micplayer.mdq";
    private static final UriMatcher sURIMatcher = new UriMatcher(-1);
    private SQLiteOpenHelper mOpenHelper = null;

    static {
        sURIMatcher.addURI(DB_URL, DB_TABLE, 1);
        sURIMatcher.addURI(DB_URL, "mdq/#", 2);
    }

    private final class DatabaseHelper extends SQLiteOpenHelper {
        public DatabaseHelper() {
            super(DownloadQueueProvider.this.getContext(), DownloadQueueProvider.DB_NAME, (SQLiteDatabase.CursorFactory) null, 1);
        }

        public void onCreate(SQLiteDatabase db) {
            DownloadQueueProvider.this.createTable(db);
        }

        public void onUpgrade(SQLiteDatabase db, int oldV, int newV) {
            if (oldV == 1) {
                if (newV == 1) {
                    return;
                }
            }
            DownloadQueueProvider.this.dropTable(db);
            DownloadQueueProvider.this.createTable(db);
        }
    }

    public boolean onCreate() {
        try {
            this.mOpenHelper = new DatabaseHelper();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public String getType(Uri uri) {
        switch (sURIMatcher.match(uri)) {
            case 1:
                return TABLE_LIST_TYPE;
            case 2:
                return TABLE_TYPE;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
    }

    /* access modifiers changed from: private */
    public void createTable(SQLiteDatabase db) {
        try {
            db.execSQL("CREATE TABLE mdq(_id INTEGER PRIMARY KEY AUTOINCREMENT,uri TEXT, method INTEGER, hint TEXT, _data TEXT, mimetype TEXT, visibility INTEGER, control INTEGER, status INTEGER, numfailed INTEGER, lastmod BIGINT, total_bytes INTEGER, current_bytes INTEGER, etag TEXT, title TEXT, album TEXT, artist TEXT, lyric TEXT, scaned BOOLEAN);");
        } catch (SQLException e) {
            SQLException ex = e;
            Log.e(Constants.TAG, "couldn't create table in downloads database");
            throw ex;
        }
    }

    /* access modifiers changed from: private */
    public void dropTable(SQLiteDatabase db) {
        try {
            db.execSQL("DROP TABLE IF EXISTS mdq");
        } catch (SQLException e) {
            SQLException ex = e;
            Log.e(Constants.TAG, "couldn't drop table in downloads database");
            throw ex;
        }
    }

    public Uri insert(Uri uri, ContentValues values) {
        try {
            SQLiteDatabase db = this.mOpenHelper.getWritableDatabase();
            if (sURIMatcher.match(uri) != 1) {
                Log.d(Constants.TAG, "calling insert on an unknown/invalid URI: " + uri);
                throw new IllegalArgumentException("Unknown/Invalid URI " + uri);
            }
            Context context = getContext();
            context.startService(new Intent(context, DownloadService.class));
            Uri ret = null;
            long rowID = db.insert(DB_TABLE, null, values);
            if (rowID != -1) {
                context.startService(new Intent(context, DownloadService.class));
                ret = Uri.parse(DownloadQueue.CONTENT_URI + "/" + rowID);
                context.getContentResolver().notifyChange(uri, null);
            } else {
                Log.d(Constants.TAG, "couldn't insert into CommentListCache database");
            }
            return ret;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        try {
            SQLiteDatabase db = this.mOpenHelper.getReadableDatabase();
            SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
            switch (sURIMatcher.match(uri)) {
                case 1:
                    qb.setTables(DB_TABLE);
                    break;
                case 2:
                    qb.setTables(DB_TABLE);
                    qb.appendWhere("_id=");
                    qb.appendWhere(uri.getPathSegments().get(1));
                    break;
                default:
                    Log.v(Constants.TAG, "querying unknown URI: " + uri);
                    throw new IllegalArgumentException("Unknown URI: " + uri);
            }
            Cursor ret = qb.query(db, projection, selection, selectionArgs, null, null, sortOrder);
            if (ret != null) {
                ret = new CursorWrapper(ret);
            }
            if (ret != null) {
                ret.setNotificationUri(getContext().getContentResolver(), uri);
                Log.v(Constants.TAG, "created cursor " + ret);
            } else {
                Log.v(Constants.TAG, "query failed in downloads database");
            }
            return ret;
        } catch (Exception e) {
            Exception exc = e;
            return null;
        }
    }

    public int update(Uri uri, ContentValues values, String where, String[] whereArgs) {
        String myWhere;
        int count;
        try {
            SQLiteDatabase db = this.mOpenHelper.getWritableDatabase();
            boolean startService = values.getAsInteger(DownloadQueue.COL_CONTROL) != null;
            int match = sURIMatcher.match(uri);
            switch (match) {
                case 1:
                case 2:
                    if (where == null) {
                        myWhere = Utils.EMPTY_STRING;
                    } else if (match == 1) {
                        myWhere = "( " + where + " )";
                    } else {
                        myWhere = "( " + where + " ) AND ";
                    }
                    if (match == 2) {
                        myWhere = String.valueOf(myWhere) + " ( _id = " + Long.parseLong(uri.getPathSegments().get(1)) + " ) ";
                    }
                    if (values.size() > 0) {
                        count = db.update(DB_TABLE, values, myWhere, whereArgs);
                    } else {
                        count = 0;
                    }
                    getContext().getContentResolver().notifyChange(uri, null);
                    if (startService) {
                        Context context = getContext();
                        context.startService(new Intent(context, DownloadService.class));
                    }
                    return count;
                default:
                    Log.d(Constants.TAG, "updating unknown/invalid URI: " + uri);
                    throw new UnsupportedOperationException("Cannot update URI: " + uri);
            }
        } catch (Exception e) {
            Exception exc = e;
            return 0;
        }
    }

    public int delete(Uri uri, String where, String[] whereArgs) {
        String myWhere;
        try {
            SQLiteDatabase db = this.mOpenHelper.getWritableDatabase();
            int match = sURIMatcher.match(uri);
            switch (match) {
                case 1:
                case 2:
                    if (where == null) {
                        myWhere = Utils.EMPTY_STRING;
                    } else if (match == 1) {
                        myWhere = "( " + where + " )";
                    } else {
                        myWhere = "( " + where + " ) AND ";
                    }
                    if (match == 2) {
                        myWhere = String.valueOf(myWhere) + " ( _id = " + Long.parseLong(uri.getPathSegments().get(1)) + " ) ";
                    }
                    int count = db.delete(DB_TABLE, myWhere, whereArgs);
                    getContext().getContentResolver().notifyChange(uri, null);
                    return count;
                default:
                    Log.d(Constants.TAG, "deleting unknown/invalid URI: " + uri);
                    throw new UnsupportedOperationException("Cannot delete URI: " + uri);
            }
        } catch (Exception e) {
            Exception exc = e;
            return 0;
        }
    }
}
