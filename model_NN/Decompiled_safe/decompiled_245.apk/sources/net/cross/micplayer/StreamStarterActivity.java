package net.cross.micplayer;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.widget.TextView;
import android.widget.Toast;
import net.cross.micplayer.service.PlaybackService;
import net.cross.micplayer.service.PlaybackServiceHelper;

public class StreamStarterActivity extends Activity {
    private static final String P_ALBUM = "album";
    private static final String P_ARTIST = "artist";
    private static final String P_HOMEBUTTON = "homebutton";
    private static final String P_LYRICURL = "lyricurl";
    private static final String P_NOTIFICATION = "notification";
    private static final String P_POSITION = "position";
    private static final String P_SONG = "song";
    private static final String P_SONGLIST = "songlist";
    private static final String P_SONGURL = "url";
    /* access modifiers changed from: private */
    public BroadcastReceiver mStatusListener = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            try {
                if (intent.getAction().equals(PlaybackService.PLAYBACK_COMPLETE)) {
                    Toast.makeText(StreamStarterActivity.this, StreamStarterActivity.this.getString(R.string.fail_to_start_stream), 0).show();
                    StreamStarterActivity.this.finish();
                    return;
                }
                Bundle bundle = StreamStarterActivity.this.getIntent().getExtras();
                long[] songList = bundle.getLongArray(StreamStarterActivity.P_SONGLIST);
                if (songList.length > 0) {
                    PlaybackServiceHelper.playAll(StreamStarterActivity.this.getApplicationContext(), songList, bundle.getInt(StreamStarterActivity.P_POSITION), false);
                } else {
                    PlaybackServiceHelper.mService.play();
                }
                PlayerActivity.startActivity(StreamStarterActivity.this, bundle.getString(StreamStarterActivity.P_SONGURL), bundle.getString("song"), bundle.getString("artist"), bundle.getString("album"), bundle.getString(StreamStarterActivity.P_LYRICURL));
                StreamStarterActivity.this.finish();
            } catch (Exception e) {
            }
        }
    };

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public static void startActivity(Context ctx, String url, String song, String artist, String album, String lyricURL) {
        Intent intent = new Intent(ctx, StreamStarterActivity.class);
        intent.putExtra(P_SONGURL, url);
        intent.putExtra("song", song);
        intent.putExtra("artist", artist);
        intent.putExtra("album", album);
        intent.putExtra(P_LYRICURL, lyricURL);
        intent.putExtra(P_NOTIFICATION, false);
        intent.putExtra(P_HOMEBUTTON, false);
        intent.putExtra(P_SONGLIST, new long[0]);
        intent.putExtra(P_POSITION, -1);
        ctx.startActivity(intent);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public static void startActivity(Context ctx, String url, String song, String artist, String album, String lyricURL, long[] songList, int position) {
        Intent intent = new Intent(ctx, StreamStarterActivity.class);
        intent.putExtra(P_SONGURL, url);
        intent.putExtra("song", song);
        intent.putExtra("artist", artist);
        intent.putExtra("album", album);
        intent.putExtra(P_LYRICURL, lyricURL);
        intent.putExtra(P_NOTIFICATION, false);
        intent.putExtra(P_HOMEBUTTON, false);
        intent.putExtra(P_SONGLIST, songList);
        intent.putExtra(P_POSITION, position);
        ctx.startActivity(intent);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public static void startActivity(Context ctx, String url, String song, String artist, String album, String lyricURL, boolean homebutton) {
        Intent intent = new Intent(ctx, StreamStarterActivity.class);
        intent.putExtra(P_SONGURL, url);
        intent.putExtra("song", song);
        intent.putExtra("artist", artist);
        intent.putExtra("album", album);
        intent.putExtra(P_LYRICURL, lyricURL);
        intent.putExtra(P_NOTIFICATION, false);
        intent.putExtra(P_HOMEBUTTON, homebutton);
        ctx.startActivity(intent);
    }

    public static Intent makeIntent(Context ctx, String url, String song, String artist, String album, String lyricURL, boolean bNotification) {
        Intent intent = new Intent(ctx, StreamStarterActivity.class);
        intent.putExtra(P_SONGURL, url);
        intent.putExtra("song", song);
        intent.putExtra("artist", artist);
        intent.putExtra("album", album);
        intent.putExtra(P_LYRICURL, lyricURL);
        intent.putExtra(P_NOTIFICATION, bNotification);
        return intent;
    }

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        Bundle bundle = getIntent().getExtras();
        if (bundle.getBoolean(P_HOMEBUTTON, false)) {
            Intent i = new Intent(getIntent());
            i.setClass(this, PlayerActivity.class);
            startActivity(i);
            finish();
        }
        if (bundle.getBoolean(P_NOTIFICATION, false)) {
            Intent i2 = new Intent(getIntent());
            i2.setClass(this, PlayerActivity.class);
            startActivity(i2);
            finish();
        }
        setVolumeControlStream(3);
        requestWindowFeature(1);
        setContentView((int) R.layout.streamstarter);
        TextView tv = (TextView) findViewById(R.id.streamloading);
        Uri uri = Uri.parse(bundle.getString(P_SONGURL));
        if (uri.getHost() == null) {
            setVisible(false);
            return;
        }
        tv.setText(getString(R.string.streamloadingtext, new Object[]{uri.getHost()}));
    }

    public void onResume() {
        super.onResume();
        PlaybackServiceHelper.bindToService(this, new ServiceConnection() {
            public void onServiceConnected(ComponentName name, IBinder service) {
                IntentFilter f = new IntentFilter();
                f.addAction(PlaybackService.ASYNC_OPEN_COMPLETE);
                f.addAction(PlaybackService.PLAYBACK_COMPLETE);
                StreamStarterActivity.this.registerReceiver(StreamStarterActivity.this.mStatusListener, new IntentFilter(f));
                Bundle bundle = StreamStarterActivity.this.getIntent().getExtras();
                PlaybackServiceHelper.mService.openAsync(bundle.getString(StreamStarterActivity.P_SONGURL), bundle.getString("song"), bundle.getString("artist"), bundle.getString("album"), bundle.getString(StreamStarterActivity.P_LYRICURL));
            }

            public void onServiceDisconnected(ComponentName name) {
            }
        });
    }

    public void onPause() {
        if (PlaybackServiceHelper.mService != null && !PlaybackServiceHelper.mService.isPlaying()) {
            PlaybackServiceHelper.mService.stop();
        }
        try {
            unregisterReceiver(this.mStatusListener);
        } catch (Exception e) {
        }
        try {
            PlaybackServiceHelper.unbindFromService(this);
        } catch (Exception e2) {
        }
        super.onPause();
    }
}
