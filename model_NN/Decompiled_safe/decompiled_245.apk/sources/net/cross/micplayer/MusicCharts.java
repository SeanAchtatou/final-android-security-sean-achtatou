package net.cross.micplayer;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import java.util.ArrayList;

public class MusicCharts extends Activity {
    static final int CONNECTING = 1;
    public static final String PREFS_NAME = "Setting.i.music.code.com";
    ArrayAdapter<String> mAdapter;
    boolean mLinkClicked = false;
    ProgressDialog mProgressDialog;
    String mRequestUrl;
    ArrayList<String> mStrings = new ArrayList<>();
    ListView mTypesList;
    WebView mWebview;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.popular);
        AdsView.createAdWhirl(this);
        findViewById(R.id.center_text).setVisibility(8);
        this.mTypesList = (ListView) findViewById(R.id.popular);
        this.mAdapter = new ArrayAdapter<>(this, 17367043, this.mStrings);
        this.mTypesList.setAdapter((ListAdapter) this.mAdapter);
        this.mTypesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
                new Intent();
                if (position == 0) {
                    BbChart100.startActivite(MusicCharts.this, "http://ggapp.appspot.com/ringtone/hot/bb100");
                } else if (position == 1) {
                    BbChart100.startActivite(MusicCharts.this, "http://ggapp.appspot.com/mp3/yahootop2/");
                } else if (position == 2) {
                    BbChart100.startActivite(MusicCharts.this, "http://ggapp.appspot.com/mp3/bhiphop");
                } else if (position == 3) {
                    BbChart100.startActivite(MusicCharts.this, "http://ggapp.appspot.com/mp3/bcountry");
                } else if (position == 4) {
                    BbChart100.startActivite(MusicCharts.this, "http://ggapp.appspot.com/mp3/bmodernrock");
                } else if (position == 5) {
                    BbChart100.startActivite(MusicCharts.this, "http://ggapp.appspot.com/mp3/bdanceclub");
                } else if (position == 6) {
                    BbChart100.startActivite(MusicCharts.this, "http://ggapp.appspot.com/mp3/brap");
                } else if (position == 7) {
                    BbChart100.startActivite(MusicCharts.this, "http://ggapp.appspot.com/mp3/bpop");
                } else if (position == 8) {
                    BbChart100.startActivite(MusicCharts.this, "http://ggapp.appspot.com/mp3/bmainrock");
                } else if (position == 9) {
                    BbChart100.startActivite(MusicCharts.this, "http://ggapp.appspot.com/mp3/badult");
                }
            }
        });
        this.mAdapter.add("Billboard Hot Music 100");
        this.mAdapter.add("Yahoo Hottest Music 10");
        this.mAdapter.add("Hot R&B / Hip-Hop Songs");
        this.mAdapter.add("Hot Country Songs");
        this.mAdapter.add("Hot Modern Rock Tracks");
        this.mAdapter.add("Hot Dance Club Play");
        this.mAdapter.add("Hot Rap Tracks");
        this.mAdapter.add("Hot Pop 100");
        this.mAdapter.add("Hot Mainstream Rock Tracks");
        this.mAdapter.add("Hot Adult Top 40 Tracks");
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                this.mProgressDialog = new ProgressDialog(this);
                this.mProgressDialog.setMessage("Please wait while search...");
                this.mProgressDialog.setIndeterminate(true);
                this.mProgressDialog.setCancelable(true);
                this.mProgressDialog.setButton("Close", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                });
                return this.mProgressDialog;
            default:
                return null;
        }
    }
}
