package net.cross.micplayer.utils;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;
import java.io.File;

public abstract class SQLiteOpenHelperEx {
    private static final String TAG = SQLiteOpenHelperEx.class.getSimpleName();
    private SQLiteDatabase mDatabase = null;
    private final SQLiteDatabase.CursorFactory mFactory;
    private boolean mIsInitializing = false;
    private final String mName;
    private final int mNewVersion;

    public abstract void onCreate(SQLiteDatabase sQLiteDatabase);

    public abstract void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2);

    public SQLiteOpenHelperEx(String name, SQLiteDatabase.CursorFactory factory, int version) {
        if (version < 1) {
            throw new IllegalArgumentException("Version must be >= 1, was " + version);
        }
        this.mName = name;
        this.mFactory = factory;
        this.mNewVersion = version;
        File dbDir = new File(this.mName).getParentFile();
        if (!dbDir.exists()) {
            dbDir.mkdirs();
        } else if (!dbDir.isDirectory() && dbDir.delete()) {
            dbDir.mkdirs();
        }
    }

    public synchronized SQLiteDatabase getWritableDatabase() {
        SQLiteDatabase sQLiteDatabase;
        if (this.mDatabase != null && this.mDatabase.isOpen() && !this.mDatabase.isReadOnly()) {
            sQLiteDatabase = this.mDatabase;
        } else if (this.mIsInitializing) {
            throw new IllegalStateException("getWritableDatabase called recursively");
        } else {
            SQLiteDatabase db = null;
            try {
                this.mIsInitializing = true;
                if (this.mName == null) {
                    db = SQLiteDatabase.create(null);
                } else {
                    db = SQLiteDatabase.openOrCreateDatabase(this.mName, this.mFactory);
                }
                int version = db.getVersion();
                if (version != this.mNewVersion) {
                    db.beginTransaction();
                    if (version == 0) {
                        onCreate(db);
                    } else {
                        onUpgrade(db, version, this.mNewVersion);
                    }
                    db.setVersion(this.mNewVersion);
                    db.setTransactionSuccessful();
                    db.endTransaction();
                }
                onOpen(db);
                this.mIsInitializing = false;
                if (1 != 0) {
                    if (this.mDatabase != null) {
                        try {
                            this.mDatabase.close();
                        } catch (Exception e) {
                        }
                    }
                    this.mDatabase = db;
                } else if (db != null) {
                    db.close();
                }
                sQLiteDatabase = db;
            } catch (Throwable th) {
                this.mIsInitializing = false;
                if (0 != 0) {
                    if (this.mDatabase != null) {
                        try {
                            this.mDatabase.close();
                        } catch (Exception e2) {
                        }
                    }
                    this.mDatabase = db;
                } else if (db != null) {
                    db.close();
                }
                throw th;
            }
        }
        return sQLiteDatabase;
    }

    public synchronized SQLiteDatabase getReadableDatabase() {
        SQLiteDatabase db;
        SQLiteDatabase sQLiteDatabase;
        if (this.mDatabase != null && this.mDatabase.isOpen()) {
            sQLiteDatabase = this.mDatabase;
        } else if (this.mIsInitializing) {
            throw new IllegalStateException("getReadableDatabase called recursively");
        } else {
            try {
                sQLiteDatabase = getWritableDatabase();
            } catch (SQLiteException e) {
                if (this.mName == null) {
                    throw e;
                }
                Log.e(TAG, "Couldn't open " + this.mName + " for writing (will try read-only):", e);
                db = null;
                this.mIsInitializing = true;
                db = SQLiteDatabase.openDatabase(this.mName, this.mFactory, 1);
                if (db.getVersion() != this.mNewVersion) {
                    throw new SQLiteException("Can't upgrade read-only database from version " + db.getVersion() + " to " + this.mNewVersion + ": " + this.mName);
                }
                onOpen(db);
                Log.w(TAG, "Opened " + this.mName + " in read-only mode");
                this.mDatabase = db;
                sQLiteDatabase = this.mDatabase;
                this.mIsInitializing = false;
                if (!(db == null || db == this.mDatabase)) {
                    db.close();
                }
            } catch (Throwable th) {
                this.mIsInitializing = false;
                if (!(db == null || db == this.mDatabase)) {
                    db.close();
                }
                throw th;
            }
        }
        return sQLiteDatabase;
    }

    public synchronized void close() {
        if (this.mIsInitializing) {
            throw new IllegalStateException("Closed during initialization");
        } else if (this.mDatabase != null && this.mDatabase.isOpen()) {
            this.mDatabase.close();
            this.mDatabase = null;
        }
    }

    public void onOpen(SQLiteDatabase db) {
    }
}
