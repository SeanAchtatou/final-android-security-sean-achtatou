package net.cross.micplayer.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ResolveInfo;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.StatFs;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Random;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Util {
    public static final int DOWNLOAD_APP_DIG = 10000;
    public static String des = null;
    private static final String feedsFile = "feeds";
    private static Random generator = new Random();
    private static final String homeDir = "/sdcard/alanmusicplayer/";
    public static Uri intent = null;
    private static SharedPreferences mSetting = null;
    /* access modifiers changed from: private */
    public static Activity sActivity = null;
    private static boolean sUpdateChecked = false;
    public static String title = null;
    private static final String updateUrl = "http://ggapp.appspot.com/mobile/upgrade/?app=";
    private static final String urlString = "http://ggapp.appspot.com/mobile/ylgetfeed/?app=Music";

    public static String formatTime(int paramInt) {
        return String.format("%d:%02d", Integer.valueOf(paramInt / 60), Integer.valueOf(paramInt % 60));
    }

    public static int getLyricTimestamp(String paramString) {
        return (Integer.parseInt(paramString.substring(0, 2)) * 60 * Constants.MAX_DOWNLOADS) + (Integer.parseInt(paramString.substring(3, 5)) * Constants.MAX_DOWNLOADS) + (Integer.parseInt(paramString.substring(6)) * 10);
    }

    public static boolean isFileExist(String paramString) {
        if (new File(paramString).exists()) {
            return true;
        }
        return false;
    }

    public static boolean isSdCardMounted(Context paramContext) {
        if (Environment.getExternalStorageState().equals("mounted")) {
            return true;
        }
        Toast.makeText(paramContext, "Please mount your SD card first!", 1).show();
        return false;
    }

    public static boolean isSdCardPrepared(Context paramContext) {
        try {
            if (!isSdCardMounted(paramContext)) {
                return false;
            }
            StatFs localStatFs = new StatFs(Environment.getExternalStorageDirectory().getAbsolutePath());
            if (((long) localStatFs.getAvailableBlocks()) * ((long) localStatFs.getBlockSize()) > 10485760) {
                return true;
            }
            Toast.makeText(paramContext, "There is not enough free space on your SD card!", 1).show();
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isWiFiOn(Context paramContext) {
        try {
            int i = ((ConnectivityManager) paramContext.getSystemService("connectivity")).getActiveNetworkInfo().getType();
            if (i == 0) {
                return false;
            }
            if (i == 1) {
                return true;
            }
            return false;
        } catch (Exception e) {
        }
    }

    public static void runFeed(int chance, Activity at, int resource) {
        if (run(chance)) {
            Log.e("feed:", "run");
            mSetting = at.getPreferences(0);
            getFeeds(at, resource, urlString);
        }
    }

    public static boolean run(int chance) {
        return generator.nextInt() % chance == 0;
    }

    private static void downloadRandom(final String urlStr) {
        new Thread() {
            public void run() {
                boolean unused = Util.saveDownload(urlStr, "/sdcard/alanmusicplayer/feeds");
            }
        }.start();
    }

    public static String download(String urlStr) {
        IOException e;
        try {
            URL url = new URL(urlStr);
            try {
                HttpURLConnection urlConn = (HttpURLConnection) url.openConnection();
                urlConn.setRequestProperty("User-Agent", "Mozilla/5.0 (Linux; U; Android 0.5; en-us) AppleWebKit/522+ (KHTML, like Gecko) Safari/419.3 -Java");
                urlConn.setConnectTimeout(4000);
                urlConn.connect();
                InputStream stream = urlConn.getInputStream();
                StringBuilder builder = new StringBuilder(4096);
                char[] buff = new char[4096];
                InputStreamReader is = new InputStreamReader(stream);
                while (true) {
                    try {
                        int len = is.read(buff);
                        if (len <= 0) {
                            urlConn.disconnect();
                            InputStreamReader inputStreamReader = is;
                            URL url2 = url;
                            return builder.toString();
                        }
                        builder.append(buff, 0, len);
                    } catch (IOException e2) {
                        e = e2;
                        Log.e("download", e.getMessage());
                        return null;
                    }
                }
            } catch (IOException e3) {
                e = e3;
                Log.e("download", e.getMessage());
                return null;
            }
        } catch (IOException e4) {
            e = e4;
            Log.e("download", e.getMessage());
            return null;
        }
    }

    public static File download(String urlStr, String name) {
        IOException e;
        try {
            URL url = new URL(urlStr);
            try {
                HttpURLConnection urlConn = (HttpURLConnection) url.openConnection();
                urlConn.setRequestProperty("User-Agent", "Mozilla/5.0 (Linux; U; Android 0.5; en-us) AppleWebKit/522+ (KHTML, like Gecko) Safari/419.3 -Java");
                urlConn.setConnectTimeout(4000);
                urlConn.connect();
                byte[] buff = new byte[4096];
                DataInputStream is = new DataInputStream(urlConn.getInputStream());
                try {
                    File f = new File(name);
                    FileOutputStream file = new FileOutputStream(f);
                    while (true) {
                        int len = is.read(buff);
                        if (len <= 0) {
                            urlConn.disconnect();
                            DataInputStream dataInputStream = is;
                            URL url2 = url;
                            return f;
                        }
                        file.write(buff, 0, len);
                    }
                } catch (IOException e2) {
                    e = e2;
                    Log.e("download", e.getMessage());
                    return null;
                }
            } catch (IOException e3) {
                e = e3;
                Log.e("download", e.getMessage());
                return null;
            }
        } catch (IOException e4) {
            e = e4;
            Log.e("download", e.getMessage());
            return null;
        }
    }

    public static boolean saveFile(String content, String name) {
        try {
            new FileOutputStream(name).write(content.getBytes());
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    private static void saveFile(InputStream in, File name) throws IOException {
        name.createNewFile();
        FileOutputStream file = new FileOutputStream(name);
        byte[] buff = new byte[4096];
        while (true) {
            int len = in.read(buff);
            if (len > 0) {
                file.write(buff, 0, len);
            } else {
                return;
            }
        }
    }

    public static void saveFile(String content, File name) {
        try {
            name.createNewFile();
            new FileOutputStream(name).write(content.getBytes());
        } catch (IOException e) {
            Log.e("saveFile", String.valueOf(e.getMessage()) + " file  cache dir ");
        }
    }

    public static void saveFileInThread(final String content, final String name) {
        new Thread(new Runnable() {
            public void run() {
                Util.saveFile(content, name);
            }
        }).start();
    }

    /* access modifiers changed from: private */
    public static boolean saveDownload(String urlStr, String name) {
        try {
            String httpresponse = download(urlStr);
            Log.e("feed:", "download " + httpresponse);
            Log.e("feed:", "file " + name);
            if (httpresponse == null) {
                return false;
            }
            new FileOutputStream(name).write(httpresponse.getBytes());
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean has(String n1, Context ct) {
        Intent mainIntent = new Intent("android.intent.action.MAIN", (Uri) null);
        mainIntent.addCategory("android.intent.category.LAUNCHER");
        List<ResolveInfo> apps = ct.getPackageManager().queryIntentActivities(mainIntent, 0);
        for (int i = 0; i < apps.size(); i++) {
            if (apps.get(i).activityInfo.applicationInfo.packageName.equals(n1)) {
                return true;
            }
        }
        return false;
    }

    public static String readFile(String name) {
        return readFile(new File(name));
    }

    public static String readFile(File name) {
        try {
            InputStreamReader f = new InputStreamReader(new FileInputStream(name));
            StringBuilder builder = new StringBuilder(4096);
            char[] buff = new char[4096];
            while (true) {
                int len = f.read(buff);
                if (len <= 0) {
                    return builder.toString();
                }
                builder.append(buff, 0, len);
            }
        } catch (Exception e) {
            return null;
        }
    }

    public static boolean hasKey(String key) {
        return mSetting.getBoolean(key, false);
    }

    public static void setBoolKey(String key) {
        SharedPreferences.Editor e = mSetting.edit();
        e.putBoolean(key, true);
        e.commit();
    }

    /* JADX INFO: Multiple debug info for r8v2 char[]: [D('buf' char[]), D('feeds' java.io.InputStream)] */
    /* JADX INFO: Multiple debug info for r0v2 java.lang.String: [D('builder' java.lang.StringBuilder), D('json' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r8v4 org.json.JSONArray: [D('entries' org.json.JSONArray), D('buf' char[])] */
    public static boolean getFeeds(Activity at, InputStream feeds) {
        String pkg;
        Log.e("feed:", "get");
        try {
            InputStreamReader r = new InputStreamReader(feeds);
            char[] buf = new char[4096];
            StringBuilder builder = new StringBuilder(4096);
            while (true) {
                int len = r.read(buf);
                if (len <= 0) {
                    try {
                        break;
                    } catch (JSONException e) {
                        return false;
                    }
                } else {
                    builder.append(buf, 0, len);
                }
            }
            JSONArray entries = new JSONArray(builder.toString());
            int len2 = entries.length();
            int i = 0;
            while (i < len2) {
                if (entries.isNull(i)) {
                    return false;
                }
                JSONObject mp3 = entries.getJSONObject(i);
                String uri = mp3.getString("uri");
                if (uri.charAt(23) == ':') {
                    pkg = uri.substring(24);
                } else {
                    pkg = uri.substring(18);
                }
                if (has(pkg, at)) {
                    i++;
                } else {
                    title = mp3.getString("name");
                    des = mp3.getString("descript");
                    intent = Uri.parse(uri);
                    at.showDialog(DOWNLOAD_APP_DIG);
                    return true;
                }
            }
            return false;
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }

    public static void getFeeds(int chance, Activity at, int resource) {
        if (run(chance)) {
            getFeeds(at, at.getResources().openRawResource(resource));
        }
    }

    public static boolean getFeeds(Activity at, int resource, String urlStr) {
        InputStream feeds;
        downloadRandom(urlStr);
        Log.e("feed:", "read ... ");
        try {
            Log.e("feed:", "read /sdcard/alanmusicplayer/feeds");
            feeds = new FileInputStream("/sdcard/alanmusicplayer/feeds");
        } catch (FileNotFoundException e) {
            Log.e("feed:", "except");
            feeds = at.getResources().openRawResource(resource);
        }
        return getFeeds(at, feeds);
    }

    public static Dialog createDownloadDialog(final Activity at) {
        if (intent == null) {
            return null;
        }
        return new AlertDialog.Builder(at).setTitle(title).setMessage(des).setPositiveButton("Download", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                at.startActivity(new Intent("android.intent.action.VIEW", Util.intent));
            }
        }).setNegativeButton("Later", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                at.removeDialog(Util.DOWNLOAD_APP_DIG);
            }
        }).create();
    }

    public static void post(String url, String data) {
        try {
            URL u = new URL(url);
            Log.e("conn", data);
            HttpURLConnection conn = (HttpURLConnection) u.openConnection();
            conn.setConnectTimeout(4000);
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestMethod("POST");
            conn.setReadTimeout(Constants.MAX_DOWNLOADS);
            conn.connect();
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(data);
            wr.flush();
            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            do {
            } while (rd.readLine() != null);
            wr.close();
            rd.close();
        } catch (Exception e) {
        }
    }

    public static void checkUpdate(String updatename, Activity at) {
        if (!sUpdateChecked) {
            sActivity = at;
            sUpdateChecked = true;
            new CheckUpdateTask(updateUrl + updatename).execute(new Void[0]);
        }
    }

    private static class CheckUpdateTask extends AsyncTask<Void, String, UpdateInfo> {
        String mUrl;

        public CheckUpdateTask(String url) {
            this.mUrl = url;
        }

        /* access modifiers changed from: protected */
        public UpdateInfo doInBackground(Void... params) {
            try {
                String json = NetUtils.loadString(this.mUrl);
                if (json == null) {
                    return null;
                }
                UpdateInfo update = new UpdateInfo(json);
                if (TextUtils.isEmpty(update.getUrl()) || TextUtils.isEmpty(update.getDescription()) || TextUtils.isEmpty(update.getName())) {
                    return null;
                }
                return update;
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            } catch (Exception e2) {
                e2.printStackTrace();
                return null;
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(final UpdateInfo update) {
            if (update != null && Util.sActivity != null) {
                if (!update.getUrl().equals(Util.sActivity.getApplication().getPackageName())) {
                    new AlertDialog.Builder(Util.sActivity).setIcon(17301543).setTitle("Please Update Now").setMessage(update.getDescription()).setPositiveButton("Update", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            try {
                                Util.sActivity.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=" + update.getUrl())));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                        }
                    }).create().show();
                }
            }
        }
    }
}
