package net.cross.micplayer.utils;

import java.net.URLEncoder;

public class LastFMHelper {
    private static final String API_KEY = "c309476a7d0c6c29d4ed6632678c5054";

    public static final String getAlbumInfoURL(String artist, String album) {
        String artist2 = URLEncoder.encode(artist);
        return "http://ws.audioscrobbler.com/2.0/?method=album.getinfo&api_key=c309476a7d0c6c29d4ed6632678c5054&artist=" + artist2 + "&album=" + URLEncoder.encode(album);
    }

    public static final String getArtistImageURL(String artist) {
        return "http://ws.audioscrobbler.com/2.0/?method=artist.getimages&artist=" + URLEncoder.encode(artist) + "&api_key=" + API_KEY + "&limit=1";
    }
}
