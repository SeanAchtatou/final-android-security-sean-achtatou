package net.cross.micplayer.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.AsyncQueryHandler;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.PowerManager;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.RemoteViews;
import com.qwapi.adclient.android.utils.Utils;
import java.io.IOException;
import java.util.Random;
import java.util.Vector;
import net.cross.micplayer.R;
import net.cross.micplayer.StreamStarterActivity;
import net.cross.micplayer.data.Downloaded;
import net.cross.micplayer.utils.Constants;

public class PlaybackService extends Service {
    public static final String ASYNC_OPEN_COMPLETE = "net.cross.micplayer.asyncopencomplete";
    public static final String CMDNAME = "command";
    public static final String CMDNEXT = "next";
    public static final String CMDPAUSE = "pause";
    public static final String CMDPREVIOUS = "previous";
    public static final String CMDSTOP = "stop";
    public static final String CMDTOGGLEPAUSE = "togglepause";
    private static final int FADEIN = 4;
    private static final int IDCOLIDX = 0;
    private static final int IDLE_DELAY = 60000;
    private static final String LOGTAG = "PlaybackService";
    private static final int MAX_HISTORY_SIZE = 10;
    public static final String META_CHANGED = "net.cross.micplayermetachanged";
    public static final String NEXT_ACTION = "net.cross.micplayer.musicservicecommand.next";
    public static final String PAUSE_ACTION = "net.cross.micplayer.musicservicecommand.pause";
    public static final int PLAYBACKSERVICE_STATUS = 100;
    public static final String PLAYBACK_COMPLETE = "net.cross.micplayerplaybackcomplete";
    public static final String PLAYSTATE_CHANGED = "net.cross.micplayerplaystatechanged";
    public static final String PREVIOUS_ACTION = "net.cross.micplayer.musicservicecommand.previous";
    public static final String QUEUE_CHANGED = "net.cross.micplayer.queuechanged";
    private static final int RELEASE_WAKELOCK = 2;
    public static final int REPEAT_ALL = 2;
    public static final int REPEAT_CURRENT = 1;
    public static final int REPEAT_NONE = 0;
    private static final int SERVER_DIED = 3;
    public static final String SERVICECMD = "net.cross.micplayer.musicservicecommand";
    public static final int SHUFFLE_AUTO = 2;
    public static final int SHUFFLE_NONE = 0;
    public static final int SHUFFLE_NORMAL = 1;
    public static final String TOGGLEPAUSE_ACTION = "net.cross.micplayer.musicservicecommand.togglepause";
    private static final int TRACK_ENDED = 1;
    private final char[] hexdigits = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
    private String mAlbum = Utils.EMPTY_STRING;
    private String mArtist = Utils.EMPTY_STRING;
    private int[] mAutoShuffleList = null;
    private final IBinder mBinder = new LocalBinder();
    private Cursor mCursor;
    String[] mCursorCols = {"audio._id AS _id", "artist", "album", "title", "_data", "mime_type", "album_id", "artist_id"};
    private Handler mDelayedStopHandler = new Handler() {
        public void handleMessage(Message msg) {
            if (!PlaybackService.this.isPlaying() && !PlaybackService.this.mResumeAfterCall && !PlaybackService.this.mServiceInUse && !PlaybackService.this.mMediaplayerHandler.hasMessages(1)) {
                PlaybackService.this.saveQueue(true);
                PlaybackService.this.stopSelf(PlaybackService.this.mServiceStartId);
            }
        }
    };
    private String mFileToPlay;
    private Vector<Integer> mHistory = new Vector<>(10);
    private BroadcastReceiver mIntentReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            String cmd = intent.getStringExtra(PlaybackService.CMDNAME);
            if (PlaybackService.CMDNEXT.equals(cmd) || PlaybackService.NEXT_ACTION.equals(action)) {
                PlaybackService.this.next(true);
            } else if (PlaybackService.CMDPREVIOUS.equals(cmd) || PlaybackService.PREVIOUS_ACTION.equals(action)) {
                PlaybackService.this.prev();
            } else if (PlaybackService.CMDTOGGLEPAUSE.equals(cmd) || PlaybackService.TOGGLEPAUSE_ACTION.equals(action)) {
                if (PlaybackService.this.isPlaying()) {
                    PlaybackService.this.pause();
                } else {
                    PlaybackService.this.play();
                }
            } else if (PlaybackService.CMDPAUSE.equals(cmd) || PlaybackService.PAUSE_ACTION.equals(action)) {
                PlaybackService.this.pause();
            } else if (PlaybackService.CMDSTOP.equals(cmd)) {
                PlaybackService.this.pause();
                PlaybackService.this.seek(0);
            }
        }
    };
    /* access modifiers changed from: private */
    public boolean mIsSupposedToBePlaying = false;
    private String mLyricURL = Utils.EMPTY_STRING;
    /* access modifiers changed from: private */
    public int mMediaMountedCount = 0;
    /* access modifiers changed from: private */
    public Handler mMediaplayerHandler = new Handler() {
        float mCurrentVolume = 1.0f;

        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    if (PlaybackService.this.mRepeatMode == 1) {
                        PlaybackService.this.seek(0);
                        PlaybackService.this.play();
                        return;
                    } else if (!PlaybackService.this.mOneShot) {
                        PlaybackService.this.next(false);
                        return;
                    } else {
                        PlaybackService.this.notifyChange(PlaybackService.PLAYBACK_COMPLETE);
                        PlaybackService.this.mIsSupposedToBePlaying = false;
                        return;
                    }
                case 2:
                    PlaybackService.this.mWakeLock.release();
                    return;
                case 3:
                default:
                    return;
                case 4:
                    if (!PlaybackService.this.isPlaying()) {
                        this.mCurrentVolume = 0.0f;
                        PlaybackService.this.mPlayer.setVolume(this.mCurrentVolume);
                        PlaybackService.this.play();
                        PlaybackService.this.mMediaplayerHandler.sendEmptyMessageDelayed(4, 10);
                        return;
                    }
                    this.mCurrentVolume += 0.01f;
                    if (this.mCurrentVolume < 1.0f) {
                        PlaybackService.this.mMediaplayerHandler.sendEmptyMessageDelayed(4, 10);
                    } else {
                        this.mCurrentVolume = 1.0f;
                    }
                    PlaybackService.this.mPlayer.setVolume(this.mCurrentVolume);
                    return;
            }
        }
    };
    /* access modifiers changed from: private */
    public boolean mOneShot = true;
    private boolean mOpenAsync = false;
    private int mOpenFailedCounter = 0;
    private PhoneStateListener mPhoneStateListener = new PhoneStateListener() {
        public void onCallStateChanged(int state, String incomingNumber) {
            boolean z;
            boolean z2;
            if (state == 1) {
                if (((AudioManager) PlaybackService.this.getSystemService("audio")).getStreamVolume(2) > 0) {
                    PlaybackService playbackService = PlaybackService.this;
                    if (PlaybackService.this.isPlaying() || PlaybackService.this.mResumeAfterCall) {
                        z2 = true;
                    } else {
                        z2 = false;
                    }
                    playbackService.mResumeAfterCall = z2;
                    PlaybackService.this.pause();
                }
            } else if (state == 2) {
                PlaybackService playbackService2 = PlaybackService.this;
                if (PlaybackService.this.isPlaying() || PlaybackService.this.mResumeAfterCall) {
                    z = true;
                } else {
                    z = false;
                }
                playbackService2.mResumeAfterCall = z;
                PlaybackService.this.pause();
            } else if (state == 0 && PlaybackService.this.mResumeAfterCall) {
                PlaybackService.this.startAndFadeIn();
                PlaybackService.this.mResumeAfterCall = false;
            }
        }
    };
    private int[] mPlayList = null;
    private int mPlayListLen = 0;
    private int mPlayPos = -1;
    /* access modifiers changed from: private */
    public MultiPlayer mPlayer;
    private SharedPreferences mPreferences;
    private boolean mQuietMode = false;
    private final Shuffler mRand = new Shuffler(null);
    /* access modifiers changed from: private */
    public int mRepeatMode = 0;
    /* access modifiers changed from: private */
    public boolean mResumeAfterCall = false;
    /* access modifiers changed from: private */
    public boolean mServiceInUse = false;
    /* access modifiers changed from: private */
    public int mServiceStartId = -1;
    private int mShuffleMode = 0;
    private String mSongURL = Utils.EMPTY_STRING;
    private String mTrackName = Utils.EMPTY_STRING;
    private BroadcastReceiver mUnmountReceiver = null;
    /* access modifiers changed from: private */
    public PowerManager.WakeLock mWakeLock;

    public class LocalBinder extends Binder {
        public LocalBinder() {
        }

        public PlaybackService getService() {
            return PlaybackService.this;
        }
    }

    public IBinder onBind(Intent intent) {
        this.mDelayedStopHandler.removeCallbacksAndMessages(null);
        this.mServiceInUse = true;
        return this.mBinder;
    }

    public void onRebind(Intent intent) {
        this.mDelayedStopHandler.removeCallbacksAndMessages(null);
        this.mServiceInUse = true;
    }

    public boolean onUnbind(Intent intent) {
        this.mServiceInUse = false;
        saveQueue(true);
        if (isPlaying() || this.mResumeAfterCall) {
            return true;
        }
        if (this.mPlayListLen > 0 || this.mMediaplayerHandler.hasMessages(1)) {
            this.mDelayedStopHandler.sendMessageDelayed(this.mDelayedStopHandler.obtainMessage(), Constants.MusicLengthLimit);
            return true;
        }
        stopSelf(this.mServiceStartId);
        return true;
    }

    public void onCreate() {
        super.onCreate();
        this.mPreferences = getSharedPreferences("Music", 3);
        registerExternalStorageListener();
        this.mPlayer = new MultiPlayer();
        this.mPlayer.setHandler(this.mMediaplayerHandler);
        ((NotificationManager) getSystemService("notification")).cancel(100);
        reloadQueue();
        IntentFilter commandFilter = new IntentFilter();
        commandFilter.addAction(SERVICECMD);
        commandFilter.addAction(TOGGLEPAUSE_ACTION);
        commandFilter.addAction(PAUSE_ACTION);
        commandFilter.addAction(NEXT_ACTION);
        commandFilter.addAction(PREVIOUS_ACTION);
        registerReceiver(this.mIntentReceiver, commandFilter);
        ((TelephonyManager) getSystemService("phone")).listen(this.mPhoneStateListener, 32);
        this.mWakeLock = ((PowerManager) getSystemService("power")).newWakeLock(1, getClass().getName());
        this.mWakeLock.setReferenceCounted(false);
        this.mDelayedStopHandler.sendMessageDelayed(this.mDelayedStopHandler.obtainMessage(), Constants.MusicLengthLimit);
    }

    public void onDestroy() {
        ((NotificationManager) getSystemService("notification")).cancel(100);
        if (isPlaying()) {
            Log.e(LOGTAG, "Service being destroyed while still playing.");
        }
        this.mPlayer.release();
        this.mPlayer = null;
        this.mDelayedStopHandler.removeCallbacksAndMessages(null);
        this.mMediaplayerHandler.removeCallbacksAndMessages(null);
        ((TelephonyManager) getSystemService("phone")).listen(this.mPhoneStateListener, 0);
        if (this.mUnmountReceiver != null) {
            unregisterReceiver(this.mUnmountReceiver);
            this.mUnmountReceiver = null;
        }
        if (this.mCursor != null) {
            this.mCursor.close();
            this.mCursor = null;
        }
        unregisterReceiver(this.mIntentReceiver);
        this.mWakeLock.release();
        super.onDestroy();
    }

    /* access modifiers changed from: private */
    public void saveQueue(boolean full) {
        if (!this.mOneShot) {
            SharedPreferences.Editor ed = this.mPreferences.edit();
            if (full) {
                StringBuilder q = new StringBuilder();
                int len = this.mPlayListLen;
                for (int i = 0; i < len; i++) {
                    int n = this.mPlayList[i];
                    if (n == 0) {
                        q.append("0;");
                    } else {
                        while (n != 0) {
                            int digit = n & 15;
                            n >>= 4;
                            q.append(this.hexdigits[digit]);
                        }
                        q.append(";");
                    }
                }
                ed.putString("queue", q.toString());
            }
            ed.putInt("curpos", this.mPlayPos);
            if (this.mPlayer.isInitialized()) {
                ed.putLong("seekpos", this.mPlayer.position());
            }
            ed.putInt("repeatmode", this.mRepeatMode);
            ed.putInt("shufflemode", this.mShuffleMode);
            ed.commit();
        }
    }

    /* access modifiers changed from: private */
    public void reloadQueue() {
        String q = null;
        int qlen = q != null ? q.length() : 0;
        if (qlen > 1) {
            int plen = 0;
            int n = 0;
            int shift = 0;
            int i = 0;
            while (true) {
                if (i >= qlen) {
                    break;
                }
                char c = q.charAt(i);
                if (c == ';') {
                    ensurePlayListCapacity(plen + 1);
                    this.mPlayList[plen] = n;
                    plen++;
                    n = 0;
                    shift = 0;
                } else {
                    if (c >= '0' && c <= '9') {
                        n += (c - '0') << shift;
                    } else if (c < 'a' || c > 'f') {
                        plen = 0;
                    } else {
                        n += ((c + 10) - 97) << shift;
                    }
                    shift += 4;
                }
                i++;
            }
            this.mPlayListLen = plen;
            int pos = this.mPreferences.getInt("curpos", 0);
            if (pos < 0 || pos >= this.mPlayListLen) {
                this.mPlayListLen = 0;
                return;
            }
            this.mPlayPos = pos;
            Cursor c2 = getContentResolver().query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, new String[]{"_id"}, "_id=" + this.mPlayList[this.mPlayPos], null, null);
            if (c2 == null || c2.getCount() == 0) {
                SystemClock.sleep(3000);
                c2 = getContentResolver().query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, this.mCursorCols, "_id=" + this.mPlayList[this.mPlayPos], null, null);
            }
            if (c2 != null) {
                c2.close();
            }
            this.mOpenFailedCounter = 20;
            this.mQuietMode = true;
            openCurrent();
            this.mQuietMode = false;
            if (!this.mPlayer.isInitialized()) {
                this.mPlayListLen = 0;
                return;
            }
            long seekpos = this.mPreferences.getLong("seekpos", 0);
            seek((seekpos < 0 || seekpos >= duration()) ? 0 : seekpos);
            int repmode = this.mPreferences.getInt("repeatmode", 0);
            if (!(repmode == 2 || repmode == 1)) {
                repmode = 0;
            }
            this.mRepeatMode = repmode;
            int shufmode = this.mPreferences.getInt("shufflemode", 0);
            if (!(shufmode == 2 || shufmode == 1)) {
                shufmode = 0;
            }
            if (shufmode == 2 && !makeAutoShuffleList()) {
                shufmode = 0;
            }
            this.mShuffleMode = shufmode;
        }
    }

    public void onStart(Intent intent, int startId) {
        this.mServiceStartId = startId;
        this.mDelayedStopHandler.removeCallbacksAndMessages(null);
        String action = intent.getAction();
        String cmd = intent.getStringExtra(CMDNAME);
        if (CMDNEXT.equals(cmd) || NEXT_ACTION.equals(action)) {
            next(true);
        } else if (CMDPREVIOUS.equals(cmd) || PREVIOUS_ACTION.equals(action)) {
            prev();
        } else if (CMDTOGGLEPAUSE.equals(cmd) || TOGGLEPAUSE_ACTION.equals(action)) {
            if (isPlaying()) {
                pause();
            } else {
                play();
            }
        } else if (CMDPAUSE.equals(cmd) || PAUSE_ACTION.equals(action)) {
            pause();
        } else if (CMDSTOP.equals(cmd)) {
            pause();
            seek(0);
        }
        this.mDelayedStopHandler.removeCallbacksAndMessages(null);
        this.mDelayedStopHandler.sendMessageDelayed(this.mDelayedStopHandler.obtainMessage(), Constants.MusicLengthLimit);
    }

    public void registerExternalStorageListener() {
        if (this.mUnmountReceiver == null) {
            this.mUnmountReceiver = new BroadcastReceiver() {
                public void onReceive(Context context, Intent intent) {
                    String action = intent.getAction();
                    if (action.equals("android.intent.action.MEDIA_EJECT")) {
                        PlaybackService.this.saveQueue(true);
                        PlaybackService.this.mOneShot = true;
                        PlaybackService.this.closeExternalStorageFiles(intent.getData().getPath());
                    } else if (action.equals("android.intent.action.MEDIA_MOUNTED")) {
                        PlaybackService playbackService = PlaybackService.this;
                        playbackService.mMediaMountedCount = playbackService.mMediaMountedCount + 1;
                        PlaybackService.this.reloadQueue();
                        PlaybackService.this.notifyChange(PlaybackService.QUEUE_CHANGED);
                        PlaybackService.this.notifyChange(PlaybackService.META_CHANGED);
                    }
                }
            };
            IntentFilter iFilter = new IntentFilter();
            iFilter.addAction("android.intent.action.MEDIA_EJECT");
            iFilter.addAction("android.intent.action.MEDIA_MOUNTED");
            iFilter.addDataScheme("file");
            registerReceiver(this.mUnmountReceiver, iFilter);
        }
    }

    public void closeExternalStorageFiles(String storagePath) {
        stop(true);
        notifyChange(META_CHANGED);
    }

    /* access modifiers changed from: private */
    public void startAndFadeIn() {
        this.mMediaplayerHandler.sendEmptyMessageDelayed(4, 10);
    }

    private void ensurePlayListCapacity(int size) {
        if (this.mPlayList == null || size > this.mPlayList.length) {
            int[] newlist = new int[(size * 2)];
            int len = this.mPlayList != null ? this.mPlayList.length : this.mPlayListLen;
            for (int i = 0; i < len; i++) {
                newlist[i] = this.mPlayList[i];
            }
            this.mPlayList = newlist;
        }
    }

    private void saveBookmarkIfNeeded() {
    }

    private void addToPlayList(long[] list, int position) {
        int addlen = list.length;
        if (position < 0) {
            this.mPlayListLen = 0;
            position = 0;
        }
        ensurePlayListCapacity(this.mPlayListLen + addlen);
        if (position > this.mPlayListLen) {
            position = this.mPlayListLen;
        }
        for (int i = this.mPlayListLen - position; i > 0; i--) {
            this.mPlayList[position + i] = this.mPlayList[(position + i) - addlen];
        }
        for (int i2 = 0; i2 < addlen; i2++) {
            this.mPlayList[position + i2] = (int) list[i2];
        }
        this.mPlayListLen += addlen;
    }

    public long[] getQueue() {
        long[] list;
        synchronized (this) {
            int len = this.mPlayListLen;
            list = new long[len];
            for (int i = 0; i < len; i++) {
                list[i] = (long) this.mPlayList[i];
            }
        }
        return list;
    }

    public int getQueuePosition() {
        int i;
        synchronized (this) {
            i = this.mPlayPos;
        }
        return i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: net.cross.micplayer.service.PlaybackService.open(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      net.cross.micplayer.service.PlaybackService.open(long[], int):void
      net.cross.micplayer.service.PlaybackService.open(java.lang.String, boolean):void */
    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void openCurrent() {
        /*
            r7 = this;
            monitor-enter(r7)
            android.database.Cursor r0 = r7.mCursor     // Catch:{ all -> 0x0068 }
            if (r0 == 0) goto L_0x000d
            android.database.Cursor r0 = r7.mCursor     // Catch:{ all -> 0x0068 }
            r0.close()     // Catch:{ all -> 0x0068 }
            r0 = 0
            r7.mCursor = r0     // Catch:{ all -> 0x0068 }
        L_0x000d:
            int r0 = r7.mPlayListLen     // Catch:{ all -> 0x0068 }
            if (r0 != 0) goto L_0x0013
            monitor-exit(r7)     // Catch:{ all -> 0x0068 }
        L_0x0012:
            return
        L_0x0013:
            r0 = 0
            r7.stop(r0)     // Catch:{ all -> 0x0068 }
            int[] r0 = r7.mPlayList     // Catch:{ all -> 0x0068 }
            int r1 = r7.mPlayPos     // Catch:{ all -> 0x0068 }
            r0 = r0[r1]     // Catch:{ all -> 0x0068 }
            java.lang.String r6 = java.lang.String.valueOf(r0)     // Catch:{ all -> 0x0068 }
            android.content.ContentResolver r0 = r7.getContentResolver()     // Catch:{ all -> 0x0068 }
            android.net.Uri r1 = android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI     // Catch:{ all -> 0x0068 }
            java.lang.String[] r2 = r7.mCursorCols     // Catch:{ all -> 0x0068 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0068 }
            java.lang.String r4 = "_id="
            r3.<init>(r4)     // Catch:{ all -> 0x0068 }
            java.lang.StringBuilder r3 = r3.append(r6)     // Catch:{ all -> 0x0068 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0068 }
            r4 = 0
            r5 = 0
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ all -> 0x0068 }
            r7.mCursor = r0     // Catch:{ all -> 0x0068 }
            android.database.Cursor r0 = r7.mCursor     // Catch:{ all -> 0x0068 }
            if (r0 == 0) goto L_0x0066
            android.database.Cursor r0 = r7.mCursor     // Catch:{ all -> 0x0068 }
            r0.moveToFirst()     // Catch:{ all -> 0x0068 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x0068 }
            r0.<init>()     // Catch:{ all -> 0x0068 }
            android.net.Uri r1 = android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI     // Catch:{ all -> 0x0068 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x0068 }
            java.lang.String r1 = "/"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x0068 }
            java.lang.StringBuilder r0 = r0.append(r6)     // Catch:{ all -> 0x0068 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0068 }
            r1 = 0
            r7.open(r0, r1)     // Catch:{ all -> 0x0068 }
        L_0x0066:
            monitor-exit(r7)     // Catch:{ all -> 0x0068 }
            goto L_0x0012
        L_0x0068:
            r0 = move-exception
            monitor-exit(r7)     // Catch:{ all -> 0x0068 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: net.cross.micplayer.service.PlaybackService.openCurrent():void");
    }

    private static class Shuffler {
        private int mPrevious;
        private Random mRandom;

        private Shuffler() {
            this.mRandom = new Random();
        }

        /* synthetic */ Shuffler(Shuffler shuffler) {
            this();
        }

        public int nextInt(int interval) {
            int ret;
            do {
                ret = this.mRandom.nextInt(interval);
                if (ret != this.mPrevious) {
                    break;
                }
            } while (interval > 1);
            this.mPrevious = ret;
            return ret;
        }
    }

    private void doAutoShuffleUpdate() {
        boolean notify = false;
        if (this.mPlayPos > 10) {
            removeTracks(0, this.mPlayPos - 9);
            notify = true;
        }
        int to_add = 7 - (this.mPlayListLen - (this.mPlayPos < 0 ? -1 : this.mPlayPos));
        for (int i = 0; i < to_add; i++) {
            Integer which = Integer.valueOf(this.mAutoShuffleList[this.mRand.nextInt(this.mAutoShuffleList.length)]);
            ensurePlayListCapacity(this.mPlayListLen + 1);
            int[] iArr = this.mPlayList;
            int i2 = this.mPlayListLen;
            this.mPlayListLen = i2 + 1;
            iArr[i2] = which.intValue();
            notify = true;
        }
        if (notify) {
            notifyChange(QUEUE_CHANGED);
        }
    }

    private boolean makeAutoShuffleList() {
        ContentResolver res = getContentResolver();
        Cursor c = null;
        try {
            c = res.query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, new String[]{"_id"}, "is_music=1", null, null);
            if (c == null || c.getCount() == 0) {
                if (c != null) {
                    c.close();
                }
                return false;
            }
            int len = c.getCount();
            int[] list = new int[len];
            for (int i = 0; i < len; i++) {
                c.moveToNext();
                list[i] = c.getInt(0);
            }
            this.mAutoShuffleList = list;
            if (c != null) {
                c.close();
            }
            return true;
        } catch (RuntimeException e) {
            if (c != null) {
                c.close();
            }
            return false;
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
            throw th;
        }
    }

    private int removeTracksInternal(int first, int last) {
        int i = 0;
        synchronized (this) {
            if (last >= first) {
                if (first < 0) {
                    first = 0;
                }
                if (last >= this.mPlayListLen) {
                    last = this.mPlayListLen - 1;
                }
                boolean gotonext = false;
                if (first <= this.mPlayPos && this.mPlayPos <= last) {
                    this.mPlayPos = first;
                    gotonext = true;
                } else if (this.mPlayPos > last) {
                    this.mPlayPos -= (last - first) + 1;
                }
                int num = (this.mPlayListLen - last) - 1;
                for (int i2 = 0; i2 < num; i2++) {
                    this.mPlayList[first + i2] = this.mPlayList[last + 1 + i2];
                }
                this.mPlayListLen -= (last - first) + 1;
                if (gotonext) {
                    if (this.mPlayListLen == 0) {
                        stop(true);
                        this.mPlayPos = -1;
                    } else {
                        if (this.mPlayPos >= this.mPlayListLen) {
                            this.mPlayPos = 0;
                        }
                        boolean wasPlaying = isPlaying();
                        stop(false);
                        openCurrent();
                        if (wasPlaying) {
                            play();
                        }
                    }
                }
                i = (last - first) + 1;
            }
        }
        return i;
    }

    public int removeTracks(int first, int last) {
        int numremoved = removeTracksInternal(first, last);
        if (numremoved > 0) {
            notifyChange(QUEUE_CHANGED);
        }
        return numremoved;
    }

    public void openAsync(String songURL, String trackName, String artist, String album, String lyricURL) {
        synchronized (this) {
            if (songURL != null) {
                this.mRepeatMode = 0;
                ensurePlayListCapacity(1);
                this.mPlayListLen = 1;
                this.mPlayPos = -1;
                this.mCursor = null;
                this.mFileToPlay = songURL;
                this.mSongURL = songURL;
                this.mTrackName = trackName;
                this.mArtist = artist;
                this.mAlbum = album;
                this.mLyricURL = lyricURL;
                this.mPlayer.setDataSourceAsync(songURL);
                this.mOneShot = true;
                this.mOpenAsync = true;
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:47:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void open(java.lang.String r10, boolean r11) {
        /*
            r9 = this;
            r8 = 1
            monitor-enter(r9)
            if (r10 != 0) goto L_0x0006
            monitor-exit(r9)     // Catch:{ all -> 0x0094 }
        L_0x0005:
            return
        L_0x0006:
            if (r11 == 0) goto L_0x0015
            r2 = 0
            r9.mRepeatMode = r2     // Catch:{ all -> 0x0094 }
            r2 = 1
            r9.ensurePlayListCapacity(r2)     // Catch:{ all -> 0x0094 }
            r2 = 1
            r9.mPlayListLen = r2     // Catch:{ all -> 0x0094 }
            r2 = -1
            r9.mPlayPos = r2     // Catch:{ all -> 0x0094 }
        L_0x0015:
            android.database.Cursor r2 = r9.mCursor     // Catch:{ all -> 0x0094 }
            if (r2 != 0) goto L_0x004a
            android.content.ContentResolver r0 = r9.getContentResolver()     // Catch:{ all -> 0x0094 }
            java.lang.String r2 = "content://media/"
            boolean r2 = r10.startsWith(r2)     // Catch:{ all -> 0x0094 }
            if (r2 == 0) goto L_0x0097
            android.net.Uri r1 = android.net.Uri.parse(r10)     // Catch:{ all -> 0x0094 }
            r3 = 0
            r4 = 0
            java.lang.String[] r4 = (java.lang.String[]) r4     // Catch:{ all -> 0x0094 }
        L_0x002d:
            java.lang.String[] r2 = r9.mCursorCols     // Catch:{ UnsupportedOperationException -> 0x00c0 }
            r5 = 0
            android.database.Cursor r2 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ UnsupportedOperationException -> 0x00c0 }
            r9.mCursor = r2     // Catch:{ UnsupportedOperationException -> 0x00c0 }
            android.database.Cursor r2 = r9.mCursor     // Catch:{ UnsupportedOperationException -> 0x00c0 }
            if (r2 == 0) goto L_0x004a
            android.database.Cursor r2 = r9.mCursor     // Catch:{ UnsupportedOperationException -> 0x00c0 }
            int r2 = r2.getCount()     // Catch:{ UnsupportedOperationException -> 0x00c0 }
            if (r2 != 0) goto L_0x00a4
            android.database.Cursor r2 = r9.mCursor     // Catch:{ UnsupportedOperationException -> 0x00c0 }
            r2.close()     // Catch:{ UnsupportedOperationException -> 0x00c0 }
            r2 = 0
            r9.mCursor = r2     // Catch:{ UnsupportedOperationException -> 0x00c0 }
        L_0x004a:
            r9.mFileToPlay = r10     // Catch:{ all -> 0x0094 }
            net.cross.micplayer.service.PlaybackService$MultiPlayer r2 = r9.mPlayer     // Catch:{ all -> 0x0094 }
            java.lang.String r5 = r9.mFileToPlay     // Catch:{ all -> 0x0094 }
            r2.setDataSource(r5)     // Catch:{ all -> 0x0094 }
            r9.mOneShot = r11     // Catch:{ all -> 0x0094 }
            net.cross.micplayer.service.PlaybackService$MultiPlayer r2 = r9.mPlayer     // Catch:{ all -> 0x0094 }
            boolean r2 = r2.isInitialized()     // Catch:{ all -> 0x0094 }
            if (r2 != 0) goto L_0x00c2
            r2 = 1
            r9.stop(r2)     // Catch:{ all -> 0x0094 }
            int r2 = r9.mOpenFailedCounter     // Catch:{ all -> 0x0094 }
            int r5 = r2 + 1
            r9.mOpenFailedCounter = r5     // Catch:{ all -> 0x0094 }
            r5 = 10
            if (r2 >= r5) goto L_0x0073
            int r2 = r9.mPlayListLen     // Catch:{ all -> 0x0094 }
            if (r2 <= r8) goto L_0x0073
            r2 = 0
            r9.next(r2)     // Catch:{ all -> 0x0094 }
        L_0x0073:
            net.cross.micplayer.service.PlaybackService$MultiPlayer r2 = r9.mPlayer     // Catch:{ all -> 0x0094 }
            boolean r2 = r2.isInitialized()     // Catch:{ all -> 0x0094 }
            if (r2 != 0) goto L_0x0091
            int r2 = r9.mOpenFailedCounter     // Catch:{ all -> 0x0094 }
            if (r2 == 0) goto L_0x0091
            r2 = 0
            r9.mOpenFailedCounter = r2     // Catch:{ all -> 0x0094 }
            boolean r2 = r9.mQuietMode     // Catch:{ all -> 0x0094 }
            if (r2 != 0) goto L_0x0091
            r2 = 2131165218(0x7f070022, float:1.7944647E38)
            r5 = 0
            android.widget.Toast r2 = android.widget.Toast.makeText(r9, r2, r5)     // Catch:{ all -> 0x0094 }
            r2.show()     // Catch:{ all -> 0x0094 }
        L_0x0091:
            monitor-exit(r9)     // Catch:{ all -> 0x0094 }
            goto L_0x0005
        L_0x0094:
            r2 = move-exception
            monitor-exit(r9)     // Catch:{ all -> 0x0094 }
            throw r2
        L_0x0097:
            android.net.Uri r1 = android.provider.MediaStore.Audio.Media.getContentUriForPath(r10)     // Catch:{ all -> 0x0094 }
            java.lang.String r3 = "_data=?"
            r2 = 1
            java.lang.String[] r4 = new java.lang.String[r2]     // Catch:{ all -> 0x0094 }
            r2 = 0
            r4[r2] = r10     // Catch:{ all -> 0x0094 }
            goto L_0x002d
        L_0x00a4:
            android.database.Cursor r2 = r9.mCursor     // Catch:{ UnsupportedOperationException -> 0x00c0 }
            r2.moveToNext()     // Catch:{ UnsupportedOperationException -> 0x00c0 }
            r2 = 1
            r9.ensurePlayListCapacity(r2)     // Catch:{ UnsupportedOperationException -> 0x00c0 }
            r2 = 1
            r9.mPlayListLen = r2     // Catch:{ UnsupportedOperationException -> 0x00c0 }
            int[] r2 = r9.mPlayList     // Catch:{ UnsupportedOperationException -> 0x00c0 }
            r5 = 0
            android.database.Cursor r6 = r9.mCursor     // Catch:{ UnsupportedOperationException -> 0x00c0 }
            r7 = 0
            int r6 = r6.getInt(r7)     // Catch:{ UnsupportedOperationException -> 0x00c0 }
            r2[r5] = r6     // Catch:{ UnsupportedOperationException -> 0x00c0 }
            r2 = 0
            r9.mPlayPos = r2     // Catch:{ UnsupportedOperationException -> 0x00c0 }
            goto L_0x004a
        L_0x00c0:
            r2 = move-exception
            goto L_0x004a
        L_0x00c2:
            r2 = 0
            r9.mOpenFailedCounter = r2     // Catch:{ all -> 0x0094 }
            goto L_0x0091
        */
        throw new UnsupportedOperationException("Method not decompiled: net.cross.micplayer.service.PlaybackService.open(java.lang.String, boolean):void");
    }

    public void open(long[] list, int position) {
        synchronized (this) {
            if (this.mShuffleMode == 2) {
                this.mShuffleMode = 1;
            }
            long oldId = getAudioId();
            int listlength = list.length;
            boolean newlist = true;
            if (this.mPlayListLen == listlength) {
                newlist = false;
                int i = 0;
                while (true) {
                    if (i >= listlength) {
                        break;
                    } else if (list[i] != ((long) this.mPlayList[i])) {
                        newlist = true;
                        break;
                    } else {
                        i++;
                    }
                }
            }
            if (newlist) {
                addToPlayList(list, -1);
                notifyChange(QUEUE_CHANGED);
            }
            int i2 = this.mPlayPos;
            if (position >= 0) {
                this.mPlayPos = position;
            } else {
                this.mPlayPos = this.mRand.nextInt(this.mPlayListLen);
            }
            this.mHistory.clear();
            saveBookmarkIfNeeded();
            openCurrent();
            if (oldId != getAudioId()) {
                notifyChange(META_CHANGED);
            }
        }
    }

    public void play() {
        if (this.mPlayer.isInitialized()) {
            long duration = this.mPlayer.duration();
            if (this.mRepeatMode != 1 && duration > 2000 && this.mPlayer.position() >= duration - 2000) {
                next(true);
            }
            this.mPlayer.start();
            setForeground(true);
            NotificationManager nm = (NotificationManager) getSystemService("notification");
            RemoteViews views = new RemoteViews(getPackageName(), (int) R.layout.statusbar);
            views.setImageViewResource(R.id.icon, R.drawable.stat_notify_musicplayer);
            String artist = getArtistName();
            views.setTextViewText(R.id.trackname, getTrackName());
            if (artist.length() <= 0) {
                artist = getString(R.string.UNKNOWN_ARTIST);
            }
            String album = getAlbumName();
            if (album.length() <= 0) {
                album = getString(R.string.UNKNOWN_ALBUM);
            }
            views.setTextViewText(R.id.artistalbum, getString(R.string.notification_artist_album, new Object[]{artist, album}));
            Notification status = new Notification();
            status.contentView = views;
            status.flags |= 2;
            status.icon = R.drawable.stat_notify_musicplayer;
            Intent intent = StreamStarterActivity.makeIntent(this, getSongURL(), getTrackName(), artist, album, getLyricURL(), true);
            intent.addFlags(268435456);
            status.contentIntent = PendingIntent.getActivity(this, 0, intent, 134217728);
            nm.notify(100, status);
            setForeground(true);
            Log.e("playbackservice", "make intent with " + getLyricURL());
            if (!this.mIsSupposedToBePlaying) {
                this.mIsSupposedToBePlaying = true;
                notifyChange(PLAYSTATE_CHANGED);
            }
        } else if (this.mPlayListLen <= 0) {
            setShuffleMode(2);
        }
    }

    public void pause() {
        synchronized (this) {
            if (isPlaying()) {
                this.mPlayer.pause();
                gotoIdleState();
                setForeground(false);
                this.mIsSupposedToBePlaying = false;
                notifyChange(PLAYSTATE_CHANGED);
                saveBookmarkIfNeeded();
            }
        }
    }

    public void stop(boolean remove_status_icon) {
        if (this.mPlayer.isInitialized()) {
            this.mPlayer.stop();
        }
        this.mFileToPlay = null;
        if (this.mCursor != null) {
            this.mCursor.close();
            this.mCursor = null;
        }
        if (remove_status_icon) {
            gotoIdleState();
        }
        setForeground(false);
        if (remove_status_icon) {
            this.mIsSupposedToBePlaying = false;
        }
    }

    public void stop() {
        stop(true);
    }

    public void prev() {
        synchronized (this) {
            if (this.mOneShot) {
                seek(0);
                play();
                return;
            }
            if (this.mShuffleMode == 1) {
                int histsize = this.mHistory.size();
                if (histsize != 0) {
                    this.mPlayPos = this.mHistory.remove(histsize - 1).intValue();
                } else {
                    return;
                }
            } else if (this.mPlayPos > 0) {
                this.mPlayPos--;
            } else {
                this.mPlayPos = this.mPlayListLen - 1;
            }
            saveBookmarkIfNeeded();
            stop(false);
            openCurrent();
            play();
            notifyChange(META_CHANGED);
        }
    }

    public void next(boolean force) {
        synchronized (this) {
            if (this.mOneShot) {
                seek(0);
                play();
            } else if (this.mPlayListLen > 0) {
                if (this.mPlayPos >= 0) {
                    this.mHistory.add(Integer.valueOf(this.mPlayPos));
                }
                if (this.mHistory.size() > 10) {
                    this.mHistory.removeElementAt(0);
                }
                if (this.mShuffleMode == 1) {
                    int numTracks = this.mPlayListLen;
                    int[] tracks = new int[numTracks];
                    for (int i = 0; i < numTracks; i++) {
                        tracks[i] = i;
                    }
                    int numHistory = this.mHistory.size();
                    int numUnplayed = numTracks;
                    for (int i2 = 0; i2 < numHistory; i2++) {
                        int idx = this.mHistory.get(i2).intValue();
                        if (idx < numTracks && tracks[idx] >= 0) {
                            numUnplayed--;
                            tracks[idx] = -1;
                        }
                    }
                    if (numUnplayed <= 0) {
                        if (this.mRepeatMode == 2 || force) {
                            numUnplayed = numTracks;
                            for (int i3 = 0; i3 < numTracks; i3++) {
                                tracks[i3] = i3;
                            }
                        } else {
                            gotoIdleState();
                            return;
                        }
                    }
                    int skip = this.mRand.nextInt(numUnplayed);
                    int cnt = -1;
                    while (true) {
                        cnt++;
                        if (tracks[cnt] >= 0 && skip - 1 < 0) {
                            break;
                        }
                    }
                    this.mPlayPos = cnt;
                } else if (this.mShuffleMode == 2) {
                    doAutoShuffleUpdate();
                    this.mPlayPos++;
                } else if (this.mPlayPos < this.mPlayListLen - 1) {
                    this.mPlayPos++;
                } else if (this.mRepeatMode == 0 && !force) {
                    gotoIdleState();
                    notifyChange(PLAYBACK_COMPLETE);
                    this.mIsSupposedToBePlaying = false;
                    return;
                } else if (this.mRepeatMode == 2 || force) {
                    this.mPlayPos = 0;
                }
                saveBookmarkIfNeeded();
                stop(false);
                openCurrent();
                play();
                notifyChange(META_CHANGED);
            }
        }
    }

    public boolean isPlaying() {
        return this.mIsSupposedToBePlaying;
    }

    public long duration() {
        if (this.mPlayer.isInitialized()) {
            return this.mPlayer.duration();
        }
        return -1;
    }

    public long position() {
        if (this.mPlayer.isInitialized()) {
            return this.mPlayer.position();
        }
        return -1;
    }

    public long seek(long pos) {
        if (!this.mPlayer.isInitialized()) {
            return -1;
        }
        if (pos < 0) {
            pos = 0;
        }
        if (pos > this.mPlayer.duration()) {
            pos = this.mPlayer.duration();
        }
        return this.mPlayer.seek(pos);
    }

    public void setShuffleMode(int shufflemode) {
        synchronized (this) {
            if (this.mShuffleMode != shufflemode || this.mPlayListLen <= 0) {
                this.mShuffleMode = shufflemode;
                if (this.mShuffleMode == 2) {
                    if (makeAutoShuffleList()) {
                        this.mPlayListLen = 0;
                        doAutoShuffleUpdate();
                        this.mPlayPos = 0;
                        openCurrent();
                        play();
                        notifyChange(META_CHANGED);
                        return;
                    }
                    this.mShuffleMode = 0;
                }
                saveQueue(false);
            }
        }
    }

    public int getShuffleMode() {
        return this.mShuffleMode;
    }

    public void setRepeatMode(int repeatmode) {
        synchronized (this) {
            this.mRepeatMode = repeatmode;
            saveQueue(false);
        }
    }

    public int getRepeatMode() {
        return this.mRepeatMode;
    }

    public int getMediaMountedCount() {
        return this.mMediaMountedCount;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        return -1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long getAudioId() {
        /*
            r2 = this;
            monitor-enter(r2)
            int r0 = r2.mPlayPos     // Catch:{ all -> 0x001a }
            if (r0 < 0) goto L_0x0016
            net.cross.micplayer.service.PlaybackService$MultiPlayer r0 = r2.mPlayer     // Catch:{ all -> 0x001a }
            boolean r0 = r0.isInitialized()     // Catch:{ all -> 0x001a }
            if (r0 == 0) goto L_0x0016
            int[] r0 = r2.mPlayList     // Catch:{ all -> 0x001a }
            int r1 = r2.mPlayPos     // Catch:{ all -> 0x001a }
            r0 = r0[r1]     // Catch:{ all -> 0x001a }
            long r0 = (long) r0     // Catch:{ all -> 0x001a }
            monitor-exit(r2)     // Catch:{ all -> 0x001a }
        L_0x0015:
            return r0
        L_0x0016:
            monitor-exit(r2)     // Catch:{ all -> 0x001a }
            r0 = -1
            goto L_0x0015
        L_0x001a:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x001a }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: net.cross.micplayer.service.PlaybackService.getAudioId():long");
    }

    private class MultiPlayer {
        MediaPlayer.OnErrorListener errorListener = new MediaPlayer.OnErrorListener() {
            public boolean onError(MediaPlayer mp, int what, int extra) {
                switch (what) {
                    case PlaybackService.PLAYBACKSERVICE_STATUS /*100*/:
                        MultiPlayer.this.mIsInitialized = false;
                        MultiPlayer.this.mMediaPlayer.release();
                        MultiPlayer.this.mMediaPlayer = new MediaPlayer();
                        MultiPlayer.this.mMediaPlayer.setWakeMode(PlaybackService.this, 1);
                        MultiPlayer.this.mHandler.sendMessageDelayed(MultiPlayer.this.mHandler.obtainMessage(3), 2000);
                        return true;
                    default:
                        Log.d("MultiPlayer", "Error: " + what + "," + extra);
                        return false;
                }
            }
        };
        MediaPlayer.OnCompletionListener listener = new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                PlaybackService.this.mWakeLock.acquire(30000);
                MultiPlayer.this.mHandler.sendEmptyMessage(1);
                MultiPlayer.this.mHandler.sendEmptyMessage(2);
            }
        };
        /* access modifiers changed from: private */
        public Handler mHandler;
        /* access modifiers changed from: private */
        public boolean mIsInitialized = false;
        /* access modifiers changed from: private */
        public MediaPlayer mMediaPlayer = new MediaPlayer();
        MediaPlayer.OnPreparedListener preparedlistener = new MediaPlayer.OnPreparedListener() {
            public void onPrepared(MediaPlayer mp) {
                PlaybackService.this.notifyChange(PlaybackService.ASYNC_OPEN_COMPLETE);
            }
        };

        public MultiPlayer() {
            this.mMediaPlayer.setWakeMode(PlaybackService.this, 1);
        }

        public void setDataSourceAsync(String path) {
            try {
                this.mMediaPlayer.reset();
                this.mMediaPlayer.setDataSource(path);
                this.mMediaPlayer.setAudioStreamType(3);
                this.mMediaPlayer.setOnPreparedListener(this.preparedlistener);
                this.mMediaPlayer.prepareAsync();
                this.mMediaPlayer.setOnCompletionListener(this.listener);
                this.mMediaPlayer.setOnErrorListener(this.errorListener);
                this.mIsInitialized = true;
            } catch (IOException e) {
                this.mIsInitialized = false;
            } catch (IllegalArgumentException e2) {
                this.mIsInitialized = false;
            }
        }

        public void setDataSource(String path) {
            try {
                this.mMediaPlayer.reset();
                this.mMediaPlayer.setOnPreparedListener(null);
                if (path.startsWith("content://")) {
                    this.mMediaPlayer.setDataSource(PlaybackService.this, Uri.parse(path));
                } else {
                    this.mMediaPlayer.setDataSource(path);
                }
                this.mMediaPlayer.setAudioStreamType(3);
                this.mMediaPlayer.prepare();
                this.mMediaPlayer.setOnCompletionListener(this.listener);
                this.mMediaPlayer.setOnErrorListener(this.errorListener);
                this.mIsInitialized = true;
            } catch (IOException e) {
                this.mIsInitialized = false;
            } catch (IllegalArgumentException e2) {
                this.mIsInitialized = false;
            }
        }

        public boolean isInitialized() {
            return this.mIsInitialized;
        }

        public void start() {
            this.mMediaPlayer.start();
        }

        public void stop() {
            this.mMediaPlayer.reset();
            this.mIsInitialized = false;
        }

        public void release() {
            stop();
            this.mMediaPlayer.release();
        }

        public void pause() {
            this.mMediaPlayer.pause();
        }

        public void setHandler(Handler handler) {
            this.mHandler = handler;
        }

        public long duration() {
            return (long) this.mMediaPlayer.getDuration();
        }

        public long position() {
            return (long) this.mMediaPlayer.getCurrentPosition();
        }

        public long seek(long whereto) {
            this.mMediaPlayer.seekTo((int) whereto);
            return whereto;
        }

        public void setVolume(float vol) {
            this.mMediaPlayer.setVolume(vol, vol);
        }
    }

    /* access modifiers changed from: private */
    public void notifyChange(String what) {
        sendBroadcast(new Intent(what));
    }

    private void gotoIdleState() {
        ((NotificationManager) getSystemService("notification")).cancel(100);
        this.mDelayedStopHandler.removeCallbacksAndMessages(null);
        this.mDelayedStopHandler.sendMessageDelayed(this.mDelayedStopHandler.obtainMessage(), Constants.MusicLengthLimit);
        setForeground(false);
    }

    public String getArtistName() {
        String string;
        synchronized (this) {
            if (this.mCursor == null) {
                string = this.mArtist;
            } else {
                string = this.mCursor.getString(this.mCursor.getColumnIndexOrThrow("artist"));
            }
        }
        return string;
    }

    public long getArtistId() {
        synchronized (this) {
            if (this.mCursor == null) {
                return -1;
            }
            long j = this.mCursor.getLong(this.mCursor.getColumnIndexOrThrow("artist_id"));
            return j;
        }
    }

    public String getAlbumName() {
        String string;
        synchronized (this) {
            if (this.mCursor == null) {
                string = this.mAlbum;
            } else {
                string = this.mCursor.getString(this.mCursor.getColumnIndexOrThrow("album"));
            }
        }
        return string;
    }

    public long getAlbumId() {
        synchronized (this) {
            if (this.mCursor == null) {
                return -1;
            }
            long j = this.mCursor.getLong(this.mCursor.getColumnIndexOrThrow("album_id"));
            return j;
        }
    }

    public String getTrackName() {
        String string;
        synchronized (this) {
            if (this.mCursor == null) {
                string = this.mTrackName;
            } else {
                string = this.mCursor.getString(this.mCursor.getColumnIndexOrThrow("title"));
            }
        }
        return string;
    }

    public String getFileName() {
        String string;
        synchronized (this) {
            if (this.mCursor == null) {
                string = this.mFileToPlay;
            } else {
                string = this.mCursor.getString(this.mCursor.getColumnIndexOrThrow("_data"));
            }
        }
        return string;
    }

    public String getSongURL() {
        return this.mSongURL;
    }

    public String getLyricURL() {
        if (this.mCursor == null) {
            return this.mLyricURL;
        }
        Cursor cursor = getMyDownloadsCursor(null);
        if (cursor == null || cursor.getCount() <= 0) {
            return Utils.EMPTY_STRING;
        }
        cursor.moveToFirst();
        return cursor.getString(cursor.getColumnIndex("lyric"));
    }

    private Cursor getMyDownloadsCursor(AsyncQueryHandler async) {
        String[] cols = {"_id", "_data", "title", "artist", "lyric", "album", "lastmod"};
        StringBuilder where = new StringBuilder();
        if (getFileName() != null) {
            where.append("_data=" + DatabaseUtils.sqlEscapeString(getFileName()));
        } else {
            where.append(Utils.EMPTY_STRING);
        }
        if (async != null) {
            async.startQuery(0, null, Downloaded.CONTENT_URI, cols, where.toString(), null, "lastmod DESC");
            return null;
        }
        try {
            ContentResolver resolver = getContentResolver();
            if (resolver != null) {
                return resolver.query(Downloaded.CONTENT_URI, cols, where.toString(), null, "lastmod DESC");
            }
            return null;
        } catch (UnsupportedOperationException e) {
            e.printStackTrace();
            return null;
        }
    }
}
