package net.cross.micplayer.service;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;
import java.util.Arrays;
import java.util.Formatter;
import java.util.HashMap;
import java.util.Locale;
import net.cross.micplayer.R;
import net.cross.micplayer.service.PlaybackService;

public class PlaybackServiceHelper {
    public static PlaybackService mService = null;
    private static HashMap<Context, ServiceBinder> sConnectionMap = new HashMap<>();
    private static StringBuilder sFormatBuilder = new StringBuilder();
    private static Formatter sFormatter = new Formatter(sFormatBuilder, Locale.getDefault());
    private static final Object[] sTimeArgs = new Object[5];

    public static boolean bindToService(Context context) {
        return bindToService(context, null);
    }

    public static boolean bindToService(Context context, ServiceConnection callback) {
        context.startService(new Intent(context, PlaybackService.class));
        ServiceBinder sb = new ServiceBinder(callback);
        sConnectionMap.put(context, sb);
        return context.bindService(new Intent().setClass(context, PlaybackService.class), sb, 0);
    }

    public static void unbindFromService(Context context) {
        ServiceBinder sb = sConnectionMap.remove(context);
        if (sb != null) {
            context.unbindService(sb);
            if (sConnectionMap.isEmpty()) {
                mService = null;
            }
        }
    }

    private static class ServiceBinder implements ServiceConnection {
        ServiceConnection mCallback;

        ServiceBinder(ServiceConnection callback) {
            this.mCallback = callback;
        }

        public void onServiceConnected(ComponentName className, IBinder service) {
            PlaybackServiceHelper.mService = ((PlaybackService.LocalBinder) service).getService();
            if (this.mCallback != null) {
                this.mCallback.onServiceConnected(className, service);
            }
        }

        public void onServiceDisconnected(ComponentName className) {
            if (this.mCallback != null) {
                this.mCallback.onServiceDisconnected(className);
            }
            PlaybackServiceHelper.mService = null;
        }
    }

    public static void playAll(Context context, long[] list, int position, boolean force_shuffle) {
        int i;
        Log.e("MusicUtils", "start playing");
        if (list.length == 0 || mService == null) {
            if (mService == null) {
                Log.e("PlaybackServiceHelper", "service is null");
            }
            if (list.length == 0) {
                Log.e("PlaybackServiceHelper", "attempt to play empty song list");
            }
            Toast.makeText(context, context.getString(R.string.emptyplaylist, Integer.valueOf(list.length)), 0).show();
            return;
        }
        if (force_shuffle) {
            mService.setShuffleMode(1);
        }
        long curid = mService.getAudioId();
        int curpos = mService.getQueuePosition();
        if (position == -1 || curpos != position || curid != list[position] || !Arrays.equals(list, mService.getQueue())) {
            if (position < 0) {
                position = 0;
            }
            PlaybackService playbackService = mService;
            if (force_shuffle) {
                i = -1;
            } else {
                i = position;
            }
            playbackService.open(list, i);
            mService.play();
            return;
        }
        mService.play();
    }

    public static String makeTimeString(Context context, long secs) {
        String durationformat = context.getString(secs < 3600 ? R.string.durationformatshort : R.string.durationformatlong);
        sFormatBuilder.setLength(0);
        Object[] timeArgs = sTimeArgs;
        timeArgs[0] = Long.valueOf(secs / 3600);
        timeArgs[1] = Long.valueOf(secs / 60);
        timeArgs[2] = Long.valueOf((secs / 60) % 60);
        timeArgs[3] = Long.valueOf(secs);
        timeArgs[4] = Long.valueOf(secs % 60);
        return sFormatter.format(durationformat, timeArgs).toString();
    }
}
