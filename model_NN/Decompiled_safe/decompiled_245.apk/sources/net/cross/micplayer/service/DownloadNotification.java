package net.cross.micplayer.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.widget.RemoteViews;
import com.qwapi.adclient.android.utils.Utils;
import java.util.HashMap;
import net.cross.micplayer.R;
import net.cross.micplayer.data.DownloadQueue;
import net.cross.micplayer.utils.Constants;

public class DownloadNotification {
    static final String LOGTAG = "DownloadNotification";
    static final String WHERE_COMPLETED = "(status >= '200')AND(visibility<>1)";
    static final String WHERE_RUNNING = "((status=191) OR (status=190))AND(control=0)";
    Context mContext;
    public NotificationManager mNotificationMgr = ((NotificationManager) this.mContext.getSystemService("notification"));
    HashMap<String, NotificationItem> mNotifications = new HashMap<>();

    static class NotificationItem {
        String mDescription;
        int mId;
        String mTitle = Utils.EMPTY_STRING;
        int mTotalCurrent = 0;
        int mTotalTotal = 0;

        NotificationItem() {
        }

        /* access modifiers changed from: package-private */
        public void addItem(String title, int currentBytes, int totalBytes) {
            this.mTotalCurrent += currentBytes;
            if (totalBytes <= 0 || this.mTotalTotal == -1) {
                this.mTotalTotal = -1;
            } else {
                this.mTotalTotal += totalBytes;
            }
            this.mTitle = title;
        }
    }

    DownloadNotification(Context ctx) {
        this.mContext = ctx;
    }

    public void updateNotification() {
        updateActiveNotification();
        updateCompletedNotification();
    }

    private void updateActiveNotification() {
        Cursor c = this.mContext.getContentResolver().query(DownloadQueue.CONTENT_URI, new String[]{"_id", "title", "artist", DownloadQueue.COL_CURRENT_BYTES, DownloadQueue.COL_TOTAL_BYTES}, WHERE_RUNNING, null, "_id");
        if (c != null) {
            this.mNotifications.clear();
            c.moveToFirst();
            while (!c.isAfterLast()) {
                int dataID = c.getInt(0);
                int max = c.getInt(4);
                int progress = c.getInt(3);
                String title = c.getString(1);
                if (title == null || title.length() == 0) {
                    title = "Unknown";
                }
                if (this.mNotifications.containsKey(Integer.valueOf(dataID))) {
                    this.mNotifications.get(Integer.toString(dataID)).addItem(title, progress, max);
                } else {
                    NotificationItem item = new NotificationItem();
                    item.mId = dataID;
                    item.mDescription = c.getString(2);
                    item.addItem(title, progress, max);
                    this.mNotifications.put(Integer.toString(dataID), item);
                }
                c.moveToNext();
            }
            c.close();
            for (NotificationItem item2 : this.mNotifications.values()) {
                Notification n = new Notification();
                n.icon = 17301633;
                n.flags |= 2;
                RemoteViews expandedView = new RemoteViews(this.mContext.getPackageName(), (int) R.layout.notify_downloading);
                expandedView.setTextViewText(R.id.description, item2.mDescription);
                expandedView.setTextViewText(R.id.title, item2.mTitle);
                expandedView.setProgressBar(R.id.progress_bar, item2.mTotalTotal, item2.mTotalCurrent, item2.mTotalTotal == -1);
                expandedView.setTextViewText(R.id.progress_text, getDownloadingText((long) item2.mTotalTotal, (long) item2.mTotalCurrent));
                expandedView.setImageViewResource(R.id.appIcon, 17301633);
                n.contentView = expandedView;
                Intent intent = new Intent(Constants.ACTION_LIST);
                intent.setClassName(this.mContext.getPackageName(), DownloadReceiver.class.getName());
                intent.setData(Uri.parse(DownloadQueue.CONTENT_URI + "/" + item2.mId));
                n.contentIntent = PendingIntent.getBroadcast(this.mContext, 0, intent, 0);
                this.mNotificationMgr.notify(item2.mId, n);
            }
        }
    }

    private void updateCompletedNotification() {
        String caption;
        Intent intent;
        Cursor c = this.mContext.getContentResolver().query(DownloadQueue.CONTENT_URI, new String[]{"_id", "title", DownloadQueue.COL_CURRENT_BYTES, DownloadQueue.COL_TOTAL_BYTES, DownloadQueue.COL_STATUS, "_data", "lastmod"}, WHERE_COMPLETED, null, "_id");
        if (c != null) {
            c.moveToFirst();
            while (!c.isAfterLast()) {
                Notification n = new Notification();
                n.flags |= 16;
                String title = c.getString(1);
                if (title == null || title.length() == 0) {
                    title = "Unknown";
                }
                Uri contentUri = Uri.parse(DownloadQueue.CONTENT_URI + "/" + c.getInt(0));
                if (DownloadQueue.isStatusError(c.getInt(4))) {
                    n.icon = 17301624;
                    caption = this.mContext.getResources().getString(R.string.notification_download_failed);
                    intent = new Intent(Constants.ACTION_LIST);
                    intent.setFlags(67108864);
                } else {
                    n.icon = 17301634;
                    caption = this.mContext.getResources().getString(R.string.notification_download_complete);
                    intent = new Intent(Constants.ACTION_OPEN);
                }
                intent.setClassName(this.mContext.getPackageName(), DownloadReceiver.class.getName());
                intent.setData(contentUri);
                n.setLatestEventInfo(this.mContext, title, caption, PendingIntent.getBroadcast(this.mContext, 0, intent, 0));
                n.when = c.getLong(6);
                this.mNotificationMgr.notify(c.getInt(0), n);
                ContentValues values = new ContentValues();
                values.put(DownloadQueue.COL_VISIBILITY, (Integer) 1);
                this.mContext.getContentResolver().update(intent.getData(), values, "_id==" + c.getInt(0), null);
                c.moveToNext();
            }
            c.close();
        }
    }

    private String getDownloadingText(long totalBytes, long currentBytes) {
        if (totalBytes <= 0) {
            return Utils.EMPTY_STRING;
        }
        StringBuilder sb = new StringBuilder();
        sb.append((100 * currentBytes) / totalBytes);
        sb.append('%');
        return sb.toString();
    }
}
