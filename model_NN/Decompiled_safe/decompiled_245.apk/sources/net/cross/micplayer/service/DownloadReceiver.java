package net.cross.micplayer.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import java.io.File;
import net.cross.micplayer.DownloadActivity;
import net.cross.micplayer.R;
import net.cross.micplayer.data.DownloadQueue;
import net.cross.micplayer.data.Downloaded;
import net.cross.micplayer.utils.Constants;

public class DownloadReceiver extends BroadcastReceiver {
    /* JADX INFO: finally extract failed */
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(Constants.ACTION_RETRY)) {
            context.startService(new Intent(context, DownloadService.class));
        } else if (intent.getAction().equals(Constants.ACTION_OPEN) || intent.getAction().equals(Constants.ACTION_LIST)) {
            Cursor cursor = context.getContentResolver().query(intent.getData(), null, null, null, null);
            if (cursor != null) {
                try {
                    if (cursor.moveToFirst()) {
                        if (intent.getAction().equals(Constants.ACTION_OPEN)) {
                            int filenameColumn = cursor.getColumnIndexOrThrow("_data");
                            int mimetypeColumn = cursor.getColumnIndexOrThrow("mimetype");
                            String filename = cursor.getString(filenameColumn);
                            String string = cursor.getString(cursor.getColumnIndexOrThrow("title"));
                            String string2 = cursor.getString(cursor.getColumnIndexOrThrow("album"));
                            String string3 = cursor.getString(cursor.getColumnIndexOrThrow("artist"));
                            String string4 = cursor.getString(cursor.getColumnIndexOrThrow("lyric"));
                            String mimetype = cursor.getString(mimetypeColumn);
                            Uri path = Uri.parse(filename);
                            if (path.getScheme() == null) {
                                path = Uri.fromFile(new File(filename));
                            }
                            Intent activityIntent = new Intent("android.intent.action.VIEW");
                            activityIntent.setDataAndType(path, mimetype);
                            activityIntent.setFlags(268435456);
                            try {
                                context.startActivity(activityIntent);
                            } catch (ActivityNotFoundException e) {
                            }
                        } else {
                            Intent activityIntent2 = new Intent("android.intent.action.VIEW");
                            activityIntent2.setFlags(335544320);
                            activityIntent2.setClassName(DownloadActivity.class.getPackage().getName(), DownloadActivity.class.getName());
                            context.startActivity(activityIntent2);
                        }
                    }
                } finally {
                    cursor.close();
                }
            }
        } else if (intent.getAction().equals(Constants.ACTION_MOVE)) {
            NotificationManager notificationMgr = (NotificationManager) context.getSystemService("notification");
            Cursor cursor2 = context.getContentResolver().query(intent.getData(), null, "status=200", null, null);
            if (cursor2 != null) {
                try {
                    cursor2.moveToFirst();
                    while (!cursor2.isAfterLast()) {
                        ContentValues values = new ContentValues();
                        String uri = cursor2.getString(cursor2.getColumnIndex("uri"));
                        values.put("uri", uri);
                        values.put("_data", cursor2.getString(cursor2.getColumnIndex("_data")));
                        values.put("mimetype", cursor2.getString(cursor2.getColumnIndex("mimetype")));
                        values.put("lastmod", Long.valueOf(cursor2.getLong(cursor2.getColumnIndex("lastmod"))));
                        values.put("artist", cursor2.getString(cursor2.getColumnIndex("artist")));
                        values.put("album", cursor2.getString(cursor2.getColumnIndex("album")));
                        values.put("lyric", cursor2.getString(cursor2.getColumnIndex("lyric")));
                        values.put("title", cursor2.getString(cursor2.getColumnIndex("title")));
                        Cursor c2 = context.getContentResolver().query(Downloaded.CONTENT_URI, null, "uri='" + uri + "'", null, null);
                        if (c2 != null) {
                            c2.moveToFirst();
                            if (c2.isAfterLast()) {
                                c2.close();
                                context.getContentResolver().insert(Downloaded.CONTENT_URI, values);
                            } else {
                                c2.close();
                                context.getContentResolver().update(Downloaded.CONTENT_URI, values, "uri='" + uri + "'", null);
                            }
                            context.getContentResolver().delete(ContentUris.withAppendedId(DownloadQueue.CONTENT_URI, cursor2.getLong(cursor2.getColumnIndexOrThrow("_id"))), null, null);
                            if (notificationMgr != null) {
                                Notification n = new Notification();
                                n.icon = 17301634;
                                n.flags |= 16;
                                String string5 = context.getResources().getString(R.string.notification_download_complete);
                                String string6 = cursor2.getString(cursor2.getColumnIndexOrThrow("_data"));
                                String string7 = cursor2.getString(cursor2.getColumnIndexOrThrow("title"));
                                String string8 = cursor2.getString(cursor2.getColumnIndexOrThrow("album"));
                                String string9 = cursor2.getString(cursor2.getColumnIndexOrThrow("artist"));
                                cursor2.getString(cursor2.getColumnIndexOrThrow("lyric"));
                            }
                        }
                        cursor2.moveToNext();
                    }
                    cursor2.close();
                } catch (Throwable th) {
                    cursor2.close();
                    throw th;
                }
            }
        }
    }
}
