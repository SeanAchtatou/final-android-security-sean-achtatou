package net.cross.micplayer;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.adwhirl.AdWhirlLayout;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import com.qwapi.adclient.android.data.Ad;
import com.qwapi.adclient.android.data.Status;
import com.qwapi.adclient.android.requestparams.AdRequestParams;
import com.qwapi.adclient.android.requestparams.AnimationType;
import com.qwapi.adclient.android.requestparams.DisplayMode;
import com.qwapi.adclient.android.requestparams.MediaType;
import com.qwapi.adclient.android.requestparams.Placement;
import com.qwapi.adclient.android.view.AdEventsListener;
import com.qwapi.adclient.android.view.QWAdView;
import java.util.Random;

public class AdsView {
    public static final String KW_MOSTVALUE = "music, free music, music news, music reviews, music articles, music information, music news online";
    private static final boolean blackscreen = isBlackScreen();
    public static boolean mShowQuattroAd = true;

    private static boolean isBlackScreen() {
        return Build.VERSION.SDK.equalsIgnoreCase("3");
    }

    public static void createAdMobAd(Activity activity, String keywords) {
        AdView adView = new AdView(activity, AdSize.BANNER, "a14c3bb5245b8fe");
        adView.setBackgroundColor(0);
        ((LinearLayout) activity.findViewById(R.id.AdsView)).addView(adView);
        adView.loadAd(new AdRequest());
    }

    public static void createAdWhirl(Activity activity) {
        if (blackscreen) {
        }
        try {
            ((LinearLayout) activity.findViewById(R.id.AdsView)).addView(new AdWhirlLayout(activity, "d3787cffdc1e418bb12abb03382ab1f8"), new RelativeLayout.LayoutParams(-1, -2));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void createQWAd(Activity activity) {
        if (mShowQuattroAd) {
            ViewGroup parentView = (ViewGroup) activity.findViewById(R.id.AdsView);
            int t = new Random().nextInt();
            if (t % 4 == 0) {
                parentView.addView(new QWAdView(activity, "MediaDownload-gaatqtwf", "ccd223dd314845979fbe911596c66f6f", MediaType.banner, Placement.bottom, DisplayMode.autoRotate, 30, AnimationType.slide, new QWAdEventsListener(parentView), true));
            } else if (t % 3 == 0) {
                parentView.addView(new QWAdView(activity, "MusicWizard-gal1grg6", "be50b52d4cba434fbb4bda60552c8ebf", MediaType.banner, Placement.bottom, DisplayMode.autoRotate, 30, AnimationType.slide, new QWAdEventsListener(parentView), true));
            } else {
                parentView.addView(new QWAdView(activity, "RingtoneWizard-gal1jbjt", "148af6ea4a2345a989efb5660d1572a2", MediaType.banner, Placement.bottom, DisplayMode.autoRotate, 30, AnimationType.slide, new QWAdEventsListener(parentView), true));
            }
        }
    }

    private static class QWAdEventsListener implements AdEventsListener {
        private ViewGroup mAdsView = null;

        public QWAdEventsListener(ViewGroup view) {
            this.mAdsView = view;
        }

        public void onAdClick(Context ctx, Ad ad) {
            Log.i("Snake", "onAdClick for Ad: " + ad.getAdType() + " : " + ad.getId());
        }

        public void onAdRequest(Context ctx, AdRequestParams params) {
            Log.i("Snake", "onAdRequest for RequestParams: " + params.toString());
        }

        public void onAdRequestFailed(Context ctx, AdRequestParams params, Status status) {
            Log.i("Snake", "onAdRequestFailed for RequestParams: " + params.toString() + " : " + status);
        }

        public void onAdRequestSuccessful(Context ctx, AdRequestParams params, Ad ad) {
            Log.i("Snake", "onAdRequestSuccessful for RequestParams: " + params.toString() + " : Ad: " + ad.getAdType() + " : " + ad.getId());
        }

        public void onDisplayAd(Context ctx, Ad ad) {
            Log.i("Snake", "onDisplayAd for Ad: " + ad.getAdType() + " : " + ad.getId());
        }
    }
}
