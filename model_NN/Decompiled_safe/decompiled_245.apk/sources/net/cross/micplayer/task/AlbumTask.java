package net.cross.micplayer.task;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import net.cross.micplayer.utils.NetUtils;

public class AlbumTask extends AsyncTask<String, Object, Integer> {
    private Listener mListener = null;

    public interface Listener {
        void AT_OnAlbumArtReady(Bitmap bitmap);

        void AT_OnAlbumSongReady(String str);

        void AT_OnBegin();

        void AT_OnEnd(boolean z);
    }

    public AlbumTask(Listener l) {
        this.mListener = l;
    }

    /* access modifiers changed from: protected */
    public Integer doInBackground(String... reqURLs) {
        String songName;
        String httpResp = NetUtils.loadStringAsPCZH(reqURLs[0]);
        if (httpResp == null) {
            return 0;
        }
        Bitmap albumArt = parseAlbumArt(httpResp);
        if (albumArt == null) {
            return 0;
        }
        publishProgress(albumArt);
        String songListTxt = SearchSongTask.getRangeText(httpResp, "songlist", "</table>");
        if (songListTxt == null) {
            return 0;
        }
        int offset = 0;
        String rowTxt = SearchSongTask.getRangeText(songListTxt, "<tr>", "</tr>");
        while (rowTxt != null) {
            offset += rowTxt.length();
            rowTxt = SearchSongTask.getRangeText(songListTxt, offset, "<tr>", "</tr>");
            if (!(rowTxt == null || (songName = parseAlbumSong(rowTxt)) == null)) {
                publishProgress(songName);
            }
        }
        return 1;
    }

    private static Bitmap parseAlbumArt(String txt) {
        String profileText = SearchSongTask.getRangeText(txt, "photobox", "</a>");
        if (profileText == null) {
            return null;
        }
        String imgURL = SearchSongTask.getRangeText(profileText, "src=\"", "\"");
        if (imgURL == null) {
            return null;
        }
        return NetUtils.loadBitmap(imgURL);
    }

    private static String parseAlbumSong(String txt) {
        int offset = txt.indexOf("</td>");
        if (offset < 0) {
            return null;
        }
        String cellTxt = SearchSongTask.getRangeText(txt.substring(offset), "<td>", "</td>");
        if (cellTxt == null) {
            return null;
        }
        return SearchSongTask.getRangeText(cellTxt, cellTxt.indexOf("hidden"), "s=\"", "\"");
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        if (this.mListener != null) {
            this.mListener.AT_OnBegin();
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    /* access modifiers changed from: protected */
    public void onProgressUpdate(Object... objs) {
        if (this.mListener == null) {
            return;
        }
        if (objs[0].getClass() == Bitmap.class) {
            this.mListener.AT_OnAlbumArtReady((Bitmap) objs[0]);
        } else if (objs[0].getClass() == String.class) {
            this.mListener.AT_OnAlbumSongReady((String) objs[0]);
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Integer result) {
        if (this.mListener != null) {
            this.mListener.AT_OnEnd(result.intValue() != 1);
        }
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        this.mListener = null;
    }
}
