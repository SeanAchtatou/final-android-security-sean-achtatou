package net.cross.micplayer.task;

import android.os.AsyncTask;
import android.util.Log;
import com.qwapi.adclient.android.utils.Utils;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import net.cross.micplayer.utils.NetUtils;
import net.cross.micplayer.utils.SiteHelper;
import net.cross.micplayer.utils.lang.StringEscapeUtils;

public class SearchSongTask extends AsyncTask<String, Object, Integer> {
    public static final String CMD_KEYWORD = "1";
    public static final String CMD_URL = "2";
    private Listener mListener = null;

    public interface Listener {
        void SST_OnBegin();

        void SST_OnDataReady(SearchResult searchResult);

        void SST_OnFinished(boolean z);

        void SST_OnPagerReady(SearchPage searchPage);
    }

    public SearchSongTask(Listener l) {
        this.mListener = l;
    }

    /* access modifiers changed from: protected */
    public Integer doInBackground(String... keywords) {
        String reqURL;
        String artistCell;
        String albumCell;
        String previewCell;
        String lyricCell;
        String cmd = keywords[0];
        if (cmd.equals("1")) {
            reqURL = SiteHelper.getSearchLink(keywords[1]);
            Log.e("Music", "search...1 " + reqURL);
        } else if (!cmd.equals(CMD_URL)) {
            return 0;
        } else {
            reqURL = keywords[1];
            Log.e("Music", "search...2 " + reqURL);
        }
        Log.e("Music", "search...3 ");
        String httpResp = NetUtils.loadStringAsPCZH(reqURL);
        if (httpResp == null) {
            return 0;
        }
        Log.e("Music", "search...4 ");
        String tableBody = getRangeText(httpResp, "<table id=\"songlist\"", "</table>");
        if (tableBody == null) {
            return 0;
        }
        String rowText = getRangeText(tableBody, 0, "<th", "</tr>");
        if (rowText == null) {
            return 0;
        }
        int offset = rowText.length();
        String rowText2 = getRangeText(tableBody, offset, "<tr", "</tr>");
        while (rowText2 != null) {
            SearchResult sr = new SearchResult();
            String numCell = getRangeText(rowText2, 0, "<td", "</td>");
            if (numCell != null) {
                int curPos = 0 + numCell.length() + 8;
                String songCell = getRangeText(rowText2, curPos, "<td", "</td>");
                if (songCell != null) {
                    int curPos2 = curPos + songCell.length() + 8;
                    if (parseSong(songCell, sr) && (artistCell = getRangeText(rowText2, curPos2, "<td", "</td>")) != null) {
                        int curPos3 = curPos2 + artistCell.length() + 8;
                        if (parseArtist(artistCell, sr) && (albumCell = getRangeText(rowText2, curPos3, "<td", "</td>")) != null) {
                            int curPos4 = curPos3 + albumCell.length() + 8;
                            if (parseAlbum(albumCell, sr) && (previewCell = getRangeText(rowText2, curPos4, "<td", "</td>")) != null) {
                                int curPos5 = curPos4 + previewCell.length() + 8;
                                String favCell = getRangeText(rowText2, curPos5, "<td", "</td>");
                                if (favCell != null) {
                                    int curPos6 = curPos5 + favCell.length() + 8;
                                    String connCell = getRangeText(rowText2, curPos6, "<td", "</td>");
                                    if (connCell != null) {
                                        int curPos7 = curPos6 + connCell.length() + 8;
                                        if (parseDown(rowText2, sr) && (lyricCell = getRangeText(rowText2, curPos7, "<td", "</td>")) != null) {
                                            int curPos8 = curPos7 + lyricCell.length() + 8;
                                            parseLyricLink(lyricCell, sr);
                                            String sizeCell2 = getRangeText(rowText2, curPos8, "<td nowrap>", "<!--size");
                                            if (sizeCell2 != null) {
                                                sr.mSize = sizeCell2;
                                                int curPos9 = curPos8 + sizeCell2.length() + 19;
                                                String formatCell = getRangeText(rowText2, curPos9, "<td", "</td>");
                                                if (formatCell != null) {
                                                    int curPos10 = curPos9 + formatCell.length() + 8;
                                                    publishProgress(sr);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            offset += rowText2.length();
            rowText2 = getRangeText(tableBody, offset, "<tr", "</tr>");
        }
        String pagerText = getRangeText(httpResp, "<div class=\"p\"", "</div>");
        SearchPage sp = new SearchPage();
        if (pagerText != null) {
            sp.mNextLink = parseNextPageLink(pagerText);
            sp.mPrevLink = parsePrevPageLink(pagerText);
        }
        publishProgress(sp);
        return 1;
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        if (this.mListener != null) {
            this.mListener.SST_OnBegin();
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    /* access modifiers changed from: protected */
    public void onProgressUpdate(Object... values) {
        if (this.mListener == null) {
            return;
        }
        if (SearchResult.class.isInstance(values[0])) {
            this.mListener.SST_OnDataReady((SearchResult) values[0]);
            Log.e("Music", "search... update 1 ");
        } else if (SearchPage.class.isInstance(values[0])) {
            this.mListener.SST_OnPagerReady((SearchPage) values[0]);
            Log.e("Music", "search... update 2 ");
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Integer result) {
        if (this.mListener != null) {
            this.mListener.SST_OnFinished(result.intValue() != 1);
        }
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        this.mListener = null;
    }

    private static void parseSearchResult(String result) {
    }

    public static String getRangeText(String src, String prefix, String surfix) {
        return getRangeText(src, 0, prefix, surfix);
    }

    public static String getRangeText(String src, int offset, String prefix, String surfix) {
        int startPos = src.indexOf(prefix, offset);
        if (startPos < 0) {
            return null;
        }
        int startPos2 = startPos + prefix.length();
        int endPos = src.indexOf(surfix, startPos2);
        if (endPos < 0) {
            return null;
        }
        return src.substring(startPos2, endPos);
    }

    private static boolean parseSong(String block, SearchResult sr) {
        sr.mSong = getRangeText(block, "title=\"", "\"");
        if (sr.mSong == null) {
            return false;
        }
        sr.mSongURL = getRangeText(block, "onClick=\"o2('", "');");
        if (sr.mSongURL == null) {
            return false;
        }
        try {
            sr.mSongURL = URLDecoder.decode(sr.mSongURL, "GBK");
            sr.mSongURL = "http://ting.mbox.sogou.com/listenV2.jsp?" + sr.mSongURL;
            try {
                sr.mSong = URLDecoder.decode(sr.mSong, "GBK");
                sr.mSong = sr.mSong.replaceAll("\\<.*?>", Utils.EMPTY_STRING);
                sr.mSong = StringEscapeUtils.unescapeHtml(sr.mSong);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            return true;
        } catch (UnsupportedEncodingException e2) {
            return false;
        }
    }

    private static boolean parseArtist(String block, SearchResult sr) {
        sr.mArtist = getRangeText(block, "title=\"", "\"");
        if (sr.mArtist == null) {
            return false;
        }
        try {
            sr.mArtist = URLDecoder.decode(sr.mArtist, "GBK");
            sr.mArtist = sr.mArtist.trim();
            sr.mArtist = sr.mArtist.replaceAll("\\<.*?>", Utils.EMPTY_STRING);
            sr.mArtist = StringEscapeUtils.unescapeHtml(sr.mArtist);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return true;
    }

    private static boolean parseAlbum(String block, SearchResult sr) {
        sr.mAlbum = getRangeText(block, "title=\"", "\"");
        if (sr.mAlbum == null) {
            return false;
        }
        sr.mAlbumURL = getRangeText(block, "href=\"", "\"");
        if (sr.mAlbumURL == null) {
            return false;
        }
        try {
            sr.mAlbum = URLDecoder.decode(sr.mAlbum, "GBK");
            sr.mAlbum = sr.mAlbum.trim();
            sr.mAlbum = sr.mAlbum.replaceAll("\\<.*?>", Utils.EMPTY_STRING);
            sr.mAlbum = StringEscapeUtils.unescapeHtml(sr.mAlbum);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return true;
    }

    private static boolean parseDown(String block, SearchResult sr) {
        sr.mSongURL = getRangeText(block, "window.open('", "','");
        if (sr.mSongURL == null) {
            return false;
        }
        try {
            sr.mSongURL = URLDecoder.decode(sr.mSongURL, "GBK");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        sr.mSongURL = "http://mp3.sogou.com" + sr.mSongURL;
        return true;
    }

    private static boolean parseLyricLink(String block, SearchResult sr) {
        sr.mLyricURL = getRangeText(block, "<a", "</a>");
        if (sr.mLyricURL == null) {
            return true;
        }
        sr.mLyricURL = getRangeText(sr.mLyricURL, "href=\"", "\"");
        if (sr.mLyricURL == null) {
            return true;
        }
        sr.mLyricURL = "http://mp3.sogou.com" + sr.mLyricURL;
        return true;
    }

    private static String parseNextPageLink(String src) {
        int endPos = src.lastIndexOf("music_page_down");
        if (endPos == -1) {
            return null;
        }
        int startPos = src.lastIndexOf("href=", endPos);
        if (startPos == -1) {
            return null;
        }
        int endPos2 = src.indexOf(" ", "href=".length() + startPos);
        if (endPos2 == -1) {
            return null;
        }
        String retval = src.substring("href=".length() + startPos, endPos2);
        if (retval.length() <= 0) {
            return null;
        }
        return "http://mp3.sogou.com/music.so" + retval;
    }

    private static String parsePrevPageLink(String src) {
        int endPos = src.indexOf("上一页");
        if (endPos == -1) {
            return null;
        }
        int startPos = src.lastIndexOf("href=", endPos);
        if (startPos == -1) {
            return null;
        }
        int endPos2 = src.indexOf(">", "href=".length() + startPos);
        if (endPos2 == -1) {
            return null;
        }
        String retval = src.substring("href=".length() + startPos, endPos2);
        if (retval.length() <= 0) {
            return null;
        }
        return "http://mp3.sogou.com/music.so" + retval;
    }
}
