package net.cross.micplayer;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.ContentUris;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.qwapi.adclient.android.utils.Utils;
import java.io.File;
import java.util.Formatter;
import java.util.Locale;
import net.cross.micplayer.editor.RingdroidEditActivity;
import net.cross.micplayer.utils.Constants;
import net.cross.micplayer.utils.Ringtone;
import net.cross.micplayer.utils.Util;

public class SongsActivity extends Activity implements AdapterView.OnItemClickListener, SimpleCursorAdapter.ViewBinder {
    private static final int DIALOG_DELETE_YESNO = 2;
    private static final int DIALOG_EDIT = 4;
    private static final int DIALOG_RING_PICKER = 3;
    private static final int MENU_STANDARD = 1;
    private static final long[] mEmptyList = new long[0];
    private static StringBuilder sFormatBuilder = new StringBuilder();
    private static Formatter sFormatter = new Formatter(sFormatBuilder, Locale.getDefault());
    private static final Object[] sTimeArgs = new Object[5];
    private StringBuilder localStringBuilder;
    String mAlbumName;
    String mArtistName;
    int mCategoryCode;
    private String[] mCursorCols;
    private String[] mCursorCols2;
    String mFilePath;
    final Handler mHandler = new Handler();
    int mID;
    int mNewGenrePos;
    String mOldGenreName;
    String mPlayPath;
    private int mPosition = -1;
    String mRowTitle;
    int mSelectedItem;
    private long[] mSongList = new long[0];
    SimpleCursorAdapter mSongListAdapter;
    ListView mSongListView;
    String mSongName;
    String mSortOrder;
    /* access modifiers changed from: private */
    public int ring_button_type;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.songs);
        setTitle(getString(R.string.title_library));
        this.mSongListView = (ListView) findViewById(R.id.lv_song);
        this.mSongListView.setOnItemClickListener(this);
        this.mCursorCols = new String[]{"_id", "title", "title_key", "_data", "album", "artist", "artist_id", "duration"};
        this.mCursorCols2 = new String[]{"_id", "audio_id", "title", "title_key", "_data", "album", "artist", "artist_id", "duration"};
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        int i = getSharedPreferences("prefs_gml", 3).getInt("sort_by", 0);
        Bundle localBundle = getIntent().getExtras();
        if (localBundle != null) {
            this.mCategoryCode = localBundle.getInt("bundle_category_code", 40);
            this.mID = localBundle.getInt("bundle_row_id");
            this.mRowTitle = localBundle.getString("bundle_row_title");
            Cursor localCursor = null;
            if (this.mCategoryCode == 40) {
                this.mSortOrder = "title_key";
                localCursor = managedQuery(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, this.mCursorCols, "title != ''" + " AND is_music=1" + " AND duration>60000", null, this.mSortOrder);
                this.localStringBuilder = new StringBuilder(" -> All Songs");
                this.mPlayPath = "All Songs";
            }
            if (this.mCategoryCode == 41) {
                this.mSortOrder = "title_key";
                StringBuilder where = new StringBuilder();
                where.append("title != ''");
                where.append(" AND is_music=1");
                where.append(" AND artist = '" + this.mRowTitle + "'");
                where.append(" AND duration>60000");
                localCursor = managedQuery(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, this.mCursorCols, where.toString(), null, this.mSortOrder);
                this.localStringBuilder = new StringBuilder(" -> Artists");
                this.mPlayPath = this.mRowTitle;
            }
            if (this.mCategoryCode == 42) {
                this.mSortOrder = "title_key";
                StringBuilder where2 = new StringBuilder();
                where2.append("title != ''");
                where2.append(" AND is_music=1");
                where2.append(" AND album = '" + this.mRowTitle + "'");
                where2.append(" AND duration>60000");
                localCursor = managedQuery(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, this.mCursorCols, where2.toString(), null, this.mSortOrder);
                this.localStringBuilder = new StringBuilder(" -> Albums");
                this.mPlayPath = this.mRowTitle;
            }
            if (this.mCategoryCode == 43) {
                localCursor = managedQuery(MediaStore.Audio.Genres.Members.getContentUri("external", (long) Integer.valueOf(this.mID).intValue()), this.mCursorCols, "duration>60000", null, null);
                this.localStringBuilder = new StringBuilder(" -> Genres");
                this.mPlayPath = this.mRowTitle;
            }
            if (this.mCategoryCode == 44) {
                this.mSortOrder = "title_key";
                localCursor = managedQuery(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, this.mCursorCols, "title != ''" + " AND is_music=1" + " AND _data like ? " + " AND duration>60000", new String[]{"%" + this.mRowTitle + "%"}, this.mSortOrder);
                this.localStringBuilder = new StringBuilder(" -> Folders");
                this.mPlayPath = this.mRowTitle;
            }
            if (this.mCategoryCode == 45) {
                localCursor = managedQuery(MediaStore.Audio.Playlists.Members.getContentUri("external", (long) Integer.valueOf(this.mID).intValue()), this.mCursorCols2, "duration>60000", null, null);
                this.localStringBuilder = new StringBuilder(" -> Playlists");
                this.mPlayPath = this.mRowTitle;
            }
            if (this.mCategoryCode != 40) {
                this.localStringBuilder.append(" -> " + this.mPlayPath);
            }
            this.localStringBuilder.insert(0, getString(R.string.title_library));
            setTitle(this.localStringBuilder.toString());
            this.mSongListAdapter = new SimpleCursorAdapter(this, R.layout.songs_row, localCursor, new String[]{"title", "artist", "album", "duration"}, new int[]{R.id.tv_song_name, R.id.tv_artist_name, R.id.tv_album_name, R.id.tv_length});
            this.mSongListAdapter.setViewBinder(this);
            this.mSongList = getSongListForCursor(localCursor);
            this.mSongListView.setAdapter((ListAdapter) this.mSongListAdapter);
        }
    }

    private static long[] getSongListForCursor(Cursor cursor) {
        int colidx;
        if (cursor == null) {
            return mEmptyList;
        }
        int len = cursor.getCount();
        long[] list = new long[len];
        cursor.moveToFirst();
        try {
            colidx = cursor.getColumnIndexOrThrow("audio_id");
        } catch (IllegalArgumentException e) {
            colidx = cursor.getColumnIndexOrThrow("_id");
        }
        for (int i = 0; i < len; i++) {
            list[i] = cursor.getLong(colidx);
            cursor.moveToNext();
        }
        return list;
    }

    /* access modifiers changed from: package-private */
    public void sendTo() {
        Intent localIntent1 = new Intent("android.intent.action.SEND");
        Intent putExtra = localIntent1.putExtra("android.intent.extra.EMAIL", Utils.EMPTY_STRING);
        new StringBuilder("Song: ");
        Intent putExtra2 = localIntent1.putExtra("android.intent.extra.SUBJECT", this.mSongName);
        Intent putExtra3 = localIntent1.putExtra("android.intent.extra.TEXT", "The attachment is the song's MP3 file.\n\nIt was downloaded by GPod Music.");
        Intent putExtra4 = localIntent1.putExtra("android.intent.extra.STREAM", Uri.fromFile(new File(this.mFilePath)));
        Intent type = localIntent1.setType("audio/*");
        startActivity(Intent.createChooser(localIntent1, "Choose client"));
    }

    public void onItemClick(AdapterView<?> adapterView, View paramView, int paramInt, long paramLong) {
        this.mPosition = paramInt;
        Cursor localCursor = this.mSongListAdapter.getCursor();
        this.mSelectedItem = localCursor.getInt(localCursor.getColumnIndex("_id"));
        Log.e("onItemClick", "media id is: " + this.mSelectedItem);
        if (this.mCategoryCode == 45) {
            this.mSelectedItem = localCursor.getInt(localCursor.getColumnIndex("audio_id"));
        }
        this.mFilePath = localCursor.getString(localCursor.getColumnIndex("_data"));
        this.mSongName = localCursor.getString(localCursor.getColumnIndex("title"));
        this.mArtistName = localCursor.getString(localCursor.getColumnIndex("artist"));
        this.mAlbumName = localCursor.getString(localCursor.getColumnIndex("album"));
        if (Util.isSdCardMounted(this)) {
            showDialog(1);
        }
    }

    public boolean setViewValue(View view, Cursor cursor, int columnIndex) {
        TextView textView = (TextView) view;
        if (cursor.getColumnIndex("duration") != columnIndex) {
            return false;
        }
        int secs = cursor.getInt(columnIndex) / Constants.MAX_DOWNLOADS;
        String durationformat = getString(secs < 3600 ? R.string.durationformatshort : R.string.durationformatlong);
        sFormatBuilder.setLength(0);
        Object[] timeArgs = sTimeArgs;
        timeArgs[0] = Integer.valueOf(secs / 3600);
        timeArgs[1] = Integer.valueOf(secs / 60);
        timeArgs[2] = Integer.valueOf((secs / 60) % 60);
        timeArgs[3] = Integer.valueOf(secs);
        timeArgs[4] = Integer.valueOf(secs % 60);
        textView.setText(sFormatter.format(durationformat, timeArgs).toString());
        return true;
    }

    /* access modifiers changed from: private */
    public void playSong(Cursor c) {
        String fPath = c.getString(c.getColumnIndex("_data"));
        if (fPath != null) {
            File songFile = new File(fPath);
            if (songFile.exists() && songFile.isFile()) {
                StreamStarterActivity.startActivity(this, fPath, c.getString(c.getColumnIndex("title")), c.getString(c.getColumnIndex("artist")), c.getString(c.getColumnIndex("album")), Utils.EMPTY_STRING, this.mSongList, this.mPosition);
                return;
            }
        }
        Toast.makeText(this, getResources().getString(R.string.ERR_FILE_NOT_EXIST), 1).show();
    }

    /* access modifiers changed from: private */
    public void shareSong(Cursor c) {
        Intent intentShare = new Intent("android.intent.action.SEND");
        int trackIDforShare = c.getInt(c.getColumnIndexOrThrow("_id"));
        intentShare.setType("audio/*");
        intentShare.putExtra("android.intent.extra.STREAM", ContentUris.withAppendedId(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, (long) trackIDforShare));
        try {
            startActivity(Intent.createChooser(intentShare, "Share via"));
        } catch (ActivityNotFoundException e) {
            Toast.makeText(this, "Unable to share", 0).show();
        }
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                return new AlertDialog.Builder(this).setTitle((int) R.string.ACTION).setItems((int) R.array.DOWNLOADED_MENU, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Cursor c;
                        Cursor c2;
                        switch (which) {
                            case 0:
                                if (SongsActivity.this.mSelectedItem != -1 && (c2 = SongsActivity.this.managedQuery(Uri.parse(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI + "/" + SongsActivity.this.mSelectedItem), null, null, null, null)) != null) {
                                    try {
                                        c2.moveToFirst();
                                        if (!c2.isAfterLast()) {
                                            SongsActivity.this.playSong(c2);
                                            c2.close();
                                            return;
                                        }
                                        return;
                                    } finally {
                                        c2.close();
                                    }
                                } else {
                                    return;
                                }
                            case 1:
                                SongsActivity.this.showDialog(2);
                                return;
                            case 2:
                                SongsActivity.this.showDialog(3);
                                return;
                            case 3:
                                if (SongsActivity.this.mSelectedItem != -1 && (c = SongsActivity.this.managedQuery(Uri.parse(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI + "/" + SongsActivity.this.mSelectedItem), null, null, null, null)) != null) {
                                    try {
                                        c.moveToFirst();
                                        if (!c.isAfterLast()) {
                                            SongsActivity.this.shareSong(c);
                                            c.close();
                                            return;
                                        }
                                        return;
                                    } finally {
                                        c.close();
                                    }
                                } else {
                                    return;
                                }
                            case 4:
                                Intent intent = new Intent(SongsActivity.this, RingdroidEditActivity.class);
                                intent.setData(Uri.parse(SongsActivity.this.mFilePath));
                                SongsActivity.this.startActivity(intent);
                                return;
                            default:
                                return;
                        }
                    }
                }).create();
            case 2:
                Cursor c = managedQuery(Uri.parse(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI + "/" + this.mSelectedItem), null, null, null, null);
                if (c == null) {
                    return null;
                }
                try {
                    c.moveToFirst();
                    if (c.isAfterLast()) {
                        c.close();
                        return null;
                    }
                    String title = c.getString(c.getColumnIndex("title"));
                    c.close();
                    if (title == null) {
                        return null;
                    }
                    View titleView = LayoutInflater.from(this).inflate((int) R.layout.delsong_msg, (ViewGroup) null);
                    ((TextView) titleView.findViewById(R.id.Title)).setText(String.valueOf(title) + getString(R.string.DELETE_SONG));
                    return new AlertDialog.Builder(this).setIcon(17301543).setCustomTitle(titleView).setPositiveButton((int) R.string.ALERT_OK, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            SongsActivity.this.deleteSong(SongsActivity.this.mSelectedItem);
                        }
                    }).setNegativeButton((int) R.string.ALERT_CANCEL, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                        }
                    }).create();
                } catch (Throwable th) {
                    c.close();
                    throw th;
                }
            case 3:
                return new AlertDialog.Builder(this).setIcon(17301543).setTitle((int) R.string.ring_picker_title).setSingleChoiceItems((int) R.array.ring_types, 0, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        SongsActivity.this.ring_button_type = whichButton;
                    }
                }).setPositiveButton((int) R.string.alertdialog_ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        int ring_type;
                        Cursor c;
                        if (SongsActivity.this.ring_button_type == 0) {
                            ring_type = 1;
                        } else if (SongsActivity.this.ring_button_type == 1) {
                            ring_type = 2;
                        } else {
                            ring_type = 4;
                        }
                        String filePath = null;
                        String title = null;
                        try {
                            if (SongsActivity.this.mSelectedItem != -1) {
                                c = SongsActivity.this.managedQuery(Uri.parse(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI + "/" + SongsActivity.this.mSelectedItem), null, null, null, null);
                                if (c != null) {
                                    c.moveToFirst();
                                    if (c.isAfterLast()) {
                                        c.close();
                                        return;
                                    }
                                    filePath = c.getString(c.getColumnIndex("_data"));
                                    title = c.getString(c.getColumnIndex("title"));
                                    c.close();
                                    if (filePath == null) {
                                        return;
                                    }
                                } else {
                                    return;
                                }
                            }
                            Uri uri = Ringtone.insertRingtone(SongsActivity.this.getContentResolver(), filePath, title);
                            if (uri != null) {
                                RingtoneManager.setActualDefaultRingtoneUri(SongsActivity.this, ring_type, uri);
                                Toast.makeText(SongsActivity.this, String.valueOf(title) + " set ok", 0).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        } catch (Throwable th) {
                            c.close();
                            throw th;
                        }
                    }
                }).setNegativeButton((int) R.string.alertdialog_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                }).create();
            default:
                return null;
        }
    }

    /* access modifiers changed from: private */
    public void deleteSong(int nItemID) {
        Cursor c = managedQuery(Uri.parse(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI + "/" + this.mSelectedItem), null, null, null, null);
        if (c != null) {
            try {
                c.moveToFirst();
                if (!c.isAfterLast()) {
                    File f = new File(c.getString(c.getColumnIndex("_data")));
                    if (f.exists() && f.isFile() && !f.delete()) {
                        f.deleteOnExit();
                    }
                    getContentResolver().delete(Uri.parse(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI + "/" + nItemID), null, null);
                    c.close();
                }
            } finally {
                c.close();
            }
        }
    }
}
