package net.cross.micplayer.editor;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsoluteLayout;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import com.qwapi.adclient.android.utils.Utils;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.net.URLEncoder;
import java.util.Random;
import net.cross.micplayer.R;
import net.cross.micplayer.editor.MarkerView;
import net.cross.micplayer.editor.WaveformView;
import net.cross.micplayer.soundfile.CheapSoundFile;
import net.cross.micplayer.utils.Constants;
import net.cross.micplayer.utils.Util;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

public class RingdroidEditActivity extends Activity implements MarkerView.MarkerListener, WaveformView.WaveformListener {
    private static final int CMD_ABOUT = 3;
    private static final int CMD_RESET = 2;
    private static final int CMD_SAVE = 1;
    public static final String EDIT = "net.cross.micplayer.action.EDIT";
    public static final String ERR_SERVER_URL = "http://ringdroid.appspot.com/err";
    public static final String PREF_ERROR_COUNT = "error_count";
    public static final String PREF_ERR_SERVER_ALLOWED = "err_server_allowed";
    public static final String PREF_ERR_SERVER_CHECK = "err_server_check";
    public static final String PREF_STATS_SERVER_ALLOWED = "stats_server_allowed";
    public static final String PREF_STATS_SERVER_CHECK = "stats_server_check";
    public static final String PREF_SUCCESS_COUNT = "success_count";
    public static final String PREF_UNIQUE_ID = "unique_id";
    private static final int REQUEST_CODE_CHOOSE_CONTACT = 2;
    private static final int REQUEST_CODE_RECORD = 1;
    public static final int SERVER_ALLOWED_NO = 1;
    public static final int SERVER_ALLOWED_UNKNOWN = 0;
    public static final int SERVER_ALLOWED_YES = 2;
    public static final String STATS_SERVER_URL = "http://ringdroid.appspot.com/add";
    private String mAlbum;
    private String mArtist;
    /* access modifiers changed from: private */
    public boolean mCanSeekAccurately;
    private String mCaption = Utils.EMPTY_STRING;
    /* access modifiers changed from: private */
    public float mDensity;
    private String mDstFilename;
    /* access modifiers changed from: private */
    public MarkerView mEndMarker;
    /* access modifiers changed from: private */
    public int mEndPos;
    /* access modifiers changed from: private */
    public TextView mEndText;
    /* access modifiers changed from: private */
    public boolean mEndVisible;
    private String mExtension;
    private ImageButton mFfwdButton;
    private View.OnClickListener mFfwdListener = new View.OnClickListener() {
        public void onClick(View sender) {
            if (RingdroidEditActivity.this.mIsPlaying) {
                int newPos = RingdroidEditActivity.this.mPlayer.getCurrentPosition() + 5000;
                if (newPos > RingdroidEditActivity.this.mPlayEndMsec) {
                    newPos = RingdroidEditActivity.this.mPlayEndMsec;
                }
                RingdroidEditActivity.this.mPlayer.seekTo(newPos);
                return;
            }
            RingdroidEditActivity.this.mEndMarker.requestFocus();
            RingdroidEditActivity.this.markerFocus(RingdroidEditActivity.this.mEndMarker);
        }
    };
    /* access modifiers changed from: private */
    public File mFile;
    private String mFilename;
    private int mFlingVelocity;
    private String mGenre;
    /* access modifiers changed from: private */
    public Handler mHandler;
    /* access modifiers changed from: private */
    public TextView mInfo;
    /* access modifiers changed from: private */
    public boolean mIsPlaying;
    private boolean mKeyDown;
    /* access modifiers changed from: private */
    public int mLastDisplayedEndPos;
    /* access modifiers changed from: private */
    public int mLastDisplayedStartPos;
    /* access modifiers changed from: private */
    public boolean mLoadingKeepGoing;
    /* access modifiers changed from: private */
    public long mLoadingLastUpdateTime;
    private long mLoadingStartTime;
    private View.OnClickListener mMarkEndListener = new View.OnClickListener() {
        public void onClick(View sender) {
            if (RingdroidEditActivity.this.mIsPlaying) {
                RingdroidEditActivity.this.mEndPos = RingdroidEditActivity.this.mWaveformView.millisecsToPixels(RingdroidEditActivity.this.mPlayer.getCurrentPosition() + RingdroidEditActivity.this.mPlayStartOffset);
                RingdroidEditActivity.this.updateDisplay();
                RingdroidEditActivity.this.handlePause();
            }
        }
    };
    private View.OnClickListener mMarkStartListener = new View.OnClickListener() {
        public void onClick(View sender) {
            if (RingdroidEditActivity.this.mIsPlaying) {
                RingdroidEditActivity.this.mStartPos = RingdroidEditActivity.this.mWaveformView.millisecsToPixels(RingdroidEditActivity.this.mPlayer.getCurrentPosition() + RingdroidEditActivity.this.mPlayStartOffset);
                RingdroidEditActivity.this.updateDisplay();
            }
        }
    };
    private int mMarkerBottomOffset;
    private int mMarkerLeftInset;
    private int mMarkerRightInset;
    private int mMarkerTopOffset;
    /* access modifiers changed from: private */
    public int mMaxPos;
    /* access modifiers changed from: private */
    public int mNewFileKind;
    /* access modifiers changed from: private */
    public int mOffset;
    /* access modifiers changed from: private */
    public int mOffsetGoal;
    private ImageButton mPlayButton;
    /* access modifiers changed from: private */
    public int mPlayEndMsec;
    private View.OnClickListener mPlayListener = new View.OnClickListener() {
        public void onClick(View sender) {
            RingdroidEditActivity.this.onPlay(RingdroidEditActivity.this.mStartPos);
        }
    };
    /* access modifiers changed from: private */
    public int mPlayStartMsec;
    /* access modifiers changed from: private */
    public int mPlayStartOffset;
    /* access modifiers changed from: private */
    public MediaPlayer mPlayer;
    /* access modifiers changed from: private */
    public ProgressDialog mProgressDialog;
    private String mRecordingFilename;
    private Uri mRecordingUri;
    private ImageButton mRewindButton;
    private View.OnClickListener mRewindListener = new View.OnClickListener() {
        public void onClick(View sender) {
            if (RingdroidEditActivity.this.mIsPlaying) {
                int newPos = RingdroidEditActivity.this.mPlayer.getCurrentPosition() - 5000;
                if (newPos < RingdroidEditActivity.this.mPlayStartMsec) {
                    newPos = RingdroidEditActivity.this.mPlayStartMsec;
                }
                RingdroidEditActivity.this.mPlayer.seekTo(newPos);
                return;
            }
            RingdroidEditActivity.this.mStartMarker.requestFocus();
            RingdroidEditActivity.this.markerFocus(RingdroidEditActivity.this.mStartMarker);
        }
    };
    private ImageButton mSaveButton;
    private View.OnClickListener mSaveListener = new View.OnClickListener() {
        public void onClick(View sender) {
            RingdroidEditActivity.this.onSave();
        }
    };
    /* access modifiers changed from: private */
    public CheapSoundFile mSoundFile;
    /* access modifiers changed from: private */
    public MarkerView mStartMarker;
    /* access modifiers changed from: private */
    public int mStartPos;
    /* access modifiers changed from: private */
    public TextView mStartText;
    /* access modifiers changed from: private */
    public boolean mStartVisible;
    private TextWatcher mTextWatcher = new TextWatcher() {
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        public void afterTextChanged(Editable s) {
            if (RingdroidEditActivity.this.mStartText.hasFocus()) {
                try {
                    RingdroidEditActivity.this.mStartPos = RingdroidEditActivity.this.mWaveformView.secondsToPixels(Double.parseDouble(RingdroidEditActivity.this.mStartText.getText().toString()));
                    RingdroidEditActivity.this.updateDisplay();
                } catch (NumberFormatException e) {
                }
            }
            if (RingdroidEditActivity.this.mEndText.hasFocus()) {
                try {
                    RingdroidEditActivity.this.mEndPos = RingdroidEditActivity.this.mWaveformView.secondsToPixels(Double.parseDouble(RingdroidEditActivity.this.mEndText.getText().toString()));
                    RingdroidEditActivity.this.updateDisplay();
                } catch (NumberFormatException e2) {
                }
            }
        }
    };
    /* access modifiers changed from: private */
    public Runnable mTimerRunnable = new Runnable() {
        public void run() {
            if (RingdroidEditActivity.this.mStartPos != RingdroidEditActivity.this.mLastDisplayedStartPos && !RingdroidEditActivity.this.mStartText.hasFocus()) {
                RingdroidEditActivity.this.mStartText.setText(RingdroidEditActivity.this.formatTime(RingdroidEditActivity.this.mStartPos));
                RingdroidEditActivity.this.mLastDisplayedStartPos = RingdroidEditActivity.this.mStartPos;
            }
            if (RingdroidEditActivity.this.mEndPos != RingdroidEditActivity.this.mLastDisplayedEndPos && !RingdroidEditActivity.this.mEndText.hasFocus()) {
                RingdroidEditActivity.this.mEndText.setText(RingdroidEditActivity.this.formatTime(RingdroidEditActivity.this.mEndPos));
                RingdroidEditActivity.this.mLastDisplayedEndPos = RingdroidEditActivity.this.mEndPos;
            }
            RingdroidEditActivity.this.mHandler.postDelayed(RingdroidEditActivity.this.mTimerRunnable, 100);
        }
    };
    private String mTitle;
    private boolean mTouchDragging;
    private int mTouchInitialEndPos;
    private int mTouchInitialOffset;
    private int mTouchInitialStartPos;
    private float mTouchStart;
    private boolean mWasGetContentIntent;
    private long mWaveformTouchStartMsec;
    /* access modifiers changed from: private */
    public WaveformView mWaveformView;
    private int mWidth;
    private int mYear;
    private ImageButton mZoomInButton;
    private View.OnClickListener mZoomInListener = new View.OnClickListener() {
        public void onClick(View sender) {
            RingdroidEditActivity.this.mWaveformView.zoomIn();
            RingdroidEditActivity.this.mStartPos = RingdroidEditActivity.this.mWaveformView.getStart();
            RingdroidEditActivity.this.mEndPos = RingdroidEditActivity.this.mWaveformView.getEnd();
            RingdroidEditActivity.this.mMaxPos = RingdroidEditActivity.this.mWaveformView.maxPos();
            RingdroidEditActivity.this.mOffset = RingdroidEditActivity.this.mWaveformView.getOffset();
            RingdroidEditActivity.this.mOffsetGoal = RingdroidEditActivity.this.mOffset;
            RingdroidEditActivity.this.enableZoomButtons();
            RingdroidEditActivity.this.updateDisplay();
        }
    };
    private ImageButton mZoomOutButton;
    private View.OnClickListener mZoomOutListener = new View.OnClickListener() {
        public void onClick(View sender) {
            RingdroidEditActivity.this.mWaveformView.zoomOut();
            RingdroidEditActivity.this.mStartPos = RingdroidEditActivity.this.mWaveformView.getStart();
            RingdroidEditActivity.this.mEndPos = RingdroidEditActivity.this.mWaveformView.getEnd();
            RingdroidEditActivity.this.mMaxPos = RingdroidEditActivity.this.mWaveformView.maxPos();
            RingdroidEditActivity.this.mOffset = RingdroidEditActivity.this.mWaveformView.getOffset();
            RingdroidEditActivity.this.mOffsetGoal = RingdroidEditActivity.this.mOffset;
            RingdroidEditActivity.this.enableZoomButtons();
            RingdroidEditActivity.this.updateDisplay();
        }
    };

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        this.mRecordingFilename = null;
        this.mRecordingUri = null;
        this.mPlayer = null;
        this.mIsPlaying = false;
        Intent intent = getIntent();
        if (intent.getBooleanExtra("privacy", false)) {
            showServerPrompt(true);
            return;
        }
        this.mWasGetContentIntent = intent.getBooleanExtra("was_get_content_intent", false);
        this.mFilename = intent.getData().toString();
        this.mSoundFile = null;
        this.mKeyDown = false;
        if (this.mFilename.equals("record")) {
            try {
                startActivityForResult(new Intent("android.provider.MediaStore.RECORD_SOUND"), 1);
            } catch (Exception e) {
                showFinalAlert(e, (int) R.string.record_error);
            }
        }
        this.mHandler = new Handler();
        loadGui();
        this.mHandler.postDelayed(this.mTimerRunnable, 100);
        if (!this.mFilename.equals("record")) {
            loadFromFile();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        Log.i("Ringdroid", "EditActivity OnDestroy");
        if (this.mPlayer != null && this.mPlayer.isPlaying()) {
            this.mPlayer.stop();
        }
        this.mPlayer = null;
        if (this.mRecordingFilename != null) {
            try {
                if (!new File(this.mRecordingFilename).delete()) {
                    showFinalAlert(new Exception(), (int) R.string.delete_tmp_error);
                }
                getContentResolver().delete(this.mRecordingUri, null, null);
            } catch (SecurityException e) {
                showFinalAlert(e, (int) R.string.delete_tmp_error);
            }
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent dataIntent) {
        if (requestCode == 2) {
            sendStatsToServerIfAllowedAndFinish();
        } else if (requestCode != 1) {
        } else {
            if (resultCode != -1) {
                finish();
            } else if (dataIntent == null) {
                finish();
            } else {
                this.mRecordingUri = dataIntent.getData();
                this.mRecordingFilename = getFilenameFromUri(this.mRecordingUri);
                this.mFilename = this.mRecordingFilename;
                loadFromFile();
            }
        }
    }

    public void onConfigurationChanged(Configuration newConfig) {
        final int saveZoomLevel = this.mWaveformView.getZoomLevel();
        super.onConfigurationChanged(newConfig);
        loadGui();
        enableZoomButtons();
        this.mHandler.postDelayed(new Runnable() {
            public void run() {
                RingdroidEditActivity.this.mStartMarker.requestFocus();
                RingdroidEditActivity.this.markerFocus(RingdroidEditActivity.this.mStartMarker);
                RingdroidEditActivity.this.mWaveformView.setZoomLevel(saveZoomLevel);
                RingdroidEditActivity.this.mWaveformView.recomputeHeights(RingdroidEditActivity.this.mDensity);
                RingdroidEditActivity.this.updateDisplay();
            }
        }, 500);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, 1, 0, (int) R.string.menu_save).setIcon((int) R.drawable.menu_save);
        menu.add(0, 2, 0, (int) R.string.menu_reset).setIcon((int) R.drawable.menu_reset);
        menu.add(0, 3, 0, (int) R.string.menu_about).setIcon((int) R.drawable.menu_about);
        return true;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.findItem(1).setVisible(true);
        menu.findItem(2).setVisible(true);
        menu.findItem(3).setVisible(true);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                onSave();
                return true;
            case 2:
                resetPositions();
                this.mOffsetGoal = 0;
                updateDisplay();
                return true;
            case 3:
                onAbout(this);
                return true;
            default:
                return false;
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 62) {
            return super.onKeyDown(keyCode, event);
        }
        onPlay(this.mStartPos);
        return true;
    }

    public void waveformDraw() {
        this.mWidth = this.mWaveformView.getMeasuredWidth();
        if (this.mOffsetGoal != this.mOffset && !this.mKeyDown) {
            updateDisplay();
        } else if (this.mIsPlaying) {
            updateDisplay();
        } else if (this.mFlingVelocity != 0) {
            updateDisplay();
        }
    }

    public void waveformTouchStart(float x) {
        this.mTouchDragging = true;
        this.mTouchStart = x;
        this.mTouchInitialOffset = this.mOffset;
        this.mFlingVelocity = 0;
        this.mWaveformTouchStartMsec = System.currentTimeMillis();
    }

    public void waveformTouchMove(float x) {
        this.mOffset = trap((int) (((float) this.mTouchInitialOffset) + (this.mTouchStart - x)));
        updateDisplay();
    }

    public void waveformTouchEnd() {
        this.mTouchDragging = false;
        this.mOffsetGoal = this.mOffset;
        if (System.currentTimeMillis() - this.mWaveformTouchStartMsec >= 300) {
            return;
        }
        if (this.mIsPlaying) {
            int seekMsec = this.mWaveformView.pixelsToMillisecs((int) (this.mTouchStart + ((float) this.mOffset)));
            if (seekMsec < this.mPlayStartMsec || seekMsec >= this.mPlayEndMsec) {
                handlePause();
            } else {
                this.mPlayer.seekTo(seekMsec - this.mPlayStartOffset);
            }
        } else {
            onPlay((int) (this.mTouchStart + ((float) this.mOffset)));
        }
    }

    public void waveformFling(float vx) {
        this.mTouchDragging = false;
        this.mOffsetGoal = this.mOffset;
        this.mFlingVelocity = (int) (-vx);
        updateDisplay();
    }

    public void markerDraw() {
    }

    public void markerTouchStart(MarkerView marker, float x) {
        this.mTouchDragging = true;
        this.mTouchStart = x;
        this.mTouchInitialStartPos = this.mStartPos;
        this.mTouchInitialEndPos = this.mEndPos;
    }

    public void markerTouchMove(MarkerView marker, float x) {
        float delta = x - this.mTouchStart;
        if (marker == this.mStartMarker) {
            this.mStartPos = trap((int) (((float) this.mTouchInitialStartPos) + delta));
            this.mEndPos = trap((int) (((float) this.mTouchInitialEndPos) + delta));
        } else {
            this.mEndPos = trap((int) (((float) this.mTouchInitialEndPos) + delta));
            if (this.mEndPos < this.mStartPos) {
                this.mEndPos = this.mStartPos;
            }
        }
        updateDisplay();
    }

    public void markerTouchEnd(MarkerView marker) {
        this.mTouchDragging = false;
        if (marker == this.mStartMarker) {
            setOffsetGoalStart();
        } else {
            setOffsetGoalEnd();
        }
    }

    public void markerLeft(MarkerView marker, int velocity) {
        this.mKeyDown = true;
        if (marker == this.mStartMarker) {
            int saveStart = this.mStartPos;
            this.mStartPos = trap(this.mStartPos - velocity);
            this.mEndPos = trap(this.mEndPos - (saveStart - this.mStartPos));
            setOffsetGoalStart();
        }
        if (marker == this.mEndMarker) {
            if (this.mEndPos == this.mStartPos) {
                this.mStartPos = trap(this.mStartPos - velocity);
                this.mEndPos = this.mStartPos;
            } else {
                this.mEndPos = trap(this.mEndPos - velocity);
            }
            setOffsetGoalEnd();
        }
        updateDisplay();
    }

    public void markerRight(MarkerView marker, int velocity) {
        this.mKeyDown = true;
        if (marker == this.mStartMarker) {
            int saveStart = this.mStartPos;
            this.mStartPos += velocity;
            if (this.mStartPos > this.mMaxPos) {
                this.mStartPos = this.mMaxPos;
            }
            this.mEndPos += this.mStartPos - saveStart;
            if (this.mEndPos > this.mMaxPos) {
                this.mEndPos = this.mMaxPos;
            }
            setOffsetGoalStart();
        }
        if (marker == this.mEndMarker) {
            this.mEndPos += velocity;
            if (this.mEndPos > this.mMaxPos) {
                this.mEndPos = this.mMaxPos;
            }
            setOffsetGoalEnd();
        }
        updateDisplay();
    }

    public void markerEnter(MarkerView marker) {
    }

    public void markerKeyUp() {
        this.mKeyDown = false;
        updateDisplay();
    }

    public void markerFocus(MarkerView marker) {
        this.mKeyDown = false;
        if (marker == this.mStartMarker) {
            setOffsetGoalStartNoUpdate();
        } else {
            setOffsetGoalEndNoUpdate();
        }
        this.mHandler.postDelayed(new Runnable() {
            public void run() {
                RingdroidEditActivity.this.updateDisplay();
            }
        }, 100);
    }

    public static void onAbout(Activity activity) {
        new AlertDialog.Builder(activity).setTitle((int) R.string.about_title).setMessage((int) R.string.about_text).setPositiveButton((int) R.string.alert_ok_button, (DialogInterface.OnClickListener) null).setCancelable(false).show();
    }

    private void loadGui() {
        setContentView((int) R.layout.editor);
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        this.mDensity = metrics.density;
        this.mMarkerLeftInset = (int) (46.0f * this.mDensity);
        this.mMarkerRightInset = (int) (48.0f * this.mDensity);
        this.mMarkerTopOffset = (int) (this.mDensity * 10.0f);
        this.mMarkerBottomOffset = (int) (this.mDensity * 10.0f);
        this.mStartText = (TextView) findViewById(R.id.starttext);
        this.mStartText.addTextChangedListener(this.mTextWatcher);
        this.mEndText = (TextView) findViewById(R.id.endtext);
        this.mEndText.addTextChangedListener(this.mTextWatcher);
        this.mPlayButton = (ImageButton) findViewById(R.id.play);
        this.mPlayButton.setOnClickListener(this.mPlayListener);
        this.mRewindButton = (ImageButton) findViewById(R.id.rew);
        this.mRewindButton.setOnClickListener(this.mRewindListener);
        this.mFfwdButton = (ImageButton) findViewById(R.id.ffwd);
        this.mFfwdButton.setOnClickListener(this.mFfwdListener);
        this.mZoomInButton = (ImageButton) findViewById(R.id.zoom_in);
        this.mZoomInButton.setOnClickListener(this.mZoomInListener);
        this.mZoomOutButton = (ImageButton) findViewById(R.id.zoom_out);
        this.mZoomOutButton.setOnClickListener(this.mZoomOutListener);
        this.mSaveButton = (ImageButton) findViewById(R.id.save);
        this.mSaveButton.setOnClickListener(this.mSaveListener);
        ((TextView) findViewById(R.id.mark_start)).setOnClickListener(this.mMarkStartListener);
        ((TextView) findViewById(R.id.mark_end)).setOnClickListener(this.mMarkStartListener);
        enableDisableButtons();
        this.mWaveformView = (WaveformView) findViewById(R.id.waveform);
        this.mWaveformView.setListener(this);
        this.mInfo = (TextView) findViewById(R.id.info);
        this.mInfo.setText(this.mCaption);
        this.mMaxPos = 0;
        this.mLastDisplayedStartPos = -1;
        this.mLastDisplayedEndPos = -1;
        if (this.mSoundFile != null) {
            this.mWaveformView.setSoundFile(this.mSoundFile);
            this.mWaveformView.recomputeHeights(this.mDensity);
            this.mMaxPos = this.mWaveformView.maxPos();
        }
        this.mStartMarker = (MarkerView) findViewById(R.id.startmarker);
        this.mStartMarker.setListener(this);
        this.mStartMarker.setAlpha(255);
        this.mStartMarker.setFocusable(true);
        this.mStartMarker.setFocusableInTouchMode(true);
        this.mStartVisible = true;
        this.mEndMarker = (MarkerView) findViewById(R.id.endmarker);
        this.mEndMarker.setListener(this);
        this.mEndMarker.setAlpha(255);
        this.mEndMarker.setFocusable(true);
        this.mEndMarker.setFocusableInTouchMode(true);
        this.mEndVisible = true;
        updateDisplay();
    }

    private void loadFromFile() {
        this.mFile = new File(this.mFilename);
        this.mExtension = getExtensionFromFilename(this.mFilename);
        SongMetadataReader metadataReader = new SongMetadataReader(this, this.mFilename);
        this.mTitle = metadataReader.mTitle;
        this.mArtist = metadataReader.mArtist;
        this.mAlbum = metadataReader.mAlbum;
        this.mYear = metadataReader.mYear;
        this.mGenre = metadataReader.mGenre;
        String titleLabel = this.mTitle;
        if (this.mArtist != null && this.mArtist.length() > 0) {
            titleLabel = String.valueOf(titleLabel) + " - " + this.mArtist;
        }
        setTitle(titleLabel);
        this.mLoadingStartTime = System.currentTimeMillis();
        this.mLoadingLastUpdateTime = System.currentTimeMillis();
        this.mLoadingKeepGoing = true;
        this.mProgressDialog = new ProgressDialog(this);
        this.mProgressDialog.setProgressStyle(1);
        this.mProgressDialog.setTitle((int) R.string.progress_dialog_loading);
        this.mProgressDialog.setCancelable(true);
        this.mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                RingdroidEditActivity.this.mLoadingKeepGoing = false;
            }
        });
        this.mProgressDialog.show();
        final CheapSoundFile.ProgressListener listener = new CheapSoundFile.ProgressListener() {
            public boolean reportProgress(double fractionComplete) {
                long now = System.currentTimeMillis();
                if (now - RingdroidEditActivity.this.mLoadingLastUpdateTime > 100) {
                    RingdroidEditActivity.this.mProgressDialog.setProgress((int) (((double) RingdroidEditActivity.this.mProgressDialog.getMax()) * fractionComplete));
                    RingdroidEditActivity.this.mLoadingLastUpdateTime = now;
                }
                return RingdroidEditActivity.this.mLoadingKeepGoing;
            }
        };
        this.mCanSeekAccurately = false;
        new Thread() {
            public void run() {
                RingdroidEditActivity.this.mCanSeekAccurately = SeekTest.CanSeekAccurately(RingdroidEditActivity.this.getPreferences(0));
                System.out.println("Seek test done, creating media player.");
                try {
                    MediaPlayer player = new MediaPlayer();
                    player.setDataSource(RingdroidEditActivity.this.mFile.getAbsolutePath());
                    player.setAudioStreamType(3);
                    player.prepare();
                    RingdroidEditActivity.this.mPlayer = player;
                } catch (IOException e) {
                    final IOException e2 = e;
                    RingdroidEditActivity.this.mHandler.post(new Runnable() {
                        public void run() {
                            RingdroidEditActivity.this.handleFatalError("ReadError", RingdroidEditActivity.this.getResources().getText(R.string.read_error), e2);
                        }
                    });
                }
            }
        }.start();
        new Thread() {
            public void run() {
                String err;
                try {
                    RingdroidEditActivity.this.mSoundFile = CheapSoundFile.create(RingdroidEditActivity.this.mFile.getAbsolutePath(), listener);
                    if (RingdroidEditActivity.this.mSoundFile == null) {
                        RingdroidEditActivity.this.mProgressDialog.dismiss();
                        String[] components = RingdroidEditActivity.this.mFile.getName().toLowerCase().split("\\.");
                        if (components.length < 2) {
                            err = RingdroidEditActivity.this.getResources().getString(R.string.no_extension_error);
                        } else {
                            err = String.valueOf(RingdroidEditActivity.this.getResources().getString(R.string.bad_extension_error)) + " " + components[components.length - 1];
                        }
                        final String finalErr = err;
                        RingdroidEditActivity.this.mHandler.post(new Runnable() {
                            public void run() {
                                RingdroidEditActivity.this.handleFatalError("UnsupportedExtension", finalErr, new Exception());
                            }
                        });
                        return;
                    }
                    RingdroidEditActivity.this.mProgressDialog.dismiss();
                    if (RingdroidEditActivity.this.mLoadingKeepGoing) {
                        RingdroidEditActivity.this.mHandler.post(new Runnable() {
                            public void run() {
                                RingdroidEditActivity.this.finishOpeningSoundFile();
                            }
                        });
                        return;
                    }
                    RingdroidEditActivity.this.finish();
                } catch (Exception e) {
                    final Exception e2 = e;
                    RingdroidEditActivity.this.mProgressDialog.dismiss();
                    e2.printStackTrace();
                    RingdroidEditActivity.this.mInfo.setText(e2.toString());
                    RingdroidEditActivity.this.mHandler.post(new Runnable() {
                        public void run() {
                            RingdroidEditActivity.this.handleFatalError("ReadError", RingdroidEditActivity.this.getResources().getText(R.string.read_error), e2);
                        }
                    });
                }
            }
        }.start();
    }

    /* access modifiers changed from: private */
    public void finishOpeningSoundFile() {
        this.mWaveformView.setSoundFile(this.mSoundFile);
        this.mWaveformView.recomputeHeights(this.mDensity);
        this.mMaxPos = this.mWaveformView.maxPos();
        this.mLastDisplayedStartPos = -1;
        this.mLastDisplayedEndPos = -1;
        this.mTouchDragging = false;
        this.mOffset = 0;
        this.mOffsetGoal = 0;
        this.mFlingVelocity = 0;
        resetPositions();
        if (this.mEndPos > this.mMaxPos) {
            this.mEndPos = this.mMaxPos;
        }
        this.mCaption = String.valueOf(this.mSoundFile.getFiletype()) + ", " + this.mSoundFile.getSampleRate() + " Hz, " + this.mSoundFile.getAvgBitrateKbps() + " kbps, " + formatTime(this.mMaxPos) + " " + getResources().getString(R.string.time_seconds);
        this.mInfo.setText(this.mCaption);
        updateDisplay();
    }

    /* access modifiers changed from: private */
    public synchronized void updateDisplay() {
        int offsetDelta;
        if (this.mIsPlaying) {
            int now = this.mPlayer.getCurrentPosition() + this.mPlayStartOffset;
            int frames = this.mWaveformView.millisecsToPixels(now);
            this.mWaveformView.setPlayback(frames);
            setOffsetGoalNoUpdate(frames - (this.mWidth / 2));
            if (now >= this.mPlayEndMsec) {
                handlePause();
            }
        }
        if (!this.mTouchDragging) {
            if (this.mFlingVelocity != 0) {
                float f = (float) this.mFlingVelocity;
                int offsetDelta2 = this.mFlingVelocity / 30;
                if (this.mFlingVelocity > 80) {
                    this.mFlingVelocity -= 80;
                } else if (this.mFlingVelocity < -80) {
                    this.mFlingVelocity += 80;
                } else {
                    this.mFlingVelocity = 0;
                }
                this.mOffset += offsetDelta2;
                if (this.mOffset + (this.mWidth / 2) > this.mMaxPos) {
                    this.mOffset = this.mMaxPos - (this.mWidth / 2);
                    this.mFlingVelocity = 0;
                }
                if (this.mOffset < 0) {
                    this.mOffset = 0;
                    this.mFlingVelocity = 0;
                }
                this.mOffsetGoal = this.mOffset;
            } else {
                int offsetDelta3 = this.mOffsetGoal - this.mOffset;
                if (offsetDelta3 > 10) {
                    offsetDelta = offsetDelta3 / 10;
                } else if (offsetDelta3 > 0) {
                    offsetDelta = 1;
                } else if (offsetDelta3 < -10) {
                    offsetDelta = offsetDelta3 / 10;
                } else if (offsetDelta3 < 0) {
                    offsetDelta = -1;
                } else {
                    offsetDelta = 0;
                }
                this.mOffset += offsetDelta;
            }
        }
        this.mWaveformView.setParameters(this.mStartPos, this.mEndPos, this.mOffset);
        this.mWaveformView.invalidate();
        this.mStartMarker.setContentDescription(((Object) getResources().getText(R.string.start_marker)) + " " + formatTime(this.mStartPos));
        this.mEndMarker.setContentDescription(((Object) getResources().getText(R.string.end_marker)) + " " + formatTime(this.mEndPos));
        int startX = (this.mStartPos - this.mOffset) - this.mMarkerLeftInset;
        if (this.mStartMarker.getWidth() + startX < 0) {
            if (this.mStartVisible) {
                this.mStartMarker.setAlpha(0);
                this.mStartVisible = false;
            }
            startX = 0;
        } else if (!this.mStartVisible) {
            this.mHandler.postDelayed(new Runnable() {
                public void run() {
                    RingdroidEditActivity.this.mStartVisible = true;
                    RingdroidEditActivity.this.mStartMarker.setAlpha(255);
                }
            }, 0);
        }
        int endX = ((this.mEndPos - this.mOffset) - this.mEndMarker.getWidth()) + this.mMarkerRightInset;
        if (this.mEndMarker.getWidth() + endX < 0) {
            if (this.mEndVisible) {
                this.mEndMarker.setAlpha(0);
                this.mEndVisible = false;
            }
            endX = 0;
        } else if (!this.mEndVisible) {
            this.mHandler.postDelayed(new Runnable() {
                public void run() {
                    RingdroidEditActivity.this.mEndVisible = true;
                    RingdroidEditActivity.this.mEndMarker.setAlpha(255);
                }
            }, 0);
        }
        this.mStartMarker.setLayoutParams(new AbsoluteLayout.LayoutParams(-2, -2, startX, this.mMarkerTopOffset));
        this.mEndMarker.setLayoutParams(new AbsoluteLayout.LayoutParams(-2, -2, endX, (this.mWaveformView.getMeasuredHeight() - this.mEndMarker.getHeight()) - this.mMarkerBottomOffset));
    }

    private void enableDisableButtons() {
        if (this.mIsPlaying) {
            this.mPlayButton.setImageResource(17301539);
            this.mPlayButton.setContentDescription(getResources().getText(R.string.stop));
            return;
        }
        this.mPlayButton.setImageResource(17301540);
        this.mPlayButton.setContentDescription(getResources().getText(R.string.play));
    }

    private void resetPositions() {
        this.mStartPos = this.mWaveformView.secondsToPixels(0.0d);
        this.mEndPos = this.mWaveformView.secondsToPixels(15.0d);
    }

    private int trap(int pos) {
        if (pos < 0) {
            return 0;
        }
        return pos > this.mMaxPos ? this.mMaxPos : pos;
    }

    private void setOffsetGoalStart() {
        setOffsetGoal(this.mStartPos - (this.mWidth / 2));
    }

    private void setOffsetGoalStartNoUpdate() {
        setOffsetGoalNoUpdate(this.mStartPos - (this.mWidth / 2));
    }

    private void setOffsetGoalEnd() {
        setOffsetGoal(this.mEndPos - (this.mWidth / 2));
    }

    private void setOffsetGoalEndNoUpdate() {
        setOffsetGoalNoUpdate(this.mEndPos - (this.mWidth / 2));
    }

    private void setOffsetGoal(int offset) {
        setOffsetGoalNoUpdate(offset);
        updateDisplay();
    }

    private void setOffsetGoalNoUpdate(int offset) {
        if (!this.mTouchDragging) {
            this.mOffsetGoal = offset;
            if (this.mOffsetGoal + (this.mWidth / 2) > this.mMaxPos) {
                this.mOffsetGoal = this.mMaxPos - (this.mWidth / 2);
            }
            if (this.mOffsetGoal < 0) {
                this.mOffsetGoal = 0;
            }
        }
    }

    /* access modifiers changed from: private */
    public String formatTime(int pixels) {
        if (this.mWaveformView == null || !this.mWaveformView.isInitialized()) {
            return Utils.EMPTY_STRING;
        }
        return formatDecimal(this.mWaveformView.pixelsToSeconds(pixels));
    }

    private String formatDecimal(double x) {
        int xWhole = (int) x;
        int xFrac = (int) ((100.0d * (x - ((double) xWhole))) + 0.5d);
        if (xFrac >= 100) {
            xWhole++;
            xFrac -= 100;
            if (xFrac < 10) {
                xFrac *= 10;
            }
        }
        if (xFrac < 10) {
            return String.valueOf(xWhole) + ".0" + xFrac;
        }
        return String.valueOf(xWhole) + "." + xFrac;
    }

    /* access modifiers changed from: private */
    public synchronized void handlePause() {
        if (this.mPlayer != null && this.mPlayer.isPlaying()) {
            this.mPlayer.pause();
        }
        this.mWaveformView.setPlayback(-1);
        this.mIsPlaying = false;
        enableDisableButtons();
    }

    /* access modifiers changed from: private */
    public synchronized void onPlay(int startPosition) {
        if (this.mIsPlaying) {
            handlePause();
        } else if (this.mPlayer != null) {
            try {
                this.mPlayStartMsec = this.mWaveformView.pixelsToMillisecs(startPosition);
                if (startPosition < this.mStartPos) {
                    this.mPlayEndMsec = this.mWaveformView.pixelsToMillisecs(this.mStartPos);
                } else if (startPosition > this.mEndPos) {
                    this.mPlayEndMsec = this.mWaveformView.pixelsToMillisecs(this.mMaxPos);
                } else {
                    this.mPlayEndMsec = this.mWaveformView.pixelsToMillisecs(this.mEndPos);
                }
                this.mPlayStartOffset = 0;
                int startFrame = this.mWaveformView.secondsToFrames(((double) this.mPlayStartMsec) * 0.001d);
                int endFrame = this.mWaveformView.secondsToFrames(((double) this.mPlayEndMsec) * 0.001d);
                int startByte = this.mSoundFile.getSeekableFrameOffset(startFrame);
                int endByte = this.mSoundFile.getSeekableFrameOffset(endFrame);
                if (this.mCanSeekAccurately && startByte >= 0 && endByte >= 0) {
                    try {
                        this.mPlayer.reset();
                        this.mPlayer.setAudioStreamType(3);
                        this.mPlayer.setDataSource(new FileInputStream(this.mFile.getAbsolutePath()).getFD(), (long) startByte, (long) (endByte - startByte));
                        this.mPlayer.prepare();
                        this.mPlayStartOffset = this.mPlayStartMsec;
                    } catch (Exception e) {
                        Exception exc = e;
                        System.out.println("Exception trying to play file subset");
                        this.mPlayer.reset();
                        this.mPlayer.setAudioStreamType(3);
                        this.mPlayer.setDataSource(this.mFile.getAbsolutePath());
                        this.mPlayer.prepare();
                        this.mPlayStartOffset = 0;
                    }
                }
                this.mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    public synchronized void onCompletion(MediaPlayer arg0) {
                        RingdroidEditActivity.this.handlePause();
                    }
                });
                this.mIsPlaying = true;
                if (this.mPlayStartOffset == 0) {
                    this.mPlayer.seekTo(this.mPlayStartMsec);
                }
                this.mPlayer.start();
                updateDisplay();
                enableDisableButtons();
            } catch (Exception e2) {
                showFinalAlert(e2, (int) R.string.play_error);
            }
        }
        return;
    }

    private void showFinalAlert(Exception e, CharSequence message) {
        CharSequence title;
        if (e != null) {
            Log.e("Ringdroid", "Error: " + ((Object) message));
            Log.e("Ringdroid", getStackTrace(e));
            title = getResources().getText(R.string.alert_title_failure);
            setResult(0, new Intent());
        } else {
            Log.i("Ringdroid", "Success: " + ((Object) message));
            title = getResources().getText(R.string.alert_title_success);
        }
        new AlertDialog.Builder(this).setTitle(title).setMessage(message).setPositiveButton((int) R.string.alert_ok_button, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                RingdroidEditActivity.this.finish();
            }
        }).setCancelable(false).show();
    }

    private void showFinalAlert(Exception e, int messageResourceId) {
        showFinalAlert(e, getResources().getText(messageResourceId));
    }

    private String makeRingtoneFilename(CharSequence title, String extension) {
        String parentdir;
        String testPath;
        switch (this.mNewFileKind) {
            case 1:
                parentdir = "/sdcard/media/audio/alarms";
                break;
            case 2:
                parentdir = "/sdcard/media/audio/notifications";
                break;
            case 3:
                parentdir = "/sdcard/media/audio/ringtones";
                break;
            default:
                parentdir = "/sdcard/media/audio/music";
                break;
        }
        File parentDirFile = new File(parentdir);
        parentDirFile.mkdirs();
        if (!parentDirFile.isDirectory()) {
            parentdir = "/sdcard";
        }
        String filename = Utils.EMPTY_STRING;
        for (int i = 0; i < title.length(); i++) {
            if (Character.isLetterOrDigit(title.charAt(i))) {
                filename = String.valueOf(filename) + title.charAt(i);
            }
        }
        int i2 = 0;
        while (i2 < 100) {
            if (i2 > 0) {
                testPath = String.valueOf(parentdir) + "/" + filename + i2 + extension;
            } else {
                testPath = String.valueOf(parentdir) + "/" + filename + extension;
            }
            try {
                new RandomAccessFile(new File(testPath), "r");
                i2++;
            } catch (Exception e) {
                return testPath;
            }
        }
        return null;
    }

    /* access modifiers changed from: private */
    public void saveRingtone(CharSequence title) {
        final String outPath = makeRingtoneFilename(title, this.mExtension);
        if (outPath == null) {
            showFinalAlert(new Exception(), (int) R.string.no_unique_filename);
            return;
        }
        this.mDstFilename = outPath;
        double startTime = this.mWaveformView.pixelsToSeconds(this.mStartPos);
        double endTime = this.mWaveformView.pixelsToSeconds(this.mEndPos);
        final int startFrame = this.mWaveformView.secondsToFrames(startTime);
        final int endFrame = this.mWaveformView.secondsToFrames(endTime);
        final int duration = (int) ((endTime - startTime) + 0.5d);
        this.mProgressDialog = new ProgressDialog(this);
        this.mProgressDialog.setProgressStyle(0);
        this.mProgressDialog.setTitle((int) R.string.progress_dialog_saving);
        this.mProgressDialog.setIndeterminate(true);
        this.mProgressDialog.setCancelable(false);
        this.mProgressDialog.show();
        final CharSequence charSequence = title;
        new Thread() {
            public void run() {
                CharSequence errorMessage;
                final File outFile = new File(outPath);
                try {
                    RingdroidEditActivity.this.mSoundFile.WriteFile(outFile, startFrame, endFrame - startFrame);
                    CheapSoundFile.create(outPath, new CheapSoundFile.ProgressListener() {
                        public boolean reportProgress(double frac) {
                            return true;
                        }
                    });
                    RingdroidEditActivity.this.mProgressDialog.dismiss();
                    final CharSequence charSequence = charSequence;
                    final String str = outPath;
                    final int i = duration;
                    RingdroidEditActivity.this.mHandler.post(new Runnable() {
                        public void run() {
                            RingdroidEditActivity.this.afterSavingRingtone(charSequence, str, outFile, i);
                        }
                    });
                } catch (Exception e) {
                    Exception e2 = e;
                    RingdroidEditActivity.this.mProgressDialog.dismiss();
                    if (e2.getMessage().equals("No space left on device")) {
                        errorMessage = RingdroidEditActivity.this.getResources().getText(R.string.no_space_error);
                        e2 = null;
                    } else {
                        errorMessage = RingdroidEditActivity.this.getResources().getText(R.string.write_error);
                    }
                    final CharSequence finalErrorMessage = errorMessage;
                    final Exception finalException = e2;
                    RingdroidEditActivity.this.mHandler.post(new Runnable() {
                        public void run() {
                            RingdroidEditActivity.this.handleFatalError("WriteError", finalErrorMessage, finalException);
                        }
                    });
                }
            }
        }.start();
    }

    /* access modifiers changed from: private */
    public void afterSavingRingtone(CharSequence title, String outPath, File outFile, int duration) {
        if (outFile.length() <= 512) {
            outFile.delete();
            new AlertDialog.Builder(this).setTitle((int) R.string.alert_title_failure).setMessage((int) R.string.too_small_error).setPositiveButton((int) R.string.alert_ok_button, (DialogInterface.OnClickListener) null).setCancelable(false).show();
            return;
        }
        long fileSize = outFile.length();
        String artist = new StringBuilder().append((Object) getResources().getText(R.string.artist_name)).toString();
        ContentValues values = new ContentValues();
        values.put("_data", outPath);
        values.put("title", title.toString());
        values.put("_size", Long.valueOf(fileSize));
        values.put("mime_type", "audio/mpeg");
        values.put("artist", artist);
        values.put("duration", Integer.valueOf(duration));
        values.put("is_ringtone", Boolean.valueOf(this.mNewFileKind == 3));
        values.put("is_notification", Boolean.valueOf(this.mNewFileKind == 2));
        values.put("is_alarm", Boolean.valueOf(this.mNewFileKind == 1));
        values.put("is_music", Boolean.valueOf(this.mNewFileKind == 0));
        Uri newUri = getContentResolver().insert(MediaStore.Audio.Media.getContentUriForPath(outPath), values);
        setResult(-1, new Intent().setData(newUri));
        SharedPreferences prefs = getPreferences(0);
        int successCount = prefs.getInt(PREF_SUCCESS_COUNT, 0);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putInt(PREF_SUCCESS_COUNT, successCount + 1);
        prefsEditor.commit();
        if (this.mWasGetContentIntent) {
            sendStatsToServerIfAllowedAndFinish();
        } else if (this.mNewFileKind == 0 || this.mNewFileKind == 1) {
            Toast.makeText(this, R.string.save_success_message, 0).show();
            sendStatsToServerIfAllowedAndFinish();
        } else if (this.mNewFileKind == 2) {
            final Uri uri = newUri;
            new AlertDialog.Builder(this).setTitle((int) R.string.alert_title_success).setMessage((int) R.string.set_default_notification).setPositiveButton((int) R.string.alert_yes_button, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    RingtoneManager.setActualDefaultRingtoneUri(RingdroidEditActivity.this, 2, uri);
                    RingdroidEditActivity.this.sendStatsToServerIfAllowedAndFinish();
                }
            }).setNegativeButton((int) R.string.alert_no_button, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    RingdroidEditActivity.this.sendStatsToServerIfAllowedAndFinish();
                }
            }).setCancelable(false).show();
        } else {
            final Uri uri2 = newUri;
            new AfterSaveActionDialog(this, Message.obtain(new Handler() {
                public void handleMessage(Message response) {
                    switch (response.arg1) {
                        case R.id.button_make_default:
                            RingtoneManager.setActualDefaultRingtoneUri(RingdroidEditActivity.this, 1, uri2);
                            Toast.makeText(RingdroidEditActivity.this, (int) R.string.default_ringtone_success_message, 0).show();
                            RingdroidEditActivity.this.sendStatsToServerIfAllowedAndFinish();
                            return;
                        case R.id.button_choose_contact:
                            RingdroidEditActivity.this.chooseContactForRingtone(uri2);
                            return;
                        default:
                            RingdroidEditActivity.this.sendStatsToServerIfAllowedAndFinish();
                            return;
                    }
                }
            })).show();
        }
    }

    /* access modifiers changed from: private */
    public void chooseContactForRingtone(Uri uri) {
        try {
            Intent intent = new Intent("android.intent.action.EDIT", uri);
            intent.setClassName(Constants.PKG_NAME, "net.cross.micplayer.editor.ChooseContactActivity");
            startActivityForResult(intent, 2);
        } catch (Exception e) {
            Log.e("Ringdroid", "EditActivity: Couldn't open Choose Contact window");
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 12 */
    /* access modifiers changed from: private */
    public void handleFatalError(final CharSequence errorInternalName, CharSequence errorString, final Exception exception) {
        Log.i("Ringdroid", "handleFatalError");
        SharedPreferences prefs = getPreferences(0);
        int failureCount = prefs.getInt(PREF_ERROR_COUNT, 0);
        final SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putInt(PREF_ERROR_COUNT, failureCount + 1);
        prefsEditor.commit();
        int serverAllowed = prefs.getInt(PREF_ERR_SERVER_ALLOWED, 0);
        if (serverAllowed == 1) {
            Log.i("Ringdroid", "ERR: SERVER_ALLOWED_NO");
            showFinalAlert(exception, errorString);
        } else if (serverAllowed == 2) {
            Log.i("Ringdroid", "SERVER_ALLOWED_YES");
            new AlertDialog.Builder(this).setTitle((int) R.string.alert_title_failure).setMessage(errorString).setPositiveButton((int) R.string.alert_ok_button, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    RingdroidEditActivity.this.sendErrToServerAndFinish(errorInternalName, exception);
                }
            }).setCancelable(false).show();
        } else {
            final int allowServerCheckIndex = prefs.getInt(PREF_ERR_SERVER_CHECK, 1);
            if (failureCount < allowServerCheckIndex) {
                Log.i("Ringdroid", "failureCount " + failureCount + " is less than " + allowServerCheckIndex);
                showFinalAlert(exception, errorString);
                return;
            }
            SpannableString message = new SpannableString(((Object) errorString) + ". " + ((Object) getResources().getText(R.string.error_server_prompt)));
            Linkify.addLinks(message, 15);
            ((TextView) new AlertDialog.Builder(this).setTitle((int) R.string.alert_title_failure).setMessage(message).setPositiveButton((int) R.string.server_yes, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    prefsEditor.putInt(RingdroidEditActivity.PREF_ERR_SERVER_ALLOWED, 2);
                    prefsEditor.commit();
                    RingdroidEditActivity.this.sendErrToServerAndFinish(errorInternalName, exception);
                }
            }).setNeutralButton((int) R.string.server_later, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    prefsEditor.putInt(RingdroidEditActivity.PREF_ERR_SERVER_CHECK, (allowServerCheckIndex * 2) + 1);
                    Log.i("Ringdroid", "Won't check again until " + ((allowServerCheckIndex * 2) + 1) + " errors.");
                    prefsEditor.commit();
                    RingdroidEditActivity.this.finish();
                }
            }).setNegativeButton((int) R.string.server_never, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    prefsEditor.putInt(RingdroidEditActivity.PREF_ERR_SERVER_ALLOWED, 1);
                    prefsEditor.commit();
                    RingdroidEditActivity.this.finish();
                }
            }).setCancelable(false).show().findViewById(16908299)).setMovementMethod(LinkMovementMethod.getInstance());
        }
    }

    /* access modifiers changed from: private */
    public void onSave() {
        if (this.mIsPlaying) {
            handlePause();
        }
        new FileSaveDialog(this, getResources(), this.mTitle, Message.obtain(new Handler() {
            public void handleMessage(Message response) {
                RingdroidEditActivity.this.mNewFileKind = response.arg1;
                RingdroidEditActivity.this.saveRingtone((CharSequence) response.obj);
            }
        })).show();
    }

    /* access modifiers changed from: private */
    public void enableZoomButtons() {
        this.mZoomInButton.setEnabled(this.mWaveformView.canZoomIn());
        this.mZoomOutButton.setEnabled(this.mWaveformView.canZoomOut());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.PrintWriter.<init>(java.io.OutputStream, boolean):void}
     arg types: [java.io.ByteArrayOutputStream, int]
     candidates:
      ClspMth{java.io.PrintWriter.<init>(java.io.File, java.lang.String):void throws java.io.FileNotFoundException, java.io.UnsupportedEncodingException}
      ClspMth{java.io.PrintWriter.<init>(java.lang.String, java.lang.String):void throws java.io.FileNotFoundException, java.io.UnsupportedEncodingException}
      ClspMth{java.io.PrintWriter.<init>(java.io.Writer, boolean):void}
      ClspMth{java.io.PrintWriter.<init>(java.io.OutputStream, boolean):void} */
    private String getStackTrace(Exception e) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        e.printStackTrace(new PrintWriter((OutputStream) stream, true));
        return stream.toString();
    }

    private String getExtensionFromFilename(String filename) {
        return filename.substring(filename.lastIndexOf(46), filename.length());
    }

    private String getFilenameFromUri(Uri uri) {
        Cursor c = managedQuery(uri, null, Utils.EMPTY_STRING, null, null);
        if (c.getCount() == 0) {
            return null;
        }
        c.moveToFirst();
        return c.getString(c.getColumnIndexOrThrow("_data"));
    }

    /* access modifiers changed from: private */
    public void sendStatsToServerIfAllowedAndFinish() {
        Log.i("Ringdroid", "sendStatsToServerIfAllowedAndFinish");
        SharedPreferences prefs = getPreferences(0);
        int serverAllowed = prefs.getInt(PREF_STATS_SERVER_ALLOWED, 0);
        if (serverAllowed == 1) {
            Log.i("Ringdroid", "SERVER_ALLOWED_NO");
            finish();
        } else if (serverAllowed == 2) {
            Log.i("Ringdroid", "SERVER_ALLOWED_YES");
            sendStatsToServerAndFinish();
        } else {
            int successCount = prefs.getInt(PREF_SUCCESS_COUNT, 0);
            int allowServerCheckIndex = prefs.getInt(PREF_STATS_SERVER_CHECK, 2);
            if (successCount < allowServerCheckIndex) {
                Log.i("Ringdroid", "successCount " + successCount + " is less than " + allowServerCheckIndex);
                finish();
                return;
            }
            showServerPrompt(false);
        }
    }

    /* access modifiers changed from: package-private */
    public void showServerPrompt(final boolean userInitiated) {
        final SharedPreferences prefs = getPreferences(0);
        SpannableString message = new SpannableString(getResources().getText(R.string.server_prompt));
        Linkify.addLinks(message, 15);
        ((TextView) new AlertDialog.Builder(this).setTitle((int) R.string.server_title).setMessage(message).setPositiveButton((int) R.string.server_yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                SharedPreferences.Editor prefsEditor = prefs.edit();
                prefsEditor.putInt(RingdroidEditActivity.PREF_STATS_SERVER_ALLOWED, 2);
                prefsEditor.commit();
                if (userInitiated) {
                    RingdroidEditActivity.this.finish();
                } else {
                    RingdroidEditActivity.this.sendStatsToServerAndFinish();
                }
            }
        }).setNeutralButton((int) R.string.server_later, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                int allowServerCheckIndex = prefs.getInt(RingdroidEditActivity.PREF_STATS_SERVER_CHECK, 2);
                int successCount = prefs.getInt(RingdroidEditActivity.PREF_SUCCESS_COUNT, 0);
                SharedPreferences.Editor prefsEditor = prefs.edit();
                if (userInitiated) {
                    prefsEditor.putInt(RingdroidEditActivity.PREF_STATS_SERVER_CHECK, successCount + 2);
                } else {
                    prefsEditor.putInt(RingdroidEditActivity.PREF_STATS_SERVER_CHECK, allowServerCheckIndex * 2);
                }
                prefsEditor.commit();
                RingdroidEditActivity.this.finish();
            }
        }).setNegativeButton((int) R.string.server_never, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                SharedPreferences.Editor prefsEditor = prefs.edit();
                prefsEditor.putInt(RingdroidEditActivity.PREF_STATS_SERVER_ALLOWED, 1);
                if (userInitiated) {
                    prefsEditor.putInt(RingdroidEditActivity.PREF_ERR_SERVER_ALLOWED, 1);
                }
                prefsEditor.commit();
                RingdroidEditActivity.this.finish();
            }
        }).setCancelable(false).show().findViewById(16908299)).setMovementMethod(LinkMovementMethod.getInstance());
    }

    /* access modifiers changed from: package-private */
    public void sendStatsToServerAndFinish() {
        Log.i("Ringdroid", "sendStatsToServerAndFinish");
        new Thread() {
            public void run() {
                RingdroidEditActivity.this.sendToServer(RingdroidEditActivity.STATS_SERVER_URL, null, null);
            }
        }.start();
        Log.i("Ringdroid", "sendStatsToServerAndFinish calling finish");
        finish();
    }

    /* access modifiers changed from: package-private */
    public void sendErrToServerAndFinish(final CharSequence errType, final Exception exception) {
        Log.i("Ringdroid", "sendErrToServerAndFinish");
        new Thread() {
            public void run() {
                RingdroidEditActivity.this.sendToServer(RingdroidEditActivity.ERR_SERVER_URL, errType, exception);
            }
        }.start();
        Log.i("Ringdroid", "sendErrToServerAndFinish calling finish");
        finish();
    }

    /* access modifiers changed from: package-private */
    public long getUniqueId() {
        SharedPreferences prefs = getPreferences(0);
        long uniqueId = prefs.getLong(PREF_UNIQUE_ID, 0);
        if (uniqueId != 0) {
            return uniqueId;
        }
        long uniqueId2 = new Random().nextLong();
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putLong(PREF_UNIQUE_ID, uniqueId2);
        prefsEditor.commit();
        return uniqueId2;
    }

    /* access modifiers changed from: package-private */
    public void sendToServer(String serverUrl, CharSequence errType, Exception exception) {
        boolean isSuccess;
        String md5;
        if (this.mTitle != null) {
            Log.i("Ringdroid", "sendStatsToServer");
            if (exception == null) {
                isSuccess = true;
            } else {
                isSuccess = false;
            }
            StringBuilder postMessage = new StringBuilder();
            String ringdroidVersion = "unknown";
            try {
                ringdroidVersion = getPackageManager().getPackageInfo(getPackageName(), -1).versionName;
            } catch (PackageManager.NameNotFoundException e) {
            }
            postMessage.append("ringdroid_version=");
            postMessage.append(URLEncoder.encode(ringdroidVersion));
            postMessage.append("&android_version=");
            postMessage.append(URLEncoder.encode(Build.VERSION.RELEASE));
            postMessage.append("&unique_id=");
            postMessage.append(getUniqueId());
            postMessage.append("&accurate_seek=");
            postMessage.append(this.mCanSeekAccurately);
            if (isSuccess) {
                postMessage.append("&title=");
                postMessage.append(URLEncoder.encode(this.mTitle));
                if (this.mArtist != null) {
                    postMessage.append("&artist=");
                    postMessage.append(URLEncoder.encode(this.mArtist));
                }
                if (this.mAlbum != null) {
                    postMessage.append("&album=");
                    postMessage.append(URLEncoder.encode(this.mAlbum));
                }
                if (this.mGenre != null) {
                    postMessage.append("&genre=");
                    postMessage.append(URLEncoder.encode(this.mGenre));
                }
                postMessage.append("&year=");
                postMessage.append(this.mYear);
                postMessage.append("&filename=");
                postMessage.append(URLEncoder.encode(this.mFilename));
                postMessage.append("&user_lat=");
                postMessage.append(URLEncoder.encode(new StringBuilder().append(0.0d).toString()));
                postMessage.append("&user_lon=");
                postMessage.append(URLEncoder.encode(new StringBuilder().append(0.0d).toString()));
                int successCount = getPreferences(0).getInt(PREF_SUCCESS_COUNT, 0);
                postMessage.append("&success_count=");
                postMessage.append(URLEncoder.encode(new StringBuilder().append(successCount).toString()));
                postMessage.append("&bitrate=");
                postMessage.append(URLEncoder.encode(new StringBuilder().append(this.mSoundFile.getAvgBitrateKbps()).toString()));
                postMessage.append("&channels=");
                postMessage.append(URLEncoder.encode(new StringBuilder().append(this.mSoundFile.getChannels()).toString()));
                try {
                    md5 = this.mSoundFile.computeMd5OfFirst10Frames();
                } catch (Exception e2) {
                    md5 = Utils.EMPTY_STRING;
                }
                postMessage.append("&md5=");
                postMessage.append(URLEncoder.encode(md5));
            } else {
                postMessage.append("&err_type=");
                postMessage.append(errType);
                postMessage.append("&err_str=");
                postMessage.append(URLEncoder.encode(getStackTrace(exception)));
                postMessage.append("&src_filename=");
                postMessage.append(URLEncoder.encode(this.mFilename));
                if (this.mDstFilename != null) {
                    postMessage.append("&dst_filename=");
                    postMessage.append(URLEncoder.encode(this.mDstFilename));
                }
            }
            if (this.mSoundFile != null) {
                double framesToSecs = 0.0d;
                double sampleRate = (double) this.mSoundFile.getSampleRate();
                if (sampleRate > 0.0d) {
                    framesToSecs = (((double) this.mSoundFile.getSamplesPerFrame()) * 1.0d) / sampleRate;
                }
                double songLen = framesToSecs * ((double) this.mSoundFile.getNumFrames());
                postMessage.append("&songlen=");
                postMessage.append(URLEncoder.encode(new StringBuilder().append(songLen).toString()));
                postMessage.append("&sound_type=");
                postMessage.append(URLEncoder.encode(this.mSoundFile.getFiletype()));
                double clipStart = ((double) this.mStartPos) * framesToSecs;
                double clipLen = ((double) (this.mEndPos - this.mStartPos)) * framesToSecs;
                postMessage.append("&clip_start=");
                postMessage.append(URLEncoder.encode(new StringBuilder().append(clipStart).toString()));
                postMessage.append("&clip_len=");
                postMessage.append(URLEncoder.encode(new StringBuilder().append(clipLen).toString()));
            }
            String fileKindName = FileSaveDialog.KindToName(this.mNewFileKind);
            postMessage.append("&clip_kind=");
            postMessage.append(URLEncoder.encode(fileKindName));
            Log.i("Ringdroid", postMessage.toString());
            try {
                HttpParams httpParams = new BasicHttpParams();
                HttpConnectionParams.setConnectionTimeout(httpParams, Util.DOWNLOAD_APP_DIG);
                HttpConnectionParams.setSoTimeout(httpParams, Util.DOWNLOAD_APP_DIG);
                DefaultHttpClient defaultHttpClient = new DefaultHttpClient(httpParams);
                HttpPost httpPost = new HttpPost(serverUrl);
                httpPost.setEntity(new ByteArrayEntity(postMessage.toString().getBytes("UTF8")));
                Log.i("Ringdroid", "Executing request");
                Log.i("Ringdroid", "Response: " + defaultHttpClient.execute(httpPost).toString());
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        }
    }
}
