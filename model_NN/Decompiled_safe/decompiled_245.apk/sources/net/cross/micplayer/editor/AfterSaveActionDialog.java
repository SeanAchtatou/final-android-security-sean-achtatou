package net.cross.micplayer.editor;

import android.app.Dialog;
import android.content.Context;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import net.cross.micplayer.R;

public class AfterSaveActionDialog extends Dialog {
    private Message mResponse;

    public AfterSaveActionDialog(Context context, Message response) {
        super(context);
        setContentView((int) R.layout.after_save_action);
        setTitle((int) R.string.alert_title_success);
        ((Button) findViewById(R.id.button_make_default)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                AfterSaveActionDialog.this.closeAndSendResult(R.id.button_make_default);
            }
        });
        ((Button) findViewById(R.id.button_choose_contact)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                AfterSaveActionDialog.this.closeAndSendResult(R.id.button_choose_contact);
            }
        });
        ((Button) findViewById(R.id.button_do_nothing)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                AfterSaveActionDialog.this.closeAndSendResult(R.id.button_do_nothing);
            }
        });
        this.mResponse = response;
    }

    /* access modifiers changed from: private */
    public void closeAndSendResult(int clickedButtonId) {
        this.mResponse.arg1 = clickedButtonId;
        this.mResponse.sendToTarget();
        dismiss();
    }
}
