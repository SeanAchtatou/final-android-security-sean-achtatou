package com.millennialmedia.android;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.Serializable;
import net.cross.micplayer.utils.Constants;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class VideoLogEvent implements Parcelable, Serializable {
    public static final Parcelable.Creator<VideoLogEvent> CREATOR = new Parcelable.Creator<VideoLogEvent>() {
        public VideoLogEvent createFromParcel(Parcel in) {
            return new VideoLogEvent(in);
        }

        public VideoLogEvent[] newArray(int size) {
            return new VideoLogEvent[size];
        }
    };
    String[] activities;
    long position;

    VideoLogEvent() {
    }

    VideoLogEvent(JSONObject logObject) throws JSONException {
        deserializeFromObj(logObject);
    }

    VideoLogEvent(Parcel in) {
        try {
            this.position = in.readLong();
            this.activities = new String[in.readInt()];
            in.readStringArray(this.activities);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: package-private */
    public void deserializeFromObj(JSONObject logObject) throws JSONException {
        try {
            if (logObject.has("time")) {
                this.position = (long) (logObject.getInt("time") * Constants.MAX_DOWNLOADS);
            }
            if (logObject.has("urls")) {
                JSONArray jsonArray = logObject.getJSONArray("urls");
                this.activities = new String[jsonArray.length()];
                for (int i = 0; i < jsonArray.length(); i++) {
                    this.activities[i] = jsonArray.getString(i);
                }
                return;
            }
            this.activities = new String[0];
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.position);
        dest.writeInt(this.activities.length);
        dest.writeStringArray(this.activities);
    }
}
