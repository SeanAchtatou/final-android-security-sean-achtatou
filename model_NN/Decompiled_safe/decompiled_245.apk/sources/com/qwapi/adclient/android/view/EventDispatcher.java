package com.qwapi.adclient.android.view;

import android.app.Activity;
import com.qwapi.adclient.android.data.Ad;
import com.qwapi.adclient.android.data.Status;
import com.qwapi.adclient.android.requestparams.AdRequestParams;

public class EventDispatcher {
    /* access modifiers changed from: private */
    public Activity activity;
    /* access modifiers changed from: private */
    public AdEventsListener eventListener;
    private boolean runOnUIThread;

    public EventDispatcher(Activity activity2, AdEventsListener adEventsListener, boolean z) {
        this.activity = activity2;
        this.eventListener = adEventsListener;
        this.runOnUIThread = this.activity != null && z;
    }

    public Activity getActivity() {
        return this.activity;
    }

    public AdEventsListener getEventListener() {
        return this.eventListener;
    }

    public boolean isRunOnUIThread() {
        return this.runOnUIThread;
    }

    /* access modifiers changed from: package-private */
    public void onAdClick(final Ad ad) {
        if (this.eventListener != null && ad != null) {
            if (this.runOnUIThread) {
                this.activity.runOnUiThread(new Runnable() {
                    public void run() {
                        EventDispatcher.this.eventListener.onAdClick(EventDispatcher.this.activity, ad);
                    }
                });
            } else {
                this.eventListener.onAdClick(this.activity, ad);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void onAdRequest(final AdRequestParams adRequestParams) {
        if (this.eventListener != null) {
            if (this.runOnUIThread) {
                this.activity.runOnUiThread(new Runnable() {
                    public void run() {
                        EventDispatcher.this.eventListener.onAdRequest(EventDispatcher.this.activity, adRequestParams);
                    }
                });
            } else {
                this.eventListener.onAdRequest(this.activity, adRequestParams);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void onAdRequestFailed(final AdRequestParams adRequestParams, final Status status) {
        if (this.eventListener != null) {
            if (this.runOnUIThread) {
                this.activity.runOnUiThread(new Runnable() {
                    public void run() {
                        EventDispatcher.this.eventListener.onAdRequestFailed(EventDispatcher.this.activity, adRequestParams, status);
                    }
                });
            } else {
                this.eventListener.onAdRequestFailed(this.activity, adRequestParams, status);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void onAdRequestSuccessful(final AdRequestParams adRequestParams, final Ad ad) {
        if (this.eventListener != null && ad != null) {
            if (this.runOnUIThread) {
                this.activity.runOnUiThread(new Runnable() {
                    public void run() {
                        EventDispatcher.this.eventListener.onAdRequestSuccessful(EventDispatcher.this.activity, adRequestParams, ad);
                    }
                });
            } else {
                this.eventListener.onAdRequestSuccessful(this.activity, adRequestParams, ad);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void onDisplayAd(final Ad ad) {
        if (this.eventListener != null && ad != null) {
            if (this.runOnUIThread) {
                this.activity.runOnUiThread(new Runnable() {
                    public void run() {
                        EventDispatcher.this.eventListener.onDisplayAd(EventDispatcher.this.activity, ad);
                    }
                });
            } else {
                this.eventListener.onDisplayAd(this.activity, ad);
            }
        }
    }

    public void setActivity(Activity activity2) {
        this.activity = activity2;
    }

    public void setEventListener(AdEventsListener adEventsListener) {
        this.eventListener = adEventsListener;
    }

    public void setRunOnUIThread(boolean z) {
        this.runOnUIThread = z;
    }
}
