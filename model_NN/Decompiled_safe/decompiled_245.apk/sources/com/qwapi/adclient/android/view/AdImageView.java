package com.qwapi.adclient.android.view;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import com.qwapi.adclient.android.utils.Utils;

public class AdImageView extends ImageView {
    private String clickUrl;

    public AdImageView(Context context, Drawable drawable, String str) {
        super(context);
        init(context, drawable, null, str);
    }

    public AdImageView(Context context, AttributeSet attributeSet, Drawable drawable, String str) {
        super(context, attributeSet);
        init(context, drawable, null, str);
    }

    public AdImageView(Context context, AttributeSet attributeSet, String str, String str2) {
        super(context, attributeSet);
        init(context, null, str, str2);
    }

    public AdImageView(Context context, String str, String str2) {
        super(context);
        init(context, null, str, str2);
    }

    private void init(Context context, Drawable drawable, String str, final String str2) {
        if (drawable != null) {
            setImageDrawable(drawable);
        }
        if (Utils.isGoodString(str2)) {
            setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    Utils.invokeLandingPage(view, str2);
                }
            });
        }
    }

    public String getClickUrl() {
        return this.clickUrl;
    }

    public void setClickUrl(String str) {
        this.clickUrl = str;
    }
}
