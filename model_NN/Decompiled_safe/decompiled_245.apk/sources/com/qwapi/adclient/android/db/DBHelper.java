package com.qwapi.adclient.android.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import com.qwapi.adclient.android.AdApiConstants;
import com.qwapi.adclient.android.data.Ad;
import com.qwapi.adclient.android.data.AdResponse;
import com.qwapi.adclient.android.data.Data;
import com.qwapi.adclient.android.data.Image;
import com.qwapi.adclient.android.data.Status;
import com.qwapi.adclient.android.data.Text;
import com.qwapi.adclient.android.requestparams.Placement;
import com.qwapi.adclient.android.utils.Utils;
import java.util.ArrayList;
import java.util.List;

public class DBHelper {
    private static final String ACTION_TEXT = "action_text";
    private static final String[] AD_COLS = {DBID, AD_ID, AD_TYPE, "placement", "section", CLICK_URL, IMAGE_URL, IMAGE_TEXT, IMAGE_HEIGHT, IMAGE_WIDTH, DATA_URL, DATA_HEIGHT, DATA_WIDTH, TEXT_BODY, TEXT_HEADLINE, AD_TEXT, ACTION_TEXT, ISBOT, XHOSTED, EXPIRATION_TS, BATCHID, IMPRESSION_COUNT, LAST_IMPRESSION_TS, CONFIRMATION_PIXEL, PIXEL_COUNT};
    private static final String AD_ID = "ad_id";
    public static final String AD_TABLE = "qw_ad";
    private static final String AD_TEXT = "ad_text";
    private static final String AD_TYPE = "ad_type";
    private static final String AD_TYPE_QUERY_PART = "ad_type = '";
    private static final String BATCHID = "batchid";
    /* access modifiers changed from: private */
    public static final String CLASSNAME = DBHelper.class.getSimpleName();
    private static final String CLICK_URL = "click_url";
    private static final String CLOSE_PAREN = ")";
    private static final String CONFIRMATION_PIXEL = "confirmation_pixel";
    private static final String CONFIRM_IMAGE = "confirm_1x1";
    private static final String DATA_HEIGHT = "data_height";
    private static final String DATA_URL = "data_url";
    private static final String DATA_WIDTH = "data_width";
    private static final String DBID = "dbid";
    public static final String DB_NAME = "qw_ad.db";
    public static final int DB_VERSION = 1;
    private static final String DELETE_AD_QUERY = "dbid = ?";
    private static final String DELETE_EXPIRED_ADS_QUERY = "expiration_ts < ?";
    private static final String DELETE_PIXEL_QUERY = "dbid = ?";
    private static final String EXPIRATION_TS = "expiration_ts";
    private static final String IMAGE_HEIGHT = "image_height";
    private static final String IMAGE_TEXT = "image_text";
    private static final String IMAGE_URL = "image_url";
    private static final String IMAGE_WIDTH = "image_width";
    private static final String IMPRESSION_COUNT = "impression_count";
    private static final String ISBOT = "isbot";
    private static final String LAST_IMPRESSION_TS = "last_impression_ts";
    private static final String NONE = "none";
    private static final String OPEN_PAREN = "(";
    private static final String OR = " OR ";
    private static final String ORDER_BY = "last_impression_ts desc";
    private static final String PIXEL = "pixel";
    private static final String[] PIXEL_COLS = {DBID, PIXEL};
    private static final String PIXEL_COUNT = "pixel_count";
    public static final String PIXEL_TABLE = "qw_pixel";
    private static final String PLACEMENT = "placement";
    private static final String SECTION = "section";
    private static final String SELECT_AD_QUERY = " AND placement = ? AND section = ? AND expiration_ts > ?";
    private static final String SELECT_EXPIRED_ADS_QUERY = "expiration_ts < ?";
    private static final String SELECT_PIXEL_QUERY = "dbid = ?";
    private static final String SINGLE_QUOTE = "'";
    private static final String TEXT_BODY = "text_body";
    private static final String TEXT_HEADLINE = "text_headline";
    private static final String UPDATE_AD_QUERY = "dbid = ?";
    private static final String XHOSTED = "xhosted";
    private SQLiteDatabase db;
    private final DBOpenHelper dbOpenHelper;

    private static class DBOpenHelper extends SQLiteOpenHelper {
        private static final String AD_TABLE_CREATE = "CREATE TABLE qw_ad (dbid INTEGER PRIMARY KEY, ad_id TEXT NOT NULL, ad_type TEXT NOT NULL, placement TEXT NOT NULL, section TEXT NOT NULL, click_url TEXT NOT NULL, image_url TEXT, image_text TEXT, image_height INTEGER, image_width INTEGER, data_url TEXT, data_height INTEGER, data_width INTEGER, text_body TEXT, text_headline TEXT, ad_text TEXT, action_text TEXT, isbot INTEGER, xhosted INTEGER, expiration_ts REAL NOT NULL, batchid TEXT NOT NULL, impression_count INTEGER, last_impression_ts REAL NOT NULL, confirmation_pixel TEXT NOT NULL, pixel_count INTEGER)";
        private static final String DROP_TABLE_QUERY = "DROP TABLE IF EXISTS ";
        private static final String PIXEL_TABLE_CREATE = "CREATE TABLE qw_pixel (dbid INTEGER, pixel TEXT)";

        public DBOpenHelper(Context context, String str, int i) {
            super(context, DBHelper.DB_NAME, (SQLiteDatabase.CursorFactory) null, 1);
        }

        public void onCreate(SQLiteDatabase sQLiteDatabase) {
            Log.d(AdApiConstants.SDK, "Creating Database");
            try {
                sQLiteDatabase.execSQL(AD_TABLE_CREATE);
                sQLiteDatabase.execSQL(PIXEL_TABLE_CREATE);
            } catch (SQLException e) {
                Log.e(AdApiConstants.SDK, DBHelper.CLASSNAME, e);
            }
        }

        public void onOpen(SQLiteDatabase sQLiteDatabase) {
            Log.d(AdApiConstants.SDK, "Opening Database");
            super.onOpen(sQLiteDatabase);
        }

        public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
            Log.d(AdApiConstants.SDK, "upgrading database");
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS qw_pixel");
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS qw_ad");
            onCreate(sQLiteDatabase);
        }
    }

    public DBHelper(Context context) {
        this.dbOpenHelper = new DBOpenHelper(context, DB_NAME, 1);
        establisDb();
    }

    private void establisDb() {
        if (this.db == null) {
            this.db = this.dbOpenHelper.getWritableDatabase();
        }
    }

    public void cleanup() {
        if (this.db != null) {
            this.db.close();
            this.db = null;
        }
    }

    public void deleteAd(Ad ad) {
        String[] strArr = {Integer.toString(ad.getDbId())};
        if (ad.getTrackingPixels().size() > 1) {
            this.db.delete(PIXEL_TABLE, "dbid = ?", strArr);
        }
        this.db.delete(AD_TABLE, "dbid = ?", strArr);
    }

    public void deleteExpiredAds() {
        Cursor cursor;
        String[] strArr = {Long.toString(System.currentTimeMillis())};
        try {
            Cursor query = this.db.query(true, AD_TABLE, AD_COLS, "expiration_ts < ?", strArr, null, null, null, null);
            try {
                int count = query.getCount();
                query.moveToFirst();
                for (int i = 0; i < count; i++) {
                    this.db.delete(PIXEL_TABLE, "dbid = ?", new String[]{query.getString(0)});
                }
                this.db.delete(AD_TABLE, "expiration_ts < ?", strArr);
                if (query != null && !query.isClosed()) {
                    query.close();
                }
            } catch (SQLException e) {
                SQLException sQLException = e;
                cursor = query;
                e = sQLException;
                try {
                    Log.v(AdApiConstants.SDK, CLASSNAME, e);
                    if (cursor != null && !cursor.isClosed()) {
                        cursor.close();
                    }
                } catch (Throwable th) {
                    th = th;
                    cursor.close();
                    throw th;
                }
            } catch (Throwable th2) {
                Throwable th3 = th2;
                cursor = query;
                th = th3;
                if (cursor != null && !cursor.isClosed()) {
                    cursor.close();
                }
                throw th;
            }
        } catch (SQLException e2) {
            e = e2;
            cursor = null;
        } catch (Throwable th4) {
            th = th4;
            cursor = null;
            cursor.close();
            throw th;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:80:0x0245, code lost:
        r4 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x0246, code lost:
        r5 = null;
        r6 = r14;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x0255, code lost:
        r4 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x0256, code lost:
        r5 = null;
        r6 = null;
        r7 = r14;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x0245 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:17:0x008b] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.qwapi.adclient.android.data.AdResponse getAd(java.util.Collection<com.qwapi.adclient.android.requestparams.MediaType> r22, com.qwapi.adclient.android.requestparams.Placement r23, java.lang.String r24) {
        /*
            r21 = this;
            java.lang.String r4 = "QuattroWirelessSDK/2.1"
            java.lang.String r5 = "getAd from database"
            android.util.Log.d(r4, r5)
            r14 = 0
            r15 = 0
            r16 = 0
            long r4 = java.lang.System.currentTimeMillis()
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.util.Iterator r6 = r22.iterator()
        L_0x0018:
            boolean r7 = r6.hasNext()
            if (r7 == 0) goto L_0x0049
            java.lang.Object r22 = r6.next()
            com.qwapi.adclient.android.requestparams.MediaType r22 = (com.qwapi.adclient.android.requestparams.MediaType) r22
            int r7 = r8.length()
            if (r7 <= 0) goto L_0x0043
            java.lang.String r7 = " OR "
            r8.append(r7)
        L_0x002f:
            java.lang.String r7 = "ad_type = '"
            java.lang.StringBuilder r7 = r8.append(r7)
            java.lang.String r9 = r22.toNamedString()
            java.lang.StringBuilder r7 = r7.append(r9)
            java.lang.String r9 = "'"
            r7.append(r9)
            goto L_0x0018
        L_0x0043:
            java.lang.String r7 = "("
            r8.append(r7)
            goto L_0x002f
        L_0x0049:
            java.lang.String r6 = ")"
            r8.append(r6)
            java.lang.String r6 = " AND placement = ? AND section = ? AND expiration_ts > ?"
            r8.append(r6)
            r6 = 3
            java.lang.String[] r9 = new java.lang.String[r6]
            r6 = 0
            if (r23 == 0) goto L_0x01d3
            java.lang.String r7 = r23.toString()
        L_0x005d:
            r9[r6] = r7
            r6 = 1
            if (r24 != 0) goto L_0x01d7
            java.lang.String r7 = "none"
        L_0x0064:
            r9[r6] = r7
            r6 = 2
            java.lang.String r4 = java.lang.Long.toString(r4)
            r9[r6] = r4
            java.util.ArrayList r17 = new java.util.ArrayList
            r17.<init>()
            r18 = 0
            r0 = r21
            android.database.sqlite.SQLiteDatabase r0 = r0.db     // Catch:{ SQLException -> 0x0207, all -> 0x022b }
            r4 = r0
            r5 = 1
            java.lang.String r6 = "qw_ad"
            java.lang.String[] r7 = com.qwapi.adclient.android.db.DBHelper.AD_COLS     // Catch:{ SQLException -> 0x0207, all -> 0x022b }
            java.lang.String r8 = r8.toString()     // Catch:{ SQLException -> 0x0207, all -> 0x022b }
            r10 = 0
            r11 = 0
            java.lang.String r12 = "last_impression_ts desc"
            r13 = 0
            android.database.Cursor r14 = r4.query(r5, r6, r7, r8, r9, r10, r11, r12, r13)     // Catch:{ SQLException -> 0x0207, all -> 0x022b }
            com.qwapi.adclient.android.data.Ad r19 = new com.qwapi.adclient.android.data.Ad     // Catch:{ SQLException -> 0x0255, all -> 0x0245 }
            r19.<init>()     // Catch:{ SQLException -> 0x0255, all -> 0x0245 }
            int r4 = r14.getCount()     // Catch:{ SQLException -> 0x0255, all -> 0x0245 }
            if (r4 <= 0) goto L_0x026d
            r14.moveToFirst()     // Catch:{ SQLException -> 0x0255, all -> 0x0245 }
            r4 = 0
            int r4 = r14.getInt(r4)     // Catch:{ SQLException -> 0x0255, all -> 0x0245 }
            r0 = r19
            r1 = r4
            r0.setDbId(r1)     // Catch:{ SQLException -> 0x0255, all -> 0x0245 }
            r4 = 1
            java.lang.String r4 = r14.getString(r4)     // Catch:{ SQLException -> 0x0255, all -> 0x0245 }
            r0 = r19
            r1 = r4
            r0.setId(r1)     // Catch:{ SQLException -> 0x0255, all -> 0x0245 }
            r4 = 2
            java.lang.String r4 = r14.getString(r4)     // Catch:{ SQLException -> 0x0255, all -> 0x0245 }
            r0 = r19
            r1 = r4
            r0.setAdType(r1)     // Catch:{ SQLException -> 0x0255, all -> 0x0245 }
            r4 = 5
            java.lang.String r4 = r14.getString(r4)     // Catch:{ SQLException -> 0x0255, all -> 0x0245 }
            r0 = r19
            r1 = r4
            r0.setClickUrl(r1)     // Catch:{ SQLException -> 0x0255, all -> 0x0245 }
            r4 = 16
            java.lang.String r4 = r14.getString(r4)     // Catch:{ SQLException -> 0x0255, all -> 0x0245 }
            r0 = r19
            r1 = r4
            r0.setAdText(r1)     // Catch:{ SQLException -> 0x0255, all -> 0x0245 }
            r4 = 17
            java.lang.String r4 = r14.getString(r4)     // Catch:{ SQLException -> 0x0255, all -> 0x0245 }
            r0 = r19
            r1 = r4
            r0.setActionText(r1)     // Catch:{ SQLException -> 0x0255, all -> 0x0245 }
            r4 = 18
            int r4 = r14.getInt(r4)     // Catch:{ SQLException -> 0x0255, all -> 0x0245 }
            r5 = 1
            if (r4 != r5) goto L_0x01db
            r4 = 1
        L_0x00e7:
            r0 = r19
            r1 = r4
            r0.setBot(r1)     // Catch:{ SQLException -> 0x0255, all -> 0x0245 }
            r4 = 21
            int r4 = r14.getInt(r4)     // Catch:{ SQLException -> 0x0255, all -> 0x0245 }
            r0 = r19
            r1 = r4
            r0.setImpressionCount(r1)     // Catch:{ SQLException -> 0x0255, all -> 0x0245 }
            r4 = 22
            int r4 = r14.getInt(r4)     // Catch:{ SQLException -> 0x0255, all -> 0x0245 }
            r0 = r19
            r1 = r4
            r0.setLastServed(r1)     // Catch:{ SQLException -> 0x0255, all -> 0x0245 }
            r4 = 19
            long r4 = r14.getLong(r4)     // Catch:{ SQLException -> 0x0255, all -> 0x0245 }
            r0 = r19
            r1 = r4
            r0.setExpiry(r1)     // Catch:{ SQLException -> 0x0255, all -> 0x0245 }
            com.qwapi.adclient.android.data.Image r4 = new com.qwapi.adclient.android.data.Image     // Catch:{ SQLException -> 0x0255, all -> 0x0245 }
            r5 = 6
            java.lang.String r5 = r14.getString(r5)     // Catch:{ SQLException -> 0x0255, all -> 0x0245 }
            r6 = 9
            int r6 = r14.getInt(r6)     // Catch:{ SQLException -> 0x0255, all -> 0x0245 }
            r7 = 8
            int r7 = r14.getInt(r7)     // Catch:{ SQLException -> 0x0255, all -> 0x0245 }
            r8 = 7
            java.lang.String r8 = r14.getString(r8)     // Catch:{ SQLException -> 0x0255, all -> 0x0245 }
            r4.<init>(r5, r6, r7, r8)     // Catch:{ SQLException -> 0x0255, all -> 0x0245 }
            r0 = r19
            r1 = r4
            r0.setImage(r1)     // Catch:{ SQLException -> 0x0255, all -> 0x0245 }
            com.qwapi.adclient.android.data.Data r4 = new com.qwapi.adclient.android.data.Data     // Catch:{ SQLException -> 0x0255, all -> 0x0245 }
            r5 = 10
            java.lang.String r5 = r14.getString(r5)     // Catch:{ SQLException -> 0x0255, all -> 0x0245 }
            r6 = 11
            int r6 = r14.getInt(r6)     // Catch:{ SQLException -> 0x0255, all -> 0x0245 }
            r7 = 12
            int r7 = r14.getInt(r7)     // Catch:{ SQLException -> 0x0255, all -> 0x0245 }
            r4.<init>(r5, r6, r7)     // Catch:{ SQLException -> 0x0255, all -> 0x0245 }
            r0 = r19
            r1 = r4
            r0.setData(r1)     // Catch:{ SQLException -> 0x0255, all -> 0x0245 }
            com.qwapi.adclient.android.data.Text r4 = new com.qwapi.adclient.android.data.Text     // Catch:{ SQLException -> 0x0255, all -> 0x0245 }
            r5 = 13
            java.lang.String r5 = r14.getString(r5)     // Catch:{ SQLException -> 0x0255, all -> 0x0245 }
            r6 = 14
            java.lang.String r6 = r14.getString(r6)     // Catch:{ SQLException -> 0x0255, all -> 0x0245 }
            r4.<init>(r5, r6)     // Catch:{ SQLException -> 0x0255, all -> 0x0245 }
            r0 = r19
            r1 = r4
            r0.setText(r1)     // Catch:{ SQLException -> 0x0255, all -> 0x0245 }
            r4 = 23
            java.lang.String r4 = r14.getString(r4)     // Catch:{ SQLException -> 0x0255, all -> 0x0245 }
            r0 = r17
            r1 = r4
            r0.add(r1)     // Catch:{ SQLException -> 0x0255, all -> 0x0245 }
            r4 = 24
            int r4 = r14.getInt(r4)     // Catch:{ SQLException -> 0x0255, all -> 0x0245 }
            com.qwapi.adclient.android.data.AdResponse r5 = new com.qwapi.adclient.android.data.AdResponse     // Catch:{ SQLException -> 0x0255, all -> 0x0245 }
            com.qwapi.adclient.android.data.Status r6 = com.qwapi.adclient.android.data.Status.getSuccess()     // Catch:{ SQLException -> 0x0255, all -> 0x0245 }
            r7 = 20
            java.lang.String r7 = r14.getString(r7)     // Catch:{ SQLException -> 0x0255, all -> 0x0245 }
            r0 = r5
            r1 = r19
            r2 = r6
            r3 = r7
            r0.<init>(r1, r2, r3)     // Catch:{ SQLException -> 0x0255, all -> 0x0245 }
            r16 = r5
        L_0x018e:
            if (r16 == 0) goto L_0x0205
            r5 = 1
            if (r4 <= r5) goto L_0x01fe
            r4 = 1
            java.lang.String[] r9 = new java.lang.String[r4]     // Catch:{ SQLException -> 0x025b, all -> 0x0245 }
            r4 = 0
            int r5 = r19.getDbId()     // Catch:{ SQLException -> 0x025b, all -> 0x0245 }
            long r5 = (long) r5     // Catch:{ SQLException -> 0x025b, all -> 0x0245 }
            java.lang.String r5 = java.lang.Long.toString(r5)     // Catch:{ SQLException -> 0x025b, all -> 0x0245 }
            r9[r4] = r5     // Catch:{ SQLException -> 0x025b, all -> 0x0245 }
            r0 = r21
            android.database.sqlite.SQLiteDatabase r0 = r0.db     // Catch:{ SQLException -> 0x025b, all -> 0x0245 }
            r4 = r0
            r5 = 1
            java.lang.String r6 = "qw_pixel"
            java.lang.String[] r7 = com.qwapi.adclient.android.db.DBHelper.PIXEL_COLS     // Catch:{ SQLException -> 0x025b, all -> 0x0245 }
            java.lang.String r8 = "dbid = ?"
            r10 = 0
            r11 = 0
            r12 = 0
            r13 = 0
            android.database.Cursor r4 = r4.query(r5, r6, r7, r8, r9, r10, r11, r12, r13)     // Catch:{ SQLException -> 0x025b, all -> 0x0245 }
            int r5 = r4.getCount()     // Catch:{ SQLException -> 0x0261, all -> 0x0249 }
            if (r5 <= 0) goto L_0x01de
            r4.moveToFirst()     // Catch:{ SQLException -> 0x0261, all -> 0x0249 }
            r6 = 0
        L_0x01c0:
            if (r6 >= r5) goto L_0x01de
            r7 = 1
            java.lang.String r7 = r4.getString(r7)     // Catch:{ SQLException -> 0x0261, all -> 0x0249 }
            r0 = r17
            r1 = r7
            r0.add(r1)     // Catch:{ SQLException -> 0x0261, all -> 0x0249 }
            r4.moveToNext()     // Catch:{ SQLException -> 0x0261, all -> 0x0249 }
            int r6 = r6 + 1
            goto L_0x01c0
        L_0x01d3:
            java.lang.String r7 = "none"
            goto L_0x005d
        L_0x01d7:
            r7 = r24
            goto L_0x0064
        L_0x01db:
            r4 = 0
            goto L_0x00e7
        L_0x01de:
            r0 = r19
            r1 = r17
            r0.setTrackingPixels(r1)     // Catch:{ SQLException -> 0x0261, all -> 0x0249 }
        L_0x01e5:
            if (r14 == 0) goto L_0x01f0
            boolean r5 = r14.isClosed()
            if (r5 != 0) goto L_0x01f0
            r14.close()
        L_0x01f0:
            if (r4 == 0) goto L_0x026a
            boolean r5 = r4.isClosed()
            if (r5 != 0) goto L_0x026a
            r4.close()
            r4 = r16
        L_0x01fd:
            return r4
        L_0x01fe:
            r0 = r19
            r1 = r17
            r0.setTrackingPixels(r1)     // Catch:{ SQLException -> 0x025b, all -> 0x0245 }
        L_0x0205:
            r4 = r15
            goto L_0x01e5
        L_0x0207:
            r4 = move-exception
            r5 = r16
            r6 = r15
            r7 = r14
        L_0x020c:
            java.lang.String r8 = "QuattroWirelessSDK/2.1"
            java.lang.String r9 = com.qwapi.adclient.android.db.DBHelper.CLASSNAME     // Catch:{ all -> 0x0251 }
            android.util.Log.v(r8, r9, r4)     // Catch:{ all -> 0x0251 }
            if (r7 == 0) goto L_0x021e
            boolean r4 = r7.isClosed()
            if (r4 != 0) goto L_0x021e
            r7.close()
        L_0x021e:
            if (r6 == 0) goto L_0x0268
            boolean r4 = r6.isClosed()
            if (r4 != 0) goto L_0x0268
            r6.close()
            r4 = r5
            goto L_0x01fd
        L_0x022b:
            r4 = move-exception
            r5 = r15
            r6 = r14
        L_0x022e:
            if (r6 == 0) goto L_0x0239
            boolean r7 = r6.isClosed()
            if (r7 != 0) goto L_0x0239
            r6.close()
        L_0x0239:
            if (r5 == 0) goto L_0x0244
            boolean r6 = r5.isClosed()
            if (r6 != 0) goto L_0x0244
            r5.close()
        L_0x0244:
            throw r4
        L_0x0245:
            r4 = move-exception
            r5 = r15
            r6 = r14
            goto L_0x022e
        L_0x0249:
            r5 = move-exception
            r6 = r14
            r20 = r4
            r4 = r5
            r5 = r20
            goto L_0x022e
        L_0x0251:
            r4 = move-exception
            r5 = r6
            r6 = r7
            goto L_0x022e
        L_0x0255:
            r4 = move-exception
            r5 = r16
            r6 = r15
            r7 = r14
            goto L_0x020c
        L_0x025b:
            r4 = move-exception
            r5 = r16
            r6 = r15
            r7 = r14
            goto L_0x020c
        L_0x0261:
            r5 = move-exception
            r6 = r4
            r7 = r14
            r4 = r5
            r5 = r16
            goto L_0x020c
        L_0x0268:
            r4 = r5
            goto L_0x01fd
        L_0x026a:
            r4 = r16
            goto L_0x01fd
        L_0x026d:
            r4 = r18
            goto L_0x018e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qwapi.adclient.android.db.DBHelper.getAd(java.util.Collection, com.qwapi.adclient.android.requestparams.Placement, java.lang.String):com.qwapi.adclient.android.data.AdResponse");
    }

    public List<AdResponse> getAllAds() {
        Cursor cursor;
        Cursor cursor2;
        Cursor cursor3;
        Log.d(AdApiConstants.SDK, "getAd from database");
        Cursor cursor4 = null;
        ArrayList arrayList = new ArrayList();
        try {
            Cursor query = this.db.query(true, AD_TABLE, AD_COLS, null, null, null, null, ORDER_BY, null);
            try {
                if (query.getCount() > 0) {
                    query.moveToFirst();
                    while (true) {
                        try {
                            Ad ad = new Ad();
                            ArrayList arrayList2 = new ArrayList();
                            ad.setDbId(query.getInt(0));
                            ad.setId(query.getString(1));
                            ad.setAdType(query.getString(2));
                            ad.setClickUrl(query.getString(5));
                            ad.setAdText(query.getString(16));
                            ad.setActionText(query.getString(17));
                            ad.setBot(query.getInt(18) == 1);
                            ad.setImpressionCount(query.getInt(21));
                            ad.setLastServed(query.getInt(22));
                            ad.setExpiry(query.getLong(19));
                            ad.setImage(new Image(query.getString(6), query.getInt(9), query.getInt(8), query.getString(7)));
                            ad.setData(new Data(query.getString(10), query.getInt(11), query.getInt(12)));
                            ad.setText(new Text(query.getString(13), query.getString(14)));
                            arrayList2.add(query.getString(23));
                            int i = query.getInt(24);
                            AdResponse adResponse = new AdResponse(ad, Status.getSuccess(), query.getString(20));
                            if (i > 1) {
                                cursor3 = this.db.query(true, PIXEL_TABLE, PIXEL_COLS, "dbid = ?", new String[]{Long.toString((long) ad.getDbId())}, null, null, null, null);
                                try {
                                    int count = cursor3.getCount();
                                    for (int i2 = 0; i2 < count; i2++) {
                                        arrayList2.add(query.getString(1));
                                    }
                                    ad.setTrackingPixels(arrayList2);
                                } catch (SQLException e) {
                                    cursor = query;
                                    Cursor cursor5 = cursor3;
                                    e = e;
                                    cursor2 = cursor5;
                                    try {
                                        Log.v(AdApiConstants.SDK, CLASSNAME, e);
                                        cursor.close();
                                        cursor2.close();
                                        return null;
                                    } catch (Throwable th) {
                                        th = th;
                                        if (cursor != null && !cursor.isClosed()) {
                                            cursor.close();
                                        }
                                        if (cursor2 != null && !cursor2.isClosed()) {
                                            cursor2.close();
                                        }
                                        throw th;
                                    }
                                } catch (Throwable th2) {
                                    cursor = query;
                                    Cursor cursor6 = cursor3;
                                    th = th2;
                                    cursor2 = cursor6;
                                    cursor.close();
                                    cursor2.close();
                                    throw th;
                                }
                            } else {
                                ad.setTrackingPixels(arrayList2);
                                cursor3 = cursor4;
                            }
                            arrayList.add(adResponse);
                            if (!query.moveToNext()) {
                                break;
                            }
                            cursor4 = cursor3;
                        } catch (SQLException e2) {
                            e = e2;
                            cursor2 = cursor4;
                            cursor = query;
                            Log.v(AdApiConstants.SDK, CLASSNAME, e);
                            cursor.close();
                            cursor2.close();
                            return null;
                        } catch (Throwable th3) {
                            th = th3;
                            cursor2 = cursor4;
                            cursor = query;
                            cursor.close();
                            cursor2.close();
                            throw th;
                        }
                    }
                } else {
                    cursor3 = null;
                }
                if (query != null && !query.isClosed()) {
                    query.close();
                }
                if (cursor3 != null && !cursor3.isClosed()) {
                    cursor3.close();
                }
                return arrayList;
            } catch (SQLException e3) {
                e = e3;
                cursor2 = null;
                cursor = query;
                Log.v(AdApiConstants.SDK, CLASSNAME, e);
                if (cursor != null && !cursor.isClosed()) {
                    cursor.close();
                }
                if (cursor2 != null && !cursor2.isClosed()) {
                    cursor2.close();
                }
                return null;
            } catch (Throwable th4) {
                th = th4;
                cursor2 = null;
                cursor = query;
                cursor.close();
                cursor2.close();
                throw th;
            }
        } catch (SQLException e4) {
            e = e4;
            cursor2 = null;
            cursor = null;
        } catch (Throwable th5) {
            th = th5;
            cursor2 = null;
            cursor = null;
            cursor.close();
            cursor2.close();
            throw th;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void insertAd(Ad ad, String str, Placement placement, String str2) {
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(AD_ID, ad.getId());
            contentValues.put(AD_TYPE, ad.getAdType());
            contentValues.put(CLICK_URL, ad.getClickUrl());
            contentValues.put(AD_TEXT, ad.getAdText());
            contentValues.put(ACTION_TEXT, ad.getActionText());
            contentValues.put(ISBOT, Boolean.valueOf(ad.isBot()));
            contentValues.put(XHOSTED, Boolean.valueOf(ad.isExternallyHosted()));
            contentValues.put(IMPRESSION_COUNT, Integer.valueOf(ad.getImpressions()));
            contentValues.put(BATCHID, str);
            contentValues.put("placement", placement != null ? placement.toString() : NONE);
            contentValues.put("section", str2 != null ? str2 : NONE);
            contentValues.put(EXPIRATION_TS, Long.valueOf(ad.getExpires()));
            contentValues.put(LAST_IMPRESSION_TS, Long.valueOf(System.currentTimeMillis()));
            if (ad.getImage() != null) {
                contentValues.put(IMAGE_URL, ad.getImage().getUrl());
                contentValues.put(IMAGE_TEXT, ad.getImage().getAltText());
                contentValues.put(IMAGE_HEIGHT, Integer.valueOf(ad.getImage().getHeight()));
                contentValues.put(IMAGE_WIDTH, Integer.valueOf(ad.getImage().getWidth()));
            } else {
                contentValues.put(IMAGE_URL, Utils.EMPTY_STRING);
                contentValues.put(IMAGE_TEXT, Utils.EMPTY_STRING);
                contentValues.put(IMAGE_HEIGHT, (Integer) 0);
                contentValues.put(IMAGE_WIDTH, (Integer) 0);
            }
            if (ad.getData() != null) {
                contentValues.put(DATA_URL, ad.getData().getUrl());
                contentValues.put(DATA_HEIGHT, Integer.valueOf(ad.getData().getHeight()));
                contentValues.put(DATA_WIDTH, Integer.valueOf(ad.getData().getWidth()));
            } else {
                contentValues.put(DATA_URL, Utils.EMPTY_STRING);
                contentValues.put(DATA_HEIGHT, (Integer) 0);
                contentValues.put(DATA_WIDTH, (Integer) 0);
            }
            if (ad.getText() != null) {
                contentValues.put(TEXT_BODY, ad.getText().getBodyText());
                contentValues.put(TEXT_HEADLINE, ad.getText().getHeadline());
            } else {
                contentValues.put(TEXT_BODY, Utils.EMPTY_STRING);
                contentValues.put(TEXT_HEADLINE, Utils.EMPTY_STRING);
            }
            List<String> trackingPixels = ad.getTrackingPixels();
            if (trackingPixels.size() > 1) {
                for (String next : trackingPixels) {
                    if (next.contains(CONFIRM_IMAGE)) {
                        contentValues.put(CONFIRMATION_PIXEL, next);
                    }
                }
            } else if (trackingPixels.size() == 1) {
                contentValues.put(CONFIRMATION_PIXEL, trackingPixels.get(0));
            }
            contentValues.put(PIXEL_COUNT, Integer.valueOf(trackingPixels.size()));
            long insertOrThrow = this.db.insertOrThrow(AD_TABLE, null, contentValues);
            if (trackingPixels.size() > 1) {
                for (String next2 : trackingPixels) {
                    if (!next2.contains(CONFIRM_IMAGE)) {
                        ContentValues contentValues2 = new ContentValues();
                        contentValues2.put(DBID, Long.valueOf(insertOrThrow));
                        contentValues2.put(PIXEL, next2);
                        this.db.insertOrThrow(PIXEL_TABLE, null, contentValues2);
                    }
                }
            }
        } catch (SQLException e) {
            Log.e(AdApiConstants.SDK, CLASSNAME, e);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void updateAd(Ad ad, String str, Placement placement, String str2) {
        if (ad != null) {
            String[] strArr = {Integer.toString(ad.getDbId())};
            ContentValues contentValues = new ContentValues();
            contentValues.put(DBID, Integer.valueOf(ad.getDbId()));
            contentValues.put(AD_ID, ad.getId());
            contentValues.put(AD_TYPE, ad.getAdType());
            contentValues.put(CLICK_URL, ad.getClickUrl());
            contentValues.put(AD_TEXT, ad.getAdText());
            contentValues.put(ACTION_TEXT, ad.getActionText());
            contentValues.put(ISBOT, Boolean.valueOf(ad.isBot()));
            contentValues.put(XHOSTED, Boolean.valueOf(ad.isExternallyHosted()));
            contentValues.put(IMPRESSION_COUNT, Integer.valueOf(ad.getImpressions()));
            contentValues.put(BATCHID, str);
            contentValues.put("placement", placement != null ? placement.toString() : NONE);
            contentValues.put("section", str2 != null ? str2 : NONE);
            contentValues.put(EXPIRATION_TS, Long.valueOf(ad.getExpires()));
            contentValues.put(LAST_IMPRESSION_TS, Long.valueOf(System.currentTimeMillis()));
            List<String> trackingPixels = ad.getTrackingPixels();
            if (trackingPixels.size() > 1) {
                for (String next : trackingPixels) {
                    if (next.contains(CONFIRM_IMAGE)) {
                        contentValues.put(CONFIRMATION_PIXEL, next);
                    }
                }
            } else if (trackingPixels.size() == 1) {
                contentValues.put(CONFIRMATION_PIXEL, trackingPixels.get(0));
            }
            contentValues.put(PIXEL_COUNT, Integer.valueOf(trackingPixels.size()));
            if (ad.getImage() != null) {
                contentValues.put(IMAGE_URL, ad.getImage().getUrl());
                contentValues.put(IMAGE_TEXT, ad.getImage().getAltText());
                contentValues.put(IMAGE_HEIGHT, Integer.valueOf(ad.getImage().getHeight()));
                contentValues.put(IMAGE_WIDTH, Integer.valueOf(ad.getImage().getWidth()));
            } else {
                contentValues.put(IMAGE_URL, Utils.EMPTY_STRING);
                contentValues.put(IMAGE_TEXT, Utils.EMPTY_STRING);
                contentValues.put(IMAGE_HEIGHT, (Integer) 0);
                contentValues.put(IMAGE_WIDTH, (Integer) 0);
            }
            if (ad.getData() != null) {
                contentValues.put(DATA_URL, ad.getData().getUrl());
                contentValues.put(DATA_HEIGHT, Integer.valueOf(ad.getData().getHeight()));
                contentValues.put(DATA_WIDTH, Integer.valueOf(ad.getData().getWidth()));
            } else {
                contentValues.put(DATA_URL, Utils.EMPTY_STRING);
                contentValues.put(DATA_HEIGHT, (Integer) 0);
                contentValues.put(DATA_WIDTH, (Integer) 0);
            }
            if (ad.getText() != null) {
                contentValues.put(TEXT_BODY, ad.getText().getBodyText());
                contentValues.put(TEXT_HEADLINE, ad.getText().getHeadline());
            } else {
                contentValues.put(TEXT_BODY, Utils.EMPTY_STRING);
                contentValues.put(TEXT_HEADLINE, Utils.EMPTY_STRING);
            }
            this.db.update(AD_TABLE, contentValues, "dbid = ?", strArr);
        }
    }
}
