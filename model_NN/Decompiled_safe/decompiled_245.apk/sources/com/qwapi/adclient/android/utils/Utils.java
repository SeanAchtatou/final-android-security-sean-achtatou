package com.qwapi.adclient.android.utils;

import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import com.qwapi.adclient.android.view.AdViewConstants;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

public class Utils {
    public static final String EMPTY_STRING = "";
    public static final String UTF8 = "utf-8";
    public static final String defaultEncoding = "utf-8";

    private static void addNameValuePair(Map<String, String[]> map, String str, String str2) {
        String[] strArr;
        String[] strArr2 = map.get(str);
        if (strArr2 == null) {
            strArr = new String[]{str2};
        } else {
            String[] strArr3 = new String[(strArr2.length + 1)];
            System.arraycopy(strArr2, 0, strArr3, 0, strArr2.length);
            strArr3[strArr2.length] = str2;
            strArr = strArr3;
        }
        map.put(str, strArr);
    }

    /* JADX WARNING: Removed duplicated region for block: B:6:0x001b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean compareMaps(java.util.Map<java.lang.String, java.lang.Object> r4, java.util.Map<java.lang.String, java.lang.Object> r5) {
        /*
            r3 = 0
            int r0 = r4.size()
            int r1 = r5.size()
            if (r0 == r1) goto L_0x000d
            r0 = r3
        L_0x000c:
            return r0
        L_0x000d:
            java.util.Set r0 = r4.entrySet()
            java.util.Iterator r0 = r0.iterator()
        L_0x0015:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x0037
            java.lang.Object r4 = r0.next()
            java.util.Map$Entry r4 = (java.util.Map.Entry) r4
            java.lang.Object r1 = r4.getValue()
            if (r1 == 0) goto L_0x0035
            java.lang.Object r2 = r4.getKey()
            java.lang.Object r2 = r5.get(r2)
            boolean r1 = r1.equals(r2)
            if (r1 != 0) goto L_0x0015
        L_0x0035:
            r0 = r3
            goto L_0x000c
        L_0x0037:
            r0 = 1
            goto L_0x000c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qwapi.adclient.android.utils.Utils.compareMaps(java.util.Map, java.util.Map):boolean");
    }

    public static String decode(String str) {
        if (!isGoodString(str)) {
            return EMPTY_STRING;
        }
        try {
            return URLDecoder.decode(str, "utf-8");
        } catch (Exception e) {
            return str;
        }
    }

    public static String encode(String str) {
        return encode(str, null);
    }

    public static String encode(String str, String str2) {
        if (str == null || str.length() <= 0) {
            return EMPTY_STRING;
        }
        try {
            return URLEncoder.encode(str, isGoodString(str2) ? str2 : "utf-8");
        } catch (Exception e) {
            Log.e("Utils.encode", e.getMessage(), e);
            return str;
        }
    }

    public static String getUserAgent() {
        return EMPTY_STRING;
    }

    public static void invokeLandingPage(View view, String str) {
        if (str != null && str.length() > 0) {
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
            intent.addFlags(268435456);
            try {
                view.getContext().startActivity(intent);
            } catch (Exception e) {
                Log.e("Problem", e.getMessage());
            }
        }
    }

    public static boolean isGoodString(String str) {
        return str != null && str.length() > 0 && str.trim().length() > 0;
    }

    public static Map<String, String[]> parse(String str, boolean z) {
        String decode;
        String str2;
        HashMap hashMap = new HashMap();
        if (isGoodString(str)) {
            String replaceAll = str.replaceAll("&amp;", AdViewConstants.AMP);
            int i = 0;
            int indexOf = replaceAll.indexOf(38);
            while (true) {
                int i2 = indexOf;
                int i3 = i;
                int i4 = i2;
                int indexOf2 = replaceAll.indexOf(61, i3);
                if (i4 >= 0 && indexOf2 > i4) {
                    decode = decode(replaceAll.substring(i3, i4));
                    str2 = EMPTY_STRING;
                } else if (indexOf2 != -1) {
                    String decode2 = decode(replaceAll.substring(i3, indexOf2));
                    if (z) {
                        String decode3 = decode(i4 > 0 ? replaceAll.substring(indexOf2 + 1, i4) : replaceAll.substring(indexOf2 + 1));
                        decode = decode2;
                        str2 = decode3;
                    } else {
                        String substring = i4 > 0 ? replaceAll.substring(indexOf2 + 1, i4) : replaceAll.substring(indexOf2 + 1);
                        decode = decode2;
                        str2 = substring;
                    }
                } else {
                    decode = decode(i4 > 0 ? replaceAll.substring(i3, i4) : replaceAll.substring(i3));
                    str2 = EMPTY_STRING;
                }
                if (isGoodString(decode)) {
                    addNameValuePair(hashMap, decode, str2);
                }
                if (i4 == -1) {
                    break;
                }
                i = i4 + 1;
                indexOf = replaceAll.indexOf(38, i);
            }
        }
        return hashMap;
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:10:0x003b */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:15:0x0069 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:53:0x00db */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:27:0x0087 */
    /* JADX INFO: additional move instructions added (6) to help type inference */
    /* JADX WARN: Type inference failed for: r2v7 */
    /* JADX WARN: Type inference failed for: r3v5 */
    /* JADX WARN: Type inference failed for: r3v9, types: [java.io.InputStream] */
    /* JADX WARN: Type inference failed for: r2v11, types: [java.io.ByteArrayOutputStream] */
    /* JADX WARN: Type inference failed for: r3v12, types: [java.io.InputStream] */
    /* JADX WARN: Type inference failed for: r2v17 */
    /* JADX WARN: Type inference failed for: r4v3, types: [java.io.ByteArrayOutputStream] */
    /* JADX WARN: Type inference failed for: r2v20 */
    /* JADX WARN: Type inference failed for: r3v16, types: [byte[]] */
    /* JADX WARN: Type inference failed for: r3v17 */
    /* JADX WARN: Type inference failed for: r2v26 */
    /* JADX WARN: Type inference failed for: r3v20 */
    /* JADX WARN: Type inference failed for: r3v22 */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x008b A[SYNTHETIC, Splitter:B:31:0x008b] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0090 A[Catch:{ Throwable -> 0x0094 }] */
    public static com.qwapi.adclient.android.utils.HttpResponse processUrl(java.lang.String r10, java.lang.String r11) {
        /*
            r9 = 0
            r8 = 0
            java.lang.String r3 = ""
            boolean r1 = isGoodString(r10)     // Catch:{ Throwable -> 0x0094 }
            if (r1 == 0) goto L_0x00af
            r1 = 0
            java.net.URL r2 = new java.net.URL     // Catch:{ Throwable -> 0x0094 }
            r2.<init>(r10)     // Catch:{ Throwable -> 0x0094 }
            java.net.URLConnection r2 = r2.openConnection()     // Catch:{ Throwable -> 0x0094 }
            r4 = 1
            r2.setDoInput(r4)     // Catch:{ Throwable -> 0x0094 }
            r4 = 0
            r2.setUseCaches(r4)     // Catch:{ Throwable -> 0x0094 }
            r2.setDoOutput(r1)     // Catch:{ Throwable -> 0x0094 }
            r1 = 15000(0x3a98, float:2.102E-41)
            r2.setReadTimeout(r1)     // Catch:{ Throwable -> 0x0094 }
            boolean r1 = isGoodString(r11)     // Catch:{ Throwable -> 0x0094 }
            if (r1 == 0) goto L_0x002f
            java.lang.String r1 = "User-Agent"
            r2.setRequestProperty(r1, r11)     // Catch:{ Throwable -> 0x0094 }
        L_0x002f:
            boolean r1 = r2 instanceof java.net.HttpURLConnection     // Catch:{ Throwable -> 0x0094 }
            if (r1 == 0) goto L_0x003b
            r0 = r2
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Throwable -> 0x0094 }
            r1 = r0
            r4 = 1
            r1.setInstanceFollowRedirects(r4)     // Catch:{ Throwable -> 0x0094 }
        L_0x003b:
            if (r8 == 0) goto L_0x0061
            java.lang.String r1 = "Content-Type"
            java.lang.String r4 = "application/x-www-form-urlencoded"
            r2.setRequestProperty(r1, r4)     // Catch:{ Throwable -> 0x0094 }
            java.lang.String r1 = "Content-Length"
            int r4 = r3.length()     // Catch:{ Throwable -> 0x0094 }
            java.lang.String r4 = java.lang.String.valueOf(r4)     // Catch:{ Throwable -> 0x0094 }
            r2.setRequestProperty(r1, r4)     // Catch:{ Throwable -> 0x0094 }
            java.io.OutputStream r1 = r2.getOutputStream()     // Catch:{ Throwable -> 0x0094 }
            java.lang.String r4 = "utf-8"
            byte[] r3 = r3.getBytes(r4)     // Catch:{ Throwable -> 0x0094 }
            r1.write(r3)     // Catch:{ Throwable -> 0x0094 }
            r1.close()     // Catch:{ Throwable -> 0x0094 }
        L_0x0061:
            java.lang.String r1 = r2.getContentEncoding()     // Catch:{ Throwable -> 0x0094 }
            if (r1 != 0) goto L_0x0069
            java.lang.String r1 = "utf-8"
        L_0x0069:
            java.io.InputStream r3 = r2.getInputStream()     // Catch:{ MalformedURLException -> 0x00ff, IOException -> 0x00d8, Throwable -> 0x00dc, all -> 0x00e9 }
            java.io.ByteArrayOutputStream r4 = new java.io.ByteArrayOutputStream     // Catch:{ MalformedURLException -> 0x0103, IOException -> 0x00f9, Throwable -> 0x00f3, all -> 0x00ed }
            r4.<init>()     // Catch:{ MalformedURLException -> 0x0103, IOException -> 0x00f9, Throwable -> 0x00f3, all -> 0x00ed }
            r5 = 2048(0x800, float:2.87E-42)
            byte[] r5 = new byte[r5]     // Catch:{ MalformedURLException -> 0x0085, IOException -> 0x00fc, Throwable -> 0x00f6, all -> 0x00f0 }
        L_0x0076:
            r6 = 0
            r7 = 2048(0x800, float:2.87E-42)
            int r6 = r3.read(r5, r6, r7)     // Catch:{ MalformedURLException -> 0x0085, IOException -> 0x00fc, Throwable -> 0x00f6, all -> 0x00f0 }
            r7 = -1
            if (r6 == r7) goto L_0x00b1
            r7 = 0
            r4.write(r5, r7, r6)     // Catch:{ MalformedURLException -> 0x0085, IOException -> 0x00fc, Throwable -> 0x00f6, all -> 0x00f0 }
            goto L_0x0076
        L_0x0085:
            r1 = move-exception
            r2 = r4
        L_0x0087:
            throw r1     // Catch:{ all -> 0x0088 }
        L_0x0088:
            r1 = move-exception
        L_0x0089:
            if (r3 == 0) goto L_0x008e
            r3.close()     // Catch:{ Throwable -> 0x0094 }
        L_0x008e:
            if (r2 == 0) goto L_0x0093
            r2.close()     // Catch:{ Throwable -> 0x0094 }
        L_0x0093:
            throw r1     // Catch:{ Throwable -> 0x0094 }
        L_0x0094:
            r1 = move-exception
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Problem in ProcessUrl,url:"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r10)
            java.lang.String r2 = r2.toString()
            java.lang.String r3 = r1.getMessage()
            android.util.Log.e(r2, r3, r1)
        L_0x00af:
            r1 = r9
        L_0x00b0:
            return r1
        L_0x00b1:
            if (r3 == 0) goto L_0x00b6
            r3.close()     // Catch:{ Throwable -> 0x0094 }
        L_0x00b6:
            if (r4 == 0) goto L_0x00bb
            r4.close()     // Catch:{ Throwable -> 0x0094 }
        L_0x00bb:
            if (r4 == 0) goto L_0x0108
            byte[] r3 = r4.toByteArray()     // Catch:{ Throwable -> 0x0094 }
            java.lang.String r4 = new java.lang.String     // Catch:{ Throwable -> 0x0094 }
            r4.<init>(r3, r1)     // Catch:{ Throwable -> 0x0094 }
            r1 = r4
        L_0x00c7:
            boolean r3 = r2 instanceof java.net.HttpURLConnection     // Catch:{ Throwable -> 0x0094 }
            if (r3 == 0) goto L_0x0106
            java.net.HttpURLConnection r2 = (java.net.HttpURLConnection) r2     // Catch:{ Throwable -> 0x0094 }
            int r2 = r2.getResponseCode()     // Catch:{ Throwable -> 0x0094 }
        L_0x00d1:
            com.qwapi.adclient.android.utils.HttpResponse r3 = new com.qwapi.adclient.android.utils.HttpResponse     // Catch:{ Throwable -> 0x0094 }
            r3.<init>(r1, r2)     // Catch:{ Throwable -> 0x0094 }
            r1 = r3
            goto L_0x00b0
        L_0x00d8:
            r1 = move-exception
            r2 = r9
            r3 = r9
        L_0x00db:
            throw r1     // Catch:{ all -> 0x0088 }
        L_0x00dc:
            r1 = move-exception
            r2 = r9
            r3 = r9
        L_0x00df:
            java.io.IOException r4 = new java.io.IOException     // Catch:{ all -> 0x0088 }
            java.lang.String r1 = r1.getMessage()     // Catch:{ all -> 0x0088 }
            r4.<init>(r1)     // Catch:{ all -> 0x0088 }
            throw r4     // Catch:{ all -> 0x0088 }
        L_0x00e9:
            r1 = move-exception
            r2 = r9
            r3 = r9
            goto L_0x0089
        L_0x00ed:
            r1 = move-exception
            r2 = r9
            goto L_0x0089
        L_0x00f0:
            r1 = move-exception
            r2 = r4
            goto L_0x0089
        L_0x00f3:
            r1 = move-exception
            r2 = r9
            goto L_0x00df
        L_0x00f6:
            r1 = move-exception
            r2 = r4
            goto L_0x00df
        L_0x00f9:
            r1 = move-exception
            r2 = r9
            goto L_0x00db
        L_0x00fc:
            r1 = move-exception
            r2 = r4
            goto L_0x00db
        L_0x00ff:
            r1 = move-exception
            r2 = r9
            r3 = r9
            goto L_0x0087
        L_0x0103:
            r1 = move-exception
            r2 = r9
            goto L_0x0087
        L_0x0106:
            r2 = r8
            goto L_0x00d1
        L_0x0108:
            r1 = r9
            goto L_0x00c7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qwapi.adclient.android.utils.Utils.processUrl(java.lang.String, java.lang.String):com.qwapi.adclient.android.utils.HttpResponse");
    }
}
