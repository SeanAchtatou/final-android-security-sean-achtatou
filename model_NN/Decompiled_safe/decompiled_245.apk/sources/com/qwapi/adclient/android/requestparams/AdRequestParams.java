package com.qwapi.adclient.android.requestparams;

import android.graphics.Color;
import android.util.Log;
import com.qwapi.adclient.android.AdApiConfig;
import com.qwapi.adclient.android.AdApiConstants;
import com.qwapi.adclient.android.DeviceContext;
import com.qwapi.adclient.android.utils.Utils;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class AdRequestParams implements Serializable {
    private static final String AMPERSAND = "&";
    public static final String ONE = "1";
    private static final String PARAM_AGE = "age";
    private static final String PARAM_AREA = "area";
    private static final String PARAM_BIRTH_DATE = "pid";
    private static final String PARAM_DEMOGRAPHIC = "dem";
    private static final String PARAM_DMA = "dma";
    private static final String PARAM_EDUCATION = "edu";
    private static final String PARAM_ETHNICITY = "eth";
    private static final String PARAM_FORMAT = "fmt";
    private static final String PARAM_FORMAT_VER = "fmtver";
    private static final String PARAM_GENDER = "g";
    private static final String PARAM_GEOGRAPIC = "geo";
    private static final String PARAM_HOUSE_HOLD_INCOME = "hhi";
    private static final String PARAM_LAT = "lat";
    private static final String PARAM_LON = "lon";
    private static final String PARAM_MEDIA_TYPE = "adm";
    private static final String PARAM_PHONE_NUMBER = "mdn";
    private static final String PARAM_PLACEMENT = "plc";
    private static final String PARAM_PUB_ID = "pid";
    private static final String PARAM_SECTION = "cid";
    private static final String PARAM_SITE_ID = "sid";
    private static final String PARAM_TEST = "test";
    private static final String PARAM_UDID = "udid";
    private static final String PARAM_USER_AGENT = "ua";
    private static final String PARAM_ZIPCODE = "zip";
    private static final String XML_VERSION = "2.0";
    public static final String ZERO = "0";
    private Map<MediaType, Integer> _mediaTypes;
    private Color backgroundColor;
    private Map<String, Object> dem_params = new HashMap();
    private Map<String, Object> geo_params = new HashMap();
    private Map<String, Object> params = new HashMap();
    private String textColor;

    public AdRequestParams(DeviceContext deviceContext) {
        this.params.put("pid", AdApiConfig.getPublihserId());
        this.params.put(PARAM_SITE_ID, AdApiConfig.getDefaultSiteId());
        this.params.put(PARAM_TEST, AdApiConfig.getTestMode() ? "1" : ZERO);
        this.params.put(PARAM_UDID, deviceContext.getDeviceId());
        this.params.put(PARAM_USER_AGENT, deviceContext.getUserAgent());
        this.params.put(PARAM_FORMAT, "xml");
        this.params.put(PARAM_FORMAT_VER, XML_VERSION);
        if (deviceContext.getLatitude() != 0.0d) {
            this.params.put(PARAM_LAT, Utils.EMPTY_STRING + deviceContext.getLatitude());
        }
        if (deviceContext.getLongitude() != 0.0d) {
            this.params.put(PARAM_LON, Utils.EMPTY_STRING + deviceContext.getLongitude());
        }
    }

    private String buildDemographicParamsQueryString() {
        StringBuilder sb = new StringBuilder(PARAM_DEMOGRAPHIC);
        StringBuilder sb2 = new StringBuilder();
        String str = Utils.EMPTY_STRING;
        for (Map.Entry next : this.dem_params.entrySet()) {
            if (next.getValue() != null) {
                sb2.append(str).append((String) next.getKey()).append('=').append(next.getValue().toString());
            }
            str = "&";
        }
        return sb.append('=').append(Utils.encode(sb2.toString())).toString();
    }

    private String buildGeographicParamsQueryString() {
        StringBuilder sb = new StringBuilder(PARAM_GEOGRAPIC);
        StringBuilder sb2 = new StringBuilder();
        String str = Utils.EMPTY_STRING;
        for (Map.Entry next : this.geo_params.entrySet()) {
            if (next.getValue() != null) {
                sb2.append(str).append((String) next.getKey()).append('=').append(next.getValue().toString());
            }
            str = "&";
        }
        return sb.append('=').append(Utils.encode(sb2.toString())).toString();
    }

    public void addMediaType(MediaType mediaType, int i) {
        if (this._mediaTypes == null) {
            this._mediaTypes = new HashMap();
        }
        if (mediaType != null) {
            if (mediaType == MediaType.banner && !this._mediaTypes.containsKey(MediaType.expandable)) {
                this._mediaTypes.put(MediaType.expandable, new Integer(i));
                this._mediaTypes.put(MediaType.animated, new Integer(i));
            }
            this._mediaTypes.put(mediaType, new Integer(i));
            return;
        }
        Log.e(AdApiConstants.SDK, "Unable to add mediaType");
    }

    public Age getAge() {
        return (Age) this.dem_params.get("age");
    }

    public String getAreaCode() {
        return (String) this.geo_params.get(PARAM_AREA);
    }

    public Color getBackgroundColor() {
        return this.backgroundColor;
    }

    public Date getBirthDate() {
        return (Date) this.dem_params.get("pid");
    }

    public String getDmaCode() {
        return (String) this.geo_params.get(PARAM_DMA);
    }

    public Education getEducation() {
        return (Education) this.dem_params.get(PARAM_EDUCATION);
    }

    public Ethnicity getEthnicity() {
        return (Ethnicity) this.dem_params.get(PARAM_ETHNICITY);
    }

    public Gender getGender() {
        return (Gender) this.params.get(PARAM_GENDER);
    }

    public Income getIncome() {
        return (Income) this.dem_params.get(PARAM_HOUSE_HOLD_INCOME);
    }

    public Map<MediaType, Integer> getMediaTypes() {
        return this._mediaTypes;
    }

    public String getPhoneNumber() {
        return (String) this.geo_params.get(PARAM_PHONE_NUMBER);
    }

    public Placement getPlacement() {
        return (Placement) this.params.get(PARAM_PLACEMENT);
    }

    public String getPubId() {
        return (String) this.params.get("pid");
    }

    public String getQueryString(boolean z, int i) {
        StringBuilder sb = new StringBuilder();
        String str = Utils.EMPTY_STRING;
        Iterator<Map.Entry<String, Object>> it = this.params.entrySet().iterator();
        while (true) {
            String str2 = str;
            if (!it.hasNext()) {
                break;
            }
            Map.Entry next = it.next();
            Object value = next.getValue();
            if (value != null) {
                sb.append(str2).append((String) next.getKey()).append('=').append(value.toString());
            }
            str = "&";
        }
        sb.append("&").append(buildDemographicParamsQueryString()).append("&").append(buildGeographicParamsQueryString());
        if (getMediaTypes().containsKey(MediaType.banner) || getMediaTypes().containsKey(MediaType.expandable)) {
            sb.append("&").append("sas=true");
        }
        if (z) {
            for (MediaType next2 : getMediaTypes().keySet()) {
                if (!(next2 == MediaType.expandable || next2 == MediaType.animated)) {
                    sb.append("&").append("adm=").append(next2.batchValue()).append(',').append(getMediaTypes().get(next2).toString());
                }
            }
            sb.append("&mode=b");
        } else {
            StringBuffer stringBuffer = new StringBuffer();
            for (MediaType next3 : getMediaTypes().keySet()) {
                if (next3 != MediaType.animated) {
                    if (stringBuffer.length() > 0) {
                        stringBuffer.append(",");
                    }
                    stringBuffer.append(next3.toString());
                }
            }
            sb.append("&adm=").append(stringBuffer);
        }
        return sb.toString();
    }

    public String getSection() {
        return (String) this.params.get(PARAM_SECTION);
    }

    public String getSiteId() {
        return (String) this.params.get(PARAM_SITE_ID);
    }

    public boolean getTestMode() {
        return "1".equals((String) this.params.get(PARAM_TEST));
    }

    public String getTextColor() {
        return this.textColor;
    }

    /* access modifiers changed from: package-private */
    public String getUdid() {
        return (String) this.params.get(PARAM_UDID);
    }

    public String getUserAgent() {
        return (String) this.params.get(PARAM_USER_AGENT);
    }

    public String getZipCode() {
        return (String) this.geo_params.get("zip");
    }

    public void removeMediaType(MediaType mediaType) {
        if (this._mediaTypes != null) {
            this._mediaTypes.remove(mediaType);
            if (mediaType == MediaType.banner) {
                this._mediaTypes.remove(MediaType.animated);
                this._mediaTypes.remove(MediaType.expandable);
            }
        }
    }

    public boolean sameAs(AdRequestParams adRequestParams) {
        if (adRequestParams == null) {
            return false;
        }
        if (!Utils.compareMaps(this.params, adRequestParams.params)) {
            return false;
        }
        if (!Utils.compareMaps(this.dem_params, adRequestParams.dem_params)) {
            return false;
        }
        return Utils.compareMaps(this.geo_params, adRequestParams.geo_params);
    }

    public void setAge(Age age) {
        this.dem_params.put("age", age);
    }

    public void setAreaCode(String str) {
        this.geo_params.put(PARAM_AREA, str);
    }

    public void setBackgroundColor(Color color) {
        this.backgroundColor = color;
    }

    public void setBirthDate(Date date) {
        this.dem_params.put("pid", date);
    }

    public void setDmaCode(String str) {
        this.geo_params.put(PARAM_DMA, str);
    }

    public void setEducation(Education education) {
        this.dem_params.put(PARAM_EDUCATION, education);
    }

    public void setEthnicity(Ethnicity ethnicity) {
        this.dem_params.put(PARAM_ETHNICITY, ethnicity);
    }

    public void setGender(Gender gender) {
        this.params.put(PARAM_GENDER, gender);
    }

    public void setIncome(Income income) {
        this.dem_params.put(PARAM_HOUSE_HOLD_INCOME, income);
    }

    public void setMediaType(Map<MediaType, Integer> map) {
        this._mediaTypes = map;
    }

    public void setPhoneNumber(String str) {
        this.geo_params.put(PARAM_PHONE_NUMBER, str);
    }

    public void setPlacement(Placement placement) {
        this.params.put(PARAM_PLACEMENT, placement);
    }

    public void setPubId(String str) {
        this.params.put("pid", str);
    }

    public void setSection(String str) {
        this.params.put(PARAM_SECTION, str);
    }

    public void setSiteId(String str) {
        this.params.put(PARAM_SITE_ID, str);
    }

    public void setTestMode(boolean z) {
        this.params.put(PARAM_TEST, z ? "1" : ZERO);
    }

    public void setTextColor(int i) {
        int red = Color.red(i);
        int green = Color.green(i);
        int blue = Color.blue(i);
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(red);
        stringBuffer.append(",");
        stringBuffer.append(green);
        stringBuffer.append(",");
        stringBuffer.append(blue);
        this.textColor = stringBuffer.toString();
    }

    /* access modifiers changed from: package-private */
    public void setUdid(String str) {
        this.params.put(PARAM_UDID, str);
    }

    public void setZipCode(String str) {
        this.geo_params.put("zip", str);
    }
}
