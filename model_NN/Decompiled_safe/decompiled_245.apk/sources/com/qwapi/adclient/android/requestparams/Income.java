package com.qwapi.adclient.android.requestparams;

import com.qwapi.adclient.android.utils.Utils;

public enum Income {
    income_lt_35000,
    income_35000_to_49999,
    income_50000_to_74499,
    income_75000_to_99999,
    income_100000_to_149999,
    income_gt_150000;

    public String toString() {
        return Utils.EMPTY_STRING + ordinal();
    }
}
