package com.qwapi.adclient.android.data;

public class Text {
    private String bodyText;
    private String headline;

    public Text(String str, String str2) {
        this.bodyText = str;
        this.headline = str2;
    }

    public String getBodyText() {
        return this.bodyText;
    }

    public String getHeadline() {
        return this.headline;
    }

    public void setBodyText(String str) {
        this.bodyText = str;
    }

    public void setHeadline(String str) {
        this.headline = str;
    }
}
