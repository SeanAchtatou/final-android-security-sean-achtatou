package com.qwapi.adclient.android.requestparams;

public enum AnimationType {
    fade,
    slide,
    hyperspace,
    pushUp,
    pushLeft;

    public String toString() {
        switch (ordinal()) {
            case 0:
                return "Fade";
            case 1:
                return "Slide";
            case 2:
                return "Hyperspace";
            case 3:
                return "Push Up";
            case 4:
                return "Push Left";
            default:
                return null;
        }
    }
}
