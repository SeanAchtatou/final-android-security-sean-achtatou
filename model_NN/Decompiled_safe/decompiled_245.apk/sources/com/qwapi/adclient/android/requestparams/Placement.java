package com.qwapi.adclient.android.requestparams;

public enum Placement {
    top,
    middle,
    bottom;

    public String toString() {
        switch (ordinal()) {
            case 0:
                return "top";
            case 1:
                return "middle";
            case 2:
                return "bottom";
            default:
                return null;
        }
    }
}
