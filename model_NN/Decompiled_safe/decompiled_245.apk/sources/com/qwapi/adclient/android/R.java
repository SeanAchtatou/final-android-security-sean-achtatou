package com.qwapi.adclient.android;

public final class R {

    public static final class anim {
        public static final int hyperspace_in = 2130968576;
        public static final int hyperspace_out = 2130968577;
        public static final int push_left_in = 2130968578;
        public static final int push_left_out = 2130968579;
        public static final int push_up_in = 2130968580;
        public static final int push_up_out = 2130968581;
    }

    public static final class array {
        public static final int ad_type = 2131165184;
        public static final int animation_type = 2131165185;
        public static final int display_mode_type = 2131165188;
        public static final int execution_mode_type = 2131165187;
        public static final int placement_type = 2131165186;
    }

    public static final class attr {
        public static final int adEventListenerClass = 2130771978;
        public static final int adInterval = 2130771971;
        public static final int animation = 2130771972;
        public static final int bgColor = 2130771982;
        public static final int defaultAdClickThru = 2130771976;
        public static final int defaultAdImage = 2130771975;
        public static final int displayMode = 2130771970;
        public static final int mediaType = 2130771969;
        public static final int placement = 2130771968;
        public static final int publisherId = 2130771974;
        public static final int renderAdOnCreate = 2130771980;
        public static final int requestMode = 2130771981;
        public static final int section = 2130771977;
        public static final int siteId = 2130771973;
        public static final int testMode = 2130771979;
        public static final int textColor = 2130771983;
    }

    public static final class drawable {
        public static final int icon = 2130837504;
        public static final int qw = 2130837505;
        public static final int sample = 2130837506;
        public static final int skip_button = 2130837507;
    }

    public static final class id {
        public static final int QWAd = 2131230720;
        public static final int cachedAdsView = 2131230726;
        public static final int nextAd = 2131230722;
        public static final int qwMemDump = 2131230725;
        public static final int qwPreferences = 2131230723;
        public static final int qwShowCachedAds = 2131230724;
        public static final int spinnerTarget = 2131230727;
        public static final int testAppView = 2131230721;
    }

    public static final class layout {
        public static final int main = 2130903040;
        public static final int qw_normal_spinner_item_style = 2130903041;
    }

    public static final class string {
        public static final int app_name = 2131099649;
        public static final int application_category_title = 2131099711;
        public static final int banner_count_preference = 2131099674;
        public static final int banner_preference = 2131099651;
        public static final int batch_category_title = 2131099671;
        public static final int batch_preference = 2131099670;
        public static final int dialog_title_animation_preference = 2131099704;
        public static final int dialog_title_display_mode_preference = 2131099699;
        public static final int dialog_title_execution_mode_preference = 2131099712;
        public static final int dialog_title_placement_preference = 2131099669;
        public static final int dialog_title_publisher_id_preference = 2131099688;
        public static final int dialog_title_site_id_preference = 2131099692;
        public static final int expandable_count_preference = 2131099683;
        public static final int expandable_preference = 2131099660;
        public static final int hello = 2131099648;
        public static final int interstitial_count_preference = 2131099677;
        public static final int interstitial_preference = 2131099654;
        public static final int media_category_title = 2131099650;
        public static final int other_category_title = 2131099695;
        public static final int placement_preference = 2131099666;
        public static final int publisher_category_title = 2131099686;
        public static final int publisher_id_preference = 2131099687;
        public static final int section_preference = 2131099663;
        public static final int site_id_preference = 2131099691;
        public static final int summary_animation_preference = 2131099706;
        public static final int summary_background_color_preference = 2131099708;
        public static final int summary_banner_ad_preference = 2131099653;
        public static final int summary_banner_count_preference = 2131099676;
        public static final int summary_batch_preference = 2131099673;
        public static final int summary_display_mode_preference = 2131099701;
        public static final int summary_execution_mode_preference = 2131099714;
        public static final int summary_expandable_ad_preference = 2131099662;
        public static final int summary_expandable_count_preference = 2131099685;
        public static final int summary_interstitial_ad_preference = 2131099656;
        public static final int summary_interstitial_count_preference = 2131099679;
        public static final int summary_placement_preference = 2131099668;
        public static final int summary_publisher_id_preference = 2131099690;
        public static final int summary_refresh_interval_preference = 2131099703;
        public static final int summary_section_preference = 2131099665;
        public static final int summary_site_id_preference = 2131099694;
        public static final int summary_testflag_preference = 2131099698;
        public static final int summary_text_ad_preference = 2131099659;
        public static final int summary_text_color_preference = 2131099710;
        public static final int summary_text_count_preference = 2131099682;
        public static final int testflag_preference = 2131099696;
        public static final int text_count_preference = 2131099680;
        public static final int text_preference = 2131099657;
        public static final int title_animation_preference = 2131099705;
        public static final int title_background_color_preference = 2131099707;
        public static final int title_banner_ad_preference = 2131099652;
        public static final int title_banner_count_preference = 2131099675;
        public static final int title_batch_preference = 2131099672;
        public static final int title_display_mode_preference = 2131099700;
        public static final int title_execution_mode_preference = 2131099713;
        public static final int title_expandable_ad_preference = 2131099661;
        public static final int title_expandable_count_preference = 2131099684;
        public static final int title_interstitial_ad_preference = 2131099655;
        public static final int title_interstitial_count_preference = 2131099678;
        public static final int title_placement_preference = 2131099667;
        public static final int title_publisher_id_preference = 2131099689;
        public static final int title_refresh_interval_preference = 2131099702;
        public static final int title_section_preference = 2131099664;
        public static final int title_site_id_preference = 2131099693;
        public static final int title_testflag_preference = 2131099697;
        public static final int title_text_ad_preference = 2131099658;
        public static final int title_text_color_preference = 2131099709;
        public static final int title_text_count_preference = 2131099681;
    }

    public static final class styleable {
        public static final int[] QWAdView = {2130771968, 2130771969, 2130771970, 2130771971, 2130771972, 2130771973, 2130771974, 2130771975, 2130771976, 2130771977, 2130771978, 2130771979, 2130771980, 2130771981, 2130771982, 2130771983};
        public static final int QWAdView_adEventListenerClass = 10;
        public static final int QWAdView_adInterval = 3;
        public static final int QWAdView_animation = 4;
        public static final int QWAdView_bgColor = 14;
        public static final int QWAdView_defaultAdClickThru = 8;
        public static final int QWAdView_defaultAdImage = 7;
        public static final int QWAdView_displayMode = 2;
        public static final int QWAdView_mediaType = 1;
        public static final int QWAdView_placement = 0;
        public static final int QWAdView_publisherId = 6;
        public static final int QWAdView_renderAdOnCreate = 12;
        public static final int QWAdView_requestMode = 13;
        public static final int QWAdView_section = 9;
        public static final int QWAdView_siteId = 5;
        public static final int QWAdView_testMode = 11;
        public static final int QWAdView_textColor = 15;
    }

    public static final class xml {
        public static final int preferences = 2131034112;
    }
}
