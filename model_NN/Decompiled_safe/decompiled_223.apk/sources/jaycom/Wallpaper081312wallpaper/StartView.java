package jaycom.Wallpaper081312wallpaper;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class StartView extends Activity {
    protected Intent MaimMenuScreenItent;
    private ImageView eoeLogo;
    private Animation fadeIn_Out;
    /* access modifiers changed from: private */
    public boolean m_bPaused;
    /* access modifiers changed from: private */
    public boolean m_bSplashActive;
    /* access modifiers changed from: private */
    public long m_dwSplashTime;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
        requestWindowFeature(1);
        setRequestedOrientation(1);
        InitView();
        InitAnmation();
        StartFlashScreen();
    }

    private void StartFlashScreen() {
        this.eoeLogo.startAnimation(this.fadeIn_Out);
        this.m_bPaused = false;
        this.m_bSplashActive = true;
        this.m_dwSplashTime = 800;
        new Thread() {
            public void run() {
                int ms = 0;
                while (StartView.this.m_bSplashActive && ((long) ms) < StartView.this.m_dwSplashTime) {
                    try {
                        sleep(100);
                        if (!StartView.this.m_bPaused) {
                            ms += 100;
                        }
                    } catch (Exception e) {
                        return;
                    }
                }
                StartView.this.MaimMenuScreenItent = new Intent();
                StartView.this.MaimMenuScreenItent.setClass(StartView.this, WallpaperView.class);
                StartView.this.startActivity(StartView.this.MaimMenuScreenItent);
                StartView.this.finish();
            }
        }.start();
    }

    private void InitAnmation() {
        this.fadeIn_Out = AnimationUtils.loadAnimation(this, R.anim.myown_design);
    }

    private void InitView() {
        setContentView((int) R.layout.flashscreen);
        this.eoeLogo = (ImageView) findViewById(R.id.eoeLogo);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        whichKey(keyCode);
        return true;
    }

    public boolean onKeyMultiple(int keyCode, int repeatCount, KeyEvent event) {
        whichKey(keyCode);
        return true;
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        whichKey(keyCode);
        return true;
    }

    private void whichKey(int keyCode) {
        switch (keyCode) {
        }
    }
}
