package com.mobclix.android.sdk;

import android.app.Activity;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class MobclixFeedback {
    private static String TAG = "mobclixFeedback";
    static Mobclix controller = Mobclix.getInstance();

    public interface Listener {
        void onFailure();

        void onSuccess();
    }

    public static void sendComment(Activity c, String comment) {
        sendComment(c, comment, null);
    }

    public static void sendComment(Activity c, String comment, Listener l) {
        if (comment != null) {
            comment.trim();
            if (comment.length() != 0) {
                Mobclix.onCreate(c);
                String url = controller.getFeedbackServer();
                StringBuffer params = new StringBuffer();
                try {
                    params.append("p=android&t=com");
                    params.append("&a=").append(URLEncoder.encode(controller.getApplicationId(), "UTF-8"));
                    params.append("&v=").append(URLEncoder.encode(controller.getApplicationVersion(), "UTF-8"));
                    params.append("&m=").append(URLEncoder.encode(controller.getMobclixVersion(), "UTF-8"));
                    params.append("&d=").append(URLEncoder.encode(controller.getDeviceId(), "UTF-8"));
                    params.append("&dt=").append(URLEncoder.encode(controller.getDeviceModel(), "UTF-8"));
                    params.append("&os=").append(URLEncoder.encode(controller.getAndroidVersion(), "UTF-8"));
                    params.append("&c=").append(URLEncoder.encode(comment, "UTF-8"));
                    new Thread(new POSTThread(url, params.toString(), l)).run();
                } catch (UnsupportedEncodingException e) {
                }
            }
        }
    }

    public static void sendRatings(Activity c, Ratings ratings) {
        sendRatings(c, ratings, null);
    }

    public static void sendRatings(Activity c, Ratings ratings, Listener l) {
        if (ratings != null) {
            Mobclix.onCreate(c);
            String url = controller.getFeedbackServer();
            StringBuffer params = new StringBuffer();
            try {
                params.append("p=android&t=rat");
                params.append("&a=").append(URLEncoder.encode(controller.getApplicationId(), "UTF-8"));
                params.append("&v=").append(URLEncoder.encode(controller.getApplicationVersion(), "UTF-8"));
                params.append("&m=").append(URLEncoder.encode(controller.getMobclixVersion(), "UTF-8"));
                params.append("&d=").append(URLEncoder.encode(controller.getDeviceId(), "UTF-8"));
                params.append("&dt=").append(URLEncoder.encode(controller.getDeviceModel(), "UTF-8"));
                params.append("&os=").append(URLEncoder.encode(controller.getAndroidVersion(), "UTF-8"));
                params.append("&1=").append(ratings.a);
                params.append("&2=").append(ratings.b);
                params.append("&3=").append(ratings.c);
                params.append("&4=").append(ratings.d);
                params.append("&5=").append(ratings.e);
                new Thread(new POSTThread(url, params.toString(), l)).run();
            } catch (UnsupportedEncodingException e) {
            }
        }
    }

    public static class Ratings {
        int a = 0;
        int b = 0;
        int c = 0;
        int d = 0;
        int e = 0;

        public void setCategoryA(int v) {
            this.a = v;
        }

        public void setCategoryB(int v) {
            this.b = v;
        }

        public void setCategoryC(int v) {
            this.c = v;
        }

        public void setCategoryD(int v) {
            this.d = v;
        }

        public void setCategoryE(int v) {
            this.e = v;
        }
    }

    static class POSTThread implements Runnable {
        Mobclix controller = Mobclix.getInstance();
        Listener listener;
        String params;
        String url;

        POSTThread(String u, String p, Listener l) {
            this.url = u;
            this.params = p;
            this.listener = l;
        }

        /* JADX WARNING: Removed duplicated region for block: B:27:0x0066 A[SYNTHETIC, Splitter:B:27:0x0066] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r9 = this;
                r8 = 1
                r3 = 0
                r1 = 0
            L_0x0003:
                com.mobclix.android.sdk.Mobclix r7 = r9.controller
                int r7 = r7.isRemoteConfigSet()
                if (r7 != r8) goto L_0x0003
                java.net.URL r6 = new java.net.URL     // Catch:{ Exception -> 0x0077 }
                java.lang.String r7 = r9.url     // Catch:{ Exception -> 0x0077 }
                r6.<init>(r7)     // Catch:{ Exception -> 0x0077 }
                java.net.URLConnection r7 = r6.openConnection()     // Catch:{ Exception -> 0x0077 }
                r0 = r7
                java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x0077 }
                r1 = r0
                java.lang.String r7 = "POST"
                r1.setRequestMethod(r7)     // Catch:{ Exception -> 0x0077 }
                r7 = 1
                r1.setDoInput(r7)     // Catch:{ Exception -> 0x0077 }
                r7 = 1
                r1.setDoOutput(r7)     // Catch:{ Exception -> 0x0077 }
                java.io.DataOutputStream r4 = new java.io.DataOutputStream     // Catch:{ Exception -> 0x0077 }
                java.io.OutputStream r7 = r1.getOutputStream()     // Catch:{ Exception -> 0x0077 }
                r4.<init>(r7)     // Catch:{ Exception -> 0x0077 }
                java.lang.String r7 = r9.params     // Catch:{ Exception -> 0x0051, all -> 0x0074 }
                r4.writeBytes(r7)     // Catch:{ Exception -> 0x0051, all -> 0x0074 }
                int r5 = r1.getResponseCode()     // Catch:{ Exception -> 0x0051, all -> 0x0074 }
                r7 = 200(0xc8, float:2.8E-43)
                if (r5 != r7) goto L_0x004d
                r9.onSuccess()     // Catch:{ Exception -> 0x0051, all -> 0x0074 }
            L_0x0040:
                r1.disconnect()     // Catch:{ Exception -> 0x0051, all -> 0x0074 }
                if (r4 == 0) goto L_0x007a
                r4.flush()     // Catch:{ Exception -> 0x0070 }
                r4.close()     // Catch:{ Exception -> 0x0070 }
                r3 = r4
            L_0x004c:
                return
            L_0x004d:
                r9.onFailure()     // Catch:{ Exception -> 0x0051, all -> 0x0074 }
                goto L_0x0040
            L_0x0051:
                r7 = move-exception
                r2 = r7
                r3 = r4
            L_0x0054:
                r9.onFailure()     // Catch:{ all -> 0x0063 }
                if (r3 == 0) goto L_0x004c
                r3.flush()     // Catch:{ Exception -> 0x0060 }
                r3.close()     // Catch:{ Exception -> 0x0060 }
                goto L_0x004c
            L_0x0060:
                r7 = move-exception
                r2 = r7
                goto L_0x004c
            L_0x0063:
                r7 = move-exception
            L_0x0064:
                if (r3 == 0) goto L_0x006c
                r3.flush()     // Catch:{ Exception -> 0x006d }
                r3.close()     // Catch:{ Exception -> 0x006d }
            L_0x006c:
                throw r7
            L_0x006d:
                r7 = move-exception
                r2 = r7
                goto L_0x004c
            L_0x0070:
                r7 = move-exception
                r2 = r7
                r3 = r4
                goto L_0x004c
            L_0x0074:
                r7 = move-exception
                r3 = r4
                goto L_0x0064
            L_0x0077:
                r7 = move-exception
                r2 = r7
                goto L_0x0054
            L_0x007a:
                r3 = r4
                goto L_0x004c
            */
            throw new UnsupportedOperationException("Method not decompiled: com.mobclix.android.sdk.MobclixFeedback.POSTThread.run():void");
        }

        public void onSuccess() {
            if (this.listener != null) {
                this.controller.getContext().runOnUiThread(new Runnable() {
                    public void run() {
                        POSTThread.this.listener.onSuccess();
                    }
                });
            }
        }

        public void onFailure() {
            if (this.listener != null) {
                this.controller.getContext().runOnUiThread(new Runnable() {
                    public void run() {
                        POSTThread.this.listener.onFailure();
                    }
                });
            }
        }
    }
}
