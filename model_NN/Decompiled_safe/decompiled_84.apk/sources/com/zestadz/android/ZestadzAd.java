package com.zestadz.android;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

public class ZestadzAd extends RelativeLayout {
    private static String mAdText;
    private static Timer t;
    private final String MY_DEBUG_TAG;
    /* access modifiers changed from: private */
    public Handler handler;
    /* access modifiers changed from: private */
    public boolean mAdVisibility;
    /* access modifiers changed from: private */
    public String userAgent;

    public ZestadzAd(Context context) {
        this(context, null, 0);
    }

    public ZestadzAd(Context context, AttributeSet attr) {
        this(context, attr, 0);
    }

    public ZestadzAd(Context context, AttributeSet attr, int defStyle) {
        super(context, attr, defStyle);
        this.MY_DEBUG_TAG = "ZestadzAD";
        setAdVisibility(true);
        if (CheckAdVisibility()) {
            displayAd(context);
            return;
        }
        if (t != null) {
            t.cancel();
        }
        RelativeLayout container = new RelativeLayout(context);
        setAdText("AD cannot be Displayed");
        TextView mTextView = new TextView(context);
        mTextView.setText(getAdText());
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(-1, -2);
        params.addRule(12);
        mTextView.setLayoutParams(params);
        container.addView(mTextView);
    }

    /* access modifiers changed from: package-private */
    public Drawable ImageOperations(Context ctx, String url, String saveFilename) {
        try {
            return Drawable.createFromStream((InputStream) fetch(url), "src");
        } catch (MalformedURLException e) {
            MalformedURLException e2 = e;
            Log.e("IMAGE OPERATION", e2.getMessage());
            e2.printStackTrace();
            return null;
        } catch (IOException e3) {
            IOException e4 = e3;
            Log.e("IMAGE OPERATION", e4.getMessage());
            e4.printStackTrace();
            return null;
        }
    }

    public Object fetch(String address) throws MalformedURLException, IOException {
        try {
            return new URL(address).getContent();
        } catch (Exception e) {
            Exception e2 = e;
            e2.printStackTrace();
            Log.e("FETCH", e2.toString());
            return null;
        }
    }

    public boolean CheckAdVisibility() {
        return this.mAdVisibility;
    }

    public void setAdVisibility(boolean vis) {
        this.mAdVisibility = vis;
    }

    public void displayAd(Context context) {
        final RelativeLayout relativeLayout = new RelativeLayout(context);
        this.handler = new Handler();
        try {
            this.userAgent = AdManager.getUserAgent(context);
            final URL url = new URL("http://a.zestadz.com/multiple_ad?ua=" + this.userAgent + "&ip=&cid=" + AdManager.getAdclientId(context) + "&meta=game&keyword=All&response_type=xml&recs=5");
            final SAXParser sp = SAXParserFactory.newInstance().newSAXParser();
            final ImageView imageView = new ImageView(context);
            final TextView textView = new TextView(context);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
            layoutParams.addRule(12);
            imageView.setLayoutParams(layoutParams);
            textView.setLayoutParams(layoutParams);
            t = new Timer();
            final Context context2 = context;
            t.scheduleAtFixedRate(new TimerTask() {
                public void run() {
                    final SAXParser sAXParser = sp;
                    final URL url = url;
                    final Context context = context2;
                    final ImageView imageView = imageView;
                    final RelativeLayout relativeLayout = relativeLayout;
                    final TextView textView = textView;
                    new Thread() {
                        public void run() {
                            try {
                                if (!ZestadzAd.this.userAgent.equalsIgnoreCase("Mozilla/5.0 (Linux; U; Android 1.5; en-us; google_sdk Build/CUPCAKE) AppleWebKit/525.10+ (KHTML, like Gecko) Version/3.0.4 Mobile Safari/523.12.2")) {
                                    XMLReader xr = sAXParser.getXMLReader();
                                    xr.setContentHandler(new ExampleHandler());
                                    URLConnection con = url.openConnection();
                                    con.setConnectTimeout(5000);
                                    con.setReadTimeout(10000);
                                    xr.parse(new InputSource(con.getInputStream()));
                                    List<String> extPicture = ParsedExampleDataSet.getExtractedPicture();
                                    List<String> extAdurl = ParsedExampleDataSet.getExtractedURL();
                                    List<String> extText = ParsedExampleDataSet.getExtractedText();
                                    List<String> extError = ParsedExampleDataSet.getExtractedError();
                                    List<String> extAdType = ParsedExampleDataSet.getExtractedAdType();
                                    for (int i = 0; i < extPicture.size(); i++) {
                                        if (!extAdType.get(i).equals("NA") && !extAdType.get(i).equals("text")) {
                                            final Drawable image = ZestadzAd.this.ImageOperations(context, extPicture.get(i).toString(), "image.jpg");
                                            final String mextURL = extAdurl.get(i).toString();
                                            Handler access$1 = ZestadzAd.this.handler;
                                            final ImageView imageView = imageView;
                                            final RelativeLayout relativeLayout = relativeLayout;
                                            final Context context = context;
                                            access$1.post(new Runnable() {
                                                public void run() {
                                                    imageView.setImageDrawable(image);
                                                    imageView.setAdjustViewBounds(ZestadzAd.this.mAdVisibility);
                                                    ZestadzAd.this.setAdVisibility(true);
                                                    imageView.setVisibility(0);
                                                    imageView.setAdjustViewBounds(true);
                                                    imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                                                    ImageView imageView = imageView;
                                                    final Context context = context;
                                                    final String str = mextURL;
                                                    imageView.setOnClickListener(new View.OnClickListener() {
                                                        public void onClick(View v) {
                                                            ProgressDialog mProgress = ProgressDialog.show(context, "Redirecting to Landing page", "Loading. Please wait...", true);
                                                            Intent intent = new Intent("android.intent.action.VIEW");
                                                            intent.setData(Uri.parse(str));
                                                            ((Activity) context).startActivity(intent);
                                                            mProgress.dismiss();
                                                        }
                                                    });
                                                    relativeLayout.removeAllViews();
                                                    relativeLayout.addView(imageView);
                                                }
                                            });
                                        } else if (extAdType.equals("NA")) {
                                            Log.i("Error has arised in response XML", extError.get(i).toString());
                                            ZestadzAd.this.setAdText(extError.get(i).toString());
                                            Handler access$12 = ZestadzAd.this.handler;
                                            final RelativeLayout relativeLayout2 = relativeLayout;
                                            final TextView textView = textView;
                                            access$12.post(new Runnable() {
                                                public void run() {
                                                    relativeLayout2.removeAllViews();
                                                    relativeLayout2.addView(textView);
                                                }
                                            });
                                        } else {
                                            if (extText.get(i).toString().equals("NA")) {
                                                ZestadzAd.this.setAdText("");
                                            } else {
                                                ZestadzAd.this.setAdText(extText.get(i).toString());
                                            }
                                            Log.i("Text Ad is the Reponse", extText.get(i).toString());
                                            Handler access$13 = ZestadzAd.this.handler;
                                            final TextView textView2 = textView;
                                            final RelativeLayout relativeLayout3 = relativeLayout;
                                            access$13.post(new Runnable() {
                                                public void run() {
                                                    textView2.setText(ZestadzAd.getAdText());
                                                    relativeLayout3.removeAllViews();
                                                    relativeLayout3.addView(textView2);
                                                }
                                            });
                                        }
                                        if (i < extPicture.size() - 1) {
                                            sleep((long) (60000 / extPicture.size()));
                                        }
                                    }
                                    return;
                                }
                                ArrayList arrayList = new ArrayList();
                                arrayList.add("http://assets.zestadz.com/ad/ad_picture/4299/chart_300x50-xlarge-xlarge.gif");
                                arrayList.add("http://assets.zestadz.com/ad/ad_picture/4497/flirtThousandsV2-300x50-xlarge-xlarge.gif");
                                arrayList.add("http://assets.zestadz.com/ad/ad_picture/4285/cricket300x50v1-xlarge.gif");
                                arrayList.add("http://assets.zestadz.com/ad/ad_picture/4212/pacman-xlarge-xlarge.gif");
                                arrayList.add("http://assets.zestadz.com/ad/ad_picture/4183/300x50-xlarge-xlarge.gif");
                                for (int i2 = 0; i2 < arrayList.size(); i2++) {
                                    final Drawable image2 = ZestadzAd.this.ImageOperations(context, ((String) arrayList.get(i2)).toString(), "image.jpg");
                                    Handler access$14 = ZestadzAd.this.handler;
                                    final ImageView imageView2 = imageView;
                                    final RelativeLayout relativeLayout4 = relativeLayout;
                                    final Context context2 = context;
                                    access$14.post(new Runnable() {
                                        public void run() {
                                            imageView2.setImageDrawable(image2);
                                            imageView2.setAdjustViewBounds(ZestadzAd.this.mAdVisibility);
                                            ZestadzAd.this.setAdVisibility(true);
                                            imageView2.setVisibility(0);
                                            imageView2.setAdjustViewBounds(true);
                                            imageView2.setScaleType(ImageView.ScaleType.FIT_XY);
                                            ImageView imageView = imageView2;
                                            final Context context = context2;
                                            imageView.setOnClickListener(new View.OnClickListener() {
                                                public void onClick(View v) {
                                                    ProgressDialog mProgress = ProgressDialog.show(context, "Redirecting to Landing page", "Loading. Please wait...", true);
                                                    Intent intent = new Intent("android.intent.action.VIEW");
                                                    intent.setData(Uri.parse("http://www.zestadz.com"));
                                                    ((Activity) context).startActivity(intent);
                                                    ((Activity) context).finish();
                                                    mProgress.dismiss();
                                                }
                                            });
                                            relativeLayout4.removeAllViews();
                                            relativeLayout4.addView(imageView2);
                                        }
                                    });
                                    if (i2 < arrayList.size() - 1) {
                                        sleep((long) (60000 / arrayList.size()));
                                    }
                                }
                            } catch (Exception e) {
                                Exception e2 = e;
                                e2.printStackTrace();
                                ZestadzAd.this.setAdText(" ");
                                Handler access$15 = ZestadzAd.this.handler;
                                final TextView textView3 = textView;
                                final RelativeLayout relativeLayout5 = relativeLayout;
                                access$15.post(new Runnable() {
                                    public void run() {
                                        textView3.setText(ZestadzAd.getAdText());
                                        relativeLayout5.removeAllViews();
                                        relativeLayout5.addView(textView3);
                                    }
                                });
                                Log.e("PARSE XML", e2.toString());
                            }
                        }
                    }.start();
                    ParsedExampleDataSet.extractedText.clear();
                    ParsedExampleDataSet.extractedPicture.clear();
                    ParsedExampleDataSet.extractedURL.clear();
                    ParsedExampleDataSet.extractedAdType.clear();
                    ParsedExampleDataSet.ExtractedError.clear();
                }
            }, 0, 60000);
        } catch (Exception e) {
            setAdText(" ");
            final Context context3 = context;
            final RelativeLayout relativeLayout2 = relativeLayout;
            this.handler.post(new Runnable() {
                public void run() {
                    TextView mTextView = new TextView(context3);
                    mTextView.setText(ZestadzAd.getAdText());
                    RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(-1, -2);
                    params.addRule(12);
                    mTextView.setLayoutParams(params);
                    relativeLayout2.removeAllViews();
                    relativeLayout2.addView(mTextView);
                }
            });
            Log.e("ZestadzAD", "Display ZestADZ Ad", e);
        }
        addView(relativeLayout);
    }

    public void setAdText(String text) {
        mAdText = text;
    }

    public static String getAdText() {
        return mAdText;
    }

    public static void stopAdpull() {
        t.cancel();
    }
}
