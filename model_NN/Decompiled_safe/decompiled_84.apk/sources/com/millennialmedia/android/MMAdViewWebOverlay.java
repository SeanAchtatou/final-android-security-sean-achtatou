package com.millennialmedia.android;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import cfg.Option;
import com.adwhirl.util.AdWhirlUtil;
import com.millennialmedia.android.MMAdViewSDK;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

class MMAdViewWebOverlay extends FrameLayout {
    private static final int kTitleMarginX = 8;
    private static final int kTitleMarginY = 9;
    private static final int kTransitionDuration = 200;
    private Button backButton;
    private Drawable close;
    private Drawable closeDisabled;
    private ConnectivityManager cm;
    private LinearLayout content;
    private Button forwardButton;
    private Drawable leftArrow;
    private Drawable leftArrowDisabled;
    /* access modifiers changed from: private */
    public RelativeLayout navBar;
    private Button navCloseButton;
    /* access modifiers changed from: private */
    public String overlayUrl;
    private Drawable rightArrow;
    private Drawable rightArrowDisabled;
    /* access modifiers changed from: private */
    public TextView title;
    Handler viewHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 2:
                    MMAdViewWebOverlay.this.dismiss(true);
                    return;
                default:
                    return;
            }
        }
    };
    protected WebView webView;

    MMAdViewWebOverlay(Context activity, int padding, long time, String transition, boolean titlebar, String titleText, boolean navbar, boolean bottombarEnabled, boolean isTransparent) {
        super(activity);
        setId(15062);
        if (activity != null) {
            this.cm = (ConnectivityManager) activity.getSystemService("connectivity");
            setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
            float scale = activity.getResources().getDisplayMetrics().density;
            Integer scaledPadding = Integer.valueOf((int) (0.0625f * scale * ((float) padding)));
            setPadding(scaledPadding.intValue(), scaledPadding.intValue(), scaledPadding.intValue(), scaledPadding.intValue());
            this.content = new LinearLayout(activity);
            this.content.setOrientation(1);
            this.content.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
            addView(this.content);
            if (titlebar) {
                RelativeLayout relativeLayout = new RelativeLayout(activity);
                relativeLayout.setLayoutParams(new FrameLayout.LayoutParams(-1, -2));
                relativeLayout.setBackgroundColor(Option.HINT_COLOR_DEFAULT);
                relativeLayout.setId(100);
                this.title = new TextView(activity);
                this.title.setText(titleText);
                this.title.setTextColor(-1);
                this.title.setBackgroundColor(Option.HINT_COLOR_DEFAULT);
                this.title.setTypeface(Typeface.DEFAULT_BOLD);
                this.title.setPadding(8, 9, 8, 9);
                this.title.setLayoutParams(new FrameLayout.LayoutParams(-2, -2));
                relativeLayout.addView(this.title);
                Button button = new Button(activity);
                button.setBackgroundColor(Option.HINT_COLOR_DEFAULT);
                button.setText("Close");
                button.setTextColor(-1);
                button.setOnTouchListener(new View.OnTouchListener() {
                    public boolean onTouch(View view2, MotionEvent event) {
                        switch (event.getAction()) {
                            case 0:
                                MMAdViewSDK.Log.v("Close button down");
                                return true;
                            case 1:
                                MMAdViewWebOverlay.this.title.setBackgroundColor(-7829368);
                                MMAdViewSDK.Log.v("Close button up");
                                MMAdViewWebOverlay.this.dismiss(true);
                                return true;
                            default:
                                return false;
                        }
                    }
                });
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
                layoutParams.addRule(11);
                relativeLayout.addView(button, layoutParams);
                this.content.addView(relativeLayout);
            }
            this.webView = new WebView(activity);
            this.webView.setId(kTransitionDuration);
            LinearLayout.LayoutParams webviewLp = new LinearLayout.LayoutParams(-1, -1);
            webviewLp.weight = 1.0f;
            this.webView.setLayoutParams(webviewLp);
            this.webView.setWebViewClient(new OverlayWebViewClient());
            this.webView.addJavascriptInterface(new OverlayJSInterface(), "interface");
            WebSettings webSettings = this.webView.getSettings();
            webSettings.setJavaScriptEnabled(true);
            webSettings.setDefaultTextEncodingName("UTF-8");
            if (isTransparent) {
                this.webView.setBackgroundColor(0);
                this.content.setBackgroundColor(0);
            } else {
                this.webView.setBackgroundColor(-1);
                this.content.setBackgroundColor(-1);
            }
            this.content.addView(this.webView);
            int navHeight = (int) ((50.0f * scale) + 0.5f);
            this.navBar = new RelativeLayout(activity);
            this.navBar.setLayoutParams(new FrameLayout.LayoutParams(-1, -2));
            this.navBar.setBackgroundColor(-3355444);
            this.navBar.setId(AdWhirlUtil.VERSION);
            this.navCloseButton = new Button(activity);
            this.navCloseButton.setBackgroundColor(Option.HINT_COLOR_DEFAULT);
            AssetManager am = activity.getAssets();
            try {
                this.close = Drawable.createFromStream(am.open("millennial_close.png"), "millennial_close.png");
                this.closeDisabled = Drawable.createFromStream(am.open("millennial_close_disabled.png"), "millennial_close_disabled.png");
            } catch (IOException e) {
                e.printStackTrace();
            }
            setCloseButtonListener(bottombarEnabled);
            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(navHeight, navHeight);
            lp.addRule(11);
            lp.addRule(15);
            this.navBar.addView(this.navCloseButton, lp);
            this.content.addView(this.navBar);
            if (navbar) {
                this.navBar.setVisibility(0);
            } else {
                this.navBar.setVisibility(8);
            }
            animateView(transition, time);
        }
    }

    private void animateView(String animation, long time) {
        if (animation == null) {
            animation = "bottomtotop";
        }
        if (animation.equals("toptobottom")) {
            TranslateAnimation translateDown = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, -1.0f, 1, 0.0f);
            translateDown.setDuration(time);
            translateDown.setAnimationListener(new Animation.AnimationListener() {
                public void onAnimationEnd(Animation arg0) {
                }

                public void onAnimationRepeat(Animation arg0) {
                }

                public void onAnimationStart(Animation arg0) {
                }
            });
            MMAdViewSDK.Log.v("Translate down");
            startAnimation(translateDown);
        } else if (animation.equals("explode")) {
            ScaleAnimation scale = new ScaleAnimation(1.1f, 0.9f, 0.1f, 0.9f, 1, 0.5f, 1, 0.5f);
            scale.setDuration(time);
            scale.setAnimationListener(new Animation.AnimationListener() {
                public void onAnimationEnd(Animation arg0) {
                }

                public void onAnimationRepeat(Animation arg0) {
                }

                public void onAnimationStart(Animation arg0) {
                }
            });
            MMAdViewSDK.Log.v("Explode");
            startAnimation(scale);
        } else {
            TranslateAnimation translateUp = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 1.0f, 1, 0.0f);
            translateUp.setDuration(time);
            translateUp.setAnimationListener(new Animation.AnimationListener() {
                public void onAnimationEnd(Animation arg0) {
                }

                public void onAnimationRepeat(Animation arg0) {
                }

                public void onAnimationStart(Animation arg0) {
                }
            });
            MMAdViewSDK.Log.v("Translate up");
            startAnimation(translateUp);
        }
    }

    /* access modifiers changed from: package-private */
    public boolean goBack() {
        if (!this.webView.canGoBack()) {
            return false;
        }
        this.webView.goBack();
        return true;
    }

    /* access modifiers changed from: package-private */
    public void loadWebContent(String url) {
        this.overlayUrl = url;
        if (this.cm.getActiveNetworkInfo() == null || !this.cm.getActiveNetworkInfo().isConnected()) {
            Log.e(MMAdViewSDK.SDKLOG, "No network available, can't load overlay.");
        } else {
            new Thread(new Runnable() {
                public void run() {
                    MMAdViewWebOverlay.this.webView.loadUrl(MMAdViewWebOverlay.this.overlayUrl);
                }
            }).start();
        }
    }

    /* access modifiers changed from: package-private */
    public void injectJS(String jsString) {
        this.webView.loadUrl(jsString);
    }

    /* access modifiers changed from: protected */
    public void setForwardButtonListener(boolean enabled) {
        if (this.forwardButton == null) {
            return;
        }
        if (enabled) {
            this.forwardButton.setBackgroundDrawable(this.rightArrow);
            this.forwardButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view2) {
                    MMAdViewSDK.Log.v("Forward button up");
                    MMAdViewWebOverlay.this.webView.goForward();
                }
            });
            this.forwardButton.setEnabled(true);
            return;
        }
        this.forwardButton.setBackgroundDrawable(this.rightArrowDisabled);
        this.forwardButton.setEnabled(false);
    }

    /* access modifiers changed from: protected */
    public void setBackButtonListener(boolean enabled) {
        if (this.backButton == null) {
            return;
        }
        if (enabled) {
            this.backButton.setBackgroundDrawable(this.leftArrow);
            this.backButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view2) {
                    MMAdViewWebOverlay.this.webView.goBack();
                    MMAdViewSDK.Log.v("Back button up");
                }
            });
            this.backButton.setEnabled(true);
            return;
        }
        this.backButton.setBackgroundDrawable(this.leftArrowDisabled);
        this.backButton.setEnabled(false);
    }

    /* access modifiers changed from: protected */
    public void setCloseButtonListener(boolean enabled) {
        if (this.navCloseButton == null) {
            return;
        }
        if (enabled) {
            this.navCloseButton.setBackgroundDrawable(this.close);
            this.navCloseButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view2) {
                    MMAdViewSDK.Log.v("Close button");
                    MMAdViewWebOverlay.this.dismiss(true);
                }
            });
            this.navCloseButton.setEnabled(true);
            return;
        }
        this.navCloseButton.setBackgroundDrawable(this.closeDisabled);
        this.navCloseButton.setEnabled(false);
    }

    /* access modifiers changed from: private */
    public void dismiss(boolean animated) {
        MMAdViewSDK.Log.d("Ad overlay closed");
        Activity activity = (Activity) getContext();
        if (activity != null) {
            if (animated) {
                AlphaAnimation animation = new AlphaAnimation(1.0f, 0.0f);
                animation.setDuration(200);
                activity.finish();
                startAnimation(animation);
                return;
            }
            activity.finish();
        }
    }

    class OverlayJSInterface {
        OverlayJSInterface() {
        }

        public void shouldCloseOverlay() {
            MMAdViewWebOverlay.this.viewHandler.sendEmptyMessage(2);
        }

        public void shouldVibrate(long milliseconds) {
            if (MMAdViewWebOverlay.this.getContext().checkCallingOrSelfPermission("android.permission.VIBRATE") == 0) {
                ((Vibrator) MMAdViewWebOverlay.this.getContext().getSystemService("vibrator")).vibrate(milliseconds);
            }
        }

        public void shouldEnableBottomBar(final boolean enabled) {
            MMAdViewSDK.Log.d("Should Enable Bottom Bar: " + enabled);
            MMAdViewWebOverlay.this.viewHandler.post(new Runnable() {
                public void run() {
                    MMAdViewWebOverlay.this.setCloseButtonListener(enabled);
                    MMAdViewWebOverlay.this.setForwardButtonListener(enabled);
                    MMAdViewWebOverlay.this.setBackButtonListener(enabled);
                }
            });
        }

        public void shouldShowBottomBar(final boolean show) {
            MMAdViewSDK.Log.d("Should show Bottom Bar: " + show);
            MMAdViewWebOverlay.this.viewHandler.post(new Runnable() {
                public void run() {
                    if (MMAdViewWebOverlay.this.navBar == null) {
                        return;
                    }
                    if (show) {
                        MMAdViewWebOverlay.this.navBar.setVisibility(0);
                    } else {
                        MMAdViewWebOverlay.this.navBar.setVisibility(8);
                    }
                }
            });
        }
    }

    final class OverlayWebViewClient extends WebViewClient {
        OverlayWebViewClient() {
        }

        public boolean shouldOverrideUrlLoading(WebView view2, String urlString) {
            String locationString;
            int rc;
            if (urlString != null) {
                String mimeTypeString = null;
                do {
                    locationString = urlString;
                    try {
                        URL connectURL = new URL(urlString);
                        HttpURLConnection.setFollowRedirects(false);
                        HttpURLConnection conn = (HttpURLConnection) connectURL.openConnection();
                        conn.setRequestMethod("GET");
                        conn.connect();
                        urlString = conn.getHeaderField("Location");
                        mimeTypeString = conn.getHeaderField("Content-Type");
                        rc = conn.getResponseCode();
                        MMAdViewSDK.Log.d("Response Code:" + conn.getResponseCode() + " Response Message:" + conn.getResponseMessage());
                        MMAdViewSDK.Log.d("urlString: " + urlString);
                        if (rc < 300) {
                            break;
                        }
                    } catch (MalformedURLException e) {
                    } catch (IOException e2) {
                        e2.printStackTrace();
                    }
                } while (rc < 400);
                Activity activity = (Activity) view2.getContext();
                if (activity == null) {
                    MMAdViewSDK.Log.d("Activity is null. Returning from click");
                    return false;
                } else if (locationString == null) {
                    return false;
                } else {
                    Uri destinationURI = Uri.parse(locationString);
                    MMAdViewSDK.Log.d("DestinationURI: " + destinationURI.toString());
                    if (mimeTypeString == null) {
                        mimeTypeString = "";
                    }
                    if (!(destinationURI == null || destinationURI.getScheme() == null)) {
                        if (destinationURI.getScheme().equalsIgnoreCase("market")) {
                            MMAdViewSDK.Log.d("Android Market URL, launch the Market Application");
                            activity.startActivity(new Intent("android.intent.action.VIEW", destinationURI));
                        } else if (destinationURI.getScheme().equalsIgnoreCase("rtsp") || (destinationURI.getScheme().equalsIgnoreCase("http") && (mimeTypeString.equalsIgnoreCase("video/mp4") || mimeTypeString.equalsIgnoreCase("video/3gpp")))) {
                            MMAdViewSDK.Log.d("Ignore MalFormedUrlException for RTSP");
                            MMAdViewSDK.Log.d("Video, launch the video player for video at: " + destinationURI);
                            Intent intent = new Intent(activity, VideoPlayer.class);
                            intent.setData(destinationURI);
                            activity.startActivityForResult(intent, 0);
                        } else if (destinationURI.getScheme().equalsIgnoreCase("tel")) {
                            MMAdViewSDK.Log.d("Telephone Number, launch the phone");
                            activity.startActivity(new Intent("android.intent.action.DIAL", destinationURI));
                        } else if (destinationURI.getScheme().equalsIgnoreCase("sms")) {
                            MMAdViewSDK.Log.d("Text message.");
                            activity.startActivity(new Intent("android.intent.action.VIEW", destinationURI));
                        } else if (destinationURI.getScheme().equalsIgnoreCase("geo")) {
                            MMAdViewSDK.Log.d("Google Maps");
                            activity.startActivity(new Intent("android.intent.action.VIEW", destinationURI));
                        } else if (!destinationURI.getScheme().equalsIgnoreCase("http") || destinationURI.getLastPathSegment() == null) {
                            MMAdViewSDK.Log.d("Uncertain about content. Stay in the overlay");
                            view2.loadUrl(destinationURI.toString());
                            shouldShowAndEnableBottomBar();
                        } else if (destinationURI.getLastPathSegment().endsWith(".mp4") || destinationURI.getLastPathSegment().endsWith(".3gp")) {
                            MMAdViewSDK.Log.d("Video, launch the video player for video at: " + destinationURI);
                            Intent intent2 = new Intent(activity, VideoPlayer.class);
                            intent2.setData(destinationURI);
                            activity.startActivityForResult(intent2, 0);
                        } else {
                            view2.loadUrl(destinationURI.toString());
                            shouldShowAndEnableBottomBar();
                        }
                    }
                }
            }
            return true;
        }

        public void onReceivedError(WebView view2, Error errorCode, String description, String failingUrl) {
            Log.e(MMAdViewSDK.SDKLOG, "Error: " + errorCode + "  " + description);
        }

        public void shouldShowAndEnableBottomBar() {
            MMAdViewSDK.Log.v("Showing and enabling bottom bar");
            if (MMAdViewWebOverlay.this.navBar != null) {
                MMAdViewWebOverlay.this.navBar.setVisibility(0);
                MMAdViewWebOverlay.this.setCloseButtonListener(true);
                MMAdViewWebOverlay.this.setForwardButtonListener(true);
                MMAdViewWebOverlay.this.setBackButtonListener(true);
            }
        }
    }
}
