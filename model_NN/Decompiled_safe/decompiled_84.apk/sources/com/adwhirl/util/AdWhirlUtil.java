package com.adwhirl.util;

import android.app.Activity;
import android.util.DisplayMetrics;

public class AdWhirlUtil {
    public static final String ADWHIRL = "AdWhirl SDK";
    public static final int CUSTOM_TYPE_BANNER = 1;
    public static final int CUSTOM_TYPE_ICON = 2;
    public static final int NETWORK_TYPE_4THSCREEN = 13;
    public static final int NETWORK_TYPE_ADMOB = 1;
    public static final int NETWORK_TYPE_ADSENSE = 14;
    public static final int NETWORK_TYPE_ADWHIRL = 10;
    public static final int NETWORK_TYPE_CUSTOM = 9;
    public static final int NETWORK_TYPE_DOUBLECLICK = 15;
    public static final int NETWORK_TYPE_EVENT = 17;
    public static final int NETWORK_TYPE_GENERIC = 16;
    public static final int NETWORK_TYPE_GREYSTRIP = 7;
    public static final int NETWORK_TYPE_INMOBI = 18;
    public static final int NETWORK_TYPE_JUMPTAP = 2;
    public static final int NETWORK_TYPE_LIVERAIL = 5;
    public static final int NETWORK_TYPE_MDOTM = 12;
    public static final int NETWORK_TYPE_MEDIALETS = 4;
    public static final int NETWORK_TYPE_MILLENNIAL = 6;
    public static final int NETWORK_TYPE_MOBCLIX = 11;
    public static final int NETWORK_TYPE_ONERIOT = 23;
    public static final int NETWORK_TYPE_QUATTRO = 8;
    public static final int NETWORK_TYPE_VIDEOEGG = 3;
    public static final int NETWORK_TYPE_ZESTADZ = 20;
    public static final int VERSION = 300;
    private static double density = -1.0d;
    public static final String locationString = "&location=%f,%f&location_timestamp=%d";
    public static final String urlClick = "http://met.adwhirl.com/exclick.php?appid=%s&nid=%s&type=%d&uuid=%s&country_code=%s&appver=%d&client=2";
    public static final String urlConfig = "http://mob.adwhirl.com/getInfo.php?appid=%s&appver=%d&client=2";
    public static final String urlCustom = "http://cus.adwhirl.com/custom.php?appid=%s&nid=%s&uuid=%s&country_code=%s%s&appver=%d&client=2";
    public static final String urlImpression = "http://met.adwhirl.com/exmet.php?appid=%s&nid=%s&type=%d&uuid=%s&country_code=%s&appver=%d&client=2";

    public static String convertToHex(byte[] data2) {
        StringBuffer buf = new StringBuffer();
        for (byte element : data2) {
            int halfbyte = (element >>> 4) & 15;
            int two_halfs = 0;
            while (true) {
                if (halfbyte < 0 || halfbyte > 9) {
                    buf.append((char) ((halfbyte - 10) + 97));
                } else {
                    buf.append((char) (halfbyte + 48));
                }
                halfbyte = element & 15;
                int two_halfs2 = two_halfs + 1;
                if (two_halfs >= 1) {
                    break;
                }
                two_halfs = two_halfs2;
            }
        }
        return buf.toString();
    }

    public static double getDensity(Activity activity) {
        if (density == -1.0d) {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            density = (double) displayMetrics.density;
        }
        return density;
    }

    public static int convertToScreenPixels(int dipPixels, double density2) {
        return (int) convertToScreenPixels((double) dipPixels, density2);
    }

    public static double convertToScreenPixels(double dipPixels, double density2) {
        return density2 > 0.0d ? dipPixels * density2 : dipPixels;
    }
}
