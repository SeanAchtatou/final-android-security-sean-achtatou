package com.simboly.puzzlebox;

import ad.AdContainer;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import data.Puzzle;
import dialog.DialogManager;
import eula.Eula;
import game.IGame;
import helper.Constants;
import helper.CrashHandler;
import helper.L;
import helper.PuzzleConfig;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import main.PuzzleTypeConfig;
import res.Res;
import screen.ScreenBase;
import screen.ScreenGameMode;
import screen.ScreenMorePuzzleList;
import screen.ScreenMyPuzzle;
import screen.ScreenMyPuzzleImageGridSize;
import screen.ScreenMyPuzzleList;
import screen.ScreenPlayMyPuzzle;
import screen.ScreenPlayPuzzleCasual;
import screen.ScreenPlayPuzzleProgress;
import screen.ScreenPuzzle;
import screen.ScreenPuzzleImage;
import screen.ScreenPuzzleNotRegistered;
import screen.ScreenSolvedAll;

public class ActivityGame extends Activity implements IGame {
    private boolean m_bQuitRequest = false;
    private AdContainer m_hAdContainer;
    private DialogManager m_hDialogManager;
    /* access modifiers changed from: private */
    public final Handler m_hHandler = new Handler();
    private KillReceiver m_hKillReceiver;
    private Puzzle m_hPuzzle;
    /* access modifiers changed from: private */
    public ScreenContainer m_hScreenContainer;
    /* access modifiers changed from: private */
    public ScreenBase m_hScreenCurrent;
    private View m_vgLockScreen;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.simboly.puzzlebox.ActivityGame.switchScreen(int, boolean):void
     arg types: [int, int]
     candidates:
      com.simboly.puzzlebox.ActivityGame.switchScreen(int, java.lang.Object):void
      game.IGame.switchScreen(int, java.lang.Object):void
      com.simboly.puzzlebox.ActivityGame.switchScreen(int, boolean):void */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Constants.init(this);
        if (Constants.debug()) {
            L.v(getClass().getSimpleName(), "onCreate");
        }
        new CrashHandler(this);
        this.m_hKillReceiver = new KillReceiver(this);
        if (!validPuzzleType()) {
            throw new RuntimeException("Invalid puzzle type");
        }
        getWindow().setFlags(128, 128);
        Eula.showEula(this);
        this.m_hKillReceiver.killOtherPuzzles(this);
        registerReceiver(this.m_hKillReceiver, new IntentFilter(KillReceiver.KILL_OTHER_PUZZLES_ACTION));
        this.m_hPuzzle = Puzzle.create(this);
        if (this.m_hPuzzle == null) {
            throw new RuntimeException("Invalid puzzle");
        }
        this.m_hPuzzle.loadImageCount(this);
        this.m_hPuzzle.loadImageIndexNextUnsolved(this, this.m_hPuzzle.getImageCount());
        this.m_hPuzzle.loadPuzzleImageData(this);
        this.m_hPuzzle.setInfo(PuzzleConfig.getInfo(this));
        this.m_hPuzzle.setGridSizeMin(Integer.valueOf(PuzzleConfig.getGridSizeMin(this)));
        this.m_hPuzzle.setGridSizeMax(Integer.valueOf(PuzzleConfig.getGridSizeMax(this)));
        this.m_hPuzzle.setAdWhirlKey(PuzzleConfig.getAdWhirlKey(this));
        this.m_hDialogManager = new DialogManager(this, this);
        ActivityRootLayout hActivityRootLayout = new ActivityRootLayout(this, this.m_hPuzzle);
        this.m_hScreenContainer = hActivityRootLayout.getScreenContainer();
        this.m_vgLockScreen = hActivityRootLayout.getLockScreen();
        this.m_hAdContainer = hActivityRootLayout.getAdContainer();
        setContentView(hActivityRootLayout);
        if (!Constants.debug()) {
            switchScreen(2, true);
        } else {
            switchScreen(1, true);
        }
    }

    private boolean validPuzzleType() {
        List<ResolveInfo> arrResolveInfoPuzzle = getPackageManager().queryIntentActivities(new Intent(PuzzleTypeConfig.START_PUZZLE_ACTION), 32);
        String sPackageName = getPackageName();
        for (ResolveInfo hResolveInfoLoop : arrResolveInfoPuzzle) {
            if (hResolveInfoLoop.activityInfo.packageName.equals(sPackageName)) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        if (Constants.debug()) {
            L.v(getClass().getSimpleName(), "onPause");
        }
        if (this.m_hScreenCurrent != null) {
            this.m_hScreenCurrent.onActivityPause();
        }
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        if (Constants.debug()) {
            L.v(getClass().getSimpleName(), "onResume");
        }
        if (this.m_hScreenCurrent != null) {
            this.m_hScreenCurrent.onActivityResume();
        }
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        L.v(getClass().getSimpleName(), "onDestroy");
        super.onDestroy();
        registerReceiver(this.m_hKillReceiver, new IntentFilter());
        unregisterReceiver(this.m_hKillReceiver);
        if (this.m_bQuitRequest) {
            new Handler().post(new Runnable() {
                public void run() {
                    System.exit(1);
                }
            });
        }
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        switch (event.getKeyCode()) {
            case 4:
                if (event.getAction() == 0) {
                    this.m_hDialogManager.showQuit(false);
                }
                return true;
            default:
                if (this.m_hScreenCurrent == null || !this.m_hScreenCurrent.dispatchKeyEvent(event)) {
                    return super.dispatchKeyEvent(event);
                }
                return true;
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int iRequestCode, int iResultCode, Intent hData) {
        super.onActivityResult(iRequestCode, iResultCode, hData);
        ScreenBase hScreen = this.m_hScreenContainer.getScreen(iRequestCode, this, this);
        if (hScreen != null) {
            hScreen.onActivityResult(iRequestCode, iResultCode, hData);
        }
    }

    public Puzzle getPuzzle() {
        return this.m_hPuzzle;
    }

    public Handler getHandler() {
        return this.m_hHandler;
    }

    public DialogManager getDialogManager() {
        return this.m_hDialogManager;
    }

    public void lockScreen() {
        if (Constants.debug()) {
            L.v("lock screen");
        }
        this.m_vgLockScreen.setVisibility(0);
    }

    public void unlockScreen() {
        if (Constants.debug()) {
            L.v("unlock screen");
        }
        this.m_vgLockScreen.setVisibility(8);
    }

    public boolean isScreenLocked() {
        return this.m_vgLockScreen.getVisibility() == 0;
    }

    public void switchScreen(int iScreenId) {
        switchScreen(iScreenId, false, null);
    }

    public void switchScreen(int iScreenId, Object hData) {
        switchScreen(iScreenId, false, hData);
    }

    public void switchScreen(int iScreenId, boolean bResetScreen) {
        switchScreen(iScreenId, bResetScreen, null);
    }

    public void switchScreen(int iScreenId, boolean bResetScreen, Object hData) {
        if (iScreenId == -1) {
            throw new RuntimeException("Invalid screen id: " + (this.m_hScreenCurrent != null ? this.m_hScreenCurrent.getId() : -1));
        }
        if (Constants.debug()) {
            L.v(getClass().getSimpleName(), "switch screen:" + iScreenId);
        }
        lockScreen();
        this.m_hAdContainer.check(this);
        final ScreenBase hScreenOld = this.m_hScreenCurrent;
        final ScreenBase hScreenNew = this.m_hScreenContainer.getScreen(iScreenId, this, this);
        if (hScreenOld != null) {
            hScreenOld.onHide();
        }
        final boolean z = bResetScreen;
        final Object obj = hData;
        this.m_hHandler.post(new Runnable() {
            public void run() {
                int i;
                ActivityGame.this.unlockScreen();
                ScreenBase screenBase = hScreenNew;
                if (hScreenOld != null) {
                    i = hScreenOld.getId();
                } else {
                    i = -1;
                }
                if (screenBase.beforeShow(i, z, obj)) {
                    Handler access$0 = ActivityGame.this.m_hHandler;
                    final ScreenBase screenBase2 = hScreenNew;
                    final ScreenBase screenBase3 = hScreenOld;
                    access$0.post(new Runnable() {
                        public void run() {
                            ActivityGame.this.m_hScreenContainer.setScreensInvisible();
                            screenBase2.setVisibility(0);
                            screenBase2.onShow();
                            ActivityGame.this.m_hScreenCurrent = screenBase2;
                            if (screenBase3 != null) {
                                screenBase3.onAfterHide();
                            }
                        }
                    });
                }
            }
        });
        System.gc();
    }

    public void quit() {
        this.m_bQuitRequest = true;
        this.m_hScreenContainer.onQuitRequest();
        finish();
    }

    private static final class ActivityRootLayout extends FrameLayout {
        private final AdContainer m_hAdContainer;
        private final ScreenContainer m_hScreenContainer;
        private final View m_vgLockScreen;

        public ActivityRootLayout(Context hContext, Puzzle hPuzzle) {
            super(hContext);
            ViewGroup.LayoutParams hLayoutParamsMatchParent = new ViewGroup.LayoutParams(-1, -1);
            setLayoutParams(hLayoutParamsMatchParent);
            ImageView ivBackground = new ImageView(hContext);
            ivBackground.setLayoutParams(hLayoutParamsMatchParent);
            ivBackground.setImageResource(Res.drawable(hContext, "background"));
            ivBackground.setScaleType(ImageView.ScaleType.FIT_XY);
            addView(ivBackground);
            LinearLayout vgContentRoot = new LinearLayout(hContext);
            vgContentRoot.setLayoutParams(hLayoutParamsMatchParent);
            vgContentRoot.setOrientation(1);
            addView(vgContentRoot);
            FrameLayout vgContent = new FrameLayout(hContext);
            vgContent.setLayoutParams(new LinearLayout.LayoutParams(-1, 0, 1.0f));
            vgContentRoot.addView(vgContent);
            this.m_hScreenContainer = new ScreenContainer(hContext);
            this.m_hScreenContainer.setLayoutParams(hLayoutParamsMatchParent);
            vgContent.addView(this.m_hScreenContainer);
            this.m_vgLockScreen = new View(hContext);
            this.m_vgLockScreen.setLayoutParams(hLayoutParamsMatchParent);
            this.m_vgLockScreen.setClickable(true);
            vgContent.addView(this.m_vgLockScreen);
            this.m_hAdContainer = new AdContainer(hContext, hPuzzle);
            vgContentRoot.addView(this.m_hAdContainer);
        }

        public ScreenContainer getScreenContainer() {
            return this.m_hScreenContainer;
        }

        public View getLockScreen() {
            return this.m_vgLockScreen;
        }

        public AdContainer getAdContainer() {
            return this.m_hAdContainer;
        }
    }

    private static final class ScreenContainer extends FrameLayout {
        private final Map<Integer, ScreenBase> m_mapScreen = new HashMap();

        public ScreenContainer(Context hContext) {
            super(hContext);
        }

        public void setScreensInvisible() {
            for (ScreenBase hScreenLoop : this.m_mapScreen.values()) {
                hScreenLoop.setVisibility(4);
            }
        }

        public void onQuitRequest() {
            for (ScreenBase hScreenLoop : this.m_mapScreen.values()) {
                hScreenLoop.onQuitRequest();
            }
        }

        public ScreenBase getScreen(int iScreenId, Activity hActivity, IGame hGame) {
            ScreenBase hScreen = this.m_mapScreen.get(Integer.valueOf(iScreenId));
            if (hScreen != null) {
                return hScreen;
            }
            ScreenBase hScreen2 = createScreen(iScreenId, hActivity, hGame);
            hScreen2.setVisibility(4);
            this.m_mapScreen.put(Integer.valueOf(hScreen2.getId()), hScreen2);
            addView(hScreen2);
            return hScreen2;
        }

        private ScreenBase createScreen(int iScreenId, Activity hActivty, IGame hGame) {
            switch (iScreenId) {
                case 1:
                    return new ScreenGameMode(hActivty, hGame);
                case 2:
                    return new ScreenPuzzle(hActivty, hGame);
                case 3:
                    return new ScreenPuzzleImage(hActivty, hGame);
                case 4:
                    return new ScreenPlayPuzzleProgress(hActivty, hGame);
                case 5:
                    return new ScreenPlayPuzzleCasual(hActivty, hGame);
                case 6:
                    throw new RuntimeException("TODO");
                case 7:
                    return new ScreenPlayMyPuzzle(hActivty, hGame);
                case 8:
                    return new ScreenSolvedAll(hActivty, hGame);
                case 9:
                    return new ScreenMorePuzzleList(hActivty, hGame);
                case 10:
                    return new ScreenMyPuzzleList(hActivty, hGame);
                case 11:
                    return new ScreenMyPuzzle(hActivty, hGame);
                case 12:
                    return new ScreenMyPuzzleImageGridSize(hActivty, hGame);
                case 13:
                    return new ScreenPuzzleNotRegistered(hActivty, hGame);
                default:
                    throw new RuntimeException("Invalid screen id");
            }
        }
    }

    private final class KillReceiver extends BroadcastReceiver {
        public static final String INTENT_KEY_PUZZLE_ID = "INTENT_KEY_PUZZLE_ID";
        public static final String KILL_OTHER_PUZZLES_ACTION = "com.simboly.puzzlesbox.intent.action.KILL_OTHER_PUZZLES";
        private final IGame m_hGame;

        public KillReceiver(IGame hGame) {
            this.m_hGame = hGame;
        }

        public void onReceive(Context context, Intent intent) {
            String sPuzzleId = intent.getStringExtra(INTENT_KEY_PUZZLE_ID);
            if (Constants.debug()) {
                L.v("Kill request from: " + sPuzzleId);
            }
            if (!sPuzzleId.equals(ActivityGame.this.getPackageName())) {
                this.m_hGame.quit();
            }
        }

        public void killOtherPuzzles(Context hContext) {
            Intent hIntent = new Intent(KILL_OTHER_PUZZLES_ACTION);
            hIntent.putExtra(INTENT_KEY_PUZZLE_ID, hContext.getPackageName());
            hContext.sendBroadcast(hIntent);
        }
    }
}
