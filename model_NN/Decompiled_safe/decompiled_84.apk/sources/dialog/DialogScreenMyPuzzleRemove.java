package dialog;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import cfg.Option;
import data.MyPuzzle;
import res.ResDimens;
import res.ResString;
import view.ButtonContainer;
import view.TitleView;

public final class DialogScreenMyPuzzleRemove extends DialogScreenBase {
    private static final int BUTTON_ID_CANCEL = 0;
    private static final int BUTTON_ID_OK = 1;
    private final View.OnClickListener m_hOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            switch (v.getId()) {
                case 0:
                    DialogScreenMyPuzzleRemove.this.m_hDialogManager.dismiss();
                    return;
                case 1:
                    DialogScreenMyPuzzleRemove.this.m_hDialogManager.dismiss();
                    DialogScreenMyPuzzleRemove.this.m_hOnDialogClick.onClick(DialogScreenMyPuzzleRemove.this.m_hDialogManager, -1);
                    return;
                default:
                    throw new RuntimeException("Invalid view id");
            }
        }
    };
    /* access modifiers changed from: private */
    public DialogInterface.OnClickListener m_hOnDialogClick;
    private final ImageView m_ivMyPuzzleIcon;
    private final TextView m_tvMyPuzzleName;

    public DialogScreenMyPuzzleRemove(Activity hActivity, DialogManager hDialogManager) {
        super(hActivity, hDialogManager);
        DisplayMetrics hMetrics = getResources().getDisplayMetrics();
        TitleView hTitle = new TitleView(hActivity, hMetrics);
        hTitle.setTitle(ResString.dialog_screen_my_puzzle_remove_title);
        addView(hTitle);
        LinearLayout vgContentContainer = new LinearLayout(hActivity);
        vgContentContainer.setLayoutParams(new LinearLayout.LayoutParams(-1, 0, 1.0f));
        vgContentContainer.setOrientation(1);
        vgContentContainer.setGravity(17);
        addView(vgContentContainer);
        TextView tvText = createTextView(hActivity);
        int iDip20 = ResDimens.getDip(hMetrics, 20);
        tvText.setPadding(iDip20, iDip20, iDip20, iDip20);
        tvText.setText(ResString.dialog_screen_my_puzzle_remove_text);
        vgContentContainer.addView(tvText);
        LinearLayout vgImageContainer = new LinearLayout(hActivity);
        int iDip72 = ResDimens.getDip(hMetrics, 72);
        vgImageContainer.setLayoutParams(new LinearLayout.LayoutParams(iDip72, iDip72));
        vgImageContainer.setGravity(17);
        vgContentContainer.addView(vgImageContainer);
        this.m_ivMyPuzzleIcon = new ImageView(hActivity);
        int iDipIconSize = ResDimens.getDip(hMetrics, 48);
        this.m_ivMyPuzzleIcon.setLayoutParams(new LinearLayout.LayoutParams(iDipIconSize, iDipIconSize));
        vgImageContainer.addView(this.m_ivMyPuzzleIcon);
        this.m_tvMyPuzzleName = createTextView(hActivity);
        this.m_tvMyPuzzleName.setPadding(iDip20, ResDimens.getDip(hMetrics, 10), iDip20, 0);
        vgContentContainer.addView(this.m_tvMyPuzzleName);
        ButtonContainer vgButtonContainer = new ButtonContainer(hActivity);
        vgButtonContainer.createButton(0, "btn_img_cancel", this.m_hOnClick);
        vgButtonContainer.createButton(1, "btn_img_ok", this.m_hOnClick);
        addView(vgButtonContainer);
    }

    private TextView createTextView(Context hContext) {
        TextView tvText = new TextView(hContext);
        tvText.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        tvText.setGravity(17);
        tvText.setTextColor(-1);
        tvText.setTextSize(1, 20.0f);
        tvText.setShadowLayer(0.5f, 0.0f, 0.0f, Option.HINT_COLOR_DEFAULT);
        tvText.setTypeface(Typeface.DEFAULT, 1);
        return tvText;
    }

    public void onShow(MyPuzzle hMyPuzzle, DialogInterface.OnClickListener hOnDialogClick) {
        this.m_hOnDialogClick = hOnDialogClick;
        this.m_tvMyPuzzleName.setText(hMyPuzzle.getName());
        this.m_ivMyPuzzleIcon.setImageBitmap(hMyPuzzle.getIcon());
    }

    public void cleanUp() {
        super.cleanUp();
        this.m_tvMyPuzzleName.setText((CharSequence) null);
        this.m_ivMyPuzzleIcon.setImageBitmap(null);
        this.m_hOnDialogClick = null;
    }
}
