package dialog;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import cfg.Option;
import data.MyPuzzle;
import data.MyPuzzleList;
import res.ResDimens;
import res.ResString;
import view.ButtonContainer;
import view.TitleView;

public final class DialogScreenMyPuzzleAdd extends DialogScreenBase {
    private static final int BUTTON_ID_CANCEL = 0;
    private static final int BUTTON_ID_OK = 2;
    private static final int BUTTON_ID_PICK_PUZZLE_ICON = 1;
    private static final int MAX_PUZZLENAME_LENGTH = 30;
    private final View m_btnOk;
    /* access modifiers changed from: private */
    public final EditText m_etMyPuzzleName;
    /* access modifiers changed from: private */
    public final Activity m_hActivity;
    /* access modifiers changed from: private */
    public MyPuzzle m_hMyPuzzle;
    /* access modifiers changed from: private */
    public Bitmap m_hMyPuzzleIcon;
    /* access modifiers changed from: private */
    public MyPuzzleList m_hMyPuzzleList;
    private final View.OnClickListener m_hOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            switch (v.getId()) {
                case 0:
                    DialogScreenMyPuzzleAdd.this.m_hDialogManager.dismiss();
                    return;
                case 1:
                    DialogScreenMyPuzzleAdd.this.m_hActivity.startActivityForResult(new Intent("android.intent.action.PICK", MediaStore.Images.Media.INTERNAL_CONTENT_URI), DialogScreenMyPuzzleAdd.this.m_iScreenId);
                    return;
                case 2:
                    String sMyPuzzleName = DialogScreenMyPuzzleAdd.this.m_etMyPuzzleName.getText().toString();
                    if (DialogScreenMyPuzzleAdd.this.m_hMyPuzzleIcon != null && !TextUtils.isEmpty(sMyPuzzleName)) {
                        if (DialogScreenMyPuzzleAdd.this.m_hMyPuzzle == null) {
                            DialogScreenMyPuzzleAdd.this.m_hMyPuzzleList.add(new MyPuzzle(sMyPuzzleName, DialogScreenMyPuzzleAdd.this.m_hMyPuzzleIcon));
                            DialogScreenMyPuzzleAdd.this.m_hOnDialogClick.onClick(DialogScreenMyPuzzleAdd.this.m_hDialogManager, -1);
                            DialogScreenMyPuzzleAdd.this.m_hDialogManager.dismiss();
                            return;
                        }
                        DialogScreenMyPuzzleAdd.this.m_hMyPuzzle.setName(sMyPuzzleName);
                        DialogScreenMyPuzzleAdd.this.m_hMyPuzzle.setIcon(DialogScreenMyPuzzleAdd.this.m_hMyPuzzleIcon);
                        DialogScreenMyPuzzleAdd.this.m_hMyPuzzle.saveToSD();
                        DialogScreenMyPuzzleAdd.this.m_hMyPuzzleList.saveToSD();
                        DialogScreenMyPuzzleAdd.this.m_hOnDialogClick.onClick(DialogScreenMyPuzzleAdd.this.m_hDialogManager, -3);
                        DialogScreenMyPuzzleAdd.this.m_hDialogManager.dismiss();
                        return;
                    }
                    return;
                default:
                    throw new RuntimeException("Invalid view id");
            }
        }
    };
    /* access modifiers changed from: private */
    public DialogInterface.OnClickListener m_hOnDialogClick;
    private TextWatcher m_hTextWatcher = new TextWatcher() {
        public void afterTextChanged(Editable s) {
            DialogScreenMyPuzzleAdd.this.setOkButtonEnable(s);
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }
    };
    private final TitleView m_hTitle;
    /* access modifiers changed from: private */
    public int m_iScreenId;
    private final ImageView m_ivMyPuzzleIcon;

    public DialogScreenMyPuzzleAdd(Activity hActivity, DialogManager hDialogManager) {
        super(hActivity, hDialogManager);
        this.m_hActivity = hActivity;
        DisplayMetrics hMetrics = getResources().getDisplayMetrics();
        this.m_hTitle = new TitleView(hActivity, hMetrics);
        addView(this.m_hTitle);
        this.m_etMyPuzzleName = new EditText(hActivity);
        this.m_etMyPuzzleName.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        this.m_etMyPuzzleName.setSelectAllOnFocus(true);
        this.m_etMyPuzzleName.setSingleLine(true);
        this.m_etMyPuzzleName.setRawInputType(1);
        this.m_etMyPuzzleName.setImeOptions(6);
        this.m_etMyPuzzleName.setFilters(new InputFilter[]{new InputFilter.LengthFilter(MAX_PUZZLENAME_LENGTH)});
        this.m_etMyPuzzleName.addTextChangedListener(this.m_hTextWatcher);
        this.m_etMyPuzzleName.setHint(ResString.dialog_screen_my_puzzle_add_name_hint);
        addView(this.m_etMyPuzzleName);
        LinearLayout vgContentContainer = new LinearLayout(hActivity);
        vgContentContainer.setLayoutParams(new LinearLayout.LayoutParams(-1, 0, 1.0f));
        vgContentContainer.setOrientation(1);
        vgContentContainer.setGravity(17);
        addView(vgContentContainer);
        TextView tvText = new TextView(hActivity);
        tvText.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        tvText.setGravity(17);
        tvText.setTextColor(-1);
        tvText.setTextSize(1, 20.0f);
        tvText.setShadowLayer(0.5f, 0.0f, 0.0f, Option.HINT_COLOR_DEFAULT);
        tvText.setTypeface(Typeface.DEFAULT, 1);
        int iDip20 = ResDimens.getDip(hMetrics, 20);
        tvText.setPadding(iDip20, iDip20, iDip20, iDip20);
        tvText.setText(ResString.dialog_screen_my_puzzle_add_text);
        vgContentContainer.addView(tvText);
        LinearLayout vgImageContainer = new LinearLayout(hActivity);
        int iDip72 = ResDimens.getDip(hMetrics, 72);
        vgImageContainer.setLayoutParams(new LinearLayout.LayoutParams(iDip72, iDip72));
        vgImageContainer.setGravity(17);
        vgContentContainer.addView(vgImageContainer);
        this.m_ivMyPuzzleIcon = new ImageView(hActivity);
        int iDipIconSize = ResDimens.getDip(hMetrics, 48);
        this.m_ivMyPuzzleIcon.setLayoutParams(new LinearLayout.LayoutParams(iDipIconSize, iDipIconSize));
        vgImageContainer.addView(this.m_ivMyPuzzleIcon);
        ButtonContainer vgButtonContainer = new ButtonContainer(hActivity);
        vgButtonContainer.createButton(0, "btn_img_cancel", this.m_hOnClick);
        vgButtonContainer.createButton(1, "btn_img_gallery", this.m_hOnClick);
        this.m_btnOk = vgButtonContainer.createButton(2, "btn_img_ok", this.m_hOnClick);
        addView(vgButtonContainer);
    }

    public void setDialogScreenMyPuzzleAddIcon(Bitmap hMyPuzzleIcon) {
        this.m_hMyPuzzleIcon = hMyPuzzleIcon;
        this.m_ivMyPuzzleIcon.setImageBitmap(hMyPuzzleIcon);
        setOkButtonEnable(this.m_etMyPuzzleName.getText());
    }

    public void onShow(int iScreenId, MyPuzzleList hMyPuzzleList, MyPuzzle hMyPuzzle, DialogInterface.OnClickListener hOnDialogClick) {
        this.m_etMyPuzzleName.clearFocus();
        this.m_iScreenId = iScreenId;
        this.m_hMyPuzzleList = hMyPuzzleList;
        this.m_hOnDialogClick = hOnDialogClick;
        if (hMyPuzzle != null) {
            this.m_hMyPuzzle = hMyPuzzle;
            this.m_hTitle.setTitle(ResString.dialog_screen_my_puzzle_add_title_edit);
            this.m_etMyPuzzleName.setText(this.m_hMyPuzzle.getName());
            this.m_hMyPuzzleIcon = this.m_hMyPuzzle.getIcon();
            this.m_ivMyPuzzleIcon.setImageBitmap(this.m_hMyPuzzleIcon);
            setOkButtonEnable(this.m_etMyPuzzleName.getText());
            return;
        }
        this.m_hMyPuzzle = null;
        this.m_hTitle.setTitle(ResString.dialog_screen_my_puzzle_add_title);
        this.m_etMyPuzzleName.setText((CharSequence) null);
        this.m_ivMyPuzzleIcon.setImageBitmap(null);
        this.m_hMyPuzzleIcon = null;
    }

    /* access modifiers changed from: private */
    public void setOkButtonEnable(Editable sMyPuzzleName) {
        if (this.m_hMyPuzzleIcon == null || TextUtils.isEmpty(sMyPuzzleName)) {
            this.m_btnOk.setEnabled(false);
        } else {
            this.m_btnOk.setEnabled(true);
        }
    }

    public void cleanUp() {
        super.cleanUp();
        this.m_hTitle.setTitle(null);
        this.m_etMyPuzzleName.setText((CharSequence) null);
        this.m_ivMyPuzzleIcon.setImageBitmap(null);
        this.m_hMyPuzzleIcon = null;
        this.m_hMyPuzzleList = null;
        this.m_hMyPuzzle = null;
        this.m_btnOk.setEnabled(true);
    }
}
