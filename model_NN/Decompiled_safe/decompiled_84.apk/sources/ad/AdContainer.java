package ad;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import cfg.Option;
import com.adwhirl.AdWhirlLayout;
import com.adwhirl.AdWhirlTargeting;
import data.Puzzle;
import helper.Constants;
import helper.L;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import main.PuzzleTypeConfig;
import res.ResDimens;

public final class AdContainer extends LinearLayout implements AdWhirlLayout.AdWhirlInterface {
    private static final String[] AD_KEYWORDS_DEFAULT = {"puzzle", "puzzles", "game", "games", "logical", "brain", "solution", "entertainment", "riddle", "pattern", "order", "slide", "drag", "drop", "jigsaw"};
    private static final int HOW_MANY_PERCENT_FOR_PUZZLE = 90;
    private static final int MAX_PERCENT = 100;
    private AdWhirlLayout m_hAdWhirlLayout;
    private final int m_iDipAdHeight = ResDimens.getDip(getResources().getDisplayMetrics(), 52);
    private String m_sAdWhirlKey;

    public AdContainer(Context hContext, Puzzle hPuzzle) {
        super(hContext);
        setBackgroundColor(Option.HINT_COLOR_DEFAULT);
        setGravity(49);
        setLayoutParams(new LinearLayout.LayoutParams(-1, this.m_iDipAdHeight));
        String sAdWhirlKeyPuzzle = hPuzzle.getAdWhirlKey();
        if (doSetAdWhirlKeyPuzzle(sAdWhirlKeyPuzzle)) {
            this.m_sAdWhirlKey = sAdWhirlKeyPuzzle;
            if (Constants.debug()) {
                L.v("Puzzle AdWhirlKey");
            }
        } else {
            this.m_sAdWhirlKey = PuzzleTypeConfig.AD_WHIRL_KEY;
            if (Constants.debug()) {
                L.v("Engine AdWhirlKey");
            }
        }
        String sKeywords = hPuzzle.getName().replace("-", " ").replace("&", " ").trim().toLowerCase();
        while (sKeywords.contains("  ")) {
            sKeywords = sKeywords.replace("  ", " ");
        }
        Set<String> arrKeywords = new HashSet<>();
        arrKeywords.addAll(new HashSet(Arrays.asList(sKeywords.split(" "))));
        arrKeywords.addAll(new HashSet(Arrays.asList(AD_KEYWORDS_DEFAULT)));
        AdWhirlTargeting.setKeywordSet(arrKeywords);
    }

    private boolean doSetAdWhirlKeyPuzzle(String sAdWhirlKeyPuzzle) {
        if (!TextUtils.isEmpty(sAdWhirlKeyPuzzle)) {
            return new Random().nextInt(100) < HOW_MANY_PERCENT_FOR_PUZZLE;
        }
        if (Constants.debug()) {
            L.v("Puzzle AdwhirlKey is null!");
        }
        return false;
    }

    public void check(Activity hActivity) {
        if (this.m_hAdWhirlLayout == null || this.m_hAdWhirlLayout.adWhirlManager == null || this.m_hAdWhirlLayout.activeRation == null) {
            createAdWhirlLayout(hActivity);
        }
    }

    private void createAdWhirlLayout(Activity hActivity) {
        if (TextUtils.isEmpty(this.m_sAdWhirlKey)) {
            if (Constants.debug()) {
                throw new RuntimeException("No puzzle adwhirl key available");
            }
            this.m_sAdWhirlKey = PuzzleTypeConfig.AD_WHIRL_KEY;
        }
        this.m_hAdWhirlLayout = new AdWhirlLayout(hActivity, this.m_sAdWhirlKey);
        this.m_hAdWhirlLayout.setAdWhirlInterface(this);
        try {
            removeAllViews();
            addView(this.m_hAdWhirlLayout, new RelativeLayout.LayoutParams(-1, this.m_iDipAdHeight));
        } catch (Exception e) {
            Exception e2 = e;
            if (Constants.debug()) {
                e2.printStackTrace();
            }
        }
    }

    public void adWhirlGeneric() {
    }
}
