package data;

import android.net.Uri;

public final class PuzzleHelper {
    public static synchronized Uri getImageUri(String sPuzzleId, int iImageIndex) {
        Uri hUri;
        synchronized (PuzzleHelper.class) {
            Uri.Builder hBuilder = new Uri.Builder();
            hBuilder.scheme("android.resource");
            hBuilder.authority(sPuzzleId);
            hBuilder.encodedPath("raw/image_" + iImageIndex);
            hUri = hBuilder.build();
        }
        return hUri;
    }

    public static synchronized Uri getIconUri(String sPuzzleId) {
        Uri hUri;
        synchronized (PuzzleHelper.class) {
            Uri.Builder hBuilder = new Uri.Builder();
            hBuilder.scheme("android.resource");
            hBuilder.authority(sPuzzleId);
            hBuilder.encodedPath("drawable/icon");
            hUri = hBuilder.build();
        }
        return hUri;
    }
}
