package data;

import android.graphics.Bitmap;
import helper.BitmapHelper;
import java.io.Serializable;
import java.util.LinkedList;

public final class MyPuzzleData implements Serializable {
    private static final long serialVersionUID = -1657128707882810207L;
    private byte[] m_arrIcon;
    private final LinkedList<MyPuzzleImage> m_arrMyPuzzleImage = new LinkedList<>();
    private transient Bitmap m_hIconTemp;
    private final String m_sId;
    private String m_sName;

    public MyPuzzleData(String sMyPuzzleId, String sMyPuzzleName, Bitmap hMyPuzzleIcon) {
        this.m_sId = sMyPuzzleId;
        this.m_sName = sMyPuzzleName;
        setIcon(hMyPuzzleIcon);
    }

    public String getId() {
        return this.m_sId;
    }

    public void setIcon(Bitmap hMyPuzzleIcon) {
        this.m_hIconTemp = hMyPuzzleIcon;
        this.m_arrIcon = BitmapHelper.getByteArray(hMyPuzzleIcon);
    }

    public Bitmap getIcon() {
        if (this.m_hIconTemp == null) {
            this.m_hIconTemp = BitmapHelper.loadBitmap(this.m_arrIcon);
        }
        return this.m_hIconTemp;
    }

    public void clearIconTemp() {
        this.m_hIconTemp = null;
    }

    public void setName(String sMyPuzzleName) {
        this.m_sName = sMyPuzzleName;
    }

    public String getName() {
        return this.m_sName;
    }

    public LinkedList<MyPuzzleImage> getMyPuzzleImageList() {
        return this.m_arrMyPuzzleImage;
    }
}
