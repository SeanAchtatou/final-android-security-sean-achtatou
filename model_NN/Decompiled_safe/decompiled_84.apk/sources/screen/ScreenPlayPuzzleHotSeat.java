package screen;

import android.app.Activity;
import game.IGame;
import screen.ScreenPlayBase;

public final class ScreenPlayPuzzleHotSeat extends ScreenPlayBase {
    public ScreenPlayPuzzleHotSeat(int iScreenId, Activity hActivity, IGame hGame) {
        super(6, hActivity, hGame);
    }

    /* access modifiers changed from: protected */
    public String getDisplayLevel() {
        return null;
    }

    /* access modifiers changed from: protected */
    public int calculateNewGridSize() {
        return 0;
    }

    /* access modifiers changed from: protected */
    public ScreenPlayBase.ResultData getResultData() {
        return null;
    }

    /* access modifiers changed from: protected */
    public void dialogResultDismiss(ScreenPlayBase.ResultData hResultData) {
    }

    /* access modifiers changed from: protected */
    public void onBackPress() {
    }
}
