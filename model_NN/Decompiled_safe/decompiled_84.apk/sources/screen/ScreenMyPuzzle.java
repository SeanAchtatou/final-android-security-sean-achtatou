package screen;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import cfg.Option;
import data.MyPuzzle;
import data.MyPuzzleImage;
import dialog.DialogManager;
import game.IGame;
import res.ResDimens;
import view.ButtonContainer;
import view.TitleView;

public final class ScreenMyPuzzle extends ScreenBase {
    private static final int BUTTON_ID_ADD = 1;
    private static final int BUTTON_ID_BACK = 0;
    private static final int BUTTON_ID_EDIT_CANCEL = 2;
    private static final int BUTTON_ID_EDIT_DOWN = 4;
    private static final int BUTTON_ID_EDIT_REMOVE = 5;
    private static final int BUTTON_ID_EDIT_UP = 3;
    /* access modifiers changed from: private */
    public ViewAdapter m_hAdapter;
    private final DialogManager m_hDialogManager;
    /* access modifiers changed from: private */
    public MyPuzzle m_hMyPuzzle;
    /* access modifiers changed from: private */
    public MyPuzzleImage m_hMyPuzzleImageSelected;
    private final AdapterView.OnItemClickListener m_hOnItemClick = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> adapterView, View v, int iPos, long lId) {
            if (iPos >= 0 && ScreenMyPuzzle.this.m_bIsCurrentScreen && !ScreenMyPuzzle.this.m_hGame.isScreenLocked() && ScreenMyPuzzle.this.m_hAdapter.getItem(iPos) != null && ScreenMyPuzzle.this.m_hMyPuzzleImageSelected == null) {
                ScreenMyPuzzle.this.m_hGame.switchScreen(7, true, new Object[]{ScreenMyPuzzle.this.m_hMyPuzzle, Integer.valueOf(iPos)});
            }
        }
    };
    private final AdapterView.OnItemLongClickListener m_hOnItemLongClick = new AdapterView.OnItemLongClickListener() {
        public boolean onItemLongClick(AdapterView<?> adapterView, View hView, int iPos, long lId) {
            if (iPos < 0) {
                return true;
            }
            MyPuzzleImage hMyPuzzleImage = ScreenMyPuzzle.this.m_hAdapter.getItem(iPos);
            if (hMyPuzzleImage == null) {
                return true;
            }
            if (ScreenMyPuzzle.this.m_hMyPuzzleImageSelected != null && ScreenMyPuzzle.this.m_hMyPuzzleImageSelected.equals(hMyPuzzleImage)) {
                return true;
            }
            ScreenMyPuzzle.this.m_hMyPuzzleImageSelected = hMyPuzzleImage;
            ScreenMyPuzzle.this.switchEditMode();
            return true;
        }
    };
    private final TitleView m_hTitle;
    private final ListView m_lvList;
    private final ButtonContainer m_vgButtonContainer;
    private final ButtonContainer m_vgButtonContainerEdit;

    public ScreenMyPuzzle(Activity hActivity, IGame hGame) {
        super(11, hActivity, hGame);
        this.m_hDialogManager = hGame.getDialogManager();
        DisplayMetrics hMetrics = getResources().getDisplayMetrics();
        this.m_hTitle = new TitleView(hActivity, hMetrics);
        addView(this.m_hTitle);
        FrameLayout vgContent = new FrameLayout(hActivity);
        vgContent.setLayoutParams(new LinearLayout.LayoutParams(-1, 0, 1.0f));
        addView(vgContent);
        this.m_lvList = new ListView(hActivity);
        this.m_lvList.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        this.m_lvList.setCacheColorHint(0);
        this.m_lvList.setOnItemClickListener(this.m_hOnItemClick);
        this.m_lvList.setOnItemLongClickListener(this.m_hOnItemLongClick);
        this.m_lvList.setFocusable(false);
        vgContent.addView(this.m_lvList);
        int iDipButtonHeight = ResDimens.getDip(hMetrics, 48);
        FrameLayout vgButtonContainerRoot = new FrameLayout(hActivity);
        vgButtonContainerRoot.setLayoutParams(new LinearLayout.LayoutParams(-1, iDipButtonHeight));
        addView(vgButtonContainerRoot);
        this.m_vgButtonContainer = new ButtonContainer(hActivity, iDipButtonHeight);
        this.m_vgButtonContainer.createButton(0, "btn_img_back", this.m_hOnClick);
        this.m_vgButtonContainer.createButton(1, "btn_img_add_my_puzzle_image", this.m_hOnClick);
        vgButtonContainerRoot.addView(this.m_vgButtonContainer);
        this.m_vgButtonContainerEdit = new ButtonContainer(hActivity, iDipButtonHeight);
        this.m_vgButtonContainerEdit.createButton(2, "btn_img_cancel", this.m_hOnClick);
        this.m_vgButtonContainerEdit.createButton(3, "btn_img_arrow_up", this.m_hOnClick);
        this.m_vgButtonContainerEdit.createButton(4, "btn_img_arrow_down", this.m_hOnClick);
        this.m_vgButtonContainerEdit.createButton(5, "btn_img_delete", this.m_hOnClick);
        vgButtonContainerRoot.addView(this.m_vgButtonContainerEdit);
    }

    private void refresh() {
        this.m_hGame.lockScreen();
        new Thread(new Runnable() {
            public void run() {
                ScreenMyPuzzle.this.m_hMyPuzzle.syncWithSD();
                ScreenMyPuzzle.this.m_hHandler.post(new Runnable() {
                    public void run() {
                        ScreenMyPuzzle.this.m_hAdapter.notifyDataSetChanged();
                        ScreenMyPuzzle.this.m_hGame.unlockScreen();
                    }
                });
            }
        }).start();
    }

    public boolean beforeShow(int iComeFromScreenId, boolean bReset, Object hData) {
        super.beforeShow(iComeFromScreenId, bReset, hData);
        if (hData != null) {
            this.m_hMyPuzzle = (MyPuzzle) hData;
            this.m_hAdapter = new ViewAdapter();
            this.m_lvList.setAdapter((ListAdapter) this.m_hAdapter);
            switchDefaultMode();
        } else if (this.m_hMyPuzzle == null) {
            this.m_hGame.switchScreen(10);
            return true;
        } else if (this.m_hAdapter == null || bReset) {
            this.m_hAdapter = new ViewAdapter();
            this.m_lvList.setAdapter((ListAdapter) this.m_hAdapter);
        } else {
            this.m_hAdapter.notifyDataSetChanged();
        }
        refresh();
        return true;
    }

    public void onShow() {
        super.onShow();
        this.m_hDialogManager.screenSwitchDismiss();
        this.m_lvList.startAnimation(this.m_hAnimIn);
    }

    public void onActivityPause() {
        super.onActivityPause();
        this.m_hDialogManager.dismiss();
        switchDefaultMode();
    }

    public void onActivityResume() {
        super.onActivityResume();
        refresh();
    }

    public void onAfterHide() {
        this.m_hMyPuzzle.clearThumbnail();
        super.onAfterHide();
    }

    /* access modifiers changed from: protected */
    public void onClick(int iViewId) {
        super.onClick(iViewId);
        switch (iViewId) {
            case 0:
                this.m_hGame.switchScreen(10);
                return;
            case 1:
                this.m_hActivity.startActivityForResult(new Intent("android.intent.action.PICK", MediaStore.Images.Media.INTERNAL_CONTENT_URI), 12);
                post(new Runnable() {
                    public void run() {
                        ScreenMyPuzzle.this.m_hGame.switchScreen(12, ScreenMyPuzzle.this.m_hMyPuzzle);
                    }
                });
                return;
            case 2:
                switchDefaultMode();
                this.m_hGame.unlockScreen();
                return;
            case 3:
                int iPosUp = this.m_hMyPuzzle.moveUp(this.m_hMyPuzzleImageSelected);
                if (iPosUp != -1) {
                    this.m_hAdapter.notifyDataSetChanged();
                    this.m_lvList.setSelection(iPosUp - 1);
                }
                this.m_hGame.unlockScreen();
                return;
            case 4:
                int iPosDown = this.m_hMyPuzzle.moveDown(this.m_hMyPuzzleImageSelected);
                if (iPosDown != -1) {
                    this.m_hAdapter.notifyDataSetChanged();
                    this.m_lvList.setSelection(iPosDown - 1);
                }
                this.m_hGame.unlockScreen();
                return;
            case 5:
                this.m_hDialogManager.showMyPuzzleImageRemove(this.m_hMyPuzzleImageSelected, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog2, int which) {
                        if (which == -1) {
                            ScreenMyPuzzle.this.m_hMyPuzzle.remove(ScreenMyPuzzle.this.m_hMyPuzzleImageSelected, true);
                            ScreenMyPuzzle.this.switchDefaultMode();
                        }
                        ScreenMyPuzzle.this.m_hGame.unlockScreen();
                    }
                });
                return;
            default:
                throw new RuntimeException("Invalid view id");
        }
    }

    /* access modifiers changed from: private */
    public void switchDefaultMode() {
        this.m_hMyPuzzleImageSelected = null;
        this.m_hTitle.setTitle(this.m_hMyPuzzle.getName());
        this.m_vgButtonContainer.setVisibility(0);
        this.m_vgButtonContainerEdit.setVisibility(8);
        this.m_hAdapter.notifyDataSetChanged();
    }

    /* access modifiers changed from: private */
    public void switchEditMode() {
        if (this.m_hMyPuzzleImageSelected == null) {
            throw new RuntimeException("data is null");
        }
        this.m_hTitle.setTitle("Edit List");
        this.m_vgButtonContainer.setVisibility(8);
        this.m_vgButtonContainerEdit.setVisibility(0);
        this.m_hAdapter.notifyDataSetChanged();
    }

    private final class ViewAdapter extends BaseAdapter {
        private final int m_iDipThumbnailSize;
        private final String m_sMoves = "Moves: ";
        private final String m_sTime = "Time: ";

        public ViewAdapter() {
            this.m_iDipThumbnailSize = ResDimens.getDip(ScreenMyPuzzle.this.getResources().getDisplayMetrics(), 96);
        }

        public int getCount() {
            if (ScreenMyPuzzle.this.m_hMyPuzzle == null) {
                return 0;
            }
            return ScreenMyPuzzle.this.m_hMyPuzzle.size();
        }

        public MyPuzzleImage getItem(int iPos) {
            return ScreenMyPuzzle.this.m_hMyPuzzle.get(iPos);
        }

        public long getItemId(int iId) {
            return (long) iId;
        }

        public View getView(int iPos, View vConvert, ViewGroup vgParent) {
            ListItem hListItem;
            if (vConvert == null) {
                hListItem = new ListItem(ScreenMyPuzzle.this.m_hActivity);
            } else {
                hListItem = (ListItem) vConvert;
            }
            MyPuzzleImage hMyPuzzleImage = getItem(iPos);
            hListItem.ivThumbnail.setImageBitmap(hMyPuzzleImage.getThumbnail(this.m_iDipThumbnailSize));
            int iGridSize = hMyPuzzleImage.getGridSize();
            hListItem.tvGridSize.setText(String.valueOf(iGridSize) + " x " + iGridSize);
            if (ScreenMyPuzzle.this.m_hMyPuzzleImageSelected == null || !hMyPuzzleImage.equals(ScreenMyPuzzle.this.m_hMyPuzzleImageSelected)) {
                hListItem.tvGridSize.setTextColor(-1);
            } else {
                hListItem.tvGridSize.setTextColor(-65536);
            }
            if (hMyPuzzleImage.getMoves() <= 0) {
                hListItem.tvMoves.setText(String.valueOf(this.m_sMoves) + "---");
                hListItem.tvMovesName.setText((CharSequence) null);
            } else {
                hListItem.tvMoves.setText(String.valueOf(this.m_sMoves) + hMyPuzzleImage.getMoves());
                hListItem.tvMovesName.setText(hMyPuzzleImage.getMovesName());
            }
            if (hMyPuzzleImage.getTimeInMs() <= 0) {
                hListItem.tvTime.setText(String.valueOf(this.m_sTime) + "---");
                hListItem.tvTimeName.setText((CharSequence) null);
            } else {
                hListItem.tvTime.setText(String.valueOf(this.m_sTime) + String.format("%.3f s", Float.valueOf(((float) hMyPuzzleImage.getTimeInMs()) / 1000.0f)));
                hListItem.tvTimeName.setText(hMyPuzzleImage.getTimeInMsName());
            }
            return hListItem;
        }

        private final class ListItem extends LinearLayout {
            public final ImageView ivThumbnail;
            public final TextView tvGridSize;
            public final TextView tvMoves;
            public final TextView tvMovesName;
            public final TextView tvTime;
            public final TextView tvTimeName;

            public ListItem(Context hContext) {
                super(hContext);
                DisplayMetrics hMetrics = getResources().getDisplayMetrics();
                setLayoutParams(new AbsListView.LayoutParams(-1, -1));
                setOrientation(0);
                setGravity(17);
                int iDip10 = ResDimens.getDip(hMetrics, 10);
                setPadding(iDip10, iDip10, iDip10, iDip10);
                LinearLayout linearLayout = new LinearLayout(hContext);
                linearLayout.setLayoutParams(new LinearLayout.LayoutParams(-2, -1));
                linearLayout.setGravity(17);
                addView(linearLayout);
                RelativeLayout relativeLayout = new RelativeLayout(hContext);
                int iDipThumbnailBorderSize = ResDimens.getDip(hMetrics, 100);
                relativeLayout.setLayoutParams(new LinearLayout.LayoutParams(iDipThumbnailBorderSize, iDipThumbnailBorderSize));
                relativeLayout.setBackgroundColor(-1);
                linearLayout.addView(relativeLayout);
                View view2 = new View(hContext);
                int iDipThumbnailSize = ResDimens.getDip(hMetrics, 96);
                RelativeLayout.LayoutParams hThumbnailBackgroundParams = new RelativeLayout.LayoutParams(iDipThumbnailSize, iDipThumbnailSize);
                hThumbnailBackgroundParams.addRule(13);
                view2.setLayoutParams(hThumbnailBackgroundParams);
                view2.setBackgroundColor(Option.HINT_COLOR_DEFAULT);
                relativeLayout.addView(view2);
                this.ivThumbnail = new ImageView(hContext);
                RelativeLayout.LayoutParams hThumbnailParams = new RelativeLayout.LayoutParams(iDipThumbnailSize, iDipThumbnailSize);
                hThumbnailParams.addRule(13);
                this.ivThumbnail.setLayoutParams(hThumbnailParams);
                relativeLayout.addView(this.ivThumbnail);
                LinearLayout linearLayout2 = new LinearLayout(hContext);
                linearLayout2.setLayoutParams(new LinearLayout.LayoutParams(0, -1, 1.0f));
                linearLayout2.setOrientation(1);
                linearLayout2.setGravity(19);
                linearLayout2.setPadding(iDip10, 0, 0, 0);
                addView(linearLayout2);
                this.tvGridSize = getTextView(hContext, 22);
                this.tvGridSize.setPadding(0, 0, 0, iDip10);
                linearLayout2.addView(this.tvGridSize);
                this.tvMoves = getTextView(hContext, 16);
                linearLayout2.addView(this.tvMoves);
                this.tvMovesName = getTextView(hContext, 16);
                linearLayout2.addView(this.tvMovesName);
                this.tvTime = getTextView(hContext, 16);
                linearLayout2.addView(this.tvTime);
                this.tvTimeName = getTextView(hContext, 16);
                linearLayout2.addView(this.tvTimeName);
            }

            private TextView getTextView(Context hContext, int iTextSize) {
                TextView tvText = new TextView(hContext);
                tvText.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
                tvText.setSingleLine();
                tvText.setTextColor(-1);
                tvText.setTextSize(1, (float) iTextSize);
                tvText.setTypeface(Typeface.DEFAULT, 1);
                tvText.setShadowLayer(0.5f, 0.0f, 0.0f, Option.HINT_COLOR_DEFAULT);
                return tvText;
            }
        }
    }
}
