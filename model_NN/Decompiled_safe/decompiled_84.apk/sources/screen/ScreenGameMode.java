package screen;

import android.app.Activity;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.LinearLayout;
import dialog.DialogManager;
import game.IGame;
import res.ResDimens;
import res.ResString;
import view.ButtonContainer;
import view.CustomButton;
import view.TitleView;

public final class ScreenGameMode extends ScreenPuzzleBase {
    private static final int BUTTON_ID_CASUAL = 3;
    private static final int BUTTON_ID_HOT_SEAT = 2;
    private static final int BUTTON_ID_MORE_PUZZLE_LIST = 4;
    private static final int BUTTON_ID_PROGRESS = 1;
    private static final int BUTTON_ID_PUZZLE_INFO = 5;
    private final View m_btnInfo;
    private final DialogManager m_hDialogManager;

    public ScreenGameMode(Activity hActivity, IGame hGame) {
        super(1, hActivity, hGame);
        this.m_hDialogManager = hGame.getDialogManager();
        DisplayMetrics hMetrics = getResources().getDisplayMetrics();
        TitleView titleView = new TitleView(hActivity, hMetrics);
        titleView.setTitle(ResString.screen_game_mode_title);
        addView(titleView);
        LinearLayout linearLayout = new LinearLayout(hActivity);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, 0, 1.0f));
        linearLayout.setOrientation(1);
        linearLayout.setGravity(17);
        int iDip25 = ResDimens.getDip(hMetrics, 25);
        linearLayout.setPadding(iDip25, 0, iDip25, 0);
        addView(linearLayout);
        int iDipButtonHeight = ResDimens.getDip(hMetrics, 48);
        View view2 = new View(hActivity);
        view2.setLayoutParams(new LinearLayout.LayoutParams(0, 0, 2.0f));
        linearLayout.addView(view2);
        CustomButton customButton = new CustomButton(hActivity, 1);
        customButton.setLayoutParams(new LinearLayout.LayoutParams(-1, iDipButtonHeight));
        customButton.setOnClickListener(this.m_hOnClick);
        customButton.setText(ResString.screen_game_mode_progress);
        linearLayout.addView(customButton);
        View view3 = new View(hActivity);
        view3.setLayoutParams(new LinearLayout.LayoutParams(0, 0, 1.0f));
        linearLayout.addView(view3);
        CustomButton customButton2 = new CustomButton(hActivity, 2);
        customButton2.setLayoutParams(new LinearLayout.LayoutParams(-1, iDipButtonHeight));
        customButton2.setOnClickListener(this.m_hOnClick);
        customButton2.setText(ResString.screen_game_mode_hot_seat);
        linearLayout.addView(customButton2);
        View view4 = new View(hActivity);
        view4.setLayoutParams(new LinearLayout.LayoutParams(0, 0, 1.0f));
        linearLayout.addView(view4);
        CustomButton customButton3 = new CustomButton(hActivity, 3);
        customButton3.setLayoutParams(new LinearLayout.LayoutParams(-1, iDipButtonHeight));
        customButton3.setOnClickListener(this.m_hOnClick);
        customButton3.setText("Casual");
        linearLayout.addView(customButton3);
        View view5 = new View(hActivity);
        view5.setLayoutParams(new LinearLayout.LayoutParams(0, 0, 2.0f));
        linearLayout.addView(view5);
        ButtonContainer buttonContainer = new ButtonContainer(hActivity, iDipButtonHeight);
        buttonContainer.createButton(4, "btn_img_back", this.m_hOnClick);
        this.m_btnInfo = buttonContainer.createButton(5, "btn_img_info", this.m_hOnClick);
        addView(buttonContainer);
    }

    public boolean beforeShow(int iSwitchBackScreenId, boolean bReset, Object hData) {
        super.beforeShow(iSwitchBackScreenId, bReset, hData);
        if (TextUtils.isEmpty(this.m_hPuzzle.getInfo())) {
            this.m_btnInfo.setEnabled(false);
        } else {
            this.m_btnInfo.setEnabled(true);
        }
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: game.IGame.switchScreen(int, boolean):void
     arg types: [int, int]
     candidates:
      game.IGame.switchScreen(int, java.lang.Object):void
      game.IGame.switchScreen(int, boolean):void */
    /* access modifiers changed from: protected */
    public void onClick(int iViewId) {
        super.onClick(iViewId);
        switch (iViewId) {
            case 1:
                this.m_hGame.switchScreen(2, true);
                return;
            case 2:
                this.m_hGame.unlockScreen();
                return;
            case 3:
                this.m_hGame.switchScreen(5, true);
                return;
            case 4:
                this.m_hGame.switchScreen(9, true);
                return;
            case 5:
                this.m_hDialogManager.showPuzzleInfo(this.m_hPuzzle.getInfo());
                return;
            default:
                throw new RuntimeException("Invalid view id");
        }
    }
}
