package cfg;

import android.content.Context;
import android.content.SharedPreferences;
import helper.PuzzleConfig;
import java.io.File;
import java.io.Serializable;

public final class Option {
    public static final int GAME_MODE_CASUAL = 1;
    public static final int GAME_MODE_HOT_SEAT = 2;
    public static final int GAME_MODE_MY_PUZZLE = 3;
    public static final int GAME_MODE_PROGRESS = 0;
    public static final int HINT_COLOR_DEFAULT = -16777216;
    public static final int HINT_COLOR_DISABLE = -1;
    private static final String PREF_FILE_OPTION = "pref_file_option";
    private static final String PREF_KEY_OPTION_HINT_COLOR = "PREF_KEY_OPTION_HINT_COLOR";
    private static final String PREF_KEY_OPTION_VIBRATION = "PREF_KEY_OPTION_VIBRATION";
    private static final boolean VIBRATION_DEFAULT = true;
    private static boolean s_bVibration;
    private static OptionOnSD s_hOptionOnSD;
    private static int s_iHintColor;
    private static boolean s_iInit = false;

    public static synchronized void init(Context hContext) {
        synchronized (Option.class) {
            if (!s_iInit) {
                s_hOptionOnSD = OptionOnSD.load();
                if (s_hOptionOnSD != null) {
                    s_iHintColor = s_hOptionOnSD.getHintColor(hContext);
                    s_bVibration = s_hOptionOnSD.getVibration(hContext);
                } else {
                    SharedPreferences hPref = hContext.getSharedPreferences(PREF_FILE_OPTION, 0);
                    s_iHintColor = hPref.getInt(PREF_KEY_OPTION_HINT_COLOR, HINT_COLOR_DEFAULT);
                    s_bVibration = hPref.getBoolean(PREF_KEY_OPTION_VIBRATION, true);
                }
                s_iInit = true;
            }
        }
    }

    public static synchronized void setHintColor(Context hContext, int iHintColor) {
        synchronized (Option.class) {
            s_iHintColor = iHintColor;
            if (!hContext.getSharedPreferences(PREF_FILE_OPTION, 0).edit().putInt(PREF_KEY_OPTION_HINT_COLOR, s_iHintColor).commit()) {
                throw new RuntimeException("Unable to save hint");
            }
            if (s_hOptionOnSD == null) {
                s_hOptionOnSD = OptionOnSD.load();
            }
            if (s_hOptionOnSD != null) {
                s_hOptionOnSD.setHintColor(hContext, iHintColor);
            }
        }
    }

    public static synchronized int getHintColor(Context hContext) {
        int i;
        synchronized (Option.class) {
            init(hContext);
            if (s_hOptionOnSD != null) {
                i = s_hOptionOnSD.getHintColor(hContext);
            } else {
                i = s_iHintColor;
            }
        }
        return i;
    }

    public static synchronized void setVibration(Context hContext, boolean bVibration) {
        synchronized (Option.class) {
            s_bVibration = bVibration;
            if (!hContext.getSharedPreferences(PREF_FILE_OPTION, 0).edit().putBoolean(PREF_KEY_OPTION_VIBRATION, bVibration).commit()) {
                throw new RuntimeException("Unable to save vibration");
            }
            if (s_hOptionOnSD == null) {
                s_hOptionOnSD = OptionOnSD.load();
            }
            if (s_hOptionOnSD != null) {
                s_hOptionOnSD.setVibration(hContext, bVibration);
            }
        }
    }

    public static synchronized boolean getVibration(Context hContext) {
        boolean z;
        synchronized (Option.class) {
            init(hContext);
            if (s_hOptionOnSD != null) {
                z = s_hOptionOnSD.getVibration(hContext);
            } else {
                z = s_bVibration;
            }
        }
        return z;
    }

    private static final class OptionOnSD implements Serializable {
        private static final transient int BUFFER_SIZE = 8192;
        private static final transient String OPTION_FILE_NAME = "option.dat";
        private static final long serialVersionUID = 321895037021576530L;
        private boolean m_bVibration = true;
        private int m_iHintColor = Option.HINT_COLOR_DEFAULT;

        private OptionOnSD() {
        }

        private static synchronized File getRootDirHidden() {
            File hDir;
            synchronized (OptionOnSD.class) {
                hDir = new File(PuzzleConfig.ROOT_DIR_HIDDEN);
                hDir.mkdirs();
            }
            return hDir;
        }

        /* JADX WARNING: Removed duplicated region for block: B:34:0x0062 A[Catch:{ all -> 0x0096 }] */
        /* JADX WARNING: Removed duplicated region for block: B:43:0x0079 A[Catch:{ all -> 0x0096 }] */
        /* JADX WARNING: Removed duplicated region for block: B:52:0x0090 A[Catch:{ all -> 0x0096 }] */
        /* JADX WARNING: Removed duplicated region for block: B:58:0x009b  */
        /* JADX WARNING: Unknown top exception splitter block from list: {B:40:0x0073=Splitter:B:40:0x0073, B:49:0x008a=Splitter:B:49:0x008a, B:31:0x005c=Splitter:B:31:0x005c} */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public static synchronized cfg.Option.OptionOnSD load() {
            /*
                r10 = 0
                java.lang.Class<cfg.Option$OptionOnSD> r7 = cfg.Option.OptionOnSD.class
                monitor-enter(r7)
                boolean r8 = helper.Global.isSDCardAvailable()     // Catch:{ all -> 0x003a }
                if (r8 != 0) goto L_0x000d
                r8 = r10
            L_0x000b:
                monitor-exit(r7)
                return r8
            L_0x000d:
                boolean r8 = helper.Constants.debug()     // Catch:{ all -> 0x003a }
                if (r8 == 0) goto L_0x0018
                java.lang.String r8 = "Load option from disc"
                helper.L.v(r8)     // Catch:{ all -> 0x003a }
            L_0x0018:
                java.io.File r1 = getRootDirHidden()     // Catch:{ all -> 0x003a }
                java.io.File r2 = new java.io.File     // Catch:{ all -> 0x003a }
                java.lang.String r8 = "option.dat"
                r2.<init>(r1, r8)     // Catch:{ all -> 0x003a }
                boolean r8 = r2.exists()     // Catch:{ all -> 0x003a }
                if (r8 != 0) goto L_0x003d
                boolean r8 = helper.Constants.debug()     // Catch:{ all -> 0x003a }
                if (r8 == 0) goto L_0x0034
                java.lang.String r8 = "Option file not exists, create new one"
                helper.L.v(r8)     // Catch:{ all -> 0x003a }
            L_0x0034:
                cfg.Option$OptionOnSD r8 = new cfg.Option$OptionOnSD     // Catch:{ all -> 0x003a }
                r8.<init>()     // Catch:{ all -> 0x003a }
                goto L_0x000b
            L_0x003a:
                r8 = move-exception
                monitor-exit(r7)
                throw r8
            L_0x003d:
                r4 = 0
                java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ ClassCastException -> 0x005a, ClassNotFoundException -> 0x0071, Exception -> 0x0088 }
                r3.<init>(r2)     // Catch:{ ClassCastException -> 0x005a, ClassNotFoundException -> 0x0071, Exception -> 0x0088 }
                java.io.ObjectInputStream r5 = new java.io.ObjectInputStream     // Catch:{ ClassCastException -> 0x005a, ClassNotFoundException -> 0x0071, Exception -> 0x0088 }
                java.io.BufferedInputStream r8 = new java.io.BufferedInputStream     // Catch:{ ClassCastException -> 0x005a, ClassNotFoundException -> 0x0071, Exception -> 0x0088 }
                r9 = 8192(0x2000, float:1.14794E-41)
                r8.<init>(r3, r9)     // Catch:{ ClassCastException -> 0x005a, ClassNotFoundException -> 0x0071, Exception -> 0x0088 }
                r5.<init>(r8)     // Catch:{ ClassCastException -> 0x005a, ClassNotFoundException -> 0x0071, Exception -> 0x0088 }
                java.lang.Object r6 = r5.readObject()     // Catch:{ ClassCastException -> 0x00ac, ClassNotFoundException -> 0x00a8, Exception -> 0x00a4, all -> 0x00a1 }
                cfg.Option$OptionOnSD r6 = (cfg.Option.OptionOnSD) r6     // Catch:{ ClassCastException -> 0x00ac, ClassNotFoundException -> 0x00a8, Exception -> 0x00a4, all -> 0x00a1 }
                helper.Global.closeInputStream(r5)     // Catch:{ all -> 0x003a }
                r8 = r6
                goto L_0x000b
            L_0x005a:
                r8 = move-exception
                r0 = r8
            L_0x005c:
                boolean r8 = helper.Constants.debug()     // Catch:{ all -> 0x0096 }
                if (r8 == 0) goto L_0x0065
                r0.printStackTrace()     // Catch:{ all -> 0x0096 }
            L_0x0065:
                r2.delete()     // Catch:{ all -> 0x0096 }
                cfg.Option$OptionOnSD r8 = new cfg.Option$OptionOnSD     // Catch:{ all -> 0x0096 }
                r8.<init>()     // Catch:{ all -> 0x0096 }
                helper.Global.closeInputStream(r4)     // Catch:{ all -> 0x003a }
                goto L_0x000b
            L_0x0071:
                r8 = move-exception
                r0 = r8
            L_0x0073:
                boolean r8 = helper.Constants.debug()     // Catch:{ all -> 0x0096 }
                if (r8 == 0) goto L_0x007c
                r0.printStackTrace()     // Catch:{ all -> 0x0096 }
            L_0x007c:
                r2.delete()     // Catch:{ all -> 0x0096 }
                cfg.Option$OptionOnSD r8 = new cfg.Option$OptionOnSD     // Catch:{ all -> 0x0096 }
                r8.<init>()     // Catch:{ all -> 0x0096 }
                helper.Global.closeInputStream(r4)     // Catch:{ all -> 0x003a }
                goto L_0x000b
            L_0x0088:
                r8 = move-exception
                r0 = r8
            L_0x008a:
                boolean r8 = helper.Constants.debug()     // Catch:{ all -> 0x0096 }
                if (r8 == 0) goto L_0x009b
                java.lang.RuntimeException r8 = new java.lang.RuntimeException     // Catch:{ all -> 0x0096 }
                r8.<init>(r0)     // Catch:{ all -> 0x0096 }
                throw r8     // Catch:{ all -> 0x0096 }
            L_0x0096:
                r8 = move-exception
            L_0x0097:
                helper.Global.closeInputStream(r4)     // Catch:{ all -> 0x003a }
                throw r8     // Catch:{ all -> 0x003a }
            L_0x009b:
                helper.Global.closeInputStream(r4)     // Catch:{ all -> 0x003a }
                r8 = r10
                goto L_0x000b
            L_0x00a1:
                r8 = move-exception
                r4 = r5
                goto L_0x0097
            L_0x00a4:
                r8 = move-exception
                r0 = r8
                r4 = r5
                goto L_0x008a
            L_0x00a8:
                r8 = move-exception
                r0 = r8
                r4 = r5
                goto L_0x0073
            L_0x00ac:
                r8 = move-exception
                r0 = r8
                r4 = r5
                goto L_0x005c
            */
            throw new UnsupportedOperationException("Method not decompiled: cfg.Option.OptionOnSD.load():cfg.Option$OptionOnSD");
        }

        /* JADX WARNING: Removed duplicated region for block: B:16:0x004c A[Catch:{ all -> 0x0053 }] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private void save() {
            /*
                r7 = this;
                boolean r5 = helper.Global.isSDCardAvailable()
                if (r5 != 0) goto L_0x0007
            L_0x0006:
                return
            L_0x0007:
                boolean r5 = helper.Constants.debug()
                if (r5 == 0) goto L_0x0012
                java.lang.String r5 = "Save option to disc"
                helper.L.v(r5)
            L_0x0012:
                java.io.File r1 = getRootDirHidden()
                java.io.File r2 = new java.io.File
                java.lang.String r5 = "option.dat.tmp"
                r2.<init>(r1, r5)
                r3 = 0
                java.io.File r5 = r2.getParentFile()     // Catch:{ Exception -> 0x0044 }
                r5.mkdirs()     // Catch:{ Exception -> 0x0044 }
                r2.createNewFile()     // Catch:{ Exception -> 0x0044 }
                java.io.ObjectOutputStream r4 = new java.io.ObjectOutputStream     // Catch:{ Exception -> 0x0044 }
                java.io.FileOutputStream r5 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0044 }
                r5.<init>(r2)     // Catch:{ Exception -> 0x0044 }
                r4.<init>(r5)     // Catch:{ Exception -> 0x0044 }
                r4.writeObject(r7)     // Catch:{ Exception -> 0x005b, all -> 0x0058 }
                java.io.File r5 = new java.io.File     // Catch:{ Exception -> 0x005b, all -> 0x0058 }
                java.lang.String r6 = "option.dat"
                r5.<init>(r1, r6)     // Catch:{ Exception -> 0x005b, all -> 0x0058 }
                r2.renameTo(r5)     // Catch:{ Exception -> 0x005b, all -> 0x0058 }
                helper.Global.closeOutputStream(r4)
                r3 = r4
                goto L_0x0006
            L_0x0044:
                r5 = move-exception
                r0 = r5
            L_0x0046:
                boolean r5 = helper.Constants.debug()     // Catch:{ all -> 0x0053 }
                if (r5 == 0) goto L_0x004f
                r0.printStackTrace()     // Catch:{ all -> 0x0053 }
            L_0x004f:
                helper.Global.closeOutputStream(r3)
                goto L_0x0006
            L_0x0053:
                r5 = move-exception
            L_0x0054:
                helper.Global.closeOutputStream(r3)
                throw r5
            L_0x0058:
                r5 = move-exception
                r3 = r4
                goto L_0x0054
            L_0x005b:
                r5 = move-exception
                r0 = r5
                r3 = r4
                goto L_0x0046
            */
            throw new UnsupportedOperationException("Method not decompiled: cfg.Option.OptionOnSD.save():void");
        }

        public synchronized void setHintColor(Context hContext, int iHintColor) {
            this.m_iHintColor = iHintColor;
            save();
        }

        public synchronized int getHintColor(Context hContext) {
            return this.m_iHintColor;
        }

        public synchronized void setVibration(Context hContext, boolean bVibration) {
            this.m_bVibration = bVibration;
            save();
        }

        public synchronized boolean getVibration(Context hContext) {
            return this.m_bVibration;
        }
    }
}
