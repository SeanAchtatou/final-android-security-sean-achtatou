package helper;

import android.os.Build;
import android.util.Log;

public final class L {
    private static final String TAG = "PuzzleBox";

    public static void v(String sTag, String sMsg) {
        if (Constants.debug()) {
            Log.v(TAG, String.valueOf(Build.MODEL) + "|" + sTag + "|" + sMsg);
        }
    }

    public static void v(String sMsg) {
        v("", sMsg);
    }

    public static void e(String sTag, String sMsg, Throwable hThrowable) {
        if (Constants.debug()) {
            Log.e(TAG, String.valueOf(Build.MODEL) + "|" + sTag + "|" + sMsg, hThrowable);
        }
    }

    public static void e(String sMsg, Throwable hThrowable) {
        e("", sMsg, hThrowable);
    }
}
