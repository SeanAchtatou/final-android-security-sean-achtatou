package helper;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import java.io.ByteArrayOutputStream;

public final class BitmapHelper {
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0031 A[Catch:{ all -> 0x005d }] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0044 A[Catch:{ all -> 0x005d }] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0057 A[Catch:{ all -> 0x005d }] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x0065 A[SYNTHETIC, Splitter:B:51:0x0065] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:39:0x0051=Splitter:B:39:0x0051, B:19:0x002b=Splitter:B:19:0x002b, B:29:0x003e=Splitter:B:29:0x003e} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized android.graphics.Bitmap loadBitmapFromByteArrayFile(java.io.File r7) {
        /*
            r6 = 0
            java.lang.Class<helper.BitmapHelper> r4 = helper.BitmapHelper.class
            monitor-enter(r4)
            boolean r5 = r7.exists()     // Catch:{ all -> 0x0062 }
            if (r5 != 0) goto L_0x000d
            r5 = r6
        L_0x000b:
            monitor-exit(r4)
            return r5
        L_0x000d:
            r2 = 0
            r0 = 0
            byte[] r0 = (byte[]) r0     // Catch:{ all -> 0x0062 }
            java.io.ObjectInputStream r3 = new java.io.ObjectInputStream     // Catch:{ ClassCastException -> 0x0029, ClassNotFoundException -> 0x003c, Exception -> 0x004f }
            java.io.FileInputStream r5 = new java.io.FileInputStream     // Catch:{ ClassCastException -> 0x0029, ClassNotFoundException -> 0x003c, Exception -> 0x004f }
            r5.<init>(r7)     // Catch:{ ClassCastException -> 0x0029, ClassNotFoundException -> 0x003c, Exception -> 0x004f }
            r3.<init>(r5)     // Catch:{ ClassCastException -> 0x0029, ClassNotFoundException -> 0x003c, Exception -> 0x004f }
            java.lang.Object r0 = r3.readObject()     // Catch:{ ClassCastException -> 0x0075, ClassNotFoundException -> 0x0071, Exception -> 0x006d, all -> 0x006a }
            byte[] r0 = (byte[]) r0     // Catch:{ ClassCastException -> 0x0075, ClassNotFoundException -> 0x0071, Exception -> 0x006d, all -> 0x006a }
            helper.Global.closeInputStream(r3)     // Catch:{ all -> 0x0062 }
            android.graphics.Bitmap r5 = loadBitmap(r0)     // Catch:{ all -> 0x0062 }
            goto L_0x000b
        L_0x0029:
            r5 = move-exception
            r1 = r5
        L_0x002b:
            boolean r5 = helper.Constants.debug()     // Catch:{ all -> 0x005d }
            if (r5 == 0) goto L_0x0034
            r1.printStackTrace()     // Catch:{ all -> 0x005d }
        L_0x0034:
            r7.delete()     // Catch:{ all -> 0x005d }
            helper.Global.closeInputStream(r2)     // Catch:{ all -> 0x0062 }
            r5 = r6
            goto L_0x000b
        L_0x003c:
            r5 = move-exception
            r1 = r5
        L_0x003e:
            boolean r5 = helper.Constants.debug()     // Catch:{ all -> 0x005d }
            if (r5 == 0) goto L_0x0047
            r1.printStackTrace()     // Catch:{ all -> 0x005d }
        L_0x0047:
            r7.delete()     // Catch:{ all -> 0x005d }
            helper.Global.closeInputStream(r2)     // Catch:{ all -> 0x0062 }
            r5 = r6
            goto L_0x000b
        L_0x004f:
            r5 = move-exception
            r1 = r5
        L_0x0051:
            boolean r5 = helper.Constants.debug()     // Catch:{ all -> 0x005d }
            if (r5 == 0) goto L_0x0065
            java.lang.RuntimeException r5 = new java.lang.RuntimeException     // Catch:{ all -> 0x005d }
            r5.<init>(r1)     // Catch:{ all -> 0x005d }
            throw r5     // Catch:{ all -> 0x005d }
        L_0x005d:
            r5 = move-exception
        L_0x005e:
            helper.Global.closeInputStream(r2)     // Catch:{ all -> 0x0062 }
            throw r5     // Catch:{ all -> 0x0062 }
        L_0x0062:
            r5 = move-exception
            monitor-exit(r4)
            throw r5
        L_0x0065:
            helper.Global.closeInputStream(r2)     // Catch:{ all -> 0x0062 }
            r5 = r6
            goto L_0x000b
        L_0x006a:
            r5 = move-exception
            r2 = r3
            goto L_0x005e
        L_0x006d:
            r5 = move-exception
            r1 = r5
            r2 = r3
            goto L_0x0051
        L_0x0071:
            r5 = move-exception
            r1 = r5
            r2 = r3
            goto L_0x003e
        L_0x0075:
            r5 = move-exception
            r1 = r5
            r2 = r3
            goto L_0x002b
        */
        throw new UnsupportedOperationException("Method not decompiled: helper.BitmapHelper.loadBitmapFromByteArrayFile(java.io.File):android.graphics.Bitmap");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x000e, code lost:
        r2 = android.graphics.BitmapFactory.decodeByteArray(r9, 0, r9.length);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized android.graphics.Bitmap loadBitmap(byte[] r9) {
        /*
            r8 = 3
            java.lang.Class<helper.BitmapHelper> r5 = helper.BitmapHelper.class
            monitor-enter(r5)
            r1 = 0
            r4 = 3
            r3 = 0
        L_0x0007:
            if (r3 <= r8) goto L_0x0012
            r6 = 0
            int r7 = r9.length     // Catch:{ all -> 0x0026 }
            android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeByteArray(r9, r6, r7)     // Catch:{ all -> 0x0026 }
            r2 = r1
        L_0x0010:
            monitor-exit(r5)
            return r2
        L_0x0012:
            r6 = 0
            int r7 = r9.length     // Catch:{ OutOfMemoryError -> 0x001c }
            android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeByteArray(r9, r6, r7)     // Catch:{ OutOfMemoryError -> 0x001c }
            if (r1 == 0) goto L_0x0035
            r2 = r1
            goto L_0x0010
        L_0x001c:
            r6 = move-exception
            r0 = r6
            if (r3 != r8) goto L_0x0029
            java.lang.RuntimeException r6 = new java.lang.RuntimeException     // Catch:{ all -> 0x0026 }
            r6.<init>(r0)     // Catch:{ all -> 0x0026 }
            throw r6     // Catch:{ all -> 0x0026 }
        L_0x0026:
            r6 = move-exception
            monitor-exit(r5)
            throw r6
        L_0x0029:
            boolean r6 = helper.Constants.debug()     // Catch:{ all -> 0x0026 }
            if (r6 == 0) goto L_0x0032
            r0.printStackTrace()     // Catch:{ all -> 0x0026 }
        L_0x0032:
            java.lang.System.gc()     // Catch:{ all -> 0x0026 }
        L_0x0035:
            int r3 = r3 + 1
            goto L_0x0007
        */
        throw new UnsupportedOperationException("Method not decompiled: helper.BitmapHelper.loadBitmap(byte[]):android.graphics.Bitmap");
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x002e A[Catch:{ all -> 0x0034 }] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x003c A[SYNTHETIC, Splitter:B:26:0x003c] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized boolean saveBitmapAsByteArray(java.io.File r5, android.graphics.Bitmap r6) {
        /*
            java.lang.Class<helper.BitmapHelper> r3 = helper.BitmapHelper.class
            monitor-enter(r3)
            r1 = 0
            java.io.File r4 = r5.getParentFile()     // Catch:{ Exception -> 0x0026 }
            r4.mkdirs()     // Catch:{ Exception -> 0x0026 }
            r5.createNewFile()     // Catch:{ Exception -> 0x0026 }
            java.io.ObjectOutputStream r2 = new java.io.ObjectOutputStream     // Catch:{ Exception -> 0x0026 }
            java.io.FileOutputStream r4 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0026 }
            r4.<init>(r5)     // Catch:{ Exception -> 0x0026 }
            r2.<init>(r4)     // Catch:{ Exception -> 0x0026 }
            byte[] r4 = getByteArray(r6)     // Catch:{ Exception -> 0x0044, all -> 0x0041 }
            r2.writeObject(r4)     // Catch:{ Exception -> 0x0044, all -> 0x0041 }
            helper.Global.closeOutputStream(r2)     // Catch:{ all -> 0x0048 }
            r4 = 1
            r1 = r2
        L_0x0024:
            monitor-exit(r3)
            return r4
        L_0x0026:
            r4 = move-exception
            r0 = r4
        L_0x0028:
            boolean r4 = helper.Constants.debug()     // Catch:{ all -> 0x0034 }
            if (r4 == 0) goto L_0x003c
            java.lang.RuntimeException r4 = new java.lang.RuntimeException     // Catch:{ all -> 0x0034 }
            r4.<init>(r0)     // Catch:{ all -> 0x0034 }
            throw r4     // Catch:{ all -> 0x0034 }
        L_0x0034:
            r4 = move-exception
        L_0x0035:
            helper.Global.closeOutputStream(r1)     // Catch:{ all -> 0x0039 }
            throw r4     // Catch:{ all -> 0x0039 }
        L_0x0039:
            r4 = move-exception
        L_0x003a:
            monitor-exit(r3)
            throw r4
        L_0x003c:
            helper.Global.closeOutputStream(r1)     // Catch:{ all -> 0x0039 }
            r4 = 0
            goto L_0x0024
        L_0x0041:
            r4 = move-exception
            r1 = r2
            goto L_0x0035
        L_0x0044:
            r4 = move-exception
            r0 = r4
            r1 = r2
            goto L_0x0028
        L_0x0048:
            r4 = move-exception
            r1 = r2
            goto L_0x003a
        */
        throw new UnsupportedOperationException("Method not decompiled: helper.BitmapHelper.saveBitmapAsByteArray(java.io.File, android.graphics.Bitmap):boolean");
    }

    public static synchronized byte[] getByteArray(Bitmap hImage) {
        Exception e;
        synchronized (BitmapHelper.class) {
            ByteArrayOutputStream hByteArrayOut = null;
            try {
                ByteArrayOutputStream hByteArrayOut2 = new ByteArrayOutputStream();
                try {
                    hImage.compress(Bitmap.CompressFormat.JPEG, 100, hByteArrayOut2);
                    byte[] byteArray = hByteArrayOut2.toByteArray();
                    try {
                        Global.closeOutputStream(hByteArrayOut2);
                        return byteArray;
                    } catch (Throwable th) {
                        th = th;
                        throw th;
                    }
                } catch (Exception e2) {
                    e = e2;
                    hByteArrayOut = hByteArrayOut2;
                } catch (Throwable th2) {
                    th = th2;
                    hByteArrayOut = hByteArrayOut2;
                    try {
                        Global.closeOutputStream(hByteArrayOut);
                        throw th;
                    } catch (Throwable th3) {
                        th = th3;
                        throw th;
                    }
                }
            } catch (Exception e3) {
                e = e3;
                try {
                    throw new RuntimeException(e);
                } catch (Throwable th4) {
                    th = th4;
                    Global.closeOutputStream(hByteArrayOut);
                    throw th;
                }
            }
        }
    }

    public static synchronized Bitmap createBitmapFromUri(Context hContext, Uri hUri) {
        Bitmap createBitmapFromUri;
        synchronized (BitmapHelper.class) {
            createBitmapFromUri = createBitmapFromUri(hContext, hUri, null, null);
        }
        return createBitmapFromUri;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001d, code lost:
        if (r10 == null) goto L_0x0034;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001f, code lost:
        r6 = scaleBitmap(r1, r10.intValue());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        helper.Global.closeInputStream(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        helper.Global.closeInputStream(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0037, code lost:
        r6 = r1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized android.graphics.Bitmap createBitmapFromUri(android.content.Context r8, android.net.Uri r9, java.lang.Integer r10, android.graphics.BitmapFactory.Options r11) {
        /*
            r7 = 3
            java.lang.Class<helper.BitmapHelper> r5 = helper.BitmapHelper.class
            monitor-enter(r5)
            r2 = 0
            r1 = 0
            r4 = 3
            r3 = 0
        L_0x0008:
            if (r3 <= r7) goto L_0x000d
            r6 = r1
        L_0x000b:
            monitor-exit(r5)
            return r6
        L_0x000d:
            android.content.ContentResolver r6 = r8.getContentResolver()     // Catch:{ OutOfMemoryError -> 0x0039, FileNotFoundException -> 0x005a, Exception -> 0x0074 }
            java.io.InputStream r2 = r6.openInputStream(r9)     // Catch:{ OutOfMemoryError -> 0x0039, FileNotFoundException -> 0x005a, Exception -> 0x0074 }
            if (r11 != 0) goto L_0x002e
            android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeStream(r2)     // Catch:{ OutOfMemoryError -> 0x0039, FileNotFoundException -> 0x005a, Exception -> 0x0074 }
        L_0x001b:
            if (r1 == 0) goto L_0x007c
            if (r10 == 0) goto L_0x0034
            int r6 = r10.intValue()     // Catch:{ OutOfMemoryError -> 0x0039, FileNotFoundException -> 0x005a, Exception -> 0x0074 }
            android.graphics.Bitmap r6 = scaleBitmap(r1, r6)     // Catch:{ OutOfMemoryError -> 0x0039, FileNotFoundException -> 0x005a, Exception -> 0x0074 }
            helper.Global.closeInputStream(r2)     // Catch:{ all -> 0x002b }
            goto L_0x000b
        L_0x002b:
            r6 = move-exception
            monitor-exit(r5)
            throw r6
        L_0x002e:
            r6 = 0
            android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeStream(r2, r6, r11)     // Catch:{ OutOfMemoryError -> 0x0039, FileNotFoundException -> 0x005a, Exception -> 0x0074 }
            goto L_0x001b
        L_0x0034:
            helper.Global.closeInputStream(r2)     // Catch:{ all -> 0x002b }
            r6 = r1
            goto L_0x000b
        L_0x0039:
            r6 = move-exception
            r0 = r6
            if (r3 != r7) goto L_0x0048
            java.lang.RuntimeException r6 = new java.lang.RuntimeException     // Catch:{ all -> 0x0043 }
            r6.<init>(r0)     // Catch:{ all -> 0x0043 }
            throw r6     // Catch:{ all -> 0x0043 }
        L_0x0043:
            r6 = move-exception
            helper.Global.closeInputStream(r2)     // Catch:{ all -> 0x002b }
            throw r6     // Catch:{ all -> 0x002b }
        L_0x0048:
            boolean r6 = helper.Constants.debug()     // Catch:{ all -> 0x0043 }
            if (r6 == 0) goto L_0x0051
            r0.printStackTrace()     // Catch:{ all -> 0x0043 }
        L_0x0051:
            java.lang.System.gc()     // Catch:{ all -> 0x0043 }
            helper.Global.closeInputStream(r2)     // Catch:{ all -> 0x002b }
        L_0x0057:
            int r3 = r3 + 1
            goto L_0x0008
        L_0x005a:
            r6 = move-exception
            r0 = r6
            if (r3 != r7) goto L_0x0064
            java.lang.RuntimeException r6 = new java.lang.RuntimeException     // Catch:{ all -> 0x0043 }
            r6.<init>(r0)     // Catch:{ all -> 0x0043 }
            throw r6     // Catch:{ all -> 0x0043 }
        L_0x0064:
            boolean r6 = helper.Constants.debug()     // Catch:{ all -> 0x0043 }
            if (r6 == 0) goto L_0x006d
            r0.printStackTrace()     // Catch:{ all -> 0x0043 }
        L_0x006d:
            java.lang.System.gc()     // Catch:{ all -> 0x0043 }
            helper.Global.closeInputStream(r2)     // Catch:{ all -> 0x002b }
            goto L_0x0057
        L_0x0074:
            r6 = move-exception
            r0 = r6
            java.lang.RuntimeException r6 = new java.lang.RuntimeException     // Catch:{ all -> 0x0043 }
            r6.<init>(r0)     // Catch:{ all -> 0x0043 }
            throw r6     // Catch:{ all -> 0x0043 }
        L_0x007c:
            helper.Global.closeInputStream(r2)     // Catch:{ all -> 0x002b }
            goto L_0x0057
        */
        throw new UnsupportedOperationException("Method not decompiled: helper.BitmapHelper.createBitmapFromUri(android.content.Context, android.net.Uri, java.lang.Integer, android.graphics.BitmapFactory$Options):android.graphics.Bitmap");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        r1 = android.graphics.BitmapFactory.decodeFile(r9, r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x003f, code lost:
        if (r1 == null) goto L_0x0065;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0041, code lost:
        r6 = scaleBitmap(r1, r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0065, code lost:
        r3 = r3 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:?, code lost:
        r2 = new android.graphics.BitmapFactory.Options();
        r2.inSampleSize = 2;
        r3 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0013, code lost:
        if (r3 <= 4) goto L_0x003b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0015, code lost:
        r6 = scaleBitmap(android.graphics.BitmapFactory.decodeFile(r9), r10);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized android.graphics.Bitmap createBitmapFromImageFile(java.lang.String r9, int r10) {
        /*
            r8 = 4
            r7 = 2
            java.lang.Class<helper.BitmapHelper> r5 = helper.BitmapHelper.class
            monitor-enter(r5)
            r1 = 0
            r3 = 0
        L_0x0007:
            if (r3 < r7) goto L_0x001f
            android.graphics.BitmapFactory$Options r2 = new android.graphics.BitmapFactory$Options     // Catch:{ all -> 0x0050 }
            r2.<init>()     // Catch:{ all -> 0x0050 }
            r6 = 2
            r2.inSampleSize = r6     // Catch:{ all -> 0x0050 }
            r4 = 4
            r3 = 0
        L_0x0013:
            if (r3 <= r8) goto L_0x003b
            android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeFile(r9)     // Catch:{ all -> 0x0050 }
            android.graphics.Bitmap r6 = scaleBitmap(r1, r10)     // Catch:{ all -> 0x0050 }
        L_0x001d:
            monitor-exit(r5)
            return r6
        L_0x001f:
            android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeFile(r9)     // Catch:{ OutOfMemoryError -> 0x002a }
            if (r1 == 0) goto L_0x0038
            android.graphics.Bitmap r6 = scaleBitmap(r1, r10)     // Catch:{ OutOfMemoryError -> 0x002a }
            goto L_0x001d
        L_0x002a:
            r6 = move-exception
            r0 = r6
            boolean r6 = helper.Constants.debug()     // Catch:{ all -> 0x0050 }
            if (r6 == 0) goto L_0x0035
            r0.printStackTrace()     // Catch:{ all -> 0x0050 }
        L_0x0035:
            java.lang.System.gc()     // Catch:{ all -> 0x0050 }
        L_0x0038:
            int r3 = r3 + 1
            goto L_0x0007
        L_0x003b:
            android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeFile(r9, r2)     // Catch:{ OutOfMemoryError -> 0x0046 }
            if (r1 == 0) goto L_0x0065
            android.graphics.Bitmap r6 = scaleBitmap(r1, r10)     // Catch:{ OutOfMemoryError -> 0x0046 }
            goto L_0x001d
        L_0x0046:
            r6 = move-exception
            r0 = r6
            if (r3 != r8) goto L_0x0053
            java.lang.RuntimeException r6 = new java.lang.RuntimeException     // Catch:{ all -> 0x0050 }
            r6.<init>(r0)     // Catch:{ all -> 0x0050 }
            throw r6     // Catch:{ all -> 0x0050 }
        L_0x0050:
            r6 = move-exception
            monitor-exit(r5)
            throw r6
        L_0x0053:
            boolean r6 = helper.Constants.debug()     // Catch:{ all -> 0x0050 }
            if (r6 == 0) goto L_0x005c
            r0.printStackTrace()     // Catch:{ all -> 0x0050 }
        L_0x005c:
            java.lang.System.gc()     // Catch:{ all -> 0x0050 }
            int r6 = r2.inSampleSize     // Catch:{ all -> 0x0050 }
            int r6 = r6 * 2
            r2.inSampleSize = r6     // Catch:{ all -> 0x0050 }
        L_0x0065:
            int r3 = r3 + 1
            goto L_0x0013
        */
        throw new UnsupportedOperationException("Method not decompiled: helper.BitmapHelper.createBitmapFromImageFile(java.lang.String, int):android.graphics.Bitmap");
    }

    /* JADX INFO: Multiple debug info for r0v11 int: [D('iHeightNew' int), D('dScalePercent' double)] */
    /* JADX INFO: Multiple debug info for r0v17 int: [D('iWidthNew' int), D('dScalePercent' double)] */
    public static synchronized Bitmap scaleBitmap(Bitmap hImage, int iImageSize) {
        synchronized (BitmapHelper.class) {
            int i = 0;
            while (true) {
                if (i > 4) {
                    hImage = null;
                    break;
                }
                try {
                    int iHeight = hImage.getHeight();
                    int iWidth = hImage.getWidth();
                    if (!(iHeight == iImageSize && iWidth == iImageSize)) {
                        if (iHeight > iWidth) {
                            hImage = Bitmap.createScaledBitmap(hImage, (int) ((((double) ((iImageSize * 100) / iHeight)) * ((double) iWidth)) / 100.0d), iImageSize, true);
                        } else if (iHeight < iWidth) {
                            hImage = Bitmap.createScaledBitmap(hImage, iImageSize, (int) ((((double) ((iImageSize * 100) / iWidth)) * ((double) iHeight)) / 100.0d), true);
                        } else {
                            hImage = Bitmap.createScaledBitmap(hImage, iImageSize, iImageSize, true);
                        }
                    }
                    if (hImage != null) {
                        break;
                    }
                    i++;
                } catch (OutOfMemoryError e) {
                    if (i == 4) {
                        throw new RuntimeException(e);
                    }
                    if (Constants.debug()) {
                        e.printStackTrace();
                    }
                    System.gc();
                }
            }
        }
        return hImage;
    }
}
