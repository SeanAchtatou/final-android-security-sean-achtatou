package helper;

import android.content.Context;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public final class RawTextFileReader {
    public static synchronized CharSequence readFile(Context hContext, int iId) {
        Exception e;
        synchronized (RawTextFileReader.class) {
            BufferedReader hReader = null;
            try {
                BufferedReader hReader2 = new BufferedReader(new InputStreamReader(hContext.getResources().openRawResource(iId)));
                try {
                    StringBuilder buffer = new StringBuilder();
                    while (true) {
                        String line = hReader2.readLine();
                        if (line == null) {
                            try {
                                Global.closeReader(hReader2);
                                return buffer;
                            } catch (Throwable th) {
                                th = th;
                                throw th;
                            }
                        } else {
                            buffer.append(line).append("\n");
                        }
                    }
                } catch (Exception e2) {
                    e = e2;
                    hReader = hReader2;
                } catch (Throwable th2) {
                    th = th2;
                    hReader = hReader2;
                    try {
                        Global.closeReader(hReader);
                        throw th;
                    } catch (Throwable th3) {
                        th = th3;
                        throw th;
                    }
                }
            } catch (Exception e3) {
                e = e3;
                try {
                    throw new RuntimeException(e);
                } catch (Throwable th4) {
                    th = th4;
                    Global.closeReader(hReader);
                    throw th;
                }
            }
        }
    }
}
