package game;

import android.os.Handler;
import data.Puzzle;
import dialog.DialogManager;

public interface IGame {
    DialogManager getDialogManager();

    Handler getHandler();

    Puzzle getPuzzle();

    boolean isScreenLocked();

    void lockScreen();

    void quit();

    void switchScreen(int i);

    void switchScreen(int i, Object obj);

    void switchScreen(int i, boolean z);

    void switchScreen(int i, boolean z, Object obj);

    void unlockScreen();
}
