package com.nd.android.pandatheme;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;
import java.io.File;

public class Util {
    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: Method info already added: java.lang.StringBuilder.<init>(java.lang.String):void in method: com.nd.android.pandatheme.Util.checkMkdirs(java.lang.String):void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Method info already added: java.lang.StringBuilder.<init>(java.lang.String):void
        	at jadx.core.dex.info.InfoStorage.putMethod(InfoStorage.java:42)
        	at jadx.core.dex.info.MethodInfo.fromDex(MethodInfo.java:50)
        	at jadx.core.dex.instructions.InsnDecoder.invoke(InsnDecoder.java:678)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:534)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public static void checkMkdirs(java.lang.String r1) {
        /*
            java.io.File r0 = new java.io.File     // Catch:{ Exception -> 0x0019 }
            r0.<init>(r5)     // Catch:{ Exception -> 0x0019 }
            boolean r2 = r0.exists()     // Catch:{ Exception -> 0x0019 }
            if (r2 != 0) goto L_0x000f
            r0.mkdirs()     // Catch:{ Exception -> 0x0019 }
        L_0x000e:
            return
        L_0x000f:
            boolean r2 = r0.isFile()     // Catch:{ Exception -> 0x0019 }
            if (r2 == 0) goto L_0x000e
            r0.mkdirs()     // Catch:{ Exception -> 0x0019 }
            goto L_0x000e
        L_0x0019:
            r2 = move-exception
            r1 = r2
            r1.printStackTrace()
            java.lang.String r2 = ""
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "checkMkdirs Exception:"
            r3.<init>(r4)
            java.lang.String r4 = r1.getMessage()
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            android.util.Log.e(r2, r3)
            goto L_0x000e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.nd.android.pandatheme.Util.checkMkdirs(java.lang.String):void");
    }

    public static boolean installApplication(Context ctx, File mainFile) {
        String setting = Settings.System.getString(ctx.getContentResolver(), "install_non_market_apps");
        if (!setting.equals("1")) {
            Settings.System.putString(ctx.getContentResolver(), "install_non_market_apps", "1");
        }
        try {
            Uri data = Uri.fromFile(mainFile);
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setFlags(268435456);
            intent.setDataAndType(data, "application/vnd.android.package-archive");
            ctx.startActivity(intent);
            if (setting.equals("1")) {
                return true;
            }
            Settings.System.putString(ctx.getContentResolver(), "install_non_market_apps", setting);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            if (setting.equals("1")) {
                return true;
            }
            Settings.System.putString(ctx.getContentResolver(), "install_non_market_apps", setting);
            return true;
        } catch (Throwable th) {
            if (!setting.equals("1")) {
                Settings.System.putString(ctx.getContentResolver(), "install_non_market_apps", setting);
            }
            throw th;
        }
    }

    public static boolean isZh(Context mContext) {
        if (mContext.getResources().getConfiguration().locale.getLanguage().equals("zh")) {
            return true;
        }
        return false;
    }
}
