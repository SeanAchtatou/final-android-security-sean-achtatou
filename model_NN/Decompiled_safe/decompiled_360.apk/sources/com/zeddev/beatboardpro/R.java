package com.zeddev.beatboardpro;

public final class R {

    public static final class attr {
        public static final int adSize = 2130771968;
        public static final int adUnitId = 2130771969;
    }

    public static final class drawable {
        public static final int boombox2 = 2130837504;
        public static final int stopbeat2 = 2130837505;
    }

    public static final class id {
        public static final int BANNER = 2131034112;
        public static final int Button01 = 2131034119;
        public static final int Button02 = 2131034120;
        public static final int Button03 = 2131034121;
        public static final int Button04 = 2131034122;
        public static final int Button05 = 2131034123;
        public static final int Button06 = 2131034124;
        public static final int Button07 = 2131034125;
        public static final int Button08 = 2131034126;
        public static final int Button09 = 2131034127;
        public static final int Button10 = 2131034128;
        public static final int Button11 = 2131034129;
        public static final int Button12 = 2131034130;
        public static final int Button13 = 2131034131;
        public static final int Button14 = 2131034132;
        public static final int Button15 = 2131034133;
        public static final int IAB_BANNER = 2131034114;
        public static final int IAB_LEADERBOARD = 2131034115;
        public static final int IAB_MRECT = 2131034113;
        public static final int ImageButton01 = 2131034134;
        public static final int RelativeLayout01 = 2131034117;
        public static final int TextView01 = 2131034118;
        public static final int adView = 2131034116;
    }

    public static final class layout {
        public static final int main = 2130903040;
    }

    public static final class raw {
        public static final int backbeat1 = 2130968576;
        public static final int beat10 = 2130968577;
        public static final int beat11 = 2130968578;
        public static final int beat12 = 2130968579;
        public static final int beat13 = 2130968580;
        public static final int beat14 = 2130968581;
        public static final int beat15 = 2130968582;
        public static final int beat3 = 2130968583;
        public static final int beat4 = 2130968584;
        public static final int beat5 = 2130968585;
        public static final int beat6 = 2130968586;
        public static final int beat7 = 2130968587;
        public static final int beat8 = 2130968588;
        public static final int beat9 = 2130968589;
        public static final int fasterbeat1 = 2130968590;
    }

    public static final class string {
        public static final int app_name = 2131099649;
        public static final int hello = 2131099648;
    }

    public static final class styleable {
        public static final int[] com_google_ads_AdView = {R.attr.adSize, R.attr.adUnitId};
        public static final int com_google_ads_AdView_adSize = 0;
        public static final int com_google_ads_AdView_adUnitId = 1;
    }
}
