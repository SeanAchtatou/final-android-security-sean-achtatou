package com.zeddev.beatboardpro;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import com.google.ads.AdRequest;
import com.google.ads.AdView;

public class thebeatboard extends Activity {
    Button btn1 = null;
    Button btn10 = null;
    Button btn11 = null;
    Button btn12 = null;
    Button btn13 = null;
    Button btn14 = null;
    Button btn15 = null;
    Button btn2 = null;
    Button btn3 = null;
    Button btn4 = null;
    Button btn5 = null;
    Button btn6 = null;
    Button btn7 = null;
    Button btn8 = null;
    Button btn9 = null;
    MediaPlayer player1 = null;
    ImageButton stopbut = null;

    public void onPause() {
        super.onPause();
        this.player1.release();
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        ((AdView) findViewById(R.id.adView)).loadAd(new AdRequest());
        View.OnClickListener startListener = new View.OnClickListener() {
            public void onClick(View v) {
                if (v.getId() == 2131099665) {
                    thebeatboard.this.player1 = MediaPlayer.create(thebeatboard.this, (int) R.raw.backbeat1);
                }
                if (v.getId() == 2131099651) {
                    thebeatboard.this.player1 = MediaPlayer.create(thebeatboard.this, (int) R.raw.fasterbeat1);
                }
                if (v.getId() == 2131099652) {
                    thebeatboard.this.player1 = MediaPlayer.create(thebeatboard.this, (int) R.raw.beat3);
                }
                if (v.getId() == 2131099653) {
                    thebeatboard.this.player1 = MediaPlayer.create(thebeatboard.this, (int) R.raw.beat4);
                }
                if (v.getId() == 2131099654) {
                    thebeatboard.this.player1 = MediaPlayer.create(thebeatboard.this, (int) R.raw.beat5);
                }
                if (v.getId() == 2131099655) {
                    thebeatboard.this.player1 = MediaPlayer.create(thebeatboard.this, (int) R.raw.beat6);
                }
                if (v.getId() == 2131099656) {
                    thebeatboard.this.player1 = MediaPlayer.create(thebeatboard.this, (int) R.raw.beat7);
                }
                if (v.getId() == 2131099657) {
                    thebeatboard.this.player1 = MediaPlayer.create(thebeatboard.this, (int) R.raw.beat8);
                }
                if (v.getId() == 2131099658) {
                    thebeatboard.this.player1 = MediaPlayer.create(thebeatboard.this, (int) R.raw.beat9);
                }
                if (v.getId() == 2131099659) {
                    thebeatboard.this.player1 = MediaPlayer.create(thebeatboard.this, (int) R.raw.beat10);
                }
                if (v.getId() == 2131099660) {
                    thebeatboard.this.player1 = MediaPlayer.create(thebeatboard.this, (int) R.raw.beat11);
                }
                if (v.getId() == 2131099661) {
                    thebeatboard.this.player1 = MediaPlayer.create(thebeatboard.this, (int) R.raw.beat12);
                }
                if (v.getId() == 2131099662) {
                    thebeatboard.this.player1 = MediaPlayer.create(thebeatboard.this, (int) R.raw.beat13);
                }
                if (v.getId() == 2131099664) {
                    thebeatboard.this.player1 = MediaPlayer.create(thebeatboard.this, (int) R.raw.beat14);
                }
                if (v.getId() == 2131099658) {
                    thebeatboard.this.player1 = MediaPlayer.create(thebeatboard.this, (int) R.raw.beat15);
                }
                if (thebeatboard.this.player1.isPlaying()) {
                    thebeatboard.this.player1.stop();
                }
                thebeatboard.this.btn1.setClickable(false);
                thebeatboard.this.btn2.setClickable(false);
                thebeatboard.this.btn3.setClickable(false);
                thebeatboard.this.btn4.setClickable(false);
                thebeatboard.this.btn5.setClickable(false);
                thebeatboard.this.btn6.setClickable(false);
                thebeatboard.this.btn7.setClickable(false);
                thebeatboard.this.btn8.setClickable(false);
                thebeatboard.this.btn9.setClickable(false);
                thebeatboard.this.btn10.setClickable(false);
                thebeatboard.this.btn11.setClickable(false);
                thebeatboard.this.btn12.setClickable(false);
                thebeatboard.this.btn13.setClickable(false);
                thebeatboard.this.btn14.setClickable(false);
                thebeatboard.this.btn15.setClickable(false);
                thebeatboard.this.player1.setLooping(true);
                thebeatboard.this.player1.setVolume(1.0f, 1.0f);
                thebeatboard.this.player1.setScreenOnWhilePlaying(true);
                thebeatboard.this.player1.start();
            }
        };
        View.OnClickListener stopListener = new View.OnClickListener() {
            public void onClick(View v) {
                if (thebeatboard.this.player1 != null) {
                    thebeatboard.this.player1.stop();
                    thebeatboard.this.player1.reset();
                    thebeatboard.this.btn1.setClickable(true);
                    thebeatboard.this.btn2.setClickable(true);
                    thebeatboard.this.btn3.setClickable(true);
                    thebeatboard.this.btn4.setClickable(true);
                    thebeatboard.this.btn5.setClickable(true);
                    thebeatboard.this.btn6.setClickable(true);
                    thebeatboard.this.btn7.setClickable(true);
                    thebeatboard.this.btn8.setClickable(true);
                    thebeatboard.this.btn9.setClickable(true);
                    thebeatboard.this.btn10.setClickable(true);
                    thebeatboard.this.btn11.setClickable(true);
                    thebeatboard.this.btn12.setClickable(true);
                    thebeatboard.this.btn13.setClickable(true);
                    thebeatboard.this.btn14.setClickable(true);
                    thebeatboard.this.btn15.setClickable(true);
                }
            }
        };
        this.btn1 = (Button) findViewById(R.id.Button01);
        this.btn1.setOnClickListener(startListener);
        this.btn2 = (Button) findViewById(R.id.Button02);
        this.btn2.setOnClickListener(startListener);
        this.btn3 = (Button) findViewById(R.id.Button03);
        this.btn3.setOnClickListener(startListener);
        this.btn4 = (Button) findViewById(R.id.Button04);
        this.btn4.setOnClickListener(startListener);
        this.btn5 = (Button) findViewById(R.id.Button05);
        this.btn5.setOnClickListener(startListener);
        this.btn6 = (Button) findViewById(R.id.Button06);
        this.btn6.setOnClickListener(startListener);
        this.btn7 = (Button) findViewById(R.id.Button07);
        this.btn7.setOnClickListener(startListener);
        this.btn8 = (Button) findViewById(R.id.Button08);
        this.btn8.setOnClickListener(startListener);
        this.btn9 = (Button) findViewById(R.id.Button09);
        this.btn9.setOnClickListener(startListener);
        this.btn10 = (Button) findViewById(R.id.Button10);
        this.btn10.setOnClickListener(startListener);
        this.btn11 = (Button) findViewById(R.id.Button11);
        this.btn11.setOnClickListener(startListener);
        this.btn12 = (Button) findViewById(R.id.Button12);
        this.btn12.setOnClickListener(startListener);
        this.btn13 = (Button) findViewById(R.id.Button13);
        this.btn13.setOnClickListener(startListener);
        this.btn14 = (Button) findViewById(R.id.Button14);
        this.btn14.setOnClickListener(startListener);
        this.btn15 = (Button) findViewById(R.id.Button15);
        this.btn15.setOnClickListener(startListener);
        this.stopbut = (ImageButton) findViewById(R.id.ImageButton01);
        this.stopbut.setOnClickListener(stopListener);
    }
}
