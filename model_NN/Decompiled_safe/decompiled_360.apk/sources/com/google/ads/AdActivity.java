package com.google.ads;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.SystemClock;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.VideoView;

public class AdActivity extends Activity implements MediaPlayer.OnCompletionListener, MediaPlayer.OnPreparedListener, View.OnClickListener {
    public static final String BASE_URL_PARAM = "baseurl";
    public static final String HTML_PARAM = "html";
    public static final String INTENT_ACTION_PARAM = "i";
    public static final String ORIENTATION_PARAM = "o";
    public static final String TYPE_PARAM = "m";
    public static final String URL_PARAM = "u";
    private static final Object a = new Object();
    private static AdActivity b = null;
    private static d c = null;
    private static AdActivity d = null;
    private g e;
    private long f;
    private RelativeLayout g;
    private boolean h;
    private VideoView i;

    private void a(g gVar, boolean z, int i2) {
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        if (gVar.getParent() != null) {
            a("Interstitial created with an AdWebView that has a parent.");
        } else if (gVar.b() != null) {
            a("Interstitial created with an AdWebView that is already in use by another AdActivity.");
        } else {
            setRequestedOrientation(i2);
            gVar.a(this);
            ImageButton imageButton = new ImageButton(getApplicationContext());
            imageButton.setImageResource(17301527);
            imageButton.setBackgroundDrawable(null);
            int applyDimension = (int) TypedValue.applyDimension(1, 1.0f, getResources().getDisplayMetrics());
            imageButton.setPadding(applyDimension, applyDimension, 0, 0);
            imageButton.setOnClickListener(this);
            this.g.addView(gVar, new ViewGroup.LayoutParams(-1, -1));
            this.g.addView(imageButton);
            setContentView(this.g);
            if (z) {
                a.a(gVar);
            }
        }
    }

    private void a(String str) {
        t.b(str);
        finish();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0025, code lost:
        r1 = new android.content.Intent(r0.getApplicationContext(), com.google.ads.AdActivity.class);
        r1.putExtra("com.google.ads.AdOpener", r5.a());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        defpackage.t.a("Launching AdActivity.");
        r0.startActivity(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0042, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0043, code lost:
        defpackage.t.a(r0.getMessage(), r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x000a, code lost:
        r0 = r4.c();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x000e, code lost:
        if (r0 != null) goto L_0x0025;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0010, code lost:
        defpackage.t.e("activity was null while launching an AdActivity.");
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void launchAdActivity(defpackage.d r4, defpackage.e r5) {
        /*
            java.lang.Object r0 = com.google.ads.AdActivity.a
            monitor-enter(r0)
            d r1 = com.google.ads.AdActivity.c     // Catch:{ all -> 0x0021 }
            if (r1 != 0) goto L_0x0016
            com.google.ads.AdActivity.c = r4     // Catch:{ all -> 0x0021 }
        L_0x0009:
            monitor-exit(r0)
            android.app.Activity r0 = r4.c()
            if (r0 != 0) goto L_0x0025
            java.lang.String r0 = "activity was null while launching an AdActivity."
            defpackage.t.e(r0)
        L_0x0015:
            return
        L_0x0016:
            d r1 = com.google.ads.AdActivity.c     // Catch:{ all -> 0x0021 }
            if (r1 == r4) goto L_0x0009
            java.lang.String r1 = "Tried to launch a new AdActivity with a different AdManager."
            defpackage.t.b(r1)     // Catch:{ all -> 0x0021 }
            monitor-exit(r0)     // Catch:{ all -> 0x0021 }
            goto L_0x0015
        L_0x0021:
            r1 = move-exception
            r4 = r1
            monitor-exit(r0)
            throw r4
        L_0x0025:
            android.content.Intent r1 = new android.content.Intent
            android.content.Context r2 = r0.getApplicationContext()
            java.lang.Class<com.google.ads.AdActivity> r3 = com.google.ads.AdActivity.class
            r1.<init>(r2, r3)
            java.lang.String r2 = "com.google.ads.AdOpener"
            android.os.Bundle r3 = r5.a()
            r1.putExtra(r2, r3)
            java.lang.String r2 = "Launching AdActivity."
            defpackage.t.a(r2)     // Catch:{ ActivityNotFoundException -> 0x0042 }
            r0.startActivity(r1)     // Catch:{ ActivityNotFoundException -> 0x0042 }
            goto L_0x0015
        L_0x0042:
            r0 = move-exception
            java.lang.String r1 = r0.getMessage()
            defpackage.t.a(r1, r0)
            goto L_0x0015
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.ads.AdActivity.launchAdActivity(d, e):void");
    }

    public VideoView getVideoView() {
        return this.i;
    }

    public void onClick(View view) {
        finish();
    }

    public void onCompletion(MediaPlayer mediaPlayer) {
        t.d("Video finished playing.");
        this.e.loadUrl("javascript:videoController.showReplayAndSplash_()");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:109:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:110:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x010f, code lost:
        r2.b();
        r10.e.setWebViewClient(r2);
        r1 = r4.get(com.google.ads.AdActivity.URL_PARAM);
        r2 = r4.get(com.google.ads.AdActivity.BASE_URL_PARAM);
        r3 = r4.get(com.google.ads.AdActivity.HTML_PARAM);
        r7 = r4.get(com.google.ads.AdActivity.ORIENTATION_PARAM);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0139, code lost:
        if (r1 == null) goto L_0x015e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x013b, code lost:
        r10.e.loadUrl(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x0140, code lost:
        r1 = 4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x0141, code lost:
        if (r7 == null) goto L_0x014c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x0149, code lost:
        if (r7.equals("p") == false) goto L_0x0171;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x014b, code lost:
        r1 = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x014c, code lost:
        a(r10.e, false, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x015e, code lost:
        if (r3 == null) goto L_0x016a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x0160, code lost:
        r10.e.loadDataWithBaseURL(r2, r3, "text/html", "utf-8", null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x016a, code lost:
        a("Could not get the URL or HTML parameter to show a web app.");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x0177, code lost:
        if (r7.equals("l") == false) goto L_0x014c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x0179, code lost:
        r1 = 0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r11) {
        /*
            r10 = this;
            r9 = 1
            r6 = 0
            r8 = 0
            super.onCreate(r11)
            com.google.ads.AdActivity r1 = com.google.ads.AdActivity.d
            if (r1 != 0) goto L_0x000c
            com.google.ads.AdActivity.d = r10
        L_0x000c:
            r10.g = r6
            r10.h = r8
            r10.i = r6
            android.content.Intent r1 = r10.getIntent()
            java.lang.String r2 = "com.google.ads.AdOpener"
            android.os.Bundle r1 = r1.getBundleExtra(r2)
            if (r1 != 0) goto L_0x0024
            java.lang.String r1 = "Could not get the Bundle used to create AdActivity."
            r10.a(r1)
        L_0x0023:
            return
        L_0x0024:
            e r2 = new e
            r2.<init>(r1)
            java.lang.String r1 = r2.b()
            java.util.HashMap r4 = r2.c()
            com.google.ads.AdActivity r2 = com.google.ads.AdActivity.d
            if (r10 != r2) goto L_0x0042
            java.lang.Object r2 = com.google.ads.AdActivity.a
            monitor-enter(r2)
            d r3 = com.google.ads.AdActivity.c     // Catch:{ all -> 0x0060 }
            if (r3 == 0) goto L_0x005a
            d r3 = com.google.ads.AdActivity.c     // Catch:{ all -> 0x0060 }
            r3.p()     // Catch:{ all -> 0x0060 }
        L_0x0041:
            monitor-exit(r2)     // Catch:{ all -> 0x0060 }
        L_0x0042:
            java.lang.String r2 = "intent"
            boolean r2 = r1.equals(r2)
            if (r2 == 0) goto L_0x00da
            r10.h = r9
            long r1 = android.os.SystemClock.elapsedRealtime()
            r10.f = r1
            if (r4 != 0) goto L_0x0063
            java.lang.String r1 = "Could not get the paramMap in launchIntent()"
            r10.a(r1)
            goto L_0x0023
        L_0x005a:
            java.lang.String r3 = "currentAdManager is null while trying to call onPresentScreen()."
            defpackage.t.e(r3)     // Catch:{ all -> 0x0060 }
            goto L_0x0041
        L_0x0060:
            r1 = move-exception
            monitor-exit(r2)
            throw r1
        L_0x0063:
            java.lang.String r1 = "u"
            java.lang.Object r1 = r4.get(r1)
            java.lang.String r1 = (java.lang.String) r1
            if (r1 != 0) goto L_0x0073
            java.lang.String r1 = "Could not get the URL parameter in launchIntent()."
            r10.a(r1)
            goto L_0x0023
        L_0x0073:
            java.lang.String r2 = "i"
            java.lang.Object r2 = r4.get(r2)
            java.lang.String r2 = (java.lang.String) r2
            java.lang.String r3 = "m"
            java.lang.Object r3 = r4.get(r3)
            java.lang.String r3 = (java.lang.String) r3
            android.net.Uri r1 = android.net.Uri.parse(r1)
            if (r2 != 0) goto L_0x00c0
            android.content.Intent r2 = new android.content.Intent
            java.lang.String r3 = "android.intent.action.VIEW"
            r2.<init>(r3, r1)
            r1 = r2
        L_0x0091:
            java.lang.String r2 = "android.intent.category.BROWSABLE"
            r1.addCategory(r2)
            java.lang.Object r2 = com.google.ads.AdActivity.a
            monitor-enter(r2)
            com.google.ads.AdActivity r3 = com.google.ads.AdActivity.b     // Catch:{ all -> 0x00d7 }
            if (r3 != 0) goto L_0x00a8
            com.google.ads.AdActivity.b = r10     // Catch:{ all -> 0x00d7 }
            d r3 = com.google.ads.AdActivity.c     // Catch:{ all -> 0x00d7 }
            if (r3 == 0) goto L_0x00d1
            d r3 = com.google.ads.AdActivity.c     // Catch:{ all -> 0x00d7 }
            r3.q()     // Catch:{ all -> 0x00d7 }
        L_0x00a8:
            monitor-exit(r2)     // Catch:{ all -> 0x00d7 }
            java.lang.String r2 = "Launching an intent from AdActivity."
            defpackage.t.a(r2)     // Catch:{ ActivityNotFoundException -> 0x00b3 }
            r10.startActivity(r1)     // Catch:{ ActivityNotFoundException -> 0x00b3 }
            goto L_0x0023
        L_0x00b3:
            r1 = move-exception
            java.lang.String r2 = r1.getMessage()
            defpackage.t.a(r2, r1)
            r10.finish()
            goto L_0x0023
        L_0x00c0:
            android.content.Intent r4 = new android.content.Intent
            r4.<init>(r2)
            if (r3 == 0) goto L_0x00cc
            r4.setDataAndType(r1, r3)
            r1 = r4
            goto L_0x0091
        L_0x00cc:
            r4.setData(r1)
            r1 = r4
            goto L_0x0091
        L_0x00d1:
            java.lang.String r3 = "currentAdManager is null while trying to call onLeaveApplication()."
            defpackage.t.e(r3)     // Catch:{ all -> 0x00d7 }
            goto L_0x00a8
        L_0x00d7:
            r1 = move-exception
            monitor-exit(r2)
            throw r1
        L_0x00da:
            android.widget.RelativeLayout r2 = new android.widget.RelativeLayout
            android.content.Context r3 = r10.getApplicationContext()
            r2.<init>(r3)
            r10.g = r2
            java.lang.String r2 = "webapp"
            boolean r2 = r1.equals(r2)
            if (r2 == 0) goto L_0x017b
            g r1 = new g
            android.content.Context r2 = r10.getApplicationContext()
            r1.<init>(r2, r6)
            r10.e = r1
            g r1 = r10.e
            defpackage.u.b(r1)
            java.lang.Object r1 = com.google.ads.AdActivity.a
            monitor-enter(r1)
            d r2 = com.google.ads.AdActivity.c     // Catch:{ all -> 0x015b }
            if (r2 == 0) goto L_0x0153
            h r2 = new h     // Catch:{ all -> 0x015b }
            d r3 = com.google.ads.AdActivity.c     // Catch:{ all -> 0x015b }
            a$a r5 = defpackage.a.C0000a.AD_TYPE     // Catch:{ all -> 0x015b }
            r7 = 0
            r2.<init>(r3, r5, r7)     // Catch:{ all -> 0x015b }
            monitor-exit(r1)
            r2.b()
            g r1 = r10.e
            r1.setWebViewClient(r2)
            java.lang.String r1 = "u"
            java.lang.Object r1 = r4.get(r1)
            java.lang.String r1 = (java.lang.String) r1
            java.lang.String r2 = "baseurl"
            java.lang.Object r2 = r4.get(r2)
            java.lang.String r2 = (java.lang.String) r2
            java.lang.String r3 = "html"
            java.lang.Object r3 = r4.get(r3)
            java.lang.String r3 = (java.lang.String) r3
            java.lang.String r5 = "o"
            java.lang.Object r4 = r4.get(r5)
            r0 = r4
            java.lang.String r0 = (java.lang.String) r0
            r7 = r0
            if (r1 == 0) goto L_0x015e
            g r2 = r10.e
            r2.loadUrl(r1)
        L_0x0140:
            r1 = 4
            if (r7 == 0) goto L_0x014c
            java.lang.String r2 = "p"
            boolean r2 = r7.equals(r2)
            if (r2 == 0) goto L_0x0171
            r1 = r9
        L_0x014c:
            g r2 = r10.e
            r10.a(r2, r8, r1)
            goto L_0x0023
        L_0x0153:
            java.lang.String r2 = "currentAdManager is null while trying to show a webapp."
            r10.a(r2)     // Catch:{ all -> 0x015b }
            monitor-exit(r1)     // Catch:{ all -> 0x015b }
            goto L_0x0023
        L_0x015b:
            r2 = move-exception
            monitor-exit(r1)
            throw r2
        L_0x015e:
            if (r3 == 0) goto L_0x016a
            g r1 = r10.e
            java.lang.String r4 = "text/html"
            java.lang.String r5 = "utf-8"
            r1.loadDataWithBaseURL(r2, r3, r4, r5, r6)
            goto L_0x0140
        L_0x016a:
            java.lang.String r1 = "Could not get the URL or HTML parameter to show a web app."
            r10.a(r1)
            goto L_0x0023
        L_0x0171:
            java.lang.String r2 = "l"
            boolean r2 = r7.equals(r2)
            if (r2 == 0) goto L_0x014c
            r1 = r8
            goto L_0x014c
        L_0x017b:
            java.lang.String r2 = "interstitial"
            boolean r2 = r1.equals(r2)
            if (r2 == 0) goto L_0x01ab
            java.lang.Object r1 = com.google.ads.AdActivity.a
            monitor-enter(r1)
            d r2 = com.google.ads.AdActivity.c     // Catch:{ all -> 0x01a8 }
            if (r2 == 0) goto L_0x01a0
            d r2 = com.google.ads.AdActivity.c     // Catch:{ all -> 0x01a8 }
            g r2 = r2.g()     // Catch:{ all -> 0x01a8 }
            r10.e = r2     // Catch:{ all -> 0x01a8 }
            d r2 = com.google.ads.AdActivity.c     // Catch:{ all -> 0x01a8 }
            int r2 = r2.k()     // Catch:{ all -> 0x01a8 }
            monitor-exit(r1)
            g r1 = r10.e
            r10.a(r1, r9, r2)
            goto L_0x0023
        L_0x01a0:
            java.lang.String r2 = "currentAdManager is null while trying to show an interstitial."
            r10.a(r2)     // Catch:{ all -> 0x01a8 }
            monitor-exit(r1)     // Catch:{ all -> 0x01a8 }
            goto L_0x0023
        L_0x01a8:
            r2 = move-exception
            monitor-exit(r1)
            throw r2
        L_0x01ab:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Unknown AdOpener, <action: "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r1 = r2.append(r1)
            java.lang.String r2 = ">"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r10.a(r1)
            goto L_0x0023
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.ads.AdActivity.onCreate(android.os.Bundle):void");
    }

    public void onDestroy() {
        if (this.g != null) {
            this.g.removeAllViews();
        }
        if (this.e != null) {
            a.b(this.e);
            this.e.a(null);
        }
        if (isFinishing()) {
            if (this.i != null) {
                this.i.stopPlayback();
                this.i = null;
            }
            synchronized (a) {
                if (this == d) {
                    if (c != null) {
                        c.o();
                        c = null;
                    } else {
                        t.e("currentAdManager is null while trying to destroy AdActivity.");
                    }
                    d = null;
                }
                if (this == b) {
                    b = null;
                }
            }
        }
        t.a("AdActivity is closing.");
        super.onDestroy();
    }

    public void onPrepared(MediaPlayer mediaPlayer) {
        t.d("Video is ready to play.");
        this.e.loadUrl("javascript:videoController.hideSplashAndPlayVideo_()");
    }

    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (this.h && hasFocus && SystemClock.elapsedRealtime() - this.f > 250) {
            t.d("Launcher AdActivity got focus and is closing.");
            finish();
        }
    }

    public void showVideo(VideoView videoView) {
        this.i = videoView;
        if (this.e == null) {
            a("Couldn't get adWebView to show the video.");
            return;
        }
        this.e.setBackgroundColor(0);
        videoView.setOnCompletionListener(this);
        videoView.setOnPreparedListener(this);
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-1, -1);
        LinearLayout linearLayout = new LinearLayout(getApplicationContext());
        linearLayout.setGravity(17);
        linearLayout.addView(videoView, layoutParams);
        this.g.addView(linearLayout, 0, layoutParams);
    }
}
