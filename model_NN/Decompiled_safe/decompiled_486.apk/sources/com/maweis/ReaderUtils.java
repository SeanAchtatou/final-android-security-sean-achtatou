package com.maweis;

import java.util.ArrayList;
import java.util.List;

public class ReaderUtils {
    private int letterNumsPerLine = 20;
    private int lines = 20;

    public String[] getScreenText(String str) {
        String tempStr;
        List<String> list = new ArrayList<>();
        str.split("\r\n");
        if (str.length() / this.letterNumsPerLine <= 20) {
            this.lines = str.length() / this.letterNumsPerLine;
        }
        for (int i = 0; i < this.lines; i++) {
            int startToken = i * this.letterNumsPerLine;
            int endToken = startToken + this.letterNumsPerLine;
            if (i == this.lines - 1) {
                tempStr = str.substring(startToken);
            } else {
                tempStr = str.substring(startToken, endToken);
            }
            list.add(tempStr);
        }
        String[] resultArray = new String[list.size()];
        list.toArray(resultArray);
        return resultArray;
    }
}
