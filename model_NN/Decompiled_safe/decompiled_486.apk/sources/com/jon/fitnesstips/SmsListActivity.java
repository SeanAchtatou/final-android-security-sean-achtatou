package com.jon.fitnesstips;

import android.app.Activity;
import android.content.Intent;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SubMenu;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;
import com.admob.android.ads.AdView;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class SmsListActivity extends Activity implements View.OnTouchListener, GestureDetector.OnGestureListener {
    private static final String FILEPATH = "/data/data/com.jon.fitnesstips/files/picsite.xml";
    private static final int FLING_MIN_DISTANCE = 100;
    private static final int FLING_MIN_VELOCITY = 200;
    private static final int ITEM1 = 2;
    private static final int ITEM2 = 3;
    private static final int ITEM3 = 4;
    private static final int ITEM4 = 5;
    private static final int ITEM5 = 6;
    private static final int ITEM6 = 7;
    private static final int ITEM7 = 8;
    private static final int ITEM8 = 9;
    private static final int ITEM9 = 10;
    private static final String SETTING = "history";
    private static final String SETTING_FOUR = "background_color";
    private static final String SETTING_THREE = "font_color";
    private static final String SETTING_TWO = "index";
    public static int backColor = 0;
    public static int fontColor = 0;
    public static int fontSize = 20;
    public static int index;
    public static List<String> smsList = new ArrayList();
    public static boolean startFlag = false;
    public static int which = 0;
    private final int REQUEST_CODE_ONE = 1;
    private final int REQUEST_CODE_TWO = 2;
    AdView adView = null;
    TextView content;
    RelativeLayout layout;
    private int len;
    GestureDetector mGestureDetector;
    private TextView myCustomView = null;
    private ImageView next;
    private ImageView prev;
    private Animation slideLeftIn;
    private Animation slideLeftOut;
    private Animation slideRightIn;
    private Animation slideRightOut;
    TextView step;
    private ViewFlipper viewFlipper;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.list2);
        this.mGestureDetector = new GestureDetector(this);
        if (getDisplayMetrics() > 320) {
            fontSize = getSharedPreferences(SETTING, 0).getInt("SIZE", 35);
        } else {
            fontSize = getSharedPreferences(SETTING, 0).getInt("SIZE", 25);
        }
        this.layout = (RelativeLayout) findViewById(R.id.layout);
        this.layout.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                SmsListActivity.this.startActivity(new Intent(SmsListActivity.this, FileEditActivity.class));
                return true;
            }
        });
        this.viewFlipper = (ViewFlipper) findViewById(R.id.flipper);
        this.slideLeftIn = AnimationUtils.loadAnimation(this, R.anim.slide_left_in);
        this.slideLeftOut = AnimationUtils.loadAnimation(this, R.anim.slide_left_out);
        this.slideRightIn = AnimationUtils.loadAnimation(this, R.anim.slide_right_in);
        this.slideRightOut = AnimationUtils.loadAnimation(this, R.anim.slide_right_out);
        this.slideLeftIn = AnimationUtils.loadAnimation(this, R.anim.slide_left_in);
        this.slideRightIn = AnimationUtils.loadAnimation(this, R.anim.slide_right_in);
        this.myCustomView = (TextView) findViewById(R.id.textContent);
        this.myCustomView.setTextSize((float) fontSize);
        this.myCustomView.setOnTouchListener(this);
        this.myCustomView.setLongClickable(true);
        this.layout.setOnTouchListener(this);
        this.layout.setLongClickable(true);
        backColor = getSharedPreferences(SETTING_FOUR, 0).getInt("BACKCOLOR", -1);
        if (backColor == -1) {
            this.layout.setBackgroundColor(-16777216);
        } else {
            this.layout.setBackgroundColor(backColor);
        }
        this.adView = (AdView) findViewById(R.id.adview1);
        if (backColor == -1) {
            this.adView.setBackgroundColor(-16777216);
        } else {
            this.adView.setBackgroundColor(backColor);
        }
        if (!readPicFile(FILEPATH)) {
            getXML();
        }
        index = getSharedPreferences(SETTING_TWO, 0).getInt("INDEX", 1);
        this.len = smsList.size();
        if (index < this.len && this.len > 1) {
            this.myCustomView.setText(smsList.get(index));
            this.viewFlipper.setInAnimation(this.slideLeftIn);
            this.viewFlipper.setOutAnimation(this.slideLeftOut);
            this.viewFlipper.showNext();
            Toast.makeText(getApplicationContext(), String.valueOf(Integer.toString(index)) + "/" + Integer.toString(this.len - 1), 0).show();
        }
        this.prev = (ImageView) findViewById(R.id.prev);
        this.prev.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                SmsListActivity.this.prevsms();
            }
        });
        this.next = (ImageView) findViewById(R.id.next);
        this.next.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                SmsListActivity.this.nextsms();
            }
        });
    }

    public int getDisplayMetrics() {
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        return dm.widthPixels;
    }

    public void onResume() {
        super.onResume();
        if (startFlag) {
            if (readPicFile(FILEPATH)) {
                this.len = smsList.size();
                if (index < this.len && this.len > 0) {
                    this.myCustomView.setText(smsList.get(index));
                    this.viewFlipper.setInAnimation(this.slideLeftIn);
                    this.viewFlipper.setOutAnimation(this.slideLeftOut);
                    this.viewFlipper.showNext();
                    Toast.makeText(getApplicationContext(), String.valueOf(Integer.toString(index)) + "/" + Integer.toString(this.len - 1), 0).show();
                }
            }
            startFlag = false;
        }
    }

    public void onDestroy() {
        super.onDestroy();
        getSharedPreferences(SETTING, 0).edit().putInt("SIZE", fontSize).commit();
        getSharedPreferences(SETTING_TWO, 0).edit().putInt("INDEX", index).commit();
        getSharedPreferences(SETTING_FOUR, 0).edit().putInt("BACKCOLOR", backColor).commit();
    }

    /* access modifiers changed from: private */
    public void nextsms() {
        index++;
        if (index < this.len) {
            this.myCustomView.setText(smsList.get(index));
            this.viewFlipper.setInAnimation(this.slideLeftIn);
            this.viewFlipper.setOutAnimation(this.slideLeftOut);
            this.viewFlipper.showNext();
            Toast.makeText(getApplicationContext(), String.valueOf(Integer.toString(index)) + "/" + Integer.toString(this.len - 1), 0).show();
            return;
        }
        index--;
    }

    /* access modifiers changed from: private */
    public void prevsms() {
        index--;
        if (index >= 1) {
            this.myCustomView.setText(smsList.get(index));
            this.viewFlipper.setInAnimation(this.slideRightIn);
            this.viewFlipper.setOutAnimation(this.slideRightOut);
            this.viewFlipper.showNext();
            Toast.makeText(getApplicationContext(), String.valueOf(Integer.toString(index)) + "/" + Integer.toString(this.len - 1), 0).show();
            return;
        }
        index++;
    }

    private void setBackground(int index2) {
        switch (index2 % 5) {
            case R.styleable.com_admob_android_ads_AdView_testing:
                this.layout.setBackgroundResource(R.drawable.t01);
                return;
            case R.styleable.com_admob_android_ads_AdView_backgroundColor:
                this.layout.setBackgroundResource(R.drawable.t02);
                return;
            case 2:
                this.layout.setBackgroundResource(R.drawable.t03);
                return;
            case 3:
                this.layout.setBackgroundResource(R.drawable.t04);
                return;
            case 4:
                this.layout.setBackgroundResource(R.drawable.t05);
                return;
            default:
                return;
        }
    }

    private boolean readPicFile(String path) {
        if (!smsList.isEmpty()) {
            smsList.clear();
        }
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(path)));
            while (true) {
                String str = in.readLine();
                if (str == null) {
                    index = 1;
                    return true;
                }
                smsList.add(Uri.decode(str));
            }
        } catch (FileNotFoundException e) {
            return false;
        } catch (Exception e2) {
            return false;
        }
    }

    private void getXML() {
        IOException e;
        AssetManager assetManager = getAssets();
        if (!smsList.isEmpty()) {
            smsList.clear();
        }
        try {
            BufferedReader readBuffer = new BufferedReader(new InputStreamReader(assetManager.open("picsite.xml")));
            while (true) {
                try {
                    String str = readBuffer.readLine();
                    if (str == null) {
                        index = 1;
                        BufferedReader bufferedReader = readBuffer;
                        return;
                    }
                    smsList.add(Uri.decode(str));
                } catch (IOException e2) {
                    e = e2;
                    e.printStackTrace();
                }
            }
        } catch (IOException e3) {
            e = e3;
            e.printStackTrace();
        }
    }

    public boolean onTouch(View v, MotionEvent event) {
        return this.mGestureDetector.onTouchEvent(event);
    }

    public boolean onDown(MotionEvent e) {
        return false;
    }

    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        if ((e1.getX() - e2.getX() > 100.0f && Math.abs(velocityX) > 200.0f) || e2.getX() - e1.getX() <= 100.0f) {
            return false;
        }
        Math.abs(velocityX);
        return false;
    }

    public void onLongPress(MotionEvent e) {
    }

    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }

    public void onShowPress(MotionEvent e) {
    }

    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, 2, 0, "Setting").setIcon((int) R.drawable.settings);
        menu.add(0, 3, 0, "Share").setIcon((int) R.drawable.edit);
        SubMenu sub = menu.addSubMenu("More");
        sub.setIcon((int) R.drawable.other);
        sub.add(0, 4, 0, "Tips");
        sub.add(0, 5, 0, "SMS");
        sub.add(0, (int) ITEM9, 0, "Facts");
        sub.add(0, (int) ITEM5, 0, "Jokes");
        sub.add(0, (int) ITEM6, 0, "Poems");
        sub.add(0, (int) ITEM7, 0, "Quotes");
        sub.add(0, (int) ITEM8, 0, "Apps");
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 2:
                startActivityForResult(new Intent(this, Settings.class), 1);
                break;
            case 3:
                startActivity(new Intent(this, FileEditActivity.class));
                break;
            case 4:
                which = 1;
                startActivity(new Intent(this, TipWebView.class));
                break;
            case 5:
                which = 2;
                startActivity(new Intent(this, TipWebView.class));
                break;
            case ITEM5 /*6*/:
                which = 3;
                startActivity(new Intent(this, TipWebView.class));
                break;
            case ITEM6 /*7*/:
                which = 4;
                startActivity(new Intent(this, TipWebView.class));
                break;
            case ITEM7 /*8*/:
                which = 5;
                startActivity(new Intent(this, TipWebView.class));
                break;
            case ITEM8 /*9*/:
                startActivity(new Intent(this, ApkWebView.class));
                break;
            case ITEM9 /*10*/:
                which = ITEM5;
                startActivity(new Intent(this, TipWebView.class));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            this.myCustomView.setTextSize((float) fontSize);
            if (backColor == -1) {
                this.layout.setBackgroundColor(-16777216);
            } else {
                this.layout.setBackgroundColor(backColor);
            }
            if (backColor == -1) {
                this.adView.setBackgroundColor(-16777216);
            } else {
                this.adView.setBackgroundColor(backColor);
            }
        }
    }
}
