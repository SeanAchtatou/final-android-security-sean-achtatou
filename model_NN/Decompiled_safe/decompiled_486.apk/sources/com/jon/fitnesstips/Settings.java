package com.jon.fitnesstips;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;

public class Settings extends PreferenceActivity {
    /* access modifiers changed from: private */
    public int size;
    private SharedPreferences sp;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings);
        this.sp = PreferenceManager.getDefaultSharedPreferences(this);
        this.sp.registerOnSharedPreferenceChangeListener(new SharedPreferences.OnSharedPreferenceChangeListener() {
            public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
                if (key.equals("size")) {
                    Settings.this.size = Integer.parseInt(sharedPreferences.getString("size", null));
                    if (Settings.this.size == 1) {
                        SmsListActivity.fontSize = 38;
                    } else if (Settings.this.size == 2) {
                        SmsListActivity.fontSize = 32;
                    } else if (Settings.this.size == 3) {
                        SmsListActivity.fontSize = 25;
                    }
                }
            }
        });
    }

    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {
        return false;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        setResult(-1, new Intent());
    }
}
