package com.jon.fitnesstips;

public final class R {

    public static final class anim {
        public static final int slide_left_in = 2130968576;
        public static final int slide_left_out = 2130968577;
        public static final int slide_right_in = 2130968578;
        public static final int slide_right_out = 2130968579;
    }

    public static final class array {
        public static final int entries_list_preference = 2131099648;
        public static final int entriesvalue_list_preference = 2131099649;
    }

    public static final class attr {
        public static final int backgroundColor = 2130771969;
        public static final int isGoneWithoutAd = 2130771973;
        public static final int keywords = 2130771971;
        public static final int refreshInterval = 2130771972;
        public static final int testing = 2130771968;
        public static final int textColor = 2130771970;
    }

    public static final class drawable {
        public static final int AntiqueWhite4 = 2130837527;
        public static final int DarkOrange4 = 2130837531;
        public static final int Firebrick = 2130837526;
        public static final int GhostWhite = 2130837529;
        public static final int LemonChiffon3 = 2130837532;
        public static final int LightSkyBlue1 = 2130837530;
        public static final int black = 2130837523;
        public static final int edit = 2130837504;
        public static final int gray91 = 2130837524;
        public static final int grey31 = 2130837525;
        public static final int home = 2130837505;
        public static final int icon = 2130837506;
        public static final int menu_latest = 2130837507;
        public static final int menu_refresh = 2130837508;
        public static final int menu_top = 2130837509;
        public static final int more = 2130837510;
        public static final int next = 2130837511;
        public static final int other = 2130837512;
        public static final int prev = 2130837513;
        public static final int search = 2130837514;
        public static final int settings = 2130837515;
        public static final int t01 = 2130837516;
        public static final int t02 = 2130837517;
        public static final int t03 = 2130837518;
        public static final int t04 = 2130837519;
        public static final int t05 = 2130837520;
        public static final int user = 2130837521;
        public static final int view = 2130837522;
        public static final int white = 2130837528;
    }

    public static final class id {
        public static final int LinearLayout01 = 2131296268;
        public static final int ScrollView01 = 2131296276;
        public static final int adview1 = 2131296272;
        public static final int adview2 = 2131296264;
        public static final int cancel = 2131296267;
        public static final int check = 2131296280;
        public static final int contact = 2131296282;
        public static final int flipper = 2131296275;
        public static final int icon = 2131296278;
        public static final int kon = 2131296263;
        public static final int konn = 2131296281;
        public static final int layout = 2131296270;
        public static final int list = 2131296287;
        public static final int logo = 2131296286;
        public static final int name = 2131296257;
        public static final int next = 2131296274;
        public static final int no = 2131296284;
        public static final int note = 2131296262;
        public static final int note_edit = 2131296265;
        public static final int ok = 2131296283;
        public static final int phone = 2131296279;
        public static final int pre = 2131296288;
        public static final int prev = 2131296273;
        public static final int search = 2131296256;
        public static final int send = 2131296266;
        public static final int share = 2131296289;
        public static final int step = 2131296271;
        public static final int summary = 2131296269;
        public static final int textContent = 2131296277;
        public static final int title = 2131296259;
        public static final int title_edit = 2131296261;
        public static final int user = 2131296260;
        public static final int webview = 2131296285;
        public static final int webview1 = 2131296258;
    }

    public static final class layout {
        public static final int apkwebview = 2130903040;
        public static final int file_select = 2130903041;
        public static final int list = 2130903042;
        public static final int list2 = 2130903043;
        public static final int list_item_icon_text = 2130903044;
        public static final int main = 2130903045;
        public static final int picwebview = 2130903046;
        public static final int sms_list = 2130903047;
        public static final int smspopup = 2130903048;
        public static final int smswebview = 2130903049;
    }

    public static final class string {
        public static final int SELECT_ALL = 2131165186;
        public static final int UNSELECT_ALL = 2131165187;
        public static final int app_name = 2131165185;
        public static final int hello = 2131165184;
    }

    public static final class style {
        public static final int Theme_FloatActivity = 2131230720;
    }

    public static final class styleable {
        public static final int[] com_admob_android_ads_AdView = {R.attr.testing, R.attr.backgroundColor, R.attr.textColor, R.attr.keywords, R.attr.refreshInterval, R.attr.isGoneWithoutAd};
        public static final int com_admob_android_ads_AdView_backgroundColor = 1;
        public static final int com_admob_android_ads_AdView_isGoneWithoutAd = 5;
        public static final int com_admob_android_ads_AdView_keywords = 3;
        public static final int com_admob_android_ads_AdView_refreshInterval = 4;
        public static final int com_admob_android_ads_AdView_testing = 0;
        public static final int com_admob_android_ads_AdView_textColor = 2;
    }

    public static final class xml {
        public static final int settings = 2131034112;
    }
}
