package com.jon.fitnesstips;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.PaintDrawable;
import android.util.AttributeSet;
import android.view.View;
import com.maweis.ReaderUtils;

public class CustomTextView extends View {
    private int alignLeft = 15;
    private int alignTop = 30;
    private int bgColor = -1;
    private Paint customPainter;
    private int fontColor = -16777216;
    private int fontSize = 30;
    private int lineHeight = 20;
    private PaintDrawable paintDrawable;
    private String text;
    private int textHeight = 20;

    public CustomTextView(Context context) {
        super(context);
        initial();
    }

    public CustomTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initial();
    }

    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initial();
    }

    private void initial() {
        this.paintDrawable = new PaintDrawable(-1);
        this.customPainter = this.paintDrawable.getPaint();
        this.customPainter.setColor(this.fontColor);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        String[] textArray = new ReaderUtils().getScreenText(this.text);
        for (int i = 0; i < textArray.length; i++) {
            canvas.drawText(textArray[i], (float) this.alignLeft, (float) (this.alignTop + (this.textHeight * i)), this.customPainter);
        }
        this.paintDrawable.draw(canvas);
    }

    public void setTextColor(int colorIntValue) {
        this.fontColor = colorIntValue;
    }

    public int getLineHeight() {
        return this.lineHeight;
    }

    public Paint getPaint() {
        return this.customPainter;
    }

    public void setTextSize(int fontSize2) {
        this.fontSize = fontSize2;
    }

    public int getFontSize() {
        return this.fontSize;
    }

    public void setFontSize(int fontSize2) {
        this.fontSize = fontSize2;
    }

    public int getFontColor() {
        return this.fontColor;
    }

    public void setFontColor(int fontColor2) {
        this.fontColor = fontColor2;
    }

    public String getText() {
        return this.text;
    }

    public void setText(String text2) {
        this.text = text2;
    }

    public int getBgColor() {
        return this.bgColor;
    }

    public void setBgColor(int bgColor2) {
        this.bgColor = bgColor2;
    }

    public PaintDrawable getPaintDrawable() {
        return this.paintDrawable;
    }

    public void setPaintDrawable(PaintDrawable paintDrawable2) {
        this.paintDrawable = paintDrawable2;
    }

    public void setLineHeight(int lineHeight2) {
        this.lineHeight = lineHeight2;
    }

    public Paint getCustomPainter() {
        return this.customPainter;
    }

    public void setCustomPainter(Paint customPainter2) {
        this.customPainter = customPainter2;
    }

    public int getAlignTop() {
        return this.alignTop;
    }

    public void setAlignTop(int alignTop2) {
        this.alignTop = alignTop2;
    }

    public int getAlignLeft() {
        return this.alignLeft;
    }

    public void setAlignLeft(int alignLeft2) {
        this.alignLeft = alignLeft2;
    }

    public int getTextHeight() {
        return this.textHeight;
    }

    public void setTextHeight(int textHeight2) {
        this.textHeight = textHeight2;
    }
}
