package com.jon.fitnesstips;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.Contacts;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ContactsList extends Activity {
    private static final int SELECT_ALL_ID = 1;
    private static final int UNSELECT_ALL_ID = 2;
    public static List<Map<String, Object>> data = new ArrayList();
    public static int g = 0;
    /* access modifiers changed from: private */
    public static ViewHolder[] goalholder;
    /* access modifiers changed from: private */
    public static int totalNum;
    Button cancel;
    private ListView listView;
    Button ok;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        this.listView = (ListView) findViewById(R.id.contact);
        prepareData();
        goalholder = new ViewHolder[totalNum];
        this.listView.setAdapter((ListAdapter) new EfficientAdapter(this));
        this.ok = (Button) findViewById(R.id.ok);
        this.ok.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                String list = ContactsList.this.getContact();
                Bundle bundle = new Bundle();
                bundle.putString("CONTACT_LIST", list);
                Intent mIntent = new Intent();
                mIntent.putExtras(bundle);
                ContactsList.this.setResult(-1, mIntent);
                ContactsList.this.finish();
            }
        });
        this.cancel = (Button) findViewById(R.id.no);
        this.cancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                ContactsList.this.setResult(-1, new Intent());
                ContactsList.this.finish();
            }
        });
    }

    public void onPause() {
        super.onPause();
        clearFlag();
    }

    private void clearFlag() {
        g = 0;
        goalholder = null;
        data.clear();
        totalNum = 0;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, 1, 2, (int) R.string.SELECT_ALL);
        menu.add(0, 2, 3, (int) R.string.UNSELECT_ALL);
        return true;
    }

    private void checkAllButton(String str, boolean flag) {
        int ids = totalNum;
        for (int i = 0; i < ids; i++) {
            new HashMap();
            Map<String, Object> item = data.get(i);
            Map<String, Object> item2 = new HashMap<>();
            item2.put("NAME", (String) item.get("NAME"));
            item2.put("NUMBER", (String) item.get("NUMBER"));
            item2.put("CHECK", str);
            data.set(i, item2);
            if (goalholder[i] != null) {
                goalholder[i].check.setChecked(flag);
            }
        }
    }

    private void selectAll() {
        checkAllButton("1", true);
    }

    private void selectNoAll() {
        checkAllButton("0", false);
    }

    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                selectAll();
                break;
            case 2:
                selectNoAll();
                break;
        }
        return super.onMenuItemSelected(featureId, item);
    }

    private static class EfficientAdapter extends BaseAdapter {
        Map<String, Object> cache = new HashMap();
        Map<String, Object> item;
        private LayoutInflater mInflater;

        public EfficientAdapter(Context context) {
            this.mInflater = LayoutInflater.from(context);
        }

        public int getCount() {
            return ContactsList.data.size();
        }

        public Object getItem(int position) {
            return ContactsList.goalholder[position];
        }

        public long getItemId(int position) {
            setting();
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView != null) {
                try {
                    if (position >= ContactsList.totalNum) {
                        ViewHolder viewHolder = (ViewHolder) convertView.getTag();
                        return convertView;
                    }
                } catch (Exception e) {
                    Exception exc = e;
                    return convertView;
                }
            }
            convertView = this.mInflater.inflate((int) R.layout.list_item_icon_text, (ViewGroup) null);
            ViewHolder holder = new ViewHolder(null);
            holder.check = (CheckBox) convertView.findViewById(R.id.check);
            holder.name = (TextView) convertView.findViewById(R.id.name);
            holder.icon = (ImageView) convertView.findViewById(R.id.icon);
            holder.phone = (TextView) convertView.findViewById(R.id.phone);
            convertView.setTag(holder);
            this.cache = ContactsList.data.get(position);
            String d = (String) this.cache.get("NAME");
            String dd = (String) this.cache.get("NUMBER");
            if (((String) this.cache.get("CHECK")).equals("1")) {
                holder.check.setChecked(true);
            } else {
                holder.check.setChecked(false);
            }
            holder.check.setText("");
            holder.name.setText(d);
            holder.phone.setText(dd);
            holder.icon.setImageResource(R.drawable.user);
            int position2 = position + 1;
            try {
                ContactsList.goalholder[position] = holder;
                return convertView;
            } catch (Exception e2) {
                return convertView;
            }
        }

        public void setting() {
            for (int i = 0; i < ContactsList.totalNum; i++) {
                if (ContactsList.goalholder[i] != null) {
                    if (ContactsList.goalholder[i].check.isChecked()) {
                        this.item = new HashMap();
                        this.item.put("NAME", ContactsList.goalholder[i].name.getText().toString());
                        this.item.put("NUMBER", ContactsList.goalholder[i].phone.getText().toString());
                        this.item.put("CHECK", "1");
                        ContactsList.data.set(i, this.item);
                    } else {
                        this.item = new HashMap();
                        this.item.put("NAME", ContactsList.goalholder[i].name.getText().toString());
                        this.item.put("NUMBER", ContactsList.goalholder[i].phone.getText().toString());
                        this.item.put("CHECK", "0");
                        ContactsList.data.set(i, this.item);
                    }
                }
            }
        }
    }

    private static class ViewHolder {
        CheckBox check;
        ImageView icon;
        TextView name;
        TextView phone;

        private ViewHolder() {
        }

        /* synthetic */ ViewHolder(ViewHolder viewHolder) {
            this();
        }
    }

    private void prepareData() {
        Cursor c = getContentResolver().query(Contacts.Phones.CONTENT_URI, null, null, null, "name ASC");
        startManagingCursor(c);
        totalNum = 0;
        while (c.moveToNext()) {
            String _name = c.getString(c.getColumnIndex("display_name"));
            String _phoneNumber = c.getString(c.getColumnIndex("number"));
            Map<String, Object> item = new HashMap<>();
            item.put("NAME", _name);
            item.put("NUMBER", _phoneNumber);
            item.put("CHECK", "0");
            data.add(item);
            totalNum++;
        }
    }

    /* access modifiers changed from: private */
    public String getContact() {
        String list = "";
        for (int i = 0; i < totalNum; i++) {
            if (goalholder[i] != null) {
                if (goalholder[i].check.isChecked()) {
                    Map<String, Object> item = new HashMap<>();
                    item.put("NAME", goalholder[i].name.getText().toString());
                    item.put("NUMBER", goalholder[i].phone.getText().toString());
                    item.put("CHECK", "1");
                    data.set(i, item);
                } else {
                    Map<String, Object> item2 = new HashMap<>();
                    item2.put("NAME", goalholder[i].name.getText().toString());
                    item2.put("NUMBER", goalholder[i].phone.getText().toString());
                    item2.put("CHECK", "0");
                    data.set(i, item2);
                }
            }
        }
        int i2 = 0;
        while (i2 < totalNum) {
            try {
                new HashMap();
                Map<String, Object> item3 = data.get(i2);
                String str = (String) item3.get("NAME");
                String number = (String) item3.get("NUMBER");
                if (((String) item3.get("CHECK")).equals("1")) {
                    list = String.valueOf(list) + number + ",";
                }
                i2++;
            } catch (Exception e) {
            }
        }
        return list;
    }
}
