package com.jon.fitnesstips;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;

public class FileEditActivity extends Activity {
    private static final int DIALOG1 = 1;
    private static final int DIALOG2 = 2;
    private static final int DIALOG3 = 3;
    private static final int DIALOG4 = 4;
    private static final int DIALOG5 = 5;
    private static final int DIALOG6 = 6;
    private static final int ITEM0 = 1;
    private static final int ITEM1 = 2;
    private final int REQUEST_CODE_ONE = 1;
    private final int REQUEST_CODE_TWO = 2;
    Button cancel;
    private List<String> contact = new ArrayList();
    TextView file;
    TextView filePath;
    private String list_contact = null;
    EditText noteEdit;
    private ProgressDialog pd;
    Button send;
    EditText tabEdit;
    EditText titleEdit;
    private ImageButton user;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.file_select);
        this.titleEdit = (EditText) findViewById(R.id.title_edit);
        this.noteEdit = (EditText) findViewById(R.id.note_edit);
        this.noteEdit.setText(SmsListActivity.smsList.get(SmsListActivity.index));
        this.user = (ImageButton) findViewById(R.id.user);
        this.user.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                FileEditActivity.this.startActivityForResult(new Intent(FileEditActivity.this, ContactsList.class), 1);
            }
        });
        this.send = (Button) findViewById(R.id.send);
        this.send.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (FileEditActivity.this.startSend()) {
                    Toast.makeText(FileEditActivity.this.getApplicationContext(), "send success", 1).show();
                    FileEditActivity.this.finish();
                    return;
                }
                Toast.makeText(FileEditActivity.this.getApplicationContext(), "select contacts first", 1).show();
            }
        });
        this.cancel = (Button) findViewById(R.id.cancel);
        this.cancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                FileEditActivity.this.finish();
            }
        });
    }

    /* access modifiers changed from: private */
    public boolean startSend() {
        String mes = this.noteEdit.getText().toString();
        if (this.contact.isEmpty()) {
            return false;
        }
        for (int i = 0; i < this.contact.size(); i++) {
            sendSMS(this.contact.get(i), mes);
        }
        return true;
    }

    private void sendSMS(String telNumStr, String messageStr) {
        if (telNumStr != null && messageStr != null) {
            int len = messageStr.length();
            SmsManager smsManager = SmsManager.getDefault();
            if (len > 70) {
                for (String sms : smsManager.divideMessage(messageStr)) {
                    smsManager.sendTextMessage(telNumStr, null, sms, null, null);
                }
                return;
            }
            smsManager.sendTextMessage(telNumStr, null, messageStr, null, null);
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Bundle extras;
        if (resultCode == -1 && requestCode == 1 && (extras = data.getExtras()) != null) {
            this.titleEdit.setText("");
            this.list_contact = extras.getString("CONTACT_LIST");
            if (this.list_contact != null && this.list_contact.length() > 0) {
                setListContact(this.list_contact);
            }
            this.titleEdit.setText(this.list_contact);
        }
    }

    private void setListContact(String list_contact2) {
        String[] des = list_contact2.split(",");
        for (String add : des) {
            this.contact.add(add);
        }
    }
}
