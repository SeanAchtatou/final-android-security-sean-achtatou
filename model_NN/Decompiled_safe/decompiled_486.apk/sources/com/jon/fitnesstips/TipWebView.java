package com.jon.fitnesstips;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class TipWebView extends Activity {
    private static final int DIALOG1 = 1;
    private static final String PATH = "/data/data/com.jon.fitnesstips/files/";
    public String fileName = "picsite.xml";
    public String filePath;
    public URL gUrl;
    Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case -1:
                    TipWebView.this.pd.dismiss();
                    TipWebView.this.showDialog(1);
                    return;
                case R.styleable.com_admob_android_ads_AdView_testing:
                    TipWebView.this.startProgress();
                    return;
                case 1:
                    TipWebView.this.pd.dismiss();
                    SmsListActivity.startFlag = true;
                    TipWebView.this.finish();
                    return;
                default:
                    return;
            }
        }
    };
    private WebView mWebView;
    public ProgressDialog pd;

    public void startProgress() {
        this.pd = ProgressDialog.show(this, "", "Downloading......");
    }

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        getWindow().requestFeature(2);
        setContentView((int) R.layout.smswebview);
        this.mWebView = (WebView) findViewById(R.id.webview);
        WebSettings webSettings = this.mWebView.getSettings();
        webSettings.setSavePassword(false);
        webSettings.setSaveFormData(false);
        webSettings.setJavaScriptEnabled(true);
        webSettings.setSupportZoom(false);
        this.mWebView.setWebChromeClient(new MyWebChromeClient());
        this.mWebView.setWebViewClient(new MyWebClient());
        this.mWebView.addJavascriptInterface(new DemoJavaScriptInterface(), "apkshare");
        if (SmsListActivity.which == 1) {
            this.mWebView.loadUrl("http://sms.apkshare.com/wo/?c=002002");
        } else if (SmsListActivity.which == 2) {
            this.mWebView.loadUrl("http://sms.apkshare.com/wo/index.php?c=008001");
        } else if (SmsListActivity.which == 3) {
            this.mWebView.loadUrl("http://sms.apkshare.com/jokers/index.php?c=010001");
        } else if (SmsListActivity.which == 4) {
            this.mWebView.loadUrl("http://sms.apkshare.com/jokers/?c=007002");
        } else if (SmsListActivity.which == 5) {
            this.mWebView.loadUrl("http://sms.apkshare.com/wo/?c=002003");
        } else if (SmsListActivity.which == 6) {
            this.mWebView.loadUrl("http://sms.apkshare.com/wo/?c=002004");
        } else {
            this.mWebView.loadUrl("http://sms.apkshare.com/wo/?c=002002");
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || !this.mWebView.canGoBack()) {
            return super.onKeyDown(keyCode, event);
        }
        this.mWebView.goBack();
        return true;
    }

    final class DemoJavaScriptInterface {
        DemoJavaScriptInterface() {
        }

        public void adver_market(String file_path) {
            Intent installIntent = new Intent("android.intent.action.VIEW");
            installIntent.setData(Uri.parse(file_path));
            TipWebView.this.startActivity(installIntent);
        }

        public void clickOnAndroid(String file_path) {
            try {
                TipWebView.this.gUrl = new URL(file_path);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            Message msg = new Message();
            msg.what = 0;
            TipWebView.this.handler.sendMessage(msg);
            new Downloader(TipWebView.this, null).start();
        }
    }

    private class Downloader extends Thread {
        private Downloader() {
        }

        /* synthetic */ Downloader(TipWebView tipWebView, Downloader downloader) {
            this();
        }

        public void run() {
            try {
                download();
                Message msg = new Message();
                msg.what = 1;
                TipWebView.this.handler.sendMessage(msg);
            } catch (IOException e) {
                Message msg2 = new Message();
                msg2.what = -1;
                TipWebView.this.handler.sendMessage(msg2);
            }
        }

        private void download() throws IOException {
            new File(TipWebView.PATH).mkdir();
            ZipInputStream zipStream = new ZipInputStream(TipWebView.this.gUrl.openConnection().getInputStream());
            byte[] buffer = new byte[65536];
            while (true) {
                ZipEntry entry = zipStream.getNextEntry();
                if (entry == null) {
                    zipStream.close();
                    return;
                }
                File outFile = new File(TipWebView.PATH + TipWebView.this.fileName);
                TipWebView.this.filePath = TipWebView.PATH + TipWebView.this.fileName;
                if (entry.isDirectory()) {
                    outFile.mkdir();
                } else {
                    outFile.createNewFile();
                    FileOutputStream out = new FileOutputStream(outFile);
                    while (true) {
                        int readBytes = zipStream.read(buffer, 0, buffer.length);
                        if (readBytes <= 0) {
                            break;
                        }
                        out.write(buffer, 0, readBytes);
                    }
                    out.close();
                }
            }
        }
    }

    final class MyWebChromeClient extends WebChromeClient {
        MyWebChromeClient() {
        }

        public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
            result.confirm();
            return true;
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        public void onProgressChanged(WebView view, int newProgress) {
            TipWebView.this.getWindow().setFeatureInt(2, newProgress * 100);
            super.onProgressChanged(view, newProgress);
        }
    }

    final class MyWebClient extends WebViewClient {
        MyWebClient() {
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
        }

        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                return buildDialog1(this);
            default:
                return null;
        }
    }

    private Dialog buildDialog1(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Fail DownLoad");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        return builder.create();
    }
}
