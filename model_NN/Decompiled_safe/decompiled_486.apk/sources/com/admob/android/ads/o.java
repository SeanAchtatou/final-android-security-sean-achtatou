package com.admob.android.ads;

import android.content.Context;
import android.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Locale;
import java.util.Properties;

/* compiled from: AdMobLocalizer */
final class o implements h {
    private static o a = null;
    private static Context b = null;
    private static Thread c = null;
    private static String d = null;
    private Properties e = null;
    private Context f;

    public static void a(Context context) {
        if (b == null) {
            b = context.getApplicationContext();
        }
        if (a == null) {
            a = new o(b);
        }
    }

    private o(Context context) {
        this.f = context;
        d = a();
        if (a != null) {
            a.e = null;
        }
        if (!b() && c == null) {
            Thread thread = new Thread(e.a("http://mm.admob.com/static/android/i18n/20100331" + "/" + d + ".properties", (String) null, AdManager.getUserId(this.f), this, 1));
            c = thread;
            thread.start();
        }
    }

    public static String a() {
        if (d == null) {
            String language = Locale.getDefault().getLanguage();
            d = language;
            if (language == null) {
                d = "en";
            }
        }
        return d;
    }

    private boolean b() {
        if (this.e == null) {
            try {
                Properties properties = new Properties();
                File a2 = a(this.f, d);
                if (a2.exists()) {
                    properties.load(new FileInputStream(a2));
                    this.e = properties;
                }
            } catch (IOException e2) {
                this.e = null;
            }
        }
        return this.e != null;
    }

    private static File a(Context context, String str) throws IOException {
        File file = new File(context.getCacheDir(), "admob_cache");
        if (!file.exists()) {
            file.mkdir();
        }
        File file2 = new File(file, AdManager.SDK_VERSION_DATE);
        if (!file2.exists()) {
            file2.mkdir();
        }
        return new File(file2, str + ".properties");
    }

    public final void a(r rVar, Exception exc) {
        if (Log.isLoggable(AdManager.LOG, 3)) {
            Log.d(AdManager.LOG, "Could not get localized strings from the AdMob servers.");
        }
    }

    public final void a(r rVar) {
        try {
            byte[] d2 = rVar.d();
            if (d2 != null) {
                FileOutputStream fileOutputStream = new FileOutputStream(a(this.f, d));
                fileOutputStream.write(d2);
                fileOutputStream.close();
            }
        } catch (Exception e2) {
            if (Log.isLoggable(AdManager.LOG, 3)) {
                Log.d(AdManager.LOG, "Could not store localized strings to cache file.");
            }
        }
    }
}
