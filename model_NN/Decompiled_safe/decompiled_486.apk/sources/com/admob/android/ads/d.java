package com.admob.android.ads;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PaintFlagsDrawFilter;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.admob.android.ads.view.AdMobWebView;
import com.jon.fitnesstips.R;
import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: Ad */
final class d implements View.OnClickListener, h {
    private static final int c = Color.rgb(102, 102, 102);
    private static final Rect d = new Rect(0, 0, 0, 0);
    private static final PointF e;
    private static final PointF f;
    private static final PointF g = new PointF(0.5f, 0.5f);
    private static final Matrix h = new Matrix();
    private static final RectF i = new RectF(0.0f, 0.0f, 0.0f, 0.0f);
    private Hashtable<String, byte[]> A = new Hashtable<>();
    private HashSet<r> B = new HashSet<>();
    private Vector<Bitmap> C = new Vector<>();
    private b D = null;
    private double E = -1.0d;
    private double F = -1.0d;
    protected View a;
    protected g b;
    private String j = null;
    private String k = null;
    private boolean l;
    private boolean m;
    private Vector<Intent> n = new Vector<>();
    private Vector<String> o = new Vector<>();
    private Vector<e> p = new Vector<>();
    private f q;
    private Rect r = null;
    private long s = 0;
    private int t = -1;
    private int u = -1;
    private a v = null;
    private int w = -1;
    private int x = -1;
    private int y = -16777216;
    private JSONObject z;

    /* compiled from: Ad */
    interface b {
        void a();
    }

    /* compiled from: Ad */
    protected enum f {
        CLICK_TO_MAP,
        CLICK_TO_VIDEO,
        CLICK_TO_APP,
        CLICK_TO_BROWSER,
        CLICK_TO_CALL,
        CLICK_TO_MUSIC,
        CLICK_TO_CANVAS,
        CLICK_TO_CONTACT
    }

    static /* synthetic */ void a(d dVar) {
        if (dVar.v != null) {
            dVar.v.a(dVar);
        }
    }

    static {
        PointF pointF = new PointF(0.0f, 0.0f);
        e = pointF;
        f = pointF;
    }

    public static d a(a aVar, JSONObject jSONObject, int i2, int i3, int i4, g gVar) {
        if (jSONObject == null || jSONObject.length() == 0) {
            return null;
        }
        d dVar = new d();
        dVar.v = aVar;
        dVar.w = i2;
        dVar.x = i3;
        dVar.y = i4;
        dVar.b = gVar;
        if (!dVar.c(jSONObject)) {
            return null;
        }
        return dVar;
    }

    protected d() {
    }

    /* access modifiers changed from: package-private */
    public final double a() {
        return this.E;
    }

    private void a(String str, boolean z2) {
        if (str != null && !"".equals(str)) {
            this.p.add(new e(str, z2));
        }
    }

    private void a(String str) {
        if (str != null && !"".equals(str)) {
            this.o.add(str);
        }
    }

    /* access modifiers changed from: package-private */
    public final int a(int i2) {
        double d2 = (double) i2;
        if (this.F > 0.0d) {
            d2 *= this.F;
        }
        return (int) d2;
    }

    /* access modifiers changed from: package-private */
    public final Rect a(Rect rect) {
        Rect rect2 = new Rect(rect);
        if (this.F > 0.0d) {
            rect2.left = a(rect.left);
            rect2.top = a(rect.top);
            rect2.right = a(rect.right);
            rect2.bottom = a(rect.bottom);
        }
        return rect2;
    }

    public final g b() {
        return this.b;
    }

    public final void a(g gVar) {
        this.b = gVar;
    }

    public final long c() {
        return this.s;
    }

    /* access modifiers changed from: package-private */
    public final boolean d() {
        return this.l;
    }

    public final void a(b bVar) {
        this.D = bVar;
    }

    public final int e() {
        return this.t;
    }

    public final int f() {
        return this.u;
    }

    public final Rect g() {
        if (this.r == null) {
            this.r = new Rect(0, 0, this.t, this.u);
        }
        return this.r;
    }

    /* access modifiers changed from: package-private */
    public final void h() {
        Iterator<Bitmap> it = this.C.iterator();
        while (it.hasNext()) {
            Bitmap next = it.next();
            if (next != null) {
                next.recycle();
            }
        }
        this.C.clear();
    }

    public final void a(JSONObject jSONObject) {
        Uri data;
        if (this.m) {
            Log.i(AdManager.LOG, "Ad clicked again.  Stats on admob.com will only reflect the first click.");
        } else {
            this.m = true;
            Log.i(AdManager.LOG, "Ad clicked.");
            if (this.b != null) {
                Context context = this.b.getContext();
                Iterator<e> it = this.p.iterator();
                while (it.hasNext()) {
                    e next = it.next();
                    AnonymousClass2 r3 = new h(this) {
                        public final void a(r rVar) {
                            if (Log.isLoggable(AdManager.LOG, 3)) {
                                Log.d(AdManager.LOG, "Click processed at " + rVar.f());
                            }
                        }

                        public final void a(r rVar, Exception exc) {
                            if (Log.isLoggable(AdManager.LOG, 3)) {
                                Log.d(AdManager.LOG, "Click processing failed at " + rVar.f(), exc);
                            }
                        }
                    };
                    JSONObject jSONObject2 = null;
                    if (next.b) {
                        jSONObject2 = jSONObject;
                    }
                    e.a(next.a, "click_time_tracking", AdManager.getUserId(context), jSONObject2, r3).g();
                }
            }
        }
        switch (AnonymousClass1.a[this.q.ordinal()]) {
            case R.styleable.com_admob_android_ads_AdView_backgroundColor /*1*/:
                Intent firstElement = this.n.firstElement();
                if (!(firstElement == null || (data = firstElement.getData()) == null)) {
                    String uri = data.toString();
                    String str = this.k;
                    if (AdView.a != null) {
                        AdView.a.post(new c(uri, str, 320, 295, this));
                        break;
                    }
                }
                break;
            default:
                j();
                break;
        }
        if (this.D != null) {
            this.D.a();
        }
    }

    /* compiled from: AdView */
    protected interface a {
        WeakReference<AdView> a;

        default a(AdView adView) {
            this.a = new WeakReference<>(adView);
        }

        default void a() {
            AdView adView = this.a.get();
            if (adView != null) {
                AdView.c(adView);
            }
        }

        default void a(d dVar) {
            AdView adView = this.a.get();
            if (adView != null) {
                synchronized (adView) {
                    if (adView.c == null || !dVar.equals(adView.c.b())) {
                        if (Log.isLoggable(AdManager.LOG, 4)) {
                            Log.i(AdManager.LOG, "Ad returned (" + (SystemClock.uptimeMillis() - adView.p) + " ms):  " + dVar);
                        }
                        adView.getContext();
                        adView.a(dVar, dVar.b());
                    } else if (Log.isLoggable(AdManager.LOG, 3)) {
                        Log.d(AdManager.LOG, "Received the same ad we already had.  Discarding it.");
                    }
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void i() {
        if (this.b != null) {
            Context context = this.b.getContext();
            Iterator<String> it = this.o.iterator();
            while (it.hasNext()) {
                e.a(it.next(), "impression_request", AdManager.getUserId(context)).g();
            }
        }
    }

    /* renamed from: com.admob.android.ads.d$1  reason: invalid class name */
    /* compiled from: Ad */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] a = new int[f.values().length];

        static {
            try {
                a[f.CLICK_TO_CANVAS.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
        }
    }

    private void j() {
        if (this.b != null) {
            Context context = this.b.getContext();
            if (context != null) {
                PackageManager packageManager = context.getPackageManager();
                Iterator<Intent> it = this.n.iterator();
                while (it.hasNext()) {
                    Intent next = it.next();
                    if (packageManager.resolveActivity(next, 65536) != null) {
                        try {
                            context.startActivity(next);
                            return;
                        } catch (Exception e2) {
                        }
                    }
                }
                if (Log.isLoggable(AdManager.LOG, 6)) {
                    Log.e(AdManager.LOG, "Could not find a resolving intent on ad click");
                }
            } else if (Log.isLoggable(AdManager.LOG, 3)) {
                Log.d(AdManager.LOG, "Context null, not able to start Activity.");
            }
        }
    }

    /* compiled from: Ad */
    static class c implements Runnable {
        private String a;
        private int b = 320;
        private int c = 295;
        private String d;
        private WeakReference<d> e;

        public c(String str, String str2, int i, int i2, d dVar) {
            this.a = str;
            this.d = str2;
            this.e = new WeakReference<>(dVar);
        }

        public final void run() {
            try {
                d dVar = this.e.get();
                if (dVar != null && dVar.b != null) {
                    Context context = dVar.b.getContext();
                    WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams(-1, -1, 1000, 2, -3);
                    layoutParams.dimAmount = 0.75f;
                    dVar.a = dVar.a(context, this.a, this.d, this.b, this.c);
                    ((WindowManager) context.getSystemService("window")).addView(dVar.a, layoutParams);
                    AdView.a(dVar.a);
                }
            } catch (Exception e2) {
                if (Log.isLoggable(AdManager.LOG, 6)) {
                    Log.e(AdManager.LOG, "exception caught in Ad$WebViewLoader.run(), " + e2.getMessage());
                }
            }
        }
    }

    public final View a(Context context, String str, String str2, int i2, int i3) {
        RelativeLayout relativeLayout = new RelativeLayout(context);
        relativeLayout.setBackgroundColor(0);
        relativeLayout.setGravity(17);
        AdMobWebView adMobWebView = new AdMobWebView(context, relativeLayout, str2);
        adMobWebView.setBackgroundColor(-1);
        adMobWebView.setId(1);
        relativeLayout.addView(adMobWebView, new RelativeLayout.LayoutParams(a(i2), a(i3)));
        adMobWebView.b = str + "#sdk";
        adMobWebView.loadUrl("http://mm.admob.com/static/android/canvas.html");
        return relativeLayout;
    }

    public final void onClick(View view) {
        if (this.b != null) {
            ((WindowManager) this.b.getContext().getSystemService("window")).removeView(this.a);
            AdView.b(this.a);
            this.a = null;
        }
    }

    public final String toString() {
        String str = this.j;
        if (str == null) {
            return "";
        }
        return str;
    }

    public final boolean equals(Object obj) {
        if (obj instanceof d) {
            return toString().equals(((d) obj).toString());
        }
        return false;
    }

    public final int hashCode() {
        return toString().hashCode();
    }

    /* compiled from: Ad */
    private static class e {
        public String a;
        public boolean b;

        public e(String str, boolean z) {
            this.a = str;
            this.b = z;
        }
    }

    private void b(JSONObject jSONObject) {
        if (jSONObject != null) {
            String optString = jSONObject.optString("a", null);
            String optString2 = jSONObject.optString("d", null);
            int optInt = jSONObject.optInt("f", 268435456);
            Bundle d2 = d(jSONObject.optJSONObject("b"));
            Vector<Intent> vector = this.n;
            Intent intent = new Intent(optString, Uri.parse(optString2));
            if (optInt != 0) {
                intent.addFlags(optInt);
            }
            if (d2 != null) {
                intent.putExtras(d2);
            }
            vector.add(intent);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.admob.android.ads.d.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.admob.android.ads.d.a(android.graphics.RectF, android.graphics.PointF):android.graphics.PointF
      com.admob.android.ads.d.a(org.json.JSONObject, android.graphics.Rect):android.view.View
      com.admob.android.ads.d.a(float, float):com.admob.android.ads.m
      com.admob.android.ads.d.a(java.lang.String, android.view.animation.Animation):void
      com.admob.android.ads.d.a(org.json.JSONObject, com.admob.android.ads.d$d):boolean
      com.admob.android.ads.d.a(org.json.JSONObject, java.lang.String):java.lang.String[]
      com.admob.android.ads.d.a(com.admob.android.ads.r, java.lang.Exception):void
      com.admob.android.ads.h.a(com.admob.android.ads.r, java.lang.Exception):void
      com.admob.android.ads.d.a(java.lang.String, boolean):void */
    private boolean c(JSONObject jSONObject) {
        f fVar;
        String optString = jSONObject.optString("jsonp_url", null);
        String optString2 = jSONObject.optString("tracking_url", null);
        a(optString, true);
        a(optString2, false);
        this.j = jSONObject.optString("text", null);
        String optString3 = jSONObject.optString("6", null);
        this.k = jSONObject.optString("8", null);
        f fVar2 = f.CLICK_TO_BROWSER;
        if ("map".equals(optString3)) {
            fVar = f.CLICK_TO_MAP;
        } else {
            fVar = "video".equals(optString3) ? f.CLICK_TO_VIDEO : "app".equals(optString3) ? f.CLICK_TO_APP : "url".equals(optString3) ? f.CLICK_TO_BROWSER : "call".equals(optString3) ? f.CLICK_TO_CALL : "itunes".equals(optString3) ? f.CLICK_TO_MUSIC : "canvas".equals(optString3) ? f.CLICK_TO_CANVAS : "contact".equals(optString3) ? f.CLICK_TO_CONTACT : fVar2;
        }
        this.q = fVar;
        if (fVar != f.CLICK_TO_CANVAS || (this.b.getContext() instanceof Activity)) {
            if (jSONObject.has("refreshInterval")) {
                this.E = jSONObject.optDouble("refreshInterval");
            }
            if (jSONObject.has("density")) {
                this.F = jSONObject.optDouble("density");
            } else {
                this.F = (double) g.c();
            }
            PointF a2 = a(jSONObject, "d", (PointF) null);
            if (a2 == null) {
                a2 = new PointF(320.0f, 48.0f);
            }
            if (a2.x < 0.0f || a2.y < 0.0f) {
                return false;
            }
            this.t = (int) a2.x;
            this.u = (int) a2.y;
            JSONObject optJSONObject = jSONObject.optJSONObject("ac");
            if (optJSONObject != null) {
                b(optJSONObject);
            }
            JSONArray optJSONArray = jSONObject.optJSONArray("ac");
            if (optJSONArray != null) {
                for (int i2 = 0; i2 < optJSONArray.length(); i2++) {
                    try {
                        b(optJSONArray.getJSONObject(i2));
                    } catch (JSONException e2) {
                        if (Log.isLoggable(AdManager.LOG, 6)) {
                            Log.e(AdManager.LOG, "Could not form an intent from ad action response: " + optJSONArray.toString());
                        }
                    }
                }
            }
            String optString4 = jSONObject.optString("cpm_url", null);
            if (optString4 != null) {
                this.l = true;
                a(optString4);
            }
            String optString5 = jSONObject.optString("tracking_pixel", null);
            if (optString5 != null) {
                try {
                    new URL(optString5);
                } catch (MalformedURLException e3) {
                    try {
                        optString5 = URLEncoder.encode(optString5, "UTF-8");
                    } catch (UnsupportedEncodingException e4) {
                    }
                }
            }
            if (optString5 != null) {
                a(optString5);
            }
            JSONObject optJSONObject2 = jSONObject.optJSONObject("markup");
            if (optJSONObject2 == null) {
                return false;
            }
            if (optString == null) {
                if (Log.isLoggable(AdManager.LOG, 6)) {
                    Log.e(AdManager.LOG, "Bad response:  didn't get clickURLString.  erroring out.");
                }
                return false;
            }
            this.z = optJSONObject2;
            try {
                n();
                m();
                double optDouble = this.z.optDouble("itid");
                if (optDouble > 0.0d) {
                    this.s = (long) (optDouble * 1000.0d);
                }
            } catch (JSONException e5) {
                if (Log.isLoggable(AdManager.LOG, 6)) {
                    Log.e(AdManager.LOG, "Could not read in the flex ad.", e5);
                }
            }
            l();
            if (k()) {
                o();
            }
            return true;
        }
        if (this.v != null) {
            this.v.a();
        }
        return false;
    }

    private static Bundle d(JSONObject jSONObject) {
        Bundle bundle;
        if (jSONObject == null) {
            return null;
        }
        Iterator<String> keys = jSONObject.keys();
        if (keys.hasNext()) {
            bundle = new Bundle();
        } else {
            bundle = null;
        }
        while (keys.hasNext()) {
            String next = keys.next();
            Object opt = jSONObject.opt(next);
            if (!(next == null || opt == null || next == null || opt == null)) {
                if (opt instanceof String) {
                    bundle.putString(next, (String) opt);
                } else if (opt instanceof Integer) {
                    bundle.putInt(next, ((Integer) opt).intValue());
                } else if (opt instanceof Boolean) {
                    bundle.putBoolean(next, ((Boolean) opt).booleanValue());
                } else if (opt instanceof Double) {
                    bundle.putDouble(next, ((Double) opt).doubleValue());
                } else if (opt instanceof Long) {
                    bundle.putLong(next, ((Long) opt).longValue());
                } else if (opt instanceof JSONObject) {
                    bundle.putBundle(next, d((JSONObject) opt));
                } else if (opt instanceof JSONArray) {
                    a(bundle, next, (JSONArray) opt);
                }
            }
        }
        return bundle;
    }

    private static void a(Bundle bundle, String str, JSONArray jSONArray) {
        if (str != null && jSONArray != null) {
            Vector vector = new Vector();
            int length = jSONArray.length();
            int i2 = 0;
            while (i2 < length) {
                try {
                    vector.add(jSONArray.get(i2));
                    i2++;
                } catch (JSONException e2) {
                    if (Log.isLoggable(AdManager.LOG, 6)) {
                        Log.e(AdManager.LOG, "couldn't read bundle array while adding extras");
                    }
                }
            }
            if (length != 0) {
                try {
                    Object obj = vector.get(0);
                    if (obj instanceof String) {
                        bundle.putStringArray(str, (String[]) vector.toArray(new String[0]));
                    } else if (obj instanceof Integer) {
                        Integer[] numArr = (Integer[]) vector.toArray(new Integer[0]);
                        int[] iArr = new int[numArr.length];
                        for (int i3 = 0; i3 < numArr.length; i3++) {
                            iArr[i3] = numArr[i3].intValue();
                        }
                        bundle.putIntArray(str, iArr);
                    } else if (obj instanceof Boolean) {
                        Boolean[] boolArr = (Boolean[]) vector.toArray(new Boolean[0]);
                        boolean[] zArr = new boolean[boolArr.length];
                        for (int i4 = 0; i4 < zArr.length; i4++) {
                            zArr[i4] = boolArr[i4].booleanValue();
                        }
                        bundle.putBooleanArray(str, zArr);
                    } else if (obj instanceof Double) {
                        Double[] dArr = (Double[]) vector.toArray(new Double[0]);
                        double[] dArr2 = new double[dArr.length];
                        for (int i5 = 0; i5 < dArr2.length; i5++) {
                            dArr2[i5] = dArr[i5].doubleValue();
                        }
                        bundle.putDoubleArray(str, dArr2);
                    } else if (obj instanceof Long) {
                        Long[] lArr = (Long[]) vector.toArray(new Long[0]);
                        long[] jArr = new long[lArr.length];
                        for (int i6 = 0; i6 < jArr.length; i6++) {
                            jArr[i6] = lArr[i6].longValue();
                        }
                        bundle.putLongArray(str, jArr);
                    }
                } catch (ArrayStoreException e3) {
                    if (Log.isLoggable(AdManager.LOG, 6)) {
                        Log.e(AdManager.LOG, "Couldn't read in array when making extras");
                    }
                }
            }
        }
    }

    private boolean k() {
        return this.B == null || this.B.size() == 0;
    }

    private void l() {
        if (this.B != null) {
            synchronized (this.B) {
                Iterator<r> it = this.B.iterator();
                while (it.hasNext()) {
                    it.next().g();
                }
            }
        }
    }

    private void m() {
        Rect rect = new Rect(0, 0, this.t, this.u);
        if (this.z.has("ta")) {
            try {
                JSONArray jSONArray = this.z.getJSONArray("ta");
                int i2 = jSONArray.getInt(0);
                int i3 = jSONArray.getInt(1);
                Rect rect2 = new Rect(i2, i3, jSONArray.getInt(2) + i2, jSONArray.getInt(3) + i3);
                if (Math.abs(rect2.width()) >= 44 && Math.abs(rect2.height()) >= 44) {
                    rect = rect2;
                }
            } catch (JSONException e2) {
                if (Log.isLoggable(AdManager.LOG, 3)) {
                    Log.d(AdManager.LOG, "could not read in the touchable area for the ad.");
                }
            }
        }
        this.r = rect;
    }

    private void n() throws JSONException {
        JSONObject optJSONObject = this.z.optJSONObject("$");
        if (optJSONObject != null) {
            Iterator<String> keys = optJSONObject.keys();
            if (this.B != null) {
                synchronized (this.B) {
                    if (this.b != null) {
                        Context context = this.b.getContext();
                        while (keys.hasNext()) {
                            String next = keys.next();
                            this.B.add(e.a(optJSONObject.getJSONObject(next).getString("u"), next, AdManager.getUserId(context), this));
                        }
                    }
                }
            }
        }
    }

    public final void a(r rVar, Exception exc) {
        String str;
        String str2;
        String str3;
        String str4;
        if (exc != null) {
            if (Log.isLoggable(AdManager.LOG, 3)) {
                if (rVar != null) {
                    String e2 = rVar.e();
                    URL f2 = rVar.f();
                    if (f2 != null) {
                        String url = f2.toString();
                        str4 = e2;
                        str3 = url;
                    } else {
                        str4 = e2;
                        str3 = null;
                    }
                } else {
                    str3 = null;
                    str4 = null;
                }
                Log.d(AdManager.LOG, "Failed downloading assets for ad: " + str4 + " " + str3, exc);
            }
        } else if (Log.isLoggable(AdManager.LOG, 3)) {
            if (rVar != null) {
                String e3 = rVar.e();
                URL f3 = rVar.f();
                if (f3 != null) {
                    String url2 = f3.toString();
                    str2 = e3;
                    str = url2;
                } else {
                    str2 = e3;
                    str = null;
                }
            } else {
                str = null;
                str2 = null;
            }
            Log.d(AdManager.LOG, "Failed downloading assets for ad: " + str2 + " " + str);
        }
        p();
    }

    public final void a(r rVar) {
        String e2 = rVar.e();
        byte[] d2 = rVar.d();
        if (d2 != null) {
            this.A.put(e2, d2);
            if (this.B != null) {
                synchronized (this.B) {
                    this.B.remove(rVar);
                }
            }
            if (k()) {
                o();
                return;
            }
            return;
        }
        if (Log.isLoggable(AdManager.LOG, 3)) {
            Log.d(AdManager.LOG, "Failed reading asset(" + e2 + ") for ad");
        }
        p();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        com.admob.android.ads.AdView.a.post(r1);
     */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0046  */
    /* JADX WARNING: Removed duplicated region for block: B:45:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void o() {
        /*
            r5 = this;
            r4 = 0
            org.json.JSONObject r0 = r5.z
            if (r0 == 0) goto L_0x004d
            org.json.JSONObject r0 = r5.z
            r5.z = r4
            java.lang.String r1 = "v"
            org.json.JSONArray r0 = r0.optJSONArray(r1)     // Catch:{ JSONException -> 0x0057 }
            if (r0 == 0) goto L_0x0069
            com.admob.android.ads.d$d r1 = new com.admob.android.ads.d$d     // Catch:{ JSONException -> 0x0057 }
            com.admob.android.ads.g r2 = r5.b     // Catch:{ JSONException -> 0x0057 }
            r1.<init>(r2, r5)     // Catch:{ JSONException -> 0x0057 }
            r2 = 0
        L_0x0019:
            int r3 = r0.length()     // Catch:{ JSONException -> 0x0057 }
            if (r2 >= r3) goto L_0x0051
            org.json.JSONObject r3 = r0.getJSONObject(r2)     // Catch:{ JSONException -> 0x0057 }
            boolean r3 = r5.a(r3, r1)     // Catch:{ JSONException -> 0x0057 }
            if (r3 != 0) goto L_0x004e
            com.admob.android.ads.d$a r0 = r5.v     // Catch:{ JSONException -> 0x0057 }
            if (r0 == 0) goto L_0x0032
            com.admob.android.ads.d$a r0 = r5.v     // Catch:{ JSONException -> 0x0057 }
            r0.a()     // Catch:{ JSONException -> 0x0057 }
        L_0x0032:
            java.util.HashSet<com.admob.android.ads.r> r0 = r5.B
            if (r0 == 0) goto L_0x0042
            java.util.HashSet<com.admob.android.ads.r> r0 = r5.B
            monitor-enter(r0)
            java.util.HashSet<com.admob.android.ads.r> r1 = r5.B     // Catch:{ all -> 0x0073 }
            r1.clear()     // Catch:{ all -> 0x0073 }
            r1 = 0
            r5.B = r1     // Catch:{ all -> 0x0073 }
            monitor-exit(r0)     // Catch:{ all -> 0x0073 }
        L_0x0042:
            java.util.Hashtable<java.lang.String, byte[]> r0 = r5.A
            if (r0 == 0) goto L_0x004d
            java.util.Hashtable<java.lang.String, byte[]> r0 = r5.A
            r0.clear()
            r5.A = r4
        L_0x004d:
            return
        L_0x004e:
            int r2 = r2 + 1
            goto L_0x0019
        L_0x0051:
            android.os.Handler r0 = com.admob.android.ads.AdView.a     // Catch:{ JSONException -> 0x0057 }
            r0.post(r1)     // Catch:{ JSONException -> 0x0057 }
            goto L_0x0032
        L_0x0057:
            r0 = move-exception
            java.lang.String r1 = "AdMobSDK"
            r2 = 3
            boolean r1 = android.util.Log.isLoggable(r1, r2)
            if (r1 == 0) goto L_0x0032
            java.lang.String r1 = "AdMobSDK"
            java.lang.String r2 = "couldn't construct the views."
            android.util.Log.d(r1, r2, r0)
            goto L_0x0032
        L_0x0069:
            com.admob.android.ads.d$a r0 = r5.v     // Catch:{ JSONException -> 0x0057 }
            if (r0 == 0) goto L_0x0032
            com.admob.android.ads.d$a r0 = r5.v     // Catch:{ JSONException -> 0x0057 }
            r0.a()     // Catch:{ JSONException -> 0x0057 }
            goto L_0x0032
        L_0x0073:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0073 }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.admob.android.ads.d.o():void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.admob.android.ads.d.a(org.json.JSONObject, java.lang.String, float):float
     arg types: [org.json.JSONObject, java.lang.String, int]
     candidates:
      com.admob.android.ads.d.a(org.json.JSONObject, java.lang.String, int):int
      com.admob.android.ads.d.a(org.json.JSONObject, java.lang.String, android.graphics.Matrix):android.graphics.Matrix
      com.admob.android.ads.d.a(org.json.JSONObject, java.lang.String, android.graphics.PointF):android.graphics.PointF
      com.admob.android.ads.d.a(org.json.JSONObject, java.lang.String, android.graphics.Rect):android.graphics.Rect
      com.admob.android.ads.d.a(org.json.JSONObject, java.lang.String, android.graphics.RectF):android.graphics.RectF
      com.admob.android.ads.d.a(float, float, android.view.View):com.admob.android.ads.b
      com.admob.android.ads.d.a(int, int, android.view.View):com.admob.android.ads.k
      com.admob.android.ads.d.a(android.os.Bundle, java.lang.String, org.json.JSONArray):void
      com.admob.android.ads.d.a(android.widget.ImageView, android.graphics.Bitmap, org.json.JSONObject):void
      com.admob.android.ads.d.a(org.json.JSONObject, android.view.animation.Animation, android.view.animation.AnimationSet):void
      com.admob.android.ads.d.a(org.json.JSONObject, java.lang.String, float):float */
    private boolean a(JSONObject jSONObject, C0000d dVar) throws JSONException {
        boolean z2;
        ImageView imageView;
        q qVar;
        Typeface typeface;
        try {
            String string = jSONObject.getString("t");
            Rect a2 = a(a(jSONObject, "f", d));
            q qVar2 = null;
            if ("l".equals(string)) {
                if (this.b != null) {
                    String string2 = jSONObject.getString("x");
                    float a3 = a(jSONObject, "fs", 13.0f);
                    JSONArray optJSONArray = jSONObject.optJSONArray("fa");
                    Typeface typeface2 = Typeface.DEFAULT;
                    if (optJSONArray != null) {
                        int i2 = 0;
                        for (int i3 = 0; i3 < optJSONArray.length(); i3++) {
                            String string3 = optJSONArray.getString(i3);
                            if ("b".equals(string3)) {
                                i2 |= 1;
                            } else if ("i".equals(string3)) {
                                i2 |= 2;
                            } else if ("m".equals(string3)) {
                                typeface2 = Typeface.MONOSPACE;
                            } else if ("s".equals(string3)) {
                                typeface2 = Typeface.SERIF;
                            } else if ("ss".equals(string3)) {
                                typeface2 = Typeface.SANS_SERIF;
                            }
                        }
                        typeface = Typeface.create(typeface2, i2);
                    } else {
                        typeface = typeface2;
                    }
                    int i4 = this.w;
                    if (jSONObject.has("fco")) {
                        int a4 = a(jSONObject, "fco", i4);
                        if (a4 != i4) {
                            i4 = a4;
                        }
                    } else {
                        i4 = jSONObject.optInt("fc", 0) == 1 ? this.x : this.w;
                    }
                    boolean optBoolean = jSONObject.optBoolean("afstfw", true);
                    float a5 = a(jSONObject, "mfs", 8.0f);
                    int optInt = jSONObject.optInt("nol", 1);
                    q qVar3 = new q(this.b.getContext(), g.c());
                    qVar3.b = optBoolean;
                    qVar3.a = qVar3.c * a5;
                    qVar3.setBackgroundColor(0);
                    qVar3.setText(string2);
                    qVar3.setTextColor(i4);
                    qVar3.setTextSize(1, a3);
                    qVar3.setTypeface(typeface);
                    qVar3.setLines(optInt);
                    qVar = qVar3;
                } else {
                    qVar = null;
                }
                qVar2 = qVar;
                z2 = true;
            } else if ("bg".equals(string)) {
                qVar2 = a(jSONObject, a2);
                z2 = false;
            } else if ("i".equals(string)) {
                if (this.b != null) {
                    String string4 = jSONObject.getString("$");
                    if (string4 != null) {
                        byte[] bArr = this.A.get(string4);
                        if (bArr != null) {
                            Bitmap decodeByteArray = BitmapFactory.decodeByteArray(bArr, 0, bArr.length);
                            ImageView imageView2 = new ImageView(this.b.getContext());
                            imageView2.setScaleType(ImageView.ScaleType.FIT_XY);
                            if (jSONObject.optBoolean("b", false)) {
                                a(imageView2, decodeByteArray, jSONObject);
                                imageView = imageView2;
                            } else {
                                this.C.add(decodeByteArray);
                                imageView2.setImageBitmap(decodeByteArray);
                                imageView = imageView2;
                            }
                        }
                    } else if (Log.isLoggable(AdManager.LOG, 3)) {
                        Log.d(AdManager.LOG, "Could not find asset name " + jSONObject);
                    }
                    imageView = null;
                } else {
                    imageView = null;
                }
                qVar2 = imageView;
                z2 = true;
            } else if ("P".equals(string)) {
                qVar2 = this.b != null ? new View(this.b.getContext()) : null;
                z2 = true;
            } else {
                z2 = true;
            }
            if (qVar2 != null) {
                if (z2) {
                    qVar2.setBackgroundColor(a(jSONObject, "bgc", 0));
                }
                PointF a6 = a(jSONObject, "ap", g);
                f c2 = f.c(qVar2);
                c2.b = a6;
                qVar2.setTag(c2);
                AnimationSet animationSet = null;
                JSONArray optJSONArray2 = jSONObject.optJSONArray("a");
                JSONObject optJSONObject = jSONObject.optJSONObject("ag");
                if (optJSONArray2 != null) {
                    animationSet = a(optJSONArray2, optJSONObject, qVar2, a2);
                }
                String optString = jSONObject.optString("ut", null);
                if (!(qVar2 == null || optString == null)) {
                    qVar2.setTag(f.c(qVar2));
                }
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(a2.width(), a2.height());
                layoutParams.addRule(9);
                layoutParams.addRule(10);
                layoutParams.setMargins(a2.left, a2.top, 0, 0);
                g gVar = new g();
                gVar.a = qVar2;
                gVar.b = layoutParams;
                gVar.c = animationSet;
                dVar.a.add(gVar);
                if (jSONObject.optBoolean("cav") && this.b != null) {
                    this.b.a(qVar2, layoutParams);
                }
                return true;
            }
            if (Log.isLoggable(AdManager.LOG, 6)) {
                Log.e(AdManager.LOG, "created a null view.");
            }
            return false;
        } catch (JSONException e2) {
            if (Log.isLoggable(AdManager.LOG, 6)) {
                Log.e(AdManager.LOG, "exception while trying to create a flex view.", e2);
            }
            return false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.admob.android.ads.d.a(org.json.JSONObject, java.lang.String, float):float
     arg types: [org.json.JSONObject, java.lang.String, int]
     candidates:
      com.admob.android.ads.d.a(org.json.JSONObject, java.lang.String, int):int
      com.admob.android.ads.d.a(org.json.JSONObject, java.lang.String, android.graphics.Matrix):android.graphics.Matrix
      com.admob.android.ads.d.a(org.json.JSONObject, java.lang.String, android.graphics.PointF):android.graphics.PointF
      com.admob.android.ads.d.a(org.json.JSONObject, java.lang.String, android.graphics.Rect):android.graphics.Rect
      com.admob.android.ads.d.a(org.json.JSONObject, java.lang.String, android.graphics.RectF):android.graphics.RectF
      com.admob.android.ads.d.a(float, float, android.view.View):com.admob.android.ads.b
      com.admob.android.ads.d.a(int, int, android.view.View):com.admob.android.ads.k
      com.admob.android.ads.d.a(android.os.Bundle, java.lang.String, org.json.JSONArray):void
      com.admob.android.ads.d.a(android.widget.ImageView, android.graphics.Bitmap, org.json.JSONObject):void
      com.admob.android.ads.d.a(org.json.JSONObject, android.view.animation.Animation, android.view.animation.AnimationSet):void
      com.admob.android.ads.d.a(org.json.JSONObject, java.lang.String, float):float */
    private AnimationSet a(JSONArray jSONArray, JSONObject jSONObject, View view, Rect rect) throws JSONException {
        Animation animation;
        AnimationSet animationSet = new AnimationSet(false);
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= jSONArray.length()) {
                break;
            }
            JSONObject jSONObject2 = jSONArray.getJSONObject(i3);
            Animation animation2 = null;
            String optString = jSONObject2.optString("t", null);
            int a2 = (int) (((double) a(jSONObject2, "d", 0.25f)) * 1000.0d);
            if ("B".equals(optString)) {
                String optString2 = jSONObject2.optString("kp", null);
                String optString3 = jSONObject2.optString("vt", null);
                if (optString2 == null || optString3 == null) {
                    if (Log.isLoggable(AdManager.LOG, 6)) {
                        Log.e(AdManager.LOG, "Could not read basic animation: keyPath(" + optString2 + ") or valueType(" + optString3 + ") is null.");
                        animation = null;
                    }
                    animation = null;
                } else if ("position".equals(optString2) && "P".equals(optString3)) {
                    animation = a(a(jSONObject2, "fv", e), a(jSONObject2, "tv", f), view, rect);
                } else if ("opacity".equals(optString2) && "F".equals(optString3)) {
                    animation = a(a(jSONObject2, "fv", 0.0f), a(jSONObject2, "tv", 0.0f));
                } else if ("transform".equals(optString2) && "AT".equals(optString3)) {
                    a(jSONObject2, "fv", h);
                    a(jSONObject2, "fv", h);
                    animation = a(view, rect, jSONObject2, jSONObject2.getJSONArray("tfv"), jSONObject2.getJSONArray("ttv"));
                } else if ("bounds".equals(optString2) && "R".equals(optString3)) {
                    animation = a(a(jSONObject2, "fv", i), a(jSONObject2, "tv", i), (View) null, rect);
                } else if ("zPosition".equals(optString2) && "F".equals(optString3)) {
                    animation = a(a(jSONObject2, "fv", 0.0f), a(jSONObject2, "tv", 0.0f), view);
                } else if (!"backgroundColor".equals(optString2) || !"C".equals(optString3)) {
                    if (Log.isLoggable(AdManager.LOG, 6)) {
                        Log.e(AdManager.LOG, "Could not read basic animation: could not interpret keyPath(" + optString2 + ") and valueType(" + optString3 + ") combination.");
                    }
                    animation = null;
                } else {
                    animation = a(a(jSONObject2, "fv", 0), a(jSONObject2, "tv", 0), view);
                }
                if (animation != null) {
                    Interpolator a3 = a(jSONObject2.optString("tf", null), -1, -1, -1);
                    if (a3 == null) {
                        a3 = null;
                    }
                    if (a3 != null) {
                        animation.setInterpolator(a3);
                    }
                }
                animation2 = animation;
            } else if ("K".equals(optString)) {
                animation2 = a(jSONObject2, view, rect, (long) a2);
            }
            if (animation2 != null) {
                animation2.setDuration((long) a2);
                a(jSONObject2, animation2, animationSet);
                animationSet.addAnimation(animation2);
                animation2.getDuration();
            }
            i2 = i3 + 1;
        }
        if (jSONObject != null) {
            a(jSONObject, animationSet, (AnimationSet) null);
        }
        return animationSet;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.admob.android.ads.d.a(org.json.JSONObject, java.lang.String, float):float
     arg types: [org.json.JSONObject, java.lang.String, int]
     candidates:
      com.admob.android.ads.d.a(org.json.JSONObject, java.lang.String, int):int
      com.admob.android.ads.d.a(org.json.JSONObject, java.lang.String, android.graphics.Matrix):android.graphics.Matrix
      com.admob.android.ads.d.a(org.json.JSONObject, java.lang.String, android.graphics.PointF):android.graphics.PointF
      com.admob.android.ads.d.a(org.json.JSONObject, java.lang.String, android.graphics.Rect):android.graphics.Rect
      com.admob.android.ads.d.a(org.json.JSONObject, java.lang.String, android.graphics.RectF):android.graphics.RectF
      com.admob.android.ads.d.a(float, float, android.view.View):com.admob.android.ads.b
      com.admob.android.ads.d.a(int, int, android.view.View):com.admob.android.ads.k
      com.admob.android.ads.d.a(android.os.Bundle, java.lang.String, org.json.JSONArray):void
      com.admob.android.ads.d.a(android.widget.ImageView, android.graphics.Bitmap, org.json.JSONObject):void
      com.admob.android.ads.d.a(org.json.JSONObject, android.view.animation.Animation, android.view.animation.AnimationSet):void
      com.admob.android.ads.d.a(org.json.JSONObject, java.lang.String, float):float */
    private static int e(JSONObject jSONObject) {
        int a2 = (int) a(jSONObject, "rc", 1.0f);
        if (a2 > 0) {
            return a2 - 1;
        }
        return a2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.admob.android.ads.d.a(org.json.JSONObject, java.lang.String, float):float
     arg types: [org.json.JSONObject, java.lang.String, int]
     candidates:
      com.admob.android.ads.d.a(org.json.JSONObject, java.lang.String, int):int
      com.admob.android.ads.d.a(org.json.JSONObject, java.lang.String, android.graphics.Matrix):android.graphics.Matrix
      com.admob.android.ads.d.a(org.json.JSONObject, java.lang.String, android.graphics.PointF):android.graphics.PointF
      com.admob.android.ads.d.a(org.json.JSONObject, java.lang.String, android.graphics.Rect):android.graphics.Rect
      com.admob.android.ads.d.a(org.json.JSONObject, java.lang.String, android.graphics.RectF):android.graphics.RectF
      com.admob.android.ads.d.a(float, float, android.view.View):com.admob.android.ads.b
      com.admob.android.ads.d.a(int, int, android.view.View):com.admob.android.ads.k
      com.admob.android.ads.d.a(android.os.Bundle, java.lang.String, org.json.JSONArray):void
      com.admob.android.ads.d.a(android.widget.ImageView, android.graphics.Bitmap, org.json.JSONObject):void
      com.admob.android.ads.d.a(org.json.JSONObject, android.view.animation.Animation, android.view.animation.AnimationSet):void
      com.admob.android.ads.d.a(org.json.JSONObject, java.lang.String, float):float */
    private void a(JSONObject jSONObject, Animation animation, AnimationSet animationSet) {
        float a2 = a(jSONObject, "bt", 0.0f);
        float a3 = a(jSONObject, "to", 0.0f);
        int e2 = e(jSONObject);
        boolean optBoolean = jSONObject.optBoolean("ar", false);
        String optString = jSONObject.optString("fm", "r");
        int i2 = (int) (((double) (a2 + 0.0f + a3)) * 1000.0d);
        float a4 = 1.0f / a(jSONObject, "s", 1.0f);
        a(animation, e2, i2, a4, optString, optBoolean);
        if (animationSet != null) {
            a(animationSet, e2, i2, a4, optString, optBoolean);
        }
    }

    private static void a(Animation animation, int i2, int i3, float f2, String str, boolean z2) {
        if (z2) {
            animation.setRepeatMode(2);
        }
        animation.setRepeatCount(i2);
        animation.setStartOffset((long) i3);
        animation.startNow();
        animation.scaleCurrentDuration(f2);
        a(str, animation);
    }

    private static void a(String str, Animation animation) {
        if (str != null && animation != null) {
            Class<?> cls = animation.getClass();
            try {
                Method method = cls.getMethod("setFillEnabled", Boolean.TYPE);
                if (method != null) {
                    method.invoke(animation, true);
                }
            } catch (Exception e2) {
            }
            if ("b".equals(str)) {
                animation.setFillBefore(true);
                animation.setFillAfter(false);
            } else if ("fb".equals(str) || "r".equals(str)) {
                animation.setFillBefore(true);
                animation.setFillAfter(true);
            } else if ("f".equals(str)) {
                animation.setFillBefore(false);
                animation.setFillAfter(true);
            } else if ("r".equals(str)) {
                animation.setFillBefore(false);
                animation.setFillAfter(false);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.admob.android.ads.j.<init>(float[], float[], float, float, float, boolean):void
     arg types: [float[], float[], float, float, int, int]
     candidates:
      com.admob.android.ads.j.<init>(float, float, float, float, float, boolean):void
      com.admob.android.ads.j.<init>(float[], float[], float, float, float, boolean):void */
    private static Animation a(View view, Rect rect, JSONObject jSONObject, JSONArray jSONArray, JSONArray jSONArray2) throws JSONException {
        String optString = jSONObject.optString("tt", null);
        if (optString != null) {
            if ("t".equals(optString)) {
                return a(e(jSONArray), e(jSONArray2), view, rect);
            }
            if ("r".equals(optString)) {
                float[] b2 = b(jSONArray);
                float[] b3 = b(jSONArray2);
                if (b2 == null || b3 == null || Arrays.equals(b2, b3)) {
                    return null;
                }
                PointF a2 = a(new RectF(rect), f.b(view));
                return new j(b2, b3, a2.x, a2.y, 0.0f, false);
            } else if ("sc".equals(optString)) {
                float[] b4 = b(jSONArray);
                float[] b5 = b(jSONArray2);
                PointF b6 = f.b(view);
                return new ScaleAnimation(b4[0], b5[0], b4[1], b5[1], 1, b6.x, 1, b6.y);
            } else if ("sk".equals(optString)) {
                float[] b7 = b(jSONArray);
                float[] b8 = b(jSONArray2);
                if (b7 == null || b8 == null || Arrays.equals(b7, b8)) {
                    return null;
                }
                return new p(b7, b8, a(new RectF(rect), f.b(view)));
            } else {
                "p".equals(optString);
            }
        }
        return null;
    }

    private static m a(float f2, float f3) {
        if (f2 != f3) {
            return new m(f2, f3);
        }
        return null;
    }

    private static l a(PointF pointF, PointF pointF2, View view, Rect rect) {
        if (pointF.equals(pointF2)) {
            return null;
        }
        PointF b2 = f.b(view);
        float width = (((float) rect.width()) * b2.x) + ((float) rect.left);
        float height = (b2.y * ((float) rect.height())) + ((float) rect.top);
        pointF.x -= width;
        pointF.y -= height;
        pointF2.x -= width;
        pointF2.y -= height;
        return new l(0, pointF.x, 0, pointF2.x, 0, pointF.y, 0, pointF2.y);
    }

    private AnimationSet a(JSONObject jSONObject, View view, Rect rect, long j2) throws JSONException {
        String string = jSONObject.getString("vt");
        float[] b2 = b(jSONObject, "kt");
        JSONArray jSONArray = jSONObject.getJSONArray("vs");
        String[] a2 = a(jSONObject, "tfs");
        JSONArray optJSONArray = jSONObject.optJSONArray("ttvs");
        int length = b2.length;
        int length2 = jSONArray.length();
        int length3 = a2.length;
        if (!(length == length2 && length2 == length3 + 1) && ((double) b2[0]) == 0.0d && ((double) b2[length - 1]) == 1.0d) {
            if (Log.isLoggable(AdManager.LOG, 6)) {
                Log.e(AdManager.LOG, "keyframe animations were invalid: numKeyTimes=" + length + " numKeyValues=" + length2 + " numKeyFunctions=" + length3 + " keyTimes[0]=" + b2[0] + " keyTimes[" + (length - 1) + "]=" + b2[length - 1]);
            }
            return null;
        }
        AnimationSet animationSet = new AnimationSet(false);
        String string2 = jSONObject.getString("kp");
        int e2 = e(jSONObject);
        for (int i2 = 0; i2 < length - 1; i2++) {
            Animation a3 = a(i2, string2, string, b2, jSONArray, a2, j2, view, rect, jSONObject, optJSONArray);
            if (a3 != null) {
                a3.setRepeatCount(e2);
                animationSet.addAnimation(a3);
            }
        }
        a(jSONObject.optString("fm", "r"), animationSet);
        return animationSet;
    }

    private static Animation a(int i2, String str, String str2, float[] fArr, JSONArray jSONArray, String[] strArr, long j2, View view, Rect rect, JSONObject jSONObject, JSONArray jSONArray2) {
        Animation animation;
        int i3 = i2 + 1;
        float f2 = fArr[i2];
        float f3 = fArr[i3];
        if (str == null || str2 == null) {
            if (Log.isLoggable(AdManager.LOG, 6)) {
                Log.e(AdManager.LOG, "Could not read keyframe animation: keyPath(" + str + ") or valueType(" + str2 + ") is null.");
                animation = null;
            }
            animation = null;
        } else {
            try {
                if ("position".equals(str) && "P".equals(str2)) {
                    animation = a(e(jSONArray.getJSONArray(i2)), e(jSONArray.getJSONArray(i3)), view, rect);
                } else if ("opacity".equals(str) && "F".equals(str2)) {
                    animation = a((float) jSONArray.getDouble(i2), (float) jSONArray.getDouble(i3));
                } else if ("bounds".equals(str) && "R".equals(str2)) {
                    animation = a(d(jSONArray.getJSONArray(i2)), d(jSONArray.getJSONArray(i3)), view, rect);
                } else if ("zPosition".equals(str) && "F".equals(str2)) {
                    animation = a((float) jSONArray.getDouble(i2), (float) jSONArray.getDouble(i3), view);
                } else if (!"backgroundColor".equals(str) || !"C".equals(str2)) {
                    if (!"transform".equals(str) || !"AT".equals(str2)) {
                        if (Log.isLoggable(AdManager.LOG, 6)) {
                            Log.e(AdManager.LOG, "Could not read keyframe animation: could not interpret keyPath(" + str + ") and valueType(" + str2 + ") combination.");
                        }
                    } else if (jSONArray2 != null) {
                        a(jSONArray.getJSONArray(i2));
                        a(jSONArray.getJSONArray(i3));
                        animation = a(view, rect, jSONObject, jSONArray2.getJSONArray(i2), jSONArray2.getJSONArray(i3));
                    }
                    animation = null;
                } else {
                    animation = a(c(jSONArray.getJSONArray(i2)), c(jSONArray.getJSONArray(i3)), view);
                }
            } catch (JSONException e2) {
            }
        }
        if (animation != null) {
            animation.setDuration(j2);
            Interpolator a2 = a(strArr[i2], (long) ((int) (((float) j2) * f2)), (long) ((int) ((f3 - f2) * ((float) j2))), j2);
            if (a2 != null) {
                animation.setInterpolator(a2);
            }
        }
        return animation;
    }

    private static k a(int i2, int i3, View view) {
        if (i2 != i3) {
            return new k(i2, i3, view);
        }
        return null;
    }

    private static b a(float f2, float f3, View view) {
        if (f2 != f3) {
            return new b(f2, f3, view);
        }
        return null;
    }

    private static n a(RectF rectF, RectF rectF2, View view, Rect rect) {
        if (rectF.equals(rectF2)) {
            return null;
        }
        PointF a2 = a(rectF, f.b(view));
        float width = (float) rect.width();
        float height = (float) rect.height();
        return new n(rectF.width() / width, rectF2.width() / width, rectF.height() / height, rectF2.height() / height, a2.x, a2.y);
    }

    private static PointF a(RectF rectF, PointF pointF) {
        float width = rectF.width();
        float height = rectF.height();
        return new PointF((width * pointF.x) + rectF.left, (height * pointF.y) + rectF.top);
    }

    /* compiled from: Ad */
    private static class g {
        public View a;
        public RelativeLayout.LayoutParams b;
        public AnimationSet c;

        /* synthetic */ g() {
            this((byte) 0);
        }

        private g(byte b2) {
        }
    }

    /* renamed from: com.admob.android.ads.d$d  reason: collision with other inner class name */
    /* compiled from: Ad */
    private static class C0000d implements Runnable {
        Vector<g> a = new Vector<>();
        private g b;
        private WeakReference<d> c;

        public C0000d(g gVar, d dVar) {
            this.b = gVar;
            this.c = new WeakReference<>(dVar);
        }

        public final void run() {
            try {
                if (this.b != null) {
                    this.b.setPadding(0, 0, 0, 0);
                    Iterator<g> it = this.a.iterator();
                    while (it.hasNext()) {
                        g next = it.next();
                        View view = next.a;
                        this.b.addView(view, next.b);
                        AnimationSet animationSet = next.c;
                        if (animationSet != null) {
                            view.startAnimation(animationSet);
                        }
                    }
                    this.b.invalidate();
                    this.b.requestLayout();
                } else if (Log.isLoggable(AdManager.LOG, 6)) {
                    Log.e(AdManager.LOG, "containerView was null in ViewAdd!");
                }
                d dVar = this.c.get();
                if (dVar != null) {
                    d.a(dVar);
                }
            } catch (Exception e) {
                if (Log.isLoggable(AdManager.LOG, 6)) {
                    Log.e(AdManager.LOG, "exception caught in Ad$ViewAdd.run(), " + e.getMessage());
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.admob.android.ads.d.a(org.json.JSONObject, java.lang.String, float):float
     arg types: [org.json.JSONObject, java.lang.String, int]
     candidates:
      com.admob.android.ads.d.a(org.json.JSONObject, java.lang.String, int):int
      com.admob.android.ads.d.a(org.json.JSONObject, java.lang.String, android.graphics.Matrix):android.graphics.Matrix
      com.admob.android.ads.d.a(org.json.JSONObject, java.lang.String, android.graphics.PointF):android.graphics.PointF
      com.admob.android.ads.d.a(org.json.JSONObject, java.lang.String, android.graphics.Rect):android.graphics.Rect
      com.admob.android.ads.d.a(org.json.JSONObject, java.lang.String, android.graphics.RectF):android.graphics.RectF
      com.admob.android.ads.d.a(float, float, android.view.View):com.admob.android.ads.b
      com.admob.android.ads.d.a(int, int, android.view.View):com.admob.android.ads.k
      com.admob.android.ads.d.a(android.os.Bundle, java.lang.String, org.json.JSONArray):void
      com.admob.android.ads.d.a(android.widget.ImageView, android.graphics.Bitmap, org.json.JSONObject):void
      com.admob.android.ads.d.a(org.json.JSONObject, android.view.animation.Animation, android.view.animation.AnimationSet):void
      com.admob.android.ads.d.a(org.json.JSONObject, java.lang.String, float):float */
    private View a(JSONObject jSONObject, Rect rect) throws JSONException {
        if (this.b == null) {
            return null;
        }
        float a2 = a(jSONObject, "ia", 0.5f);
        float a3 = a(jSONObject, "epy", 0.4375f);
        int a4 = a(jSONObject, "bc", this.y);
        try {
            Bitmap createBitmap = Bitmap.createBitmap(rect.width(), rect.height(), Bitmap.Config.ARGB_8888);
            this.C.add(createBitmap);
            Canvas canvas = new Canvas(createBitmap);
            int height = ((int) (a3 * ((float) rect.height()))) + rect.top;
            Rect rect2 = new Rect(rect.left, rect.top, rect.right, height);
            Paint paint = new Paint();
            paint.setColor(-1);
            paint.setStyle(Paint.Style.FILL);
            canvas.drawRect(rect2, paint);
            GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{Color.argb((int) (a2 * 255.0f), Color.red(a4), Color.green(a4), Color.blue(a4)), a4});
            gradientDrawable.setBounds(rect2);
            gradientDrawable.draw(canvas);
            Rect rect3 = new Rect(rect.left, height, rect.right, rect.bottom);
            Paint paint2 = new Paint();
            paint2.setColor(a4);
            paint2.setStyle(Paint.Style.FILL);
            canvas.drawRect(rect3, paint2);
            View view = new View(this.b.getContext());
            view.setBackgroundDrawable(new BitmapDrawable(createBitmap));
            return view;
        } catch (Throwable th) {
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.admob.android.ads.d.a(org.json.JSONObject, java.lang.String, float):float
     arg types: [org.json.JSONObject, java.lang.String, int]
     candidates:
      com.admob.android.ads.d.a(org.json.JSONObject, java.lang.String, int):int
      com.admob.android.ads.d.a(org.json.JSONObject, java.lang.String, android.graphics.Matrix):android.graphics.Matrix
      com.admob.android.ads.d.a(org.json.JSONObject, java.lang.String, android.graphics.PointF):android.graphics.PointF
      com.admob.android.ads.d.a(org.json.JSONObject, java.lang.String, android.graphics.Rect):android.graphics.Rect
      com.admob.android.ads.d.a(org.json.JSONObject, java.lang.String, android.graphics.RectF):android.graphics.RectF
      com.admob.android.ads.d.a(float, float, android.view.View):com.admob.android.ads.b
      com.admob.android.ads.d.a(int, int, android.view.View):com.admob.android.ads.k
      com.admob.android.ads.d.a(android.os.Bundle, java.lang.String, org.json.JSONArray):void
      com.admob.android.ads.d.a(android.widget.ImageView, android.graphics.Bitmap, org.json.JSONObject):void
      com.admob.android.ads.d.a(org.json.JSONObject, android.view.animation.Animation, android.view.animation.AnimationSet):void
      com.admob.android.ads.d.a(org.json.JSONObject, java.lang.String, float):float */
    private void a(ImageView imageView, Bitmap bitmap, JSONObject jSONObject) {
        Bitmap bitmap2;
        float a2 = a(jSONObject, "bw", 0.5f);
        int a3 = a(jSONObject, "bdc", c);
        float a4 = a(jSONObject, "br", 6.5f);
        if (a2 < 1.0f) {
            a2 = 1.0f;
        }
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        try {
            Bitmap createBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            createBitmap.eraseColor(0);
            Canvas canvas = new Canvas(createBitmap);
            canvas.setDrawFilter(new PaintFlagsDrawFilter(0, 1));
            float f2 = a4 + a2;
            Path path = new Path();
            RectF rectF = new RectF(0.0f, 0.0f, (float) width, (float) height);
            path.addRoundRect(rectF, f2, f2, Path.Direction.CCW);
            canvas.clipPath(path, Region.Op.REPLACE);
            canvas.drawBitmap(bitmap, 0.0f, 0.0f, new Paint(3));
            canvas.clipRect(rectF, Region.Op.REPLACE);
            Paint paint = new Paint(1);
            paint.setStrokeWidth(a2);
            paint.setColor(a3);
            paint.setStyle(Paint.Style.STROKE);
            Path path2 = new Path();
            float f3 = a2 / 2.0f;
            path2.addRoundRect(new RectF(f3, f3, ((float) width) - f3, ((float) height) - f3), a4, a4, Path.Direction.CCW);
            canvas.drawPath(path2, paint);
            if (bitmap != null) {
                bitmap.recycle();
            }
            bitmap2 = createBitmap;
        } catch (Throwable th) {
            bitmap2 = bitmap;
        }
        this.C.add(bitmap2);
        imageView.setImageBitmap(bitmap2);
    }

    private void p() {
        if (this.B != null) {
            synchronized (this.B) {
                Iterator<r> it = this.B.iterator();
                while (it.hasNext()) {
                    it.next().b();
                }
                this.B.clear();
                this.B = null;
            }
        }
        if (this.A != null) {
            this.A.clear();
            this.A = null;
        }
        this.z = null;
        if (this.v != null) {
            this.v.a();
        }
    }

    private static Interpolator a(String str, long j2, long j3, long j4) {
        AccelerateInterpolator accelerateInterpolator = "i".equals(str) ? new AccelerateInterpolator() : "o".equals(str) ? new DecelerateInterpolator() : "io".equals(str) ? new AccelerateDecelerateInterpolator() : "l".equals(str) ? new LinearInterpolator() : null;
        if (accelerateInterpolator == null || j2 == -1 || j3 == -1 || j4 == -1) {
            return accelerateInterpolator;
        }
        return new c(accelerateInterpolator, j2, j3, j4);
    }

    private static Matrix a(JSONArray jSONArray) {
        float[] b2 = b(jSONArray);
        if (b2 == null || b2.length != 9) {
            return null;
        }
        Matrix matrix = new Matrix();
        matrix.setValues(b2);
        return matrix;
    }

    private static Matrix a(JSONObject jSONObject, String str, Matrix matrix) {
        float[] b2 = b(jSONObject, str);
        if (b2 == null || b2.length != 9) {
            return matrix;
        }
        Matrix matrix2 = new Matrix();
        matrix2.setValues(b2);
        return matrix2;
    }

    private static String[] a(JSONObject jSONObject, String str) {
        JSONArray optJSONArray = jSONObject.optJSONArray(str);
        if (optJSONArray == null) {
            return null;
        }
        int length = optJSONArray.length();
        try {
            String[] strArr = new String[length];
            for (int i2 = 0; i2 < length; i2++) {
                strArr[i2] = optJSONArray.getString(i2);
            }
            return strArr;
        } catch (JSONException e2) {
            return null;
        }
    }

    private static float[] b(JSONArray jSONArray) {
        int length = jSONArray.length();
        try {
            float[] fArr = new float[length];
            for (int i2 = 0; i2 < length; i2++) {
                fArr[i2] = (float) jSONArray.getDouble(i2);
            }
            return fArr;
        } catch (JSONException e2) {
            return null;
        }
    }

    private static float[] b(JSONObject jSONObject, String str) {
        JSONArray optJSONArray = jSONObject.optJSONArray(str);
        if (optJSONArray == null) {
            return null;
        }
        return b(optJSONArray);
    }

    private static float a(JSONObject jSONObject, String str, float f2) {
        return (float) jSONObject.optDouble(str, (double) f2);
    }

    private static int c(JSONArray jSONArray) throws JSONException {
        return Color.argb((int) (jSONArray.getDouble(3) * 255.0d), (int) (jSONArray.getDouble(0) * 255.0d), (int) (jSONArray.getDouble(1) * 255.0d), (int) (jSONArray.getDouble(2) * 255.0d));
    }

    private static int a(JSONObject jSONObject, String str, int i2) {
        if (jSONObject == null || !jSONObject.has(str)) {
            return i2;
        }
        try {
            JSONArray jSONArray = jSONObject.getJSONArray(str);
            return Color.argb((int) (jSONArray.getDouble(3) * 255.0d), (int) (jSONArray.getDouble(0) * 255.0d), (int) (jSONArray.getDouble(1) * 255.0d), (int) (jSONArray.getDouble(2) * 255.0d));
        } catch (Exception e2) {
            return i2;
        }
    }

    private static RectF d(JSONArray jSONArray) throws JSONException {
        float f2 = (float) jSONArray.getDouble(0);
        float f3 = (float) jSONArray.getDouble(1);
        return new RectF(f2, f3, ((float) jSONArray.getDouble(2)) + f2, ((float) jSONArray.getDouble(3)) + f3);
    }

    private static RectF a(JSONObject jSONObject, String str, RectF rectF) {
        if (jSONObject == null || !jSONObject.has(str)) {
            return rectF;
        }
        try {
            return d(jSONObject.getJSONArray(str));
        } catch (JSONException e2) {
            return rectF;
        }
    }

    private static Rect a(JSONObject jSONObject, String str, Rect rect) {
        if (jSONObject == null || !jSONObject.has(str)) {
            return rect;
        }
        try {
            JSONArray jSONArray = jSONObject.getJSONArray(str);
            int i2 = (int) jSONArray.getDouble(0);
            int i3 = (int) jSONArray.getDouble(1);
            return new Rect(i2, i3, ((int) jSONArray.getDouble(2)) + i2, ((int) jSONArray.getDouble(3)) + i3);
        } catch (JSONException e2) {
            return rect;
        }
    }

    private static PointF e(JSONArray jSONArray) throws JSONException {
        return new PointF((float) jSONArray.getDouble(0), (float) jSONArray.getDouble(1));
    }

    private static PointF a(JSONObject jSONObject, String str, PointF pointF) {
        if (jSONObject == null || !jSONObject.has(str)) {
            return pointF;
        }
        try {
            return e(jSONObject.getJSONArray(str));
        } catch (JSONException e2) {
            return pointF;
        }
    }
}
