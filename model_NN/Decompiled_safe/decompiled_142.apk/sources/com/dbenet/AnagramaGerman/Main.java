package com.dbenet.AnagramaGerman;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import java.util.Random;

public class Main extends Activity implements View.OnClickListener {
    public static final long TIME_FOR_NEXT = 2000;
    public static final long TIME_FOR_WORD = 10000;
    public AdView adView;
    public AnagramaApplication aplic;
    public Button btnClear;
    public Button btnNext;
    public String dictionary;
    public LinearLayout layoutParaula;
    public LinearLayout layoutPeces;
    public Runnable mExecNextWord = new Runnable() {
        public void run() {
            Main.this.btnNext.setClickable(true);
            Main.this.nextWord(true);
        }
    };
    /* access modifiers changed from: private */
    public Handler mHandler = new Handler();
    public long mStartTime = 0;
    public Runnable mUpdateTimeTask = new Runnable() {
        public void run() {
            long millis = ((long) Main.this.aplic.time) - (System.currentTimeMillis() - Main.this.mStartTime);
            if (millis < 0) {
                millis = 0;
            }
            int seconds = (int) (millis / 1000);
            int minutes = seconds / 60;
            int seconds2 = seconds % 60;
            if (seconds2 < 10) {
                Main.this.timeLeftTxt.setText(minutes + ":0" + seconds2);
            } else {
                Main.this.timeLeftTxt.setText(minutes + ":" + seconds2);
            }
            if (millis == 0) {
                Main.this.game_ended();
            } else {
                Main.this.mHandler.postDelayed(Main.this.mUpdateTimeTask, 100);
            }
        }
    };
    public LinearLayout mainLayout;
    public String paraulaResultant = "";
    public String paraulaSelected = "";
    public int pecesColocades = 0;
    public int punts = 0;
    public TextView puntsTxt;
    public AdRequest request;
    public int time;
    public TextView timeLeftTxt;
    public int wordLegth;
    public boolean word_winned = false;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        this.aplic = (AnagramaApplication) getApplication();
        this.wordLegth = getWordLenght();
        this.dictionary = getDictionary();
        this.puntsTxt = (TextView) findViewById(R.id.points);
        this.puntsTxt.setText(new StringBuilder().append(this.punts).toString());
        this.timeLeftTxt = (TextView) findViewById(R.id.timeLeft);
        this.layoutPeces = (LinearLayout) findViewById(R.id.layoutPeces);
        this.layoutParaula = (LinearLayout) findViewById(R.id.layoutParaula);
        this.btnClear = (Button) findViewById(R.id.btnClear);
        this.btnNext = (Button) findViewById(R.id.btnNext);
        this.btnClear.setOnClickListener(this);
        this.btnNext.setOnClickListener(this);
        this.aplic.wordList = UtilsLang.getDictionary(this.aplic, getDictionary(), getWordLenght());
        this.adView = new AdView(this, AdSize.BANNER, UtilsLang.ID_AD);
        LinearLayout layout = (LinearLayout) findViewById(R.id.adsLayout);
        this.mainLayout = (LinearLayout) findViewById(R.id.mainLayout);
        layout.addView(this.adView);
        layout.invalidate();
        this.mainLayout.invalidate();
        this.request = new AdRequest();
        this.request.setTesting(false);
        UtilsLang.fill_request_words(this.request);
        this.adView.loadAd(this.request);
        this.aplic.actualitza_pref = true;
        new_game();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.mHandler.removeCallbacks(this.mUpdateTimeTask);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.statistics /*2131361820*/:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(String.valueOf("\n" + getString(R.string.gamesPlayed) + ": " + this.aplic.gamesPlayed) + "\n\n" + getString(R.string.highScoreIs) + ": " + this.aplic.highScore).setCancelable(true);
                AlertDialog alert = builder.create();
                alert.setTitle((int) R.string.statistics);
                alert.show();
                return true;
            case R.id.deleteStatistics /*2131361821*/:
                this.aplic.gamesPlayed = 0;
                this.aplic.highScore = 0;
                return true;
            case R.id.exit /*2131361822*/:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onClick(View arg0) {
        switch (arg0.getId()) {
            case 0:
                click_peca(arg0.getId());
                return;
            case 1:
                click_peca(arg0.getId());
                return;
            case 2:
                click_peca(arg0.getId());
                return;
            case 3:
                click_peca(arg0.getId());
                return;
            case 4:
                click_peca(arg0.getId());
                return;
            case 5:
                click_peca(arg0.getId());
                return;
            case 6:
                click_peca(arg0.getId());
                return;
            case 7:
                click_peca(arg0.getId());
                return;
            case 8:
                click_peca(arg0.getId());
                return;
            case 9:
                click_peca(arg0.getId());
                return;
            case 10:
                click_peca(arg0.getId());
                return;
            case R.id.btnClear /*2131361799*/:
                clearWord();
                return;
            case R.id.btnNext /*2131361800*/:
                nextWord(false);
                return;
            default:
                return;
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        Log.d(getClass().getName(), "back button pressed");
        return true;
    }

    public void nextWord(boolean firstTime) {
        if (firstTime || this.word_winned) {
            this.btnClear.setBackgroundResource(R.drawable.botons);
            this.word_winned = false;
            this.paraulaResultant = "";
            this.pecesColocades = 0;
            this.layoutPeces.removeAllViews();
            this.layoutParaula.removeAllViews();
            this.paraulaSelected = this.aplic.wordList.get(new Random().nextInt(this.aplic.wordList.size() - 1));
            Log.d("dbenet", "Paraula seleccionada: " + this.paraulaSelected);
            String paraulaDesordenada = this.aplic.desordenaParaula(this.paraulaSelected);
            for (int i = 0; i < getWordLenght(); i++) {
                BigTextButton btn = new BigTextButton(getApplicationContext());
                btn.setBackgroundResource(R.drawable.anag_letras);
                btn.setId(i + 0);
                Log.d("dbenet", "Id boto: " + btn.getId());
                btn.setLayoutParams(new LinearLayout.LayoutParams(-1, -1, 1.0f));
                btn.setText(new StringBuilder().append(paraulaDesordenada.charAt(i)).toString());
                btn.setClickable(true);
                btn.setOnClickListener(this);
                this.layoutPeces.addView(btn);
            }
            pinta_paraula_resultant();
            this.btnClear.setClickable(true);
            return;
        }
        this.pecesColocades = getWordLenght();
        this.paraulaResultant = this.paraulaSelected;
        pinta_paraula_resultant();
        this.btnClear.setClickable(false);
        this.btnNext.setClickable(false);
        for (int i2 = 0; i2 < getWordLenght(); i2++) {
            ((BigTextButton) findViewById(i2)).setClickable(false);
        }
        for (int i3 = 0; i3 < getWordLenght(); i3++) {
            ((BigTextButton) findViewById(i3 + 20)).setBackgroundResource(R.drawable.anag_letras_push);
        }
        this.mHandler.postDelayed(this.mExecNextWord, TIME_FOR_NEXT);
    }

    public void pinta_paraula_resultant() {
        this.layoutParaula.removeAllViews();
        for (int i = 0; i < getWordLenght(); i++) {
            BigTextButton btn = new BigTextButton(getApplicationContext());
            btn.setBackgroundResource(R.drawable.anag_letras);
            btn.setId(i + 20);
            btn.setLayoutParams(new LinearLayout.LayoutParams(-1, -1, 1.0f));
            Log.d("dbenet", "Id boto paraula: " + btn.getId());
            char lletra = ' ';
            if (i < this.pecesColocades) {
                lletra = this.paraulaResultant.charAt(i);
            }
            btn.setText(new StringBuilder().append(lletra).toString());
            btn.setClickable(false);
            this.layoutParaula.addView(btn);
        }
    }

    public String getDictionary() {
        return this.aplic.dictionary;
    }

    public int getWordLenght() {
        return this.aplic.wordLength;
    }

    public void click_peca(int id) {
        BigTextButton botoClicat = (BigTextButton) findViewById(id);
        botoClicat.setClickable(false);
        botoClicat.setBackgroundResource(R.drawable.anag_letras_verd);
        this.paraulaResultant = String.valueOf(this.paraulaResultant) + botoClicat.getText();
        this.pecesColocades++;
        pinta_paraula_resultant();
        if (this.pecesColocades != getWordLenght()) {
            return;
        }
        if (this.aplic.wordList.contains(this.paraulaResultant)) {
            word_win();
            return;
        }
        Log.d("dbenet", "Paraula no igual ");
        pinta_resultat_erroni();
    }

    public void clearWord() {
        this.paraulaResultant = "";
        this.pecesColocades = 0;
        for (int i = 0; i < getWordLenght(); i++) {
            BigTextButton botoPeca = (BigTextButton) findViewById(i);
            botoPeca.setClickable(true);
            botoPeca.setBackgroundResource(R.drawable.anag_letras);
        }
        pinta_paraula_resultant();
    }

    public void word_win() {
        Log.d("dbenet", "Paraula guanyada");
        this.word_winned = true;
        this.punts++;
        this.puntsTxt.setText(new StringBuilder().append(this.punts).toString());
        if (!this.aplic.noTime) {
            this.mStartTime += TIME_FOR_WORD;
        }
        this.btnClear.setClickable(false);
        this.btnClear.setBackgroundResource(R.drawable.anag_push_boton);
        for (int i = 0; i < getWordLenght(); i++) {
            ((BigTextButton) findViewById(i + 20)).setBackgroundResource(R.drawable.anag_letras_verd);
        }
    }

    public void pinta_resultat_erroni() {
        for (int i = 0; i < getWordLenght(); i++) {
            ((BigTextButton) findViewById(i + 20)).setBackgroundResource(R.drawable.anag_letras_push);
        }
    }

    public void new_game() {
        this.punts = 0;
        this.aplic.gamesPlayed++;
        this.puntsTxt.setText(new StringBuilder().append(this.punts).toString());
        nextWord(true);
        if (!this.aplic.noTime) {
            this.mStartTime = System.currentTimeMillis();
            this.mHandler.removeCallbacks(this.mUpdateTimeTask);
            this.mHandler.postDelayed(this.mUpdateTimeTask, 100);
        }
    }

    public void game_ended() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        boolean madeHighScore = false;
        if (this.punts > this.aplic.highScore) {
            madeHighScore = true;
            this.aplic.highScore = this.punts;
        }
        String cadena = String.valueOf("\n" + ((Object) getText(R.string.finishedGame))) + "\n\n" + ((Object) getText(R.string.yourScoreIs)) + ":  " + this.punts;
        if (madeHighScore) {
            cadena = String.valueOf(cadena) + "\n\n" + ((Object) getText(R.string.congratulations)) + "\n";
        }
        builder.setMessage(cadena).setCancelable(true).setPositiveButton((int) R.string.newGame, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Main.this.new_game();
            }
        }).setNegativeButton((int) R.string.mainMenu, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Main.this.finish();
            }
        });
        builder.create().show();
    }
}
