package com.dbenet.AnagramaGerman;

import android.app.AlertDialog;
import android.app.Application;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class AnagramaApplication extends Application implements SharedPreferences.OnSharedPreferenceChangeListener {
    public static final boolean DEV_MODE = false;
    public static final int INDEX_BOTONS_PARAULA = 20;
    public static final int INDEX_BOTONS_PECES = 0;
    public int actual_dictionary = -1;
    public int actual_wordLength = -1;
    public boolean actualitza_pref = false;
    public String dictionary;
    public int gamesPlayed = 0;
    public int highScore = 0;
    public boolean noTime = false;
    public int time;
    public int wordLength;
    public List<String> wordList;

    public List<String> readWords(int dictionaryFile, int wordLengthFile) {
        if (dictionaryFile == this.actual_dictionary && this.actual_wordLength == wordLengthFile && this.wordList != null && this.wordList.size() > 0) {
            return this.wordList;
        }
        List<String> dictionary2 = new ArrayList<>();
        try {
            BufferedReader buffreader = new BufferedReader(new InputStreamReader(getResources().openRawResource(dictionaryFile)));
            Log.d("dbenet", "Comença llegir diccionari");
            String line = buffreader.readLine();
            while (line != null && line.length() > 0) {
                if (line.length() == wordLengthFile) {
                    dictionary2.add(line.toUpperCase());
                }
                line = buffreader.readLine();
            }
            Log.d("dbenet", "Acaba llegir diccionari");
            this.actual_dictionary = dictionaryFile;
            this.actual_wordLength = wordLengthFile;
            return dictionary2;
        } catch (Exception e) {
            e.printStackTrace();
            this.actual_dictionary = -1;
            this.actual_wordLength = -1;
            return dictionary2;
        }
    }

    public String desordenaParaula(String paraula) {
        String res = "";
        Random rdn = new Random();
        while (paraula.length() > 0) {
            int index = rdn.nextInt(paraula.length());
            res = String.valueOf(res) + paraula.charAt(index);
            paraula = String.valueOf(paraula.substring(0, index)) + paraula.substring(index + 1);
        }
        return res;
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
    }

    public void onLowMemory() {
        super.onLowMemory();
        Toast.makeText(this, (int) R.string.lowMemory, 1).show();
    }

    public void mostra_estadistiques() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(String.valueOf("\n" + getString(R.string.gamesPlayed) + ": " + this.gamesPlayed) + "\n\n" + getString(R.string.highScoreIs) + ": " + this.highScore).setCancelable(true);
        AlertDialog alert = builder.create();
        alert.setTitle((int) R.string.statistics);
        alert.show();
    }
}
