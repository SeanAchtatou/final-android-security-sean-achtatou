package com.dbenet.AnagramaGerman;

public final class R {

    public static final class array {
        public static final int dictionaryArray = 2131099648;
        public static final int dictionaryArrayValues = 2131099649;
        public static final int timeArray = 2131099652;
        public static final int timeArrayValues = 2131099653;
        public static final int wordLengthArray = 2131099650;
        public static final int wordLengthArrayValues = 2131099651;
    }

    public static final class attr {
    }

    public static final class color {
        public static final int black = 2131165186;
        public static final int blanc_groc = 2131165190;
        public static final int blue_sky = 2131165184;
        public static final int green = 2131165189;
        public static final int red = 2131165188;
        public static final int vert_manu = 2131165185;
        public static final int white = 2131165187;
    }

    public static final class drawable {
        public static final int anag_boton = 2130837504;
        public static final int anag_fondo = 2130837505;
        public static final int anag_letras = 2130837506;
        public static final int anag_letras_push = 2130837507;
        public static final int anag_letras_verd = 2130837508;
        public static final int anag_logo_1 = 2130837509;
        public static final int anag_marcador_1 = 2130837510;
        public static final int anag_push_boton = 2130837511;
        public static final int background_gradient = 2130837512;
        public static final int boton_anag = 2130837513;
        public static final int boton_anag_2 = 2130837514;
        public static final int botons = 2130837515;
        public static final int ic_menu_exit = 2130837516;
    }

    public static final class id {
        public static final int adsLayout = 2131361801;
        public static final int adsLayoutPrincipal = 2131361819;
        public static final int btnClear = 2131361799;
        public static final int btnExit = 2131361818;
        public static final int btnNext = 2131361800;
        public static final int btnNextDiccionari = 2131361808;
        public static final int btnNextTime = 2131361816;
        public static final int btnNextlength = 2131361812;
        public static final int btnPlay = 2131361817;
        public static final int btnPrevDiccionari = 2131361806;
        public static final int btnPrevLength = 2131361810;
        public static final int btnPrevTime = 2131361814;
        public static final int deleteStatistics = 2131361821;
        public static final int diccionariOption = 2131361807;
        public static final int exit = 2131361822;
        public static final int layoutBotons = 2131361798;
        public static final int layoutDiccionari = 2131361805;
        public static final int layoutOptions = 2131361804;
        public static final int layoutParaula = 2131361797;
        public static final int layoutPeces = 2131361796;
        public static final int layoutTamany = 2131361809;
        public static final int layoutTamps = 2131361813;
        public static final int layoutTemps = 2131361793;
        public static final int layoutTitol = 2131361803;
        public static final int lengthOption = 2131361811;
        public static final int mainLayout = 2131361792;
        public static final int points = 2131361795;
        public static final int principalLayout = 2131361802;
        public static final int statistics = 2131361820;
        public static final int timeLeft = 2131361794;
        public static final int timeOption = 2131361815;
    }

    public static final class layout {
        public static final int main = 2130903040;
        public static final int principal = 2130903041;
    }

    public static final class menu {
        public static final int menu = 2131296256;
        public static final int menu_principal = 2131296257;
    }

    public static final class raw {
        public static final int aleman = 2131034112;
        public static final int aleman_facil = 2131034113;
        public static final int sowpods = 2131034114;
    }

    public static final class string {
        public static final int about = 2131230739;
        public static final int app_name = 2131230721;
        public static final int bestScore = 2131230741;
        public static final int changedPrefs = 2131230744;
        public static final int clear = 2131230723;
        public static final int congratulations = 2131230743;
        public static final int deleteStatistics = 2131230738;
        public static final int dictionary = 2131230735;
        public static final int exit = 2131230727;
        public static final int exit_game = 2131230747;
        public static final int finishedGame = 2131230728;
        public static final int gamesPlayed = 2131230740;
        public static final int hello = 2131230720;
        public static final int highScoreIs = 2131230730;
        public static final int loadingDictionary = 2131230731;
        public static final int lowMemory = 2131230732;
        public static final int mainMenu = 2131230748;
        public static final int newGame = 2131230726;
        public static final int next = 2131230724;
        public static final int ok = 2131230745;
        public static final int play = 2131230746;
        public static final int points = 2131230722;
        public static final int preferences = 2131230736;
        public static final int sound = 2131230742;
        public static final int statistics = 2131230737;
        public static final int timeLeft = 2131230725;
        public static final int wordLength = 2131230733;
        public static final int wordLengthInfo = 2131230734;
        public static final int yourScoreIs = 2131230729;
    }

    public static final class xml {
        public static final int preferences = 2130968576;
    }
}
