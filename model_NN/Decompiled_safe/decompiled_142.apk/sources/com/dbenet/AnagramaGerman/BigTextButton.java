package com.dbenet.AnagramaGerman;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.ImageButton;

public class BigTextButton extends ImageButton {
    String mText = "";
    int mTextBaseline;
    Paint mTextPaint;
    int mViewHeight;
    int mViewWidth;

    public BigTextButton(Context context) {
        super(context);
        init();
    }

    public BigTextButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        parseAttrs(attrs);
        init();
    }

    public BigTextButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        parseAttrs(attrs);
        init();
    }

    private void parseAttrs(AttributeSet attrs) {
        for (int i = 0; i < attrs.getAttributeCount(); i++) {
            if (attrs.getAttributeName(i).equalsIgnoreCase("text")) {
                this.mText = attrs.getAttributeValue(i);
            }
        }
    }

    private void init() {
        this.mTextPaint = new Paint();
        this.mTextPaint.setColor(-16777216);
        this.mTextPaint.setTypeface(Typeface.SERIF);
        this.mTextPaint.setTextAlign(Paint.Align.CENTER);
        this.mTextPaint.setAntiAlias(true);
    }

    /* access modifiers changed from: package-private */
    public void adjustTextScale() {
        this.mTextPaint.setTextScaleX(1.0f);
        Rect bounds = new Rect();
        this.mTextPaint.getTextBounds(this.mText, 0, this.mText.length(), bounds);
        int w = bounds.right - bounds.left;
        this.mTextBaseline = bounds.bottom + ((this.mViewHeight - (bounds.bottom - bounds.top)) / 2);
        this.mTextPaint.setTextScaleX(((float) ((this.mViewWidth - getPaddingLeft()) - getPaddingRight())) / ((float) w));
    }

    /* access modifiers changed from: package-private */
    public void adjustTextSize() {
        this.mTextPaint.setTextSize(100.0f);
        this.mTextPaint.setTextScaleX(1.0f);
        Rect bounds = new Rect();
        this.mTextPaint.getTextBounds(this.mText, 0, this.mText.length(), bounds);
        this.mTextPaint.setTextSize(((((float) this.mViewHeight) * 0.7f) / ((float) (bounds.bottom - bounds.top))) * 100.0f);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        this.mViewWidth = w;
        this.mViewHeight = h;
        adjustTextSize();
        adjustTextScale();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawText(this.mText, (float) (this.mViewWidth / 2), (float) (this.mViewHeight - this.mTextBaseline), this.mTextPaint);
    }

    public String getText() {
        return this.mText;
    }

    public void setText(String mText2) {
        this.mText = mText2;
    }

    public void resize() {
        adjustTextSize();
        adjustTextScale();
    }
}
