package com.dbenet.AnagramaGerman;

import com.google.ads.AdRequest;
import java.util.List;

public class UtilsLang {
    public static final String ID_AD = "a14e08d89b47f9e";

    public static String getDefaultDictionary() {
        return "aleman";
    }

    public static List<String> getDictionary(AnagramaApplication aplic, String dict, int wordLength) {
        if (dict.equals("aleman")) {
            return aplic.readWords(R.raw.aleman, wordLength);
        }
        if (dict.equals("aleman_facil")) {
            return aplic.readWords(R.raw.aleman_facil, wordLength);
        }
        return aplic.readWords(R.raw.sowpods, wordLength);
    }

    public static void fill_request_words(AdRequest request) {
        request.addKeyword("games");
        request.addKeyword("game");
        request.addKeyword("gamming");
        request.addKeyword("online");
        request.addKeyword("anagram");
        request.addKeyword("anagramm");
        request.addKeyword("word");
        request.addKeyword("wort");
        request.addKeyword("dictionary");
        request.addKeyword("wörterbuch");
        request.addKeyword("letter");
        request.addKeyword("brief");
        request.addKeyword("spiele");
        request.addKeyword("spiel");
        request.addKeyword("android");
    }
}
