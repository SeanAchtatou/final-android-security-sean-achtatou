package com.dbenet.AnagramaGerman;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;

public class Principal extends Activity implements View.OnClickListener {
    public static final int HANDLE_INVALIDA_LAYOUT = 0;
    public AdView adView;
    public AnagramaApplication aplic;
    public Button btnExit;
    public Button btnPlay;
    public BigTextButton diccionariView;
    public int indexDictionary = 0;
    public int indexLength = 0;
    public int indexTime = 0;
    public LinearLayout layoutTitol;
    public Handler mHandler;
    public ImageButton nextDictionary;
    public ImageButton nextLength;
    public ImageButton nextTime;
    public ImageButton prevDictionary;
    public ImageButton prevLength;
    public ImageButton prevTime;
    public LinearLayout principalLayout;
    public AdRequest request;
    public BigTextButton timeView;
    public BigTextButton wordLengthView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.principal);
        this.aplic = (AnagramaApplication) getApplication();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this.aplic);
        prefs.registerOnSharedPreferenceChangeListener(this.aplic);
        this.aplic.gamesPlayed = prefs.getInt("gamesPlayed", 0);
        this.aplic.highScore = prefs.getInt("highScore", 0);
        this.indexDictionary = prefs.getInt("indexDictionary", 0);
        this.indexLength = prefs.getInt("indexLength", 1);
        this.indexTime = prefs.getInt("indexTime", 0);
        this.prevDictionary = (ImageButton) findViewById(R.id.btnPrevDiccionari);
        this.nextDictionary = (ImageButton) findViewById(R.id.btnNextDiccionari);
        this.prevDictionary.setOnClickListener(this);
        this.nextDictionary.setOnClickListener(this);
        this.prevLength = (ImageButton) findViewById(R.id.btnPrevLength);
        this.nextLength = (ImageButton) findViewById(R.id.btnNextlength);
        this.prevLength.setOnClickListener(this);
        this.nextLength.setOnClickListener(this);
        this.prevTime = (ImageButton) findViewById(R.id.btnPrevTime);
        this.nextTime = (ImageButton) findViewById(R.id.btnNextTime);
        this.prevTime.setOnClickListener(this);
        this.nextTime.setOnClickListener(this);
        this.btnPlay = (Button) findViewById(R.id.btnPlay);
        this.btnPlay.setOnClickListener(this);
        this.btnExit = (Button) findViewById(R.id.btnExit);
        this.btnExit.setOnClickListener(this);
        this.layoutTitol = (LinearLayout) findViewById(R.id.layoutTitol);
        this.diccionariView = (BigTextButton) findViewById(R.id.diccionariOption);
        this.wordLengthView = (BigTextButton) findViewById(R.id.lengthOption);
        this.timeView = (BigTextButton) findViewById(R.id.timeOption);
        this.adView = new AdView(this, AdSize.BANNER, UtilsLang.ID_AD);
        LinearLayout layout = (LinearLayout) findViewById(R.id.adsLayoutPrincipal);
        this.principalLayout = (LinearLayout) findViewById(R.id.principalLayout);
        layout.addView(this.adView);
        layout.invalidate();
        this.principalLayout.invalidate();
        this.request = new AdRequest();
        this.request.setTesting(false);
        UtilsLang.fill_request_words(this.request);
        this.adView.loadAd(this.request);
        this.mHandler = new Handler() {
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case 0:
                        Principal.this.principalLayout.invalidate();
                        return;
                    default:
                        return;
                }
            }
        };
        pinta_titol();
        pinta_diccionari();
        pinta_wordLength();
        pinta_temps();
    }

    public void onClick(View arg0) {
        switch (arg0.getId()) {
            case R.id.btnPrevDiccionari /*2131361806*/:
                prevDictionary();
                return;
            case R.id.diccionariOption /*2131361807*/:
            case R.id.layoutTamany /*2131361809*/:
            case R.id.lengthOption /*2131361811*/:
            case R.id.layoutTamps /*2131361813*/:
            case R.id.timeOption /*2131361815*/:
            default:
                return;
            case R.id.btnNextDiccionari /*2131361808*/:
                nextDictionary();
                return;
            case R.id.btnPrevLength /*2131361810*/:
                prevLength();
                return;
            case R.id.btnNextlength /*2131361812*/:
                nextLength();
                return;
            case R.id.btnPrevTime /*2131361814*/:
                prevTime();
                return;
            case R.id.btnNextTime /*2131361816*/:
                nextTime();
                return;
            case R.id.btnPlay /*2131361817*/:
                setGameParams();
                Toast.makeText(this, (int) R.string.loadingDictionary, 0).show();
                startActivity(new Intent(this.aplic, Main.class));
                return;
            case R.id.btnExit /*2131361818*/:
                finish();
                return;
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        Log.d(getClass().getName(), "back button pressed");
        return true;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_principal, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.statistics /*2131361820*/:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(String.valueOf("\n" + getString(R.string.gamesPlayed) + ": " + this.aplic.gamesPlayed) + "\n\n" + getString(R.string.highScoreIs) + ": " + this.aplic.highScore).setCancelable(true);
                AlertDialog alert = builder.create();
                alert.setTitle((int) R.string.statistics);
                alert.show();
                return true;
            case R.id.deleteStatistics /*2131361821*/:
                this.aplic.gamesPlayed = 0;
                this.aplic.highScore = 0;
                return true;
            case R.id.exit /*2131361822*/:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(this.aplic).edit();
        editor.putInt("gamesPlayed", this.aplic.gamesPlayed);
        editor.putInt("highScore", this.aplic.highScore);
        editor.putInt("indexDictionary", this.indexDictionary);
        editor.putInt("indexLength", this.indexLength);
        editor.putInt("indexTime", this.indexTime);
        editor.commit();
    }

    public void prevDictionary() {
        String[] dictionaries = getResources().getStringArray(R.array.dictionaryArrayValues);
        this.indexDictionary--;
        if (this.indexDictionary < 0) {
            this.indexDictionary = dictionaries.length - 1;
        }
        pinta_diccionari();
    }

    public void nextDictionary() {
        String[] dictionaries = getResources().getStringArray(R.array.dictionaryArrayValues);
        this.indexDictionary++;
        if (this.indexDictionary >= dictionaries.length) {
            this.indexDictionary = 0;
        }
        pinta_diccionari();
    }

    public void prevLength() {
        String[] wordLengths = getResources().getStringArray(R.array.wordLengthArrayValues);
        this.indexLength--;
        if (this.indexLength < 0) {
            this.indexLength = wordLengths.length - 1;
        }
        pinta_wordLength();
    }

    public void nextLength() {
        String[] wordLengths = getResources().getStringArray(R.array.wordLengthArrayValues);
        this.indexLength++;
        if (this.indexLength >= wordLengths.length) {
            this.indexLength = 0;
        }
        pinta_wordLength();
    }

    public void prevTime() {
        String[] time = getResources().getStringArray(R.array.timeArrayValues);
        this.indexTime--;
        if (this.indexTime < 0) {
            this.indexTime = time.length - 1;
        }
        pinta_temps();
    }

    public void nextTime() {
        String[] time = getResources().getStringArray(R.array.timeArrayValues);
        this.indexTime++;
        if (this.indexTime >= time.length) {
            this.indexTime = 0;
        }
        pinta_temps();
    }

    public void setGameParams() {
        String[] dictionaries = getResources().getStringArray(R.array.dictionaryArrayValues);
        this.aplic.dictionary = dictionaries[this.indexDictionary];
        String[] wordLengths = getResources().getStringArray(R.array.wordLengthArrayValues);
        this.aplic.wordLength = Integer.parseInt(wordLengths[this.indexLength]);
        String[] time = getResources().getStringArray(R.array.timeArrayValues);
        this.aplic.time = Integer.parseInt(time[this.indexTime]);
        if (this.aplic.time == 0) {
            this.aplic.noTime = true;
        } else {
            this.aplic.noTime = false;
        }
    }

    public void pinta_diccionari() {
        String[] dictionariesValues = getResources().getStringArray(R.array.dictionaryArray);
        if (this.indexDictionary > dictionariesValues.length - 1) {
            this.indexDictionary = 0;
        }
        this.diccionariView.setText(dictionariesValues[this.indexDictionary]);
        this.diccionariView.resize();
        this.mHandler.sendEmptyMessage(0);
    }

    public void pinta_wordLength() {
        String[] wordLengthValues = getResources().getStringArray(R.array.wordLengthArray);
        if (this.indexLength > wordLengthValues.length - 1) {
            this.indexLength = 0;
        }
        this.wordLengthView.setText(wordLengthValues[this.indexLength]);
        this.wordLengthView.resize();
        this.mHandler.sendEmptyMessage(0);
    }

    public void pinta_temps() {
        String[] timeValues = getResources().getStringArray(R.array.timeArray);
        if (this.indexTime > timeValues.length - 1) {
            this.indexTime = 0;
        }
        this.timeView.setText(timeValues[this.indexTime]);
        this.timeView.resize();
        this.mHandler.sendEmptyMessage(0);
    }

    public void pinta_titol() {
        this.layoutTitol.removeAllViews();
        String titol = getString(R.string.app_name).toUpperCase();
        for (int i = 0; i < titol.length(); i++) {
            BigTextButton btn = new BigTextButton(getApplicationContext());
            btn.setBackgroundResource(R.drawable.anag_letras);
            btn.setLayoutParams(new LinearLayout.LayoutParams(-1, -1, 1.0f));
            btn.setText(new StringBuilder().append(titol.charAt(i)).toString());
            btn.setClickable(false);
            this.layoutTitol.addView(btn);
        }
    }
}
