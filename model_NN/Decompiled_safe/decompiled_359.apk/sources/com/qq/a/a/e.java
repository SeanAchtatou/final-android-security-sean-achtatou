package com.qq.a.a;

import android.net.Uri;
import com.tencent.open.SocialConstants;

/* compiled from: ProGuard */
public final class e {
    public final String A = "visibility";
    public final String B = "transparency";
    public final String C = "hasAlarm";

    /* renamed from: a  reason: collision with root package name */
    public final Uri f260a = Uri.parse("content://calendar/events");
    public final Uri b = Uri.parse("content://com.android.calendar/events");
    public final Uri c = Uri.parse("content://calendar/calendars");
    public final Uri d = Uri.parse("content://com.android.calendar/calendars");
    public final Uri e = Uri.parse("content://calendar/reminder");
    public final Uri f = Uri.parse("content://com.android.calendar/reminder");
    public final String g = "_id";
    public final String h = SocialConstants.PARAM_URL;
    public final String i = "name";
    public final String j = "displayName";
    public final String k = "hidden";
    public final String l = "selected";
    public final String m = "timezone";
    public final String n = "_id";
    public final String o = "calendar_id";
    public final String p = "htmlUri";
    public final String q = "commentsUri";
    public final String r = "title";
    public final String s = "eventLocation";
    public final String t = SocialConstants.PARAM_COMMENT;
    public final String u = "eventStatus";
    public final String v = "dtstart";
    public final String w = "dtend";
    public final String x = "eventTimezone";
    public final String y = "duration";
    public final String z = "allDay";
}
