package com.qq.jce.wup;

import com.qq.taf.jce.JceInputStream;
import com.qq.taf.jce.JceOutputStream;
import com.qq.taf.jce.JceUtil;
import com.tencent.connect.common.Constants;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class UniAttribute extends OldUniAttribute {
    JceInputStream _is = new JceInputStream();
    protected HashMap<String, byte[]> _newData = null;
    private HashMap<String, Object> cachedData = new HashMap<>();

    public /* bridge */ /* synthetic */ String getEncodeName() {
        return super.getEncodeName();
    }

    public /* bridge */ /* synthetic */ void setEncodeName(String x0) {
        super.setEncodeName(x0);
    }

    public void useVersion3() {
        this._newData = new HashMap<>();
    }

    public void clearCacheData() {
        this.cachedData.clear();
    }

    public Set<String> getKeySet() {
        if (this._newData != null) {
            return Collections.unmodifiableSet(this._newData.keySet());
        }
        return Collections.unmodifiableSet(this._data.keySet());
    }

    public boolean isEmpty() {
        if (this._newData != null) {
            return this._newData.isEmpty();
        }
        return this._data.isEmpty();
    }

    public int size() {
        if (this._newData != null) {
            return this._newData.size();
        }
        return this._data.size();
    }

    public boolean containsKey(String key) {
        if (this._newData != null) {
            return this._newData.containsKey(key);
        }
        return this._data.containsKey(key);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceOutputStream.write(java.lang.Object, int):void
     arg types: [T, int]
     candidates:
      com.qq.taf.jce.JceOutputStream.write(byte, int):void
      com.qq.taf.jce.JceOutputStream.write(double, int):void
      com.qq.taf.jce.JceOutputStream.write(float, int):void
      com.qq.taf.jce.JceOutputStream.write(int, int):void
      com.qq.taf.jce.JceOutputStream.write(long, int):void
      com.qq.taf.jce.JceOutputStream.write(com.qq.taf.jce.JceStruct, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Boolean, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Byte, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Double, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Float, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Integer, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Long, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Short, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.String, int):void
      com.qq.taf.jce.JceOutputStream.write(java.util.Collection, int):void
      com.qq.taf.jce.JceOutputStream.write(java.util.Map, int):void
      com.qq.taf.jce.JceOutputStream.write(short, int):void
      com.qq.taf.jce.JceOutputStream.write(boolean, int):void
      com.qq.taf.jce.JceOutputStream.write(byte[], int):void
      com.qq.taf.jce.JceOutputStream.write(double[], int):void
      com.qq.taf.jce.JceOutputStream.write(float[], int):void
      com.qq.taf.jce.JceOutputStream.write(int[], int):void
      com.qq.taf.jce.JceOutputStream.write(long[], int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Object[], int):void
      com.qq.taf.jce.JceOutputStream.write(short[], int):void
      com.qq.taf.jce.JceOutputStream.write(boolean[], int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Object, int):void */
    public <T> void put(String name, T t) {
        if (this._newData == null) {
            super.put(name, t);
        } else if (name == null) {
            throw new IllegalArgumentException("put key can not is null");
        } else if (t == null) {
            throw new IllegalArgumentException("put value can not is null");
        } else if (t instanceof Set) {
            throw new IllegalArgumentException("can not support Set");
        } else {
            JceOutputStream _out = new JceOutputStream();
            _out.setServerEncoding(this.encodeName);
            _out.write((Object) t, 0);
            this._newData.put(name, JceUtil.getJceBufArray(_out.getByteBuffer()));
        }
    }

    public <T> T getJceStruct(String name, T proxy) throws ObjectCreateException {
        if (!this._newData.containsKey(name)) {
            return null;
        }
        if (this.cachedData.containsKey(name)) {
            return this.cachedData.get(name);
        }
        try {
            Object o = decodeData(this._newData.get(name), proxy);
            if (o == null) {
                return o;
            }
            saveDataCache(name, o);
            return o;
        } catch (Exception ex) {
            throw new ObjectCreateException(ex);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
     arg types: [T, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object */
    public <T> T getByClass(String name, T proxy) throws ObjectCreateException {
        if (this._newData != null) {
            if (!this._newData.containsKey(name)) {
                return null;
            }
            if (this.cachedData.containsKey(name)) {
                return this.cachedData.get(name);
            }
            try {
                Object o = decodeData(this._newData.get(name), proxy);
                if (o == null) {
                    return o;
                }
                saveDataCache(name, o);
                return o;
            } catch (Exception ex) {
                throw new ObjectCreateException(ex);
            }
        } else if (!this._data.containsKey(name)) {
            return null;
        } else {
            if (this.cachedData.containsKey(name)) {
                return this.cachedData.get(name);
            }
            byte[] data = new byte[0];
            Iterator i$ = ((HashMap) this._data.get(name)).entrySet().iterator();
            if (i$.hasNext()) {
                Map.Entry<String, byte[]> e = (Map.Entry) i$.next();
                String className = e.getKey();
                data = e.getValue();
            }
            try {
                this._is.wrap(data);
                this._is.setServerEncoding(this.encodeName);
                Object o2 = this._is.read((Object) proxy, 0, true);
                saveDataCache(name, o2);
                return o2;
            } catch (Exception ex2) {
                throw new ObjectCreateException(ex2);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
     arg types: [T, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object */
    public <T> T getByClass(String name, T proxy, T defaultValue) throws ObjectCreateException {
        if (this._newData != null) {
            if (!this._newData.containsKey(name)) {
                return defaultValue;
            }
            if (this.cachedData.containsKey(name)) {
                return this.cachedData.get(name);
            }
            try {
                T o = decodeData(this._newData.get(name), proxy);
                if (o != null) {
                    saveDataCache(name, o);
                }
                return o;
            } catch (Exception ex) {
                throw new ObjectCreateException(ex);
            }
        } else if (!this._data.containsKey(name)) {
            return defaultValue;
        } else {
            if (this.cachedData.containsKey(name)) {
                return this.cachedData.get(name);
            }
            byte[] data = new byte[0];
            Iterator i$ = ((HashMap) this._data.get(name)).entrySet().iterator();
            if (i$.hasNext()) {
                Map.Entry<String, byte[]> e = (Map.Entry) i$.next();
                String className = e.getKey();
                data = e.getValue();
            }
            try {
                this._is.wrap(data);
                this._is.setServerEncoding(this.encodeName);
                T o2 = this._is.read((Object) proxy, 0, true);
                saveDataCache(name, o2);
                return o2;
            } catch (Exception ex2) {
                throw new ObjectCreateException(ex2);
            }
        }
    }

    public <T> T get(String name, T proxy, Object defaultValue) {
        return !this._newData.containsKey(name) ? defaultValue : getByClass(name, proxy);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
     arg types: [java.lang.Object, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object */
    private Object decodeData(byte[] data, Object proxy) {
        this._is.wrap(data);
        this._is.setServerEncoding(this.encodeName);
        return this._is.read(proxy, 0, true);
    }

    private void saveDataCache(String name, Object o) {
        this.cachedData.put(name, o);
    }

    public <T> T getJceStruct(String name) throws ObjectCreateException {
        return getJceStruct(name, true, null);
    }

    public <T> T getJceStruct(String name, boolean initialize, ClassLoader loader) throws ObjectCreateException {
        if (this._newData == null) {
            return super.getJceStruct(name, initialize, loader);
        }
        throw new RuntimeException("data is encoded by new version, please use getJceStruct(String name,T proxy)");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.jce.wup.UniAttribute.get(java.lang.String, boolean, java.lang.ClassLoader):T
     arg types: [java.lang.String, int, ?[OBJECT, ARRAY]]
     candidates:
      com.qq.jce.wup.UniAttribute.get(java.lang.String, java.lang.Object, java.lang.Object):T
      com.qq.jce.wup.UniAttribute.get(java.lang.String, boolean, java.lang.ClassLoader):T */
    public <T> T get(String name) throws ObjectCreateException {
        return get(name, true, (ClassLoader) null);
    }

    public <T> T get(String name, boolean initialize, ClassLoader loader) throws ObjectCreateException {
        if (this._newData == null) {
            return super.get(name, initialize, loader);
        }
        throw new RuntimeException("data is encoded by new version, please use getByClass(String name, T proxy)");
    }

    public <T> T get(String name, Object defaultValue) {
        return get(name, defaultValue, true, null);
    }

    public <T> T get(String name, Object defaultValue, boolean initialize, ClassLoader loader) {
        if (this._newData == null) {
            return super.get(name, defaultValue, initialize, loader);
        }
        throw new RuntimeException("data is encoded by new version, please use getByClass(String name, T proxy, Object defaultValue)");
    }

    public <T> T remove(String name) throws ObjectCreateException {
        return remove(name, true, null);
    }

    public <T> T remove(String name, boolean initialize, ClassLoader loader) throws ObjectCreateException {
        if (this._newData == null) {
            return super.remove(name, initialize, loader);
        }
        if (!this._newData.containsKey(name)) {
            return null;
        }
        this._newData.remove(name);
        return null;
    }

    public <T> T remove(String name, T proxy) throws ObjectCreateException {
        if (!this._newData.containsKey(name)) {
            return null;
        }
        if (proxy != null) {
            return decodeData(this._newData.remove(name), proxy);
        }
        this._newData.remove(name);
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceOutputStream.write(java.util.Map, int):void
     arg types: [java.util.HashMap<java.lang.String, byte[]>, int]
     candidates:
      com.qq.taf.jce.JceOutputStream.write(byte, int):void
      com.qq.taf.jce.JceOutputStream.write(double, int):void
      com.qq.taf.jce.JceOutputStream.write(float, int):void
      com.qq.taf.jce.JceOutputStream.write(int, int):void
      com.qq.taf.jce.JceOutputStream.write(long, int):void
      com.qq.taf.jce.JceOutputStream.write(com.qq.taf.jce.JceStruct, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Boolean, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Byte, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Double, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Float, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Integer, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Long, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Object, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Short, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.String, int):void
      com.qq.taf.jce.JceOutputStream.write(java.util.Collection, int):void
      com.qq.taf.jce.JceOutputStream.write(short, int):void
      com.qq.taf.jce.JceOutputStream.write(boolean, int):void
      com.qq.taf.jce.JceOutputStream.write(byte[], int):void
      com.qq.taf.jce.JceOutputStream.write(double[], int):void
      com.qq.taf.jce.JceOutputStream.write(float[], int):void
      com.qq.taf.jce.JceOutputStream.write(int[], int):void
      com.qq.taf.jce.JceOutputStream.write(long[], int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Object[], int):void
      com.qq.taf.jce.JceOutputStream.write(short[], int):void
      com.qq.taf.jce.JceOutputStream.write(boolean[], int):void
      com.qq.taf.jce.JceOutputStream.write(java.util.Map, int):void */
    public byte[] encode() {
        if (this._newData == null) {
            return super.encode();
        }
        JceOutputStream _os = new JceOutputStream(0);
        _os.setServerEncoding(this.encodeName);
        _os.write((Map) this._newData, 0);
        return JceUtil.getJceBufArray(_os.getByteBuffer());
    }

    public void decodeVersion3(byte[] buffer) {
        this._is.wrap(buffer);
        this._is.setServerEncoding(this.encodeName);
        HashMap<String, byte[]> _tempdata = new HashMap<>(1);
        _tempdata.put(Constants.STR_EMPTY, new byte[0]);
        this._newData = this._is.readMap(_tempdata, 0, false);
    }

    public void decodeVersion2(byte[] buffer) {
        super.decode(buffer);
    }

    public void decode(byte[] buffer) {
        try {
            super.decode(buffer);
        } catch (Exception e) {
            this._is.wrap(buffer);
            this._is.setServerEncoding(this.encodeName);
            HashMap<String, byte[]> _tempdata = new HashMap<>(1);
            _tempdata.put(Constants.STR_EMPTY, new byte[0]);
            this._newData = this._is.readMap(_tempdata, 0, false);
        }
    }
}
