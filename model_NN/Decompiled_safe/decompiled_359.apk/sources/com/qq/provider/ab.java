package com.qq.provider;

import android.app.KeyguardManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.os.PowerManager;
import android.os.Process;
import android.os.RemoteException;
import android.os.StatFs;
import android.os.SystemProperties;
import android.provider.MediaStore;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import com.qq.AppService.AppService;
import com.qq.AppService.AuthorReceiver;
import com.qq.AppService.IPCService;
import com.qq.AppService.OpenGLActivity;
import com.qq.AppService.PowerReceiver;
import com.qq.AppService.UsbPage;
import com.qq.AppService.ad;
import com.qq.AppService.s;
import com.qq.b.a;
import com.qq.c.d;
import com.qq.d.g.o;
import com.qq.g.c;
import com.qq.ndk.Native;
import com.tencent.connect.common.Constants;
import com.tencent.smtt.sdk.WebView;
import com.tencent.wcs.c.b;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Map;

/* compiled from: ProGuard */
public final class ab {

    /* renamed from: a  reason: collision with root package name */
    private static ab f320a = null;
    private static String c = null;
    private int b = 0;
    private int d = 0;
    private int e = 0;
    /* access modifiers changed from: private */
    public volatile int f = -1;

    public static ab a() {
        if (f320a != null) {
            return f320a;
        }
        f320a = new ab();
        return f320a;
    }

    private ab() {
    }

    public static String a(Context context) {
        if (c != null || context == null) {
            return c;
        }
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences("com.qq.connect", 0);
        if (sharedPreferences != null) {
            c = sharedPreferences.getString("USBDeviceID", null);
        }
        return c;
    }

    public void a(Context context, c cVar) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        String simSerialNumber = telephonyManager.getSimSerialNumber();
        String deviceId = telephonyManager.getDeviceId();
        String subscriberId = telephonyManager.getSubscriberId();
        String line1Number = telephonyManager.getLine1Number();
        String string = Settings.Secure.getString(context.getContentResolver(), "android_id");
        String b2 = b(context);
        ArrayList arrayList = new ArrayList();
        arrayList.add(s.a(simSerialNumber));
        arrayList.add(s.a(deviceId));
        arrayList.add(s.a(subscriberId));
        arrayList.add(s.a(line1Number));
        arrayList.add(s.a(string));
        arrayList.add(s.a(b2));
        cVar.a(arrayList);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0081, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0094, code lost:
        r0 = r1;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0081 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:3:0x0007] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void b(android.content.Context r6, com.qq.g.c r7) {
        /*
            r5 = this;
            int r0 = r5.b
            if (r0 != 0) goto L_0x003e
            r1 = 0
            java.lang.String r0 = "power"
            java.lang.Object r0 = r6.getSystemService(r0)     // Catch:{ Exception -> 0x006e, all -> 0x0081 }
            android.os.PowerManager r0 = (android.os.PowerManager) r0     // Catch:{ Exception -> 0x006e, all -> 0x0081 }
            r2 = 805306394(0x3000001a, float:4.6566273E-10)
            java.lang.String r3 = "lock"
            android.os.PowerManager$WakeLock r1 = r0.newWakeLock(r2, r3)     // Catch:{ Exception -> 0x006e, all -> 0x0081 }
            r1.acquire()     // Catch:{ Exception -> 0x0093, all -> 0x0081 }
            java.lang.String r0 = "window"
            java.lang.Object r0 = r6.getSystemService(r0)     // Catch:{ Exception -> 0x0093, all -> 0x0081 }
            android.view.WindowManager r0 = (android.view.WindowManager) r0     // Catch:{ Exception -> 0x0093, all -> 0x0081 }
            android.view.Display r0 = r0.getDefaultDisplay()     // Catch:{ Exception -> 0x0093, all -> 0x0081 }
            android.util.DisplayMetrics r2 = new android.util.DisplayMetrics     // Catch:{ Exception -> 0x0093, all -> 0x0081 }
            r2.<init>()     // Catch:{ Exception -> 0x0093, all -> 0x0081 }
            r0.getMetrics(r2)     // Catch:{ Exception -> 0x0093, all -> 0x0081 }
            int r0 = com.qq.provider.k.a(r2)     // Catch:{ Exception -> 0x0093, all -> 0x0081 }
            r5.b = r0     // Catch:{ Exception -> 0x0093, all -> 0x0081 }
            if (r1 == 0) goto L_0x003e
            boolean r0 = r1.isHeld()
            if (r0 == 0) goto L_0x003e
            r1.release()
        L_0x003e:
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            int r1 = r5.b
            byte[] r1 = com.qq.AppService.s.a(r1)
            r0.add(r1)
            java.lang.String r1 = "com.qq.connect"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "dpi "
            java.lang.StringBuilder r2 = r2.append(r3)
            int r3 = r5.b
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            android.util.Log.d(r1, r2)
            r7.a(r0)
            r0 = 0
            r7.a(r0)
        L_0x006d:
            return
        L_0x006e:
            r0 = move-exception
            r0 = r1
        L_0x0070:
            r1 = 8
            r7.a(r1)     // Catch:{ all -> 0x008e }
            if (r0 == 0) goto L_0x006d
            boolean r1 = r0.isHeld()
            if (r1 == 0) goto L_0x006d
            r0.release()
            goto L_0x006d
        L_0x0081:
            r0 = move-exception
        L_0x0082:
            if (r1 == 0) goto L_0x008d
            boolean r2 = r1.isHeld()
            if (r2 == 0) goto L_0x008d
            r1.release()
        L_0x008d:
            throw r0
        L_0x008e:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x0082
        L_0x0093:
            r0 = move-exception
            r0 = r1
            goto L_0x0070
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qq.provider.ab.b(android.content.Context, com.qq.g.c):void");
    }

    public void a(Context context, c cVar, Socket socket) {
        String str;
        String str2;
        String str3;
        String str4 = null;
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        if (cVar.e() >= 3) {
            str = cVar.j();
            str2 = cVar.j();
        } else {
            str = null;
            str2 = null;
        }
        if (cVar.e() >= 5) {
            str3 = cVar.j();
            str4 = cVar.j();
        } else {
            str3 = null;
        }
        Log.d("com.qq.connect", " switchservice " + h);
        b.a("SysInfoManager switchService type " + h + " pc_name " + str + " pc_guid " + str2 + " uin " + str3 + " nick_name " + str4);
        AppService.a(h, str, str2, str3, str4);
        if (h == 0) {
            try {
                ad.a(context, ad.d(context));
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            if (IPCService.f203a != null && IPCService.f203a.b != null) {
                try {
                    IPCService.f203a.b.b();
                } catch (RemoteException e3) {
                    e3.printStackTrace();
                }
            }
        } else {
            long currentTimeMillis = System.currentTimeMillis();
            a.a(context);
            Log.d("com.qq.connect", "aurora_init time spent " + (System.currentTimeMillis() - currentTimeMillis));
            IPCService g = AppService.g();
            if (h == 1) {
                if (!(g == null || g.b == null)) {
                    try {
                        g.b.a(1);
                    } catch (RemoteException e4) {
                        e4.printStackTrace();
                    }
                }
            } else if (h == 2) {
                if (!(g == null || g.b == null)) {
                    try {
                        g.b.a(2);
                    } catch (RemoteException e5) {
                        e5.printStackTrace();
                    }
                }
            } else if (!(h != 3 || g == null || g.b == null)) {
                try {
                    g.b.a(3);
                } catch (RemoteException e6) {
                    e6.printStackTrace();
                }
            }
            if (h == 1) {
                AppService.c(true);
                cVar.a(0);
            } else if (h == 2) {
                UsbPage.a();
                AppService.b(true);
                cVar.a(0);
            } else if (h == 3) {
                UsbPage.a();
                AppService.a(true);
                cVar.a(0);
            } else if (h == 4) {
                h.c().b(context, cVar);
                AppService.k();
                try {
                    ad.a(context, ad.d(context));
                } catch (Exception e7) {
                    e7.printStackTrace();
                }
                cVar.a(0);
            } else {
                cVar.a(1);
            }
        }
    }

    public void a(Context context, Socket socket) {
        try {
            socket.close();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        Intent intent = new Intent();
        intent.setClass(context, AppService.class);
        context.stopService(intent);
    }

    public void c(Context context, c cVar) {
        Runtime.getRuntime().gc();
    }

    public void d(Context context, c cVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(s.a(Build.MODEL));
        arrayList.add(s.a(Build.VERSION.RELEASE));
        arrayList.add(s.a(Build.BRAND));
        arrayList.add(s.a(Build.VERSION.SDK));
        cVar.a(arrayList);
        cVar.a(0);
    }

    public void e(Context context, c cVar) {
        FileReader fileReader;
        String str;
        String str2;
        String str3;
        int indexOf;
        try {
            fileReader = new FileReader("/proc/cpuinfo");
        } catch (FileNotFoundException e2) {
            e2.printStackTrace();
            fileReader = null;
        }
        if (fileReader == null) {
            cVar.a(8);
            return;
        }
        LineNumberReader lineNumberReader = new LineNumberReader(fileReader);
        String str4 = null;
        String str5 = null;
        String str6 = null;
        String str7 = null;
        String str8 = null;
        String str9 = null;
        String str10 = null;
        String str11 = null;
        String str12 = null;
        String str13 = null;
        String str14 = null;
        try {
            str3 = null;
            str = null;
            str2 = null;
            String readLine = lineNumberReader.readLine();
            while (readLine != null) {
                try {
                    if (readLine.indexOf("Processor") >= 0) {
                        int indexOf2 = readLine.indexOf(":");
                        if (indexOf2 > 0) {
                            str3 = readLine.substring(indexOf2 + 2);
                        }
                        str5 = str2;
                        str14 = str;
                        str4 = str3;
                    } else if (readLine.indexOf("MIPS") >= 0) {
                        int indexOf3 = readLine.indexOf(":");
                        if (indexOf3 > 0) {
                            if (str2 == null) {
                                str2 = readLine.substring(indexOf3 + 2);
                            } else {
                                str2 = str2 + " +  " + readLine.substring(indexOf3 + 2);
                            }
                        }
                        str5 = str2;
                        str14 = str;
                        str4 = str3;
                    } else if (readLine.indexOf("Features") >= 0) {
                        int indexOf4 = readLine.indexOf(":");
                        if (indexOf4 > 0) {
                            str6 = readLine.substring(indexOf4 + 2);
                        }
                        str5 = str2;
                        str14 = str;
                        str4 = str3;
                    } else if (readLine.indexOf("implementer") >= 0) {
                        int indexOf5 = readLine.indexOf(":");
                        if (indexOf5 > 0) {
                            str7 = readLine.substring(indexOf5 + 2);
                        }
                        str5 = str2;
                        str14 = str;
                        str4 = str3;
                    } else if (readLine.indexOf("architecture") >= 0) {
                        int indexOf6 = readLine.indexOf(":");
                        if (indexOf6 > 0) {
                            str8 = readLine.substring(indexOf6 + 2);
                        }
                        str5 = str2;
                        str14 = str;
                        str4 = str3;
                    } else if (readLine.indexOf("variant") >= 0) {
                        int indexOf7 = readLine.indexOf(":");
                        if (indexOf7 > 0) {
                            str9 = readLine.substring(indexOf7 + 2);
                        }
                        str5 = str2;
                        str14 = str;
                        str4 = str3;
                    } else if (readLine.indexOf("part") >= 0) {
                        int indexOf8 = readLine.indexOf(":");
                        if (indexOf8 > 0) {
                            str10 = readLine.substring(indexOf8 + 2);
                        }
                        str5 = str2;
                        str14 = str;
                        str4 = str3;
                    } else if (readLine.indexOf("revision") >= 0) {
                        int indexOf9 = readLine.indexOf(":");
                        if (indexOf9 > 0) {
                            str11 = readLine.substring(indexOf9 + 2);
                        }
                        str5 = str2;
                        str14 = str;
                        str4 = str3;
                    } else if (readLine.indexOf("Hardware") >= 0) {
                        int indexOf10 = readLine.indexOf(":");
                        if (indexOf10 > 0) {
                            str12 = readLine.substring(indexOf10 + 2);
                        }
                        str5 = str2;
                        str14 = str;
                        str4 = str3;
                    } else if (readLine.indexOf("Revision") >= 0) {
                        int indexOf11 = readLine.indexOf(":");
                        if (indexOf11 > 0) {
                            str13 = readLine.substring(indexOf11 + 2);
                        }
                        str5 = str2;
                        str14 = str;
                        str4 = str3;
                    } else if (readLine.indexOf("Serial") < 0 || (indexOf = readLine.indexOf(":")) <= 0) {
                        str5 = str2;
                        str14 = str;
                        str4 = str3;
                    } else {
                        String substring = readLine.substring(indexOf + 2);
                        str5 = str2;
                        str14 = substring;
                        str4 = str3;
                    }
                    str3 = str4;
                    str = str14;
                    str2 = str5;
                    readLine = lineNumberReader.readLine();
                } catch (Exception e3) {
                    Exception exc = e3;
                    str5 = str2;
                    str14 = str;
                    str4 = str3;
                    e = exc;
                    e.printStackTrace();
                    str3 = str4;
                    str = str14;
                    str2 = str5;
                    lineNumberReader.close();
                    fileReader.close();
                    ArrayList arrayList = new ArrayList();
                    arrayList.add(s.a(str3));
                    arrayList.add(s.a(str2));
                    arrayList.add(s.a(str6));
                    arrayList.add(s.a(str7));
                    arrayList.add(s.a(str8));
                    arrayList.add(s.a(str9));
                    arrayList.add(s.a(str10));
                    arrayList.add(s.a(str11));
                    arrayList.add(s.a(str12));
                    arrayList.add(s.a(str13));
                    arrayList.add(s.a(str));
                    cVar.a(arrayList);
                    cVar.a(0);
                }
            }
        } catch (Exception e4) {
            e = e4;
            e.printStackTrace();
            str3 = str4;
            str = str14;
            str2 = str5;
            lineNumberReader.close();
            fileReader.close();
            ArrayList arrayList2 = new ArrayList();
            arrayList2.add(s.a(str3));
            arrayList2.add(s.a(str2));
            arrayList2.add(s.a(str6));
            arrayList2.add(s.a(str7));
            arrayList2.add(s.a(str8));
            arrayList2.add(s.a(str9));
            arrayList2.add(s.a(str10));
            arrayList2.add(s.a(str11));
            arrayList2.add(s.a(str12));
            arrayList2.add(s.a(str13));
            arrayList2.add(s.a(str));
            cVar.a(arrayList2);
            cVar.a(0);
        }
        try {
            lineNumberReader.close();
        } catch (IOException e5) {
            e5.printStackTrace();
        }
        try {
            fileReader.close();
        } catch (IOException e6) {
            e6.printStackTrace();
        }
        ArrayList arrayList22 = new ArrayList();
        arrayList22.add(s.a(str3));
        arrayList22.add(s.a(str2));
        arrayList22.add(s.a(str6));
        arrayList22.add(s.a(str7));
        arrayList22.add(s.a(str8));
        arrayList22.add(s.a(str9));
        arrayList22.add(s.a(str10));
        arrayList22.add(s.a(str11));
        arrayList22.add(s.a(str12));
        arrayList22.add(s.a(str13));
        arrayList22.add(s.a(str));
        cVar.a(arrayList22);
        cVar.a(0);
    }

    public void f(Context context, c cVar) {
        int i;
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
            if (telephonyManager == null) {
                cVar.a(8);
                return;
            }
            Log.d("com.qq.connect", "get PhoneInfo" + telephonyManager.getPhoneType());
            ArrayList arrayList = new ArrayList();
            arrayList.add(s.a(telephonyManager.getDeviceId()));
            arrayList.add(s.a(telephonyManager.getDeviceSoftwareVersion()));
            arrayList.add(s.a(telephonyManager.getLine1Number()));
            if (telephonyManager.getPhoneType() == 2 || telephonyManager.getPhoneType() == 1) {
                arrayList.add(s.a(telephonyManager.getNetworkOperator()));
                arrayList.add(s.a(telephonyManager.getNetworkOperatorName()));
            } else {
                arrayList.add(s.a(telephonyManager.getNetworkOperator()));
                arrayList.add(s.a(telephonyManager.getNetworkOperatorName()));
            }
            if (telephonyManager.getSimState() == 5) {
                arrayList.add(s.a(telephonyManager.getSimSerialNumber()));
            } else {
                arrayList.add(s.hr);
            }
            arrayList.add(s.a(telephonyManager.getSubscriberId()));
            if (telephonyManager.isNetworkRoaming()) {
                arrayList.add(s.a(1));
            } else {
                arrayList.add(s.a(0));
            }
            arrayList.add(s.a(telephonyManager.getNetworkType()));
            if (AppService.j) {
                i = 1;
            } else {
                i = 0;
            }
            arrayList.add(s.a(i));
            cVar.a(arrayList);
            cVar.a(0);
        } catch (Exception e2) {
            cVar.a(8);
        }
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(com.qq.g.c r7) {
        /*
            r6 = this;
            r5 = 4
            r4 = 8
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ IOException -> 0x002a }
            java.io.FileReader r2 = new java.io.FileReader     // Catch:{ IOException -> 0x002a }
            java.lang.String r3 = "/proc/version"
            r2.<init>(r3)     // Catch:{ IOException -> 0x002a }
            r3 = 256(0x100, float:3.59E-43)
            r1.<init>(r2, r3)     // Catch:{ IOException -> 0x002a }
            java.lang.String r2 = r1.readLine()     // Catch:{ all -> 0x0025 }
            r1.close()     // Catch:{ IOException -> 0x002a }
            if (r2 != 0) goto L_0x0032
            r0 = 8
            r7.a(r0)     // Catch:{ IOException -> 0x002a }
        L_0x0024:
            return
        L_0x0025:
            r0 = move-exception
            r1.close()     // Catch:{ IOException -> 0x002a }
            throw r0     // Catch:{ IOException -> 0x002a }
        L_0x002a:
            r0 = move-exception
            r0.printStackTrace()
            r7.a(r4)
            goto L_0x0024
        L_0x0032:
            java.lang.String r1 = "\\w+\\s+\\w+\\s+([^\\s]+)\\s+\\(([^\\s@]+(?:@[^\\s.]+)?)[^)]*\\)\\s+\\((?:[^(]*\\([^)]*\\))?[^)]*\\)\\s+([^\\s]+)\\s+(?:PREEMPT\\s+)?(.+)"
            java.util.regex.Pattern r1 = java.util.regex.Pattern.compile(r1)     // Catch:{ IOException -> 0x002a }
            java.util.regex.Matcher r1 = r1.matcher(r2)     // Catch:{ IOException -> 0x002a }
            boolean r2 = r1.matches()     // Catch:{ IOException -> 0x002a }
            if (r2 != 0) goto L_0x0048
            r0 = 8
            r7.a(r0)     // Catch:{ IOException -> 0x002a }
            goto L_0x0024
        L_0x0048:
            int r2 = r1.groupCount()     // Catch:{ IOException -> 0x002a }
            if (r2 >= r5) goto L_0x0057
            r7.a(r0)     // Catch:{ IOException -> 0x002a }
            r0 = 8
            r7.a(r0)     // Catch:{ IOException -> 0x002a }
            goto L_0x0024
        L_0x0057:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x002a }
            r3 = 1
            java.lang.String r3 = r1.group(r3)     // Catch:{ IOException -> 0x002a }
            r2.<init>(r3)     // Catch:{ IOException -> 0x002a }
            java.lang.String r3 = "\n"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ IOException -> 0x002a }
            r3 = 2
            java.lang.String r3 = r1.group(r3)     // Catch:{ IOException -> 0x002a }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ IOException -> 0x002a }
            java.lang.String r3 = " "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ IOException -> 0x002a }
            r3 = 3
            java.lang.String r3 = r1.group(r3)     // Catch:{ IOException -> 0x002a }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ IOException -> 0x002a }
            java.lang.String r3 = "\n"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ IOException -> 0x002a }
            r3 = 4
            java.lang.String r3 = r1.group(r3)     // Catch:{ IOException -> 0x002a }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ IOException -> 0x002a }
            r2.toString()     // Catch:{ IOException -> 0x002a }
            r2 = 1
            java.lang.String r2 = r1.group(r2)     // Catch:{ IOException -> 0x002a }
            byte[] r2 = com.qq.AppService.s.a(r2)     // Catch:{ IOException -> 0x002a }
            r0.add(r2)     // Catch:{ IOException -> 0x002a }
            r2 = 2
            java.lang.String r2 = r1.group(r2)     // Catch:{ IOException -> 0x002a }
            byte[] r2 = com.qq.AppService.s.a(r2)     // Catch:{ IOException -> 0x002a }
            r0.add(r2)     // Catch:{ IOException -> 0x002a }
            r2 = 3
            java.lang.String r2 = r1.group(r2)     // Catch:{ IOException -> 0x002a }
            byte[] r2 = com.qq.AppService.s.a(r2)     // Catch:{ IOException -> 0x002a }
            r0.add(r2)     // Catch:{ IOException -> 0x002a }
            r2 = 4
            java.lang.String r1 = r1.group(r2)     // Catch:{ IOException -> 0x002a }
            byte[] r1 = com.qq.AppService.s.a(r1)     // Catch:{ IOException -> 0x002a }
            r0.add(r1)     // Catch:{ IOException -> 0x002a }
            r7.a(r0)
            r0 = 0
            r7.a(r0)
            goto L_0x0024
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qq.provider.ab.a(com.qq.g.c):void");
    }

    public void g(Context context, c cVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(s.a(Build.DEVICE));
        arrayList.add(s.a(Build.USER));
        cVar.a(arrayList);
        cVar.a(0);
    }

    public void h(Context context, c cVar) {
        int i;
        ArrayList arrayList = new ArrayList();
        arrayList.add(s.a("129"));
        if (cVar.e() > 0) {
            int parseInt = Integer.parseInt(cVar.j());
            int parseInt2 = Integer.parseInt("129");
            if (parseInt > parseInt2) {
                i = -2;
            } else if (parseInt == parseInt2) {
                i = 2;
            } else if (parseInt >= 124 && parseInt < parseInt2) {
                i = 1;
            } else if (parseInt < parseInt2) {
                i = -3;
            }
            arrayList.add(s.a(i));
            cVar.a(arrayList);
            cVar.a(0);
        }
        i = 0;
        arrayList.add(s.a(i));
        cVar.a(arrayList);
        cVar.a(0);
    }

    public void i(Context context, c cVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(s.a(((TelephonyManager) context.getSystemService("phone")).getDeviceId()));
        cVar.a(arrayList);
        cVar.a(0);
    }

    public void j(Context context, c cVar) {
        if (cVar.e() < 1) {
            cVar.a(0);
            return;
        }
        String j = cVar.j();
        if (s.b(j)) {
            cVar.a(1);
            return;
        }
        Intent intent = new Intent("android.intent.action.CALL", Uri.parse(WebView.SCHEME_TEL + j));
        intent.addFlags(268435456);
        try {
            context.startActivity(intent);
            cVar.a(0);
        } catch (Exception e2) {
            e2.printStackTrace();
            cVar.a(8);
        }
    }

    public void k(Context context, c cVar) {
        boolean equals = Environment.getExternalStorageState().equals("mounted");
        long j = 0;
        long j2 = 0;
        StatFs statFs = new StatFs(Environment.getDataDirectory().getPath());
        long blockSize = (long) statFs.getBlockSize();
        long availableBlocks = (((long) statFs.getAvailableBlocks()) * blockSize) / 1024;
        long blockCount = (((long) statFs.getBlockCount()) * blockSize) / 1024;
        StatFs statFs2 = new StatFs(Environment.getRootDirectory().getPath());
        long blockSize2 = (long) statFs2.getBlockSize();
        long availableBlocks2 = (((long) statFs2.getAvailableBlocks()) * blockSize2) / 1024;
        long blockCount2 = (((long) statFs2.getBlockCount()) * blockSize2) / 1024;
        StatFs statFs3 = new StatFs(Environment.getDownloadCacheDirectory().getPath());
        long blockSize3 = (long) statFs3.getBlockSize();
        long availableBlocks3 = (((long) statFs3.getAvailableBlocks()) * blockSize3) / 1024;
        long blockCount3 = (((long) statFs3.getBlockCount()) * blockSize3) / 1024;
        if (equals) {
            StatFs statFs4 = new StatFs(Environment.getExternalStorageDirectory().getPath());
            long blockSize4 = (long) statFs4.getBlockSize();
            j = (((long) statFs4.getAvailableBlocks()) * blockSize4) / 1024;
            j2 = (((long) statFs4.getBlockCount()) * blockSize4) / 1024;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(s.a((int) availableBlocks));
        arrayList.add(s.a((int) blockCount));
        arrayList.add(s.a((int) j));
        arrayList.add(s.a((int) j2));
        arrayList.add(s.a((int) availableBlocks2));
        arrayList.add(s.a((int) blockCount2));
        arrayList.add(s.a((int) availableBlocks3));
        arrayList.add(s.a((int) blockCount3));
        cVar.a(0);
        cVar.a(arrayList);
    }

    /* JADX WARNING: Removed duplicated region for block: B:54:0x0100 A[SYNTHETIC, Splitter:B:54:0x0100] */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0105 A[SYNTHETIC, Splitter:B:57:0x0105] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0111 A[SYNTHETIC, Splitter:B:63:0x0111] */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x0116 A[SYNTHETIC, Splitter:B:66:0x0116] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void l(android.content.Context r12, com.qq.g.c r13) {
        /*
            r11 = this;
            r1 = 0
            r7 = 0
            java.io.FileReader r6 = new java.io.FileReader     // Catch:{ Exception -> 0x00f3, all -> 0x010c }
            java.lang.String r0 = "/proc/meminfo"
            r6.<init>(r0)     // Catch:{ Exception -> 0x00f3, all -> 0x010c }
            java.io.LineNumberReader r5 = new java.io.LineNumberReader     // Catch:{ Exception -> 0x013f, all -> 0x013a }
            r5.<init>(r6)     // Catch:{ Exception -> 0x013f, all -> 0x013a }
            java.lang.String r0 = r5.readLine()     // Catch:{ Exception -> 0x0147 }
            r4 = r0
            r2 = r7
            r3 = r7
            r1 = r7
            r0 = r7
        L_0x0017:
            if (r4 == 0) goto L_0x00a0
            java.lang.String r8 = "MemTotal"
            boolean r8 = r4.startsWith(r8)     // Catch:{ Exception -> 0x014e }
            if (r8 == 0) goto L_0x0040
            java.lang.String r8 = " "
            java.lang.String[] r4 = r4.split(r8)     // Catch:{ Exception -> 0x014e }
            int r8 = r4.length     // Catch:{ Exception -> 0x014e }
            int r8 = r8 + -2
            r8 = r4[r8]     // Catch:{ Exception -> 0x014e }
            if (r8 == 0) goto L_0x0037
            int r8 = r4.length     // Catch:{ Exception -> 0x014e }
            int r8 = r8 + -2
            r4 = r4[r8]     // Catch:{ Exception -> 0x014e }
            int r0 = java.lang.Integer.parseInt(r4)     // Catch:{ Exception -> 0x014e }
        L_0x0037:
            r4 = r0
        L_0x0038:
            java.lang.String r0 = r5.readLine()     // Catch:{ Exception -> 0x0153 }
            r10 = r0
            r0 = r4
            r4 = r10
            goto L_0x0017
        L_0x0040:
            java.lang.String r8 = "MemFree"
            boolean r8 = r4.startsWith(r8)     // Catch:{ Exception -> 0x014e }
            if (r8 == 0) goto L_0x0060
            java.lang.String r8 = " "
            java.lang.String[] r4 = r4.split(r8)     // Catch:{ Exception -> 0x014e }
            int r8 = r4.length     // Catch:{ Exception -> 0x014e }
            int r8 = r8 + -2
            r8 = r4[r8]     // Catch:{ Exception -> 0x014e }
            if (r8 == 0) goto L_0x005e
            int r8 = r4.length     // Catch:{ Exception -> 0x014e }
            int r8 = r8 + -2
            r4 = r4[r8]     // Catch:{ Exception -> 0x014e }
            int r1 = java.lang.Integer.parseInt(r4)     // Catch:{ Exception -> 0x014e }
        L_0x005e:
            r4 = r0
            goto L_0x0038
        L_0x0060:
            java.lang.String r8 = "Buffers"
            boolean r8 = r4.startsWith(r8)     // Catch:{ Exception -> 0x014e }
            if (r8 == 0) goto L_0x0080
            java.lang.String r8 = " "
            java.lang.String[] r4 = r4.split(r8)     // Catch:{ Exception -> 0x014e }
            int r8 = r4.length     // Catch:{ Exception -> 0x014e }
            int r8 = r8 + -2
            r8 = r4[r8]     // Catch:{ Exception -> 0x014e }
            if (r8 == 0) goto L_0x007e
            int r8 = r4.length     // Catch:{ Exception -> 0x014e }
            int r8 = r8 + -2
            r4 = r4[r8]     // Catch:{ Exception -> 0x014e }
            int r2 = java.lang.Integer.parseInt(r4)     // Catch:{ Exception -> 0x014e }
        L_0x007e:
            r4 = r0
            goto L_0x0038
        L_0x0080:
            java.lang.String r8 = "Cached"
            boolean r8 = r4.startsWith(r8)     // Catch:{ Exception -> 0x014e }
            if (r8 == 0) goto L_0x0159
            java.lang.String r8 = " "
            java.lang.String[] r4 = r4.split(r8)     // Catch:{ Exception -> 0x014e }
            int r8 = r4.length     // Catch:{ Exception -> 0x014e }
            int r8 = r8 + -2
            r8 = r4[r8]     // Catch:{ Exception -> 0x014e }
            if (r8 == 0) goto L_0x0159
            int r8 = r4.length     // Catch:{ Exception -> 0x014e }
            int r8 = r8 + -2
            r4 = r4[r8]     // Catch:{ Exception -> 0x014e }
            int r3 = java.lang.Integer.parseInt(r4)     // Catch:{ Exception -> 0x014e }
            r4 = r0
            goto L_0x0038
        L_0x00a0:
            if (r5 == 0) goto L_0x00a5
            r5.close()     // Catch:{ IOException -> 0x012e }
        L_0x00a5:
            if (r6 == 0) goto L_0x00aa
            r6.close()     // Catch:{ IOException -> 0x0134 }
        L_0x00aa:
            r4 = r0
            r10 = r1
            r1 = r3
            r3 = r10
        L_0x00ae:
            java.lang.String r0 = "activity"
            java.lang.Object r0 = r12.getSystemService(r0)
            android.app.ActivityManager r0 = (android.app.ActivityManager) r0
            android.app.ActivityManager$MemoryInfo r5 = new android.app.ActivityManager$MemoryInfo
            r5.<init>()
            r0.getMemoryInfo(r5)
            long r5 = r5.availMem
            r8 = 1024(0x400, double:5.06E-321)
            long r5 = r5 / r8
            int r0 = (int) r5
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>()
            byte[] r4 = com.qq.AppService.s.a(r4)
            r5.add(r4)
            byte[] r3 = com.qq.AppService.s.a(r3)
            r5.add(r3)
            byte[] r2 = com.qq.AppService.s.a(r2)
            r5.add(r2)
            byte[] r1 = com.qq.AppService.s.a(r1)
            r5.add(r1)
            byte[] r0 = com.qq.AppService.s.a(r0)
            r5.add(r0)
            r13.a(r5)
            r13.a(r7)
            return
        L_0x00f3:
            r0 = move-exception
            r4 = r0
            r5 = r1
            r6 = r1
            r2 = r7
            r3 = r7
            r1 = r7
            r0 = r7
        L_0x00fb:
            r4.printStackTrace()     // Catch:{ all -> 0x013d }
            if (r5 == 0) goto L_0x0103
            r5.close()     // Catch:{ IOException -> 0x0124 }
        L_0x0103:
            if (r6 == 0) goto L_0x0108
            r6.close()     // Catch:{ IOException -> 0x0129 }
        L_0x0108:
            r4 = r3
            r3 = r1
            r1 = r0
            goto L_0x00ae
        L_0x010c:
            r0 = move-exception
            r5 = r1
            r6 = r1
        L_0x010f:
            if (r5 == 0) goto L_0x0114
            r5.close()     // Catch:{ IOException -> 0x011a }
        L_0x0114:
            if (r6 == 0) goto L_0x0119
            r6.close()     // Catch:{ IOException -> 0x011f }
        L_0x0119:
            throw r0
        L_0x011a:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0114
        L_0x011f:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0119
        L_0x0124:
            r4 = move-exception
            r4.printStackTrace()
            goto L_0x0103
        L_0x0129:
            r4 = move-exception
            r4.printStackTrace()
            goto L_0x0108
        L_0x012e:
            r4 = move-exception
            r4.printStackTrace()
            goto L_0x00a5
        L_0x0134:
            r4 = move-exception
            r4.printStackTrace()
            goto L_0x00aa
        L_0x013a:
            r0 = move-exception
            r5 = r1
            goto L_0x010f
        L_0x013d:
            r0 = move-exception
            goto L_0x010f
        L_0x013f:
            r0 = move-exception
            r4 = r0
            r5 = r1
            r2 = r7
            r3 = r7
            r1 = r7
            r0 = r7
            goto L_0x00fb
        L_0x0147:
            r0 = move-exception
            r4 = r0
            r2 = r7
            r1 = r7
            r3 = r7
            r0 = r7
            goto L_0x00fb
        L_0x014e:
            r4 = move-exception
            r10 = r3
            r3 = r0
            r0 = r10
            goto L_0x00fb
        L_0x0153:
            r0 = move-exception
            r10 = r0
            r0 = r3
            r3 = r4
            r4 = r10
            goto L_0x00fb
        L_0x0159:
            r4 = r0
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qq.provider.ab.l(android.content.Context, com.qq.g.c):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:31:0x0098 A[SYNTHETIC, Splitter:B:31:0x0098] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x009d A[SYNTHETIC, Splitter:B:34:0x009d] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00ab A[SYNTHETIC, Splitter:B:41:0x00ab] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00b0 A[SYNTHETIC, Splitter:B:44:0x00b0] */
    /* JADX WARNING: Removed duplicated region for block: B:71:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void m(android.content.Context r11, com.qq.g.c r12) {
        /*
            r10 = this;
            r2 = 0
            r8 = -1
            r4 = 0
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>()
            java.io.FileReader r3 = new java.io.FileReader     // Catch:{ Exception -> 0x008c, all -> 0x00a6 }
            java.lang.String r0 = "/proc/meminfo"
            r3.<init>(r0)     // Catch:{ Exception -> 0x008c, all -> 0x00a6 }
            java.io.LineNumberReader r1 = new java.io.LineNumberReader     // Catch:{ Exception -> 0x00d2, all -> 0x00ca }
            r1.<init>(r3)     // Catch:{ Exception -> 0x00d2, all -> 0x00ca }
            java.lang.String r0 = r1.readLine()     // Catch:{ Exception -> 0x00d6, all -> 0x00cd }
            r2 = r4
            r9 = r4
            r4 = r0
            r0 = r9
        L_0x001c:
            if (r4 == 0) goto L_0x0063
            java.lang.String r6 = "MemTotal"
            int r6 = r4.indexOf(r6)     // Catch:{ Exception -> 0x00d6, all -> 0x00cd }
            if (r6 == r8) goto L_0x003e
            java.lang.String r6 = " "
            java.lang.String[] r6 = r4.split(r6)     // Catch:{ Exception -> 0x00d6, all -> 0x00cd }
            int r7 = r6.length     // Catch:{ Exception -> 0x00d6, all -> 0x00cd }
            int r7 = r7 + -2
            r7 = r6[r7]     // Catch:{ Exception -> 0x00d6, all -> 0x00cd }
            if (r7 == 0) goto L_0x003e
            int r2 = r6.length     // Catch:{ Exception -> 0x00d6, all -> 0x00cd }
            int r2 = r2 + -2
            r2 = r6[r2]     // Catch:{ Exception -> 0x00d6, all -> 0x00cd }
            int r2 = java.lang.Integer.parseInt(r2)     // Catch:{ Exception -> 0x00d6, all -> 0x00cd }
            int r2 = r2 / 1024
        L_0x003e:
            java.lang.String r6 = "MemFree"
            int r6 = r4.indexOf(r6)     // Catch:{ Exception -> 0x00d6, all -> 0x00cd }
            if (r6 == r8) goto L_0x005e
            java.lang.String r6 = " "
            java.lang.String[] r4 = r4.split(r6)     // Catch:{ Exception -> 0x00d6, all -> 0x00cd }
            int r6 = r4.length     // Catch:{ Exception -> 0x00d6, all -> 0x00cd }
            int r6 = r6 + -2
            r6 = r4[r6]     // Catch:{ Exception -> 0x00d6, all -> 0x00cd }
            if (r6 == 0) goto L_0x005e
            int r0 = r4.length     // Catch:{ Exception -> 0x00d6, all -> 0x00cd }
            int r0 = r0 + -2
            r0 = r4[r0]     // Catch:{ Exception -> 0x00d6, all -> 0x00cd }
            int r0 = java.lang.Integer.parseInt(r0)     // Catch:{ Exception -> 0x00d6, all -> 0x00cd }
            int r0 = r0 / 1024
        L_0x005e:
            java.lang.String r4 = r1.readLine()     // Catch:{ Exception -> 0x00d6, all -> 0x00cd }
            goto L_0x001c
        L_0x0063:
            int r4 = r2 - r0
            byte[] r2 = com.qq.AppService.s.a(r2)     // Catch:{ Exception -> 0x00d6, all -> 0x00cd }
            r5.add(r2)     // Catch:{ Exception -> 0x00d6, all -> 0x00cd }
            byte[] r2 = com.qq.AppService.s.a(r4)     // Catch:{ Exception -> 0x00d6, all -> 0x00cd }
            r5.add(r2)     // Catch:{ Exception -> 0x00d6, all -> 0x00cd }
            byte[] r0 = com.qq.AppService.s.a(r0)     // Catch:{ Exception -> 0x00d6, all -> 0x00cd }
            r5.add(r0)     // Catch:{ Exception -> 0x00d6, all -> 0x00cd }
            r12.a(r5)     // Catch:{ Exception -> 0x00d6, all -> 0x00cd }
            r0 = 0
            r12.a(r0)     // Catch:{ Exception -> 0x00d6, all -> 0x00cd }
            if (r1 == 0) goto L_0x0086
            r1.close()     // Catch:{ IOException -> 0x00c3 }
        L_0x0086:
            if (r3 == 0) goto L_0x008b
            r3.close()     // Catch:{ IOException -> 0x00c8 }
        L_0x008b:
            return
        L_0x008c:
            r0 = move-exception
            r1 = r2
        L_0x008e:
            r0.printStackTrace()     // Catch:{ all -> 0x00cf }
            r0 = 8
            r12.a(r0)     // Catch:{ all -> 0x00cf }
            if (r1 == 0) goto L_0x009b
            r1.close()     // Catch:{ IOException -> 0x00be }
        L_0x009b:
            if (r2 == 0) goto L_0x008b
            r2.close()     // Catch:{ IOException -> 0x00a1 }
            goto L_0x008b
        L_0x00a1:
            r0 = move-exception
        L_0x00a2:
            r0.printStackTrace()
            goto L_0x008b
        L_0x00a6:
            r0 = move-exception
            r1 = r2
            r3 = r2
        L_0x00a9:
            if (r1 == 0) goto L_0x00ae
            r1.close()     // Catch:{ IOException -> 0x00b4 }
        L_0x00ae:
            if (r3 == 0) goto L_0x00b3
            r3.close()     // Catch:{ IOException -> 0x00b9 }
        L_0x00b3:
            throw r0
        L_0x00b4:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00ae
        L_0x00b9:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00b3
        L_0x00be:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x009b
        L_0x00c3:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0086
        L_0x00c8:
            r0 = move-exception
            goto L_0x00a2
        L_0x00ca:
            r0 = move-exception
            r1 = r2
            goto L_0x00a9
        L_0x00cd:
            r0 = move-exception
            goto L_0x00a9
        L_0x00cf:
            r0 = move-exception
            r3 = r2
            goto L_0x00a9
        L_0x00d2:
            r0 = move-exception
            r1 = r2
            r2 = r3
            goto L_0x008e
        L_0x00d6:
            r0 = move-exception
            r2 = r3
            goto L_0x008e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qq.provider.ab.m(android.content.Context, com.qq.g.c):void");
    }

    public void n(Context context, c cVar) {
        String c2 = n.c();
        ArrayList arrayList = new ArrayList();
        arrayList.add(s.a(c2));
        cVar.a(arrayList);
    }

    public void o(Context context, c cVar) {
        String c2 = n.c();
        ArrayList arrayList = new ArrayList();
        if (c2 == null) {
            arrayList.add(s.a(0));
            arrayList.add(s.a(0));
        } else {
            StatFs statFs = new StatFs(c2);
            long blockSize = (long) statFs.getBlockSize();
            arrayList.add(s.a((int) ((((long) statFs.getBlockCount()) * blockSize) / 1024)));
            arrayList.add(s.a((int) ((blockSize * ((long) statFs.getAvailableBlocks())) / 1024)));
        }
        cVar.a(arrayList);
        cVar.a(0);
    }

    public void p(Context context, c cVar) {
        ArrayList arrayList = new ArrayList();
        if (AppService.e || this.d <= 0 || this.e <= 0) {
            PowerManager.WakeLock wakeLock = null;
            try {
                wakeLock = ((PowerManager) context.getSystemService("power")).newWakeLock(805306394, "lock");
                wakeLock.acquire();
                Display defaultDisplay = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
                defaultDisplay.getMetrics(new DisplayMetrics());
                this.e = defaultDisplay.getWidth();
                this.d = defaultDisplay.getHeight();
                arrayList.add(s.a(this.d));
                arrayList.add(s.a(this.e));
                if (wakeLock != null && wakeLock.isHeld()) {
                    wakeLock.release();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
                cVar.a(1);
                if (wakeLock != null && wakeLock.isHeld()) {
                    wakeLock.release();
                    return;
                }
                return;
            } catch (Throwable th) {
                if (wakeLock != null && wakeLock.isHeld()) {
                    wakeLock.release();
                }
                throw th;
            }
        } else {
            arrayList.add(s.a(this.d));
            arrayList.add(s.a(this.e));
        }
        cVar.a(arrayList);
        cVar.a(0);
    }

    public void q(Context context, c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        if (s.b(j)) {
            cVar.a(1);
            return;
        }
        File file = new File(j);
        if (!file.exists()) {
            cVar.a(6);
            return;
        }
        Cursor query = context.getContentResolver().query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, null, "_data = " + file.getAbsolutePath(), null, null);
        if (query == null) {
            cVar.a(8);
        } else if (query.moveToFirst()) {
            RingtoneManager.setActualDefaultRingtoneUri(context, 1, Uri.withAppendedPath(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, Constants.STR_EMPTY + query.getInt(query.getColumnIndex("_id"))));
            cVar.a(0);
            query.close();
        } else {
            query.close();
            Cursor query2 = context.getContentResolver().query(MediaStore.Audio.Media.INTERNAL_CONTENT_URI, null, "_data = " + file.getAbsolutePath(), null, null);
            if (query2 == null) {
                cVar.a(8);
            } else if (query2.moveToFirst()) {
                RingtoneManager.setActualDefaultRingtoneUri(context, 1, Uri.withAppendedPath(MediaStore.Audio.Media.INTERNAL_CONTENT_URI, Constants.STR_EMPTY + query2.getInt(query2.getColumnIndex("_id"))));
                cVar.a(0);
                query2.close();
            } else {
                query2.close();
                cVar.a(7);
            }
        }
    }

    public void r(Context context, c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        byte[] k = cVar.k();
        if (k == null) {
            cVar.a(1);
            return;
        }
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(k);
        try {
            context.setWallpaper(byteArrayInputStream);
            try {
                byteArrayInputStream.close();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
            cVar.a(0);
        } catch (IOException e3) {
            e3.printStackTrace();
            try {
                byteArrayInputStream.close();
            } catch (IOException e4) {
                e4.printStackTrace();
            }
            cVar.a(7);
        }
    }

    public static void s(Context context, c cVar) {
        if (cVar.e() < 2) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        if (j != null && j.equals("USBDeviceID")) {
            AppService.j = true;
        }
        String j2 = cVar.j();
        if (s.b(j) || s.b(j2)) {
            cVar.a(1);
            return;
        }
        SharedPreferences.Editor edit = context.getSharedPreferences("com.qq.connect", 0).edit();
        edit.putString(j, j2);
        edit.commit();
        cVar.a(0);
    }

    public void t(Context context, c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        if (s.b(j)) {
            cVar.a(1);
            return;
        }
        String string = context.getSharedPreferences("com.qq.connect", 0).getString(j, null);
        if (s.b(string)) {
            cVar.a(7);
            return;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(s.a(string));
        cVar.a(arrayList);
        cVar.a(0);
    }

    public void u(Context context, c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        if (s.b(j)) {
            cVar.a(1);
            return;
        }
        SharedPreferences.Editor edit = context.getSharedPreferences("com.qq.connect", 0).edit();
        try {
            edit.remove(j);
            edit.commit();
        } catch (Exception e2) {
            cVar.a(8);
        }
    }

    public void v(Context context, c cVar) {
        SharedPreferences.Editor edit = context.getSharedPreferences("com.qq.connect", 0).edit();
        try {
            edit.clear();
            edit.commit();
        } catch (Exception e2) {
            cVar.a(8);
        }
    }

    public void w(Context context, c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
        }
        if (cVar.h() < 15000) {
        }
        int c2 = d.c(context);
        Log.d("com.qq.connect", " do_root:::  root " + c2);
        if (c2 == 0) {
            h.c(c2);
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(s.a(c2));
        cVar.a(arrayList);
        cVar.a(0);
    }

    public void x(Context context, c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        cVar.h();
        int a2 = d.a(0, context);
        Log.d("com.qq.connect", " isroot:::  root " + a2);
        ArrayList arrayList = new ArrayList();
        arrayList.add(s.a(a2));
        cVar.a(arrayList);
        cVar.a(0);
    }

    public void y(Context context, c cVar) {
        a.a(context);
        if (a.b || a.c) {
            cVar.a(0);
            return;
        }
        d.b(context);
        if (d.f267a != null) {
            d.a(0, context);
            a.a(context);
        }
        if (a.c) {
            cVar.a(0);
        } else {
            cVar.a(4);
        }
    }

    public void a(Context context, int i) {
        int i2 = 0;
        Log.d("com.qq.connect", "captureScreen>>>>>>>>>>>...");
        a.a(context);
        if (!a.b && !a.c) {
            d.b(context);
            if (d.f267a != null) {
                d.a(0, context);
                a.a(context);
                AuthorReceiver.a(2000);
            }
        }
        Thread.currentThread().setPriority(10);
        Native nativeR = new Native();
        if (a.b) {
            i2 = nativeR.captureScreenToFd(i, "aurora_usb", AuthorReceiver.c(), 40, 0);
        } else if (a.c) {
            i2 = nativeR.captureScreenToFd(i, "aurora_root", AuthorReceiver.b(), 40, 0);
        } else {
            Log.d("com.qq.connect", "captureScreenToFd usb&root not on ");
        }
        Thread.currentThread().setPriority(5);
        Log.d("com.qq.connect", "captureScreenToFd over " + i2);
    }

    public boolean z(Context context, c cVar) {
        int i;
        byte[] bArr;
        a.a(context);
        if (!a.b && !a.c) {
            d.b(context);
            if (d.f267a != null) {
                d.a(0, context);
                a.a(context);
                AuthorReceiver.a(2000);
            }
        }
        if (!Build.PRODUCT.equals("X100") || !Build.MODEL.equals("JY-G1")) {
            i = 0;
        } else {
            i = 1;
        }
        long currentTimeMillis = System.currentTimeMillis();
        Native nativeR = new Native();
        if (a.c) {
            bArr = nativeR.getScreenShotByAurora("aurora_root", AuthorReceiver.b(), 100, i);
        } else if (a.b) {
            bArr = nativeR.getScreenShotByAurora("aurora_usb", AuthorReceiver.c(), 100, i);
        } else {
            bArr = null;
        }
        if (bArr == null || bArr.length == 0) {
            cVar.a(8);
            return false;
        }
        cVar.b = new byte[(bArr.length + 8 + 4)];
        System.arraycopy(s.a(cVar.b.length - 4), 0, cVar.b, 0, 4);
        System.arraycopy(s.hs, 0, cVar.b, 4, 4);
        System.arraycopy(s.a(bArr.length), 0, cVar.b, 8, 4);
        System.arraycopy(bArr, 0, cVar.b, 12, bArr.length);
        cVar.a(0);
        Log.d("com.qq.connect", "getScreenShot3 " + (System.currentTimeMillis() - currentTimeMillis) + "." + cVar.b.length);
        return true;
    }

    public boolean a(Context context, BufferedOutputStream bufferedOutputStream) {
        boolean z;
        int i;
        boolean z2 = false;
        File file = new File("/dev/graphics/fb0");
        if (!file.exists()) {
            try {
                bufferedOutputStream.write(s.a(4));
                bufferedOutputStream.write(s.a(4));
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        } else {
            byte[] bArr = null;
            synchronized (com.qq.g.b.class) {
                if (com.qq.g.b.f292a <= 0 || com.qq.g.b.l == null) {
                    z = false;
                } else {
                    z = true;
                }
                if (z) {
                    int length = com.qq.g.b.l.length;
                    int i2 = com.qq.g.b.c * (com.qq.g.b.f292a >> 3) * com.qq.g.b.b;
                    bArr = new byte[(length + 4)];
                    System.arraycopy(com.qq.g.b.l, 0, bArr, 0, length);
                    System.arraycopy(s.a(i2), 0, bArr, length, 4);
                    System.arraycopy(s.a(length + i2), 0, bArr, 0, 4);
                    i = i2;
                } else {
                    i = 0;
                }
            }
            if (!z) {
                try {
                    bufferedOutputStream.write(s.a(4));
                    bufferedOutputStream.write(s.a(7));
                } catch (Exception e3) {
                    e3.printStackTrace();
                }
            } else {
                try {
                    bufferedOutputStream.write(bArr);
                } catch (IOException e4) {
                    e4.printStackTrace();
                }
                try {
                    FileInputStream fileInputStream = new FileInputStream(file);
                    byte[] bArr2 = new byte[8192];
                    try {
                        if (Build.BRAND.equals("moto_chn") && Build.MODEL.equals("ME511")) {
                            fileInputStream.skip((long) i);
                        }
                        int i3 = 0;
                        int i4 = 0;
                        while (i3 < 10 && i4 < i) {
                            int read = fileInputStream.read(bArr2);
                            if (read > 0) {
                                bufferedOutputStream.write(bArr2, 0, read);
                                bufferedOutputStream.flush();
                                i4 += read;
                                i3 = 0;
                            }
                            if (read < bArr2.length && i4 < i) {
                                try {
                                    Thread.sleep(10);
                                } catch (Exception e5) {
                                    e5.printStackTrace();
                                }
                            }
                            i3++;
                        }
                        z2 = true;
                    } catch (Exception e6) {
                        e6.printStackTrace();
                    }
                    try {
                        fileInputStream.close();
                    } catch (IOException e7) {
                        e7.printStackTrace();
                    }
                } catch (FileNotFoundException e8) {
                    e8.printStackTrace();
                    try {
                        bufferedOutputStream.write(s.a(4));
                        bufferedOutputStream.write(s.a(4));
                    } catch (Exception e9) {
                        e9.printStackTrace();
                    }
                }
            }
        }
        return z2;
    }

    public boolean A(Context context, c cVar) {
        boolean z;
        int i;
        int i2;
        int i3;
        int read;
        boolean z2 = true;
        File file = new File("/dev/graphics/fb0");
        if (!file.exists()) {
            cVar.a(4);
            return false;
        }
        synchronized (com.qq.g.b.class) {
            if (com.qq.g.b.f292a <= 0 || com.qq.g.b.l == null) {
                z = false;
            } else {
                z = true;
            }
            if (z) {
                i2 = com.qq.g.b.l.length;
                i = (((com.qq.g.b.b * com.qq.g.b.f292a) + 31) / 32) * 4 * com.qq.g.b.c;
                cVar.b = new byte[(i + 4 + i2)];
                System.arraycopy(com.qq.g.b.l, 0, cVar.b, 0, i2);
                System.arraycopy(s.a(i), 0, cVar.b, i2, 4);
            } else {
                i = 0;
                i2 = 0;
            }
        }
        if (!z) {
            cVar.a(4);
            return false;
        }
        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
            int i4 = i2 + 4;
            try {
                if (Build.BRAND.equals("moto_chn") && Build.MODEL.equals("ME511")) {
                    bufferedInputStream.skip((long) i);
                }
                int i5 = i4;
                for (char c2 = 0; c2 < 10 && i > 0 && (read = bufferedInputStream.read(cVar.b, i5, i)) > 0; c2 = 1) {
                    i -= read;
                    i5 += read;
                }
                i3 = i;
            } catch (Exception e2) {
                e2.printStackTrace();
                z2 = false;
                i3 = i;
            }
            try {
                fileInputStream.close();
                bufferedInputStream.close();
            } catch (IOException e3) {
                e3.printStackTrace();
            }
            if (i3 <= 0) {
                cVar.a(0);
            } else {
                cVar.a(7);
            }
            return z2;
        } catch (FileNotFoundException e4) {
            e4.printStackTrace();
            cVar.a(4);
            return false;
        }
    }

    public void B(Context context, c cVar) {
        WifiManager wifiManager = (WifiManager) context.getSystemService("wifi");
        if (wifiManager == null) {
            cVar.a(8);
            return;
        }
        ArrayList arrayList = new ArrayList();
        boolean isWifiEnabled = wifiManager.isWifiEnabled();
        int wifiState = wifiManager.getWifiState();
        arrayList.add(s.a(isWifiEnabled ? 1 : 0));
        arrayList.add(s.a(wifiState));
        WifiInfo connectionInfo = wifiManager.getConnectionInfo();
        if (connectionInfo == null) {
            arrayList.add(s.a(0));
            arrayList.add(s.a(0));
            arrayList.add(s.hr);
            arrayList.add(s.hr);
        } else {
            arrayList.add(s.a(connectionInfo.getIpAddress()));
            arrayList.add(s.a(connectionInfo.getLinkSpeed()));
            arrayList.add(s.a(connectionInfo.getMacAddress()));
            arrayList.add(s.a(connectionInfo.getBSSID()));
        }
        cVar.a(arrayList);
        cVar.a(0);
    }

    public void C(Context context, c cVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(s.a(PowerReceiver.f207a));
        arrayList.add(s.a(PowerReceiver.b));
        cVar.a(arrayList);
        cVar.a(0);
    }

    public void D(Context context, c cVar) {
        SensorManager sensorManager = (SensorManager) context.getSystemService("sensor");
        if (sensorManager == null) {
            cVar.a(8);
            return;
        }
        ArrayList arrayList = new ArrayList();
        Sensor defaultSensor = sensorManager.getDefaultSensor(1);
        if (defaultSensor == null) {
            arrayList.add(s.a(0));
            arrayList.add(s.a(0));
        } else {
            arrayList.add(s.a(1));
            arrayList.add(s.a(defaultSensor.getVersion()));
        }
        Sensor defaultSensor2 = sensorManager.getDefaultSensor(4);
        if (defaultSensor2 == null) {
            arrayList.add(s.a(0));
            arrayList.add(s.a(0));
        } else {
            arrayList.add(s.a(1));
            arrayList.add(s.a(defaultSensor2.getVersion()));
        }
        Sensor defaultSensor3 = sensorManager.getDefaultSensor(5);
        if (defaultSensor3 == null) {
            arrayList.add(s.a(0));
            arrayList.add(s.a(0));
        } else {
            arrayList.add(s.a(1));
            arrayList.add(s.a(defaultSensor3.getVersion()));
        }
        Sensor defaultSensor4 = sensorManager.getDefaultSensor(2);
        if (defaultSensor4 == null) {
            arrayList.add(s.a(0));
            arrayList.add(s.a(0));
        } else {
            arrayList.add(s.a(1));
            arrayList.add(s.a(defaultSensor4.getVersion()));
        }
        Sensor defaultSensor5 = sensorManager.getDefaultSensor(3);
        if (defaultSensor5 == null) {
            arrayList.add(s.a(0));
            arrayList.add(s.a(0));
        } else {
            arrayList.add(s.a(1));
            arrayList.add(s.a(defaultSensor5.getVersion()));
        }
        Sensor defaultSensor6 = sensorManager.getDefaultSensor(6);
        if (defaultSensor6 == null) {
            arrayList.add(s.a(0));
            arrayList.add(s.a(0));
        } else {
            arrayList.add(s.a(1));
            arrayList.add(s.a(defaultSensor6.getVersion()));
        }
        Sensor defaultSensor7 = sensorManager.getDefaultSensor(8);
        if (defaultSensor7 == null) {
            arrayList.add(s.a(0));
            arrayList.add(s.a(0));
        } else {
            arrayList.add(s.a(1));
            arrayList.add(s.a(defaultSensor7.getVersion()));
        }
        Sensor defaultSensor8 = sensorManager.getDefaultSensor(7);
        if (defaultSensor8 == null) {
            arrayList.add(s.a(0));
            arrayList.add(s.a(0));
        } else {
            arrayList.add(s.a(1));
            arrayList.add(s.a(defaultSensor8.getVersion()));
        }
        cVar.a(arrayList);
        cVar.a(0);
    }

    public void E(Context context, c cVar) {
        if (context == null) {
            cVar.a(8);
            return;
        }
        ((KeyguardManager) context.getSystemService("keyguard")).newKeyguardLock("unLock").disableKeyguard();
        ((PowerManager) context.getSystemService("power")).newWakeLock(805306394, "lock").acquire();
        new ac(this, context).start();
        while (OpenGLActivity.b == null) {
            synchronized (OpenGLActivity.c) {
                try {
                    OpenGLActivity.c.wait(60);
                } catch (InterruptedException e2) {
                    e2.printStackTrace();
                }
            }
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(s.a(OpenGLActivity.b.f220a));
        arrayList.add(s.a(OpenGLActivity.b.b));
        arrayList.add(s.a(OpenGLActivity.b.c));
        arrayList.add(s.a(OpenGLActivity.b.d));
        OpenGLActivity.b = null;
        cVar.a(arrayList);
        cVar.a(0);
        return;
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0090  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x009a  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00a4  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00c4  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00c6  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00c8  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x00cc  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void F(android.content.Context r13, com.qq.g.c r14) {
        /*
            r12 = this;
            r1 = 0
            r2 = 0
            r3 = 0
            int r0 = com.qq.util.j.c
            r4 = 11
            if (r0 < r4) goto L_0x0023
            java.lang.Class<android.os.Environment> r0 = android.os.Environment.class
            java.lang.String r4 = "isExternalStorageEmulated"
            r5 = 0
            java.lang.Class[] r5 = new java.lang.Class[r5]     // Catch:{ Exception -> 0x00b4 }
            java.lang.reflect.Method r0 = r0.getMethod(r4, r5)     // Catch:{ Exception -> 0x00b4 }
            r4 = 0
            r5 = 0
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Exception -> 0x00b4 }
            java.lang.Object r0 = r0.invoke(r4, r5)     // Catch:{ Exception -> 0x00b4 }
            java.lang.Boolean r0 = (java.lang.Boolean) r0     // Catch:{ Exception -> 0x00b4 }
            boolean r0 = r0.booleanValue()     // Catch:{ Exception -> 0x00b4 }
            r1 = r0
        L_0x0023:
            int r0 = com.qq.util.j.c
            r4 = 9
            if (r0 < r4) goto L_0x00be
            java.lang.Class<android.os.Environment> r0 = android.os.Environment.class
            java.lang.String r4 = "isExternalStorageRemovable"
            r5 = 0
            java.lang.Class[] r5 = new java.lang.Class[r5]     // Catch:{ Exception -> 0x00ba }
            java.lang.reflect.Method r0 = r0.getMethod(r4, r5)     // Catch:{ Exception -> 0x00ba }
            r4 = 0
            r5 = 0
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Exception -> 0x00ba }
            java.lang.Object r0 = r0.invoke(r4, r5)     // Catch:{ Exception -> 0x00ba }
            java.lang.Boolean r0 = (java.lang.Boolean) r0     // Catch:{ Exception -> 0x00ba }
            boolean r0 = r0.booleanValue()     // Catch:{ Exception -> 0x00ba }
        L_0x0042:
            int r2 = com.qq.util.j.c
            r4 = 8
            if (r2 < r4) goto L_0x00cc
            java.io.File r4 = com.qq.provider.n.b()
            java.lang.String r5 = com.qq.provider.n.c()
            if (r4 == 0) goto L_0x00ca
            if (r1 != 0) goto L_0x00ca
            r3 = 1
            r2 = r3
        L_0x0056:
            if (r2 != 0) goto L_0x0089
            if (r5 == 0) goto L_0x0089
            if (r4 == 0) goto L_0x00c2
            if (r1 == 0) goto L_0x00c2
            android.os.StatFs r2 = new android.os.StatFs
            r2.<init>(r5)
            int r3 = r2.getBlockSize()
            long r5 = (long) r3
            int r2 = r2.getAvailableBlocks()
            long r2 = (long) r2
            android.os.StatFs r7 = new android.os.StatFs
            java.lang.String r4 = r4.getAbsolutePath()
            r7.<init>(r4)
            int r4 = r7.getBlockSize()
            long r8 = (long) r4
            int r4 = r7.getAvailableBlocks()
            long r10 = (long) r4
            int r4 = (r5 > r8 ? 1 : (r5 == r8 ? 0 : -1))
            if (r4 != 0) goto L_0x00c0
            int r2 = (r2 > r10 ? 1 : (r2 == r10 ? 0 : -1))
            if (r2 != 0) goto L_0x00c0
            r2 = 0
        L_0x0089:
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>()
            if (r1 == 0) goto L_0x00c4
            r1 = 1
        L_0x0091:
            byte[] r1 = com.qq.AppService.s.a(r1)
            r3.add(r1)
            if (r0 == 0) goto L_0x00c6
            r0 = 1
        L_0x009b:
            byte[] r0 = com.qq.AppService.s.a(r0)
            r3.add(r0)
            if (r2 == 0) goto L_0x00c8
            r0 = 1
        L_0x00a5:
            byte[] r0 = com.qq.AppService.s.a(r0)
            r3.add(r0)
            r14.a(r3)
            r0 = 0
            r14.a(r0)
            return
        L_0x00b4:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0023
        L_0x00ba:
            r0 = move-exception
            r0.printStackTrace()
        L_0x00be:
            r0 = r2
            goto L_0x0042
        L_0x00c0:
            r2 = 1
            goto L_0x0089
        L_0x00c2:
            r2 = 1
            goto L_0x0089
        L_0x00c4:
            r1 = 0
            goto L_0x0091
        L_0x00c6:
            r0 = 0
            goto L_0x009b
        L_0x00c8:
            r0 = 0
            goto L_0x00a5
        L_0x00ca:
            r2 = r3
            goto L_0x0056
        L_0x00cc:
            r2 = r3
            goto L_0x0089
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qq.provider.ab.F(android.content.Context, com.qq.g.c):void");
    }

    public void G(Context context, c cVar) {
        if (context == null) {
            cVar.a(8);
            return;
        }
        OpenGLActivity.a();
        ((KeyguardManager) context.getSystemService("keyguard")).newKeyguardLock("unLock").disableKeyguard();
        PowerManager.WakeLock newWakeLock = ((PowerManager) context.getSystemService("power")).newWakeLock(805306394, "lock");
        newWakeLock.acquire();
        new ad(this, context).start();
        int i = 0;
        while (OpenGLActivity.f205a == null && i < 100) {
            i++;
            synchronized (OpenGLActivity.c) {
                try {
                    OpenGLActivity.c.wait(60);
                } catch (InterruptedException e2) {
                    e2.printStackTrace();
                }
            }
        }
        int a2 = a(OpenGLActivity.f205a);
        ArrayList arrayList = new ArrayList();
        arrayList.add(s.a(a2));
        cVar.a(arrayList);
        cVar.a(0);
        if (newWakeLock != null && newWakeLock.isHeld()) {
            newWakeLock.release();
            return;
        }
        return;
    }

    public void H(Context context, c cVar) {
        int i = 4500;
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        if (h > 4500) {
            i = h;
        }
        this.f = -2;
        new ae(this, context, cVar).start();
        synchronized (cVar) {
            try {
                cVar.wait((long) i);
            } catch (InterruptedException e2) {
                e2.printStackTrace();
            }
        }
        if (this.f == -1) {
            cVar.a(8);
        } else if (this.f == -2) {
            cVar.a(4);
        } else {
            ArrayList arrayList = new ArrayList();
            arrayList.add(s.a(this.f));
            cVar.a(arrayList);
            cVar.a(0);
        }
    }

    public static int a(String str) {
        Locale locale = Locale.CHINA;
        if (str == null || str.length() == 0) {
            return 0;
        }
        String upperCase = str.toUpperCase(locale);
        if (upperCase.indexOf("SGX") > -1 || upperCase.indexOf("ADRENO") > -1 || upperCase.indexOf("NVIDIA") > -1 || upperCase.indexOf("MALI") > -1 || upperCase.indexOf("GC530") > -1) {
            return 1;
        }
        if (str.indexOf("PixelFlinger") > -1 || str.indexOf("VideoCore IV HW") <= -1) {
            return 0;
        }
        return 1;
    }

    public void I(Context context, c cVar) {
        String str;
        String str2;
        String str3 = Build.DEVICE;
        String str4 = Build.MODEL;
        String str5 = SystemProperties.get("ro.hardware", null);
        String str6 = Build.BOARD;
        String str7 = Build.BRAND;
        String str8 = Build.MANUFACTURER;
        String str9 = Build.ID;
        String str10 = Build.VERSION.SDK;
        String str11 = null;
        byte[] bArr = new byte[1024];
        int a2 = n.a(new File("/proc/version"), bArr, 1024);
        if (a2 > 0) {
            str11 = new String(bArr, 0, a2);
        }
        String b2 = b();
        ArrayList arrayList = new ArrayList();
        arrayList.add(s.a(str3));
        arrayList.add(s.a(str4));
        arrayList.add(s.a(str5));
        arrayList.add(s.a(str6));
        arrayList.add(s.a(str7));
        arrayList.add(s.a(str8));
        arrayList.add(s.a(str9));
        arrayList.add(s.a(str10));
        arrayList.add(s.a(str11));
        arrayList.add(s.a(b2));
        String str12 = SystemProperties.get("romzj.rom.id", null);
        String str13 = SystemProperties.get("romzj.rom.version", null);
        String str14 = SystemProperties.get("romzj.rom.version.code", null);
        arrayList.add(s.a(str12));
        arrayList.add(s.a(str13));
        arrayList.add(s.a(str14));
        String str15 = null;
        o.a().a(context);
        Map<String, String> b3 = o.a().b();
        if (b3 != null) {
            String b4 = o.a().b(context);
            str15 = b3.get("rombrand");
            str = b3.get("romversion");
            str2 = b4;
        } else {
            str = null;
            str2 = null;
        }
        arrayList.add(s.a(str15));
        arrayList.add(s.a(str));
        String str16 = SystemProperties.get("ro.build.display.id", null);
        String str17 = SystemProperties.get("ro.build.version.incremental", null);
        String str18 = SystemProperties.get("ro.build.fingerprint", null);
        String deviceId = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        arrayList.add(s.a(str16));
        arrayList.add(s.a(str17));
        arrayList.add(s.a(str18));
        arrayList.add(s.a(deviceId));
        arrayList.add(s.a(str2));
        cVar.a(arrayList);
        cVar.a(0);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:136:0x0405, code lost:
        r1 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x031f, code lost:
        r1 = th;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:120:0x03c7  */
    /* JADX WARNING: Removed duplicated region for block: B:121:0x03d3  */
    /* JADX WARNING: Removed duplicated region for block: B:129:0x03f2  */
    /* JADX WARNING: Removed duplicated region for block: B:130:0x03f5  */
    /* JADX WARNING: Removed duplicated region for block: B:131:0x03f8  */
    /* JADX WARNING: Removed duplicated region for block: B:132:0x03fb  */
    /* JADX WARNING: Removed duplicated region for block: B:138:0x040b  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0108  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x014a  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x017c  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x01b1  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x01d1  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x01f0  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x0233  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x023d  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x0247  */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x031f A[ExcHandler: all (th java.lang.Throwable), Splitter:B:72:0x02ad] */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x032c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void J(android.content.Context r16, com.qq.g.c r17) {
        /*
            r15 = this;
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>()
            r1 = 1
            byte[] r1 = com.qq.AppService.s.a(r1)
            r5.add(r1)
            java.lang.String r1 = b(r16)
            java.lang.String r2 = a(r16)
            byte[] r1 = com.qq.AppService.s.a(r1)
            r5.add(r1)
            byte[] r1 = com.qq.AppService.s.a(r2)
            r5.add(r1)
            boolean r1 = com.qq.AppService.s.b(r2)
            if (r1 == 0) goto L_0x0269
            r1 = 0
        L_0x002a:
            byte[] r1 = com.qq.AppService.s.a(r1)
            r5.add(r1)
            java.lang.String r1 = android.os.Build.BRAND
            byte[] r1 = com.qq.AppService.s.a(r1)
            r5.add(r1)
            java.lang.String r1 = android.os.Build.MODEL
            byte[] r1 = com.qq.AppService.s.a(r1)
            r5.add(r1)
            java.lang.String r1 = android.os.Build.VERSION.RELEASE
            byte[] r1 = com.qq.AppService.s.a(r1)
            r5.add(r1)
            java.lang.String r1 = android.os.Build.VERSION.SDK
            byte[] r1 = com.qq.AppService.s.a(r1)
            r5.add(r1)
            r2 = 0
            java.lang.String r1 = "phone"
            r0 = r16
            java.lang.Object r1 = r0.getSystemService(r1)     // Catch:{ Exception -> 0x026c }
            android.telephony.TelephonyManager r1 = (android.telephony.TelephonyManager) r1     // Catch:{ Exception -> 0x026c }
        L_0x0060:
            if (r1 == 0) goto L_0x0288
            java.lang.String r2 = r1.getDeviceId()
            byte[] r2 = com.qq.AppService.s.a(r2)
            r5.add(r2)
            java.lang.String r2 = r1.getDeviceSoftwareVersion()
            byte[] r2 = com.qq.AppService.s.a(r2)
            r5.add(r2)
            java.lang.String r2 = r1.getSimSerialNumber()
            byte[] r2 = com.qq.AppService.s.a(r2)
            r5.add(r2)
            int r2 = r1.getPhoneType()
            r3 = 2
            if (r2 == r3) goto L_0x0091
            int r2 = r1.getPhoneType()
            r3 = 1
            if (r2 != r3) goto L_0x0270
        L_0x0091:
            java.lang.String r2 = r1.getNetworkOperator()
            byte[] r2 = com.qq.AppService.s.a(r2)
            r5.add(r2)
            java.lang.String r2 = r1.getNetworkOperatorName()
            byte[] r2 = com.qq.AppService.s.a(r2)
            r5.add(r2)
        L_0x00a7:
            int r1 = r1.getNetworkType()
            byte[] r1 = com.qq.AppService.s.a(r1)
            r5.add(r1)
        L_0x00b2:
            java.lang.String r1 = com.qq.AppService.PowerReceiver.f207a
            byte[] r1 = com.qq.AppService.s.a(r1)
            r5.add(r1)
            int r1 = com.qq.AppService.PowerReceiver.b
            byte[] r1 = com.qq.AppService.s.a(r1)
            r5.add(r1)
            boolean r1 = com.qq.AppService.AppService.e
            if (r1 != 0) goto L_0x02a8
            int r1 = r15.d
            if (r1 <= 0) goto L_0x02a8
            int r1 = r15.e
            if (r1 <= 0) goto L_0x02a8
            int r1 = r15.e
            byte[] r1 = com.qq.AppService.s.a(r1)
            r5.add(r1)
            int r1 = r15.d
            byte[] r1 = com.qq.AppService.s.a(r1)
            r5.add(r1)
        L_0x00e2:
            java.io.File r1 = android.os.Environment.getExternalStorageDirectory()
            java.lang.String r1 = r1.getAbsolutePath()
            byte[] r1 = com.qq.AppService.s.a(r1)
            r5.add(r1)
            java.lang.String r1 = com.qq.provider.n.c()
            byte[] r2 = com.qq.AppService.s.a(r1)
            r5.add(r2)
            java.lang.String r2 = android.os.Environment.getExternalStorageState()
            java.lang.String r3 = "mounted"
            boolean r3 = r2.equals(r3)
            if (r3 == 0) goto L_0x032c
            r2 = 0
            byte[] r2 = com.qq.AppService.s.a(r2)
            r5.add(r2)
        L_0x0110:
            java.io.File r2 = android.os.Environment.getRootDirectory()
            android.os.StatFs r3 = new android.os.StatFs
            java.lang.String r2 = r2.getPath()
            r3.<init>(r2)
            int r2 = r3.getBlockSize()
            long r6 = (long) r2
            int r2 = r3.getBlockCount()
            long r8 = (long) r2
            long r8 = r8 * r6
            r10 = 1024(0x400, double:5.06E-321)
            long r8 = r8 / r10
            int r2 = (int) r8
            byte[] r2 = com.qq.AppService.s.a(r2)
            r5.add(r2)
            int r2 = r3.getAvailableBlocks()
            long r2 = (long) r2
            long r2 = r2 * r6
            r6 = 1024(0x400, double:5.06E-321)
            long r2 = r2 / r6
            int r2 = (int) r2
            byte[] r2 = com.qq.AppService.s.a(r2)
            r5.add(r2)
            java.io.File r2 = com.qq.provider.n.b()
            if (r2 == 0) goto L_0x03c7
            android.os.StatFs r3 = new android.os.StatFs
            java.lang.String r2 = r2.getAbsolutePath()
            r3.<init>(r2)
            int r2 = r3.getBlockSize()
            long r6 = (long) r2
            int r2 = r3.getBlockCount()
            long r8 = (long) r2
            long r8 = r8 * r6
            r10 = 1024(0x400, double:5.06E-321)
            long r8 = r8 / r10
            int r2 = (int) r8
            byte[] r2 = com.qq.AppService.s.a(r2)
            r5.add(r2)
            int r2 = r3.getAvailableBlocks()
            long r2 = (long) r2
            long r2 = r2 * r6
            r6 = 1024(0x400, double:5.06E-321)
            long r2 = r2 / r6
            int r2 = (int) r2
            byte[] r2 = com.qq.AppService.s.a(r2)
            r5.add(r2)
        L_0x017a:
            if (r1 == 0) goto L_0x03d3
            android.os.StatFs r2 = new android.os.StatFs
            r2.<init>(r1)
            int r1 = r2.getBlockSize()
            long r3 = (long) r1
            int r1 = r2.getBlockCount()
            long r6 = (long) r1
            long r6 = r6 * r3
            r8 = 1024(0x400, double:5.06E-321)
            long r6 = r6 / r8
            int r1 = (int) r6
            byte[] r1 = com.qq.AppService.s.a(r1)
            r5.add(r1)
            int r1 = r2.getAvailableBlocks()
            long r1 = (long) r1
            long r1 = r1 * r3
            r3 = 1024(0x400, double:5.06E-321)
            long r1 = r1 / r3
            int r1 = (int) r1
            byte[] r1 = com.qq.AppService.s.a(r1)
            r5.add(r1)
        L_0x01a8:
            r2 = 0
            r3 = 0
            r4 = 0
            int r1 = com.qq.util.j.c
            r6 = 11
            if (r1 < r6) goto L_0x01cb
            java.lang.Class<android.os.Environment> r1 = android.os.Environment.class
            java.lang.String r6 = "isExternalStorageEmulated"
            r7 = 0
            java.lang.Class[] r7 = new java.lang.Class[r7]     // Catch:{ Exception -> 0x03df }
            java.lang.reflect.Method r1 = r1.getMethod(r6, r7)     // Catch:{ Exception -> 0x03df }
            r6 = 0
            r7 = 0
            java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ Exception -> 0x03df }
            java.lang.Object r1 = r1.invoke(r6, r7)     // Catch:{ Exception -> 0x03df }
            java.lang.Boolean r1 = (java.lang.Boolean) r1     // Catch:{ Exception -> 0x03df }
            boolean r1 = r1.booleanValue()     // Catch:{ Exception -> 0x03df }
            r2 = r1
        L_0x01cb:
            int r1 = com.qq.util.j.c
            r6 = 9
            if (r1 < r6) goto L_0x03e9
            java.lang.Class<android.os.Environment> r1 = android.os.Environment.class
            java.lang.String r6 = "isExternalStorageRemovable"
            r7 = 0
            java.lang.Class[] r7 = new java.lang.Class[r7]     // Catch:{ Exception -> 0x03e5 }
            java.lang.reflect.Method r1 = r1.getMethod(r6, r7)     // Catch:{ Exception -> 0x03e5 }
            r6 = 0
            r7 = 0
            java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ Exception -> 0x03e5 }
            java.lang.Object r1 = r1.invoke(r6, r7)     // Catch:{ Exception -> 0x03e5 }
            java.lang.Boolean r1 = (java.lang.Boolean) r1     // Catch:{ Exception -> 0x03e5 }
            boolean r1 = r1.booleanValue()     // Catch:{ Exception -> 0x03e5 }
        L_0x01ea:
            int r3 = com.qq.util.j.c
            r6 = 8
            if (r3 < r6) goto L_0x040b
            java.io.File r6 = com.qq.provider.n.b()
            java.lang.String r7 = com.qq.provider.n.c()
            if (r6 == 0) goto L_0x0408
            if (r2 != 0) goto L_0x0408
            r4 = 1
            r3 = r4
        L_0x01fe:
            if (r3 != 0) goto L_0x0231
            if (r7 == 0) goto L_0x0231
            if (r6 == 0) goto L_0x03ef
            if (r2 == 0) goto L_0x03ef
            android.os.StatFs r3 = new android.os.StatFs
            r3.<init>(r7)
            int r4 = r3.getBlockSize()
            long r7 = (long) r4
            int r3 = r3.getAvailableBlocks()
            long r3 = (long) r3
            android.os.StatFs r9 = new android.os.StatFs
            java.lang.String r6 = r6.getAbsolutePath()
            r9.<init>(r6)
            int r6 = r9.getBlockSize()
            long r10 = (long) r6
            int r6 = r9.getAvailableBlocks()
            long r12 = (long) r6
            int r6 = (r7 > r10 ? 1 : (r7 == r10 ? 0 : -1))
            if (r6 != 0) goto L_0x03ec
            int r3 = (r3 > r12 ? 1 : (r3 == r12 ? 0 : -1))
            if (r3 != 0) goto L_0x03ec
            r3 = 0
        L_0x0231:
            if (r2 == 0) goto L_0x03f2
            r2 = 1
        L_0x0234:
            byte[] r2 = com.qq.AppService.s.a(r2)
            r5.add(r2)
            if (r1 == 0) goto L_0x03f5
            r1 = 1
        L_0x023e:
            byte[] r1 = com.qq.AppService.s.a(r1)
            r5.add(r1)
            if (r3 == 0) goto L_0x03f8
            r1 = 1
        L_0x0248:
            byte[] r1 = com.qq.AppService.s.a(r1)
            r5.add(r1)
            r1 = 0
            java.lang.String r2 = com.qq.c.d.a(r16)
            if (r2 != 0) goto L_0x03fb
        L_0x0256:
            byte[] r1 = com.qq.AppService.s.a(r1)
            r5.add(r1)
            r0 = r17
            r0.a(r5)
            r1 = 0
            r0 = r17
            r0.a(r1)
            return
        L_0x0269:
            r1 = 1
            goto L_0x002a
        L_0x026c:
            r1 = move-exception
            r1 = r2
            goto L_0x0060
        L_0x0270:
            java.lang.String r2 = r1.getNetworkOperator()
            byte[] r2 = com.qq.AppService.s.a(r2)
            r5.add(r2)
            java.lang.String r2 = r1.getNetworkOperatorName()
            byte[] r2 = com.qq.AppService.s.a(r2)
            r5.add(r2)
            goto L_0x00a7
        L_0x0288:
            byte[] r1 = com.qq.AppService.s.hr
            r5.add(r1)
            byte[] r1 = com.qq.AppService.s.hr
            r5.add(r1)
            byte[] r1 = com.qq.AppService.s.hr
            r5.add(r1)
            byte[] r1 = com.qq.AppService.s.hr
            r5.add(r1)
            byte[] r1 = com.qq.AppService.s.hr
            r5.add(r1)
            byte[] r1 = com.qq.AppService.s.hs
            r5.add(r1)
            goto L_0x00b2
        L_0x02a8:
            r2 = 0
            java.lang.String r1 = "power"
            r0 = r16
            java.lang.Object r1 = r0.getSystemService(r1)     // Catch:{ Exception -> 0x0300, all -> 0x031f }
            android.os.PowerManager r1 = (android.os.PowerManager) r1     // Catch:{ Exception -> 0x0300, all -> 0x031f }
            r3 = 805306394(0x3000001a, float:4.6566273E-10)
            java.lang.String r4 = "lock"
            android.os.PowerManager$WakeLock r2 = r1.newWakeLock(r3, r4)     // Catch:{ Exception -> 0x0300, all -> 0x031f }
            r2.acquire()     // Catch:{ Exception -> 0x0404, all -> 0x031f }
            java.lang.String r1 = "window"
            r0 = r16
            java.lang.Object r1 = r0.getSystemService(r1)     // Catch:{ Exception -> 0x0404, all -> 0x031f }
            android.view.WindowManager r1 = (android.view.WindowManager) r1     // Catch:{ Exception -> 0x0404, all -> 0x031f }
            android.view.Display r1 = r1.getDefaultDisplay()     // Catch:{ Exception -> 0x0404, all -> 0x031f }
            android.util.DisplayMetrics r3 = new android.util.DisplayMetrics     // Catch:{ Exception -> 0x0404, all -> 0x031f }
            r3.<init>()     // Catch:{ Exception -> 0x0404, all -> 0x031f }
            r1.getMetrics(r3)     // Catch:{ Exception -> 0x0404, all -> 0x031f }
            int r3 = r1.getWidth()     // Catch:{ Exception -> 0x0404, all -> 0x031f }
            r15.e = r3     // Catch:{ Exception -> 0x0404, all -> 0x031f }
            int r1 = r1.getHeight()     // Catch:{ Exception -> 0x0404, all -> 0x031f }
            r15.d = r1     // Catch:{ Exception -> 0x0404, all -> 0x031f }
            int r1 = r15.e     // Catch:{ Exception -> 0x0404, all -> 0x031f }
            byte[] r1 = com.qq.AppService.s.a(r1)     // Catch:{ Exception -> 0x0404, all -> 0x031f }
            r5.add(r1)     // Catch:{ Exception -> 0x0404, all -> 0x031f }
            int r1 = r15.d     // Catch:{ Exception -> 0x0404, all -> 0x031f }
            byte[] r1 = com.qq.AppService.s.a(r1)     // Catch:{ Exception -> 0x0404, all -> 0x031f }
            r5.add(r1)     // Catch:{ Exception -> 0x0404, all -> 0x031f }
            if (r2 == 0) goto L_0x00e2
            boolean r1 = r2.isHeld()
            if (r1 == 0) goto L_0x00e2
            r2.release()
            goto L_0x00e2
        L_0x0300:
            r1 = move-exception
            r1 = r2
        L_0x0302:
            r2 = 0
            byte[] r2 = com.qq.AppService.s.a(r2)     // Catch:{ all -> 0x03fe }
            r5.add(r2)     // Catch:{ all -> 0x03fe }
            r2 = 0
            byte[] r2 = com.qq.AppService.s.a(r2)     // Catch:{ all -> 0x03fe }
            r5.add(r2)     // Catch:{ all -> 0x03fe }
            if (r1 == 0) goto L_0x00e2
            boolean r2 = r1.isHeld()
            if (r2 == 0) goto L_0x00e2
            r1.release()
            goto L_0x00e2
        L_0x031f:
            r1 = move-exception
        L_0x0320:
            if (r2 == 0) goto L_0x032b
            boolean r3 = r2.isHeld()
            if (r3 == 0) goto L_0x032b
            r2.release()
        L_0x032b:
            throw r1
        L_0x032c:
            java.lang.String r3 = "mounted_ro"
            boolean r3 = r2.equals(r3)
            if (r3 == 0) goto L_0x033e
            r2 = 1
            byte[] r2 = com.qq.AppService.s.a(r2)
            r5.add(r2)
            goto L_0x0110
        L_0x033e:
            java.lang.String r3 = "shared"
            boolean r3 = r2.equals(r3)
            if (r3 == 0) goto L_0x0350
            r2 = 2
            byte[] r2 = com.qq.AppService.s.a(r2)
            r5.add(r2)
            goto L_0x0110
        L_0x0350:
            java.lang.String r3 = "nofs"
            boolean r3 = r2.equals(r3)
            if (r3 == 0) goto L_0x0362
            r2 = 3
            byte[] r2 = com.qq.AppService.s.a(r2)
            r5.add(r2)
            goto L_0x0110
        L_0x0362:
            java.lang.String r3 = "removed"
            boolean r3 = r2.equals(r3)
            if (r3 == 0) goto L_0x0374
            r2 = 4
            byte[] r2 = com.qq.AppService.s.a(r2)
            r5.add(r2)
            goto L_0x0110
        L_0x0374:
            java.lang.String r3 = "unmounted"
            boolean r3 = r2.equals(r3)
            if (r3 == 0) goto L_0x0386
            r2 = 5
            byte[] r2 = com.qq.AppService.s.a(r2)
            r5.add(r2)
            goto L_0x0110
        L_0x0386:
            java.lang.String r3 = "unmountable"
            boolean r3 = r2.equals(r3)
            if (r3 == 0) goto L_0x0398
            r2 = 6
            byte[] r2 = com.qq.AppService.s.a(r2)
            r5.add(r2)
            goto L_0x0110
        L_0x0398:
            java.lang.String r3 = "checking"
            boolean r3 = r2.equals(r3)
            if (r3 == 0) goto L_0x03aa
            r2 = 7
            byte[] r2 = com.qq.AppService.s.a(r2)
            r5.add(r2)
            goto L_0x0110
        L_0x03aa:
            java.lang.String r3 = "bad_removal"
            boolean r2 = r2.equals(r3)
            if (r2 == 0) goto L_0x03bd
            r2 = 8
            byte[] r2 = com.qq.AppService.s.a(r2)
            r5.add(r2)
            goto L_0x0110
        L_0x03bd:
            r2 = -1
            byte[] r2 = com.qq.AppService.s.a(r2)
            r5.add(r2)
            goto L_0x0110
        L_0x03c7:
            byte[] r2 = com.qq.AppService.s.hs
            r5.add(r2)
            byte[] r2 = com.qq.AppService.s.hs
            r5.add(r2)
            goto L_0x017a
        L_0x03d3:
            byte[] r1 = com.qq.AppService.s.hs
            r5.add(r1)
            byte[] r1 = com.qq.AppService.s.hs
            r5.add(r1)
            goto L_0x01a8
        L_0x03df:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x01cb
        L_0x03e5:
            r1 = move-exception
            r1.printStackTrace()
        L_0x03e9:
            r1 = r3
            goto L_0x01ea
        L_0x03ec:
            r3 = 1
            goto L_0x0231
        L_0x03ef:
            r3 = 1
            goto L_0x0231
        L_0x03f2:
            r2 = 0
            goto L_0x0234
        L_0x03f5:
            r1 = 0
            goto L_0x023e
        L_0x03f8:
            r1 = 0
            goto L_0x0248
        L_0x03fb:
            r1 = 2
            goto L_0x0256
        L_0x03fe:
            r2 = move-exception
            r14 = r2
            r2 = r1
            r1 = r14
            goto L_0x0320
        L_0x0404:
            r1 = move-exception
            r1 = r2
            goto L_0x0302
        L_0x0408:
            r3 = r4
            goto L_0x01fe
        L_0x040b:
            r3 = r4
            goto L_0x0231
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qq.provider.ab.J(android.content.Context, com.qq.g.c):void");
    }

    private static boolean a(File file) {
        int i;
        if (!file.exists() || file.length() < 8 || file.length() > 256) {
            return false;
        }
        byte[] bArr = new byte[Process.PROC_COMBINE];
        FileInputStream fileInputStream = null;
        try {
            fileInputStream = new FileInputStream(file);
        } catch (FileNotFoundException e2) {
        }
        if (fileInputStream == null) {
            return false;
        }
        try {
            i = fileInputStream.read(bArr, 0, Process.PROC_COMBINE);
        } catch (IOException e3) {
            e3.printStackTrace();
            i = 0;
        }
        try {
            fileInputStream.close();
        } catch (IOException e4) {
            e4.printStackTrace();
        }
        if (i < 8) {
            return false;
        }
        if (s.b(new String(bArr, 0, i))) {
        }
        return true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0077  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0098 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x009f  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00c1  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String b(android.content.Context r12) {
        /*
            r10 = 8
            r9 = 255(0xff, float:3.57E-43)
            r3 = 0
            java.io.File r1 = com.qq.provider.n.b()
            r0 = 0
            java.io.File r5 = new java.io.File
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            android.content.pm.ApplicationInfo r4 = r12.getApplicationInfo()
            java.lang.String r4 = r4.dataDir
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r4 = "/.qm_guid"
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r2 = r2.toString()
            r5.<init>(r2)
            if (r1 == 0) goto L_0x0046
            java.io.File r0 = new java.io.File
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r1 = r1.getAbsolutePath()
            java.lang.StringBuilder r1 = r2.append(r1)
            java.lang.String r2 = "/.qm_guid"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
        L_0x0046:
            r1 = 256(0x100, float:3.59E-43)
            byte[] r6 = new byte[r1]
            if (r0 == 0) goto L_0x013b
            boolean r2 = r0.exists()
            boolean r1 = r5.exists()
            if (r2 == 0) goto L_0x01ac
            long r7 = r0.length()
            int r4 = (r7 > r10 ? 1 : (r7 == r10 ? 0 : -1))
            if (r4 >= 0) goto L_0x0062
            r0.delete()
            r2 = r3
        L_0x0062:
            if (r2 == 0) goto L_0x01af
            boolean r2 = a(r0)
            if (r2 != 0) goto L_0x01ac
            r0.delete()
            java.lang.String r2 = "com.qq.connect"
            java.lang.String r4 = "rm illegal sd guid!!!"
            android.util.Log.d(r2, r4)
            r4 = r3
        L_0x0075:
            if (r1 == 0) goto L_0x01a6
            long r7 = r5.length()
            int r2 = (r7 > r10 ? 1 : (r7 == r10 ? 0 : -1))
            if (r2 >= 0) goto L_0x0083
            r5.delete()
            r1 = r3
        L_0x0083:
            if (r1 == 0) goto L_0x01a9
            boolean r1 = a(r5)
            if (r1 != 0) goto L_0x01a6
            r5.delete()
            java.lang.String r1 = "com.qq.connect"
            java.lang.String r2 = "rm illegal in guid!!!"
            android.util.Log.d(r1, r2)
            r2 = r3
        L_0x0096:
            if (r4 == 0) goto L_0x00b2
            if (r2 != 0) goto L_0x00b2
            com.qq.provider.n.b(r0, r5)     // Catch:{ Throwable -> 0x00ba }
        L_0x009d:
            if (r2 == 0) goto L_0x00bf
            java.lang.String r1 = "com.qq.connect"
            java.lang.String r2 = "using guid in !"
            android.util.Log.d(r1, r2)
            int r1 = com.qq.provider.n.a(r5, r6, r9)
            if (r1 <= 0) goto L_0x00bf
            java.lang.String r0 = new java.lang.String
            r0.<init>(r6, r3, r1)
        L_0x00b1:
            return r0
        L_0x00b2:
            if (r2 == 0) goto L_0x009d
            if (r4 != 0) goto L_0x009d
            com.qq.provider.n.b(r5, r0)     // Catch:{ Throwable -> 0x00ba }
            goto L_0x009d
        L_0x00ba:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x009d
        L_0x00bf:
            if (r4 == 0) goto L_0x00d4
            java.lang.String r1 = "com.qq.connect"
            java.lang.String r2 = "using guid sd !"
            android.util.Log.d(r1, r2)
            int r1 = com.qq.provider.n.a(r0, r6, r9)
            if (r1 <= 0) goto L_0x00d4
            java.lang.String r0 = new java.lang.String
            r0.<init>(r6, r3, r1)
            goto L_0x00b1
        L_0x00d4:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            long r2 = java.lang.System.currentTimeMillis()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "{"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.util.UUID r2 = java.util.UUID.randomUUID()
            java.lang.String r2 = r2.toString()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "}"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "129"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            java.lang.String r2 = "com.qq.connect"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "create hash:"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r3 = r3.append(r1)
            java.lang.String r3 = r3.toString()
            android.util.Log.d(r2, r3)
            r5.createNewFile()     // Catch:{ IOException -> 0x0131 }
        L_0x011e:
            r0.createNewFile()     // Catch:{ IOException -> 0x0136 }
        L_0x0121:
            byte[] r2 = r1.getBytes()
            com.qq.provider.n.a(r5, r2)
            byte[] r2 = r1.getBytes()
            com.qq.provider.n.a(r0, r2)
            r0 = r1
            goto L_0x00b1
        L_0x0131:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x011e
        L_0x0136:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0121
        L_0x013b:
            boolean r0 = r5.exists()
            if (r0 == 0) goto L_0x014e
            int r1 = com.qq.provider.n.a(r5, r6, r9)
            if (r1 <= 0) goto L_0x014e
            java.lang.String r0 = new java.lang.String
            r0.<init>(r6, r3, r1)
            goto L_0x00b1
        L_0x014e:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            long r1 = java.lang.System.currentTimeMillis()
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "{"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.util.UUID r1 = java.util.UUID.randomUUID()
            java.lang.String r1 = r1.toString()
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "}"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "129"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            java.lang.String r1 = "com.qq.connect"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "create hash:"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r0)
            java.lang.String r2 = r2.toString()
            android.util.Log.d(r1, r2)
            r5.createNewFile()     // Catch:{ IOException -> 0x01a1 }
        L_0x0198:
            byte[] r1 = r0.getBytes()
            com.qq.provider.n.a(r5, r1)
            goto L_0x00b1
        L_0x01a1:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0198
        L_0x01a6:
            r2 = r1
            goto L_0x0096
        L_0x01a9:
            r2 = r1
            goto L_0x0096
        L_0x01ac:
            r4 = r2
            goto L_0x0075
        L_0x01af:
            r4 = r2
            goto L_0x0075
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qq.provider.ab.b(android.content.Context):java.lang.String");
    }

    public static String b() {
        FileReader fileReader;
        String str;
        Exception e2;
        int indexOf;
        String str2 = null;
        try {
            fileReader = new FileReader("/proc/cpuinfo");
        } catch (FileNotFoundException e3) {
            e3.printStackTrace();
            fileReader = null;
        }
        if (fileReader != null) {
            LineNumberReader lineNumberReader = new LineNumberReader(fileReader);
            try {
                str = null;
                String readLine = lineNumberReader.readLine();
                while (readLine != null) {
                    try {
                        if (readLine.indexOf("Hardware") >= 0 && (indexOf = readLine.indexOf(":")) > 0) {
                            str = readLine.substring(indexOf + 2);
                        }
                        readLine = lineNumberReader.readLine();
                    } catch (Exception e4) {
                        e2 = e4;
                        e2.printStackTrace();
                        str2 = str;
                        lineNumberReader.close();
                        fileReader.close();
                        return str2;
                    }
                }
                str2 = str;
            } catch (Exception e5) {
                Exception exc = e5;
                str = null;
                e2 = exc;
                e2.printStackTrace();
                str2 = str;
                lineNumberReader.close();
                fileReader.close();
                return str2;
            }
            try {
                lineNumberReader.close();
            } catch (IOException e6) {
                e6.printStackTrace();
            }
            try {
                fileReader.close();
            } catch (IOException e7) {
                e7.printStackTrace();
            }
        }
        return str2;
    }
}
