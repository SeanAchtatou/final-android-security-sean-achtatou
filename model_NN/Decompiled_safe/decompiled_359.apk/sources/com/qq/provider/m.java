package com.qq.provider;

import com.qq.AppService.s;
import com.qq.g.c;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class m {

    /* renamed from: a  reason: collision with root package name */
    public volatile long f345a = 0;
    public long b = 0;
    public BufferedInputStream c = null;
    public BufferedOutputStream d = null;
    public String e = null;
    public File f = null;
    private RandomAccessFile g = null;
    private c h = null;
    private byte[] i = null;
    private byte[] j = new byte[16384];

    public m(BufferedInputStream bufferedInputStream, BufferedOutputStream bufferedOutputStream) {
        this.c = bufferedInputStream;
        this.d = bufferedOutputStream;
    }

    public void a() {
        for (int i2 = 0; i2 < 2000 && this.c.available() < 4; i2++) {
            Thread.sleep(25);
        }
    }

    public int b() {
        this.h = new c();
        byte[] bArr = new byte[4];
        if (this.c.read(bArr) < 4) {
            return -1;
        }
        int a2 = s.a(bArr);
        this.i = new byte[4];
        if (this.c.read(this.i) < 4) {
            return -1;
        }
        this.h.b = new byte[a2];
        System.arraycopy(bArr, 0, this.h.b, 0, 4);
        System.arraycopy(this.i, 0, this.h.b, 4, 4);
        int i2 = 8;
        int i3 = 0;
        while (i3 < 1000 && i2 < a2) {
            int read = this.c.read(this.h.b, i2, a2 - i2);
            if (read > 0) {
                i2 += read;
                i3 = 0;
            }
            i3++;
        }
        this.h.c();
        return a2;
    }

    public int c() {
        if (this.h == null || this.h.b == null || this.i == null) {
            return -1;
        }
        if (s.b(this.i, s.dF)) {
            g();
        } else if (s.b(this.i, s.dG)) {
            a(this.h);
        } else if (s.b(this.i, s.dH)) {
            b(this.h);
        } else if (s.b(this.i, s.dI)) {
            return c(this.h);
        } else {
            if (s.b(this.i, s.dJ)) {
                d(this.h);
                return -1;
            }
        }
        return 0;
    }

    public void d() {
        if (this.d != null) {
            this.d.write(s.a(8));
            this.d.write(s.a(0));
            this.d.flush();
        }
    }

    public void a(int i2) {
        int i3;
        this.d.write(s.a(this.h.a(), this.h.b()));
        this.d.flush();
        if (i2 > 0) {
            this.d.write(s.a(i2));
            this.d.flush();
            int i4 = 0;
            while (i4 < i2) {
                int read = this.g.read(this.j, 0, i2 - i4 >= this.j.length ? this.j.length : i2 - i4);
                if (read <= -1) {
                    break;
                }
                if (read > 0) {
                    this.d.write(this.j, 0, read);
                    i3 = read + i4;
                } else {
                    i3 = i4;
                }
                i4 = i3;
            }
            if (i4 > 0) {
                this.f345a = ((long) i4) + this.f345a;
            }
            this.d.flush();
        }
    }

    /* JADX INFO: finally extract failed */
    public void e() {
        try {
            d();
            while (true) {
                a();
                if (b() >= 0) {
                    int c2 = c();
                    if (c2 < 0) {
                        break;
                    }
                    a(c2);
                } else {
                    break;
                }
            }
            if (this.g != null) {
                try {
                    this.g.close();
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
                this.g = null;
            }
        } catch (Exception e3) {
            e3.printStackTrace();
            if (this.g != null) {
                try {
                    this.g.close();
                } catch (IOException e4) {
                    e4.printStackTrace();
                }
            }
        } catch (Throwable th) {
            if (this.g != null) {
                try {
                    this.g.close();
                } catch (IOException e5) {
                    e5.printStackTrace();
                }
                this.g = null;
            }
            throw th;
        }
    }

    public void f() {
        if (this.g != null) {
            try {
                this.g.close();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
            this.g = null;
        }
    }

    public void g() {
        if (this.h.e() < 1) {
            this.h.a(1);
            return;
        }
        this.e = this.h.j();
        if (s.b(this.e)) {
            this.h.a(1);
            return;
        }
        this.f = new File(this.e);
        if (this.f == null || !this.f.exists() || this.f.isDirectory()) {
            this.h.a(6);
            return;
        }
        this.b = this.f.length();
        this.f345a = 0;
        f();
        try {
            this.g = new RandomAccessFile(this.f, "r");
            ArrayList arrayList = new ArrayList();
            arrayList.add(s.a(this.b));
            this.h.a(0);
            this.h.a(arrayList);
        } catch (Exception e2) {
            e2.printStackTrace();
            if (this.g != null) {
                try {
                    this.g.close();
                } catch (IOException e3) {
                    e3.printStackTrace();
                }
            }
            this.h.a(8);
        }
    }

    public void a(c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        long i2 = cVar.i();
        if (i2 < 0) {
            cVar.a(1);
        } else if (this.g == null) {
            cVar.a(6);
        } else if (this.f == null || !this.f.exists() || this.f.isDirectory()) {
            cVar.a(6);
        } else if (this.b < i2) {
            cVar.a(6);
        } else {
            try {
                this.g.seek(i2);
                this.f345a = i2;
                cVar.a(0);
            } catch (Exception e2) {
                try {
                    if (this.g != null) {
                        this.g.close();
                    }
                } catch (Exception e3) {
                    e3.printStackTrace();
                }
                e2.printStackTrace();
                cVar.a(8);
            }
        }
    }

    public void b(c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        long i2 = cVar.i();
        if (i2 <= 0) {
            cVar.a(1);
        } else if (this.g == null) {
            cVar.a(6);
        } else if (this.f345a + i2 > this.b) {
            cVar.a(7);
        } else {
            try {
                this.f345a += (long) this.g.skipBytes((int) i2);
                cVar.a(0);
            } catch (Exception e2) {
                e2.printStackTrace();
                cVar.a(8);
            }
        }
    }

    public int c(c cVar) {
        if (cVar.e() < 2) {
            cVar.a(1);
            return 0;
        }
        int h2 = cVar.h();
        if (((long) h2) >= this.b) {
            cVar.a(1);
            return 0;
        }
        if (((long) h2) < this.f345a) {
            this.f345a = (long) h2;
            try {
                this.g.seek((long) h2);
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        } else if (((long) h2) > this.f345a) {
            try {
                this.g.skipBytes((int) (((long) h2) - this.f345a));
            } catch (IOException e3) {
                e3.printStackTrace();
            }
            this.f345a = (long) h2;
        }
        int h3 = cVar.h();
        if (h3 <= 0) {
            cVar.a(1);
            return 0;
        }
        if (((long) h3) + this.f345a > this.b) {
            h3 = (int) (this.b - this.f345a);
        }
        cVar.a(0);
        return h3;
    }

    public void d(c cVar) {
        try {
            d();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        f();
        cVar.a(0);
    }
}
