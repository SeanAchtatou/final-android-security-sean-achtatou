package com.qq.provider;

import android.content.Context;
import android.content.Intent;
import com.qq.AppService.OpenGLActivity;

/* compiled from: ProGuard */
class ad extends Thread {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Context f322a;
    final /* synthetic */ ab b;

    ad(ab abVar, Context context) {
        this.b = abVar;
        this.f322a = context;
    }

    public void run() {
        super.run();
        Intent intent = new Intent();
        intent.setClass(this.f322a, OpenGLActivity.class);
        intent.addFlags(268435456);
        intent.addFlags(134217728);
        this.f322a.startActivity(intent);
    }
}
