package com.qq.provider;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.media.AudioManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Looper;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import com.qq.AppService.s;
import com.qq.d.h.a;
import com.qq.g.c;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class ag {

    /* renamed from: a  reason: collision with root package name */
    private a f325a;

    private ag() {
        this.f325a = null;
        this.f325a = new a();
    }

    public static ag a() {
        return new ag();
    }

    public void a(Context context, c cVar) {
        ArrayList<byte[]> y = y(context);
        if (y.size() == 0) {
            y.add(s.a(0));
        }
        cVar.a(0);
        cVar.a(y);
    }

    public void b(Context context, c cVar) {
        a(cVar, context);
    }

    private void a(c cVar, Context context) {
        Uri uri;
        Uri uri2;
        Uri uri3;
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        if (cVar.e() < (h * 2) + 1) {
            cVar.a(1);
            return;
        }
        int i = 0;
        while (i < h) {
            int h2 = cVar.h();
            int i2 = h2 / 4;
            int i3 = h2 % 4;
            this.f325a.getClass();
            if (i3 >= 0) {
                this.f325a.getClass();
                if (i3 <= 3) {
                    this.f325a.getClass();
                    if (i2 == 1) {
                        int h3 = cVar.h();
                        if (h3 > -1) {
                            a(context, h3);
                        }
                    } else {
                        this.f325a.getClass();
                        if (i2 == 2) {
                            int h4 = cVar.h();
                            if (h4 > -1) {
                                b(context, h4);
                            }
                        } else {
                            this.f325a.getClass();
                            if (i2 == 3) {
                                int h5 = cVar.h();
                                if (h5 > -1) {
                                    c(context, h5);
                                }
                            } else {
                                this.f325a.getClass();
                                if (i2 == 4) {
                                    int h6 = cVar.h();
                                    if (h6 > -1) {
                                        d(context, h6);
                                    }
                                } else {
                                    this.f325a.getClass();
                                    if (i2 == 5) {
                                        int h7 = cVar.h();
                                        if (h7 > -1) {
                                            e(context, h7);
                                        }
                                    } else {
                                        this.f325a.getClass();
                                        if (i2 == 6) {
                                            int h8 = cVar.h();
                                            if (h8 > -1) {
                                                f(context, h8);
                                            }
                                        } else {
                                            this.f325a.getClass();
                                            if (i2 == 7) {
                                                int h9 = cVar.h();
                                                if (h9 > -1) {
                                                    g(context, h9);
                                                }
                                            } else {
                                                this.f325a.getClass();
                                                if (i2 == 8) {
                                                    int h10 = cVar.h();
                                                    if (h10 > -1) {
                                                        a(context, 0, h10);
                                                    }
                                                } else {
                                                    this.f325a.getClass();
                                                    if (i2 == 9) {
                                                        int h11 = cVar.h();
                                                        if (h11 > -1) {
                                                            a(context, 1, h11);
                                                        }
                                                    } else {
                                                        this.f325a.getClass();
                                                        if (i2 == 10) {
                                                            int h12 = cVar.h();
                                                            if (h12 > -1) {
                                                                a(context, 2, h12);
                                                            }
                                                        } else {
                                                            this.f325a.getClass();
                                                            if (i2 == 12) {
                                                                int h13 = cVar.h();
                                                                if (h13 > -1) {
                                                                    a(context, 4, h13);
                                                                }
                                                            } else {
                                                                this.f325a.getClass();
                                                                if (i2 == 11) {
                                                                    int h14 = cVar.h();
                                                                    if (h14 > -1) {
                                                                        a(context, 3, h14);
                                                                    }
                                                                } else {
                                                                    this.f325a.getClass();
                                                                    if (i2 == 13) {
                                                                        int h15 = cVar.h();
                                                                        if (h15 > -1) {
                                                                            a(context, 5, h15);
                                                                        }
                                                                    } else {
                                                                        this.f325a.getClass();
                                                                        if (i2 == 14) {
                                                                            int h16 = cVar.h();
                                                                            if (h16 > -1) {
                                                                                a(context, 8, h16);
                                                                            }
                                                                        } else {
                                                                            this.f325a.getClass();
                                                                            if (i2 == 15) {
                                                                                int h17 = cVar.h();
                                                                                int h18 = cVar.h();
                                                                                if (h17 > 0) {
                                                                                    if (h18 > 0) {
                                                                                        uri3 = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                                                                                    } else {
                                                                                        uri3 = MediaStore.Audio.Media.INTERNAL_CONTENT_URI;
                                                                                    }
                                                                                    RingtoneManager.setActualDefaultRingtoneUri(context, 1, Uri.withAppendedPath(uri3, Constants.STR_EMPTY + h17));
                                                                                }
                                                                            } else {
                                                                                this.f325a.getClass();
                                                                                if (i2 == 16) {
                                                                                    int h19 = cVar.h();
                                                                                    int h20 = cVar.h();
                                                                                    if (h19 > 0) {
                                                                                        if (h20 > 0) {
                                                                                            uri2 = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                                                                                        } else {
                                                                                            uri2 = MediaStore.Audio.Media.INTERNAL_CONTENT_URI;
                                                                                        }
                                                                                        RingtoneManager.setActualDefaultRingtoneUri(context, 2, Uri.withAppendedPath(uri2, Constants.STR_EMPTY + h19));
                                                                                    }
                                                                                } else {
                                                                                    this.f325a.getClass();
                                                                                    if (i2 == 17) {
                                                                                        int h21 = cVar.h();
                                                                                        int h22 = cVar.h();
                                                                                        if (h21 > 0) {
                                                                                            if (h22 > 0) {
                                                                                                uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                                                                                            } else {
                                                                                                uri = MediaStore.Audio.Media.INTERNAL_CONTENT_URI;
                                                                                            }
                                                                                            RingtoneManager.setActualDefaultRingtoneUri(context, 4, Uri.withAppendedPath(uri, Constants.STR_EMPTY + h21));
                                                                                        }
                                                                                    } else {
                                                                                        this.f325a.getClass();
                                                                                        if (i2 == 18) {
                                                                                            int h23 = cVar.h();
                                                                                            if (h23 > -1) {
                                                                                                j(context, h23);
                                                                                            }
                                                                                        } else {
                                                                                            this.f325a.getClass();
                                                                                            if (i2 == 19) {
                                                                                                int h24 = cVar.h();
                                                                                                if (h24 > -1) {
                                                                                                    k(context, h24);
                                                                                                }
                                                                                            } else {
                                                                                                this.f325a.getClass();
                                                                                                if (i2 == 20) {
                                                                                                    int h25 = cVar.h();
                                                                                                    if (h25 > -1) {
                                                                                                        l(context, h25);
                                                                                                    }
                                                                                                } else {
                                                                                                    this.f325a.getClass();
                                                                                                    if (i2 == 21) {
                                                                                                        int h26 = cVar.h();
                                                                                                        if (h26 > -1) {
                                                                                                            m(context, h26);
                                                                                                        }
                                                                                                    } else {
                                                                                                        this.f325a.getClass();
                                                                                                        if (i2 == 22) {
                                                                                                            int h27 = cVar.h();
                                                                                                            if (h27 > -1) {
                                                                                                                n(context, h27);
                                                                                                            }
                                                                                                        } else {
                                                                                                            this.f325a.getClass();
                                                                                                            if (i2 == 23) {
                                                                                                                int h28 = cVar.h();
                                                                                                                if (h28 > -1) {
                                                                                                                    o(context, h28);
                                                                                                                }
                                                                                                            } else {
                                                                                                                this.f325a.getClass();
                                                                                                                if (i2 == 24) {
                                                                                                                    int h29 = cVar.h();
                                                                                                                    if (h29 > -1) {
                                                                                                                        p(context, h29);
                                                                                                                    }
                                                                                                                } else {
                                                                                                                    this.f325a.getClass();
                                                                                                                    if (i2 == 25) {
                                                                                                                        int h30 = cVar.h();
                                                                                                                        if (h30 > -1) {
                                                                                                                            q(context, h30);
                                                                                                                        }
                                                                                                                    } else {
                                                                                                                        this.f325a.getClass();
                                                                                                                        if (i2 == 26) {
                                                                                                                            int h31 = cVar.h();
                                                                                                                            if (h31 > -1) {
                                                                                                                                r(context, h31);
                                                                                                                            }
                                                                                                                        } else {
                                                                                                                            this.f325a.getClass();
                                                                                                                            if (i2 == 27) {
                                                                                                                                int h32 = cVar.h();
                                                                                                                                if (h32 > -1) {
                                                                                                                                    s(context, h32);
                                                                                                                                }
                                                                                                                            } else {
                                                                                                                                this.f325a.getClass();
                                                                                                                                if (i2 == 28) {
                                                                                                                                    int h33 = cVar.h();
                                                                                                                                    if (h33 > -1) {
                                                                                                                                        t(context, h33);
                                                                                                                                    }
                                                                                                                                } else {
                                                                                                                                    this.f325a.getClass();
                                                                                                                                    if (i2 == 29) {
                                                                                                                                        int h34 = cVar.h();
                                                                                                                                        if (h34 > -1) {
                                                                                                                                            u(context, h34);
                                                                                                                                        }
                                                                                                                                    } else {
                                                                                                                                        this.f325a.getClass();
                                                                                                                                        if (i2 == 30) {
                                                                                                                                            int h35 = cVar.h();
                                                                                                                                            if (h35 > -1) {
                                                                                                                                                u(context, h35);
                                                                                                                                            }
                                                                                                                                        } else {
                                                                                                                                            this.f325a.getClass();
                                                                                                                                            if (i2 == 31) {
                                                                                                                                                int h36 = cVar.h();
                                                                                                                                                if (h36 > -1) {
                                                                                                                                                    v(context, h36);
                                                                                                                                                }
                                                                                                                                            } else {
                                                                                                                                                this.f325a.getClass();
                                                                                                                                                if (i2 == 32) {
                                                                                                                                                    int h37 = cVar.h();
                                                                                                                                                    if (h37 > -1) {
                                                                                                                                                        w(context, h37);
                                                                                                                                                    }
                                                                                                                                                } else {
                                                                                                                                                    this.f325a.getClass();
                                                                                                                                                    if (i2 == 33) {
                                                                                                                                                        int h38 = cVar.h();
                                                                                                                                                        if (h38 > -1) {
                                                                                                                                                            x(context, h38);
                                                                                                                                                        }
                                                                                                                                                    } else {
                                                                                                                                                        this.f325a.getClass();
                                                                                                                                                        if (i2 == 34) {
                                                                                                                                                            String j = cVar.j();
                                                                                                                                                            if (j != null) {
                                                                                                                                                                a(context, j);
                                                                                                                                                            }
                                                                                                                                                        } else {
                                                                                                                                                            this.f325a.getClass();
                                                                                                                                                            if (i3 == 3) {
                                                                                                                                                                cVar.g();
                                                                                                                                                                cVar.g();
                                                                                                                                                            } else {
                                                                                                                                                                cVar.g();
                                                                                                                                                            }
                                                                                                                                                        }
                                                                                                                                                    }
                                                                                                                                                }
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                }
                                                                                                                            }
                                                                                                                        }
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    i++;
                }
            }
            cVar.a(1);
            return;
        }
        cVar.a(0);
    }

    private ArrayList<byte[]> y(Context context) {
        int i;
        int i2;
        int i3;
        int i4;
        ArrayList<byte[]> arrayList = new ArrayList<>();
        arrayList.add(s.a(0));
        int a2 = a(context);
        if (a2 > -1) {
            this.f325a.getClass();
            this.f325a.getClass();
            arrayList.add(s.a(a(1, 0)));
            arrayList.add(s.a(a2));
            i = 1;
        } else {
            i = 0;
        }
        int b = b(context);
        if (b > -1) {
            this.f325a.getClass();
            this.f325a.getClass();
            arrayList.add(s.a(a(2, 0)));
            arrayList.add(s.a(b));
            i++;
        }
        int c = c(context);
        if (c > -1) {
            this.f325a.getClass();
            this.f325a.getClass();
            arrayList.add(s.a(a(3, 0)));
            arrayList.add(s.a(c));
            i++;
        }
        int d = d(context);
        if (d > -1) {
            this.f325a.getClass();
            this.f325a.getClass();
            arrayList.add(s.a(a(4, 0)));
            arrayList.add(s.a(d));
            i++;
        }
        int e = e(context);
        if (e > -1) {
            this.f325a.getClass();
            this.f325a.getClass();
            arrayList.add(s.a(a(5, 0)));
            arrayList.add(s.a(e));
            i++;
        }
        int f = f(context);
        if (f > -1) {
            this.f325a.getClass();
            this.f325a.getClass();
            arrayList.add(s.a(a(6, 0)));
            arrayList.add(s.a(f));
            i++;
        }
        int g = g(context);
        if (g > -1) {
            this.f325a.getClass();
            this.f325a.getClass();
            arrayList.add(s.a(a(7, 0)));
            arrayList.add(s.a(g));
            i++;
        }
        int h = h(context, 0);
        if (h > -1) {
            this.f325a.getClass();
            this.f325a.getClass();
            arrayList.add(s.a(a(8, 0)));
            arrayList.add(s.a(h));
            i++;
        }
        int h2 = h(context, 1);
        if (h2 > -1) {
            this.f325a.getClass();
            this.f325a.getClass();
            arrayList.add(s.a(a(9, 0)));
            arrayList.add(s.a(h2));
            i++;
        }
        int h3 = h(context, 2);
        if (h3 > -1) {
            this.f325a.getClass();
            this.f325a.getClass();
            arrayList.add(s.a(a(10, 0)));
            arrayList.add(s.a(h3));
            i++;
        }
        int h4 = h(context, 3);
        if (h4 > -1) {
            this.f325a.getClass();
            this.f325a.getClass();
            arrayList.add(s.a(a(11, 0)));
            arrayList.add(s.a(h4));
            i++;
        }
        int h5 = h(context, 4);
        if (h5 > -1) {
            this.f325a.getClass();
            this.f325a.getClass();
            arrayList.add(s.a(a(12, 0)));
            arrayList.add(s.a(h5));
            i++;
        }
        int h6 = h(context, 5);
        if (h6 > -1) {
            this.f325a.getClass();
            this.f325a.getClass();
            arrayList.add(s.a(a(12, 0)));
            arrayList.add(s.a(h6));
            i++;
        }
        int h7 = h(context, 1);
        if (h7 > -1) {
            this.f325a.getClass();
            this.f325a.getClass();
            arrayList.add(s.a(a(14, 0)));
            arrayList.add(s.a(h7));
            i++;
        }
        Uri i5 = i(context, 1);
        if (i5 != null) {
            try {
                i4 = Integer.parseInt(i5.getLastPathSegment());
            } catch (Exception e2) {
                e2.printStackTrace();
                i4 = 0;
            }
            if (i4 > 0) {
                this.f325a.getClass();
                this.f325a.getClass();
                arrayList.add(s.a(a(15, 3)));
                arrayList.add(s.a(i4));
                if (i5.toString().contains("/external/")) {
                    arrayList.add(s.a(1));
                } else {
                    arrayList.add(s.a(0));
                }
                i++;
            }
        }
        Uri i6 = i(context, 2);
        if (i6 != null) {
            try {
                i3 = Integer.parseInt(i6.getLastPathSegment());
            } catch (Exception e3) {
                e3.printStackTrace();
                i3 = 0;
            }
            if (i3 > 0) {
                this.f325a.getClass();
                this.f325a.getClass();
                arrayList.add(s.a(a(16, 3)));
                arrayList.add(s.a(i3));
                if (i6.toString().contains("/external/")) {
                    arrayList.add(s.a(1));
                } else {
                    arrayList.add(s.a(0));
                }
                i++;
            }
        }
        Uri i7 = i(context, 4);
        if (i7 != null) {
            try {
                i2 = Integer.parseInt(i7.getLastPathSegment());
            } catch (Exception e4) {
                e4.printStackTrace();
                i2 = 0;
            }
            if (i2 > 0) {
                this.f325a.getClass();
                this.f325a.getClass();
                arrayList.add(s.a(a(17, 3)));
                arrayList.add(s.a(i2));
                if (i7.toString().contains("/external/")) {
                    arrayList.add(s.a(1));
                } else {
                    arrayList.add(s.a(0));
                }
                i++;
            }
        }
        int h8 = h(context);
        if (h8 > -1) {
            this.f325a.getClass();
            this.f325a.getClass();
            arrayList.add(s.a(a(18, 0)));
            arrayList.add(s.a(h8));
            i++;
        }
        int i8 = i(context);
        if (i8 > -1) {
            this.f325a.getClass();
            this.f325a.getClass();
            arrayList.add(s.a(a(19, 0)));
            arrayList.add(s.a(i8));
            i++;
        }
        int j = j(context);
        if (j > -1) {
            this.f325a.getClass();
            this.f325a.getClass();
            arrayList.add(s.a(a(20, 0)));
            arrayList.add(s.a(j));
            i++;
        }
        int k = k(context);
        if (k > -1) {
            this.f325a.getClass();
            this.f325a.getClass();
            arrayList.add(s.a(a(21, 0)));
            arrayList.add(s.a(k));
            i++;
        }
        int l = l(context);
        if (l > -1) {
            this.f325a.getClass();
            this.f325a.getClass();
            arrayList.add(s.a(a(22, 0)));
            arrayList.add(s.a(l));
            i++;
        }
        int m = m(context);
        if (m > -1) {
            this.f325a.getClass();
            this.f325a.getClass();
            arrayList.add(s.a(a(23, 0)));
            arrayList.add(s.a(m));
            i++;
        }
        int n = n(context);
        if (n > -1) {
            this.f325a.getClass();
            this.f325a.getClass();
            arrayList.add(s.a(a(24, 0)));
            arrayList.add(s.a(n));
            i++;
        }
        int o = o(context);
        if (o > -1) {
            this.f325a.getClass();
            this.f325a.getClass();
            arrayList.add(s.a(a(25, 0)));
            arrayList.add(s.a(o));
            i++;
        }
        int p = p(context);
        if (p > -1) {
            this.f325a.getClass();
            this.f325a.getClass();
            arrayList.add(s.a(a(26, 0)));
            arrayList.add(s.a(p));
            i++;
        }
        int q = q(context);
        if (q > -1) {
            this.f325a.getClass();
            this.f325a.getClass();
            arrayList.add(s.a(a(27, 0)));
            arrayList.add(s.a(q));
            i++;
        }
        int r = r(context);
        if (r > -1) {
            this.f325a.getClass();
            this.f325a.getClass();
            arrayList.add(s.a(a(28, 0)));
            arrayList.add(s.a(r));
            i++;
        }
        int s = s(context);
        if (s > -1) {
            this.f325a.getClass();
            this.f325a.getClass();
            arrayList.add(s.a(a(29, 0)));
            arrayList.add(s.a(s));
            i++;
        }
        int t = t(context);
        if (t > -1) {
            this.f325a.getClass();
            this.f325a.getClass();
            arrayList.add(s.a(a(30, 0)));
            arrayList.add(s.a(t));
            i++;
        }
        int u = u(context);
        if (u > -1) {
            this.f325a.getClass();
            this.f325a.getClass();
            arrayList.add(s.a(a(31, 0)));
            arrayList.add(s.a(u));
            i++;
        }
        int v = v(context);
        if (v > -1) {
            this.f325a.getClass();
            this.f325a.getClass();
            arrayList.add(s.a(a(32, 0)));
            arrayList.add(s.a(v));
            i++;
        }
        int w = w(context);
        if (w > -1) {
            this.f325a.getClass();
            this.f325a.getClass();
            arrayList.add(s.a(a(33, 0)));
            arrayList.add(s.a(w));
            i++;
        }
        String x = x(context);
        if (x != null) {
            this.f325a.getClass();
            this.f325a.getClass();
            arrayList.add(s.a(a(34, 2)));
            arrayList.add(s.a(x));
            i++;
        }
        arrayList.set(0, s.a(i));
        return arrayList;
    }

    private int a(int i, int i2) {
        this.f325a.getClass();
        if (i < 1) {
            return -1;
        }
        this.f325a.getClass();
        if (i > 34) {
            return -1;
        }
        this.f325a.getClass();
        if (i2 < 0) {
            return -1;
        }
        this.f325a.getClass();
        if (i2 <= 3) {
            return (i * 4) + i2;
        }
        return -1;
    }

    public void a(Context context, int i) {
        int i2;
        boolean z = true;
        if (i > 0) {
            i2 = 1;
        } else {
            i2 = 0;
        }
        Settings.System.putInt(context.getContentResolver(), "airplane_mode_on", i2);
        if (i2 <= 0) {
            z = false;
        }
        Intent intent = new Intent("android.intent.action.AIRPLANE_MODE");
        intent.putExtra("state", z);
        context.sendBroadcast(intent);
    }

    public int a(Context context) {
        try {
            return Settings.System.getInt(context.getContentResolver(), "airplane_mode_on");
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
            return -1;
        }
    }

    public void b(Context context, int i) {
        boolean z;
        if (i > 0) {
            z = true;
        } else {
            z = false;
        }
        ((WifiManager) context.getSystemService("wifi")).setWifiEnabled(z);
    }

    public int b(Context context) {
        return ((WifiManager) context.getSystemService("wifi")).isWifiEnabled() ? 1 : 0;
    }

    public synchronized void c(Context context, int i) {
        Looper.prepare();
        boolean z = false;
        if (i > 0) {
            z = true;
        }
        BluetoothAdapter defaultAdapter = BluetoothAdapter.getDefaultAdapter();
        if (z != defaultAdapter.isEnabled()) {
            if (z) {
                defaultAdapter.enable();
            } else {
                defaultAdapter.disable();
            }
        }
        Looper.myLooper().quit();
    }

    public int c(Context context) {
        return Settings.System.getInt(context.getContentResolver(), "bluetooth_on", -1);
    }

    public void d(Context context, int i) {
        if (i >= 0 && i <= 2) {
            ((AudioManager) context.getSystemService("audio")).setRingerMode(i);
        }
    }

    public int d(Context context) {
        return ((AudioManager) context.getSystemService("audio")).getRingerMode();
    }

    public void e(Context context, int i) {
        if (i >= 0 && i <= 2) {
            ((AudioManager) context.getSystemService("audio")).setVibrateSetting(0, i);
        }
    }

    public int e(Context context) {
        return ((AudioManager) context.getSystemService("audio")).getVibrateSetting(0);
    }

    public void f(Context context, int i) {
        if (i >= 0 && i <= 2) {
            ((AudioManager) context.getSystemService("audio")).setVibrateSetting(1, i);
        }
    }

    public int f(Context context) {
        return ((AudioManager) context.getSystemService("audio")).getVibrateSetting(1);
    }

    public int g(Context context) {
        try {
            return Settings.System.getInt(context.getContentResolver(), "notifications_use_ring_volume");
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    public void g(Context context, int i) {
        Settings.System.putInt(context.getContentResolver(), "notifications_use_ring_volume", i);
    }

    public void a(Context context, int i, int i2) {
        if (i == 0 || i == 1 || i == 2 || i == 3 || i == 4 || i == 5) {
            ((AudioManager) context.getSystemService("audio")).setStreamVolume(i, i2, 0);
        }
    }

    public int h(Context context, int i) {
        if (i == 0 || i == 1 || i == 2 || i == 3 || i == 4 || i == 5) {
            return ((AudioManager) context.getSystemService("audio")).getStreamVolume(i);
        }
        return -1;
    }

    public Uri i(Context context, int i) {
        Cursor query = context.getContentResolver().query(RingtoneManager.getDefaultUri(i), null, null, null, null);
        if (query == null) {
            return null;
        }
        if (!query.moveToFirst()) {
            query.close();
            return null;
        }
        Uri parse = Uri.parse(query.getString(query.getColumnIndex("value")));
        Log.e("com.qq.connect", parse.toString());
        query.close();
        return parse;
    }

    public int h(Context context) {
        try {
            return Settings.System.getInt(context.getContentResolver(), "dtmf_tone");
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    public void j(Context context, int i) {
        int i2;
        if (i > 0) {
            i2 = 1;
        } else {
            i2 = 0;
        }
        Settings.System.putInt(context.getContentResolver(), "dtmf_tone", i2);
    }

    public void k(Context context, int i) {
        int i2;
        if (i > 0) {
            i2 = 1;
        } else {
            i2 = 0;
        }
        Settings.System.putInt(context.getContentResolver(), "sound_effects_enabled", i2);
    }

    public int i(Context context) {
        return Settings.System.getInt(context.getContentResolver(), "sound_effects_enabled", -1);
    }

    public void l(Context context, int i) {
        int i2;
        if (i > 0) {
            i2 = 1;
        } else {
            i2 = 0;
        }
        Settings.System.putInt(context.getContentResolver(), "haptic_feedback_enabled", i2);
    }

    public int j(Context context) {
        return Settings.System.getInt(context.getContentResolver(), "haptic_feedback_enabled", -1);
    }

    public void m(Context context, int i) {
        int i2;
        if (i > 0) {
            i2 = 1;
        } else {
            i2 = 0;
        }
        Settings.System.putInt(context.getContentResolver(), "accelerometer_rotation", i2);
    }

    public int k(Context context) {
        return Settings.System.getInt(context.getContentResolver(), "accelerometer_rotation", -1);
    }

    public void n(Context context, int i) {
        float f;
        if (i > 0) {
            f = 1.0f;
        } else {
            f = 0.0f;
        }
        Settings.System.putFloat(context.getContentResolver(), "window_animation_scale", f);
    }

    public int l(Context context) {
        return (int) Settings.System.getFloat(context.getContentResolver(), "window_animation_scale", -1.0f);
    }

    public void o(Context context, int i) {
        float f;
        if (i > 0) {
            f = 1.0f;
        } else {
            f = 0.0f;
        }
        Settings.System.putFloat(context.getContentResolver(), "transition_animation_scale", f);
    }

    public int m(Context context) {
        return (int) Settings.System.getFloat(context.getContentResolver(), "transition_animation_scale", -1.0f);
    }

    public void p(Context context, int i) {
        Settings.System.putInt(context.getContentResolver(), "screen_brightness_mode", i);
    }

    public int n(Context context) {
        return Settings.System.getInt(context.getContentResolver(), "screen_brightness_mode", -1);
    }

    public void q(Context context, int i) {
        Settings.System.putInt(context.getContentResolver(), "screen_brightness", i);
    }

    public int o(Context context) {
        return Settings.System.getInt(context.getContentResolver(), "screen_brightness", -1);
    }

    public void r(Context context, int i) {
        Settings.System.putInt(context.getContentResolver(), "screen_off_timeout", i);
    }

    public int p(Context context) {
        return Settings.System.getInt(context.getContentResolver(), "screen_off_timeout", 0);
    }

    public int q(Context context) {
        return Settings.System.getInt(context.getContentResolver(), "show_web_suggestions", -1);
    }

    public void s(Context context, int i) {
        int i2;
        if (i > 0) {
            i2 = 1;
        } else {
            i2 = 0;
        }
        Settings.System.putInt(context.getContentResolver(), "show_web_suggestions", i2);
    }

    public int r(Context context) {
        return Settings.System.getInt(context.getContentResolver(), "auto_punctuate", -1);
    }

    public void t(Context context, int i) {
        int i2;
        if (i > 0) {
            i2 = 1;
        } else {
            i2 = 0;
        }
        Settings.System.putInt(context.getContentResolver(), "auto_punctuate", i2);
    }

    public int s(Context context) {
        return Settings.System.getInt(context.getContentResolver(), "auto_replace", -1);
    }

    public void u(Context context, int i) {
        int i2;
        if (i > 0) {
            i2 = 1;
        } else {
            i2 = 0;
        }
        Settings.System.putInt(context.getContentResolver(), "auto_replace", i2);
    }

    public int t(Context context) {
        return Settings.System.getInt(context.getContentResolver(), "auto_caps", -1);
    }

    public int u(Context context) {
        return Settings.System.getInt(context.getContentResolver(), "show_password", -1);
    }

    public void v(Context context, int i) {
        int i2;
        if (i > 0) {
            i2 = 1;
        } else {
            i2 = 0;
        }
        Settings.System.putInt(context.getContentResolver(), "show_password", i2);
    }

    public int v(Context context) {
        return Settings.System.getInt(context.getContentResolver(), "auto_time", -1);
    }

    public void w(Context context, int i) {
        int i2;
        if (i > 0) {
            i2 = 1;
        } else {
            i2 = 0;
        }
        Settings.System.putInt(context.getContentResolver(), "auto_time", i2);
    }

    public int w(Context context) {
        return Settings.System.getInt(context.getContentResolver(), "time_12_24", -1);
    }

    public void x(Context context, int i) {
        Settings.System.putInt(context.getContentResolver(), "time_12_24", i);
    }

    public String x(Context context) {
        return Settings.System.getString(context.getContentResolver(), "date_format");
    }

    public void a(Context context, String str) {
        if (str != null) {
            Settings.System.putString(context.getContentResolver(), "date_format", str);
        }
    }
}
