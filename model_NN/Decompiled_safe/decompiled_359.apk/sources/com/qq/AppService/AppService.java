package com.qq.AppService;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Process;
import android.os.RemoteException;
import android.text.TextUtils;
import com.qq.c.d;
import com.qq.k.b;
import com.qq.l.l;
import com.qq.m.f;
import com.qq.m.g;
import com.qq.provider.ab;
import com.qq.provider.ak;
import com.qq.provider.h;
import com.qq.provider.n;
import com.qq.provider.o;
import com.qq.provider.q;
import com.qq.receiver.NetReceiver;
import com.qq.util.QQUtils;
import com.qq.util.p;
import com.qq.util.z;
import com.tencent.assistant.plugin.PluginIPCClient;
import com.tencent.assistant.plugin.UserLoginInfo;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.bg;
import com.tencent.connect.common.Constants;
import com.tencent.wcs.proxy.c;
import com.tencent.wcs.proxy.d.a;
import java.io.File;

/* compiled from: ProGuard */
public final class AppService extends Service {
    /* access modifiers changed from: private */
    public static Notification A = null;

    /* renamed from: a  reason: collision with root package name */
    public static ax f199a = null;
    public static w b = null;
    public static volatile boolean c = false;
    public static volatile boolean d = false;
    public static volatile boolean e = false;
    public static String g = null;
    public static String h = null;
    public static String i = null;
    public static boolean j = false;
    public static String k = null;
    public static volatile String l = null;
    public static int n;
    /* access modifiers changed from: private */
    public static volatile AppService o = null;
    /* access modifiers changed from: private */
    public static Notification z = null;
    private a B = new f(this);
    private g C = new g(this);
    public volatile boolean f = true;
    public NetReceiver m = null;
    private c p;
    /* access modifiers changed from: private */
    public BusinessConnectionType q;
    private PowerReceiver r = null;
    private SMSReceiver s = null;
    private volatile boolean t = false;
    private volatile Handler u = null;
    private PluginIPCClient v = null;
    private UserLoginInfo w = null;
    /* access modifiers changed from: private */
    public UserLoginInfo x = null;
    /* access modifiers changed from: private */
    public h y;

    /* compiled from: ProGuard */
    public enum BusinessConnectionType {
        NONE,
        QRCODE,
        QQ
    }

    public void onLowMemory() {
        super.onLowMemory();
    }

    public static AppService a() {
        return o;
    }

    public static Context b() {
        return o;
    }

    public static void c() {
        if (o != null) {
            o.d();
        }
    }

    public void d() {
        stopSelf();
    }

    public static void a(Notification notification) {
        if (o != null) {
            if (notification != null) {
                try {
                    o.startForeground(12345, notification);
                } catch (Throwable th) {
                    th.printStackTrace();
                }
            } else {
                o.stopForeground(true);
            }
        }
    }

    public static synchronized void e() {
        synchronized (AppService.class) {
            if (o != null) {
                int b2 = p.b(Process.myPid());
                if (b2 > 2) {
                    try {
                        ((NotificationManager) o.getSystemService("notification")).cancel(12345);
                    } catch (Throwable th) {
                    }
                } else if (b2 <= 1 && !h.b) {
                    try {
                        ((NotificationManager) o.getSystemService("notification")).cancel(12345);
                    } catch (Throwable th2) {
                    }
                }
            }
        }
    }

    public static synchronized void f() {
        synchronized (AppService.class) {
            if (o != null) {
                if (!c) {
                }
            }
        }
    }

    public static IPCService g() {
        if (o == null) {
            return null;
        }
        return IPCService.f203a;
    }

    public static void h() {
        if (o != null) {
            if (r() && h.e != null) {
                h.e(0);
                try {
                    Thread.sleep(150);
                } catch (InterruptedException e2) {
                    e2.printStackTrace();
                }
                h.c().b(o.getApplicationContext(), new com.qq.g.c());
            }
            l();
            m();
        }
    }

    public static void i() {
        if (o != null) {
            if (q() && h.e != null) {
                h.a(false);
                try {
                    Thread.sleep(150);
                } catch (InterruptedException e2) {
                    e2.printStackTrace();
                }
                h.c().b(o.getApplicationContext(), new com.qq.g.c());
            }
            l();
            m();
        }
    }

    public static void j() {
        if (o != null) {
            if (u() && h.e != null) {
                h.a(false);
                try {
                    Thread.sleep(150);
                } catch (InterruptedException e2) {
                    e2.printStackTrace();
                }
                h.c().b(o.getApplicationContext(), new com.qq.g.c());
            }
            l();
            m();
        }
    }

    public static void k() {
        if (o != null && g != null) {
            g = null;
            h.a(false);
            h.a(0, (String) null);
            try {
                Thread.sleep(150);
            } catch (InterruptedException e2) {
                e2.printStackTrace();
            }
            h.c().b(o.getApplicationContext(), new com.qq.g.c());
        }
    }

    public static synchronized void a(int i2, String str, String str2, String str3, String str4) {
        synchronized (AppService.class) {
            if (o != null) {
                if (i2 == 0) {
                    if (r()) {
                        h();
                    } else if (q()) {
                        i();
                    } else if (u()) {
                        j();
                    } else {
                        l();
                        m();
                        h.c().b((Context) null, new com.qq.g.c());
                    }
                    com.tencent.assistant.st.a.a().c((byte) 2);
                } else if (str != null) {
                    h = str;
                    i = str2;
                }
                if (!(str3 == null || str4 == null)) {
                    if (o.w == null) {
                        o.w = new UserLoginInfo();
                    }
                    o.w.setNickName(str4);
                    o.w.setUin(Long.parseLong(str3.trim()));
                    o.w.setPic(QQUtils.a(str3, QQUtils.Quality.Qua_40));
                }
            }
        }
    }

    public static void a(int i2) {
        if (o != null && i2 == 0) {
            l();
            m();
            h.c().b((Context) null, new com.qq.g.c());
            if (IPCService.f203a != null && IPCService.f203a.b != null) {
                try {
                    IPCService.f203a.b.b();
                } catch (RemoteException e2) {
                    e2.printStackTrace();
                }
            }
        }
    }

    public static void a(String str, String str2, String str3, boolean z2) {
        XLog.d("com.qq.connect", "onPCPIng ..." + str + " >" + str2);
        if (o != null) {
            try {
                ad.a(o, ad.a(o, str2, str3, str, z2));
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        if (IPCService.f203a != null && IPCService.f203a.b != null) {
            try {
                IPCService.f203a.b.a(str, str2, str3, z2);
            } catch (RemoteException e3) {
                e3.printStackTrace();
            }
        }
    }

    public static void a(boolean z2) {
        int i2 = 1;
        if (o != null) {
            d = true;
            e = false;
            o.q = BusinessConnectionType.QQ;
            if (o.u != null) {
                Message obtain = Message.obtain();
                obtain.what = 3;
                if (!z2) {
                    i2 = 0;
                }
                obtain.arg1 = i2;
                o.u.sendMessage(obtain);
            }
        }
    }

    public static void b(boolean z2) {
        int i2 = 1;
        if (o != null) {
            d = true;
            e = false;
            o.q = BusinessConnectionType.QRCODE;
            if (o.u != null) {
                Message obtain = Message.obtain();
                obtain.what = 2;
                if (!z2) {
                    i2 = 0;
                }
                obtain.arg1 = i2;
                o.u.sendMessage(obtain);
            }
        }
    }

    public static void l() {
        e = false;
        d = false;
        g = null;
        i = null;
        h = null;
    }

    public static void m() {
        if (o != null) {
            o.q = BusinessConnectionType.NONE;
            if (o.u != null) {
                o.u.sendEmptyMessage(0);
            }
            if (o.u != null) {
                o.u.removeMessages(5);
                o.u.sendEmptyMessageDelayed(5, 6000);
            }
            q.a(o);
        }
    }

    public static void c(boolean z2) {
        int i2 = 1;
        if (o != null) {
            d = false;
            e = true;
            o.q = BusinessConnectionType.NONE;
            if (o.u != null) {
                Message obtain = Message.obtain();
                obtain.what = 1;
                if (!z2) {
                    i2 = 0;
                }
                obtain.arg1 = i2;
                o.u.sendMessage(obtain);
            }
        }
    }

    public static void n() {
        if (o != null && o.u != null) {
            o.u.removeMessages(4);
            o.u.sendEmptyMessageDelayed(4, 30000);
        }
    }

    public static void o() {
        if (o != null && o.u != null) {
            o.u.removeMessages(4);
        }
    }

    private int Y() {
        if (q()) {
            return 2;
        }
        if (r()) {
            return 1;
        }
        if (s()) {
            return 1;
        }
        if (!t()) {
            return u() ? 3 : 0;
        }
        return 2;
    }

    public void p() {
        int Y = Y();
        Intent intent = new Intent("com.tencent.android.qqdownloader.action.CONNECT_PC_STATE");
        intent.putExtra("pc_state_result", Y);
        intent.putExtra("pc_connect_name", TextUtils.isEmpty(h) ? Constants.STR_EMPTY : h);
        sendBroadcast(intent);
    }

    public void onCreate() {
        super.onCreate();
        System.setProperty("java.net.preferIPv6Addresses", "false");
        o = this;
        this.f = false;
        c = false;
        d = false;
        e = false;
        h = null;
        g = null;
        this.u = new b(this);
        try {
            if (b.a(AstApp.i()) == 0) {
                b.a(AstApp.i(), System.currentTimeMillis());
            }
        } catch (Throwable th) {
        }
        Z();
    }

    public void onDestroy() {
        super.onDestroy();
        XLog.d("com.qq.connect", "onDestory!");
        aa();
        this.u = null;
        o = null;
        d = false;
        e = false;
        c = false;
        this.w = null;
        this.x = null;
        k = null;
        this.f = false;
        if (!this.t) {
            try {
                ad.a(this, ad.g(this));
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        if (this.m != null) {
            NetReceiver.b(this, this.m);
        }
        if (!(IPCService.f203a == null || IPCService.f203a.b == null)) {
            try {
                IPCService.f203a.b.b(0);
            } catch (RemoteException e3) {
                e3.printStackTrace();
            }
        }
        h.a(getApplicationContext());
        try {
            if (this.r != null) {
                unregisterReceiver(this.r);
            }
            if (this.s != null) {
                unregisterReceiver(this.s);
            }
        } catch (Exception e4) {
        }
        synchronized (AppService.class) {
            if (b != null) {
                b.a();
                b = null;
            }
            if (f199a != null) {
                f199a.a();
                f199a = null;
            }
        }
        WifiPage.f211a = false;
        try {
            ((NotificationManager) getSystemService("notification")).cancel(12345);
        } catch (NullPointerException e5) {
            e5.printStackTrace();
        }
        WifiPage.a();
        UsbPage.a();
        g = null;
        l = null;
        File file = new File(getApplicationInfo().dataDir + File.separator + "apk");
        if (file.exists()) {
            n.a(file);
        }
        q.a(this);
        ak.a(this);
        if (com.qq.c.a.f264a.exists()) {
            com.qq.c.a.f264a.delete();
        }
        d.a();
        this.t = false;
        o.a();
        h.f342a = null;
        com.qq.util.b.b().a();
        B();
    }

    public void onStart(Intent intent, int i2) {
        Bundle bundle;
        super.onStart(intent, i2);
        this.t = false;
        try {
            this.r = new PowerReceiver();
            registerReceiver(this.r, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
            registerReceiver(this.r, new IntentFilter("android.hardware.usb.action.USB_STATE"));
            this.s = new SMSReceiver();
            IntentFilter intentFilter = new IntentFilter("android.provider.Telephony.SMS_RECEIVED");
            intentFilter.setPriority(Integer.MAX_VALUE);
            registerReceiver(this.s, intentFilter);
        } catch (Exception e2) {
        }
        c = true;
        if (intent != null) {
            bundle = bg.a(intent);
        } else {
            bundle = null;
        }
        if (bundle != null) {
            k = bundle.getString("qq");
        }
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("com.qq.connect", 0);
        if (!(sharedPreferences == null || sharedPreferences.getString("USBDeviceID", null) == null)) {
            j = true;
        }
        if (!h.b) {
            h.a(getApplicationContext(), new Handler());
        }
        synchronized (AppService.class) {
            if (b == null) {
                b = new w(getApplicationContext());
                b.a(getApplicationContext());
            }
            if (f199a == null) {
                f199a = new ax(getApplicationContext());
                f199a.start();
            }
        }
        new com.qq.c.g(getApplicationContext()).start();
        XLog.d("com.qq.connect", "AppService onStart");
        r.f250a = b.c(this);
        new r(this, 0).start();
        if (k != null) {
        }
        int a2 = af.a(getApplicationContext());
        if (!(a2 == 0 || a2 == -1)) {
            ay.b = com.qq.util.n.a(a2 >>> 16);
        }
        WifiPage.b(this);
        this.m = new NetReceiver();
        this.m.b = new c(this);
        try {
            NetReceiver.a(this, this.m);
        } catch (Throwable th) {
            th.printStackTrace();
        }
        if (!(IPCService.f203a == null || IPCService.f203a.b == null)) {
            try {
                IPCService.f203a.b.c(d ? 1 : 0);
            } catch (RemoteException e3) {
                e3.printStackTrace();
            }
        }
        h.f342a = new d(this);
        if (h.e != null) {
            h.e.a(h.f342a);
        }
        TemporaryThreadManager.get().start(new e(this));
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    private void Z() {
        if (this.v == null) {
            try {
                this.v = new PluginIPCClient();
                this.v.init(getApplicationContext());
                if (this.y == null) {
                    this.y = new h(this);
                }
                this.v.setLoginStateCallback(this.y);
                this.v.sendQueryLoginState();
                this.y.a("service", PluginIPCClient.LOGIN_ADDTION_INFO_GET_USER_INFO);
                this.v.sendQueryLoginInfo(PluginIPCClient.LOGIN_ADDTION_INFO_GET_USER_INFO);
            } catch (Exception e2) {
            }
        }
    }

    private void aa() {
        if (this.v != null) {
            this.v.setLoginStateCallback(null);
            this.v.close(this);
            this.v = null;
        }
    }

    public static boolean q() {
        return c && d && g != null && !g.contains("127.0.0.1") && h.b && h.e != null && h.e.b;
    }

    public static boolean r() {
        return c && e && h.b && h.e != null && h.e.b;
    }

    public static boolean s() {
        return c && e;
    }

    public static boolean t() {
        return c && d && g != null;
    }

    public static boolean u() {
        return c && d && g != null && g.contains("127.0.0.1") && h.b && h.e != null && h.e.b;
    }

    public static String v() {
        if (l == null) {
            l = ab.b(AstApp.i());
        }
        return l;
    }

    public static boolean w() {
        if (o == null || o.p == null) {
            return false;
        }
        return o.p.e();
    }

    public static boolean x() {
        if (o == null || o.p == null) {
            return false;
        }
        return o.p.o();
    }

    public static boolean y() {
        if (o == null || o.p == null) {
            return false;
        }
        return o.p.p();
    }

    public static void z() {
        com.qq.l.p.m().b(com.qq.l.p.m().p().c(), 0, -1);
        if (o != null) {
            if (o.p == null) {
                o.p = new c(o.getApplicationContext());
                o.p.a(o.B);
            }
            o.p.a();
        }
    }

    public static void A() {
        if (o != null && o.p != null) {
            o.p.j();
        }
    }

    public static void B() {
        if (o != null && o.p != null) {
            o.p.d();
            o.p = null;
        }
    }

    public static void C() {
        if (o != null && o.p != null) {
            o.p.h();
        }
    }

    public static void D() {
        if (o != null && o.p != null) {
            o.p.i();
        }
    }

    public static void E() {
        if (o != null && o.p != null) {
            o.p.k();
        }
    }

    public static void F() {
        if (o != null && o.p != null) {
            o.p.l();
        }
    }

    public static void G() {
        if (o != null && o.p != null) {
            o.p.m();
        }
    }

    public static void H() {
        if (o != null && o.p != null) {
            o.p.n();
        }
    }

    public static String I() {
        if (k != null) {
            return k;
        }
        if (o == null || o.x == null) {
            return null;
        }
        k = o.x.getUin() + Constants.STR_EMPTY;
        return k;
    }

    public static boolean J() {
        return (o == null || o.x == null || (o.x.getState() != 2 && o.x.getState() != 3)) ? false : true;
    }

    public static String K() {
        if (o == null || o.x == null) {
            return null;
        }
        return o.x.getNickName();
    }

    public static String L() {
        if (o == null || o.x == null) {
            return null;
        }
        return o.x.getPic();
    }

    public static long M() {
        if (o == null || o.x == null) {
            return 0;
        }
        return o.x.getUin();
    }

    public static String N() {
        if (o == null || o.w == null) {
            return null;
        }
        return o.w.getNickName();
    }

    public static String O() {
        if (o == null || o.w == null) {
            return null;
        }
        return o.w.getPic();
    }

    public static long P() {
        if (o == null || o.w == null) {
            return 0;
        }
        return o.w.getUin();
    }

    public static void a(BusinessConnectionType businessConnectionType) {
        if (o != null) {
            o.q = businessConnectionType;
        }
    }

    public static BusinessConnectionType Q() {
        if (o != null) {
            return o.q;
        }
        return BusinessConnectionType.NONE;
    }

    public static void R() {
        com.qq.l.p.m().b(com.qq.l.p.m().p().c(), 0, -1);
        com.qq.l.p.m().c(com.qq.l.p.m().p().c(), 1, -1);
    }

    public void S() {
        com.qq.l.p.m().a(new l());
        com.qq.l.p.m().a(1, 4, -1);
        com.qq.l.p.m().b(601, 0, -1);
        com.qq.l.p.m().c(601, 1, -1);
    }

    /* access modifiers changed from: private */
    public void a(UserLoginInfo userLoginInfo) {
        if (userLoginInfo != null) {
            this.x = userLoginInfo;
            k = this.x.getUin() + Constants.STR_EMPTY;
            TemporaryThreadManager.get().start(new f(o, this.x.getUin(), z.a(this.x.getA2()), 1, this.C));
        }
    }

    /* access modifiers changed from: private */
    public void b(UserLoginInfo userLoginInfo) {
        if (userLoginInfo != null && userLoginInfo.getUin() > 0) {
            TemporaryThreadManager.get().start(new f(o, userLoginInfo.getUin(), z.a(userLoginInfo.getA2()), 0, null));
            this.x = null;
            k = null;
        }
    }

    public static void T() {
        if (o != null && o.v != null && o.y != null) {
            o.y.a("activity", PluginIPCClient.LOGIN_ADDTION_INFO_GET_USER_INFO);
            o.v.sendQueryLoginInfo(PluginIPCClient.LOGIN_ADDTION_INFO_GET_USER_INFO);
        }
    }

    public static void U() {
        if (o != null && o.v != null && o.y != null) {
            o.y.a("activity", PluginIPCClient.LOGIN_ADDTION_INFO_LOGIN);
            o.v.sendQueryLoginInfo(PluginIPCClient.LOGIN_ADDTION_INFO_LOGIN);
        }
    }
}
