package com.qq.AppService;

import android.content.ContentValues;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.database.Cursor;
import com.qq.g.c;
import com.qq.provider.ebooks3.EbooksProvider;
import java.util.ArrayList;

/* compiled from: ProGuard */
public final class a {

    /* renamed from: a  reason: collision with root package name */
    private static a f212a = null;
    private ArrayList<byte[]> b = null;

    private a() {
    }

    public static a a() {
        if (f212a == null) {
            f212a = new a();
        }
        return f212a;
    }

    public void a(Context context) {
        int count;
        byte[] bArr;
        this.b = new ArrayList<>();
        Cursor query = context.getContentResolver().query(EbooksProvider.f339a, null, "kind = -2", null, null);
        if (query != null && (count = query.getCount()) > 0) {
            boolean moveToFirst = query.moveToFirst();
            for (int i = 0; i < count && moveToFirst; i++) {
                try {
                    bArr = query.getString(query.getColumnIndex("data")).getBytes("utf-8");
                } catch (Throwable th) {
                    th.printStackTrace();
                    bArr = null;
                }
                if (bArr == null) {
                    query.moveToNext();
                } else {
                    this.b.add(bArr);
                    query.moveToNext();
                }
            }
        }
        if (query != null) {
            query.close();
        }
    }

    public void a(Context context, c cVar) {
        boolean z;
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(cVar.j(), 4096);
            String[] strArr = packageInfo.requestedPermissions;
            ArrayList arrayList = new ArrayList();
            if (strArr == null) {
                arrayList.add(s.a(0));
                cVar.a(arrayList);
                cVar.a(0);
                return;
            }
            int i = 0;
            while (true) {
                if (i >= strArr.length) {
                    z = false;
                    break;
                } else if (strArr[i].equals("android.permission.SEND_SMS")) {
                    z = true;
                    break;
                } else {
                    i++;
                }
            }
            if (!z) {
                arrayList.add(s.a(0));
                cVar.a(arrayList);
                cVar.a(0);
                return;
            }
            if (this.b == null) {
                a(context);
            }
            int a2 = a(context, packageInfo.applicationInfo.sourceDir, "classes.dex", this.b);
            if (a2 < 0) {
                cVar.a(8);
                return;
            }
            arrayList.add(s.a(a2));
            cVar.a(arrayList);
            cVar.a(0);
        } catch (Exception e) {
            e.printStackTrace();
            cVar.a(1);
        }
    }

    public void b(Context context, c cVar) {
        Cursor query = context.getContentResolver().query(EbooksProvider.f339a, null, "kind = -3", null, null);
        if (query == null) {
            cVar.a(8);
            return;
        }
        ArrayList arrayList = new ArrayList();
        try {
            if (query.moveToFirst()) {
                arrayList.add(s.a(query.getInt(query.getColumnIndex("size"))));
            } else {
                arrayList.add(s.a(0));
            }
            if (query != null) {
                query.close();
            }
            cVar.a(arrayList);
            cVar.a(0);
        } catch (Throwable th) {
            if (query != null) {
                query.close();
            }
            throw th;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void c(Context context, c cVar) {
        if (cVar.e() < 2) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        int h2 = cVar.h();
        if (cVar.e() < h2 + 2) {
            cVar.a(1);
            return;
        }
        context.getContentResolver().delete(EbooksProvider.f339a, "kind <= -2", null);
        ContentValues contentValues = new ContentValues();
        contentValues.put("kind", (Integer) -3);
        contentValues.put("size", Integer.valueOf(h));
        context.getContentResolver().insert(EbooksProvider.f339a, contentValues);
        contentValues.clear();
        for (int i = 0; i < h2; i++) {
            String j = cVar.j();
            contentValues.put("kind", (Integer) -2);
            if (!s.b(j)) {
                contentValues.put("data", j);
            } else {
                contentValues.putNull("data");
            }
            context.getContentResolver().insert(EbooksProvider.f339a, contentValues);
            contentValues.clear();
        }
        a(context);
        cVar.a(0);
    }

    /* JADX WARNING: Removed duplicated region for block: B:108:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x0095 A[SYNTHETIC, Splitter:B:59:0x0095] */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x009a A[SYNTHETIC, Splitter:B:62:0x009a] */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x00a6 A[SYNTHETIC, Splitter:B:68:0x00a6] */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x00ab A[SYNTHETIC, Splitter:B:71:0x00ab] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int a(android.content.Context r10, java.lang.String r11, java.lang.String r12, java.util.ArrayList<byte[]> r13) {
        /*
            r9 = this;
            if (r11 != 0) goto L_0x0004
            r0 = -1
        L_0x0003:
            return r0
        L_0x0004:
            java.io.File r0 = new java.io.File
            r0.<init>(r11)
            boolean r0 = r0.exists()
            if (r0 != 0) goto L_0x0011
            r0 = -1
            goto L_0x0003
        L_0x0011:
            r2 = 0
            r0 = 131072(0x20000, float:1.83671E-40)
            byte[] r6 = new byte[r0]
            r1 = 0
            java.util.zip.ZipFile r4 = new java.util.zip.ZipFile     // Catch:{ Exception -> 0x008e, all -> 0x00a1 }
            r4.<init>(r11)     // Catch:{ Exception -> 0x008e, all -> 0x00a1 }
            java.util.zip.ZipEntry r0 = r4.getEntry(r12)     // Catch:{ Exception -> 0x00d7, all -> 0x00ce }
            if (r0 != 0) goto L_0x0033
            r0 = 0
            if (r1 == 0) goto L_0x0028
            r1.close()     // Catch:{ IOException -> 0x00be }
        L_0x0028:
            if (r4 == 0) goto L_0x0003
            r4.close()     // Catch:{ IOException -> 0x002e }
            goto L_0x0003
        L_0x002e:
            r1 = move-exception
        L_0x002f:
            r1.printStackTrace()
            goto L_0x0003
        L_0x0033:
            java.io.InputStream r3 = r4.getInputStream(r0)     // Catch:{ Exception -> 0x00d7, all -> 0x00ce }
            r1 = 0
            r0 = 0
        L_0x0039:
            int r2 = r3.available()     // Catch:{ Exception -> 0x00da, all -> 0x00d1 }
            if (r2 <= 0) goto L_0x006c
            if (r1 != 0) goto L_0x006c
            int r7 = r3.read(r6)     // Catch:{ Exception -> 0x00da, all -> 0x00d1 }
            int r2 = r13.size()     // Catch:{ Exception -> 0x00da, all -> 0x00d1 }
            if (r2 <= 0) goto L_0x0039
            r2 = 0
            r5 = r2
            r8 = r0
            r0 = r1
            r1 = r8
        L_0x0050:
            int r2 = r13.size()     // Catch:{ Exception -> 0x00da, all -> 0x00d1 }
            if (r5 >= r2) goto L_0x00de
            java.lang.Object r0 = r13.get(r5)     // Catch:{ Exception -> 0x00da, all -> 0x00d1 }
            byte[] r0 = (byte[]) r0     // Catch:{ Exception -> 0x00da, all -> 0x00d1 }
            boolean r2 = com.qq.AppService.s.a(r6, r0, r7)     // Catch:{ Exception -> 0x00da, all -> 0x00d1 }
            int r0 = r1 + 1
            if (r2 == 0) goto L_0x0066
            r1 = r2
            goto L_0x0039
        L_0x0066:
            int r1 = r5 + 1
            r5 = r1
            r1 = r0
            r0 = r2
            goto L_0x0050
        L_0x006c:
            r3.close()     // Catch:{ Exception -> 0x00da, all -> 0x00d1 }
            r2 = 0
            if (r1 == 0) goto L_0x007f
            if (r2 == 0) goto L_0x0077
            r2.close()     // Catch:{ IOException -> 0x00c4 }
        L_0x0077:
            if (r4 == 0) goto L_0x0003
            r4.close()     // Catch:{ IOException -> 0x007d }
            goto L_0x0003
        L_0x007d:
            r1 = move-exception
            goto L_0x002f
        L_0x007f:
            r0 = 0
            if (r2 == 0) goto L_0x0085
            r2.close()     // Catch:{ IOException -> 0x00c9 }
        L_0x0085:
            if (r4 == 0) goto L_0x0003
            r4.close()     // Catch:{ IOException -> 0x008c }
            goto L_0x0003
        L_0x008c:
            r1 = move-exception
            goto L_0x002f
        L_0x008e:
            r0 = move-exception
        L_0x008f:
            r0.printStackTrace()     // Catch:{ all -> 0x00d3 }
            r0 = -1
            if (r1 == 0) goto L_0x0098
            r1.close()     // Catch:{ IOException -> 0x00b9 }
        L_0x0098:
            if (r2 == 0) goto L_0x0003
            r2.close()     // Catch:{ IOException -> 0x009f }
            goto L_0x0003
        L_0x009f:
            r1 = move-exception
            goto L_0x002f
        L_0x00a1:
            r0 = move-exception
            r3 = r1
            r4 = r2
        L_0x00a4:
            if (r3 == 0) goto L_0x00a9
            r3.close()     // Catch:{ IOException -> 0x00af }
        L_0x00a9:
            if (r4 == 0) goto L_0x00ae
            r4.close()     // Catch:{ IOException -> 0x00b4 }
        L_0x00ae:
            throw r0
        L_0x00af:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00a9
        L_0x00b4:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00ae
        L_0x00b9:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0098
        L_0x00be:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0028
        L_0x00c4:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0077
        L_0x00c9:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0085
        L_0x00ce:
            r0 = move-exception
            r3 = r1
            goto L_0x00a4
        L_0x00d1:
            r0 = move-exception
            goto L_0x00a4
        L_0x00d3:
            r0 = move-exception
            r3 = r1
            r4 = r2
            goto L_0x00a4
        L_0x00d7:
            r0 = move-exception
            r2 = r4
            goto L_0x008f
        L_0x00da:
            r0 = move-exception
            r1 = r3
            r2 = r4
            goto L_0x008f
        L_0x00de:
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x0039
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qq.AppService.a.a(android.content.Context, java.lang.String, java.lang.String, java.util.ArrayList):int");
    }
}
