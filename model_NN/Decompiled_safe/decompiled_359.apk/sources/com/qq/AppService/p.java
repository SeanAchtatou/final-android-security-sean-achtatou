package com.qq.AppService;

import com.tencent.assistant.manager.av;
import com.tencent.assistant.module.a.a.a;
import com.tencent.assistant.module.a.a.f;
import com.tencent.assistant.module.a.m;
import com.tencent.assistant.module.update.AppUpdateConst;
import com.tencent.assistant.module.update.c;
import com.tencent.assistant.plugin.PluginHelper;

/* compiled from: ProGuard */
final class p implements Runnable {
    p() {
    }

    public void run() {
        if (!AstApp.r()) {
            synchronized (AstApp.f) {
                if (!AstApp.r()) {
                    boolean unused = AstApp.o = true;
                    new m().run();
                    new a().run();
                    c.a().a(AppUpdateConst.RequestLaunchType.TYPE_STARTUP);
                    new f().run();
                    AstApp.H();
                    AstApp.I();
                    av.a();
                    PluginHelper.execFreeWifiInit();
                }
            }
        }
    }
}
