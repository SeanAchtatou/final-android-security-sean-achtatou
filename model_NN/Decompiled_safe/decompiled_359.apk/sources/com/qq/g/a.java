package com.qq.g;

import android.content.Context;
import android.net.NetworkInfo;
import android.os.Build;
import com.qq.AppService.AppService;
import com.qq.AppService.af;
import com.qq.AppService.ay;
import com.qq.AppService.s;
import com.qq.provider.ab;
import com.qq.util.n;
import java.net.InetAddress;

/* compiled from: ProGuard */
public class a {

    /* renamed from: a  reason: collision with root package name */
    public int f291a;
    public int b = 0;
    public String c = null;
    public int d;
    public String e = null;
    public String f = null;
    public String g = null;
    public String h = null;
    public String i = null;
    public String j = null;
    public String k = null;
    public String l = null;
    public String m = null;
    public String n = null;

    public static byte[] a(a aVar) {
        byte[] a2 = s.a(aVar.g);
        byte[] a3 = s.a(aVar.c);
        byte[] a4 = s.a(aVar.h);
        int length = a2.length + 4 + 16 + a3.length + 4 + 32 + ((a4.length + 4) * 2);
        byte[] a5 = s.a(aVar.k);
        int length2 = length + a5.length + 4;
        byte[] a6 = s.a(aVar.l);
        int length3 = length2 + a6.length + 4;
        byte[] a7 = s.a(aVar.m);
        byte[] a8 = s.a(aVar.e);
        int length4 = length3 + a7.length + 4 + 8 + a8.length + 4;
        byte[] bArr = new byte[length4];
        System.arraycopy(s.a(4), 0, bArr, 0, 4);
        System.arraycopy(s.a(length4), 0, bArr, 4, 4);
        System.arraycopy(s.a(4), 0, bArr, 8, 4);
        System.arraycopy(s.a(0), 0, bArr, 12, 4);
        System.arraycopy(s.a(a2.length), 0, bArr, 16, 4);
        System.arraycopy(a2, 0, bArr, 20, a2.length);
        int length5 = a2.length + 20;
        System.arraycopy(s.a(a3.length), 0, bArr, length5, 4);
        int i2 = length5 + 4;
        System.arraycopy(a3, 0, bArr, i2, a3.length);
        int length6 = i2 + a3.length;
        System.arraycopy(s.a(4), 0, bArr, length6, 4);
        int i3 = length6 + 4;
        System.arraycopy(s.a(aVar.d), 0, bArr, i3, 4);
        int i4 = i3 + 4;
        System.arraycopy(s.a(4), 0, bArr, i4, 4);
        int i5 = i4 + 4;
        System.arraycopy(s.a(aVar.b), 0, bArr, i5, 4);
        int i6 = i5 + 4;
        System.arraycopy(s.a(4), 0, bArr, i6, 4);
        int i7 = i6 + 4;
        System.arraycopy(s.a(Integer.parseInt(aVar.i)), 0, bArr, i7, 4);
        int i8 = i7 + 4;
        System.arraycopy(s.a(a4.length), 0, bArr, i8, 4);
        int i9 = i8 + 4;
        System.arraycopy(a4, 0, bArr, i9, a4.length);
        int length7 = i9 + a4.length;
        System.arraycopy(s.a(a4.length), 0, bArr, length7, 4);
        int i10 = length7 + 4;
        System.arraycopy(a4, 0, bArr, i10, a4.length);
        int length8 = i10 + a4.length;
        System.arraycopy(s.a(a5.length), 0, bArr, length8, 4);
        int i11 = length8 + 4;
        System.arraycopy(a5, 0, bArr, i11, a5.length);
        int length9 = i11 + a5.length;
        System.arraycopy(s.a(a6.length), 0, bArr, length9, 4);
        int i12 = length9 + 4;
        System.arraycopy(a6, 0, bArr, i12, a6.length);
        int length10 = i12 + a6.length;
        System.arraycopy(s.a(a7.length), 0, bArr, length10, 4);
        int i13 = length10 + 4;
        System.arraycopy(a7, 0, bArr, i13, a7.length);
        int length11 = i13 + a7.length;
        System.arraycopy(s.a(4), 0, bArr, length11, 4);
        int i14 = length11 + 4;
        System.arraycopy(s.a(1), 0, bArr, i14, 4);
        int i15 = i14 + 4;
        System.arraycopy(s.a(a8.length), 0, bArr, i15, 4);
        int i16 = i15 + 4;
        System.arraycopy(a8, 0, bArr, i16, a8.length);
        int length12 = i16 + a8.length;
        System.arraycopy(s.a(4), 0, bArr, length12, 4);
        int i17 = length12 + 4;
        System.arraycopy(s.a(aVar.f291a), 0, bArr, i17, 4);
        int i18 = i17 + 4;
        return bArr;
    }

    public void a(Context context, String str) {
        int a2;
        this.f291a = 1;
        this.e = str;
        this.f = null;
        if (AppService.l == null) {
            AppService.l = ab.b(context);
        }
        this.g = AppService.l;
        if (!(ay.b != null || (a2 = af.a(context)) == 0 || a2 == -1)) {
            ay.b = n.a(a2 >>> 16);
        }
        this.h = ay.b;
        this.i = "129";
        this.j = ab.a(context);
        this.k = Build.MODEL;
        this.l = Build.BRAND;
        this.m = Build.VERSION.SDK;
    }

    public void a(Context context) {
        int a2;
        this.f291a = 1;
        this.e = AppService.k;
        this.f = null;
        if (AppService.l == null) {
            AppService.l = ab.b(context);
        }
        this.g = AppService.l;
        if (!(ay.b != null || (a2 = af.a(context)) == 0 || a2 == -1)) {
            ay.b = n.a(a2 >>> 16);
        }
        this.h = ay.b;
        this.i = "129";
        this.j = ab.a(context);
        this.k = Build.MODEL;
        this.l = Build.BRAND;
        this.m = Build.VERSION.SDK;
    }

    public void a(NetworkInfo networkInfo) {
        if (networkInfo == null) {
            this.b = -1;
        } else {
            this.b = networkInfo.getType();
        }
    }

    public void a(Context context, InetAddress inetAddress) {
        int a2;
        this.d = 13091;
        if (inetAddress != null) {
            this.c = inetAddress.getHostAddress();
        }
        if (!(ay.b != null || (a2 = af.a(context.getApplicationContext())) == 0 || a2 == -1)) {
            ay.b = n.a(a2 >>> 16);
        }
        this.h = ay.b;
    }
}
