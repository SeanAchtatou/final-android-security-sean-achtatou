package com.qq.l;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;

/* compiled from: ProGuard */
public class i {

    /* renamed from: a  reason: collision with root package name */
    private static i f308a;
    private Handler b;

    private i() {
        HandlerThread handlerThread = new HandlerThread("SimpleTimerTask");
        handlerThread.start();
        Looper looper = handlerThread.getLooper();
        if (looper != null) {
            this.b = new j(this, looper);
        }
    }

    public static synchronized i a() {
        i iVar;
        synchronized (i.class) {
            if (f308a == null) {
                f308a = new i();
            }
            iVar = f308a;
        }
        return iVar;
    }

    public void a(h hVar) {
        this.b.sendMessageDelayed(this.b.obtainMessage(hVar.a(), 0, 0, hVar), (long) (hVar.d() * 1000));
    }
}
