package com.tencent.connector.a;

import com.qq.AppService.t;
import com.qq.l.p;
import com.tencent.assistant.st.a;
import com.tencent.wcs.c.b;

/* compiled from: ProGuard */
class d extends Thread {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ c f3516a;

    d(c cVar) {
        this.f3516a = cVar;
    }

    public void run() {
        if (this.f3516a.e != null) {
            boolean z = false;
            long currentTimeMillis = System.currentTimeMillis();
            synchronized (this.f3516a.e) {
                try {
                    this.f3516a.e.wait((long) this.f3516a.d);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    z = true;
                }
            }
            long currentTimeMillis2 = System.currentTimeMillis();
            if (!z && currentTimeMillis2 - currentTimeMillis > ((long) (this.f3516a.d - 100))) {
                a.a().b((byte) 7);
                p.m().c(p.m().p().b(), 2, 1);
                t.a();
                if (this.f3516a.b != null) {
                    this.f3516a.b.sendMessage(this.f3516a.b.obtainMessage(20, this.f3516a.c));
                }
                b.a("WaitPCRequestTask waiting timeout !!!");
            }
        }
    }
}
