package com.tencent.connector;

import android.app.Activity;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.Toast;
import com.google.zxing.a;
import com.google.zxing.h;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.event.listener.b;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import com.tencent.connector.component.ViewfinderView;
import com.tencent.connector.qrcode.a.d;
import com.tencent.connector.qrcode.a.g;
import com.tencent.connector.qrcode.decoder.CaptureActivityHandler;
import com.tencent.connector.qrcode.decoder.e;
import java.io.IOException;
import java.util.Vector;

/* compiled from: ProGuard */
public class CaptureActivity extends Activity implements SurfaceHolder.Callback, b {

    /* renamed from: a  reason: collision with root package name */
    private SecondNavigationTitleViewV5 f3507a;
    private CaptureActivityHandler b;
    private SurfaceView c;
    private ViewfinderView d;
    private boolean e;
    private Vector<a> f;
    private String g;
    private e h;
    /* access modifiers changed from: private */
    public MediaPlayer i;
    private boolean j;
    private boolean k;
    /* access modifiers changed from: private */
    public ImageView l;
    /* access modifiers changed from: private */
    public Animation m;
    /* access modifiers changed from: private */
    public boolean n = false;
    private final MediaPlayer.OnCompletionListener o = new b(this);
    private final MediaPlayer.OnErrorListener p = new c(this);

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_capture_qrcode);
        d.a(getApplication());
        this.f3507a = (SecondNavigationTitleViewV5) findViewById(R.id.title_view);
        this.f3507a.a(this);
        this.f3507a.b(getString(R.string.title_capture_qrcode));
        this.f3507a.d();
        this.l = (ImageView) findViewById(R.id.qrcode_laser_line);
        this.d = (ViewfinderView) findViewById(R.id.viewfinder_view);
        this.d.setOnStartLaserAnimationListener(new a(this));
        this.e = false;
        this.h = new e(this);
        AstApp.i().k().addConnectionEventListener(5002, this);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        g.f3563a = false;
        this.c = (SurfaceView) findViewById(R.id.preview_view);
        SurfaceHolder holder = this.c.getHolder();
        if (this.e) {
            a(holder);
        } else {
            holder.addCallback(this);
            holder.setType(3);
        }
        this.f = null;
        this.g = null;
        this.j = true;
        if (((AudioManager) getSystemService("audio")).getRingerMode() != 2) {
            this.j = false;
        }
        this.k = true;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.b != null) {
            this.b.a();
            this.b = null;
        }
        d.a().b();
        this.n = false;
        if (this.l != null) {
            this.l.clearAnimation();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        AstApp.i().k().removeConnectionEventListener(5002, this);
        if (this.h != null) {
            this.h.b();
        }
        super.onDestroy();
        this.f3507a = null;
        this.b = null;
        this.c = null;
        this.d = null;
        this.f = null;
        if (this.i != null) {
            this.i.release();
            this.i = null;
        }
    }

    private void a(SurfaceHolder surfaceHolder) {
        try {
            d.a().a(surfaceHolder);
            if (this.b == null) {
                this.b = new CaptureActivityHandler(this, this.f, this.g);
            }
        } catch (IOException e2) {
            e2.printStackTrace();
            f();
        } catch (RuntimeException e3) {
            e3.printStackTrace();
            f();
        }
    }

    public void surfaceChanged(SurfaceHolder surfaceHolder, int i2, int i3, int i4) {
    }

    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        if (!this.e) {
            this.e = true;
            a(surfaceHolder);
        }
    }

    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        this.e = false;
    }

    public ViewfinderView a() {
        return this.d;
    }

    public Handler b() {
        return this.b;
    }

    public void c() {
        this.d.drawViewfinder();
    }

    public void a(h hVar, Bitmap bitmap) {
        this.h.a();
        e();
        String a2 = hVar.a();
        this.n = false;
        if (this.l != null) {
            this.l.setVisibility(4);
            this.l.clearAnimation();
        }
        Intent intent = new Intent();
        intent.putExtra("result", a2);
        setResult(-1, intent);
        finish();
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 4) {
            Intent intent = new Intent();
            intent.putExtra("result", g.f3563a);
            setResult(0, intent);
        }
        return super.onKeyDown(i2, keyEvent);
    }

    private void d() {
        AssetFileDescriptor assetFileDescriptor;
        if (this.j && this.i == null) {
            setVolumeControlStream(3);
            this.i = new MediaPlayer();
            this.i.setAudioStreamType(3);
            this.i.setOnCompletionListener(this.o);
            this.i.setOnErrorListener(this.p);
            try {
                assetFileDescriptor = getResources().openRawResourceFd(R.raw.beep);
            } catch (Resources.NotFoundException e2) {
                e2.printStackTrace();
                assetFileDescriptor = null;
            }
            if (assetFileDescriptor != null) {
                try {
                    this.i.setDataSource(assetFileDescriptor.getFileDescriptor(), assetFileDescriptor.getStartOffset(), assetFileDescriptor.getLength());
                    this.i.setVolume(0.1f, 0.1f);
                    this.i.prepare();
                } catch (IOException e3) {
                    this.i.release();
                    this.i = null;
                }
                try {
                    assetFileDescriptor.close();
                } catch (IOException e4) {
                    e4.printStackTrace();
                }
            } else {
                this.i.release();
                this.i = null;
            }
        }
    }

    private void e() {
        d();
        if (this.j && this.i != null) {
            this.i.start();
        }
        if (this.k) {
            ((Vibrator) getSystemService("vibrator")).vibrate(200);
        }
    }

    public void a(Message message) {
        switch (message.what) {
            case 5002:
                finish();
                return;
            default:
                return;
        }
    }

    private void f() {
        boolean hasSystemFeature = getPackageManager().hasSystemFeature("android.hardware.camera");
        int checkCallingOrSelfPermission = checkCallingOrSelfPermission("android.permission.CAMERA");
        if (!hasSystemFeature) {
            Toast.makeText(this, (int) R.string.toast_error_camera_not_found, 1).show();
        } else if (checkCallingOrSelfPermission != 0) {
            Toast.makeText(this, (int) R.string.toast_error_camera_no_permission, 1).show();
        } else {
            Toast.makeText(this, (int) R.string.toast_error_in_open_camera, 1).show();
        }
        finish();
    }
}
