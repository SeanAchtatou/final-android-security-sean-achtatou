package com.tencent.connector.component;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import com.google.zxing.j;
import com.tencent.android.qqdownloader.R;
import com.tencent.connector.qrcode.a.d;
import java.util.Collection;
import java.util.HashSet;

/* compiled from: ProGuard */
public final class ViewfinderView extends View {

    /* renamed from: a  reason: collision with root package name */
    private static final String f3529a = ViewfinderView.class.getSimpleName();
    private long b = 100;
    private final Paint c = new Paint();
    private final Paint d;
    private final int e;
    private final int f;
    private final int g;
    private final int h;
    private final int i;
    private Collection<j> j;
    private Collection<j> k;
    private String l;
    private String m;
    private float n;
    private LaserAnimationListener o;

    /* compiled from: ProGuard */
    public interface LaserAnimationListener {
        void onStartAnimation(Rect rect, int i);
    }

    public ViewfinderView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        PorterDuffXfermode porterDuffXfermode = new PorterDuffXfermode(PorterDuff.Mode.CLEAR);
        this.d = new Paint();
        this.d.setColor(16777215);
        this.d.setAlpha(0);
        this.d.setXfermode(porterDuffXfermode);
        this.d.setAntiAlias(true);
        this.e = getResources().getColor(R.color.viewfinder_mask);
        this.f = getResources().getColor(R.color.possible_result_points);
        this.g = getResources().getColor(R.color.viewfinder_frame);
        this.h = getResources().getColor(R.color.viewfinder_frame_corner);
        this.i = getResources().getColor(R.color.tips_text);
        this.l = getResources().getString(R.string.tip_qrcode_1);
        this.m = getResources().getString(R.string.tip_qrcode_2);
        this.j = new HashSet(5);
        this.n = getResources().getDisplayMetrics().density;
    }

    public void setOnStartLaserAnimationListener(LaserAnimationListener laserAnimationListener) {
        this.o = laserAnimationListener;
    }

    public void onDraw(Canvas canvas) {
        Rect f2 = d.a().f();
        if (f2 != null) {
            a(canvas, f2);
            b(canvas, f2);
            c(canvas, f2);
            d(canvas, f2);
            a(f2);
        }
    }

    public void drawViewfinder() {
        invalidate();
    }

    public void addPossibleResultPoint(j jVar) {
        this.j.add(jVar);
    }

    private Point a(j jVar) {
        Point point = new Point(getWidth(), getHeight());
        Point e2 = d.a().e();
        return new Point((int) (((((float) point.x) * 1.0f) * jVar.a()) / ((float) e2.x)), (int) (((((float) point.y) * 1.0f) * jVar.b()) / ((float) e2.y)));
    }

    private void a(Canvas canvas, Rect rect) {
        this.c.setColor(this.e);
        canvas.drawRect(0.0f, 0.0f, (float) getWidth(), (float) rect.top, this.c);
        canvas.drawRect(0.0f, (float) rect.top, (float) c(rect), (float) (rect.bottom + 1), this.c);
        canvas.drawRect((float) (b(rect) + 1), (float) rect.top, (float) getWidth(), (float) (rect.bottom + 1), this.c);
        canvas.drawRect(0.0f, (float) (rect.bottom + 1), (float) getWidth(), (float) getHeight(), this.c);
    }

    private void b(Canvas canvas, Rect rect) {
        this.c.setColor(this.h);
        int c2 = c();
        int b2 = b();
        int a2 = a();
        int c3 = c(rect);
        int b3 = b(rect);
        canvas.drawRect((float) (c3 + c2), (float) (rect.top + c2), (float) (c3 + a2 + c2), (float) (rect.top + b2 + c2), this.c);
        canvas.drawRect((float) (c3 + c2), (float) (rect.top + b2 + c2), (float) (c3 + b2 + c2), (float) (rect.top + a2 + c2), this.c);
        canvas.drawRect((float) ((b3 - a2) - c2), (float) (rect.top + c2), (float) (b3 - c2), (float) (rect.top + b2 + c2), this.c);
        canvas.drawRect((float) ((b3 - b2) - c2), (float) (rect.top + b2 + c2), (float) (b3 - c2), (float) (rect.top + a2 + c2), this.c);
        canvas.drawRect((float) (c3 + c2), (float) ((rect.bottom - b2) - c2), (float) (c3 + a2 + c2), (float) (rect.bottom - c2), this.c);
        canvas.drawRect((float) (c3 + c2), (float) ((rect.bottom - a2) - c2), (float) (c3 + b2 + c2), (float) ((rect.bottom - b2) - c2), this.c);
        canvas.drawRect((float) ((b3 - a2) - c2), (float) ((rect.bottom - b2) - c2), (float) (b3 - c2), (float) (rect.bottom - c2), this.c);
        canvas.drawRect((float) ((b3 - b2) - c2), (float) ((rect.bottom - a2) - c2), (float) (b3 - c2), (float) ((rect.bottom - b2) - c2), this.c);
    }

    private void c(Canvas canvas, Rect rect) {
        this.c.setColor(this.i);
        this.c.setTextSize(e());
        this.c.setAntiAlias(true);
        canvas.drawText(this.l, (float) ((int) ((((float) getWidth()) - this.c.measureText(this.l)) / 2.0f)), (float) (rect.bottom + d()), this.c);
        canvas.drawText(this.m, (float) ((int) ((((float) getWidth()) - this.c.measureText(this.m)) / 2.0f)), (float) ((int) (((float) (rect.bottom + d())) + e() + ((float) f()))), this.c);
    }

    private void d(Canvas canvas, Rect rect) {
        Collection<j> collection = this.j;
        Collection<j> collection2 = this.k;
        if (collection == null || collection.isEmpty()) {
            this.k = null;
        } else {
            this.j = new HashSet(5);
            this.k = collection;
            this.c.setAlpha(255);
            this.c.setColor(this.f);
            for (j a2 : collection) {
                Point a3 = a(a2);
                canvas.drawCircle((float) (c(rect) + a3.x), (float) (a3.y + rect.top), 6.0f, this.c);
            }
        }
        if (collection2 != null) {
            this.c.setAlpha(127);
            this.c.setColor(this.f);
            for (j a4 : collection2) {
                Point a5 = a(a4);
                canvas.drawCircle((float) (c(rect) + a5.x), (float) (a5.y + rect.top), 3.0f, this.c);
            }
        }
        postInvalidateDelayed(this.b, c(rect), rect.top, b(rect), rect.bottom);
    }

    private void a(Rect rect) {
        if (this.o != null) {
            this.o.onStartAnimation(rect, c());
        }
    }

    private int a() {
        return (int) ((20.0f * this.n) + 0.5f);
    }

    private int b() {
        return (int) ((5.0f * this.n) + 0.5f);
    }

    private int c() {
        return 0;
    }

    private int d() {
        return (int) ((34.7f * this.n) + 0.5f);
    }

    private float e() {
        return (16.0f * this.n) + 0.5f;
    }

    private int f() {
        return (int) ((5.0f * this.n) + 0.5f);
    }

    private int b(Rect rect) {
        return (getWidth() + rect.width()) / 2;
    }

    private int c(Rect rect) {
        return (getWidth() - rect.width()) / 2;
    }
}
