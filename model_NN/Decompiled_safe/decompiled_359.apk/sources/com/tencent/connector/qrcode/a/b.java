package com.tencent.connector.qrcode.a;

import android.content.Context;
import android.graphics.Point;
import android.hardware.Camera;
import android.os.Build;
import android.view.Display;
import android.view.WindowManager;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

/* compiled from: ProGuard */
final class b {

    /* renamed from: a  reason: collision with root package name */
    private static final String f3559a = b.class.getSimpleName();
    private static final Pattern b = Pattern.compile(",");
    private final Context c;
    private Point d;
    private Point e;
    private Point f;
    private int g;
    private String h;

    b(Context context) {
        this.c = context;
    }

    /* access modifiers changed from: package-private */
    public void a(Camera camera) {
        Camera.Parameters parameters = camera.getParameters();
        this.g = parameters.getPreviewFormat();
        this.h = parameters.get("preview-format");
        Display defaultDisplay = ((WindowManager) this.c.getSystemService("window")).getDefaultDisplay();
        this.d = new Point(defaultDisplay.getWidth(), defaultDisplay.getHeight());
        this.e = a(parameters, this.d);
        this.f = new Point(this.e.y, this.e.x);
    }

    /* access modifiers changed from: package-private */
    public void b(Camera camera) {
        Camera.Parameters parameters = camera.getParameters();
        parameters.setPreviewSize(this.e.x, this.e.y);
        a(parameters);
        if (d.f3560a <= 4) {
            parameters.set("rotation", 90);
            parameters.set("orientation", "portrait");
        } else if (d.f3560a > 7 || d.f3560a < 5) {
            a(camera, 90);
        } else if (Build.MODEL.equals("HTC Hero")) {
            a(camera, 90);
        } else {
            a(parameters, 90);
            parameters.set("rotation", 90);
            parameters.set("orientation", "portrait");
        }
        camera.setParameters(parameters);
    }

    /* access modifiers changed from: protected */
    public void a(Camera camera, int i) {
        try {
            Method method = camera.getClass().getMethod("setDisplayOrientation", Integer.TYPE);
            if (method != null) {
                method.invoke(camera, Integer.valueOf(i));
            }
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: protected */
    public void a(Camera.Parameters parameters, int i) {
        try {
            Method method = parameters.getClass().getMethod("setRotation", Integer.TYPE);
            if (method != null) {
                method.invoke(parameters, Integer.valueOf(i));
            }
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: package-private */
    public Point a() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public Point b() {
        return this.f;
    }

    /* access modifiers changed from: package-private */
    public Point c() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public int d() {
        return this.g;
    }

    /* access modifiers changed from: package-private */
    public String e() {
        return this.h;
    }

    private static Point a(Camera.Parameters parameters, Point point) {
        Point b2 = b(parameters, point);
        if (b2 == null) {
            return new Point((point.x >> 3) << 3, (point.y >> 3) << 3);
        }
        return b2;
    }

    private static int a(CharSequence charSequence, int i) {
        String[] split = b.split(charSequence);
        int length = split.length;
        int i2 = 0;
        int i3 = 0;
        while (i2 < length) {
            try {
                double parseDouble = Double.parseDouble(split[i2].trim());
                int i4 = (int) (10.0d * parseDouble);
                if (Math.abs(((double) i) - parseDouble) >= ((double) Math.abs(i - i3))) {
                    i4 = i3;
                }
                i2++;
                i3 = i4;
            } catch (NumberFormatException e2) {
                return i;
            }
        }
        return i3;
    }

    private void a(Camera.Parameters parameters) {
        String str = parameters.get("zoom-supported");
        if (str == null || Boolean.parseBoolean(str)) {
            int i = 27;
            String str2 = parameters.get("max-zoom");
            if (str2 != null) {
                try {
                    int parseDouble = (int) (Double.parseDouble(str2) * 10.0d);
                    if (27 <= parseDouble) {
                        parseDouble = 27;
                    }
                    i = parseDouble;
                } catch (NumberFormatException e2) {
                }
            }
            String str3 = parameters.get("taking-picture-zoom-max");
            if (str3 != null) {
                try {
                    int parseInt = Integer.parseInt(str3);
                    if (i > parseInt) {
                        i = parseInt;
                    }
                } catch (NumberFormatException e3) {
                }
            }
            String str4 = parameters.get("mot-zoom-values");
            if (str4 != null) {
                i = a(str4, i);
            }
            String str5 = parameters.get("mot-zoom-step");
            if (str5 != null) {
                try {
                    int parseDouble2 = (int) (Double.parseDouble(str5.trim()) * 10.0d);
                    if (parseDouble2 > 1) {
                        i -= i % parseDouble2;
                    }
                } catch (NumberFormatException e4) {
                }
            }
            if (!(str2 == null && str4 == null)) {
                parameters.set("zoom", String.valueOf(((double) i) / 10.0d));
            }
            if (str3 != null) {
                parameters.set("taking-picture-zoom", i);
            }
        }
    }

    private static Point b(Camera.Parameters parameters, Point point) {
        ArrayList<Camera.Size> arrayList;
        int i;
        int i2;
        Point point2;
        List<Camera.Size> supportedPreviewSizes = parameters.getSupportedPreviewSizes();
        if (supportedPreviewSizes == null) {
            arrayList = new ArrayList<>();
        } else {
            arrayList = new ArrayList<>(supportedPreviewSizes);
        }
        Collections.sort(arrayList, new c());
        StringBuilder sb = new StringBuilder();
        for (Camera.Size size : arrayList) {
            sb.append(size.width).append('x').append(size.height).append(' ');
        }
        Point point3 = null;
        float f2 = ((float) point.x) / ((float) point.y);
        float f3 = Float.POSITIVE_INFINITY;
        for (Camera.Size size2 : arrayList) {
            int i3 = size2.width;
            int i4 = size2.height;
            int i5 = i3 * i4;
            if (i5 >= 150400 && i5 <= 2073600) {
                boolean z = i3 > i4;
                if (z) {
                    i = i4;
                } else {
                    i = i3;
                }
                if (z) {
                    i2 = i3;
                } else {
                    i2 = i4;
                }
                if (i == point.x && i2 == point.y) {
                    return new Point(i3, i4);
                }
                float abs = Math.abs((((float) i) / ((float) i2)) - f2);
                if (abs < f3) {
                    point2 = new Point(i3, i4);
                } else {
                    abs = f3;
                    point2 = point3;
                }
                point3 = point2;
                f3 = abs;
            }
        }
        if (point3 != null) {
            return point3;
        }
        Camera.Size previewSize = parameters.getPreviewSize();
        if (previewSize == null) {
            return new Point();
        }
        return new Point(previewSize.width, previewSize.height);
    }
}
