package com.tencent.android.qqdownloader.wxapi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import com.qq.AppService.AstApp;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.g.o;
import com.tencent.assistant.login.b.a;
import com.tencent.assistant.utils.XLog;
import com.tencent.mm.sdk.modelbase.BaseReq;
import com.tencent.mm.sdk.modelbase.BaseResp;
import com.tencent.mm.sdk.modelmsg.SendMessageToWX;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.sdk.openapi.WXAPIFactory;

/* compiled from: ProGuard */
public class WXEntryActivity extends Activity implements IWXAPIEventHandler {
    private static String b = "WXEntryActivity";

    /* renamed from: a  reason: collision with root package name */
    private IWXAPI f379a;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        XLog.d(b, "WXEntryActivity--onCreate");
        this.f379a = WXAPIFactory.createWXAPI(this, "wx3909f6add1206543", false);
        this.f379a.registerApp("wx3909f6add1206543");
        try {
            this.f379a.handleIntent(getIntent(), this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        try {
            this.f379a.handleIntent(intent, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onReq(BaseReq baseReq) {
    }

    public void onResp(BaseResp baseResp) {
        int i;
        int i2 = 4;
        boolean z = true;
        XLog.d(b, "WXEntryActivity--onResp");
        if (a.h().i()) {
            a.h().a(baseResp);
            a.h().j();
        }
        if (baseResp instanceof SendMessageToWX.Resp) {
            if (o.a() != 1) {
                z = false;
            }
            if (baseResp.errCode == 0) {
                Message obtainMessage = AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_SHARE_SUCCESS);
                if (z) {
                    i = 4;
                } else {
                    i = 3;
                }
                obtainMessage.obj = Integer.valueOf(i);
                AstApp.i().j().sendMessage(obtainMessage);
            } else {
                Message obtainMessage2 = AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_SHARE_FAIL);
                if (!z) {
                    i2 = 3;
                }
                obtainMessage2.obj = Integer.valueOf(i2);
                AstApp.i().j().sendMessage(obtainMessage2);
            }
        }
        finish();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        XLog.d(b, "WXEntryActivity--onDestroy--configuration " + getChangingConfigurations());
        if (this.f379a != null) {
            this.f379a.unregisterApp();
        }
    }
}
