package com.tencent.connect.share;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.tencent.assistant.component.txscrollview.TXTabBarLayout;
import com.tencent.open.utils.AsynLoadImgBack;

/* compiled from: ProGuard */
final class g extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AsynLoadImgBack f3505a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    g(Looper looper, AsynLoadImgBack asynLoadImgBack) {
        super(looper);
        this.f3505a = asynLoadImgBack;
    }

    public void handleMessage(Message message) {
        switch (message.what) {
            case TXTabBarLayout.TABITEM_TIPS_TEXT_ID:
                this.f3505a.batchSaved(0, message.getData().getStringArrayList("images"));
                return;
            default:
                super.handleMessage(message);
                return;
        }
    }
}
