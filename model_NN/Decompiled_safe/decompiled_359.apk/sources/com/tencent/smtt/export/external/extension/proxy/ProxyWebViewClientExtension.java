package com.tencent.smtt.export.external.extension.proxy;

import android.os.Bundle;
import com.tencent.smtt.export.external.extension.interfaces.IX5WebViewClientExtension;
import com.tencent.smtt.export.external.extension.interfaces.IX5WebViewExtension;

public abstract class ProxyWebViewClientExtension implements IX5WebViewClientExtension {
    protected IX5WebViewClientExtension mWebViewClientExt;

    public void onPreReadFinished() {
        if (this.mWebViewClientExt != null) {
            this.mWebViewClientExt.onPreReadFinished();
        }
    }

    public void onPromptScaleSaved() {
        if (this.mWebViewClientExt != null) {
            this.mWebViewClientExt.onPromptScaleSaved();
        }
    }

    public void onUrlChange(String src, String dest) {
        if (this.mWebViewClientExt != null) {
            this.mWebViewClientExt.onUrlChange(src, dest);
        }
    }

    public void onHideListBox() {
        if (this.mWebViewClientExt != null) {
            this.mWebViewClientExt.onHideListBox();
        }
    }

    public void onShowListBox(String[] array, int[] enabledArray, int[] displayParamters, int selection) {
        if (this.mWebViewClientExt != null) {
            this.mWebViewClientExt.onShowListBox(array, enabledArray, displayParamters, selection);
        }
    }

    public void onShowMutilListBox(String[] array, int[] enabledArray, int[] displayParamters, int[] selection) {
        if (this.mWebViewClientExt != null) {
            this.mWebViewClientExt.onShowMutilListBox(array, enabledArray, displayParamters, selection);
        }
    }

    public void onInputBoxTextChanged(IX5WebViewExtension mWebView, String newValue) {
        if (this.mWebViewClientExt != null) {
            this.mWebViewClientExt.onInputBoxTextChanged(mWebView, newValue);
        }
    }

    public void onTransitionToCommitted() {
        if (this.mWebViewClientExt != null) {
            this.mWebViewClientExt.onTransitionToCommitted();
        }
    }

    public void onUploadProgressStart(int arg1) {
        if (this.mWebViewClientExt != null) {
            this.mWebViewClientExt.onUploadProgressStart(arg1);
        }
    }

    public void onUploadProgressChange(int arg1, int arg2, String obj) {
        if (this.mWebViewClientExt != null) {
            this.mWebViewClientExt.onUploadProgressChange(arg1, arg2, obj);
        }
    }

    public void onFlingScrollBegin(int duration, int arg1, int arg2) {
        if (this.mWebViewClientExt != null) {
            this.mWebViewClientExt.onFlingScrollBegin(duration, arg1, arg2);
        }
    }

    public void onFlingScrollEnd() {
        if (this.mWebViewClientExt != null) {
            this.mWebViewClientExt.onFlingScrollEnd();
        }
    }

    public void onScrollChanged(int oldx, int oldy, int newx, int newy) {
        if (this.mWebViewClientExt != null) {
            this.mWebViewClientExt.onScrollChanged(oldx, oldy, newx, newy);
        }
    }

    public void onSoftKeyBoardShow() {
        if (this.mWebViewClientExt != null) {
            this.mWebViewClientExt.onSoftKeyBoardShow();
        }
    }

    public void onSoftKeyBoardHide(int scrollY) {
        if (this.mWebViewClientExt != null) {
            this.mWebViewClientExt.onSoftKeyBoardHide(scrollY);
        }
    }

    public void onSetButtonStatus(boolean IsTextFieldPrev, boolean mIsTextFieldNext) {
        if (this.mWebViewClientExt != null) {
            this.mWebViewClientExt.onSetButtonStatus(IsTextFieldPrev, mIsTextFieldNext);
        }
    }

    public void onHistoryItemChanged() {
        if (this.mWebViewClientExt != null) {
            this.mWebViewClientExt.onHistoryItemChanged();
        }
    }

    public void hideAddressBar() {
        if (this.mWebViewClientExt != null) {
            this.mWebViewClientExt.hideAddressBar();
        }
    }

    public void handlePluginTag(String url, String mimeType, boolean isClick, String outHTML) {
        if (this.mWebViewClientExt != null) {
            this.mWebViewClientExt.handlePluginTag(url, mimeType, isClick, outHTML);
        }
    }

    public void onDoubleTapStart() {
        if (this.mWebViewClientExt != null) {
            this.mWebViewClientExt.onDoubleTapStart();
        }
    }

    public void onPinchToZoomStart() {
        if (this.mWebViewClientExt != null) {
            this.mWebViewClientExt.onPinchToZoomStart();
        }
    }

    public void onSlidingTitleOffScreen(int yDistanceToSlideTitleOffScreen, int slideTitleDuration) {
        if (this.mWebViewClientExt != null) {
            this.mWebViewClientExt.onSlidingTitleOffScreen(yDistanceToSlideTitleOffScreen, slideTitleDuration);
        }
    }

    public boolean preShouldOverrideUrlLoading(IX5WebViewExtension view, String url) {
        if (this.mWebViewClientExt != null) {
            return this.mWebViewClientExt.preShouldOverrideUrlLoading(view, url);
        }
        return false;
    }

    public void onMissingPluginClicked(String mimeType, String resoureURL, String pluginPageURL, int installType) {
        if (this.mWebViewClientExt != null) {
            this.mWebViewClientExt.onMissingPluginClicked(mimeType, resoureURL, pluginPageURL, installType);
        }
    }

    public void onReportAdFilterInfo(int adBlockNum, int popupBlockNum, String url, boolean isMobileSite) {
        if (this.mWebViewClientExt != null) {
            this.mWebViewClientExt.onReportAdFilterInfo(adBlockNum, popupBlockNum, url, isMobileSite);
        }
    }

    public void onNativeCrashReport(int type, String extraMsg) {
        if (this.mWebViewClientExt != null) {
            this.mWebViewClientExt.onNativeCrashReport(type, extraMsg);
        }
    }

    public Object onMiscCallBack(String method, Bundle bundle) {
        if (this.mWebViewClientExt != null) {
            return this.mWebViewClientExt.onMiscCallBack(method, bundle);
        }
        return null;
    }
}
