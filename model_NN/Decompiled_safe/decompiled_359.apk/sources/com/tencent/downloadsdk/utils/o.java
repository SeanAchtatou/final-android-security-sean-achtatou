package com.tencent.downloadsdk.utils;

import java.util.Vector;

public class o {
    private static o b;

    /* renamed from: a  reason: collision with root package name */
    private Vector<byte[]> f3654a = new Vector<>();

    private o() {
    }

    public static synchronized o a() {
        o oVar;
        synchronized (o.class) {
            if (b == null) {
                b = new o();
            }
            oVar = b;
        }
        return oVar;
    }

    public synchronized void a(byte[] bArr) {
    }

    public synchronized byte[] a(int i) {
        return new byte[i];
    }
}
