package com.tencent.downloadsdk;

import android.text.TextUtils;
import java.util.ArrayList;

class j implements ai {

    /* renamed from: a  reason: collision with root package name */
    protected ArrayList<String> f3609a = new ArrayList<>();
    protected ArrayList<String> b = new ArrayList<>();
    final /* synthetic */ c c;
    private int d = 0;

    public j(c cVar) {
        this.c = cVar;
        this.f3609a.addAll(cVar.k);
    }

    public int a() {
        return this.b.size();
    }

    public String a(int i) {
        if (i >= 0 && i < this.b.size()) {
            String str = this.b.get(i);
            if (!TextUtils.isEmpty(str)) {
                return str;
            }
        }
        return null;
    }

    public void a(String str) {
        if (!TextUtils.isEmpty(str)) {
            if (this.b.contains(str)) {
                this.b.remove(str);
            }
            this.b.add(0, str);
        }
    }

    public int b() {
        return this.f3609a.size();
    }

    public String b(int i) {
        if (i >= 0 && i < this.f3609a.size()) {
            String str = this.f3609a.get(i);
            if (!TextUtils.isEmpty(str)) {
                this.d = i;
                return str;
            }
        }
        return null;
    }

    public void c() {
        int size = this.f3609a.size();
        for (int i = this.d; i < size; i++) {
            String str = this.f3609a.get(i);
            if (!TextUtils.isEmpty(str)) {
                this.b.add(str);
            }
        }
    }
}
