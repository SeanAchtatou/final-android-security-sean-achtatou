package com.tencent.downloadsdk;

public class i {

    /* renamed from: a  reason: collision with root package name */
    public int f3608a;
    public byte[] b;

    private i(int i, byte[] bArr) {
        this.f3608a = i;
        this.b = bArr;
    }

    static i a(int i) {
        return new i(i, null);
    }

    static i a(int i, byte[] bArr) {
        return new i(i, bArr);
    }
}
