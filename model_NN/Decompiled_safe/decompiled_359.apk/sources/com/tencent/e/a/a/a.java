package com.tencent.e.a.a;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Environment;
import android.os.SystemClock;
import com.tencent.connect.common.Constants;
import java.io.File;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class a {

    /* renamed from: a  reason: collision with root package name */
    protected String f3660a = Constants.STR_EMPTY;

    public a() {
        if ("mounted".equals(Environment.getExternalStorageState()) && Environment.getExternalStorageDirectory().canWrite()) {
            this.f3660a = String.valueOf(Environment.getExternalStorageDirectory().getPath()) + "/tencent/assistant/";
            File file = new File(this.f3660a);
            if (!file.exists()) {
                file.mkdirs();
            }
            this.f3660a = String.valueOf(this.f3660a) + ".SystemConfig.db";
        }
    }

    public boolean a(long j) {
        SQLiteDatabase b = b();
        if (b == null) {
            return false;
        }
        b.delete("channeldata", "itemId=?", new String[]{new StringBuilder(String.valueOf(j)).toString()});
        a(b);
        return true;
    }

    public ArrayList<c> a() {
        ArrayList<c> arrayList = null;
        SQLiteDatabase c = c();
        if (c != null) {
            Cursor rawQuery = c.rawQuery("select * from channeldata", null);
            if (rawQuery != null && rawQuery.moveToFirst()) {
                int columnIndex = rawQuery.getColumnIndex("itemId");
                int columnIndex2 = rawQuery.getColumnIndex("itemData");
                arrayList = new ArrayList<>();
                do {
                    int i = rawQuery.getInt(columnIndex);
                    c a2 = c.a(rawQuery.getBlob(columnIndex2));
                    if (a2 != null) {
                        a2.f3662a = i;
                        arrayList.add(a2);
                    }
                } while (rawQuery.moveToNext());
                a(c);
            }
            rawQuery.close();
            a(c);
        }
        return arrayList;
    }

    private SQLiteDatabase b() {
        SQLiteDatabase sQLiteDatabase = null;
        int i = 0;
        while (true) {
            if (i < 20) {
                try {
                    sQLiteDatabase = SQLiteDatabase.openOrCreateDatabase(this.f3660a, (SQLiteDatabase.CursorFactory) null);
                } catch (SQLiteException e) {
                }
                if (sQLiteDatabase != null) {
                    b(sQLiteDatabase);
                    break;
                }
                SystemClock.sleep(50);
                i++;
            } else {
                break;
            }
        }
        return sQLiteDatabase;
    }

    private synchronized SQLiteDatabase c() {
        SQLiteDatabase sQLiteDatabase = null;
        synchronized (this) {
            if (new File(this.f3660a).exists()) {
                for (int i = 0; i < 20; i++) {
                    try {
                        sQLiteDatabase = SQLiteDatabase.openDatabase(this.f3660a, null, 1);
                    } catch (SQLiteException e) {
                    }
                    if (sQLiteDatabase != null) {
                        break;
                    }
                    SystemClock.sleep(50);
                }
            }
        }
        return sQLiteDatabase;
    }

    private void a(SQLiteDatabase sQLiteDatabase) {
        if (sQLiteDatabase != null && sQLiteDatabase.isOpen()) {
            try {
                sQLiteDatabase.close();
            } catch (Exception e) {
            }
        }
    }

    private void b(SQLiteDatabase sQLiteDatabase) {
        int version = sQLiteDatabase.getVersion();
        if (version != 1) {
            sQLiteDatabase.beginTransaction();
            if (version == 0) {
                try {
                    sQLiteDatabase.execSQL("CREATE TABLE if not exists channeldata( itemId INTEGER PRIMARY KEY AUTOINCREMENT, itemData BLOB);");
                } catch (Throwable th) {
                    sQLiteDatabase.endTransaction();
                    throw th;
                }
            }
            sQLiteDatabase.setVersion(1);
            sQLiteDatabase.setTransactionSuccessful();
            sQLiteDatabase.endTransaction();
        }
    }
}
