package com.tencent.wcs.f;

import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

/* compiled from: ProGuard */
public class c implements Delayed {

    /* renamed from: a  reason: collision with root package name */
    public AtomicLong f3853a;
    private long b;
    private int c;
    private long d = System.currentTimeMillis();

    public c(int i, long j) {
        this.c = i;
        this.b = j;
        this.f3853a = new AtomicLong(System.nanoTime() + TimeUnit.NANOSECONDS.convert(j, TimeUnit.MILLISECONDS));
    }

    public int a() {
        return this.c;
    }

    public long b() {
        return this.d;
    }

    /* renamed from: a */
    public int compareTo(Delayed delayed) {
        if (this.f3853a.get() > ((c) delayed).f3853a.get()) {
            return 1;
        }
        if (this.f3853a.get() < ((c) delayed).f3853a.get()) {
            return -1;
        }
        return 0;
    }

    public long getDelay(TimeUnit timeUnit) {
        return timeUnit.convert(this.f3853a.get() - System.nanoTime(), TimeUnit.NANOSECONDS);
    }
}
