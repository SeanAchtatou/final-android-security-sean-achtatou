package com.tencent.wcs.f;

/* compiled from: ProGuard */
public abstract class h extends Thread {

    /* renamed from: a  reason: collision with root package name */
    protected volatile boolean f3856a;
    protected volatile boolean b;
    protected volatile boolean c;
    protected final Object d = new Object();
    protected final Object e = new Object();
    protected final int f;

    public abstract void a();

    public abstract void b();

    public abstract void c();

    public abstract boolean d();

    public abstract boolean e();

    public h(int i) {
        this.f = i;
        this.f3856a = false;
        this.b = false;
        this.c = false;
    }

    public void f() {
        this.f3856a = false;
        this.b = true;
        start();
    }

    public void g() {
        this.c = false;
        synchronized (this.e) {
            this.e.notify();
        }
    }

    public void h() {
        this.c = true;
    }

    public void i() {
        this.b = false;
        this.c = false;
        this.f3856a = false;
        interrupt();
    }

    public void j() {
        this.f3856a = false;
        if (e()) {
            synchronized (this.d) {
                this.d.notify();
            }
        }
    }

    public void run() {
        super.run();
        while (this.b) {
            if (!this.c) {
                this.f3856a = true;
                a();
                synchronized (this.d) {
                    try {
                        this.d.wait((long) (this.f * 1000));
                    } catch (InterruptedException e2) {
                        e2.printStackTrace();
                    }
                }
                b();
                if (this.b && !this.c && this.f3856a) {
                    c();
                    this.f3856a = false;
                    if (d()) {
                        return;
                    }
                }
            } else {
                synchronized (this.e) {
                    try {
                        this.e.wait();
                    } catch (InterruptedException e3) {
                        e3.printStackTrace();
                    }
                }
            }
        }
        return;
    }
}
