package com.tencent.wcs.proxy;

import android.content.Context;
import android.util.SparseArray;
import com.qq.AppService.AppService;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.utils.t;
import com.tencent.connect.common.Constants;
import com.tencent.wcs.c.b;
import com.tencent.wcs.f.g;
import com.tencent.wcs.jce.MMHeartBeat;
import com.tencent.wcs.proxy.a.c;
import com.tencent.wcs.proxy.b.n;
import com.tencent.wcs.proxy.b.q;
import com.tencent.wcs.proxy.c.a;
import com.tencent.wcs.proxy.c.d;
import com.tencent.wcs.proxy.c.e;
import com.tencent.wcs.proxy.c.f;

/* compiled from: ProGuard */
public class l implements d {

    /* renamed from: a  reason: collision with root package name */
    public volatile boolean f3923a = false;
    /* access modifiers changed from: private */
    public final Context b;
    /* access modifiers changed from: private */
    public final c c;
    /* access modifiers changed from: private */
    public final a d;
    private volatile g e;
    /* access modifiers changed from: private */
    public volatile c f;
    /* access modifiers changed from: private */
    public volatile com.tencent.wcs.proxy.d.a g;
    /* access modifiers changed from: private */
    public volatile boolean h = false;
    private final SparseArray<b> i;
    private final a j;
    private com.tencent.wcs.a.a k;

    public l(Context context, c cVar) {
        this.b = context;
        this.c = cVar;
        this.i = new SparseArray<>();
        this.d = new a(this, this);
        this.j = new a();
        this.k = new com.tencent.wcs.a.a();
    }

    public void a(c cVar) {
        if (this.f != null) {
            this.f.b();
        }
        this.f = cVar;
        this.f.a();
    }

    public c a() {
        return this.f;
    }

    public void a(com.tencent.wcs.proxy.d.a aVar) {
        this.g = aVar;
    }

    public boolean b() {
        if (this.d != null) {
            return this.d.e();
        }
        return false;
    }

    public boolean c() {
        if (this.d == null || this.f == null || !this.f.a(this.b) || !this.d.a()) {
            return false;
        }
        this.f.a();
        this.e = new g(this);
        this.e.f();
        this.j.c();
        return true;
    }

    public void d() {
        this.h = false;
        this.j.c();
        if (this.f != null) {
            this.f.b();
        }
        if (this.d != null) {
            this.d.b();
        }
        if (this.e != null) {
            this.e.i();
        }
        if (this.k != null) {
            this.k.b();
        }
    }

    public boolean e() {
        return this.h;
    }

    /* access modifiers changed from: private */
    public boolean p() {
        if (this.d != null) {
            return this.d.e();
        }
        return false;
    }

    public void f() {
        this.d.f();
    }

    public void g() {
        MMHeartBeat mMHeartBeat;
        if (this.d.e()) {
            if (!this.f3923a) {
                this.f3923a = true;
                b.a("WirelessProxyService sendHeartBeat only one ,deviceID: " + AppService.v() + " versionCode: " + t.o() + Constants.STR_EMPTY);
                mMHeartBeat = new MMHeartBeat(0, 0, AppService.v(), null, t.o() + Constants.STR_EMPTY);
            } else {
                mMHeartBeat = new MMHeartBeat(0, 0, AppService.v(), null, null);
            }
            a(100, mMHeartBeat);
        }
    }

    public void a(int i2, JceStruct jceStruct) {
        if (jceStruct == null) {
            b.a("WirelessProxyService sendMsg error,reason comdid: " + i2 + " JceStruct is null");
            return;
        }
        e eVar = (e) n.b().c((Object[]) new Void[0]);
        eVar.a(i2);
        eVar.a(jceStruct);
        this.d.a(eVar);
    }

    public void a(int i2, b bVar) {
        if (this.i != null) {
            this.i.put(i2, bVar);
        }
    }

    public void a(int i2) {
        if (this.i != null) {
            this.i.remove(i2);
        }
    }

    public void a(f fVar) {
        if (fVar == null || fVar.c() == null) {
            q.b().b((Object) fVar);
            return;
        }
        if (this.k != null) {
            if (this.k.a() == 0) {
                if (fVar.d() == 1997 || fVar.d() == 1202) {
                    b.a("WirelessProxyService compatible AuthVersion: " + fVar.d());
                    this.k.a(fVar.d());
                }
            } else if ((fVar.d() == 1997 || fVar.d() == 1202) && fVar.d() != this.k.a()) {
                b.a("WirelessProxyService Waring !!! hasAuth version : " + this.k.a() + " but new version: " + fVar.d());
                return;
            }
        }
        if (this.i != null) {
            b bVar = this.i.get(fVar.d());
            if (bVar != null) {
                bVar.a(fVar);
            } else {
                b.a("WirelessProxyService get unknown cmd " + fVar.d());
            }
        }
        if (this.j.a(fVar.d()) && this.c != null) {
            this.c.d();
        }
        if (fVar.d() != 1202) {
            q.b().b((Object) fVar);
        }
    }

    public void h() {
        if (this.e != null) {
            this.e.j();
        }
    }

    public void i() {
        if (!this.h) {
            this.h = true;
            com.tencent.wcs.proxy.e.a.a().a(new m(this));
        }
    }

    public void j() {
        com.tencent.wcs.proxy.e.a.a().a(new o(this));
    }

    public void k() {
        if (this.k != null) {
            this.k.b();
        }
    }

    public void l() {
        if (this.e != null) {
            this.e.h();
        }
    }

    public void m() {
        if (this.e != null) {
            this.e.g();
        }
    }

    public void n() {
        if (this.j != null) {
            this.j.a();
        }
    }

    public void o() {
        if (this.j != null) {
            this.j.b();
        }
    }
}
