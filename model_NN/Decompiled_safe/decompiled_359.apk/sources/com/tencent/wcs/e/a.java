package com.tencent.wcs.e;

import android.text.TextUtils;
import com.qq.AppService.AppService;
import com.qq.AppService.AstApp;
import com.qq.AppService.ad;
import com.qq.taf.jce.JceInputStream;
import com.tencent.assistant.st.STConstAction;
import com.tencent.wcs.jce.ServicePushSessionKey;
import com.tencent.wcs.jce.VerifySessionKeyRequest;
import com.tencent.wcs.jce.VerifySessionKeyResponse;
import com.tencent.wcs.proxy.b;
import com.tencent.wcs.proxy.c.f;
import com.tencent.wcs.proxy.l;

/* compiled from: ProGuard */
public class a implements b {

    /* renamed from: a  reason: collision with root package name */
    public ServicePushSessionKey f3850a;
    private l b;

    public a(l lVar) {
        this.b = lVar;
    }

    public String a() {
        if (this.f3850a != null) {
            return this.f3850a.a();
        }
        return null;
    }

    public void a(f fVar) {
        int d = fVar.d();
        if (d == 1206) {
            JceInputStream jceInputStream = new JceInputStream(fVar.c());
            ServicePushSessionKey servicePushSessionKey = new ServicePushSessionKey();
            try {
                servicePushSessionKey.readFrom(jceInputStream);
            } catch (Throwable th) {
                servicePushSessionKey = null;
            }
            if (servicePushSessionKey != null) {
                com.tencent.wcs.c.b.a("SessionKeyManager COMMAND_ID_PUSH_SESSION_KEY_REQUEST , " + servicePushSessionKey);
                this.f3850a = servicePushSessionKey;
                int b2 = servicePushSessionKey.b();
                if (b2 == 1) {
                    this.f3850a = null;
                    AppService.A();
                } else if (b2 == 2) {
                    if (TextUtils.isEmpty(this.f3850a.d())) {
                        this.f3850a = null;
                        return;
                    }
                    try {
                        ad.a(AstApp.i(), ad.a(AstApp.i(), b2, this.f3850a.d(), this.f3850a.e()));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    this.f3850a = null;
                } else if (b2 == 3) {
                    try {
                        ad.a(AstApp.i(), ad.a(AstApp.i(), b2, null, null));
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                    this.f3850a = null;
                } else if (this.f3850a != null && this.f3850a.b() == 0 && this.b != null) {
                    VerifySessionKeyRequest verifySessionKeyRequest = new VerifySessionKeyRequest();
                    verifySessionKeyRequest.a(AppService.I() != null ? Long.parseLong(AppService.I()) : 0);
                    verifySessionKeyRequest.c(AppService.v());
                    verifySessionKeyRequest.b(this.f3850a.c());
                    verifySessionKeyRequest.a(this.f3850a.a());
                    com.tencent.wcs.c.b.a("SessionKeyManager send COMMAND_ID_VERIFY_SESSION_REQUEST, " + verifySessionKeyRequest);
                    this.b.a((int) STConstAction.ACTION_HIT_SCAN_NEW_PC, verifySessionKeyRequest);
                }
            }
        } else if (d == 1209) {
            JceInputStream jceInputStream2 = new JceInputStream(fVar.c());
            VerifySessionKeyResponse verifySessionKeyResponse = new VerifySessionKeyResponse();
            verifySessionKeyResponse.readFrom(jceInputStream2);
            com.tencent.wcs.c.b.a("SessionKeyManager COMMAND_ID_VERIFY_SESSION_RESPONSE , " + verifySessionKeyResponse);
            if (verifySessionKeyResponse.a() != 0) {
                this.f3850a = null;
            }
        }
    }
}
