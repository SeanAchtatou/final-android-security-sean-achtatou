package com.tencent.wcs.c;

import com.tencent.assistant.utils.XLog;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

/* compiled from: ProGuard */
public class a extends Thread {

    /* renamed from: a  reason: collision with root package name */
    private static final SimpleDateFormat f3847a = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss.SSS ");
    private static final SimpleDateFormat b = new SimpleDateFormat("yyyyMMdd");
    private BlockingQueue<String> c;
    private volatile boolean d;

    public a() {
        this.d = false;
        this.c = new LinkedBlockingQueue(30);
        this.d = true;
    }

    public void a(String str) {
        try {
            this.c.put(f3847a.format(Calendar.getInstance().getTime()) + str + "\r\n");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void run() {
        String str;
        while (this.d) {
            if (this.c != null) {
                try {
                    str = this.c.poll(5000, TimeUnit.MILLISECONDS);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    str = null;
                }
                if (str == null) {
                    continue;
                } else if (this.d) {
                    XLog.f(str, b.format(Calendar.getInstance().getTime()) + "_wcs.log", true);
                } else {
                    return;
                }
            }
        }
    }
}
