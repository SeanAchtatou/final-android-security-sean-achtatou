package com.tencent.wcs.agent;

import com.qq.taf.jce.JceInputStream;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.wcs.agent.config.a;
import com.tencent.wcs.b.c;
import com.tencent.wcs.jce.RecvPacketSeqList;
import com.tencent.wcs.jce.ServiceTransData;
import com.tencent.wcs.proxy.b.b;
import com.tencent.wcs.proxy.b.k;
import com.tencent.wcs.proxy.e.d;
import com.tencent.wcs.proxy.l;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.concurrent.PriorityBlockingQueue;

/* compiled from: ProGuard */
public class h {

    /* renamed from: a  reason: collision with root package name */
    private Socket f3835a;
    private InputStream b;
    private OutputStream c;
    private Runnable d;
    private Runnable e;
    private volatile boolean f = false;
    /* access modifiers changed from: private */
    public volatile boolean g = false;
    private final Object h = new Object();
    private final Object i = new Object();
    private final Object j = new Object();
    /* access modifiers changed from: private */
    public final a k;
    private final l l;
    /* access modifiers changed from: private */
    public final int m;
    /* access modifiers changed from: private */
    public final int n;
    /* access modifiers changed from: private */
    public final int o;
    private PriorityBlockingQueue<ServiceTransData> p;
    private volatile int q;
    private volatile int r;
    /* access modifiers changed from: private */
    public p s;
    private volatile q t;
    private final String u;

    static /* synthetic */ int h(h hVar) {
        int i2 = hVar.r;
        hVar.r = i2 + 1;
        return i2;
    }

    public h(int i2, int i3, String str, l lVar, a aVar) {
        this.m = i2;
        this.n = i3;
        this.u = str;
        this.p = new PriorityBlockingQueue<>(c.e + 10, new i(this));
        this.l = lVar;
        this.k = aVar;
        this.t = new q(this);
        this.o = this.k.a(i2);
    }

    public void a() {
        if (!this.f) {
            synchronized (this.j) {
                try {
                    InetSocketAddress inetSocketAddress = new InetSocketAddress("127.0.0.1", this.o);
                    this.f3835a = new Socket();
                    this.f3835a.setTcpNoDelay(true);
                    this.f3835a.setKeepAlive(false);
                    this.f3835a.connect(inetSocketAddress, EventDispatcherEnum.CACHE_EVENT_START);
                    if (this.f3835a != null) {
                        try {
                            synchronized (this.i) {
                                this.b = this.f3835a.getInputStream();
                            }
                            synchronized (this.h) {
                                this.c = this.f3835a.getOutputStream();
                            }
                        } catch (IOException e2) {
                            e2.printStackTrace();
                            return;
                        }
                    }
                    this.q = 0;
                    this.r = 0;
                    this.f = true;
                    this.e = new j(this);
                    this.d = new k(this);
                    b.a().a(this.e);
                    b.a().a(this.d);
                } catch (UnknownHostException e3) {
                    e3.printStackTrace();
                } catch (IOException e4) {
                    e4.printStackTrace();
                }
            }
        }
    }

    public void b() {
        if (this.f) {
            this.f = false;
            if (this.t != null) {
                this.t.h();
            }
            synchronized (this.j) {
                synchronized (this.i) {
                    try {
                        if (this.b != null) {
                            this.b.close();
                            this.b = null;
                        }
                    } catch (IOException e2) {
                    }
                }
                synchronized (this.h) {
                    try {
                        if (this.c != null) {
                            this.c.close();
                            this.c = null;
                        }
                    } catch (IOException e3) {
                    }
                }
                try {
                    if (this.f3835a != null) {
                        this.f3835a.close();
                        this.f3835a = null;
                    }
                } catch (IOException e4) {
                }
                if (this.e != null) {
                    this.e = null;
                }
                if (this.d != null) {
                    this.d = null;
                }
                if (this.p != null) {
                    if (!this.p.isEmpty()) {
                        this.p.clear();
                    }
                    this.p = null;
                }
                this.g = false;
            }
            this.q = 0;
            this.r = 0;
        }
    }

    public void a(p pVar) {
        synchronized (this.j) {
            this.s = pVar;
        }
    }

    public boolean c() {
        return this.f;
    }

    public void a(ServiceTransData serviceTransData) {
        if (c() && this.p != null) {
            this.p.put(serviceTransData);
            if (this.t != null) {
                this.t.a();
            }
        }
    }

    /* access modifiers changed from: private */
    public void e() {
        while (this.f) {
            if (this.p != null) {
                ServiceTransData peek = this.p.peek();
                if (peek != null && peek.e() == this.q) {
                    if (this.t != null) {
                        this.t.e();
                    }
                    ServiceTransData poll = this.p.poll();
                    if (poll == null) {
                        continue;
                    } else {
                        int f2 = poll.f();
                        if (f2 == 1) {
                            this.q++;
                            k.b().b((Object) poll);
                        } else if (f2 == 0) {
                            synchronized (this.h) {
                                if (!(this.c == null || poll.c() == null)) {
                                    this.c.write(poll.c());
                                    this.c.flush();
                                }
                            }
                            if (!(this.t == null || poll.c() == null)) {
                                this.t.g();
                                this.t.a(this.q, poll.c().length);
                            }
                            this.q++;
                            k.b().b((Object) poll);
                        } else if (f2 == 5) {
                            if (this.t != null) {
                                this.t.f();
                            }
                            k.b().b((Object) poll);
                        } else if (f2 == 2) {
                            if (c.f3846a) {
                                com.tencent.wcs.c.b.a("AgentSession server calling close channelId " + this.m + " messageId " + this.n + " port " + this.o + " remote " + this.k.a() + ", close it now");
                            }
                            this.g = true;
                            if (this.s != null) {
                                this.s.a(this.n);
                            }
                            k.b().b((Object) poll);
                            return;
                        }
                    }
                } else if (peek == null || peek.e() >= this.q) {
                    if (!(peek == null || this.t == null || !this.t.c())) {
                        if (this.t.d()) {
                            com.tencent.wcs.c.b.a("AgentSession miss at " + this.q + ", for too many times, " + " channelId " + this.m + " messageId " + this.n + " port " + this.o + " remote " + this.k.a() + ", close it now");
                            int i2 = this.r;
                            this.r = i2 + 1;
                            a(null, i2, 2);
                            if (this.s != null) {
                                this.s.a(this.n);
                                return;
                            }
                            return;
                        }
                        if (c.f3846a) {
                            com.tencent.wcs.c.b.a("AgentSession expect " + this.q + ", but got " + peek.e() + " channelId " + this.m + " messageId " + this.n + " port " + this.o + " type " + peek.f() + " remote " + this.k.a() + ", after count overflow");
                        }
                        a(this.q, this.p);
                    }
                    if (this.t != null && this.t.b()) {
                        if (this.p != null) {
                            peek = this.p.peek();
                        }
                        if (peek == null || peek.e() != this.q) {
                            a(this.q, this.p);
                        }
                        if (c.f3846a) {
                            if (peek != null) {
                                com.tencent.wcs.c.b.a("AgentSession expect " + this.q + ", but got " + peek.e() + " channelId " + this.m + " messageId " + this.n + " port " + this.o + " type " + peek.f() + " remote " + this.k.a() + ", after time delay");
                            } else {
                                com.tencent.wcs.c.b.a("AgentSession expect " + this.q + ", but got null " + " channel id " + this.m + " messageId " + this.n + " port " + this.o + " remote " + this.k.a() + ", after time delay");
                            }
                        }
                    }
                } else {
                    k.b().b((Object) this.p.poll());
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void f() {
        int read;
        if (this.f) {
            byte[] bArr = new byte[49152];
            while (this.b != null && (read = this.b.read(bArr)) != -1) {
                byte[] bArr2 = new byte[read];
                System.arraycopy(bArr, 0, bArr2, 0, read);
                a(bArr2, this.r, 0);
                if (this.t != null) {
                    this.t.a(this.r, bArr2);
                }
                this.r++;
            }
            if (!this.g) {
                if (c.f3846a) {
                    com.tencent.wcs.c.b.a("AgentSession read input closed channelId " + this.m + " messageId " + this.n + " port " + this.o + " remote " + this.k.a() + ", close it now");
                }
                int i2 = this.r;
                this.r = i2 + 1;
                a(null, i2, 2);
                if (this.s != null) {
                    this.s.a(this.n);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void g() {
        if (this.f) {
            byte[] bArr = new byte[49152];
            boolean z = false;
            do {
                int i2 = 0;
                while (true) {
                    if (i2 >= 49152) {
                        break;
                    }
                    int length = bArr.length - i2;
                    if (this.b == null) {
                        z = true;
                        break;
                    }
                    int read = this.b.read(bArr, i2, length);
                    if (read > 0) {
                        i2 += read;
                    }
                    if (read == -1) {
                        z = true;
                        break;
                    }
                }
                byte[] bArr2 = new byte[i2];
                System.arraycopy(bArr, 0, bArr2, 0, i2);
                a(bArr2, this.r, 0);
                if (this.t != null) {
                    this.t.a(this.r, bArr2);
                }
                this.r++;
            } while (!z);
            if (!this.g) {
                int i3 = this.r;
                this.r = i3 + 1;
                a(null, i3, 2);
                if (this.s != null) {
                    this.s.a(this.n);
                }
            }
        }
    }

    public void a(byte[] bArr, int i2, int i3) {
        ServiceTransData b2 = b(bArr, i2, i3);
        if (this.l != null) {
            if (c.f3846a) {
                com.tencent.wcs.c.b.a("AgentSession echo data type " + i3 + " , at seq " + b2.e() + " channel id " + b2.b() + " message id " + b2.d() + " size " + (b2.c() != null ? b2.c().length : 0) + " remote " + this.k.a());
            }
            this.l.a(this.k.c(), b2);
        }
    }

    private ServiceTransData b(byte[] bArr, int i2, int i3) {
        ServiceTransData serviceTransData = (ServiceTransData) k.b().c((Object[]) new Void[0]);
        serviceTransData.b = this.m;
        serviceTransData.d = this.n;
        serviceTransData.f3886a = this.u;
        if (bArr == null || bArr.length <= c.h) {
            serviceTransData.c = bArr;
            serviceTransData.h = 0;
        } else {
            serviceTransData.c = d.a(bArr);
            serviceTransData.h = 1;
        }
        if (serviceTransData.c != null) {
            serviceTransData.g = serviceTransData.c.length;
        } else {
            serviceTransData.g = 0;
        }
        if (!(bArr == null || this.o == 14087)) {
            try {
                serviceTransData.c = com.qq.util.k.a("TencentMolo&&##%%!!!1234".getBytes("UTF-8"), serviceTransData.c);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        serviceTransData.e = i2;
        serviceTransData.f = i3;
        return serviceTransData;
    }

    private void a(int i2, PriorityBlockingQueue<ServiceTransData> priorityBlockingQueue) {
        if (this.t != null && priorityBlockingQueue != null) {
            Iterator<ServiceTransData> it = priorityBlockingQueue.iterator();
            ArrayList arrayList = new ArrayList();
            while (it.hasNext()) {
                arrayList.add(Integer.valueOf(it.next().e()));
            }
            Collections.sort(arrayList);
            this.t.a(i2, arrayList);
        }
    }

    public int d() {
        return this.o;
    }

    public void a(int i2) {
        if (this.t != null) {
            this.t.a(i2);
        }
    }

    public void a(int i2, byte[] bArr) {
        if (this.t != null) {
            ArrayList<Integer> arrayList = null;
            if (bArr != null) {
                JceInputStream jceInputStream = new JceInputStream(bArr);
                RecvPacketSeqList recvPacketSeqList = new RecvPacketSeqList();
                recvPacketSeqList.readFrom(jceInputStream);
                arrayList = recvPacketSeqList.a();
            }
            if (arrayList == null) {
                arrayList = new ArrayList<>();
            }
            this.t.a(i2, this.r, arrayList);
        }
    }
}
