package com.tencent.assistantv2.adapter.smartlist;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.tencent.assistant.component.homeEntry.HomeEntryTemplateView;
import com.tencent.assistant.model.b;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.bc;
import com.tencent.assistantv2.adapter.smartlist.SmartListAdapter;

/* compiled from: ProGuard */
public class DiscoverPageListAdapter extends SmartListAdapter {

    /* renamed from: a  reason: collision with root package name */
    private HomeEntryTemplateView f2903a = null;
    private View b = null;

    public DiscoverPageListAdapter(Context context, View view, b bVar) {
        super(context, view, bVar);
    }

    /* access modifiers changed from: protected */
    public SmartListAdapter.SmartListType a() {
        return SmartListAdapter.SmartListType.DiscoverPage;
    }

    /* access modifiers changed from: protected */
    public String a(int i) {
        return "07";
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        if (SmartItemType.BANNER.ordinal() != getItemViewType(i)) {
            return super.getView(i, view, viewGroup);
        }
        this.f2903a = bc.a(g(), this.f2903a);
        if (this.f2903a != null) {
            return this.f2903a;
        }
        XLog.v("home_entry", "DiscoverPageListAdapter--getView--create new exception ");
        if (this.b == null) {
            this.b = new LinearLayout(g());
        }
        return this.b;
    }
}
