package com.tencent.assistantv2.adapter.smartlist;

import android.content.Context;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import com.qq.AppService.AstApp;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.component.smartcard.ISmartcard;
import com.tencent.assistant.component.smartcard.SmartcardFactory;
import com.tencent.assistant.component.smartcard.SmartcardListener;
import com.tencent.assistant.component.txscrollview.TXGetMoreListView;
import com.tencent.assistant.component.txscrollview.TXRefreshGetMoreListView;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.g.p;
import com.tencent.assistant.m;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.a.i;
import com.tencent.assistant.model.b;
import com.tencent.assistant.model.d;
import com.tencent.assistant.model.e;
import com.tencent.assistant.model.r;
import com.tencent.assistant.module.callback.w;
import com.tencent.assistant.module.en;
import com.tencent.assistant.module.u;
import com.tencent.assistant.net.c;
import com.tencent.assistant.protocol.jce.ColorCardItem;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ba;
import com.tencent.assistant.utils.ct;
import com.tencent.assistantv2.activity.MainActivity;
import com.tencent.assistantv2.component.QuickBannerView;
import com.tencent.assistantv2.component.SoftwareBannerView;
import com.tencent.assistantv2.component.banner.f;
import com.tencent.assistantv2.component.topbanner.TopBannerView;
import com.tencent.assistantv2.component.topbanner.a;
import com.tencent.assistantv2.model.SimpleEbookModel;
import com.tencent.assistantv2.model.SimpleVideoModel;
import com.tencent.assistantv2.st.business.CostTimeSTManager;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.model.STCommonInfo;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: ProGuard */
public abstract class SmartListAdapter extends BaseAdapter implements UIEventListener {
    private static int u = SmartItemType.values().length;
    private boolean A = false;
    private a B;
    private TopBannerView C;
    private r D;
    private boolean E = true;
    private boolean F = false;
    private SmartcardListener G = new ag(this);

    /* renamed from: a  reason: collision with root package name */
    private boolean f2905a = false;
    private boolean b = false;
    private boolean c = false;
    private AstApp d = AstApp.i();
    private Context e;
    private List<f> f = new ArrayList();
    private List<ColorCardItem> g = new ArrayList();
    /* access modifiers changed from: private */
    public List<e> h = new ArrayList();
    private QuickBannerView i;
    private List<com.tencent.assistant.g.a> j;
    private int k = 1;
    private ListView l;
    private int m = 2000;
    private long n = -100;
    private long o = 0;
    private b p = new b();
    private int q = 0;
    private w r;
    private boolean s = false;
    private Map<String, Integer> t = new HashMap(30);
    private IViewInvalidater v;
    private boolean w = false;
    private e x = null;
    private com.tencent.assistantv2.st.b.b y = null;
    private String z = null;

    /* compiled from: ProGuard */
    public enum BannerType {
        None,
        HomePage,
        QuickEntrance,
        All
    }

    /* compiled from: ProGuard */
    public enum SmartListType {
        DiscoverPage,
        AppPage,
        SoftWare,
        SearchPage,
        GamePage,
        CategoryDetailPage
    }

    /* access modifiers changed from: protected */
    public abstract SmartListType a();

    /* access modifiers changed from: protected */
    public abstract String a(int i2);

    public SmartListAdapter(Context context, View view, b bVar) {
        this.e = context;
        this.p = bVar;
        if (view != null && (view instanceof TXRefreshGetMoreListView)) {
            this.l = ((TXRefreshGetMoreListView) view).getListView();
        } else if (view != null && (view instanceof TXGetMoreListView)) {
            this.l = ((TXGetMoreListView) view).getListView();
        } else if (view instanceof ListView) {
            this.l = (ListView) view;
        }
        this.d.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE, this);
        this.d.k().addUIEventListener(1016, this);
        this.d.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ENTRANCE_DOWNLOAD_PAGE, this);
        this.d.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ENTRANCE_UPDATE_PAGE, this);
        this.d.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS, this);
        this.d.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_LOGOUT, this);
        this.j = new ArrayList();
        this.y = new com.tencent.assistantv2.st.b.b();
    }

    public void a(a aVar) {
        this.B = aVar;
    }

    public void a(boolean z2, List<e> list, List<f> list2, List<ColorCardItem> list3) {
        a(z2, list, list2, list3, 0);
    }

    public void a(boolean z2, List<e> list, List<f> list2, List<ColorCardItem> list3, long j2) {
        if (list != null) {
            if (z2) {
                this.h.clear();
                this.f.clear();
                this.g.clear();
                if (!(list2 == null && list3 == null)) {
                    if (list2 != null) {
                        this.f.addAll(list2);
                    }
                    if (list3 != null) {
                        this.g.addAll(list3);
                    }
                    if (this.i != null) {
                        this.i.a(j2, this.f, this.g);
                    }
                }
                if (a() == SmartListType.AppPage) {
                    k.a(200501, CostTimeSTManager.TIMETYPE.END, System.currentTimeMillis());
                } else if (a() == SmartListType.GamePage) {
                    k.a((int) STConst.ST_PAGE_COMPETITIVE, CostTimeSTManager.TIMETYPE.END, System.currentTimeMillis());
                } else if (a() == SmartListType.DiscoverPage) {
                    k.a((int) STConst.ST_PAGE_COMPETITIVE, CostTimeSTManager.TIMETYPE.END, System.currentTimeMillis());
                }
                this.q++;
            }
            this.h.addAll(list);
            this.s = true;
            notifyDataSetChanged();
        }
    }

    public void a(boolean z2, List<e> list, List<f> list2, r rVar, List<ColorCardItem> list3, long j2) {
        if (list != null) {
            if (z2) {
                this.F = true;
                this.h.clear();
                this.f.clear();
                this.g.clear();
                if (!(list2 == null && list3 == null && rVar == null)) {
                    if (list2 != null) {
                        this.f.addAll(list2);
                    }
                    if (list3 != null) {
                        this.g.addAll(list3);
                    }
                    if (this.i != null) {
                        this.i.a(j2, this.f, this.g);
                    }
                    if (rVar != null) {
                        if ((this.D == null && rVar.f1671a != 0) || !(this.D == null || (rVar.f1671a == this.D.f1671a && this.D.m == rVar.m))) {
                            this.D = rVar;
                            XLog.d("jiaxinwu", "replace banner");
                        }
                        if (this.D != null) {
                            this.D.k = com.tencent.assistantv2.component.topbanner.b.a().e(this.D);
                            if (com.tencent.assistantv2.component.topbanner.b.a().d(this.D)) {
                                c(true);
                                if (this.C != null) {
                                    this.C.a(this.D);
                                }
                            } else {
                                c(false);
                                if (this.C != null) {
                                    this.C.a();
                                    this.B.c();
                                }
                            }
                        }
                    }
                }
                if (a() == SmartListType.AppPage) {
                    k.a(200501, CostTimeSTManager.TIMETYPE.END, System.currentTimeMillis());
                } else if (a() == SmartListType.GamePage) {
                    k.a((int) STConst.ST_PAGE_COMPETITIVE, CostTimeSTManager.TIMETYPE.END, System.currentTimeMillis());
                } else if (a() == SmartListType.DiscoverPage) {
                    k.a((int) STConst.ST_PAGE_COMPETITIVE, CostTimeSTManager.TIMETYPE.END, System.currentTimeMillis());
                }
                this.q++;
            }
            this.h.addAll(list);
            this.s = true;
            notifyDataSetChanged();
        }
    }

    public void a(int i2, long j2) {
        this.m = i2;
        this.n = j2;
    }

    public void a(int i2, long j2, long j3) {
        this.m = i2;
        this.n = j2;
        this.o = j3;
    }

    public void d() {
        this.h.clear();
        this.f.clear();
        this.g.clear();
        if (this.l != null) {
            for (int i2 = 0; i2 < this.l.getChildCount(); i2++) {
                View childAt = this.l.getChildAt(i2);
                if (childAt != null && (childAt.getTag() instanceof v)) {
                    ((v) childAt.getTag()).s.k = 0;
                }
            }
        }
        notifyDataSetChanged();
    }

    public void a(IViewInvalidater iViewInvalidater) {
        this.v = iViewInvalidater;
    }

    public void a(SimpleAppModel simpleAppModel) {
        if (this.h != null && simpleAppModel != null && !TextUtils.isEmpty(simpleAppModel.c)) {
            for (e next : this.h) {
                if (next != null && next.c() != null && simpleAppModel.c.equals(next.c().c)) {
                    next.c().ao = null;
                    this.w = true;
                    this.x = next;
                    notifyDataSetChanged();
                    return;
                }
            }
        }
    }

    public void a(String str) {
        this.z = str;
    }

    public String e() {
        return this.z;
    }

    public long f() {
        if (this.p == null) {
            return -1;
        }
        return this.p.a();
    }

    public void a(b bVar) {
        this.p = bVar;
    }

    public Context g() {
        return this.e;
    }

    public int h() {
        if (this.h != null) {
            return this.h.size();
        }
        return 0;
    }

    public int getCount() {
        int size = this.h != null ? this.h.size() : 0;
        if (this.c) {
            size++;
        }
        if (this.A) {
            return size + 1;
        }
        return size;
    }

    public boolean i() {
        return this.h != null && this.h.size() > 0;
    }

    public boolean j() {
        return this.s;
    }

    public Object getItem(int i2) {
        int c2 = c(i2);
        if (this.h == null || c2 < 0) {
            return null;
        }
        return this.h.get(c2);
    }

    public void k() {
        this.d.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE, this);
        this.d.k().removeUIEventListener(1016, this);
        this.d.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_DOWNLOADLIST_READY, this);
        en.a().unregister(this.r);
        p.a().c();
        for (com.tencent.assistant.g.a b2 : this.j) {
            b2.b();
        }
        if (this.i != null && (this.i instanceof SoftwareBannerView)) {
            ((SoftwareBannerView) this.i).h();
        }
    }

    public void l() {
        this.d.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE, this);
        this.d.k().addUIEventListener(1016, this);
        this.d.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_DOWNLOADLIST_READY, this);
        this.d.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ENTRANCE_UPDATE_PAGE, this);
        this.d.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ENTRANCE_DOWNLOAD_PAGE, this);
        this.d.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_LOGOUT, this);
        for (com.tencent.assistant.g.a a2 : this.j) {
            a2.a();
        }
        en.a().register(this.r);
        if (this.i != null && (this.i instanceof SoftwareBannerView)) {
            ((SoftwareBannerView) this.i).i();
        }
    }

    public void m() {
        this.d.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_ENTRANCE_DOWNLOAD_PAGE, this);
        this.d.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_ENTRANCE_UPDATE_PAGE, this);
        this.d.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS, this);
        this.d.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_ENTRANCE_UPDATE_PAGE, this);
        this.d.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_ENTRANCE_DOWNLOAD_PAGE, this);
        this.d.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_LOGOUT, this);
    }

    public View getView(int i2, View view, ViewGroup viewGroup) {
        e eVar = null;
        int c2 = c(i2);
        if (this.h != null && c2 >= 0 && c2 < this.h.size()) {
            eVar = this.h.get(c2);
        }
        int itemViewType = getItemViewType(i2);
        if (itemViewType >= SmartItemType.values().length && eVar != null) {
            i f2 = eVar.f();
            aa a2 = new aa().b(this.m).a(this.z).a(f()).a(i2);
            if (view == null || !(view instanceof ISmartcard)) {
                return SmartcardFactory.getInstance().createSmartcard(this.e, f2, this.G, this.q, a2, this.v);
            }
            return SmartcardFactory.getInstance().resetSmartcard((ISmartcard) view, this.e, f2, this.G, this.q, a2, this.v);
        } else if (SmartItemType.BANNER.ordinal() == itemViewType) {
            if (this.i == null) {
                if (a() == SmartListType.AppPage) {
                    this.i = new SoftwareBannerView(this.e, null, SmartListType.AppPage.ordinal());
                } else if (a() == SmartListType.GamePage) {
                    this.i = new SoftwareBannerView(this.e, null, SmartListType.GamePage.ordinal());
                } else {
                    this.i = new QuickBannerView(this.e, null, a().ordinal());
                }
                this.i.a((long) this.k, this.f, this.g);
            }
            return this.i;
        } else if (SmartItemType.TOPBANNER.ordinal() == itemViewType) {
            if (this.C == null) {
                this.C = new TopBannerView(this.e, null, SmartListType.DiscoverPage.ordinal());
                this.C.a(this.B);
                if (this.D != null) {
                    this.C.a(this.D);
                }
            }
            this.C.a(this.E, this.F);
            XLog.d("jiaxinwu", "refreshView(topBanner)");
            this.E = false;
            this.F = false;
            return this.C;
        } else {
            ab a3 = new ab().a(this.p).a(this.m).b(this.f2905a).c(this.b).a(this.n).a(this.l);
            if (this.w && this.x != null && this.x == eVar && this.x.c != null) {
                a3.a(true);
            }
            d e2 = u.e(eVar.c());
            STInfoV2 a4 = a(i2, eVar, 100, e2);
            if (!b() || ((this.e instanceof MainActivity) && ((MainActivity) this.e).a(a()))) {
                boolean z2 = true;
                if (c()) {
                    if (a4 == null || a4.scene != this.m) {
                        z2 = false;
                    } else {
                        z2 = true;
                    }
                }
                if (z2) {
                    a3.a(a4.scene);
                    if (this.y != null) {
                        this.y.exposure(a4);
                    }
                }
            }
            a3.a(a4);
            a3.a(e2);
            a3.a(this.y);
            a3.a(a());
            return ac.a(this.e, a3, view, SmartItemType.values()[itemViewType], i2, eVar, this.v);
        }
    }

    public int getItemViewType(int i2) {
        if (i2 == 0) {
            if (this.A) {
                return SmartItemType.TOPBANNER.ordinal();
            }
            if (this.c) {
                return SmartItemType.BANNER.ordinal();
            }
        }
        if (i2 == 1 && this.A && this.c) {
            return SmartItemType.BANNER.ordinal();
        }
        e eVar = this.h.get(c(i2));
        if (eVar.b == 2 && eVar.g != null) {
            String d2 = eVar.g.d();
            Integer num = this.t.get(d2);
            if (num == null) {
                int i3 = u;
                u = i3 + 1;
                num = Integer.valueOf(i3);
                if (num.intValue() >= getViewTypeCount()) {
                    num = Integer.valueOf(getViewTypeCount() - 1);
                }
                this.t.put(d2, num);
            }
            return num.intValue();
        } else if (s() || eVar.b == 5) {
            if (eVar.b == 1) {
                SimpleAppModel c2 = eVar.c();
                if (c2 != null) {
                    SimpleAppModel.CARD_TYPE card_type = c2.U;
                    if (SimpleAppModel.CARD_TYPE.NORMAL == card_type) {
                        if (eVar.g()) {
                            return SmartItemType.NORMAL.ordinal();
                        }
                        return SmartItemType.NORMAL_NO_REASON.ordinal();
                    } else if (SimpleAppModel.CARD_TYPE.QUALITY == card_type) {
                        return SmartItemType.COMPETITIVE.ordinal();
                    }
                }
            } else if (eVar.b == 3) {
                SimpleVideoModel d3 = eVar.d();
                if (d3 != null) {
                    SimpleVideoModel.CARD_TYPE card_type2 = d3.j;
                    if (card_type2 == SimpleVideoModel.CARD_TYPE.NORMAL) {
                        return SmartItemType.VIDEO_NORMAL.ordinal();
                    }
                    if (card_type2 == SimpleVideoModel.CARD_TYPE.RICH) {
                        return SmartItemType.VIDEO_RICH.ordinal();
                    }
                }
                return SmartItemType.VIDEO_NORMAL.ordinal();
            } else if (eVar.b == 4) {
                SimpleEbookModel e2 = eVar.e();
                if (e2 != null) {
                    SimpleEbookModel.CARD_TYPE card_type3 = e2.j;
                    if (card_type3 == SimpleEbookModel.CARD_TYPE.NORMAL) {
                        return SmartItemType.EBOOK_NORMAL.ordinal();
                    }
                    if (card_type3 == SimpleEbookModel.CARD_TYPE.RICH) {
                        return SmartItemType.EBOOK_RICH.ordinal();
                    }
                }
                return SmartItemType.EBOOK_NORMAL.ordinal();
            } else if (eVar.b == 5) {
                return SmartItemType.NORMAL_LIST.ordinal();
            }
            return SmartItemType.NORMAL.ordinal();
        } else if (eVar.g()) {
            return SmartItemType.NORMAL.ordinal();
        } else {
            return SmartItemType.NORMAL_NO_REASON.ordinal();
        }
    }

    private int c(int i2) {
        if (!this.A || !this.c) {
            return (this.A || this.c) ? i2 - 1 : i2;
        }
        return i2 - 2;
    }

    public int getViewTypeCount() {
        return SmartItemType.values().length + 30;
    }

    public void handleUIEvent(Message message) {
        DownloadInfo downloadInfo;
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE:
                if (message.obj instanceof DownloadInfo) {
                    downloadInfo = (DownloadInfo) message.obj;
                } else {
                    downloadInfo = null;
                }
                if (downloadInfo != null && !TextUtils.isEmpty(downloadInfo.downloadTicket)) {
                    for (SimpleAppModel next : q()) {
                        if (next != null) {
                            String q2 = next.q();
                            if (!TextUtils.isEmpty(q2) && q2.equals(downloadInfo.downloadTicket)) {
                                r();
                                return;
                            }
                        }
                    }
                    return;
                }
                return;
            case 1016:
                u.g(q());
                r();
                return;
            case EventDispatcherEnum.UI_EVENT_DOWNLOADLIST_READY:
                r();
                return;
            case EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS:
            case EventDispatcherEnum.UI_EVENT_LOGOUT:
                t();
                return;
            case EventDispatcherEnum.UI_EVENT_ENTRANCE_DOWNLOAD_PAGE:
                a(2, 0);
                return;
            case EventDispatcherEnum.UI_EVENT_ENTRANCE_UPDATE_PAGE:
                a(3, 0);
                a(4, 0);
                return;
            default:
                return;
        }
    }

    private List<SimpleAppModel> q() {
        ArrayList arrayList = new ArrayList();
        for (e next : this.h) {
            if (next != null && next.b == 1) {
                arrayList.add(next.c());
            }
        }
        return arrayList;
    }

    private void r() {
        ba.a().post(new af(this));
    }

    public void a(boolean z2) {
        this.c = z2;
    }

    public void b(boolean z2) {
        this.E = z2;
    }

    public boolean n() {
        if (this.D != null) {
            this.D.k = com.tencent.assistantv2.component.topbanner.b.a().e(this.D);
            if (com.tencent.assistantv2.component.topbanner.b.a().d(this.D)) {
                c(true);
            } else {
                c(false);
                if (this.C != null) {
                    this.C.a();
                    this.B.c();
                }
            }
        }
        return this.A;
    }

    public void c(boolean z2) {
        this.A = z2;
    }

    public long getItemId(int i2) {
        return (long) i2;
    }

    private boolean s() {
        if ((!c.d() || c.k()) && m.a().p()) {
            return false;
        }
        return true;
    }

    private void a(int i2, int i3) {
        i f2;
        ArrayList arrayList = new ArrayList();
        for (e next : this.h) {
            if (next.b == 2 && (f2 = next.f()) != null && f2.i == i2 && f2.j == i3) {
                arrayList.add(next);
                STInfoV2 sTInfoV2 = new STInfoV2(f2.i + 202900, STConst.ST_DEFAULT_SLOT, this.m, STConst.ST_DEFAULT_SLOT, 100);
                sTInfoV2.extraData = f2.j + "||" + f2.i + "|" + 1;
                k.a(sTInfoV2);
            }
        }
        if (arrayList.size() > 0) {
            this.h.removeAll(arrayList);
            notifyDataSetChanged();
        }
    }

    private void t() {
        i f2;
        ArrayList arrayList = new ArrayList();
        if (com.tencent.assistant.login.d.a().k()) {
            long p2 = com.tencent.assistant.login.d.a().p();
            for (e next : this.h) {
                if (next.b == 2 && (f2 = next.f()) != null && a(f2, p2)) {
                    arrayList.add(next);
                    STInfoV2 sTInfoV2 = new STInfoV2(f2.i + 202900, STConst.ST_DEFAULT_SLOT, this.m, STConst.ST_DEFAULT_SLOT, 100);
                    sTInfoV2.extraData = f2.j + "||" + f2.i + "|" + 1;
                    k.a(sTInfoV2);
                }
            }
            if (arrayList.size() > 0) {
                this.h.removeAll(arrayList);
                notifyDataSetChanged();
            }
        }
    }

    private boolean a(i iVar, long j2) {
        if (!(iVar instanceof com.tencent.assistant.model.a.k) || iVar.i != 7 || ((com.tencent.assistant.model.a.k) iVar).f1644a == j2) {
            return false;
        }
        return true;
    }

    private String d(int i2) {
        return a(i2) + "_" + ct.a(i2) + "|" + (i2 % 10);
    }

    public STInfoV2 a(int i2, e eVar, int i3, d dVar) {
        SimpleAppModel simpleAppModel;
        if (eVar != null) {
            simpleAppModel = eVar.c();
        } else {
            simpleAppModel = null;
        }
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.e, simpleAppModel, d(i2), i3, null, dVar);
        if (!(buildSTInfo == null || this.n == -100)) {
            buildSTInfo.updateContentId(STCommonInfo.ContentIdType.CATEGORY, this.n + "_" + this.o);
        }
        return buildSTInfo;
    }

    public boolean b() {
        return true;
    }

    public boolean c() {
        return false;
    }

    public void b(int i2) {
        if (this.C != null && i2 == 8) {
            this.A = false;
            notifyDataSetChanged();
        }
    }

    public boolean o() {
        if (this.A && this.C != null) {
            return true;
        }
        return false;
    }

    public BannerType p() {
        boolean z2;
        boolean z3 = false;
        if (!(this.i instanceof SoftwareBannerView)) {
            return BannerType.None;
        }
        if (this.i.e() == null || this.i.e().size() <= 0) {
            z2 = false;
        } else {
            z2 = true;
        }
        if (this.i.f() != null && this.i.f().size() > 0 && com.tencent.assistantv2.component.banner.e.a(this.i.f())) {
            z3 = true;
        }
        if (z2 && z3) {
            return BannerType.All;
        }
        if (z2) {
            return BannerType.HomePage;
        }
        if (z3) {
            return BannerType.QuickEntrance;
        }
        return BannerType.None;
    }
}
