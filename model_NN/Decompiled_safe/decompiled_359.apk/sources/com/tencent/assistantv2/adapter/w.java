package com.tencent.assistantv2.adapter;

import android.os.Bundle;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.link.b;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class w extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SimpleAppModel f2934a;
    final /* synthetic */ STInfoV2 b;
    final /* synthetic */ u c;

    w(u uVar, SimpleAppModel simpleAppModel, STInfoV2 sTInfoV2) {
        this.c = uVar;
        this.f2934a = simpleAppModel;
        this.b = sTInfoV2;
    }

    public void onTMAClick(View view) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("com.tencent.assistant.ACTION_URL", this.f2934a.aa);
        b.b(this.c.e, this.f2934a.aa.f1970a, bundle);
    }

    public STInfoV2 getStInfo() {
        if (this.b != null) {
            this.b.actionId = 200;
            this.b.status = "01";
        }
        return this.b;
    }
}
