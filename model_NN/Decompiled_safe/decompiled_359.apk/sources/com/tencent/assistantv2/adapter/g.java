package com.tencent.assistantv2.adapter;

import android.content.Intent;
import android.view.View;
import com.tencent.assistant.component.appdetail.CommentDetailTabView;
import com.tencent.assistant.component.appdetail.process.s;
import com.tencent.assistant.component.listener.OnTMAParamExClickListener;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistantv2.activity.AppDetailActivityV5;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class g extends OnTMAParamExClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SimpleAppModel f2891a;
    final /* synthetic */ int b;
    final /* synthetic */ CategoryDetailListAdapter c;

    g(CategoryDetailListAdapter categoryDetailListAdapter, SimpleAppModel simpleAppModel, int i) {
        this.c = categoryDetailListAdapter;
        this.f2891a = simpleAppModel;
        this.b = i;
    }

    public void onTMAClick(View view) {
        Intent intent = new Intent(this.c.e, AppDetailActivityV5.class);
        intent.putExtra(CommentDetailTabView.PARAMS_SIMPLE_MODEL_INFO, this.f2891a);
        this.c.e.startActivity(intent);
    }

    public STInfoV2 getStInfo(View view) {
        String str = "01";
        if (s.a(this.f2891a)) {
            str = "02";
        }
        return this.c.a(this.f2891a, this.b, str, 200);
    }
}
