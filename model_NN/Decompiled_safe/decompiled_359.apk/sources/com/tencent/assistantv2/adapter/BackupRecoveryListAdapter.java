package com.tencent.assistantv2.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.st.STConstAction;
import com.tencent.assistant.utils.bt;
import com.tencent.assistant.utils.ct;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoV2;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class BackupRecoveryListAdapter extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    private Context f2869a;
    private LayoutInflater b;
    private ArrayList<SimpleAppModel> c;
    private ArrayList<Boolean> d;
    private int e;
    private long f;

    public void a(List<SimpleAppModel> list, int i, long j) {
        this.c.clear();
        this.c.addAll(list);
        this.d.clear();
        for (int i2 = 0; i2 < list.size(); i2++) {
            this.d.add(true);
        }
        this.e = i;
        this.f = j;
        notifyDataSetChanged();
    }

    public int getCount() {
        return this.c.size();
    }

    public Object getItem(int i) {
        return this.c.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        f fVar;
        if (view == null) {
            f fVar2 = new f(this);
            view = this.b.inflate((int) R.layout.popwindow_backuprecovery_item, (ViewGroup) null);
            fVar2.f2890a = (TXImageView) view.findViewById(R.id.icon);
            fVar2.b = (TextView) view.findViewById(R.id.name);
            fVar2.c = (TextView) view.findViewById(R.id.size);
            fVar2.d = (ImageView) view.findViewById(R.id.check_box);
            view.setTag(fVar2);
            fVar = fVar2;
        } else {
            fVar = (f) view.getTag();
        }
        view.setTag(R.id.tma_st_slot_tag, b(i));
        a(fVar, i);
        return view;
    }

    private String b(int i) {
        return "03_" + ct.a(i + 1);
    }

    private void a(f fVar, int i) {
        SimpleAppModel simpleAppModel = this.c.get(i);
        fVar.f2890a.updateImageView(simpleAppModel.e, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
        fVar.b.setText(simpleAppModel.d);
        fVar.c.setText(bt.a(simpleAppModel.k));
        fVar.d.setSelected(this.d.get(i).booleanValue());
    }

    public void a(int i) {
        SimpleAppModel simpleAppModel = this.c.get(i);
        if (simpleAppModel != null) {
            byte[] bArr = simpleAppModel.y;
        }
        boolean booleanValue = this.d.get(i).booleanValue();
        this.d.set(i, Boolean.valueOf(!booleanValue));
        if (booleanValue) {
            this.e--;
            this.f -= this.c.get(i).k;
        } else {
            this.e++;
            this.f = this.c.get(i).k + this.f;
        }
        notifyDataSetChanged();
        if (this.f2869a instanceof BaseActivity) {
            BaseActivity baseActivity = (BaseActivity) this.f2869a;
            STInfoV2 sTInfoV2 = new STInfoV2(baseActivity.f(), b(i), baseActivity.p(), STConst.ST_DEFAULT_SLOT, booleanValue ? STConstAction.ACTION_HIT_APK_UNCHECK : STConstAction.ACTION_HIT_APK_CHECK);
            if (simpleAppModel != null) {
                sTInfoV2.extraData = simpleAppModel.c + "|" + simpleAppModel.g;
                sTInfoV2.updateWithSimpleAppModel(simpleAppModel);
            }
            k.a(sTInfoV2);
        }
    }

    public int a() {
        return this.e;
    }

    public long b() {
        return this.f;
    }

    public ArrayList<Boolean> c() {
        return this.d;
    }
}
