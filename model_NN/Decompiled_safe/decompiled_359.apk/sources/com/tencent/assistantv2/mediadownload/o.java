package com.tencent.assistantv2.mediadownload;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.download.SimpleDownloadInfo;
import com.tencent.assistant.download.k;
import com.tencent.assistant.event.EventDispatcher;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.manager.DownloadProxy;
import com.tencent.assistant.utils.FileUtil;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.ct;
import com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager;
import com.tencent.assistantv2.db.a.d;
import com.tencent.assistantv2.model.AbstractDownloadInfo;
import com.tencent.assistantv2.model.a;
import com.tencent.assistantv2.model.h;
import com.tencent.downloadsdk.DownloadManager;
import com.tencent.downloadsdk.DownloadTask;
import com.tencent.downloadsdk.ac;
import com.tencent.downloadsdk.ae;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: ProGuard */
public class o {
    private static final Object e = new Object();
    private static o f;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Map<String, h> f3297a = new ConcurrentHashMap(5);
    /* access modifiers changed from: private */
    public d b = new d();
    /* access modifiers changed from: private */
    public EventDispatcher c = AstApp.i().j();
    /* access modifiers changed from: private */
    public InstallUninstallDialogManager d = new InstallUninstallDialogManager();
    private Handler g = k.a();
    private ae h = new t(this);

    private o() {
        this.d.a(true);
        try {
            List<h> a2 = this.b.a();
            ArrayList<ac> a3 = DownloadManager.a().a(8);
            for (h next : a2) {
                next.d();
                this.f3297a.put(next.c, next);
            }
            if (a3 != null) {
                Iterator<ac> it = a3.iterator();
                while (it.hasNext()) {
                    ac next2 = it.next();
                    h hVar = this.f3297a.get(next2.b);
                    if (hVar != null) {
                        if (hVar.k == null) {
                            hVar.k = new a();
                        }
                        hVar.k.b = next2.c;
                        hVar.k.f3314a = next2.d;
                    }
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public boolean a(h hVar) {
        this.g.post(new p(this, hVar));
        return true;
    }

    public boolean b(h hVar) {
        if (hVar == null || TextUtils.isEmpty(hVar.c)) {
            return false;
        }
        if (hVar.e() || hVar.c()) {
            this.c.sendMessage(this.c.obtainMessage(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_SUCC, hVar));
            e(hVar);
            return true;
        } else if (!this.f3297a.containsKey(hVar.c) && TextUtils.isEmpty(hVar.f3307a)) {
            return false;
        } else {
            String a2 = hVar.a();
            if (TextUtils.isEmpty(a2) || !a2.startsWith("/data/data/")) {
                ArrayList arrayList = new ArrayList(1);
                arrayList.add(hVar.b);
                DownloadTask downloadTask = new DownloadTask(hVar.f(), hVar.c, 0, 0, hVar.a(), hVar.f3307a, arrayList, null);
                ae a3 = com.tencent.assistantv2.st.k.a(hVar.c, 0, 0, (byte) hVar.f(), hVar.l, SimpleDownloadInfo.UIType.NORMAL, SimpleDownloadInfo.DownloadType.VIDEO);
                downloadTask.b = DownloadInfo.getPriority(SimpleDownloadInfo.DownloadType.VIDEO, SimpleDownloadInfo.UIType.NORMAL);
                hVar.g = downloadTask.d();
                downloadTask.a(this.h);
                downloadTask.a(a3);
                DownloadManager.a().a(downloadTask);
                boolean d2 = d(hVar);
                if (DownloadManager.a().c(8, hVar.c) || DownloadProxy.a().h() < 2) {
                    if (hVar.i != AbstractDownloadInfo.DownState.DOWNLOADING) {
                        hVar.i = AbstractDownloadInfo.DownState.DOWNLOADING;
                        this.c.sendMessage(this.c.obtainMessage(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_START, hVar));
                    }
                    this.c.sendMessage(this.c.obtainMessage(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOADING, hVar));
                } else {
                    hVar.i = AbstractDownloadInfo.DownState.QUEUING;
                    this.c.sendMessage(this.c.obtainMessage(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_QUEUING, hVar));
                }
                if (d2) {
                    this.c.sendMessage(this.c.obtainMessage(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_ADD, hVar));
                }
                this.b.a(hVar);
                return true;
            }
            TemporaryThreadManager.get().start(new q(this));
            return false;
        }
    }

    private boolean d(h hVar) {
        try {
            if (!this.f3297a.containsKey(hVar.c)) {
                this.f3297a.put(hVar.c, hVar);
                hVar.e = System.currentTimeMillis();
                return true;
            }
            if (this.f3297a.get(hVar.c) != hVar) {
                this.f3297a.put(hVar.c, hVar);
            }
            return false;
        } catch (NullPointerException e2) {
            return false;
        }
    }

    private void e(h hVar) {
        if (hVar != null && !TextUtils.isEmpty(hVar.c)) {
            this.f3297a.put(hVar.c, hVar);
            this.b.a(hVar);
        }
    }

    public void a(String str) {
        TemporaryThreadManager.get().start(new r(this, str));
    }

    /* access modifiers changed from: private */
    public void d(String str) {
        h hVar;
        if (!TextUtils.isEmpty(str) && (hVar = this.f3297a.get(str)) != null) {
            DownloadManager.a().a(8, str);
            if (hVar.i == AbstractDownloadInfo.DownState.DOWNLOADING || hVar.i == AbstractDownloadInfo.DownState.QUEUING) {
                hVar.i = AbstractDownloadInfo.DownState.PAUSED;
                this.c.sendMessage(this.c.obtainMessage(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_PAUSE, hVar));
                this.b.a(hVar);
            }
        }
    }

    public boolean b(String str) {
        this.g.post(new s(this, str));
        return true;
    }

    public boolean a(String str, boolean z) {
        h remove;
        if (TextUtils.isEmpty(str) || (remove = this.f3297a.remove(str)) == null) {
            return false;
        }
        DownloadManager.a().b(8, str);
        this.b.a(str);
        if (z) {
            FileUtil.deleteFile(remove.h);
        }
        remove.i = AbstractDownloadInfo.DownState.DELETE;
        this.c.sendMessage(this.c.obtainMessage(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_DELETE, remove));
        return true;
    }

    public List<h> a() {
        return a(0);
    }

    private List<h> a(int i) {
        ArrayList arrayList = new ArrayList(this.f3297a.size());
        for (Map.Entry next : this.f3297a.entrySet()) {
            if (i == 0 || (i == 1 && (((h) next.getValue()).i == AbstractDownloadInfo.DownState.DOWNLOADING || ((h) next.getValue()).i == AbstractDownloadInfo.DownState.QUEUING))) {
                arrayList.add(next.getValue());
            }
        }
        return arrayList;
    }

    public List<h> b() {
        return a(1);
    }

    public h c(String str) {
        return this.f3297a.get(str);
    }

    public static o c() {
        if (f == null) {
            synchronized (e) {
                if (f == null) {
                    f = new o();
                }
            }
        }
        return f;
    }

    public void d() {
        TemporaryThreadManager.get().start(new w(this));
    }

    public void e() {
        TemporaryThreadManager.get().start(new x(this));
    }

    public int a(Context context, h hVar) {
        String[] b2;
        if (hVar == null) {
            return 0;
        }
        if (!hVar.e()) {
            return -4;
        }
        LocalApkInfo installedApkInfo = ApkResourceManager.getInstance().getInstalledApkInfo(hVar.r);
        if (installedApkInfo == null) {
            return -2;
        }
        if (installedApkInfo.mVersionCode < hVar.u) {
            return -3;
        }
        if (TextUtils.isEmpty(hVar.t)) {
            return -1;
        }
        try {
            Uri parse = Uri.parse(a(hVar, hVar.t));
            if (parse.getScheme().equals("intent")) {
                String host = parse.getHost();
                String query = parse.getQuery();
                Intent intent = new Intent(host);
                if (!TextUtils.isEmpty(query) && (b2 = ct.b(query, "&")) != null && b2.length > 0) {
                    for (String str : b2) {
                        String[] b3 = ct.b(str, "=");
                        if (str != null && str.length() > 1) {
                            intent.putExtra(b3[0], b3[1]);
                        }
                    }
                }
                intent.setFlags(268435456);
                context.startActivity(intent);
                return 0;
            }
            Intent intent2 = new Intent("android.intent.action.VIEW", parse);
            intent2.setFlags(268435456);
            context.startActivity(intent2);
            return 0;
        } catch (Exception e2) {
            e2.printStackTrace();
            return -2;
        }
    }

    public int c(h hVar) {
        LocalApkInfo installedApkInfo = ApkResourceManager.getInstance().getInstalledApkInfo(hVar.r);
        if (installedApkInfo == null) {
            return -2;
        }
        if (installedApkInfo.mVersionCode < hVar.u) {
            return -3;
        }
        return 0;
    }

    private String a(h hVar, String str) {
        if (TextUtils.isEmpty(str) || hVar == null || TextUtils.isEmpty(hVar.h)) {
            return str;
        }
        return str.replace("${video_path}", hVar.h);
    }

    public void f() {
        if (this.f3297a != null && this.f3297a.size() > 0) {
            for (Map.Entry<String, h> key : this.f3297a.entrySet()) {
                d((String) key.getKey());
            }
        }
    }

    public int g() {
        int i = 0;
        if (this.f3297a == null || this.f3297a.size() <= 0) {
            return 0;
        }
        Iterator<Map.Entry<String, h>> it = this.f3297a.entrySet().iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return i2;
            }
            Map.Entry next = it.next();
            if (next.getValue() != null && (((h) next.getValue()).i == AbstractDownloadInfo.DownState.DOWNLOADING || ((h) next.getValue()).i == AbstractDownloadInfo.DownState.QUEUING)) {
                i2++;
            }
            i = i2;
        }
    }

    public int h() {
        int i = 0;
        if (this.f3297a == null || this.f3297a.size() <= 0) {
            return 0;
        }
        Iterator<Map.Entry<String, h>> it = this.f3297a.entrySet().iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return i2;
            }
            Map.Entry next = it.next();
            if (next.getValue() != null && ((h) next.getValue()).i == AbstractDownloadInfo.DownState.FAIL) {
                i2++;
            }
            i = i2;
        }
    }
}
