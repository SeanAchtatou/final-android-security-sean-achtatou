package com.tencent.assistantv2.manager;

import com.tencent.assistant.AppConst;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.download.SimpleDownloadInfo;
import com.tencent.assistant.manager.DownloadProxy;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.u;
import com.tencent.assistant.module.w;

/* compiled from: ProGuard */
class c implements w {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f3272a;

    c(a aVar) {
        this.f3272a = aVar;
    }

    public boolean a(SimpleAppModel simpleAppModel) {
        boolean z;
        DownloadInfo a2 = DownloadProxy.a().a(simpleAppModel);
        AppConst.AppState d = u.d(simpleAppModel);
        if ((a2 != null && a2.uiType == SimpleDownloadInfo.UIType.NORMAL) || d == AppConst.AppState.INSTALLED || d == AppConst.AppState.UPDATE) {
            z = true;
        } else {
            z = false;
        }
        if (z || this.f3272a.f.contains(Long.valueOf(simpleAppModel.f1634a))) {
            return true;
        }
        return false;
    }
}
