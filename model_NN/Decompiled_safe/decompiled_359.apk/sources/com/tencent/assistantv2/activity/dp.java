package com.tencent.assistantv2.activity;

import android.view.View;
import android.view.ViewTreeObserver;

/* compiled from: ProGuard */
class dp implements ViewTreeObserver.OnGlobalLayoutListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ View f2839a;
    final /* synthetic */ SearchActivity b;

    dp(SearchActivity searchActivity, View view) {
        this.b = searchActivity;
        this.f2839a = view;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.activity.SearchActivity.a(com.tencent.assistantv2.activity.SearchActivity, boolean):boolean
     arg types: [com.tencent.assistantv2.activity.SearchActivity, int]
     candidates:
      com.tencent.assistantv2.activity.SearchActivity.a(com.tencent.assistantv2.activity.SearchActivity, int):int
      com.tencent.assistantv2.activity.SearchActivity.a(com.tencent.assistantv2.activity.SearchActivity, java.lang.String):java.lang.String
      com.tencent.assistantv2.activity.SearchActivity.a(com.tencent.assistantv2.activity.SearchActivity, com.tencent.assistant.model.SimpleAppModel):void
      com.tencent.assistantv2.activity.SearchActivity.a(com.tencent.assistantv2.activity.SearchActivity, com.tencent.assistant.protocol.jce.AdvancedHotWord):void
      com.tencent.assistantv2.activity.SearchActivity.a(com.tencent.assistantv2.component.search.SearchResultTabPagesBase$TabType, com.tencent.assistant.model.SimpleAppModel):void
      com.tencent.assistantv2.activity.SearchActivity.a(java.lang.String, int):void
      com.tencent.assistant.activity.BaseActivity.a(java.lang.String, java.lang.String):void
      com.tencent.assistantv2.activity.SearchActivity.a(com.tencent.assistantv2.activity.SearchActivity, boolean):boolean */
    public void onGlobalLayout() {
        int height = this.f2839a.getRootView().getHeight() - this.f2839a.getHeight();
        if (this.b.R != height) {
            int unused = this.b.R = height;
            if (height > 100) {
                if (!this.b.Q) {
                    boolean unused2 = this.b.Q = true;
                    this.b.a("002", 200);
                }
            } else if (this.b.Q) {
                boolean unused3 = this.b.Q = false;
            }
        }
    }
}
