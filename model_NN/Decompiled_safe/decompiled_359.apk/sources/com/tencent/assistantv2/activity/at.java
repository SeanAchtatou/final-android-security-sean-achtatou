package com.tencent.assistantv2.activity;

import android.view.View;
import android.widget.AdapterView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAItemExClickListener;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class at extends OnTMAItemExClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f2767a;
    final /* synthetic */ AssistantTabActivity b;

    at(AssistantTabActivity assistantTabActivity, int i) {
        this.b = assistantTabActivity;
        this.f2767a = i;
    }

    public void onTMAItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        this.b.x.a(this.f2767a, (String) view.getTag(R.id.plugin_list_action_tag), view);
    }

    public STInfoV2 getStInfo(View view) {
        String str = STConst.ST_DEFAULT_SLOT;
        if (view.getTag(R.id.tma_st_slot_tag) instanceof String) {
            str = (String) view.getTag(R.id.tma_st_slot_tag);
        }
        return this.b.a(str, STConst.ST_STATUS_DEFAULT, 200);
    }
}
