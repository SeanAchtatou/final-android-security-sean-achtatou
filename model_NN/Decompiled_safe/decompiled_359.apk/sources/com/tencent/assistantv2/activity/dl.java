package com.tencent.assistantv2.activity;

import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

/* compiled from: ProGuard */
class dl implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SearchActivity f2835a;

    dl(SearchActivity searchActivity) {
        this.f2835a = searchActivity;
    }

    public void run() {
        if (this.f2835a.u != null) {
            EditText a2 = this.f2835a.u.a();
            InputMethodManager inputMethodManager = (InputMethodManager) this.f2835a.getSystemService("input_method");
            if (inputMethodManager != null) {
                inputMethodManager.showSoftInput(a2, 1);
            }
        }
    }
}
