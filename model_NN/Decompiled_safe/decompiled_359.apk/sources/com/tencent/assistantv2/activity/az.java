package com.tencent.assistantv2.activity;

import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamExClickListener;
import com.tencent.assistant.protocol.jce.TagGroup;
import com.tencent.assistantv2.st.model.STCommonInfo;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class az extends OnTMAParamExClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CategoryDetailActivity f2772a;

    az(CategoryDetailActivity categoryDetailActivity) {
        this.f2772a = categoryDetailActivity;
    }

    public void onTMAClick(View view) {
        TagGroup tagGroup = (TagGroup) view.getTag();
        if (tagGroup != null) {
            long unused = this.f2772a.z = tagGroup.a();
            this.f2772a.v.a(this.f2772a.f(), this.f2772a.x, this.f2772a.z);
            if (this.f2772a.u != null) {
                this.f2772a.u.j();
                this.f2772a.u.a(this.f2772a.z);
            }
        }
    }

    public STInfoV2 getStInfo(View view) {
        int i;
        try {
            i = ((Integer) view.getTag(R.id.category_detail_btn_index)).intValue();
        } catch (Exception e) {
            e.printStackTrace();
            i = 0;
        }
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f2772a, 200);
        buildSTInfo.updateContentId(STCommonInfo.ContentIdType.CATEGORY, this.f2772a.x + "_" + this.f2772a.z);
        buildSTInfo.slotId = a.a("05", i);
        return buildSTInfo;
    }
}
