package com.tencent.assistantv2.activity;

import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.utils.ba;

/* compiled from: ProGuard */
class cf extends ApkResCallback.Stub {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ cc f2802a;

    cf(cc ccVar) {
        this.f2802a = ccVar;
    }

    public void onInstalledApkDataChanged(LocalApkInfo localApkInfo, int i) {
        ba.a().post(new cg(this));
    }
}
