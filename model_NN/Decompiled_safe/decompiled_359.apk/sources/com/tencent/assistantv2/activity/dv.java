package com.tencent.assistantv2.activity;

import android.text.TextUtils;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.e;
import com.tencent.assistant.module.callback.ac;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistantv2.activity.SearchActivity;
import com.tencent.assistantv2.model.b.f;
import com.tencent.assistantv2.model.b.g;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
class dv extends ac {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SearchActivity f2845a;

    private dv(SearchActivity searchActivity) {
        this.f2845a = searchActivity;
    }

    /* synthetic */ dv(SearchActivity searchActivity, df dfVar) {
        this(searchActivity);
    }

    public void a(g gVar, int i) {
        XLog.d("SearchActivity", "onNotifyUISearchHotWordsFinished:(errorcode)" + i);
        f fVar = null;
        if (gVar != null) {
            fVar = (f) gVar.a(Integer.valueOf(this.f2845a.L));
        }
        if (i == 0 && fVar != null) {
            XLog.d("SearchActivity", "onNotifyUISearchHotWordsFinished:" + this.f2845a.L + ":" + fVar.b());
            this.f2845a.v.a(fVar);
        }
        if (this.f2845a.H() == SearchActivity.Layer.Search) {
            XLog.d("SearchActivity", "onNotifyUISearchHotWordsFinished:(is Layer.Search)");
            if (i != 0) {
                XLog.d("SearchActivity", "onNotifyUISearchHotWordsFinished:(is Layer.Search): not ok");
                if (-800 == i) {
                    XLog.d("SearchActivity", "onNotifyUISearchHotWordsFinished:(is Layer.Search): unable");
                    this.f2845a.d(30);
                    return;
                }
                XLog.d("SearchActivity", "onNotifyUISearchHotWordsFinished:(is Layer.Search): others");
                this.f2845a.d(20);
            } else if (fVar == null) {
                this.f2845a.G();
            } else if (TextUtils.isEmpty(this.f2845a.u.a().getText())) {
                this.f2845a.C();
            }
        }
    }

    public void a(ArrayList<SimpleAppModel> arrayList, ArrayList<String> arrayList2, int i) {
        if (i != 0) {
            this.f2845a.c(8);
        } else if (arrayList.size() == 0 && arrayList2.size() == 0) {
            this.f2845a.c(8);
        } else {
            if (this.f2845a.f() != 200703) {
                this.f2845a.a(this.f2845a.q(), (String) null, (String) null);
            }
            this.f2845a.z.a(arrayList, arrayList2, this.f2845a.u.a().getText().toString());
            this.f2845a.D();
        }
    }

    public void a(int i, int i2, boolean z, int i3, ArrayList<String> arrayList, boolean z2, List<e> list, long j, String str, int i4) {
    }
}
