package com.tencent.assistantv2.activity;

import android.app.Activity;
import android.content.Intent;
import com.tencent.assistant.activity.PermissionActivity;
import com.tencent.assistant.activity.ReportActivity;
import com.tencent.assistant.component.appdetail.AppdetailFloatingDialog;

/* compiled from: ProGuard */
class s implements AppdetailFloatingDialog.IOnFloatViewListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppDetailActivityV5 f2861a;

    s(AppDetailActivityV5 appDetailActivityV5) {
        this.f2861a = appDetailActivityV5;
    }

    public void showPermission() {
        if (this.f2861a.ae != null) {
            Intent intent = new Intent(this.f2861a.z, PermissionActivity.class);
            intent.putStringArrayListExtra("com.tencent.assistant.PERMISSION_LIST", this.f2861a.ae.f1660a.f1993a.a().get(0).o);
            this.f2861a.z.startActivity(intent);
        }
    }

    public void showReport() {
        if (this.f2861a.ae != null) {
            Intent intent = new Intent(this.f2861a.z, ReportActivity.class);
            intent.putExtra("apkid", this.f2861a.ae.f1660a.f1993a.b.get(0).a());
            intent.putExtra("appid", this.f2861a.ae.f1660a.f1993a.f1989a.f1999a);
            this.f2861a.z.startActivity(intent);
        }
    }

    public void shareToQQ() {
        this.f2861a.v().b((Activity) this.f2861a.z, this.f2861a.a(this.f2861a.ae, this.f2861a.ac));
    }

    public void shareToQZ() {
        this.f2861a.v().a((Activity) this.f2861a.z, this.f2861a.a(this.f2861a.ae, this.f2861a.ac));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.g.e.a(android.content.Context, com.tencent.assistant.model.ShareAppModel, boolean):void
     arg types: [android.content.Context, com.tencent.assistant.model.ShareAppModel, int]
     candidates:
      com.tencent.assistant.g.e.a(android.app.Activity, com.tencent.assistant.model.ShareAppModel, boolean):void
      com.tencent.assistant.g.e.a(android.app.Activity, com.tencent.assistant.model.ShareBaseModel, boolean):void
      com.tencent.assistant.g.e.a(android.content.Context, com.tencent.assistant.model.ShareBaseModel, boolean):void
      com.tencent.assistant.g.e.a(android.content.Context, com.tencent.assistant.model.ShareAppModel, boolean):void */
    public void shareToWX() {
        this.f2861a.v().a(this.f2861a.z, this.f2861a.a(this.f2861a.ae, this.f2861a.ac), false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.g.e.a(android.content.Context, com.tencent.assistant.model.ShareAppModel, boolean):void
     arg types: [android.content.Context, com.tencent.assistant.model.ShareAppModel, int]
     candidates:
      com.tencent.assistant.g.e.a(android.app.Activity, com.tencent.assistant.model.ShareAppModel, boolean):void
      com.tencent.assistant.g.e.a(android.app.Activity, com.tencent.assistant.model.ShareBaseModel, boolean):void
      com.tencent.assistant.g.e.a(android.content.Context, com.tencent.assistant.model.ShareBaseModel, boolean):void
      com.tencent.assistant.g.e.a(android.content.Context, com.tencent.assistant.model.ShareAppModel, boolean):void */
    public void shareToTimeLine() {
        this.f2861a.v().a(this.f2861a.z, this.f2861a.a(this.f2861a.ae, this.f2861a.ac), true);
    }

    public void doCollectioon() {
    }
}
