package com.tencent.assistantv2.activity;

import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.HorizontalScrollView;
import android.widget.TextView;
import android.widget.Toast;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.db.table.z;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.cq;
import com.tencent.assistant.manager.notification.v;
import com.tencent.assistant.module.callback.ac;
import com.tencent.assistant.module.callback.ag;
import com.tencent.assistant.module.timer.RecoverAppListReceiver;
import com.tencent.assistant.protocol.jce.DesktopShortCut;
import com.tencent.assistant.protocol.jce.GetPhoneUserAppListResponse;
import com.tencent.assistant.receiver.StorageLowReceiver;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.thumbnailCache.p;
import com.tencent.assistant.utils.FunctionUtils;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ba;
import com.tencent.assistant.utils.bx;
import com.tencent.assistant.utils.ct;
import com.tencent.assistant.utils.e;
import com.tencent.assistant.utils.t;
import com.tencent.assistantv2.adapter.q;
import com.tencent.assistantv2.adapter.smartlist.SmartListAdapter;
import com.tencent.assistantv2.b.ae;
import com.tencent.assistantv2.b.ak;
import com.tencent.assistantv2.b.al;
import com.tencent.assistantv2.b.am;
import com.tencent.assistantv2.component.MainActionHeaderView;
import com.tencent.assistantv2.component.TXViewPager;
import com.tencent.assistantv2.component.TabView;
import com.tencent.assistantv2.component.TopTabContainer;
import com.tencent.assistantv2.component.TopTabWidget;
import com.tencent.assistantv2.manager.MainTabType;
import com.tencent.assistantv2.st.business.StartUpCostTimeSTManager;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.assistantv2.st.page.c;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: ProGuard */
public class MainActivity extends BaseActivity implements UIEventListener {
    private static MainActivity J;
    public static int x = 0;
    public static int y;
    boolean A = true;
    boolean B = true;
    p C = new cn(this);
    private Bundle D = new Bundle();
    private TXViewPager E;
    /* access modifiers changed from: private */
    public q F;
    /* access modifiers changed from: private */
    public TopTabWidget G;
    /* access modifiers changed from: private */
    public TopTabContainer H;
    /* access modifiers changed from: private */
    public int I = 0;
    private boolean K = false;
    /* access modifiers changed from: private */
    public boolean L = true;
    /* access modifiers changed from: private */
    public HorizontalScrollView M;
    private int N;
    private int O = -1;
    /* access modifiers changed from: private */
    public float P = 0.0f;
    private ak Q;
    /* access modifiers changed from: private */
    public MainActionHeaderView R;
    private int S = 0;
    /* access modifiers changed from: private */
    public boolean T = false;
    private StorageLowReceiver U = null;
    private boolean V = false;
    private Toast W = null;
    private long X = 0;
    /* access modifiers changed from: private */
    public ArrayList<DesktopShortCut> Y = null;
    /* access modifiers changed from: private */
    public z Z = null;
    /* access modifiers changed from: private */
    public ag aa = new cm(this);
    /* access modifiers changed from: private */
    public ac ab = new co(this);
    MainTabType n = MainTabType.VIDEO;
    int t = 0;
    Intent u;
    int v = 5;
    db w = new db(this, null);
    GetPhoneUserAppListResponse z;

    public int f() {
        if (this.F == null || this.F.a(this.I) == null) {
            return 2000;
        }
        return ((ay) this.F.a(this.I)).J();
    }

    /* access modifiers changed from: protected */
    public boolean g() {
        return false;
    }

    public void onCreate(Bundle bundle) {
        k.a(StartUpCostTimeSTManager.TIMETYPE.B12_TIME, System.currentTimeMillis(), 0);
        J = this;
        super.onCreate(bundle);
        w();
        System.currentTimeMillis();
        overridePendingTransition(-1, -1);
        System.currentTimeMillis();
        try {
            setContentView((int) R.layout.main);
            this.Q = ae.a().a(0);
            this.R = (MainActionHeaderView) findViewById(R.id.myactionbar);
            this.H = (TopTabContainer) findViewById(R.id.mytabs);
            this.M = (HorizontalScrollView) findViewById(R.id.my_tab_scroller);
            this.G = this.H.a();
            this.E = (TXViewPager) findViewById(R.id.viewpager);
            this.P = (float) (getResources().getDimensionPixelSize(R.dimen.navigation_curcor_moving_lenth) + y);
            A();
            if (this.R != null) {
                this.R.a();
            }
            System.currentTimeMillis();
            x();
            this.H.a(this.I, this.P);
            f(this.I);
            c(getIntent());
            if (this.F != null && this.F.a(this.I) != null) {
                ((ay) this.F.a(this.I)).I();
            }
        } catch (Throwable th) {
            this.V = true;
            cq.a().b();
            finish();
        }
    }

    /* access modifiers changed from: private */
    public void b(boolean z2) {
        m.a().b(Constants.STR_EMPTY, "key_re_app_list_state", Integer.valueOf(RecoverAppListReceiver.RecoverAppListState.NOMAL.ordinal()));
        x = RecoverAppListReceiver.RecoverAppListState.NOMAL.ordinal();
        if (z2 && this.z != null && this.z.c.size() > 0) {
            ba.a().post(new ck(this));
        }
    }

    public static MainActivity i() {
        return J;
    }

    private void x() {
        int i = 0;
        int c = ae.a().c(0);
        while (true) {
            int i2 = i;
            if (i2 < this.Q.b.size()) {
                a(this.Q.b.get(i2), i2, c);
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        this.D.clear();
        c(intent);
    }

    private void c(Intent intent) {
        int i;
        int i2 = 0;
        this.u = intent;
        if (intent.getBooleanExtra("action_key_push_huanji", false)) {
            c.a(122, 14, false);
        }
        int intExtra = intent.getIntExtra("com.tencent.assistantv2.TAB_TYPE", MainTabType.DISCOVER.ordinal());
        Iterator<al> it = this.Q.b.iterator();
        while (true) {
            i = i2;
            if (!it.hasNext() || intExtra == it.next().b) {
                this.I = i;
                this.E.setCurrentItem(this.I);
                this.G.b(this.I).setSelected(true);
                h(this.I);
                Bundle extras = intent.getExtras();
            } else {
                i2 = i + 1;
            }
        }
        this.I = i;
        this.E.setCurrentItem(this.I);
        this.G.b(this.I).setSelected(true);
        h(this.I);
        Bundle extras2 = intent.getExtras();
        if (extras2 != null) {
            this.D.putAll(extras2);
        }
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS, this);
        TemporaryThreadManager.get().start(new cs(this));
    }

    public Bundle j() {
        Bundle bundle = new Bundle(this.D);
        this.D.clear();
        return bundle;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        int i;
        this.T = true;
        super.onResume();
        if (!this.V) {
            if (this.A) {
                i = 50;
            } else {
                i = 10;
            }
            if (this.K && this.I < this.F.getCount()) {
                ay ayVar = (ay) this.F.a(this.I);
                this.K = false;
                if (ayVar.L()) {
                    ba.a().postDelayed(new ct(this, ayVar), (long) i);
                }
            }
            this.S = this.A ? EventDispatcherEnum.CACHE_EVENT_START : 100;
            ba.a().postDelayed(new cu(this), (long) this.S);
            if (this.A) {
                this.A = false;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.T = false;
        super.onPause();
        if (!this.V) {
            overridePendingTransition(-1, -1);
            ba.a().post(new cv(this));
            this.K = true;
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        if (!this.V) {
            ba.a().postDelayed(new cw(this), (long) this.S);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (!this.V) {
            B();
            ba.a().postDelayed(new cx(this), (long) this.S);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS, this);
        }
    }

    private void f(int i) {
        k.a(StartUpCostTimeSTManager.TIMETYPE.B13_TIME, System.currentTimeMillis(), 0);
        this.E = (TXViewPager) findViewById(R.id.viewpager);
        if (this.F == null) {
            this.F = new q(e(), this, this.Q.b, null);
        }
        this.E.setAdapter(this.F);
        this.E.setOnPageChangeListener(new dd(this));
        this.G.a(new cy(this));
    }

    public void a(int i, boolean z2) {
        g(i);
        this.E.setCurrentItem(i);
        if (z2) {
            this.E.requestFocus(2);
        }
    }

    private void g(int i) {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this, 200);
        if (buildSTInfo != null) {
            buildSTInfo.slotId = a.a("04", i);
            if (i == this.F.getCount() - 1) {
                buildSTInfo.slotId = a.a("04", i + 2);
                buildSTInfo.status = b(i);
            } else if (this.F.getCount() >= 2 && i == this.F.getCount() - 2) {
                buildSTInfo.status = b(i);
            }
        }
        k.a(buildSTInfo);
    }

    /* access modifiers changed from: package-private */
    public String b(int i) {
        if (i == this.F.getCount() - 1) {
            return ct.b(this.Q.b.get(i).b);
        }
        if (this.F.getCount() < 2 || i != this.F.getCount() - 2) {
            return STConst.ST_STATUS_DEFAULT;
        }
        return ct.b(this.Q.b.get(i).b);
    }

    public boolean a(SmartListAdapter.SmartListType smartListType) {
        MainTabType a2 = this.Q.a(this.I).a();
        if (a2 == MainTabType.DISCOVER && smartListType == SmartListAdapter.SmartListType.DiscoverPage) {
            return true;
        }
        if (a2 == MainTabType.APP && smartListType == SmartListAdapter.SmartListType.AppPage) {
            return true;
        }
        if (a2 == MainTabType.GAME && smartListType == SmartListAdapter.SmartListType.GamePage) {
            return true;
        }
        return false;
    }

    private void a(al alVar, int i, int i2) {
        try {
            TabView tabView = new TabView(getBaseContext());
            TextView textView = (TextView) tabView.findViewById(R.id.title);
            String str = alVar.f2950a;
            if (str.length() > 2) {
                str = str.substring(0, 2);
            }
            textView.setText(str);
            if (i2 < alVar.c) {
                tabView.a(alVar.c);
                this.O = i;
            }
            tabView.setTag(R.id.tma_st_slot_tag, Integer.valueOf(alVar.b));
            tabView.setTag(Integer.valueOf(i));
            this.G.addView(tabView);
            if (i < this.Q.b.size() - 1) {
                this.G.a(i, this);
            }
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: private */
    public void a(int i, int i2, String str) {
        ay ayVar = (ay) this.F.a(i);
        if (ayVar != null) {
            a(((BaseActivity) ayVar.Q).q(), a.a("04", i2), str);
        }
    }

    /* access modifiers changed from: package-private */
    public void c(int i) {
        int i2;
        int i3;
        if (this.F.getCount() > 0) {
            if (this.N == 0) {
                this.N = getResources().getDimensionPixelSize(R.dimen.navigation_curcor_moving_lenth);
            }
            int i4 = ((this.N + y) * i) - y;
            try {
                i2 = getWindowManager().getDefaultDisplay().getWidth();
            } catch (Exception e) {
                Point point = new Point();
                getWindowManager().getDefaultDisplay().getSize(point);
                i2 = point.x;
            }
            int scrollX = this.M.getScrollX();
            if (i4 < scrollX) {
                i3 = i4 - scrollX;
            } else {
                i3 = ((this.N + i4) + y) - scrollX > i2 ? (((this.N + i4) + y) - scrollX) - i2 : 0;
            }
            if (i3 != 0) {
                dc dcVar = new dc(this);
                dcVar.a(scrollX, i3);
                this.M.post(dcVar);
            }
        }
    }

    /* access modifiers changed from: private */
    public void h(int i) {
        if (this.F.a(i) != null) {
            ay ayVar = (ay) this.F.a(i);
            d(ayVar.D());
            e(ayVar.E());
            if (this.O == i) {
                int c = ae.a().c(0) + 1;
                al a2 = this.Q.a(i);
                if (a2 == null || a2.c < c) {
                    ((TabView) this.G.b(i)).a(0);
                    return;
                }
                ((TabView) this.G.b(i)).a(c);
                ae.a().b(0);
            }
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4) {
            return false;
        }
        y();
        return true;
    }

    private void y() {
        try {
            long currentTimeMillis = System.currentTimeMillis();
            if (currentTimeMillis - this.X > 2000) {
                if (this.W != null) {
                    this.W.cancel();
                }
                this.X = currentTimeMillis;
                Toast makeText = Toast.makeText(this, getString(R.string.app_return_click_title), 0);
                this.W = makeText;
                makeText.show();
                return;
            }
            if (this.W != null) {
                Log.d("Donald", "toast.cancel");
                this.W.cancel();
            }
            FunctionUtils.a(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void d(int i) {
        if (this.R != null) {
            XLog.d("yanhui-srt", "main:type:" + i);
            this.R.a(i);
        }
    }

    public void e(int i) {
        if (this.R != null) {
            XLog.d("yanhui-hwc", "main:category:" + i);
            this.R.b(i);
        }
    }

    public void onWindowFocusChanged(boolean z2) {
        super.onWindowFocusChanged(z2);
        if (this.B) {
            this.B = false;
            z();
        }
    }

    private void z() {
        ba.a().postDelayed(new cz(this), 2000);
        TemporaryThreadManager.get().start(new cl(this));
    }

    /* access modifiers changed from: private */
    public void a(DesktopShortCut desktopShortCut) {
        this.Z.a(desktopShortCut);
        if (!TextUtils.isEmpty(desktopShortCut.c())) {
            if (ApkResourceManager.getInstance().getInstalledApkInfo(desktopShortCut.c()) == null && !TextUtils.isEmpty(desktopShortCut.a())) {
                com.tencent.assistant.thumbnailCache.k.b().a(desktopShortCut.a(), 1, this.C);
            }
        } else if (!TextUtils.isEmpty(desktopShortCut.e().a())) {
            com.tencent.assistant.thumbnailCache.k.b().a(desktopShortCut.a(), 1, this.C);
        }
    }

    /* access modifiers changed from: private */
    public boolean a(DesktopShortCut desktopShortCut, DesktopShortCut desktopShortCut2) {
        if (TextUtils.isEmpty(desktopShortCut.c()) || TextUtils.isEmpty(desktopShortCut2.c())) {
            if (TextUtils.isEmpty(desktopShortCut.e().f1970a) || TextUtils.isEmpty(desktopShortCut2.e().f1970a)) {
                return false;
            }
            return desktopShortCut.e().f1970a.equals(desktopShortCut2.e().f1970a);
        } else if (!desktopShortCut.c().equals(desktopShortCut2.c())) {
            return false;
        } else {
            if ((TextUtils.isEmpty(desktopShortCut.d()) || TextUtils.isEmpty(desktopShortCut2.d()) || !desktopShortCut.d().equals(desktopShortCut2.d())) && (!TextUtils.isEmpty(desktopShortCut.d()) || !TextUtils.isEmpty(desktopShortCut2.d()))) {
                return false;
            }
            return true;
        }
    }

    /* access modifiers changed from: private */
    public void a(ArrayList<DesktopShortCut> arrayList) {
        if (arrayList != null && arrayList.size() != 0) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < arrayList.size()) {
                    DesktopShortCut desktopShortCut = arrayList.get(i2);
                    if (!TextUtils.isEmpty(desktopShortCut.d) && ApkResourceManager.getInstance().getInstalledApkInfo(desktopShortCut.c()) != null && desktopShortCut.f() == 1) {
                        Intent intent = new Intent("com.tencent.assistant.SHORTCUT");
                        intent.setClassName("com.tencent.android.qqdownloader", "com.tencent.assistant.activity.ShortCutActivity");
                        intent.putExtra("pkgName", desktopShortCut.c());
                        if (!TextUtils.isEmpty(desktopShortCut.d())) {
                            intent.putExtra("channelId", desktopShortCut.d());
                        }
                        e.a(this, desktopShortCut.b(), intent);
                    }
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(Bitmap bitmap, String str, Intent intent) {
        e.a(this, bitmap, str, intent);
    }

    private void A() {
        this.U = new StorageLowReceiver();
        registerReceiver(this.U, new IntentFilter("android.intent.action.DEVICE_STORAGE_LOW"));
    }

    private void B() {
        if (this.U != null) {
            unregisterReceiver(this.U);
        }
    }

    public void showErrorPage(boolean z2) {
        if (this.F != null) {
            this.F.a(z2, this.t);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.manager.notification.v.a(com.tencent.assistant.protocol.jce.GetPhoneUserAppListResponse, boolean):void
     arg types: [com.tencent.assistant.protocol.jce.GetPhoneUserAppListResponse, int]
     candidates:
      com.tencent.assistant.manager.notification.v.a(int, long):void
      com.tencent.assistant.manager.notification.v.a(int, android.app.Notification):void
      com.tencent.assistant.manager.notification.v.a(int, java.lang.String):void
      com.tencent.assistant.manager.notification.v.a(java.lang.String, java.lang.String):void
      com.tencent.assistant.manager.notification.v.a(java.lang.String, boolean):void
      com.tencent.assistant.manager.notification.v.a(boolean, int):void
      com.tencent.assistant.manager.notification.v.a(com.tencent.assistant.protocol.jce.GetPhoneUserAppListResponse, boolean):void */
    /* access modifiers changed from: private */
    public void a(GetPhoneUserAppListResponse getPhoneUserAppListResponse, boolean z2) {
        if (AstApp.i().l()) {
            bx.a(this, false, f(), getPhoneUserAppListResponse, z2);
        } else {
            v.a().a(getPhoneUserAppListResponse, false);
        }
    }

    /* access modifiers changed from: private */
    public void d(Intent intent) {
        ba.a().post(new cp(this, intent));
    }

    /* access modifiers changed from: package-private */
    public void v() {
        am amVar = new am();
        amVar.register(this.w);
        amVar.a(true);
        amVar.a();
    }

    /* access modifiers changed from: package-private */
    public void b(Intent intent) {
        cr crVar = new cr(this);
        crVar.hasTitle = true;
        crVar.titleRes = getResources().getString(R.string.video_down_tips);
        crVar.blockCaller = true;
        boolean booleanExtra = intent.getBooleanExtra("action_key_push_huanji", false);
        XLog.e("zhangyuanchao", "-------isFromPush-------" + booleanExtra);
        if (booleanExtra) {
            crVar.contentRes = getResources().getString(R.string.recover_old_apps_txt_with_tips);
        } else {
            crVar.contentRes = getResources().getString(R.string.recover_old_apps_txt);
        }
        crVar.lBtnTxtRes = getResources().getString(R.string.down_page_dialog_left_del);
        crVar.rBtnTxtRes = getResources().getString(R.string.qq_login_title);
        com.tencent.assistant.utils.v.a(crVar);
        k.a(new STInfoV2(STConst.ST_PAGE_GUIDE_RESULT_HUANJI_QQ_LOGIN, STConst.ST_DEFAULT_SLOT, 0, STConst.ST_DEFAULT_SLOT, 100));
    }

    /* access modifiers changed from: package-private */
    public void w() {
        try {
            if ((t.c == 800 && t.b == 480) || ((t.c == 854 && t.b == 480) || ((t.c == 480 && t.b == 320) || ((t.c == 960 && t.b == 640) || (t.c == 320 && t.b == 240))))) {
                y = getResources().getDimensionPixelSize(R.dimen.navigation_curcor_moving_divider_lenth_for_special);
            } else {
                y = getResources().getDimensionPixelSize(R.dimen.navigation_curcor_moving_divider_lenth);
            }
        } catch (Exception e) {
        }
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS:
                if (message.obj instanceof Bundle) {
                    Bundle bundle = (Bundle) message.obj;
                    if (bundle.containsKey(AppConst.KEY_FROM_TYPE) && bundle.getInt(AppConst.KEY_FROM_TYPE) == 18) {
                        v();
                        return;
                    }
                    return;
                }
                return;
            default:
                return;
        }
    }
}
