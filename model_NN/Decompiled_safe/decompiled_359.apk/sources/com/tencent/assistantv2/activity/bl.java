package com.tencent.assistantv2.activity;

import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamExClickListener;
import com.tencent.assistant.protocol.jce.TagGroup;
import com.tencent.assistantv2.st.model.STCommonInfo;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class bl extends OnTMAParamExClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GameCategoryDetailActivity f2783a;

    bl(GameCategoryDetailActivity gameCategoryDetailActivity) {
        this.f2783a = gameCategoryDetailActivity;
    }

    public void onTMAClick(View view) {
        TagGroup tagGroup = (TagGroup) view.getTag();
        if (tagGroup != null) {
            long unused = this.f2783a.z = tagGroup.a();
            this.f2783a.v.a(this.f2783a.f(), this.f2783a.x, this.f2783a.z);
            if (this.f2783a.u != null) {
                this.f2783a.u.j();
                this.f2783a.u.k();
                this.f2783a.u.a(this.f2783a.z);
            }
        }
    }

    public STInfoV2 getStInfo(View view) {
        int i;
        try {
            int intValue = ((Integer) view.getTag(R.id.category_detail_btn_index)).intValue();
            TagGroup tagGroup = (TagGroup) view.getTag();
            if (tagGroup != null) {
                long unused = this.f2783a.z = tagGroup.a();
            }
            i = intValue;
        } catch (Exception e) {
            Exception exc = e;
            i = 0;
            exc.printStackTrace();
        }
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f2783a, 200);
        buildSTInfo.updateContentId(STCommonInfo.ContentIdType.CATEGORY, this.f2783a.x + "_" + this.f2783a.z);
        buildSTInfo.slotId = a.a("05", i);
        return buildSTInfo;
    }
}
