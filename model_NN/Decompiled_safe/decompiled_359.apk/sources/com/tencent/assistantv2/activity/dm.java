package com.tencent.assistantv2.activity;

import android.view.View;
import com.tencent.assistantv2.activity.SearchActivity;

/* compiled from: ProGuard */
class dm implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SearchActivity f2836a;

    dm(SearchActivity searchActivity) {
        this.f2836a = searchActivity;
    }

    public void onClick(View view) {
        this.f2836a.A.setVisibility(8);
        if (this.f2836a.H() == SearchActivity.Layer.Search) {
            this.f2836a.B.setVisibility(0);
            this.f2836a.y();
            this.f2836a.C();
        }
    }
}
