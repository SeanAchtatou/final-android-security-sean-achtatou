package com.tencent.assistantv2.activity;

import android.os.Bundle;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.adapter.AppCategoryListAdapter;
import com.tencent.assistant.component.GameCategoryListPage;
import com.tencent.assistant.component.invalidater.ViewPageScrollListener;
import com.tencent.assistant.module.by;
import com.tencent.assistant.module.callback.l;
import com.tencent.assistant.protocol.jce.AppCategory;
import com.tencent.assistant.protocol.jce.ColorCardItem;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import java.util.List;

/* compiled from: ProGuard */
public class GameCategoryActivity extends BaseActivity implements l {
    private SecondNavigationTitleViewV5 n;
    private by t;
    private GameCategoryListPage u;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.act_game_category);
        w();
        x();
    }

    private void w() {
        this.n = (SecondNavigationTitleViewV5) findViewById(R.id.title_view);
        this.n.i();
        this.n.d(false);
        this.n.a(this);
        this.n.b(getResources().getString(R.string.game_category_title));
        this.n.d(v());
        this.u = (GameCategoryListPage) findViewById(R.id.clp_act_list);
    }

    private void x() {
        this.t = new by();
        this.t.register(this);
        this.t.b();
    }

    private void y() {
        this.u.setCategoryType(i());
        this.u.onResume();
        this.u.setVisibility(0);
        this.u.setViewPageListener(new ViewPageScrollListener());
        List<ColorCardItem> c = this.t.c(i());
        List<AppCategory> a2 = this.t.a(i());
        List<AppCategory> b = this.t.b(i());
        AppCategoryListAdapter appCategoryListAdapter = new AppCategoryListAdapter(this, this.u, j(), null, null, this.t.a());
        appCategoryListAdapter.a(c, a2, b);
        this.u.setAdapter(appCategoryListAdapter);
        this.u.refreshData();
    }

    /* access modifiers changed from: protected */
    public long i() {
        return -2;
    }

    /* access modifiers changed from: protected */
    public AppCategoryListAdapter.CategoryType j() {
        return AppCategoryListAdapter.CategoryType.CATEGORYTYPEGAME;
    }

    /* access modifiers changed from: protected */
    public int v() {
        return 0;
    }

    public int f() {
        return STConst.ST_PAGE_GAME_CATEGORY;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.n.l();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.n.m();
    }

    public void a(int i, int i2, List<ColorCardItem> list, List<AppCategory> list2, List<AppCategory> list3) {
        y();
    }
}
