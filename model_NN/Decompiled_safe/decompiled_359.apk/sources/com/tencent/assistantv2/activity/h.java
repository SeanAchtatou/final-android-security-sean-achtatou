package com.tencent.assistantv2.activity;

import android.text.TextUtils;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.ShareAppDialog;
import com.tencent.assistant.component.appdetail.process.m;
import com.tencent.assistant.g.p;
import com.tencent.assistant.model.ShareAppModel;

/* compiled from: ProGuard */
class h implements m {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppDetailActivityV5 f2850a;

    h(AppDetailActivityV5 appDetailActivityV5) {
        this.f2850a = appDetailActivityV5;
    }

    public void a(int i) {
        boolean z;
        p.a().a(this.f2850a.f());
        if (i == 5) {
            z = p.a().a(this.f2850a.f(), this.f2850a);
        } else {
            z = false;
        }
        if (!z && this.f2850a.ac.j() && 5 == i) {
            ShareAppModel unused = this.f2850a.a(this.f2850a.ae, this.f2850a.ac);
            if (TextUtils.isEmpty(this.f2850a.ad.l)) {
                this.f2850a.ad.l = this.f2850a.getString(R.string.share_specail_app_default_local_content);
            }
            ShareAppDialog a2 = p.a().a(this.f2850a, this.f2850a.ad, false, 0, R.string.dialog_share_to_friend);
            if (a2 != null) {
                a2.setOnCancelListener(this.f2850a.bm);
            }
        }
        if (1 == i || 3 == i) {
            a();
        } else if (2 == i || 4 == i) {
            b();
        }
    }

    public void a() {
        ShareAppModel unused = this.f2850a.a(this.f2850a.ae, this.f2850a.ac);
        if (TextUtils.isEmpty(this.f2850a.ad.l)) {
            this.f2850a.ad.l = this.f2850a.getString(R.string.share_bate_default_local_content);
        }
        ShareAppDialog a2 = p.a().a(this.f2850a, this.f2850a.ad, true, R.drawable.yyb_mascot_celebrate, R.string.share_get_ticket);
        if (a2 != null) {
            a2.setOnCancelListener(this.f2850a.bm);
        }
    }

    public void b() {
        ShareAppModel unused = this.f2850a.a(this.f2850a.ae, this.f2850a.ac);
        if (TextUtils.isEmpty(this.f2850a.ad.l)) {
            this.f2850a.ad.l = this.f2850a.getString(R.string.share_take_num_default_local_content);
        }
        ShareAppDialog a2 = p.a().a(this.f2850a, this.f2850a.ad, true, R.drawable.yyb_mascot_celebrate, R.string.share_get_num);
        if (a2 != null) {
            a2.setOnCancelListener(this.f2850a.bm);
            a2.setCanceledOnTouchOutside(true);
        }
    }
}
