package com.tencent.assistantv2.model;

import android.os.Parcel;
import android.os.Parcelable;

/* compiled from: ProGuard */
final class f implements Parcelable.Creator<SimpleEbookModel> {
    f() {
    }

    /* renamed from: a */
    public SimpleEbookModel createFromParcel(Parcel parcel) {
        SimpleEbookModel simpleEbookModel = new SimpleEbookModel();
        simpleEbookModel.f3310a = parcel.readInt();
        simpleEbookModel.b = parcel.readString();
        simpleEbookModel.c = parcel.readString();
        simpleEbookModel.d = parcel.readString();
        simpleEbookModel.e = parcel.readString();
        simpleEbookModel.f = parcel.readString();
        simpleEbookModel.g = parcel.readString();
        simpleEbookModel.h = parcel.readString();
        simpleEbookModel.i = parcel.readString();
        simpleEbookModel.k = parcel.readString();
        simpleEbookModel.l = parcel.readString();
        return simpleEbookModel;
    }

    /* renamed from: a */
    public SimpleEbookModel[] newArray(int i) {
        return new SimpleEbookModel[i];
    }
}
