package com.tencent.assistantv2.model;

import android.text.TextUtils;
import com.tencent.assistant.utils.FileUtil;

/* compiled from: ProGuard */
public class d extends AbstractDownloadInfo {
    public String m;

    public static final d a(String str, String str2, String str3) {
        d dVar = new d();
        if (TextUtils.isEmpty(str)) {
            dVar.c = System.currentTimeMillis() + "_" + str2;
        } else {
            dVar.c = a(str, str2);
        }
        dVar.b = str3;
        dVar.e = System.currentTimeMillis();
        return dVar;
    }

    public static String a(String str, String str2) {
        return str + "_" + str2;
    }

    public int f() {
        return 100;
    }

    public String a() {
        return FileUtil.getDynamicFileDir();
    }

    public String g() {
        if (TextUtils.isEmpty(this.f3307a)) {
            return "downloadfile_" + this.e;
        }
        return this.f3307a;
    }

    public String toString() {
        return "FileDownInfo{, downUrl='" + this.b + '\'' + ", downId='" + this.c + '\'' + ", fileSize=" + this.d + ", createTime=" + this.e + ", finishTime=" + this.f + ", downloadingPath='" + this.g + '\'' + ", savePath='" + this.h + '\'' + ", downState=" + this.i + ", errorCode=" + this.j + ", DownResponse=" + this.k + '}';
    }
}
