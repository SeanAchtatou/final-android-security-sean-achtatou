package com.tencent.assistantv2.model;

import com.tencent.assistant.utils.FileUtil;

/* compiled from: ProGuard */
public class h extends d {
    public String n;
    public String o;
    public String p;
    public String q;
    public String r;
    public String s;
    public String t;
    public int u;
    public String v;
    public String w;

    public int f() {
        return 8;
    }

    public String a() {
        return FileUtil.getDynamicVideoDir();
    }

    public String toString() {
        return "VideoDownInfo{name='" + this.n + '\'' + ", desc='" + this.o + '\'' + ", downUrl='" + this.b + '\'' + ", filename='" + this.f3307a + '\'' + ", downId='" + this.c + '\'' + ", coverUrl='" + this.p + '\'' + ", fileSize=" + this.d + ", cpName='" + this.q + '\'' + ", createTime=" + this.e + ", finishTime=" + this.f + ", downloadingPath='" + this.g + '\'' + ", savePath='" + this.h + '\'' + ", downState=" + this.i + ", openPackageName='" + this.r + '\'' + ", openActivity='" + this.s + '\'' + ", openUri='" + this.t + '\'' + ", minVersionCode=" + this.u + ", playerName='" + this.v + '\'' + ", extraParams='" + this.w + '\'' + ", errorCode=" + this.j + ", videoDownResponse=" + this.k + '}';
    }
}
