package com.tencent.assistantv2.component.appdetail;

import android.text.TextUtils;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.link.b;
import com.tencent.assistantv2.activity.AppDetailActivityV5;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class q extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ r f3081a;
    final /* synthetic */ DetailGameNewsView b;

    q(DetailGameNewsView detailGameNewsView, r rVar) {
        this.b = detailGameNewsView;
        this.f3081a = rVar;
    }

    public void onTMAClick(View view) {
        if (view == this.b.f[0]) {
            b.a(this.b.f3050a, this.f3081a.c[0].e);
        } else if (view == this.b.f[1]) {
            b.a(this.b.f3050a, this.f3081a.c[1].e);
        } else if (view == this.b.f[2]) {
            b.a(this.b.f3050a, this.f3081a.c[2].e);
        } else if (view == this.b.g && !TextUtils.isEmpty(this.f3081a.b)) {
            b.a(this.b.f3050a, this.f3081a.b);
        } else if (view != this.b.j[0] || this.f3081a.c[0] == null) {
            if (view == this.b.j[1]) {
                if (!this.f3081a.c[1].d && !TextUtils.isEmpty(this.f3081a.c[1].f)) {
                    b.a(this.b.f3050a, this.f3081a.c[1].f);
                } else if (this.f3081a.c[1].d && !TextUtils.isEmpty(this.f3081a.c[1].e)) {
                    b.a(this.b.f3050a, this.f3081a.c[1].e);
                }
            } else if (view != this.b.j[2]) {
            } else {
                if (!this.f3081a.c[2].d && !TextUtils.isEmpty(this.f3081a.c[2].f)) {
                    b.a(this.b.f3050a, this.f3081a.c[2].f);
                } else if (this.f3081a.c[2].d && !TextUtils.isEmpty(this.f3081a.c[2].e)) {
                    b.a(this.b.f3050a, this.f3081a.c[2].e);
                }
            }
        } else if (!this.f3081a.c[0].d && !TextUtils.isEmpty(this.f3081a.c[0].f)) {
            b.a(this.b.f3050a, this.f3081a.c[0].f);
        } else if (this.f3081a.c[0].d && !TextUtils.isEmpty(this.f3081a.c[0].e)) {
            b.a(this.b.f3050a, this.f3081a.c[0].e);
        }
    }

    public STInfoV2 getStInfo() {
        if (!(this.b.f3050a instanceof AppDetailActivityV5)) {
            return null;
        }
        STInfoV2 i = ((AppDetailActivityV5) this.b.f3050a).i();
        if (this.clickViewId == this.b.g.getId()) {
            i.slotId = a.a("09", 0);
        } else if (this.clickViewId == this.b.j[0].getId()) {
            i.slotId = a.a("09", 1);
        } else if (this.clickViewId == this.b.j[1].getId()) {
            i.slotId = a.a("09", 2);
        } else if (this.clickViewId == this.b.j[2].getId()) {
            i.slotId = a.a("09", 3);
        }
        i.actionId = 200;
        return i;
    }
}
