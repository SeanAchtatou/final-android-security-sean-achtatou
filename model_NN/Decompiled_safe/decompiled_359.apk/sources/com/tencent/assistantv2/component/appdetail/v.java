package com.tencent.assistantv2.component.appdetail;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamExClickListener;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class v extends OnTMAParamExClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ FriendTalkView f3086a;

    v(FriendTalkView friendTalkView) {
        this.f3086a = friendTalkView;
    }

    public STInfoV2 getStInfo(View view) {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f3086a.d, 200);
        buildSTInfo.scene = STConst.ST_PAGE_APP_DETAIL;
        buildSTInfo.slotId = this.f3086a.c + "_" + "011";
        return buildSTInfo;
    }

    public void onTMAClick(View view) {
        if (this.f3086a.k != null) {
            this.f3086a.k.a(view);
        }
    }
}
