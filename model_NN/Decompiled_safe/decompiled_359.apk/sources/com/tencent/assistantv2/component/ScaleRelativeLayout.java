package com.tencent.assistantv2.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.animation.LinearInterpolator;
import android.widget.RelativeLayout;
import com.tencent.assistant.utils.df;
import com.tencent.assistant.utils.t;

/* compiled from: ProGuard */
public class ScaleRelativeLayout extends RelativeLayout {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public String f3010a = "ScaleRelativeLayout";
    /* access modifiers changed from: private */
    public cq b;
    /* access modifiers changed from: private */
    public cu c;
    /* access modifiers changed from: private */
    public float d;
    /* access modifiers changed from: private */
    public float e;
    /* access modifiers changed from: private */
    public boolean f = false;
    /* access modifiers changed from: private */
    public int g = -1;
    private cr h = new cs(this);

    public ScaleRelativeLayout(Context context) {
        super(context);
        d();
        c();
    }

    public ScaleRelativeLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        d();
        c();
    }

    private void c() {
        int a2 = df.a(getContext(), 40.0f);
        this.d = (float) (t.b - a2);
        this.e = (float) (a2 + df.a(getContext(), 20.0f));
    }

    public void a(long j) {
        if (this.b != null) {
            this.b.setDuration(j);
        }
    }

    private void d() {
        this.b = new cq(0.8f, 1.0f);
        this.b.setFillAfter(true);
        this.b.setDuration(300);
        this.b.setInterpolator(new LinearInterpolator());
    }

    public boolean a() {
        return true;
    }

    public void a(cu cuVar) {
        this.c = cuVar;
    }

    public void setVisibility(int i) {
        this.g = i;
        if (i == -1) {
            i = 0;
        }
        super.setVisibility(i);
        if (this.b != null && this.g != 8) {
            this.b.a(this.h);
            startAnimation(this.b);
        }
    }

    public int b() {
        return this.g;
    }
}
