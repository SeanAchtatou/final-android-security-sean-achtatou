package com.tencent.assistantv2.component;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HeaderViewListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.cy;
import com.tencent.assistant.component.SwitchButton;
import com.tencent.assistant.component.appdetail.CloseViewRunnable;
import com.tencent.assistant.component.invalidater.ListViewScrollListener;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessage;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessageHandler;
import com.tencent.assistant.component.txscrollview.ITXRefreshListViewListener;
import com.tencent.assistant.component.txscrollview.RankRefreshGetMoreListView;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.m;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.callback.b;
import com.tencent.assistant.module.k;
import com.tencent.assistant.net.c;
import com.tencent.assistant.utils.e;
import com.tencent.assistantv2.activity.ay;
import com.tencent.assistantv2.adapter.RankNormalListAdapter;
import com.tencent.assistantv2.manager.i;
import com.tencent.assistantv2.st.business.CostTimeSTManager;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import java.util.List;

/* compiled from: ProGuard */
public class RankNormalListView extends RankRefreshGetMoreListView implements ITXRefreshListViewListener {
    public static final String ST_HIDE_INSTALLED_APPS = "08";
    /* access modifiers changed from: private */
    public static String[] mDefaultTips = null;
    private static final int showHideInstalledAppsAreaNum = 5;
    private static final int totalClickCount = 10;
    private static final int totalCountNum = 20;
    protected final String KEY_DATA = "key_data";
    protected final String KEY_ISFIRSTPAGE = "isFirstPage";
    protected final int MSG_LOAD_LOCAL_APPLIST = 2;
    protected final int MSG_REFRESH_APPLIST = 1;
    protected b enginecallBack = new ch(this);
    public boolean isGameRank = false;
    private long lastUpdateTime = 0;
    protected RankNormalListAdapter mAdapter = null;
    protected k mAppEngine = null;
    boolean[] mClickCount = new boolean[10];
    int mClickCountNum = 0;
    CloseViewRunnable mCloseViewRunnable;
    protected ay mFragment;
    View mHideInstalledAppArea;
    SwitchButton mHideInstalledAppBtn;
    LinearLayout mHideInstalledAppLayout;
    TextView mHideInstalledAppTips;
    protected RankNormalListPage mListPage;
    protected cl mListener;
    /* access modifiers changed from: protected */
    public ViewInvalidateMessageHandler pageMessageHandler = new ci(this);
    protected int retryCount = 1;
    /* access modifiers changed from: protected */
    public ListViewScrollListener scrolllistener;
    protected ImageView topPaddingView;

    public RankNormalListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setSelector(new ColorDrawable(0));
    }

    public void setScrollListener(ListViewScrollListener listViewScrollListener) {
        this.scrolllistener = listViewScrollListener;
        setOnScrollerListener(this.scrolllistener);
    }

    public void registerRefreshListener(cl clVar) {
        this.mListener = clVar;
    }

    public void setAppEngine(k kVar) {
        this.mAppEngine = kVar;
        this.mAppEngine.register(this.enginecallBack);
        setRefreshListViewListener(this);
    }

    public void setListPage(RankNormalListPage rankNormalListPage) {
        this.mListPage = rankNormalListPage;
    }

    public k getmAppEngine() {
        return this.mAppEngine;
    }

    public void loadFirstPage(boolean z) {
        if (this.mAdapter == null) {
            initAdapter();
        }
        if (this.mAdapter.getCount() <= 0 || z) {
            this.mAppEngine.a(z);
            return;
        }
        this.scrolllistener.sendMessage(new ViewInvalidateMessage(2, null, this.pageMessageHandler));
    }

    public void onTXRefreshListViewRefresh(TXScrollViewBase.ScrollState scrollState) {
        if (scrollState == TXScrollViewBase.ScrollState.ScrollState_FromEnd) {
            this.mAppEngine.f();
        } else if (scrollState != TXScrollViewBase.ScrollState.ScrollState_FromStart) {
        } else {
            if (this.mHideInstalledAppArea != null) {
                showHideInstalledAppArea(true, true);
                this.mHeaderLayout.findViewById(R.id.tips).setVisibility(0);
                onTopRefreshCompleteNoAnimation();
                return;
            }
            onTopRefreshComplete();
        }
    }

    public void showHideInstalledAppArea(boolean z, boolean z2) {
        if (this.mHideInstalledAppArea == null) {
            return;
        }
        if (!z) {
            this.mHideInstalledAppArea.setClickable(false);
            if (!z2) {
                this.mHideInstalledAppArea.setVisibility(8);
                this.isHideInstalledAppAreaAdded = false;
                this.mHideInstalledAppTips.setVisibility(8);
                if (this.mCloseViewRunnable != null) {
                    this.mCloseViewRunnable.isRunning = false;
                }
            } else if (this.isHideInstalledAppAreaAdded) {
                this.isHideInstalledAppAreaAdded = false;
                if (this.mCloseViewRunnable == null) {
                    this.mCloseViewRunnable = new CloseViewRunnable(this.mHideInstalledAppArea);
                }
                this.mCloseViewRunnable.isRunning = true;
                this.mHideInstalledAppArea.postDelayed(this.mCloseViewRunnable, 5);
            }
        } else if (!this.isHideInstalledAppAreaAdded) {
            if (this.mCloseViewRunnable != null) {
                this.mCloseViewRunnable.isRunning = false;
            }
            this.mHideInstalledAppArea.setVisibility(0);
            this.mHideInstalledAppArea.setBackgroundResource(R.drawable.v2_button_background_selector);
            this.mHideInstalledAppArea.setClickable(true);
            this.mHideInstalledAppArea.setPressed(false);
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) this.mHideInstalledAppArea.getLayoutParams();
            if (layoutParams.height != getResources().getDimensionPixelSize(R.dimen.hide_installed_apps_area_height)) {
                layoutParams.height = getResources().getDimensionPixelSize(R.dimen.hide_installed_apps_area_height);
                this.mHideInstalledAppArea.setLayoutParams(layoutParams);
            }
            this.isHideInstalledAppAreaAdded = true;
            buildSTInfoAndReport();
        }
    }

    /* access modifiers changed from: package-private */
    public void buildSTInfoAndReport() {
        STInfoV2 sTInfoV2 = new STInfoV2(this.mFragment.J(), ST_HIDE_INSTALLED_APPS, this.mFragment.J(), ST_HIDE_INSTALLED_APPS, 100);
        if (!this.isGameRank || !(this.mFragment instanceof cy)) {
            sTInfoV2.slotId = a.a(ST_HIDE_INSTALLED_APPS, 0);
        } else {
            sTInfoV2.slotId = a.a(((cy) this.mFragment).K(), 0);
        }
        if (!i.a().b().c()) {
            sTInfoV2.status = "01";
        } else {
            sTInfoV2.status = "02";
        }
        com.tencent.assistantv2.st.k.a(sTInfoV2);
    }

    public void onResume() {
        if (this.mHideInstalledAppBtn != null) {
            this.mHideInstalledAppBtn.setSwitchState(i.a().b().c());
        }
        if (this.mAdapter != null && this.mAdapter.getCount() > 0) {
            this.mAdapter.c();
        }
        if (this.mHideInstalledAppBtn != null && this.mAdapter != null && this.mAdapter.getCount() > 0) {
            if (!this.isGameRank) {
                if (m.a().ak()) {
                    showHideInstalledAppArea(true, false);
                    m.a().y(false);
                }
            } else if (m.a().al()) {
                showHideInstalledAppArea(true, false);
                m.a().z(false);
            }
        }
    }

    public void setBaseFragment(ay ayVar) {
        this.mFragment = ayVar;
    }

    public void addHeaderView() {
        if (this.mHideInstalledAppArea == null) {
            this.mHideInstalledAppLayout = (LinearLayout) LayoutInflater.from(getContext()).inflate((int) R.layout.v5_hide_installed_apps_area, (ViewGroup) null);
            ((ListView) this.mScrollContentView).addHeaderView(this.mHideInstalledAppLayout);
            this.mHideInstalledAppArea = this.mHideInstalledAppLayout.findViewById(R.id.header_container);
            this.mHideInstalledAppBtn = (SwitchButton) this.mHideInstalledAppLayout.findViewById(R.id.hide_btn);
            this.mHideInstalledAppBtn.setClickable(false);
            this.mHideInstalledAppTips = (TextView) this.mHideInstalledAppLayout.findViewById(R.id.tips);
            this.mHideInstalledAppBtn.setSwitchState(i.a().b().c());
            if (this.isGameRank) {
                setRankHeaderPaddingBottomAdded(-this.mHideInstalledAppBtn.getResources().getDimensionPixelSize(R.dimen.normal_list_margin_lfet));
            }
            this.mHideInstalledAppArea.setOnClickListener(new cj(this));
        }
    }

    /* access modifiers changed from: private */
    public void onAppListLoadedFinishedHandler(int i, int i2, boolean z, int i3) {
        boolean z2 = false;
        if (i2 == 0) {
            this.mListener.onNetworkNoError();
            if (this.mAdapter == null) {
                initAdapter();
            }
            if (this.mAdapter.getCount() == 0) {
                this.mListener.onErrorHappened(10);
                if (z) {
                    com.tencent.assistantv2.st.k.a(getListPageId(), CostTimeSTManager.TIMETYPE.CANCEL, System.currentTimeMillis());
                    return;
                }
                return;
            }
            this.mAdapter.notifyDataSetChanged();
            this.lastUpdateTime = System.currentTimeMillis();
            if (z) {
                com.tencent.assistantv2.st.k.a(getListPageId(), CostTimeSTManager.TIMETYPE.END, System.currentTimeMillis());
            }
            if (i3 > 0) {
                z2 = this.mAppEngine.h();
            }
            onRefreshComplete(z2, true);
        } else if (z) {
            if (-800 == i2) {
                this.mListener.onErrorHappened(30);
            } else if (this.retryCount > 0) {
                this.retryCount--;
                this.mAppEngine.e();
            } else if (c.a()) {
                this.mListener.onErrorHappened(20);
                return;
            } else {
                this.mListener.onErrorHappened(30);
                return;
            }
            com.tencent.assistantv2.st.k.a(getListPageId(), CostTimeSTManager.TIMETYPE.CANCEL, System.currentTimeMillis());
        } else {
            onRefreshComplete(this.mAppEngine.h(), false);
            this.mListener.onNextPageLoadFailed();
        }
    }

    public ListView getListView() {
        return (ListView) this.mScrollContentView;
    }

    public k getAppEngine() {
        return this.mAppEngine;
    }

    private int getListPageId() {
        if (this.mListPage == null) {
            return 2000;
        }
        return this.mListPage.getPageId();
    }

    /* access modifiers changed from: protected */
    public void initAdapter() {
        ListAdapter adapter = ((ListView) this.mScrollContentView).getAdapter();
        if (adapter instanceof HeaderViewListAdapter) {
            this.mAdapter = (RankNormalListAdapter) ((HeaderViewListAdapter) adapter).getWrappedAdapter();
        } else {
            this.mAdapter = (RankNormalListAdapter) ((ListView) this.mScrollContentView).getAdapter();
        }
        if (this.mAdapter != null && mDefaultTips == null) {
            mDefaultTips = AstApp.i().getResources().getStringArray(R.array.logo_tips3);
        }
    }

    public void clearData() {
        if (this.mAdapter != null) {
            this.mAdapter.a();
        }
    }

    public void recycleData() {
        super.recycleData();
        this.mAppEngine.unregister(this.enginecallBack);
    }

    public void removeTopPadding() {
        ((ListView) this.mScrollContentView).removeHeaderView(this.topPaddingView);
    }

    /* access modifiers changed from: private */
    public boolean meetInstalledCount(List<SimpleAppModel> list) {
        int i;
        if (list != null && list.size() > 0) {
            int i2 = 0;
            int i3 = 0;
            while (i2 < 20) {
                if (i2 < list.size()) {
                    SimpleAppModel simpleAppModel = list.get(i2);
                    if (e.a(simpleAppModel.c, simpleAppModel.g)) {
                        i = i3 + 1;
                        if (i >= 3) {
                            return true;
                        }
                        i2++;
                        i3 = i;
                    }
                }
                i = i3;
                i2++;
                i3 = i;
            }
        }
        return false;
    }

    public RankNormalListAdapter getmAdapter() {
        return this.mAdapter;
    }

    public void setmAdapter(RankNormalListAdapter rankNormalListAdapter) {
        this.mAdapter = rankNormalListAdapter;
    }

    public boolean getNeedRefresh() {
        return this.lastUpdateTime == 0 || System.currentTimeMillis() - this.lastUpdateTime > m.a().ah();
    }
}
