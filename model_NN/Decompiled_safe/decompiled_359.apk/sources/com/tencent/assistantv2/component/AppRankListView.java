package com.tencent.assistantv2.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HeaderViewListAdapter;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.SwitchButton;
import com.tencent.assistant.component.appdetail.CloseViewRunnable;
import com.tencent.assistant.component.invalidater.ListViewScrollListener;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessage;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessageHandler;
import com.tencent.assistant.component.txscrollview.ITXRefreshListViewListener;
import com.tencent.assistant.component.txscrollview.RankRefreshGetMoreListView;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.module.callback.b;
import com.tencent.assistant.module.k;
import com.tencent.assistantv2.adapter.RankNormalListAdapter;
import com.tencent.assistantv2.manager.i;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
public class AppRankListView extends RankRefreshGetMoreListView implements ITXRefreshListViewListener {
    /* access modifiers changed from: private */
    public static String[] s;

    /* renamed from: a  reason: collision with root package name */
    protected k f2978a = null;
    protected RankNormalListAdapter b = null;
    protected g c;
    protected int d = 1;
    protected ListViewScrollListener e;
    LinearLayout f;
    View g;
    SwitchButton h;
    TextView i;
    CloseViewRunnable j;
    boolean[] k = new boolean[10];
    int l = 0;
    protected final int m = 1;
    protected final int n = 2;
    protected final String o = "isFirstPage";
    protected final String p = "key_data";
    protected b q = new c(this);
    protected ViewInvalidateMessageHandler r = new d(this);

    public void a(ListViewScrollListener listViewScrollListener) {
        this.e = listViewScrollListener;
        setOnScrollerListener(this.e);
    }

    public void a(g gVar) {
        this.c = gVar;
    }

    public void a(k kVar) {
        this.f2978a = kVar;
        this.f2978a.register(this.q);
        setRefreshListViewListener(this);
    }

    public AppRankListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a();
    }

    public ListView getListView() {
        return (ListView) this.mScrollContentView;
    }

    /* access modifiers changed from: protected */
    public void a() {
        ListAdapter adapter = ((ListView) this.mScrollContentView).getAdapter();
        if (adapter instanceof HeaderViewListAdapter) {
            this.b = (RankNormalListAdapter) ((HeaderViewListAdapter) adapter).getWrappedAdapter();
        } else {
            this.b = (RankNormalListAdapter) ((ListView) this.mScrollContentView).getAdapter();
        }
        if (this.b != null && s == null) {
            s = AstApp.i().getResources().getStringArray(R.array.logo_tips3);
        }
    }

    public void b() {
        if (this.b == null) {
            a();
        }
        if (this.b.getCount() > 0) {
            this.e.sendMessage(new ViewInvalidateMessage(2, null, this.r));
            return;
        }
        this.f2978a.c();
    }

    public void onTXRefreshListViewRefresh(TXScrollViewBase.ScrollState scrollState) {
        if (scrollState == TXScrollViewBase.ScrollState.ScrollState_FromEnd) {
            this.f2978a.f();
        } else if (scrollState != TXScrollViewBase.ScrollState.ScrollState_FromStart) {
        } else {
            if (this.g != null) {
                a(true, true);
                this.mHeaderLayout.findViewById(R.id.tips).setVisibility(0);
                onTopRefreshCompleteNoAnimation();
                return;
            }
            onTopRefreshComplete();
        }
    }

    public void recycleData() {
        super.recycleData();
        this.f2978a.unregister(this.q);
    }

    public void c() {
        if (this.g == null) {
            this.f = (LinearLayout) LayoutInflater.from(getContext()).inflate((int) R.layout.v5_hide_installed_apps_area, (ViewGroup) null);
            ((ListView) this.mScrollContentView).addHeaderView(this.f);
            this.g = this.f.findViewById(R.id.header_container);
            this.h = (SwitchButton) this.f.findViewById(R.id.hide_btn);
            this.h.setClickable(false);
            this.i = (TextView) this.f.findViewById(R.id.tips);
            this.h.setSwitchState(i.a().b().c());
            setRankHeaderPaddingBottomAdded(-this.h.getResources().getDimensionPixelSize(R.dimen.sliding_drawer_group_icon_down_margin));
            this.g.setOnClickListener(new e(this));
        }
    }

    public void a(boolean z, boolean z2) {
        if (this.g == null) {
            return;
        }
        if (!z) {
            this.g.setClickable(false);
            if (!z2) {
                this.g.setVisibility(8);
                this.isHideInstalledAppAreaAdded = false;
                this.i.setVisibility(8);
                if (this.j != null) {
                    this.j.isRunning = false;
                }
            } else if (this.isHideInstalledAppAreaAdded) {
                this.isHideInstalledAppAreaAdded = false;
                if (this.j == null) {
                    this.j = new CloseViewRunnable(this.g);
                }
                this.j.isRunning = true;
                this.g.postDelayed(this.j, 5);
            }
        } else if (!this.isHideInstalledAppAreaAdded) {
            if (this.j != null) {
                this.j.isRunning = false;
            }
            this.g.setBackgroundResource(R.drawable.v2_button_background_selector);
            this.g.setPressed(false);
            this.g.setVisibility(0);
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) this.g.getLayoutParams();
            if (layoutParams.height != getResources().getDimensionPixelSize(R.dimen.hide_installed_apps_area_height)) {
                layoutParams.height = getResources().getDimensionPixelSize(R.dimen.hide_installed_apps_area_height);
                this.g.setLayoutParams(layoutParams);
            }
            this.g.setClickable(true);
            this.isHideInstalledAppAreaAdded = true;
            d();
        }
    }

    /* access modifiers changed from: private */
    public void a(int i2, int i3, boolean z) {
        if (i3 == 0) {
            this.c.c();
            if (this.b == null) {
                a();
            }
            if (this.b.getCount() == 0) {
                this.c.a(10);
                return;
            }
            this.b.notifyDataSetChanged();
            onRefreshComplete(this.f2978a.h(), true);
        } else if (!z) {
            onRefreshComplete(this.f2978a.h(), false);
            this.c.d();
        } else if (-800 == i3) {
            this.c.a(30);
        } else if (this.d <= 0) {
            this.c.a(20);
        } else {
            this.d--;
            this.f2978a.e();
        }
    }

    /* access modifiers changed from: package-private */
    public void d() {
        STInfoV2 sTInfoV2 = new STInfoV2(((BaseActivity) getContext()).f(), RankNormalListView.ST_HIDE_INSTALLED_APPS, ((BaseActivity) getContext()).f(), RankNormalListView.ST_HIDE_INSTALLED_APPS, 100);
        sTInfoV2.slotId = a.a(RankNormalListView.ST_HIDE_INSTALLED_APPS, 0);
        if (!i.a().b().c()) {
            sTInfoV2.status = "01";
        } else {
            sTInfoV2.status = "02";
        }
        com.tencent.assistantv2.st.k.a(sTInfoV2);
    }
}
