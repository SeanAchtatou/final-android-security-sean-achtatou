package com.tencent.assistantv2.component.appdetail;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.protocol.jce.AppTagInfo;

/* compiled from: ProGuard */
public class DetailTagItemView extends TextView {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Context f3051a;

    public DetailTagItemView(Context context) {
        super(context);
        a(context);
    }

    public DetailTagItemView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    public DetailTagItemView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet);
        a(context);
    }

    private void a(Context context) {
        this.f3051a = context;
        setGravity(17);
    }

    public DetailTagItemView a(AppTagInfo appTagInfo, long j, String str, int i) {
        setText(appTagInfo.b);
        setTextColor(getResources().getColor(R.color.appdetail_tag_text_color_blue));
        setBackgroundResource(R.drawable.v5_appdetail_tag_background_blue);
        setOnClickListener(new t(this, appTagInfo, j, str, i));
        return this;
    }
}
