package com.tencent.assistantv2.component.fps;

import android.widget.ImageView;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.download.a;
import com.tencent.assistantv2.component.y;

/* compiled from: ProGuard */
class b extends y {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ FPSRankNormalItem f3215a;

    b(FPSRankNormalItem fPSRankNormalItem) {
        this.f3215a = fPSRankNormalItem;
    }

    public void a(DownloadInfo downloadInfo) {
        if (downloadInfo != null) {
            a.a().a(downloadInfo);
            com.tencent.assistant.utils.a.a((ImageView) this.f3215a.j.findViewWithTag(downloadInfo.downloadTicket));
        }
    }
}
