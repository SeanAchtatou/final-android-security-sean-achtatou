package com.tencent.assistantv2.component.appdetail;

import android.text.TextUtils;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.u;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class k extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    int f3075a = this.b;
    final /* synthetic */ int b;
    final /* synthetic */ CustomRelateAppViewV5 c;
    private byte d = 0;

    k(CustomRelateAppViewV5 customRelateAppViewV5, int i) {
        this.c = customRelateAppViewV5;
        this.b = i;
    }

    public void onTMAClick(View view) {
        if (((SimpleAppModel) this.c.m.get(this.f3075a)).b <= 0 || TextUtils.isEmpty(((SimpleAppModel) this.c.m.get(this.f3075a)).i)) {
            this.c.l[this.f3075a].a((SimpleAppModel) this.c.m.get(this.f3075a));
            this.c.k[this.f3075a].a();
            this.c.k[this.f3075a].setTag(Integer.valueOf(this.c.r.a((SimpleAppModel) this.c.m.get(this.f3075a), this.d).b));
            return;
        }
        this.c.a((SimpleAppModel) this.c.m.get(this.f3075a), view, this.f3075a);
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.c.f, (SimpleAppModel) this.c.m.get(this.f3075a), this.c.a(this.f3075a), 200, a.a(u.d((SimpleAppModel) this.c.m.get(this.f3075a)), (SimpleAppModel) this.c.m.get(this.f3075a)));
        buildSTInfo.actionId = a.a(u.d((SimpleAppModel) this.c.m.get(this.f3075a)));
        buildSTInfo.recommendId = ((SimpleAppModel) this.c.m.get(this.f3075a)).y;
        return buildSTInfo;
    }
}
