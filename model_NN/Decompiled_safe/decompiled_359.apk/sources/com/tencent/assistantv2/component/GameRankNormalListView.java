package com.tencent.assistantv2.component;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HeaderViewListAdapter;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.cy;
import com.tencent.assistant.component.SwitchButton;
import com.tencent.assistant.component.appdetail.CloseViewRunnable;
import com.tencent.assistant.component.invalidater.ListViewScrollListener;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessage;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessageHandler;
import com.tencent.assistant.component.txscrollview.ITXRefreshListViewListener;
import com.tencent.assistant.component.txscrollview.RankRefreshGetMoreListView;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.m;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.bp;
import com.tencent.assistant.module.callback.b;
import com.tencent.assistant.net.c;
import com.tencent.assistant.utils.e;
import com.tencent.assistantv2.activity.ay;
import com.tencent.assistantv2.adapter.RankNormalListAdapter;
import com.tencent.assistantv2.manager.i;
import com.tencent.assistantv2.st.business.CostTimeSTManager;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import java.util.List;

/* compiled from: ProGuard */
public class GameRankNormalListView extends RankRefreshGetMoreListView implements ITXRefreshListViewListener {
    /* access modifiers changed from: private */
    public static String[] w;

    /* renamed from: a  reason: collision with root package name */
    protected GameRankNormalListPage f2994a;
    protected bp b = null;
    protected RankNormalListAdapter c = null;
    protected ar d;
    protected int e = 1;
    protected ListViewScrollListener f;
    protected ay g;
    LinearLayout h;
    View i;
    SwitchButton j;
    CloseViewRunnable k;
    TextView l;
    boolean[] m = new boolean[10];
    int n = 0;
    public boolean o = false;
    protected final int p = 1;
    protected final int q = 2;
    protected final String r = "isFirstPage";
    protected final String s = "key_data";
    protected b t = new an(this);
    protected ViewInvalidateMessageHandler u = new ao(this);
    private long v = 0;

    public GameRankNormalListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setSelector(new ColorDrawable(0));
    }

    public void a(ListViewScrollListener listViewScrollListener) {
        this.f = listViewScrollListener;
        setOnScrollerListener(this.f);
    }

    public void a(ar arVar) {
        this.d = arVar;
    }

    public void a(bp bpVar) {
        this.b = bpVar;
        this.b.register(this.t);
        setRefreshListViewListener(this);
    }

    public void a(boolean z) {
        if (this.c == null) {
            e();
        }
        if (this.c.getCount() <= 0 || z) {
            this.b.a(z);
            return;
        }
        this.f.sendMessage(new ViewInvalidateMessage(2, null, this.u));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.component.GameRankNormalListView.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.tencent.assistantv2.component.GameRankNormalListView.a(com.tencent.assistantv2.component.GameRankNormalListView, java.util.List):boolean
      com.tencent.assistantv2.component.GameRankNormalListView.a(boolean, boolean):void */
    public void onTXRefreshListViewRefresh(TXScrollViewBase.ScrollState scrollState) {
        if (scrollState == TXScrollViewBase.ScrollState.ScrollState_FromEnd) {
            this.b.f();
        } else if (scrollState != TXScrollViewBase.ScrollState.ScrollState_FromStart) {
        } else {
            if (this.i != null) {
                a(true, true);
                this.mHeaderLayout.findViewById(R.id.tips).setVisibility(0);
                onTopRefreshCompleteNoAnimation();
                return;
            }
            onTopRefreshComplete();
        }
    }

    public void a(boolean z, boolean z2) {
        if (this.i == null) {
            return;
        }
        if (!z) {
            this.i.setClickable(false);
            if (!z2) {
                this.i.setVisibility(8);
                this.isHideInstalledAppAreaAdded = false;
                this.l.setVisibility(8);
                if (this.k != null) {
                    this.k.isRunning = false;
                }
            } else if (this.isHideInstalledAppAreaAdded) {
                this.isHideInstalledAppAreaAdded = false;
                if (this.k == null) {
                    this.k = new CloseViewRunnable(this.i);
                }
                this.k.isRunning = true;
                this.i.postDelayed(this.k, 5);
            }
        } else if (!this.isHideInstalledAppAreaAdded) {
            if (this.k != null) {
                this.k.isRunning = false;
            }
            this.i.setVisibility(0);
            this.i.setBackgroundResource(R.drawable.v2_button_background_selector);
            this.i.setClickable(true);
            this.i.setPressed(false);
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) this.i.getLayoutParams();
            if (layoutParams.height != getResources().getDimensionPixelSize(R.dimen.hide_installed_apps_area_height)) {
                layoutParams.height = getResources().getDimensionPixelSize(R.dimen.hide_installed_apps_area_height);
                this.i.setLayoutParams(layoutParams);
            }
            this.isHideInstalledAppAreaAdded = true;
            a();
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        STInfoV2 sTInfoV2 = new STInfoV2(this.g.J(), RankNormalListView.ST_HIDE_INSTALLED_APPS, this.g.J(), RankNormalListView.ST_HIDE_INSTALLED_APPS, 100);
        if (!this.o || !(this.g instanceof cy)) {
            sTInfoV2.slotId = a.a(RankNormalListView.ST_HIDE_INSTALLED_APPS, 0);
        } else {
            sTInfoV2.slotId = a.a(((cy) this.g).K(), 0);
        }
        if (!i.a().b().c()) {
            sTInfoV2.status = "01";
        } else {
            sTInfoV2.status = "02";
        }
        k.a(sTInfoV2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.component.GameRankNormalListView.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.tencent.assistantv2.component.GameRankNormalListView.a(com.tencent.assistantv2.component.GameRankNormalListView, java.util.List):boolean
      com.tencent.assistantv2.component.GameRankNormalListView.a(boolean, boolean):void */
    public void b() {
        if (this.j != null) {
            this.j.setSwitchState(i.a().b().c());
        }
        if (this.c != null && this.c.getCount() > 0) {
            this.c.c();
        }
        if (this.j != null && this.c != null && this.c.getCount() > 0) {
            if (!this.o) {
                if (m.a().ak()) {
                    a(true, false);
                    m.a().y(false);
                }
            } else if (m.a().al()) {
                a(true, false);
                m.a().z(false);
            }
        }
    }

    public void a(ay ayVar) {
        this.g = ayVar;
    }

    public void c() {
        if (this.i == null) {
            this.h = (LinearLayout) LayoutInflater.from(getContext()).inflate((int) R.layout.v5_hide_installed_apps_area, (ViewGroup) null);
            ((ListView) this.mScrollContentView).addHeaderView(this.h);
            this.i = this.h.findViewById(R.id.header_container);
            this.j = (SwitchButton) this.h.findViewById(R.id.hide_btn);
            this.j.setClickable(false);
            this.l = (TextView) this.h.findViewById(R.id.tips);
            this.j.setSwitchState(i.a().b().c());
            if (this.o) {
                setRankHeaderPaddingBottomAdded(-this.j.getResources().getDimensionPixelSize(R.dimen.normal_list_margin_lfet));
            }
            this.i.setOnClickListener(new ap(this));
        }
    }

    /* access modifiers changed from: private */
    public void a(int i2, int i3, boolean z, int i4) {
        boolean z2 = false;
        if (i3 == 0) {
            this.d.G();
            if (this.c == null) {
                e();
            }
            if (this.c.getCount() == 0) {
                this.d.c(10);
                if (z) {
                    k.a(i(), CostTimeSTManager.TIMETYPE.CANCEL, System.currentTimeMillis());
                    return;
                }
                return;
            }
            this.c.notifyDataSetChanged();
            this.v = System.currentTimeMillis();
            if (z) {
                k.a(i(), CostTimeSTManager.TIMETYPE.END, System.currentTimeMillis());
            }
            if (i4 > 0) {
                z2 = this.b.h();
            }
            onRefreshComplete(z2, true);
        } else if (z) {
            if (-800 == i3) {
                this.d.c(30);
            } else if (this.e > 0) {
                this.e--;
                this.b.e();
            } else if (c.a()) {
                this.d.c(20);
                return;
            } else {
                this.d.c(30);
                return;
            }
            k.a(i(), CostTimeSTManager.TIMETYPE.CANCEL, System.currentTimeMillis());
        } else {
            onRefreshComplete(this.b.h(), false);
            this.d.H();
        }
    }

    public ListView getListView() {
        return (ListView) this.mScrollContentView;
    }

    public bp d() {
        return this.b;
    }

    private int i() {
        if (this.f2994a == null) {
            return 2000;
        }
        return this.f2994a.c();
    }

    /* access modifiers changed from: protected */
    public void e() {
        ListAdapter adapter = ((ListView) this.mScrollContentView).getAdapter();
        if (adapter instanceof HeaderViewListAdapter) {
            this.c = (RankNormalListAdapter) ((HeaderViewListAdapter) adapter).getWrappedAdapter();
        } else {
            this.c = (RankNormalListAdapter) ((ListView) this.mScrollContentView).getAdapter();
        }
        if (this.c != null && w == null) {
            w = AstApp.i().getResources().getStringArray(R.array.logo_tips3);
        }
    }

    public void recycleData() {
        super.recycleData();
        this.b.unregister(this.t);
    }

    /* access modifiers changed from: private */
    public boolean a(List<SimpleAppModel> list) {
        int i2;
        if (list != null && list.size() > 0) {
            int i3 = 0;
            int i4 = 0;
            while (i3 < 20) {
                if (i3 < list.size()) {
                    SimpleAppModel simpleAppModel = list.get(i3);
                    if (e.a(simpleAppModel.c, simpleAppModel.g)) {
                        i2 = i4 + 1;
                        if (i2 >= 3) {
                            return true;
                        }
                        i3++;
                        i4 = i2;
                    }
                }
                i2 = i4;
                i3++;
                i4 = i2;
            }
        }
        return false;
    }

    public RankNormalListAdapter f() {
        return this.c;
    }

    public boolean g() {
        return this.v == 0 || System.currentTimeMillis() - this.v > m.a().ah();
    }
}
