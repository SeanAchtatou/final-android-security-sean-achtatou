package com.tencent.assistantv2.component;

import com.qq.AppService.AstApp;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.link.b;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.mediadownload.m;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class ai extends AppConst.TwoBtnDialogInfo {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ m f3035a;
    final /* synthetic */ FileDownloadButton b;

    ai(FileDownloadButton fileDownloadButton, m mVar) {
        this.b = fileDownloadButton;
        this.f3035a = mVar;
    }

    public void onLeftBtnClick() {
        this.b.a(STConst.ST_PAGE_DOWNLOAD_FILE_CANT_OPEN, AstApp.m() != null ? AstApp.m().f() : 2000, a.a("03", 1), 200);
    }

    public void onRightBtnClick() {
        b.a(this.b.getContext(), "tmast://search?selflink=true&key=" + this.f3035a.b);
        this.b.a(STConst.ST_PAGE_DOWNLOAD_FILE_CANT_OPEN, AstApp.m() != null ? AstApp.m().f() : 2000, a.a("03", 0), 200);
    }

    public void onCancell() {
    }
}
