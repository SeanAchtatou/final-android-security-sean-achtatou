package com.tencent.assistantv2.component;

import android.animation.Animator;

/* renamed from: com.tencent.assistantv2.component.do  reason: invalid class name */
/* compiled from: ProGuard */
class Cdo implements Animator.AnimatorListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ dw f3180a;
    final /* synthetic */ TxManagerCommContainView b;

    Cdo(TxManagerCommContainView txManagerCommContainView, dw dwVar) {
        this.b = txManagerCommContainView;
        this.f3180a = dwVar;
    }

    public void onAnimationStart(Animator animator) {
        if (this.f3180a != null) {
            this.f3180a.a();
        }
        this.b.h();
    }

    public void onAnimationRepeat(Animator animator) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.component.TxManagerCommContainView.a(com.tencent.assistantv2.component.TxManagerCommContainView, boolean):boolean
     arg types: [com.tencent.assistantv2.component.TxManagerCommContainView, int]
     candidates:
      com.tencent.assistantv2.component.TxManagerCommContainView.a(java.lang.String, int):android.text.SpannableString
      com.tencent.assistantv2.component.TxManagerCommContainView.a(com.tencent.assistantv2.component.TxManagerCommContainView, float):void
      com.tencent.assistantv2.component.TxManagerCommContainView.a(java.lang.String, java.lang.String):void
      com.tencent.assistantv2.component.TxManagerCommContainView.a(android.app.Activity, com.tencent.assistant.model.PluginStartEntry):void
      com.tencent.assistantv2.component.TxManagerCommContainView.a(android.view.View, android.widget.RelativeLayout$LayoutParams):void
      com.tencent.assistantv2.component.TxManagerCommContainView.a(java.lang.String, android.text.SpannableString):void
      com.tencent.assistantv2.component.TxManagerCommContainView.a(com.tencent.assistantv2.component.TxManagerCommContainView, boolean):boolean */
    public void onAnimationEnd(Animator animator) {
        boolean unused = this.b.z = true;
        if (this.b.z) {
            this.b.g();
        }
        if (this.f3180a != null) {
            this.f3180a.b();
        }
    }

    public void onAnimationCancel(Animator animator) {
    }
}
