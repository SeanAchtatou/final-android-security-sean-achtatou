package com.tencent.assistantv2.component;

import android.content.Context;
import android.os.Message;
import android.util.AttributeSet;
import android.widget.Button;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.manager.BookingManager;
import com.tencent.assistant.model.a.c;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
public class BookingButton extends Button implements UIEventListener {

    /* renamed from: a  reason: collision with root package name */
    private Context f2981a;
    /* access modifiers changed from: private */
    public c b;
    private long c;

    public BookingButton(Context context) {
        this(context, null);
    }

    public BookingButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.c = 0;
        this.f2981a = context;
        a();
    }

    private void a() {
        setHeight(this.f2981a.getResources().getDimensionPixelSize(R.dimen.download_button_height));
        setMinWidth(this.f2981a.getResources().getDimensionPixelSize(R.dimen.download_button_minwidth_2));
        setGravity(17);
        setTextSize(0, (float) this.f2981a.getResources().getDimensionPixelSize(R.dimen.download_button_font_size));
        setSingleLine(true);
        setBackgroundResource(R.drawable.state_bg_install_selector);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_BOOKING_STATUS_CHANGED, this);
    }

    public void a(c cVar, STInfoV2 sTInfoV2) {
        this.b = cVar;
        if (this.b.f1637a != null) {
            this.c = this.b.f1637a.f1634a;
            BookingManager.a().a(this.c, this.b.g);
        }
        a(b());
        setOnClickListener(new n(this, sTInfoV2));
    }

    /* access modifiers changed from: private */
    public BookingManager.BOOKINGSTATUS b() {
        BookingManager.BOOKINGSTATUS b2 = BookingManager.a().b(this.b.f1637a.f1634a);
        if (b2 != BookingManager.BOOKINGSTATUS.UNBOOKED) {
            return b2;
        }
        try {
            return BookingManager.BOOKINGSTATUS.values()[this.b.h];
        } catch (Exception e) {
            e.printStackTrace();
            return b2;
        }
    }

    private void a(BookingManager.BOOKINGSTATUS bookingstatus) {
        if (bookingstatus == BookingManager.BOOKINGSTATUS.BOOKED) {
            setText(this.f2981a.getString(R.string.bookingbtn_booked));
        } else {
            setText(this.f2981a.getString(R.string.bookingbtn_gobooking));
        }
    }

    public void handleUIEvent(Message message) {
        long j;
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_BOOKING_STATUS_CHANGED:
                if (message.obj instanceof Long) {
                    j = ((Long) message.obj).longValue();
                } else {
                    j = -1;
                }
                long a2 = BookingManager.a().a(this.c);
                if (a2 != -1 && a2 == j) {
                    a(BookingManager.a().b(this.c));
                    return;
                }
                return;
            default:
                return;
        }
    }
}
