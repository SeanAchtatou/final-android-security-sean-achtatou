package com.tencent.assistantv2.component;

import com.tencent.assistant.AppConst;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessage;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessageHandler;
import com.tencent.assistant.m;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.business.CostTimeSTManager;
import com.tencent.assistantv2.st.k;
import java.util.List;
import java.util.Map;

/* compiled from: ProGuard */
class d extends ViewInvalidateMessageHandler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppRankListView f3169a;

    d(AppRankListView appRankListView) {
        this.f3169a = appRankListView;
    }

    public void handleMessage(ViewInvalidateMessage viewInvalidateMessage) {
        int i;
        if (viewInvalidateMessage.what == 1) {
            int i2 = viewInvalidateMessage.arg1;
            int i3 = viewInvalidateMessage.arg2;
            Map map = (Map) viewInvalidateMessage.params;
            boolean booleanValue = ((Boolean) map.get("isFirstPage")).booleanValue();
            Object obj = map.get("key_data");
            if (!(obj == null || this.f3169a.b == null)) {
                this.f3169a.b.a(booleanValue, (List) obj);
                if (booleanValue) {
                    k.a((int) STConst.ST_PAGE_RANK_CLASSIC, CostTimeSTManager.TIMETYPE.END, System.currentTimeMillis());
                }
            }
            this.f3169a.a(i3, i2, booleanValue);
            if (booleanValue) {
                int i4 = 0;
                int i5 = 0;
                for (SimpleAppModel simpleAppModel : (List) obj) {
                    i4++;
                    if (i4 > 20) {
                        break;
                    }
                    if (simpleAppModel.t() == AppConst.AppState.INSTALLED) {
                        i = i5 + 1;
                    } else {
                        i = i5;
                    }
                    i5 = i;
                }
                if ((i5 < 5 || !m.a().ai()) && !m.a().al()) {
                    this.f3169a.isHideInstalledAppAreaAdded = false;
                    this.f3169a.g.setVisibility(8);
                } else {
                    m.a().w(false);
                    this.f3169a.isHideInstalledAppAreaAdded = true;
                    this.f3169a.d();
                    m.a().z(false);
                }
                this.f3169a.b.d();
                return;
            }
            return;
        }
        this.f3169a.c.c();
        if (this.f3169a.b != null) {
            this.f3169a.b.notifyDataSetChanged();
        }
    }
}
