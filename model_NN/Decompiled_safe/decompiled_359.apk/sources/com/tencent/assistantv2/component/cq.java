package com.tencent.assistantv2.component;

import android.view.animation.Animation;
import android.view.animation.Transformation;

/* compiled from: ProGuard */
public class cq extends Animation {

    /* renamed from: a  reason: collision with root package name */
    private cr f3161a;
    private float b;
    private float c;

    public cq(float f, float f2) {
        this.b = f;
        this.c = f2;
    }

    public void a(cr crVar) {
        this.f3161a = crVar;
    }

    /* access modifiers changed from: protected */
    public void applyTransformation(float f, Transformation transformation) {
        if (this.f3161a != null) {
            this.f3161a.a(f, transformation, this.b, this.c);
        }
        super.applyTransformation(f, transformation);
    }
}
