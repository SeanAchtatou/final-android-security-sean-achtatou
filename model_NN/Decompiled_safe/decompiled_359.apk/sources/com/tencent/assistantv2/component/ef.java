package com.tencent.assistantv2.component;

import android.text.TextUtils;
import com.tencent.smtt.sdk.WebView;

/* compiled from: ProGuard */
class ef implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f3194a;
    final /* synthetic */ WebView b;
    final /* synthetic */ ed c;

    ef(ed edVar, String str, WebView webView) {
        this.c = edVar;
        this.f3194a = str;
        this.b = webView;
    }

    public void run() {
        if (!TextUtils.isEmpty(this.f3194a)) {
            try {
                if (this.b != null) {
                    this.b.loadUrl(this.f3194a);
                }
            } catch (Exception e) {
            }
        }
    }
}
