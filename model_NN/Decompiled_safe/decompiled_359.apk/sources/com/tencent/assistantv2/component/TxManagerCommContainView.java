package com.tencent.assistantv2.component;

import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewStub;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.Global;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.activity.PluginDetailActivity;
import com.tencent.assistant.model.PluginStartEntry;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.plugin.PluginInfo;
import com.tencent.assistant.plugin.mgr.c;
import com.tencent.assistant.plugin.mgr.d;
import com.tencent.assistant.plugin.proxy.PluginProxyActivity;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ba;
import com.tencent.assistant.utils.df;
import com.tencent.assistant.utils.t;
import com.tencent.beacon.event.a;
import java.util.HashMap;
import java.util.Random;

/* compiled from: ProGuard */
public class TxManagerCommContainView extends RelativeLayout {
    /* access modifiers changed from: private */
    public static float k = 0.7f;
    /* access modifiers changed from: private */
    public static float l = 0.7f;
    private volatile boolean A = false;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Context f3019a;
    private ViewStub b = null;
    private RelativeLayout c = null;
    private TextView d = null;
    private ImageView e = null;
    private Button f = null;
    private RelativeLayout g = null;
    /* access modifiers changed from: private */
    public TxTopContainerView h;
    /* access modifiers changed from: private */
    public TxBottomContainerView i;
    /* access modifiers changed from: private */
    public RelativeLayout j;
    private ViewStub m = null;
    private RelativeLayout n = null;
    private TextView o = null;
    private TextView p = null;
    private TextView q = null;
    /* access modifiers changed from: private */
    public ImageView r = null;
    /* access modifiers changed from: private */
    public ImageView s = null;
    private Random t = new Random(System.currentTimeMillis());
    /* access modifiers changed from: private */
    public Activity u = null;
    /* access modifiers changed from: private */
    public Class<?> v = null;
    /* access modifiers changed from: private */
    public PluginStartEntry w = null;
    /* access modifiers changed from: private */
    public View x;
    /* access modifiers changed from: private */
    public boolean y = true;
    /* access modifiers changed from: private */
    public volatile boolean z = false;

    public TxManagerCommContainView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f3019a = context;
        LayoutInflater.from(context).inflate((int) R.layout.manager_common_container_layout, this);
        f();
    }

    private void e() {
        k = (1.0f - (((float) df.a(getContext(), 144.0f)) / ((float) getMeasuredHeight()))) - 0.05f;
        XLog.i("TxManagerCommContainView", "before---BOTTOM_HEIGHT_PERCENT_UP = " + k);
        if (k > 0.7f) {
            k = 0.7f;
        }
    }

    private void f() {
        this.h = (TxTopContainerView) findViewById(R.id.top_container);
        this.i = (TxBottomContainerView) findViewById(R.id.bottom_container);
        this.j = (RelativeLayout) findViewById(R.id.footer_container);
        this.j.setVisibility(8);
        this.m = (ViewStub) findViewById(R.id.clear_success_viewStub);
    }

    public void a(View view) {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(df.a(getContext(), 144.0f), df.a(getContext(), 144.0f));
        layoutParams.topMargin = df.a(getContext(), 140.0f);
        layoutParams.addRule(14);
        this.x = view;
        this.h.addView(view, layoutParams);
    }

    public void b(View view) {
        this.i.addView(view, new RelativeLayout.LayoutParams(-1, -1));
    }

    public void a(View view, RelativeLayout.LayoutParams layoutParams) {
        this.j.addView(view, layoutParams);
    }

    public void a() {
        b();
        if (Build.VERSION.SDK_INT >= 14) {
            this.x.setScaleX(1.0f);
            this.x.setScaleY(1.0f);
        }
    }

    public void b() {
        if (this.h.getChildCount() == 2) {
            this.h.removeViewAt(1);
        }
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.h.getLayoutParams();
        layoutParams.height = -1;
        RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) this.i.getLayoutParams();
        layoutParams2.height = 0;
        this.h.setLayoutParams(layoutParams);
        this.i.setLayoutParams(layoutParams2);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(df.a(getContext(), 144.0f), df.a(getContext(), 144.0f));
        layoutParams3.topMargin = df.a(getContext(), 140.0f);
        layoutParams3.addRule(14);
        this.x.setLayoutParams(layoutParams3);
        this.x.clearAnimation();
        this.i.setVisibility(8);
        this.j.setVisibility(8);
        int childCount = this.j.getChildCount();
        if (childCount != 0) {
            for (int i2 = 0; i2 < childCount; i2++) {
                this.j.getChildAt(i2).setVisibility(8);
            }
        }
        if (this.n != null) {
            this.n.setVisibility(4);
        }
        setBackgroundColor(Color.argb(255, 242, 242, 242));
    }

    private void b(dw dwVar) {
        du duVar = new du(this, 1.0f, l, 1.0f, l, 1, 0.5f, 1, 0.5f);
        duVar.setDuration(500);
        duVar.setInterpolator(new AccelerateInterpolator());
        duVar.setFillEnabled(true);
        duVar.setFillAfter(true);
        duVar.setFillBefore(false);
        duVar.setAnimationListener(new dm(this, dwVar));
        this.x.startAnimation(duVar);
        h();
    }

    /* access modifiers changed from: private */
    public void a(float f2) {
        int i2 = (int) (242.0f + (13.0f * f2));
        setBackgroundColor(Color.argb(255, i2, i2, i2));
    }

    public void a(dw dwVar) {
        e();
        this.z = false;
        this.A = false;
        this.i.setVisibility(0);
        this.y = true;
        if (Build.VERSION.SDK_INT < 14) {
            b(dwVar);
            return;
        }
        AnimatorSet animatorSet = new AnimatorSet();
        ValueAnimator ofFloat = ValueAnimator.ofFloat(0.0f, k);
        ValueAnimator.setFrameDelay(10);
        ofFloat.addUpdateListener(new dn(this));
        animatorSet.play(ofFloat);
        animatorSet.setDuration(500L);
        animatorSet.addListener(new Cdo(this, dwVar));
        animatorSet.setInterpolator(new AccelerateInterpolator());
        animatorSet.start();
    }

    /* access modifiers changed from: private */
    public void g() {
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.x.getLayoutParams();
        if (layoutParams.getRules()[13] != -1) {
            XLog.d("TxManagerCommContainView", "manualSetTopContainViewState --- ");
            layoutParams.addRule(13);
            layoutParams.width = df.a(getContext(), 144.0f);
            layoutParams.height = df.a(getContext(), 144.0f);
            layoutParams.topMargin = 0;
            layoutParams.bottomMargin = 0;
            this.x.setLayoutParams(layoutParams);
            if (Build.VERSION.SDK_INT >= 14) {
                this.x.setScaleX(l);
                this.x.setScaleY(l);
            }
        }
        setBackgroundColor(Color.argb(255, 255, 255, 255));
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, df.a(this.f3019a, 1.0f));
        layoutParams2.addRule(12);
        View view = new View(this.f3019a);
        view.setBackgroundColor(Color.argb(255, 224, 224, 224));
        this.h.addView(view, layoutParams2);
    }

    /* access modifiers changed from: private */
    public void h() {
        TranslateAnimation translateAnimation = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 1.0f, 1, 0.0f);
        translateAnimation.setDuration(300);
        translateAnimation.setFillAfter(false);
        translateAnimation.setFillEnabled(true);
        translateAnimation.setFillBefore(true);
        translateAnimation.setAnimationListener(new dp(this));
        this.j.setAnimation(translateAnimation);
    }

    public void a(String str, SpannableString spannableString) {
        a(str, spannableString, (SpannableString) null);
    }

    public void a(String str, SpannableString spannableString, SpannableString spannableString2) {
        if (this.n == null) {
            this.m.inflate();
            this.n = (RelativeLayout) findViewById(R.id.clear_success_content);
            this.o = (TextView) findViewById(R.id.txt_finish);
            this.p = (TextView) findViewById(R.id.txt_finish_tips);
            this.q = (TextView) findViewById(R.id.txt_finish_guide);
            this.r = (ImageView) findViewById(R.id.success_green_circle);
            this.s = (ImageView) findViewById(R.id.success_duigou);
            this.b = (ViewStub) findViewById(R.id.inter_manage_interface);
        }
        this.n.setVisibility(0);
        this.j.setVisibility(8);
        this.h.setVisibility(4);
        this.i.setVisibility(4);
        this.o.setText(str);
        this.p.setText(spannableString);
        if (!TextUtils.isEmpty(spannableString2)) {
            this.q.setText(spannableString2);
            this.q.setHighlightColor(this.f3019a.getResources().getColor(R.color.share_yyb_bg_color_pressed));
            this.q.setMovementMethod(LinkMovementMethod.getInstance());
            this.q.setVisibility(0);
        } else {
            this.q.setVisibility(8);
        }
        ScaleAnimation scaleAnimation = new ScaleAnimation(0.5f, 1.0f, 0.5f, 1.0f, 1, 0.5f, 1, 0.5f);
        scaleAnimation.setDuration(300);
        scaleAnimation.setInterpolator(new OvershootInterpolator(1.2f));
        ScaleAnimation scaleAnimation2 = new ScaleAnimation(0.5f, 1.0f, 0.5f, 1.0f, 1, 0.5f, 1, 0.5f);
        scaleAnimation2.setDuration(300);
        scaleAnimation2.setInterpolator(new OvershootInterpolator(1.2f));
        scaleAnimation.setAnimationListener(new dq(this));
        scaleAnimation2.setAnimationListener(new dr(this));
        this.r.startAnimation(scaleAnimation);
        ba.a().postDelayed(new ds(this, scaleAnimation2), 100);
    }

    public void a(int i2, String str, int i3, int i4, int i5) {
        if (this.b != null && this.c == null) {
            this.b.inflate();
            this.e = (ImageView) findViewById(R.id.icon_xiaobao);
            this.e.setImageResource(i2);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
            layoutParams.setMargins(df.a(this.f3019a, (float) i3), 0, df.a(this.f3019a, (float) i4), df.a(this.f3019a, 23.0f));
            layoutParams.addRule(12);
            this.c = (RelativeLayout) findViewById(R.id.inter_manage_layout);
            this.c.setLayoutParams(layoutParams);
            this.d = (TextView) findViewById(R.id.title_txt);
        }
        this.d.setText(a(str, i5));
        this.d.setHighlightColor(this.f3019a.getResources().getColor(R.color.share_yyb_bg_color_pressed));
        this.d.setMovementMethod(LinkMovementMethod.getInstance());
        this.c.setVisibility(0);
    }

    public void a(Activity activity, Class<?> cls, PluginStartEntry pluginStartEntry) {
        this.u = activity;
        this.v = cls;
        this.w = pluginStartEntry;
    }

    public void a(Activity activity, PluginStartEntry pluginStartEntry) {
        this.u = activity;
        this.w = pluginStartEntry;
    }

    /* access modifiers changed from: private */
    public void i() {
        if (this.v != null) {
            Intent intent = new Intent(this.f3019a, this.v);
            if (this.w != null) {
                intent.putExtra("dock_plugin", this.w);
            }
            if (this.u != null) {
                this.u.finish();
            }
            this.f3019a.startActivity(intent);
        }
    }

    /* access modifiers changed from: private */
    public void j() {
        if (this.w != null) {
            int f2 = ((BaseActivity) this.f3019a).f();
            if (this.w.a().equals(AstApp.i().getPackageName()) || this.w.a().equals("com.tencent.android.qqdownloader")) {
                try {
                    Intent intent = new Intent();
                    intent.setClassName(this.f3019a, this.w.c());
                    intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, f2);
                    this.f3019a.startActivity(intent);
                    if (this.u != null) {
                        this.u.finish();
                    }
                } catch (Throwable th) {
                    th.printStackTrace();
                    if (this.t.nextInt(100) < 10) {
                        throw new RuntimeException(th);
                    }
                }
            } else {
                PluginInfo a2 = d.b().a(this.w.a());
                if (a2 != null && (a2.getVersion() >= this.w.b() || c.a(this.w.a()) >= 0)) {
                    PluginInfo.PluginEntry pluginEntryByStartActivity = a2.getPluginEntryByStartActivity(this.w.c());
                    if (pluginEntryByStartActivity != null) {
                        try {
                            PluginProxyActivity.a(AstApp.i(), pluginEntryByStartActivity.getHostPlugInfo().getPackageName(), pluginEntryByStartActivity.getHostPlugInfo().getVersion(), pluginEntryByStartActivity.getStartActivity(), pluginEntryByStartActivity.getHostPlugInfo().getInProcess(), null, pluginEntryByStartActivity.getHostPlugInfo().getLaunchApplication());
                            if (this.u != null) {
                                this.u.finish();
                            }
                        } catch (Throwable th2) {
                            th2.printStackTrace();
                        }
                    } else {
                        Toast.makeText(this.f3019a, (int) R.string.plugin_entry_not_exist, 0).show();
                    }
                } else if (this.w.d() < 0) {
                    Toast.makeText(this.f3019a, (int) R.string.plugin_entry_not_exist, 0).show();
                } else {
                    try {
                        Intent intent2 = new Intent(this.f3019a, PluginDetailActivity.class);
                        intent2.putExtra("plugin_start_entry", this.w);
                        intent2.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, f2);
                        this.f3019a.startActivity(intent2);
                        if (this.u != null) {
                            this.u.finish();
                        }
                    } catch (Throwable th3) {
                        th3.printStackTrace();
                        if (this.t.nextInt(100) < 10) {
                            throw new RuntimeException(th3);
                        }
                    }
                }
            }
        }
    }

    private SpannableString a(String str, int i2) {
        dt dtVar = new dt(this);
        SpannableString spannableString = new SpannableString(str.substring(0, str.length() - i2) + "$");
        TextView textView = new TextView(this.f3019a);
        textView.setTextColor(getResources().getColor(R.color.blue_color));
        textView.setText(str.subSequence(str.length() - i2, str.length()));
        spannableString.setSpan(new eu(this.f3019a, textView), spannableString.length() - 1, spannableString.length(), 33);
        spannableString.setSpan(new dv(this, dtVar), spannableString.length() - 1, spannableString.length(), 33);
        return spannableString;
    }

    /* access modifiers changed from: private */
    public void a(String str, String str2) {
        HashMap hashMap = new HashMap();
        hashMap.put("B1", Global.getPhoneGuidAndGen());
        hashMap.put("B2", Global.getQUAForBeacon());
        hashMap.put("B3", t.g());
        hashMap.put("B4", str);
        hashMap.put("B5", str2);
        XLog.d("beacon", "beacon report >> featureLinkClick. " + hashMap.toString());
        a.a("featureLinkClick", true, -1, -1, hashMap, true);
    }
}
