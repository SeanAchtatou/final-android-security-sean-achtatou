package com.tencent.assistantv2.component.banner;

import android.content.Context;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class m implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Context f3111a;
    final /* synthetic */ j b;

    m(j jVar, Context context) {
        this.b = jVar;
        this.f3111a = context;
    }

    public void run() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f3111a, 100);
        if (buildSTInfo != null) {
            buildSTInfo.scene = STConst.ST_PAGE_GAME_FULI;
            buildSTInfo.slotId = "06_008";
            k.a(buildSTInfo);
        }
    }
}
