package com.tencent.assistantv2.st.business;

import com.tencent.assistant.AppConst;
import com.tencent.assistant.login.d;
import com.tencent.assistant.protocol.jce.Reporter;
import com.tencent.assistant.st.STListener;
import com.tencent.assistant.st.g;
import com.tencent.assistantv2.st.a;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
public abstract class BaseSTManagerV2 implements STListener {
    protected a b = null;

    public void setILogger(a aVar) {
        this.b = aVar;
    }

    public g getSTStruct() {
        return null;
    }

    public void report(STInfoV2 sTInfoV2) {
    }

    public void reportRealTime(STInfoV2 sTInfoV2) {
    }

    /* access modifiers changed from: protected */
    public Reporter f() {
        Reporter reporter = new Reporter();
        AppConst.IdentityType d = d.a().d();
        if (d == AppConst.IdentityType.MOBILEQ) {
            reporter.b = String.valueOf(d.a().p());
        } else if (d == AppConst.IdentityType.WX) {
            reporter.b = d.a().s();
        }
        reporter.f2283a = d.ordinal();
        return reporter;
    }
}
