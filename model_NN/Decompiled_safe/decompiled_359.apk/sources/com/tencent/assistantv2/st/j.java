package com.tencent.assistantv2.st;

import com.tencent.assistant.db.table.x;
import com.tencent.assistant.db.table.y;
import com.tencent.assistant.protocol.jce.StatReportItem;
import com.tencent.assistant.st.g;
import com.tencent.assistant.st.h;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistantv2.st.business.o;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class j {

    /* renamed from: a  reason: collision with root package name */
    private a f3354a = null;

    public void a(a aVar) {
        this.f3354a = aVar;
    }

    public List<Long> a(ArrayList<StatReportItem> arrayList) {
        g a2;
        XLog.d("logReport", "**** loadLogFromCache");
        ArrayList arrayList2 = new ArrayList();
        if (arrayList == null) {
            return arrayList2;
        }
        List<Integer> b = x.a().b();
        if (b != null) {
            for (Integer next : b) {
                if (!(next == null || (a2 = a(next.byteValue())) == null)) {
                    arrayList.add(a2.b);
                    arrayList2.addAll(a2.f2537a);
                }
            }
        }
        return arrayList2;
    }

    public ArrayList<StatReportItem> a() {
        XLog.d("reportSTInstall", "**** loadInstallLogFromCache");
        return o.a().b();
    }

    public g a(byte b) {
        y a2;
        byte[] a3;
        int b2 = i.b(b);
        if (!h.a(i.a(b)) || b2 < 0 || (a2 = x.a().a(b, b2)) == null || a2.b == null || a2.b.size() == 0 || (a3 = h.a(a2.b)) == null) {
            return null;
        }
        StatReportItem statReportItem = new StatReportItem();
        statReportItem.f2374a = b;
        statReportItem.c = a3;
        statReportItem.b = i.a();
        if (this.f3354a != null) {
        }
        g gVar = new g();
        gVar.b = statReportItem;
        gVar.f2537a = a2.f1244a;
        return gVar;
    }

    public void a(List<Long> list) {
        x.a().a(list);
    }
}
