package com.tencent.assistant.component;

import android.os.Handler;
import android.os.Message;
import com.tencent.assistant.module.u;

/* compiled from: ProGuard */
class dw extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ UpdateIgnoreListView f1031a;

    dw(UpdateIgnoreListView updateIgnoreListView) {
        this.f1031a = updateIgnoreListView;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.adapter.AppUpdateIgnoreListAdapter.a(java.util.List<com.tencent.assistant.model.SimpleAppModel>, boolean):void
     arg types: [java.util.List<com.tencent.assistant.model.SimpleAppModel>, int]
     candidates:
      com.tencent.assistant.adapter.AppUpdateIgnoreListAdapter.a(com.tencent.assistant.model.SimpleAppModel, int):com.tencent.assistantv2.st.page.STInfoV2
      com.tencent.assistant.adapter.AppUpdateIgnoreListAdapter.a(com.tencent.assistant.adapter.AppUpdateIgnoreListAdapter, java.lang.String):java.lang.String
      com.tencent.assistant.adapter.AppUpdateIgnoreListAdapter.a(com.tencent.assistant.adapter.AppUpdateIgnoreListAdapter, com.tencent.assistant.model.SimpleAppModel):void
      com.tencent.assistant.adapter.AppUpdateIgnoreListAdapter.a(com.tencent.assistant.adapter.x, int):void
      com.tencent.assistant.adapter.AppUpdateIgnoreListAdapter.a(java.util.List<com.tencent.assistant.model.SimpleAppModel>, boolean):void */
    public void handleMessage(Message message) {
        switch (message.what) {
            case 1:
                this.f1031a.mUpdateIgnoreListAdapter.a(u.f(), true);
                return;
            case 2:
                this.f1031a.mUpdateIgnoreListAdapter.a(u.f(), false);
                return;
            default:
                return;
        }
    }
}
