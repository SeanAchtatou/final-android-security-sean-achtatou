package com.tencent.assistant.component.listener;

import android.view.View;
import android.widget.AdapterView;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
public abstract class OnTMAItemExClickListener implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    private int f1073a = 0;

    public abstract void onTMAItemClick(AdapterView<?> adapterView, View view, int i, long j);

    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        this.f1073a = i;
        onTMAItemClick(adapterView, view, i, j);
        a(view);
    }

    /* access modifiers changed from: protected */
    public void a(View view) {
        k.a(getStInfo(view));
    }

    public int getPosition() {
        return this.f1073a;
    }

    public STInfoV2 getStInfo(View view) {
        return null;
    }
}
