package com.tencent.assistant.component;

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.manager.as;
import com.tencent.assistant.protocol.jce.UserTaskCfg;

/* compiled from: ProGuard */
public class UserTaskView {
    private static final String LOG_TAG = "UserTaskView";

    public static Boolean canOpenUserTaskView(UserTaskCfg userTaskCfg) {
        if (userTaskCfg == null) {
            return false;
        }
        if (userTaskCfg.f2418a != 1) {
            return false;
        }
        if (userTaskCfg.e <= 0 || userTaskCfg.f < 0) {
            return false;
        }
        UserTaskCfg o = as.w().o();
        if (o == null) {
            new UserTaskCfg();
            UserTaskCfg userTaskCfg2 = (UserTaskCfg) userTaskCfg.clone();
            userTaskCfg2.f2418a = 1;
            userTaskCfg2.b = 0;
            userTaskCfg2.e = userTaskCfg.e;
            userTaskCfg2.f = userTaskCfg.e;
            userTaskCfg2.k = 0;
            as.w().b(userTaskCfg2);
            return true;
        } else if (userTaskCfg.e == o.e) {
            if (o.f2418a == 1) {
                return true;
            }
            return false;
        } else if (o.f - userTaskCfg.e >= 7) {
            o.b = 0;
            o.f = userTaskCfg.e;
            o.k = 0;
            as.w().b(o);
            return true;
        } else if (o.b == 0) {
            return true;
        } else {
            if (o.k < userTaskCfg.k) {
                return true;
            }
            return false;
        }
    }

    public static View initUserTaskView(Context context, int i) {
        new View(context);
        UserTaskCfg n = as.w().n();
        if (n == null) {
            return null;
        }
        switch (i) {
            case 1:
                View inflate = ((Activity) context).getLayoutInflater().inflate((int) R.layout.downloadlist_header_layout1, (ViewGroup) null);
                TextView textView = (TextView) inflate.findViewById(R.id.mainTitle);
                TextView textView2 = (TextView) inflate.findViewById(R.id.leftTime);
                TextView textView3 = (TextView) inflate.findViewById(R.id.taskDuration);
                if (!TextUtils.isEmpty(n.c)) {
                    textView.setText(n.c);
                }
                if (n.f > 0) {
                    textView2.setText(Html.fromHtml("<html >您剩余 <font color='#5AC65C'>" + n.f + "</font> 次机会</html>"));
                } else {
                    textView2.setText("已获取所有机会");
                    ((TextView) inflate.findViewById(R.id.getQB)).setVisibility(8);
                }
                String obj = DateFormat.format("MM.dd", n.i * 1000).toString();
                textView3.setText("活动时间：" + obj + " - " + DateFormat.format("MM.dd", n.j * 1000).toString());
                return inflate;
            case 2:
                View inflate2 = ((Activity) context).getLayoutInflater().inflate((int) R.layout.downloadlist_header_layout2, (ViewGroup) null);
                TXImageView tXImageView = (TXImageView) inflate2.findViewById(R.id.bannerImg);
                if (!TextUtils.isEmpty(n.h.f2252a)) {
                    tXImageView.updateImageView(n.h.f2252a, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
                }
                return inflate2;
            default:
                return null;
        }
    }
}
