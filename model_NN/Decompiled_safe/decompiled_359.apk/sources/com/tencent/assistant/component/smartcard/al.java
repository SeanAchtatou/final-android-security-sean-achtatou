package com.tencent.assistant.component.smartcard;

import android.content.Intent;
import android.view.View;
import com.tencent.assistant.component.appdetail.CommentDetailTabView;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistantv2.activity.AppDetailActivityV5;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class al extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ STInfoV2 f1137a;
    final /* synthetic */ SmartSquareAppWithUserItem b;

    al(SmartSquareAppWithUserItem smartSquareAppWithUserItem, STInfoV2 sTInfoV2) {
        this.b = smartSquareAppWithUserItem;
        this.f1137a = sTInfoV2;
    }

    public void onTMAClick(View view) {
        Intent intent = new Intent(this.b.f1121a, AppDetailActivityV5.class);
        if (this.f1137a != null) {
            intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, this.f1137a.scene);
        }
        intent.putExtra(CommentDetailTabView.PARAMS_SIMPLE_MODEL_INFO, this.b.i.f1653a);
        this.b.f1121a.startActivity(intent);
    }

    public STInfoV2 getStInfo() {
        if (this.f1137a != null) {
            this.f1137a.actionId = 200;
            this.f1137a.updateStatusToDetail(this.b.i.f1653a);
        }
        return this.f1137a;
    }
}
