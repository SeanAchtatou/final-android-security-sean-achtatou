package com.tencent.assistant.component.homeEntry;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.Html;
import android.text.TextUtils;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.protocol.jce.EntranceBlock;
import com.tencent.assistant.utils.df;

/* compiled from: ProGuard */
public class HomeEntryCell2 extends HomeEntryCellBase {
    private TextView c;
    private TextView d;
    private TextView e;
    private TXImageView f;

    public HomeEntryCell2(Context context, EntranceBlock entranceBlock) {
        super(context, entranceBlock);
    }

    public void init() {
        inflate(this.f1052a, R.layout.home_entry_template_cell2, this);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        layoutParams.width = df.a(this.f1052a, 172.0f);
        layoutParams.height = df.a(this.f1052a, 90.0f);
        setLayoutParams(layoutParams);
        setGravity(16);
        setPadding(df.a(this.f1052a, 7.0f), 0, 0, 0);
        this.f = (TXImageView) findViewById(R.id.icon);
        this.c = (TextView) findViewById(R.id.name);
        this.d = (TextView) findViewById(R.id.title);
        this.e = (TextView) findViewById(R.id.desc);
    }

    public void fillValue() {
        if (this.b != null) {
            if (!TextUtils.isEmpty(this.b.b)) {
                this.c.setText(Html.fromHtml(this.b.b));
            }
            if (this.b.g != null) {
                if (this.b.g.containsKey("title")) {
                    this.d.setText(Html.fromHtml(this.b.g.get("title")));
                }
                if (this.b.g.containsKey("intro")) {
                    this.e.setText(Html.fromHtml(this.b.g.get("intro")));
                }
            }
        }
    }

    public void setImg(Bitmap bitmap) {
        this.f.setImageBitmap(bitmap);
    }
}
