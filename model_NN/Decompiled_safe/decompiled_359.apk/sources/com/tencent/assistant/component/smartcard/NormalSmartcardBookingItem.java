package com.tencent.assistant.component.smartcard;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.model.a.c;
import com.tencent.assistant.model.a.i;
import com.tencent.assistantv2.component.BookingButton;
import com.tencent.assistantv2.component.ListItemInfoView;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
public class NormalSmartcardBookingItem extends NormalSmartcardBaseItem {
    private TextView f;
    private BookingButton i;
    private TXImageView j;
    private TextView k;
    private ListItemInfoView l;
    private TextView m;

    public NormalSmartcardBookingItem(Context context) {
        this(context, null);
    }

    public NormalSmartcardBookingItem(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public NormalSmartcardBookingItem(Context context, i iVar, SmartcardListener smartcardListener, IViewInvalidater iViewInvalidater) {
        super(context, iVar, smartcardListener, iViewInvalidater);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!this.hasInit) {
            this.hasInit = true;
            d();
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.b.inflate((int) R.layout.smartcard_booking, this);
        this.f = (TextView) findViewById(R.id.title);
        this.j = (TXImageView) findViewById(R.id.iconimg);
        this.i = (BookingButton) findViewById(R.id.bookingbtn);
        this.k = (TextView) findViewById(R.id.appname);
        this.l = (ListItemInfoView) findViewById(R.id.iteminfo);
        this.l.a(ListItemInfoView.InfoType.CATEGORY_SIZE);
        this.m = (TextView) findViewById(R.id.desc);
        f();
    }

    /* access modifiers changed from: protected */
    public void b() {
        f();
    }

    private void f() {
        c cVar = null;
        if (this.smartcardModel instanceof c) {
            cVar = (c) this.smartcardModel;
        }
        if (cVar != null && cVar.f1637a != null) {
            String str = cVar.n;
            STInfoV2 a2 = a("03_001", 200);
            if (a2 != null) {
                a2.updateWithSimpleAppModel(cVar.f1637a);
            }
            setOnClickListener(new p(this, str, a2));
            this.f.setText(cVar.k);
            this.i.a(cVar, a2);
            this.j.updateImageView(cVar.f1637a.e, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            this.k.setText(cVar.f1637a.d);
            this.l.a(cVar.f1637a);
            this.m.setText(cVar.b);
        }
    }
}
