package com.tencent.assistant.component;

import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class ca extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PopViewDialog f983a;

    ca(PopViewDialog popViewDialog) {
        this.f983a = popViewDialog;
    }

    public void onTMAClick(View view) {
        this.f983a.shareBtnView.setSelected(!this.f983a.shareBtnView.isSelected());
        if (this.f983a.shareBtnView.isSelected()) {
            this.f983a.shareBtnView2.setSelected(false);
            boolean unused = this.f983a.shareToQZ = true;
            boolean unused2 = this.f983a.shareToWX = false;
            if (!this.f983a.loginProxy.k()) {
                this.f983a.errorTips.setText((int) R.string.comment_share_qzonetips);
                this.f983a.errorTips.setVisibility(0);
                return;
            }
            return;
        }
        boolean unused3 = this.f983a.shareToQZ = false;
        this.f983a.errorTips.setVisibility(4);
    }

    public STInfoV2 getStInfo() {
        STInfoV2 access$200 = this.f983a.buildFloatingSTInfo();
        if (access$200 != null) {
            access$200.slotId = a.a(this.f983a.getColumn(), "005");
            if (!this.f983a.shareBtnView.isSelected()) {
                access$200.status = "01";
            } else {
                access$200.status = "02";
            }
            access$200.actionId = 200;
        }
        return access$200;
    }
}
