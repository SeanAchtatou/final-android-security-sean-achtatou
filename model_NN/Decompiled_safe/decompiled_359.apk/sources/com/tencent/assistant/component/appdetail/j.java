package com.tencent.assistant.component.appdetail;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import com.tencent.assistant.activity.BrowserActivity;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.activity.AppDetailActivityV5;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class j extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Context f919a;
    final /* synthetic */ AppdetailFlagView b;

    j(AppdetailFlagView appdetailFlagView, Context context) {
        this.b = appdetailFlagView;
        this.f919a = context;
    }

    public void onTMAClick(View view) {
        Intent intent = new Intent(this.f919a, BrowserActivity.class);
        intent.putExtra("com.tencent.assistant.BROWSER_URL", "http://3gimg.qq.com/xiaoxie/xiaoxie.html?mode=0");
        this.f919a.startActivity(intent);
    }

    public STInfoV2 getStInfo() {
        if (!(this.f919a instanceof AppDetailActivityV5)) {
            return null;
        }
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.b.getContext(), 200);
        buildSTInfo.slotId = a.a(this.b.f, this.b.h);
        return buildSTInfo;
    }
}
