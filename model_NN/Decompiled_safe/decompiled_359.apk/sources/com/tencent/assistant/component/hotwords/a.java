package com.tencent.assistant.component.hotwords;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.utils.ct;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class a extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SimpleAppModel f1062a;
    final /* synthetic */ HotwordsItem b;

    a(HotwordsItem hotwordsItem, SimpleAppModel simpleAppModel) {
        this.b = hotwordsItem;
        this.f1062a = simpleAppModel;
    }

    public void onTMAClick(View view) {
        if (this.b.f1061a != null) {
            this.b.f1061a.onHotwordsItemAction(this.b.g, this.b.position);
        }
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.b.b, this.f1062a, this.b.slotId + "_" + ct.a(this.b.position), 200, null);
        if (buildSTInfo != null) {
            buildSTInfo.expatiation = this.b.slotId + ";" + this.b.pageId + ";" + "1;" + this.b.expatiation;
            buildSTInfo.searchPreId = this.b.searchPreId;
        }
        return buildSTInfo;
    }
}
