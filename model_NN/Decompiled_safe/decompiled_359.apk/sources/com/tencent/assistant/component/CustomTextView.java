package com.tencent.assistant.component;

import android.content.Context;
import android.graphics.Paint;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.TextView;
import com.tencent.assistant.utils.ct;
import com.tencent.assistant.utils.df;

/* compiled from: ProGuard */
public class CustomTextView extends TextView {
    private boolean cutFlag = false;
    private boolean isCut = false;
    private Paint mPaint;
    private String text;

    public CustomTextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        init();
    }

    public CustomTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init();
    }

    public CustomTextView(Context context) {
        super(context);
        init();
    }

    private void init() {
        this.mPaint = getPaint();
        this.text = getText().toString();
        refreshView();
    }

    public void setString(String str) {
        if (!TextUtils.isEmpty(str)) {
            this.text = str;
            requestLayout();
            refreshView();
        }
    }

    private void refreshView() {
        String str = this.text;
        if (this.cutFlag) {
            str = ct.a(this.text);
            float measureText = this.mPaint.measureText(str);
            float measuredWidth = (((float) ((getMeasuredWidth() - getPaddingLeft()) - getPaddingRight())) * 2.0f) - df.a(50.0f);
            if (measureText > measuredWidth) {
                str = str.substring(0, this.mPaint.breakText(str, true, measuredWidth, null)) + "...";
                this.isCut = true;
            } else {
                this.isCut = false;
            }
        } else {
            this.isCut = false;
        }
        setText(str);
        requestLayout();
    }

    public void setCutFlag(boolean z) {
        this.cutFlag = z;
        if (!TextUtils.isEmpty(this.text)) {
            requestLayout();
            refreshView();
        }
    }

    public boolean getCutFlag() {
        return this.cutFlag;
    }

    public boolean isCut() {
        return this.isCut;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        refreshView();
    }
}
