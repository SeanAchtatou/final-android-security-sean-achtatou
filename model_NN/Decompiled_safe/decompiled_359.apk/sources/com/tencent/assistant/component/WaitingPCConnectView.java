package com.tencent.assistant.component;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
public class WaitingPCConnectView extends LinearLayout {
    private Context context;
    private TextView loading_pc;

    public WaitingPCConnectView(Context context2) {
        super(context2);
        this.context = context2;
        init();
    }

    public WaitingPCConnectView(Context context2, AttributeSet attributeSet) {
        super(context2, attributeSet);
        this.context = context2;
        init();
    }

    private void init() {
        this.loading_pc = (TextView) LayoutInflater.from(this.context).inflate((int) R.layout.waiting_pc_connect, this).findViewById(R.id.loading_pc);
    }

    public void updatePc(String str) {
        if (this.loading_pc != null && !TextUtils.isEmpty(str)) {
            this.loading_pc.setText(String.format(this.context.getString(R.string.photo_backup_connect_pc_waiting), str));
        }
    }

    public void getPcListStart() {
        if (this.loading_pc != null) {
            this.loading_pc.setText(this.context.getString(R.string.photo_backup_geting_pc));
        }
    }
}
