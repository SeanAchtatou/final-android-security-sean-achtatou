package com.tencent.assistant.component;

import android.widget.RatingBar;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class bw implements RatingBar.OnRatingBarChangeListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PopViewDialog f978a;

    bw(PopViewDialog popViewDialog) {
        this.f978a = popViewDialog;
    }

    public void onRatingChanged(RatingBar ratingBar, float f, boolean z) {
        this.f978a.tipsText.setTextColor(this.f978a.mContext.getResources().getColor(R.color.appadmin_risk_tips));
        if (f > 4.0f) {
            this.f978a.tipsText.setText((int) R.string.comment_five_star);
        } else if (f > 3.0f) {
            this.f978a.tipsText.setText((int) R.string.comment_four_star);
        } else if (f > 2.0f) {
            this.f978a.tipsText.setText((int) R.string.comment_three_star);
        } else if (f > 1.0f) {
            this.f978a.tipsText.setText((int) R.string.comment_two_star);
        } else if (f > 0.0f) {
            this.f978a.tipsText.setText((int) R.string.comment_one_star);
        } else {
            this.f978a.tipsText.setText((int) R.string.comment_pop_star_text);
        }
        STInfoV2 access$200 = this.f978a.buildFloatingSTInfo();
        if (access$200 != null) {
            access$200.slotId = a.a(this.f978a.getColumn(), "001");
            access$200.actionId = 200;
            k.a(access$200);
        }
    }
}
