package com.tencent.assistant.component;

import android.content.Intent;
import android.view.View;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.listener.OnTMAClickListener;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistantv2.activity.SearchActivity;

/* compiled from: ProGuard */
class cy extends OnTMAClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SecondNavigationTitleView f1007a;

    cy(SecondNavigationTitleView secondNavigationTitleView) {
        this.f1007a = secondNavigationTitleView;
    }

    public void onTMAClick(View view) {
        Intent intent = new Intent(this.f1007a.context, SearchActivity.class);
        if (this.f1007a.context instanceof BaseActivity) {
            intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, ((BaseActivity) this.f1007a.context).f());
        }
        this.f1007a.context.startActivity(intent);
    }
}
