package com.tencent.assistant.component;

import com.tencent.assistant.component.TouchAnalizer;

/* compiled from: ProGuard */
public interface TouchBehaviorListener {
    boolean onInvoke(TouchAnalizer.BehaviorType behaviorType, float f, float f2, int i);
}
