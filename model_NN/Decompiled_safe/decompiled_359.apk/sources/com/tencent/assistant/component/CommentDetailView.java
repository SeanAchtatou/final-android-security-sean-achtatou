package com.tencent.assistant.component;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.appdetail.CommentDetailTabView;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessageHandler;
import com.tencent.assistant.component.invalidater.ViewPageScrollListener;
import com.tencent.assistant.component.txscrollview.ITXRefreshListViewListener;
import com.tencent.assistant.component.txscrollview.TXAppIconView;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.a;
import com.tencent.assistant.module.bf;
import com.tencent.assistant.module.bi;
import com.tencent.assistant.module.callback.k;
import com.tencent.assistant.module.e;
import com.tencent.assistant.module.ek;
import com.tencent.assistant.protocol.jce.CommentDetail;
import com.tencent.assistant.protocol.jce.CommentReply;
import com.tencent.assistant.protocol.jce.CommentTagInfo;
import com.tencent.assistant.protocol.jce.RatingInfo;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.ct;
import com.tencent.assistant.utils.cv;
import com.tencent.assistant.utils.df;
import com.tencent.assistant.utils.t;
import com.tencent.assistantv2.activity.AppDetailActivityV5;
import com.tencent.assistantv2.component.al;
import com.tencent.assistantv2.component.appdetail.b;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

/* compiled from: ProGuard */
public class CommentDetailView extends LinearLayout implements ITXRefreshListViewListener {
    private static final int MAX_EXPAND_REPLY = 4;
    private static final int MAX_REPLY = 2;
    private static final int MAX_REPLY_ME = 2;
    public static boolean isTestMode = false;
    private final String KEY_COMMENT = "comment";
    private final String KEY_CUR_VERSION_COMMENT = "curVerComment";
    private final String KEY_DATA = "data";
    private final String KEY_HASNEXT = "hasNext";
    private final String KEY_ISFIRSTPAGE = "isFirstPage";
    private final String KEY_OLD_COMMENT_ID = "oldCommentId";
    private final String KEY_REQTAGINFO = "reqTagInfo";
    private final String KEY_TAG_LIST = "taglist";
    private final int MSG_PAGE_COMMENT_RSP = 1;
    private final int MSG_PAGE_WRITE_COMMENT_RSP = 2;
    private ArrayList<CommentTagInfo> allTagList = null;
    /* access modifiers changed from: private */
    public long apkId = 0;
    public k callBack = new v(this);
    /* access modifiers changed from: private */
    public String cancelpraiseColId = "09_";
    /* access modifiers changed from: private */
    public boolean clickCommentTag = false;
    private int[] colorResIdList = {R.drawable.pinglun_icon_jinghua1, R.drawable.pinglun_icon_jinghua2};
    private long commentCount = 0;
    private bf commentEngine = new bf();
    /* access modifiers changed from: private */
    public LinearLayout commentListView;
    private View commentTxtArea;
    /* access modifiers changed from: private */
    public ArrayList<CommentDetail> comments = new ArrayList<>();
    private View commentsTitle;
    /* access modifiers changed from: private */
    public bi engine = null;
    private NormalErrorRecommendPage errorPage;
    public al flowListener = new t(this);
    /* access modifiers changed from: private */
    public boolean isLoadingFirstPageData = false;
    private final String kEY_SELECTED_DATA = "selectedData";
    private LoadingView loadingView = null;
    private int mCommentListSizeBeforeAdd = 0;
    /* access modifiers changed from: private */
    public Context mContext;
    /* access modifiers changed from: private */
    public CommentFooterView mFooterView;
    /* access modifiers changed from: private */
    public CommentHeaderTagView mHeaderView;
    private LayoutInflater mInflater;
    /* access modifiers changed from: private */
    public CommentSucceedListener mListener;
    private ek modifyEngine = new ek();
    private boolean needInitScroll = false;
    private View.OnClickListener onFooterClick = new u(this);
    /* access modifiers changed from: private */
    public ViewPageScrollListener pageChangeListener;
    /* access modifiers changed from: private */
    public ViewInvalidateMessageHandler pageMessageHandler = new w(this);
    private b pagerHeightListener;
    /* access modifiers changed from: private */
    public Animation praiseAnimation = null;
    /* access modifiers changed from: private */
    public String praiseColId = "08_";
    /* access modifiers changed from: private */
    public int praiseCount = 0;
    /* access modifiers changed from: private */
    public e praiseEngine = new e();
    /* access modifiers changed from: private */
    public String[] praiseTextList = {"+1", "好", "顶", "赞", "朕觉OK", "不明觉厉", "吊炸天"};
    private RatingInfo ratingInfo = null;
    private long replayId;
    private a replyEngine = a.a();
    private int retryCount = 3;
    /* access modifiers changed from: private */
    public LinearLayout selectCommentList;
    /* access modifiers changed from: private */
    public ArrayList<CommentDetail> selectComments;
    private TextView selectCommentsCount;
    private TextView selectCommentsCountNum;
    /* access modifiers changed from: private */
    public SimpleAppModel simpleAppModel = null;
    private int versionCode;

    /* compiled from: ProGuard */
    public interface CommentSucceedListener {
        void onCommentSucceed(boolean z, long j);

        void onGetOldComment(long j, String str, int i, int i2);
    }

    static /* synthetic */ int access$2308(CommentDetailView commentDetailView) {
        int i = commentDetailView.praiseCount;
        commentDetailView.praiseCount = i + 1;
        return i;
    }

    public CommentDetailView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.mContext = context;
        initBaseView();
    }

    public CommentDetailView(Context context) {
        super(context);
        this.mContext = context;
        initBaseView();
    }

    public void initDetailView(Map<String, Object> map) {
        if (map != null) {
            setViewParams(map);
            initView();
        }
    }

    public bf getCommentEngine() {
        if (this.commentEngine == null) {
            this.commentEngine = new bf();
        }
        return this.commentEngine;
    }

    private void initBaseView() {
        this.mInflater = LayoutInflater.from(this.mContext);
        this.mInflater.inflate((int) R.layout.comment_detail_activity, this);
        this.selectCommentsCount = (TextView) findViewById(R.id.select_comment_numbers);
        this.selectCommentList = (LinearLayout) findViewById(R.id.select_comment_layout);
        this.selectCommentsCountNum = (TextView) findViewById(R.id.select_comment_numbers_txt);
        this.commentListView = (LinearLayout) findViewById(R.id.comment_Layout);
        this.errorPage = (NormalErrorRecommendPage) findViewById(R.id.error_page);
        this.errorPage.setButtonClickListener(new s(this));
        this.loadingView = (LoadingView) findViewById(R.id.loading_view);
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) this.loadingView.getLayoutParams();
        layoutParams.topMargin = (int) (-80.0f * t.d);
        this.loadingView.setLayoutParams(layoutParams);
        this.praiseAnimation = AnimationUtils.loadAnimation(this.mContext, R.anim.comment_praise_animation);
    }

    private void initView() {
        initHeaderView();
        initFooterView();
        initLayout();
    }

    private void setViewParams(Map<String, Object> map) {
        this.simpleAppModel = (SimpleAppModel) map.get(CommentDetailTabView.PARAMS_SIMPLE_MODEL_INFO);
        this.ratingInfo = (RatingInfo) map.get(CommentDetailTabView.PARAMS_RATING_INFO);
        this.apkId = ((Long) map.get(CommentDetailTabView.PARAMS_APK_ID)).longValue();
        this.commentCount = ((Long) map.get(CommentDetailTabView.PARAMS_COMMENT_COUNT)).longValue();
        this.versionCode = ((Integer) map.get(CommentDetailTabView.PARAMS_VERSION_CODE)).intValue();
        this.replayId = ((Long) map.get(CommentDetailTabView.PARAMS_REPLY_ID)).longValue();
        this.allTagList = (ArrayList) map.get(CommentDetailTabView.PARAMS_ALLTAG_LIST);
    }

    public void setCommentListener(CommentSucceedListener commentSucceedListener) {
        this.mListener = commentSucceedListener;
    }

    public void refreshFirstPageData(CommentTagInfo commentTagInfo) {
        showBar();
        this.isLoadingFirstPageData = true;
        this.engine.a(this.simpleAppModel.f1634a, this.apkId, this.simpleAppModel.c, this.simpleAppModel.g, commentTagInfo, this.allTagList);
    }

    public void requestNextPageData() {
        if (this.mFooterView != null) {
            this.mFooterView.freshState(2);
            this.engine.b();
        }
    }

    private void initHeaderView() {
        this.mHeaderView = (CommentHeaderTagView) findViewById(R.id.comment_header);
        this.mHeaderView.setAverageScore(String.valueOf(((double) Math.round(this.ratingInfo.b * 10.0d)) / 10.0d));
        long j = this.ratingInfo.f2271a;
        this.mHeaderView.setTotalUser(String.format(getResources().getString(R.string.comment_detail_average_users), ct.a(j)));
        this.mHeaderView.setOnFlowLayoutListener(this.flowListener);
        this.commentTxtArea = findViewById(R.id.comment_txt_area);
    }

    private void initFooterView() {
        this.mFooterView = new CommentFooterView(this.mContext, TXScrollViewBase.ScrollDirection.SCROLL_DIRECTION_VERTICAL, TXScrollViewBase.ScrollMode.NONE);
        this.mFooterView.setOnClickListener(this.onFooterClick);
        this.mFooterView.freshState(1);
    }

    public int getFooterViewState() {
        if (this.mFooterView == null) {
            return 1;
        }
        return this.mFooterView.getState();
    }

    private void initLayout() {
        this.mHeaderView.setVisibility(8);
        this.selectCommentsCount.setVisibility(8);
        this.selectCommentList.setVisibility(8);
        this.selectCommentsCountNum.setVisibility(8);
        this.commentListView.setVisibility(8);
    }

    public void onResume() {
        if (this.engine == null) {
            this.engine = new bi();
        }
        this.engine.register(this.callBack);
        if (this.commentEngine == null) {
            this.commentEngine = new bf();
        }
        this.commentEngine.register(this.callBack);
        this.replyEngine.register(this.callBack);
        this.modifyEngine.register(this.callBack);
        this.praiseEngine.register(this.callBack);
    }

    public void onPause() {
        if (this.engine == null) {
        }
    }

    public void onDestroy() {
        this.comments.clear();
        if (this.selectComments != null) {
            this.selectComments.clear();
        }
        this.engine.unregister(this.callBack);
        this.commentEngine.unregister(this.callBack);
        this.modifyEngine.unregister(this.callBack);
        this.replyEngine.unregister(this.callBack);
        this.praiseEngine.unregister(this.callBack);
    }

    private void refreshHeaderView(CommentDetail commentDetail) {
        HashMap hashMap = (HashMap) this.ratingInfo.c;
        hashMap.put(Integer.valueOf(commentDetail.b), Long.valueOf(((Long) hashMap.get(Integer.valueOf(commentDetail.b))).longValue() + 1));
        RatingInfo ratingInfo2 = this.ratingInfo;
        long j = ratingInfo2.f2271a + 1;
        ratingInfo2.f2271a = j;
        this.mHeaderView.setTotalUser(String.format(getResources().getString(R.string.comment_detail_average_users), ct.a(j)));
    }

    /* access modifiers changed from: private */
    public void showListView() {
        this.loadingView.setVisibility(8);
        findViewById(R.id.bottom_view).setVisibility(0);
        this.commentListView.setVisibility(0);
        this.mHeaderView.setVisibility(0);
        this.errorPage.setVisibility(8);
        this.commentTxtArea.setVisibility(0);
    }

    /* access modifiers changed from: private */
    public void setSelectVisibility(int i) {
        this.selectCommentList.setVisibility(i);
        if (!(this.selectCommentsCount == null || this.selectCommentsCountNum == null)) {
            this.selectCommentsCount.setVisibility(i);
            this.selectCommentsCountNum.setVisibility(i);
        }
        if (i == 0 && this.commentTxtArea != null) {
            this.commentTxtArea.setVisibility(0);
        }
    }

    private void showBar() {
        this.loadingView.setVisibility(0);
        if (this.commentListView != null) {
            this.commentListView.setVisibility(8);
        }
        if (!this.clickCommentTag && this.mHeaderView != null) {
            this.mHeaderView.setVisibility(8);
        }
        if (this.selectCommentList != null) {
            setSelectVisibility(8);
        }
        if (this.errorPage != null) {
            this.errorPage.setVisibility(8);
        }
    }

    public void hideBar() {
        this.loadingView.setVisibility(8);
    }

    private void showErrorPage(int i) {
        this.loadingView.setVisibility(8);
        this.commentListView.setVisibility(8);
        setSelectVisibility(8);
        this.errorPage.setVisibility(0);
        this.errorPage.setErrorType(i);
        findViewById(R.id.bottom_view).setVisibility(8);
    }

    public void popMsgToUI(CommentDetail commentDetail) {
        refreshHeaderView(commentDetail);
        if (this.comments.size() > 0 && this.comments.get(0).l) {
            ((TextView) this.commentListView.getChildAt(0).findViewById(R.id.nick_name)).setText(this.comments.get(0).c);
        }
        this.comments.add(0, commentDetail);
        this.commentListView.addView(fillValues(commentDetail, 0), 0);
    }

    public void setPageChangeListener(ViewPageScrollListener viewPageScrollListener) {
        this.pageChangeListener = viewPageScrollListener;
    }

    /* access modifiers changed from: private */
    public void onCommentListResponseHandle(int i, boolean z, CommentTagInfo commentTagInfo, List<CommentDetail> list, List<CommentDetail> list2, List<CommentTagInfo> list3, boolean z2, CommentDetail commentDetail) {
        if (i == 0) {
            showListView();
            if (z) {
                List list4 = null;
                setSelectVisibility(8);
                if (list3 != null && list3.size() > 0) {
                    this.mHeaderView.setCommentTagListData(commentTagInfo, list3);
                    this.mHeaderView.setTagListVisible(0);
                }
                if (this.clickCommentTag) {
                    this.comments.clear();
                    this.commentListView.removeAllViews();
                    requestLayout();
                    this.clickCommentTag = false;
                }
                if (list == null || list.size() == 0) {
                    showErrorPage(100);
                    this.mFooterView.setVisibility(8);
                    if (list4 == null || list4.size() == 0) {
                        this.commentTxtArea.setVisibility(8);
                    }
                    this.mListener.onGetOldComment(-1, Constants.STR_EMPTY, -1, -1);
                    return;
                }
                if (list.get(0).l && list.get(0).k != null) {
                    Iterator<CommentReply> it = list.get(0).k.iterator();
                    while (it.hasNext()) {
                        CommentReply next = it.next();
                        if (this.replayId > 0 && next.f2042a == this.replayId) {
                            this.needInitScroll = true;
                        }
                    }
                }
                fillComment(list);
                this.comments.addAll(list);
                if (this.engine.a()) {
                    this.mFooterView.freshState(1);
                } else {
                    this.mFooterView.setVisibility(8);
                }
                if (commentDetail == null || commentDetail.h <= 0) {
                    this.mListener.onGetOldComment(-1, Constants.STR_EMPTY, -1, -1);
                } else {
                    this.mListener.onGetOldComment(commentDetail.h, commentDetail.g, commentDetail.b, commentDetail.d);
                }
            } else {
                if (list != null && list.size() > 0) {
                    fillComment(list);
                    if (this.comments != null) {
                        this.comments.addAll(list);
                    }
                }
                if (this.engine.a()) {
                    this.mFooterView.freshState(1);
                } else {
                    this.mFooterView.setVisibility(8);
                }
            }
            if (!z2) {
                this.mFooterView.setVisibility(8);
                this.mFooterView.freshState(3);
            }
        } else if (!z) {
            this.mFooterView.freshState(4);
            Toast.makeText(this.mContext, this.mContext.getString(R.string.is_next_page_error_happen), 0).show();
        } else if (-800 == i) {
            showErrorPage(30);
        } else if (this.retryCount <= 0) {
            showErrorPage(20);
        } else {
            this.engine.a(this.simpleAppModel.f1634a, this.apkId, this.simpleAppModel.c, this.simpleAppModel.g, null, this.allTagList);
            this.retryCount--;
        }
    }

    /* access modifiers changed from: private */
    public void onWriteCommentFinishHandle(int i, CommentDetail commentDetail, long j) {
        boolean z = false;
        if (com.tencent.assistant.login.a.a.b(i)) {
            if (i == 0) {
                showListView();
                if (commentDetail != null) {
                    processWriteRequestSucc(commentDetail);
                    popMsgToUI(commentDetail);
                }
            } else if (i != 1) {
                processWriteRequestFailed();
            } else if (commentDetail != null) {
                removeComment(j);
                this.comments.add(0, commentDetail);
                this.commentListView.addView(fillValues(commentDetail, 0), 0);
                Toast.makeText(this.mContext, this.mContext.getString(R.string.comment_modify_succ), 0).show();
            }
            if (this.mListener != null) {
                long j2 = -1;
                if (commentDetail != null) {
                    j2 = commentDetail.h;
                }
                CommentSucceedListener commentSucceedListener = this.mListener;
                if (i == 0 || i == 1) {
                    z = true;
                }
                commentSucceedListener.onCommentSucceed(z, j2);
            }
        }
    }

    public void clickCommentTag(CommentTagInfo commentTagInfo) {
        this.mHeaderView.clickCommentTag(commentTagInfo);
    }

    private void removeComment(long j) {
        if (this.selectComments != null) {
            for (int i = 0; i < this.selectComments.size(); i++) {
                if (this.selectComments.get(i).h == j) {
                    this.selectComments.remove(i);
                    this.selectCommentList.removeViewAt(i);
                    return;
                }
            }
        }
        if (this.comments != null) {
            for (int i2 = 0; i2 < this.comments.size(); i2++) {
                if (this.comments.get(i2).h == j) {
                    this.comments.remove(i2);
                    this.commentListView.removeViewAt(i2);
                    return;
                }
            }
        }
    }

    public void processWriteRequestFailed() {
        Toast.makeText(this.mContext, this.mContext.getString(R.string.comment_failed), 0).show();
    }

    public void processWriteRequestSucc(CommentDetail commentDetail) {
        if (commentDetail.a() < this.versionCode) {
            Toast.makeText(this.mContext, this.mContext.getString(R.string.comment_succ_old), 0).show();
        } else {
            Toast.makeText(this.mContext, this.mContext.getString(R.string.comment_succ), 0).show();
        }
    }

    public void onTXRefreshListViewRefresh(TXScrollViewBase.ScrollState scrollState) {
        requestNextPageData();
    }

    private void fillSelectComment(List<CommentDetail> list) {
        View fillValues;
        if (list.size() > 10) {
            list = list.subList(0, 10);
        }
        this.selectCommentsCount.setText(String.format(getResources().getString(R.string.select_comment_detail_num), Integer.valueOf(list.size())).substring(0, 4));
        this.selectCommentsCountNum.setText(String.format(getResources().getString(R.string.select_comment_detail_num), Integer.valueOf(list.size())).substring(4));
        for (int i = 0; i < Math.min(list.size(), 4); i++) {
            if (!isRepeatComment(list.get(i)) && (fillValues = fillValues(list.get(i), i)) != null) {
                if (i == list.size() - 1) {
                    ((RelativeLayout.LayoutParams) ((ImageView) fillValues.findViewById(R.id.divider)).getLayoutParams()).setMargins(0, df.b(3.0f), 0, 0);
                }
                this.selectCommentList.addView(fillValues);
                reportSTLog(com.tencent.assistantv2.st.page.a.a("07", i), 100);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void reportSTLog(String str, int i) {
        if (getContext() instanceof AppDetailActivityV5) {
            STInfoV2 i2 = ((AppDetailActivityV5) getContext()).i();
            if (i == 100) {
                STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.mContext, 100);
                buildSTInfo.slotId = str;
                buildSTInfo.appId = i2.appId;
                com.tencent.assistantv2.st.k.a(buildSTInfo);
                return;
            }
            i2.slotId = str;
            i2.actionId = i;
            com.tencent.assistantv2.st.k.a(i2);
        }
    }

    private void fillComment(List<CommentDetail> list) {
        View fillValues;
        this.commentListView.removeView(this.mFooterView);
        this.mCommentListSizeBeforeAdd = this.commentListView.getChildCount();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < list.size()) {
                if (!isRepeatComment(list.get(i2)) && (fillValues = fillValues(list.get(i2), i2)) != null) {
                    this.commentListView.addView(fillValues);
                }
                i = i2 + 1;
            } else {
                this.commentListView.addView(this.mFooterView);
                requestLayout();
                return;
            }
        }
    }

    private boolean isRepeatComment(CommentDetail commentDetail) {
        if (this.selectComments != null) {
            for (int i = 0; i < this.selectComments.size(); i++) {
                if (this.selectComments.get(i).h == commentDetail.h) {
                    return true;
                }
            }
        }
        if (this.comments == null) {
            return false;
        }
        for (int i2 = 0; i2 < this.comments.size(); i2++) {
            if (this.comments.get(i2).h == commentDetail.h) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        if (this.needInitScroll) {
            this.needInitScroll = false;
            if (this.pagerHeightListener != null) {
                this.pagerHeightListener.b(this.commentListView.getTop());
            }
        }
    }

    public void setPagerHeight(int i) {
        this.loadingView.getLayoutParams().height = i;
        this.errorPage.getLayoutParams().height = i;
    }

    /* access modifiers changed from: private */
    public View fillValues(CommentDetail commentDetail, int i) {
        long j;
        int i2;
        ac acVar = new ac(this, null);
        View inflate = this.mInflater.inflate((int) R.layout.comment_view, (ViewGroup) null);
        ((TXAppIconView) inflate.findViewById(R.id.picture)).updateImageView(commentDetail.i, R.drawable.common_icon_useravarta_default, TXImageView.TXImageViewType.ROUND_IMAGE);
        TextView textView = (TextView) inflate.findViewById(R.id.nick_name);
        TextView textView2 = (TextView) inflate.findViewById(R.id.likeanimation);
        TextView textView3 = (TextView) inflate.findViewById(R.id.likecount);
        TextView textView4 = (TextView) inflate.findViewById(R.id.myorfriends);
        ImageView imageView = (ImageView) inflate.findViewById(R.id.colorselect);
        if (commentDetail.l && i == 0) {
            textView.setText((int) R.string.comment_nick_my_comment);
            textView.setTextColor(Color.parseColor("#ff7a0c"));
            textView4.setVisibility(8);
        } else if (TextUtils.isEmpty(commentDetail.c)) {
            textView.setText((int) R.string.comment_default_nick_name);
            textView4.setVisibility(8);
        } else if (!commentDetail.c.startsWith("(好友)")) {
            textView.setText(commentDetail.c);
            textView4.setVisibility(8);
        } else {
            String substring = commentDetail.c.substring(4);
            if (substring.length() > 8) {
                substring = substring.substring(0, 8) + "...";
            }
            if (TextUtils.isEmpty(substring)) {
                textView.setText((int) R.string.comment_default_nick_name);
                textView4.setVisibility(8);
            } else {
                textView.setText(substring);
                textView.setTextColor(Color.parseColor("#ff7a0c"));
                textView4.setVisibility(0);
            }
        }
        acVar.b = textView;
        acVar.c = textView4;
        RatingView ratingView = (RatingView) inflate.findViewById(R.id.comment_score);
        acVar.f881a = ratingView;
        ratingView.setRating((float) commentDetail.b);
        TextView textView5 = (TextView) inflate.findViewById(R.id.time);
        acVar.d = textView5;
        if (commentDetail.m <= 0) {
            j = commentDetail.f2041a;
        } else {
            j = commentDetail.m;
        }
        textView5.setText(formatTime(j * 1000));
        TextView textView6 = (TextView) inflate.findViewById(R.id.content);
        acVar.e = textView6;
        textView6.setText(commentDetail.g);
        if (commentDetail.t) {
            imageView.setImageResource(this.colorResIdList[new Random().nextInt(this.colorResIdList.length)]);
            imageView.setVisibility(0);
        }
        textView3.setText(ct.a(commentDetail.b()) + Constants.STR_EMPTY);
        textView3.setTag(R.id.comment_praise_count, Long.valueOf(commentDetail.b()));
        Drawable drawable = getResources().getDrawable(R.drawable.pinglun_icon_zan);
        drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
        Drawable drawable2 = getResources().getDrawable(R.drawable.pinglun_icon_yizan);
        drawable2.setBounds(0, 0, drawable2.getMinimumWidth(), drawable2.getMinimumHeight());
        textView3.setCompoundDrawablePadding(df.a(this.mContext, 6.0f));
        if (commentDetail.c() == 0) {
            textView3.setCompoundDrawables(drawable, null, null, null);
            textView3.setTextColor(Color.parseColor("#6e6e6e"));
            textView3.setTag(false);
        } else if (commentDetail.c() == 1) {
            textView3.setCompoundDrawables(drawable2, null, null, null);
            textView3.setTextColor(Color.parseColor("#b68a46"));
            textView3.setTag(true);
        } else {
            textView3.setCompoundDrawables(drawable, null, null, null);
            textView3.setTextColor(Color.parseColor("#6e6e6e"));
            textView3.setTag(false);
        }
        com.tencent.assistantv2.st.k.a(new STInfoV2(STConst.ST_PAGE_APP_DETAIL_COMMENT, this.praiseColId + ct.a(i + 1), 0, STConst.ST_DEFAULT_SLOT, 100));
        textView3.setTag(R.id.tma_st_slot_tag, ct.a(i + 1));
        textView3.setOnClickListener(new x(this, drawable, commentDetail, i, drawable2, textView2));
        acVar.h = textView3;
        TextView textView7 = (TextView) inflate.findViewById(R.id.version);
        if (!TextUtils.isEmpty(t.u()) && t.u().equals("LGE-Nexus 5") && Build.VERSION.SDK_INT >= 20) {
            textView7.setPadding(textView7.getPaddingLeft(), getResources().getDimensionPixelSize(R.dimen.app_detail_float_share_yyb_dialog_y), textView7.getPaddingRight(), textView7.getBottom());
            ratingView.setPadding(ratingView.getPaddingLeft(), getResources().getDimensionPixelSize(R.dimen.app_detail_float_share_yyb_dialog_y), ratingView.getPaddingRight(), ratingView.getBottom());
        }
        if (commentDetail.d > 0) {
            textView7.setVisibility(0);
            if (this.versionCode <= commentDetail.d) {
                textView7.setText(this.mContext.getString(R.string.comment_detail_current_version));
            } else {
                textView7.setText(commentDetail.u);
            }
        }
        LinearLayout linearLayout = (LinearLayout) inflate.findViewById(R.id.replies_list);
        acVar.f = linearLayout;
        if (commentDetail.k == null || commentDetail.k.size() <= 0) {
            linearLayout.setVisibility(8);
        } else {
            int size = commentDetail.k.size();
            boolean z = commentDetail.l;
            if (isTestMode) {
                z = !z;
            }
            if (z) {
                int i3 = 0;
                if (size > 2) {
                    int i4 = size - 2;
                    linearLayout.addView(getMoreReplyLayout(acVar, commentDetail, size - 2, commentDetail.k.subList(0, i4), true, i, linearLayout, z, this.mCommentListSizeBeforeAdd), 0, new LinearLayout.LayoutParams(-1, df.b(36.0f)));
                    i3 = i4;
                }
                for (int i5 = i3; i5 < size; i5++) {
                    CommentReply commentReply = commentDetail.k.get(i5);
                    View replyView = getReplyView(commentReply.e, i, commentReply.d, commentReply.c, commentReply.b);
                    linearLayout.addView(replyView);
                    if (i5 == size - 1) {
                        replyView.findViewById(R.id.divider).setVisibility(8);
                    }
                }
                if (!commentDetail.k.get(size - 1).d.equals(commentDetail.c)) {
                    linearLayout.addView(getReplyLayout(linearLayout, commentDetail), new LinearLayout.LayoutParams(-1, df.b(28.0f)));
                }
            } else {
                if (size > 2) {
                    i2 = 2;
                } else {
                    i2 = size;
                }
                for (int i6 = 0; i6 < i2; i6++) {
                    CommentReply commentReply2 = commentDetail.k.get(i6);
                    View replyView2 = getReplyView(commentReply2.e, i, commentReply2.d, commentReply2.c, commentReply2.b);
                    linearLayout.addView(replyView2);
                    if (i6 == i2 - 1 && size <= 2) {
                        replyView2.findViewById(R.id.divider).setVisibility(8);
                    }
                }
                if (size > 2) {
                    linearLayout.addView(getMoreReplyLayout(acVar, commentDetail, size - 2, commentDetail.k.subList(i2, size), false, i, linearLayout, z, this.mCommentListSizeBeforeAdd), new LinearLayout.LayoutParams(-1, df.b(28.0f)));
                }
            }
        }
        inflate.setTag(acVar);
        return inflate;
    }

    /* access modifiers changed from: private */
    public void updateComment(ac acVar, String str, int i, long j, String str2, boolean z) {
        if (i > 0) {
            acVar.f881a.setRating((float) i);
        }
        if (!TextUtils.isEmpty(str)) {
            acVar.e.setText(str);
        }
        if (!TextUtils.isEmpty(str2)) {
            acVar.b.setText(str2);
        }
        if (j > 0) {
            acVar.d.setText(formatTime(1000 * j));
        }
        if (z) {
            acVar.f.removeAllViews();
            acVar.f.setVisibility(8);
        }
    }

    public static String formatTime(long j) {
        return cv.g(j);
    }

    private View getReplyLayout(LinearLayout linearLayout, CommentDetail commentDetail) {
        View inflate = this.mInflater.inflate((int) R.layout.comment_reply_layout, (ViewGroup) null);
        ((TextView) inflate.findViewById(R.id.reply)).setOnClickListener(new aa(this, commentDetail));
        return inflate;
    }

    /* access modifiers changed from: private */
    public View getMoreReplyLayout(ac acVar, CommentDetail commentDetail, int i, List<CommentReply> list, boolean z, int i2, ViewGroup viewGroup, boolean z2, int i3) {
        RelativeLayout relativeLayout = (RelativeLayout) this.mInflater.inflate((int) R.layout.comment_more_reply_layout, (ViewGroup) null);
        TextView textView = (TextView) relativeLayout.findViewById(R.id.display_more_replies);
        if (!z) {
            relativeLayout.findViewById(R.id.divider).setVisibility(8);
        }
        if (z) {
            textView.setText(this.mContext.getString(R.string.comment_display_more_replies_main));
        } else {
            textView.setText(this.mContext.getString(R.string.comment_display_more_replies_guest));
        }
        textView.setTag(Boolean.valueOf(z2));
        textView.setTag(R.id.tag, Integer.valueOf(i3));
        textView.setOnClickListener(new ab(this, acVar, list, z, viewGroup, i2, commentDetail, i3, textView, z2));
        return relativeLayout;
    }

    /* access modifiers changed from: private */
    public View getReplyView(boolean z, int i, String str, String str2, long j) {
        View inflate = LayoutInflater.from(this.mContext).inflate((int) R.layout.comment_reply_view, (ViewGroup) null);
        TextView textView = (TextView) inflate.findViewById(R.id.nick_name);
        if (z && i == 0) {
            str = this.mContext.getResources().getString(R.string.comment_nick_me);
        }
        textView.setText(str);
        ((TextView) inflate.findViewById(R.id.content)).setText(str2);
        ((TextView) inflate.findViewById(R.id.time)).setText(formatTime(1000 * j));
        return inflate;
    }

    public void setPagerHeightListener(b bVar) {
        this.pagerHeightListener = bVar;
    }

    public ek getModifyAppCommentEngine() {
        return this.modifyEngine;
    }
}
