package com.tencent.assistant.component;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.ScaleAnimation;
import android.view.animation.Transformation;
import android.view.animation.TranslateAnimation;
import com.tencent.assistant.component.TouchAnalizer;
import com.tencent.assistant.utils.df;
import com.tencent.assistant.utils.r;

/* compiled from: ProGuard */
public class AnimationImageView extends View implements Animation.AnimationListener, TouchBehaviorListener {
    private static final long CLICK_DELAY = 550;
    private Animation anim;
    private boolean animAfterLayout = true;
    private int[] animAttrs;
    private boolean animFixCenter = false;
    private boolean bControl = true;
    private boolean dragRet = false;
    private float dragX = -1.0f;
    private float dragY = -1.0f;
    private boolean dragging = false;
    private Drawable hScrollBar;
    private boolean longClicked = false;
    private Bitmap mBitmap;
    private Drawable mDrawable;
    private Handler mHandler = new a(this);
    private int mHeight;
    private float mRotate = 0.0f;
    private float mRotateTmp = 0.0f;
    private float mScale = 1.0f;
    private int mWidth;
    private float[] matrixValue;
    private boolean multiCtrl = false;
    /* access modifiers changed from: private */
    public View.OnClickListener onClickListener;
    private Paint paint = new Paint(1);
    private Shieldable parent;
    private int positionX = 0;
    private int positionY = 0;
    private float rotateX = 0.0f;
    private float rotateY = 0.0f;
    private Animation sbAnim;
    private Transformation sbTrans;
    private boolean shouldDrawScrollBar = false;
    private Animation.AnimationListener startListener;
    private TouchAnalizer t = new TouchAnalizer();
    private Transformation trans;
    private Drawable vScrollBar;

    public void setOnClickListener(View.OnClickListener onClickListener2) {
        this.onClickListener = onClickListener2;
    }

    public AnimationImageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        init(context);
    }

    public AnimationImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init(context);
    }

    public AnimationImageView(Context context) {
        super(context);
        init(context);
    }

    private void init(Context context) {
        this.paint.setFilterBitmap(true);
        this.t.setListener(TouchAnalizer.BehaviorType.SINGLE_CLICK, this);
        if (r.d() <= 4) {
            this.t.setListener(TouchAnalizer.BehaviorType.SINGLE_DRAG, this);
        } else {
            this.t.setListener(TouchAnalizer.BehaviorType.DRAG, this);
            this.t.setListener(TouchAnalizer.BehaviorType.PINCH, this);
        }
        this.t.setListener(TouchAnalizer.BehaviorType.DOUBLE_CLICK, this);
        this.t.setListener(TouchAnalizer.BehaviorType.LONG_CLICK, this);
    }

    public void startAnimation(Animation animation) {
        startAnimation(animation, false);
    }

    public void startAnimation(Animation animation, boolean z) {
        this.anim = animation;
        if (this.anim != null) {
            this.anim.start();
            this.trans = new Transformation();
            this.matrixValue = new float[9];
            invalidate();
        }
        this.animFixCenter = z;
    }

    public void startAnimationAfterLayout(int[] iArr) {
        this.animAfterLayout = true;
        this.animAttrs = iArr;
    }

    public void setOriPicPos(int[] iArr) {
        this.animAttrs = iArr;
        this.animAfterLayout = false;
    }

    private void createAndStartAnimation() {
        int i;
        float f;
        if (this.animAttrs != null && this.animAttrs.length >= 4) {
            int[] iArr = new int[2];
            df.a(this, null, iArr);
            iArr[0] = 0;
            int[] iArr2 = this.animAttrs;
            float f2 = ((float) iArr2[2]) / ((float) this.mWidth);
            int i2 = (int) (((float) iArr2[3]) / f2);
            if (this.mBitmap != null) {
                i = (this.mBitmap.getHeight() * this.mWidth) / this.mBitmap.getWidth();
            } else if (this.mDrawable != null) {
                i = (this.mDrawable.getIntrinsicHeight() * this.mWidth) / this.mDrawable.getIntrinsicWidth();
            } else {
                i = i2;
            }
            ScaleAnimation scaleAnimation = new ScaleAnimation(f2, 1.0f, f2, 1.0f, 0.0f, 0.0f);
            scaleAnimation.setInterpolator(new DecelerateInterpolator());
            scaleAnimation.setDuration(300);
            float f3 = (float) (iArr2[0] - iArr[0]);
            float f4 = (float) (iArr2[1] - iArr[1]);
            if (this.mHeight > i) {
                f = (float) ((this.mHeight - i) / 2);
            } else {
                f = 0.0f;
            }
            TranslateAnimation translateAnimation = new TranslateAnimation(f3, 0.0f, f4, f);
            translateAnimation.initialize(this.mWidth, this.mHeight, this.mWidth, this.mHeight);
            translateAnimation.setInterpolator(new DecelerateInterpolator());
            translateAnimation.setDuration(300);
            AnimationSet animationSet = new AnimationSet(true);
            animationSet.addAnimation(scaleAnimation);
            animationSet.addAnimation(translateAnimation);
            if (this.startListener != null) {
                animationSet.setAnimationListener(this.startListener);
            }
            startAnimation(animationSet);
        } else if (this.mBitmap != null) {
            int height = (this.mBitmap.getHeight() * this.mWidth) / this.mBitmap.getWidth();
            this.positionY = this.mHeight > height ? (this.mHeight - height) / 2 : 0;
            invalidate();
        }
    }

    public boolean createBackAnimation(Animation.AnimationListener animationListener) {
        int i;
        float f;
        if (this.animAttrs != null && this.animAttrs.length >= 4) {
            int[] iArr = new int[2];
            df.a(this, null, iArr);
            iArr[0] = 0;
            int[] iArr2 = this.animAttrs;
            float f2 = ((float) iArr2[2]) / ((float) this.mWidth);
            int i2 = (int) (((float) iArr2[3]) / f2);
            if (this.mBitmap != null) {
                i = (this.mBitmap.getHeight() * this.mWidth) / this.mBitmap.getWidth();
            } else if (this.mDrawable != null) {
                i = (this.mDrawable.getIntrinsicHeight() * this.mWidth) / this.mDrawable.getIntrinsicWidth();
            } else {
                i = i2;
            }
            ScaleAnimation scaleAnimation = new ScaleAnimation(1.0f, f2, 1.0f, f2, 0.0f, 0.0f);
            scaleAnimation.setInterpolator(new DecelerateInterpolator());
            scaleAnimation.setDuration(300);
            float f3 = (float) (iArr2[0] - iArr[0]);
            if (this.mHeight > i) {
                f = (float) ((this.mHeight - i) / 2);
            } else {
                f = 0.0f;
            }
            TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, f3, f, (float) (iArr2[1] - iArr[1]));
            translateAnimation.initialize(this.mWidth, this.mHeight, this.mWidth, this.mHeight);
            translateAnimation.setInterpolator(new DecelerateInterpolator());
            translateAnimation.setDuration(300);
            AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.3f);
            alphaAnimation.setInterpolator(new LinearInterpolator());
            alphaAnimation.setStartOffset(150);
            alphaAnimation.setDuration(100);
            AnimationSet animationSet = new AnimationSet(true);
            animationSet.addAnimation(scaleAnimation);
            animationSet.addAnimation(translateAnimation);
            animationSet.addAnimation(alphaAnimation);
            if (animationListener != null) {
                animationSet.setAnimationListener(animationListener);
            }
            startAnimation(animationSet);
            return true;
        } else if (this.mBitmap == null) {
            return false;
        } else {
            int height = (this.mBitmap.getHeight() * this.mWidth) / this.mBitmap.getWidth();
            this.positionY = this.mHeight > height ? (this.mHeight - height) / 2 : 0;
            invalidate();
            return false;
        }
    }

    public void setImageBitmap(Bitmap bitmap) {
        this.mBitmap = bitmap;
        this.bControl = true;
        this.mDrawable = null;
        invalidate();
    }

    public Bitmap getBitmap() {
        return this.mBitmap;
    }

    public Drawable getDrawable() {
        return this.mDrawable;
    }

    public void setImageDrawable(Drawable drawable) {
        this.mDrawable = drawable;
        if (this.mDrawable != null) {
            this.mDrawable.setBounds(0, 0, getWidth(), getHeight());
            this.mDrawable.setCallback(this);
            if (drawable.isStateful()) {
                drawable.setState(getDrawableState());
            }
            this.mBitmap = null;
        }
        invalidate();
    }

    public void invalidate(int i, int i2, int i3, int i4) {
        super.invalidate();
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        this.mWidth = i3 - i;
        this.mHeight = i4 - i2;
        if (this.mDrawable != null) {
            this.mDrawable.setBounds(0, 0, i3 - i, i4 - i2);
        }
        if (this.animAfterLayout) {
            createAndStartAnimation();
            this.animAfterLayout = false;
        }
        tryDrag(0.0f, 0.0f);
    }

    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        boolean z = false;
        this.multiCtrl = false;
        this.dragRet = false;
        if (motionEvent.getAction() == 0) {
            this.dragging = false;
            this.longClicked = false;
            this.mHandler.removeMessages(0);
            if (this.mScale > 1.0f) {
                this.dragRet = true;
            }
        }
        this.t.inputTouchEvent(motionEvent);
        if (this.parent != null) {
            Shieldable shieldable = this.parent;
            if (this.dragRet || this.multiCtrl) {
                z = true;
            }
            shieldable.setShielded(z);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean verifyDrawable(Drawable drawable) {
        return this.mDrawable == drawable || super.verifyDrawable(drawable);
    }

    public void draw(Canvas canvas) {
        int i;
        int i2;
        int i3;
        int i4;
        boolean z;
        float f;
        int i5;
        int i6;
        boolean z2 = false;
        super.draw(canvas);
        int i7 = this.positionX;
        int i8 = this.positionY;
        if (this.mBitmap == null || !this.mBitmap.isRecycled()) {
            if (this.mBitmap != null) {
                i2 = this.mBitmap.getWidth();
                i = this.mBitmap.getHeight();
            } else if (this.mDrawable != null) {
                i2 = this.mDrawable.getIntrinsicWidth();
                i = this.mDrawable.getIntrinsicHeight();
            } else {
                i = 0;
                i2 = 0;
            }
            float f2 = ((float) this.mWidth) / ((float) (i2 > 0 ? i2 : this.mWidth));
            int i9 = (int) (((float) i) * f2 * this.mScale);
            int i10 = (int) (((float) i2) * f2 * this.mScale);
            if (i9 < this.mHeight) {
                i3 = (this.mHeight - i9) / 2;
            } else if (i9 < this.mHeight || this.positionY <= 0) {
                i3 = i8;
            } else {
                this.positionY = 0;
                i3 = 0;
            }
            float f3 = this.mScale;
            if (this.anim != null) {
                boolean transformation = this.anim.getTransformation(System.currentTimeMillis(), this.trans);
                this.trans.getMatrix().getValues(this.matrixValue);
                this.paint.setAlpha((int) (this.trans.getAlpha() * 255.0f));
                float f4 = this.matrixValue[0];
                int i11 = (int) this.matrixValue[2];
                int i12 = (int) this.matrixValue[5];
                if (this.animFixCenter) {
                    int i13 = (int) (((float) i) * f2 * f4);
                    if (i13 < this.mHeight) {
                        i12 = (this.mHeight - i13) / 2;
                    }
                    int i14 = (int) (((float) i2) * f2 * f4);
                    if (i11 > 0 || i14 <= this.mWidth) {
                        z = transformation;
                        int i15 = i12;
                        i5 = (this.mWidth - i14) / 2;
                        f = f4;
                        i3 = i15;
                    }
                }
                f = f4;
                i3 = i12;
                i5 = i11;
                z = transformation;
            } else {
                if (i7 > 0 || i10 <= this.mWidth) {
                    i4 = (this.mWidth - i10) / 2;
                } else {
                    i4 = i7;
                }
                this.paint.setAlpha(255);
                z = false;
                int i16 = i4;
                f = f3;
                i5 = i16;
            }
            int i17 = (int) (((float) this.mWidth) * f);
            int i18 = (int) (((float) i) * f2 * f);
            canvas.rotate((float) (((((double) (this.mRotate + this.mRotateTmp)) / 3.141592653589793d) * 180.0d) + 360.0d), this.rotateX, this.rotateY);
            if (this.mBitmap != null) {
                canvas.drawBitmap(this.mBitmap, new Rect(0, 0, this.mBitmap.getWidth(), this.mBitmap.getHeight()), new Rect(i5, i3, i17 + i5, i18 + i3), this.paint);
            } else if (this.mDrawable != null) {
                this.mDrawable.setBounds(i5, i3, i17 + i5, i18 + i3);
                this.mDrawable.draw(canvas);
            }
            this.positionX = i5;
            this.positionY = i3;
            if (this.shouldDrawScrollBar && (i17 > this.mWidth || i18 > this.mHeight)) {
                if (this.sbAnim != null) {
                    z2 = this.sbAnim.getTransformation(System.currentTimeMillis(), this.sbTrans);
                    i6 = (int) (this.sbTrans.getAlpha() * 255.0f);
                } else {
                    i6 = 255;
                }
                if (i17 > this.mWidth && this.hScrollBar != null) {
                    this.hScrollBar.setBounds(((-i5) * this.mWidth) / i17, this.mHeight - this.hScrollBar.getIntrinsicHeight(), (((-i5) + this.mWidth) * this.mWidth) / i17, this.mHeight);
                    this.hScrollBar.setAlpha(i6);
                    this.hScrollBar.draw(canvas);
                }
                if (i18 > this.mHeight && this.vScrollBar != null) {
                    this.vScrollBar.setBounds(this.mWidth - this.vScrollBar.getIntrinsicWidth(), ((-i3) * this.mHeight) / i18, this.mWidth, (((-i3) + this.mHeight) * this.mHeight) / i18);
                    this.vScrollBar.setAlpha(i6);
                    this.vScrollBar.draw(canvas);
                }
                z |= z2;
            }
            if (z) {
                invalidate();
            } else {
                this.anim = null;
            }
        }
    }

    public boolean onInvoke(TouchAnalizer.BehaviorType behaviorType, float f, float f2, int i) {
        if (!this.longClicked) {
            switch (b.f955a[behaviorType.ordinal()]) {
                case 1:
                    this.mHandler.sendEmptyMessageDelayed(0, CLICK_DELAY);
                    break;
                case 2:
                case 3:
                    if (this.bControl) {
                        if (!this.dragging) {
                            this.dragX = f;
                            this.dragY = f2;
                            this.dragging = true;
                        } else {
                            this.dragRet = tryDrag(f - this.dragX, f2 - this.dragY);
                            this.dragX = f;
                            this.dragY = f2;
                            if (!this.dragRet) {
                                this.dragging = false;
                            }
                        }
                        if (i == 2) {
                            this.dragging = false;
                            break;
                        }
                    }
                    break;
            }
        }
        return true;
    }

    private void showScrollBar() {
        this.shouldDrawScrollBar = true;
        this.mHandler.removeMessages(1);
        this.mHandler.sendEmptyMessageDelayed(1, 1000);
        invalidate();
    }

    /* access modifiers changed from: private */
    public void hideScrollBar() {
        this.sbAnim = new AlphaAnimation(1.0f, 0.0f);
        this.sbAnim.setDuration(800);
        this.sbAnim.start();
        this.sbAnim.setAnimationListener(this);
        if (this.sbTrans == null) {
            this.sbTrans = new Transformation();
        }
        invalidate();
    }

    public void onAnimationEnd(Animation animation) {
        this.shouldDrawScrollBar = false;
        this.sbAnim = null;
        invalidate();
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationStart(Animation animation) {
    }

    private boolean tryDrag(float f, float f2) {
        int i;
        int intrinsicHeight;
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4;
        boolean z5;
        boolean z6;
        boolean z7 = false;
        float sqrt = (float) Math.sqrt((double) ((f * f) + (f2 * f2)));
        if (f == 0.0f) {
            f = 1.0E-7f;
        }
        float atan = (float) Math.atan((double) (f2 / Math.abs(f)));
        if (f < 0.0f) {
            atan = (float) (3.141592653589793d - ((double) atan));
        }
        float cos = ((float) Math.cos((double) ((atan - this.mRotate) - this.mRotateTmp))) * sqrt;
        float sin = ((float) Math.sin((double) ((atan - this.mRotate) - this.mRotateTmp))) * sqrt;
        int i2 = this.mWidth;
        if (this.mBitmap != null) {
            float width = ((float) this.mWidth) / ((float) this.mBitmap.getWidth());
            i = (int) (((float) this.mWidth) * this.mScale);
            intrinsicHeight = (int) (width * ((float) this.mBitmap.getHeight()) * this.mScale);
        } else {
            if (this.mDrawable != null) {
                float intrinsicWidth = ((float) this.mWidth) / ((float) this.mDrawable.getIntrinsicWidth());
                i = (int) (((float) this.mWidth) * this.mScale);
                intrinsicHeight = (int) (intrinsicWidth * ((float) this.mDrawable.getIntrinsicHeight()) * this.mScale);
            }
            return z7;
        }
        if (i > this.mWidth) {
            if (((float) this.positionX) + cos > ((float) (this.mWidth - i))) {
                this.positionX = (int) (((float) this.positionX) + cos);
                z5 = false;
            } else {
                this.positionX = this.mWidth - i;
                z5 = true;
            }
            if (this.positionX > 0) {
                z6 = true;
            } else {
                z6 = false;
            }
            boolean z8 = z6 | z5;
            this.positionX = this.positionX > 0 ? 0 : this.positionX;
            z = z8;
        } else {
            this.positionX = (this.mWidth - i) / 2;
            z = true;
        }
        if (intrinsicHeight > this.mHeight) {
            if (((float) this.positionY) + sin > ((float) (this.mHeight - intrinsicHeight))) {
                this.positionY = (int) (((float) this.positionY) + sin);
                z3 = false;
            } else {
                this.positionY = this.mHeight - intrinsicHeight;
                z3 = true;
            }
            if (this.positionY > 0) {
                z4 = true;
            } else {
                z4 = false;
            }
            boolean z9 = z4 | z3;
            this.positionY = this.positionY > 0 ? 0 : this.positionY;
            z2 = z9;
        } else {
            this.positionY = (this.mHeight - intrinsicHeight) / 2;
            z2 = true;
        }
        if ((!z || !z2) && ((!z2 || Math.abs(sin) <= Math.abs(cos) * 2.0f) && (!z || Math.abs(cos) <= Math.abs(sin) * 2.0f))) {
            z7 = true;
        }
        if (z7) {
            showScrollBar();
        }
        invalidate();
        return z7;
    }

    public void setShieldableParent(Shieldable shieldable) {
        this.parent = shieldable;
    }

    public void sethScrollBar(Drawable drawable) {
        this.hScrollBar = drawable;
    }

    public void setvScrollBar(Drawable drawable) {
        this.vScrollBar = drawable;
    }

    public int getmWidth() {
        return this.mWidth;
    }

    public int getmHeight() {
        return this.mHeight;
    }

    public void setStartListener(Animation.AnimationListener animationListener) {
        this.startListener = animationListener;
    }
}
