package com.tencent.assistant.component;

import com.tencent.assistant.component.invalidater.ViewInvalidateMessage;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.callback.b;
import com.tencent.assistant.protocol.jce.TagGroup;
import java.util.HashMap;
import java.util.List;

/* compiled from: ProGuard */
class cp implements b {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RankFriendsListView f998a;

    cp(RankFriendsListView rankFriendsListView) {
        this.f998a = rankFriendsListView;
    }

    public void a(int i, int i2, boolean z, List<SimpleAppModel> list, List<TagGroup> list2) {
        if (this.f998a.scrolllistener != null) {
            ViewInvalidateMessage viewInvalidateMessage = new ViewInvalidateMessage(1, null, this.f998a.pageMessageHandler);
            viewInvalidateMessage.arg1 = i2;
            viewInvalidateMessage.arg2 = i;
            HashMap hashMap = new HashMap();
            hashMap.put("isFirstPage", Boolean.valueOf(z));
            hashMap.put("key_data", list);
            viewInvalidateMessage.params = hashMap;
            this.f998a.scrolllistener.sendMessage(viewInvalidateMessage);
        }
    }
}
