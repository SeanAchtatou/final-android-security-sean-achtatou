package com.tencent.assistant.component;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.pictureprocessor.DrawCallbackProgressBar;
import com.tencent.assistant.activity.pictureprocessor.a;
import com.tencent.assistant.manager.cq;
import com.tencent.assistant.thumbnailCache.k;
import com.tencent.assistant.thumbnailCache.o;
import com.tencent.assistant.thumbnailCache.p;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class PicView extends LinearLayout implements a, p {
    private static final int MSG_LOAD_PIC_CALCEL = 2;
    private static final int MSG_LOAD_PIC_FAIL = 1;
    private static final int MSG_LOAD_PIC_SUCCESS = 0;
    /* access modifiers changed from: private */
    public boolean animationFlag = false;
    /* access modifiers changed from: private */
    public OnPicViewListener animationListener;
    private Context context;
    private float downloadProgress = 90.0f;
    private float downloadProgressTemp = 50.0f;
    /* access modifiers changed from: private */
    public boolean failFlag = false;
    private AnimationImageView imageView;
    private boolean isDownloadSuc;
    private Bitmap mCont;
    private String mCurrentPicThumbnailUrl;
    private String mCurrentPicUrl;
    /* access modifiers changed from: private */
    public Handler mHandler = null;
    private LayoutInflater mInflater;
    private ArrayList<String> mReqUrls = new ArrayList<>();
    /* access modifiers changed from: private */
    public DrawCallbackProgressBar progressbar;
    /* access modifiers changed from: private */
    public o request = null;
    private Animation.AnimationListener startListener = new bq(this);
    /* access modifiers changed from: private */
    public TextView textview;

    /* compiled from: ProGuard */
    public interface OnPicViewListener {
        void onAnimationViewClick();
    }

    public PicView(Context context2) {
        super(context2);
        this.context = context2;
        init();
    }

    public PicView(Context context2, AttributeSet attributeSet) {
        super(context2, attributeSet);
        this.context = context2;
        init();
    }

    public void setAnimationListenr(OnPicViewListener onPicViewListener) {
        this.animationListener = onPicViewListener;
    }

    private void init() {
        this.mInflater = LayoutInflater.from(getContext());
        initLayout();
        createHandler();
    }

    private void createHandler() {
        this.mHandler = new bo(this);
    }

    /* access modifiers changed from: private */
    public void onPicLoadSuccess(o oVar) {
        if (oVar != null && oVar.c().equals(this.mCurrentPicUrl)) {
            Bitmap bitmap = oVar.f;
            this.mCont = bitmap;
            if (bitmap != null && !bitmap.isRecycled()) {
                this.isDownloadSuc = true;
                this.imageView.setVisibility(0);
                this.imageView.setImageBitmap(bitmap);
                this.progressbar.setVisibility(8);
                this.textview.setVisibility(8);
            }
        }
    }

    private void initLayout() {
        View inflate = this.mInflater.inflate((int) R.layout.show_pic_view_item, this);
        this.imageView = (AnimationImageView) inflate.findViewById(R.id.img);
        try {
            this.imageView.setvScrollBar(getResources().getDrawable(R.drawable.wb_scrollbar));
            this.imageView.sethScrollBar(getResources().getDrawable(R.drawable.wb_scrollbar_wide));
        } catch (Throwable th) {
            cq.a().b();
        }
        this.progressbar = (DrawCallbackProgressBar) inflate.findViewById(R.id.img_progress);
        this.progressbar.a(this);
        this.textview = (TextView) inflate.findViewById(R.id.txt_progress);
        this.imageView.setOnClickListener(new bp(this));
        this.imageView.setStartListener(this.startListener);
        this.textview.setText(getResources().getString(R.string.is_connecting));
    }

    public void doRefresh() {
    }

    public void setPicInf(String str) {
        setPicInf(str, null);
    }

    public void setPicInf(String str, OnPicViewListener onPicViewListener) {
        if (onPicViewListener != null) {
            this.animationListener = onPicViewListener;
        }
        if (str != null) {
            this.mCurrentPicUrl = str;
            loadPic(str, null, 2);
        }
    }

    public void setPicInf(String str, String str2, OnPicViewListener onPicViewListener) {
        if (onPicViewListener != null) {
            this.animationListener = onPicViewListener;
        }
        if (str != null) {
            this.mCurrentPicUrl = str;
            this.mCurrentPicThumbnailUrl = str2;
            loadPic(str, str2, 2);
        }
    }

    public String getUrl() {
        return this.mCurrentPicUrl;
    }

    public void recycle() {
    }

    public void release() {
        recycle();
    }

    public boolean isDownloadSuc() {
        return this.isDownloadSuc;
    }

    private void loadPic(String str, String str2, int i) {
        Bitmap a2;
        this.mCont = k.b().a(str, i, this);
        if (this.mCont != null) {
            this.isDownloadSuc = true;
            this.imageView.setImageBitmap(this.mCont);
            this.progressbar.setVisibility(8);
            this.textview.setVisibility(8);
            return;
        }
        this.mReqUrls.add(str);
        this.progressbar.setVisibility(0);
        this.textview.setVisibility(0);
        if (str2 == null || (a2 = k.b().a(str2, i, null)) == null) {
            try {
                this.imageView.setImageBitmap(BitmapFactory.decodeResource(getContext().getResources(), R.drawable.pic_defaule));
            } catch (Throwable th) {
                cq.a().b();
            }
        } else {
            this.imageView.setImageBitmap(a2);
        }
    }

    private Bitmap getShortCutBitmap(int i) {
        if (this.mCurrentPicUrl == null) {
            return null;
        }
        if (this.mCont != null) {
            return this.mCont;
        }
        return BitmapFactory.decodeResource(getContext().getResources(), R.drawable.pic_defaule);
    }

    public void onLoadPicFail() {
        this.imageView.setImageBitmap(getErrorBitmap(this.imageView.getmWidth(), this.imageView.getmHeight(), R.drawable.appdetail_picbreak));
        this.progressbar.setVisibility(8);
        this.textview.setVisibility(8);
    }

    public void downloading(o oVar) {
    }

    public void onDraw() {
        float f = 99.99f;
        if (this.downloadProgressTemp >= 0.0f && this.downloadProgress >= 0.0f) {
            float random = (float) (Math.random() * 0.57d);
            if (this.downloadProgressTemp + random < this.downloadProgress + 10.0f && this.downloadProgressTemp < 100.0f) {
                this.downloadProgressTemp = random + this.downloadProgressTemp;
            }
            if (this.downloadProgressTemp < 99.99f) {
                f = this.downloadProgressTemp;
            }
            this.downloadProgressTemp = f;
            this.textview.setText(String.format("%.1f", Float.valueOf(this.downloadProgressTemp)) + "%");
        }
    }

    public void startScaleAnim(int[] iArr) {
        this.animationFlag = true;
        this.imageView.startAnimationAfterLayout(iArr);
    }

    public void setShilble(Shieldable shieldable) {
        this.imageView.setShieldableParent(shieldable);
    }

    public void setOriPicPos(int[] iArr) {
        this.imageView.setOriPicPos(iArr);
    }

    public boolean animOut(Animation.AnimationListener animationListener2) {
        if (this.imageView != null) {
            return this.imageView.createBackAnimation(animationListener2);
        }
        return false;
    }

    public void thumbnailRequestStarted(o oVar) {
    }

    public void thumbnailRequestCompleted(o oVar) {
        if (this.mReqUrls.contains(oVar.c())) {
            if (this.animationFlag) {
                this.request = oVar;
            } else {
                this.mHandler.obtainMessage(0, oVar).sendToTarget();
            }
        }
    }

    public void thumbnailRequestCancelled(o oVar) {
        this.mHandler.obtainMessage(2, oVar).sendToTarget();
    }

    public void thumbnailRequestFailed(o oVar) {
        if (this.mReqUrls.contains(oVar.c())) {
            if (this.animationFlag) {
                this.failFlag = true;
            } else {
                this.mHandler.sendEmptyMessageDelayed(1, 0);
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0030  */
    /* JADX WARNING: Removed duplicated region for block: B:26:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private android.graphics.Bitmap getErrorBitmap(int r11, int r12, int r13) {
        /*
            r10 = this;
            r8 = 1
            r2 = 0
            r0 = 0
            if (r11 <= 0) goto L_0x0007
            if (r12 > 0) goto L_0x0008
        L_0x0007:
            return r0
        L_0x0008:
            android.content.Context r1 = r10.getContext()     // Catch:{ Throwable -> 0x0093 }
            android.content.res.Resources r1 = r1.getResources()     // Catch:{ Throwable -> 0x0093 }
            android.graphics.Bitmap r4 = android.graphics.BitmapFactory.decodeResource(r1, r13)     // Catch:{ Throwable -> 0x0093 }
            if (r4 == 0) goto L_0x0007
            int r3 = r4.getWidth()     // Catch:{ Throwable -> 0x00a3 }
            int r1 = r4.getHeight()     // Catch:{ Throwable -> 0x00a7 }
            int r5 = r11 / r3
            int r5 = r5 * r1
            int r12 = r5 + 100
            android.graphics.Bitmap$Config r5 = android.graphics.Bitmap.Config.ARGB_8888     // Catch:{ Throwable -> 0x00aa }
            android.graphics.Bitmap r5 = android.graphics.Bitmap.createBitmap(r11, r12, r5)     // Catch:{ Throwable -> 0x00aa }
            r9 = r1
            r1 = r5
            r5 = r4
            r4 = r3
            r3 = r9
        L_0x002e:
            if (r1 == 0) goto L_0x0007
            android.graphics.Canvas r0 = new android.graphics.Canvas
            r0.<init>(r1)
            android.graphics.Paint r6 = new android.graphics.Paint
            r6.<init>(r8)
            int r4 = r11 - r4
            int r4 = r4 / 2
            float r4 = (float) r4
            int r7 = r12 - r3
            int r7 = r7 + -100
            int r7 = r7 / 2
            float r7 = (float) r7
            r0.drawBitmap(r5, r4, r7, r6)
            android.graphics.Paint r4 = new android.graphics.Paint
            r4.<init>(r8)
            android.graphics.Paint$Align r5 = android.graphics.Paint.Align.CENTER
            r4.setTextAlign(r5)
            r5 = 1108082688(0x420c0000, float:35.0)
            r4.setTextSize(r5)
            r5 = -1
            r4.setColor(r5)
            android.content.res.Resources r5 = r10.getResources()
            r6 = 2131362302(0x7f0a01fe, float:1.834438E38)
            java.lang.String r5 = r5.getString(r6)
            android.graphics.Rect r6 = new android.graphics.Rect
            r6.<init>()
            int r7 = r5.length()
            r4.getTextBounds(r5, r2, r7, r6)
            int r2 = r6.bottom
            int r6 = r6.top
            int r2 = r2 - r6
            int r6 = r11 / 2
            float r6 = (float) r6
            int r3 = r3 + r12
            int r3 = r3 + 100
            int r3 = r3 / 2
            int r2 = r2 / 2
            int r2 = r3 - r2
            float r2 = (float) r2
            r0.drawText(r5, r6, r2, r4)
            r2 = 31
            r0.save(r2)
            r0.restore()
            r0 = r1
            goto L_0x0007
        L_0x0093:
            r1 = move-exception
            r1 = r2
            r3 = r2
            r4 = r0
        L_0x0097:
            com.tencent.assistant.manager.cq r5 = com.tencent.assistant.manager.cq.a()
            r5.b()
            r5 = r4
            r4 = r3
            r3 = r1
            r1 = r0
            goto L_0x002e
        L_0x00a3:
            r1 = move-exception
            r1 = r2
            r3 = r2
            goto L_0x0097
        L_0x00a7:
            r1 = move-exception
            r1 = r2
            goto L_0x0097
        L_0x00aa:
            r5 = move-exception
            goto L_0x0097
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.component.PicView.getErrorBitmap(int, int, int):android.graphics.Bitmap");
    }
}
