package com.tencent.assistant.component.smartcard;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.a.i;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.b.d;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.STPageInfo;
import com.tencent.assistantv2.st.page.a;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public abstract class NormalSmartcardBaseItem extends ISmartcard {
    protected View.OnClickListener g = new n(this);
    protected View.OnClickListener h = new o(this);

    public NormalSmartcardBaseItem(Context context) {
        super(context);
    }

    public NormalSmartcardBaseItem(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public NormalSmartcardBaseItem(Context context, i iVar, SmartcardListener smartcardListener, IViewInvalidater iViewInvalidater) {
        super(context, iVar, smartcardListener, iViewInvalidater);
        a();
    }

    /* access modifiers changed from: protected */
    public SimpleAppModel c() {
        return null;
    }

    /* access modifiers changed from: protected */
    public void d() {
        if (this.smartcardListener != null) {
            this.smartcardListener.onSmartcardExposure(this.smartcardModel.i, this.smartcardModel.j);
        }
        SimpleAppModel c = c();
        long j = -1;
        if (c != null) {
            j = c.f1634a;
        }
        a(STConst.ST_DEFAULT_SLOT, 100, this.smartcardModel.r, j);
    }

    /* access modifiers changed from: protected */
    public void a(SimpleAppModel simpleAppModel, STInfoV2 sTInfoV2) {
        if (this.smartcardListener != null) {
            this.smartcardListener.onSmartcardContentAppExposure(this.smartcardModel.i, this.smartcardModel.j, simpleAppModel);
        }
        if (this.e == null) {
            this.e = new d();
        }
        this.e.a(sTInfoV2);
    }

    /* access modifiers changed from: protected */
    public int a(int i) {
        int i2 = 0;
        switch (i) {
            case STConst.ST_PAGE_COMPETITIVE /*2001*/:
                i2 = 1;
                break;
            case 200501:
                i2 = 3;
                break;
            case STConst.ST_PAGE_GAME_POPULAR /*200601*/:
                i2 = 2;
                break;
        }
        if (this.smartcardModel == null) {
            return STConst.ST_PAGE_SMARTCARD;
        }
        return (i2 * 100) + 20290000 + this.smartcardModel.i;
    }

    /* access modifiers changed from: protected */
    public String e() {
        if (this.smartcardModel == null) {
            return STConst.ST_DEFAULT_SLOT;
        }
        return this.smartcardModel.i + Constants.STR_EMPTY;
    }

    /* access modifiers changed from: protected */
    public String b(int i) {
        return this.smartcardModel.j + "||" + this.smartcardModel.i + "|" + i;
    }

    /* access modifiers changed from: protected */
    public void a(String str, int i, byte[] bArr, long j) {
        STInfoV2 b = b(str, i, bArr, j);
        if (b != null) {
            k.a(b);
        }
    }

    /* access modifiers changed from: protected */
    public STInfoV2 b(String str, int i, byte[] bArr, long j) {
        STInfoV2 a2 = a(str, i);
        if (a2 != null) {
            if (this.smartcardModel != null) {
                a2.pushInfo = b(0);
            }
            if (j > 0) {
                a2.appId = j;
            }
            if (a2.recommendId == null || a2.recommendId.length == 0) {
                a2.recommendId = bArr;
            }
        }
        return a2;
    }

    /* access modifiers changed from: protected */
    public STInfoV2 a(String str, int i) {
        STPageInfo sTPageInfo;
        String str2;
        if (this.f1115a instanceof BaseActivity) {
            sTPageInfo = ((BaseActivity) this.f1115a).q();
        } else {
            sTPageInfo = null;
        }
        if (sTPageInfo == null) {
            return null;
        }
        String str3 = sTPageInfo.b;
        if (this.smartcardModel.t >= 0) {
            str2 = (this.smartcardModel.t < 9 ? "0" : Constants.STR_EMPTY) + String.valueOf(this.smartcardModel.t + 1);
        } else {
            str2 = str3;
        }
        STInfoV2 sTInfoV2 = new STInfoV2(a(sTPageInfo.f3358a), str, sTPageInfo.f3358a, a.b(str2, STConst.ST_STATUS_DEFAULT), i);
        sTInfoV2.pushInfo = b(0);
        sTInfoV2.updateWithSimpleAppModel(c());
        return sTInfoV2;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!this.hasInit) {
            this.hasInit = true;
            d();
        }
    }

    public int contextToPageId(Context context) {
        STPageInfo q;
        if (!(context instanceof BaseActivity) || (q = ((BaseActivity) context).q()) == null) {
            return 0;
        }
        return q.f3358a;
    }
}
