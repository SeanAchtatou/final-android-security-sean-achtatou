package com.tencent.assistant.component;

import com.tencent.assistant.component.invalidater.ViewInvalidateMessage;
import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.utils.ba;
import java.util.HashMap;

/* compiled from: ProGuard */
class ay extends ApkResCallback.Stub {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ LocalPkgSizeTextView f953a;

    private ay(LocalPkgSizeTextView localPkgSizeTextView) {
        this.f953a = localPkgSizeTextView;
    }

    /* synthetic */ ay(LocalPkgSizeTextView localPkgSizeTextView, ax axVar) {
        this(localPkgSizeTextView);
    }

    public void onGetPkgSizeFinish(LocalApkInfo localApkInfo) {
        if (this.f953a.mPkgName != null && localApkInfo.mPackageName != null) {
            if (this.f953a.invalidater != null) {
                ViewInvalidateMessage viewInvalidateMessage = new ViewInvalidateMessage(0);
                HashMap hashMap = new HashMap();
                hashMap.put("NAME", localApkInfo.mPackageName);
                hashMap.put("SIZE", Long.valueOf(localApkInfo.occupySize));
                viewInvalidateMessage.params = hashMap;
                viewInvalidateMessage.target = this.f953a.invalidateHandler;
                this.f953a.invalidater.dispatchMessage(viewInvalidateMessage);
                return;
            }
            String str = localApkInfo.mPackageName;
            long j = localApkInfo.occupySize;
            if (this.f953a.mPkgName.equals(localApkInfo.mPackageName)) {
                ba.a().post(new az(this, str, j));
            }
        }
    }
}
