package com.tencent.assistant.component;

import android.view.View;

/* compiled from: ProGuard */
class s implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CommentDetailView f1106a;

    s(CommentDetailView commentDetailView) {
        this.f1106a = commentDetailView;
    }

    public void onClick(View view) {
        if (!this.f1106a.isLoadingFirstPageData && this.f1106a.engine != null && this.f1106a.simpleAppModel != null) {
            this.f1106a.refreshFirstPageData(null);
        }
    }
}
