package com.tencent.assistant.component;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
class ad implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CommentHeaderTagView f882a;

    ad(CommentHeaderTagView commentHeaderTagView) {
        this.f882a = commentHeaderTagView;
    }

    public void onClick(View view) {
        int childCount = this.f882a.taglistlayout.getChildCount();
        for (int i = 0; i < childCount; i++) {
            RelativeLayout relativeLayout = (RelativeLayout) this.f882a.taglistlayout.getChildAt(i);
            View findViewById = relativeLayout.findViewById(R.id.commenttag_selectimg_id);
            if (findViewById != null) {
                relativeLayout.removeView(findViewById);
            }
        }
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(7, view.getId());
        layoutParams.addRule(8, view.getId());
        ImageView imageView = new ImageView(this.f882a.mContext);
        imageView.setImageResource(R.drawable.pinglun_icon_on);
        imageView.setId(R.id.commenttag_selectimg_id);
        ((RelativeLayout) view.getParent()).addView(imageView, layoutParams);
        if (this.f882a.mClickListener != null) {
            this.f882a.mClickListener.a(view, null);
        }
    }
}
