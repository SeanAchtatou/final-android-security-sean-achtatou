package com.tencent.assistant.component.smartcard;

import android.content.Intent;
import android.view.View;
import com.tencent.assistant.component.appdetail.CommentDetailTabView;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistantv2.activity.AppDetailActivityV5;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class ak extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ STInfoV2 f1136a;
    final /* synthetic */ SmartSquareAppItem b;

    ak(SmartSquareAppItem smartSquareAppItem, STInfoV2 sTInfoV2) {
        this.b = smartSquareAppItem;
        this.f1136a = sTInfoV2;
    }

    public void onTMAClick(View view) {
        Intent intent = new Intent(this.b.f1120a, AppDetailActivityV5.class);
        if (this.f1136a != null) {
            intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, this.f1136a.scene);
        }
        intent.putExtra(CommentDetailTabView.PARAMS_SIMPLE_MODEL_INFO, this.b.i.f1653a);
        this.b.f1120a.startActivity(intent);
    }

    public STInfoV2 getStInfo() {
        if (this.f1136a != null) {
            this.f1136a.actionId = 200;
            this.f1136a.updateStatusToDetail(this.b.i.f1653a);
        }
        return this.f1136a;
    }
}
