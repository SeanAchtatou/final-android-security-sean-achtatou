package com.tencent.assistant.component;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.appdetail.AppdetailFloatingDialog;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.c;
import com.tencent.assistant.utils.ck;
import com.tencent.assistant.utils.df;
import com.tencent.assistantv2.st.model.StatInfo;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class SecondNavigationTitleView extends LinearLayout {
    public static final ArrayList<String> SPECIAL_MODEL = new ArrayList<>();
    public static final String TMA_ST_NAVBAR_HOME_TAG = "10_001";
    public static final String TMA_ST_NAVBAR_SEARCH_TAG = "01_001";
    public static final String TMA_ST_NAVBAR_SHARE_TAG = "03_001";
    /* access modifiers changed from: private */
    public c appModel;
    private ImageView backImg;
    private RelativeLayout backLayout;
    private ImageView barSplit;
    private ImageView barSplit2;
    private RelativeLayout containerLayout;
    /* access modifiers changed from: private */
    public Context context;
    private View.OnClickListener defaultClickListener = new cx(this);
    /* access modifiers changed from: private */
    public AppdetailFloatingDialog dialog;
    private DownloadProgressButton downLayout;
    private View.OnClickListener feedbackDefaultListener = new da(this);
    private View feedbackIcon;
    private LinearLayout feedbackLayout;
    /* access modifiers changed from: private */
    public boolean firstShowFloatFlag = true;
    private LinearLayout homeLayout;
    private View.OnClickListener homeLayoutDefailtListener = new cz(this);
    /* access modifiers changed from: private */
    public Activity hostActivity = null;
    private ImageView iv_home;
    /* access modifiers changed from: private */
    public ImageView iv_red_dot;
    private ImageView mainIcon;
    private View.OnClickListener rightDefaultListener = new cy(this);
    private ImageView rightImg;
    private ImageView rightImgExt;
    private LinearLayout rightLayout;
    private LinearLayout rightLayoutExt;
    /* access modifiers changed from: private */
    public LinearLayout showMore;
    private LinearLayout showShare;
    /* access modifiers changed from: private */
    public SimpleAppModel simpleAppModel;
    private StatInfo statInfo;
    private LinearLayout titleLayout;
    private TextView titleNumTxt;
    private TextView titleTxt;

    static {
        SPECIAL_MODEL.add("MI-ONE Plus");
    }

    public SecondNavigationTitleView(Context context2, AttributeSet attributeSet) {
        super(context2, attributeSet);
        this.context = context2;
        init();
    }

    public SecondNavigationTitleView(Context context2) {
        super(context2);
        this.context = context2;
        init();
    }

    private void init() {
        View inflate = LayoutInflater.from(this.context).inflate((int) R.layout.second_navigation_layout, this);
        this.containerLayout = (RelativeLayout) inflate.findViewById(R.id.title_container_layout);
        if (SPECIAL_MODEL.contains(Build.MODEL) && this.containerLayout != null) {
            this.containerLayout.setBackgroundResource(R.color.normal_color_bar);
        }
        this.backLayout = (RelativeLayout) inflate.findViewById(R.id.back_layout);
        this.backLayout.setOnClickListener(this.defaultClickListener);
        this.backImg = (ImageView) inflate.findViewById(R.id.backImg);
        this.rightImg = (ImageView) inflate.findViewById(R.id.right_img);
        this.titleTxt = (TextView) inflate.findViewById(R.id.title_txt);
        this.titleNumTxt = (TextView) inflate.findViewById(R.id.title_num_txt);
        this.titleLayout = (LinearLayout) inflate.findViewById(R.id.title_layout);
        this.rightLayout = (LinearLayout) findViewById(R.id.right_layout);
        this.rightLayout.setVisibility(0);
        this.rightLayout.setTag(R.id.tma_st_slot_tag, TMA_ST_NAVBAR_SEARCH_TAG);
        this.rightLayout.setOnClickListener(this.rightDefaultListener);
        this.rightLayoutExt = (LinearLayout) findViewById(R.id.right_layout_ext);
        this.rightImgExt = (ImageView) findViewById(R.id.right_img_ext);
        this.rightLayoutExt.setVisibility(8);
        this.rightLayoutExt.setOnClickListener(this.rightDefaultListener);
        this.mainIcon = (ImageView) findViewById(R.id.main_icon);
        this.feedbackLayout = (LinearLayout) findViewById(R.id.feedback_layout);
        this.feedbackLayout.setOnClickListener(this.feedbackDefaultListener);
        this.feedbackIcon = findViewById(R.id.feedback_icon);
        this.homeLayout = (LinearLayout) findViewById(R.id.home_layout);
        this.homeLayout.setTag(R.id.tma_st_slot_tag, TMA_ST_NAVBAR_HOME_TAG);
        this.homeLayout.setOnClickListener(this.homeLayoutDefailtListener);
        this.iv_home = (ImageView) findViewById(R.id.iv_home);
        this.downLayout = (DownloadProgressButton) findViewById(R.id.down_layout);
        this.barSplit = (ImageView) findViewById(R.id.bar_split);
        this.barSplit2 = (ImageView) findViewById(R.id.bar_split2);
        this.iv_red_dot = (ImageView) findViewById(R.id.iv_red_dot);
        initDialog();
        this.showShare = (LinearLayout) findViewById(R.id.show_share);
        this.showMore = (LinearLayout) findViewById(R.id.show_more);
        this.showMore.setOnClickListener(new cw(this));
    }

    public void firstFloatingShow() {
        if (!ck.a() && this.iv_red_dot != null) {
            this.iv_red_dot.setVisibility(0);
        }
    }

    private void initDialog() {
        this.dialog = new AppdetailFloatingDialog(this.context, R.style.notDimdialog);
        Window window = this.dialog.getWindow();
        WindowManager.LayoutParams attributes = window.getAttributes();
        attributes.gravity = 49;
        attributes.y = 250;
        Display defaultDisplay = this.dialog.getWindow().getWindowManager().getDefaultDisplay();
        attributes.height = (int) (((double) defaultDisplay.getHeight()) * 0.5d);
        attributes.width = (int) (((double) defaultDisplay.getWidth()) * 0.95d);
        window.setAttributes(attributes);
        this.dialog.setCanceledOnTouchOutside(true);
    }

    public void hiddeSearch() {
        if (this.rightLayout != null) {
            this.rightLayout.setVisibility(8);
        }
    }

    public void hideSort() {
        if (this.rightLayoutExt != null) {
            this.rightLayoutExt.setVisibility(8);
        }
    }

    public void showSort() {
        if (this.rightLayoutExt != null) {
            this.rightLayoutExt.setVisibility(0);
        }
    }

    public void showMoreLayout() {
        if (this.showMore != null) {
            this.showMore.setVisibility(0);
        }
    }

    public void setButtonVisiable(boolean z) {
        if (this.showShare != null) {
            this.showShare.setVisibility(z ? 0 : 8);
        }
    }

    public void setButtonEnable(boolean z) {
        if (this.showShare != null) {
            this.showShare.setEnabled(z);
        }
    }

    public void disableShareArea() {
        if (this.dialog != null) {
            this.dialog.disableShareArea();
        }
    }

    public void setHomeSrc(int i) {
        if (this.iv_home != null) {
            this.iv_home.setImageResource(i);
            ViewGroup.LayoutParams layoutParams = this.iv_home.getLayoutParams();
            layoutParams.height = df.a(this.context, 52.0f);
            layoutParams.width = df.a(this.context, 52.0f);
            this.iv_home.setLayoutParams(layoutParams);
        }
    }

    public void setButtonOnClickListener(View.OnClickListener onClickListener) {
        if (this.showShare != null) {
            this.showShare.setOnClickListener(onClickListener);
        }
    }

    public void setHomeClickListener(View.OnClickListener onClickListener) {
        if (this.homeLayout != null) {
            this.homeLayout.setOnClickListener(onClickListener);
        }
    }

    public void setBackClickListener(View.OnClickListener onClickListener) {
        if (this.backLayout != null) {
            this.backLayout.setOnClickListener(onClickListener);
        }
    }

    public void showUserCare(boolean z) {
        boolean z2 = !ck.b();
        this.dialog.shwoUserCare(z);
        if (z && z2 && this.iv_red_dot != null) {
            this.iv_red_dot.setVisibility(0);
        }
    }

    public void showHomeLayout() {
        if (this.homeLayout != null) {
            this.homeLayout.setVisibility(0);
            this.rightLayout.setVisibility(8);
        }
        if (this.barSplit2 != null) {
            this.barSplit2.setVisibility(0);
        }
    }

    public void showBarSplit() {
        if (this.barSplit2 != null) {
            this.barSplit2.setVisibility(0);
        }
    }

    public void showDownArrowBar() {
        if (this.rightLayout.getVisibility() == 0) {
            this.barSplit.setVisibility(0);
        }
        this.downLayout.setVisibility(0);
        this.downLayout.onResume();
    }

    public void hideDownArrowBar() {
        if (this.downLayout.getVisibility() == 0) {
            this.downLayout.setVisibility(8);
        }
    }

    public void onResume() {
        if (this.downLayout.getVisibility() == 0) {
            this.downLayout.onResume();
        }
    }

    public void onPause() {
        this.downLayout.onPause();
    }

    public void setTitle(String str) {
        if (this.titleTxt != null && !TextUtils.isEmpty(str)) {
            this.titleTxt.setText(str);
        }
    }

    public void setTitle(String str, TextUtils.TruncateAt truncateAt) {
        if (this.titleTxt != null && !TextUtils.isEmpty(str)) {
            this.titleTxt.setEllipsize(truncateAt);
            this.titleTxt.setText(str);
        }
    }

    public void setNumTitle(String str) {
        if (this.titleNumTxt == null) {
            return;
        }
        if (!TextUtils.isEmpty(str)) {
            this.titleNumTxt.setText(str);
            this.titleNumTxt.setVisibility(0);
            return;
        }
        this.titleNumTxt.setVisibility(8);
    }

    public void setLeftButtonImg(Drawable drawable) {
        if (this.backImg != null && drawable != null) {
            this.backImg.setBackgroundDrawable(drawable);
        }
    }

    public void setRightButtonImg(Drawable drawable) {
        if (this.rightImg != null && drawable != null) {
            this.rightImg.setImageDrawable(drawable);
        }
    }

    public void setRightButtonExtImg(Drawable drawable) {
        if (this.rightImgExt != null && drawable != null) {
            this.rightImgExt.setImageDrawable(drawable);
        }
    }

    public void setRightButtonClickListener(View.OnClickListener onClickListener) {
        if (this.rightLayout != null) {
            this.rightLayout.setOnClickListener(onClickListener);
        }
    }

    public void setRightExtButtonClickListener(View.OnClickListener onClickListener) {
        if (this.rightLayoutExt != null) {
            this.rightLayoutExt.setOnClickListener(onClickListener);
        }
    }

    public void setLeftButtonClickListener(View.OnClickListener onClickListener) {
        if (this.backLayout != null) {
            this.backLayout.setOnClickListener(onClickListener);
        }
    }

    public void setActivityContext(Activity activity) {
        this.hostActivity = activity;
    }

    public void isFirstLevelNavigation(boolean z) {
        if (z) {
            this.mainIcon.setVisibility(0);
            this.backImg.setVisibility(8);
            this.backLayout.setOnClickListener(null);
            this.backLayout.setClickable(false);
            showDownArrowBar();
            return;
        }
        this.mainIcon.setVisibility(8);
        this.backLayout.setVisibility(0);
        this.backImg.setVisibility(0);
        this.backLayout.setOnClickListener(this.defaultClickListener);
        this.backLayout.setClickable(true);
    }

    public void isNeedFeedbackOnNavigation(boolean z) {
        if (z) {
            this.feedbackLayout.setVisibility(0);
        } else {
            this.feedbackLayout.setVisibility(8);
        }
    }

    public void hasNewCallback(boolean z) {
        if (z) {
            this.feedbackIcon.setVisibility(0);
        } else {
            this.feedbackIcon.setVisibility(8);
        }
    }

    public void setCustomLeftDefaultListener(View.OnClickListener onClickListener) {
        if (this.backLayout != null) {
            this.backLayout.setOnClickListener(onClickListener);
        }
    }

    public void setFloatingWindowListener(AppdetailFloatingDialog.IOnFloatViewListener iOnFloatViewListener) {
        this.dialog.setListener(iOnFloatViewListener);
    }

    public void setData(SimpleAppModel simpleAppModel2, StatInfo statInfo2, c cVar) {
        if (checkSimpleModel(simpleAppModel2)) {
            this.simpleAppModel = simpleAppModel2;
            this.statInfo = statInfo2;
            this.appModel = cVar;
            if (this.dialog != null && this.dialog.isShowing()) {
                this.dialog.refreshState(cVar.f1660a.f, this.simpleAppModel.f1634a);
            }
        }
    }

    private boolean checkSimpleModel(SimpleAppModel simpleAppModel2) {
        if (simpleAppModel2 != null && !TextUtils.isEmpty(simpleAppModel2.d) && !TextUtils.isEmpty(simpleAppModel2.i) && simpleAppModel2.p >= 0) {
            return true;
        }
        return false;
    }

    public AppdetailFloatingDialog getFloatingDialog() {
        return this.dialog;
    }

    public LinearLayout getShareLayout() {
        return this.showShare;
    }
}
