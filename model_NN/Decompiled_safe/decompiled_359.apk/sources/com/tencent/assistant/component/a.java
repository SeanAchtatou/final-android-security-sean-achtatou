package com.tencent.assistant.component;

import android.os.Handler;
import android.os.Message;

/* compiled from: ProGuard */
class a extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AnimationImageView f878a;

    a(AnimationImageView animationImageView) {
        this.f878a = animationImageView;
    }

    public void handleMessage(Message message) {
        if (message.what == 0) {
            if (this.f878a.onClickListener != null) {
                this.f878a.onClickListener.onClick(this.f878a);
            }
        } else if (message.what == 1) {
            this.f878a.hideScrollBar();
        }
    }
}
