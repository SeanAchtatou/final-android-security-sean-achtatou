package com.tencent.assistant.component;

import android.content.Intent;
import android.view.View;
import com.tencent.assistant.activity.HelperFeedbackActivity;

/* compiled from: ProGuard */
class da implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SecondNavigationTitleView f1010a;

    da(SecondNavigationTitleView secondNavigationTitleView) {
        this.f1010a = secondNavigationTitleView;
    }

    public void onClick(View view) {
        this.f1010a.context.startActivity(new Intent(this.f1010a.context, HelperFeedbackActivity.class));
    }
}
