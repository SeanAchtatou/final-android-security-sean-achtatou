package com.tencent.assistant.component;

import android.view.View;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class by implements View.OnFocusChangeListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PopViewDialog f980a;

    by(PopViewDialog popViewDialog) {
        this.f980a = popViewDialog;
    }

    public void onFocusChange(View view, boolean z) {
        STInfoV2 access$200 = this.f980a.buildFloatingSTInfo();
        if (access$200 != null && z) {
            access$200.slotId = a.a(this.f980a.getColumn(), "002");
            access$200.actionId = 200;
            k.a(access$200);
        }
    }
}
