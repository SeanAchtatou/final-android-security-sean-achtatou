package com.tencent.assistant.component;

import com.tencent.assistant.component.SideBar;

/* compiled from: ProGuard */
class cg implements SideBar.OnTouchingLetterChangedListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PreInstallAppListView f989a;

    cg(PreInstallAppListView preInstallAppListView) {
        this.f989a = preInstallAppListView;
    }

    public void onTouchingLetterChanged(String str) {
        int a2 = this.f989a.mAdapter.a(str.charAt(0));
        if (a2 != -1) {
            this.f989a.mListView.post(new ch(this, a2));
        }
    }
}
