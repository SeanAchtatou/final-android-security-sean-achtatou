package com.tencent.assistant.component;

import com.tencent.assistant.m;
import com.tencent.assistant.model.QuickEntranceNotify;
import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: ProGuard */
class cm implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ArrayList f995a;
    final /* synthetic */ QuickEntranceView b;

    cm(QuickEntranceView quickEntranceView, ArrayList arrayList) {
        this.b = quickEntranceView;
        this.f995a = arrayList;
    }

    public void run() {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
            Iterator it = this.f995a.iterator();
            while (it.hasNext()) {
                objectOutputStream.writeObject((QuickEntranceNotify) it.next());
            }
            m.a().g(byteArrayOutputStream.toByteArray());
            objectOutputStream.close();
            byteArrayOutputStream.close();
        } catch (Exception e) {
        }
    }
}
