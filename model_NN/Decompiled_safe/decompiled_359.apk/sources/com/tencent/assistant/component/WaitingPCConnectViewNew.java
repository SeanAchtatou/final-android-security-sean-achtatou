package com.tencent.assistant.component;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
public class WaitingPCConnectViewNew extends LinearLayout {
    private Context context;
    private TextView pcName;

    public WaitingPCConnectViewNew(Context context2) {
        super(context2);
        this.context = context2;
        init();
    }

    public WaitingPCConnectViewNew(Context context2, AttributeSet attributeSet) {
        super(context2, attributeSet);
        this.context = context2;
        init();
    }

    private void init() {
        this.pcName = (TextView) LayoutInflater.from(this.context).inflate((int) R.layout.waiting_pc_new, this).findViewById(R.id.pcName);
    }

    public void updatePc(String str) {
        if (this.pcName != null && !TextUtils.isEmpty(str)) {
            this.pcName.setText(str);
        }
    }

    public void getPcListStart() {
        if (this.pcName != null) {
            this.pcName.setText(this.context.getString(R.string.photo_backup_geting_pc));
        }
    }
}
