package com.tencent.assistant.component.smartcard;

import android.os.Bundle;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.link.b;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class p extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f1155a;
    final /* synthetic */ STInfoV2 b;
    final /* synthetic */ NormalSmartcardBookingItem c;

    p(NormalSmartcardBookingItem normalSmartcardBookingItem, String str, STInfoV2 sTInfoV2) {
        this.c = normalSmartcardBookingItem;
        this.f1155a = str;
        this.b = sTInfoV2;
    }

    public void onTMAClick(View view) {
        Bundle bundle = new Bundle();
        bundle.putInt(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, this.c.a(this.c.contextToPageId(this.c.f1115a)));
        b.b(this.c.getContext(), this.f1155a, bundle);
    }

    public STInfoV2 getStInfo() {
        return this.b;
    }
}
