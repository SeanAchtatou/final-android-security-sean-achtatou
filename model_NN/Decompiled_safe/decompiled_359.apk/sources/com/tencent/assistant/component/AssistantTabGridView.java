package com.tencent.assistant.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.GridView;
import java.lang.reflect.InvocationTargetException;

/* compiled from: ProGuard */
public class AssistantTabGridView extends GridView {
    public AssistantTabGridView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        try {
            getClass().getMethod("setOverScrollMode", Integer.TYPE).invoke(this, 2);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e2) {
            e2.printStackTrace();
        } catch (IllegalAccessException e3) {
            e3.printStackTrace();
        } catch (InvocationTargetException e4) {
            e4.printStackTrace();
        }
    }

    public void onMeasure(int i, int i2) {
        super.onMeasure(i, View.MeasureSpec.makeMeasureSpec(536870911, Integer.MIN_VALUE));
    }
}
