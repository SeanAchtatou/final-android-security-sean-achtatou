package com.tencent.assistant.component.invalidater;

import android.widget.AbsListView;

/* compiled from: ProGuard */
public class ListViewScrollListener extends CommonViewInvalidater implements AbsListView.OnScrollListener {

    /* renamed from: a  reason: collision with root package name */
    private int f1064a = 0;

    public void onScrollStateChanged(AbsListView absListView, int i) {
        this.f1064a = i;
        if (canHandleMessage()) {
            handleQueueMsg();
        }
    }

    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
    }

    /* access modifiers changed from: protected */
    public boolean canHandleMessage() {
        return this.f1064a == 0;
    }
}
