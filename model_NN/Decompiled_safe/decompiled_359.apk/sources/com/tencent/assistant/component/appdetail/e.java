package com.tencent.assistant.component.appdetail;

import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.u;
import com.tencent.assistantv2.activity.AppDetailActivityV5;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class e extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppdetailDownloadBar f914a;

    e(AppdetailDownloadBar appdetailDownloadBar) {
        this.f914a = appdetailDownloadBar;
    }

    public void onTMAClick(View view) {
        SimpleAppModel a2;
        boolean z = true;
        if (view == this.f914a.r) {
            this.f914a.z.a(view);
        } else if (view == this.f914a.s) {
            this.f914a.z.a(view);
        } else if (this.f914a.z != null && (a2 = this.f914a.z.a()) != null) {
            AppConst.AppState d = u.d(a2);
            if (!(a2.h() || a2.i() || (a2.d() && (a2.P & 1) > 0)) || !(d == AppConst.AppState.DOWNLOAD || d == AppConst.AppState.UPDATE)) {
                z = false;
            }
            if ((view.getId() == R.id.wx_auth || view.getId() == R.id.qq_auth || z) && this.f914a.g == -1) {
                this.f914a.g++;
            }
            if (this.f914a.g < 0) {
                if (this.f914a.J) {
                    boolean unused = this.f914a.J = false;
                    this.f914a.a();
                }
            } else if (this.f914a.g == 0) {
                this.f914a.g++;
            }
            this.f914a.z.a(view);
        }
    }

    public STInfoV2 getStInfo() {
        if (!(this.f914a.i instanceof AppDetailActivityV5)) {
            return null;
        }
        STInfoV2 i = ((AppDetailActivityV5) this.f914a.i).i();
        i.slotId = this.f914a.c;
        if (this.f914a.z == null || i == null || this.f914a.s == null || this.f914a.r == null) {
            return null;
        }
        SimpleAppModel a2 = this.f914a.z.a();
        if (a2 != null) {
            i.isImmediately = a2.au == 1;
        }
        if (this.clickViewId != R.id.wx_auth && this.clickViewId != R.id.qq_auth && this.clickViewId != this.f914a.r.getId() && this.clickViewId != this.f914a.s.getId()) {
            AppConst.AppState d = u.d(a2);
            i.actionId = a.a(d);
            i.status = a.a(d, a2);
        } else if (this.clickViewId == R.id.qq_auth) {
            i.actionId = 200;
            i.status = "03";
        } else if (this.clickViewId == R.id.wx_auth) {
            i.actionId = 200;
            i.status = "04";
        } else if (this.clickViewId == this.f914a.r.getId()) {
            i.slotId = this.f914a.d;
            i.actionId = 200;
            if (a2 == null || ApkResourceManager.getInstance().getLocalApkInfo(a2.c) == null) {
                i.status = "03";
            } else if (this.f914a.e) {
                i.status = "02";
            } else {
                i.status = "01";
            }
        }
        i.actionFlag = this.f914a.z.e();
        return i;
    }
}
