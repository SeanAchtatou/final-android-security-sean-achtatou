package com.tencent.assistant.component;

import android.view.View;
import com.qq.AppService.AstApp;
import com.tencent.assistant.activity.SelfUpdateActivity;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class db extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SelfForceUpdateView f1011a;

    db(SelfForceUpdateView selfForceUpdateView) {
        this.f1011a = selfForceUpdateView;
    }

    public void onTMAClick(View view) {
        AstApp.i().f();
    }

    public STInfoV2 getStInfo() {
        if (!(this.f1011a.getContext() instanceof SelfUpdateActivity)) {
            return null;
        }
        STInfoV2 i = ((SelfUpdateActivity) this.f1011a.getContext()).i();
        i.slotId = "01_002";
        i.actionId = 200;
        i.extraData = "02";
        return i;
    }
}
