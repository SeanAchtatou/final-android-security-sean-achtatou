package com.tencent.assistant.component;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import java.lang.reflect.Field;

/* compiled from: ProGuard */
public class MyViewPager extends ViewPager implements Shieldable {
    private boolean interceptable = true;
    private Bitmap mPageBackground;
    private Paint p = new Paint(1);

    public MyViewPager(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public MyViewPager(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public void dispatchDraw(Canvas canvas) {
        if (this.mPageBackground != null) {
            int width = this.mPageBackground.getWidth();
            int height = this.mPageBackground.getHeight();
            int count = getAdapter().getCount();
            int scrollX = getScrollX();
            int width2 = (getWidth() * height) / getHeight();
            int width3 = (((width - width2) / (count - 1)) * scrollX) / getWidth();
            canvas.drawBitmap(this.mPageBackground, new Rect(width3, 0, width2 + width3, height), new Rect(scrollX, 0, getWidth() + scrollX, getHeight()), this.p);
        }
        super.dispatchDraw(canvas);
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0 || this.interceptable) {
            try {
                return super.onInterceptTouchEvent(motionEvent);
            } catch (Exception e) {
                return false;
            }
        } else {
            Class<ViewPager> cls = ViewPager.class;
            try {
                Field declaredField = cls.getDeclaredField("mLastMotionX");
                declaredField.setAccessible(true);
                declaredField.set(this, Float.valueOf(motionEvent.getX()));
                cls.getDeclaredField("mInitialMotionX");
                declaredField.setAccessible(true);
                declaredField.set(this, Float.valueOf(motionEvent.getX()));
                return false;
            } catch (IllegalAccessException | IllegalArgumentException | NoSuchFieldException | SecurityException e2) {
                return false;
            }
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        try {
            return super.onTouchEvent(motionEvent);
        } catch (Exception e) {
            return false;
        }
    }

    public void setPageBackground(Bitmap bitmap) {
        this.mPageBackground = bitmap;
        this.p.setFilterBitmap(true);
    }

    public void setShielded(boolean z) {
        this.interceptable = !z;
    }
}
