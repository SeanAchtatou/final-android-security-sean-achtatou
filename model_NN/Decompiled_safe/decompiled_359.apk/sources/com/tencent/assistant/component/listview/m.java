package com.tencent.assistant.component.listview;

import com.tencent.assistant.component.listview.ManagerGeneralController;

/* compiled from: ProGuard */
class m implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ManagerGeneralController.PositionInfo f1093a;
    final /* synthetic */ ManagerGeneralController.BussinessListener b;
    final /* synthetic */ Object c;
    final /* synthetic */ ManagerGeneralController d;

    m(ManagerGeneralController managerGeneralController, ManagerGeneralController.PositionInfo positionInfo, ManagerGeneralController.BussinessListener bussinessListener, Object obj) {
        this.d = managerGeneralController;
        this.f1093a = positionInfo;
        this.b = bussinessListener;
        this.c = obj;
    }

    public void run() {
        this.d.c.delete(this.f1093a.groupPosition, this.f1093a.childPosition, false, new n(this));
    }
}
