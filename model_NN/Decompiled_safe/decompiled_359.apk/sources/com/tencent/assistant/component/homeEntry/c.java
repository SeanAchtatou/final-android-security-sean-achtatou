package com.tencent.assistant.component.homeEntry;

import android.graphics.Bitmap;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
class c implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Bitmap f1056a;
    final /* synthetic */ HomeEntryCellBase b;

    c(HomeEntryCellBase homeEntryCellBase, Bitmap bitmap) {
        this.b = homeEntryCellBase;
        this.f1056a = bitmap;
    }

    public void run() {
        XLog.v("home_entry", "cell---requestImg--cell name:" + getClass().getSimpleName() + "--bitmap refresh2222222---------------local");
        this.b.setImg(this.f1056a);
    }
}
