package com.tencent.assistant.component.listview;

import android.view.animation.Animation;

/* compiled from: ProGuard */
class j implements Animation.AnimationListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ BubbleAnimationView f1090a;

    j(BubbleAnimationView bubbleAnimationView) {
        this.f1090a = bubbleAnimationView;
    }

    public void onAnimationStart(Animation animation) {
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationEnd(Animation animation) {
        this.f1090a.f1078a = false;
        if (!this.f1090a.e.isEmpty()) {
            this.f1090a.a();
        }
    }
}
