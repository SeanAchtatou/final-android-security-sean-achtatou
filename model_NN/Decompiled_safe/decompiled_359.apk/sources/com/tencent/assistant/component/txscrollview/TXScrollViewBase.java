package com.tencent.assistant.component.txscrollview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.PointF;
import android.support.v4.view.ViewCompat;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import com.tencent.android.qqdownloader.b;
import com.tencent.assistant.utils.t;

/* compiled from: ProGuard */
public abstract class TXScrollViewBase<T extends View> extends LinearLayout {
    public static final float FRICTION = 2.0f;
    public static final int SMOOTH_SCROLL_DURATION_MS = 200;
    protected FrameLayout mContentViewWrapper;
    protected ScrollState mCurScrollState = ScrollState.ScrollState_Initial;
    private TXScrollViewBase<T>.SmoothScrollRunnable mCurrentSmoothScrollRunnable = null;
    protected PointF mInitialMotionPointF = new PointF(0.0f, 0.0f);
    protected boolean mIsBeingDragged = false;
    protected PointF mLastMotionPointF = new PointF(0.0f, 0.0f);
    /* access modifiers changed from: private */
    public Interpolator mScrollAnimationInterpolator = new DecelerateInterpolator();
    protected T mScrollContentView;
    protected ScrollDirection mScrollDirection = ScrollDirection.SCROLL_DIRECTION_VERTICAL;
    protected ScrollMode mScrollMode = ScrollMode.BOTH;
    private View mTipsView = null;
    private int mTouchSlop = 0;

    /* compiled from: ProGuard */
    public interface ISmoothScrollRunnableListener {
        void onSmoothScrollFinished();
    }

    /* compiled from: ProGuard */
    public enum ScrollState {
        ScrollState_Initial,
        ScrollState_FromStart,
        ScrollState_FromEnd
    }

    /* access modifiers changed from: protected */
    public abstract T createScrollContentView(Context context);

    /* access modifiers changed from: protected */
    public abstract boolean isContentFullScreen();

    /* access modifiers changed from: protected */
    public abstract boolean isReadyForScrollEnd();

    /* access modifiers changed from: protected */
    public abstract boolean isReadyForScrollStart();

    public TXScrollViewBase(Context context, ScrollDirection scrollDirection, ScrollMode scrollMode) {
        super(context);
        this.mScrollDirection = scrollDirection;
        this.mScrollMode = scrollMode;
        initView(context);
    }

    public TXScrollViewBase(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, b.n);
        if (obtainStyledAttributes != null) {
            this.mScrollDirection = ScrollDirection.a(obtainStyledAttributes.getInt(1, 0));
            this.mScrollMode = ScrollMode.mapIntToValue(obtainStyledAttributes.getInt(0, 3));
            obtainStyledAttributes.recycle();
        }
        initView(context);
    }

    /* access modifiers changed from: protected */
    public void initView(Context context) {
        if (this.mScrollDirection == ScrollDirection.SCROLL_DIRECTION_HORIZONTAL) {
            setOrientation(0);
        } else {
            setOrientation(1);
        }
        setGravity(17);
        this.mTouchSlop = ViewConfiguration.get(context).getScaledTouchSlop();
        this.mScrollContentView = createScrollContentView(context);
        addScrollContentView(context, this.mScrollContentView);
    }

    public void setTipsView(View view) {
        if (view != null) {
            removeAllViews();
            super.addView(view, -1, new LinearLayout.LayoutParams(-1, -1));
        }
    }

    public final boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        float f;
        float f2;
        int action = motionEvent.getAction();
        if (action == 3 || action == 1) {
            this.mIsBeingDragged = false;
            return false;
        } else if (this.mIsBeingDragged && action != 0) {
            return true;
        } else {
            switch (action) {
                case 0:
                    if (isReadyForScroll()) {
                        PointF pointF = this.mLastMotionPointF;
                        PointF pointF2 = this.mInitialMotionPointF;
                        float x = motionEvent.getX();
                        pointF2.x = x;
                        pointF.x = x;
                        PointF pointF3 = this.mLastMotionPointF;
                        PointF pointF4 = this.mInitialMotionPointF;
                        float y = motionEvent.getY();
                        pointF4.y = y;
                        pointF3.y = y;
                        this.mIsBeingDragged = false;
                        break;
                    }
                    break;
                case 2:
                    if (isReadyForScroll()) {
                        float x2 = motionEvent.getX();
                        float y2 = motionEvent.getY();
                        if (this.mScrollDirection == ScrollDirection.SCROLL_DIRECTION_HORIZONTAL) {
                            f = x2 - this.mLastMotionPointF.x;
                            f2 = y2 - this.mLastMotionPointF.y;
                        } else {
                            f = y2 - this.mLastMotionPointF.y;
                            f2 = x2 - this.mLastMotionPointF.x;
                        }
                        float abs = Math.abs(f);
                        if (abs > ((float) this.mTouchSlop) && abs > Math.abs(f2)) {
                            if (f >= 1.0f && isReadyForScrollStart()) {
                                this.mLastMotionPointF.x = x2;
                                this.mLastMotionPointF.y = y2;
                                this.mIsBeingDragged = true;
                                this.mCurScrollState = ScrollState.ScrollState_FromStart;
                            }
                            if (f <= -1.0f && isReadyForScrollEnd()) {
                                this.mLastMotionPointF.x = x2;
                                this.mLastMotionPointF.y = y2;
                                this.mIsBeingDragged = true;
                                this.mCurScrollState = ScrollState.ScrollState_FromEnd;
                                break;
                            }
                        }
                    }
                    break;
            }
            return this.mIsBeingDragged;
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0 && motionEvent.getEdgeFlags() != 0) {
            return false;
        }
        switch (motionEvent.getAction()) {
            case 0:
                if (!isReadyForScroll()) {
                    return false;
                }
                PointF pointF = this.mLastMotionPointF;
                PointF pointF2 = this.mInitialMotionPointF;
                float x = motionEvent.getX();
                pointF2.x = x;
                pointF.x = x;
                PointF pointF3 = this.mLastMotionPointF;
                PointF pointF4 = this.mInitialMotionPointF;
                float y = motionEvent.getY();
                pointF4.y = y;
                pointF3.y = y;
                return true;
            case 1:
            case 3:
                return onTouchEventCancelAndUp();
            case 2:
                if (!this.mIsBeingDragged) {
                    return false;
                }
                this.mLastMotionPointF.x = motionEvent.getX();
                this.mLastMotionPointF.y = motionEvent.getY();
                if (this.mScrollMode != ScrollMode.NOSCROLL) {
                    scrollMoveEvent();
                }
                return true;
            default:
                return false;
        }
    }

    /* access modifiers changed from: protected */
    public boolean onTouchEventCancelAndUp() {
        if (!this.mIsBeingDragged) {
            return false;
        }
        this.mIsBeingDragged = false;
        smoothScrollTo(0);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        onScrollViewSizeChange(i, i2);
        post(new s(this));
    }

    /* access modifiers changed from: protected */
    public void onScrollViewSizeChange(int i, int i2) {
        refreshScrollContentViewSize(i, i2);
    }

    /* access modifiers changed from: protected */
    public int getMaximumScrollOffset() {
        if (this.mScrollDirection == ScrollDirection.SCROLL_DIRECTION_HORIZONTAL) {
            return Math.round(((float) getWidth()) / 2.0f);
        }
        return Math.round(((float) getHeight()) / 2.0f);
    }

    public void addView(View view, int i, ViewGroup.LayoutParams layoutParams) {
        if (view != null && this.mScrollContentView != null && (this.mScrollContentView instanceof ViewGroup)) {
            ((ViewGroup) this.mScrollContentView).addView(view, i, layoutParams);
        }
    }

    /* access modifiers changed from: protected */
    public void addViewInternal(View view, int i, ViewGroup.LayoutParams layoutParams) {
        super.addView(view, i, layoutParams);
    }

    /* access modifiers changed from: protected */
    public final void refreshScrollContentViewSize(int i, int i2) {
        if (this.mContentViewWrapper != null) {
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) this.mContentViewWrapper.getLayoutParams();
            if (this.mScrollDirection == ScrollDirection.SCROLL_DIRECTION_HORIZONTAL) {
                if (layoutParams.width != i) {
                    layoutParams.width = i;
                    this.mContentViewWrapper.requestLayout();
                }
            } else if (layoutParams.height != i2) {
                layoutParams.height = i2;
                this.mContentViewWrapper.requestLayout();
            }
        }
    }

    private void addScrollContentView(Context context, T t) {
        if (t != null) {
            super.addView(t, -1, new LinearLayout.LayoutParams(-1, -1));
        }
    }

    /* access modifiers changed from: protected */
    public boolean isReadyForScroll() {
        if (isReadyForScrollStart() || isReadyForScrollEnd()) {
            return true;
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    /* access modifiers changed from: protected */
    public int scrollMoveEvent() {
        float f;
        float f2;
        int round;
        if (this.mScrollDirection == ScrollDirection.SCROLL_DIRECTION_HORIZONTAL) {
            f = this.mInitialMotionPointF.x;
            f2 = this.mLastMotionPointF.x;
        } else {
            f = this.mInitialMotionPointF.y;
            f2 = this.mLastMotionPointF.y;
        }
        if (this.mCurScrollState == ScrollState.ScrollState_FromStart) {
            round = Math.round(Math.min(f - f2, 0.0f) / 2.0f);
        } else {
            round = Math.round(Math.max(f - f2, 0.0f) / 2.0f);
        }
        contentViewScrollTo(round);
        return round;
    }

    /* access modifiers changed from: protected */
    public void contentViewScrollTo(int i) {
        int maximumScrollOffset = getMaximumScrollOffset();
        int min = Math.min(maximumScrollOffset, Math.max(-maximumScrollOffset, i));
        if (this.mScrollDirection == ScrollDirection.SCROLL_DIRECTION_HORIZONTAL) {
            scrollTo(min, 0);
        } else {
            scrollTo(0, min);
        }
    }

    public final void smoothScrollTo(int i) {
        smoothScrollTo(i, (long) getSmoothScrollDuration(), 0, null);
    }

    public final void smoothScrollTo(int i, ISmoothScrollRunnableListener iSmoothScrollRunnableListener) {
        smoothScrollTo(i, (long) getSmoothScrollDuration(), 0, iSmoothScrollRunnableListener);
    }

    public final void smoothScrollTo(int i, long j, long j2, ISmoothScrollRunnableListener iSmoothScrollRunnableListener) {
        int scrollY;
        if (this.mCurrentSmoothScrollRunnable != null) {
            this.mCurrentSmoothScrollRunnable.stop();
        }
        if (this.mScrollDirection == ScrollDirection.SCROLL_DIRECTION_HORIZONTAL) {
            scrollY = getScrollX();
        } else {
            scrollY = getScrollY();
        }
        if (scrollY != i) {
            this.mCurrentSmoothScrollRunnable = new SmoothScrollRunnable(scrollY, i, j, iSmoothScrollRunnableListener);
            if (j2 > 0) {
                postDelayed(this.mCurrentSmoothScrollRunnable, j2);
            } else {
                post(this.mCurrentSmoothScrollRunnable);
            }
        }
    }

    /* compiled from: ProGuard */
    public enum ScrollMode {
        PULL_FROM_START,
        PULL_FROM_END,
        BOTH,
        NONE,
        NOSCROLL;

        static ScrollMode mapIntToValue(int i) {
            switch (i) {
                case 0:
                    return PULL_FROM_START;
                case 1:
                    return PULL_FROM_END;
                case 2:
                    return BOTH;
                case 3:
                    return NONE;
                case 4:
                    return NOSCROLL;
                default:
                    return BOTH;
            }
        }
    }

    /* compiled from: ProGuard */
    public enum ScrollDirection {
        SCROLL_DIRECTION_VERTICAL,
        SCROLL_DIRECTION_HORIZONTAL;

        static ScrollDirection a(int i) {
            switch (i) {
                case 0:
                    return SCROLL_DIRECTION_VERTICAL;
                case 1:
                    return SCROLL_DIRECTION_HORIZONTAL;
                default:
                    return SCROLL_DIRECTION_VERTICAL;
            }
        }
    }

    /* compiled from: ProGuard */
    public class SmoothScrollRunnable implements Runnable {
        private final Interpolator b;
        private final int c;
        private final int d;
        private final long e;
        private boolean f = true;
        private long g = -1;
        private int h = -1;
        private ISmoothScrollRunnableListener i;

        public SmoothScrollRunnable(int i2, int i3, long j, ISmoothScrollRunnableListener iSmoothScrollRunnableListener) {
            this.d = i2;
            this.c = i3;
            this.e = j;
            this.i = iSmoothScrollRunnableListener;
            this.b = TXScrollViewBase.this.mScrollAnimationInterpolator;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.lang.Math.max(long, long):long}
         arg types: [long, int]
         candidates:
          ClspMth{java.lang.Math.max(double, double):double}
          ClspMth{java.lang.Math.max(int, int):int}
          ClspMth{java.lang.Math.max(float, float):float}
          ClspMth{java.lang.Math.max(long, long):long} */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.lang.Math.min(long, long):long}
         arg types: [long, int]
         candidates:
          ClspMth{java.lang.Math.min(double, double):double}
          ClspMth{java.lang.Math.min(float, float):float}
          ClspMth{java.lang.Math.min(int, int):int}
          ClspMth{java.lang.Math.min(long, long):long} */
        public void run() {
            if (this.g == -1) {
                this.g = System.currentTimeMillis();
            } else {
                this.h = this.d - Math.round(this.b.getInterpolation(((float) Math.max(Math.min(((System.currentTimeMillis() - this.g) * 1000) / this.e, 1000L), 0L)) / 1000.0f) * ((float) (this.d - this.c)));
                TXScrollViewBase.this.contentViewScrollTo(this.h);
            }
            if (this.f && this.c != this.h) {
                String t = t.t();
                if (TextUtils.isEmpty(t) || (!t.contains("OZZO138T") && !t.contains("W9800B"))) {
                    ViewCompat.postOnAnimation(TXScrollViewBase.this, this);
                }
            } else if (this.i != null) {
                ISmoothScrollRunnableListener iSmoothScrollRunnableListener = this.i;
                this.i = null;
                iSmoothScrollRunnableListener.onSmoothScrollFinished();
            }
        }

        public void stop() {
            this.f = false;
            TXScrollViewBase.this.removeCallbacks(this);
            if (this.i != null) {
                ISmoothScrollRunnableListener iSmoothScrollRunnableListener = this.i;
                this.i = null;
                iSmoothScrollRunnableListener.onSmoothScrollFinished();
            }
        }
    }

    public void recycleData() {
    }

    /* access modifiers changed from: protected */
    public int getSmoothScrollDuration() {
        return 200;
    }
}
