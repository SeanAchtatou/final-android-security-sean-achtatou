package com.tencent.assistant.component;

import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.appdetail.CommentDetailTabView;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.manager.DownloadProxy;
import com.tencent.assistant.manager.f;
import com.tencent.assistant.manager.g;
import com.tencent.assistant.module.u;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistantv2.activity.AppDetailActivityV5;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class e extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppIconView f1035a;

    e(AppIconView appIconView) {
        this.f1035a = appIconView;
    }

    public void onTMAClick(View view) {
        DownloadInfo a2 = DownloadProxy.a().a(this.f1035a.mAppModel);
        if (a2 != null && a2.needReCreateInfo(this.f1035a.mAppModel)) {
            DownloadProxy.a().b(a2.downloadTicket);
            a2 = null;
        }
        StatInfo a3 = a.a(this.f1035a.statInfo);
        if (a2 == null) {
            a2 = DownloadInfo.createDownloadInfo(this.f1035a.mAppModel, a3);
            if ((view instanceof g) && this.f1035a.mAppModel != null) {
                f.a().a(this.f1035a.mAppModel.q(), (g) view);
            }
        } else {
            a2.updateDownloadInfoStatInfo(a3);
        }
        if (!TextUtils.isEmpty(this.f1035a.mAppModel.q())) {
            switch (h.f1051a[u.d(this.f1035a.mAppModel).ordinal()]) {
                case 1:
                case 2:
                    if (this.f1035a.mProgressBar != null) {
                        this.f1035a.mProgressBar.setVisibility(0);
                    }
                    com.tencent.assistant.download.a.a().b(a2.downloadTicket);
                    return;
                case 3:
                case 4:
                    if (this.f1035a.mProgressBar != null) {
                        this.f1035a.mProgressBar.setVisibility(0);
                    }
                    com.tencent.assistant.download.a.a().b(a2);
                    return;
                default:
                    if (this.f1035a.iconClickListener != null) {
                        this.f1035a.iconClickListener.onClick();
                        return;
                    }
                    Intent intent = new Intent(this.f1035a.mContext, AppDetailActivityV5.class);
                    if (this.f1035a.mContext instanceof BaseActivity) {
                        intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, ((BaseActivity) this.f1035a.mContext).f());
                    }
                    intent.putExtra(CommentDetailTabView.PARAMS_SIMPLE_MODEL_INFO, this.f1035a.mAppModel);
                    intent.putExtra("statInfo", this.f1035a.statInfo);
                    this.f1035a.mContext.startActivity(intent);
                    return;
            }
        }
    }

    public STInfoV2 getStInfo() {
        if (this.f1035a.statInfo == null || !(this.f1035a.statInfo instanceof STInfoV2)) {
            return null;
        }
        this.f1035a.statInfo.updateStatus(this.f1035a.mAppModel);
        return (STInfoV2) this.f1035a.statInfo;
    }
}
