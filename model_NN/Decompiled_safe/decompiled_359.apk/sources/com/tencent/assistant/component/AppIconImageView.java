package com.tencent.assistant.component;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.PaintFlagsDrawFilter;
import android.graphics.Rect;
import android.util.AttributeSet;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.thumbnailCache.k;

/* compiled from: ProGuard */
public class AppIconImageView extends TXImageView {
    Rect destRect;
    private boolean gray;
    float[] grayArray;
    ColorMatrix mCm;
    ColorMatrixColorFilter mCmf;
    private Paint mPaint;
    PaintFlagsDrawFilter mPfd;
    Rect srcRect;

    public AppIconImageView(Context context) {
        this(context, null);
    }

    public AppIconImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.mPaint = new Paint();
        this.grayArray = new float[]{0.35f, 0.0f, 0.0f, 0.0f, 30.0f, 0.0f, 0.35f, 0.0f, 0.0f, 30.0f, 0.0f, 0.0f, 0.35f, 0.0f, 30.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f};
        this.gray = false;
        this.mCm = new ColorMatrix(this.grayArray);
        this.mCmf = new ColorMatrixColorFilter(this.mCm);
        this.srcRect = new Rect();
        this.destRect = new Rect();
        this.mPfd = new PaintFlagsDrawFilter(0, 2);
    }

    public void updateImageView(String str, int i, TXImageView.TXImageViewType tXImageViewType) {
        super.updateImageView(str, i, tXImageViewType);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        if (this.gray) {
            Bitmap a2 = k.b().a(this.mImageUrlString, this.mImageType.getThumbnailRequestType(), this);
            if (a2 != null) {
                this.mPaint.setAntiAlias(true);
                this.mPaint.setFilterBitmap(true);
                this.mCm.set(this.grayArray);
                this.mPaint.setColorFilter(this.mCmf);
                this.srcRect.set(0, 0, a2.getWidth(), a2.getHeight());
                this.destRect.set(0, 0, getWidth(), getHeight());
                canvas.setDrawFilter(this.mPfd);
                canvas.drawBitmap(a2, this.srcRect, this.destRect, this.mPaint);
                return;
            }
            super.onDraw(canvas);
            return;
        }
        super.onDraw(canvas);
    }

    public void setGray(boolean z) {
        if (this.gray != z) {
            invalidate();
        }
        this.gray = z;
    }
}
