package com.tencent.assistant.component.smartcard;

import android.content.Context;
import android.graphics.Canvas;
import android.text.Html;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.appdetail.process.s;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.a.i;
import com.tencent.assistant.model.a.k;
import com.tencent.assistant.model.a.l;
import com.tencent.assistant.model.d;
import com.tencent.assistant.utils.bt;
import com.tencent.assistant.utils.df;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class NormalSmartcardPersonalizedItem extends NormalSmartcardBaseItem {
    private View f;
    private TextView i;
    private TextView j;
    private ImageView k;
    private ImageView l;
    private LinearLayout m;
    /* access modifiers changed from: private */
    public int n;
    private ArrayList<l> o;

    static /* synthetic */ int a(NormalSmartcardPersonalizedItem normalSmartcardPersonalizedItem, int i2) {
        int i3 = normalSmartcardPersonalizedItem.n + i2;
        normalSmartcardPersonalizedItem.n = i3;
        return i3;
    }

    public NormalSmartcardPersonalizedItem(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public NormalSmartcardPersonalizedItem(Context context, i iVar, SmartcardListener smartcardListener, IViewInvalidater iViewInvalidater) {
        super(context, iVar, smartcardListener, iViewInvalidater);
    }

    public NormalSmartcardPersonalizedItem(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!this.hasInit) {
            this.hasInit = true;
            d();
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.c = this.b.inflate((int) R.layout.smartcard_personalized, this);
        this.f = findViewById(R.id.title_layout);
        this.i = (TextView) findViewById(R.id.title);
        this.j = (TextView) findViewById(R.id.desc);
        this.k = (ImageView) findViewById(R.id.close);
        this.l = (ImageView) findViewById(R.id.divider);
        this.m = (LinearLayout) findViewById(R.id.app_list);
        this.n = 0;
        f();
    }

    /* access modifiers changed from: protected */
    public void b() {
        f();
    }

    private void f() {
        this.m.removeAllViews();
        k kVar = (k) this.smartcardModel;
        if (kVar == null || kVar.d <= 0 || kVar.c == null || kVar.c.size() == 0 || kVar.c.size() < kVar.d) {
            c(8);
            setBackgroundResource(17170445);
            setPadding(0, 0, 0, 0);
            return;
        }
        setBackgroundResource(R.drawable.bg_card_selector_padding);
        c(0);
        if (kVar.p) {
            this.k.setVisibility(0);
        } else {
            this.k.setVisibility(8);
        }
        this.k.setOnClickListener(this.g);
        this.i.setText(kVar.k);
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.l.getLayoutParams();
        if (TextUtils.isEmpty(kVar.m)) {
            layoutParams.setMargins(0, df.b(14.0f), 0, 0);
            this.j.setVisibility(8);
        } else {
            layoutParams.setMargins(0, df.b(9.0f), 0, 0);
            this.j.setText(kVar.m);
            this.j.setVisibility(0);
        }
        ArrayList<l> g = g();
        if (this.o == null) {
            this.o = new ArrayList<>();
        }
        this.o.clear();
        this.o.addAll(g);
        int i2 = 0;
        while (i2 < g.size()) {
            View a2 = a(g.get(i2), i2, i2 == g.size() + -1);
            if (a2 != null) {
                this.m.addView(a2);
            }
            i2++;
        }
        if (kVar.c.size() >= kVar.d * 2) {
            View a3 = a(kVar.o);
            a3.setTag(R.id.tma_st_smartcard_tag, e());
            a3.setOnClickListener(new s(this, kVar));
            this.m.addView(a3, new LinearLayout.LayoutParams(-1, df.b(40.0f)));
        }
    }

    private ArrayList<l> g() {
        ArrayList<l> arrayList = new ArrayList<>();
        ArrayList arrayList2 = new ArrayList(((k) this.smartcardModel).c);
        int i2 = 0;
        int i3 = this.n;
        while (true) {
            int i4 = i2;
            if (i4 >= ((k) this.smartcardModel).d) {
                return arrayList;
            }
            int size = i3 % ((k) this.smartcardModel).c.size();
            arrayList.add(arrayList2.get(size));
            i3 = size + 1;
            i2 = i4 + 1;
        }
    }

    private View a(l lVar, int i2, boolean z) {
        if (lVar == null || lVar.f1645a == null) {
            return null;
        }
        STInfoV2 a2 = a(lVar, i2);
        View inflate = this.b.inflate((int) R.layout.smartcard_personalized_item, (ViewGroup) null);
        TXImageView tXImageView = (TXImageView) inflate.findViewById(R.id.icon);
        tXImageView.updateImageView(lVar.f1645a.e, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
        ((TextView) inflate.findViewById(R.id.name)).setText(lVar.f1645a.d);
        DownloadButton downloadButton = (DownloadButton) inflate.findViewById(R.id.btn);
        downloadButton.a(lVar.f1645a);
        SimpleAppModel simpleAppModel = lVar.f1645a;
        tXImageView.setTag(simpleAppModel.q());
        downloadButton.setTag(R.id.tma_st_smartcard_tag, e());
        if (s.a(simpleAppModel)) {
            downloadButton.setClickable(false);
        } else {
            downloadButton.setClickable(true);
            downloadButton.a(a2, new t(this), (d) null, downloadButton);
        }
        inflate.setTag(R.id.tma_st_smartcard_tag, e());
        inflate.setOnClickListener(new u(this, simpleAppModel, a2));
        ((TextView) inflate.findViewById(R.id.app_size)).setText(bt.a(simpleAppModel.k));
        TXImageView tXImageView2 = (TXImageView) inflate.findViewById(R.id.nicke_cion_1);
        TXImageView tXImageView3 = (TXImageView) inflate.findViewById(R.id.nicke_cion_2);
        if (lVar.c != null) {
            if (lVar.c.size() > 0) {
                tXImageView2.setVisibility(0);
                tXImageView2.updateImageView(lVar.c.get(0).b, R.drawable.tab_my_face, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            } else {
                tXImageView2.setVisibility(8);
            }
            if (lVar.c.size() > 1) {
                tXImageView3.setVisibility(0);
                tXImageView3.updateImageView(lVar.c.get(1).b, R.drawable.tab_my_face, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            } else {
                tXImageView3.setVisibility(8);
            }
        }
        ((TextView) inflate.findViewById(R.id.desc)).setText(Html.fromHtml(lVar.b));
        ImageView imageView = (ImageView) inflate.findViewById(R.id.divider);
        if (z) {
            imageView.setVisibility(8);
        } else {
            imageView.setVisibility(0);
        }
        return inflate;
    }

    private void c(int i2) {
        this.c.setVisibility(i2);
        this.f.setVisibility(i2);
        this.m.setVisibility(i2);
    }

    private View a(String str) {
        View inflate = this.b.inflate((int) R.layout.smartcard_list_footer, (ViewGroup) null);
        ((TextView) inflate.findViewById(R.id.text)).setText(str);
        ImageView imageView = (ImageView) inflate.findViewById(R.id.icon);
        imageView.setImageResource(R.drawable.next);
        imageView.setVisibility(0);
        return inflate;
    }

    private STInfoV2 a(l lVar, int i2) {
        STInfoV2 a2 = a(a.a("03", i2), 100);
        if (!(a2 == null || lVar == null)) {
            a2.updateWithSimpleAppModel(lVar.f1645a);
        }
        if (this.e == null) {
            this.e = new com.tencent.assistantv2.st.b.d();
        }
        this.e.a(a2);
        return a2;
    }
}
