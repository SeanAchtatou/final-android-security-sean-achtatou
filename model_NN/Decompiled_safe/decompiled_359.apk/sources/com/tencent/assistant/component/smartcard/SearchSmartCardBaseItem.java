package com.tencent.assistant.component.smartcard;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.model.a.i;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.STPageInfo;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
public abstract class SearchSmartCardBaseItem extends ISmartcard {
    protected long f = 0;
    protected View.OnClickListener g = new x(this);
    protected View.OnClickListener h = new y(this);

    /* access modifiers changed from: protected */
    public abstract String c();

    /* access modifiers changed from: protected */
    public abstract int d();

    /* access modifiers changed from: protected */
    public abstract String e();

    /* access modifiers changed from: protected */
    public abstract long f();

    /* access modifiers changed from: protected */
    public abstract int g();

    public SearchSmartCardBaseItem(Context context) {
        super(context);
    }

    public SearchSmartCardBaseItem(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public SearchSmartCardBaseItem(Context context, i iVar, SmartcardListener smartcardListener) {
        super(context, iVar, smartcardListener, null);
        a();
    }

    public void resetSmartcardBaseItem(i iVar) {
        this.smartcardModel = iVar;
        b();
    }

    public void setSearchId(long j) {
        this.f = j;
    }

    /* access modifiers changed from: protected */
    public void h() {
        if (this.smartcardListener != null) {
            this.smartcardListener.onSmartcardExposure(this.smartcardModel.i, this.smartcardModel.j);
        }
        a(c(), 100, this.smartcardModel.r, -1);
    }

    /* access modifiers changed from: protected */
    public int a(int i) {
        if (this.f1115a instanceof BaseActivity) {
            return ((BaseActivity) this.f1115a).f();
        }
        return STConst.ST_PAGE_SEARCH_RESULT_ALL;
    }

    /* access modifiers changed from: protected */
    public String b(int i) {
        return this.smartcardModel.j + "||" + this.smartcardModel.i + "|" + i;
    }

    /* access modifiers changed from: protected */
    public void a(String str, int i, byte[] bArr, long j) {
        STInfoV2 a2 = a(str, i);
        if (a2 == null) {
            return;
        }
        if (a2.scene != g()) {
            XLog.d("SearchSmartCardBaseItem", "stInfo.scene != getScene(), return, report nothing!");
            return;
        }
        if (this.smartcardModel != null) {
            a2.pushInfo = b(0);
        }
        if (j > 0) {
            a2.appId = j;
        }
        a2.resourceType = d();
        a2.extraData = e();
        a2.searchId = f();
        a2.recommendId = bArr;
        k.a(a2);
    }

    /* access modifiers changed from: protected */
    public STInfoV2 a(String str, int i) {
        STPageInfo sTPageInfo;
        if (this.f1115a instanceof BaseActivity) {
            sTPageInfo = ((BaseActivity) this.f1115a).q();
        } else {
            sTPageInfo = null;
        }
        if (sTPageInfo == null) {
            return null;
        }
        STInfoV2 sTInfoV2 = new STInfoV2(sTPageInfo.f3358a, str, sTPageInfo.c, a.b(sTPageInfo.b, STConst.ST_STATUS_DEFAULT), i);
        sTInfoV2.pushInfo = b(0);
        return sTInfoV2;
    }
}
