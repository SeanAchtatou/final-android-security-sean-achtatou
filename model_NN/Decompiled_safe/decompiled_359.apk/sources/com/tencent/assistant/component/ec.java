package com.tencent.assistant.component;

import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.ds;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class ec implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ eb f1038a;

    ec(eb ebVar) {
        this.f1038a = ebVar;
    }

    public void run() {
        String str;
        if (this.f1038a.extraMsgView == null || this.f1038a.c.f1036a.extraMsgViewCheckBox == null) {
            str = null;
        } else if (this.f1038a.c.f1036a.extraMsgViewCheckBox.isChecked()) {
            str = "1";
        } else {
            str = "0";
        }
        STInfoV2 sTInfoV2 = new STInfoV2(STConst.ST_PAGE_UPDATE_UPDATEALL, "03_001", STConst.ST_PAGE_UPDATE, STConst.ST_DEFAULT_SLOT, 200);
        sTInfoV2.status = str;
        sTInfoV2.pushInfo = this.f1038a.c.f1036a.pushInfo;
        k.a(sTInfoV2);
        int unused = this.f1038a.c.f1036a.callFrom = 0;
        int unused2 = this.f1038a.c.f1036a.userMode = 0;
        this.f1038a.c.f1036a.updateAll();
        if (this.f1038a.extraMsgView != null && this.f1038a.c.f1036a.extraMsgViewCheckBox != null && this.f1038a.c.f1036a.extraMsgViewCheckBox.isChecked()) {
            if (this.f1038a.c.f1036a.mobileManagerAppModelFromServer != null) {
                this.f1038a.c.f1036a.downloadTencentMobileManager();
                return;
            }
            ds dsVar = new ds();
            SimpleAppModel simpleAppModel = new SimpleAppModel();
            simpleAppModel.f1634a = this.f1038a.f1037a.e;
            simpleAppModel.c = this.f1038a.f1037a.g;
            simpleAppModel.ac = this.f1038a.f1037a.f;
            dsVar.a(simpleAppModel);
            dsVar.register(this.f1038a.c.f1036a);
        }
    }
}
