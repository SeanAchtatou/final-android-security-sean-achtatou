package com.tencent.assistant.component;

import android.os.Handler;
import android.os.Message;
import com.tencent.assistant.thumbnailCache.o;

/* compiled from: ProGuard */
class bo extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PicView f970a;

    bo(PicView picView) {
        this.f970a = picView;
    }

    public void handleMessage(Message message) {
        this.f970a.progressbar.setVisibility(8);
        this.f970a.textview.setVisibility(8);
        switch (message.what) {
            case 0:
                this.f970a.onPicLoadSuccess((o) message.obj);
                return;
            case 1:
                this.f970a.onLoadPicFail();
                return;
            default:
                return;
        }
    }
}
