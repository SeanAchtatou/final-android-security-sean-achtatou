package com.tencent.assistant.component.spaceclean;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.model.spaceclean.SubRubbishInfo;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class d extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SubRubbishInfo f1170a;
    final /* synthetic */ g b;
    final /* synthetic */ RubbishItemView c;

    d(RubbishItemView rubbishItemView, SubRubbishInfo subRubbishInfo, g gVar) {
        this.c = rubbishItemView;
        this.f1170a = subRubbishInfo;
        this.b = gVar;
    }

    public void onTMAClick(View view) {
        if (!RubbishItemView.isDeleting) {
            if (!RubbishItemView.isFirstSelect || this.f1170a.d || this.f1170a.e) {
                this.c.b(this.b.e, this.f1170a);
                return;
            }
            RubbishItemView.isFirstSelect = false;
            this.c.a(this.b.e, this.f1170a);
        }
    }

    public STInfoV2 getStInfo() {
        this.c.m.actionId = 200;
        if (this.f1170a.d) {
            this.c.m.status = "02";
        } else {
            this.c.m.status = "01";
        }
        return this.c.m;
    }
}
