package com.tencent.assistant.component.txscrollview;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AbsListView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.tencent.assistant.component.txscrollview.TXRefreshScrollViewBase;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.utils.ba;
import com.tencent.assistant.utils.cj;

/* compiled from: ProGuard */
public class TXRefreshGetMoreListView extends TXRefreshScrollViewBase<ListView> implements AbsListView.OnScrollListener {

    /* renamed from: a  reason: collision with root package name */
    protected TXLoadingLayoutBase f1190a;
    protected TXLoadingLayoutBase b;
    protected TXRefreshScrollViewBase.RefreshState c = TXRefreshScrollViewBase.RefreshState.RESET;
    private ListAdapter d;
    private String e;
    private int f = 0;
    private IScrollListener g;
    private Bitmap h = null;
    private boolean i = false;
    private boolean j = false;

    public TXRefreshGetMoreListView(Context context, TXScrollViewBase.ScrollMode scrollMode) {
        super(context, TXScrollViewBase.ScrollDirection.SCROLL_DIRECTION_VERTICAL, TXScrollViewBase.ScrollMode.PULL_FROM_START);
        ((ListView) this.mScrollContentView).setCacheColorHint(0);
    }

    public TXRefreshGetMoreListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        ((ListView) this.mScrollContentView).setCacheColorHint(0);
    }

    public void setRefreshTimeKey(String str) {
        this.e = str;
        if (this.mHeaderLayout != null) {
            ((RefreshListLoading) this.mHeaderLayout).setRefreshTimeKey(str);
        }
    }

    /* access modifiers changed from: protected */
    public void updateUIFroMode() {
        LinearLayout.LayoutParams loadingLayoutLayoutParams = getLoadingLayoutLayoutParams();
        if (this.mHeaderLayout != null) {
            if (this.mHeaderLayout.getParent() == this) {
                removeView(this.mHeaderLayout);
            }
            addViewInternal(this.mHeaderLayout, 0, loadingLayoutLayoutParams);
        }
        this.mFooterLayout = null;
        refreshLoadingLayoutSize();
    }

    public void resetLoadLoadingLayoutBg() {
        if (this.i) {
            if (this.f1190a != null) {
                this.f1190a.setBackgroundDrawable(null);
                this.f1190a.setDrawingCacheEnabled(false);
                ((RefreshListLoading) this.f1190a).resetLoadingResource();
            }
            if (this.mHeaderLayout != null) {
                this.mHeaderLayout.setBackgroundDrawable(null);
                this.mHeaderLayout.setDrawingCacheEnabled(false);
                ((RefreshListLoading) this.mHeaderLayout).resetLoadingResource();
            }
            this.i = false;
            if (this.h != null && !this.h.isRecycled()) {
                this.h.recycle();
                this.h = null;
            }
        }
    }

    public void updateBackground(Context context, Bitmap bitmap) {
        Bitmap createBitmap;
        if (context != null && bitmap != null && !bitmap.isRecycled()) {
            if (this.h != null) {
                this.h = null;
            }
            this.h = bitmap;
            if (this.f1190a != null && this.mHeaderLayout != null && (createBitmap = Bitmap.createBitmap(this.h, 0, this.h.getHeight() - headerLoadingLayoutContentHeight, this.h.getWidth(), headerLoadingLayoutContentHeight)) != null) {
                this.f1190a.setBackgroundDrawable(new BitmapDrawable(createBitmap));
                this.f1190a.setDrawingCacheEnabled(true);
                this.mHeaderLayout.setBackgroundDrawable(new BitmapDrawable(this.h));
                this.mHeaderLayout.setDrawingCacheEnabled(true);
                this.i = true;
                ((RefreshListLoading) this.mHeaderLayout).refreshLoadingResource();
                ((RefreshListLoading) this.f1190a).refreshLoadingResource();
            }
        }
    }

    public void onRefreshComplete(boolean z, boolean z2, String str) {
        if (this.f1190a != null) {
            if (z) {
                this.f1190a.refreshSuc();
                this.mHeaderLayout.refreshSuc();
            } else {
                this.f1190a.refreshFail(str);
                this.mHeaderLayout.refreshFail(str);
            }
        }
        this.mHeaderLayout.hideAllSubViews();
        ba.a().postDelayed(new l(this, z2), 900);
    }

    public void onRefreshComplete(boolean z) {
        onRefreshComplete(z, true);
    }

    public void onRefreshComplete(boolean z, boolean z2) {
        super.onRefreshComplete(z);
        if (this.c == TXRefreshScrollViewBase.RefreshState.REFRESHING) {
            if (z) {
                this.c = TXRefreshScrollViewBase.RefreshState.RESET;
            } else {
                this.c = TXRefreshScrollViewBase.RefreshState.REFRESH_LOAD_FINISH;
            }
        } else if (this.c == TXRefreshScrollViewBase.RefreshState.REFRESH_LOAD_FINISH) {
            this.c = TXRefreshScrollViewBase.RefreshState.RESET;
        } else if (this.c == TXRefreshScrollViewBase.RefreshState.RESET && !z) {
            this.c = TXRefreshScrollViewBase.RefreshState.REFRESH_LOAD_FINISH;
        }
        a(z2);
    }

    /* access modifiers changed from: protected */
    public void onReset() {
        int i2;
        TXLoadingLayoutBase tXLoadingLayoutBase;
        TXLoadingLayoutBase tXLoadingLayoutBase2;
        boolean z;
        if (this.mCurScrollState != TXScrollViewBase.ScrollState.ScrollState_FromStart || this.mHeaderLayout == null || this.f1190a == null) {
            i2 = 0;
            tXLoadingLayoutBase = null;
            tXLoadingLayoutBase2 = null;
            z = false;
        } else {
            tXLoadingLayoutBase2 = this.mHeaderLayout;
            tXLoadingLayoutBase = this.f1190a;
            i2 = 0 - this.mHeaderLayout.getContentSize();
            z = Math.abs(((ListView) this.mScrollContentView).getFirstVisiblePosition() - 0) <= 1;
        }
        if (tXLoadingLayoutBase != null && tXLoadingLayoutBase.getVisibility() == 0) {
            tXLoadingLayoutBase.setVisibility(8);
            if (Build.VERSION.SDK_INT >= 11) {
                tXLoadingLayoutBase2.setTranslationY(0.0f);
            }
            tXLoadingLayoutBase2.showAllSubViews();
            if (z) {
                ((ListView) this.mScrollContentView).setSelection(0);
                contentViewScrollTo(i2);
            }
        }
        this.mIsBeingDragged = false;
        this.mLayoutVisibilityChangesEnabled = true;
        smoothScrollTo(0);
        postDelayed(new m(this), 200);
    }

    /* access modifiers changed from: protected */
    public void onRefreshing() {
        int i2;
        TXLoadingLayoutBase tXLoadingLayoutBase;
        TXLoadingLayoutBase tXLoadingLayoutBase2 = null;
        ListAdapter adapter = ((ListView) this.mScrollContentView).getAdapter();
        if (adapter == null || adapter.isEmpty()) {
            super.onRefreshing();
            return;
        }
        if (this.mCurScrollState != TXScrollViewBase.ScrollState.ScrollState_FromStart || this.mHeaderLayout == null || this.f1190a == null) {
            i2 = 0;
            tXLoadingLayoutBase = null;
        } else {
            tXLoadingLayoutBase = this.mHeaderLayout;
            tXLoadingLayoutBase2 = this.f1190a;
            i2 = getScrollY() + this.mHeaderLayout.getContentSize();
        }
        if (tXLoadingLayoutBase2 != null) {
            tXLoadingLayoutBase2.setVisibility(0);
            tXLoadingLayoutBase2.refreshing();
        }
        if (tXLoadingLayoutBase != null) {
            tXLoadingLayoutBase.refreshing();
            if (Build.VERSION.SDK_INT >= 11 && this.i) {
                tXLoadingLayoutBase.setTranslationY((float) headerLoadingLayoutContentHeight);
            }
            tXLoadingLayoutBase.hideAllSubViews();
        }
        this.mLayoutVisibilityChangesEnabled = false;
        contentViewScrollTo(i2);
        ((ListView) this.mScrollContentView).setSelection(0);
        smoothScrollTo(0, new n(this));
    }

    /* access modifiers changed from: protected */
    public void onLoadFinish() {
        if (this.mCurScrollState == TXScrollViewBase.ScrollState.ScrollState_FromStart && this.mHeaderLayout != null && this.f1190a != null) {
            TXLoadingLayoutBase tXLoadingLayoutBase = this.mHeaderLayout;
            TXLoadingLayoutBase tXLoadingLayoutBase2 = this.f1190a;
            int contentSize = 0 - this.mHeaderLayout.getContentSize();
            boolean z = Math.abs(((ListView) this.mScrollContentView).getFirstVisiblePosition() - 0) <= 1;
            if (tXLoadingLayoutBase2 != null && tXLoadingLayoutBase2.getVisibility() == 0) {
                tXLoadingLayoutBase2.setVisibility(8);
                if (Build.VERSION.SDK_INT >= 11) {
                    tXLoadingLayoutBase.setTranslationY(0.0f);
                }
                tXLoadingLayoutBase.showAllSubViews();
                if (z) {
                    ((ListView) this.mScrollContentView).setSelection(0);
                    contentViewScrollTo(contentSize);
                }
            }
            this.mIsBeingDragged = false;
            this.mLayoutVisibilityChangesEnabled = true;
            if (this.mHeaderLayout != null) {
                this.mHeaderLayout.reset();
            }
            smoothScrollTo(0);
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
        a(true);
    }

    /* access modifiers changed from: protected */
    public void a(boolean z) {
        if (this.b != null) {
            switch (o.f1211a[this.c.ordinal()]) {
                case 1:
                    if (z) {
                        this.b.loadSuc();
                        return;
                    } else {
                        this.b.loadFail();
                        return;
                    }
                case 2:
                    int i2 = 0;
                    if (getRawAdapter() != null) {
                        i2 = getRawAdapter().getCount();
                    }
                    this.b.loadFinish(cj.b(getContext(), i2));
                    return;
                case 3:
                    this.b.refreshing();
                    return;
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ListView createScrollContentView(Context context) {
        ListView listView = new ListView(context);
        if (this.mScrollMode != TXScrollViewBase.ScrollMode.NONE) {
            this.f1190a = a(context, listView, TXScrollViewBase.ScrollMode.PULL_FROM_START);
            this.b = createLoadingLayout(context, TXScrollViewBase.ScrollMode.PULL_FROM_END);
            this.b.setVisibility(0);
            listView.addFooterView(this.b);
            listView.setOnScrollListener(this);
        }
        return listView;
    }

    /* access modifiers changed from: protected */
    public TXLoadingLayoutBase a(Context context, ListView listView, TXScrollViewBase.ScrollMode scrollMode) {
        FrameLayout frameLayout = new FrameLayout(getContext());
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, -2, 1);
        TXLoadingLayoutBase createLoadingLayout = createLoadingLayout(context, scrollMode);
        createLoadingLayout.setVisibility(8);
        frameLayout.addView(createLoadingLayout, layoutParams);
        listView.addHeaderView(frameLayout, null, false);
        return createLoadingLayout;
    }

    public void addFooterView(View view) {
        if (view != null) {
            if (this.b != null) {
                ((ListView) this.mScrollContentView).removeFooterView(this.b);
            }
            this.b = (TXLoadingLayoutBase) view;
            ((ListView) this.mScrollContentView).addFooterView(this.b);
            this.b.setVisibility(0);
            a();
        }
    }

    public void onScrollStateChanged(AbsListView absListView, int i2) {
        this.f = i2;
        if (this.g != null) {
            this.g.onScrollStateChanged(absListView, i2);
        }
        if (i2 == 0 && this.j && this.c == TXRefreshScrollViewBase.RefreshState.RESET) {
            if (this.mRefreshListViewListener != null) {
                this.mRefreshListViewListener.onTXRefreshListViewRefresh(TXScrollViewBase.ScrollState.ScrollState_FromEnd);
            }
            this.c = TXRefreshScrollViewBase.RefreshState.REFRESHING;
            a();
        }
    }

    public void onScroll(AbsListView absListView, int i2, int i3, int i4) {
        if (this.mScrollContentView != null) {
            this.j = isReadyForScrollEnd();
            if (this.g != null) {
                this.g.onScroll(absListView, i2, i3, i4);
            }
        }
    }

    /* access modifiers changed from: protected */
    public TXLoadingLayoutBase createLoadingLayout(Context context, TXScrollViewBase.ScrollMode scrollMode) {
        return new RefreshListLoading(context, TXScrollViewBase.ScrollDirection.SCROLL_DIRECTION_VERTICAL, scrollMode);
    }

    /* access modifiers changed from: protected */
    public boolean isReadyForScrollStart() {
        View childAt;
        ListAdapter adapter = ((ListView) this.mScrollContentView).getAdapter();
        if (adapter == null || adapter.isEmpty()) {
            return true;
        }
        if ((this.i && this.mRefreshState == TXRefreshScrollViewBase.RefreshState.REFRESHING) || ((ListView) this.mScrollContentView).getFirstVisiblePosition() > 1 || (childAt = ((ListView) this.mScrollContentView).getChildAt(0)) == null) {
            return false;
        }
        return childAt.getTop() >= ((ListView) this.mScrollContentView).getTop();
    }

    /* access modifiers changed from: protected */
    public boolean isReadyForScrollEnd() {
        View childAt;
        ListAdapter adapter = ((ListView) this.mScrollContentView).getAdapter();
        if (adapter == null || adapter.isEmpty()) {
            return true;
        }
        int count = ((ListView) this.mScrollContentView).getCount() - 1;
        int lastVisiblePosition = ((ListView) this.mScrollContentView).getLastVisiblePosition();
        if (lastVisiblePosition < count - 1 || (childAt = ((ListView) this.mScrollContentView).getChildAt(lastVisiblePosition - ((ListView) this.mScrollContentView).getFirstVisiblePosition())) == null) {
            return false;
        }
        return childAt.getBottom() <= ((ListView) this.mScrollContentView).getBottom();
    }

    /* access modifiers changed from: protected */
    public boolean isContentFullScreen() {
        return false;
    }

    public void setDivider(Drawable drawable) {
        ((ListView) this.mScrollContentView).setDivider(drawable);
    }

    public void setListSelector(int i2) {
        ((ListView) this.mScrollContentView).setSelector(i2);
    }

    public void setSelection(int i2) {
        ((ListView) this.mScrollContentView).setSelection(i2);
    }

    public void setAdapter(ListAdapter listAdapter) {
        if (listAdapter != null) {
            this.d = listAdapter;
            ((ListView) this.mScrollContentView).setAdapter(listAdapter);
        }
    }

    public ListAdapter getRawAdapter() {
        return this.d;
    }

    public void setSelector(Drawable drawable) {
        if (this.mScrollContentView != null && drawable != null) {
            ((ListView) this.mScrollContentView).setSelector(drawable);
        }
    }

    public int getFirstVisiblePosition() {
        if (this.mScrollContentView != null) {
            return ((ListView) this.mScrollContentView).getFirstVisiblePosition();
        }
        return -1;
    }

    public ListView getListView() {
        return (ListView) this.mScrollContentView;
    }

    public boolean isScrollStateIdle() {
        return this.f == 0;
    }

    public void setIScrollerListener(IScrollListener iScrollListener) {
        this.g = iScrollListener;
    }

    public int pointToPosition(int i2, int i3) {
        if (this.mScrollContentView != null) {
            return ((ListView) this.mScrollContentView).pointToPosition(i2, i3);
        }
        return -1;
    }
}
