package com.tencent.assistant.component;

import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.utils.ba;

/* compiled from: ProGuard */
class bg extends ApkResCallback.Stub {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ NormalErrorRecommendPage f962a;

    bg(NormalErrorRecommendPage normalErrorRecommendPage) {
        this.f962a = normalErrorRecommendPage;
    }

    public void onInstalledApkDataChanged(LocalApkInfo localApkInfo, int i) {
        if (localApkInfo != null) {
            ba.a().post(new bh(this, i, localApkInfo));
        }
    }
}
