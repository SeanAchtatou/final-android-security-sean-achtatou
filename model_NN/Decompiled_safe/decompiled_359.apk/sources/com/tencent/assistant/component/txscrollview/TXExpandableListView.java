package com.tencent.assistant.component.txscrollview;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ListAdapter;
import com.tencent.assistant.component.txscrollview.TXRefreshScrollViewBase;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.utils.cj;

/* compiled from: ProGuard */
public class TXExpandableListView extends TXScrollViewBase<ExpandableListView> implements AbsListView.OnScrollListener {
    private boolean end = false;
    protected TXLoadingLayoutBase mFooterLoadingView;
    protected TXRefreshScrollViewBase.RefreshState mGetMoreRefreshState = TXRefreshScrollViewBase.RefreshState.RESET;
    protected View mHeaderView;
    protected AbsListView.OnScrollListener mOnScrollListener = null;
    private ExpandableListAdapter mRawAdapter;
    protected ITXRefreshListViewListener mRefreshListViewListener = null;

    public TXExpandableListView(Context context) {
        super(context, TXScrollViewBase.ScrollDirection.SCROLL_DIRECTION_VERTICAL, TXScrollViewBase.ScrollMode.NONE);
    }

    public TXExpandableListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void onRefreshComplete(boolean z) {
        onRefreshComplete(z, true);
    }

    public void onRefreshComplete(boolean z, boolean z2) {
        if (this.mGetMoreRefreshState == TXRefreshScrollViewBase.RefreshState.REFRESHING) {
            if (z) {
                this.mGetMoreRefreshState = TXRefreshScrollViewBase.RefreshState.RESET;
            } else {
                this.mGetMoreRefreshState = TXRefreshScrollViewBase.RefreshState.REFRESH_LOAD_FINISH;
            }
        } else if (this.mGetMoreRefreshState == TXRefreshScrollViewBase.RefreshState.REFRESH_LOAD_FINISH) {
            this.mGetMoreRefreshState = TXRefreshScrollViewBase.RefreshState.RESET;
        } else if (this.mGetMoreRefreshState == TXRefreshScrollViewBase.RefreshState.RESET && !z) {
            this.mGetMoreRefreshState = TXRefreshScrollViewBase.RefreshState.REFRESH_LOAD_FINISH;
        }
        updateFootViewState(z2);
    }

    public void setAdapter(ExpandableListAdapter expandableListAdapter) {
        if (expandableListAdapter != null) {
            this.mRawAdapter = expandableListAdapter;
            if (this.mScrollContentView != null) {
                ((ExpandableListView) this.mScrollContentView).setAdapter(expandableListAdapter);
            }
        }
    }

    public ExpandableListAdapter getRawAdapter() {
        return this.mRawAdapter;
    }

    public ExpandableListView getContentView() {
        return (ExpandableListView) this.mScrollContentView;
    }

    public void setDivider(Drawable drawable) {
        if (this.mScrollContentView != null) {
            ((ExpandableListView) this.mScrollContentView).setDivider(drawable);
        }
    }

    public void setChildDivider(Drawable drawable) {
        ((ExpandableListView) this.mScrollContentView).setChildDivider(drawable);
    }

    public void addHeaderView(View view) {
        if (view != null) {
            if (this.mHeaderView != null) {
                ((ExpandableListView) this.mScrollContentView).removeHeaderView(this.mHeaderView);
            }
            this.mHeaderView = view;
            ((ExpandableListView) this.mScrollContentView).addHeaderView(view);
            this.mHeaderView.setVisibility(0);
        }
    }

    public void removeHeaderView() {
        if (this.mHeaderView != null) {
            ((ExpandableListView) this.mScrollContentView).removeHeaderView(this.mHeaderView);
            this.mHeaderView = null;
        }
    }

    public void addFooterView(TXLoadingLayoutBase tXLoadingLayoutBase) {
        if (tXLoadingLayoutBase != null) {
            if (this.mFooterLoadingView != null) {
                ((ExpandableListView) this.mScrollContentView).removeFooterView(this.mFooterLoadingView);
            }
            this.mFooterLoadingView = tXLoadingLayoutBase;
            ((ExpandableListView) this.mScrollContentView).addFooterView(this.mFooterLoadingView);
            this.mFooterLoadingView.setVisibility(0);
            updateFootViewState();
        }
    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener onItemClickListener) {
        ((ExpandableListView) this.mScrollContentView).setOnItemClickListener(onItemClickListener);
    }

    public void setOnChildClickListener(ExpandableListView.OnChildClickListener onChildClickListener) {
        ((ExpandableListView) this.mScrollContentView).setOnChildClickListener(onChildClickListener);
    }

    public void setOnScrollListener(AbsListView.OnScrollListener onScrollListener) {
        this.mOnScrollListener = onScrollListener;
    }

    public void setOnItemSelectedListener(AdapterView.OnItemSelectedListener onItemSelectedListener) {
        ((ExpandableListView) this.mScrollContentView).setOnItemSelectedListener(onItemSelectedListener);
    }

    public void setRefreshListViewListener(ITXRefreshListViewListener iTXRefreshListViewListener) {
        this.mRefreshListViewListener = iTXRefreshListViewListener;
    }

    /* access modifiers changed from: protected */
    public void updateFootViewState() {
        updateFootViewState(true);
    }

    /* access modifiers changed from: protected */
    public void updateFootViewState(boolean z) {
        if (this.mFooterLoadingView != null) {
            switch (d.f1200a[this.mGetMoreRefreshState.ordinal()]) {
                case 1:
                    if (z) {
                        this.mFooterLoadingView.loadSuc();
                        return;
                    } else {
                        this.mFooterLoadingView.loadFail();
                        return;
                    }
                case 2:
                    updateFootViewText();
                    return;
                case 3:
                    this.mFooterLoadingView.refreshing();
                    return;
                default:
                    return;
            }
        }
    }

    public void updateFootViewText() {
        int i = 0;
        if (getRawAdapter() != null) {
            int groupCount = getRawAdapter().getGroupCount();
            int i2 = 0;
            while (i2 < groupCount) {
                int childrenCount = getRawAdapter().getChildrenCount(i2) + i;
                i2++;
                i = childrenCount;
            }
        }
        if (this.mFooterLoadingView != null && i > 0) {
            this.mFooterLoadingView.loadFinish(cj.b(getContext(), i));
        }
    }

    /* access modifiers changed from: protected */
    public ExpandableListView createScrollContentView(Context context) {
        ExpandableListView expandableListView = new ExpandableListView(context);
        expandableListView.setDivider(null);
        expandableListView.setGroupIndicator(null);
        expandableListView.setChildDivider(null);
        if (!(this.mScrollMode == TXScrollViewBase.ScrollMode.NONE || this.mScrollMode == TXScrollViewBase.ScrollMode.NOSCROLL)) {
            this.mFooterLoadingView = new RefreshListLoading(context, TXScrollViewBase.ScrollDirection.SCROLL_DIRECTION_VERTICAL, TXScrollViewBase.ScrollMode.PULL_FROM_END);
            this.mFooterLoadingView.setVisibility(0);
            expandableListView.addFooterView(this.mFooterLoadingView);
        }
        expandableListView.setOnScrollListener(this);
        return expandableListView;
    }

    /* access modifiers changed from: protected */
    public boolean isReadyForScrollStart() {
        View childAt;
        ListAdapter adapter = ((ExpandableListView) this.mScrollContentView).getAdapter();
        if (adapter == null || adapter.isEmpty()) {
            return true;
        }
        if (((ExpandableListView) this.mScrollContentView).getFirstVisiblePosition() > 0 || (childAt = ((ExpandableListView) this.mScrollContentView).getChildAt(0)) == null) {
            return false;
        }
        return childAt.getTop() >= ((ExpandableListView) this.mScrollContentView).getTop();
    }

    /* access modifiers changed from: protected */
    public boolean isReadyForScrollEnd() {
        View childAt;
        ListAdapter adapter = ((ExpandableListView) this.mScrollContentView).getAdapter();
        if (adapter == null || adapter.isEmpty()) {
            return true;
        }
        int count = ((ExpandableListView) this.mScrollContentView).getCount() - 1;
        int lastVisiblePosition = ((ExpandableListView) this.mScrollContentView).getLastVisiblePosition();
        if (lastVisiblePosition < count || (childAt = ((ExpandableListView) this.mScrollContentView).getChildAt(lastVisiblePosition - ((ExpandableListView) this.mScrollContentView).getFirstVisiblePosition())) == null) {
            return false;
        }
        return childAt.getBottom() <= ((ExpandableListView) this.mScrollContentView).getBottom();
    }

    /* access modifiers changed from: protected */
    public boolean isContentFullScreen() {
        if (((ExpandableListView) this.mScrollContentView).getFirstVisiblePosition() > 0) {
            return true;
        }
        if (((ExpandableListView) this.mScrollContentView).getLastVisiblePosition() < ((ExpandableListView) this.mScrollContentView).getCount() - 1) {
            return true;
        }
        return false;
    }

    public void onScrollStateChanged(AbsListView absListView, int i) {
        if (this.mOnScrollListener != null) {
            this.mOnScrollListener.onScrollStateChanged(absListView, i);
        }
        if (i == 0 && this.end) {
            onEndPostion();
        }
    }

    private void onEndPostion() {
        if (this.mGetMoreRefreshState == TXRefreshScrollViewBase.RefreshState.RESET) {
            if (this.mRefreshListViewListener != null) {
                this.mRefreshListViewListener.onTXRefreshListViewRefresh(TXScrollViewBase.ScrollState.ScrollState_FromEnd);
            }
            this.mGetMoreRefreshState = TXRefreshScrollViewBase.RefreshState.REFRESHING;
            updateFootViewState();
        }
    }

    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
        if (this.mOnScrollListener != null) {
            this.mOnScrollListener.onScroll(absListView, i, i2, i3);
        }
        if (this.mScrollContentView != null) {
            this.end = isReadyForScrollEnd();
            if (this.end && !isContentFullScreen()) {
                onEndPostion();
            }
        }
    }

    public void setGroupIndicator(Drawable drawable) {
        ((ExpandableListView) this.mScrollContentView).setGroupIndicator(drawable);
    }

    public void setSelector(int i) {
        ((ExpandableListView) this.mScrollContentView).setSelector(i);
    }

    public void setOnGroupClickListener(ExpandableListView.OnGroupClickListener onGroupClickListener) {
        ((ExpandableListView) this.mScrollContentView).setOnGroupClickListener(onGroupClickListener);
    }

    public void expandGroup(int i) {
        ((ExpandableListView) this.mScrollContentView).expandGroup(i);
    }

    public long getExpandableListPosition(int i) {
        return ((ExpandableListView) this.mScrollContentView).getExpandableListPosition(i);
    }

    public int getFirstVisiblePosition() {
        return ((ExpandableListView) this.mScrollContentView).getFirstVisiblePosition();
    }

    public boolean isGroupExpanded(int i) {
        return ((ExpandableListView) this.mScrollContentView).isGroupExpanded(i);
    }

    public int pointToPosition(int i, int i2) {
        return ((ExpandableListView) this.mScrollContentView).pointToPosition(i, i2);
    }

    public View getExpandChildAt(int i) {
        return ((ExpandableListView) this.mScrollContentView).getChildAt(i);
    }

    public TXRefreshScrollViewBase.RefreshState getMoreRefreshState() {
        return this.mGetMoreRefreshState;
    }

    public void setSelection(int i) {
        ((ExpandableListView) this.mScrollContentView).setSelection(i);
    }
}
