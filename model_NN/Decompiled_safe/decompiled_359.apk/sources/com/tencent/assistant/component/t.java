package com.tencent.assistant.component;

import android.view.View;
import com.tencent.assistant.protocol.jce.CommentTagInfo;
import com.tencent.assistantv2.component.al;

/* compiled from: ProGuard */
class t implements al {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CommentDetailView f1174a;

    t(CommentDetailView commentDetailView) {
        this.f1174a = commentDetailView;
    }

    public void a(int i) {
        if (i <= 2) {
            this.f1174a.mHeaderView.setMoreImageVisible(8);
            return;
        }
        this.f1174a.mHeaderView.setMoreImageVisible(0);
        if (i > 3) {
            this.f1174a.mHeaderView.delLineViews(3);
        }
    }

    public void a(View view, CommentTagInfo commentTagInfo) {
        boolean unused = this.f1174a.clickCommentTag = true;
        this.f1174a.refreshFirstPageData(commentTagInfo);
    }
}
