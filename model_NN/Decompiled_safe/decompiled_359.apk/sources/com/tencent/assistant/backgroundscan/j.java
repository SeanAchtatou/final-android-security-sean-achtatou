package com.tencent.assistant.backgroundscan;

import com.tencent.assistant.backgroundscan.BackgroundScanManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.bt;
import com.tencent.tmsecurelite.commom.DataEntity;
import com.tencent.tmsecurelite.optimize.f;
import org.json.JSONException;

/* compiled from: ProGuard */
class j extends f {

    /* renamed from: a  reason: collision with root package name */
    long f859a = 0;
    final /* synthetic */ BackgroundScanManager b;

    j(BackgroundScanManager backgroundScanManager) {
        this.b = backgroundScanManager;
    }

    public void c() {
        XLog.i("BackgroundScan", "<scan> rubbish scan start !");
        this.f859a = 0;
    }

    public void a(int i) {
    }

    public void b() {
        a(this.f859a);
        XLog.i("BackgroundScan", "<scan> rubbish scan finish, rubbish size = " + bt.c(this.f859a));
    }

    public void a() {
        a(0L);
        XLog.i("BackgroundScan", "<scan> rubbish  scan canceled !");
    }

    public void a(int i, DataEntity dataEntity) {
        try {
            this.f859a += dataEntity.getLong("rubbish.size");
        } catch (JSONException e) {
            XLog.e("BackgroundScan", "<scan> mRubbishScanListener -> onRubbishFound", e);
        }
    }

    private void a(long j) {
        BackgroundScan a2 = this.b.a((byte) 4);
        a2.e = j;
        com.tencent.assistant.db.table.f.a().b(a2);
        this.b.i.put((byte) 4, BackgroundScanManager.SStatus.finish);
    }
}
