package com.tencent.assistant.backgroundscan;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
public class BackgroundPushReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        XLog.i("BackgroundScan", "<receiver> onReceive");
        String action = intent.getAction();
        if (action.equals("com.tencent.android.qqdownloader.background.PUSH_CLICK")) {
            XLog.i("BackgroundScan", "<receiver> click notification");
            a(intent);
        } else if (action.equals("com.tencent.android.qqdownloader.background.PUSH_CANCEL")) {
            XLog.i("BackgroundScan", "<receiver> delete notification");
            b(intent);
        }
    }

    private void a(Intent intent) {
        TemporaryThreadManager.get().start(new d(this, intent));
    }

    private void b(Intent intent) {
        TemporaryThreadManager.get().start(new e(this, intent));
    }
}
