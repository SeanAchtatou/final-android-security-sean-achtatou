package com.tencent.assistant.backgroundscan;

import com.tencent.assistant.backgroundscan.BackgroundScanManager;
import com.tencent.assistant.db.table.f;
import com.tencent.assistant.manager.spaceclean.SpaceScanManager;
import com.tencent.assistant.model.spaceclean.SubRubbishInfo;
import com.tencent.assistant.module.callback.al;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.bt;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: ProGuard */
class k extends al {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ BackgroundScanManager f860a;

    k(BackgroundScanManager backgroundScanManager) {
        this.f860a = backgroundScanManager;
    }

    public void a(long j, SubRubbishInfo subRubbishInfo) {
    }

    public void a(boolean z) {
        XLog.i("BackgroundScan", "<scan> bigfile scan cleaned !");
        a(0);
    }

    public void a(ArrayList<SubRubbishInfo> arrayList) {
        long j = 0;
        Iterator<SubRubbishInfo> it = arrayList.iterator();
        while (true) {
            long j2 = j;
            if (it.hasNext()) {
                SubRubbishInfo next = it.next();
                if (next.f1673a == SubRubbishInfo.RubbishType.BIG_FILE) {
                    j = j2 + next.c;
                } else {
                    j = j2;
                }
            } else {
                a(j2);
                XLog.i("BackgroundScan", "<scan> bigfile scan finish, bigfile size = " + bt.c(j2));
                return;
            }
        }
    }

    private void a(long j) {
        BackgroundScan a2 = this.f860a.a((byte) 5);
        a2.e = j;
        f.a().b(a2);
        this.f860a.i.put((byte) 5, BackgroundScanManager.SStatus.finish);
        SpaceScanManager.a().b(this.f860a.l);
    }
}
