package com.tencent.assistant.backgroundscan;

import android.content.Intent;
import android.net.Uri;
import com.qq.AppService.AstApp;
import com.tencent.assistant.activity.ApkMgrActivity;
import com.tencent.assistant.activity.BigFileCleanActivity;
import com.tencent.assistant.activity.SpaceCleanActivity;
import com.tencent.assistant.b.a;
import com.tencent.assistant.securescan.StartScanActivity;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistantv2.activity.AssistantTabActivity;
import com.tencent.open.SocialConstants;

/* compiled from: ProGuard */
class d implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Intent f853a;
    final /* synthetic */ BackgroundPushReceiver b;

    d(BackgroundPushReceiver backgroundPushReceiver, Intent intent) {
        this.b = backgroundPushReceiver;
        this.f853a = intent;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void run() {
        byte byteExtra = this.f853a.getByteExtra(SocialConstants.PARAM_TYPE, (byte) 0);
        if (byteExtra > 0) {
            Intent intent = new Intent();
            intent.addFlags(67108864);
            intent.addFlags(268435456);
            intent.putExtra(a.G, true);
            intent.putExtra("com.tencent.android.qqdownloader.key.PAGE_FROM", BackgroundPushReceiver.class.getSimpleName());
            intent.putExtra("notification_id", 119);
            intent.putExtra("notification_push_sub_type", 13);
            switch (byteExtra) {
                case 1:
                    XLog.d("BackgroundScan", "<receiver> will jump to 管理首页界面");
                    intent.setClass(AstApp.i(), AssistantTabActivity.class);
                    break;
                case 2:
                    XLog.d("BackgroundScan", "<receiver> will jump to 手机加速界面");
                    intent.setAction("android.intent.action.VIEW");
                    intent.setData(Uri.parse("tmast://mobileaccel"));
                    break;
                case 3:
                    XLog.d("BackgroundScan", "<receiver> will jump to 安装包管理界面");
                    intent.setClass(AstApp.i(), ApkMgrActivity.class);
                    break;
                case 4:
                    XLog.d("BackgroundScan", "<receiver> will jump to 垃圾清理界面");
                    intent.setClass(AstApp.i(), SpaceCleanActivity.class);
                    break;
                case 5:
                    XLog.d("BackgroundScan", "<receiver> will jump to 大文件扫描界面");
                    intent.setClass(AstApp.i(), BigFileCleanActivity.class);
                    break;
                case 6:
                    XLog.d("BackgroundScan", "<receiver> will jump to 病毒扫描界面");
                    intent.setClass(AstApp.i(), StartScanActivity.class);
                    break;
            }
            AstApp.i().startActivity(intent);
            a.b().a(byteExtra, 1);
            a.b().a();
            n.a().a("b_new_scan_push_click", byteExtra, this.f853a.getStringExtra("contentTitle"), this.f853a.getStringExtra("contentText"));
        }
    }
}
