package com.tencent.assistant.usercenter;

import android.content.Context;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.utils.ct;

/* compiled from: ProGuard */
public class UserCenterMsgAdapter extends BaseAdapter implements UIEventListener {

    /* renamed from: a  reason: collision with root package name */
    private Context f2607a;
    private LayoutInflater b;
    private AstApp c = AstApp.i();

    public UserCenterMsgAdapter(Context context) {
        this.f2607a = context;
        this.b = LayoutInflater.from(context);
    }

    public int getCount() {
        return 0;
    }

    public Object getItem(int i) {
        return null;
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null || view.getTag() == null || !(view.getTag() instanceof a)) {
            view = this.b.inflate((int) R.layout.item_uc_msg, (ViewGroup) null);
            a aVar = new a(this);
            aVar.f2609a = view.findViewById(R.id.root);
            aVar.b = (TextView) view.findViewById(R.id.tv_msg);
            view.setTag(aVar);
        } else {
            a aVar2 = (a) view.getTag();
        }
        view.setTag(R.id.tma_st_slot_tag, a(i));
        view.setTag(R.id.personal_tag, a(i));
        return view;
    }

    public void handleUIEvent(Message message) {
        int i = message.what;
    }

    public String a(int i) {
        return "03_" + ct.a(i + 1);
    }
}
