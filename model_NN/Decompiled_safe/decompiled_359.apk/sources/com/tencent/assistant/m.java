package com.tencent.assistant;

import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.db.table.d;
import com.tencent.assistant.module.wisedownload.e;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.ct;
import com.tencent.connect.common.Constants;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: ProGuard */
public class m {
    private static volatile m b;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public d f1462a = new d(AstApp.i());
    private ConcurrentHashMap<String, String> c = new ConcurrentHashMap<>();

    private m() {
        b();
    }

    public static synchronized m a() {
        m mVar;
        synchronized (m.class) {
            if (b == null) {
                b = new m();
            }
            mVar = b;
        }
        return mVar;
    }

    public void b() {
        this.f1462a.a(this.c);
    }

    public boolean a(String str, String str2, boolean z) {
        try {
            return Boolean.parseBoolean(a(str + "_" + str2, Boolean.valueOf(z)));
        } catch (NumberFormatException e) {
            return z;
        }
    }

    public boolean a(String str, boolean z) {
        return a(Constants.STR_EMPTY, str, z);
    }

    public int a(String str, String str2, int i) {
        try {
            return Integer.valueOf(a(str + "_" + str2, Integer.valueOf(i))).intValue();
        } catch (NumberFormatException e) {
            return i;
        }
    }

    public int a(String str, int i) {
        return a(Constants.STR_EMPTY, str, i);
    }

    public byte a(String str, String str2, byte b2) {
        try {
            return Byte.valueOf(a(str + "_" + str2, Byte.valueOf(b2))).byteValue();
        } catch (NumberFormatException e) {
            return b2;
        }
    }

    public byte a(String str, byte b2) {
        return a(Constants.STR_EMPTY, str, b2);
    }

    public long a(String str, String str2, long j) {
        try {
            return Long.valueOf(a(str + "_" + str2, Long.valueOf(j))).longValue();
        } catch (NumberFormatException e) {
            return j;
        }
    }

    public long a(String str, long j) {
        return a(Constants.STR_EMPTY, str, j);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, long):long
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String */
    public String a(String str, String str2, String str3) {
        return a(str + "_" + str2, (Object) str3);
    }

    public String a(String str, String str2) {
        return a(Constants.STR_EMPTY, str, str2);
    }

    public String a(String str, Object obj) {
        if (this.c.containsKey(str)) {
            return this.c.get(str);
        }
        return String.valueOf(obj);
    }

    public boolean a(String str, String str2, Object obj) {
        this.c.put(str + "_" + str2, String.valueOf(obj));
        if (!this.f1462a.a(str, str2, String.valueOf(obj))) {
            return this.f1462a.b(str, str2, String.valueOf(obj));
        }
        return false;
    }

    public boolean b(String str, String str2, Object obj) {
        this.c.put(str + "_" + str2, String.valueOf(obj));
        TemporaryThreadManager.get().start(new n(this, str, str2, obj));
        return true;
    }

    public boolean b(String str, Object obj) {
        return a(Constants.STR_EMPTY, str, obj);
    }

    public String c() {
        return a("channel_id", "NA");
    }

    public void a(String str) {
        b("channel_id", str);
    }

    public int d() {
        return a("server_address", -1);
    }

    public void a(int i) {
        b("server_address", Integer.valueOf(i));
    }

    public String e() {
        return "530";
    }

    public int f() {
        return a("install_request_root_dialog_times", 0);
    }

    public boolean b(int i) {
        return b("install_request_root_dialog_last_datetime", Long.valueOf(System.currentTimeMillis())) && b("install_request_root_dialog_times", Integer.valueOf(i));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean
      com.tencent.assistant.m.a(java.lang.String, long):long */
    public long g() {
        return a("install_request_root_dialog_last_datetime", 0L);
    }

    public boolean h() {
        return !TextUtils.isEmpty(a("key_permanent_root_avaliable", Constants.STR_EMPTY));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, long):long
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean */
    public boolean i() {
        return a("key_permanent_root_avaliable", false);
    }

    public boolean a(boolean z) {
        return b("key_permanent_root_avaliable", Boolean.valueOf(z));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, long):long
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean */
    public boolean j() {
        return a("request_root_permission", false);
    }

    public boolean b(boolean z) {
        return b("request_root_permission", Boolean.valueOf(z));
    }

    public void a(AppConst.ROOT_STATUS root_status) {
        b("root", Integer.valueOf(root_status.ordinal()));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, long):long
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean */
    public boolean k() {
        return a("request_auto_install", true);
    }

    public boolean c(boolean z) {
        return b("request_auto_install", Boolean.valueOf(z));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, long):long
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean */
    public boolean l() {
        return a("request_auto_del_package", true);
    }

    public boolean d(boolean z) {
        return b("request_auto_del_package", Boolean.valueOf(z));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, long):long
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean */
    public boolean m() {
        return a("key_show_wifi_tips_before_download", true);
    }

    public boolean e(boolean z) {
        return b("key_show_wifi_tips_before_download", Boolean.valueOf(z));
    }

    public AppConst.ROOT_STATUS n() {
        return AppConst.ROOT_STATUS.values()[a("root", 0)];
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, long):long
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean */
    public boolean o() {
        return a("show_float_window", false);
    }

    public boolean f(boolean z) {
        return b("show_float_window", Boolean.valueOf(z));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, long):long
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean */
    public boolean p() {
        return a("show_thumbnail", false);
    }

    public boolean g(boolean z) {
        return b("show_thumbnail", Boolean.valueOf(z));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, long):long
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean */
    public boolean q() {
        return a("software_update_prompt", true);
    }

    public boolean h(boolean z) {
        return b("software_update_prompt", Boolean.valueOf(z));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, long):long
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean */
    public boolean r() {
        return a("recommend_message_push_prompt", true);
    }

    public boolean i(boolean z) {
        return b("recommend_message_push_prompt", Boolean.valueOf(z));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, long):long
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean */
    public boolean s() {
        return a("phone_manager_push_prompt", true);
    }

    public boolean j(boolean z) {
        return b("phone_manager_push_prompt", Boolean.valueOf(z));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, long):long
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean */
    public boolean t() {
        return a("personal_message_push_prompt", true);
    }

    public boolean k(boolean z) {
        return b("personal_message_push_prompt", Boolean.valueOf(z));
    }

    public boolean l(boolean z) {
        return b("personal_freewifi_push_prompt", Boolean.valueOf(z));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, long):long
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean */
    public boolean u() {
        return a("personal_freewifi_push_prompt", true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean
      com.tencent.assistant.m.a(java.lang.String, long):long */
    public long a(byte b2) {
        return a("union_data_" + ((int) b2), -1L);
    }

    public void a(byte b2, long j) {
        b("union_data_" + ((int) b2), Long.valueOf(j));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean
      com.tencent.assistant.m.a(java.lang.String, long):long */
    public long v() {
        return a("local_plugin_list_v", -1L);
    }

    public void a(long j) {
        b("local_plugin_list_v", Long.valueOf(j));
    }

    public int b(byte b2) {
        return a("report_size_" + ((int) b2), 100);
    }

    public void a(byte b2, int i) {
        b("report_size_" + ((int) b2), Integer.valueOf(i));
    }

    public int w() {
        return a("ST_report_interval", 10800);
    }

    public void c(int i) {
        b("ST_report_interval", Integer.valueOf(i));
    }

    public byte c(byte b2) {
        return a("report_netType_" + ((int) b2), (byte) 7);
    }

    public void a(byte b2, byte b3) {
        b("report_netType_" + ((int) b2), Byte.valueOf(b3));
    }

    public void b(byte b2, int i) {
        b("socket_buffer_" + ((int) b2), Integer.valueOf(i));
    }

    public void c(byte b2, int i) {
        b("save_length_" + ((int) b2), Integer.valueOf(i));
    }

    public void d(byte b2, int i) {
        b("notify_length_" + ((int) b2), Integer.valueOf(i));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, long):long
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean */
    public boolean x() {
        return a("old_receiver_disabled", false);
    }

    public void m(boolean z) {
        b("old_receiver_disabled", Boolean.valueOf(z));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, long):long
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean */
    public boolean y() {
        return a("key_exist_continue_download", true);
    }

    public void n(boolean z) {
        b("key_exist_continue_download", Boolean.valueOf(z));
    }

    public void b(long j) {
        b("key_last_refresh_time", Long.valueOf(j));
    }

    public void a(byte b2, String str) {
        b("key_webview_sethardwareacce_modelsetting_" + ((int) b2), str);
    }

    public String d(byte b2) {
        return a("key_webview_sethardwareacce_modelsetting_" + ((int) b2), Constants.STR_EMPTY);
    }

    public void b(byte b2, String str) {
        b("key_webview_sethardwareacce_sdkidsetting_" + ((int) b2), str);
    }

    public String e(byte b2) {
        return a("key_webview_sethardwareacce_sdkidsetting_" + ((int) b2), Constants.STR_EMPTY);
    }

    public void b(String str) {
        b("key_webview_config_json", str);
    }

    public String z() {
        return a("key_webview_config_json", Constants.STR_EMPTY);
    }

    public void c(long j) {
        b("key_server_time_offset", Long.valueOf(j));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean
      com.tencent.assistant.m.a(java.lang.String, long):long */
    public long A() {
        return a("key_server_time_offset", 0L);
    }

    public void c(String str) {
        b("key_selfupdate_popInfo", str);
    }

    public String B() {
        return a("key_selfupdate_popInfo", Constants.STR_EMPTY);
    }

    public void d(String str) {
        b("key_selfupdate_exitInfo", str);
    }

    public String C() {
        return a("key_selfupdate_exitInfo", Constants.STR_EMPTY);
    }

    public String a(AppConst.WISE_DOWNLOAD_SWITCH_TYPE wise_download_switch_type) {
        if (wise_download_switch_type == AppConst.WISE_DOWNLOAD_SWITCH_TYPE.UPDATE) {
            return "key_wise_update_show_switch";
        }
        if (wise_download_switch_type == AppConst.WISE_DOWNLOAD_SWITCH_TYPE.NEW_DOWNLOAD) {
            return "key_wise_new_download_show_switch";
        }
        return null;
    }

    public String b(AppConst.WISE_DOWNLOAD_SWITCH_TYPE wise_download_switch_type) {
        if (wise_download_switch_type == AppConst.WISE_DOWNLOAD_SWITCH_TYPE.UPDATE) {
            return "key_wised_update_control_switch";
        }
        if (wise_download_switch_type == AppConst.WISE_DOWNLOAD_SWITCH_TYPE.NEW_DOWNLOAD) {
            return "key_wised_new_download_control_switch";
        }
        return null;
    }

    public String c(AppConst.WISE_DOWNLOAD_SWITCH_TYPE wise_download_switch_type) {
        if (wise_download_switch_type == AppConst.WISE_DOWNLOAD_SWITCH_TYPE.UPDATE) {
            return "key_is_wise_update_switch_2_server";
        }
        if (wise_download_switch_type == AppConst.WISE_DOWNLOAD_SWITCH_TYPE.NEW_DOWNLOAD) {
            return "key_is_wise_new_download_switch_2_server";
        }
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, long):long
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean */
    public boolean d(AppConst.WISE_DOWNLOAD_SWITCH_TYPE wise_download_switch_type) {
        String a2 = a(wise_download_switch_type);
        if (a2 == null) {
            return false;
        }
        return a(a2, false);
    }

    public boolean a(AppConst.WISE_DOWNLOAD_SWITCH_TYPE wise_download_switch_type, boolean z) {
        int i = 0;
        String a2 = a(wise_download_switch_type);
        if (a2 == null) {
            return false;
        }
        int a3 = a(b(wise_download_switch_type), 0);
        if (a3 == 0 || a3 == 1) {
            if (z) {
                i = 1;
            }
            if (i != a3) {
                b(b(wise_download_switch_type), Integer.valueOf(i));
            }
        }
        return b(a2, Boolean.valueOf(z));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, long):long
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean */
    public boolean e(AppConst.WISE_DOWNLOAD_SWITCH_TYPE wise_download_switch_type) {
        String c2 = c(wise_download_switch_type);
        if (c2 == null) {
            return false;
        }
        return a(c2, true);
    }

    public boolean b(AppConst.WISE_DOWNLOAD_SWITCH_TYPE wise_download_switch_type, boolean z) {
        String c2 = c(wise_download_switch_type);
        if (c2 == null) {
            return false;
        }
        return b(c2, Boolean.valueOf(z));
    }

    public boolean f(AppConst.WISE_DOWNLOAD_SWITCH_TYPE wise_download_switch_type) {
        boolean z = true;
        String b2 = b(wise_download_switch_type);
        if (b2 == null) {
            return false;
        }
        if (a(b2, 0) != 1) {
            z = false;
        }
        return z;
    }

    public void a(AppConst.WISE_DOWNLOAD_SWITCH_TYPE wise_download_switch_type, int i) {
        boolean z = true;
        String b2 = b(wise_download_switch_type);
        if (b2 != null) {
            if (i == 0 || i == 1 || i == 2) {
                if (i == 0 || i == 1) {
                    if (i != 1) {
                        z = false;
                    }
                    a(wise_download_switch_type, z);
                }
                b(b2, Integer.valueOf(i));
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, long):long
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean */
    public boolean D() {
        return a("key_is_auto_download_tips_show", false);
    }

    public boolean o(boolean z) {
        return b("key_is_auto_download_tips_show", Boolean.valueOf(z));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean
      com.tencent.assistant.m.a(java.lang.String, long):long */
    public long E() {
        return a("key_ying_yong_bao_latest_launched_time", 0L);
    }

    public boolean a(long j, int i) {
        return b("key_rec_download_last_time_" + j, Integer.valueOf(i));
    }

    public int d(long j) {
        return a("key_rec_download_last_time_" + j, 0);
    }

    public boolean d(int i) {
        return b("key_rec_download_show_last_dialog_time", Integer.valueOf(i));
    }

    public int F() {
        return a("key_rec_download_show_last_dialog_time", 0);
    }

    public boolean a(Long l, int i) {
        return b("key_rec_popup_shown_times_" + l, Integer.valueOf(i));
    }

    public int e(long j) {
        return a("key_rec_popup_shown_times_" + j, 0);
    }

    public boolean b(Long l, int i) {
        return b("key_rec_popup_current_month_" + l, Integer.valueOf(i));
    }

    public int f(long j) {
        return a("key_rec_popup_current_month_" + j, 0);
    }

    public void a(byte[] bArr) {
        if (!this.f1462a.a(Constants.STR_EMPTY, "key_rec_download_config", Constants.STR_EMPTY, bArr)) {
            this.f1462a.b(Constants.STR_EMPTY, "key_rec_download_config", Constants.STR_EMPTY, bArr);
        }
    }

    public byte[] G() {
        return this.f1462a.d(Constants.STR_EMPTY, "key_new_rec_download_config");
    }

    public void b(byte[] bArr) {
        if (!this.f1462a.a(Constants.STR_EMPTY, "key_new_rec_download_config", Constants.STR_EMPTY, bArr)) {
            this.f1462a.b(Constants.STR_EMPTY, "key_new_rec_download_config", Constants.STR_EMPTY, bArr);
        }
    }

    public void c(byte[] bArr) {
        if (!this.f1462a.a(Constants.STR_EMPTY, "key_call_app_after_install_config", Constants.STR_EMPTY, bArr)) {
            this.f1462a.b(Constants.STR_EMPTY, "key_call_app_after_install_config", Constants.STR_EMPTY, bArr);
        }
    }

    public byte[] H() {
        return this.f1462a.d(Constants.STR_EMPTY, "key_call_app_after_install_config");
    }

    public void d(byte[] bArr) {
        if (!this.f1462a.a(Constants.STR_EMPTY, "key_get_apk_auto_open_action", Constants.STR_EMPTY, bArr)) {
            this.f1462a.b(Constants.STR_EMPTY, "key_get_apk_auto_open_action", Constants.STR_EMPTY, bArr);
        }
    }

    public void e(byte[] bArr) {
        if (!this.f1462a.a(Constants.STR_EMPTY, "key_local_manage_push_config", Constants.STR_EMPTY, bArr)) {
            this.f1462a.b(Constants.STR_EMPTY, "key_local_manage_push_config", Constants.STR_EMPTY, bArr);
        }
    }

    public void p(boolean z) {
        b("key_local_manage_push_config_changed", Boolean.valueOf(z));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, long):long
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean */
    public boolean I() {
        return a("key_local_manage_push_config_changed", false);
    }

    public byte[] J() {
        return this.f1462a.d(Constants.STR_EMPTY, "key_local_manage_push_config");
    }

    public void a(String str, byte[] bArr) {
        TemporaryThreadManager.get().start(new o(this, str, bArr));
    }

    public byte[] e(String str) {
        return this.f1462a.d(Constants.STR_EMPTY, str);
    }

    public byte[] e(int i) {
        return e("key_navi_type_" + i);
    }

    public void a(int i, byte[] bArr) {
        a("key_navi_type_" + i, bArr);
    }

    public void f(byte[] bArr) {
        a("key_game_navi_type", bArr);
    }

    public byte[] K() {
        return e("key_game_navi_type");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, long):long
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean */
    public boolean L() {
        return a("key_rubbish_short_cut_switch", false);
    }

    public void q(boolean z) {
        b("key_rubbish_short_cut_switch", Boolean.valueOf(z));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, long):long
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean */
    public boolean M() {
        return a("key_rubbish_short_cut_create", false);
    }

    public void r(boolean z) {
        b("key_rubbish_short_cut_create", Boolean.valueOf(z));
    }

    public byte[] N() {
        return this.f1462a.d(Constants.STR_EMPTY, "key_notify_card");
    }

    public void g(byte[] bArr) {
        if (!this.f1462a.a(Constants.STR_EMPTY, "key_notify_card", Constants.STR_EMPTY, bArr)) {
            this.f1462a.b(Constants.STR_EMPTY, "key_notify_card", Constants.STR_EMPTY, bArr);
        }
    }

    public void O() {
        b("key_app_treasure_box_show_time", Long.valueOf(System.currentTimeMillis()));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean
      com.tencent.assistant.m.a(java.lang.String, long):long */
    public long P() {
        return a("key_app_treasure_box_show_time", 0L);
    }

    public void Q() {
        b("key_app_treasure_box_exit_dialog_show_times", Integer.valueOf(a("key_app_treasure_box_exit_dialog_show_times", 0) + 1));
    }

    public long R() {
        return (long) a("key_app_treasure_box_exit_dialog_show_times", 0);
    }

    public int S() {
        return a("key_wise_upload_diff_related_file", 0);
    }

    public void f(int i) {
        b("key_wise_upload_diff_related_file", Integer.valueOf(i));
    }

    public void f(String str) {
        b("hottab_download_filtertip_times", str);
    }

    public String T() {
        return a("hottab_download_filtertip_times", Constants.STR_EMPTY);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, long):long
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean */
    public boolean U() {
        return a("hottab_filter_setting", false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean
      com.tencent.assistant.m.a(java.lang.String, long):long */
    public long V() {
        return a("key_go_background_time", 0L);
    }

    public void g(long j) {
        b("key_go_background_time", Long.valueOf(j));
    }

    public void s(boolean z) {
        b("hottab_filter_setting", Boolean.valueOf(z));
    }

    public int W() {
        return a("key_wifi_auto_download_install_task_screenon", 5);
    }

    public int X() {
        return a("key_wifi_auto_download_install_task_screenoff", 3);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, long):long
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean */
    public boolean Y() {
        return a("key_switch_app_download_key_is_uesed", false);
    }

    public void t(boolean z) {
        b("key_switch_app_download_key_is_uesed", Boolean.valueOf(z));
    }

    public void a(e eVar) {
        if (eVar == null) {
            this.f1462a.a("key_i_w_u_i", Constants.STR_EMPTY);
            return;
        }
        String str = eVar.f1912a + "|" + eVar.b + "|" + eVar.c;
        if (!this.f1462a.a("key_i_w_u_i", str)) {
            this.f1462a.b("key_i_w_u_i", str);
        }
    }

    public e Z() {
        String[] b2;
        String a2 = this.f1462a.a("key_i_w_u_i");
        if (TextUtils.isEmpty(a2) || (b2 = ct.b(a2, "|")) == null || b2.length != 3) {
            return null;
        }
        e eVar = new e();
        eVar.f1912a = b2[0];
        eVar.b = b2[1];
        eVar.c = ct.d(b2[2]);
        return eVar;
    }

    public boolean aa() {
        String a2 = this.f1462a.a("key_r_i_u");
        if (a2 == null || !a2.equals("no")) {
            return true;
        }
        return false;
    }

    public void ab() {
        if (!this.f1462a.a("key_r_i_u", "no")) {
            this.f1462a.b("key_r_i_u", "no");
        }
    }

    public void h(long j) {
        b("key_wifi_auto_download_bookingdownload_pushtime", Long.valueOf(j));
    }

    public void g(String str) {
        b("key_wifi_auto_download_bookingdownload_cachedTicket", str);
    }

    public String ac() {
        return a("key_wifi_auto_download_bookingdownload_cachedTicket", Constants.STR_EMPTY);
    }

    public void i(long j) {
        b("key_wifi_auto_download_subscriptionownload_pushtime", Long.valueOf(j));
    }

    public void h(String str) {
        b("key_wifi_auto_download_subscriptiondownload_cachedTicket", str);
    }

    public String ad() {
        return a("key_wifi_auto_download_subscriptiondownload_cachedTicket", Constants.STR_EMPTY);
    }

    public void g(int i) {
        b("key_system_install_result_check_interval", Integer.valueOf(i));
    }

    public int ae() {
        return a("key_system_install_result_check_interval", 360);
    }

    public void u(boolean z) {
        b("key_download_start_check_apk_file_size", Boolean.valueOf(z));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, long):long
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean */
    public boolean af() {
        return a("key_download_start_check_apk_file_size", true);
    }

    public void v(boolean z) {
        b("key_download_start_check_patch_file_size", Boolean.valueOf(z));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, long):long
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean */
    public boolean ag() {
        return a("key_download_start_check_patch_file_size", true);
    }

    public long ah() {
        return ((long) a("home_page_interval", 3600)) * 1000;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, long):long
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean */
    public boolean ai() {
        return a("key_hide_installed_apps_first_time", true);
    }

    public void w(boolean z) {
        b("key_hide_installed_apps_first_time", Boolean.valueOf(z));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, long):long
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean */
    public boolean aj() {
        return a("key_show_other_page_hide_installed_apps_is_first_time", true);
    }

    public void x(boolean z) {
        b("key_show_other_page_hide_installed_apps_is_first_time", Boolean.valueOf(z));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, long):long
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean */
    public boolean ak() {
        return a("key_show_other_page_hide_installed_apps_first_time_rank", false);
    }

    public void y(boolean z) {
        b("key_show_other_page_hide_installed_apps_first_time_rank", Boolean.valueOf(z));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, long):long
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean */
    public boolean al() {
        return a("key_show_other_page_hide_installed_apps_first_time_app_game", false);
    }

    public void z(boolean z) {
        b("key_show_other_page_hide_installed_apps_first_time_app_game", Boolean.valueOf(z));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean
      com.tencent.assistant.m.a(java.lang.String, long):long */
    public long am() {
        return a("key_installd_auto_open_interval", 0L);
    }

    public void h(int i) {
        b("key_appa_updatelist_request_fail_retry_max_times", Integer.valueOf(i));
    }

    public void j(long j) {
        b("key_installd_auto_open_interval", Long.valueOf(j));
    }

    public int an() {
        return a("key_appa_updatelist_request_fail_retry_max_times", 0);
    }

    public void i(int i) {
        b("key_appa_updatelist_request_fail_retry_current_times", Integer.valueOf(i));
    }

    public int ao() {
        return a("key_appa_updatelist_request_fail_retry_current_times", 0);
    }

    public void a(long j, int i, int i2, int i3) {
        b("KEY_AUTO_DOWNLOAD_PUSH_COUNT_STATS", j + "|" + i + "|" + i2 + "|" + i3);
    }

    public Object[] ap() {
        String[] split;
        String a2 = a("KEY_AUTO_DOWNLOAD_PUSH_COUNT_STATS", (Object) null);
        if (a2 == null || (split = a2.split("\\|")) == null || split.length != 4) {
            return null;
        }
        Object[] objArr = new Object[split.length];
        try {
            objArr[0] = Long.valueOf(split[0]);
            objArr[1] = Integer.valueOf(split[1]);
            objArr[2] = Integer.valueOf(split[2]);
            objArr[3] = Integer.valueOf(split[3]);
            return objArr;
        } catch (NumberFormatException e) {
            return null;
        }
    }
}
