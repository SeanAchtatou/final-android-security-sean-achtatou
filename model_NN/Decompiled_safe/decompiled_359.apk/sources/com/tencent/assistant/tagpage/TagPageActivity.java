package com.tencent.assistant.tagpage;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AbsListView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.LoadingView;
import com.tencent.assistant.component.NormalErrorRecommendPage;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.manager.NetworkMonitor;
import com.tencent.assistant.manager.cq;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.u;
import com.tencent.assistant.net.APN;
import com.tencent.assistant.net.c;
import com.tencent.assistant.protocol.jce.AppTagInfo;
import com.tencent.assistant.protocol.jce.CardItem;
import com.tencent.assistant.protocol.jce.GetAppFromTagResponse;
import com.tencent.assistant.protocol.scu.RequestResponePair;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ba;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.STPageInfo;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class TagPageActivity extends BaseActivity implements View.OnClickListener, AbsListView.OnScrollListener, NetworkMonitor.ConnectivityChangeListener, u, v {
    private static int K = 29;
    public static int n = -1;
    private ArrayList<SimpleAppModel> A = new ArrayList<>();
    /* access modifiers changed from: private */
    public TagPageCardAdapter B = null;
    private b C = null;
    private TagPageHeaderView D = null;
    private String E = Constants.STR_EMPTY;
    private String F = Constants.STR_EMPTY;
    private String G = Constants.STR_EMPTY;
    private String H = Constants.STR_EMPTY;
    private String I = Constants.STR_EMPTY;
    private String J = Constants.STR_EMPTY;
    private int L = 0;
    private boolean M = true;
    private boolean N = false;
    private ArrayList<AppTagInfo> O = new ArrayList<>();
    private Bitmap P = null;
    private boolean Q = false;
    private Context t = null;
    private LoadingView u = null;
    /* access modifiers changed from: private */
    public NormalErrorRecommendPage v = null;
    private SecondNavigationTitleViewV5 w = null;
    private TagPageListView x = null;
    private int y = 0;
    private int z = 0;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        i();
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setFormat(-3);
        this.t = this;
        setContentView((int) R.layout.tag_page_activity_layout);
        j();
        v();
        w();
        this.C = new b();
        this.C.register(this);
        x();
        cq.a().a((NetworkMonitor.ConnectivityChangeListener) this);
        a(STConst.ST_DEFAULT_SLOT, Constants.STR_EMPTY, 100);
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        XLog.i("TagPageActivity", "*** onNewIntent ***");
        super.onNewIntent(intent);
    }

    private void i() {
        Intent intent = getIntent();
        this.E = intent.getStringExtra("tagID");
        this.F = intent.getStringExtra("tagName");
        this.G = intent.getStringExtra("appID");
        this.H = intent.getStringExtra("pkgName");
        this.I = intent.getStringExtra("tagSubTitle");
        this.J = intent.getStringExtra("firstIconUrl");
        XLog.i("TagPageActivity", "mTagId = " + this.E + ", mFirstIconUrl = " + this.J);
    }

    private void j() {
        this.u = (LoadingView) findViewById(R.id.loading_view);
        this.w = (SecondNavigationTitleViewV5) findViewById(R.id.title);
        this.w.b(this.F);
        this.w.c(0);
        this.w.c(false);
        this.w.i();
        this.w.c(this);
        this.w.a((Activity) this);
        this.B = new TagPageCardAdapter(this, this.A);
        this.B.a(this);
        this.x = (TagPageListView) findViewById(R.id.list);
        this.D = new TagPageHeaderView(this.t, this, this.J);
        this.x.addHeaderView(this.D);
        this.x.setOnScrollListener(this);
        this.x.setDivider(null);
        this.x.setSelector(new ColorDrawable(0));
        this.x.setCacheColorHint(17170445);
        this.x.addClickLoadMore();
        this.x.setAdapter(this.B);
        this.x.a(false);
        b(true);
    }

    private void v() {
        if (this.D != null) {
            this.D.a(this.F);
            this.D.b(this.I);
        }
    }

    private void w() {
        this.v = (NormalErrorRecommendPage) findViewById(R.id.error_page);
        this.v.setButtonClickListener(new h(this));
        this.v.setIsAutoLoading(true);
    }

    /* access modifiers changed from: private */
    public void b(boolean z2) {
        if (this.u != null) {
            this.u.setVisibility(z2 ? 0 : 8);
        }
    }

    private void b(int i) {
        if (this.v != null) {
            this.v.setErrorType(i);
            this.v.setVisibility(0);
            this.u.setVisibility(8);
        }
    }

    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
        if (this.y <= 0 && this.w != null) {
            this.y = this.w.getHeight();
        }
        if (this.z <= 0 && absListView.getChildAt(0) != null) {
            this.z = absListView.getChildAt(0).getHeight();
            XLog.i("TagPageActivity", "[onScroll] ---> nHeaderViewHeight = " + this.z);
        }
        if (i == 0 && (absListView.getChildAt(0) instanceof TagPageHeaderView)) {
            int top = ((TagPageHeaderView) absListView.getChildAt(0)).getTop() * -1;
            int i4 = this.z - this.y;
            int i5 = this.z / 3;
            if (top >= i5 && top <= i4) {
                float f = ((float) (top - i5)) * 255.0f;
                if (i4 != i5) {
                    i4 -= i5;
                }
                this.w.c((int) (f / ((float) i4)));
                if (this.Q) {
                    this.Q = false;
                    this.w.c(false);
                }
            } else if (top > i4) {
                this.w.c(255);
                if (!this.Q) {
                    this.Q = true;
                    this.w.c(true);
                }
            } else {
                this.w.c(0);
                if (this.Q) {
                    this.Q = false;
                    this.w.c(false);
                }
            }
        } else if (i >= 1) {
            this.w.c(255);
            if (!this.Q) {
                this.Q = true;
                this.w.c(true);
            }
        }
    }

    public void onScrollStateChanged(AbsListView absListView, int i) {
        if (i == 0) {
            if (n < absListView.getFirstVisiblePosition() - 1 || n > absListView.getLastVisiblePosition() + 1) {
                XLog.e("TagPageActivity", "*** stopVideoPlay ***");
                this.B.a();
            }
            if (absListView.getLastVisiblePosition() == absListView.getCount() - 1 && this.C != null) {
                if (this.M) {
                    x();
                } else {
                    this.x.a();
                }
            }
            if (c.d()) {
                ba.a().postDelayed(new i(this, absListView), 100);
                return;
            }
            return;
        }
        if (i == 2) {
        }
    }

    public void onWindowFocusChanged(boolean z2) {
        if (this.y <= 0 && this.w != null) {
            this.y = this.w.getHeight();
            XLog.i("TagPageActivity", "[onWindowFocusChanged] ---> naviTitle.getHeight() = " + this.w.getHeight());
        }
        if (this.z <= 0 && this.x.getChildAt(0) != null) {
            this.z = this.x.getChildAt(0).getHeight();
            XLog.i("TagPageActivity", "[onWindowFocusChanged] ---> nHeaderViewHeight = " + this.z);
        }
        super.onWindowFocusChanged(z2);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        getWindow().setFormat(-3);
        if (this.w != null) {
            this.w.l();
        }
        this.B.notifyDataSetChanged();
        this.B.b();
        super.onResume();
    }

    /* access modifiers changed from: private */
    public void x() {
        if (this.G == null || this.H == null) {
            this.C.a(this.E, this.F, 1, "1", this.L, this.L + K);
        } else {
            this.C.a(this.E, this.F, (long) Integer.parseInt(this.G), this.H, this.L, this.L + K);
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        if (this.w != null) {
            this.w.m();
        }
        if (this.B != null) {
            this.B.c();
        }
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        cq.a().b(this);
        if (this.B != null) {
            this.B.d();
        }
        if (this.C != null) {
            this.C.unregister(this);
        }
        if (this.P != null && !this.P.isRecycled()) {
            this.P.recycle();
            this.P = null;
        }
        XLog.i("TagPageActivity", "*** onDestroy ***");
        super.onDestroy();
    }

    public void a(int i, int i2, List<RequestResponePair> list) {
        XLog.i("TagPageActivity", "*** onNotifyUISucceed ***");
        b(false);
        this.x.a(true);
        if (list == null || list.size() <= 0) {
            XLog.i("TagPageActivity", "[GetTagPageEngine] ---> onRequestSuccessed (error)");
        } else {
            XLog.i("TagPageActivity", "[GetTagPageEngine] ---> onRequestSuccessed, (responses.size() = " + list.size() + ")");
            this.O.clear();
            for (RequestResponePair next : list) {
                if (next.response instanceof GetAppFromTagResponse) {
                    GetAppFromTagResponse getAppFromTagResponse = (GetAppFromTagResponse) next.response;
                    XLog.i("TagPageActivity", "@@@ appResponse.totalResult = " + getAppFromTagResponse.e);
                    XLog.i("TagPageActivity", "@@@ appResponse.title = " + getAppFromTagResponse.b);
                    this.L += K + 1;
                    if (this.L < getAppFromTagResponse.e) {
                        this.M = true;
                        this.x.onRefreshComplete(this.M, true);
                    } else {
                        this.M = false;
                        this.x.onRefreshComplete(this.M, true);
                    }
                    Iterator<CardItem> it = getAppFromTagResponse.c.iterator();
                    while (it.hasNext()) {
                        CardItem next2 = it.next();
                        if (next2 != null) {
                            SimpleAppModel a2 = u.a(next2);
                            if (a2 != null && Build.VERSION.SDK_INT < 11) {
                                a2.aF = Constants.STR_EMPTY;
                            }
                            this.A.add(a2);
                        }
                    }
                    if (getAppFromTagResponse.f != null) {
                        this.O.addAll(getAppFromTagResponse.f);
                    }
                    if (!TextUtils.isEmpty(getAppFromTagResponse.b)) {
                        this.F = getAppFromTagResponse.b;
                    }
                    if (!TextUtils.isEmpty(getAppFromTagResponse.h)) {
                        this.I = getAppFromTagResponse.h;
                    }
                }
            }
            if (this.B != null) {
                this.B.a(this.A);
                this.B.notifyDataSetChanged();
            }
            if (this.D != null) {
                this.D.a(this.O);
                if (!TextUtils.isEmpty(this.F)) {
                    this.D.a(this.F);
                }
                if (!TextUtils.isEmpty(this.I)) {
                    this.D.b(this.I);
                }
            }
        }
        XLog.i("TagPageActivity", ">>> mAppInfos.size() = " + this.A.size());
    }

    public void b(int i, int i2, List<RequestResponePair> list) {
        XLog.i("TagPageActivity", "*** onNotifyUIFailed ***");
        if (!c.a()) {
            b(30);
        } else {
            b(20);
        }
        if (this.B != null) {
            this.B.a(new ArrayList());
            this.B.notifyDataSetChanged();
        }
        if (this.x != null) {
            this.x.a(false);
        }
    }

    public void onClick(View view) {
        String str;
        String str2;
        String str3;
        switch (view.getId()) {
            case R.id.setting_network /*2131166196*/:
                XLog.i("TagPageActivity", "setting_network");
                return;
            case R.id.back_layout_all /*2131166330*/:
                finish();
                return;
            case R.id.tv_tag_left /*2131166465*/:
                if (this.O != null && this.O.size() > 0) {
                    if (this.A == null || this.A.size() <= 0) {
                        str3 = null;
                    } else {
                        str3 = this.A.get(0).e;
                    }
                    w.a(this.t, this.O.get(0), str3);
                }
                a("06_001", Constants.STR_EMPTY, 200);
                finish();
                return;
            case R.id.tv_tag_middle /*2131166466*/:
                if (this.O != null && this.O.size() > 1) {
                    if (this.A == null || this.A.size() <= 0) {
                        str2 = null;
                    } else {
                        str2 = this.A.get(0).e;
                    }
                    w.a(this.t, this.O.get(1), str2);
                }
                a("06_002", Constants.STR_EMPTY, 200);
                finish();
                return;
            case R.id.tv_tag_right /*2131166467*/:
                if (this.O != null && this.O.size() > 2) {
                    if (this.A == null || this.A.size() <= 0) {
                        str = null;
                    } else {
                        str = this.A.get(0).e;
                    }
                    w.a(this.t, this.O.get(2), str);
                }
                a("06_003", Constants.STR_EMPTY, 200);
                finish();
                return;
            default:
                return;
        }
    }

    public void a(TXImageView tXImageView, Bitmap bitmap, int i) {
        if (!this.N && this.D != null && bitmap != null && i == 0) {
            XLog.i("TagPageActivity", "[TagPageActivity] ---> updateBackground");
            this.N = true;
            if (this.P != null && !this.P.isRecycled()) {
                this.P.recycle();
                this.P = null;
            }
            try {
                this.P = a.a(bitmap, bitmap.getWidth(), bitmap.getHeight());
            } catch (OutOfMemoryError e) {
                cq.a().b();
            }
            if (this.P != null && !this.P.isRecycled()) {
                this.D.a(this.P);
                this.D.invalidate();
            }
        }
    }

    public int f() {
        return STConst.ST_TAG_DETAIL_PAGE;
    }

    public STPageInfo q() {
        this.o.f3358a = STConst.ST_TAG_DETAIL_PAGE;
        return this.o;
    }

    public void a(String str, String str2, int i) {
        XLog.i("TagPageActivity", "[logReport] ---> actionId = " + i + ", slotId = " + str + ", extraData = " + str2);
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this, i);
        buildSTInfo.slotId = str;
        buildSTInfo.extraData = str2;
        k.a(buildSTInfo);
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 4 && keyEvent.getRepeatCount() == 0) {
            a("03_001", Constants.STR_EMPTY, 200);
        }
        return super.onKeyDown(i, keyEvent);
    }

    public void onConnected(APN apn) {
    }

    public void onDisconnected(APN apn) {
    }

    public void onConnectivityChanged(APN apn, APN apn2) {
        XLog.i("TagPageActivity", "*** onConnectivityChanged ***");
        if (this.B != null) {
            this.B.notifyDataSetChanged();
        }
    }
}
