package com.tencent.assistant.thumbnailCache;

import com.tencent.assistant.utils.FileUtil;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

/* compiled from: ProGuard */
public class d {

    /* renamed from: a  reason: collision with root package name */
    protected File f2570a;
    private final AtomicLong b;
    private final int c;
    private Map<String, Long> d = Collections.synchronizedMap(new HashMap());
    private ArrayList<String> e = new ArrayList<>();
    private boolean f = false;

    public d(File file, int i) {
        this.f2570a = file;
        this.c = i;
        this.b = new AtomicLong();
        a();
    }

    public void a() {
        e();
        d();
        f();
    }

    private boolean c() {
        boolean z = false;
        long j = 0;
        File[] listFiles = this.f2570a.listFiles();
        if (listFiles != null) {
            for (File file : listFiles) {
                if (!file.getPath().endsWith(".nomedia")) {
                    j += (long) b(file.getPath());
                    if (this.d.get(file.getPath()) == null) {
                        this.d.put(file.getPath(), Long.valueOf(file.lastModified()));
                        z = true;
                    }
                }
            }
            this.b.set(j);
        }
        return z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00a6, code lost:
        r1 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:?, code lost:
        r7.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:?, code lost:
        r0.b.set(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x00e1, code lost:
        r1 = th;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00a5 A[ExcHandler: Exception (e java.lang.Exception), Splitter:B:4:0x002f] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x00ce A[SYNTHETIC, Splitter:B:55:0x00ce] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x00d3  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean d() {
        /*
            r20 = this;
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r0 = r20
            java.io.File r2 = r0.f2570a
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "/.dirinfo"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = r1.toString()
            r5 = 0
            r4 = 1
            r1 = 0
            r0 = r20
            java.util.Map<java.lang.String, java.lang.Long> r10 = r0.d
            monitor-enter(r10)
            java.io.DataInputStream r7 = new java.io.DataInputStream     // Catch:{ Exception -> 0x00e6, all -> 0x00c9 }
            java.io.BufferedInputStream r3 = new java.io.BufferedInputStream     // Catch:{ Exception -> 0x00e6, all -> 0x00c9 }
            java.io.FileInputStream r8 = new java.io.FileInputStream     // Catch:{ Exception -> 0x00e6, all -> 0x00c9 }
            r8.<init>(r2)     // Catch:{ Exception -> 0x00e6, all -> 0x00c9 }
            r3.<init>(r8)     // Catch:{ Exception -> 0x00e6, all -> 0x00c9 }
            r7.<init>(r3)     // Catch:{ Exception -> 0x00e6, all -> 0x00c9 }
            int r8 = r7.readInt()     // Catch:{ Exception -> 0x00a5, all -> 0x00e1 }
            r3 = 0
            r1 = 0
            r9 = r8
            r15 = r3
            r16 = r4
            r4 = r15
            r17 = r1
            r2 = r17
            r1 = r16
        L_0x0041:
            if (r9 == 0) goto L_0x00b7
            if (r1 == 0) goto L_0x00b7
            r8 = 2
            r15 = r2
            r3 = r4
            r4 = r1
            r1 = r15
        L_0x004a:
            if (r8 == 0) goto L_0x00ea
            if (r4 == 0) goto L_0x00ea
            int r11 = r7.readInt()     // Catch:{ Exception -> 0x00a5, all -> 0x00e1 }
            int r12 = r7.readInt()     // Catch:{ Exception -> 0x00a5, all -> 0x00e1 }
            if (r12 > 0) goto L_0x0090
            r4 = 0
            r8 = r4
        L_0x005a:
            if (r8 == 0) goto L_0x00e8
            r11 = 0
            long r13 = com.tencent.assistant.utils.FileUtil.getFileLastModified(r3)     // Catch:{ Exception -> 0x00a5, all -> 0x00e3 }
            int r4 = (r11 > r13 ? 1 : (r11 == r13 ? 0 : -1))
            if (r4 == 0) goto L_0x00e8
            r0 = r20
            java.util.Map<java.lang.String, java.lang.Long> r4 = r0.d     // Catch:{ Exception -> 0x00a5, all -> 0x00e3 }
            java.lang.Long r11 = java.lang.Long.valueOf(r1)     // Catch:{ Exception -> 0x00a5, all -> 0x00e3 }
            r4.put(r3, r11)     // Catch:{ Exception -> 0x00a5, all -> 0x00e3 }
            r0 = r20
            java.util.ArrayList<java.lang.String> r4 = r0.e     // Catch:{ Exception -> 0x00a5, all -> 0x00e3 }
            r4.add(r3)     // Catch:{ Exception -> 0x00a5, all -> 0x00e3 }
            r0 = r20
            int r4 = r0.b(r3)     // Catch:{ Exception -> 0x00a5, all -> 0x00e3 }
            long r11 = (long) r4
            long r4 = r5 + r11
        L_0x0081:
            int r6 = r9 + -1
            r9 = r6
            r15 = r3
            r16 = r1
            r2 = r16
            r1 = r8
            r18 = r4
            r5 = r18
            r4 = r15
            goto L_0x0041
        L_0x0090:
            switch(r11) {
                case 0: goto L_0x0097;
                case 1: goto L_0x00b2;
                default: goto L_0x0093;
            }
        L_0x0093:
            r4 = 0
        L_0x0094:
            int r8 = r8 + -1
            goto L_0x004a
        L_0x0097:
            byte[] r11 = new byte[r12]     // Catch:{ Exception -> 0x00a5, all -> 0x00e1 }
            int r13 = r7.read(r11)     // Catch:{ Exception -> 0x00a5, all -> 0x00e1 }
            if (r12 != r13) goto L_0x00b0
            java.lang.String r3 = new java.lang.String     // Catch:{ Exception -> 0x00a5, all -> 0x00e1 }
            r3.<init>(r11)     // Catch:{ Exception -> 0x00a5, all -> 0x00e1 }
            goto L_0x0094
        L_0x00a5:
            r1 = move-exception
            r1 = r7
        L_0x00a7:
            r2 = 0
            if (r1 == 0) goto L_0x00ad
            r1.close()     // Catch:{ IOException -> 0x00dd }
        L_0x00ad:
            r1 = r2
        L_0x00ae:
            monitor-exit(r10)     // Catch:{ all -> 0x00c6 }
            return r1
        L_0x00b0:
            r4 = 0
            goto L_0x0094
        L_0x00b2:
            long r1 = r7.readLong()     // Catch:{ Exception -> 0x00a5, all -> 0x00e1 }
            goto L_0x0094
        L_0x00b7:
            if (r7 == 0) goto L_0x00bc
            r7.close()     // Catch:{ IOException -> 0x00db }
        L_0x00bc:
            if (r1 == 0) goto L_0x00ae
            r0 = r20
            java.util.concurrent.atomic.AtomicLong r2 = r0.b     // Catch:{ all -> 0x00c6 }
            r2.set(r5)     // Catch:{ all -> 0x00c6 }
            goto L_0x00ae
        L_0x00c6:
            r1 = move-exception
            monitor-exit(r10)     // Catch:{ all -> 0x00c6 }
            throw r1
        L_0x00c9:
            r2 = move-exception
            r7 = r1
            r1 = r2
        L_0x00cc:
            if (r7 == 0) goto L_0x00d1
            r7.close()     // Catch:{ IOException -> 0x00df }
        L_0x00d1:
            if (r4 == 0) goto L_0x00da
            r0 = r20
            java.util.concurrent.atomic.AtomicLong r2 = r0.b     // Catch:{ all -> 0x00c6 }
            r2.set(r5)     // Catch:{ all -> 0x00c6 }
        L_0x00da:
            throw r1     // Catch:{ all -> 0x00c6 }
        L_0x00db:
            r2 = move-exception
            goto L_0x00bc
        L_0x00dd:
            r1 = move-exception
            goto L_0x00ad
        L_0x00df:
            r2 = move-exception
            goto L_0x00d1
        L_0x00e1:
            r1 = move-exception
            goto L_0x00cc
        L_0x00e3:
            r1 = move-exception
            r4 = r8
            goto L_0x00cc
        L_0x00e6:
            r2 = move-exception
            goto L_0x00a7
        L_0x00e8:
            r4 = r5
            goto L_0x0081
        L_0x00ea:
            r8 = r4
            goto L_0x005a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.thumbnailCache.d.d():boolean");
    }

    public void a(String str) {
        if (this.f) {
            long currentTimeMillis = System.currentTimeMillis();
            synchronized (this.d) {
                if (this.d.get(str) != null) {
                    this.d.remove(str);
                    this.e.remove(str);
                }
                this.e.add(0, str);
                this.d.put(str, Long.valueOf(currentTimeMillis));
            }
        }
    }

    private void e() {
        synchronized (this.d) {
            this.d.clear();
            this.e.clear();
            this.b.set(0);
            this.f = false;
        }
    }

    /* access modifiers changed from: protected */
    public int b(String str) {
        return 1;
    }

    private void f() {
        new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        synchronized (this.d) {
            if (c()) {
                ArrayList arrayList = new ArrayList();
                for (String next : this.d.keySet()) {
                    arrayList.add(new e(this, next, Long.valueOf(this.d.get(next).longValue())));
                }
                this.e.clear();
                Collections.sort(arrayList);
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    this.e.add(((e) it.next()).f2571a);
                }
                arrayList.clear();
            }
            if (this.c <= this.e.size()) {
                int size = this.e.size() / 2;
                for (int size2 = this.e.size() - 1; size2 > 0 && size > 0; size2--) {
                    FileUtil.deleteFile(this.e.get(size2));
                    this.d.remove(this.e.get(size2));
                    this.b.set(this.b.get() - ((long) b(this.e.get(size2))));
                    this.e.remove(size2);
                    size--;
                }
            }
            this.f = true;
        }
        b();
    }

    /* JADX WARNING: Removed duplicated region for block: B:32:0x00c0 A[SYNTHETIC, Splitter:B:32:0x00c0] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00dc A[SYNTHETIC, Splitter:B:40:0x00dc] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:28:0x00ba=Splitter:B:28:0x00ba, B:37:0x00ca=Splitter:B:37:0x00ca, B:42:0x00df=Splitter:B:42:0x00df} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void b() {
        /*
            r9 = this;
            r2 = 0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.io.File r1 = r9.f2570a
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "/.dirinfo"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r3 = r0.toString()
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.io.File r1 = r9.f2570a
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "/.dirinfo_bak"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r4 = r0.toString()
            boolean r0 = r9.f
            if (r0 != 0) goto L_0x0030
        L_0x002f:
            return
        L_0x0030:
            java.util.Map<java.lang.String, java.lang.Long> r5 = r9.d
            monitor-enter(r5)
            java.util.Map<java.lang.String, java.lang.Long> r0 = r9.d     // Catch:{ all -> 0x003d }
            int r0 = r0.size()     // Catch:{ all -> 0x003d }
            if (r0 != 0) goto L_0x0040
            monitor-exit(r5)     // Catch:{ all -> 0x003d }
            goto L_0x002f
        L_0x003d:
            r0 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x003d }
            throw r0
        L_0x0040:
            r0 = 0
            java.io.DataOutputStream r1 = new java.io.DataOutputStream     // Catch:{ Exception -> 0x00bd, all -> 0x00c6 }
            java.io.BufferedOutputStream r6 = new java.io.BufferedOutputStream     // Catch:{ Exception -> 0x00bd, all -> 0x00c6 }
            java.io.FileOutputStream r7 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x00bd, all -> 0x00c6 }
            r7.<init>(r4)     // Catch:{ Exception -> 0x00bd, all -> 0x00c6 }
            r6.<init>(r7)     // Catch:{ Exception -> 0x00bd, all -> 0x00c6 }
            r1.<init>(r6)     // Catch:{ Exception -> 0x00bd, all -> 0x00c6 }
            java.util.Map<java.lang.String, java.lang.Long> r0 = r9.d     // Catch:{ Exception -> 0x00e6, all -> 0x00e4 }
            int r0 = r0.size()     // Catch:{ Exception -> 0x00e6, all -> 0x00e4 }
            r1.writeInt(r0)     // Catch:{ Exception -> 0x00e6, all -> 0x00e4 }
        L_0x0059:
            java.util.ArrayList<java.lang.String> r0 = r9.e     // Catch:{ Exception -> 0x00e6, all -> 0x00e4 }
            int r0 = r0.size()     // Catch:{ Exception -> 0x00e6, all -> 0x00e4 }
            if (r2 >= r0) goto L_0x00a5
            java.util.Map<java.lang.String, java.lang.Long> r0 = r9.d     // Catch:{ Exception -> 0x00e6, all -> 0x00e4 }
            java.util.ArrayList<java.lang.String> r6 = r9.e     // Catch:{ Exception -> 0x00e6, all -> 0x00e4 }
            java.lang.Object r6 = r6.get(r2)     // Catch:{ Exception -> 0x00e6, all -> 0x00e4 }
            java.lang.Object r0 = r0.get(r6)     // Catch:{ Exception -> 0x00e6, all -> 0x00e4 }
            if (r0 == 0) goto L_0x00a1
            java.util.ArrayList<java.lang.String> r0 = r9.e     // Catch:{ Exception -> 0x00e6, all -> 0x00e4 }
            java.lang.Object r0 = r0.get(r2)     // Catch:{ Exception -> 0x00e6, all -> 0x00e4 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x00e6, all -> 0x00e4 }
            r6 = 0
            r1.writeInt(r6)     // Catch:{ Exception -> 0x00e6, all -> 0x00e4 }
            int r6 = r0.length()     // Catch:{ Exception -> 0x00e6, all -> 0x00e4 }
            r1.writeInt(r6)     // Catch:{ Exception -> 0x00e6, all -> 0x00e4 }
            byte[] r6 = r0.getBytes()     // Catch:{ Exception -> 0x00e6, all -> 0x00e4 }
            r1.write(r6)     // Catch:{ Exception -> 0x00e6, all -> 0x00e4 }
            java.util.Map<java.lang.String, java.lang.Long> r6 = r9.d     // Catch:{ Exception -> 0x00e6, all -> 0x00e4 }
            java.lang.Object r0 = r6.get(r0)     // Catch:{ Exception -> 0x00e6, all -> 0x00e4 }
            java.lang.Long r0 = (java.lang.Long) r0     // Catch:{ Exception -> 0x00e6, all -> 0x00e4 }
            long r6 = r0.longValue()     // Catch:{ Exception -> 0x00e6, all -> 0x00e4 }
            r0 = 1
            r1.writeInt(r0)     // Catch:{ Exception -> 0x00e6, all -> 0x00e4 }
            r0 = 64
            r1.writeInt(r0)     // Catch:{ Exception -> 0x00e6, all -> 0x00e4 }
            r1.writeLong(r6)     // Catch:{ Exception -> 0x00e6, all -> 0x00e4 }
        L_0x00a1:
            int r0 = r2 + 1
            r2 = r0
            goto L_0x0059
        L_0x00a5:
            com.tencent.assistant.utils.FileUtil.deleteFile(r3)     // Catch:{ all -> 0x003d }
            java.io.File r0 = new java.io.File     // Catch:{ all -> 0x003d }
            r0.<init>(r4)     // Catch:{ all -> 0x003d }
            java.io.File r2 = new java.io.File     // Catch:{ all -> 0x003d }
            r2.<init>(r3)     // Catch:{ all -> 0x003d }
            r0.renameTo(r2)     // Catch:{ all -> 0x003d }
            if (r1 == 0) goto L_0x00ba
            r1.close()     // Catch:{ IOException -> 0x00e0 }
        L_0x00ba:
            monitor-exit(r5)     // Catch:{ all -> 0x003d }
            goto L_0x002f
        L_0x00bd:
            r1 = move-exception
        L_0x00be:
            if (r0 == 0) goto L_0x00ba
            r0.close()     // Catch:{ IOException -> 0x00c4 }
            goto L_0x00ba
        L_0x00c4:
            r0 = move-exception
            goto L_0x00ba
        L_0x00c6:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
        L_0x00ca:
            com.tencent.assistant.utils.FileUtil.deleteFile(r3)     // Catch:{ all -> 0x003d }
            java.io.File r2 = new java.io.File     // Catch:{ all -> 0x003d }
            r2.<init>(r4)     // Catch:{ all -> 0x003d }
            java.io.File r4 = new java.io.File     // Catch:{ all -> 0x003d }
            r4.<init>(r3)     // Catch:{ all -> 0x003d }
            r2.renameTo(r4)     // Catch:{ all -> 0x003d }
            if (r1 == 0) goto L_0x00df
            r1.close()     // Catch:{ IOException -> 0x00e2 }
        L_0x00df:
            throw r0     // Catch:{ all -> 0x003d }
        L_0x00e0:
            r0 = move-exception
            goto L_0x00ba
        L_0x00e2:
            r1 = move-exception
            goto L_0x00df
        L_0x00e4:
            r0 = move-exception
            goto L_0x00ca
        L_0x00e6:
            r0 = move-exception
            r0 = r1
            goto L_0x00be
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.thumbnailCache.d.b():void");
    }
}
