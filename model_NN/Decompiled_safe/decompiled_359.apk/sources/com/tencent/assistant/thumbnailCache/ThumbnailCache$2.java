package com.tencent.assistant.thumbnailCache;

import java.util.LinkedHashMap;
import java.util.Map;

/* compiled from: ProGuard */
class ThumbnailCache$2 extends LinkedHashMap<String, a> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b f2566a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    ThumbnailCache$2(b bVar, int i, float f, boolean z) {
        super(i, f, z);
        this.f2566a = bVar;
    }

    /* access modifiers changed from: protected */
    public boolean removeEldestEntry(Map.Entry<String, a> entry) {
        return size() > this.f2566a.e;
    }
}
