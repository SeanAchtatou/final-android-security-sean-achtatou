package com.tencent.assistant.event.listener;

import android.os.Message;

/* compiled from: ProGuard */
public interface UIEventListener {
    void handleUIEvent(Message message);
}
