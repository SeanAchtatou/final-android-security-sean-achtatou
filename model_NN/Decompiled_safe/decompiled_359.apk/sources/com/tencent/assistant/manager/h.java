package com.tencent.assistant.manager;

import com.tencent.assistant.h.d;
import com.tencent.assistant.localres.x;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.smartcard.o;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.a.i;
import com.tencent.assistant.model.b;
import com.tencent.assistant.model.e;
import com.tencent.assistant.module.al;
import com.tencent.assistant.module.callback.ai;
import com.tencent.assistant.module.fn;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class h extends x<ai> {

    /* renamed from: a  reason: collision with root package name */
    private fn f1542a = new fn();
    /* access modifiers changed from: private */
    public long b = 0;
    private al c = al.a();
    /* access modifiers changed from: private */
    public d d = new d();
    private k e = new k(this, null);
    /* access modifiers changed from: private */
    public volatile boolean f = false;
    /* access modifiers changed from: private */
    public volatile boolean g = false;
    /* access modifiers changed from: private */
    public j h;
    private int i;
    private List<Long> j = new ArrayList();

    public h() {
        this.c.register(this.e);
        this.f1542a.register(this.e);
    }

    public void a() {
        this.f = false;
        this.g = false;
        this.c.e();
        this.f1542a.a(1);
    }

    public int b() {
        return this.c.f();
    }

    public void c() {
        a();
    }

    public b d() {
        b b2 = this.c.b();
        b2.c(a(b2.c(), 0));
        b2.b((List<SimpleAppModel>) null);
        return b2;
    }

    public void e() {
        if (this.b > 0 && System.currentTimeMillis() - this.b > m.a().ah()) {
            c();
        }
    }

    public void a(j jVar) {
        if (this.f && this.g && jVar != null) {
            int i2 = this.i;
            if (jVar.e) {
                i2 = 0;
            }
            a(new i(this, jVar, a(jVar.g, i2)));
        }
    }

    public synchronized List<e> a(List<SimpleAppModel> list, int i2) {
        ArrayList arrayList;
        if (list != null) {
            if (list.size() > 0) {
                if (i2 == 0) {
                    this.j.clear();
                }
                ArrayList arrayList2 = new ArrayList();
                for (Long next : this.j) {
                    for (SimpleAppModel next2 : list) {
                        if (next2.f1634a == next.longValue()) {
                            arrayList2.add(next2);
                        }
                    }
                }
                ArrayList arrayList3 = new ArrayList(list);
                arrayList3.removeAll(arrayList2);
                ArrayList arrayList4 = new ArrayList();
                int i3 = 0;
                while (i3 < arrayList3.size()) {
                    e eVar = new e();
                    List<i> a2 = this.d.a(i2);
                    if (a2 != null && a2.size() > 0) {
                        eVar.g = o.a().a(a2, this.j, i2);
                        if (eVar.g != null) {
                            o.a().a(eVar.g);
                            eVar.b = 2;
                            List<Long> h2 = eVar.g.h();
                            if (h2 != null) {
                                this.j.addAll(h2);
                            }
                        }
                    }
                    if (eVar.g == null) {
                        do {
                            int i4 = i3;
                            SimpleAppModel simpleAppModel = (SimpleAppModel) arrayList3.get(i4);
                            if (!this.j.contains(Long.valueOf(simpleAppModel.f1634a))) {
                                eVar.c = simpleAppModel;
                                eVar.b = 1;
                                this.j.add(Long.valueOf(simpleAppModel.f1634a));
                            }
                            i3 = i4 + 1;
                            if (eVar.c != null) {
                                break;
                            }
                        } while (i3 < arrayList3.size());
                    }
                    i2++;
                    arrayList4.add(eVar);
                }
                if (arrayList4 != null) {
                    this.i += arrayList4.size();
                }
                arrayList = arrayList4;
            }
        }
        arrayList = null;
        return arrayList;
    }
}
