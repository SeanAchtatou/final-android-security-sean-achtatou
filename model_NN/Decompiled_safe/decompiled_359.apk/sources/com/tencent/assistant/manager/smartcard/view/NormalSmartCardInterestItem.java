package com.tencent.assistant.manager.smartcard.view;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.Spanned;
import android.text.TextUtils;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.component.smartcard.NormalSmartCardAppHorizontalNode;
import com.tencent.assistant.component.smartcard.NormalSmartCardAppVerticalNode;
import com.tencent.assistant.component.smartcard.NormalSmartcardBaseItem;
import com.tencent.assistant.component.smartcard.SmartcardListener;
import com.tencent.assistant.manager.smartcard.b.d;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.a.i;
import com.tencent.assistant.model.a.t;
import com.tencent.assistant.utils.cn;
import com.tencent.assistant.utils.df;
import com.tencent.assistantv2.component.ListItemInfoView;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.cloud.smartcard.b.b;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class NormalSmartCardInterestItem extends NormalSmartcardBaseItem {
    private RelativeLayout f;
    private TextView i;
    private TextView j;
    private TextView k;
    private LinearLayout l;
    private LinearLayout m;
    private List<NormalSmartCardAppHorizontalNode> n;

    public NormalSmartCardInterestItem(Context context, i iVar, SmartcardListener smartcardListener, IViewInvalidater iViewInvalidater) {
        super(context, iVar, smartcardListener, iViewInvalidater);
        setBackgroundResource(R.drawable.common_card_normal);
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.c = inflate(this.f1115a, R.layout.smartcard_interest_frame_layout, this);
        this.i = (TextView) this.c.findViewById(R.id.card_title);
        this.j = (TextView) this.c.findViewById(R.id.card_sub_title);
        this.k = (TextView) this.c.findViewById(R.id.more_txt);
        this.f = (RelativeLayout) this.c.findViewById(R.id.card_title_layout);
        this.l = (LinearLayout) this.c.findViewById(R.id.card_app_list_layout);
        this.m = (LinearLayout) this.c.findViewById(R.id.card_app_vertical_list_layout);
        f();
    }

    private void f() {
        d dVar = (d) this.smartcardModel;
        if (!TextUtils.isEmpty(dVar.k)) {
            this.i.setText(dVar.k);
            int a2 = cn.a(dVar.e());
            if (a2 != 0) {
                Drawable drawable = getResources().getDrawable(a2);
                drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
                this.i.setCompoundDrawables(drawable, null, null, null);
                this.i.setCompoundDrawablePadding(df.a(this.f1115a, 7.0f));
                this.i.setPadding(0, 0, 0, 0);
            } else {
                this.i.setCompoundDrawables(null, null, null, null);
                this.i.setPadding(df.a(this.f1115a, 7.0f), 0, 0, 0);
            }
            this.f.setVisibility(0);
            if (!TextUtils.isEmpty(dVar.l)) {
                this.j.setText(dVar.l);
                this.j.setPadding(df.a(this.f1115a, 7.0f), 0, 0, 0);
                this.j.setVisibility(0);
            } else {
                this.j.setVisibility(8);
            }
            if (TextUtils.isEmpty(dVar.o) || TextUtils.isEmpty(dVar.n)) {
                this.k.setVisibility(8);
            } else {
                this.k.setText(dVar.o);
                this.k.setOnClickListener(this.h);
                this.k.setVisibility(0);
            }
        } else {
            this.f.setVisibility(8);
        }
        if (dVar.g() == 2) {
            this.l.setVisibility(8);
            a(dVar);
            return;
        }
        this.m.setVisibility(8);
        b(dVar);
    }

    private void a(d dVar) {
        int i2;
        boolean z;
        List<t> list = dVar.f1592a;
        this.m.setVisibility(0);
        int size = list.size();
        if (size > 3) {
            i2 = 3;
        } else {
            i2 = size;
        }
        int childCount = this.m.getChildCount();
        if (childCount < i2) {
            for (int i3 = 0; i3 < i2 - childCount; i3++) {
                NormalSmartCardAppVerticalNode normalSmartCardAppVerticalNode = new NormalSmartCardAppVerticalNode(this.f1115a);
                normalSmartCardAppVerticalNode.setMinimumHeight(df.a(this.f1115a, 90.0f));
                this.m.addView(normalSmartCardAppVerticalNode, new LinearLayout.LayoutParams(-1, -2, 1.0f));
            }
        }
        int childCount2 = this.m.getChildCount();
        for (int i4 = 0; i4 < childCount2; i4++) {
            NormalSmartCardAppVerticalNode normalSmartCardAppVerticalNode2 = (NormalSmartCardAppVerticalNode) this.m.getChildAt(i4);
            if (normalSmartCardAppVerticalNode2 != null) {
                if (i4 < i2) {
                    normalSmartCardAppVerticalNode2.setVisibility(0);
                } else {
                    normalSmartCardAppVerticalNode2.setVisibility(8);
                }
            }
        }
        for (int i5 = 0; i5 < i2; i5++) {
            SimpleAppModel simpleAppModel = list.get(i5).f1653a;
            NormalSmartCardAppVerticalNode normalSmartCardAppVerticalNode3 = (NormalSmartCardAppVerticalNode) this.m.getChildAt(i5);
            Spanned a2 = list.get(i5).a();
            STInfoV2 a3 = a(list.get(i5), i5);
            int a4 = a(contextToPageId(this.f1115a));
            if (i5 < i2 - 1) {
                z = true;
            } else {
                z = false;
            }
            normalSmartCardAppVerticalNode3.setData(simpleAppModel, a2, a3, a4, z, ListItemInfoView.InfoType.CATEGORY_SIZE);
        }
    }

    private void b(d dVar) {
        int i2;
        List<t> list = dVar.f1592a;
        this.l.setVisibility(0);
        if (list.size() >= 3) {
            int size = list.size();
            if (list.size() > 3) {
                i2 = 3;
            } else {
                i2 = size;
            }
            if (this.n == null) {
                g();
                this.n = new ArrayList(3);
                for (int i3 = 0; i3 < i2; i3++) {
                    NormalSmartCardAppHorizontalNode normalSmartCardAppHorizontalNode = new NormalSmartCardAppHorizontalNode(this.f1115a);
                    this.n.add(normalSmartCardAppHorizontalNode);
                    normalSmartCardAppHorizontalNode.setPadding(0, df.a(getContext(), 10.0f), 0, df.a(getContext(), 2.0f));
                    normalSmartCardAppHorizontalNode.setBackgroundResource(R.drawable.v2_button_background_light_selector);
                    this.l.addView(normalSmartCardAppHorizontalNode, new LinearLayout.LayoutParams(0, -2, 1.0f));
                    normalSmartCardAppHorizontalNode.setData(list.get(i3).f1653a, list.get(i3).a(), a(list.get(i3), i3), a(contextToPageId(this.f1115a)));
                }
                return;
            }
            for (int i4 = 0; i4 < i2; i4++) {
                this.n.get(i4).setData(list.get(i4).f1653a, list.get(i4).a(), a(list.get(i4), i4), a(contextToPageId(this.f1115a)));
            }
        }
    }

    private void g() {
        this.n = null;
        this.l.removeAllViews();
    }

    private STInfoV2 a(t tVar, int i2) {
        STInfoV2 a2 = a(a.a("05", i2), 100);
        if (!(a2 == null || tVar == null)) {
            a2.updateWithSimpleAppModel(tVar.f1653a);
        }
        if (this.e == null) {
            this.e = new com.tencent.assistantv2.st.b.d();
        }
        this.e.a(a2);
        return a2;
    }

    /* access modifiers changed from: protected */
    public void b() {
        f();
    }

    /* access modifiers changed from: protected */
    public String b(int i2) {
        if (this.smartcardModel instanceof b) {
            return ((b) this.smartcardModel).m();
        }
        return super.b(i2);
    }
}
