package com.tencent.assistant.manager.smartcard;

import com.tencent.assistant.model.a.i;
import com.tencent.assistant.model.a.m;
import com.tencent.assistant.model.a.q;
import com.tencent.assistant.model.a.r;
import com.tencent.assistant.model.a.s;
import com.tencent.assistant.utils.cv;
import java.util.List;

/* compiled from: ProGuard */
public class k extends y {
    public boolean a(i iVar, List<Long> list) {
        if (iVar == null || !(iVar instanceof m)) {
            return false;
        }
        m mVar = (m) iVar;
        return a(mVar, (r) this.f1610a.get(Integer.valueOf(mVar.k())), (s) this.b.get(Integer.valueOf(mVar.k())), list);
    }

    /* access modifiers changed from: protected */
    public boolean a(m mVar, r rVar, s sVar, List<Long> list) {
        if (sVar == null) {
            return false;
        }
        mVar.a(list);
        if (mVar.d == null || System.currentTimeMillis() / 1000 > sVar.h) {
            return false;
        }
        return true;
    }

    public void a(q qVar) {
        r rVar;
        int i;
        if (qVar != null) {
            r rVar2 = (r) this.f1610a.get(Integer.valueOf(qVar.a()));
            if (rVar2 == null) {
                r rVar3 = new r();
                rVar3.e = qVar.f1650a;
                rVar3.f = qVar.b;
                rVar = rVar3;
            } else {
                rVar = rVar2;
            }
            if (qVar.d) {
                rVar.d = true;
            }
            if (qVar.c) {
                if (cv.b(qVar.e * 1000)) {
                    rVar.f1651a++;
                }
                s sVar = (s) this.b.get(Integer.valueOf(qVar.a()));
                if (sVar != null) {
                    i = sVar.i;
                } else {
                    i = 7;
                }
                if (cv.a(qVar.e * 1000, i)) {
                    rVar.b++;
                }
                rVar.c++;
            }
            this.f1610a.put(Integer.valueOf(rVar.a()), rVar);
        }
    }

    public void a(s sVar) {
        this.b.put(Integer.valueOf(sVar.a()), sVar);
    }

    public void a(i iVar) {
        if (iVar != null && iVar.i == 23) {
            m mVar = (m) iVar;
            s sVar = (s) this.b.get(Integer.valueOf(mVar.k()));
            if (sVar != null) {
                mVar.p = sVar.d;
            }
        }
    }
}
