package com.tencent.assistant.manager;

import com.tencent.assistant.download.DownloadInfo;
import com.tencent.downloadsdk.DownloadManager;
import java.util.List;

/* compiled from: ProGuard */
class aa implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ List f1472a;
    final /* synthetic */ DownloadProxy b;

    aa(DownloadProxy downloadProxy, List list) {
        this.b = downloadProxy;
        this.f1472a = list;
    }

    public void run() {
        for (DownloadInfo downloadInfo : this.f1472a) {
            DownloadManager.a().a(downloadInfo.getDownloadSubType(), downloadInfo.downloadTicket);
        }
    }
}
