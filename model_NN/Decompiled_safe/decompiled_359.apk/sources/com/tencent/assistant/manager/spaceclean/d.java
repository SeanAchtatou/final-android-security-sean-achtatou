package com.tencent.assistant.manager.spaceclean;

import com.tencent.assistant.module.callback.ak;
import com.tencent.assistant.utils.FileUtil;
import com.tencent.assistant.utils.XLog;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: ProGuard */
class d implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ArrayList f1618a;
    final /* synthetic */ ak b;
    final /* synthetic */ SpaceScanManager c;

    d(SpaceScanManager spaceScanManager, ArrayList arrayList, ak akVar) {
        this.c = spaceScanManager;
        this.f1618a = arrayList;
        this.b = akVar;
    }

    public void run() {
        Iterator it = this.f1618a.iterator();
        while (it.hasNext()) {
            FileUtil.deleteFileOrDir((String) it.next());
        }
        XLog.d("miles", "SpaceScanManager >> cleanRubbishAsync onCleanFinished");
        this.b.a(true);
    }
}
