package com.tencent.assistant.manager.notification.a.a;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/* compiled from: ProGuard */
public class g {

    /* renamed from: a  reason: collision with root package name */
    private List<a> f1553a = new LinkedList();
    private i b = null;
    private boolean c = false;

    public void a(i iVar) {
        this.b = iVar;
    }

    public void a() {
        if (this.f1553a != null) {
            this.f1553a.clear();
        }
        this.c = false;
    }

    public void a(a aVar) {
        if (aVar != null) {
            aVar.a(new h(this));
            this.f1553a.add(aVar);
        }
    }

    public void b() {
        if (!c()) {
            for (a next : this.f1553a) {
                if (next != null && !next.a()) {
                    next.b();
                }
            }
            return;
        }
        a(0);
    }

    /* access modifiers changed from: private */
    public synchronized boolean c() {
        boolean z;
        Iterator<a> it = this.f1553a.iterator();
        while (true) {
            if (!it.hasNext()) {
                z = true;
                break;
            }
            a next = it.next();
            if (next != null && !next.a()) {
                z = false;
                break;
            }
        }
        return z;
    }

    /* access modifiers changed from: private */
    public synchronized void a(int i) {
        if (!this.c && this.b != null) {
            this.b.a(i);
        }
        this.c = true;
    }
}
