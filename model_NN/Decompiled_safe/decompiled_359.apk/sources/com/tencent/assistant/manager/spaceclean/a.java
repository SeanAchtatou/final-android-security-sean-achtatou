package com.tencent.assistant.manager.spaceclean;

import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.utils.XLog;
import java.util.List;

/* compiled from: ProGuard */
class a extends ApkResCallback.Stub {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SpaceScanManager f1615a;

    a(SpaceScanManager spaceScanManager) {
        this.f1615a = spaceScanManager;
    }

    public void onLoadInstalledApkSuccess(List<LocalApkInfo> list) {
        this.f1615a.b(list);
        XLog.d("miles", "SpaceScanManager >> onLoadInstalledApkSuccess.");
    }

    public void onLoadInstalledApkFail(int i, String str) {
    }

    public void onLoadInstalledApkExtraSuccess(List<LocalApkInfo> list) {
        this.f1615a.b(list);
        XLog.d("miles", "SpaceScanManager >> onLoadInstalledApkExtraSuccess.");
    }

    public void onInstalledAppTimeUpdate(List<LocalApkInfo> list) {
        this.f1615a.b(list);
        XLog.d("miles", "SpaceScanManager >> onInstalledAppTimeUpdate.");
    }
}
