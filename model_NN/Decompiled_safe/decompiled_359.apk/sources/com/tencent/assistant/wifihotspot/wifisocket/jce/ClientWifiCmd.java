package com.tencent.assistant.wifihotspot.wifisocket.jce;

import java.io.Serializable;

/* compiled from: ProGuard */
public final class ClientWifiCmd implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    public static final ClientWifiCmd f2732a = new ClientWifiCmd(0, 0, "SessionIdentity");
    public static final ClientWifiCmd b = new ClientWifiCmd(1, 1, "FileOption");
    public static final ClientWifiCmd c = new ClientWifiCmd(2, 2, "Heartbeat");
    public static final ClientWifiCmd d = new ClientWifiCmd(3, 3, "UserInfo");
    static final /* synthetic */ boolean e = (!ClientWifiCmd.class.desiredAssertionStatus());
    private static ClientWifiCmd[] f = new ClientWifiCmd[4];
    private int g;
    private String h = new String();

    public String toString() {
        return this.h;
    }

    private ClientWifiCmd(int i, int i2, String str) {
        this.h = str;
        this.g = i2;
        f[i] = this;
    }
}
