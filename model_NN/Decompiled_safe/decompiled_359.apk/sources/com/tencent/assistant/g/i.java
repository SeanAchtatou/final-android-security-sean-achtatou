package com.tencent.assistant.g;

import android.widget.Toast;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.model.ShareAppModel;
import com.tencent.assistant.model.ShareBaseModel;

/* compiled from: ProGuard */
class i extends AppConst.TwoBtnDialogInfo {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ e f1315a;

    i(e eVar) {
        this.f1315a = eVar;
    }

    public void onRightBtnClick() {
        if (this.f1315a.d == null) {
            Toast.makeText(AstApp.i().getApplicationContext(), (int) R.string.share_info_err, 0).show();
            return;
        }
        if (this.f1315a.d instanceof ShareAppModel) {
            ShareAppModel shareAppModel = (ShareAppModel) this.f1315a.d;
            this.f1315a.a(shareAppModel.e, this.f1315a.c(shareAppModel), this.f1315a.b(shareAppModel), shareAppModel.b, shareAppModel.f, null);
        }
        if (this.f1315a.d instanceof ShareBaseModel) {
            ShareBaseModel shareBaseModel = (ShareBaseModel) this.f1315a.d;
            this.f1315a.a(-1, shareBaseModel.f1633a, shareBaseModel.b, shareBaseModel.e, shareBaseModel.c, shareBaseModel.d);
        }
    }

    public void onLeftBtnClick() {
    }

    public void onCancell() {
    }
}
