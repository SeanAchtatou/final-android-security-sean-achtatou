package com.tencent.assistant.g;

import android.app.Dialog;
import java.lang.ref.WeakReference;

/* compiled from: ProGuard */
class h implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ e f1314a;

    h(e eVar) {
        this.f1314a = eVar;
    }

    public void run() {
        for (WeakReference weakReference : e.g) {
            Dialog dialog = (Dialog) weakReference.get();
            if (!(dialog == null || this.f1314a.e == null || this.f1314a.e.get() == null || this.f1314a.e.get().isFinishing())) {
                try {
                    dialog.dismiss();
                } catch (Throwable th) {
                }
            }
        }
    }
}
