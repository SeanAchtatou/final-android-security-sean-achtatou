package com.tencent.assistant.guessfavor;

import android.widget.ImageView;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.utils.a;
import com.tencent.assistantv2.component.y;

/* compiled from: ProGuard */
class f extends y {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GuessFavorAdapter f1339a;

    f(GuessFavorAdapter guessFavorAdapter) {
        this.f1339a = guessFavorAdapter;
    }

    public void a(DownloadInfo downloadInfo) {
        ImageView imageView = (ImageView) this.f1339a.e.findViewWithTag(downloadInfo.downloadTicket);
        if (imageView != null) {
            a.a(imageView);
        }
        com.tencent.assistant.download.a.a().a(downloadInfo);
    }
}
