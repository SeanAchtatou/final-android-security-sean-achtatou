package com.tencent.assistant.login.model;

import android.text.TextUtils;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.login.a.c;
import com.tencent.assistant.protocol.jce.TicketOAuth2;
import com.tencent.assistant.utils.bh;
import com.tencent.assistant.utils.bj;

/* compiled from: ProGuard */
public class d extends a {
    private String d;
    private String e;
    private String f;

    public d(String str, String str2, String str3) {
        super(AppConst.IdentityType.WX);
        this.d = str;
        this.e = str2;
        this.f = str3;
        getTicket();
    }

    public String b() {
        return this.d;
    }

    public String c() {
        return this.e;
    }

    public String d() {
        return this.f;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.login.a.c.a(java.lang.String, java.lang.String, boolean):byte[]
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.tencent.assistant.login.a.c.a(byte[], java.lang.String, boolean):java.lang.String
      com.tencent.assistant.login.a.c.a(java.lang.String, java.lang.String, boolean):byte[] */
    public JceStruct a() {
        byte[] bArr;
        byte[] bArr2 = null;
        if (!TextUtils.isEmpty(this.e)) {
            bArr = c.a(this.e, "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCnWCwGlN/jRLEi4PxWrOqy2I0tSRs8UhjX+Q5nkYVDiqerMhSH8c6jJgjYbVsETfH85wgSjFR7U5STafxsBBAUWADt7dy7NS0GuNN9IyX5U0AEBSI9GPsPd7JmtwiiOS1cJKnHIUb+fKwaaWTDvi208KOvGe2WplhKpaQ2Eo9kTwIDAQAB", true);
        } else {
            bArr = null;
        }
        if (!TextUtils.isEmpty(this.f)) {
            bArr2 = c.a(this.f, "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCnWCwGlN/jRLEi4PxWrOqy2I0tSRs8UhjX+Q5nkYVDiqerMhSH8c6jJgjYbVsETfH85wgSjFR7U5STafxsBBAUWADt7dy7NS0GuNN9IyX5U0AEBSI9GPsPd7JmtwiiOS1cJKnHIUb+fKwaaWTDvi208KOvGe2WplhKpaQ2Eo9kTwIDAQAB", true);
        }
        return new TicketOAuth2(this.d, bArr, bArr2);
    }

    public byte[] getKey() {
        if (!TextUtils.isEmpty(this.e)) {
            return bj.a(this.e);
        }
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.login.a.c.a(byte[], java.lang.String, boolean):java.lang.String
     arg types: [byte[], java.lang.String, int]
     candidates:
      com.tencent.assistant.login.a.c.a(java.lang.String, java.lang.String, boolean):byte[]
      com.tencent.assistant.login.a.c.a(byte[], java.lang.String, boolean):java.lang.String */
    public static d a(byte[] bArr) {
        d dVar;
        if (bArr == null) {
            return null;
        }
        TicketOAuth2 ticketOAuth2 = (TicketOAuth2) bh.b(bArr, TicketOAuth2.class);
        if (!(ticketOAuth2 == null || ticketOAuth2.b() == null)) {
            String a2 = c.a(ticketOAuth2.b(), "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCnWCwGlN/jRLEi4PxWrOqy2I0tSRs8UhjX+Q5nkYVDiqerMhSH8c6jJgjYbVsETfH85wgSjFR7U5STafxsBBAUWADt7dy7NS0GuNN9IyX5U0AEBSI9GPsPd7JmtwiiOS1cJKnHIUb+fKwaaWTDvi208KOvGe2WplhKpaQ2Eo9kTwIDAQAB", true);
            String a3 = c.a(ticketOAuth2.c(), "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCnWCwGlN/jRLEi4PxWrOqy2I0tSRs8UhjX+Q5nkYVDiqerMhSH8c6jJgjYbVsETfH85wgSjFR7U5STafxsBBAUWADt7dy7NS0GuNN9IyX5U0AEBSI9GPsPd7JmtwiiOS1cJKnHIUb+fKwaaWTDvi208KOvGe2WplhKpaQ2Eo9kTwIDAQAB", true);
            if (!TextUtils.isEmpty(a2) && !TextUtils.isEmpty(ticketOAuth2.a())) {
                dVar = new d(ticketOAuth2.a(), a2, a3);
                return dVar;
            }
        }
        dVar = null;
        return dVar;
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof d)) {
            return false;
        }
        d dVar = (d) obj;
        try {
            if (!this.d.equals(dVar.d) || !this.e.equals(dVar.e)) {
                return false;
            }
            return true;
        } catch (Exception e2) {
            return false;
        }
    }

    public int hashCode() {
        if (this.d != null) {
            return this.d.hashCode();
        }
        return 1;
    }
}
