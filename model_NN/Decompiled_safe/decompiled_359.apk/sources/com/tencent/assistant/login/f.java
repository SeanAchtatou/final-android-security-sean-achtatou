package com.tencent.assistant.login;

import android.view.View;
import com.tencent.assistant.plugin.PluginHelper;

/* compiled from: ProGuard */
class f implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PluginLoadingDialog f1460a;

    f(PluginLoadingDialog pluginLoadingDialog) {
        this.f1460a = pluginLoadingDialog;
    }

    public void onClick(View view) {
        int requireInstall = PluginHelper.requireInstall(b.b);
        if (requireInstall != 1) {
            if (requireInstall == -1) {
                this.f1460a.b();
            } else if (requireInstall == 0) {
                this.f1460a.b();
            }
        }
    }
}
