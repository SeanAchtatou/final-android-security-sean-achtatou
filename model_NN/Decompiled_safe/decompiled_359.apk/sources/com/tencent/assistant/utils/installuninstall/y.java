package com.tencent.assistant.utils.installuninstall;

import android.widget.Toast;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoV2;
import java.util.List;

/* compiled from: ProGuard */
class y extends AppConst.TwoBtnDialogInfo {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ List f2716a;
    final /* synthetic */ p b;

    y(p pVar, List list) {
        this.b = pVar;
        this.f2716a = list;
    }

    public void onLeftBtnClick() {
        int i;
        k.a(new STInfoV2(STConst.ST_DIALOG_INSTALL_ROOT_AUTH_ALERT, "03_001", AstApp.m() != null ? AstApp.m().f() : 2000, STConst.ST_DEFAULT_SLOT, 200));
        if (((InstallUninstallTaskBean) this.f2716a.get(0)).action == 1) {
            i = 0;
        } else {
            i = 1;
        }
        p.b(1, i);
        Toast.makeText(AstApp.i().getBaseContext(), AstApp.i().getBaseContext().getString(R.string.request_root_negative_click_toast), 1).show();
        for (InstallUninstallTaskBean installUninstallTaskBean : this.f2716a) {
            if (installUninstallTaskBean.action == 1) {
                ac.a().a(installUninstallTaskBean);
            } else if (installUninstallTaskBean.action == -1 && installUninstallTaskBean.trySystemAfterSilentFail) {
                this.b.f2708a.sendMessage(this.b.f2708a.obtainMessage(1024, installUninstallTaskBean.packageName));
                InstallUninstallUtil.a(installUninstallTaskBean.packageName);
            }
        }
    }

    public void onRightBtnClick() {
        int i;
        k.a(new STInfoV2(STConst.ST_DIALOG_INSTALL_ROOT_AUTH_ALERT, "03_002", AstApp.m() != null ? AstApp.m().f() : 2000, STConst.ST_DEFAULT_SLOT, 200));
        if (((InstallUninstallTaskBean) this.f2716a.get(0)).action == 1) {
            i = 0;
        } else {
            i = 1;
        }
        p.b(2, i);
        p.e().post(new z(this));
    }

    public void onCancell() {
        int i;
        k.a(new STInfoV2(STConst.ST_DIALOG_INSTALL_ROOT_AUTH_ALERT, "03_001", AstApp.m() != null ? AstApp.m().f() : 2000, STConst.ST_DEFAULT_SLOT, 200));
        if (((InstallUninstallTaskBean) this.f2716a.get(0)).action == 1) {
            i = 0;
        } else {
            i = 1;
        }
        p.b(1, i);
        try {
            Toast.makeText(AstApp.i().getBaseContext(), AstApp.i().getBaseContext().getString(R.string.request_root_negative_click_toast), 1).show();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        for (InstallUninstallTaskBean installUninstallTaskBean : this.f2716a) {
            if (installUninstallTaskBean.action == 1) {
                ac.a().a(installUninstallTaskBean);
            } else if (installUninstallTaskBean.action == -1 && installUninstallTaskBean.trySystemAfterSilentFail) {
                this.b.f2708a.sendMessage(this.b.f2708a.obtainMessage(1024, installUninstallTaskBean.packageName));
                InstallUninstallUtil.a(installUninstallTaskBean.packageName);
            }
        }
    }
}
