package com.tencent.assistant.utils;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.model.a.a;
import com.tencent.assistant.model.a.b;
import com.tencent.assistant.model.a.c;
import com.tencent.assistant.model.a.d;
import com.tencent.assistant.model.a.g;
import com.tencent.assistant.model.a.h;
import com.tencent.assistant.model.a.i;
import com.tencent.assistant.model.a.k;
import com.tencent.assistant.model.a.m;
import com.tencent.assistant.model.a.n;
import com.tencent.assistant.model.a.o;
import com.tencent.assistant.model.a.u;
import com.tencent.assistant.model.a.v;
import com.tencent.assistant.protocol.jce.SmartCard;
import com.tencent.assistant.protocol.jce.SmartCardAppList;
import com.tencent.assistant.protocol.jce.SmartCardCpaAdvertise;
import com.tencent.assistant.protocol.jce.SmartCardGrabNumber;
import com.tencent.assistant.protocol.jce.SmartCardHotWords;
import com.tencent.assistant.protocol.jce.SmartCardLibao;
import com.tencent.assistant.protocol.jce.SmartCardOp;
import com.tencent.assistant.protocol.jce.SmartCardPlayerShow;
import com.tencent.assistant.protocol.jce.SmartCardRecommandation;
import com.tencent.assistant.protocol.jce.SmartCardReservation;
import com.tencent.assistant.protocol.jce.SmartCardTemplate;
import com.tencent.tmsecurelite.optimize.ISystemOptimize;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class cm {
    public static List<i> a(List<SmartCard> list, int i) {
        i iVar;
        if (list == null || list.size() <= 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= list.size()) {
                return arrayList;
            }
            try {
                iVar = a(list.get(i3));
            } catch (Exception e) {
                XLog.e("SmartCard", "getSmartCardModel exception", e);
                iVar = null;
            }
            if (iVar != null) {
                iVar.s = i;
                arrayList.add(iVar);
            }
            i2 = i3 + 1;
        }
    }

    public static i a(SmartCard smartCard) {
        JceStruct b;
        if (smartCard == null || smartCard.b == null) {
            return null;
        }
        switch (smartCard.f2313a) {
            case 0:
                v vVar = new v();
                vVar.i = 0;
                vVar.a((SmartCardOp) bh.b(smartCard.b, SmartCardOp.class));
                return vVar;
            case 1:
                g gVar = new g();
                gVar.a((SmartCardLibao) bh.b(smartCard.b, SmartCardLibao.class), smartCard.f2313a);
                return gVar;
            case 2:
            case 3:
            case 4:
                b bVar = new b();
                bVar.a((SmartCardAppList) bh.b(smartCard.b, SmartCardAppList.class), smartCard.f2313a);
                return bVar;
            case 5:
            case 7:
            case 8:
                k kVar = new k();
                if (kVar.a(smartCard.f2313a, (SmartCardRecommandation) bh.b(smartCard.b, SmartCardRecommandation.class))) {
                    return kVar;
                }
                break;
            case 6:
                o oVar = new o();
                oVar.a((SmartCardAppList) bh.b(smartCard.b, SmartCardAppList.class), smartCard.f2313a);
                return oVar;
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case ISystemOptimize.T_getMemoryUsageAsync /*27*/:
                SmartCardTemplate smartCardTemplate = (SmartCardTemplate) bh.b(smartCard.b, SmartCardTemplate.class);
                u uVar = new u();
                if (uVar.a(smartCard.f2313a, smartCardTemplate.g, smartCardTemplate)) {
                    return uVar;
                }
                break;
            case 16:
                c cVar = new c();
                cVar.a((SmartCardReservation) bh.b(smartCard.b, SmartCardReservation.class), smartCard.f2313a);
                return cVar;
            case 17:
                n nVar = new n();
                nVar.a((SmartCardGrabNumber) bh.b(smartCard.b, SmartCardGrabNumber.class), smartCard.f2313a);
                return nVar;
            case 18:
            case 19:
            case 20:
            case 21:
            case ISystemOptimize.T_hasRootAsync /*25*/:
            case ISystemOptimize.T_askForRootAsync /*26*/:
            default:
                com.tencent.assistant.manager.smartcard.c a2 = com.tencent.assistant.manager.smartcard.b.a(smartCard.f2313a);
                if (!(a2 == null || (b = bh.b(smartCard.b, a2.a())) == null)) {
                    a b2 = a2.b();
                    if (b2.a(smartCard.f2313a, b)) {
                        return b2;
                    }
                }
                break;
            case ISystemOptimize.T_killTaskAsync /*22*/:
                h hVar = new h();
                hVar.a((SmartCardHotWords) bh.b(smartCard.b, SmartCardHotWords.class), smartCard.f2313a);
                return hVar;
            case 23:
                m mVar = new m();
                mVar.a((SmartCardPlayerShow) bh.b(smartCard.b, SmartCardPlayerShow.class), smartCard.f2313a);
                return mVar;
            case ISystemOptimize.T_checkVersionAsync /*24*/:
                d dVar = new d();
                dVar.a((SmartCardCpaAdvertise) bh.b(smartCard.b, SmartCardCpaAdvertise.class), smartCard.f2313a);
                return dVar;
        }
        return null;
    }
}
