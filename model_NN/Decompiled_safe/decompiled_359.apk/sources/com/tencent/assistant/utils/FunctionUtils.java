package com.tencent.assistant.utils;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.text.Html;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.Global;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.DownloadProxy;
import com.tencent.assistant.manager.SelfUpdateManager;
import com.tencent.assistant.manager.cq;
import com.tencent.assistant.manager.cr;
import com.tencent.assistant.model.PluginStartEntry;
import com.tencent.assistant.model.j;
import com.tencent.assistant.module.dm;
import com.tencent.assistant.plugin.PluginInfo;
import com.tencent.assistant.plugin.PluginProxyManager;
import com.tencent.assistant.plugin.mgr.d;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.beacon.event.a;
import com.tencent.connect.common.Constants;
import java.util.HashMap;
import java.util.List;

/* compiled from: ProGuard */
public class FunctionUtils {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static Dialog f2612a = null;

    /* compiled from: ProGuard */
    public enum InstallSpaceNotEnoughForwardPage {
        INSTALLED_APP_MANAGER,
        SPACE_CLEAR_MANAGER
    }

    public static void a(int i, long j) {
        TemporaryThreadManager.get().start(new aq(j, i));
    }

    protected static PluginStartEntry a(String str) {
        PluginInfo a2 = d.b().a(str);
        if (a2 == null) {
            return null;
        }
        List<j> b = dm.a().b();
        if (b != null && b.size() > 0) {
            for (j next : b) {
                if (next.c.equals(a2.getPackageName()) && dm.a().c(a2.getPackageName())) {
                    return new PluginStartEntry(next.f1666a, next.e, next.c, next.d, next.p, next.h);
                }
            }
        }
        return null;
    }

    public static void a(int i, boolean z, InstallSpaceNotEnoughForwardPage installSpaceNotEnoughForwardPage) {
        TemporaryThreadManager.get().start(new ar(installSpaceNotEnoughForwardPage, i, z));
    }

    public static synchronized void a() {
        synchronized (FunctionUtils.class) {
            ba.a().post(new as());
        }
    }

    public static synchronized void b() {
        synchronized (FunctionUtils.class) {
            TemporaryThreadManager.get().start(new au());
        }
    }

    public static void a(Activity activity) {
        int h = DownloadProxy.a().h();
        if (h != 0) {
            if (SelfUpdateManager.a().o().c()) {
                SelfUpdateManager.a().o().b();
            }
            a(activity, h);
        } else if (SelfUpdateManager.a().o().c()) {
            d(activity);
        } else {
            activity.finish();
            AstApp.i().f();
        }
        cr.a().d();
        if (!Global.isOfficial()) {
            d();
        }
    }

    private static void a(Activity activity, int i) {
        Dialog dialog = new Dialog(activity, R.style.dialog);
        dialog.setCancelable(true);
        View a2 = a(activity, dialog, i);
        if (a2 != null) {
            dialog.addContentView(a2, new ViewGroup.LayoutParams(-1, -2));
            dialog.setOwnerActivity(activity);
            if (!activity.isFinishing()) {
                dialog.show();
            }
        }
    }

    private static View a(Activity activity, Dialog dialog, int i) {
        try {
            View inflate = activity.getLayoutInflater().inflate((int) R.layout.dialog_exist_layout, (ViewGroup) null);
            ((TextView) inflate.findViewById(R.id.msg)).setText(activity.getString(R.string.dialog_exist_downloading, new Object[]{Integer.valueOf(i)}));
            CheckBox checkBox = (CheckBox) inflate.findViewById(R.id.exist_check);
            checkBox.setChecked(m.a().y());
            checkBox.setOnCheckedChangeListener(new av());
            ((Button) inflate.findViewById(R.id.nagitive_btn)).setOnClickListener(new aw(dialog));
            ((Button) inflate.findViewById(R.id.positive_btn)).setOnClickListener(new ax(dialog, activity));
            return inflate;
        } catch (Throwable th) {
            th.printStackTrace();
            cq.a().b();
            return null;
        }
    }

    private static void d(Activity activity) {
        Dialog dialog = new Dialog(activity, R.style.dialog);
        dialog.setCancelable(true);
        dialog.addContentView(a(activity, dialog), new ViewGroup.LayoutParams(activity.getResources().getDimensionPixelSize(R.dimen.self_update_width), activity.getResources().getDimensionPixelSize(R.dimen.selfdetail_dialog_height)));
        dialog.setOwnerActivity(activity);
        if (!activity.isFinishing()) {
            dialog.show();
        }
        SelfUpdateManager.a().o().b();
        k.a(b(activity));
    }

    public static STInfoV2 b(Activity activity) {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(activity, 100);
        buildSTInfo.slotId = "201601";
        return buildSTInfo;
    }

    private static View a(Activity activity, Dialog dialog) {
        View inflate = activity.getLayoutInflater().inflate((int) R.layout.dialog_selfupdate_normal, (ViewGroup) null);
        TXImageView tXImageView = (TXImageView) inflate.findViewById(R.id.self_update_banner);
        TextView textView = (TextView) inflate.findViewById(R.id.msg_versionfeature);
        Button button = (Button) inflate.findViewById(R.id.btn_ignore);
        Button button2 = (Button) inflate.findViewById(R.id.btn_update);
        TextView textView2 = (TextView) inflate.findViewById(R.id.title_tip_1);
        TextView textView3 = (TextView) inflate.findViewById(R.id.title_tip_2);
        SelfUpdateManager.SelfUpdateInfo d = SelfUpdateManager.a().d();
        if (d != null) {
            if (!TextUtils.isEmpty(d.h)) {
                textView.setText(String.format(activity.getResources().getString(R.string.dialog_update_feature), d.h));
            }
            if (SelfUpdateManager.a().a(d.e)) {
                textView2.setVisibility(0);
                textView2.setText((int) R.string.selfupdate_apk_file_ready);
                button2.setText((int) R.string.selfupdate_update_right_now);
                button2.setTextColor(activity.getResources().getColor(R.color.appdetail_btn_text_color_to_install));
                button2.setBackgroundResource(R.drawable.appdetail_bar_btn_to_install_selector_v5);
            } else {
                textView2.setVisibility(8);
                button2.setText(Constants.STR_EMPTY);
                button2.append(new SpannableString(activity.getResources().getString(R.string.download)));
                SpannableString spannableString = new SpannableString(" (" + bt.b(d.a()) + "B)");
                spannableString.setSpan(new AbsoluteSizeSpan(15, true), 0, spannableString.length(), 17);
                button2.append(spannableString);
            }
            textView3.setText(Html.fromHtml(d.q));
            tXImageView.setImageBitmap(a(BitmapFactory.decodeResource(activity.getResources(), R.drawable.self_update_banner)));
            button.setOnClickListener(new ay(dialog, activity));
            button2.setOnClickListener(new az(dialog, activity, d));
            k.a(new STInfoV2(STConst.ST_PAGE_SELF_UPDATE_EXIT, STConst.ST_DEFAULT_SLOT, STConst.ST_PAGE_SELF_UPDATE_EXIT, STConst.ST_DEFAULT_SLOT, 100));
        }
        return inflate;
    }

    /* access modifiers changed from: private */
    public static boolean e(Activity activity) {
        if (activity.getParent() != null) {
            return activity.getParent().moveTaskToBack(true);
        }
        return activity.moveTaskToBack(true);
    }

    public static Bitmap a(Bitmap bitmap) {
        Bitmap createBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        Paint paint = new Paint();
        Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight() + 4);
        RectF rectF = new RectF(rect);
        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(-12434878);
        paint.setAntiAlias(true);
        canvas.drawRoundRect(rectF, 4.0f, 4.0f, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        return createBitmap;
    }

    private static void d() {
        HashMap hashMap = new HashMap();
        hashMap.put("B1", PluginProxyManager.getPhoneGuid());
        a.a("double_click_exit", true, -1, -1, hashMap, true);
    }
}
