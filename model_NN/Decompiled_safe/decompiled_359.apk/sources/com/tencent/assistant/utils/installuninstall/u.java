package com.tencent.assistant.utils.installuninstall;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.qq.AppService.AstApp;
import com.tencent.assistant.m;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.v;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoV2;
import java.util.List;

/* compiled from: ProGuard */
final class u extends Handler {
    u(Looper looper) {
        super(looper);
    }

    public void handleMessage(Message message) {
        List list;
        int i;
        int i2 = 1;
        if (message.obj == null) {
            return;
        }
        if (message.what == 1) {
            InstallUninstallTaskBean installUninstallTaskBean = (InstallUninstallTaskBean) message.obj;
            v.a(p.a().b(installUninstallTaskBean));
            m.a().b(m.a().f() + 1);
            if (AstApp.m() != null) {
                k.a(new STInfoV2(STConst.ST_DIALOG_INSTALL_ROOT_AUTH_ALERT, STConst.ST_DEFAULT_SLOT, AstApp.m().f(), STConst.ST_DEFAULT_SLOT, 100));
                if (installUninstallTaskBean.action == 1) {
                    i = 0;
                } else {
                    i = 1;
                }
                p.b(0, i);
            }
        } else if (message.what == 2 && (list = (List) message.obj) != null && !list.isEmpty()) {
            v.a(p.a().b(list));
            m.a().b(m.a().f() + 1);
            if (AstApp.m() != null) {
                k.a(new STInfoV2(STConst.ST_DIALOG_INSTALL_ROOT_AUTH_ALERT, STConst.ST_DEFAULT_SLOT, AstApp.m().f(), STConst.ST_DEFAULT_SLOT, 100));
                if (((InstallUninstallTaskBean) list.get(0)).action == 1) {
                    i2 = 0;
                }
                p.b(0, i2);
            }
        }
    }
}
