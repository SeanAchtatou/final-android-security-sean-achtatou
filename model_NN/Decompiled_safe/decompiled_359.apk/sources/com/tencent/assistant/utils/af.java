package com.tencent.assistant.utils;

import android.app.Activity;
import android.content.Intent;
import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.link.b;

/* compiled from: ProGuard */
final class af extends AppConst.TwoBtnDialogInfo {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Activity f2628a;

    af(Activity activity) {
        this.f2628a = activity;
    }

    public void onRightBtnClick() {
        Intent intent = new Intent("android.settings.SETTINGS");
        if (b.a(this.f2628a, intent)) {
            this.f2628a.startActivity(intent);
        } else {
            Toast.makeText(this.f2628a, (int) R.string.dialog_not_found_activity, 0).show();
        }
    }

    public void onLeftBtnClick() {
    }

    public void onCancell() {
    }
}
