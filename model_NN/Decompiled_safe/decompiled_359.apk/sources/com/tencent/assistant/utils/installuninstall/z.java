package com.tencent.assistant.utils.installuninstall;

import android.widget.Toast;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.cr;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
class z implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ y f2717a;

    z(y yVar) {
        this.f2717a = yVar;
    }

    public void run() {
        int i;
        boolean a2 = InstallUninstallUtil.a();
        boolean c = cr.a().c();
        XLog.d("InstallUninstallHelper", "handle root switch, permRootReqResult=" + a2 + ", tempRootReqResult=" + c);
        if (a2 || c) {
            m.a().b(true);
            for (InstallUninstallTaskBean installUninstallTaskBean : this.f2717a.f2716a) {
                if (c) {
                    i = 2;
                } else {
                    i = 1;
                }
                installUninstallTaskBean.style = i;
                if (installUninstallTaskBean.action == 1) {
                    ac.a().b(installUninstallTaskBean);
                } else if (installUninstallTaskBean.action == -1) {
                    TemporaryThreadManager.get().start(new aa(this, installUninstallTaskBean));
                }
            }
            return;
        }
        Toast.makeText(AstApp.i().getBaseContext(), AstApp.i().getBaseContext().getString(R.string.toast_root_request_fail), 1).show();
        for (InstallUninstallTaskBean installUninstallTaskBean2 : this.f2717a.f2716a) {
            if (installUninstallTaskBean2.action == 1) {
                ac.a().a(installUninstallTaskBean2);
            } else if (installUninstallTaskBean2.action == -1 && installUninstallTaskBean2.trySystemAfterSilentFail) {
                this.f2717a.b.f2708a.sendMessage(this.f2717a.b.f2708a.obtainMessage(1024, installUninstallTaskBean2.packageName));
                InstallUninstallUtil.a(installUninstallTaskBean2.packageName);
            }
        }
    }
}
