package com.tencent.assistant.utils;

/* compiled from: ProGuard */
public class bs {
    /* JADX WARNING: Removed duplicated region for block: B:28:0x005b A[SYNTHETIC, Splitter:B:28:0x005b] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0060 A[SYNTHETIC, Splitter:B:31:0x0060] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x006d A[SYNTHETIC, Splitter:B:38:0x006d] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0072 A[SYNTHETIC, Splitter:B:41:0x0072] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a(java.lang.String r6) {
        /*
            r3 = 0
            java.lang.String r0 = ""
            java.util.zip.ZipFile r2 = new java.util.zip.ZipFile     // Catch:{ IOException -> 0x0091, all -> 0x0069 }
            r2.<init>(r6)     // Catch:{ IOException -> 0x0091, all -> 0x0069 }
            java.io.File r1 = new java.io.File     // Catch:{ IOException -> 0x0055 }
            r1.<init>(r6)     // Catch:{ IOException -> 0x0055 }
            boolean r1 = r1.exists()     // Catch:{ IOException -> 0x0055 }
            if (r1 == 0) goto L_0x003c
            java.lang.String r1 = "META-INF/MANIFEST.MF"
            java.util.zip.ZipEntry r1 = r2.getEntry(r1)     // Catch:{ IOException -> 0x0055 }
            if (r1 != 0) goto L_0x0021
            java.lang.String r1 = "META-INF\\MANIFEST.MF"
            java.util.zip.ZipEntry r1 = r2.getEntry(r1)     // Catch:{ IOException -> 0x0055 }
        L_0x0021:
            if (r1 == 0) goto L_0x0031
            java.io.BufferedInputStream r4 = new java.io.BufferedInputStream     // Catch:{ IOException -> 0x0055 }
            java.io.InputStream r1 = r2.getInputStream(r1)     // Catch:{ IOException -> 0x0055 }
            r4.<init>(r1)     // Catch:{ IOException -> 0x0055 }
            java.lang.String r0 = com.tencent.assistant.utils.bj.a(r4)     // Catch:{ IOException -> 0x0094, all -> 0x008e }
            r3 = r4
        L_0x0031:
            if (r3 == 0) goto L_0x0036
            r3.close()     // Catch:{ IOException -> 0x0085 }
        L_0x0036:
            if (r2 == 0) goto L_0x003b
            r2.close()     // Catch:{ IOException -> 0x008a }
        L_0x003b:
            return r0
        L_0x003c:
            java.lang.String r1 = "ManifestMd5Generator"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0055 }
            r4.<init>()     // Catch:{ IOException -> 0x0055 }
            java.lang.String r5 = "Apk file not exist: "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ IOException -> 0x0055 }
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ IOException -> 0x0055 }
            java.lang.String r4 = r4.toString()     // Catch:{ IOException -> 0x0055 }
            com.tencent.assistant.utils.XLog.e(r1, r4)     // Catch:{ IOException -> 0x0055 }
            goto L_0x0031
        L_0x0055:
            r1 = move-exception
        L_0x0056:
            r1.printStackTrace()     // Catch:{ all -> 0x008c }
            if (r3 == 0) goto L_0x005e
            r3.close()     // Catch:{ IOException -> 0x0080 }
        L_0x005e:
            if (r2 == 0) goto L_0x003b
            r2.close()     // Catch:{ IOException -> 0x0064 }
            goto L_0x003b
        L_0x0064:
            r1 = move-exception
        L_0x0065:
            r1.printStackTrace()
            goto L_0x003b
        L_0x0069:
            r0 = move-exception
            r2 = r3
        L_0x006b:
            if (r3 == 0) goto L_0x0070
            r3.close()     // Catch:{ IOException -> 0x0076 }
        L_0x0070:
            if (r2 == 0) goto L_0x0075
            r2.close()     // Catch:{ IOException -> 0x007b }
        L_0x0075:
            throw r0
        L_0x0076:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0070
        L_0x007b:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0075
        L_0x0080:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x005e
        L_0x0085:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0036
        L_0x008a:
            r1 = move-exception
            goto L_0x0065
        L_0x008c:
            r0 = move-exception
            goto L_0x006b
        L_0x008e:
            r0 = move-exception
            r3 = r4
            goto L_0x006b
        L_0x0091:
            r1 = move-exception
            r2 = r3
            goto L_0x0056
        L_0x0094:
            r1 = move-exception
            r3 = r4
            goto L_0x0056
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.utils.bs.a(java.lang.String):java.lang.String");
    }
}
