package com.tencent.assistant.utils.installuninstall;

import com.tencent.assistant.download.DownloadInfo;

/* compiled from: ProGuard */
class q implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DownloadInfo f2709a;
    final /* synthetic */ boolean b;
    final /* synthetic */ p c;

    q(p pVar, DownloadInfo downloadInfo, boolean z) {
        this.c = pVar;
        this.f2709a = downloadInfo;
        this.b = z;
    }

    public void run() {
        if (this.f2709a != null) {
            this.c.a(this.f2709a.downloadTicket, this.f2709a.packageName, this.f2709a.name, this.f2709a.getFilePath(), this.f2709a.versionCode, this.f2709a.signatrue, this.f2709a.downloadTicket, this.f2709a.fileSize, this.b, this.f2709a);
        }
    }
}
