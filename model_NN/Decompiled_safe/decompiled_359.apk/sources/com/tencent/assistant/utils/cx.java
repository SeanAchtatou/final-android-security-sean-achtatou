package com.tencent.assistant.utils;

import java.text.SimpleDateFormat;

/* compiled from: ProGuard */
final class cx extends ThreadLocal<SimpleDateFormat> {
    cx() {
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public SimpleDateFormat initialValue() {
        return new SimpleDateFormat("yy年MM月dd日");
    }
}
