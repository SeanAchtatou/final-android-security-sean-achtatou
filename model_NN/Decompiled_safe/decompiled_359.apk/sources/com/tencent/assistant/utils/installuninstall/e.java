package com.tencent.assistant.utils.installuninstall;

/* compiled from: ProGuard */
class e extends o {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ InstallUninstallDialogManager f2699a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    e(InstallUninstallDialogManager installUninstallDialogManager) {
        super(installUninstallDialogManager);
        this.f2699a = installUninstallDialogManager;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager, boolean):boolean
     arg types: [com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager, int]
     candidates:
      com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager, com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_RESULT):com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_RESULT
      com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_TYPE, com.tencent.assistant.utils.installuninstall.InstallUninstallTaskBean):boolean
      com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager, boolean):boolean */
    public void onLeftBtnClick() {
        boolean unused = this.f2699a.c = false;
        this.f2699a.c();
        this.f2699a.a(this.b.downloadTicket);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager, boolean):boolean
     arg types: [com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager, int]
     candidates:
      com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager, com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_RESULT):com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_RESULT
      com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_TYPE, com.tencent.assistant.utils.installuninstall.InstallUninstallTaskBean):boolean
      com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager, boolean):boolean */
    public void onRightBtnClick() {
        boolean unused = this.f2699a.c = true;
        this.f2699a.c();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager, boolean):boolean
     arg types: [com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager, int]
     candidates:
      com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager, com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_RESULT):com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_RESULT
      com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_TYPE, com.tencent.assistant.utils.installuninstall.InstallUninstallTaskBean):boolean
      com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager, boolean):boolean */
    public void onCancell() {
        boolean unused = this.f2699a.c = false;
        this.f2699a.c();
        this.f2699a.a(this.b.downloadTicket);
    }
}
