package com.tencent.assistant.floatingwindow;

import android.app.Dialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.m;

/* compiled from: ProGuard */
class l implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ j f1298a;

    l(j jVar) {
        this.f1298a = jVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean
     arg types: [java.lang.String, boolean]
     candidates:
      com.tencent.assistant.m.b(byte, int):void
      com.tencent.assistant.m.b(byte, java.lang.String):void
      com.tencent.assistant.m.b(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.b(java.lang.Long, int):boolean
      com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean */
    public void run() {
        if (AstApp.i().l() && AstApp.m() != null) {
            Dialog dialog = new Dialog(AstApp.m(), R.style.dialog);
            Window window = dialog.getWindow();
            window.setLayout(-1, -2);
            WindowManager.LayoutParams attributes = window.getAttributes();
            attributes.gravity = 17;
            attributes.width = (int) (((float) window.getWindowManager().getDefaultDisplay().getWidth()) * 0.867f);
            window.setAttributes(attributes);
            View inflate = LayoutInflater.from(AstApp.m()).inflate((int) R.layout.float_window_create_dialog_layout, (ViewGroup) null);
            ((Button) inflate.findViewById(R.id.positive_btn)).setOnClickListener(new m(this, dialog));
            dialog.setContentView(inflate, new WindowManager.LayoutParams(-1, -2));
            if (AstApp.i().l()) {
                dialog.show();
                m.a().b("key_has_show_float_window_create_tips", (Object) true);
            }
        }
    }
}
