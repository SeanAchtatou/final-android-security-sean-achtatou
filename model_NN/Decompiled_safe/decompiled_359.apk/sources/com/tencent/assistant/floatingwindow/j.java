package com.tencent.assistant.floatingwindow;

import android.content.Context;
import android.text.SpannableString;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.m;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.ba;
import com.tencent.assistant.utils.br;
import com.tencent.assistantv2.st.b.b;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
public class j {
    private static j h = null;

    /* renamed from: a  reason: collision with root package name */
    private FloatWindowSmallView f1296a;
    private FloatWindowBigView b;
    private FloatWindowResultView c;
    private WindowManager.LayoutParams d;
    private WindowManager.LayoutParams e;
    private WindowManager.LayoutParams f;
    private WindowManager g;

    private j() {
    }

    public static synchronized j a() {
        j jVar;
        synchronized (j.class) {
            if (h == null) {
                h = new j();
            }
            jVar = h;
        }
        return jVar;
    }

    public void b() {
        TemporaryThreadManager.get().start(new k(this));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, long):long
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean */
    /* access modifiers changed from: private */
    public void h() {
        if (!m.a().a("key_has_show_float_window_create_tips", false)) {
            ba.a().post(new l(this));
        }
    }

    public void c() {
        TemporaryThreadManager.get().start(new n(this));
    }

    public void a(Context context) {
        WindowManager i = i();
        int width = i.getDefaultDisplay().getWidth();
        int height = i.getDefaultDisplay().getHeight();
        if (this.f1296a == null) {
            this.f1296a = new FloatWindowSmallView(context);
            if (this.d == null) {
                this.d = new WindowManager.LayoutParams();
                this.d.type = STConst.ST_PAGE_HOT;
                this.d.format = 1;
                this.d.flags = 40;
                this.d.gravity = 51;
                this.d.width = FloatWindowSmallView.f1280a;
                this.d.height = FloatWindowSmallView.b;
                this.d.x = width;
                this.d.y = height / 2;
            }
            this.f1296a.a(this.d);
            i.addView(this.f1296a, this.d);
        }
    }

    public void b(Context context) {
        if (this.f1296a != null) {
            i().removeView(this.f1296a);
            this.f1296a.a();
            this.f1296a = null;
        }
    }

    public void c(Context context) {
        WindowManager i = i();
        int width = i.getDefaultDisplay().getWidth();
        int height = i.getDefaultDisplay().getHeight();
        if (this.b == null) {
            this.b = new FloatWindowBigView(context);
            if (this.e == null) {
                this.e = new WindowManager.LayoutParams();
                this.e.x = (width / 2) - (FloatWindowBigView.f1278a / 2);
                this.e.y = (height / 2) - (FloatWindowBigView.b / 2);
                this.e.type = STConst.ST_PAGE_HOT;
                this.e.flags = 131328;
                this.e.format = 1;
                this.e.gravity = 51;
                this.e.width = -1;
                this.e.height = -1;
            }
            i.addView(this.b, this.e);
        }
    }

    public void d(Context context) {
        if (this.b != null) {
            i().removeView(this.b);
            this.b = null;
        }
    }

    public void d() {
        if (this.f1296a != null) {
            View findViewById = this.f1296a.findViewById(R.id.small_window_layout);
            ((TextView) this.f1296a.findViewById(R.id.percent)).setText(f());
            float d2 = br.d();
            if (d2 < 0.7f) {
                findViewById.setBackgroundResource(R.drawable.icon_float_window_small_circle_bg_1);
            } else if (d2 <= 0.7f || d2 >= 0.85f) {
                findViewById.setBackgroundResource(R.drawable.icon_float_window_small_circle_bg_3);
            } else {
                findViewById.setBackgroundResource(R.drawable.icon_float_window_small_circle_bg_2);
            }
        }
    }

    public boolean e() {
        return (this.f1296a == null && this.b == null) ? false : true;
    }

    private WindowManager i() {
        if (this.g == null) {
            this.g = (WindowManager) AstApp.i().getSystemService("window");
        }
        return this.g;
    }

    public SpannableString f() {
        int d2 = (int) (br.d() * 100.0f);
        String str = d2 + "%";
        SpannableString spannableString = new SpannableString(str);
        spannableString.setSpan(new AbsoluteSizeSpan(10, true), str.lastIndexOf("%"), str.length(), 33);
        if (d2 > 85) {
            spannableString.setSpan(new ForegroundColorSpan(AstApp.i().getResources().getColor(R.color.floating_winddow_high_memory_text_color)), 0, str.length(), 33);
        } else {
            spannableString.setSpan(new ForegroundColorSpan(AstApp.i().getResources().getColor(R.color.white)), 0, str.length(), 33);
        }
        return spannableString;
    }

    public void a(String str, String str2) {
        if (this.c == null) {
            WindowManager i = i();
            int width = i.getDefaultDisplay().getWidth();
            i.getDefaultDisplay().getHeight();
            this.c = new FloatWindowResultView(AstApp.i());
            this.c.a(str);
            this.c.b(str2);
            if (this.f == null) {
                this.f = new WindowManager.LayoutParams();
                this.f.x = (width / 2) - (FloatWindowResultView.f1279a / 2);
                this.f.y = 0;
                this.f.type = STConst.ST_PAGE_NECESSARY;
                this.f.format = 1;
                this.f.gravity = 51;
                this.f.width = FloatWindowResultView.f1279a;
                this.f.height = FloatWindowResultView.b;
            }
            i.addView(this.c, this.f);
        }
    }

    public void g() {
        if (this.c != null) {
            i().removeView(this.c);
            this.c = null;
        }
    }

    public void a(Animation.AnimationListener animationListener) {
        if (this.c != null) {
            this.c.a(animationListener);
        }
    }

    public void a(int i, String str, int i2) {
        b.getInstance().exposure(new STInfoV2(i, str, 2000, STConst.ST_DEFAULT_SLOT, i2));
    }
}
