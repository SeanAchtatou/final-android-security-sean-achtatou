package com.tencent.assistant.floatingwindow;

import android.view.animation.Animation;
import com.tencent.assistant.utils.ba;

/* compiled from: ProGuard */
class e implements Animation.AnimationListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ FloatWindowSmallView f1291a;

    e(FloatWindowSmallView floatWindowSmallView) {
        this.f1291a = floatWindowSmallView;
    }

    public void onAnimationStart(Animation animation) {
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationEnd(Animation animation) {
        this.f1291a.e.setVisibility(8);
        ba.a().postDelayed(new f(this), 1000);
    }
}
