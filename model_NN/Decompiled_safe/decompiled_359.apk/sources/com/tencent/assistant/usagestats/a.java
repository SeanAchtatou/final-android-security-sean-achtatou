package com.tencent.assistant.usagestats;

import com.tencent.assistant.usagestats.UsagestatsSTManager;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistantv2.st.business.BaseSTManagerV2;

/* compiled from: ProGuard */
public class a extends BaseSTManagerV2 {

    /* renamed from: a  reason: collision with root package name */
    private static a f2591a = null;

    public static synchronized a a() {
        a aVar;
        synchronized (a.class) {
            if (f2591a == null) {
                f2591a = new a();
            }
            aVar = f2591a;
        }
        return aVar;
    }

    public byte getSTType() {
        return 17;
    }

    public void flush() {
    }

    public void a(UsagestatsSTManager.ReportScene reportScene) {
        TemporaryThreadManager.get().start(new b(this, reportScene));
    }
}
