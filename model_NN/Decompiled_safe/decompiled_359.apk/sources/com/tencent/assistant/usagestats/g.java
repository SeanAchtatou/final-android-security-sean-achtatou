package com.tencent.assistant.usagestats;

import java.util.Iterator;

/* compiled from: ProGuard */
final class g<T> implements Iterator<T> {

    /* renamed from: a  reason: collision with root package name */
    final int f2596a;
    int b;
    int c;
    boolean d = false;
    final /* synthetic */ f e;

    g(f fVar, int i) {
        this.e = fVar;
        this.f2596a = i;
        this.b = fVar.a();
    }

    public boolean hasNext() {
        return this.c < this.b;
    }

    public T next() {
        T a2 = this.e.a(this.c, this.f2596a);
        this.c++;
        this.d = true;
        return a2;
    }

    public void remove() {
        if (!this.d) {
            throw new IllegalStateException();
        }
        this.c--;
        this.b--;
        this.d = false;
        this.e.a(this.c);
    }
}
