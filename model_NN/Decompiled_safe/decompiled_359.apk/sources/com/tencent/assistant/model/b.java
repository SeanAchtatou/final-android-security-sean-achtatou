package com.tencent.assistant.model;

import java.util.List;

/* compiled from: ProGuard */
public class b {

    /* renamed from: a  reason: collision with root package name */
    private List<SimpleAppModel> f1659a;
    private List<?> b;
    private long c = 0;
    private String d = null;
    private long e = 0;
    private List<e> f;
    private String g = null;

    public long a() {
        return this.e;
    }

    public void a(long j) {
        this.e = j;
    }

    public String b() {
        return this.d;
    }

    public void a(String str) {
        this.d = str;
    }

    public void a(List<?> list) {
        this.b = list;
    }

    public List<SimpleAppModel> c() {
        return this.f1659a;
    }

    public void b(List<SimpleAppModel> list) {
        this.f1659a = list;
    }

    public long d() {
        return this.c;
    }

    public void b(long j) {
        this.c = j;
    }

    public List<e> e() {
        return this.f;
    }

    public void c(List<e> list) {
        this.f = list;
    }
}
