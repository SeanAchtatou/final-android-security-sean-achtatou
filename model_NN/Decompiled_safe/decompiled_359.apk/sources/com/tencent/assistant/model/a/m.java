package com.tencent.assistant.model.a;

import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.u;
import com.tencent.assistant.protocol.jce.SmartCardPlayerShow;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class m extends i {

    /* renamed from: a  reason: collision with root package name */
    public String f1646a;
    public boolean b;
    public long c;
    public SimpleAppModel d;
    public String e;

    public List<Long> h() {
        if (this.d == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(Long.valueOf(this.d.f1634a));
        return arrayList;
    }

    public void a(List<Long> list) {
        if (list != null && list.size() != 0 && this.d != null) {
            for (Long longValue : list) {
                if (longValue.longValue() == this.d.f1634a) {
                    this.d = null;
                }
            }
        }
    }

    public void a(SmartCardPlayerShow smartCardPlayerShow, int i) {
        if (smartCardPlayerShow != null) {
            this.i = i;
            this.d = u.a(smartCardPlayerShow.f2327a);
            this.f1646a = smartCardPlayerShow.b;
            this.e = smartCardPlayerShow.c;
            this.c = smartCardPlayerShow.e;
        }
    }
}
