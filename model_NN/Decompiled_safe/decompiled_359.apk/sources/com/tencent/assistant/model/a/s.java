package com.tencent.assistant.model.a;

import com.tencent.assistant.protocol.jce.SmartCardCfg;

/* compiled from: ProGuard */
public class s {

    /* renamed from: a  reason: collision with root package name */
    public int f1652a;
    public int b;
    public int c;
    public boolean d;
    public int e;
    public int f;
    public int g;
    public long h;
    public int i;
    public int j;

    public void a(SmartCardCfg smartCardCfg) {
        if (smartCardCfg != null) {
            this.e = smartCardCfg.f2316a;
            this.d = smartCardCfg.b;
            this.g = smartCardCfg.e;
            this.f1652a = smartCardCfg.c;
            this.b = smartCardCfg.d;
            this.i = smartCardCfg.f;
            this.j = smartCardCfg.g;
        }
    }

    public String toString() {
        return "cardtype:" + this.e + ",cardid:" + this.f + ",maxdayshow:" + this.f1652a + ",week max show:" + this.b;
    }

    public int a() {
        return i.a(this.e, this.f);
    }
}
