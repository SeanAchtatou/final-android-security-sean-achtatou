package com.tencent.assistant.model;

import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.protocol.jce.AppDetailWithComment;

/* compiled from: ProGuard */
public class c {

    /* renamed from: a  reason: collision with root package name */
    public AppDetailWithComment f1660a;
    public LocalApkInfo b;

    public c(AppDetailWithComment appDetailWithComment) {
        this.f1660a = appDetailWithComment;
    }

    public void a(LocalApkInfo localApkInfo) {
        this.b = localApkInfo;
    }

    public SimpleAppModel a() {
        SimpleAppModel simpleAppModel = new SimpleAppModel(this.f1660a);
        simpleAppModel.a(this.b);
        return simpleAppModel;
    }

    public String b() {
        if (this.f1660a != null) {
            return this.f1660a.f1993a.f1989a.b;
        }
        if (this.b != null) {
            return this.b.mPackageName;
        }
        return null;
    }
}
