package com.tencent.assistant.model.a;

import com.tencent.assistant.protocol.jce.SmartCardLibao;
import com.tencent.assistant.protocol.jce.SmartCardLibaoItem;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class g extends i {

    /* renamed from: a  reason: collision with root package name */
    public int f1641a;
    public int b = 0;
    public ArrayList<SmartCardLibaoItem> c = null;

    public void a(SmartCardLibao smartCardLibao, int i) {
        b(smartCardLibao, i);
        if (smartCardLibao != null) {
            this.c = smartCardLibao.f;
            this.b = smartCardLibao.e;
        }
    }

    private void b(SmartCardLibao smartCardLibao, int i) {
        this.i = i;
        if (smartCardLibao != null) {
            this.k = smartCardLibao.f2321a;
            this.m = smartCardLibao.b;
            this.n = smartCardLibao.c;
            this.o = smartCardLibao.d;
            this.q = smartCardLibao.g;
        }
    }
}
