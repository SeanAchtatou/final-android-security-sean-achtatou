package com.tencent.assistant.model.a;

import com.tencent.assistant.module.u;
import com.tencent.assistant.protocol.jce.SmartCardRecommandation;
import com.tencent.assistant.protocol.jce.SmartCardRecommandedItem;
import com.tencent.assistant.protocol.jce.SmartCardRecommandedPerson;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class k extends i {

    /* renamed from: a  reason: collision with root package name */
    public long f1644a;
    public int b = 0;
    public List<l> c;
    public int d;
    public int e;

    public boolean a(int i, SmartCardRecommandation smartCardRecommandation) {
        if (smartCardRecommandation == null) {
            return false;
        }
        this.i = i;
        this.k = smartCardRecommandation.f2328a;
        this.f1644a = smartCardRecommandation.c;
        this.m = smartCardRecommandation.d;
        this.o = smartCardRecommandation.e;
        if (this.c == null) {
            this.c = new ArrayList();
        } else {
            this.c.clear();
        }
        if (smartCardRecommandation.b == null || smartCardRecommandation.b.size() <= 0) {
            return false;
        }
        Iterator<SmartCardRecommandedItem> it = smartCardRecommandation.b.iterator();
        while (it.hasNext()) {
            SmartCardRecommandedItem next = it.next();
            l lVar = new l();
            lVar.b = next.b;
            lVar.f1645a = u.a(next.f2329a);
            lVar.c = b(next.c);
            this.c.add(lVar);
        }
        return true;
    }

    public List<j> b(List<SmartCardRecommandedPerson> list) {
        ArrayList arrayList = new ArrayList();
        if (list != null && list.size() > 0) {
            for (SmartCardRecommandedPerson a2 : list) {
                j jVar = new j();
                jVar.a(a2);
                arrayList.add(jVar);
            }
        }
        return arrayList;
    }

    public List<Long> h() {
        ArrayList arrayList = new ArrayList();
        if (this.c != null && this.c.size() > 0) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= this.d || i2 >= this.c.size()) {
                    break;
                }
                arrayList.add(Long.valueOf(this.c.get(i2).f1645a.f1634a));
                i = i2 + 1;
            }
        }
        return arrayList;
    }

    public void a(List<Long> list) {
        if (list != null && list.size() != 0 && this.c != null && this.c.size() != 0) {
            ArrayList arrayList = new ArrayList();
            for (l next : this.c) {
                if (list.contains(Long.valueOf(next.f1645a.f1634a))) {
                    arrayList.add(next);
                }
            }
            this.c.removeAll(arrayList);
        }
    }
}
