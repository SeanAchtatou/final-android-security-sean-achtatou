package com.tencent.assistant.model;

import android.os.Parcel;
import android.os.Parcelable;

/* compiled from: ProGuard */
final class i implements Parcelable.Creator<OnlinePCListItemModel> {
    i() {
    }

    /* renamed from: a */
    public OnlinePCListItemModel[] newArray(int i) {
        return null;
    }

    /* renamed from: a */
    public OnlinePCListItemModel createFromParcel(Parcel parcel) {
        OnlinePCListItemModel onlinePCListItemModel = new OnlinePCListItemModel();
        onlinePCListItemModel.f1629a = parcel.readString();
        onlinePCListItemModel.b = parcel.readString();
        onlinePCListItemModel.c = parcel.readInt();
        onlinePCListItemModel.d = parcel.readString();
        onlinePCListItemModel.e = parcel.readString();
        return onlinePCListItemModel;
    }
}
