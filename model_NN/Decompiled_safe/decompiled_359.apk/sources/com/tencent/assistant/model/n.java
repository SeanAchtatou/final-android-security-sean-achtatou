package com.tencent.assistant.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import java.util.ArrayList;

/* compiled from: ProGuard */
final class n implements Parcelable.Creator<SimpleAppModel> {
    n() {
    }

    /* renamed from: a */
    public SimpleAppModel createFromParcel(Parcel parcel) {
        boolean z = false;
        SimpleAppModel simpleAppModel = new SimpleAppModel();
        simpleAppModel.f1634a = parcel.readLong();
        simpleAppModel.b = parcel.readLong();
        simpleAppModel.c = parcel.readString();
        simpleAppModel.d = parcel.readString();
        simpleAppModel.e = parcel.readString();
        simpleAppModel.p = parcel.readLong();
        simpleAppModel.q = parcel.readDouble();
        simpleAppModel.f = parcel.readString();
        simpleAppModel.g = parcel.readInt();
        simpleAppModel.i = parcel.readString();
        if (parcel.readByte() == 1) {
            if (simpleAppModel.j == null) {
                simpleAppModel.j = new ArrayList<>();
            }
            parcel.readStringList(simpleAppModel.j);
        }
        simpleAppModel.k = parcel.readLong();
        simpleAppModel.o = parcel.readString();
        simpleAppModel.X = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        simpleAppModel.m = parcel.readString();
        simpleAppModel.n = parcel.readLong();
        simpleAppModel.t = parcel.readString();
        if (parcel.readByte() == 1) {
            if (simpleAppModel.u == null) {
                simpleAppModel.u = new ArrayList<>();
            }
            parcel.readStringList(simpleAppModel.u);
        }
        simpleAppModel.v = parcel.readLong();
        simpleAppModel.w = parcel.readString();
        simpleAppModel.x = parcel.readInt() != 0;
        simpleAppModel.B = parcel.readLong();
        simpleAppModel.C = parcel.readString();
        simpleAppModel.D = parcel.readInt();
        simpleAppModel.E = parcel.readString();
        simpleAppModel.F = parcel.readInt();
        simpleAppModel.G = parcel.readString();
        simpleAppModel.H = parcel.readLong();
        simpleAppModel.I = parcel.readLong();
        simpleAppModel.J = parcel.readString();
        simpleAppModel.K = parcel.readLong();
        simpleAppModel.L = parcel.readLong();
        simpleAppModel.M = parcel.readLong();
        simpleAppModel.N = parcel.readLong();
        simpleAppModel.O = parcel.readByte();
        if (parcel.readInt() != 0) {
            z = true;
        }
        simpleAppModel.ab = z;
        simpleAppModel.P = parcel.readByte();
        simpleAppModel.ac = parcel.readString();
        simpleAppModel.ad = parcel.readInt();
        simpleAppModel.Q = parcel.readByte();
        int readInt = parcel.readInt();
        if (readInt > 0) {
            simpleAppModel.y = new byte[readInt];
            parcel.readByteArray(simpleAppModel.y);
        }
        simpleAppModel.af = parcel.readString();
        simpleAppModel.R = parcel.readLong();
        simpleAppModel.ah = parcel.readInt();
        simpleAppModel.ai = parcel.readString();
        simpleAppModel.aj = parcel.readString();
        if (parcel.readByte() == 1) {
            if (simpleAppModel.an == null) {
                simpleAppModel.an = new ArrayList<>();
            }
            parcel.readStringList(simpleAppModel.an);
        }
        if (parcel.readByte() == 1) {
            if (simpleAppModel.ao == null) {
                simpleAppModel.ao = new ArrayList<>();
            }
            parcel.readStringList(simpleAppModel.ao);
        }
        simpleAppModel.ar = parcel.readInt();
        simpleAppModel.as = parcel.readInt();
        simpleAppModel.at = parcel.readInt();
        simpleAppModel.au = parcel.readByte();
        simpleAppModel.z = parcel.readString();
        simpleAppModel.A = parcel.readInt();
        simpleAppModel.ag = parcel.readByte();
        simpleAppModel.aB = parcel.readByte();
        simpleAppModel.aA = parcel.readInt();
        simpleAppModel.aF = parcel.readString();
        simpleAppModel.aG = parcel.readString();
        simpleAppModel.aH = parcel.readString();
        simpleAppModel.aI = parcel.readInt();
        simpleAppModel.aJ = parcel.readString();
        simpleAppModel.aK = parcel.readString();
        if (parcel.readByte() == 1) {
            if (simpleAppModel.ap == null) {
                simpleAppModel.ap = new ArrayList<>();
            }
            parcel.readStringList(simpleAppModel.ap);
        }
        simpleAppModel.aC = parcel.readString();
        simpleAppModel.aD = parcel.readString();
        simpleAppModel.aE = new o();
        simpleAppModel.aE.f1668a = parcel.readString();
        simpleAppModel.aE.b = parcel.readString();
        return simpleAppModel;
    }

    /* renamed from: a */
    public SimpleAppModel[] newArray(int i) {
        return null;
    }
}
