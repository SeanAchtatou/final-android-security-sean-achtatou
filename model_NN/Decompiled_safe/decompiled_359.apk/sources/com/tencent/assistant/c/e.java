package com.tencent.assistant.c;

import android.view.View;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class e implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ BaseActivity f868a;
    final /* synthetic */ d b;

    e(d dVar, BaseActivity baseActivity) {
        this.b = dVar;
        this.f868a = baseActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.c.d.a(com.tencent.assistant.c.d, boolean):void
     arg types: [com.tencent.assistant.c.d, int]
     candidates:
      com.tencent.assistant.c.d.a(com.tencent.assistant.c.d, android.graphics.drawable.AnimationDrawable):void
      com.tencent.assistant.c.d.a(com.tencent.assistant.c.d, boolean):void */
    public void onClick(View view) {
        if (this.b.c != null) {
            this.b.e();
            this.b.b(true);
            STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f868a, 200);
            if (buildSTInfo != null) {
                buildSTInfo.slotId = "15_001";
                k.a(buildSTInfo);
            }
        }
    }
}
