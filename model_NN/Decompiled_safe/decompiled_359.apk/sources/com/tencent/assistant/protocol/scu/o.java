package com.tencent.assistant.protocol.scu;

import java.util.Comparator;

/* compiled from: ProGuard */
public class o implements Comparator<m> {
    /* renamed from: a */
    public int compare(m mVar, m mVar2) {
        if (mVar.d > mVar2.d) {
            return 1;
        }
        if (mVar.d < mVar2.d) {
            return -1;
        }
        return (int) (mVar.e - mVar2.e);
    }
}
