package com.tencent.assistant.protocol.scu;

import com.qq.taf.jce.JceStruct;

/* compiled from: ProGuard */
public class RequestResponePair {
    public int errorCode;
    public JceStruct request;
    public JceStruct response;
}
