package com.tencent.assistant.protocol.jce;

import com.qq.taf.jce.JceDisplayer;
import com.qq.taf.jce.JceInputStream;
import com.qq.taf.jce.JceOutputStream;
import com.qq.taf.jce.JceStruct;
import com.qq.taf.jce.JceUtil;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public final class ApaQua extends JceStruct implements Cloneable {
    static final /* synthetic */ boolean x = (!ApaQua.class.desiredAssertionStatus());

    /* renamed from: a  reason: collision with root package name */
    public String f1976a = Constants.STR_EMPTY;
    public String b = Constants.STR_EMPTY;
    public int c = 0;
    public String d = Constants.STR_EMPTY;
    public String e = Constants.STR_EMPTY;
    public String f = Constants.STR_EMPTY;
    public String g = Constants.STR_EMPTY;
    public String h = Constants.STR_EMPTY;
    public String i = Constants.STR_EMPTY;
    public int j = 0;
    public int k = 0;
    public int l = 0;
    public int m = 0;
    public int n = 0;
    public String o = Constants.STR_EMPTY;
    public String p = Constants.STR_EMPTY;
    public String q = Constants.STR_EMPTY;
    public String r = Constants.STR_EMPTY;
    public String s = Constants.STR_EMPTY;
    public String t = Constants.STR_EMPTY;
    public String u = Constants.STR_EMPTY;
    public int v = 0;
    public int w = 0;

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        ApaQua apaQua = (ApaQua) obj;
        if (!JceUtil.equals(this.f1976a, apaQua.f1976a) || !JceUtil.equals(this.b, apaQua.b) || !JceUtil.equals(this.c, apaQua.c) || !JceUtil.equals(this.d, apaQua.d) || !JceUtil.equals(this.e, apaQua.e) || !JceUtil.equals(this.f, apaQua.f) || !JceUtil.equals(this.g, apaQua.g) || !JceUtil.equals(this.h, apaQua.h) || !JceUtil.equals(this.i, apaQua.i) || !JceUtil.equals(this.j, apaQua.j) || !JceUtil.equals(this.k, apaQua.k) || !JceUtil.equals(this.l, apaQua.l) || !JceUtil.equals(this.m, apaQua.m) || !JceUtil.equals(this.n, apaQua.n) || !JceUtil.equals(this.o, apaQua.o) || !JceUtil.equals(this.p, apaQua.p) || !JceUtil.equals(this.q, apaQua.q) || !JceUtil.equals(this.r, apaQua.r) || !JceUtil.equals(this.s, apaQua.s) || !JceUtil.equals(this.t, apaQua.t) || !JceUtil.equals(this.u, apaQua.u) || !JceUtil.equals(this.v, apaQua.v) || !JceUtil.equals(this.w, apaQua.w)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        try {
            throw new Exception("Need define key first!");
        } catch (Exception e2) {
            e2.printStackTrace();
            return 0;
        }
    }

    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e2) {
            if (x) {
                return null;
            }
            throw new AssertionError();
        }
    }

    public void writeTo(JceOutputStream jceOutputStream) {
        jceOutputStream.write(this.f1976a, 0);
        jceOutputStream.write(this.b, 1);
        jceOutputStream.write(this.c, 2);
        jceOutputStream.write(this.d, 3);
        jceOutputStream.write(this.e, 4);
        jceOutputStream.write(this.f, 5);
        jceOutputStream.write(this.g, 6);
        jceOutputStream.write(this.h, 7);
        jceOutputStream.write(this.i, 8);
        jceOutputStream.write(this.j, 9);
        jceOutputStream.write(this.k, 10);
        jceOutputStream.write(this.l, 11);
        jceOutputStream.write(this.m, 12);
        jceOutputStream.write(this.n, 13);
        jceOutputStream.write(this.o, 14);
        jceOutputStream.write(this.p, 15);
        jceOutputStream.write(this.q, 16);
        jceOutputStream.write(this.r, 17);
        jceOutputStream.write(this.s, 18);
        if (this.t != null) {
            jceOutputStream.write(this.t, 19);
        }
        if (this.u != null) {
            jceOutputStream.write(this.u, 20);
        }
        jceOutputStream.write(this.v, 21);
        jceOutputStream.write(this.w, 22);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int */
    public void readFrom(JceInputStream jceInputStream) {
        this.f1976a = jceInputStream.readString(0, true);
        this.b = jceInputStream.readString(1, true);
        this.c = jceInputStream.read(this.c, 2, true);
        this.d = jceInputStream.readString(3, true);
        this.e = jceInputStream.readString(4, true);
        this.f = jceInputStream.readString(5, true);
        this.g = jceInputStream.readString(6, true);
        this.h = jceInputStream.readString(7, true);
        this.i = jceInputStream.readString(8, true);
        this.j = jceInputStream.read(this.j, 9, true);
        this.k = jceInputStream.read(this.k, 10, true);
        this.l = jceInputStream.read(this.l, 11, true);
        this.m = jceInputStream.read(this.m, 12, true);
        this.n = jceInputStream.read(this.n, 13, true);
        this.o = jceInputStream.readString(14, true);
        this.p = jceInputStream.readString(15, true);
        this.q = jceInputStream.readString(16, true);
        this.r = jceInputStream.readString(17, true);
        this.s = jceInputStream.readString(18, true);
        this.t = jceInputStream.readString(19, false);
        this.u = jceInputStream.readString(20, false);
        this.v = jceInputStream.read(this.v, 21, false);
        this.w = jceInputStream.read(this.w, 22, false);
    }

    public void display(StringBuilder sb, int i2) {
        JceDisplayer jceDisplayer = new JceDisplayer(sb, i2);
        jceDisplayer.display(this.f1976a, "orig");
        jceDisplayer.display(this.b, "appName");
        jceDisplayer.display(this.c, "appVersion");
        jceDisplayer.display(this.d, "channel");
        jceDisplayer.display(this.e, "subChannel");
        jceDisplayer.display(this.f, "buildNumber");
        jceDisplayer.display(this.g, "browser");
        jceDisplayer.display(this.h, "browserBuildNumber");
        jceDisplayer.display(this.i, "sdkVersionName");
        jceDisplayer.display(this.j, "sdkVersionCode");
        jceDisplayer.display(this.k, "romRootFlag");
        jceDisplayer.display(this.l, "xResolution");
        jceDisplayer.display(this.m, "yResolution");
        jceDisplayer.display(this.n, "fontSize");
        jceDisplayer.display(this.o, "phoneBrand");
        jceDisplayer.display(this.p, "phoneMode");
        jceDisplayer.display(this.q, "channelId");
        jceDisplayer.display(this.r, "lc");
        jceDisplayer.display(this.s, "version");
        jceDisplayer.display(this.t, "manufacture");
        jceDisplayer.display(this.u, "productName");
        jceDisplayer.display(this.v, "rootState");
        jceDisplayer.display(this.w, "tempRoot");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.String, boolean):com.qq.taf.jce.JceDisplayer
     arg types: [java.lang.String, int]
     candidates:
      com.qq.taf.jce.JceDisplayer.displaySimple(byte, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(char, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(com.qq.taf.jce.JceStruct, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Collection, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Map, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(boolean, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(byte[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(char[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.String, boolean):com.qq.taf.jce.JceDisplayer */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceDisplayer.displaySimple(int, boolean):com.qq.taf.jce.JceDisplayer
     arg types: [int, int]
     candidates:
      com.qq.taf.jce.JceDisplayer.displaySimple(byte, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(char, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(com.qq.taf.jce.JceStruct, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.String, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Collection, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Map, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(boolean, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(byte[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(char[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int, boolean):com.qq.taf.jce.JceDisplayer */
    public void displaySimple(StringBuilder sb, int i2) {
        JceDisplayer jceDisplayer = new JceDisplayer(sb, i2);
        jceDisplayer.displaySimple(this.f1976a, true);
        jceDisplayer.displaySimple(this.b, true);
        jceDisplayer.displaySimple(this.c, true);
        jceDisplayer.displaySimple(this.d, true);
        jceDisplayer.displaySimple(this.e, true);
        jceDisplayer.displaySimple(this.f, true);
        jceDisplayer.displaySimple(this.g, true);
        jceDisplayer.displaySimple(this.h, true);
        jceDisplayer.displaySimple(this.i, true);
        jceDisplayer.displaySimple(this.j, true);
        jceDisplayer.displaySimple(this.k, true);
        jceDisplayer.displaySimple(this.l, true);
        jceDisplayer.displaySimple(this.m, true);
        jceDisplayer.displaySimple(this.n, true);
        jceDisplayer.displaySimple(this.o, true);
        jceDisplayer.displaySimple(this.p, true);
        jceDisplayer.displaySimple(this.q, true);
        jceDisplayer.displaySimple(this.r, true);
        jceDisplayer.displaySimple(this.s, true);
        jceDisplayer.displaySimple(this.t, true);
        jceDisplayer.displaySimple(this.u, true);
        jceDisplayer.displaySimple(this.v, true);
        jceDisplayer.displaySimple(this.w, false);
    }
}
