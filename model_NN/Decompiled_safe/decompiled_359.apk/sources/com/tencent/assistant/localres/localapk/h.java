package com.tencent.assistant.localres.localapk;

import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.module.callback.CallbackHelper;

/* compiled from: ProGuard */
class h implements CallbackHelper.Caller<j> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f1444a;
    final /* synthetic */ String b;
    final /* synthetic */ LocalApkInfo c;
    final /* synthetic */ a d;

    h(a aVar, int i, String str, LocalApkInfo localApkInfo) {
        this.d = aVar;
        this.f1444a = i;
        this.b = str;
        this.c = localApkInfo;
    }

    /* renamed from: a */
    public void call(j jVar) {
        jVar.a(this.f1444a, this.b, this.c);
    }
}
