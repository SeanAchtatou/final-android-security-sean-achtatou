package com.tencent.assistant.localres;

import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.utils.e;
import java.lang.ref.WeakReference;
import java.util.Iterator;

/* compiled from: ProGuard */
class f implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f1430a;
    final /* synthetic */ boolean b;
    final /* synthetic */ ApkResourceManager c;

    f(ApkResourceManager apkResourceManager, String str, boolean z) {
        this.c = apkResourceManager;
        this.f1430a = str;
        this.b = z;
    }

    public void run() {
        LocalApkInfo b2 = e.b(this.f1430a);
        long currentTimeMillis = System.currentTimeMillis();
        b2.mLastLaunchTime = currentTimeMillis;
        b2.mFakeLastLaunchTime = currentTimeMillis;
        this.c.h.put(b2.mPackageName, b2);
        Iterator it = this.c.e.iterator();
        while (it.hasNext()) {
            ApkResCallback apkResCallback = (ApkResCallback) ((WeakReference) it.next()).get();
            if (apkResCallback != null) {
                if (apkResCallback.onInstallApkChangedNeedReplacedEvent()) {
                    apkResCallback.onInstalledApkDataChanged(b2, 1, this.b);
                } else {
                    apkResCallback.onInstalledApkDataChanged(b2, 1);
                }
            }
        }
        this.c.c.a(b2, 1);
        if (this.c.g != null) {
            this.c.g.a(b2.getLocalApkInfoKey(), 1);
        }
    }
}
