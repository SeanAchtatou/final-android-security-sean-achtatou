package com.tencent.assistant.localres;

import android.os.Message;
import android.text.TextUtils;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.manager.DownloadProxy;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
class a implements UIEventListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ApkResourceManager f1412a;

    a(ApkResourceManager apkResourceManager) {
        this.f1412a = apkResourceManager;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.localres.ApkResourceManager.a(com.tencent.assistant.localres.ApkResourceManager, boolean):void
     arg types: [com.tencent.assistant.localres.ApkResourceManager, int]
     candidates:
      com.tencent.assistant.localres.ApkResourceManager.a(com.tencent.assistant.localres.ApkResourceManager, com.tencent.assistant.db.table.n):com.tencent.assistant.db.table.n
      com.tencent.assistant.localres.ApkResourceManager.a(com.tencent.assistant.localres.ApkResourceManager, com.tencent.assistant.localres.model.LocalApkInfo):void
      com.tencent.assistant.localres.ApkResourceManager.a(com.tencent.assistant.download.DownloadInfo, com.tencent.assistant.localres.model.LocalApkInfo):boolean
      com.tencent.assistant.localres.ApkResourceManager.a(com.tencent.assistant.localres.ApkResourceManager, boolean):void */
    public void handleUIEvent(Message message) {
        DownloadInfo d;
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_SUCC:
                String str = Constants.STR_EMPTY;
                if (message.obj instanceof String) {
                    str = (String) message.obj;
                }
                if (!TextUtils.isEmpty(str) && (d = DownloadProxy.a().d(str)) != null) {
                    this.f1412a.g.a(d.getFilePath());
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_APP_GOFRONT:
                this.f1412a.c(true);
                return;
            default:
                return;
        }
    }
}
