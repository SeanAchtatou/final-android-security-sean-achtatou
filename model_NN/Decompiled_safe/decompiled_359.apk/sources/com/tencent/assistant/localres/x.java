package com.tencent.assistant.localres;

import com.tencent.assistant.module.callback.ActionCallback;
import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.assistant.utils.ba;

/* compiled from: ProGuard */
public abstract class x<T extends ActionCallback> {

    /* renamed from: a  reason: collision with root package name */
    private CallbackHelper<T> f1450a = new CallbackHelper<>();

    public void a(ActionCallback actionCallback) {
        this.f1450a.register(actionCallback);
    }

    public void b(ActionCallback actionCallback) {
        this.f1450a.unregister(actionCallback);
    }

    /* access modifiers changed from: protected */
    public void a(CallbackHelper.Caller caller) {
        ba.a().post(new y(this, caller));
    }

    /* access modifiers changed from: protected */
    public void b(CallbackHelper.Caller caller) {
        this.f1450a.broadcast(caller);
    }
}
