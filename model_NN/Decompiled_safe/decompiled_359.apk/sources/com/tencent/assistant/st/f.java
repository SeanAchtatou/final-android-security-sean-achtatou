package com.tencent.assistant.st;

import com.qq.AppService.AstApp;
import com.tencent.b.b.c;
import com.tencent.connect.common.Constants;
import java.util.UUID;

/* compiled from: ProGuard */
public class f {

    /* renamed from: a  reason: collision with root package name */
    private static String f2536a = Constants.STR_EMPTY;
    private static String b = Constants.STR_EMPTY;
    private static String c = Constants.STR_EMPTY;
    private static long d = 0;
    private static String e = Constants.STR_EMPTY;
    private static int f = -100;
    private static byte g = 6;

    public static final void a(String str) {
        f2536a = str;
    }

    public static final String a() {
        return b;
    }

    public static final void b(String str) {
        b = str;
    }

    public static final long b() {
        return d;
    }

    public static final void a(long j) {
        d = j;
    }

    public static final void c(String str) {
        e = str;
    }

    public static final byte c() {
        return g;
    }

    public static final void a(byte b2) {
        g = b2;
    }

    public static final void a(int i) {
        f = i;
    }

    public static final int d() {
        return f;
    }

    public static final String e() {
        return c.b(AstApp.i().getApplicationContext());
    }

    public static String f() {
        return UUID.randomUUID().toString() + "_" + System.currentTimeMillis();
    }
}
