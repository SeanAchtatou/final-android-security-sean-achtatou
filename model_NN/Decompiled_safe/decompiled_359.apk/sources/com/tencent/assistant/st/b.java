package com.tencent.assistant.st;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.module.BaseEngine;
import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.assistant.module.callback.aa;
import com.tencent.assistant.protocol.jce.StatReportItem;
import com.tencent.assistant.protocol.jce.StatReportRequest;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class b extends BaseEngine<aa> {
    public int a(ArrayList<StatReportItem> arrayList) {
        StatReportRequest statReportRequest = new StatReportRequest();
        statReportRequest.f2375a = arrayList;
        return send(statReportRequest);
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        notifyDataChanged(new c(this, i));
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        notifyDataChanged(new d(this, i, i2));
    }

    /* access modifiers changed from: protected */
    public void notifyDataChangedInMainThread(CallbackHelper.Caller<aa> caller) {
        runOnUiThread(new e(this, caller));
    }
}
