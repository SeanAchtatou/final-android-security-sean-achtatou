package com.tencent.assistant.module.wisedownload.condition;

import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.m;
import com.tencent.assistant.module.wisedownload.b;
import com.tencent.assistant.protocol.jce.AutoDownloadCfg;
import com.tencent.assistant.protocol.jce.AutoDownloadPolicy;
import com.tencent.assistant.utils.cv;
import java.util.Calendar;
import java.util.Map;

/* compiled from: ProGuard */
public class n extends ThresholdCondition {
    private boolean c;
    private int d;
    private int e;
    private int f;
    private int g;

    public n(b bVar) {
        a(bVar);
    }

    public void a(b bVar) {
        AutoDownloadCfg j;
        Map<Integer, AutoDownloadPolicy> map;
        if (bVar != null && (j = bVar.j()) != null && (map = j.g) != null) {
            this.c = true;
            AutoDownloadPolicy autoDownloadPolicy = map.get(1);
            if (autoDownloadPolicy != null) {
                this.d = a(autoDownloadPolicy.f2013a);
                this.e = a(autoDownloadPolicy.b);
            }
            AutoDownloadPolicy autoDownloadPolicy2 = map.get(2);
            if (autoDownloadPolicy2 != null) {
                this.f = a(autoDownloadPolicy2.f2013a);
                this.g = a(autoDownloadPolicy2.b);
            }
        }
    }

    public boolean a() {
        return b() || h();
    }

    public boolean a(int i, int i2) {
        if (!this.c || i2 <= i) {
            return false;
        }
        long a2 = a(i);
        long a3 = a(i2);
        long currentTimeMillis = System.currentTimeMillis();
        if (currentTimeMillis < a2 || currentTimeMillis > a3) {
            return false;
        }
        return true;
    }

    public boolean b() {
        if (!a(this.d, this.e)) {
            return false;
        }
        if (cv.b(m.a().E()) || AstApp.p() != null) {
            return true;
        }
        return false;
    }

    public boolean h() {
        if (a(this.f, this.g) && AstApp.p() == null) {
            return true;
        }
        return false;
    }

    private static int a(String str) {
        if (TextUtils.isEmpty(str) || str.length() < 2) {
            return 0;
        }
        if ('0' == str.charAt(0)) {
            return Integer.valueOf(str.substring(1, 2)).intValue();
        }
        return Integer.valueOf(str.substring(0, 2)).intValue();
    }

    private static long a(int i) {
        Calendar instance = Calendar.getInstance();
        instance.set(11, i);
        instance.set(12, 0);
        instance.set(13, 0);
        instance.set(14, 0);
        return instance.getTimeInMillis();
    }
}
