package com.tencent.assistant.module;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.module.callback.d;
import com.tencent.assistant.protocol.jce.ReportAppRequest;
import com.tencent.assistant.protocol.jce.ReportAppResponse;

/* compiled from: ProGuard */
public class x extends BaseEngine<d> {
    public int a(long j, long j2, byte[] bArr, String str) {
        ReportAppRequest reportAppRequest = new ReportAppRequest();
        reportAppRequest.f2279a = j;
        reportAppRequest.b = j2;
        reportAppRequest.c = bArr;
        reportAppRequest.d = str;
        return send(reportAppRequest);
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        ReportAppRequest reportAppRequest = (ReportAppRequest) jceStruct;
        int i2 = i;
        notifyDataChangedInMainThread(new y(this, i2, (ReportAppResponse) jceStruct2, reportAppRequest.f2279a, reportAppRequest.b));
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        notifyDataChangedInMainThread(new z(this, i, i2));
    }
}
