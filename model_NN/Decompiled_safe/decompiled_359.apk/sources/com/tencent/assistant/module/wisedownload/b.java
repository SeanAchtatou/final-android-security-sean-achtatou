package com.tencent.assistant.module.wisedownload;

import android.os.Bundle;
import com.tencent.assistant.manager.as;
import com.tencent.assistant.module.update.u;
import com.tencent.assistant.module.wisedownload.condition.ThresholdCondition;
import com.tencent.assistant.module.wisedownload.condition.g;
import com.tencent.assistant.module.wisedownload.condition.j;
import com.tencent.assistant.protocol.jce.AutoDownloadCfg;
import java.util.List;

/* compiled from: ProGuard */
public abstract class b {

    /* renamed from: a  reason: collision with root package name */
    protected ThresholdCondition f1905a;
    protected ThresholdCondition b;
    protected ThresholdCondition c;
    protected ThresholdCondition d;
    protected boolean e;
    protected AutoDownloadCfg f;
    private List<String> g;

    public abstract void a(o oVar);

    public abstract boolean b();

    public b() {
        a();
    }

    private void a() {
        this.e = b();
        d();
        this.g = u.a().e();
        this.f1905a = g.a(this, ThresholdCondition.CONDITION_TYPE.CONDITION_SWITCH);
        this.c = g.a(this, ThresholdCondition.CONDITION_TYPE.CONDITION_TIME);
        c();
    }

    /* access modifiers changed from: protected */
    public void c() {
        this.b = g.a(this, ThresholdCondition.CONDITION_TYPE.CONDITION_PRIMARY);
    }

    public void d() {
        this.f = as.w().i();
    }

    public boolean e() {
        if (this.f1905a != null) {
            return this.f1905a.a();
        }
        return false;
    }

    public ThresholdCondition.CONDITION_RESULT_CODE f() {
        if (!e()) {
            return ThresholdCondition.CONDITION_RESULT_CODE.FAIL_SWITCH;
        }
        if (this.b != null && !this.b.a()) {
            return this.b.c() != ThresholdCondition.CONDITION_RESULT_CODE.OK ? this.b.c() : ThresholdCondition.CONDITION_RESULT_CODE.FAIL_PRIMARY_UNKNOW;
        }
        if (this.d == null || this.d.a()) {
            return ThresholdCondition.CONDITION_RESULT_CODE.OK;
        }
        return this.d.c() != ThresholdCondition.CONDITION_RESULT_CODE.OK ? this.d.c() : ThresholdCondition.CONDITION_RESULT_CODE.FAIL_OTHER_UNKNOW;
    }

    public boolean a(ThresholdCondition.CONDITION_TRIGGER_ACTION condition_trigger_action, Bundle bundle) {
        switch (c.f1906a[condition_trigger_action.ordinal()]) {
            case 1:
            case 2:
                if (this.b == null) {
                    return true;
                }
                if (!((j) this.b).b()) {
                    return true;
                }
                break;
            case 3:
            case 4:
                if (!ThresholdCondition.d()) {
                    return true;
                }
                break;
            case 5:
                return true;
            case 6:
                if (this.b != null) {
                    this.b.a(bundle);
                    if (!((j) this.b).j()) {
                        return true;
                    }
                }
                break;
        }
        return false;
    }

    public void g() {
        d();
        this.g = u.a().e();
        if (this.c != null) {
            this.c.a(this);
        }
        if (this.b != null) {
            this.b.a(this);
        }
        if (this.d != null) {
            this.d.a(this);
        }
    }

    public void a(ThresholdCondition.CONDITION_TYPE condition_type) {
        switch (c.b[condition_type.ordinal()]) {
            case 1:
                if (this.f1905a != null) {
                    this.e = b();
                    this.f1905a.a(this);
                    return;
                }
                return;
            case 2:
                if (this.c != null) {
                    d();
                    this.c.a(this);
                    return;
                }
                return;
            case 3:
                if (this.b != null) {
                    d();
                    this.g = u.a().e();
                    this.b.a(this);
                    return;
                }
                return;
            case 4:
                if (this.d != null) {
                    d();
                    this.d.a(this);
                    return;
                }
                return;
            default:
                return;
        }
    }

    public boolean h() {
        return this.e;
    }

    public List<String> i() {
        return this.g;
    }

    public AutoDownloadCfg j() {
        return this.f;
    }

    public void b(o oVar) {
        boolean z;
        int i;
        boolean z2;
        boolean z3;
        boolean z4;
        boolean z5 = false;
        if (oVar != null) {
            if (!oVar.a()) {
                long j = 0;
                if (this.b == null || !(this.b instanceof j)) {
                    z = false;
                    i = 0;
                    z2 = false;
                    z3 = false;
                    z4 = false;
                } else {
                    j jVar = (j) this.b;
                    z4 = ThresholdCondition.e();
                    z3 = ThresholdCondition.d();
                    z2 = jVar.k();
                    i = jVar.l();
                    z5 = jVar.j();
                    z = jVar.i();
                    j = ThresholdCondition.f();
                }
                oVar.a(z4, z3, z2, i, z5, z, j);
            }
            a(oVar);
        }
    }
}
