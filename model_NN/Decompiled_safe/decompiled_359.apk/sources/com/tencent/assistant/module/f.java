package com.tencent.assistant.module;

import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.assistant.module.callback.k;
import com.tencent.assistant.protocol.jce.PraiseCommentRequest;
import com.tencent.assistant.protocol.jce.PraiseCommentResponse;

/* compiled from: ProGuard */
class f implements CallbackHelper.Caller<k> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f1812a;
    final /* synthetic */ PraiseCommentResponse b;
    final /* synthetic */ PraiseCommentRequest c;
    final /* synthetic */ e d;

    f(e eVar, int i, PraiseCommentResponse praiseCommentResponse, PraiseCommentRequest praiseCommentRequest) {
        this.d = eVar;
        this.f1812a = i;
        this.b = praiseCommentResponse;
        this.c = praiseCommentRequest;
    }

    /* renamed from: a */
    public void call(k kVar) {
        kVar.a(this.f1812a, this.b.f2263a, this.c.f2262a, this.b.b, this.b.c);
    }
}
