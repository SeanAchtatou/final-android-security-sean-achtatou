package com.tencent.assistant.module;

import com.tencent.assistant.protocol.jce.GetCommentListResponse;

/* compiled from: ProGuard */
class bj implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GetCommentListResponse f1722a;
    final /* synthetic */ int b;
    final /* synthetic */ bi c;

    bj(bi biVar, GetCommentListResponse getCommentListResponse, int i) {
        this.c = biVar;
        this.f1722a = getCommentListResponse;
        this.b = i;
    }

    public void run() {
        boolean z = true;
        bi biVar = this.c;
        if (this.f1722a.c != 1) {
            z = false;
        }
        boolean unused = biVar.e = z;
        byte[] unused2 = this.c.f = this.f1722a.d;
        this.c.notifyDataChanged(new bk(this));
        if (this.c.e) {
            this.c.g.a(this.c.f);
        }
    }
}
