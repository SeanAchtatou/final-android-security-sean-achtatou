package com.tencent.assistant.module.a;

import android.os.Handler;
import com.tencent.assistant.utils.TemporaryThreadManager;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.PriorityBlockingQueue;

/* compiled from: ProGuard */
public class d implements l {

    /* renamed from: a  reason: collision with root package name */
    private static d f1682a;
    /* access modifiers changed from: private */
    public PriorityBlockingQueue<g> b = new PriorityBlockingQueue<>(10, new h());
    private List<g> c = new ArrayList();
    private Handler d = new e(this);

    public static synchronized d a() {
        d dVar;
        synchronized (d.class) {
            if (f1682a == null) {
                f1682a = new d();
            }
            dVar = f1682a;
        }
        return dVar;
    }

    private d() {
    }

    public synchronized boolean a(g gVar) {
        boolean z;
        if (gVar != null) {
            if (!this.b.contains(gVar)) {
                gVar.a(this);
                if (gVar.a().size() > 0) {
                    z = this.c.add(gVar);
                } else {
                    z = this.b.add(gVar);
                }
            }
        }
        z = false;
        return z;
    }

    public void b() {
        new f(this, "StartThread").start();
    }

    public void b(g gVar) {
    }

    public synchronized void c(g gVar) {
        ArrayList arrayList = new ArrayList();
        for (g next : this.c) {
            if (next.a().contains(gVar)) {
                next.a().remove(gVar);
                if (next.a().size() == 0) {
                    TemporaryThreadManager.get().start(next);
                    arrayList.add(next);
                }
            }
        }
        this.c.removeAll(arrayList);
        if (this.c.size() == 0) {
        }
    }

    public void a(g gVar, Exception exc) {
        exc.printStackTrace();
    }
}
