package com.tencent.assistant.module;

import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.assistant.module.callback.q;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/* compiled from: ProGuard */
class dg implements CallbackHelper.Caller<q> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f1773a;
    final /* synthetic */ boolean b;
    final /* synthetic */ LinkedHashMap c;
    final /* synthetic */ ArrayList d;
    final /* synthetic */ boolean e;
    final /* synthetic */ de f;

    dg(de deVar, int i, boolean z, LinkedHashMap linkedHashMap, ArrayList arrayList, boolean z2) {
        this.f = deVar;
        this.f1773a = i;
        this.b = z;
        this.c = linkedHashMap;
        this.d = arrayList;
        this.e = z2;
    }

    /* renamed from: a */
    public void call(q qVar) {
        qVar.a(this.f1773a, 0, this.b, this.c, this.d, this.e);
    }
}
