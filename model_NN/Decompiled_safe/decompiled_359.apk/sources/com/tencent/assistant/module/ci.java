package com.tencent.assistant.module;

import com.tencent.assistant.manager.as;
import com.tencent.assistant.protocol.jce.GftGetTreasureBoxSettingResponse;
import com.tencent.assistant.protocol.jce.NpcListCfg;
import com.tencent.assistant.protocol.jce.TreasureBoxCfg;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.bh;

/* compiled from: ProGuard */
class ci implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GftGetTreasureBoxSettingResponse f1750a;
    final /* synthetic */ ce b;

    ci(ce ceVar, GftGetTreasureBoxSettingResponse gftGetTreasureBoxSettingResponse) {
        this.b = ceVar;
        this.f1750a = gftGetTreasureBoxSettingResponse;
    }

    public void run() {
        try {
            for (TreasureBoxCfg next : this.f1750a.a()) {
                XLog.v("cfg", "cfg.type : " + next.f2407a);
                switch (next.f2407a) {
                    case 1:
                        NpcListCfg npcListCfg = (NpcListCfg) bh.b(next.b, NpcListCfg.class);
                        if (npcListCfg == null) {
                            break;
                        } else {
                            as.w().b(npcListCfg);
                            break;
                        }
                }
            }
        } catch (Exception e) {
            XLog.e("GAME_FT", getClass().getSimpleName() + "getTreasureBoxSettingResponse fail.");
        }
    }
}
