package com.tencent.assistant.module;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.as;
import com.tencent.assistant.model.QuickEntranceNotify;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.callback.b;
import com.tencent.assistant.protocol.jce.GetAppListRequest;
import com.tencent.assistant.protocol.jce.GetAppListResponse;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class k extends BaseEngine<b> implements eb {

    /* renamed from: a  reason: collision with root package name */
    public long f1840a;
    public int b;
    private long c;
    private short d;
    /* access modifiers changed from: private */
    public boolean e;
    /* access modifiers changed from: private */
    public long f;
    private List<SimpleAppModel> g;
    private byte[] h;
    private boolean i;
    private int j;
    private int k;
    private List<SimpleAppModel> l;
    private byte[] m;
    private boolean n;
    private r o;
    private com.tencent.assistant.model.b p;
    /* access modifiers changed from: private */
    public int q;

    public k(long j2, int i2) {
        boolean z = false;
        this.e = false;
        this.f = -1;
        this.i = true;
        this.j = -1;
        this.k = -1;
        this.l = new ArrayList();
        this.n = true;
        this.p = new com.tencent.assistant.model.b();
        this.q = 0;
        this.f1840a = j2;
        this.b = i2;
        if (this.f1840a == -2 && (this.b == 1 || this.b == 2)) {
            z = true;
        }
        if (z) {
            this.e = true;
            dy.a().a(this);
        }
        this.o = new r(this);
    }

    public k(long j2, int i2, short s) {
        this(j2, i2);
        this.d = s;
    }

    private boolean i() {
        return this.f1840a == 0 && this.b == 99;
    }

    public com.tencent.assistant.model.b a() {
        this.p.b(this.f);
        this.p.b(g());
        return this.p;
    }

    public GetAppListResponse b() {
        return as.w().a(this.f1840a, this.b, (byte[]) null);
    }

    public void c() {
        TemporaryThreadManager.get().start(new l(this));
    }

    public void a(boolean z) {
        TemporaryThreadManager.get().start(new m(this, z));
    }

    public void a(long j2) {
        this.c = j2;
        c();
    }

    /* access modifiers changed from: private */
    public boolean j() {
        GetAppListResponse b2;
        boolean z = false;
        if (!this.e || (b2 = b()) == null || i()) {
            return false;
        }
        long a2 = m.a().a((byte) 5);
        if (a2 != -11 && b2.e != a2) {
            if (b2.e != a2) {
            }
            return false;
        } else if (b2.b == null || b2.b.size() <= 0) {
            return false;
        } else {
            this.f = b2.e;
            this.p.b(this.f);
            if (b2.d == 1) {
                z = true;
            }
            this.i = z;
            this.h = b2.c;
            this.g = u.b(b2.b);
            this.l.clear();
            this.l.addAll(this.g);
            this.q = this.l.get(this.l.size() - 1).al;
            this.n = this.i;
            this.m = this.h;
            ArrayList arrayList = new ArrayList(this.g);
            if (this.g != null && this.g.size() > 0) {
                notifyDataChangedInMainThread(new n(this, arrayList));
                if (this.n) {
                    this.o.a(this.m);
                }
            }
            return true;
        }
    }

    public void d() {
        if (this.e) {
            if (this.f != m.a().a((byte) 5) && !i()) {
                e();
            }
        }
    }

    public void a(ArrayList<QuickEntranceNotify> arrayList) {
    }

    public int e() {
        if (this.j > 0) {
            cancel(this.j);
        }
        this.j = b((byte[]) null);
        return this.j;
    }

    /* access modifiers changed from: private */
    public int a(byte[] bArr) {
        if (this.k > 0) {
            cancel(this.k);
        }
        this.k = b(bArr);
        return this.k;
    }

    public int f() {
        if (this.m == null || this.m.length == 0) {
            return -1;
        }
        if (this.o.c()) {
            int a2 = this.o.a();
            this.o.b();
            return a2;
        } else if (this.m != this.o.d() || this.o.e() == null) {
            return a(this.m);
        } else {
            r j2 = this.o.clone();
            boolean z = this.f != j2.h();
            this.f = j2.h();
            if (this.f != m.a().a((byte) 5) && !i()) {
                return a(this.m);
            }
            int a3 = j2.a();
            this.p.b(this.f);
            if (z) {
                this.g = j2.e();
                this.h = j2.g();
                this.i = j2.f();
                this.l.clear();
            }
            ArrayList arrayList = new ArrayList(j2.e());
            this.l.addAll(arrayList);
            if (this.l.size() > 0) {
                this.q = this.l.get(this.l.size() - 1).al;
            }
            this.n = j2.f();
            this.m = j2.g();
            XLog.d("voken", "getNextPage mNextPageContext = " + this.m);
            notifyDataChangedInMainThread(new o(this, a3, z, arrayList));
            if (this.o.e) {
                this.o.a(this.o.g());
            }
            return this.o.a();
        }
    }

    private int b(byte[] bArr) {
        return a(-1, bArr);
    }

    /* access modifiers changed from: private */
    public int a(int i2, byte[] bArr) {
        GetAppListRequest getAppListRequest = new GetAppListRequest();
        if (0 != this.c) {
            getAppListRequest.f2083a = this.c;
            getAppListRequest.b = 4;
        } else {
            getAppListRequest.f2083a = this.f1840a;
            getAppListRequest.b = this.b;
        }
        getAppListRequest.c = this.d > 0 ? this.d : 30;
        getAppListRequest.e = this.q;
        XLog.d("voken", "sendRequest mNextPageContext = " + this.m);
        if (bArr == null) {
            bArr = new byte[0];
        }
        getAppListRequest.d = bArr;
        return send(i2, getAppListRequest);
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        boolean z = true;
        if (jceStruct2 != null) {
            GetAppListResponse getAppListResponse = (GetAppListResponse) jceStruct2;
            GetAppListRequest getAppListRequest = (GetAppListRequest) jceStruct;
            ArrayList<SimpleAppModel> b2 = u.b(getAppListResponse.b);
            if (i2 == this.o.a()) {
                r rVar = this.o;
                long j2 = getAppListResponse.e;
                if (getAppListResponse.d != 1) {
                    z = false;
                }
                rVar.a(j2, b2, z, getAppListResponse.c);
            } else if (this.m != getAppListResponse.c) {
                boolean z2 = getAppListResponse.e != this.f || getAppListRequest.d == null || getAppListRequest.d.length == 0;
                if (getAppListResponse.d != 1) {
                    z = false;
                }
                byte[] bArr = getAppListResponse.c;
                this.f = getAppListResponse.e;
                this.p.b(this.f);
                if (z2) {
                    this.g = b2;
                    this.h = bArr;
                    this.i = z;
                    this.l.clear();
                }
                this.l.addAll(b2);
                if (this.l.size() > 0) {
                    this.q = this.l.get(this.l.size() - 1).al;
                    XLog.d("voken", "onRequestSuccessed pageContext = " + bArr);
                }
                this.n = z;
                this.m = bArr;
                notifyDataChangedInMainThread(new p(this, i2, z2, b2, getAppListResponse));
                if (this.n) {
                    this.o.a(this.m);
                }
            }
            if (this.e) {
                as.w().a(this.f1840a, this.b, getAppListRequest.d, getAppListResponse);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i2, int i3, JceStruct jceStruct, JceStruct jceStruct2) {
        if (i2 != this.o.a()) {
            GetAppListRequest getAppListRequest = (GetAppListRequest) jceStruct;
            notifyDataChangedInMainThread(new q(this, i2, i3, getAppListRequest.d == null || getAppListRequest.d.length == 0));
            return;
        }
        this.o.i();
    }

    public List<SimpleAppModel> g() {
        return this.l;
    }

    public boolean h() {
        return this.n;
    }

    public String toString() {
        return "categoryId:" + this.f1840a + " sortId:" + this.b;
    }
}
