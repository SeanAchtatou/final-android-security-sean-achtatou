package com.tencent.assistant.module.update;

import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.Global;
import com.tencent.assistant.download.SimpleDownloadInfo;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistant.localres.l;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.m;
import com.tencent.assistant.module.aw;
import com.tencent.assistant.module.fh;
import com.tencent.assistant.module.wisedownload.i;
import com.tencent.assistant.protocol.jce.AppInfoForIgnore;
import com.tencent.assistant.protocol.jce.AppInfoForUpdate;
import com.tencent.assistant.protocol.jce.AutoDownloadInfo;
import com.tencent.assistant.protocol.jce.CheckSelfUpdateRequest;
import com.tencent.assistant.protocol.jce.DownloadSubscriptionInfo;
import com.tencent.assistant.protocol.jce.GetAutoDownloadResponse;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.cv;
import com.tencent.assistant.utils.t;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/* compiled from: ProGuard */
public class u extends aw {

    /* renamed from: a  reason: collision with root package name */
    private static u f1898a = null;
    /* access modifiers changed from: private */
    public int b = -1;
    /* access modifiers changed from: private */
    public t c = new t();
    /* access modifiers changed from: private */
    public LocalApkInfo d;
    private ApkResCallback.Stub e = new v(this);

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        GetAutoDownloadResponse getAutoDownloadResponse;
        if (this.b == i && jceStruct2 != null && (jceStruct2 instanceof GetAutoDownloadResponse) && (getAutoDownloadResponse = (GetAutoDownloadResponse) jceStruct2) != null && getAutoDownloadResponse.f2098a == 0) {
            if (this.c == null) {
                this.c = new t();
            }
            this.c.a(getAutoDownloadResponse);
            i.a().g();
            i.a().f();
        }
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
    }

    /* access modifiers changed from: private */
    public void a(String str, int i) {
        if (!TextUtils.isEmpty(str) && this.c != null) {
            AutoDownloadInfo a2 = a(str, i, this.c.h());
            if (a2 != null) {
                this.c.c(a2);
            }
            AutoDownloadInfo a3 = a(str, i, this.c.d());
            if (a3 != null) {
                this.c.a(a3);
            }
            AutoDownloadInfo a4 = a(str, i, this.c.g());
            if (a4 != null) {
                this.c.b(a4);
            }
            AutoDownloadInfo a5 = a(str, i, this.c.j());
            if (a5 != null) {
                this.c.d(a5);
            }
        }
    }

    private AutoDownloadInfo a(String str, int i, List<AutoDownloadInfo> list) {
        if (list == null || list.isEmpty()) {
            return null;
        }
        for (AutoDownloadInfo next : list) {
            if (str.endsWith(next.f2010a) && i == next.d) {
                return next;
            }
        }
        return null;
    }

    private u() {
    }

    public static synchronized u a() {
        u uVar;
        synchronized (u.class) {
            if (f1898a == null) {
                f1898a = new u();
            }
            uVar = f1898a;
        }
        return uVar;
    }

    public void b() {
        ApkResourceManager.getInstance().registerApkResCallback(this.e);
        fh.a().b();
    }

    /* access modifiers changed from: private */
    public void l() {
        TemporaryThreadManager.get().start(new x(this));
    }

    /* access modifiers changed from: private */
    public ArrayList<AppInfoForUpdate> a(boolean z) {
        boolean z2 = true;
        List<LocalApkInfo> localApkInfos = ApkResourceManager.getInstance().getLocalApkInfos();
        l.d(localApkInfos);
        if (z) {
            localApkInfos = b(localApkInfos);
        }
        if (localApkInfos == null || localApkInfos.isEmpty()) {
            z2 = false;
        }
        if (z2) {
            return c(localApkInfos);
        }
        return null;
    }

    /* access modifiers changed from: private */
    public ArrayList<AppInfoForIgnore> m() {
        ArrayList<AppInfoForIgnore> arrayList = new ArrayList<>();
        Set<s> e2 = k.b().e();
        if (e2 == null || e2.isEmpty()) {
            return null;
        }
        for (s next : e2) {
            AppInfoForIgnore appInfoForIgnore = new AppInfoForIgnore();
            appInfoForIgnore.a(next.f1896a);
            appInfoForIgnore.a(next.c);
            appInfoForIgnore.b(next.b);
            arrayList.add(appInfoForIgnore);
        }
        return arrayList;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean
      com.tencent.assistant.m.a(java.lang.String, long):long */
    private List<LocalApkInfo> b(List<LocalApkInfo> list) {
        long a2 = m.a().a("app_update_refresh_suc_time", 0L);
        ArrayList arrayList = new ArrayList();
        int size = list.size();
        long d2 = cv.d();
        for (int i = 0; i < size; i++) {
            LocalApkInfo localApkInfo = list.get(i);
            if (cv.a(localApkInfo.mInstallDate, d2) == 0 && localApkInfo.mInstallDate >= a2) {
                arrayList.add(localApkInfo);
            }
        }
        return arrayList;
    }

    private ArrayList<AppInfoForUpdate> c(List<LocalApkInfo> list) {
        ArrayList<AppInfoForUpdate> arrayList = new ArrayList<>();
        for (LocalApkInfo next : list) {
            AppInfoForUpdate appInfoForUpdate = new AppInfoForUpdate();
            if (next.mAppid > 0) {
                appInfoForUpdate.e = next.mAppid;
            } else {
                appInfoForUpdate.f2001a = next.mPackageName;
            }
            appInfoForUpdate.c = next.mVersionCode;
            appInfoForUpdate.g = next.mVersionName;
            appInfoForUpdate.b = next.signature;
            appInfoForUpdate.d = next.manifestMd5;
            appInfoForUpdate.f = next.getAppType();
            appInfoForUpdate.j = next.mAppName;
            appInfoForUpdate.i = (long) next.launchCount;
            appInfoForUpdate.h = next.mLastLaunchTime;
            arrayList.add(appInfoForUpdate);
        }
        return arrayList;
    }

    /* access modifiers changed from: private */
    public CheckSelfUpdateRequest n() {
        int i = 1;
        ApkResourceManager.getInstance().getSelfInfo(new y(this));
        if (this.d == null) {
            return null;
        }
        CheckSelfUpdateRequest checkSelfUpdateRequest = new CheckSelfUpdateRequest();
        checkSelfUpdateRequest.b = this.d.manifestMd5;
        checkSelfUpdateRequest.f2031a = t.o();
        checkSelfUpdateRequest.d = 0;
        if (Global.isLite()) {
            checkSelfUpdateRequest.c = 1;
        } else {
            checkSelfUpdateRequest.c = 0;
        }
        checkSelfUpdateRequest.e = this.d.mInstallDate;
        if (!AstApp.i().l()) {
            i = 0;
        }
        checkSelfUpdateRequest.h = (byte) i;
        return checkSelfUpdateRequest;
    }

    public List<AutoDownloadInfo> c() {
        List<AutoDownloadInfo> c2 = this.c != null ? this.c.c() : null;
        if (c2 == null || c2.isEmpty()) {
            return c2;
        }
        ArrayList arrayList = new ArrayList();
        s sVar = new s();
        Set<s> e2 = k.b().e();
        for (AutoDownloadInfo next : c2) {
            sVar.a(next.f2010a, next.i, next.d, false);
            if (!e2.contains(sVar)) {
                try {
                    arrayList.add(next);
                } catch (NullPointerException e3) {
                    e3.printStackTrace();
                }
            }
        }
        return arrayList;
    }

    public List<AutoDownloadInfo> d() {
        List<AutoDownloadInfo> c2 = c();
        if (c2 == null || c2.isEmpty()) {
            return c2;
        }
        if (c2 != null && !c2.isEmpty()) {
            Iterator<AutoDownloadInfo> it = c2.iterator();
            while (it.hasNext()) {
                if (ApkResourceManager.getInstance().getLocalApkInfo(it.next().f2010a) == null) {
                    it.remove();
                }
            }
        }
        return c2;
    }

    /* access modifiers changed from: private */
    public ArrayList<AppInfoForIgnore> a(ArrayList<AutoDownloadInfo> arrayList) {
        if (arrayList == null || arrayList.isEmpty()) {
            return null;
        }
        ArrayList<AppInfoForIgnore> arrayList2 = new ArrayList<>();
        Iterator<AutoDownloadInfo> it = arrayList.iterator();
        while (it.hasNext()) {
            AutoDownloadInfo next = it.next();
            AppInfoForIgnore appInfoForIgnore = new AppInfoForIgnore();
            appInfoForIgnore.f2000a = next.f2010a;
            appInfoForIgnore.b = next.c;
            appInfoForIgnore.c = next.d;
            appInfoForIgnore.d = next.j;
            arrayList2.add(appInfoForIgnore);
        }
        return arrayList2;
    }

    public List<String> e() {
        if (this.c != null) {
            return this.c.e();
        }
        return null;
    }

    public List<AutoDownloadInfo> f() {
        if (this.c != null) {
            return this.c.f();
        }
        return null;
    }

    public List<AutoDownloadInfo> g() {
        if (this.c != null) {
            return this.c.h();
        }
        return null;
    }

    public List<AutoDownloadInfo> h() {
        if (this.c != null) {
            return this.c.i();
        }
        return null;
    }

    public List<AutoDownloadInfo> i() {
        if (this.c != null) {
            return this.c.j();
        }
        return null;
    }

    public DownloadSubscriptionInfo a(SimpleDownloadInfo.UIType uIType, String str, int i) {
        List<AutoDownloadInfo> j;
        if (!TextUtils.isEmpty(str) && uIType == SimpleDownloadInfo.UIType.WISE_SUBSCRIPTION_DOWNLOAD && (j = this.c.j()) != null) {
            for (AutoDownloadInfo next : j) {
                if (next != null && str.equalsIgnoreCase(next.f2010a) && i == next.d) {
                    return next.z;
                }
            }
        }
        return null;
    }

    public boolean b(SimpleDownloadInfo.UIType uIType, String str, int i) {
        DownloadSubscriptionInfo a2 = a(uIType, str, i);
        if (a2 == null || a2.e == 0) {
            return false;
        }
        return true;
    }

    public List<AutoDownloadInfo> j() {
        if (this.c != null) {
            return this.c.k();
        }
        return null;
    }

    public boolean a(AutoDownloadInfo autoDownloadInfo) {
        List<AutoDownloadInfo> f;
        if (this.c == null || (f = this.c.f()) == null || f.isEmpty()) {
            return false;
        }
        return f.remove(autoDownloadInfo);
    }

    public void k() {
        if (this.c != null) {
            this.c.a();
        }
    }

    public void a(List<AutoDownloadInfo> list) {
        if (this.c != null) {
            this.c.a(list);
        }
    }
}
