package com.tencent.assistant.module;

import android.text.TextUtils;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.module.callback.aj;
import com.tencent.assistant.protocol.jce.AppSimpleDetail;
import com.tencent.assistant.protocol.jce.SourceCheckRequest;
import com.tencent.assistant.protocol.jce.SourceCheckResponse;

/* compiled from: ProGuard */
public class fs extends BaseEngine<aj> {
    public int a(String str, String str2, byte b) {
        if (TextUtils.isEmpty(str)) {
            return -1;
        }
        SourceCheckRequest sourceCheckRequest = new SourceCheckRequest();
        sourceCheckRequest.a(str);
        sourceCheckRequest.c = str2;
        sourceCheckRequest.b = b;
        return send(sourceCheckRequest);
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        notifyDataChangedInMainThread(new ft(this, jceStruct, i, (SourceCheckResponse) jceStruct2));
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        notifyDataChangedInMainThread(new fu(this, i, i2));
    }

    /* access modifiers changed from: private */
    public AppSimpleDetail a(SourceCheckResponse sourceCheckResponse) {
        if (sourceCheckResponse == null || sourceCheckResponse.d == null || sourceCheckResponse.d.size() <= 0) {
            return null;
        }
        return sourceCheckResponse.d.get(0);
    }
}
