package com.tencent.assistant.module;

import android.os.Message;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.utils.TemporaryThreadManager;

/* compiled from: ProGuard */
class dz implements UIEventListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ dy f1790a;

    dz(dy dyVar) {
        this.f1790a = dyVar;
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_APP_GOFRONT:
                TemporaryThreadManager.get().start(new ea(this));
                return;
            default:
                return;
        }
    }
}
