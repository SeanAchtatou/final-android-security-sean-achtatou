package com.tencent.assistant.module.update;

import com.tencent.assistant.module.update.AppUpdateConst;

/* compiled from: ProGuard */
/* synthetic */ class r {

    /* renamed from: a  reason: collision with root package name */
    static final /* synthetic */ int[] f1895a = new int[AppUpdateConst.RequestLaunchType.values().length];

    static {
        try {
            f1895a[AppUpdateConst.RequestLaunchType.TYPE_STARTUP.ordinal()] = 1;
        } catch (NoSuchFieldError e) {
        }
        try {
            f1895a[AppUpdateConst.RequestLaunchType.TYPE_TIMER.ordinal()] = 2;
        } catch (NoSuchFieldError e2) {
        }
        try {
            f1895a[AppUpdateConst.RequestLaunchType.TYPE_INTIME_UPDATE_PUSH.ordinal()] = 3;
        } catch (NoSuchFieldError e3) {
        }
        try {
            f1895a[AppUpdateConst.RequestLaunchType.TYPE_ASSISTANT_RETRY.ordinal()] = 4;
        } catch (NoSuchFieldError e4) {
        }
        try {
            f1895a[AppUpdateConst.RequestLaunchType.TYPE_APP_UNINSTALL.ordinal()] = 5;
        } catch (NoSuchFieldError e5) {
        }
        try {
            f1895a[AppUpdateConst.RequestLaunchType.TYPE_APP_INSTALLED.ordinal()] = 6;
        } catch (NoSuchFieldError e6) {
        }
        try {
            f1895a[AppUpdateConst.RequestLaunchType.TYPE_APP_REPLACED.ordinal()] = 7;
        } catch (NoSuchFieldError e7) {
        }
    }
}
