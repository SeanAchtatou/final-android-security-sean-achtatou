package com.tencent.assistant.module.b;

import com.tencent.assistant.db.a.a;
import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistant.localres.model.LocalApkInfo;

/* compiled from: ProGuard */
class c extends ApkResCallback.Stub {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b f1711a;

    c(b bVar) {
        this.f1711a = bVar;
    }

    public void onInstalledApkDataChanged(LocalApkInfo localApkInfo, int i) {
        a aVar;
        if (localApkInfo != null && i == 2 && (aVar = (a) this.f1711a.i.get(localApkInfo.mPackageName)) != null && aVar.f == localApkInfo.mVersionCode) {
            this.f1711a.b(aVar);
        }
    }
}
