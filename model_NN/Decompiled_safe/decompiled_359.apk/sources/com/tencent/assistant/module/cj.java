package com.tencent.assistant.module;

import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.assistantv2.model.a.e;
import java.util.ArrayList;

/* compiled from: ProGuard */
class cj implements CallbackHelper.Caller<e> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f1751a;
    final /* synthetic */ boolean b;
    final /* synthetic */ byte[] c;
    final /* synthetic */ boolean d;
    final /* synthetic */ ArrayList e;
    final /* synthetic */ ce f;

    cj(ce ceVar, int i, boolean z, byte[] bArr, boolean z2, ArrayList arrayList) {
        this.f = ceVar;
        this.f1751a = i;
        this.b = z;
        this.c = bArr;
        this.d = z2;
        this.e = arrayList;
    }

    /* renamed from: a */
    public void call(e eVar) {
        eVar.a(this.f1751a, 0, this.b, this.c, this.d, this.f.h, this.e, this.f.l);
    }
}
