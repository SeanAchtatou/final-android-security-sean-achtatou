package com.tencent.assistant.module;

import com.tencent.assistant.js.i;
import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.assistant.module.callback.n;
import com.tencent.assistant.protocol.jce.GetDomainCapabilityResponse;

/* compiled from: ProGuard */
class cx implements CallbackHelper.Caller<n> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GetDomainCapabilityResponse f1763a;
    final /* synthetic */ int b;
    final /* synthetic */ cw c;

    cx(cw cwVar, GetDomainCapabilityResponse getDomainCapabilityResponse, int i) {
        this.c = cwVar;
        this.f1763a = getDomainCapabilityResponse;
        this.b = i;
    }

    /* renamed from: a */
    public void call(n nVar) {
        i iVar = null;
        if (this.f1763a != null && this.f1763a.f2118a == 0) {
            iVar = new i(this.f1763a.c, this.f1763a.b);
        }
        if (iVar != null) {
            nVar.onGetDomainCapabilitySuccess(this.b, 0, iVar);
        }
    }
}
