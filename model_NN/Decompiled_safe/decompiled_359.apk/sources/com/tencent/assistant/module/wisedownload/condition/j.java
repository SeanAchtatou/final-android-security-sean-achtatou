package com.tencent.assistant.module.wisedownload.condition;

import android.net.wifi.WifiInfo;
import android.text.TextUtils;
import com.tencent.assistant.module.wisedownload.b;
import com.tencent.assistant.module.wisedownload.condition.ThresholdCondition;
import com.tencent.assistant.protocol.jce.AutoDownloadCfg;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class j extends ThresholdCondition {
    protected AutoDownloadCfg c;
    private int d;
    private int e;
    private boolean f = true;
    private List<String> g;

    public j(b bVar) {
        a(bVar);
    }

    public void a(b bVar) {
        boolean z = true;
        if (bVar != null) {
            this.c = bVar.j();
            if (this.c != null) {
                this.d = this.c.f2009a;
                this.e = this.c.b;
                if (!(this.c.f == 0 || this.c.f == 1)) {
                    z = false;
                }
                this.f = z;
            }
            this.g = bVar.i();
        }
    }

    public boolean a() {
        a(ThresholdCondition.CONDITION_RESULT_CODE.OK);
        return b() && h() && j() && i();
    }

    public boolean b() {
        boolean e2 = e();
        if (!e2) {
            a(ThresholdCondition.CONDITION_RESULT_CODE.FAIL_PRIMARY_SCREEN);
        }
        return e2;
    }

    public boolean h() {
        boolean d2 = d();
        if (!d2) {
            a(ThresholdCondition.CONDITION_RESULT_CODE.FAIL_PRIMARY_WIFI);
        }
        return d2;
    }

    public boolean i() {
        boolean z;
        if (this.g == null || this.g.isEmpty()) {
            a(ThresholdCondition.CONDITION_RESULT_CODE.FAIL_PRIMARY_RELIABLE_WIFI);
            return false;
        }
        WifiInfo g2 = ThresholdCondition.g();
        if (g2 == null || TextUtils.isEmpty(g2.getBSSID())) {
            a(ThresholdCondition.CONDITION_RESULT_CODE.FAIL_PRIMARY_RELIABLE_WIFI);
            return false;
        }
        String bssid = g2.getBSSID();
        Iterator<String> it = this.g.iterator();
        while (true) {
            if (it.hasNext()) {
                if (bssid.equalsIgnoreCase(it.next())) {
                    z = true;
                    break;
                }
            } else {
                z = false;
                break;
            }
        }
        if (z) {
            return z;
        }
        a(ThresholdCondition.CONDITION_RESULT_CODE.FAIL_PRIMARY_RELIABLE_WIFI);
        return z;
    }

    public boolean j() {
        if (this.f1907a != null) {
            boolean z = this.f1907a.getBoolean("battery_charging_status", true);
            int i = this.f1907a.getInt("battery_level", 100);
            if (z) {
                if (i < this.e) {
                    a(ThresholdCondition.CONDITION_RESULT_CODE.FAIL_PRIMARY_CHARGING);
                }
                if (i >= this.e) {
                    return true;
                }
                return false;
            }
            if (i < this.d) {
                a(ThresholdCondition.CONDITION_RESULT_CODE.FAIL_PRIMARY_UNCHARGING);
            }
            if (i < this.d) {
                return false;
            }
            return true;
        }
        a(ThresholdCondition.CONDITION_RESULT_CODE.FAIL_PRIMARY_NO_RECEIVE_BATTERY);
        return false;
    }

    public boolean k() {
        if (this.f1907a != null) {
            return this.f1907a.getBoolean("battery_charging_status", true);
        }
        return false;
    }

    public int l() {
        if (this.f1907a != null) {
            return this.f1907a.getInt("battery_level", 100);
        }
        return 0;
    }
}
