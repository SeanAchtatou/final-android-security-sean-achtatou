package com.tencent.assistant.js;

import android.content.SharedPreferences;
import com.qq.AppService.AstApp;
import com.tencent.assistant.login.PluginLoginIn;
import com.tencent.assistant.utils.bj;

/* compiled from: ProGuard */
public class m {
    private static SharedPreferences a() {
        return AstApp.i().getApplicationContext().getSharedPreferences("openId_map", 0);
    }

    public static void a(long j, String str) {
        SharedPreferences.Editor edit = a().edit();
        edit.putString(bj.b(PluginLoginIn.getUin() + "&" + j), str);
        edit.commit();
    }
}
