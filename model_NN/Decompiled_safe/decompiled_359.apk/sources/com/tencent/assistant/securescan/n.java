package com.tencent.assistant.securescan;

import android.os.Handler;
import android.os.Message;

/* compiled from: ProGuard */
class n extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ScanningAnimView f2512a;

    n(ScanningAnimView scanningAnimView) {
        this.f2512a = scanningAnimView;
    }

    public void handleMessage(Message message) {
        super.handleMessage(message);
        switch (message.what) {
            case 10:
                this.f2512a.startScanAnim();
                return;
            default:
                return;
        }
    }
}
