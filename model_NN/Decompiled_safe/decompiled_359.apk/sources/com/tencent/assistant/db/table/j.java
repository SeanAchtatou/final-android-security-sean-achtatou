package com.tencent.assistant.db.table;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.db.helper.AstDbHelper;
import com.tencent.assistant.db.helper.SQLiteDatabaseWrapper;
import com.tencent.assistant.db.helper.SqliteHelper;
import com.tencent.assistantv2.st.model.StatInfo;

/* compiled from: ProGuard */
public class j implements IBaseTable {
    /* JADX WARNING: Removed duplicated region for block: B:20:0x003b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.tencent.assistantv2.st.model.StatInfo a(java.lang.String r7) {
        /*
            r6 = this;
            r0 = 0
            boolean r1 = android.text.TextUtils.isEmpty(r7)
            if (r1 != 0) goto L_0x002c
            com.tencent.assistant.db.helper.SqliteHelper r1 = r6.getHelper()
            com.tencent.assistant.db.helper.SQLiteDatabaseWrapper r1 = r1.getWritableDatabaseWrapper()
            java.lang.String r2 = "select * from download_statinfo where stat_download_ticket = ?"
            r3 = 1
            java.lang.String[] r3 = new java.lang.String[r3]     // Catch:{ Exception -> 0x002d, all -> 0x0035 }
            r4 = 0
            r3[r4] = r7     // Catch:{ Exception -> 0x002d, all -> 0x0035 }
            android.database.Cursor r1 = r1.rawQuery(r2, r3)     // Catch:{ Exception -> 0x002d, all -> 0x0035 }
            if (r1 == 0) goto L_0x0027
            boolean r2 = r1.moveToFirst()     // Catch:{ Exception -> 0x0041, all -> 0x003f }
            if (r2 == 0) goto L_0x0027
            com.tencent.assistantv2.st.model.StatInfo r0 = r6.a(r1)     // Catch:{ Exception -> 0x0041, all -> 0x003f }
        L_0x0027:
            if (r1 == 0) goto L_0x002c
            r1.close()
        L_0x002c:
            return r0
        L_0x002d:
            r1 = move-exception
            r1 = r0
        L_0x002f:
            if (r1 == 0) goto L_0x002c
            r1.close()
            goto L_0x002c
        L_0x0035:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x0039:
            if (r1 == 0) goto L_0x003e
            r1.close()
        L_0x003e:
            throw r0
        L_0x003f:
            r0 = move-exception
            goto L_0x0039
        L_0x0041:
            r2 = move-exception
            goto L_0x002f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.db.table.j.a(java.lang.String):com.tencent.assistantv2.st.model.StatInfo");
    }

    public void a(String str, StatInfo statInfo) {
        SQLiteDatabaseWrapper writableDatabaseWrapper;
        if (!TextUtils.isEmpty(str) && statInfo != null && (writableDatabaseWrapper = getHelper().getWritableDatabaseWrapper()) != null) {
            try {
                ContentValues contentValues = new ContentValues();
                contentValues.put("stat_download_ticket", str);
                a(contentValues, statInfo);
                if (writableDatabaseWrapper.update("download_statinfo", contentValues, "stat_download_ticket = ?", new String[]{str}) <= 0) {
                    writableDatabaseWrapper.insert("download_statinfo", null, contentValues);
                }
            } catch (Exception e) {
            }
        }
    }

    public synchronized void b(String str) {
        if (!TextUtils.isEmpty(str)) {
            getHelper().getWritableDatabaseWrapper().delete("download_statinfo", "stat_download_ticket = ?", new String[]{str});
        }
    }

    private StatInfo a(Cursor cursor) {
        StatInfo statInfo = new StatInfo();
        statInfo.scene = (int) cursor.getLong(cursor.getColumnIndexOrThrow("stat_scene"));
        statInfo.sourceScene = (int) cursor.getLong(cursor.getColumnIndexOrThrow("stat_source_scene"));
        statInfo.slotId = cursor.getString(cursor.getColumnIndexOrThrow("stat_slotid"));
        statInfo.sourceSceneSlotId = cursor.getString(cursor.getColumnIndexOrThrow("stat_source_slotid"));
        statInfo.status = cursor.getString(cursor.getColumnIndexOrThrow("stat_staus"));
        statInfo.extraData = cursor.getString(cursor.getColumnIndexOrThrow("stat_extradata"));
        statInfo.recommendId = cursor.getBlob(cursor.getColumnIndexOrThrow("stat_recommendid"));
        statInfo.contentId = cursor.getString(cursor.getColumnIndexOrThrow("stat_contentid"));
        statInfo.pushInfo = cursor.getString(cursor.getColumnIndexOrThrow("stat_pushinfo"));
        statInfo.searchId = cursor.getLong(cursor.getColumnIndexOrThrow("stat_searchid"));
        statInfo.searchPreId = cursor.getString(cursor.getColumnIndexOrThrow("stat_pre_searchid"));
        statInfo.expatiation = cursor.getString(cursor.getColumnIndexOrThrow("stat_expatition"));
        statInfo.callerVia = cursor.getString(cursor.getColumnIndexOrThrow("stat_via"));
        statInfo.callerUin = cursor.getString(cursor.getColumnIndexOrThrow("stat_uin"));
        statInfo.callerPackageName = cursor.getString(cursor.getColumnIndexOrThrow("stat_host_packagename"));
        statInfo.callerVersionCode = cursor.getString(cursor.getColumnIndexOrThrow("stat_host_versioncode"));
        statInfo.traceId = cursor.getString(cursor.getColumnIndexOrThrow("stat_traceId"));
        statInfo.d = (int) cursor.getLong(cursor.getColumnIndexOrThrow("stat_actionflag"));
        return statInfo;
    }

    private void a(ContentValues contentValues, StatInfo statInfo) {
        if (statInfo != null) {
            contentValues.put("stat_scene", Integer.valueOf(statInfo.scene));
            contentValues.put("stat_source_scene", Integer.valueOf(statInfo.sourceScene));
            contentValues.put("stat_slotid", statInfo.slotId);
            contentValues.put("stat_source_slotid", statInfo.sourceSceneSlotId);
            contentValues.put("stat_staus", statInfo.status);
            contentValues.put("stat_extradata", statInfo.extraData);
            contentValues.put("stat_recommendid", statInfo.recommendId);
            contentValues.put("stat_contentid", statInfo.contentId);
            contentValues.put("stat_pushinfo", statInfo.pushInfo);
            contentValues.put("stat_searchid", Long.valueOf(statInfo.searchId));
            contentValues.put("stat_pre_searchid", statInfo.searchPreId);
            contentValues.put("stat_expatition", statInfo.expatiation);
            contentValues.put("stat_via", statInfo.callerVia);
            contentValues.put("stat_uin", statInfo.callerUin);
            contentValues.put("stat_host_packagename", statInfo.callerPackageName);
            contentValues.put("stat_host_versioncode", statInfo.callerVersionCode);
            contentValues.put("stat_traceId", statInfo.traceId);
            contentValues.put("stat_actionflag", Integer.valueOf(statInfo.d));
        }
    }

    public int tableVersion() {
        return 1;
    }

    public String tableName() {
        return "download_statinfo";
    }

    public String createTableSQL() {
        return "CREATE TABLE if not exists download_statinfo (_id INTEGER PRIMARY KEY AUTOINCREMENT,stat_download_ticket TEXT,stat_scene INTEGER,stat_source_scene INTEGER,stat_slotid TEXT,stat_source_slotid TEXT,stat_staus TEXT,stat_extradata TEXT,stat_recommendid BLOB,stat_contentid TEXT,stat_pushinfo TEXT,stat_searchid INTEGER,stat_pre_searchid TEXT,stat_expatition TEXT,stat_via TEXT,stat_uin TEXT,stat_host_packagename TEXT,stat_host_versioncode TEXT,stat_traceId TEXT,stat_actionflag TEXT);";
    }

    public String[] getAlterSQL(int i, int i2) {
        if (i == 10 && i2 == 11) {
            return new String[]{"CREATE TABLE if not exists download_statinfo (_id INTEGER PRIMARY KEY AUTOINCREMENT,stat_download_ticket TEXT,stat_scene INTEGER,stat_source_scene INTEGER,stat_slotid TEXT,stat_source_slotid TEXT,stat_staus TEXT,stat_extradata TEXT,stat_recommendid BLOB,stat_contentid TEXT,stat_pushinfo TEXT,stat_searchid INTEGER,stat_pre_searchid TEXT,stat_expatition TEXT,stat_via TEXT,stat_uin TEXT,stat_host_packagename TEXT,stat_host_versioncode TEXT,stat_traceId TEXT,stat_actionflag TEXT);"};
        } else if (i == 11 && i2 == 12) {
            return new String[]{"alter table download_statinfo add column stat_via TEXT;", "alter table download_statinfo add column stat_uin TEXT;", "alter table download_statinfo add column stat_host_packagename TEXT;", "alter table download_statinfo add column stat_host_versioncode TEXT;", "alter table download_statinfo add column stat_traceId TEXT;"};
        } else if (i != 12 || i2 != 13) {
            return null;
        } else {
            return new String[]{"alter table download_statinfo add column stat_actionflag TEXT;"};
        }
    }

    public void beforeTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public void afterTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public SqliteHelper getHelper() {
        return AstDbHelper.get(AstApp.i());
    }
}
