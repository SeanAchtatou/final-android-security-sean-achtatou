package com.tencent.assistant.appbakcup;

import android.app.Dialog;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.protocol.jce.BackupDevice;
import com.tencent.assistant.utils.cv;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class BackupDeviceAdapter extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    private Context f827a;
    private ArrayList<BackupDevice> b;
    /* access modifiers changed from: private */
    public int c = -1;
    /* access modifiers changed from: private */
    public k d;
    private Dialog e;

    public BackupDeviceAdapter(Context context) {
        this.f827a = context;
    }

    public int getCount() {
        if (this.b == null) {
            return 0;
        }
        return this.b.size();
    }

    /* renamed from: a */
    public BackupDevice getItem(int i) {
        if (this.b == null || this.b.size() == 0) {
            return null;
        }
        return this.b.get(i);
    }

    public long getItemId(int i) {
        return 0;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        l lVar;
        if (view == null || view.getTag() == null) {
            view = View.inflate(this.f827a, R.layout.device_item, null);
            l lVar2 = new l(null);
            lVar2.f838a = (TextView) view.findViewById(R.id.device_name);
            lVar2.b = (TextView) view.findViewById(R.id.backup_time);
            lVar2.c = (ProgressBar) view.findViewById(R.id.progress);
            lVar2.d = (ImageView) view.findViewById(R.id.last_line);
            view.setTag(lVar2);
            lVar = lVar2;
        } else {
            lVar = (l) view.getTag();
        }
        BackupDevice a2 = getItem(i);
        if (a2 != null) {
            String b2 = a2.b();
            if (TextUtils.isEmpty(b2)) {
                b2 = "未知设备";
            }
            lVar.f838a.setText(b2);
            String c2 = cv.c(Long.valueOf(a2.c()));
            if (!TextUtils.isEmpty(c2)) {
                lVar.b.setText(c2);
            }
            ProgressBar progressBar = lVar.c;
            if (this.c == i) {
                progressBar.setVisibility(0);
            } else {
                progressBar.setVisibility(8);
            }
            lVar.d.setVisibility(0);
            view.setOnClickListener(new j(this, a2, progressBar, i));
        }
        return view;
    }

    public void a(ArrayList<BackupDevice> arrayList) {
        this.b = arrayList;
        this.c = -1;
    }

    public void a(Dialog dialog) {
        this.e = dialog;
    }

    public void a(k kVar) {
        this.d = kVar;
    }

    public void a() {
        this.c = -1;
        notifyDataSetChanged();
    }
}
