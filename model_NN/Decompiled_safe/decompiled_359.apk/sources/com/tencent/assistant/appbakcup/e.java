package com.tencent.assistant.appbakcup;

import android.view.View;
import android.widget.TextView;
import com.tencent.assistant.component.AppBackupAppItemInfo;
import com.tencent.assistant.component.AppIconView;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.component.ListItemInfoView;

/* compiled from: ProGuard */
class e {

    /* renamed from: a  reason: collision with root package name */
    View f833a;
    TextView b;
    AppIconView c;
    TextView d;
    TextView e;
    TextView f;
    DownloadButton g;
    ListItemInfoView h;
    AppBackupAppItemInfo i;

    private e() {
    }

    /* synthetic */ e(a aVar) {
        this();
    }
}
