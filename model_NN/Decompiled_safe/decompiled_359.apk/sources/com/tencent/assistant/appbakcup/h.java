package com.tencent.assistant.appbakcup;

import android.view.View;
import android.widget.TextView;

/* compiled from: ProGuard */
class h implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ BackupApplistDialog f836a;

    h(BackupApplistDialog backupApplistDialog) {
        this.f836a = backupApplistDialog;
    }

    public void onClick(View view) {
        boolean z = true;
        boolean unused = this.f836a.j = true;
        TextView c = this.f836a.f;
        if (this.f836a.f.isSelected()) {
            z = false;
        }
        c.setSelected(z);
        this.f836a.d.a(this.f836a.f.isSelected());
        this.f836a.b();
    }
}
