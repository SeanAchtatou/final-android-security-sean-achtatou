package com.tencent.assistant.plugin.mgr;

import com.tencent.assistant.Global;
import com.tencent.assistant.model.j;
import com.tencent.assistant.plugin.PluginInfo;

/* compiled from: ProGuard */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private static b[] f1945a = {new b("com.tencent.mobileassistant_login", 1), new b("com.tencent.mobileassistant_login", 2), new b("p.com.tencent.android.qqdownloader", 1), new b("p.com.tencent.android.qqdownloader", 2), new b("p.com.tencent.android.qqdownloader", 3), new b("p.com.tencent.android.qqdownloader", 0), new b("com.tencent.assistant.root", 1), new b("com.tencent.assistant.root", 2), new b("com.tencent.assistant.root", 3), new b("com.tencent.assistant.root", 4)};

    public static boolean a(PluginInfo pluginInfo) {
        if (pluginInfo != null && pluginInfo.getMinFLevel() <= e.b() && pluginInfo.getMinBaoVersion() <= Global.getAppVersionCode()) {
            return a(pluginInfo.getPackageName(), pluginInfo.getVersion());
        }
        return false;
    }

    public static boolean a(String str, int i) {
        if (f1945a != null && f1945a.length > 0) {
            for (b bVar : f1945a) {
                if (bVar.f1946a.equals(str) && (bVar.b == i || bVar.b == 0)) {
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean a(j jVar) {
        return true;
    }
}
