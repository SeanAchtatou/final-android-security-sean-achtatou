package com.tencent.assistant.plugin;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/* compiled from: ProGuard */
final class a implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Method f1941a;
    final /* synthetic */ Object b;

    a(Method method, Object obj) {
        this.f1941a = method;
        this.b = obj;
    }

    public void run() {
        try {
            this.f1941a.invoke(this.b, new Object[0]);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e2) {
            e2.printStackTrace();
        } catch (InvocationTargetException e3) {
            e3.printStackTrace();
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }
}
