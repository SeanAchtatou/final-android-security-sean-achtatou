package com.tencent.assistant.plugin.mgr;

import android.os.Message;
import com.qq.AppService.AstApp;
import com.tencent.assistant.event.EventDispatcherEnum;

/* compiled from: ProGuard */
public class PluginDispatcher {
    public static void dispatchMemoryCleanSuc(Object obj) {
        baseDispatch(EventDispatcherEnum.UI_EVENT_MGR_MEMORY_CLEAN_SUCCESS, obj);
    }

    public static void dispatchMemoryCleanFail(Object obj) {
        baseDispatch(EventDispatcherEnum.UI_EVENT_MGR_MEMORY_CLEAN_FAIL, obj);
    }

    public static void baseDispatch(int i, Object obj) {
        Message obtainMessage = AstApp.i().j().obtainMessage(i);
        if (obj != null) {
            obtainMessage.obj = obj;
        }
        AstApp.i().j().sendMessage(obtainMessage);
    }
}
