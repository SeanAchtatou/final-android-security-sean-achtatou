package com.tencent.assistant.activity;

import android.util.Log;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;

/* compiled from: ProGuard */
class ca implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DownloadActivity f442a;

    ca(DownloadActivity downloadActivity) {
        this.f442a = downloadActivity;
    }

    public void run() {
        LocalApkInfo installedApkInfo = ApkResourceManager.getInstance().getInstalledApkInfo(this.f442a.G.c, true);
        Log.i("icerao", "get current apk info finish." + this.f442a.G.c + ",local apkinfo:" + installedApkInfo);
        this.f442a.a(installedApkInfo);
    }
}
