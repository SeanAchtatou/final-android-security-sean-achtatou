package com.tencent.assistant.activity;

import com.tencent.assistant.AppConst;
import com.tencent.assistant.login.d;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class ap extends AppConst.TwoBtnDialogInfo {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f404a;
    final /* synthetic */ int b;
    final /* synthetic */ AppBackupActivity c;

    ap(AppBackupActivity appBackupActivity, int i, int i2) {
        this.c = appBackupActivity;
        this.f404a = i;
        this.b = i2;
    }

    public void onRightBtnClick() {
        if (this.f404a == 1) {
            this.c.b(3);
            k.a(new STInfoV2(STConst.ST_PAGE_APP_BACKUP_SWITCHDEVICE, "03_001", 0, STConst.ST_DEFAULT_SLOT, 100));
        } else if (this.f404a == 2) {
            if (this.c.w == null || this.c.w.a() == null || this.c.w.a().isEmpty()) {
                this.c.F();
                this.c.E();
            } else {
                this.c.b(4);
            }
            k.a(new STInfoV2(STConst.ST_PAGE_APP_BACKUP_REFRESHDEVICE, "03_001", 0, STConst.ST_DEFAULT_SLOT, 100));
        }
    }

    public void onLeftBtnClick() {
        if (this.b == 2 && d.a().j()) {
            if (this.c.v.d()) {
                this.c.H();
            } else {
                this.c.G();
            }
        }
        if (this.f404a == 1) {
            k.a(new STInfoV2(STConst.ST_PAGE_APP_BACKUP_SWITCHDEVICE, "04_001", 0, STConst.ST_DEFAULT_SLOT, 100));
        } else {
            k.a(new STInfoV2(STConst.ST_PAGE_APP_BACKUP_REFRESHDEVICE, "04_001", 0, STConst.ST_DEFAULT_SLOT, 100));
        }
    }

    public void onCancell() {
    }
}
