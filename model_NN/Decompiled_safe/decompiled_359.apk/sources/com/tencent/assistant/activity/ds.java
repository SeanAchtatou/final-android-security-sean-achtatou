package com.tencent.assistant.activity;

import android.text.Editable;
import android.text.TextWatcher;

/* compiled from: ProGuard */
class ds implements TextWatcher {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ HelperFeedbackActivity f534a;

    ds(HelperFeedbackActivity helperFeedbackActivity) {
        this.f534a = helperFeedbackActivity;
    }

    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public void afterTextChanged(Editable editable) {
        if (editable.length() >= 300) {
            this.f534a.u.showOverflowView();
        } else {
            this.f534a.u.hideOverflowView();
        }
    }
}
