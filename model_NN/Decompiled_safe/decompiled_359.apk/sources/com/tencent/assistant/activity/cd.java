package com.tencent.assistant.activity;

import com.tencent.assistant.AppConst;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.module.u;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class cd implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ STInfoV2 f445a;
    final /* synthetic */ boolean b;
    final /* synthetic */ DownloadInfo c;
    final /* synthetic */ DownloadActivity d;

    cd(DownloadActivity downloadActivity, STInfoV2 sTInfoV2, boolean z, DownloadInfo downloadInfo) {
        this.d = downloadActivity;
        this.f445a = sTInfoV2;
        this.b = z;
        this.c = downloadInfo;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.st.page.a.a(com.tencent.assistant.AppConst$AppState, boolean):java.lang.String
     arg types: [com.tencent.assistant.AppConst$AppState, int]
     candidates:
      com.tencent.assistantv2.st.page.a.a(int, java.lang.String):com.tencent.assistantv2.st.page.STPageInfo
      com.tencent.assistantv2.st.page.a.a(android.content.Context, java.lang.String):com.tencent.assistantv2.st.page.STPageInfo
      com.tencent.assistantv2.st.page.a.a(com.tencent.assistant.AppConst$AppState, com.tencent.assistant.model.SimpleAppModel):java.lang.String
      com.tencent.assistantv2.st.page.a.a(com.tencent.assistantv2.st.model.STCommonInfo$ContentIdType, java.lang.String):java.lang.String
      com.tencent.assistantv2.st.page.a.a(java.lang.String, int):java.lang.String
      com.tencent.assistantv2.st.page.a.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistantv2.st.page.a.a(com.tencent.assistant.AppConst$AppState, boolean):java.lang.String */
    public void run() {
        if (this.f445a != null) {
            STInfoV2 sTInfoV2 = this.f445a;
            if (this.b) {
                sTInfoV2.actionId = 900;
                sTInfoV2.status = STConst.ST_STATUS_DEFAULT;
            } else {
                AppConst.AppState b2 = u.b(this.c);
                sTInfoV2.actionId = a.a(b2);
                sTInfoV2.status = a.a(b2, false);
            }
            sTInfoV2.isImmediately = true;
            k.a(sTInfoV2);
        }
        if (this.c != null) {
            com.tencent.assistant.download.a.a().a(this.c);
        }
    }
}
