package com.tencent.assistant.activity;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.module.u;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class ff extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ fd f575a;

    ff(fd fdVar) {
        this.f575a = fdVar;
    }

    public void onTMAClick(View view) {
        this.f575a.f573a.v();
    }

    public STInfoV2 getStInfo() {
        return STInfoBuilder.buildSTInfo(this.f575a.f573a, this.f575a.f573a.z, "03_001", a.a(u.d(this.f575a.f573a.z)), a.a(u.d(this.f575a.f573a.z), this.f575a.f573a.z));
    }
}
