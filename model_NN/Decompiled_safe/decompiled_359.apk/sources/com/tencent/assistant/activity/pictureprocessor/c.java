package com.tencent.assistant.activity.pictureprocessor;

import android.support.v4.view.ViewPager;

/* compiled from: ProGuard */
class c implements ViewPager.OnPageChangeListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ShowPictureActivity f651a;

    c(ShowPictureActivity showPictureActivity) {
        this.f651a = showPictureActivity;
    }

    public void onPageSelected(int i) {
        int unused = this.f651a.h = i;
        if (this.f651a.f649a != null) {
            this.f651a.i.setSelect(this.f651a.f649a.size(), i);
        }
        this.f651a.e.b(i);
        this.f651a.a(i);
    }

    public void onPageScrolled(int i, float f, int i2) {
    }

    public void onPageScrollStateChanged(int i) {
    }
}
