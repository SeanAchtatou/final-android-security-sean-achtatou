package com.tencent.assistant.activity;

import android.os.IBinder;
import com.tencent.assistant.utils.XLog;
import com.tencent.tmsecurelite.commom.DataEntity;
import com.tencent.tmsecurelite.optimize.f;
import org.json.JSONException;

/* compiled from: ProGuard */
class q extends f {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ApkMgrActivity f652a;

    q(ApkMgrActivity apkMgrActivity) {
        this.f652a = apkMgrActivity;
    }

    public IBinder asBinder() {
        return null;
    }

    public void c() {
        XLog.d("ApkMgrActivity", "onScanStarted.");
    }

    public void a(int i) {
        XLog.d("ApkMgrActivity", "onScanProgressChanged. progress = " + i);
    }

    public void b() {
        if (!this.f652a.K) {
            if (this.f652a.R.hasMessages(110011)) {
                this.f652a.R.removeMessages(110011);
            }
            this.f652a.R.sendMessage(this.f652a.R.obtainMessage(110011));
            XLog.i("ApkMgrActivity", "onScanFinished in ApkMgrActivity");
        }
    }

    public void a() {
        XLog.d("ApkMgrActivity", "onScanCanceled in ApkMgrActivity");
        if (!this.f652a.K) {
            if (this.f652a.R.hasMessages(110011)) {
                this.f652a.R.removeMessages(110011);
            }
            this.f652a.R.sendMessage(this.f652a.R.obtainMessage(110011));
        }
    }

    public void a(int i, DataEntity dataEntity) {
        XLog.i("ig_rubbish", dataEntity.toString());
        try {
            if (!dataEntity.getBoolean("rubbish.suggest")) {
                return;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (!this.f652a.K) {
            boolean unused = this.f652a.K = true;
            if (this.f652a.R.hasMessages(110011)) {
                this.f652a.R.removeMessages(110011);
            }
            this.f652a.R.sendMessage(this.f652a.R.obtainMessage(110011));
        }
        XLog.i("ApkMgrActivity", "onRubbishFound in ApkMgrActivity");
    }
}
