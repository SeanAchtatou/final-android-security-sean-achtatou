package com.tencent.assistant.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.adapter.DownloadInfoMultiAdapter;
import com.tencent.assistant.component.DownloadListFooterView;
import com.tencent.assistant.component.NormalErrorRecommendPage;
import com.tencent.assistant.component.UserTaskView;
import com.tencent.assistant.component.invalidater.ListViewScrollListener;
import com.tencent.assistant.component.txscrollview.TXExpandableListView;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.download.SimpleDownloadInfo;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.DownloadProxy;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.callback.aj;
import com.tencent.assistant.module.callback.s;
import com.tencent.assistant.module.ds;
import com.tencent.assistant.module.fs;
import com.tencent.assistant.module.u;
import com.tencent.assistant.protocol.jce.AppSimpleDetail;
import com.tencent.assistant.protocol.jce.InstalledAppItem;
import com.tencent.assistant.protocol.jce.UserTaskCfg;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.st.f;
import com.tencent.assistant.st.h;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.ba;
import com.tencent.assistant.utils.ct;
import com.tencent.assistant.utils.v;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import com.tencent.assistantv2.mediadownload.c;
import com.tencent.assistantv2.mediadownload.n;
import com.tencent.assistantv2.model.d;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.connect.common.Constants;

@SuppressLint({"NewApi"})
/* compiled from: ProGuard */
public class DownloadActivity extends BaseActivity implements UIEventListener, aj, s {
    private static int F = 2;
    private TextView A;
    private TextView B;
    private Button C;
    private int D;
    /* access modifiers changed from: private */
    public int E = -1;
    /* access modifiers changed from: private */
    public SimpleAppModel G;
    private String H;
    private boolean I = false;
    private boolean J = false;
    private boolean K = false;
    private int L = 0;
    private String M = Constants.STR_EMPTY;
    private final int N = 1;
    /* access modifiers changed from: private */
    public n O;
    private Dialog P = null;
    private String Q;
    private final int R = 2;
    private NormalErrorRecommendPage S;
    /* access modifiers changed from: private */
    public long T = 0;
    private boolean U = false;
    private boolean V = false;
    private volatile boolean W = false;
    /* access modifiers changed from: private */
    public volatile boolean X = false;
    /* access modifiers changed from: private */
    public Boolean Y = true;
    /* access modifiers changed from: private */
    public UserTaskCfg Z;
    private Boolean aa = false;
    private String ab = "11_001";
    private boolean ac = false;
    private boolean ad = false;
    private ListViewScrollListener ae = new cf(this);
    /* access modifiers changed from: private */
    public Handler af = new cg(this);
    /* access modifiers changed from: private */
    public Context n;
    private RelativeLayout t;
    private SecondNavigationTitleViewV5 u;
    /* access modifiers changed from: private */
    public TXExpandableListView v;
    /* access modifiers changed from: private */
    public DownloadInfoMultiAdapter w;
    private ds x = new ds();
    private fs y = new fs();
    /* access modifiers changed from: private */
    public View z;

    public void a(Boolean bool) {
        this.aa = bool;
    }

    public int f() {
        if (this.aa.booleanValue()) {
            return STConst.ST_PAGE_DOWNLOAD_USERTASK;
        }
        return STConst.ST_PAGE_DOWNLOAD;
    }

    public int p() {
        if (this.O == null) {
            return super.p();
        }
        if (this.O.d() == 2) {
            return STConst.ST_PAGE_DOWNLOAD_FROM_OUTTER_TMAST;
        }
        return STConst.ST_PAGE_DOWNLOAD_FROM_OUTTER;
    }

    public boolean g() {
        return false;
    }

    private STInfoV2 a(SimpleAppModel simpleAppModel) {
        AppConst.AppState d = u.d(simpleAppModel);
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this, simpleAppModel, STConst.ST_DEFAULT_SLOT, a.a(d), a.a(d, simpleAppModel));
        if (buildSTInfo != null) {
            buildSTInfo.updateWithExternalPara(this.p);
        }
        return buildSTInfo;
    }

    private void i() {
        STInfoV2 a2 = a(this.G);
        if (a2 != null) {
            a2.actionId = 100;
        }
        k.a(a2);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        try {
            setContentView((int) R.layout.activity_download);
            this.n = this;
            this.x.register(this);
            this.y.register(this);
            j();
            z();
            this.w.h();
            this.w.f671a = this.ac;
            v();
            E();
            AstApp.i().j().sendEmptyMessage(EventDispatcherEnum.UI_EVENT_ENTRANCE_DOWNLOAD_PAGE);
            i();
        } catch (Throwable th) {
            this.ad = true;
            finish();
        }
    }

    private void j() {
        long j;
        Intent intent = getIntent();
        this.O = n.a(intent);
        if (this.O != null) {
            this.I = true;
            if (this.O.d() == 1) {
                f.b("ANDROID.YYB.DOWNURL");
                if (this.p != null) {
                    this.p.f3357a = "ANDROID.YYB.DOWNURL";
                    return;
                }
                return;
            }
            return;
        }
        Bundle extras = intent.getExtras();
        if (extras != null) {
            String string = extras.getString(com.tencent.assistant.b.a.c);
            if (TextUtils.isEmpty(string)) {
                j = ct.c(extras.getString(com.tencent.assistant.b.a.f845a));
            } else {
                j = 0;
            }
            long c = ct.c(extras.getString(com.tencent.assistant.b.a.b));
            this.Q = extras.getString(com.tencent.assistant.b.a.e);
            int d = ct.d(extras.getString(com.tencent.assistant.b.a.h));
            String string2 = extras.getString(com.tencent.assistant.b.a.d);
            String string3 = extras.getString(com.tencent.assistant.b.a.C);
            this.I = extras.getBoolean(com.tencent.assistant.b.a.t);
            this.J = !TextUtils.isEmpty(extras.getString(com.tencent.assistant.b.a.w));
            this.L = ct.d(extras.getString(com.tencent.assistant.b.a.H));
            if (ct.d(extras.getString(com.tencent.assistant.b.a.D)) == 1) {
                this.K = true;
            }
            this.M = extras.getString(com.tencent.assistant.b.a.I);
            if (!TextUtils.isEmpty(string) || c > 0) {
                this.G = new SimpleAppModel();
                this.G.f1634a = j;
                this.G.c = string;
                this.G.d = string2;
                this.G.b = c;
                this.G.ac = string3;
                this.G.g = d;
                this.H = extras.getString(com.tencent.assistant.b.a.s);
                this.G.Q = (byte) ct.a(this.H, 0);
                if (this.G.Q == 1) {
                    this.G.ad = ct.d(extras.getString(com.tencent.assistant.b.a.J));
                }
            }
            this.ac = extras.getBoolean("has_new_app_to_install", false);
            m.a().t(true);
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        if (!this.ad) {
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_UPDATE_DELETE, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_ADD, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_INSTALL, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_DOWNLOADLIST_READY, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_DELETE, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOADING, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_QREADER_DELETE, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_ADD, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_DELETE, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOADING, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_ADD, this);
            this.w.f();
            this.w.notifyDataSetChanged();
            w();
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        if (!this.ad) {
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_UPDATE_DELETE, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_ADD, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_INSTALL, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_DOWNLOADLIST_READY, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_DELETE, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOADING, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_QREADER_DELETE, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_ADD, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_DELETE, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOADING, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_ADD, this);
            this.w.g();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (!this.ad) {
            if (this.w != null) {
                this.w.i();
            }
            if (this.x != null) {
                this.x.unregister(this);
            }
            if (this.y != null) {
                this.y.unregister(this);
            }
        }
    }

    private void v() {
        if (DownloadProxy.a().j()) {
            x();
            this.U = true;
            this.w.a();
            InstalledAppItem installedAppItem = null;
            if (this.G != null) {
                installedAppItem = new InstalledAppItem();
                installedAppItem.b = this.G.f1634a;
                installedAppItem.f2221a = this.G.c;
                installedAppItem.c = this.G.g;
            }
            this.w.a(this.M, installedAppItem);
            for (int i = 0; i < this.w.getGroupCount(); i++) {
                this.v.expandGroup(i);
            }
            w();
        }
    }

    private void w() {
        if (this.w.c() == 0 && this.w.j() == DownloadInfoMultiAdapter.CreatingTaskStatusEnum.NONE) {
            this.t.setVisibility(8);
            if (this.S.getVisibility() != 0) {
                this.S.setErrorType(60);
                this.S.setErrorText(getString(R.string.down_page_empty_text));
                this.S.setErrorImage(R.drawable.emptypage_pic_07);
                this.S.setVisibility(0);
                return;
            }
            return;
        }
        this.S.setVisibility(8);
        this.t.setVisibility(0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.activity.DownloadActivity.a(com.tencent.assistant.download.DownloadInfo, boolean, com.tencent.assistantv2.st.page.STInfoV2):void
     arg types: [com.tencent.assistant.download.DownloadInfo, int, com.tencent.assistantv2.st.page.STInfoV2]
     candidates:
      com.tencent.assistant.activity.BaseActivity.a(com.tencent.assistantv2.st.page.STPageInfo, java.lang.String, java.lang.String):void
      android.support.v4.app.FragmentActivity.a(java.lang.String, java.io.PrintWriter, android.view.View):void
      android.support.v4.app.FragmentActivity.a(java.lang.String, boolean, boolean):android.support.v4.app.z
      android.support.v4.app.FragmentActivity.a(android.support.v4.app.Fragment, android.content.Intent, int):void
      com.tencent.assistant.activity.DownloadActivity.a(com.tencent.assistant.download.DownloadInfo, boolean, com.tencent.assistantv2.st.page.STInfoV2):void */
    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0076  */
    /* JADX WARNING: Removed duplicated region for block: B:24:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void x() {
        /*
            r6 = this;
            r5 = 0
            r1 = 0
            com.tencent.assistant.model.SimpleAppModel r0 = r6.G
            if (r0 == 0) goto L_0x00ab
            com.tencent.assistant.manager.DownloadProxy r0 = com.tencent.assistant.manager.DownloadProxy.a()
            com.tencent.assistant.model.SimpleAppModel r2 = r6.G
            java.lang.String r2 = r2.q()
            com.tencent.assistant.download.DownloadInfo r0 = r0.d(r2)
            if (r0 != 0) goto L_0x00ad
            com.tencent.assistant.manager.DownloadProxy r0 = com.tencent.assistant.manager.DownloadProxy.a()
            com.tencent.assistant.model.SimpleAppModel r2 = r6.G
            java.lang.String r2 = r2.c
            com.tencent.assistant.model.SimpleAppModel r3 = r6.G
            int r3 = r3.g
            com.tencent.assistant.model.SimpleAppModel r4 = r6.G
            int r4 = r4.ad
            com.tencent.assistant.download.DownloadInfo r0 = r0.a(r2, r3, r4)
            r2 = r0
        L_0x002b:
            if (r2 == 0) goto L_0x00ab
            r6.G = r5
            r0 = 1
            com.tencent.assistant.model.SimpleAppModel r3 = r6.G
            com.tencent.assistantv2.st.page.STInfoV2 r3 = r6.a(r3)
            r6.a(r2)
            r6.a(r2, r1, r3)
        L_0x003c:
            com.tencent.assistant.model.SimpleAppModel r2 = r6.G
            if (r2 == 0) goto L_0x006b
            if (r0 != 0) goto L_0x006b
            com.tencent.assistant.adapter.DownloadInfoMultiAdapter r0 = r6.w
            com.tencent.assistant.adapter.DownloadInfoMultiAdapter$CreatingTaskStatusEnum r1 = com.tencent.assistant.adapter.DownloadInfoMultiAdapter.CreatingTaskStatusEnum.CREATING
            r0.a(r1)
            com.tencent.assistant.localres.ApkResourceManager r0 = com.tencent.assistant.localres.ApkResourceManager.getInstance()
            boolean r0 = r0.isLocalApkDataReady()
            if (r0 == 0) goto L_0x005e
            java.lang.String r0 = "icerao"
            java.lang.String r1 = "local apk is ready."
            android.util.Log.i(r0, r1)
            r6.a(r5)
        L_0x005d:
            return
        L_0x005e:
            com.tencent.assistant.utils.TemporaryThreadManager r0 = com.tencent.assistant.utils.TemporaryThreadManager.get()
            com.tencent.assistant.activity.ca r1 = new com.tencent.assistant.activity.ca
            r1.<init>(r6)
            r0.start(r1)
            goto L_0x005d
        L_0x006b:
            com.tencent.assistant.adapter.DownloadInfoMultiAdapter r0 = r6.w
            com.tencent.assistant.adapter.DownloadInfoMultiAdapter$CreatingTaskStatusEnum r2 = com.tencent.assistant.adapter.DownloadInfoMultiAdapter.CreatingTaskStatusEnum.NONE
            r0.a(r2)
            com.tencent.assistantv2.mediadownload.n r0 = r6.O
            if (r0 == 0) goto L_0x005d
            com.tencent.assistantv2.mediadownload.c r0 = com.tencent.assistantv2.mediadownload.c.c()
            com.tencent.assistantv2.mediadownload.n r2 = r6.O
            com.tencent.assistantv2.model.d r0 = r0.a(r2)
            if (r0 == 0) goto L_0x0096
            r1 = 900(0x384, float:1.261E-42)
            com.tencent.assistantv2.st.page.STInfoV2 r1 = com.tencent.assistantv2.st.page.STInfoBuilder.buildSTInfo(r6, r1)
            com.tencent.assistantv2.st.model.StatInfo r1 = com.tencent.assistantv2.st.page.a.a(r1)
            r0.l = r1
            com.tencent.assistantv2.mediadownload.c r1 = com.tencent.assistantv2.mediadownload.c.c()
            r1.a(r0)
            goto L_0x005d
        L_0x0096:
            r6.y()
            com.tencent.assistant.module.fs r0 = r6.y
            com.tencent.assistantv2.mediadownload.n r2 = r6.O
            java.lang.String r2 = r2.a()
            com.tencent.assistantv2.mediadownload.n r3 = r6.O
            java.lang.String r3 = r3.b()
            r0.a(r2, r3, r1)
            goto L_0x005d
        L_0x00ab:
            r0 = r1
            goto L_0x003c
        L_0x00ad:
            r2 = r0
            goto L_0x002b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.activity.DownloadActivity.x():void");
    }

    private void y() {
        AppConst.LoadingDialogInfo loadingDialogInfo = new AppConst.LoadingDialogInfo();
        loadingDialogInfo.loadingText = "正在进行安全检查...";
        loadingDialogInfo.blockCaller = true;
        this.P = v.a(loadingDialogInfo);
        if (this.P != null) {
            this.P.setCancelable(true);
            this.P.setCanceledOnTouchOutside(false);
        }
    }

    /* access modifiers changed from: private */
    public synchronized void a(LocalApkInfo localApkInfo) {
        if (!this.X) {
            this.X = true;
            if (localApkInfo == null) {
                if (!TextUtils.isEmpty(this.G.c)) {
                    localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(this.G.c);
                } else if (this.G.f1634a > 0) {
                    localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(this.G.f1634a);
                }
            }
            if (localApkInfo != null && !TextUtils.isEmpty(localApkInfo.manifestMd5)) {
                this.G.ak = localApkInfo.manifestMd5;
                this.G.D = localApkInfo.mVersionCode;
            }
            this.T = (long) this.x.a(this.G);
            Log.i("icerao", "apk pkgname:" + this.G.c + ",manifestMD5:" + this.G.ak + ",requestid:" + this.T);
        }
    }

    private void a(DownloadInfo downloadInfo) {
        Bundle extras = getIntent().getExtras();
        if (extras != null && downloadInfo != null) {
            downloadInfo.initCallParamsFromActionBundle(extras);
        }
    }

    private void z() {
        this.u = (SecondNavigationTitleViewV5) findViewById(R.id.title_view);
        if (D()) {
            this.u.g();
        }
        if (!this.I || this.K) {
            this.u.b(getResources().getString(R.string.down_page_title));
        } else {
            this.u.a(getResources().getString(R.string.down_page_title));
        }
        this.u.b(getString(R.string.down_page_title));
        this.u.a(this);
        this.u.d();
        this.t = (RelativeLayout) findViewById(R.id.container_layout);
        this.v = (TXExpandableListView) findViewById(R.id.list_view);
        this.z = findViewById(R.id.pop_bar);
        this.A = (TextView) findViewById(R.id.group_title);
        this.B = (TextView) findViewById(R.id.group_title_num);
        this.C = (Button) findViewById(R.id.group_action);
        this.w = new DownloadInfoMultiAdapter(this.n, this.v, new ch(this));
        this.w.a(D());
        this.v.setDivider(null);
        TemporaryThreadManager.get().start(new ci(this));
        try {
            DownloadListFooterView downloadListFooterView = new DownloadListFooterView(this.n, TXScrollViewBase.ScrollDirection.SCROLL_DIRECTION_VERTICAL, TXScrollViewBase.ScrollMode.NONE);
            downloadListFooterView.getRecommendAppView().setVisibility(8);
            downloadListFooterView.setHotwordCardVisibility(8);
            this.v.addFooterView(downloadListFooterView);
            this.w.a(downloadListFooterView);
        } catch (Throwable th) {
        }
        this.v.setAdapter(this.w);
        this.v.setGroupIndicator(null);
        if (D()) {
            this.v.setOnScrollListener(null);
            this.z.setVisibility(8);
        } else {
            this.v.setOnScrollListener(this.ae);
            this.z.setVisibility(0);
        }
        this.w.a(this.ae);
        this.v.setSelector(R.drawable.transparent_selector);
        this.v.setOnGroupClickListener(new cj(this));
        this.S = (NormalErrorRecommendPage) findViewById(R.id.error_page);
        this.S.setActivityPageId(STConst.ST_PAGE_DOWNLOAD_ERROR_PAGE);
        this.S.setVisibility(8);
    }

    /* access modifiers changed from: private */
    public void A() {
        View initUserTaskView = UserTaskView.initUserTaskView(this, this.Z.b);
        if (initUserTaskView != null) {
            RelativeLayout relativeLayout = (RelativeLayout) initUserTaskView.findViewById(R.id.header_layout);
            relativeLayout.setTag(R.id.tma_st_slot_tag, this.ab);
            relativeLayout.setOnClickListener(new ck(this));
            ((RelativeLayout) initUserTaskView.findViewById(R.id.close_zone)).setOnClickListener(new cl(this));
            TemporaryThreadManager.get().start(new cm(this));
            this.v.addHeaderView(initUserTaskView);
            a((Boolean) true);
        }
    }

    public void a(int i, int i2, byte b, byte b2, AppSimpleDetail appSimpleDetail) {
        if (this.P != null) {
            this.P.dismiss();
        }
        if (appSimpleDetail != null) {
            this.Q = "1";
            this.T = (long) i;
            onGetAppInfoSuccess(i, i2, appSimpleDetail);
        } else if (this.O != null) {
            b(i, i2, b, b2, this.O.a());
        }
    }

    private void b(int i, int i2, byte b, byte b2, String str) {
        if (b2 == 1) {
            cn cnVar = new cn(this, str);
            cnVar.titleRes = getString(R.string.file_down_unsafe_tips);
            cnVar.contentRes = getString(R.string.file_down_unsafe_tips_content);
            cnVar.lBtnTxtRes = getString(R.string.file_down_cancel);
            cnVar.rBtnTxtRes = getString(R.string.file_down_confirm);
            v.a(this, cnVar);
            k.a(new STInfoV2(STConst.ST_PAGE_DOWNLOAD_URL_UNSAFE, a.a(STConst.ST_DEFAULT_SLOT, 0), f(), STConst.ST_DEFAULT_SLOT, 100));
            return;
        }
        if (b == 1) {
            Toast.makeText(this, (int) R.string.file_down_outter_apk_tips, 0).show();
        } else if (b2 == 0) {
            Toast.makeText(this, (int) R.string.file_down_outter_unkown_file_tips, 0).show();
        }
        d a2 = d.a(this.O.c(), this.O.b(), str);
        a2.l = a.a(STInfoBuilder.buildSTInfo(this, 900));
        c.c().a(a2);
    }

    public void a(int i, int i2, byte b, byte b2, String str) {
        if (!TextUtils.isEmpty(str)) {
            b(i, i2, b, b2, str);
        }
    }

    public void a(int i, int i2) {
        a(i, i2, (byte) 2, (byte) 0, (AppSimpleDetail) null);
    }

    public void onGetAppInfoSuccess(int i, int i2, AppSimpleDetail appSimpleDetail) {
        boolean z2 = false;
        boolean z3 = true;
        Log.e("icerao", "get appinfo succ.seq:" + i + ",errorCode:" + i2 + ",is appmodel null?" + (appSimpleDetail == null));
        if (appSimpleDetail == null) {
            b(11);
            if (((long) i) == this.T) {
                ba.a().post(new co(this));
                return;
            }
            return;
        }
        SimpleAppModel a2 = u.a(appSimpleDetail);
        DownloadInfo a3 = DownloadProxy.a().a(a2);
        if (a3 != null && a3.needReCreateInfo(a2)) {
            DownloadProxy.a().b(a3.downloadTicket);
            a3 = null;
        }
        ba.a().post(new cb(this, i));
        STInfoV2 a4 = a(a2);
        if (a3 == null) {
            StatInfo a5 = a.a(a4);
            if (this.I) {
                a2.af = this.M;
            }
            a3 = DownloadInfo.createDownloadInfo(a2, a5);
            a3.downloadState = SimpleDownloadInfo.DownloadState.PAUSED;
            if (!b("2")) {
                z2 = true;
            }
            a3.autoInstall = z2;
            a(a3);
            DownloadProxy.a().d(a3);
            AstApp.i().j().sendMessage(AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_ADD, a3));
        } else {
            if (a3.downloadState == SimpleDownloadInfo.DownloadState.SUCC && !a3.isDownloadFileExist()) {
                z2 = true;
            }
            a(a3);
            ba.a().post(new cc(this));
            z3 = z2;
        }
        a(a3, z3, a4);
        if (((long) i) == this.T) {
            this.G = null;
        }
    }

    private void a(DownloadInfo downloadInfo, boolean z2, STInfoV2 sTInfoV2) {
        boolean z3 = true;
        if (!this.V) {
            this.V = true;
            if (b("2")) {
                z3 = false;
            }
            downloadInfo.autoInstall = z3;
            if ((b("3") && com.tencent.assistant.net.c.d()) || b("1")) {
                TemporaryThreadManager.get().start(new cd(this, sTInfoV2, z2, downloadInfo));
            }
        }
    }

    private boolean b(String str) {
        if (this.Q == null) {
            this.Q = Constants.STR_EMPTY;
        }
        if ("0".equals(str)) {
            return this.Q.length() == 0 || this.Q.contains("0");
        }
        return this.Q.contains(str);
    }

    public void onGetAppInfoFail(int i, int i2) {
        if (((long) i) == this.T) {
            ba.a().post(new ce(this));
        }
        b(12);
        Log.e("icerao", "get appinfo fail.seq:" + i + ",errorCode:" + i2);
    }

    /* access modifiers changed from: private */
    public void B() {
        long j;
        int pointToPosition = this.v.pointToPosition(0, C() - 10);
        int pointToPosition2 = this.v.pointToPosition(0, 0);
        if (pointToPosition2 != -1) {
            j = this.v.getExpandableListPosition(pointToPosition2);
            int packedPositionChild = ExpandableListView.getPackedPositionChild(j);
            int packedPositionGroup = ExpandableListView.getPackedPositionGroup(j);
            if (packedPositionChild == -1) {
                View expandChildAt = this.v.getExpandChildAt(pointToPosition2 - this.v.getFirstVisiblePosition());
                if (expandChildAt == null) {
                    this.D = 100;
                } else {
                    this.D = expandChildAt.getHeight();
                }
            }
            if (F > 0) {
                this.E = packedPositionGroup;
                String[] b = this.w.b(packedPositionGroup);
                if (b != null) {
                    this.A.setText(b[0]);
                    this.B.setText(" " + b[1]);
                }
                if (packedPositionGroup == 1) {
                    this.C.setText(this.n.getResources().getString(R.string.down_page_group_clear_text));
                    this.C.setVisibility(0);
                    this.C.setOnClickListener(this.w.b);
                } else {
                    this.C.setVisibility(8);
                }
                if (this.E != packedPositionGroup || !this.v.isGroupExpanded(packedPositionGroup)) {
                    this.z.setVisibility(8);
                } else {
                    this.z.setVisibility(0);
                }
            }
            if (F == 0) {
                this.z.setVisibility(8);
            }
        } else {
            j = 0;
        }
        if (this.E != -1) {
            if (j == 0 && pointToPosition == 0) {
                this.z.setVisibility(8);
                return;
            }
            int C2 = C();
            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) this.z.getLayoutParams();
            marginLayoutParams.topMargin = (-(this.D - C2)) - 5;
            this.z.setLayoutParams(marginLayoutParams);
        }
    }

    private int C() {
        int i = this.D;
        int pointToPosition = this.v.pointToPosition(0, this.D);
        if (pointToPosition == -1 || ExpandableListView.getPackedPositionGroup(this.v.getExpandableListPosition(pointToPosition)) == this.E) {
            return i;
        }
        View expandChildAt = this.v.getExpandChildAt(pointToPosition - this.v.getFirstVisiblePosition());
        if (expandChildAt != null) {
            return expandChildAt.getTop();
        }
        return 100;
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING /*1003*/:
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE /*1009*/:
            case EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOADING /*1148*/:
            case EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_ADD /*1152*/:
            case EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_DELETE /*1153*/:
            case EventDispatcherEnum.UI_EVENT_FILE_DOWNLOADING /*1157*/:
            case EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_ADD /*1161*/:
            case EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_DELETE /*1162*/:
            case EventDispatcherEnum.UI_EVENT_QREADER_DELETE /*1170*/:
                w();
                return;
            case EventDispatcherEnum.UI_EVENT_DOWNLOADLIST_READY /*1038*/:
                if (!this.U) {
                    v();
                    return;
                }
                return;
            default:
                return;
        }
    }

    private boolean D() {
        if (!this.I || this.J || this.K) {
            return false;
        }
        return true;
    }

    private void E() {
        if (this.I && this.L != 0 && this.v != null && this.w != null && this.K && this.L < this.w.getGroupCount()) {
            int i = 0;
            for (int i2 = 0; i2 < this.L; i2++) {
                i += this.w.getChildrenCount(i2);
            }
            this.v.setSelection(i - 1);
            this.af.sendEmptyMessageDelayed(1, 100);
        }
    }

    private void b(int i) {
        if (!TextUtils.isEmpty(this.p.f3357a)) {
            String str = Constants.STR_EMPTY;
            if (this.G != null) {
                str = h.a(this.p.c, this.p.d, this.G.ac, this.p.f3357a, this.p.b, String.valueOf(this.G.f1634a), this.p.f, this.G.c, String.valueOf(this.G.g));
            }
            k.a("StatIpcToDownload", this.p.e, str + "_" + i);
        }
    }
}
