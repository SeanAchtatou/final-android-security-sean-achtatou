package com.tencent.assistant.activity;

import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.download.SimpleDownloadInfo;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.manager.av;
import com.tencent.assistant.model.j;
import com.tencent.assistant.module.u;
import com.tencent.assistant.plugin.PluginInfo;
import com.tencent.assistant.plugin.mgr.c;
import com.tencent.assistant.plugin.mgr.d;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.bt;

/* compiled from: ProGuard */
public class PluginDownActivity extends BaseActivity implements UIEventListener {
    private j A;
    private String B;
    private AstApp C = AstApp.i();
    private TextView n;
    private TextView t;
    private RelativeLayout u;
    private TextView v;
    private TextView w;
    private ProgressBar x;
    private Button y;
    private Button z;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        overridePendingTransition(R.anim.activity_fade_in, R.anim.activity_fade_out);
        setContentView((int) R.layout.activity_plugin_down);
        this.B = getIntent().getStringExtra("package_name");
        if (TextUtils.isEmpty(this.B)) {
            finish();
            return;
        }
        PluginInfo a2 = d.b().a(this.B);
        this.A = av.a().a(this.B);
        if (a2 != null && (this.A == null || this.A.d <= a2.getVersion() || c.a(this.B) >= 0)) {
            AstApp.i().j().sendMessage(AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_PLUGIN_DIALOG_NORMAL_INSTALLED, this.B));
            finish();
        } else if (this.A == null) {
            Toast.makeText(this, (int) R.string.plugin_not_cofig, 0).show();
            finish();
        } else {
            this.C.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLOAD_SUCC, this);
            this.C.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLOAD_DOWNLOADING, this);
            this.C.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLOAD_QUEUING, this);
            this.C.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLOAD_PAUSE, this);
            this.C.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLOAD_FAIL, this);
            this.C.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLOAD_DELETE, this);
            this.C.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_FAIL, this);
            this.C.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_SUCC, this);
            this.C.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_PRELOAD_FAIL, this);
            this.C.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_PRELOAD_SUCC, this);
            this.C.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_FINAL_FAIL, this);
            i();
        }
    }

    private void i() {
        this.n = (TextView) findViewById(R.id.title);
        this.t = (TextView) findViewById(R.id.plugin_desc);
        this.u = (RelativeLayout) findViewById(R.id.downding_zone);
        this.v = (TextView) findViewById(R.id.speed);
        this.w = (TextView) findViewById(R.id.down_size);
        this.x = (ProgressBar) findViewById(R.id.down_progress);
        this.y = (Button) findViewById(R.id.cancel_btn);
        this.z = (Button) findViewById(R.id.down_btn);
        this.y.setOnClickListener(new ev(this));
        DownloadInfo j = j();
        if (j != null) {
            a(j);
            return;
        }
        Toast.makeText(this, (int) R.string.plugin_not_download, 0).show();
        finish();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.activity.PluginDownActivity.a(android.widget.Button, boolean):void
     arg types: [android.widget.Button, int]
     candidates:
      com.tencent.assistant.activity.PluginDownActivity.a(com.tencent.assistant.download.DownloadInfo, com.tencent.assistant.model.j):void
      com.tencent.assistant.activity.BaseActivity.a(java.lang.String, java.lang.String):void
      com.tencent.assistant.activity.PluginDownActivity.a(android.widget.Button, boolean):void */
    private void a(DownloadInfo downloadInfo) {
        AppConst.AppState appState;
        boolean z2 = downloadInfo.uiType == SimpleDownloadInfo.UIType.PLUGIN_PREDOWNLOAD;
        j a2 = av.a().a(this.B);
        if (d.b().a(this.B) != null) {
            this.t.setText(getResources().getString(R.string.plugin_download_update_dialog_default_desc));
        } else {
            this.t.setText(getResources().getString(R.string.plugin_download_dialog_default_desc));
        }
        AppConst.AppState a3 = u.a(downloadInfo, a2, (PluginInfo) null);
        if (z2) {
            appState = AppConst.AppState.DOWNLOAD;
        } else {
            appState = a3;
        }
        if (appState == AppConst.AppState.DOWNLOADING || appState == AppConst.AppState.DOWNLOADED) {
            this.z.setText(getResources().getString(R.string.plugin_down_dialog_downloading));
            a(this.z, false);
            b(downloadInfo);
        } else if (appState == AppConst.AppState.QUEUING) {
            this.z.setText(getResources().getString(R.string.plugin_down_dialog_wait));
            a(this.z, false);
            b(downloadInfo);
        } else if (appState == AppConst.AppState.FAIL || appState == AppConst.AppState.PAUSED) {
            this.z.setText(getResources().getString(R.string.plugin_down_dialog_redownload));
            a(this.z, true);
            a(downloadInfo, a2);
            b(downloadInfo);
        } else if (appState == AppConst.AppState.DOWNLOAD) {
            this.t.setVisibility(0);
            this.u.setVisibility(8);
            this.z.setText(String.format(getResources().getString(R.string.plugin_down_dialog_size), bt.a(a2.j)));
            this.z.setEnabled(true);
            a(downloadInfo, a2);
        }
    }

    private void a(Button button, boolean z2) {
        if (z2) {
            button.setEnabled(true);
            button.setTextColor(getResources().getColor(R.color.state_normal));
            button.setBackgroundDrawable(getResources().getDrawable(R.drawable.common_blue_btn_selector));
            return;
        }
        button.setEnabled(false);
        button.setTextColor(getResources().getColor(R.color.plugin_downloading_disabled_color));
        button.setBackgroundDrawable(getResources().getDrawable(R.drawable.common_btn_big_disabled));
    }

    private void a(DownloadInfo downloadInfo, j jVar) {
        this.z.setOnClickListener(new ew(this, downloadInfo, jVar));
    }

    private void b(DownloadInfo downloadInfo) {
        this.t.setVisibility(8);
        this.u.setVisibility(0);
        this.v.setText(downloadInfo.response.c);
        this.x.setProgress(downloadInfo.getProgress());
        this.x.setSecondaryProgress(0);
        this.w.setText(bt.a(downloadInfo.response.f1263a) + "/" + bt.a(downloadInfo.response.b));
    }

    private DownloadInfo j() {
        if (this.A == null) {
            return null;
        }
        DownloadInfo b = av.a().b(this.A);
        if (b == null) {
            return av.a().a(this.A, SimpleDownloadInfo.UIType.PLUGIN_PREDOWNLOAD);
        }
        return b;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.C.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLOAD_SUCC, this);
        this.C.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLOAD_DOWNLOADING, this);
        this.C.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLOAD_QUEUING, this);
        this.C.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLOAD_PAUSE, this);
        this.C.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLOAD_FAIL, this);
        this.C.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLOAD_DELETE, this);
        this.C.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_FAIL, this);
        this.C.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_SUCC, this);
        this.C.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_PRELOAD_FAIL, this);
        this.C.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_PRELOAD_SUCC, this);
        this.C.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_FINAL_FAIL, this);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (isFinishing()) {
            return true;
        }
        finish();
        return true;
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_SUCC /*1105*/:
                PluginInfo a2 = d.b().a(this.B);
                if (a2 != null) {
                    TemporaryThreadManager.get().start(new ey(this, a2));
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_FAIL /*1106*/:
            case EventDispatcherEnum.UI_EVENT_PLUGIN_PRELOAD_FAIL /*1108*/:
                String str = (String) message.obj;
                if (str != null && str.equals(this.B)) {
                    Toast.makeText(this, (int) R.string.plugin_install_fail, 0).show();
                    if (!isFinishing()) {
                        finish();
                        return;
                    }
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_PLUGIN_PRELOAD_SUCC /*1107*/:
                String str2 = (String) message.obj;
                if (str2 != null && str2.equals(this.B)) {
                    AstApp.i().j().sendMessage(AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_PLUGIN_DIALOG_NORMAL_INSTALLED, this.B));
                    finish();
                    return;
                }
                return;
            default:
                if ((message.obj instanceof String) && ((String) message.obj).equals(this.A.b)) {
                    a(j());
                    return;
                }
                return;
        }
    }

    public void finish() {
        super.finish();
    }

    public void overridePendingTransition(int i, int i2) {
    }

    public boolean n() {
        return false;
    }
}
