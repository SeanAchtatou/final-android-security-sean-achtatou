package com.tencent.assistant.activity;

import android.view.View;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.manager.av;
import com.tencent.assistant.model.j;
import com.tencent.assistant.module.u;
import com.tencent.assistant.plugin.PluginInfo;
import com.tencent.assistant.utils.TemporaryThreadManager;

/* compiled from: ProGuard */
class ew implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DownloadInfo f565a;
    final /* synthetic */ j b;
    final /* synthetic */ PluginDownActivity c;

    ew(PluginDownActivity pluginDownActivity, DownloadInfo downloadInfo, j jVar) {
        this.c = pluginDownActivity;
        this.f565a = downloadInfo;
        this.b = jVar;
    }

    public void onClick(View view) {
        AppConst.AppState a2 = u.a(this.f565a, this.b, (PluginInfo) null);
        if (a2 == AppConst.AppState.DOWNLOADED) {
            String downloadingPath = this.f565a.getDownloadingPath();
            if (this.b != null) {
                TemporaryThreadManager.get().start(new ex(this, downloadingPath, this.b.c));
            }
        } else if (a2 != AppConst.AppState.DOWNLOADING && a2 != AppConst.AppState.QUEUING) {
            av.a().c(this.b);
        }
    }
}
