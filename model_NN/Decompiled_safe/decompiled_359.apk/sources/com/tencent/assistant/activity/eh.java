package com.tencent.assistant.activity;

import android.os.Message;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.utils.XLog;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
class eh extends ApkResCallback.Stub {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ InstalledAppManagerActivity f550a;

    private eh(InstalledAppManagerActivity installedAppManagerActivity) {
        this.f550a = installedAppManagerActivity;
    }

    /* synthetic */ eh(InstalledAppManagerActivity installedAppManagerActivity, dy dyVar) {
        this(installedAppManagerActivity);
    }

    public void onInstalledApkDataChanged(LocalApkInfo localApkInfo, int i) {
        if (localApkInfo != null) {
            Message obtainMessage = this.f550a.V.obtainMessage();
            obtainMessage.obj = localApkInfo;
            obtainMessage.arg1 = i;
            obtainMessage.what = 10702;
            this.f550a.V.removeMessages(10702);
            this.f550a.V.sendMessage(obtainMessage);
        }
    }

    public void onLoadInstalledApkSuccess(List<LocalApkInfo> list) {
        if (list != null) {
            int unused = this.f550a.M = list.size();
            int unused2 = this.f550a.N = 0;
            for (LocalApkInfo localApkInfo : list) {
                if (ApkResourceManager.getInstance().getPkgSize(localApkInfo.mPackageName) > 0) {
                    InstalledAppManagerActivity.n(this.f550a);
                }
            }
            Message obtainMessage = this.f550a.V.obtainMessage();
            obtainMessage.obj = list;
            obtainMessage.what = 10701;
            this.f550a.V.removeMessages(10701);
            this.f550a.V.sendMessage(obtainMessage);
        }
    }

    public void onLoadInstalledApkExtraSuccess(List<LocalApkInfo> list) {
        if (this.f550a.V != null) {
            Message obtainMessage = this.f550a.V.obtainMessage();
            obtainMessage.obj = list;
            obtainMessage.what = 10701;
            this.f550a.V.removeMessages(10701);
            this.f550a.V.sendMessage(obtainMessage);
        }
    }

    public void onInstalledAppTimeUpdate(List<LocalApkInfo> list) {
        XLog.d("miles", "onInstalledAppTimeUpdate. size = " + list.size());
        if (this.f550a.V != null) {
            Message obtainMessage = this.f550a.V.obtainMessage();
            obtainMessage.obj = list;
            obtainMessage.what = 10701;
            this.f550a.V.removeMessages(10701);
            this.f550a.V.sendMessage(obtainMessage);
        }
    }

    public void onLoadInstalledApkFail(int i, String str) {
        this.f550a.V.sendEmptyMessage(10703);
    }

    public void onGetPkgSizeFinish(LocalApkInfo localApkInfo) {
        InstalledAppManagerActivity.n(this.f550a);
        Iterator it = this.f550a.t.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            LocalApkInfo localApkInfo2 = (LocalApkInfo) it.next();
            if (localApkInfo.mPackageName.equals(localApkInfo2.mPackageName)) {
                localApkInfo2.occupySize = localApkInfo.occupySize;
                break;
            }
        }
        if (this.f550a.M == this.f550a.N) {
            XLog.d("miles", "InstalledAppManagerActivity >> totalAppCount = sizeAlreadyGetAppCount =" + this.f550a.N);
            Message obtainMessage = this.f550a.V.obtainMessage();
            obtainMessage.obj = localApkInfo;
            obtainMessage.arg1 = 4;
            obtainMessage.what = 10702;
            this.f550a.V.removeMessages(10702);
            this.f550a.V.sendMessage(obtainMessage);
        }
    }
}
