package com.tencent.assistant.activity.debug;

import android.view.View;
import android.widget.AdapterView;
import com.tencent.assistant.Global;
import com.tencent.assistant.m;

/* compiled from: ProGuard */
class aw implements AdapterView.OnItemSelectedListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ServerAdressSettingActivity f497a;

    aw(ServerAdressSettingActivity serverAdressSettingActivity) {
        this.f497a = serverAdressSettingActivity;
    }

    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long j) {
        if (i != this.f497a.f) {
            m.a().a(this.f497a.c = this.f497a.e + i);
            this.f497a.f473a.setSelection(this.f497a.e);
            this.f497a.d.setText("当前：" + Global.getServerAddressName());
            this.f497a.c();
        }
    }

    public void onNothingSelected(AdapterView<?> adapterView) {
    }
}
