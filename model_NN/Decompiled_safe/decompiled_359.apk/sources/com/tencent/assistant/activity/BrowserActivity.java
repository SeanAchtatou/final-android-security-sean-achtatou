package com.tencent.assistant.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.ValueCallback;
import android.webkit.WebView;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.jg.EType;
import com.jg.JgClassChecked;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.a.a;
import com.tencent.assistant.component.WebViewFooter;
import com.tencent.assistant.component.appdetail.AppBarTabView;
import com.tencent.assistant.component.appdetail.AppdetailFloatingDialog;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.component.txscrollview.TXTabBarLayout;
import com.tencent.assistant.g.p;
import com.tencent.assistant.js.l;
import com.tencent.assistant.login.d;
import com.tencent.assistant.manager.cq;
import com.tencent.assistant.model.ShareBaseModel;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.protocol.jce.ActionUrl;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ba;
import com.tencent.assistant.utils.bg;
import com.tencent.assistant.utils.ct;
import com.tencent.assistant.utils.dd;
import com.tencent.assistant.utils.df;
import com.tencent.assistant.utils.dh;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import com.tencent.assistantv2.component.TxWebViewContainer;
import com.tencent.assistantv2.component.dz;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import java.io.Serializable;

@JgClassChecked(author = 71024, fComment = "确认已进行安全校验", lastDate = "20140527", level = 1, reviewer = 89182, vComment = {EType.JSEXECUTECHECK, EType.ACTIVITYCHECK})
/* compiled from: ProGuard */
public class BrowserActivity extends ShareBaseActivity {
    private static String G;
    private static String H;
    private static long I;
    /* access modifiers changed from: private */
    public static String J = "0";
    private boolean A = false;
    /* access modifiers changed from: private */
    public String B;
    /* access modifiers changed from: private */
    public String C = Constants.STR_EMPTY;
    /* access modifiers changed from: private */
    public String D = "NONE";
    private AppdetailFloatingDialog E;
    private boolean F = false;
    private String K = "/qqdownloader/3";
    private boolean L = false;
    private ShareBaseModel M;
    private String N = ".*mq.wsq.qq.com.*|.*m.wsq.qq.com.*";
    private String O = "1";
    private String P = "0";
    private int Q = 0;
    private boolean R = false;
    private boolean S = false;
    private WebViewFooter.IWebViewFooterListener T = new WebViewFooter.IWebViewFooterListener() {
        public void onFresh() {
            BrowserActivity.this.t.i();
        }

        public void onForward() {
            BrowserActivity.this.t.h();
        }

        public void onBack() {
            BrowserActivity.this.t.g();
        }
    };
    private AppdetailFloatingDialog.IOnFloatViewListener U = new AppdetailFloatingDialog.IOnFloatViewListener() {
        public void showPermission() {
        }

        public void showReport() {
        }

        public void shareToQQ() {
            BrowserActivity.this.w();
        }

        public void shareToQZ() {
            BrowserActivity.this.x();
        }

        public void shareToWX() {
            BrowserActivity.this.y();
        }

        public void shareToTimeLine() {
            BrowserActivity.this.z();
        }

        public void doCollectioon() {
        }
    };
    protected Context n;
    protected TxWebViewContainer t;
    protected SecondNavigationTitleViewV5 u;
    public WebViewFooter v;
    public View.OnClickListener w = new View.OnClickListener() {
        public void onClick(View view) {
            if ("1".equals(BrowserActivity.J)) {
                if (BrowserActivity.this.t == null || !BrowserActivity.this.t.e()) {
                    BrowserActivity.this.finish();
                } else {
                    BrowserActivity.this.t.g();
                }
            } else if (!"2".equals(BrowserActivity.J)) {
                BrowserActivity.this.finish();
            } else if (BrowserActivity.this.t != null) {
                BrowserActivity.this.t.a("javascript:if(!!window.backClick){backClick()};void(0);");
            } else {
                BrowserActivity.this.finish();
            }
        }
    };
    public View.OnClickListener x = new View.OnClickListener() {
        public void onClick(View view) {
            try {
                String b = d.a().l() ? a.b() : a.a();
                Intent intent = new Intent(BrowserActivity.this, BrowserActivity.class);
                intent.putExtra("com.tencent.assistant.BROWSER_URL", b);
                BrowserActivity.this.n.startActivity(intent);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
    private RelativeLayout y;
    private boolean z = false;

    @JgClassChecked(author = 71024, fComment = "确认已进行安全校验", lastDate = "20140527", level = 1, reviewer = 89182, vComment = {EType.JSEXECUTECHECK})
    /* compiled from: ProGuard */
    public interface WebChromeClientListener {
        Activity getActivity();

        void onProgressChanged(WebView webView, int i);

        void onReceivedTitle(WebView webView, String str);
    }

    static /* synthetic */ String a(BrowserActivity browserActivity, Object obj) {
        String str = browserActivity.B + obj;
        browserActivity.B = str;
        return str;
    }

    public int f() {
        int i = STConst.ST_PAGE_FROM_WEBVIEW;
        if (this.B == null) {
            return STConst.ST_PAGE_FROM_WEBVIEW;
        }
        if (this.B.contains(a.a()) || this.B.contains(a.b())) {
            return STConst.ST_PAGE_MY_APPBAR;
        }
        if (this.B != null && this.B.matches(this.N)) {
            i = STConst.ST_PAGE_ASSISTANT_APP_LOCAL;
        }
        if (this.L) {
            return STConst.ST_PAGE_ASSISTANT_APP_EXTERNAL;
        }
        return i;
    }

    public boolean g() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        getWindow().requestFeature(1);
        dh.a();
        this.n = this;
        try {
            setContentView((int) R.layout.browser_layout);
            b(getIntent());
        } catch (Throwable th) {
            this.S = true;
            cq.a().b();
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        b(intent);
    }

    private void b(Intent intent) {
        if (intent.hasExtra("com.tencent.assistant.BROWSER_URL")) {
            String a2 = bg.a(intent, "com.tencent.assistant.BROWSER_URL");
            if (!TextUtils.isEmpty(a2) && a2.equals(this.B)) {
                return;
            }
        }
        this.C = Constants.STR_EMPTY;
        try {
            c(intent);
        } catch (Throwable th) {
        }
        G();
        D();
        dz dzVar = new dz();
        dzVar.f3190a = this.K;
        dzVar.b = this.Q;
        if (this.B != null && this.B.matches(this.N)) {
            dzVar.d = 2;
        }
        this.t.a(dzVar);
        J();
    }

    private void c(Intent intent) {
        String str;
        ActionUrl actionUrl;
        if (intent.hasExtra("com.tencent.assistant.BROWSER_URL")) {
            this.B = bg.a(intent, "com.tencent.assistant.BROWSER_URL");
            if (!TextUtils.isEmpty(this.B)) {
                this.F = this.B.contains(".swf");
                this.L = this.B.contains("qOpenAppId") || this.B.contains("qPackageName");
                if (this.B.matches(this.N)) {
                    this.z = true;
                }
            }
        }
        Bundle a2 = bg.a(getIntent());
        if (a2 == null || !a2.containsKey("com.tencent.assistant.activity.BROWSER_TYPE")) {
            str = null;
        } else {
            String string = a2.getString("com.tencent.assistant.activity.BROWSER_TYPE");
            if ("0".equals(string)) {
                this.z = true;
            }
            str = string;
        }
        if (a2 != null && a2.containsKey("com.tencent.assistant.activity.BROWSER_ACCELERATE")) {
            String string2 = a2.getString("com.tencent.assistant.activity.BROWSER_ACCELERATE");
            if ("0".equals(string2)) {
                this.Q = 2;
            }
            if ("1".equals(string2)) {
                this.Q = 1;
            }
        }
        if (a2 != null && a2.containsKey("suport.zoom")) {
            String string3 = a2.getString("suport.zoom");
            if ("0".equals(string3)) {
                this.R = false;
            }
            if ("1".equals(string3)) {
                this.R = true;
            }
        }
        if (a2 != null && a2.containsKey("com.tencent.assistant.activity.PKGNAME_APPBAR")) {
            this.C = a2.getString("com.tencent.assistant.activity.PKGNAME_APPBAR");
        }
        if (a2 != null && a2.containsKey("goback")) {
            J = a2.getString("goback");
        }
        if (intent.hasExtra("com.tencent.assistant.ACTION_URL")) {
            Serializable b = bg.b(intent, "com.tencent.assistant.ACTION_URL");
            if ((b instanceof ActionUrl) && (actionUrl = (ActionUrl) b) != null) {
                int b2 = actionUrl.b();
                this.D = a(b2, this.B);
                if (str == null) {
                    this.z = a(b2, 1);
                }
                this.A = a(b2, 2);
                return;
            }
            return;
        }
        this.D = a(0, this.B);
    }

    private void D() {
        if (this.L) {
            this.K = "/qqdownloader/3/external";
        } else {
            this.K = "/qqdownloader/3";
        }
        if (!TextUtils.isEmpty(this.B)) {
            Uri parse = Uri.parse(this.B);
            G = TextUtils.isEmpty(G) ? parse.getQueryParameter("qOpenId") : G;
            H = TextUtils.isEmpty(H) ? parse.getQueryParameter("qAccessToken") : H;
            I = I == 0 ? ct.c(parse.getQueryParameter("qOpenAppId")) : I;
            this.q = parse.getQueryParameter("qPackageName");
            this.B = this.B.replaceAll("((qOpenAppId)|(qPackageName)|(qAccessToken)|(qOpenId))=[^& ]*&*", Constants.STR_EMPTY);
            STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this, 100);
            buildSTInfo.pushInfo = getIntent().getStringExtra("preActivityPushInfo");
            k.a(buildSTInfo);
        }
    }

    private void F() {
        l lVar = new l();
        lVar.f1359a = G;
        lVar.b = H;
        lVar.c = I;
        com.tencent.assistant.js.k.a(this.n, this.B, lVar);
    }

    private String a(int i, String str) {
        String str2 = a(i, 0) ? AppBarTabView.AUTH_TYPE_ALL : "NONE";
        if (!str2.equals("NONE") || TextUtils.isEmpty(str) || !com.tencent.assistant.js.k.a(Uri.parse(str).getHost())) {
            return str2;
        }
        return AppBarTabView.AUTH_TYPE_ALL;
    }

    private boolean a(int i, int i2) {
        return ((i >>> i2) & 1) == 1;
    }

    @SuppressLint({"NewApi"})
    private void G() {
        this.t = (TxWebViewContainer) findViewById(R.id.webviewcontainer);
        this.y = (RelativeLayout) findViewById(R.id.browser_footer_layout);
        H();
        j();
        b(false);
    }

    private void H() {
        this.u = (SecondNavigationTitleViewV5) findViewById(R.id.browser_header_view);
        this.u.a(this);
        if (this.F || this.A) {
            this.u.setVisibility(8);
        }
        this.u.i();
        if (this.L) {
            this.u.a((int) R.drawable.bar_white);
            this.u.g();
        }
        this.u.a(this.U);
        this.u.b(this.x);
        this.u.e(this.w);
        this.u.b(" ");
    }

    public void b(boolean z2) {
        if (this.u != null) {
            this.u.a(z2);
            this.u.a(new OnTMAParamClickListener() {
                public void onTMAClick(View view) {
                    if (BrowserActivity.this.t != null) {
                        BrowserActivity.this.t.l();
                    }
                }

                public STInfoV2 getStInfo() {
                    STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(BrowserActivity.this.n, 200);
                    if (buildSTInfo != null) {
                        buildSTInfo.slotId = "03_001";
                    }
                    return buildSTInfo;
                }
            });
        }
    }

    public void i() {
        if (this.u != null) {
            this.E = this.u.o();
            if (this.E != null && !isFinishing()) {
                try {
                    this.E.show();
                    this.E.hideLayoutBesidesShare();
                    this.E.refreshShareState();
                    Window window = this.E.getWindow();
                    WindowManager.LayoutParams attributes = window.getAttributes();
                    attributes.gravity = 53;
                    if (this.u.p() != null) {
                        attributes.y = getResources().getDimensionPixelSize(R.dimen.app_detail_float_bar_content_height) - getResources().getDimensionPixelSize(R.dimen.app_detail_float_share_yyb_dialog_y);
                    }
                    this.E.getWindow().getWindowManager();
                    window.setAttributes(attributes);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void setTitle(CharSequence charSequence) {
        if (this.u != null && !TextUtils.isEmpty(charSequence)) {
            this.u.b(charSequence.toString());
        }
    }

    public void j() {
        this.v = (WebViewFooter) findViewById(R.id.browser_footer_view);
        this.v.setWebViewFooterListener(this.T);
        I();
        c(this.z);
    }

    public void c(boolean z2) {
        if (z2 || this.F) {
            this.v.setVisibility(8);
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.t.getLayoutParams();
            if (layoutParams == null) {
                layoutParams = new RelativeLayout.LayoutParams(-1, -1);
            }
            layoutParams.bottomMargin = 0;
            this.t.setLayoutParams(layoutParams);
        }
        if (!z2 && !this.F) {
            this.v.setVisibility(0);
            RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) this.t.getLayoutParams();
            if (layoutParams2 == null) {
                layoutParams2 = new RelativeLayout.LayoutParams(-1, -1);
            }
            layoutParams2.bottomMargin = df.a(getApplicationContext(), 50.0f);
            this.t.setLayoutParams(layoutParams2);
        }
    }

    private void I() {
        if (this.t != null) {
            if (this.t.e()) {
                this.v.setBackEnable(true);
            } else {
                this.v.setBackEnable(false);
            }
            if (this.t.f()) {
                this.v.setForwardEnable(true);
            } else {
                this.v.setForwardEnable(false);
            }
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4 || this.r || !this.t.e()) {
            return super.onKeyDown(i, keyEvent);
        }
        this.t.g();
        return true;
    }

    private void J() {
        this.t.a(0);
        ba.a().post(new Runnable() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.tencent.assistant.activity.BrowserActivity.a(com.tencent.assistant.activity.BrowserActivity, java.lang.Object):java.lang.String
             arg types: [com.tencent.assistant.activity.BrowserActivity, java.lang.String]
             candidates:
              com.tencent.assistant.activity.BrowserActivity.a(int, java.lang.String):java.lang.String
              com.tencent.assistant.activity.BrowserActivity.a(com.tencent.assistant.activity.BrowserActivity, java.lang.String):java.lang.String
              com.tencent.assistant.activity.BrowserActivity.a(int, int):boolean
              com.tencent.assistant.activity.BaseActivity.a(java.lang.String, java.lang.String):void
              com.tencent.assistant.activity.BrowserActivity.a(com.tencent.assistant.activity.BrowserActivity, java.lang.Object):java.lang.String */
            public void run() {
                if (!TextUtils.isEmpty(BrowserActivity.this.B)) {
                    com.tencent.assistant.js.k.a(BrowserActivity.this, BrowserActivity.this.B, BrowserActivity.this.D);
                    if (BrowserActivity.this.t != null) {
                        BrowserActivity.this.t.m();
                    }
                    if (BrowserActivity.this.t != null) {
                        if (BrowserActivity.this.C != null && !TextUtils.isEmpty(BrowserActivity.this.C)) {
                            BrowserActivity.a(BrowserActivity.this, (Object) ("&pkgName=" + BrowserActivity.this.C));
                        }
                        XLog.i("xjp", "[doRefresh] ---> url : " + BrowserActivity.this.B);
                        BrowserActivity.this.t.a(BrowserActivity.this.B);
                        String unused = BrowserActivity.this.C = Constants.STR_EMPTY;
                    }
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (!this.S) {
            if (this.t != null) {
                this.t.a();
            }
            F();
            this.u.l();
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (!this.S) {
            if (this.t != null) {
                this.t.b();
            }
            this.u.m();
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.S) {
            super.onDestroy();
            return;
        }
        if (this.t != null) {
            this.t.c();
        }
        this.x = null;
        super.onDestroy();
    }

    public void onDetachedFromWindow() {
        if (this.t != null) {
            this.t.d();
        }
        super.onDetachedFromWindow();
    }

    public void d(boolean z2) {
        if (this.u != null) {
            if (z2) {
                this.u.setVisibility(0);
            } else {
                this.u.setVisibility(8);
            }
        }
    }

    public void v() {
        try {
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(this.B));
            intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, f());
            startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 100 && this.t != null) {
            ValueCallback<Uri> j = this.t.j();
            if (j != null) {
                j.onReceiveValue((intent == null || i2 != -1) ? null : intent.getData());
                this.t.k();
            } else {
                return;
            }
        }
        if (i != 101) {
            return;
        }
        if (intent != null) {
            Uri data = intent.getData();
            if (data != null) {
                String a2 = dd.a(this.n, data);
                if (a2 == null) {
                    if (this.t != null) {
                        this.t.a(this.P, Constants.STR_EMPTY);
                    }
                } else if (this.t != null) {
                    this.t.a(this.O, a2);
                }
            }
        } else if (this.t != null) {
            this.t.a(this.P, Constants.STR_EMPTY);
        }
    }

    public void a(ShareBaseModel shareBaseModel) {
        this.M = shareBaseModel;
    }

    public void w() {
        E().b(this, this.M);
    }

    public void x() {
        E().a(this, this.M);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.g.e.a(android.content.Context, com.tencent.assistant.model.ShareBaseModel, boolean):void
     arg types: [com.tencent.assistant.activity.BrowserActivity, com.tencent.assistant.model.ShareBaseModel, int]
     candidates:
      com.tencent.assistant.g.e.a(android.app.Activity, com.tencent.assistant.model.ShareAppModel, boolean):void
      com.tencent.assistant.g.e.a(android.app.Activity, com.tencent.assistant.model.ShareBaseModel, boolean):void
      com.tencent.assistant.g.e.a(android.content.Context, com.tencent.assistant.model.ShareAppModel, boolean):void
      com.tencent.assistant.g.e.a(android.content.Context, com.tencent.assistant.model.ShareBaseModel, boolean):void */
    public void y() {
        E().a((Context) this, this.M, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.g.e.a(android.content.Context, com.tencent.assistant.model.ShareBaseModel, boolean):void
     arg types: [com.tencent.assistant.activity.BrowserActivity, com.tencent.assistant.model.ShareBaseModel, int]
     candidates:
      com.tencent.assistant.g.e.a(android.app.Activity, com.tencent.assistant.model.ShareAppModel, boolean):void
      com.tencent.assistant.g.e.a(android.app.Activity, com.tencent.assistant.model.ShareBaseModel, boolean):void
      com.tencent.assistant.g.e.a(android.content.Context, com.tencent.assistant.model.ShareAppModel, boolean):void
      com.tencent.assistant.g.e.a(android.content.Context, com.tencent.assistant.model.ShareBaseModel, boolean):void */
    public void z() {
        E().a((Context) this, this.M, true);
    }

    public void A() {
        p.a().a(this, this.M);
    }

    public void B() {
        Intent intent = new Intent("android.intent.action.GET_CONTENT");
        intent.setType("*/*");
        intent.addCategory("android.intent.category.OPENABLE");
        try {
            startActivityForResult(Intent.createChooser(intent, "请选择一个文件"), TXTabBarLayout.TABITEM_TIPS_TEXT_ID);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(this, "please install File Manager", 0).show();
        }
    }
}
