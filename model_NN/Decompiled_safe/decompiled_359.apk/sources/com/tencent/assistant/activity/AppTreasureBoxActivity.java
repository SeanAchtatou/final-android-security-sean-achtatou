package com.tencent.assistant.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.component.appdetail.CommentDetailTabView;
import com.tencent.assistant.component.treasurebox.AppTreasureBoxCell;
import com.tencent.assistant.component.treasurebox.AppTreasureBoxView;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.DownloadProxy;
import com.tencent.assistant.manager.as;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.s;
import com.tencent.assistant.module.u;
import com.tencent.assistant.module.wisedownload.t;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.protocol.jce.AutoDownloadCfg;
import com.tencent.assistant.protocol.jce.AutoDownloadInfo;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.st.STConstAction;
import com.tencent.assistant.utils.ba;
import com.tencent.assistant.utils.ct;
import com.tencent.assistant.utils.e;
import com.tencent.assistant.utils.installuninstall.InstallUninstallTaskBean;
import com.tencent.assistant.utils.installuninstall.p;
import com.tencent.assistant.utils.r;
import com.tencent.assistant.utils.v;
import com.tencent.assistantv2.activity.AppDetailActivityV5;
import com.tencent.assistantv2.st.b.b;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class AppTreasureBoxActivity extends BaseActivity implements AppTreasureBoxCell.AppTreasureBoxItemClickListener, UIEventListener {
    /* access modifiers changed from: private */
    public Dialog A;
    private int B = 50;
    private String C = null;
    private b D = null;
    /* access modifiers changed from: private */
    public Context n;
    private TextView t;
    private TextView u;
    private LinearLayout v;
    private AppTreasureBoxView w;
    /* access modifiers changed from: private */
    public View x;
    private List<s> y = new ArrayList();
    /* access modifiers changed from: private */
    public Dialog z;

    public int f() {
        return STConst.ST_PAGE_APP_TREASURE_BOX;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.n = this;
        getWindow().setWindowAnimations(2131427367);
        v();
    }

    private void v() {
        this.x = ((LayoutInflater) getSystemService("layout_inflater")).inflate((int) R.layout.treasure_box_inner_page, (ViewGroup) null);
        AutoDownloadCfg i = as.w().i();
        if (i != null) {
            this.C = i.k;
        }
        this.t = (TextView) this.x.findViewById(R.id.title_title);
        this.t.setText((int) R.string.app_treasure_box_title_txt);
        this.u = (TextView) this.x.findViewById(R.id.title_desc);
        if (!TextUtils.isEmpty(this.C)) {
            this.u.setText(this.C);
        }
        this.v = (LinearLayout) this.x.findViewById(R.id.title_close_area);
        this.v.setOnClickListener(new au(this));
        this.v.setOnClickListener(new av(this));
        this.w = (AppTreasureBoxView) this.x.findViewById(R.id.box);
        this.z = new Dialog(this, R.style.TreasureBoxDialog);
        this.z.setContentView(this.x);
        this.z.setCanceledOnTouchOutside(false);
        this.z.setOnKeyListener(new aw(this));
        this.z.setOnDismissListener(new ax(this));
        this.B = ((int) TypedValue.applyDimension(1, 50.0f, getResources().getDisplayMetrics())) + r.e;
        ba.a().postDelayed(new ay(this), 0);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_START, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_SUCC, this);
        AstApp.i().k().addUIEventListener(1027, this);
    }

    public void onResume() {
        y();
        this.w.refreshItemList(this.y, this);
        this.w.onGoInScreen();
        this.x.invalidate();
        super.onResume();
    }

    public void onPause() {
        this.w.onLeaveScreen();
        super.onPause();
    }

    public void onDestroy() {
        this.w.onDestroy();
        if (this.z != null) {
            this.z.dismiss();
            this.z = null;
        }
        if (this.A != null) {
            this.A.dismiss();
            this.A = null;
        }
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_START, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_SUCC, this);
        AstApp.i().k().removeUIEventListener(1027, this);
        super.onDestroy();
    }

    public void onTouchOpenedPosition(int i) {
        s sVar;
        if (i >= 0 && i < this.y.size() && (sVar = this.y.get(i)) != null) {
            sVar.b = true;
            c(i, 200);
        }
    }

    public void onCloseBtnClick() {
        if (this.y == null || this.y.isEmpty()) {
            j();
            return;
        }
        if (this.x != null) {
            this.x.setVisibility(4);
        }
        if (this.A == null) {
            this.A = i();
        }
        if (this.A == null || m.a().R() >= 2) {
            j();
            return;
        }
        this.A.setOnKeyListener(new az(this));
        if (!isFinishing()) {
            this.A.show();
        }
        m.a().Q();
        k.a(new STInfoV2(STConst.ST_PAGE_APP_TREASURE_BOX_EXIT_DIALOG, Constants.STR_EMPTY, p(), STConst.ST_DEFAULT_SLOT, 100));
    }

    public void onActionBtnClick(int i, int i2) {
        SimpleAppModel simpleAppModel;
        AppConst.AppState appState = null;
        if (i >= 0 && i < this.y.size()) {
            STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.n, 200);
            String str = STConst.ST_STATUS_DEFAULT;
            String str2 = "001";
            if (i == 1) {
                str = "03";
                str2 = "001";
            } else if (i == 2) {
                str = "04";
                str2 = "001";
            } else if (i == 3) {
                str = "05";
                str2 = "001";
            }
            buildSTInfo.slotId = a.a(str, str2);
            s sVar = this.y.get(i);
            if (sVar == null || sVar.f1672a == null) {
                simpleAppModel = null;
            } else {
                simpleAppModel = u.b(this.y.get(i).f1672a);
                appState = u.d(simpleAppModel);
            }
            if (i2 == 0) {
                d(i);
                buildSTInfo.status = a.a(appState, simpleAppModel);
            } else if (!(i2 != 1 || sVar == null || sVar.f1672a == null)) {
                buildSTInfo.actionId = a.a(appState);
                if (e.a(sVar.f1672a.f2010a, sVar.f1672a.d)) {
                    c(i);
                } else {
                    b(i);
                }
            }
            k.a(buildSTInfo);
        }
    }

    public void onAppExposure(int i) {
        if (i >= 0 && i < this.y.size()) {
            a(this.y.get(i), i);
        }
    }

    private List<s> w() {
        List<AutoDownloadInfo> a2 = t.a();
        if (a2 == null || a2.isEmpty()) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (AutoDownloadInfo autoDownloadInfo : a2) {
            s sVar = new s();
            sVar.f1672a = autoDownloadInfo;
            arrayList.add(sVar);
        }
        return arrayList;
    }

    /* access modifiers changed from: private */
    public void x() {
        if (this.y == null) {
            this.y = new ArrayList();
        }
        this.y.clear();
        List<s> w2 = w();
        if (w2 != null && !w2.isEmpty()) {
            this.y.addAll(w2);
        }
        this.w.refreshItemList(this.y, this);
    }

    private void y() {
        DownloadInfo a2;
        if (!this.y.isEmpty()) {
            ArrayList arrayList = new ArrayList();
            for (s next : this.y) {
                if (!(next == null || next.f1672a == null || (a2 = DownloadProxy.a().a(next.f1672a.f2010a, next.f1672a.d)) == null || !a2.isDownloaded())) {
                    arrayList.add(next);
                }
            }
            this.y.clear();
            if (!arrayList.isEmpty()) {
                this.y.addAll(arrayList);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.utils.installuninstall.p.a(com.tencent.assistant.download.DownloadInfo, boolean):void
     arg types: [com.tencent.assistant.download.DownloadInfo, int]
     candidates:
      com.tencent.assistant.utils.installuninstall.p.a(com.tencent.assistant.utils.installuninstall.p, com.tencent.assistant.utils.installuninstall.InstallUninstallTaskBean):com.tencent.assistant.AppConst$TwoBtnDialogInfo
      com.tencent.assistant.utils.installuninstall.p.a(int, int):void
      com.tencent.assistant.utils.installuninstall.p.a(com.tencent.assistant.utils.installuninstall.p, java.util.List):void
      com.tencent.assistant.utils.installuninstall.p.a(java.util.ArrayList<com.tencent.assistant.download.DownloadInfo>, boolean):void
      com.tencent.assistant.utils.installuninstall.p.a(com.tencent.assistant.download.DownloadInfo, boolean):void */
    private void b(int i) {
        s sVar = this.y.get(i);
        if (!(sVar == null || sVar.f1672a == null)) {
            DownloadInfo a2 = DownloadProxy.a().a(sVar.f1672a.f2010a, sVar.f1672a.d);
            if (a2 == null || !a2.isDownloaded()) {
                Toast.makeText(this, (int) R.string.app_treasure_box_download_toast_package_deleted, 1).show();
                this.y.remove(i);
                this.w.refreshItemList(this.y, this);
            } else {
                a2.statInfo.recommendId = sVar.f1672a.m;
                p.a().a(a2, false);
            }
        }
        c(i, STConstAction.ACTION_HIT_INSTALL);
    }

    private void c(int i) {
        s sVar = this.y.get(i);
        if (sVar == null || sVar.f1672a == null || TextUtils.isEmpty(sVar.f1672a.f2010a) || !e.a(sVar.f1672a.f2010a, sVar.f1672a.d)) {
            Toast.makeText(this, (int) R.string.app_treasure_box_download_toast_app_uninstalled, 1).show();
            this.y.remove(i);
            this.w.refreshItemList(this.y, this);
        } else {
            r.a(sVar.f1672a.f2010a, (Bundle) null);
        }
        c(i, STConstAction.ACTION_HIT_OPEN);
    }

    private void d(int i) {
        s sVar = this.y.get(i);
        if (!(sVar == null || sVar.f1672a == null)) {
            SimpleAppModel a2 = u.a(sVar.f1672a);
            if (a2 != null) {
                Intent intent = new Intent(this, AppDetailActivityV5.class);
                intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, f());
                intent.putExtra(CommentDetailTabView.PARAMS_SIMPLE_MODEL_INFO, a2);
                startActivity(intent);
            } else {
                return;
            }
        }
        c(i, 200);
    }

    private void z() {
        ArrayList arrayList = new ArrayList();
        if (this.y != null && !this.y.isEmpty()) {
            for (s next : this.y) {
                if (next.b && !e.a(next.f1672a.f2010a, next.f1672a.d)) {
                    arrayList.add(next.f1672a);
                }
            }
            if (!arrayList.isEmpty()) {
                com.tencent.assistant.module.update.u.a().a((List<AutoDownloadInfo>) arrayList);
            }
        }
    }

    public int a_() {
        return getResources().getDisplayMetrics().heightPixels;
    }

    /* access modifiers changed from: private */
    public void A() {
        this.x.measure(0, 0);
        WindowManager.LayoutParams attributes = this.z.getWindow().getAttributes();
        attributes.x = 0;
        attributes.y = (a_() - this.B) - this.x.getMeasuredHeight();
        attributes.dimAmount = 0.0f;
        attributes.alpha = 1.0f;
        attributes.gravity = 48;
        attributes.width = -1;
        attributes.height = -2;
        attributes.horizontalMargin = 16.0f;
        this.z.getWindow().setAttributes(attributes);
        this.z.getWindow().setWindowAnimations(2131427370);
        if (!isFinishing()) {
            this.z.show();
        }
        this.w.onGoInScreen();
    }

    public Dialog i() {
        ba baVar = new ba(this);
        baVar.hasTitle = true;
        baVar.titleRes = getResources().getString(R.string.app_treasure_box_exit_confirm_dialog_title);
        baVar.contentRes = getResources().getString(R.string.app_treasure_box_exit_confirm_dialog_content);
        baVar.lBtnTxtRes = getResources().getString(R.string.app_treasure_box_exit_confirm_dialog_left_btn_txt);
        baVar.rBtnTxtRes = getResources().getString(R.string.app_treasure_box_exit_confirm_dialog_right_btn_txt);
        baVar.blockCaller = true;
        return v.b(baVar);
    }

    public STInfoV2 a(int i, int i2) {
        return new STInfoV2(i, Constants.STR_EMPTY, STConst.ST_PAGE_APP_TREASURE_BOX, a.a("06", "001"), i2);
    }

    public void j() {
        if (this.x != null) {
            this.x.setVisibility(0);
            this.z.dismiss();
        }
        z();
        AstApp.i().j().sendMessage(AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_SHOW_TREASURE_BOX_ENTRY));
        AstApp.i().j().sendMessageDelayed(AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_DISMISS_TREASURE_BOX_ENTRY), 5000);
    }

    /* access modifiers changed from: private */
    public String b(int i, int i2) {
        return '0' + String.valueOf(i) + "_" + ct.a(i2);
    }

    private void a(s sVar, int i) {
        if (sVar != null && sVar.f1672a != null) {
            String b = b(i + 1, 2);
            int f = f();
            int p = p();
            if (this.D == null) {
                this.D = new b();
            }
            STInfoV2 exposureSTInfoV2 = this.D.getExposureSTInfoV2(sVar.f1672a.m, sVar.f1672a.j, f, p, b);
            if (exposureSTInfoV2 != null) {
                k.a(exposureSTInfoV2);
            }
        }
    }

    private void c(int i, int i2) {
        if (i >= 0 && i < this.y.size()) {
            STInfoV2 sTInfoV2 = new STInfoV2(f(), b(i + 1, 1), p(), STConst.ST_DEFAULT_SLOT, i2);
            s sVar = this.y.get(i);
            if (!(sVar == null || sVar.f1672a == null)) {
                sTInfoV2.appId = sVar.f1672a == null ? 0 : sVar.f1672a.j;
                sTInfoV2.recommendId = sVar.f1672a.m;
            }
            k.a(sTInfoV2);
        }
    }

    public void handleUIEvent(Message message) {
        DownloadInfo d;
        InstallUninstallTaskBean installUninstallTaskBean = (InstallUninstallTaskBean) message.obj;
        if (installUninstallTaskBean != null && !TextUtils.isEmpty(installUninstallTaskBean.packageName) && (d = DownloadProxy.a().d(installUninstallTaskBean.downloadTicket)) != null && this.y != null && !this.y.isEmpty()) {
            s sVar = null;
            for (s next : this.y) {
                if (next.f1672a == null || TextUtils.isEmpty(d.packageName) || !d.packageName.endsWith(next.f1672a.f2010a) || d.versionCode != next.f1672a.d) {
                    next = sVar;
                }
                sVar = next;
            }
            if (sVar != null) {
                switch (message.what) {
                    case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_START /*1025*/:
                        sVar.c = true;
                        break;
                    case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_SUCC /*1026*/:
                    case 1027:
                        sVar.c = false;
                        break;
                }
                this.w.refreshItemList(this.y, this);
            }
        }
    }
}
