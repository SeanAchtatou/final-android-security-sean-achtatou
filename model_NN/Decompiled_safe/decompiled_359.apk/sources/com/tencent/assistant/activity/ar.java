package com.tencent.assistant.activity;

import com.tencent.assistant.download.a;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.u;
import com.tencent.assistant.protocol.jce.BackupApp;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: ProGuard */
class ar implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ArrayList f406a;
    final /* synthetic */ aq b;

    ar(aq aqVar, ArrayList arrayList) {
        this.b = aqVar;
        this.f406a = arrayList;
    }

    public void run() {
        ArrayList<SimpleAppModel> c = u.c((ArrayList<BackupApp>) this.f406a);
        if (c != null && c.size() > 0) {
            Iterator<SimpleAppModel> it = c.iterator();
            while (it.hasNext()) {
                SimpleAppModel next = it.next();
                a.a().a(next, STInfoBuilder.buildDownloadSTInfo(this.b.f405a.ae, next));
            }
        }
    }
}
