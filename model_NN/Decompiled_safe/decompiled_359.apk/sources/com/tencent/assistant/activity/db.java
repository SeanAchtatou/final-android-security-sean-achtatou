package com.tencent.assistant.activity;

import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.utils.ba;

/* compiled from: ProGuard */
class db extends ApkResCallback.Stub {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ cy f469a;

    db(cy cyVar) {
        this.f469a = cyVar;
    }

    public void onInstalledApkDataChanged(LocalApkInfo localApkInfo, int i) {
        ba.a().post(new dc(this));
    }
}
