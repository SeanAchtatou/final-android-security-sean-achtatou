package com.tencent.assistant.activity;

import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.connector.ipc.a;

/* compiled from: ProGuard */
class eq implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PhotoBackupNewActivity f559a;

    eq(PhotoBackupNewActivity photoBackupNewActivity) {
        this.f559a = photoBackupNewActivity;
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cancel /*2131165467*/:
                this.f559a.d(5);
                return;
            case R.id.ok /*2131165468*/:
                this.f559a.d(5);
                a.a().i();
                return;
            case R.id.no_backup /*2131165731*/:
                this.f559a.d(2);
                this.f559a.finish();
                return;
            case R.id.connect_pc /*2131165732*/:
                this.f559a.d(2);
                this.f559a.v();
                return;
            case R.id.cancel_backup /*2131165742*/:
                this.f559a.d(1);
                if (!a.a().d()) {
                    this.f559a.finish();
                    return;
                }
                return;
            default:
                return;
        }
    }
}
