package com.tencent.assistant.activity;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;

/* compiled from: ProGuard */
class bz implements TextWatcher {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CommentReplyActivity f440a;

    bz(CommentReplyActivity commentReplyActivity) {
        this.f440a = commentReplyActivity;
    }

    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public void afterTextChanged(Editable editable) {
        String substring;
        this.f440a.n.setEnabled(!TextUtils.isEmpty(editable.toString()));
        if (this.f440a.t.getLineCount() > 6) {
            String obj = editable.toString();
            int selectionStart = this.f440a.t.getSelectionStart();
            if (selectionStart != this.f440a.t.getSelectionEnd() || selectionStart >= obj.length() || selectionStart < 1) {
                substring = obj.substring(0, obj.length() - 1);
            } else {
                substring = obj.substring(0, selectionStart - 1) + obj.substring(selectionStart);
            }
            this.f440a.t.setText(substring);
            this.f440a.t.setSelection(this.f440a.t.getText().length());
        }
    }
}
