package com.tencent.assistant.activity;

import android.content.pm.PackageInfo;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.module.callback.m;
import com.tencent.assistant.module.u;
import com.tencent.assistant.module.update.k;
import com.tencent.assistant.protocol.jce.AppSimpleDetail;
import com.tencent.assistant.utils.e;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
class hi implements m {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ UpdateListActivity f632a;

    private hi(UpdateListActivity updateListActivity) {
        this.f632a = updateListActivity;
    }

    /* synthetic */ hi(UpdateListActivity updateListActivity, hd hdVar) {
        this(updateListActivity);
    }

    public void a(int i, int i2) {
    }

    public void a(int i, int i2, List<AppSimpleDetail> list) {
        if (this.f632a.D == i && list != null && list.size() > 0) {
            ArrayList<AppSimpleDetail> arrayList = new ArrayList<>();
            List<PackageInfo> c = e.c(AstApp.i());
            for (AppSimpleDetail next : list) {
                if (next != null) {
                    int i3 = 0;
                    while (true) {
                        if (i3 >= (c == null ? 0 : c.size())) {
                            break;
                        }
                        PackageInfo packageInfo = c.get(i3);
                        if (packageInfo == null || TextUtils.isEmpty(packageInfo.packageName) || !packageInfo.packageName.equals(next.e)) {
                            i3++;
                        } else if (packageInfo.versionCode < next.g) {
                            arrayList.add(next);
                        }
                    }
                }
            }
            k.b().a(this.f632a.a(arrayList));
            ArrayList arrayList2 = new ArrayList();
            for (AppSimpleDetail appSimpleDetail : arrayList) {
                if (appSimpleDetail != null) {
                    arrayList2.add(u.a(appSimpleDetail));
                }
            }
            if (this.f632a.w != null) {
                this.f632a.w.updateAppList(arrayList2);
            }
        }
    }
}
