package com.tencent.assistant.activity;

import android.os.Handler;
import android.os.Message;
import com.tencent.assistant.component.spaceclean.RubbishItemView;
import com.tencent.assistant.model.spaceclean.a;
import com.tencent.assistant.utils.XLog;
import com.tencent.tmsecurelite.optimize.ISystemOptimize;

/* compiled from: ProGuard */
class br extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ BigFileCleanActivity f432a;

    br(BigFileCleanActivity bigFileCleanActivity) {
        this.f432a = bigFileCleanActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.activity.BigFileCleanActivity.a(com.tencent.assistant.activity.BigFileCleanActivity, long, boolean, int):void
     arg types: [com.tencent.assistant.activity.BigFileCleanActivity, long, int, int]
     candidates:
      com.tencent.assistant.activity.BaseActivity.a(android.widget.LinearLayout, int, int, int):void
      com.tencent.assistant.activity.BigFileCleanActivity.a(com.tencent.assistant.activity.BigFileCleanActivity, long, boolean, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.activity.BigFileCleanActivity.a(com.tencent.assistant.activity.BigFileCleanActivity, boolean):void
     arg types: [com.tencent.assistant.activity.BigFileCleanActivity, int]
     candidates:
      com.tencent.assistant.activity.BigFileCleanActivity.a(com.tencent.assistant.activity.BigFileCleanActivity, int):int
      com.tencent.assistant.activity.BigFileCleanActivity.a(com.tencent.assistant.activity.BigFileCleanActivity, long):long
      com.tencent.assistant.activity.BigFileCleanActivity.a(com.tencent.assistant.activity.BigFileCleanActivity, com.tencent.assistant.model.spaceclean.a):long
      com.tencent.assistant.activity.BigFileCleanActivity.a(long, boolean):void
      com.tencent.assistant.activity.BigFileCleanActivity.a(android.content.pm.PackageManager, java.lang.String):boolean
      com.tencent.assistant.activity.BaseActivity.a(java.lang.String, java.lang.String):void
      com.tencent.assistant.activity.BigFileCleanActivity.a(com.tencent.assistant.activity.BigFileCleanActivity, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.activity.BigFileCleanActivity.a(com.tencent.assistant.activity.BigFileCleanActivity, long, boolean):void
     arg types: [com.tencent.assistant.activity.BigFileCleanActivity, int, int]
     candidates:
      com.tencent.assistant.activity.BigFileCleanActivity.a(long, boolean, int):void
      com.tencent.assistant.activity.BigFileCleanActivity.a(com.tencent.assistant.activity.BigFileCleanActivity, android.content.pm.PackageManager, java.lang.String):boolean
      com.tencent.assistant.activity.BaseActivity.a(com.tencent.assistantv2.st.page.STPageInfo, java.lang.String, java.lang.String):void
      android.support.v4.app.FragmentActivity.a(java.lang.String, java.io.PrintWriter, android.view.View):void
      android.support.v4.app.FragmentActivity.a(java.lang.String, boolean, boolean):android.support.v4.app.z
      android.support.v4.app.FragmentActivity.a(android.support.v4.app.Fragment, android.content.Intent, int):void
      com.tencent.assistant.activity.BigFileCleanActivity.a(com.tencent.assistant.activity.BigFileCleanActivity, long, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.activity.BigFileCleanActivity.a(com.tencent.assistant.activity.BigFileCleanActivity, long, boolean, int):void
     arg types: [com.tencent.assistant.activity.BigFileCleanActivity, int, int, int]
     candidates:
      com.tencent.assistant.activity.BaseActivity.a(android.widget.LinearLayout, int, int, int):void
      com.tencent.assistant.activity.BigFileCleanActivity.a(com.tencent.assistant.activity.BigFileCleanActivity, long, boolean, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.activity.BigFileCleanActivity.b(com.tencent.assistant.activity.BigFileCleanActivity, boolean):boolean
     arg types: [com.tencent.assistant.activity.BigFileCleanActivity, int]
     candidates:
      com.tencent.assistant.activity.BigFileCleanActivity.b(com.tencent.assistant.activity.BigFileCleanActivity, long):long
      com.tencent.assistant.activity.BigFileCleanActivity.b(com.tencent.assistant.activity.BigFileCleanActivity, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.activity.BigFileCleanActivity.a(com.tencent.assistant.activity.BigFileCleanActivity, long, boolean):void
     arg types: [com.tencent.assistant.activity.BigFileCleanActivity, long, int]
     candidates:
      com.tencent.assistant.activity.BigFileCleanActivity.a(long, boolean, int):void
      com.tencent.assistant.activity.BigFileCleanActivity.a(com.tencent.assistant.activity.BigFileCleanActivity, android.content.pm.PackageManager, java.lang.String):boolean
      com.tencent.assistant.activity.BaseActivity.a(com.tencent.assistantv2.st.page.STPageInfo, java.lang.String, java.lang.String):void
      android.support.v4.app.FragmentActivity.a(java.lang.String, java.io.PrintWriter, android.view.View):void
      android.support.v4.app.FragmentActivity.a(java.lang.String, boolean, boolean):android.support.v4.app.z
      android.support.v4.app.FragmentActivity.a(android.support.v4.app.Fragment, android.content.Intent, int):void
      com.tencent.assistant.activity.BigFileCleanActivity.a(com.tencent.assistant.activity.BigFileCleanActivity, long, boolean):void */
    public void handleMessage(Message message) {
        super.handleMessage(message);
        switch (message.what) {
            case ISystemOptimize.T_checkVersionAsync /*24*/:
                this.f432a.a(((Long) message.obj).longValue(), true, 1);
                this.f432a.b(true);
                this.f432a.a(0);
                return;
            case ISystemOptimize.T_hasRootAsync /*25*/:
                long longValue = ((Long) message.obj).longValue();
                XLog.d("miles", "BigFileCleanActivity >> MSG_RUBBISH_SCAN_FINISHED reveived. sizeTotal = " + longValue);
                if (longValue > 0) {
                    this.f432a.a(longValue, false, 1);
                    this.f432a.b(false);
                    this.f432a.a(0);
                    return;
                }
                this.f432a.a(0L, false);
                return;
            case ISystemOptimize.T_askForRootAsync /*26*/:
                this.f432a.a(this.f432a.a((a) message.obj));
                return;
            case ISystemOptimize.T_getMemoryUsageAsync /*27*/:
                this.f432a.a(0L, false, 2);
                this.f432a.O.sendEmptyMessageDelayed(28, 1000);
                return;
            case ISystemOptimize.T_isMemoryReachWarningAsync /*28*/:
                XLog.d("miles", "BigFileCleanActivity >> MSG_RUBBISH_DELETE_ALL_FINISHED recived.");
                boolean unused = this.f432a.H = true;
                if (this.f432a.G && this.f432a.H) {
                    this.f432a.a(this.f432a.E, true);
                }
                RubbishItemView.isDeleting = false;
                return;
            case ISystemOptimize.T_getSysRubbishAsync /*29*/:
                XLog.d("miles", "BigFileCleanActivity >> MSG_ACCELERATE_SCAN_FINISHED recived.");
                if (this.f432a.G && this.f432a.H) {
                    this.f432a.a(this.f432a.E, true);
                    return;
                }
                return;
            default:
                return;
        }
    }
}
