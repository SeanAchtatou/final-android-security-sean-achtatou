package com.tencent.assistant.activity;

import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamExClickListener;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class fc extends OnTMAParamExClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ReportActivity f572a;

    fc(ReportActivity reportActivity) {
        this.f572a = reportActivity;
    }

    public void onTMAClick(View view) {
        boolean z = true;
        switch (view.getId()) {
            case R.id.submit /*2131165815*/:
                if (this.f572a.v()) {
                    this.f572a.G.a(this.f572a.D, this.f572a.E, this.f572a.x(), this.f572a.C.getText().toString());
                    this.f572a.finish();
                    return;
                }
                Toast.makeText(this.f572a, (int) R.string.report_no_content, 0).show();
                return;
            case R.id.checkbox_downlaod /*2131166282*/:
                this.f572a.u.setSelected(!this.f572a.u.isSelected());
                if (this.f572a.u.isSelected()) {
                    this.f572a.v.setEnabled(false);
                    this.f572a.y.setEnabled(false);
                    this.f572a.x.setEnabled(false);
                    this.f572a.z.setEnabled(false);
                    this.f572a.B.setEnabled(false);
                    this.f572a.v.setSelected(false);
                    this.f572a.y.setSelected(false);
                    this.f572a.x.setSelected(false);
                    this.f572a.z.setSelected(false);
                    this.f572a.B.setSelected(false);
                    this.f572a.t.setEnabled(true);
                    return;
                }
                this.f572a.v.setEnabled(true);
                this.f572a.y.setEnabled(true);
                this.f572a.x.setEnabled(true);
                this.f572a.z.setEnabled(true);
                this.f572a.B.setEnabled(true);
                return;
            case R.id.checkbox_version /*2131166283*/:
                TextView p = this.f572a.w;
                if (this.f572a.w.isSelected()) {
                    z = false;
                }
                p.setSelected(z);
                return;
            case R.id.checkbox_fee /*2131166284*/:
                TextView j = this.f572a.y;
                if (this.f572a.y.isSelected()) {
                    z = false;
                }
                j.setSelected(z);
                this.f572a.w();
                return;
            case R.id.checkbox_right /*2131166285*/:
                TextView q = this.f572a.A;
                if (this.f572a.A.isSelected()) {
                    z = false;
                }
                q.setSelected(z);
                return;
            case R.id.checkbox_install /*2131166286*/:
                this.f572a.v.setSelected(!this.f572a.v.isSelected());
                if (this.f572a.v.isSelected()) {
                    this.f572a.u.setEnabled(false);
                    this.f572a.y.setEnabled(false);
                    this.f572a.x.setEnabled(false);
                    this.f572a.z.setEnabled(false);
                    this.f572a.B.setEnabled(false);
                    this.f572a.u.setSelected(false);
                    this.f572a.y.setSelected(false);
                    this.f572a.x.setSelected(false);
                    this.f572a.z.setSelected(false);
                    this.f572a.B.setSelected(false);
                    this.f572a.t.setEnabled(true);
                    return;
                }
                this.f572a.u.setEnabled(true);
                this.f572a.y.setEnabled(true);
                this.f572a.x.setEnabled(true);
                this.f572a.z.setEnabled(true);
                this.f572a.B.setEnabled(true);
                return;
            case R.id.checkbox_plugin /*2131166287*/:
                TextView k = this.f572a.x;
                if (this.f572a.x.isSelected()) {
                    z = false;
                }
                k.setSelected(z);
                this.f572a.w();
                return;
            case R.id.checkbox_virus /*2131166288*/:
                TextView l = this.f572a.z;
                if (this.f572a.z.isSelected()) {
                    z = false;
                }
                l.setSelected(z);
                this.f572a.w();
                return;
            case R.id.checkbox_private /*2131166289*/:
                TextView m = this.f572a.B;
                if (this.f572a.B.isSelected()) {
                    z = false;
                }
                m.setSelected(z);
                this.f572a.w();
                return;
            default:
                return;
        }
    }

    public STInfoV2 getStInfo(View view) {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f572a, 200);
        if (buildSTInfo == null) {
            return null;
        }
        buildSTInfo.extraData = this.f572a.C.getText().toString();
        switch (view.getId()) {
            case R.id.submit /*2131165815*/:
                buildSTInfo.slotId = a.a("06", "010");
                break;
            case R.id.checkbox_downlaod /*2131166282*/:
                buildSTInfo.slotId = a.a("06", "001");
                buildSTInfo.status = this.f572a.u.isSelected() ? "01" : "02";
                break;
            case R.id.checkbox_version /*2131166283*/:
                buildSTInfo.slotId = a.a("06", "007");
                buildSTInfo.status = this.f572a.w.isSelected() ? "01" : "02";
                break;
            case R.id.checkbox_fee /*2131166284*/:
                buildSTInfo.slotId = a.a("06", "003");
                buildSTInfo.status = this.f572a.y.isSelected() ? "01" : "02";
                break;
            case R.id.checkbox_right /*2131166285*/:
                buildSTInfo.slotId = a.a("06", "008");
                buildSTInfo.status = this.f572a.A.isSelected() ? "01" : "02";
                break;
            case R.id.checkbox_install /*2131166286*/:
                buildSTInfo.slotId = a.a("06", "002");
                buildSTInfo.status = this.f572a.v.isSelected() ? "01" : "02";
                break;
            case R.id.checkbox_plugin /*2131166287*/:
                buildSTInfo.slotId = a.a("06", "004");
                buildSTInfo.status = this.f572a.x.isSelected() ? "01" : "02";
                break;
            case R.id.checkbox_virus /*2131166288*/:
                buildSTInfo.slotId = a.a("06", "005");
                buildSTInfo.status = this.f572a.z.isSelected() ? "01" : "02";
                break;
            case R.id.checkbox_private /*2131166289*/:
                buildSTInfo.slotId = a.a("06", "006");
                buildSTInfo.status = this.f572a.B.isSelected() ? "01" : "02";
                break;
        }
        return buildSTInfo;
    }
}
