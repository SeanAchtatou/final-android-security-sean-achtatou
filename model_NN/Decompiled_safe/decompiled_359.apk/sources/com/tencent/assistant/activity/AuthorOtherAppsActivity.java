package com.tencent.assistant.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.adapter.AppAdapter;
import com.tencent.assistant.component.LoadingView;
import com.tencent.assistant.component.NormalErrorRecommendPage;
import com.tencent.assistant.component.txscrollview.ITXRefreshListViewListener;
import com.tencent.assistant.component.txscrollview.TXGetMoreListView;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.b;
import com.tencent.assistant.module.callback.f;
import com.tencent.assistant.module.cq;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.df;
import com.tencent.assistantv2.component.RankNormalListView;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class AuthorOtherAppsActivity extends BaseActivity implements ITXRefreshListViewListener, f {
    private AppAdapter A;
    private LoadingView B;
    private NormalErrorRecommendPage C;
    /* access modifiers changed from: private */
    public int D;
    private View.OnClickListener E = new bb(this);
    /* access modifiers changed from: private */
    public cq n;
    /* access modifiers changed from: private */
    public String t;
    private String u;
    private ArrayList<SimpleAppModel> v;
    /* access modifiers changed from: private */
    public byte[] w;
    private boolean x;
    private SecondNavigationTitleViewV5 y;
    private TXGetMoreListView z;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.activity.AuthorOtherAppsActivity.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.tencent.assistant.activity.AuthorOtherAppsActivity.a(com.tencent.assistant.activity.AuthorOtherAppsActivity, int):int
      com.tencent.assistant.activity.BaseActivity.a(java.lang.String, java.lang.String):void
      com.tencent.assistant.activity.AuthorOtherAppsActivity.a(boolean, boolean):void */
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        i();
        super.onCreate(bundle);
        setContentView((int) R.layout.author_other_apps_layout);
        this.n = new cq();
        this.n.f1758a = 20;
        j();
        this.n.register(this);
        this.D = this.n.a(this.t);
        a(false, false);
    }

    /* access modifiers changed from: private */
    public void a(boolean z2, boolean z3) {
        if (z2) {
            this.B.setVisibility(4);
            if (z3) {
                this.z.setVisibility(0);
                this.C.setVisibility(4);
                return;
            }
            this.z.setVisibility(4);
            this.C.setVisibility(0);
            return;
        }
        this.B.setVisibility(0);
        this.z.setVisibility(4);
        this.C.setVisibility(4);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.y.l();
        this.A.notifyDataSetChanged();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.y.m();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.n.unregister(this);
        super.onDestroy();
    }

    public int f() {
        return STConst.ST_PAGE_APP_AUTHOR_RELATE;
    }

    private void i() {
        Intent intent = getIntent();
        this.t = intent.getStringExtra("pkgname");
        this.u = intent.getStringExtra("author_name");
        this.v = intent.getParcelableArrayListExtra("first_page_data");
        this.w = intent.getByteArrayExtra("page_context");
        this.x = intent.getBooleanExtra("has_next", false);
    }

    private void j() {
        this.y = (SecondNavigationTitleViewV5) findViewById(R.id.title);
        this.y.a(this);
        this.y.a(String.format(getString(R.string.author_other_title), this.u), TextUtils.TruncateAt.MIDDLE);
        this.y.i();
        this.z = (TXGetMoreListView) findViewById(R.id.list);
        this.B = (LoadingView) findViewById(R.id.loading_view);
        this.C = (NormalErrorRecommendPage) findViewById(R.id.error_page);
        this.C.setButtonClickListener(this.E);
        ImageView imageView = new ImageView(this);
        imageView.setLayoutParams(new AbsListView.LayoutParams(-1, df.a(this, 6.0f)));
        imageView.setBackgroundColor(getResources().getColor(17170445));
        this.z.addHeaderView(imageView);
        this.A = new AppAdapter(this, this.z, new b());
        this.A.a(f(), -100, RankNormalListView.ST_HIDE_INSTALLED_APPS);
        this.z.setAdapter(this.A);
        this.z.setDivider(null);
        this.z.onRefreshComplete(this.x);
        this.z.setRefreshListViewListener(this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.activity.AuthorOtherAppsActivity.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.tencent.assistant.activity.AuthorOtherAppsActivity.a(com.tencent.assistant.activity.AuthorOtherAppsActivity, int):int
      com.tencent.assistant.activity.BaseActivity.a(java.lang.String, java.lang.String):void
      com.tencent.assistant.activity.AuthorOtherAppsActivity.a(boolean, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.adapter.AppAdapter.a(boolean, java.util.List<com.tencent.assistant.model.SimpleAppModel>):void
     arg types: [int, java.util.List<com.tencent.assistant.model.SimpleAppModel>]
     candidates:
      com.tencent.assistant.adapter.AppAdapter.a(com.tencent.assistant.model.SimpleAppModel, int):com.tencent.assistantv2.st.page.STInfoV2
      com.tencent.assistant.adapter.AppAdapter.a(boolean, java.util.List<com.tencent.assistant.model.SimpleAppModel>):void */
    public void a(int i, int i2, boolean z2, List<SimpleAppModel> list, boolean z3, byte[] bArr) {
        if (i != this.D) {
            return;
        }
        if (i2 == 0) {
            if (list != null && list.size() != 0) {
                this.w = bArr;
                this.x = z3;
                this.A.a(false, list);
                this.A.notifyDataSetChanged();
                a(true, true);
                this.z.onRefreshComplete(this.x);
            } else if (this.A.getCount() > 0) {
                a(true, true);
                this.z.onRefreshComplete(false);
            } else {
                this.C.setErrorType(10);
                a(true, false);
            }
        } else if (i2 == -800) {
            this.C.setErrorType(30);
            a(true, false);
        } else if (this.A.getCount() > 0) {
            this.z.onRefreshComplete(z3, false);
            a(true, true);
        } else {
            this.C.setErrorType(20);
            a(true, false);
        }
    }

    public void onTXRefreshListViewRefresh(TXScrollViewBase.ScrollState scrollState) {
        if (TXScrollViewBase.ScrollState.ScrollState_FromEnd == scrollState && this.x) {
            this.D = this.n.a(this.t, this.w);
        }
    }

    public void h() {
        STInfoV2 s = s();
        if (s != null) {
            s.extraData = this.u + Constants.STR_EMPTY;
        }
        k.a(s);
    }
}
