package com.tencent.assistant.activity;

import android.widget.AbsListView;
import com.tencent.assistant.component.invalidater.ListViewScrollListener;

/* compiled from: ProGuard */
class da extends ListViewScrollListener {

    /* renamed from: a  reason: collision with root package name */
    boolean f468a = false;
    final /* synthetic */ cy b;

    da(cy cyVar) {
        this.b = cyVar;
    }

    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
        super.onScroll(absListView, i, i2, i3);
        if (i == 2) {
            this.f468a = true;
        } else if (i == 1) {
            this.f468a = false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.component.GameRankNormalListView.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.tencent.assistantv2.component.GameRankNormalListView.a(com.tencent.assistantv2.component.GameRankNormalListView, java.util.List):boolean
      com.tencent.assistantv2.component.GameRankNormalListView.a(boolean, boolean):void */
    public void onScrollStateChanged(AbsListView absListView, int i) {
        super.onScrollStateChanged(absListView, i);
        if (i == 0 && this.f468a) {
            this.b.V.a(false, false);
        }
    }
}
