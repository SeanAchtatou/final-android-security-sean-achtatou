package com.tencent.assistant.adapter;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import com.tencent.assistant.component.PicView;
import com.tencent.connect.common.Constants;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/* compiled from: ProGuard */
public class cg extends PagerAdapter {

    /* renamed from: a  reason: collision with root package name */
    private ArrayList<String> f740a = new ArrayList<>();
    private ArrayList<String> b = new ArrayList<>();
    private HashMap<Integer, WeakReference<PicView>> c = new HashMap<>();
    private Context d;
    private ArrayList<int[]> e;
    /* access modifiers changed from: private */
    public ch f;
    private int g = -1;
    private HashMap<Integer, PicView> h = new HashMap<>();

    public cg(Context context) {
        this.d = context;
    }

    public void a(ch chVar) {
        this.f = chVar;
    }

    public int getCount() {
        return this.f740a.size();
    }

    public void startUpdate(View view) {
    }

    public void a(int i) {
        this.g = i;
    }

    public void a(ArrayList<int[]> arrayList) {
        this.e = arrayList;
    }

    public Object instantiateItem(View view, int i) {
        PicView picView = new PicView(this.d);
        this.c.put(Integer.valueOf(i), new WeakReference(picView));
        int[] iArr = null;
        if (this.e != null) {
            if (this.e.size() > i + 1) {
                iArr = this.e.get(i + 1);
            } else if (this.e.size() > 0) {
                iArr = this.e.get(0);
            }
        }
        if (i == this.g) {
            if (iArr != null) {
                picView.startScaleAnim(iArr);
            }
            this.g = -1;
        } else if (iArr != null) {
            picView.setOriPicPos(iArr);
        }
        if (i < this.b.size()) {
            picView.setPicInf(this.f740a.get(i), this.b.get(i), new ci(this, i));
        } else {
            picView.setPicInf(this.f740a.get(i), new ci(this, i));
        }
        this.h.put(Integer.valueOf(i), picView);
        ((ViewGroup) view).addView(picView);
        return picView;
    }

    public void destroyItem(View view, int i, Object obj) {
        PicView picView = (PicView) obj;
        picView.recycle();
        this.h.remove(Integer.valueOf(i));
        ((ViewPager) view).removeView(picView);
    }

    public void finishUpdate(View view) {
    }

    public int getItemPosition(Object obj) {
        return -2;
    }

    public boolean isViewFromObject(View view, Object obj) {
        return view == obj;
    }

    public Parcelable saveState() {
        return null;
    }

    public void restoreState(Parcelable parcelable, ClassLoader classLoader) {
    }

    public void a(List<String> list, List<String> list2) {
        if (!(list == null || list.size() == 0)) {
            this.f740a.clear();
            this.f740a.addAll(list);
        }
        if (!(list2 == null || list2.size() == 0)) {
            this.b.clear();
            this.b.addAll(list2);
        }
        notifyDataSetChanged();
    }

    public void b(int i) {
        PicView picView;
        for (Integer intValue : this.c.keySet()) {
            int intValue2 = intValue.intValue();
            WeakReference weakReference = this.c.get(Integer.valueOf(intValue2));
            if (weakReference != null && intValue2 != i) {
                PicView picView2 = (PicView) weakReference.get();
                if (picView2 != null) {
                    picView2.release();
                }
            } else if (!(weakReference == null || (picView = (PicView) weakReference.get()) == null)) {
                picView.doRefresh();
            }
        }
    }

    public String c(int i) {
        for (Integer intValue : this.h.keySet()) {
            int intValue2 = intValue.intValue();
            PicView picView = this.h.get(Integer.valueOf(intValue2));
            if (intValue2 == i && picView != null && picView != null) {
                return picView.getUrl();
            }
        }
        return Constants.STR_EMPTY;
    }

    public boolean a(Animation.AnimationListener animationListener, int i) {
        WeakReference weakReference = this.c.get(Integer.valueOf(i));
        if (weakReference == null) {
            return false;
        }
        PicView picView = (PicView) weakReference.get();
        if (picView == null) {
            return false;
        }
        return picView.animOut(animationListener);
    }
}
