package com.tencent.assistant.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Pair;
import android.util.SparseBooleanArray;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.component.appdetail.CommentDetailTabView;
import com.tencent.assistant.component.txscrollview.TXExpandableListView;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.cq;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.d;
import com.tencent.assistant.module.u;
import com.tencent.assistant.module.update.k;
import com.tencent.assistant.module.wisedownload.j;
import com.tencent.assistant.protocol.jce.StatUpdateManageAction;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistantv2.activity.AppDetailActivityV5;
import com.tencent.assistantv2.adapter.smartlist.ab;
import com.tencent.assistantv2.adapter.smartlist.v;
import com.tencent.assistantv2.st.b.b;
import com.tencent.assistantv2.st.page.STExternalInfo;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.cloud.updaterec.a;
import com.tencent.cloud.updaterec.h;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/* compiled from: ProGuard */
public class ac extends BaseExpandableListAdapter {

    /* renamed from: a  reason: collision with root package name */
    public View f689a;
    public boolean b = false;
    public SparseIntArray c = new SparseIntArray();
    public SparseIntArray d = new SparseIntArray();
    public int e = -1;
    h f = new aj(this);
    protected ab g = new ab();
    a h;
    /* access modifiers changed from: private */
    public Context i;
    private LayoutInflater j;
    private final int k = 1;
    private final int l = 2;
    private final int[] m = {1, 2};
    /* access modifiers changed from: private */
    public Map<Long, Integer> n;
    /* access modifiers changed from: private */
    public ArrayList<Integer> o = new ArrayList<>();
    /* access modifiers changed from: private */
    public LinkedHashMap<Integer, ArrayList<SimpleAppModel>> p = new LinkedHashMap<>();
    /* access modifiers changed from: private */
    public ArrayList<SimpleAppModel> q = new ArrayList<>();
    /* access modifiers changed from: private */
    public SparseBooleanArray r = new SparseBooleanArray();
    /* access modifiers changed from: private */
    public String s = null;
    private StatUpdateManageAction t = null;
    private String u = Constants.STR_EMPTY;
    private STExternalInfo v;
    private Activity w;
    private b x = null;
    private Comparator<SimpleAppModel> y = new al(this);

    public ac(Context context, View view, StatUpdateManageAction statUpdateManageAction) {
        AstApp.i();
        this.i = context;
        this.j = LayoutInflater.from(this.i);
        this.t = statUpdateManageAction;
        this.f689a = view;
    }

    private int b(int i2) {
        switch (i2) {
            case 1:
            case 2:
                return 1;
            default:
                return 2;
        }
    }

    public void a(STExternalInfo sTExternalInfo, String str) {
        this.v = sTExternalInfo;
        this.u = str;
    }

    public void a(Map<Long, Integer> map) {
        this.n = map;
    }

    public void a(List<SimpleAppModel> list, boolean z) {
        ArrayList arrayList;
        if (list == null) {
            list = new ArrayList<>();
        }
        if (this.q.size() == 0 || z) {
            this.q.clear();
            this.q.addAll(list);
            if (this.c != null) {
                this.c.clear();
            }
            if (this.d != null) {
                this.d.clear();
            }
        } else {
            ArrayList arrayList2 = new ArrayList();
            Iterator<SimpleAppModel> it = this.q.iterator();
            while (it.hasNext()) {
                arrayList2.add(Integer.valueOf(it.next().c.hashCode()));
            }
            ArrayList arrayList3 = new ArrayList();
            int i2 = 0;
            Iterator<SimpleAppModel> it2 = list.iterator();
            while (true) {
                int i3 = i2;
                if (!it2.hasNext()) {
                    break;
                }
                SimpleAppModel next = it2.next();
                arrayList3.add(Integer.valueOf(next.c.hashCode()));
                if (!arrayList2.contains(Integer.valueOf(next.c.hashCode()))) {
                    this.q.add(i3, next);
                }
                i2 = i3 + 1;
            }
            Iterator<SimpleAppModel> it3 = this.q.iterator();
            while (it3.hasNext()) {
                SimpleAppModel next2 = it3.next();
                if (!arrayList3.contains(Integer.valueOf(next2.c.hashCode()))) {
                    if (!(this.c != null && this.c.indexOfKey(next2.c.hashCode()) >= 0)) {
                        it3.remove();
                    }
                }
            }
        }
        this.o.clear();
        this.p.clear();
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        ArrayList arrayList4 = null;
        Iterator<SimpleAppModel> it4 = this.q.iterator();
        while (it4.hasNext()) {
            SimpleAppModel next3 = it4.next();
            int a2 = a(next3);
            if (!this.o.contains(Integer.valueOf(a2))) {
                this.o.add(Integer.valueOf(a2));
            }
            ArrayList arrayList5 = this.p.get(Integer.valueOf(a2));
            if (arrayList5 == null) {
                arrayList5 = new ArrayList();
                this.p.put(Integer.valueOf(a2), arrayList5);
            }
            AppConst.AppState d2 = u.d(next3);
            boolean z2 = this.c != null && this.c.indexOfKey(next3.c.hashCode()) >= 0;
            boolean z3 = this.d != null && this.d.indexOfKey(next3.c.hashCode()) >= 0;
            boolean z4 = AppConst.AppState.DOWNLOADED == d2 && (!z2 || z3);
            boolean z5 = AppConst.AppState.INSTALLING == d2 && (!z2 || z3);
            boolean z6 = AppConst.AppState.INSTALLED == d2 && z3;
            if (z4 || z5 || z6) {
                ArrayList arrayList6 = (ArrayList) linkedHashMap.get(Integer.valueOf(a2));
                if (arrayList6 == null) {
                    arrayList6 = new ArrayList(5);
                    linkedHashMap.put(Integer.valueOf(a2), arrayList6);
                }
                arrayList6.add(next3);
                if (this.d != null && this.d.indexOfKey(next3.c.hashCode()) < 0) {
                    this.d.put(next3.c.hashCode(), 0);
                }
            } else if (this.n == null || !this.n.containsKey(Long.valueOf(next3.f1634a))) {
                arrayList5.add(next3);
            } else {
                if (arrayList4 == null) {
                    arrayList = new ArrayList(4);
                } else {
                    arrayList = arrayList4;
                }
                arrayList.add(next3);
                arrayList4 = arrayList;
            }
        }
        if (!linkedHashMap.isEmpty()) {
            for (Integer intValue : linkedHashMap.keySet()) {
                int intValue2 = intValue.intValue();
                ArrayList arrayList7 = (ArrayList) linkedHashMap.get(Integer.valueOf(intValue2));
                if (!arrayList7.isEmpty()) {
                    Collections.sort(arrayList7, this.y);
                    this.p.get(Integer.valueOf(intValue2)).addAll(0, arrayList7);
                }
            }
        }
        if (arrayList4 != null) {
            Collections.sort(arrayList4, new ad(this));
            ArrayList arrayList8 = this.p.get(1);
            if (arrayList8 == null) {
                arrayList8 = new ArrayList(5);
                this.p.put(1, arrayList8);
            }
            arrayList8.addAll(0, arrayList4);
        }
        try {
            Collections.sort(this.o);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        notifyDataSetChanged();
    }

    private int a(SimpleAppModel simpleAppModel) {
        int i2;
        try {
            ArrayList<Integer> c2 = k.b().c(simpleAppModel.c);
            if (c2 == null || c2.size() <= 0) {
                i2 = 0;
            } else {
                i2 = c2.get(0).intValue();
            }
            try {
                if (this.n != null && this.n.containsKey(Long.valueOf(simpleAppModel.f1634a))) {
                    i2 = 1;
                }
            } catch (Exception e2) {
            }
        } catch (Exception e3) {
            i2 = 0;
        }
        return b(i2);
    }

    public void a() {
    }

    public void b() {
        if (this.r != null) {
            this.r.clear();
            this.r = null;
        }
        if (this.c != null) {
            this.c.clear();
            this.c = null;
        }
        if (this.d != null) {
            this.d.clear();
            this.d = null;
        }
    }

    public int getGroupCount() {
        if (this.o != null) {
            return this.o.size();
        }
        return 0;
    }

    public int getChildrenCount(int i2) {
        ArrayList arrayList;
        if (this.p == null || this.o == null || getGroup(i2) == null || (arrayList = this.p.get((Integer) getGroup(i2))) == null || arrayList.isEmpty()) {
            return 0;
        }
        return arrayList.size();
    }

    public Object getGroup(int i2) {
        if (i2 < 0 || i2 >= this.o.size()) {
            return null;
        }
        return this.o.get(i2);
    }

    private int c(int i2) {
        Object group = getGroup(i2);
        if (group != null) {
            return ((Integer) group).intValue();
        }
        return -1;
    }

    public Object getChild(int i2, int i3) {
        ArrayList arrayList;
        if (this.p == null || this.o == null || getGroup(i2) == null || (arrayList = this.p.get(getGroup(i2))) == null || arrayList.size() <= i3) {
            return null;
        }
        return arrayList.get(i3);
    }

    public long getGroupId(int i2) {
        return (long) i2;
    }

    public long getChildId(int i2, int i3) {
        return (long) i3;
    }

    public boolean hasStableIds() {
        return true;
    }

    public boolean isChildSelectable(int i2, int i3) {
        return true;
    }

    public View getGroupView(int i2, boolean z, View view, ViewGroup viewGroup) {
        am amVar;
        if (view == null || !(view.getTag() instanceof am)) {
            view = this.j.inflate((int) R.layout.updatelist_group_item, (ViewGroup) null);
            am amVar2 = new am(this, null);
            amVar2.f699a = view;
            amVar2.b = (RelativeLayout) view.findViewById(R.id.layout_group);
            amVar2.c = (TextView) view.findViewById(R.id.group_title);
            amVar2.d = (TextView) view.findViewById(R.id.group_title_num);
            view.setTag(amVar2);
            amVar = amVar2;
        } else {
            amVar = (am) view.getTag();
        }
        String[] a2 = a(c(i2));
        if (a2 != null) {
            amVar.c.setText(a2[0]);
            amVar.d.setText(" " + a2[1]);
        }
        return view;
    }

    public String[] a(int i2) {
        int i3;
        if (i2 == 1) {
            return new String[]{this.i.getResources().getString(R.string.app_update_group_recommend), "(" + (this.p.get(Integer.valueOf(i2)) != null ? a(this.p.get(Integer.valueOf(i2))) : 0) + ")"};
        } else if (i2 != 2) {
            return null;
        } else {
            if (this.p.get(Integer.valueOf(i2)) != null) {
                i3 = a(this.p.get(Integer.valueOf(i2)));
            } else {
                i3 = 0;
            }
            return new String[]{this.i.getResources().getString(R.string.app_update_group_other), "(" + i3 + ")"};
        }
    }

    private int a(List<SimpleAppModel> list) {
        List<SimpleAppModel> a2;
        int i2;
        int i3 = 0;
        if (list != null && !list.isEmpty() && (a2 = u.a(this.b)) != null && !a2.isEmpty()) {
            ArrayList arrayList = new ArrayList();
            for (SimpleAppModel simpleAppModel : a2) {
                arrayList.add(Integer.valueOf(simpleAppModel.c.hashCode()));
            }
            for (SimpleAppModel simpleAppModel2 : list) {
                if (arrayList.contains(Integer.valueOf(simpleAppModel2.c.hashCode()))) {
                    i2 = i3 + 1;
                } else {
                    i2 = i3;
                }
                i3 = i2;
            }
        }
        return i3;
    }

    public View getChildView(int i2, int i3, boolean z, View view, ViewGroup viewGroup) {
        SimpleAppModel simpleAppModel = (SimpleAppModel) getChild(i2, i3);
        Pair<View, x> a2 = w.a(this.i, this.j, view, 1);
        x xVar = (x) a2.second;
        View view2 = (View) a2.first;
        STInfoV2 b2 = b(simpleAppModel, i2, i3);
        if (simpleAppModel != null) {
            a(xVar, simpleAppModel, i2, i3, b2);
            com.tencent.cloud.b.a.a(this.i, ((TXExpandableListView) this.f689a).getContentView(), this.f, xVar, simpleAppModel, i3);
        }
        this.e = i3;
        XLog.d("zhangyuanchao", "-mCurrentSelectPosition-" + this.e + "-childPosition-" + i3);
        return view2;
    }

    private String a(int i2, int i3) {
        if (c(i2) == 1) {
            return com.tencent.assistantv2.st.page.a.a("04", i3);
        }
        return com.tencent.assistantv2.st.page.a.a("05", i3);
    }

    private void a(x xVar, SimpleAppModel simpleAppModel, int i2, int i3, STInfoV2 sTInfoV2) {
        if (xVar != null && simpleAppModel != null) {
            xVar.f821a.setOnClickListener(new ae(this, simpleAppModel, sTInfoV2));
            w.a(this.i, xVar, simpleAppModel, sTInfoV2);
            AppConst.AppState d2 = u.d(simpleAppModel);
            if (d2 == AppConst.AppState.INSTALLED || this.b) {
                xVar.l.setVisibility(8);
            } else {
                xVar.l.setVisibility(0);
                xVar.l.setOnClickListener(new af(this, simpleAppModel, i2, i3, sTInfoV2));
            }
            if (this.b && !xVar.f.f2987a) {
                xVar.f.a();
            } else if (!this.b && xVar.f.f2987a) {
                xVar.f.b();
            }
            boolean a2 = j.a(simpleAppModel.c);
            if (d2 == AppConst.AppState.QUEUING || d2 == AppConst.AppState.DOWNLOADING || d2 == AppConst.AppState.INSTALLING || a2) {
                this.c.put(simpleAppModel.c.hashCode(), simpleAppModel.g);
            }
            String str = simpleAppModel.o;
            if (TextUtils.isEmpty(str)) {
                xVar.i.setVisibility(8);
            } else {
                String format = String.format(this.i.getResources().getString(R.string.update_feature), "\n" + str);
                xVar.k.setText(format);
                String replace = format.replaceAll("\r\n", "\n").replaceAll("\n\n", "\n").replace("\n", Constants.STR_EMPTY);
                xVar.i.setVisibility(0);
                xVar.i.setOnClickListener(new ag(this, simpleAppModel, xVar, sTInfoV2));
                xVar.j.setTag(Long.valueOf(simpleAppModel.f1634a));
                xVar.j.a(new ah(this, xVar, simpleAppModel.f1634a));
                xVar.j.setText(replace);
                if (this.s == null || !this.s.equals(simpleAppModel.c)) {
                    xVar.j.setVisibility(0);
                    xVar.k.setVisibility(8);
                    try {
                        xVar.m.setImageDrawable(this.i.getResources().getDrawable(R.drawable.icon_open));
                    } catch (Throwable th) {
                        cq.a().b();
                    }
                } else {
                    xVar.k.setVisibility(0);
                    xVar.j.setVisibility(8);
                    try {
                        xVar.m.setImageDrawable(this.i.getResources().getDrawable(R.drawable.icon_close));
                    } catch (Throwable th2) {
                        cq.a().b();
                    }
                    notifyDataSetChanged();
                }
            }
            xVar.f.a(sTInfoV2, new ai(this, simpleAppModel, i2, i3), (d) null, xVar.f, xVar.g);
        }
    }

    /* access modifiers changed from: private */
    public void a(SimpleAppModel simpleAppModel, View view) {
        boolean j2 = simpleAppModel.j();
        if (this.h == null) {
            this.h = new a(view.getContext(), ((TXExpandableListView) this.f689a).getContentView(), this.f);
            com.tencent.cloud.a.a.a().register(this.h);
        } else if (this.h instanceof a) {
            this.h.a(view.getContext(), ((TXExpandableListView) this.f689a).getContentView(), this.f);
        }
        if (com.tencent.cloud.b.a.a(view.getContext(), simpleAppModel, ((TXExpandableListView) this.f689a).getContentView(), this.f, (View) view.getParent(), !j2)) {
            Object tag = ((View) view.getParent()).getTag();
            if (tag instanceof v) {
                simpleAppModel.ao = null;
                ((v) tag).q.setVisibility(8);
            }
        }
    }

    /* access modifiers changed from: private */
    public void b(SimpleAppModel simpleAppModel) {
        if (this.r != null && simpleAppModel != null && simpleAppModel.c != null && this.r.indexOfKey(simpleAppModel.c.hashCode()) > 0) {
            this.r.clear();
        }
    }

    /* access modifiers changed from: private */
    public void c(SimpleAppModel simpleAppModel) {
        StatUpdateManageAction statUpdateManageAction = this.t;
        statUpdateManageAction.e = (short) (statUpdateManageAction.e + 1);
        Intent intent = new Intent(this.i, AppDetailActivityV5.class);
        intent.putExtra(CommentDetailTabView.PARAMS_SIMPLE_MODEL_INFO, simpleAppModel);
        this.i.startActivity(intent);
    }

    /* access modifiers changed from: private */
    public void a(SimpleAppModel simpleAppModel, int i2, int i3) {
        ArrayList arrayList;
        if (simpleAppModel != null) {
            StatUpdateManageAction statUpdateManageAction = this.t;
            statUpdateManageAction.d = (short) (statUpdateManageAction.d + 1);
            if (!k.b().a(simpleAppModel)) {
                ArrayList arrayList2 = this.p.get(getGroup(i2));
                if (arrayList2 != null && arrayList2.size() > i3) {
                    arrayList2.remove(i3);
                }
                if (getGroup(i2) != null && ((arrayList = this.p.get((Integer) getGroup(i2))) == null || arrayList.isEmpty())) {
                    this.o.remove(i2);
                }
            }
            b(simpleAppModel);
            this.q.remove(simpleAppModel);
            this.c.delete(simpleAppModel.c.hashCode());
            notifyDataSetChanged();
            if (m.a().aa()) {
                m.a().ab();
                ak akVar = new ak(this);
                akVar.titleRes = this.i.getResources().getString(R.string.ignore_update_tips_title);
                akVar.contentRes = this.i.getResources().getString(R.string.ignore_update_tips_content);
                com.tencent.assistant.utils.v.a(this.w, akVar);
            }
        }
    }

    public void a(Activity activity) {
        this.w = activity;
    }

    public List<SimpleAppModel> c() {
        return this.q;
    }

    private STInfoV2 b(SimpleAppModel simpleAppModel, int i2, int i3) {
        if (simpleAppModel == null) {
            return null;
        }
        if (this.x == null) {
            this.x = new b();
        }
        AppConst.AppState d2 = u.d(simpleAppModel);
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.i, simpleAppModel, a(i2, i3), 100, com.tencent.assistantv2.st.page.a.b(d2));
        if (buildSTInfo != null) {
            buildSTInfo.updateWithExternalPara(this.v);
            buildSTInfo.pushInfo = this.u;
        }
        this.x.exposure(buildSTInfo);
        return buildSTInfo;
    }

    public void d() {
        this.b = true;
    }

    public void e() {
        this.b = false;
    }
}
