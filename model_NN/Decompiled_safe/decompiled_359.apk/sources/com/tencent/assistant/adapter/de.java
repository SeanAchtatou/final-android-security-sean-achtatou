package com.tencent.assistant.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.tencent.assistant.component.AppIconView;
import com.tencent.assistant.component.RatingView;
import com.tencent.assistantv2.component.DownloadButton;

/* compiled from: ProGuard */
class de {

    /* renamed from: a  reason: collision with root package name */
    View f764a;
    ImageView b;
    AppIconView c;
    TextView d;
    RatingView e;
    TextView f;
    DownloadButton g;
    View h;
    TextView i;
    TextView j;
    TextView k;
    ImageView l;
    final /* synthetic */ cw m;

    private de(cw cwVar) {
        this.m = cwVar;
    }

    /* synthetic */ de(cw cwVar, cx cxVar) {
        this(cwVar);
    }
}
