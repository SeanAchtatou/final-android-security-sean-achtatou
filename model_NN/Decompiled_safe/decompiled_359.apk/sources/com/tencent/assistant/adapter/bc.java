package com.tencent.assistant.adapter;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
class bc extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ bq f716a;
    final /* synthetic */ DownloadInfoMultiAdapter b;

    bc(DownloadInfoMultiAdapter downloadInfoMultiAdapter, bq bqVar) {
        this.b = downloadInfoMultiAdapter;
        this.f716a = bqVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.adapter.DownloadInfoMultiAdapter.a(com.tencent.assistant.adapter.DownloadInfoMultiAdapter, boolean):boolean
     arg types: [com.tencent.assistant.adapter.DownloadInfoMultiAdapter, int]
     candidates:
      com.tencent.assistant.adapter.DownloadInfoMultiAdapter.a(int, int):java.lang.String
      com.tencent.assistant.adapter.DownloadInfoMultiAdapter.a(com.tencent.assistantv2.mediadownload.m, com.tencent.assistantv2.model.d):java.lang.String
      com.tencent.assistant.adapter.DownloadInfoMultiAdapter.a(com.tencent.assistant.adapter.DownloadInfoMultiAdapter, java.util.List):java.util.ArrayList
      com.tencent.assistant.adapter.DownloadInfoMultiAdapter.a(java.util.List<com.tencent.assistant.download.DownloadInfoWrapper>, int):java.util.ArrayList<com.tencent.assistant.protocol.jce.InstalledAppItem>
      com.tencent.assistant.adapter.DownloadInfoMultiAdapter.a(com.tencent.assistant.adapter.DownloadInfoMultiAdapter, com.tencent.assistant.component.invalidater.ViewInvalidateMessage):void
      com.tencent.assistant.adapter.DownloadInfoMultiAdapter.a(com.tencent.assistant.adapter.DownloadInfoMultiAdapter, com.tencent.assistant.download.DownloadInfo):void
      com.tencent.assistant.adapter.DownloadInfoMultiAdapter.a(com.tencent.assistant.adapter.DownloadInfoMultiAdapter, com.tencent.assistant.download.DownloadInfoWrapper):void
      com.tencent.assistant.adapter.DownloadInfoMultiAdapter.a(com.tencent.assistant.adapter.bs, com.tencent.assistant.adapter.DownloadInfoMultiAdapter$CreatingTaskStatusEnum):void
      com.tencent.assistant.adapter.DownloadInfoMultiAdapter.a(com.tencent.assistant.adapter.bu, com.tencent.assistantv2.model.d):void
      com.tencent.assistant.adapter.DownloadInfoMultiAdapter.a(com.tencent.assistant.adapter.bx, com.tencent.assistantv2.model.h):void
      com.tencent.assistant.adapter.DownloadInfoMultiAdapter.a(com.tencent.assistant.adapter.by, boolean):void
      com.tencent.assistant.adapter.DownloadInfoMultiAdapter.a(com.tencent.assistant.download.DownloadInfoWrapper, android.view.View):void
      com.tencent.assistant.adapter.DownloadInfoMultiAdapter.a(java.util.List<com.tencent.assistant.download.DownloadInfoWrapper>, long):boolean
      com.tencent.assistant.adapter.DownloadInfoMultiAdapter.a(java.util.List<com.tencent.assistant.download.DownloadInfoWrapper>, com.tencent.assistant.download.DownloadInfoWrapper):boolean
      com.tencent.assistant.adapter.DownloadInfoMultiAdapter.a(java.lang.String, com.tencent.assistant.protocol.jce.InstalledAppItem):void
      com.tencent.assistant.adapter.DownloadInfoMultiAdapter.a(com.tencent.assistant.adapter.DownloadInfoMultiAdapter, boolean):boolean */
    public void onTMAClick(View view) {
        this.f716a.c.setVisibility(8);
        boolean unused = this.b.s = true;
        this.b.l();
        this.b.notifyDataSetChanged();
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.b.m, 200);
        buildSTInfo.status = Constants.VIA_REPORT_TYPE_JOININ_GROUP;
        return buildSTInfo;
    }
}
