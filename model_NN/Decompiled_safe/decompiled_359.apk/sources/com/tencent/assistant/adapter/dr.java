package com.tencent.assistant.adapter;

import android.os.Message;
import com.tencent.assistant.component.SwitchButton;
import com.tencent.assistant.manager.cr;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.installuninstall.InstallUninstallUtil;

/* compiled from: ProGuard */
class dr implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SwitchButton f777a;
    final /* synthetic */ Cdo b;

    dr(Cdo doVar, SwitchButton switchButton) {
        this.b = doVar;
        this.f777a = switchButton;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.adapter.do.b(com.tencent.assistant.adapter.do, boolean):boolean
     arg types: [com.tencent.assistant.adapter.do, int]
     candidates:
      com.tencent.assistant.adapter.do.b(int, int):int
      com.tencent.assistant.adapter.do.b(com.tencent.assistant.adapter.do, boolean):boolean */
    public void run() {
        boolean z;
        boolean unused = this.b.j = true;
        boolean c = cr.a().c();
        if (!c) {
            z = InstallUninstallUtil.a();
        } else {
            z = false;
        }
        XLog.d("SettingAdapter", "handle root switch, permRootReqResult=" + z + ", tempRootReqResult=" + c);
        Message obtainMessage = this.b.k.obtainMessage();
        obtainMessage.obj = this.f777a;
        obtainMessage.what = 1;
        if (z || c) {
            obtainMessage.arg1 = 1;
        } else {
            obtainMessage.arg1 = 0;
        }
        this.b.k.removeMessages(1);
        this.b.k.sendMessage(obtainMessage);
        boolean unused2 = this.b.j = false;
        this.b.c = false;
    }
}
