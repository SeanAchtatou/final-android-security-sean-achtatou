package com.tencent.assistant.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.utils.bt;
import com.tencent.assistant.utils.ct;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class StartPopWindowGridViewAdapter extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    private Context f681a = null;
    private LayoutInflater b = null;
    private ArrayList<SimpleAppModel> c = new ArrayList<>();
    private ArrayList<Boolean> d = new ArrayList<>();
    private int e;
    private long f;

    public StartPopWindowGridViewAdapter(Context context) {
        this.f681a = context;
        this.b = LayoutInflater.from(context);
    }

    public void a(List<SimpleAppModel> list, int i, long j) {
        this.c.clear();
        this.c.addAll(list);
        this.d.clear();
        for (int i2 = 0; i2 < list.size(); i2++) {
            this.d.add(true);
        }
        this.e = i;
        this.f = j;
        notifyDataSetChanged();
    }

    public int getCount() {
        return this.c.size();
    }

    public Object getItem(int i) {
        return this.c.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        ec ecVar;
        if (view == null) {
            ec ecVar2 = new ec(this);
            view = this.b.inflate((int) R.layout.popwindow_newuser_recommend_item, (ViewGroup) null);
            ecVar2.f787a = (TXImageView) view.findViewById(R.id.icon);
            ecVar2.b = (TextView) view.findViewById(R.id.name);
            ecVar2.c = (TextView) view.findViewById(R.id.size);
            ecVar2.d = (ImageView) view.findViewById(R.id.check_box);
            view.setTag(ecVar2);
            ecVar = ecVar2;
        } else {
            ecVar = (ec) view.getTag();
        }
        view.setTag(R.id.tma_st_slot_tag, b(i));
        a(ecVar, i);
        return view;
    }

    private String b(int i) {
        return "03_" + ct.a(i + 1);
    }

    private void a(ec ecVar, int i) {
        SimpleAppModel simpleAppModel = this.c.get(i);
        ecVar.f787a.updateImageView(simpleAppModel.e, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
        ecVar.b.setText(simpleAppModel.d);
        ecVar.c.setText(bt.a(simpleAppModel.k));
        ecVar.d.setSelected(this.d.get(i).booleanValue());
    }

    public void a(int i) {
        SimpleAppModel simpleAppModel = this.c.get(i);
        boolean booleanValue = this.d.get(i).booleanValue();
        this.d.set(i, Boolean.valueOf(!booleanValue));
        if (booleanValue) {
            this.e--;
            this.f -= this.c.get(i).k;
        } else {
            this.e++;
            this.f += this.c.get(i).k;
        }
        notifyDataSetChanged();
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f681a, 200);
        if (buildSTInfo != null) {
            buildSTInfo.extraData = simpleAppModel != null ? simpleAppModel.c + "|" + simpleAppModel.g : Constants.STR_EMPTY;
            buildSTInfo.slotId = b(i);
            buildSTInfo.updateWithSimpleAppModel(simpleAppModel);
            buildSTInfo.status = !booleanValue ? "01" : "02";
            k.a(buildSTInfo);
        }
    }

    public int a() {
        return this.e;
    }

    public long b() {
        return this.f;
    }

    public ArrayList<Boolean> c() {
        return this.d;
    }
}
