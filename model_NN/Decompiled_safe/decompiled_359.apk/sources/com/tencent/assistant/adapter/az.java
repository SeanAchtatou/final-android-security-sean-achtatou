package com.tencent.assistant.adapter;

import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.module.callback.z;
import com.tencent.assistant.protocol.jce.GetRecommendAppListResponse;
import com.tencent.assistant.protocol.jce.RecommendAppInfo;
import com.tencent.assistant.protocol.jce.SmartCardHotWords;
import java.util.ArrayList;

/* compiled from: ProGuard */
class az implements z {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DownloadInfoMultiAdapter f712a;

    az(DownloadInfoMultiAdapter downloadInfoMultiAdapter) {
        this.f712a = downloadInfoMultiAdapter;
    }

    public void a(int i, int i2, String str, GetRecommendAppListResponse getRecommendAppListResponse) {
        if (this.f712a.u != null) {
            ArrayList<RecommendAppInfo> arrayList = getRecommendAppListResponse.b;
            if (arrayList != null && !arrayList.isEmpty()) {
                ArrayList b = this.f712a.b(this.f712a.a(arrayList));
                if (b.size() >= 3) {
                    this.f712a.u.getRecommendAppView().a(2, b, getRecommendAppListResponse.d);
                    this.f712a.u.getRecommendAppView().setVisibility(0);
                }
            }
            if (getRecommendAppListResponse.b() != null) {
                SmartCardHotWords b2 = getRecommendAppListResponse.b();
                if (b2 == null || b2.a() == null || b2.a().size() < 5) {
                    this.f712a.u.setHotwordCardVisibility(8);
                    return;
                }
                this.f712a.u.freshHotwordCardData(b2.f2320a, this.f712a.m.getResources().getString(R.string.hotwords_card_more_text), b2.c, b2.a());
                this.f712a.u.setHotwordCardVisibility(0);
            }
        }
    }
}
