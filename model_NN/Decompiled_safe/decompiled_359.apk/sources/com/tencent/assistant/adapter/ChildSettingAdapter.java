package com.tencent.assistant.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.k;
import com.tencent.assistant.utils.ct;
import com.tencent.assistant.utils.df;
import com.tencent.assistantv2.model.ItemElement;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.STPageInfo;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class ChildSettingAdapter extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    private Context f670a;
    private LayoutInflater b;
    private List<ItemElement> c = new ArrayList();
    private STInfoV2 d = null;
    private STPageInfo e = null;

    public ChildSettingAdapter(Context context) {
        this.f670a = context;
        if (this.f670a instanceof BaseActivity) {
            this.e = ((BaseActivity) this.f670a).q();
        } else {
            this.e = new STPageInfo();
        }
        this.b = LayoutInflater.from(context);
    }

    public void a(List<ItemElement> list) {
        if (list != null) {
            if (this.c == null) {
                this.c = new ArrayList();
            }
            this.c.clear();
            this.c.addAll(list);
        }
    }

    public int getCount() {
        if (this.c != null) {
            return this.c.size();
        }
        return 0;
    }

    public Object getItem(int i) {
        if (this.c == null || this.c.size() <= i) {
            return null;
        }
        return this.c.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public int getItemViewType(int i) {
        ItemElement itemElement = null;
        if (this.c != null && this.c.size() > i) {
            itemElement = this.c.get(i);
        }
        if (itemElement != null) {
            return itemElement.c;
        }
        return 1;
    }

    public int getViewTypeCount() {
        return 2;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        ItemElement itemElement = (ItemElement) getItem(i);
        if (itemElement != null) {
            switch (getItemViewType(i)) {
                case 0:
                    view = a(itemElement, view, i);
                    break;
            }
            view.setOnClickListener(new ap(this, i));
        }
        return view;
    }

    private View a(ItemElement itemElement, View view, int i) {
        ar arVar;
        if (view == null || !(view.getTag() instanceof ar)) {
            view = this.b.inflate((int) R.layout.child_setting_item_checkbox, (ViewGroup) null);
            ar arVar2 = new ar(this);
            arVar2.f704a = view.findViewById(R.id.item_layout);
            arVar2.b = (TextView) view.findViewById(R.id.item_title);
            arVar2.c = (TextView) view.findViewById(R.id.item_description);
            arVar2.d = (LinearLayout) view.findViewById(R.id.item_left_layout);
            arVar2.e = (TextView) view.findViewById(R.id.check);
            arVar2.f = view.findViewById(R.id.item_line);
            view.setTag(arVar2);
            arVar = arVar2;
        } else {
            arVar = (ar) view.getTag();
        }
        a(itemElement, arVar, i);
        return view;
    }

    private void a(ItemElement itemElement, ar arVar, int i) {
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) arVar.f704a.getLayoutParams();
        marginLayoutParams.setMargins(marginLayoutParams.leftMargin, df.b((float) itemElement.e), marginLayoutParams.rightMargin, marginLayoutParams.bottomMargin);
        arVar.f704a.setLayoutParams(marginLayoutParams);
        arVar.b.setText(itemElement.f3309a);
        if (itemElement.b != null) {
            arVar.c.setText(itemElement.b);
            arVar.c.setVisibility(0);
            arVar.d.setPadding(0, df.a(this.f670a, 20.0f), 0, df.a(this.f670a, 17.0f));
        } else {
            arVar.c.setVisibility(8);
            arVar.d.setPadding(0, 0, 0, 0);
        }
        arVar.e.setSelected(k.a(itemElement.d));
        arVar.e.setOnClickListener(new aq(this, arVar, itemElement, i));
        arVar.f704a.setBackgroundResource(R.drawable.helper_cardbg_all_selector_new);
        if (i == this.c.size() - 1) {
            arVar.f.setVisibility(8);
        } else if (i == 0) {
            arVar.f.setVisibility(0);
        } else {
            arVar.f.setVisibility(0);
        }
    }

    /* access modifiers changed from: private */
    public String a(int i) {
        return "03_" + ct.a(i + 1);
    }

    /* access modifiers changed from: private */
    public String a(boolean z) {
        if (z) {
            return "01";
        }
        return "02";
    }

    /* access modifiers changed from: private */
    public STInfoV2 a(String str, String str2, int i) {
        if (this.d == null) {
            this.d = new STInfoV2(this.e.f3358a, str, this.e.c, this.e.d, i);
        }
        this.d.status = str2;
        this.d.slotId = str;
        this.d.actionId = i;
        return this.d;
    }
}
