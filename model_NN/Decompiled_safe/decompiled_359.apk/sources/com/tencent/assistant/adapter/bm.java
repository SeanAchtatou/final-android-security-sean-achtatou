package com.tencent.assistant.adapter;

import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.utils.v;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
class bm extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DownloadInfo f726a;
    final /* synthetic */ STInfoV2 b;
    final /* synthetic */ DownloadInfoMultiAdapter c;

    bm(DownloadInfoMultiAdapter downloadInfoMultiAdapter, DownloadInfo downloadInfo, STInfoV2 sTInfoV2) {
        this.c = downloadInfoMultiAdapter;
        this.f726a = downloadInfo;
        this.b = sTInfoV2;
    }

    public void onTMAClick(View view) {
        if (this.f726a != null) {
            bn bnVar = new bn(this);
            bnVar.titleRes = this.c.m.getResources().getString(R.string.downloaded_delete_confirm_title);
            bnVar.contentRes = this.c.m.getResources().getString(R.string.downloaded_delete_confirm);
            bnVar.rBtnTxtRes = this.c.m.getResources().getString(R.string.downloaded_delete_confirm_btn);
            v.a(bnVar);
        }
    }

    public STInfoV2 getStInfo() {
        if (this.b != null) {
            this.b.status = Constants.VIA_REPORT_TYPE_MAKE_FRIEND;
            this.b.actionId = 200;
        }
        return this.b;
    }
}
