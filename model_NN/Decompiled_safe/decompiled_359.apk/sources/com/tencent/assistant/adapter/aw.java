package com.tencent.assistant.adapter;

import com.tencent.assistant.component.invalidater.ViewInvalidateMessage;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessageHandler;

/* compiled from: ProGuard */
class aw extends ViewInvalidateMessageHandler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DownloadInfoMultiAdapter f709a;

    aw(DownloadInfoMultiAdapter downloadInfoMultiAdapter) {
        this.f709a = downloadInfoMultiAdapter;
    }

    public void handleMessage(ViewInvalidateMessage viewInvalidateMessage) {
        this.f709a.a(viewInvalidateMessage);
    }
}
