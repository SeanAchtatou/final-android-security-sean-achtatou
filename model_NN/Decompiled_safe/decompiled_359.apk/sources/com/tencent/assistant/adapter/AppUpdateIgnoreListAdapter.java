package com.tencent.assistant.adapter;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Pair;
import android.util.SparseBooleanArray;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.component.AppInfoPopupWindow;
import com.tencent.assistant.component.appdetail.CommentDetailTabView;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.u;
import com.tencent.assistant.module.update.k;
import com.tencent.assistant.protocol.jce.StatUpdateManageAction;
import com.tencent.assistantv2.activity.AppDetailActivityV5;
import com.tencent.assistantv2.st.b.b;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class AppUpdateIgnoreListAdapter extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    public View f668a;
    public SparseIntArray b = new SparseIntArray();
    public AppInfoPopupWindow c;
    private AstApp d = AstApp.i();
    /* access modifiers changed from: private */
    public Context e;
    private LayoutInflater f;
    private ArrayList<SimpleAppModel> g = new ArrayList<>();
    /* access modifiers changed from: private */
    public SparseBooleanArray h = new SparseBooleanArray();
    private StatUpdateManageAction i = null;
    private String j = null;
    private final int k = 10;
    private final int l = 2;
    /* access modifiers changed from: private */
    public String m;
    private b n = null;

    public AppUpdateIgnoreListAdapter(Context context, View view, StatUpdateManageAction statUpdateManageAction) {
        this.e = context;
        this.f = LayoutInflater.from(this.e);
        this.i = statUpdateManageAction;
        this.f668a = view;
    }

    public void a(String str) {
        this.j = str;
    }

    public void a() {
        Iterator<SimpleAppModel> it = this.g.iterator();
        while (it.hasNext()) {
            SimpleAppModel next = it.next();
            AppConst.AppState d2 = u.d(next);
            if (d2 == AppConst.AppState.QUEUING || d2 == AppConst.AppState.DOWNLOADING || d2 == AppConst.AppState.DOWNLOADED || d2 == AppConst.AppState.INSTALLING) {
                this.b.put(next.c.hashCode(), next.g);
            }
        }
    }

    public void a(List<SimpleAppModel> list, boolean z) {
        boolean z2;
        if (list != null && list.size() > 0) {
            if (this.g.size() == 0 || z) {
                this.g.clear();
                for (SimpleAppModel next : list) {
                    if (a(next)) {
                        this.g.add(next);
                    }
                }
            } else {
                ArrayList arrayList = new ArrayList();
                Iterator<SimpleAppModel> it = this.g.iterator();
                while (it.hasNext()) {
                    arrayList.add(Integer.valueOf(it.next().c.hashCode()));
                }
                ArrayList arrayList2 = new ArrayList();
                int i2 = 0;
                for (SimpleAppModel next2 : list) {
                    arrayList2.add(Integer.valueOf(next2.c.hashCode()));
                    if (!arrayList.contains(Integer.valueOf(next2.c.hashCode())) && a(next2)) {
                        this.g.add(i2, next2);
                    }
                    i2++;
                }
                Iterator<SimpleAppModel> it2 = this.g.iterator();
                while (it2.hasNext()) {
                    SimpleAppModel next3 = it2.next();
                    if (!arrayList2.contains(Integer.valueOf(next3.c.hashCode()))) {
                        if (this.b == null || this.b.indexOfKey(next3.c.hashCode()) < 0) {
                            z2 = false;
                        } else {
                            z2 = true;
                        }
                        if (!z2) {
                            it2.remove();
                        }
                    }
                }
            }
            notifyDataSetChanged();
        }
    }

    private boolean a(SimpleAppModel simpleAppModel) {
        return simpleAppModel != null && !TextUtils.isEmpty(simpleAppModel.c);
    }

    public void b() {
        if (this.c != null) {
            this.c.dismiss();
        }
    }

    public void c() {
        if (this.h != null) {
            this.h.clear();
            this.h = null;
        }
        if (this.c != null) {
            this.c.dismiss();
            this.c = null;
        }
        if (this.b != null) {
            this.b.clear();
            this.b = null;
        }
    }

    public int getCount() {
        if (this.g != null) {
            return this.g.size();
        }
        return 0;
    }

    /* renamed from: a */
    public SimpleAppModel getItem(int i2) {
        if (this.g != null) {
            return this.g.get(i2);
        }
        return null;
    }

    public long getItemId(int i2) {
        return 0;
    }

    public View getView(int i2, View view, ViewGroup viewGroup) {
        Pair<View, x> a2 = w.a(this.e, this.f, view, 2);
        View view2 = (View) a2.first;
        a((x) a2.second, i2);
        return view2;
    }

    private String b(int i2) {
        return a.a("03_", i2);
    }

    private void a(x xVar, int i2) {
        SimpleAppModel a2 = getItem(i2);
        if (xVar != null && a2 != null) {
            STInfoV2 a3 = a(a2, i2);
            xVar.f821a.setOnClickListener(new y(this, a2, a3));
            w.a(this.e, xVar, a2, a3);
            AppConst.AppState d2 = u.d(a2);
            if (d2 == AppConst.AppState.INSTALLED) {
                xVar.l.setVisibility(8);
            } else {
                xVar.l.setVisibility(0);
                xVar.l.setOnClickListener(new z(this, a2, a3));
            }
            if (d2 == AppConst.AppState.QUEUING || d2 == AppConst.AppState.DOWNLOADING || d2 == AppConst.AppState.DOWNLOADED || d2 == AppConst.AppState.INSTALLING) {
                this.b.put(a2.c.hashCode(), a2.g);
            }
            String str = a2.o;
            if (TextUtils.isEmpty(str)) {
                xVar.i.setVisibility(8);
                return;
            }
            String format = String.format(this.e.getResources().getString(R.string.update_feature), "\n" + str);
            xVar.k.setText(format);
            String replace = format.replaceAll("\r\n", "\n").replaceAll("\n\n", "\n").replace("\n", Constants.STR_EMPTY);
            xVar.i.setVisibility(0);
            xVar.i.setOnClickListener(new aa(this, a2, xVar, a3));
            xVar.j.setTag(Long.valueOf(a2.f1634a));
            xVar.j.a(new ab(this, xVar, a2.f1634a));
            xVar.j.setText(replace);
            if (this.m == null || !this.m.equals(a2.c)) {
                xVar.j.setVisibility(0);
                xVar.k.setVisibility(8);
                xVar.m.setImageDrawable(this.e.getResources().getDrawable(R.drawable.icon_open));
                return;
            }
            xVar.k.setVisibility(0);
            xVar.j.setVisibility(8);
            xVar.m.setImageDrawable(this.e.getResources().getDrawable(R.drawable.icon_close));
            notifyDataSetChanged();
        }
    }

    private void b(SimpleAppModel simpleAppModel) {
        if (this.h.indexOfKey(simpleAppModel.c.hashCode()) > 0) {
            this.h.clear();
        }
    }

    /* access modifiers changed from: private */
    public void c(SimpleAppModel simpleAppModel) {
        StatUpdateManageAction statUpdateManageAction = this.i;
        statUpdateManageAction.e = (short) (statUpdateManageAction.e + 1);
        Intent intent = new Intent(this.e, AppDetailActivityV5.class);
        intent.putExtra(CommentDetailTabView.PARAMS_SIMPLE_MODEL_INFO, simpleAppModel);
        this.e.startActivity(intent);
    }

    /* access modifiers changed from: private */
    public void d(SimpleAppModel simpleAppModel) {
        if (simpleAppModel != null) {
            k.b().b(simpleAppModel);
            b(simpleAppModel);
            this.g.remove(simpleAppModel);
            this.b.delete(simpleAppModel.c.hashCode());
            notifyDataSetChanged();
        }
    }

    private STInfoV2 a(SimpleAppModel simpleAppModel, int i2) {
        if (simpleAppModel == null) {
            return null;
        }
        if (this.n == null) {
            this.n = new b();
        }
        AppConst.AppState d2 = u.d(simpleAppModel);
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.e, simpleAppModel, b(i2), 100, a.a(d2, simpleAppModel));
        this.n.exposure(buildSTInfo);
        return buildSTInfo;
    }
}
