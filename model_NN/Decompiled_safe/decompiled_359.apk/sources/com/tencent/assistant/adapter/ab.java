package com.tencent.assistant.adapter;

import com.tencent.assistantv2.component.o;

/* compiled from: ProGuard */
class ab implements o {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ x f688a;
    final /* synthetic */ long b;
    final /* synthetic */ AppUpdateIgnoreListAdapter c;

    ab(AppUpdateIgnoreListAdapter appUpdateIgnoreListAdapter, x xVar, long j) {
        this.c = appUpdateIgnoreListAdapter;
        this.f688a = xVar;
        this.b = j;
    }

    public void a(boolean z) {
        Long l = (Long) this.f688a.j.getTag();
        if (l != null && this.b == l.longValue()) {
            if (!z) {
                this.f688a.m.setVisibility(8);
                this.f688a.i.setOnClickListener(null);
                return;
            }
            this.f688a.m.setVisibility(0);
        }
    }
}
