package com.tencent.assistant.adapter;

import android.content.Intent;
import android.view.View;
import com.tencent.assistant.component.appdetail.CommentDetailTabView;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistantv2.activity.AppDetailActivityV5;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class cd extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SimpleAppModel f737a;
    final /* synthetic */ STInfoV2 b;
    final /* synthetic */ OneMoreAdapter c;

    cd(OneMoreAdapter oneMoreAdapter, SimpleAppModel simpleAppModel, STInfoV2 sTInfoV2) {
        this.c = oneMoreAdapter;
        this.f737a = simpleAppModel;
        this.b = sTInfoV2;
    }

    public void onTMAClick(View view) {
        Intent intent = new Intent(this.c.f674a, AppDetailActivityV5.class);
        intent.putExtra(CommentDetailTabView.PARAMS_SIMPLE_MODEL_INFO, this.f737a);
        intent.putExtra("st_common_data", this.b);
        this.c.f674a.startActivity(intent);
    }

    public STInfoV2 getStInfo() {
        if (this.b != null) {
            this.b.actionId = 200;
            this.b.status = "01";
        }
        return this.b;
    }
}
