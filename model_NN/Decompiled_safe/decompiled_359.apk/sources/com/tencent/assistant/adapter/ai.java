package com.tencent.assistant.adapter;

import android.view.View;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistantv2.component.y;
import com.tencent.cloud.updaterec.g;
import java.util.ArrayList;

/* compiled from: ProGuard */
class ai extends y {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SimpleAppModel f695a;
    final /* synthetic */ int b;
    final /* synthetic */ int c;
    final /* synthetic */ ac d;

    ai(ac acVar, SimpleAppModel simpleAppModel, int i, int i2) {
        this.d = acVar;
        this.f695a = simpleAppModel;
        this.b = i;
        this.c = i2;
    }

    public void a(View view) {
        this.d.b(this.f695a);
    }

    public void a(View view, AppConst.AppState appState) {
        g a2;
        if (!((appState != AppConst.AppState.DOWNLOAD && appState != AppConst.AppState.UPDATE && appState != AppConst.AppState.PAUSED) || (a2 = this.d.f.a(((View) view.getParent()).getTag())) == null || a2.l == 2)) {
            this.d.a(this.f695a, view);
        }
        if (appState == AppConst.AppState.DOWNLOAD || appState == AppConst.AppState.UPDATE || appState == AppConst.AppState.DOWNLOADING || appState == AppConst.AppState.QUEUING || appState == AppConst.AppState.PAUSED || appState == AppConst.AppState.DOWNLOADED) {
            this.d.c.put(this.f695a.c.hashCode(), this.f695a.g);
        } else if (appState == AppConst.AppState.INSTALLED) {
            this.d.q.remove(this.f695a);
            this.d.c.delete(this.f695a.c.hashCode());
            ArrayList arrayList = (ArrayList) this.d.p.get(this.d.getGroup(this.b));
            if (arrayList != null && arrayList.size() > this.c) {
                arrayList.remove(this.c);
            }
            if (this.d.getGroup(this.b) != null) {
                ArrayList arrayList2 = (ArrayList) this.d.p.get((Integer) this.d.getGroup(this.b));
                if (arrayList2 == null || arrayList2.isEmpty()) {
                    this.d.o.remove(this.b);
                }
            }
            this.d.notifyDataSetChanged();
        } else if (appState == AppConst.AppState.INSTALLING) {
            this.d.notifyDataSetChanged();
        }
    }

    public int a() {
        return 1;
    }
}
