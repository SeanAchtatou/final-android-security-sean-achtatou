package com.tencent.assistant.adapter;

import android.os.Bundle;
import android.view.View;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.link.b;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class cy extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SimpleAppModel f757a;
    final /* synthetic */ int b;
    final /* synthetic */ int c;
    final /* synthetic */ cw d;

    cy(cw cwVar, SimpleAppModel simpleAppModel, int i, int i2) {
        this.d = cwVar;
        this.f757a = simpleAppModel;
        this.b = i;
        this.c = i2;
    }

    public void onTMAClick(View view) {
        Bundle bundle = new Bundle();
        if (this.d.c instanceof BaseActivity) {
            bundle.putInt(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, ((BaseActivity) this.d.c).f());
        }
        bundle.putSerializable("statInfo", this.d.k);
        bundle.putSerializable("com.tencent.assistant.ACTION_URL", this.f757a.aa);
        b.b(this.d.c, this.f757a.aa.f1970a, bundle);
    }

    public STInfoV2 getStInfo() {
        return STInfoBuilder.buildSTInfo(this.d.c, this.f757a, this.d.b(this.b, this.c), 200, null);
    }
}
