package com.tencent.assistant.adapter;

import android.widget.TextView;
import com.tencent.cloud.updaterec.g;
import com.tencent.cloud.updaterec.h;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
class aj implements h {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ac f696a;

    aj(ac acVar) {
        this.f696a = acVar;
    }

    public g a(Object obj) {
        if (obj != null && (obj instanceof x)) {
            x xVar = (x) obj;
            if (xVar.n != null) {
                return xVar.n;
            }
        }
        return null;
    }

    public g b(Object obj) {
        if (obj == null || !(obj instanceof x)) {
            return null;
        }
        return ((x) obj).n;
    }

    public TextView c(Object obj) {
        if (obj != null && (obj instanceof x)) {
            x xVar = (x) obj;
            if (xVar.n != null) {
                return xVar.n.h;
            }
        }
        return null;
    }

    public String a() {
        if (this.f696a.g == null || this.f696a.g.e() == null) {
            return Constants.STR_EMPTY;
        }
        return this.f696a.g.e().contentId;
    }
}
