package com.tencent.assistant.sdk;

import android.content.Context;
import android.text.TextUtils;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.manager.DownloadProxy;
import com.tencent.assistant.sdk.b.a;
import com.tencent.assistant.sdk.param.jce.IPCBaseParam;
import com.tencent.assistant.sdk.param.jce.IPCRequest;
import com.tencent.assistant.sdk.param.jce.QueryDownloadTaskRequest;
import com.tencent.assistant.sdk.param.jce.QueryDownloadTaskResponse;

/* compiled from: ProGuard */
public class v extends r {
    private QueryDownloadTaskRequest k;
    private QueryDownloadTaskResponse l;

    public v(Context context, IPCRequest iPCRequest) {
        super(context, iPCRequest);
    }

    public void a(JceStruct jceStruct) {
        if (jceStruct instanceof QueryDownloadTaskRequest) {
            this.k = (QueryDownloadTaskRequest) jceStruct;
            if (this.k != null) {
                this.b = this.k.f2484a;
            }
        }
    }

    /* access modifiers changed from: protected */
    public JceStruct a() {
        if (this.k == null || this.k.a() == null) {
            return null;
        }
        DownloadInfo a2 = p.a(this.k.a());
        if (a2 == null || a2.response == null) {
            LocalApkInfo localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(this.k.f2484a.d, Integer.valueOf(this.k.f2484a.c).intValue(), 0);
            if (localApkInfo == null) {
                return null;
            }
            this.l = new QueryDownloadTaskResponse();
            this.l.d = localApkInfo.occupySize;
            this.l.e = localApkInfo.occupySize;
            this.l.b = localApkInfo.mLocalFilePath;
            this.l.c = 4;
            this.l.f = DownloadProxy.a().l();
            this.l.g = DownloadProxy.a().k();
            return this.l;
        }
        this.l = new QueryDownloadTaskResponse();
        this.l.d = a2.response.f1263a;
        this.l.e = a2.response.b;
        this.l.b = a2.getCurrentValidPath();
        this.l.c = a.a(a2);
        this.l.f = DownloadProxy.a().l();
        this.l.g = DownloadProxy.a().k();
        return this.l;
    }

    /* access modifiers changed from: protected */
    public IPCBaseParam b() {
        if (this.k == null) {
            return null;
        }
        return this.k.a();
    }

    /* access modifiers changed from: protected */
    public boolean a(DownloadInfo downloadInfo) {
        return this.b != null && !TextUtils.isEmpty(this.b.c) && this.b.c.equals(String.valueOf(downloadInfo.versionCode)) && !TextUtils.isEmpty(this.b.d) && this.b.d.equals(downloadInfo.packageName);
    }
}
