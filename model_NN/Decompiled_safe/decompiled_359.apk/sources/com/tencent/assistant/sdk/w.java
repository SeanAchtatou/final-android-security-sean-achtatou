package com.tencent.assistant.sdk;

import android.content.Context;
import android.text.TextUtils;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.link.b;
import com.tencent.assistant.sdk.param.jce.IPCBaseParam;
import com.tencent.assistant.sdk.param.jce.IPCRequest;
import com.tencent.assistant.sdk.param.jce.URIActionRequest;

/* compiled from: ProGuard */
public class w extends r {
    private URIActionRequest k;

    public w(Context context, IPCRequest iPCRequest) {
        super(context, iPCRequest);
    }

    /* access modifiers changed from: protected */
    public void a(JceStruct jceStruct) {
        if (jceStruct instanceof URIActionRequest) {
            this.k = (URIActionRequest) jceStruct;
            String str = this.k.f2491a;
            if (!TextUtils.isEmpty(str)) {
                b.b(this.c, str);
            }
        }
    }

    /* access modifiers changed from: protected */
    public JceStruct a() {
        return null;
    }

    /* access modifiers changed from: protected */
    public IPCBaseParam b() {
        return null;
    }

    /* access modifiers changed from: protected */
    public boolean a(DownloadInfo downloadInfo) {
        return false;
    }
}
