package com.tencent.cloud.a;

import com.tencent.assistant.AppConst;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.download.SimpleDownloadInfo;
import com.tencent.assistant.manager.DownloadProxy;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.u;
import com.tencent.assistant.module.w;

/* compiled from: ProGuard */
class d implements w {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f3456a;

    d(a aVar) {
        this.f3456a = aVar;
    }

    public boolean a(SimpleAppModel simpleAppModel) {
        DownloadInfo a2 = DownloadProxy.a().a(simpleAppModel);
        AppConst.AppState d = u.d(simpleAppModel);
        return (a2 != null && a2.uiType == SimpleDownloadInfo.UIType.NORMAL) || d == AppConst.AppState.INSTALLED || d == AppConst.AppState.UPDATE;
    }
}
