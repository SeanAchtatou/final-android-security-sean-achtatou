package com.tencent.cloud.smartcard.c;

import android.util.Pair;
import com.tencent.assistant.manager.smartcard.y;
import com.tencent.assistant.model.a.i;
import com.tencent.assistant.model.a.r;
import java.util.List;

/* compiled from: ProGuard */
public class b extends y {
    public boolean a(i iVar, List<Long> list) {
        Pair<Boolean, r> b = b(iVar);
        if (iVar instanceof com.tencent.cloud.smartcard.b.b) {
            if (!((com.tencent.cloud.smartcard.b.b) iVar).l() && ((com.tencent.cloud.smartcard.b.b) iVar).f3464a != null && ((com.tencent.cloud.smartcard.b.b) iVar).f3464a.size() == 0) {
                return false;
            }
            if ((((com.tencent.cloud.smartcard.b.b) iVar).f3464a == null || ((com.tencent.cloud.smartcard.b.b) iVar).f3464a.size() == 0) && (((com.tencent.cloud.smartcard.b.b) iVar).e() == null || ((com.tencent.cloud.smartcard.b.b) iVar).e().size() == 0)) {
                return false;
            }
        }
        if (!((Boolean) b.first).booleanValue()) {
            return false;
        }
        return true;
    }
}
