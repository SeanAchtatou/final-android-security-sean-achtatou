package com.tencent.cloud.smartcard.view;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.component.smartcard.NormalSmartCardAppNode;
import com.tencent.assistant.component.smartcard.NormalSmartCardAppTinyNode;
import com.tencent.assistant.component.smartcard.NormalSmartcardBaseItem;
import com.tencent.assistant.component.smartcard.SmartcardListener;
import com.tencent.assistant.model.a.i;
import com.tencent.assistant.model.a.t;
import com.tencent.assistant.protocol.jce.SmartCardPicNode;
import com.tencent.assistant.utils.cn;
import com.tencent.assistant.utils.df;
import com.tencent.assistantv2.component.ListItemInfoView;
import com.tencent.assistantv2.st.b.d;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.cloud.smartcard.b.b;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class NormalSmartCardPicItem extends NormalSmartcardBaseItem {
    private RelativeLayout f;
    private TextView i;
    private TextView j;
    private LinearLayout k;
    private LinearLayout l;
    private NormalSmartCardAppNode m;
    private List<NormalSmartCardAppTinyNode> n;
    private List<NormalSmartCardPicItemPicNode> o;

    public NormalSmartCardPicItem(Context context, i iVar, SmartcardListener smartcardListener, IViewInvalidater iViewInvalidater) {
        super(context, iVar, smartcardListener, iViewInvalidater);
        setBackgroundResource(R.drawable.common_card_normal);
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.c = inflate(this.f1115a, R.layout.smartcard_pic_frame_layout, this);
        this.i = (TextView) this.c.findViewById(R.id.card_title);
        this.j = (TextView) this.c.findViewById(R.id.more_txt);
        this.f = (RelativeLayout) this.c.findViewById(R.id.card_title_layout);
        this.k = (LinearLayout) this.c.findViewById(R.id.smart_pic_layout);
        this.l = (LinearLayout) this.c.findViewById(R.id.card_app_list_layout);
        f();
    }

    private void f() {
        int i2;
        int i3;
        boolean z;
        b bVar = (b) this.smartcardModel;
        if (!TextUtils.isEmpty(bVar.k)) {
            this.i.setText(bVar.k);
            int a2 = cn.a(bVar.f());
            if (a2 != 0) {
                Drawable drawable = getResources().getDrawable(a2);
                drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
                this.i.setCompoundDrawables(drawable, null, null, null);
                this.i.setCompoundDrawablePadding(df.a(this.f1115a, 7.0f));
                this.i.setPadding(0, 0, 0, 0);
            } else {
                this.i.setCompoundDrawables(null, null, null, null);
                this.i.setPadding(df.a(this.f1115a, 7.0f), 0, 0, 0);
            }
            this.f.setVisibility(0);
            if (!TextUtils.isEmpty(bVar.o)) {
                this.j.setText(bVar.o);
                this.j.setOnClickListener(this.h);
                this.j.setVisibility(0);
            } else {
                this.j.setVisibility(8);
            }
        } else {
            this.f.setVisibility(8);
        }
        List<SmartCardPicNode> e = bVar.e();
        if (e == null || e.size() <= 0) {
            this.k.setVisibility(8);
        } else {
            int size = e.size();
            if (size > 3) {
                i3 = 3;
            } else {
                i3 = size;
            }
            if (this.o == null) {
                this.o = new ArrayList(i3);
                c(i3);
            } else if (this.o.size() != i3) {
                this.o.clear();
                this.k.removeAllViews();
                c(i3);
            }
            for (int i4 = 0; i4 < i3; i4++) {
                NormalSmartCardPicItemPicNode normalSmartCardPicItemPicNode = this.o.get(i4);
                SmartCardPicNode smartCardPicNode = e.get(i4);
                STInfoV2 a3 = a(a.a("04", i4), 200);
                int g = bVar.g();
                if (i3 == 3) {
                    z = true;
                } else {
                    z = false;
                }
                normalSmartCardPicItemPicNode.a(smartCardPicNode, a3, g, z, i3 >= 2);
            }
            this.k.setVisibility(0);
        }
        List<t> list = bVar.f3464a;
        if (list == null || list.size() == 0) {
            this.l.setVisibility(8);
            return;
        }
        this.l.setVisibility(0);
        if (list.size() < 3) {
            if (this.m == null) {
                g();
                this.m = new NormalSmartCardAppNode(this.f1115a, (int) R.layout.smartcard_applist_item_for_pic);
                this.m.setMinimumHeight(df.a(getContext(), 67.0f));
                this.l.addView(this.m);
                this.m.setData(list.get(0).f1653a, list.get(0).a(), a(list.get(0), 0), a(contextToPageId(this.f1115a)), false, ListItemInfoView.InfoType.DOWNTIMES_SIZE);
                return;
            }
            this.m.setData(list.get(0).f1653a, list.get(0).a(), a(list.get(0), 0), a(contextToPageId(this.f1115a)), false, ListItemInfoView.InfoType.DOWNTIMES_SIZE);
        } else if (list.size() >= 3) {
            int size2 = list.size();
            if (list.size() > 3) {
                i2 = 3;
            } else {
                i2 = size2;
            }
            if (this.n == null) {
                g();
                this.n = new ArrayList(3);
                for (int i5 = 0; i5 < i2; i5++) {
                    NormalSmartCardAppTinyNode normalSmartCardAppTinyNode = new NormalSmartCardAppTinyNode(this.f1115a);
                    this.n.add(normalSmartCardAppTinyNode);
                    normalSmartCardAppTinyNode.setPadding(0, df.a(getContext(), 12.0f), 0, df.a(getContext(), 2.0f));
                    this.l.addView(normalSmartCardAppTinyNode, new LinearLayout.LayoutParams(0, -2, 1.0f));
                    normalSmartCardAppTinyNode.setData(list.get(i5).f1653a, a(list.get(i5), i5), a(contextToPageId(this.f1115a)));
                }
                return;
            }
            for (int i6 = 0; i6 < i2; i6++) {
                this.n.get(i6).setData(list.get(i6).f1653a, a(list.get(i6), i6), a(contextToPageId(this.f1115a)));
            }
        }
    }

    private void g() {
        this.m = null;
        this.n = null;
        this.l.removeAllViews();
    }

    private void c(int i2) {
        for (int i3 = 0; i3 < i2; i3++) {
            NormalSmartCardPicItemPicNode normalSmartCardPicItemPicNode = new NormalSmartCardPicItemPicNode(this.f1115a);
            this.o.add(normalSmartCardPicItemPicNode);
            if (i2 > 1) {
                int a2 = df.a(this.f1115a, 7.5f);
                normalSmartCardPicItemPicNode.setPadding(a2, 0, a2, 0);
            }
            this.k.addView(normalSmartCardPicItemPicNode, new LinearLayout.LayoutParams(-2, -2, 1.0f));
        }
    }

    private STInfoV2 a(t tVar, int i2) {
        STInfoV2 a2 = a(a.a("05", i2), 100);
        if (!(a2 == null || tVar == null)) {
            a2.updateWithSimpleAppModel(tVar.f1653a);
        }
        if (this.e == null) {
            this.e = new d();
        }
        this.e.a(a2);
        return a2;
    }

    /* access modifiers changed from: protected */
    public void b() {
        f();
    }

    /* access modifiers changed from: protected */
    public String b(int i2) {
        if (this.smartcardModel instanceof b) {
            return ((b) this.smartcardModel).m();
        }
        return super.b(i2);
    }
}
