package com.tencent.open;

import com.tencent.open.a.n;
import java.lang.reflect.Method;
import java.util.List;

/* compiled from: ProGuard */
public class e {
    public void call(String str, List<String> list, d dVar) {
        Object invoke;
        Method[] declaredMethods = getClass().getDeclaredMethods();
        Method method = null;
        int length = declaredMethods.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                break;
            }
            Method method2 = declaredMethods[i];
            if (method2.getName().equals(str) && method2.getParameterTypes().length == list.size()) {
                method = method2;
                break;
            }
            i++;
        }
        if (method != null) {
            try {
                switch (list.size()) {
                    case 0:
                        invoke = method.invoke(this, new Object[0]);
                        break;
                    case 1:
                        invoke = method.invoke(this, list.get(0));
                        break;
                    case 2:
                        invoke = method.invoke(this, list.get(0), list.get(1));
                        break;
                    case 3:
                        invoke = method.invoke(this, list.get(0), list.get(1), list.get(2));
                        break;
                    case 4:
                        invoke = method.invoke(this, list.get(0), list.get(1), list.get(2), list.get(3));
                        break;
                    case 5:
                        invoke = method.invoke(this, list.get(0), list.get(1), list.get(2), list.get(3), list.get(4));
                        break;
                    default:
                        invoke = method.invoke(this, list.get(0), list.get(1), list.get(2), list.get(3), list.get(4), list.get(5));
                        break;
                }
                Class<?> returnType = method.getReturnType();
                n.b(c.b, "-->call, result: " + invoke + " | ReturnType: " + returnType.getName());
                if ("void".equals(returnType.getName()) || returnType == Void.class) {
                    if (dVar != null) {
                        dVar.a((Object) null);
                    }
                } else if (dVar != null && customCallback()) {
                    dVar.a(invoke != null ? invoke.toString() : null);
                }
            } catch (Exception e) {
                n.b(c.b, "-->handler call mehtod ex. targetMethod: " + method, e);
                if (dVar != null) {
                    dVar.a();
                }
            }
        } else if (dVar != null) {
            dVar.a();
        }
    }

    public boolean customCallback() {
        return false;
    }
}
