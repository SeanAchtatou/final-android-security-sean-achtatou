package com.tencent.tmsecurelite.d;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import com.tencent.tmsecurelite.commom.d;

/* compiled from: ProGuard */
public abstract class c extends Binder implements a {
    public c() {
        attachInterface(this, "com.tencent.tmsecurelite.IPaySecure");
    }

    public static a a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.tencent.tmsecurelite.IPaySecure");
        if (queryLocalInterface == null || !(queryLocalInterface instanceof a)) {
            return new b(iBinder);
        }
        return (a) queryLocalInterface;
    }

    /* access modifiers changed from: protected */
    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
        switch (i) {
            case 1:
                parcel.enforceInterface("com.tencent.tmsecurelite.IPaySecure");
                a(parcel.readInt() == 0, d.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
        }
        return true;
    }
}
