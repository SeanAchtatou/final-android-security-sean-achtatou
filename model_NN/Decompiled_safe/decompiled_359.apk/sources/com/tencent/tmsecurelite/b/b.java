package com.tencent.tmsecurelite.b;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import com.tencent.tmsecurelite.commom.DataEntity;
import com.tencent.tmsecurelite.commom.d;
import com.tencent.tmsecurelite.optimize.ISystemOptimize;
import java.util.ArrayList;

/* compiled from: ProGuard */
public abstract class b extends Binder implements c {
    public b() {
        attachInterface(this, "com.tencent.tmsecurelite.IDisturbIntercept");
    }

    public static c a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.tencent.tmsecurelite.IDisturbIntercept");
        if (queryLocalInterface == null || !(queryLocalInterface instanceof c)) {
            return new a(iBinder);
        }
        return (c) queryLocalInterface;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
        boolean z = false;
        switch (i) {
            case 1:
                parcel.enforceInterface("com.tencent.tmsecurelite.IDisturbIntercept");
                ArrayList<DataEntity> a2 = a(parcel.readInt());
                parcel2.writeNoException();
                DataEntity.writeToParcel(a2, parcel2);
                return true;
            case 2:
                parcel.enforceInterface("com.tencent.tmsecurelite.IDisturbIntercept");
                boolean a3 = a(parcel.readInt(), parcel.readInt());
                parcel2.writeNoException();
                if (a3) {
                    z = true;
                }
                parcel2.writeInt(z ? 1 : 0);
                return true;
            case 3:
                parcel.enforceInterface("com.tencent.tmsecurelite.IDisturbIntercept");
                int readInt = parcel.readInt();
                int readInt2 = parcel.readInt();
                if (parcel.readInt() == 1) {
                    z = true;
                }
                a(readInt, readInt2, z);
                parcel2.writeNoException();
                return true;
            case 4:
                parcel.enforceInterface("com.tencent.tmsecurelite.IDisturbIntercept");
                int readInt3 = parcel.readInt();
                int readInt4 = parcel.readInt();
                if (parcel.readInt() == 1) {
                    z = true;
                }
                b(readInt3, readInt4, z);
                parcel2.writeNoException();
                return true;
            case 5:
                parcel.enforceInterface("com.tencent.tmsecurelite.IDisturbIntercept");
                b(parcel.readInt());
                parcel2.writeNoException();
                return true;
            case 6:
                parcel.enforceInterface("com.tencent.tmsecurelite.IDisturbIntercept");
                ArrayList<DataEntity> c = c(parcel.readInt());
                parcel2.writeNoException();
                DataEntity.writeToParcel(c, parcel2);
                return true;
            case 7:
                parcel.enforceInterface("com.tencent.tmsecurelite.IDisturbIntercept");
                boolean a4 = a(parcel.readString(), parcel.readString(), parcel.readInt());
                parcel2.writeNoException();
                if (a4) {
                    z = true;
                }
                parcel2.writeInt(z ? 1 : 0);
                return true;
            case 8:
                parcel.enforceInterface("com.tencent.tmsecurelite.IDisturbIntercept");
                b(parcel.readInt(), parcel.readInt());
                parcel2.writeNoException();
                return true;
            case 9:
                parcel.enforceInterface("com.tencent.tmsecurelite.IDisturbIntercept");
                int a5 = a();
                parcel2.writeNoException();
                parcel2.writeInt(a5);
                return true;
            case 10:
                parcel.enforceInterface("com.tencent.tmsecurelite.IDisturbIntercept");
                int b = b();
                parcel2.writeNoException();
                parcel2.writeInt(b);
                return true;
            case 11:
                parcel.enforceInterface("com.tencent.tmsecurelite.IDisturbIntercept");
                c(parcel.readInt(), parcel.readInt());
                parcel2.writeNoException();
                return true;
            case 12:
                parcel.enforceInterface("com.tencent.tmsecurelite.IDisturbIntercept");
                int c2 = c();
                parcel2.writeNoException();
                parcel2.writeInt(c2);
                return true;
            case 13:
                parcel.enforceInterface("com.tencent.tmsecurelite.IDisturbIntercept");
                d(parcel.readInt());
                parcel2.writeNoException();
                return true;
            case 14:
                parcel.enforceInterface("com.tencent.tmsecurelite.IDisturbIntercept");
                boolean e = e(parcel.readInt());
                parcel2.writeNoException();
                if (e) {
                    z = true;
                }
                parcel2.writeInt(z ? 1 : 0);
                return true;
            case 15:
                parcel.enforceInterface("com.tencent.tmsecurelite.IDisturbIntercept");
                a(parcel.readInt(), d.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                return true;
            case 16:
                parcel.enforceInterface("com.tencent.tmsecurelite.IDisturbIntercept");
                a(parcel.readInt(), parcel.readInt(), d.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                return true;
            case 17:
                parcel.enforceInterface("com.tencent.tmsecurelite.IDisturbIntercept");
                int readInt5 = parcel.readInt();
                int readInt6 = parcel.readInt();
                if (parcel.readInt() == 1) {
                    z = true;
                }
                a(readInt5, readInt6, z, d.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                return true;
            case 18:
                parcel.enforceInterface("com.tencent.tmsecurelite.IDisturbIntercept");
                int readInt7 = parcel.readInt();
                int readInt8 = parcel.readInt();
                if (parcel.readInt() == 1) {
                    z = true;
                }
                b(readInt7, readInt8, z, d.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                return true;
            case 19:
                parcel.enforceInterface("com.tencent.tmsecurelite.IDisturbIntercept");
                b(parcel.readInt(), d.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                return true;
            case 20:
                parcel.enforceInterface("com.tencent.tmsecurelite.IDisturbIntercept");
                c(parcel.readInt(), d.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                return true;
            case 21:
                parcel.enforceInterface("com.tencent.tmsecurelite.IDisturbIntercept");
                a(parcel.readString(), parcel.readString(), parcel.readInt(), d.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                return true;
            case ISystemOptimize.T_killTaskAsync /*22*/:
                parcel.enforceInterface("com.tencent.tmsecurelite.IDisturbIntercept");
                b(parcel.readInt(), parcel.readInt(), d.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                return true;
            case 23:
                parcel.enforceInterface("com.tencent.tmsecurelite.IDisturbIntercept");
                a(d.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                return true;
            case ISystemOptimize.T_checkVersionAsync /*24*/:
                parcel.enforceInterface("com.tencent.tmsecurelite.IDisturbIntercept");
                b(d.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                return true;
            case ISystemOptimize.T_hasRootAsync /*25*/:
                parcel.enforceInterface("com.tencent.tmsecurelite.IDisturbIntercept");
                c(parcel.readInt(), parcel.readInt(), d.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                return true;
            case ISystemOptimize.T_askForRootAsync /*26*/:
                parcel.enforceInterface("com.tencent.tmsecurelite.IDisturbIntercept");
                c(d.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                return true;
            case ISystemOptimize.T_getMemoryUsageAsync /*27*/:
                parcel.enforceInterface("com.tencent.tmsecurelite.IDisturbIntercept");
                d(parcel.readInt(), d.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                return true;
            case ISystemOptimize.T_getSysRubbishAsync /*29*/:
                parcel.enforceInterface("com.tencent.tmsecurelite.IDisturbIntercept");
                d(d.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
        }
        return super.onTransact(i, parcel, parcel2, i2);
    }

    public boolean e(int i) {
        return 1 >= i;
    }
}
