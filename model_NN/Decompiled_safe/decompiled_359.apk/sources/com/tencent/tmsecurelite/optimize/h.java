package com.tencent.tmsecurelite.optimize;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import com.tencent.tmsecurelite.commom.DataEntity;
import com.tencent.tmsecurelite.commom.b;
import com.tencent.tmsecurelite.commom.d;
import java.util.ArrayList;

/* compiled from: ProGuard */
public abstract class h extends Binder implements ISystemOptimize {
    public h() {
        attachInterface(this, ISystemOptimize.INTERFACE);
    }

    public static ISystemOptimize a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface(ISystemOptimize.INTERFACE);
        if (queryLocalInterface == null || !(queryLocalInterface instanceof ISystemOptimize)) {
            return new g(iBinder);
        }
        return (ISystemOptimize) queryLocalInterface;
    }

    /* access modifiers changed from: protected */
    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
        boolean z;
        boolean z2;
        boolean z3 = false;
        switch (i) {
            case 1:
                parcel.enforceInterface(ISystemOptimize.INTERFACE);
                ArrayList<DataEntity> findAppsWithCache = findAppsWithCache();
                parcel2.writeNoException();
                DataEntity.writeToParcel(findAppsWithCache, parcel2);
                return true;
            case 2:
                parcel.enforceInterface(ISystemOptimize.INTERFACE);
                boolean clearAppsCache = clearAppsCache();
                parcel2.writeNoException();
                if (!clearAppsCache) {
                    z3 = true;
                }
                parcel2.writeInt(z3 ? 1 : 0);
                return true;
            case 3:
                parcel.enforceInterface(ISystemOptimize.INTERFACE);
                if (parcel.readInt() == 0) {
                    z3 = true;
                }
                ArrayList<DataEntity> findAppsWithAutoboot = findAppsWithAutoboot(z3);
                parcel2.writeNoException();
                DataEntity.writeToParcel(findAppsWithAutoboot, parcel2);
                return true;
            case 4:
                parcel.enforceInterface(ISystemOptimize.INTERFACE);
                String readString = parcel.readString();
                if (parcel.readInt() == 0) {
                    z2 = true;
                } else {
                    z2 = false;
                }
                boolean autobootState = setAutobootState(readString, z2);
                parcel2.writeNoException();
                if (!autobootState) {
                    z3 = true;
                }
                parcel2.writeInt(z3 ? 1 : 0);
                return true;
            case 5:
                parcel.enforceInterface(ISystemOptimize.INTERFACE);
                ArrayList<String> createStringArrayList = parcel.createStringArrayList();
                if (parcel.readInt() == 0) {
                    z = true;
                } else {
                    z = false;
                }
                boolean autobootStates = setAutobootStates(createStringArrayList, z);
                parcel2.writeNoException();
                if (!autobootStates) {
                    z3 = true;
                }
                parcel2.writeInt(z3 ? 1 : 0);
                return true;
            case 6:
                parcel.enforceInterface(ISystemOptimize.INTERFACE);
                if (parcel.readInt() == 0) {
                    z3 = true;
                }
                ArrayList<DataEntity> allRuningTask = getAllRuningTask(z3);
                parcel2.writeNoException();
                DataEntity.writeToParcel(allRuningTask, parcel2);
                return true;
            case 7:
                parcel.enforceInterface(ISystemOptimize.INTERFACE);
                boolean killTask = killTask(parcel.readString());
                parcel2.writeNoException();
                if (!killTask) {
                    z3 = true;
                }
                parcel2.writeInt(z3 ? 1 : 0);
                return true;
            case 8:
                parcel.enforceInterface(ISystemOptimize.INTERFACE);
                boolean killTasks = killTasks(parcel.createStringArrayList());
                parcel2.writeNoException();
                if (!killTasks) {
                    z3 = true;
                }
                parcel2.writeInt(z3 ? 1 : 0);
                return true;
            case 9:
                parcel.enforceInterface(ISystemOptimize.INTERFACE);
                boolean checkVersion = checkVersion(parcel.readInt());
                parcel2.writeNoException();
                if (!checkVersion) {
                    z3 = true;
                }
                parcel2.writeInt(z3 ? 1 : 0);
                return true;
            case 10:
                parcel.enforceInterface(ISystemOptimize.INTERFACE);
                boolean hasRoot = hasRoot();
                parcel2.writeNoException();
                if (!hasRoot) {
                    z3 = true;
                }
                parcel2.writeInt(z3 ? 1 : 0);
                return true;
            case 11:
                parcel.enforceInterface(ISystemOptimize.INTERFACE);
                boolean askForRoot = askForRoot();
                parcel2.writeNoException();
                if (!askForRoot) {
                    z3 = true;
                }
                parcel2.writeInt(z3 ? 1 : 0);
                return true;
            case 12:
                parcel.enforceInterface(ISystemOptimize.INTERFACE);
                int memoryUsage = getMemoryUsage();
                parcel2.writeNoException();
                parcel2.writeInt(memoryUsage);
                return true;
            case 13:
                parcel.enforceInterface(ISystemOptimize.INTERFACE);
                boolean isMemoryReachWarning = isMemoryReachWarning();
                parcel2.writeNoException();
                if (!isMemoryReachWarning) {
                    z3 = true;
                }
                parcel2.writeInt(z3 ? 1 : 0);
                return true;
            case 14:
                parcel.enforceInterface(ISystemOptimize.INTERFACE);
                getSysRubbishSize(d.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                return true;
            case 15:
                parcel.enforceInterface(ISystemOptimize.INTERFACE);
                cleanSysRubbish();
                parcel2.writeNoException();
                return true;
            case 16:
                parcel.enforceInterface(ISystemOptimize.INTERFACE);
                findAppsWithCacheAsync(d.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                return true;
            case 17:
                parcel.enforceInterface(ISystemOptimize.INTERFACE);
                clearAppsCacheAsync(d.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                return true;
            case 18:
                parcel.enforceInterface(ISystemOptimize.INTERFACE);
                if (parcel.readInt() == 0) {
                    z3 = true;
                }
                findAppsWithAutobootAsync(z3, d.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                return true;
            case 19:
                parcel.enforceInterface(ISystemOptimize.INTERFACE);
                String readString2 = parcel.readString();
                if (parcel.readInt() == 0) {
                    z3 = true;
                }
                setAutobootStateAsync(readString2, z3, d.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                return true;
            case 20:
                parcel.enforceInterface(ISystemOptimize.INTERFACE);
                ArrayList<String> createStringArrayList2 = parcel.createStringArrayList();
                if (parcel.readInt() == 0) {
                    z3 = true;
                }
                setAutobootStatesAsync(createStringArrayList2, z3, d.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                return true;
            case 21:
                parcel.enforceInterface(ISystemOptimize.INTERFACE);
                if (parcel.readInt() == 0) {
                    z3 = true;
                }
                getAllRuningTaskAsync(z3, d.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                return true;
            case ISystemOptimize.T_killTaskAsync:
                parcel.enforceInterface(ISystemOptimize.INTERFACE);
                killTaskAsync(parcel.readString(), d.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                return true;
            case 23:
                parcel.enforceInterface(ISystemOptimize.INTERFACE);
                killTasksAsync(parcel.createStringArrayList(), d.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                return true;
            case ISystemOptimize.T_checkVersionAsync:
            default:
                return super.onTransact(i, parcel, parcel2, i2);
            case ISystemOptimize.T_hasRootAsync:
                parcel.enforceInterface(ISystemOptimize.INTERFACE);
                hasRootAsync(d.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                return true;
            case ISystemOptimize.T_askForRootAsync:
                parcel.enforceInterface(ISystemOptimize.INTERFACE);
                askForRootAsync(d.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                return true;
            case ISystemOptimize.T_getMemoryUsageAsync:
                parcel.enforceInterface(ISystemOptimize.INTERFACE);
                getMemoryUsageAsync(d.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                return true;
            case ISystemOptimize.T_isMemoryReachWarningAsync:
                parcel.enforceInterface(ISystemOptimize.INTERFACE);
                isMemoryReachWarningAsync(d.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                return true;
            case ISystemOptimize.T_getSysRubbishAsync:
                parcel.enforceInterface(ISystemOptimize.INTERFACE);
                int sysRubbishSizeAsync = getSysRubbishSizeAsync(d.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                parcel2.writeInt(sysRubbishSizeAsync);
                return true;
            case 30:
                parcel.enforceInterface(ISystemOptimize.INTERFACE);
                cleanSysRubbishAsync(d.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                return true;
            case 31:
                parcel.enforceInterface(ISystemOptimize.INTERFACE);
                updateTmsConfigAsync(d.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                return true;
            case 32:
                parcel.enforceInterface(ISystemOptimize.INTERFACE);
                startScanRubbish(f.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                return true;
            case ISystemOptimize.T_cancelScanRubbish:
                parcel.enforceInterface(ISystemOptimize.INTERFACE);
                int cancelScanRubbish = cancelScanRubbish();
                parcel2.writeNoException();
                parcel2.writeInt(cancelScanRubbish);
                return true;
            case ISystemOptimize.T_cleanRubbishAsync:
                parcel.enforceInterface(ISystemOptimize.INTERFACE);
                b a2 = d.a(parcel.readStrongBinder());
                ArrayList arrayList = new ArrayList();
                parcel.readStringList(arrayList);
                cleanRubbishAsync(a2, arrayList);
                parcel2.writeNoException();
                return true;
        }
    }

    public boolean checkVersion(int i) {
        return 2 >= i;
    }
}
