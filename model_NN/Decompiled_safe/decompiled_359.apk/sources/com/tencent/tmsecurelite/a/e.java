package com.tencent.tmsecurelite.a;

import android.os.IBinder;
import android.os.Parcel;

/* compiled from: ProGuard */
public final class e implements o {

    /* renamed from: a  reason: collision with root package name */
    private IBinder f3810a;

    public e(IBinder iBinder) {
        this.f3810a = iBinder;
    }

    public IBinder asBinder() {
        return this.f3810a;
    }

    public void a(int i) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInt(i);
            this.f3810a.transact(1, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }
}
