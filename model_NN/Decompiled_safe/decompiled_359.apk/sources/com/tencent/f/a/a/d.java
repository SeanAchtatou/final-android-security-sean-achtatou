package com.tencent.f.a.a;

import android.support.v4.view.MotionEventCompat;

/* compiled from: ProGuard */
public final class d implements Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private int f3666a;

    public d(byte[] bArr) {
        this(bArr, 0);
    }

    public d(byte[] bArr, int i) {
        this.f3666a = (bArr[i + 1] << 8) & MotionEventCompat.ACTION_POINTER_INDEX_MASK;
        this.f3666a += bArr[i] & 255;
    }

    public d(int i) {
        this.f3666a = i;
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof d) || this.f3666a != ((d) obj).b()) {
            return false;
        }
        return true;
    }

    public byte[] a() {
        return new byte[]{(byte) (this.f3666a & 255), (byte) ((this.f3666a & MotionEventCompat.ACTION_POINTER_INDEX_MASK) >> 8)};
    }

    public int b() {
        return this.f3666a;
    }

    public int hashCode() {
        return this.f3666a;
    }
}
