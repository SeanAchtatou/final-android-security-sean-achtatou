package com.tencent.beacon.event;

import android.content.Context;
import com.tencent.beacon.a.g;
import com.tencent.beacon.a.h;
import com.tencent.beacon.d.a;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public final class f {

    /* renamed from: a  reason: collision with root package name */
    private static f f3433a = null;
    private String b = Constants.STR_EMPTY;
    private String c = Constants.STR_EMPTY;
    private String d = Constants.STR_EMPTY;
    private String e = Constants.STR_EMPTY;
    private String f = Constants.STR_EMPTY;

    public static synchronized f a(Context context) {
        f fVar;
        synchronized (f.class) {
            if (f3433a == null) {
                f3433a = new f(context);
            }
            fVar = f3433a;
        }
        return fVar;
    }

    public final synchronized String a() {
        return this.b;
    }

    public final synchronized String b() {
        return this.c;
    }

    public final synchronized String c() {
        return this.d;
    }

    public final synchronized String d() {
        return this.e;
    }

    public final synchronized String e() {
        return this.f;
    }

    private f(Context context) {
        if (context == null) {
            a.d(" DetailUserInfo context == null? pls check!", new Object[0]);
        }
        a.b(" start to create DetailUserInfo", new Object[0]);
        long currentTimeMillis = System.currentTimeMillis();
        h.a(context);
        g m = g.m();
        this.b = h.b(context);
        a.b(" imei:" + this.b, new Object[0]);
        if (!Constants.STR_EMPTY.equals(this.b)) {
            try {
                if (Constants.STR_EMPTY.equals(com.tencent.beacon.a.a.b(context, "IMEI_DENGTA", Constants.STR_EMPTY))) {
                    com.tencent.beacon.a.a.a(context, "IMEI_DENGTA", this.b);
                }
            } catch (Exception e2) {
            }
            if (Constants.STR_EMPTY.equals(m.i())) {
                m.b(this.b);
            }
        }
        this.c = h.e(context);
        this.d = h.c(context);
        this.e = h.d(context);
        String k = m.k();
        if (k == null || Constants.STR_EMPTY.equals(k)) {
            this.f = h.b(com.tencent.beacon.a.a.b(context));
            m.c(this.f);
        } else {
            this.f = h.b(k);
        }
        a.b(" detail create cost: %d  values:\n %s", Long.valueOf(System.currentTimeMillis() - currentTimeMillis), toString());
    }
}
