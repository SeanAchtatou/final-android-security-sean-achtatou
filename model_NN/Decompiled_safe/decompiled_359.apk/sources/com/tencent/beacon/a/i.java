package com.tencent.beacon.a;

import android.content.Context;
import android.net.TrafficStats;
import android.os.Process;
import com.tencent.beacon.a.a.d;
import com.tencent.beacon.d.b;
import com.tencent.beacon.f.a;
import com.tencent.beacon.f.k;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/* compiled from: ProGuard */
public final class i implements a {
    private static i e = null;

    /* renamed from: a  reason: collision with root package name */
    private d f3400a;
    private d b;
    private d c;
    private Context d = null;

    private i(Context context) {
        this.d = context;
        k.a(this.d).a(this);
        d();
        if (e() > 0) {
            f();
        }
    }

    public static synchronized i a(Context context) {
        i iVar;
        synchronized (i.class) {
            if (e == null) {
                e = new i(context);
            }
            iVar = e;
        }
        return iVar;
    }

    public static d b(Context context) {
        return a(context).b();
    }

    public static d c(Context context) {
        return a(context).a();
    }

    public static void a(Context context, d dVar) {
        dVar.i = dVar.m;
        dVar.h = dVar.l;
        dVar.g = dVar.e;
        dVar.f = dVar.d;
        a(context).c(dVar);
    }

    private d a() {
        try {
            int myUid = Process.myUid();
            long uidRxBytes = TrafficStats.getUidRxBytes(myUid);
            long uidTxBytes = TrafficStats.getUidTxBytes(myUid);
            List<d> n = h.n(this.d);
            if (n != null) {
                for (d dVar : n) {
                    this.c = dVar;
                }
            }
            if (this.c == null) {
                this.c = new d(2, System.currentTimeMillis(), 0, 0, 0, 0, 0);
                this.c.m = uidRxBytes;
                this.c.l = uidTxBytes;
                c(this.c);
            } else if (!(uidRxBytes == this.c.m && uidTxBytes == this.c.l)) {
                boolean a2 = b.a(this.d);
                this.c.b = System.currentTimeMillis();
                if (uidRxBytes - this.c.m > 0) {
                    this.c.k = uidRxBytes - this.c.m;
                } else {
                    this.c.k = uidRxBytes;
                }
                if (uidTxBytes - this.c.l > 0) {
                    this.c.j = uidTxBytes - this.c.l;
                } else {
                    this.c.j = uidTxBytes;
                }
                if (a2) {
                    this.c.d = this.c.d + this.c.k + this.c.j;
                } else {
                    this.c.e = this.c.e + this.c.k + this.c.j;
                }
                this.c.m = uidRxBytes;
                this.c.l = uidTxBytes;
                c(this.c);
                return this.c;
            }
            return this.c;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public static void d(Context context) {
        i a2 = a(context);
        long time = new Date().getTime();
        d b2 = a2.b();
        if (b2 != null && b2.a() >= 0) {
            h.b(a2.d, new d[]{b2});
        }
        a2.a(new d(0, time, 0, 0, 0, 0, 0));
    }

    public static d e(Context context) {
        return a(context).c();
    }

    public final void a(int i, int i2, long j, long j2, boolean z, String str) {
        com.tencent.beacon.d.a.h(" req:%d  res:%d  send:%d  recv:%d  result: %b  msg:%s", Integer.valueOf(i), Integer.valueOf(i2), Long.valueOf(j), Long.valueOf(j2), Boolean.valueOf(z), str);
        if (i != 5) {
            d();
            e();
            a(j, j2, b.a(this.d));
            f();
            com.tencent.beacon.d.a.f(" [total:%s] \n[today:%s]", b(), c());
        }
    }

    private synchronized d b() {
        return this.f3400a;
    }

    private synchronized void a(d dVar) {
        this.f3400a = dVar;
    }

    private synchronized d c() {
        e();
        return this.b;
    }

    private synchronized void b(d dVar) {
        this.b = dVar;
    }

    private void d() {
        List<d> m = h.m(this.d);
        if (m != null) {
            for (d next : m) {
                if (next.f3384a == 0) {
                    a(next);
                } else if (next.f3384a == 1) {
                    b(next);
                }
            }
        }
    }

    private synchronized int e() {
        int i;
        long f = com.tencent.beacon.b.a.f();
        long time = new Date().getTime();
        int i2 = 0;
        if (this.b == null || this.b.b < f) {
            this.b = new d(1, time, 0, 0, 0, 0, 0);
            i2 = 1;
        }
        if (this.f3400a == null) {
            this.f3400a = new d(0, time, 0, 0, 0, 0, 0);
            i = i2 + 1;
        } else {
            i = i2;
        }
        return i;
    }

    private synchronized void a(long j, long j2, boolean z) {
        long time = new Date().getTime();
        long j3 = j + j2;
        long j4 = z ? j3 : 0;
        if (z) {
            j3 = 0;
        }
        if (this.b == null) {
            this.b = new d(1, time, 1, j4, j3, j, j2);
        } else {
            long a2 = this.b.a();
            this.b = new d(1, this.b.b, 1 + this.b.c, this.b.d + j4, this.b.e + j3, this.b.j + j, this.b.k + j2);
            this.b.a(a2);
        }
        if (this.f3400a == null) {
            this.f3400a = new d(0, time, 1, j4, j3, j, j2);
        } else {
            long a3 = this.f3400a.a();
            this.f3400a = new d(0, this.f3400a.b, this.f3400a.c + 1, j4 + this.f3400a.d, j3 + this.f3400a.e, this.f3400a.j + j, this.f3400a.k + j2);
            this.f3400a.a(a3);
        }
    }

    private void f() {
        ArrayList arrayList = new ArrayList();
        d b2 = b();
        if (b2 != null) {
            arrayList.add(b2);
        }
        d c2 = c();
        if (c2 != null) {
            arrayList.add(c2);
        }
        if (arrayList.size() > 0) {
            h.a(this.d, (d[]) arrayList.toArray(new d[arrayList.size()]));
        }
    }

    private void c(d dVar) {
        ArrayList arrayList = new ArrayList();
        if (dVar != null) {
            arrayList.add(dVar);
        }
        if (arrayList.size() > 0) {
            h.c(this.d, (d[]) arrayList.toArray(new d[arrayList.size()]));
        }
    }
}
