package com.tencent.beacon.a.b;

import android.content.Context;
import com.tencent.beacon.f.i;

/* compiled from: ProGuard */
public final class a implements i {

    /* renamed from: a  reason: collision with root package name */
    private Context f3387a = null;

    public a(Context context) {
        this.f3387a = context;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:79:0x01c8, code lost:
        if (r2.d().equals(r1.e) == false) goto L_0x01ca;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x01f9, code lost:
        if (r2.g().equals(r1.g) == false) goto L_0x01fb;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(int r16, byte[] r17, boolean r18) {
        /*
            r15 = this;
            r1 = 101(0x65, float:1.42E-43)
            r0 = r16
            if (r0 == r1) goto L_0x0016
            java.lang.String r1 = "com strategy unmatch key: %d"
            r2 = 1
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r3 = 0
            java.lang.Integer r4 = java.lang.Integer.valueOf(r16)
            r2[r3] = r4
            com.tencent.beacon.d.a.c(r1, r2)
        L_0x0015:
            return
        L_0x0016:
            if (r17 == 0) goto L_0x0015
            r0 = r17
            int r1 = r0.length
            if (r1 <= 0) goto L_0x0015
            android.content.Context r1 = r15.f3387a
            com.tencent.beacon.a.b.c r1 = com.tencent.beacon.a.b.c.a(r1)
            com.tencent.beacon.a.b.g r7 = r1.b()
            if (r7 != 0) goto L_0x0032
            java.lang.String r1 = "imposible! common strategy null!"
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]
            com.tencent.beacon.d.a.c(r1, r2)
            goto L_0x0015
        L_0x0032:
            com.tencent.beacon.c.e.a r8 = new com.tencent.beacon.c.e.a     // Catch:{ Throwable -> 0x007a }
            r8.<init>()     // Catch:{ Throwable -> 0x007a }
            com.tencent.beacon.e.a r1 = new com.tencent.beacon.e.a     // Catch:{ Throwable -> 0x007a }
            r0 = r17
            r1.<init>(r0)     // Catch:{ Throwable -> 0x007a }
            r8.a(r1)     // Catch:{ Throwable -> 0x007a }
            if (r8 == 0) goto L_0x0045
            if (r7 != 0) goto L_0x0087
        L_0x0045:
            r1 = 0
        L_0x0046:
            if (r1 == 0) goto L_0x0015
            if (r18 == 0) goto L_0x0068
            java.lang.String r1 = "update common strategy should save "
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Throwable -> 0x007a }
            com.tencent.beacon.d.a.e(r1, r2)     // Catch:{ Throwable -> 0x007a }
            android.content.Context r1 = r15.f3387a     // Catch:{ Throwable -> 0x007a }
            if (r17 == 0) goto L_0x0068
            com.tencent.beacon.a.b.k r2 = new com.tencent.beacon.a.b.k     // Catch:{ Throwable -> 0x007a }
            r2.<init>()     // Catch:{ Throwable -> 0x007a }
            r0 = r16
            r2.a(r0)     // Catch:{ Throwable -> 0x007a }
            r0 = r17
            r2.a(r0)     // Catch:{ Throwable -> 0x007a }
            com.tencent.beacon.a.h.a(r1, r2)     // Catch:{ Throwable -> 0x007a }
        L_0x0068:
            java.lang.String r1 = "com strategy changed notify! "
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Throwable -> 0x007a }
            com.tencent.beacon.d.a.e(r1, r2)     // Catch:{ Throwable -> 0x007a }
            android.content.Context r1 = r15.f3387a     // Catch:{ Throwable -> 0x007a }
            com.tencent.beacon.a.b.c r1 = com.tencent.beacon.a.b.c.a(r1)     // Catch:{ Throwable -> 0x007a }
            r1.a(r7)     // Catch:{ Throwable -> 0x007a }
            goto L_0x0015
        L_0x007a:
            r1 = move-exception
            r1.printStackTrace()
            java.lang.String r1 = "error to common strategy!"
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]
            com.tencent.beacon.d.a.d(r1, r2)
            goto L_0x0015
        L_0x0087:
            r4 = 0
            java.lang.String r1 = r8.c     // Catch:{ Throwable -> 0x007a }
            java.lang.String r2 = r7.b()     // Catch:{ Throwable -> 0x007a }
            boolean r1 = r1.equals(r2)     // Catch:{ Throwable -> 0x007a }
            if (r1 != 0) goto L_0x00a7
            java.lang.String r1 = "strategy url changed to: %s"
            r2 = 1
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Throwable -> 0x007a }
            r3 = 0
            java.lang.String r4 = r8.c     // Catch:{ Throwable -> 0x007a }
            r2[r3] = r4     // Catch:{ Throwable -> 0x007a }
            com.tencent.beacon.d.a.b(r1, r2)     // Catch:{ Throwable -> 0x007a }
            r4 = 1
            java.lang.String r1 = r8.c     // Catch:{ Throwable -> 0x007a }
            r7.a(r1)     // Catch:{ Throwable -> 0x007a }
        L_0x00a7:
            int r1 = r8.b     // Catch:{ Throwable -> 0x007a }
            int r2 = r7.c()     // Catch:{ Throwable -> 0x007a }
            if (r1 == r2) goto L_0x00c6
            java.lang.String r1 = "QueryPeriod changed to: %d"
            r2 = 1
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Throwable -> 0x007a }
            r3 = 0
            int r4 = r8.b     // Catch:{ Throwable -> 0x007a }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Throwable -> 0x007a }
            r2[r3] = r4     // Catch:{ Throwable -> 0x007a }
            com.tencent.beacon.d.a.b(r1, r2)     // Catch:{ Throwable -> 0x007a }
            r4 = 1
            int r1 = r8.b     // Catch:{ Throwable -> 0x007a }
            r7.a(r1)     // Catch:{ Throwable -> 0x007a }
        L_0x00c6:
            java.util.ArrayList<com.tencent.beacon.c.e.d> r9 = r8.f3418a     // Catch:{ Throwable -> 0x007a }
            if (r7 != 0) goto L_0x00d8
            r1 = 0
        L_0x00cb:
            if (r1 == 0) goto L_0x02e7
            r2 = 1
        L_0x00ce:
            java.util.Map<java.lang.String, java.lang.String> r3 = r8.d     // Catch:{ Throwable -> 0x007a }
            if (r7 != 0) goto L_0x02bd
            r1 = 0
        L_0x00d3:
            if (r1 == 0) goto L_0x02e4
            r1 = 1
            goto L_0x0046
        L_0x00d8:
            r1 = 0
            if (r9 == 0) goto L_0x0284
            android.util.SparseArray r10 = r7.e()     // Catch:{ Throwable -> 0x007a }
            if (r10 == 0) goto L_0x00cb
            r2 = 0
            r5 = r2
            r3 = r1
        L_0x00e4:
            int r1 = r10.size()     // Catch:{ Throwable -> 0x007a }
            if (r5 >= r1) goto L_0x02ed
            java.lang.Object r1 = r10.valueAt(r5)     // Catch:{ Throwable -> 0x007a }
            r0 = r1
            com.tencent.beacon.a.b.h r0 = (com.tencent.beacon.a.b.h) r0     // Catch:{ Throwable -> 0x007a }
            r2 = r0
            java.util.Iterator r11 = r9.iterator()     // Catch:{ Throwable -> 0x007a }
        L_0x00f6:
            boolean r1 = r11.hasNext()     // Catch:{ Throwable -> 0x007a }
            if (r1 == 0) goto L_0x027f
            java.lang.Object r1 = r11.next()     // Catch:{ Throwable -> 0x007a }
            com.tencent.beacon.c.e.d r1 = (com.tencent.beacon.c.e.d) r1     // Catch:{ Throwable -> 0x007a }
            byte r6 = r1.f3421a     // Catch:{ Throwable -> 0x007a }
            int r12 = r2.f()     // Catch:{ Throwable -> 0x007a }
            if (r6 != r12) goto L_0x0231
            java.lang.String r6 = "server module strategy mid: %d"
            r12 = 1
            java.lang.Object[] r12 = new java.lang.Object[r12]     // Catch:{ Throwable -> 0x007a }
            r13 = 0
            byte r14 = r1.f3421a     // Catch:{ Throwable -> 0x007a }
            java.lang.Byte r14 = java.lang.Byte.valueOf(r14)     // Catch:{ Throwable -> 0x007a }
            r12[r13] = r14     // Catch:{ Throwable -> 0x007a }
            com.tencent.beacon.d.a.a(r6, r12)     // Catch:{ Throwable -> 0x007a }
            byte r6 = r1.b     // Catch:{ Throwable -> 0x007a }
            r12 = 1
            if (r6 != r12) goto L_0x0235
            r6 = 1
        L_0x0121:
            boolean r12 = r2.a()     // Catch:{ Throwable -> 0x007a }
            if (r12 == r6) goto L_0x0143
            java.lang.String r3 = "mid: %d , isUsable changed: %b "
            r12 = 2
            java.lang.Object[] r12 = new java.lang.Object[r12]     // Catch:{ Throwable -> 0x007a }
            r13 = 0
            byte r14 = r1.f3421a     // Catch:{ Throwable -> 0x007a }
            java.lang.Byte r14 = java.lang.Byte.valueOf(r14)     // Catch:{ Throwable -> 0x007a }
            r12[r13] = r14     // Catch:{ Throwable -> 0x007a }
            r13 = 1
            java.lang.Boolean r14 = java.lang.Boolean.valueOf(r6)     // Catch:{ Throwable -> 0x007a }
            r12[r13] = r14     // Catch:{ Throwable -> 0x007a }
            com.tencent.beacon.d.a.b(r3, r12)     // Catch:{ Throwable -> 0x007a }
            r3 = 1
            r2.a(r6)     // Catch:{ Throwable -> 0x007a }
        L_0x0143:
            java.lang.String r6 = r2.b()     // Catch:{ Throwable -> 0x007a }
            java.lang.String r12 = r1.c     // Catch:{ Throwable -> 0x007a }
            boolean r6 = r6.equals(r12)     // Catch:{ Throwable -> 0x007a }
            if (r6 != 0) goto L_0x016b
            java.lang.String r3 = "mid: %d , url changed: %s"
            r6 = 2
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ Throwable -> 0x007a }
            r12 = 0
            byte r13 = r1.f3421a     // Catch:{ Throwable -> 0x007a }
            java.lang.Byte r13 = java.lang.Byte.valueOf(r13)     // Catch:{ Throwable -> 0x007a }
            r6[r12] = r13     // Catch:{ Throwable -> 0x007a }
            r12 = 1
            java.lang.String r13 = r1.c     // Catch:{ Throwable -> 0x007a }
            r6[r12] = r13     // Catch:{ Throwable -> 0x007a }
            com.tencent.beacon.d.a.b(r3, r6)     // Catch:{ Throwable -> 0x007a }
            r3 = 1
            java.lang.String r6 = r1.c     // Catch:{ Throwable -> 0x007a }
            r2.a(r6)     // Catch:{ Throwable -> 0x007a }
        L_0x016b:
            byte r6 = r1.f3421a     // Catch:{ Throwable -> 0x007a }
            r12 = 1
            if (r6 == r12) goto L_0x0175
            byte r6 = r1.f3421a     // Catch:{ Throwable -> 0x007a }
            r12 = 2
            if (r6 != r12) goto L_0x01af
        L_0x0175:
            java.util.Map r6 = r2.c()     // Catch:{ Throwable -> 0x007a }
            if (r6 == 0) goto L_0x0238
            java.util.Map<java.lang.String, java.lang.String> r6 = r1.d     // Catch:{ Throwable -> 0x007a }
            if (r6 == 0) goto L_0x0238
            java.util.Map r6 = r2.c()     // Catch:{ Throwable -> 0x007a }
            java.util.Map<java.lang.String, java.lang.String> r12 = r1.d     // Catch:{ Throwable -> 0x007a }
            boolean r6 = r6.equals(r12)     // Catch:{ Throwable -> 0x007a }
            if (r6 != 0) goto L_0x01af
            java.lang.String r3 = "mid: %d , detail changed..."
            r6 = 1
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ Throwable -> 0x007a }
            r12 = 0
            byte r13 = r1.f3421a     // Catch:{ Throwable -> 0x007a }
            java.lang.Byte r13 = java.lang.Byte.valueOf(r13)     // Catch:{ Throwable -> 0x007a }
            r6[r12] = r13     // Catch:{ Throwable -> 0x007a }
            com.tencent.beacon.d.a.b(r3, r6)     // Catch:{ Throwable -> 0x007a }
            r3 = 1
            java.util.Map<java.lang.String, java.lang.String> r6 = r1.d     // Catch:{ Throwable -> 0x007a }
            r2.a(r6)     // Catch:{ Throwable -> 0x007a }
            android.content.Context r6 = r15.f3387a     // Catch:{ Throwable -> 0x007a }
            com.tencent.beacon.a.b.c r6 = com.tencent.beacon.a.b.c.a(r6)     // Catch:{ Throwable -> 0x007a }
            byte r12 = r1.f3421a     // Catch:{ Throwable -> 0x007a }
            java.util.Map<java.lang.String, java.lang.String> r13 = r1.d     // Catch:{ Throwable -> 0x007a }
            r6.a(r12, r13)     // Catch:{ Throwable -> 0x007a }
        L_0x01af:
            byte r6 = r1.f3421a     // Catch:{ Throwable -> 0x007a }
            r12 = 1
            if (r6 != r12) goto L_0x0216
            java.util.Set r6 = r2.d()     // Catch:{ Throwable -> 0x007a }
            if (r6 == 0) goto L_0x0268
            java.util.ArrayList<java.lang.String> r6 = r1.e     // Catch:{ Throwable -> 0x007a }
            if (r6 == 0) goto L_0x0268
            java.util.Set r6 = r2.d()     // Catch:{ Throwable -> 0x007a }
            java.util.ArrayList<java.lang.String> r12 = r1.e     // Catch:{ Throwable -> 0x007a }
            boolean r6 = r6.equals(r12)     // Catch:{ Throwable -> 0x007a }
            if (r6 != 0) goto L_0x01e5
        L_0x01ca:
            java.lang.String r3 = "mid: %d , PreventEventCode changed..."
            r6 = 1
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ Throwable -> 0x007a }
            r12 = 0
            byte r13 = r1.f3421a     // Catch:{ Throwable -> 0x007a }
            java.lang.Byte r13 = java.lang.Byte.valueOf(r13)     // Catch:{ Throwable -> 0x007a }
            r6[r12] = r13     // Catch:{ Throwable -> 0x007a }
            com.tencent.beacon.d.a.b(r3, r6)     // Catch:{ Throwable -> 0x007a }
            r3 = 1
            java.util.ArrayList<java.lang.String> r6 = r1.e     // Catch:{ Throwable -> 0x007a }
            java.util.HashSet r6 = com.tencent.beacon.b.a.a(r6)     // Catch:{ Throwable -> 0x007a }
            r2.a(r6)     // Catch:{ Throwable -> 0x007a }
        L_0x01e5:
            java.util.Set r6 = r2.g()     // Catch:{ Throwable -> 0x007a }
            if (r6 == 0) goto L_0x0274
            java.util.ArrayList<java.lang.String> r6 = r1.g     // Catch:{ Throwable -> 0x007a }
            if (r6 == 0) goto L_0x0274
            java.util.Set r6 = r2.g()     // Catch:{ Throwable -> 0x007a }
            java.util.ArrayList<java.lang.String> r12 = r1.g     // Catch:{ Throwable -> 0x007a }
            boolean r6 = r6.equals(r12)     // Catch:{ Throwable -> 0x007a }
            if (r6 != 0) goto L_0x0216
        L_0x01fb:
            java.lang.String r3 = "mid: %d , SampleEventSet changed..."
            r6 = 1
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ Throwable -> 0x007a }
            r12 = 0
            byte r13 = r1.f3421a     // Catch:{ Throwable -> 0x007a }
            java.lang.Byte r13 = java.lang.Byte.valueOf(r13)     // Catch:{ Throwable -> 0x007a }
            r6[r12] = r13     // Catch:{ Throwable -> 0x007a }
            com.tencent.beacon.d.a.b(r3, r6)     // Catch:{ Throwable -> 0x007a }
            r3 = 1
            java.util.ArrayList<java.lang.String> r6 = r1.g     // Catch:{ Throwable -> 0x007a }
            java.util.HashSet r6 = com.tencent.beacon.b.a.a(r6)     // Catch:{ Throwable -> 0x007a }
            r2.b(r6)     // Catch:{ Throwable -> 0x007a }
        L_0x0216:
            byte r6 = r1.f3421a     // Catch:{ Throwable -> 0x007a }
            r12 = 2
            if (r6 != r12) goto L_0x0231
            java.lang.String r6 = "mid: %d , SpeedMonitorStrategy"
            r12 = 1
            java.lang.Object[] r12 = new java.lang.Object[r12]     // Catch:{ Throwable -> 0x007a }
            r13 = 0
            byte r14 = r1.f3421a     // Catch:{ Throwable -> 0x007a }
            java.lang.Byte r14 = java.lang.Byte.valueOf(r14)     // Catch:{ Throwable -> 0x007a }
            r12[r13] = r14     // Catch:{ Throwable -> 0x007a }
            com.tencent.beacon.d.a.b(r6, r12)     // Catch:{ Throwable -> 0x007a }
            com.tencent.beacon.c.e.e r1 = r1.f     // Catch:{ Throwable -> 0x007a }
            r2.a(r1)     // Catch:{ Throwable -> 0x007a }
        L_0x0231:
            r1 = r3
            r3 = r1
            goto L_0x00f6
        L_0x0235:
            r6 = 0
            goto L_0x0121
        L_0x0238:
            java.util.Map<java.lang.String, java.lang.String> r6 = r1.d     // Catch:{ Throwable -> 0x007a }
            if (r6 == 0) goto L_0x01af
            java.util.Map r6 = r2.c()     // Catch:{ Throwable -> 0x007a }
            if (r6 != 0) goto L_0x01af
            java.lang.String r3 = "mid: %d , detail changed..."
            r6 = 1
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ Throwable -> 0x007a }
            r12 = 0
            byte r13 = r1.f3421a     // Catch:{ Throwable -> 0x007a }
            java.lang.Byte r13 = java.lang.Byte.valueOf(r13)     // Catch:{ Throwable -> 0x007a }
            r6[r12] = r13     // Catch:{ Throwable -> 0x007a }
            com.tencent.beacon.d.a.b(r3, r6)     // Catch:{ Throwable -> 0x007a }
            r3 = 1
            java.util.Map<java.lang.String, java.lang.String> r6 = r1.d     // Catch:{ Throwable -> 0x007a }
            r2.a(r6)     // Catch:{ Throwable -> 0x007a }
            android.content.Context r6 = r15.f3387a     // Catch:{ Throwable -> 0x007a }
            com.tencent.beacon.a.b.c r6 = com.tencent.beacon.a.b.c.a(r6)     // Catch:{ Throwable -> 0x007a }
            byte r12 = r1.f3421a     // Catch:{ Throwable -> 0x007a }
            java.util.Map<java.lang.String, java.lang.String> r13 = r1.d     // Catch:{ Throwable -> 0x007a }
            r6.a(r12, r13)     // Catch:{ Throwable -> 0x007a }
            goto L_0x01af
        L_0x0268:
            java.util.Set r6 = r2.d()     // Catch:{ Throwable -> 0x007a }
            if (r6 != 0) goto L_0x01ca
            java.util.ArrayList<java.lang.String> r6 = r1.e     // Catch:{ Throwable -> 0x007a }
            if (r6 != 0) goto L_0x01ca
            goto L_0x01e5
        L_0x0274:
            java.util.Set r6 = r2.g()     // Catch:{ Throwable -> 0x007a }
            if (r6 != 0) goto L_0x01fb
            java.util.ArrayList<java.lang.String> r6 = r1.g     // Catch:{ Throwable -> 0x007a }
            if (r6 != 0) goto L_0x01fb
            goto L_0x0216
        L_0x027f:
            int r1 = r5 + 1
            r5 = r1
            goto L_0x00e4
        L_0x0284:
            android.util.SparseArray r5 = r7.e()     // Catch:{ Throwable -> 0x007a }
            if (r5 == 0) goto L_0x00cb
            int r6 = r5.size()     // Catch:{ Throwable -> 0x007a }
            r2 = 0
            r3 = r2
            r2 = r1
        L_0x0291:
            if (r3 >= r6) goto L_0x02ea
            java.lang.Object r1 = r5.valueAt(r3)     // Catch:{ Throwable -> 0x007a }
            com.tencent.beacon.a.b.h r1 = (com.tencent.beacon.a.b.h) r1     // Catch:{ Throwable -> 0x007a }
            boolean r9 = r1.a()     // Catch:{ Throwable -> 0x007a }
            if (r9 == 0) goto L_0x02b7
            java.lang.String r2 = "mid: %d , server not response strategy, sdk local close it!"
            r9 = 1
            java.lang.Object[] r9 = new java.lang.Object[r9]     // Catch:{ Throwable -> 0x007a }
            r10 = 0
            int r11 = r1.f()     // Catch:{ Throwable -> 0x007a }
            java.lang.Integer r11 = java.lang.Integer.valueOf(r11)     // Catch:{ Throwable -> 0x007a }
            r9[r10] = r11     // Catch:{ Throwable -> 0x007a }
            com.tencent.beacon.d.a.b(r2, r9)     // Catch:{ Throwable -> 0x007a }
            r2 = 1
            r9 = 0
            r1.a(r9)     // Catch:{ Throwable -> 0x007a }
        L_0x02b7:
            r1 = r2
            int r2 = r3 + 1
            r3 = r2
            r2 = r1
            goto L_0x0291
        L_0x02bd:
            r1 = 0
            if (r3 == 0) goto L_0x02d6
            java.util.Map r4 = r7.d()     // Catch:{ Throwable -> 0x007a }
            if (r4 == 0) goto L_0x02d6
            java.util.Map r4 = r7.d()     // Catch:{ Throwable -> 0x007a }
            boolean r4 = r3.equals(r4)     // Catch:{ Throwable -> 0x007a }
            if (r4 != 0) goto L_0x00d3
            r7.a(r3)     // Catch:{ Throwable -> 0x007a }
            r1 = 1
            goto L_0x00d3
        L_0x02d6:
            if (r3 == 0) goto L_0x00d3
            java.util.Map r4 = r7.d()     // Catch:{ Throwable -> 0x007a }
            if (r4 != 0) goto L_0x00d3
            r7.a(r3)     // Catch:{ Throwable -> 0x007a }
            r1 = 1
            goto L_0x00d3
        L_0x02e4:
            r1 = r2
            goto L_0x0046
        L_0x02e7:
            r2 = r4
            goto L_0x00ce
        L_0x02ea:
            r1 = r2
            goto L_0x00cb
        L_0x02ed:
            r1 = r3
            goto L_0x00cb
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.beacon.a.b.a.a(int, byte[], boolean):void");
    }
}
