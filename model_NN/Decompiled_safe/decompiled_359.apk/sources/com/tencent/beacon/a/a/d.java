package com.tencent.beacon.a.a;

import java.io.Serializable;
import java.util.Locale;

/* compiled from: ProGuard */
public class d implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    public int f3384a;
    public long b;
    public long c;
    public long d;
    public long e;
    public long f;
    public long g;
    public long h;
    public long i;
    public long j;
    public long k;
    public long l;
    public long m;
    private long n = -1;

    public d(int i2, long j2, long j3, long j4, long j5, long j6, long j7) {
        this.f3384a = i2;
        this.b = j2;
        this.c = j3;
        this.d = j4;
        this.e = j5;
        this.j = j6;
        this.k = j7;
    }

    public String toString() {
        try {
            return String.format(Locale.US, "[tp:%d , st:%d ,counts:%d, wifi:%d , notWifi:%d , up:%d , dn:%d]", Integer.valueOf(this.f3384a), Long.valueOf(this.b), Long.valueOf(this.c), Long.valueOf(this.d), Long.valueOf(this.e), Long.valueOf(this.j), Long.valueOf(this.k));
        } catch (Throwable th) {
            th.printStackTrace();
            return null;
        }
    }

    public final synchronized long a() {
        return this.n;
    }

    public final synchronized void a(long j2) {
        this.n = j2;
    }
}
