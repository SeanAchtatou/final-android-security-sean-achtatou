package com.tencent.feedback.proguard;

import com.tencent.connect.common.Constants;

public final class G extends C0008j {

    /* renamed from: a  reason: collision with root package name */
    public String f3707a = Constants.STR_EMPTY;
    public String b = Constants.STR_EMPTY;
    private String c = Constants.STR_EMPTY;
    private String d = Constants.STR_EMPTY;
    private String e = Constants.STR_EMPTY;
    private String f = Constants.STR_EMPTY;

    public final void a(ag agVar) {
        this.f3707a = agVar.b(0, true);
        this.c = agVar.b(1, true);
        this.b = agVar.b(2, true);
        this.d = agVar.b(3, true);
        this.e = agVar.b(4, true);
        this.f = agVar.b(5, false);
    }

    public final void a(ah ahVar) {
        ahVar.a(this.f3707a, 0);
        ahVar.a(this.c, 1);
        ahVar.a(this.b, 2);
        ahVar.a(this.d, 3);
        ahVar.a(this.e, 4);
        if (this.f != null) {
            ahVar.a(this.f, 5);
        }
    }

    public final void a(StringBuilder sb, int i) {
    }
}
