package com.tencent.feedback.common;

import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.os.Process;
import android.util.Log;
import com.tencent.assistant.st.STConst;
import com.tencent.connect.common.Constants;
import com.tencent.feedback.proguard.ac;
import java.util.List;

/* compiled from: ProGuard */
public final class b {

    /* renamed from: a  reason: collision with root package name */
    private static String f3677a = null;
    private static Boolean b = null;
    private static Boolean c = null;

    public static String a(Context context) {
        if (context == null) {
            return Constants.STR_EMPTY;
        }
        try {
            Object obj = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData.get("APPKEY_DENGTA");
            if (obj != null) {
                return obj.toString();
            }
        } catch (Throwable th) {
            g.a("rqdp{  no appkey !!}", new Object[0]);
        }
        return Constants.STR_EMPTY;
    }

    public static String b(Context context) {
        if (context == null) {
            return null;
        }
        return context.getPackageName();
    }

    public static synchronized String c(Context context) {
        String str;
        int i = 0;
        synchronized (b.class) {
            try {
                PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context == null ? null : context.getPackageName(), 0);
                String str2 = packageInfo.versionName;
                int i2 = packageInfo.versionCode;
                if (str2 == null || str2.trim().length() <= 0) {
                    str = Constants.STR_EMPTY;
                } else {
                    char[] charArray = str2.toCharArray();
                    for (char c2 : charArray) {
                        if (c2 == '.') {
                            i++;
                        }
                    }
                    if (i < 3) {
                        str = str2 + "." + i2;
                    } else {
                        str = str2;
                    }
                    g.a("rqdp{  version:} %s", str);
                }
            } catch (Throwable th) {
                th.printStackTrace();
                g.d(th.toString(), new Object[0]);
                str = null;
            }
        }
        return str;
    }

    public static String d(Context context) {
        g.b("rqdp{AppInfo.getUUID() Start}", new Object[0]);
        if (context == null) {
            g.d("context == null", new Object[0]);
            return Constants.STR_EMPTY;
        }
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (applicationInfo == null || applicationInfo.metaData == null) {
                g.d("appInfo == null || appInfo.metaData == null", new Object[0]);
                return Constants.STR_EMPTY;
            }
            Object obj = applicationInfo.metaData.get("com.tencent.rdm.uuid");
            String obj2 = obj != null ? obj.toString() : Constants.STR_EMPTY;
            Log.d("rqdp{ RDMUUID }:%s", obj2);
            return obj2;
        } catch (Throwable th) {
            th.printStackTrace();
            g.d(th.toString(), new Object[0]);
            return Constants.STR_EMPTY;
        }
    }

    public static synchronized boolean e(Context context) {
        boolean z = false;
        synchronized (b.class) {
            g.b("rqdp{  Read Log Permittion! start}", new Object[0]);
            if (context != null) {
                if (b == null) {
                    b = Boolean.valueOf(c(context, "android.permission.READ_LOGS"));
                }
                z = b.booleanValue();
            }
        }
        return z;
    }

    public static synchronized boolean f(Context context) {
        boolean z = false;
        synchronized (b.class) {
            g.b("rqdp{  Read write Permittion! start}", new Object[0]);
            if (context != null) {
                if (c == null) {
                    c = Boolean.valueOf(c(context, "android.permission.WRITE_EXTERNAL_STORAGE"));
                }
                z = c.booleanValue();
            }
        }
        return z;
    }

    private static boolean c(Context context, String str) {
        String str2;
        g.b("rqdp{  AppInfo.isContainReadLogPermission() start}", new Object[0]);
        if (context == null || str == null || str.trim().length() <= 0) {
            return false;
        }
        try {
            String[] strArr = context.getPackageManager().getPackageInfo(context.getPackageName(), 4096).requestedPermissions;
            if (strArr != null) {
                for (String equals : strArr) {
                    if (str.equals(equals)) {
                        g.b("rqdp{  AppInfo.isContainReadLogPermission() end}", new Object[0]);
                        return true;
                    }
                }
            }
            return false;
        } catch (Throwable th) {
            th.printStackTrace();
            return false;
        } finally {
            str2 = "rqdp{  AppInfo.isContainReadLogPermission() end}";
            g.b(str2, new Object[0]);
        }
    }

    public static String a(Context context, String str) {
        if (context == null || str == null) {
            return null;
        }
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(str, 0);
            if (applicationInfo != null) {
                return applicationInfo.sourceDir;
            }
            return null;
        } catch (Throwable th) {
            th.printStackTrace();
            return null;
        }
    }

    public static String g(Context context) {
        if (context == null) {
            return null;
        }
        String a2 = a(context, context.getPackageName());
        if (a2 != null) {
            return ac.a(a2);
        }
        g.d("rqdp{  No found the apk file on the device,please check it!}", new Object[0]);
        return null;
    }

    public static boolean b(Context context, String str) {
        if (context == null || str == null || str.trim().length() <= 0) {
            return false;
        }
        try {
            List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = ((ActivityManager) context.getSystemService("activity")).getRunningAppProcesses();
            if (runningAppProcesses == null || runningAppProcesses.size() == 0) {
                g.b("rqdp{  no running proc}", new Object[0]);
                return false;
            }
            for (ActivityManager.RunningAppProcessInfo next : runningAppProcesses) {
                if (next.importance == 100) {
                    for (String equals : next.pkgList) {
                        if (str.equals(equals)) {
                            g.b("rqdp{  current seen pn:}%s", next.processName);
                            return true;
                        }
                    }
                    continue;
                }
            }
            g.b("rqdp{  current unseen pn:}%s", str);
            return false;
        } catch (Throwable th) {
            th.printStackTrace();
            g.d("rqdp{  Failed to judge }[%s]", th.getLocalizedMessage());
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0072, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0073, code lost:
        r0.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:?, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:?, code lost:
        return null;
     */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x00e1 A[SYNTHETIC, Splitter:B:54:0x00e1] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.util.SparseArray<java.lang.String> a(int r14) {
        /*
            r5 = 8
            r1 = 0
            r3 = 0
            r12 = 1
            android.util.SparseArray r2 = new android.util.SparseArray     // Catch:{ Exception -> 0x00fd, all -> 0x00f8 }
            r2.<init>()     // Catch:{ Exception -> 0x00fd, all -> 0x00f8 }
            java.lang.Runtime r0 = java.lang.Runtime.getRuntime()     // Catch:{ Exception -> 0x00fd, all -> 0x00f8 }
            java.lang.String r4 = "ps"
            java.lang.Process r4 = r0.exec(r4)     // Catch:{ Exception -> 0x00fd, all -> 0x00f8 }
            if (r4 == 0) goto L_0x00e5
            java.io.BufferedReader r0 = new java.io.BufferedReader     // Catch:{ Exception -> 0x00fd, all -> 0x00f8 }
            java.io.InputStreamReader r6 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x00fd, all -> 0x00f8 }
            java.io.InputStream r4 = r4.getInputStream()     // Catch:{ Exception -> 0x00fd, all -> 0x00f8 }
            r6.<init>(r4)     // Catch:{ Exception -> 0x00fd, all -> 0x00f8 }
            r0.<init>(r6)     // Catch:{ Exception -> 0x00fd, all -> 0x00f8 }
            java.lang.String r4 = "\\S+\\s+\\d+\\s+\\d+\\s+\\d+\\s+\\d+\\s+\\S{8}\\s+\\S{8}\\s+\\S\\s+.+"
            java.util.regex.Pattern r6 = java.util.regex.Pattern.compile(r4)     // Catch:{ Exception -> 0x00a8, all -> 0x00db }
        L_0x002a:
            java.lang.String r4 = r0.readLine()     // Catch:{ Exception -> 0x00a8, all -> 0x00db }
            if (r4 == 0) goto L_0x00e6
            int r3 = r3 + 1
            if (r3 == r12) goto L_0x002a
            java.util.regex.Matcher r7 = r6.matcher(r4)     // Catch:{ Exception -> 0x00a8, all -> 0x00db }
            boolean r7 = r7.matches()     // Catch:{ Exception -> 0x00a8, all -> 0x00db }
            if (r7 != 0) goto L_0x0052
            java.lang.String r2 = "ps result not supported format %s"
            r3 = 1
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Exception -> 0x00a8, all -> 0x00db }
            r5 = 0
            r3[r5] = r4     // Catch:{ Exception -> 0x00a8, all -> 0x00db }
            com.tencent.feedback.common.g.c(r2, r3)     // Catch:{ Exception -> 0x00a8, all -> 0x00db }
            r0.close()     // Catch:{ IOException -> 0x004d }
        L_0x004c:
            return r1
        L_0x004d:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x004c
        L_0x0052:
            java.lang.String r7 = r4.trim()     // Catch:{ Exception -> 0x00a8, all -> 0x00db }
            java.lang.String r8 = "\\s+"
            java.lang.String[] r7 = r7.split(r8)     // Catch:{ Exception -> 0x00a8, all -> 0x00db }
            if (r7 == 0) goto L_0x0063
            int r8 = r7.length     // Catch:{ Exception -> 0x00a8, all -> 0x00db }
            r9 = 9
            if (r8 >= r9) goto L_0x0077
        L_0x0063:
            java.lang.String r2 = "ps result not supported format %s"
            r3 = 1
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Exception -> 0x00a8, all -> 0x00db }
            r5 = 0
            r3[r5] = r4     // Catch:{ Exception -> 0x00a8, all -> 0x00db }
            com.tencent.feedback.common.g.c(r2, r3)     // Catch:{ Exception -> 0x00a8, all -> 0x00db }
            r0.close()     // Catch:{ IOException -> 0x0072 }
            goto L_0x004c
        L_0x0072:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x004c
        L_0x0077:
            r4 = 0
            r4 = r7[r4]     // Catch:{ Exception -> 0x00a8, all -> 0x00db }
            int r4 = android.os.Process.getUidForName(r4)     // Catch:{ Exception -> 0x00a8, all -> 0x00db }
            if (r4 != r14) goto L_0x002a
            java.lang.StringBuffer r8 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x00a8, all -> 0x00db }
            r8.<init>()     // Catch:{ Exception -> 0x00a8, all -> 0x00db }
            r4 = r5
        L_0x0086:
            int r9 = r7.length     // Catch:{ Exception -> 0x00a8, all -> 0x00db }
            if (r4 >= r9) goto L_0x00ba
            if (r4 <= r5) goto L_0x00a2
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00a8, all -> 0x00db }
            java.lang.String r10 = " "
            r9.<init>(r10)     // Catch:{ Exception -> 0x00a8, all -> 0x00db }
            r10 = r7[r4]     // Catch:{ Exception -> 0x00a8, all -> 0x00db }
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ Exception -> 0x00a8, all -> 0x00db }
            java.lang.String r9 = r9.toString()     // Catch:{ Exception -> 0x00a8, all -> 0x00db }
            r8.append(r9)     // Catch:{ Exception -> 0x00a8, all -> 0x00db }
        L_0x009f:
            int r4 = r4 + 1
            goto L_0x0086
        L_0x00a2:
            r9 = r7[r4]     // Catch:{ Exception -> 0x00a8, all -> 0x00db }
            r8.append(r9)     // Catch:{ Exception -> 0x00a8, all -> 0x00db }
            goto L_0x009f
        L_0x00a8:
            r2 = move-exception
            r13 = r2
            r2 = r0
            r0 = r13
        L_0x00ac:
            r0.printStackTrace()     // Catch:{ all -> 0x00fa }
            if (r2 == 0) goto L_0x004c
            r2.close()     // Catch:{ IOException -> 0x00b5 }
            goto L_0x004c
        L_0x00b5:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x004c
        L_0x00ba:
            java.lang.String r4 = r8.toString()     // Catch:{ Exception -> 0x00a8, all -> 0x00db }
            r8 = 1
            r8 = r7[r8]     // Catch:{ Exception -> 0x00a8, all -> 0x00db }
            int r8 = java.lang.Integer.parseInt(r8)     // Catch:{ Exception -> 0x00a8, all -> 0x00db }
            r2.put(r8, r4)     // Catch:{ Exception -> 0x00a8, all -> 0x00db }
            java.lang.String r8 = "same uid %s %s"
            r9 = 2
            java.lang.Object[] r9 = new java.lang.Object[r9]     // Catch:{ Exception -> 0x00a8, all -> 0x00db }
            r10 = 0
            r11 = 1
            r7 = r7[r11]     // Catch:{ Exception -> 0x00a8, all -> 0x00db }
            r9[r10] = r7     // Catch:{ Exception -> 0x00a8, all -> 0x00db }
            r7 = 1
            r9[r7] = r4     // Catch:{ Exception -> 0x00a8, all -> 0x00db }
            com.tencent.feedback.common.g.b(r8, r9)     // Catch:{ Exception -> 0x00a8, all -> 0x00db }
            goto L_0x002a
        L_0x00db:
            r1 = move-exception
            r13 = r1
            r1 = r0
            r0 = r13
        L_0x00df:
            if (r1 == 0) goto L_0x00e4
            r1.close()     // Catch:{ IOException -> 0x00f3 }
        L_0x00e4:
            throw r0
        L_0x00e5:
            r0 = r1
        L_0x00e6:
            if (r0 == 0) goto L_0x00eb
            r0.close()     // Catch:{ IOException -> 0x00ee }
        L_0x00eb:
            r1 = r2
            goto L_0x004c
        L_0x00ee:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00eb
        L_0x00f3:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00e4
        L_0x00f8:
            r0 = move-exception
            goto L_0x00df
        L_0x00fa:
            r0 = move-exception
            r1 = r2
            goto L_0x00df
        L_0x00fd:
            r0 = move-exception
            r2 = r1
            goto L_0x00ac
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.feedback.common.b.a(int):android.util.SparseArray");
    }

    public static String a(Context context, int i) {
        for (ActivityManager.RunningAppProcessInfo next : ((ActivityManager) context.getSystemService("activity")).getRunningAppProcesses()) {
            if (next.pid == i) {
                return next.processName;
            }
        }
        return STConst.ST_INSTALL_FAIL_STR_UNKNOWN;
    }

    public static String h(Context context) {
        try {
            return a(context, Process.myPid());
        } catch (Throwable th) {
            th.printStackTrace();
            return null;
        }
    }
}
