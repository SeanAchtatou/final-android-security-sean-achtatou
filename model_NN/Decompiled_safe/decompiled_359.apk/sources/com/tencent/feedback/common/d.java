package com.tencent.feedback.common;

import android.util.SparseArray;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/* compiled from: ProGuard */
final class d extends c {
    private ScheduledExecutorService b;
    private SparseArray<ScheduledFuture<?>> c;

    public d() {
        this(Executors.newScheduledThreadPool(3));
    }

    public d(ScheduledExecutorService scheduledExecutorService) {
        this.b = null;
        this.c = null;
        if (scheduledExecutorService == null || scheduledExecutorService.isShutdown()) {
            throw new IllegalArgumentException("ScheduledExecutorService is not valiable!");
        }
        this.b = scheduledExecutorService;
        this.c = new SparseArray<>();
    }

    public final synchronized boolean a(Runnable runnable) {
        boolean z = false;
        synchronized (this) {
            if (!b()) {
                g.d("rqdp{  ScheduleTaskHandlerImp was closed , should not post!}", new Object[0]);
            } else if (runnable == null) {
                g.d("rqdp{  task runner should not be null}", new Object[0]);
            } else {
                this.b.execute(runnable);
                z = true;
            }
        }
        return z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.common.d.a(int, boolean):boolean
     arg types: [int, int]
     candidates:
      com.tencent.feedback.common.d.a(java.lang.Runnable, long):boolean
      com.tencent.feedback.common.c.a(java.lang.Runnable, long):boolean
      com.tencent.feedback.common.d.a(int, boolean):boolean */
    public final synchronized boolean a(int i, Runnable runnable, long j, long j2) {
        long j3;
        boolean z;
        if (!b()) {
            g.d("rqdp{  ScheduleTaskHandlerImp was closed , should not post!}", new Object[0]);
            z = false;
        } else if (runnable == null) {
            g.d("rqdp{  task runner should not be null}", new Object[0]);
            z = false;
        } else {
            if (f3678a) {
                if (j2 <= 10000) {
                    j2 = 10000;
                }
                j3 = j2;
            } else {
                j3 = j2;
            }
            a(19, true);
            ScheduledFuture<?> scheduleAtFixedRate = this.b.scheduleAtFixedRate(runnable, 0, j3, TimeUnit.MILLISECONDS);
            if (scheduleAtFixedRate != null) {
                g.b("rqdp{  add a new future! taskId} %d ,rqdp{   periodTime} %d", 19, Long.valueOf(j3));
                this.c.put(19, scheduleAtFixedRate);
            }
            z = true;
        }
        return z;
    }

    public final synchronized boolean a(int i, boolean z) {
        boolean z2 = false;
        synchronized (this) {
            if (!b()) {
                g.d("rqdp{  ScheduleTaskHandlerImp was closed , should all stopped}!", new Object[0]);
            } else {
                ScheduledFuture scheduledFuture = this.c.get(i);
                if (scheduledFuture != null && !scheduledFuture.isCancelled()) {
                    g.b("cancel a old future!", new Object[0]);
                    scheduledFuture.cancel(true);
                }
                this.c.remove(i);
                z2 = true;
            }
        }
        return z2;
    }

    public final synchronized boolean a(Runnable runnable, long j) {
        boolean z = false;
        synchronized (this) {
            if (!b()) {
                g.d("rqdp{  ScheduleTaskHandlerImp was closed , should not post!}", new Object[0]);
            } else if (runnable == null) {
                g.d("rqdp{  task runner should not be null}", new Object[0]);
            } else {
                if (j <= 0) {
                    j = 0;
                }
                this.b.schedule(runnable, j, TimeUnit.MILLISECONDS);
                z = true;
            }
        }
        return z;
    }

    private synchronized boolean b() {
        return this.b != null && !this.b.isShutdown();
    }
}
