package com.google.zxing.a.a;

final class h {

    /* renamed from: a  reason: collision with root package name */
    private final int f122a;
    private final g[] b;

    private h(int i, g gVar) {
        this.f122a = i;
        this.b = new g[]{gVar};
    }

    h(int i, g gVar, f fVar) {
        this(i, gVar);
    }

    private h(int i, g gVar, g gVar2) {
        this.f122a = i;
        this.b = new g[]{gVar, gVar2};
    }

    h(int i, g gVar, g gVar2, f fVar) {
        this(i, gVar, gVar2);
    }

    /* access modifiers changed from: package-private */
    public int a() {
        return this.f122a;
    }

    /* access modifiers changed from: package-private */
    public g[] b() {
        return this.b;
    }
}
