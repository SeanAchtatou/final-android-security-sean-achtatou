package com.google.zxing;

import java.util.Hashtable;

public final class i {

    /* renamed from: a  reason: collision with root package name */
    public static final i f196a = new i("OTHER");
    public static final i b = new i("ORIENTATION");
    public static final i c = new i("BYTE_SEGMENTS");
    public static final i d = new i("ERROR_CORRECTION_LEVEL");
    public static final i e = new i("ISSUE_NUMBER");
    public static final i f = new i("SUGGESTED_PRICE");
    public static final i g = new i("POSSIBLE_COUNTRY");
    private static final Hashtable h = new Hashtable();
    private final String i;

    private i(String str) {
        this.i = str;
        h.put(str, this);
    }

    public String toString() {
        return this.i;
    }
}
