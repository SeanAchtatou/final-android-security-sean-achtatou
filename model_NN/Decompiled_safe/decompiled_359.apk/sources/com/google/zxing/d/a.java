package com.google.zxing.d;

import com.google.zxing.NotFoundException;
import com.google.zxing.c;
import com.google.zxing.common.b;
import com.google.zxing.common.i;
import com.google.zxing.d;
import com.google.zxing.d.a.n;
import com.google.zxing.g;
import com.google.zxing.h;
import com.google.zxing.j;
import java.util.Hashtable;

public class a implements g {

    /* renamed from: a  reason: collision with root package name */
    private static final j[] f173a = new j[0];
    private final n b = new n();

    public static b a(b bVar) {
        int i = 1;
        int c = bVar.c();
        int b2 = bVar.b();
        int min = Math.min(c, b2);
        int[] a2 = bVar.a();
        if (a2 == null) {
            throw NotFoundException.a();
        }
        int i2 = a2[0];
        int i3 = a2[1];
        while (i2 < min && i3 < min && bVar.a(i2, i3)) {
            i2++;
            i3++;
        }
        if (i2 == min || i3 == min) {
            throw NotFoundException.a();
        }
        int i4 = i2 - a2[0];
        if (i4 == 0) {
            throw NotFoundException.a();
        }
        int i5 = b2 - 1;
        while (i5 > i2 && !bVar.a(i5, i3)) {
            i5--;
        }
        if (i5 <= i2) {
            throw NotFoundException.a();
        }
        int i6 = i5 + 1;
        if ((i6 - i2) % i4 != 0) {
            throw NotFoundException.a();
        }
        int i7 = ((i6 - i2) / i4) + 1;
        if (i4 != 1) {
            i = i4 >> 1;
        }
        int i8 = i2 - i;
        int i9 = i3 - i;
        if (((i7 - 1) * i4) + i8 >= b2 || ((i7 - 1) * i4) + i9 >= c) {
            throw NotFoundException.a();
        }
        b bVar2 = new b(i7);
        for (int i10 = 0; i10 < i7; i10++) {
            int i11 = i9 + (i10 * i4);
            for (int i12 = 0; i12 < i7; i12++) {
                if (bVar.a((i12 * i4) + i8, i11)) {
                    bVar2.b(i12, i10);
                }
            }
        }
        return bVar2;
    }

    public h a(c cVar, Hashtable hashtable) {
        com.google.zxing.common.g a2;
        j[] b2;
        if (hashtable == null || !hashtable.containsKey(d.b)) {
            i a3 = new com.google.zxing.d.b.c(cVar.c()).a(hashtable);
            a2 = this.b.a(a3.a(), hashtable);
            b2 = a3.b();
        } else {
            a2 = this.b.a(a(cVar.c()), hashtable);
            b2 = f173a;
        }
        h hVar = new h(a2.b(), a2.a(), b2, com.google.zxing.a.f114a);
        if (a2.c() != null) {
            hVar.a(com.google.zxing.i.c, a2.c());
        }
        if (a2.d() != null) {
            hVar.a(com.google.zxing.i.d, a2.d().toString());
        }
        return hVar;
    }

    public void a() {
    }
}
