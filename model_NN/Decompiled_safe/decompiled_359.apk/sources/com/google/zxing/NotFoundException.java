package com.google.zxing;

public final class NotFoundException extends ReaderException {

    /* renamed from: a  reason: collision with root package name */
    private static final NotFoundException f113a = new NotFoundException();

    private NotFoundException() {
    }

    public static NotFoundException a() {
        return f113a;
    }
}
