package com.google.zxing.b.a.a.a;

final class o extends q {
    private final String b;
    private final int c;
    private final boolean d;

    o(int i, String str) {
        super(i);
        this.b = str;
        this.d = false;
        this.c = 0;
    }

    o(int i, String str, int i2) {
        super(i);
        this.d = true;
        this.c = i2;
        this.b = str;
    }

    /* access modifiers changed from: package-private */
    public String a() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public boolean b() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public int c() {
        return this.c;
    }
}
