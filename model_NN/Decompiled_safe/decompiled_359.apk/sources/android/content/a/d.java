package android.content.a;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;

/* compiled from: ProGuard */
public final class d {

    /* renamed from: a  reason: collision with root package name */
    public int f20a;
    private InputStream b;
    private boolean c;

    public d() {
    }

    public d(InputStream inputStream, boolean z) {
        a(inputStream, z);
    }

    public final void a(InputStream inputStream, boolean z) {
        this.b = inputStream;
        this.c = z;
        this.f20a = 0;
    }

    public final void a() {
        if (this.b != null) {
            try {
                this.b.close();
            } catch (IOException e) {
            }
            a(null, false);
        }
    }

    public final int b() {
        return a(4);
    }

    public final int a(int i) {
        int i2 = 0;
        if (i < 0 || i > 4) {
            throw new IllegalArgumentException();
        }
        if (this.c) {
            int i3 = (i - 1) * 8;
            while (i3 >= 0) {
                int read = this.b.read();
                if (read == -1) {
                    throw new EOFException();
                }
                this.f20a++;
                int i4 = (read << i3) | i2;
                i3 -= 8;
                i2 = i4;
            }
        } else {
            int i5 = i * 8;
            int i6 = 0;
            while (i6 != i5) {
                int read2 = this.b.read();
                if (read2 == -1) {
                    throw new EOFException();
                }
                this.f20a++;
                int i7 = (read2 << i6) | i2;
                i6 += 8;
                i2 = i7;
            }
        }
        return i2;
    }

    public final int[] b(int i) {
        int[] iArr = new int[i];
        a(iArr, 0, i);
        return iArr;
    }

    public final void a(int[] iArr, int i, int i2) {
        while (i2 > 0) {
            iArr[i] = b();
            i2--;
            i++;
        }
    }

    public final void c(int i) {
        if (i > 0) {
            long skip = this.b.skip((long) i);
            this.f20a = (int) (((long) this.f20a) + skip);
            if (skip != ((long) i)) {
                throw new EOFException();
            }
        }
    }

    public final void c() {
        c(4);
    }
}
