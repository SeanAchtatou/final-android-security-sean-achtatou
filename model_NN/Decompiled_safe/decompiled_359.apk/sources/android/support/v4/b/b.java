package android.support.v4.b;

import android.util.Log;
import java.io.Writer;

/* compiled from: ProGuard */
public class b extends Writer {

    /* renamed from: a  reason: collision with root package name */
    private final String f67a;
    private StringBuilder b = new StringBuilder(128);

    public b(String str) {
        this.f67a = str;
    }

    public void close() {
        a();
    }

    public void flush() {
        a();
    }

    public void write(char[] cArr, int i, int i2) {
        for (int i3 = 0; i3 < i2; i3++) {
            char c = cArr[i + i3];
            if (c == 10) {
                a();
            } else {
                this.b.append(c);
            }
        }
    }

    private void a() {
        if (this.b.length() > 0) {
            Log.d(this.f67a, this.b.toString());
            this.b.delete(0, this.b.length());
        }
    }
}
