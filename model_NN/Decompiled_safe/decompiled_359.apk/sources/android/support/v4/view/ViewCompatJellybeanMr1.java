package android.support.v4.view;

import android.view.View;

/* compiled from: ProGuard */
public class ViewCompatJellybeanMr1 {
    public static int getLabelFor(View view) {
        return view.getLabelFor();
    }

    public static void setLabelFor(View view, int i) {
        view.setLabelFor(i);
    }
}
