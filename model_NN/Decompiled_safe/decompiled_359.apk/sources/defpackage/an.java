package defpackage;

import android.content.DialogInterface;
import com.tencent.securemodule.ui.TransparentActivity;

/* renamed from: an  reason: default package */
public class an implements DialogInterface.OnCancelListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TransparentActivity f17a;

    public an(TransparentActivity transparentActivity) {
        this.f17a = transparentActivity;
    }

    public void onCancel(DialogInterface dialogInterface) {
        this.f17a.finish();
    }
}
