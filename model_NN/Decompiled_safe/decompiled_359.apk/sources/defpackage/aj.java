package defpackage;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import com.tencent.securemodule.service.IControlService;
import com.tencent.securemodule.ui.TransparentActivity;

/* renamed from: aj  reason: default package */
public class aj implements ServiceConnection {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TransparentActivity f13a;

    public aj(TransparentActivity transparentActivity) {
        this.f13a = transparentActivity;
    }

    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        IControlService unused = this.f13a.m = (IControlService) iBinder;
    }

    public void onServiceDisconnected(ComponentName componentName) {
        IControlService unused = this.f13a.m = (IControlService) null;
    }
}
