package defpackage;

import android.content.DialogInterface;
import android.content.Intent;
import com.tencent.securemodule.impl.SecureService;
import com.tencent.securemodule.ui.TransparentActivity;

/* renamed from: ap  reason: default package */
public class ap implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ boolean f105a;
    final /* synthetic */ TransparentActivity b;

    public ap(TransparentActivity transparentActivity, boolean z) {
        this.b = transparentActivity;
        this.f105a = z;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        if (this.f105a) {
            try {
                Intent intent = new Intent();
                intent.addFlags(268435456);
                intent.setAction("com.tencent.action.virus_scan");
                this.b.startActivity(intent);
            } catch (Throwable th) {
                th.printStackTrace();
            }
        } else {
            SecureService.a(this.b.h.getApplicationContext(), "1000011");
        }
        this.b.finish();
    }
}
