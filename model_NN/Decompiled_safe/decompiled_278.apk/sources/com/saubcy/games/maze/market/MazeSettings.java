package com.saubcy.games.maze.market;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.ColorMatrixColorFilter;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MazeSettings extends Activity implements View.OnClickListener {
    public static final float[] BT_NOT_SELECTED = {1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f};
    public static final float[] BT_SELECTED = {2.0f, 0.0f, 0.0f, 0.0f, 2.0f, 0.0f, 2.0f, 0.0f, 0.0f, 2.0f, 0.0f, 0.0f, 2.0f, 0.0f, 2.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f};
    public static final int SECTION_CONTROLLS = 1;
    public static final int SECTION_LEVEL = 0;
    /* access modifiers changed from: private */
    public int OpMode;
    /* access modifiers changed from: private */
    public int blockSize;
    /* access modifiers changed from: private */
    public final String[] blockSizeChoice = {"5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15"};
    private ImageButton btn_cancel = null;
    private ImageButton btn_save = null;
    private ListView[] configList = new ListView[2];
    private int gameMode;
    /* access modifiers changed from: private */
    public int robotSpeed;
    /* access modifiers changed from: private */
    public String[] robotSpeedChoice;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setRequestedOrientation(1);
        loadViews();
        setDefaultValue();
        loadConfigView();
    }

    private void loadViews() {
        setContentView((int) R.layout.maze_settings);
        this.robotSpeedChoice = new String[20];
        for (int i = 1; i <= 20; i++) {
            this.robotSpeedChoice[i - 1] = new StringBuilder().append(i).toString();
        }
        this.btn_save = (ImageButton) findViewById(R.id.btn_save);
        this.btn_save.setOnClickListener(this);
        this.btn_save.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == 0) {
                    v.getBackground().setColorFilter(new ColorMatrixColorFilter(MazeSettings.BT_SELECTED));
                    v.setBackgroundDrawable(v.getBackground());
                    return false;
                } else if (event.getAction() != 1) {
                    return false;
                } else {
                    v.getBackground().setColorFilter(new ColorMatrixColorFilter(MazeSettings.BT_NOT_SELECTED));
                    v.setBackgroundDrawable(v.getBackground());
                    return false;
                }
            }
        });
        this.btn_cancel = (ImageButton) findViewById(R.id.btn_cancel);
        this.btn_cancel.setOnClickListener(this);
        this.btn_cancel.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == 0) {
                    v.getBackground().setColorFilter(new ColorMatrixColorFilter(MazeSettings.BT_SELECTED));
                    v.setBackgroundDrawable(v.getBackground());
                    return false;
                } else if (event.getAction() != 1) {
                    return false;
                } else {
                    v.getBackground().setColorFilter(new ColorMatrixColorFilter(MazeSettings.BT_NOT_SELECTED));
                    v.setBackgroundDrawable(v.getBackground());
                    return false;
                }
            }
        });
        this.configList[0] = (ListView) findViewById(R.id.lv_section_level);
        this.configList[0].setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                switch (position) {
                    case 0:
                        MazeSettings.this.showBlockSizeChooseDialog();
                        return;
                    case 1:
                        MazeSettings.this.showRobotSpeedChooseDialog();
                        return;
                    default:
                        return;
                }
            }
        });
        this.configList[1] = (ListView) findViewById(R.id.lv_section_controlls);
        this.configList[1].setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                MazeSettings.this.showControllsChooseDialog();
            }
        });
    }

    private void setDefaultValue() {
        this.gameMode = getIntent().getIntExtra(Welcome.GAMEMODE, 0);
        if (this.gameMode == 5) {
            ((TextView) findViewById(R.id.index_3)).setVisibility(8);
            this.configList[1].setVisibility(8);
        }
        this.blockSize = getIntent().getIntExtra(Welcome.DEFAULTSIZE, 5);
        if (this.gameMode == 5) {
            this.blockSize /= 3;
        }
        this.robotSpeed = getIntent().getIntExtra(Welcome.DEFAULTSPEED, 300);
        this.OpMode = getIntent().getIntExtra(Welcome.OPMODE, 0);
    }

    /* access modifiers changed from: private */
    public void loadConfigView() {
        List<Map<String, Object>> list = new ArrayList<>();
        Map<String, Object> map = new HashMap<>();
        map.put("title", new StringBuilder(String.valueOf(this.blockSize)).toString());
        map.put("tips", getBaseContext().getResources().getText(R.string.config_change_title_section_level_01_tips).toString());
        map.put("image", Integer.valueOf((int) R.drawable.arrow));
        list.add(map);
        if (1 == this.gameMode || 3 == this.gameMode || 4 == this.gameMode) {
            Map<String, Object> map2 = new HashMap<>();
            map2.put("title", new StringBuilder(String.valueOf(this.robotSpeed)).toString());
            map2.put("tips", getBaseContext().getResources().getText(R.string.config_change_title_section_level_02_tips).toString());
            map2.put("image", Integer.valueOf((int) R.drawable.arrow));
            list.add(map2);
        }
        this.configList[0].setAdapter((ListAdapter) new SimpleAdapter(this, list, R.layout.maze_settings_row, new String[]{"title", "tips", "image"}, new int[]{R.id.tv_config_title, R.id.tv_config_tips, R.id.iv_config_status}));
        List<Map<String, Object>> list2 = new ArrayList<>();
        Map<String, Object> map3 = new HashMap<>();
        String controlMode = null;
        switch (this.OpMode) {
            case 0:
                controlMode = getBaseContext().getResources().getText(R.string.config_change_title_section_controlls_01_a).toString();
                break;
            case 1:
                controlMode = getBaseContext().getResources().getText(R.string.config_change_title_section_controlls_01_b).toString();
                break;
            case 2:
                controlMode = getBaseContext().getResources().getText(R.string.config_change_title_section_controlls_01_c).toString();
                break;
        }
        map3.put("title", controlMode);
        map3.put("tips", getBaseContext().getResources().getText(R.string.config_change_title_section_controlls_01_tips).toString());
        map3.put("image", Integer.valueOf((int) R.drawable.arrow));
        list2.add(map3);
        this.configList[1].setAdapter((ListAdapter) new SimpleAdapter(this, list2, R.layout.maze_settings_row, new String[]{"title", "tips", "image"}, new int[]{R.id.tv_config_title, R.id.tv_config_tips, R.id.iv_config_status}));
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_save /*2131099667*/:
                backValue(-1);
                return;
            case R.id.btn_cancel /*2131099668*/:
                backValue(0);
                return;
            default:
                return;
        }
    }

    private void backValue(int result) {
        if (this.gameMode == 5) {
            this.blockSize *= 3;
        }
        Intent intent = new Intent();
        intent.putExtra(Welcome.DEFAULTSIZE, new StringBuilder(String.valueOf(this.blockSize)).toString());
        intent.putExtra(Welcome.DEFAULTSPEED, new StringBuilder(String.valueOf(this.robotSpeed)).toString());
        intent.putExtra(Welcome.OPMODE, new StringBuilder(String.valueOf(this.OpMode)).toString());
        setResult(result, intent);
        finish();
    }

    /* access modifiers changed from: protected */
    public boolean checkSensor() {
        if (((SensorManager) getSystemService("sensor")).getSensorList(3).size() > 0) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: private */
    public void showBlockSizeChooseDialog() {
        int i = 0;
        while (i < this.blockSizeChoice.length && Integer.parseInt(this.blockSizeChoice[i]) != this.blockSize) {
            i++;
        }
        new AlertDialog.Builder(this).setTitle(getBaseContext().getResources().getText(R.string.config_change_title_section_level_01).toString()).setSingleChoiceItems(this.blockSizeChoice, i, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                MazeSettings.this.blockSize = Integer.parseInt(MazeSettings.this.blockSizeChoice[which]);
                MazeSettings.this.loadConfigView();
                dialog.cancel();
            }
        }).setNegativeButton(getBaseContext().getResources().getText(R.string.config_change_title_cancel).toString(), (DialogInterface.OnClickListener) null).show();
    }

    /* access modifiers changed from: private */
    public void showRobotSpeedChooseDialog() {
        int i = 0;
        while (i < this.robotSpeedChoice.length && Integer.parseInt(this.robotSpeedChoice[i]) != this.robotSpeed) {
            i++;
        }
        new AlertDialog.Builder(this).setTitle(getBaseContext().getResources().getText(R.string.config_change_title_section_level_02).toString()).setSingleChoiceItems(this.robotSpeedChoice, i, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                MazeSettings.this.robotSpeed = Integer.parseInt(MazeSettings.this.robotSpeedChoice[which]);
                MazeSettings.this.loadConfigView();
                dialog.cancel();
            }
        }).setNegativeButton(getBaseContext().getResources().getText(R.string.config_change_title_cancel).toString(), (DialogInterface.OnClickListener) null).show();
    }

    /* access modifiers changed from: private */
    public void showControllsChooseDialog() {
        new AlertDialog.Builder(this).setTitle(getBaseContext().getResources().getText(R.string.config_change_title_section_controlls_01).toString()).setSingleChoiceItems((int) R.array.ControllsModeList, this.OpMode, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                MazeSettings.this.OpMode = which;
                MazeSettings.this.loadConfigView();
                dialog.cancel();
            }
        }).setNegativeButton(getBaseContext().getResources().getText(R.string.config_change_title_cancel).toString(), (DialogInterface.OnClickListener) null).show();
    }
}
