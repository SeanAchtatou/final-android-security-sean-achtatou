package com.saubcy.games.maze.market;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.Stack;

public class MazeGenerator extends View {
    public static final int BOTTOM = 3;
    public static final int CENTERX = 0;
    public static final int CENTERY = 1;
    public static final int COL = 1;
    public static final int[] COLOPVALUE;
    public static final int KEYNUM = 3;
    public static final int LEFT = 0;
    public static final int RIGHT = 2;
    public static final int ROW = 0;
    public static final int[] ROWOPVALUE;
    public static final int TOP = 1;
    public static final int X = 0;
    public static final int Y = 1;
    private static boolean isDraw = false;
    private boolean DragState;
    private int[] centerPos = new int[2];
    private final Rect cursor = new Rect();
    protected float[] cursorCoordinate = {0.0f, 0.0f};
    protected int[] cursorPos = new int[2];
    protected DrawCell[][] drawMap = null;
    private int keyGotNum;
    private KeyPos[] keyPos;
    private Rect[] keys;
    /* access modifiers changed from: private */
    public LongTouchMoveHandler ltmh = null;
    protected int m_nCol;
    protected int m_nRow;
    private MazeCell[][] mazeMap = null;
    private int nArrowWidth;
    private int nCursorHeightUnit;
    private int nCursorWidthUnit;
    private int nDistance;
    private int nUnit;
    private int nViewHeight;
    private int nViewWidth;
    private Paint pBlock = new Paint();
    private Paint pCursor = new Paint();
    private Paint pKey = null;
    private Paint pPass = new Paint();
    private Paint pRobot = null;
    private Paint pSide = new Paint();
    private MazeGame parent = null;
    private Random random = null;
    protected Rect robot = null;
    private int[] robotPos = new int[2];
    private boolean rollback = false;
    private Thread4LongTouch t4lt = null;
    private Stack<MazeCell> tobeVisited = null;
    private int xOffset;
    private int yOffset;

    static {
        int[] iArr = new int[4];
        iArr[1] = -1;
        iArr[3] = 1;
        ROWOPVALUE = iArr;
        int[] iArr2 = new int[4];
        iArr2[0] = -1;
        iArr2[2] = 1;
        COLOPVALUE = iArr2;
    }

    public MazeGenerator(Context context) {
        super(context);
        init();
    }

    public MazeGenerator(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int wMeasureSpec, int hMeasureSpec) {
        this.nViewWidth = this.parent.nWidth - 10;
        this.nViewHeight = this.parent.nHeight;
        setMeasuredDimension(this.nViewWidth, this.nViewHeight);
    }

    public void init() {
        setFocusable(true);
        this.random = new Random();
        this.ltmh = new LongTouchMoveHandler(this, null);
        this.pBlock.setColor(getResources().getColor(R.color.maze_block));
        this.pPass.setColor(getResources().getColor(R.color.maze_road));
        this.pSide.setColor(getResources().getColor(R.color.maze_block_side));
        this.pSide.setStyle(Paint.Style.STROKE);
        this.pCursor.setColor(getResources().getColor(R.color.maze_curcor));
        this.robot = new Rect();
        this.pRobot = new Paint();
        this.pKey = new Paint();
        this.pKey.setColor(getResources().getColor(R.color.maze_key));
    }

    public void setMaze(MazeGame m, int nRow, int nCol) {
        setFocusable(true);
        this.parent = m;
        this.m_nRow = nRow;
        this.m_nCol = nCol;
        this.DragState = false;
        this.nCursorWidthUnit = this.parent.nWidthUnit;
        this.nCursorHeightUnit = this.parent.nHeightUnit;
        if (5 == this.parent.GameMode) {
            this.nCursorWidthUnit /= 2;
            this.nCursorHeightUnit /= 2;
        }
        this.nViewWidth = this.parent.nWidth - 10;
        this.nViewHeight = this.parent.nHeight - 50;
        this.xOffset = (this.nViewWidth - (((this.m_nCol * 2) + 1) * this.parent.nWidthUnit)) / 2;
        this.yOffset = 0;
        this.centerPos[0] = (this.nViewWidth / 2) + this.xOffset;
        this.centerPos[1] = ((this.nViewHeight * 2) / 3) + this.yOffset;
        this.nUnit = 20;
        this.nDistance = (this.nUnit / 2) + this.nUnit;
        this.nArrowWidth = this.nUnit * 5;
        switch (this.parent.GameMode) {
            case 1:
                this.pRobot.setColor(getResources().getColor(R.color.maze_robot));
                break;
            case 3:
                this.pRobot.setColor(getResources().getColor(R.color.maze_police));
                break;
            case 4:
                this.pRobot.setColor(getResources().getColor(R.color.maze_police));
                break;
        }
        if (this.t4lt != null && this.t4lt.isAlive()) {
            this.t4lt.interrupt();
            this.t4lt = null;
        }
        if (2 == this.parent.GameMode || 4 == this.parent.GameMode || 6 == this.parent.GameMode) {
            this.keyGotNum = 0;
            this.keyPos = new KeyPos[3];
            this.keys = new Rect[3];
            for (int i = 0; i < 3; i++) {
                this.keyPos[i] = new KeyPos();
            }
            setKeyPos();
            for (int i2 = 0; i2 < 3; i2++) {
                this.keys[i2] = new Rect();
                getKey(i2);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawColor(-16777216);
        if (isDraw) {
            drawMaze(canvas);
            if (2 == this.parent.GameMode || 4 == this.parent.GameMode || 6 == this.parent.GameMode) {
                for (int i = 0; i < 3; i++) {
                    if (this.keyPos[i].got) {
                        canvas.drawRect(this.keys[i], this.pPass);
                    } else {
                        canvas.drawRect(this.keys[i], this.pKey);
                    }
                }
            }
            canvas.drawRect(this.cursor, this.pCursor);
            if (1 == this.parent.GameMode || 3 == this.parent.GameMode || 4 == this.parent.GameMode) {
                canvas.drawRect(this.robot, this.pRobot);
            }
        }
    }

    private void getCursor() {
        if (5 == this.parent.GameMode) {
            this.cursor.set((this.cursorPos[1] * this.parent.nWidthUnit) + this.xOffset + (this.nCursorWidthUnit / 2), (this.cursorPos[0] * this.parent.nHeightUnit) + this.yOffset + (this.nCursorHeightUnit / 2), (((this.cursorPos[1] + 1) * this.parent.nWidthUnit) + this.xOffset) - (this.nCursorWidthUnit / 2), (((this.cursorPos[0] + 1) * this.parent.nHeightUnit) + this.yOffset) - (this.nCursorHeightUnit / 2));
            this.cursorCoordinate[0] = (float) ((this.cursorPos[1] * this.parent.nWidthUnit) + this.xOffset + (this.nCursorWidthUnit / 2) + (this.nCursorWidthUnit / 2));
            this.cursorCoordinate[1] = (float) ((this.cursorPos[0] * this.parent.nHeightUnit) + this.yOffset + (this.nCursorHeightUnit / 2) + (this.nCursorHeightUnit / 2));
            return;
        }
        this.cursor.set((this.cursorPos[1] * this.parent.nWidthUnit) + this.xOffset, (this.cursorPos[0] * this.parent.nHeightUnit) + this.yOffset, ((this.cursorPos[1] + 1) * this.parent.nWidthUnit) + this.xOffset, ((this.cursorPos[0] + 1) * this.parent.nHeightUnit) + this.yOffset);
    }

    private void getRobot() {
        this.robot.set((this.robotPos[1] * this.parent.nWidthUnit) + this.xOffset, (this.robotPos[0] * this.parent.nHeightUnit) + this.yOffset, ((this.robotPos[1] + 1) * this.parent.nWidthUnit) + this.xOffset, ((this.robotPos[0] + 1) * this.parent.nHeightUnit) + this.yOffset);
    }

    private void getKey(int i) {
        this.keys[i].set((this.keyPos[i].col * this.parent.nWidthUnit) + this.xOffset, (this.keyPos[i].row * this.parent.nHeightUnit) + this.yOffset, ((this.keyPos[i].col + 1) * this.parent.nWidthUnit) + this.xOffset, ((this.keyPos[i].row + 1) * this.parent.nHeightUnit) + this.yOffset);
    }

    /* access modifiers changed from: private */
    public boolean setCursor(int nRow, int nCol) {
        if (!isMoveValid(nRow, nCol)) {
            return false;
        }
        if (!this.parent.runFlag && !this.parent.finishFlag) {
            this.parent.sw.controlCount(false);
        }
        this.cursorPos[0] = nRow;
        this.cursorPos[1] = nCol;
        invalidate(this.cursor);
        getCursor();
        invalidate(this.cursor);
        if (this.parent.checkIsGet()) {
            this.parent.loseGame();
            return true;
        }
        checkKeyGot();
        checkFinish();
        return true;
    }

    private void checkKeyGot() {
        if (2 == this.parent.GameMode || 4 == this.parent.GameMode || 6 == this.parent.GameMode) {
            for (int i = 0; i < 3; i++) {
                if (!this.keyPos[i].got && this.cursorPos[0] == this.keyPos[i].row && this.cursorPos[1] == this.keyPos[i].col) {
                    this.keyPos[i].got = true;
                    this.keyGotNum++;
                    invalidate(this.keys[i]);
                    if (6 == this.parent.GameMode) {
                        this.parent.resteMazeWall();
                        return;
                    }
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void setRobot(int nRow, int nCol) {
        if (isMoveValid(nRow, nCol)) {
            this.robotPos[0] = nRow;
            this.robotPos[1] = nCol;
            invalidate(this.robot);
            getRobot();
            invalidate(this.robot);
        }
    }

    private void checkFinish() {
        if (((2 != this.parent.GameMode && 4 != this.parent.GameMode && 6 != this.parent.GameMode) || this.keyGotNum >= 3) && this.parent.runFlag && this.cursorPos[0] == ((this.m_nRow * 2) + 1) - 1 && this.cursorPos[1] == ((this.m_nCol * 2) + 1) - 2) {
            this.parent.sw.controlCount(false);
            this.parent.finishFlag = true;
            if (1 == this.parent.GameMode || 3 == this.parent.GameMode || 4 == this.parent.GameMode) {
                this.parent.mr.stopRobot();
            }
            this.parent.showGameFinishDialog();
        }
    }

    private boolean isMoveValid(int nRow, int nCol) {
        if (nRow < 0 || nCol < 0 || nRow > ((this.m_nRow * 2) + 1) - 1 || nCol > ((this.m_nCol * 2) + 1) - 1) {
            return false;
        }
        if (this.drawMap[nRow][nCol].m_blocked) {
            return false;
        }
        return true;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (5 == this.parent.GameMode) {
            return false;
        }
        switch (keyCode) {
            case 19:
                setCursor(this.cursorPos[0] - 1, this.cursorPos[1]);
                break;
            case 20:
                setCursor(this.cursorPos[0] + 1, this.cursorPos[1]);
                break;
            case 21:
                setCursor(this.cursorPos[0], this.cursorPos[1] - 1);
                break;
            case 22:
                setCursor(this.cursorPos[0], this.cursorPos[1] + 1);
                break;
            default:
                return super.onKeyDown(keyCode, event);
        }
        return false;
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (5 == this.parent.GameMode) {
            switch (event.getAction()) {
                case 0:
                    setDragState(event.getX(), event.getY());
                    break;
                case 1:
                    this.DragState = false;
                    break;
                case 2:
                    if (this.parent.runFlag) {
                        setNewPos(event.getX(), event.getY());
                        break;
                    } else {
                        return true;
                    }
            }
            return true;
        } else if (1 == this.parent.OpMode) {
            if (!this.parent.runFlag && !this.parent.finishFlag) {
                this.parent.sw.controlCount(false);
            }
            return true;
        } else {
            switch (event.getAction()) {
                case 0:
                    int direction = -1;
                    int nX = (int) event.getX();
                    int nY = (int) event.getY();
                    if (nX > this.centerPos[0] - (this.nDistance + this.nArrowWidth) && nX < this.centerPos[0] - this.nDistance && nY < this.centerPos[1] + this.nUnit && nY > this.centerPos[1] - this.nUnit) {
                        direction = 0;
                        if (setCursor(this.cursorPos[0], this.cursorPos[1] - 1)) {
                            this.parent.vibratorBack();
                        }
                    } else if (nX < this.centerPos[0] + this.nUnit && nX > this.centerPos[0] - this.nUnit && nY > this.centerPos[1] - (this.nDistance + this.nArrowWidth) && nY < this.centerPos[1] - this.nDistance) {
                        direction = 1;
                        if (setCursor(this.cursorPos[0] - 1, this.cursorPos[1])) {
                            this.parent.vibratorBack();
                        }
                    } else if (nX < this.centerPos[0] + this.nDistance + this.nArrowWidth && nX > this.centerPos[0] + this.nDistance && nY < this.centerPos[1] + this.nUnit && nY > this.centerPos[1] - this.nUnit) {
                        direction = 2;
                        if (setCursor(this.cursorPos[0], this.cursorPos[1] + 1)) {
                            this.parent.vibratorBack();
                        }
                    } else if (nX < this.centerPos[0] + this.nUnit && nX > this.centerPos[0] - this.nUnit && nY < this.centerPos[1] + this.nDistance + this.nArrowWidth && nY > this.centerPos[1] + this.nDistance) {
                        direction = 3;
                        if (setCursor(this.cursorPos[0] + 1, this.cursorPos[1])) {
                            this.parent.vibratorBack();
                        }
                    }
                    if (direction >= 0) {
                        this.t4lt = new Thread4LongTouch(direction);
                        this.t4lt.start();
                    }
                    return true;
                case 1:
                    if (this.t4lt != null && this.t4lt.isAlive()) {
                        this.t4lt.interrupt();
                        this.t4lt = null;
                    }
                    return true;
                default:
                    return false;
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean move(int direction) {
        if (5 == this.parent.GameMode) {
            return false;
        }
        if (getDirectionCount() >= 3) {
            this.parent.isCrossState = true;
        } else {
            this.parent.isCrossState = false;
        }
        long currentTime = System.currentTimeMillis();
        if (this.parent.lastSensorGetTimer > 0 && this.parent.isCrossState && currentTime - this.parent.lastSensorGetTimer < 500) {
            return true;
        }
        this.parent.lastSensorGetTimer = currentTime;
        switch (direction) {
            case 0:
                return setCursor(this.cursorPos[0], this.cursorPos[1] - 1);
            case 1:
                return setCursor(this.cursorPos[0] - 1, this.cursorPos[1]);
            case 2:
                return setCursor(this.cursorPos[0], this.cursorPos[1] + 1);
            case 3:
                return setCursor(this.cursorPos[0] + 1, this.cursorPos[1]);
            default:
                return false;
        }
    }

    /* access modifiers changed from: protected */
    public boolean move4Gesture(int direction, int step) {
        if (5 == this.parent.GameMode) {
            return false;
        }
        int directionCount = getDirectionCount();
        if (step > 0 && directionCount >= 3) {
            return false;
        }
        switch (direction) {
            case 0:
                return setCursor(this.cursorPos[0], this.cursorPos[1] - 1);
            case 1:
                return setCursor(this.cursorPos[0] - 1, this.cursorPos[1]);
            case 2:
                return setCursor(this.cursorPos[0], this.cursorPos[1] + 1);
            case 3:
                return setCursor(this.cursorPos[0] + 1, this.cursorPos[1]);
            default:
                return false;
        }
    }

    /* access modifiers changed from: protected */
    public int getDirectionCount() {
        int directionCount = 0;
        for (int i = 0; i < 4; i++) {
            switch (i) {
                case 0:
                    if (!isMoveValid(this.cursorPos[0], this.cursorPos[1] - 1)) {
                        break;
                    } else {
                        directionCount++;
                        break;
                    }
                case 1:
                    if (!isMoveValid(this.cursorPos[0] - 1, this.cursorPos[1])) {
                        break;
                    } else {
                        directionCount++;
                        break;
                    }
                case 2:
                    if (!isMoveValid(this.cursorPos[0], this.cursorPos[1] + 1)) {
                        break;
                    } else {
                        directionCount++;
                        break;
                    }
                case 3:
                    if (!isMoveValid(this.cursorPos[0] + 1, this.cursorPos[1])) {
                        break;
                    } else {
                        directionCount++;
                        break;
                    }
            }
        }
        return directionCount;
    }

    private void setNewPos(float x, float y) {
        if (this.DragState) {
            invalidate(this.cursor);
            this.cursorCoordinate[0] = x;
            this.cursorCoordinate[1] = y;
            this.cursor.set((int) (this.cursorCoordinate[0] - ((float) (this.nCursorWidthUnit / 2))), (int) (this.cursorCoordinate[1] - ((float) (this.nCursorHeightUnit / 2))), (int) (this.cursorCoordinate[0] + ((float) (this.nCursorWidthUnit / 2))), (int) (this.cursorCoordinate[1] + ((float) (this.nCursorHeightUnit / 2))));
            invalidate(this.cursor);
            if (checkCollision()) {
                this.parent.loseGame();
            } else if (checkWin()) {
                this.parent.sw.controlCount(false);
                this.parent.finishFlag = true;
                this.parent.showGameFinishDialog();
            }
        }
    }

    private void setDragState(float x, float y) {
        if (x > this.cursorCoordinate[0] - ((float) (this.nCursorWidthUnit / 2)) && x < this.cursorCoordinate[0] + ((float) (this.nCursorWidthUnit / 2)) && y > this.cursorCoordinate[1] - ((float) (this.nCursorHeightUnit / 2)) && y < this.cursorCoordinate[1] + ((float) (this.nCursorHeightUnit / 2))) {
            this.DragState = true;
            if (!this.parent.runFlag && !this.parent.finishFlag) {
                this.parent.sw.controlCount(false);
            }
        }
    }

    private boolean checkCollision() {
        boolean yDirection;
        for (int i = 0; i < (this.m_nRow * 2) + 1; i++) {
            for (int j = 0; j < (this.m_nCol * 2) + 1; j++) {
                if (this.drawMap[i][j].m_blocked) {
                    float blockLeftTopX = (float) ((this.parent.nWidthUnit * j) + this.xOffset);
                    float blockLeftTopY = (float) ((this.parent.nHeightUnit * i) + this.yOffset);
                    float blockWidth = (float) this.parent.nWidthUnit;
                    float blockHeight = (float) this.parent.nHeightUnit;
                    float cursorLeftTopX = this.cursorCoordinate[0] - ((float) (this.nCursorWidthUnit / 2));
                    float cursorLeftTopY = this.cursorCoordinate[1] - ((float) (this.nCursorHeightUnit / 2));
                    float cursorWidth = (float) this.nCursorWidthUnit;
                    float cursorHeight = (float) this.nCursorHeightUnit;
                    boolean xDirection = Math.abs(((blockWidth / 2.0f) + blockLeftTopX) - ((cursorWidth / 2.0f) + cursorLeftTopX)) < Math.abs((blockWidth + cursorWidth) / 2.0f);
                    if (Math.abs(((blockHeight / 2.0f) + blockLeftTopY) - ((cursorHeight / 2.0f) + cursorLeftTopY)) < Math.abs((blockHeight + cursorHeight) / 2.0f)) {
                        yDirection = true;
                    } else {
                        yDirection = false;
                    }
                    if (xDirection && yDirection) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private boolean checkWin() {
        boolean yDirection;
        if (this.cursorCoordinate[1] >= ((float) ((this.parent.nHeightUnit * (((this.m_nRow * 2) + 1) - 1)) + this.yOffset + (this.parent.nHeightUnit / 2)))) {
            yDirection = true;
        } else {
            yDirection = false;
        }
        if (yDirection) {
            return true;
        }
        return false;
    }

    private void drawMaze(Canvas canvas) {
        for (int i = 0; i < (this.m_nRow * 2) + 1; i++) {
            for (int j = 0; j < (this.m_nCol * 2) + 1; j++) {
                if (this.drawMap[i][j].m_blocked) {
                    canvas.drawRect((float) ((this.parent.nWidthUnit * j) + this.xOffset), (float) ((this.parent.nHeightUnit * i) + this.yOffset), (float) ((this.parent.nWidthUnit * (j + 1)) + this.xOffset), (float) ((this.parent.nHeightUnit * (i + 1)) + this.yOffset), this.pBlock);
                    canvas.drawRect((float) ((this.parent.nWidthUnit * j) + this.xOffset), (float) ((this.parent.nHeightUnit * i) + this.yOffset), (float) ((this.parent.nWidthUnit * (j + 1)) + this.xOffset), (float) ((this.parent.nHeightUnit * (i + 1)) + this.yOffset), this.pSide);
                } else {
                    canvas.drawRect((float) ((this.parent.nWidthUnit * j) + this.xOffset), (float) ((this.parent.nHeightUnit * i) + this.yOffset), (float) ((this.parent.nWidthUnit * (j + 1)) + this.xOffset), (float) ((this.parent.nHeightUnit * (i + 1)) + this.yOffset), this.pPass);
                }
            }
        }
        if (5 != this.parent.GameMode && 1 != this.parent.OpMode && 2 != this.parent.OpMode) {
            drawArrows(canvas);
        }
    }

    private void drawArrows(Canvas canvas) {
        Paint paintContent = new Paint();
        Paint paintSide = new Paint();
        paintContent.setColor(-1);
        paintContent.setAlpha(64);
        paintContent.setAntiAlias(true);
        paintSide.setStyle(Paint.Style.STROKE);
        paintSide.setStrokeWidth(1.0f);
        paintSide.setAntiAlias(true);
        paintSide.setColor(-16777216);
        paintSide.setAlpha(93);
        canvas.drawCircle((float) this.centerPos[0], (float) this.centerPos[1], (float) this.nUnit, paintContent);
        canvas.drawCircle((float) this.centerPos[0], (float) this.centerPos[1], (float) this.nUnit, paintSide);
        Path path = new Path();
        path.moveTo((float) (this.centerPos[0] - (this.nDistance + this.nArrowWidth)), (float) this.centerPos[1]);
        path.lineTo((float) (this.centerPos[0] - this.nDistance), (float) (this.centerPos[1] - this.nUnit));
        path.lineTo((float) (this.centerPos[0] - this.nDistance), (float) (this.centerPos[1] + this.nUnit));
        path.close();
        canvas.drawPath(path, paintContent);
        path.moveTo((float) this.centerPos[0], (float) (this.centerPos[1] - (this.nDistance + this.nArrowWidth)));
        path.lineTo((float) (this.centerPos[0] - this.nUnit), (float) (this.centerPos[1] - this.nDistance));
        path.lineTo((float) (this.centerPos[0] + this.nUnit), (float) (this.centerPos[1] - this.nDistance));
        path.close();
        canvas.drawPath(path, paintContent);
        path.moveTo((float) (this.centerPos[0] + this.nDistance + this.nArrowWidth), (float) this.centerPos[1]);
        path.lineTo((float) (this.centerPos[0] + this.nDistance), (float) (this.centerPos[1] - this.nUnit));
        path.lineTo((float) (this.centerPos[0] + this.nDistance), (float) (this.centerPos[1] + this.nUnit));
        path.close();
        canvas.drawPath(path, paintContent);
        path.moveTo((float) this.centerPos[0], (float) (this.centerPos[1] + this.nDistance + this.nArrowWidth));
        path.lineTo((float) (this.centerPos[0] - this.nUnit), (float) (this.centerPos[1] + this.nDistance));
        path.lineTo((float) (this.centerPos[0] + this.nUnit), (float) (this.centerPos[1] + this.nDistance));
        path.close();
        canvas.drawPath(path, paintContent);
    }

    private void createDrawMap(int mode) {
        this.drawMap = (DrawCell[][]) Array.newInstance(DrawCell.class, (this.m_nRow * 2) + 1, (this.m_nCol * 2) + 1);
        for (int i = 0; i < (this.m_nRow * 2) + 1; i++) {
            for (int j = 0; j < (this.m_nCol * 2) + 1; j++) {
                this.drawMap[i][j] = new DrawCell();
            }
        }
        for (int i2 = 0; i2 < this.m_nRow; i2++) {
            for (int j2 = 0; j2 < this.m_nCol; j2++) {
                setDrawMapCell(this.mazeMap[i2][j2]);
            }
        }
        this.drawMap[0][1].m_blocked = false;
        this.drawMap[((this.m_nRow * 2) + 1) - 1][((this.m_nCol * 2) + 1) - 2].m_blocked = false;
        if (1 == mode) {
            this.cursorPos[0] = 0;
            this.cursorPos[1] = 1;
            getCursor();
            if (1 == this.parent.GameMode || 3 == this.parent.GameMode || 4 == this.parent.GameMode) {
                switch (this.parent.GameMode) {
                    case 1:
                        this.drawMap[0][((this.m_nCol * 2) + 1) - 2].m_blocked = false;
                        this.robotPos[0] = 0;
                        this.robotPos[1] = ((this.m_nCol * 2) + 1) - 2;
                        break;
                    case 3:
                        this.robotPos[0] = ((this.parent.mg.m_nRow * 2) + 1) / 2;
                        this.robotPos[1] = 1;
                        break;
                    case 4:
                        this.robotPos[0] = ((this.parent.mg.m_nRow * 2) + 1) / 2;
                        this.robotPos[1] = 1;
                        break;
                }
                getRobot();
            }
        }
    }

    private void setKeyPos() {
        List<Integer> list = new ArrayList<>();
        while (list.size() < 3) {
            int flag = 1;
            int pos = this.random.nextInt(this.m_nRow * this.m_nCol);
            int i = 0;
            while (true) {
                if (i >= list.size()) {
                    break;
                } else if (pos == ((Integer) list.get(i)).intValue()) {
                    flag = 0;
                    break;
                } else {
                    i++;
                }
            }
            if (1 == flag) {
                list.add(Integer.valueOf(pos));
            }
        }
        for (int i2 = 0; i2 < 3; i2++) {
            int pos2 = ((Integer) list.get(i2)).intValue();
            int nRow = pos2 / this.m_nCol;
            int nCol = pos2 % this.m_nCol;
            this.keyPos[i2].row = (nRow * 2) + 1;
            this.keyPos[i2].col = (nCol * 2) + 1;
        }
    }

    private void setDrawMapCell(MazeCell cell) {
        int nDrawMapRow = (cell.m_nRow * 2) + 1;
        int nDrawMapCol = (cell.m_nCol * 2) + 1;
        this.drawMap[nDrawMapRow][nDrawMapCol].m_blocked = false;
        for (int i = 0; i < 4; i++) {
            if (this.drawMap[ROWOPVALUE[i] + nDrawMapRow][COLOPVALUE[i] + nDrawMapCol].m_blocked) {
                this.drawMap[ROWOPVALUE[i] + nDrawMapRow][COLOPVALUE[i] + nDrawMapCol].m_blocked = cell.walls[i];
            }
        }
    }

    private void operateCellByStack(MazeCell cell) {
        if (cell.m_bIsVisited) {
            this.rollback = true;
            return;
        }
        cell.m_bIsVisited = true;
        if (this.rollback && cell.m_nPreRow >= 0) {
            removeWall(this.mazeMap[cell.m_nPreRow][cell.m_nPreCol], cell, cell.m_nPos4Pre);
            this.rollback = false;
        }
        List<Integer> list = randomNeighbor();
        int nLastPush = -1;
        MazeCell nextCell = null;
        for (int i = 0; i < list.size(); i++) {
            int nPos = list.get(i).intValue();
            int nRow = cell.m_nRow + ROWOPVALUE[nPos];
            int nCol = cell.m_nCol + COLOPVALUE[nPos];
            if (isNeighborCellValid(nRow, nCol)) {
                nLastPush = nPos;
                nextCell = this.mazeMap[nRow][nCol];
                nextCell.m_nPreRow = cell.m_nRow;
                nextCell.m_nPreCol = cell.m_nCol;
                nextCell.m_nPos4Pre = nPos;
                this.tobeVisited.push(nextCell);
            }
        }
        if (nLastPush >= 0) {
            removeWall(cell, nextCell, nLastPush);
        } else {
            this.rollback = true;
        }
    }

    private void removeWall(MazeCell cell_1, MazeCell cell_2, int nPos) {
        cell_1.walls[nPos] = false;
        switch (nPos) {
            case 0:
                cell_2.walls[2] = false;
                return;
            case 1:
                cell_2.walls[3] = false;
                return;
            case 2:
                cell_2.walls[0] = false;
                return;
            case 3:
                cell_2.walls[1] = false;
                return;
            default:
                return;
        }
    }

    private boolean isNeighborCellValid(int nRow, int nCol) {
        if (nCol < 0 || nCol >= this.m_nCol || nRow < 0 || nRow >= this.m_nRow) {
            return false;
        }
        if (this.mazeMap[nRow][nCol].m_bIsVisited) {
            return false;
        }
        return true;
    }

    public void buildMazeByStack(int mode) {
        this.rollback = false;
        this.tobeVisited = new Stack<>();
        this.mazeMap = (MazeCell[][]) Array.newInstance(MazeCell.class, this.m_nRow, this.m_nCol);
        for (int i = 0; i < this.m_nRow; i++) {
            for (int j = 0; j < this.m_nCol; j++) {
                this.mazeMap[i][j] = new MazeCell(i, j);
            }
        }
        try {
            int nRow = this.random.nextInt(this.m_nRow);
            this.tobeVisited.push(this.mazeMap[nRow][this.random.nextInt(this.m_nCol)]);
            while (this.tobeVisited.size() > 0) {
                MazeCell cell = this.tobeVisited.pop();
                cell.walls[0] = true;
                operateCellByStack(cell);
            }
            isDraw = true;
            createDrawMap(mode);
            invalidate();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    private List<Integer> randomNeighbor() {
        List<Integer> list = new ArrayList<>();
        list.add(0);
        list.add(1);
        list.add(2);
        list.add(3);
        Collections.shuffle(list);
        return list;
    }

    public class MazeCell {
        protected boolean m_bIsVisited = false;
        /* access modifiers changed from: private */
        public int m_nCol;
        protected int m_nPos4Pre;
        protected int m_nPreCol;
        protected int m_nPreRow;
        /* access modifiers changed from: private */
        public int m_nRow;
        protected boolean[] walls = {true, true, true, true};

        public MazeCell(int nRow, int nCol) {
            this.m_nRow = nRow;
            this.m_nCol = nCol;
            this.m_nPreRow = -1;
            this.m_nPreCol = -1;
        }
    }

    public class DrawCell {
        protected boolean m_blocked = true;

        public DrawCell() {
        }
    }

    private class LongTouchMoveHandler extends Handler {
        private LongTouchMoveHandler() {
        }

        /* synthetic */ LongTouchMoveHandler(MazeGenerator mazeGenerator, LongTouchMoveHandler longTouchMoveHandler) {
            this();
        }

        public void handleMessage(Message msg) {
            boolean unused = MazeGenerator.this.setCursor(MazeGenerator.this.cursorPos[0] + MazeGenerator.ROWOPVALUE[msg.what], MazeGenerator.this.cursorPos[1] + MazeGenerator.COLOPVALUE[msg.what]);
        }
    }

    private class KeyPos {
        protected int col;
        protected boolean got = false;
        protected int row;

        public KeyPos() {
        }
    }

    private class Thread4LongTouch extends Thread {
        private int direction;

        public Thread4LongTouch(int d) {
            this.direction = d;
        }

        public void run() {
            try {
                sleep(500);
                while (MazeGenerator.this.ltmh.sendEmptyMessage(this.direction)) {
                    try {
                        sleep(200);
                    } catch (InterruptedException e) {
                        return;
                    }
                }
            } catch (InterruptedException e2) {
            }
        }
    }
}
