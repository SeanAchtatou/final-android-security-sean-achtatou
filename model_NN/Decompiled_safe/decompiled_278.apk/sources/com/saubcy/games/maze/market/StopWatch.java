package com.saubcy.games.maze.market;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class StopWatch extends SurfaceView implements SurfaceHolder.Callback {
    public static final float BaseScale = 0.6f;
    public static final int BaseWidth = 320;
    protected static final int[] NumberId = {R.drawable.number_0, R.drawable.number_1, R.drawable.number_2, R.drawable.number_3, R.drawable.number_4, R.drawable.number_5, R.drawable.number_6, R.drawable.number_7, R.drawable.number_8, R.drawable.number_9};
    protected int RealWidth = BaseWidth;
    private boolean firstOpen = true;
    protected SurfaceHolder holder;
    protected int hours;
    protected Matrix matrix = null;
    protected int minutes;
    /* access modifiers changed from: private */
    public MazeGame parent = null;
    protected Bitmap sawColon = null;
    protected Bitmap sawDot = null;
    protected Bitmap[] sawNumbers = null;
    protected int seconds;
    protected ShowTime st = null;
    protected int tseconds;

    public StopWatch(Context context) {
        super(context);
        this.parent = (MazeGame) context;
        init();
    }

    public StopWatch(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.parent = (MazeGame) context;
        init();
    }

    public void surfaceChanged(SurfaceHolder holder2, int format, int width, int height) {
    }

    public void surfaceCreated(SurfaceHolder holder2) {
        if (this.firstOpen) {
            this.st = new ShowTime(this);
            this.st.start();
            this.firstOpen = false;
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder2) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    /* access modifiers changed from: protected */
    public void onMeasure(int wMeasureSpec, int hMeasureSpec) {
        Bitmap bitmap = Bitmap.createBitmap(this.sawColon, 0, 0, this.sawColon.getWidth(), this.sawColon.getHeight(), this.matrix, true);
        setMeasuredDimension(bitmap.getWidth() * 10, bitmap.getHeight());
    }

    private void init() {
        this.holder = getHolder();
        this.holder.addCallback(this);
        this.sawNumbers = new Bitmap[NumberId.length];
        for (int i = 0; i < NumberId.length; i++) {
            this.sawNumbers[i] = ((BitmapDrawable) getResources().getDrawable(NumberId[i])).getBitmap();
        }
        this.sawColon = ((BitmapDrawable) getResources().getDrawable(R.drawable.colon)).getBitmap();
        this.sawDot = ((BitmapDrawable) getResources().getDrawable(R.drawable.dot)).getBitmap();
        float scaleW = ((float) this.RealWidth) / 320.0f;
        this.matrix = new Matrix();
        this.matrix.postScale(0.6f * scaleW, 0.6f * scaleW);
        resetTime();
    }

    public void controlCount(boolean isResume) {
        if (!this.parent.runFlag) {
            this.parent.runFlag = true;
            this.st = new ShowTime(this);
            this.st.start();
            if (1 == this.parent.GameMode || 3 == this.parent.GameMode || 4 == this.parent.GameMode) {
                this.parent.mr.findTheWay(isResume);
                return;
            }
            return;
        }
        if (this.st != null) {
            if (this.st.isAlive()) {
                this.st.interrupt();
            }
            this.st = null;
        }
        this.parent.runFlag = false;
    }

    public void resetTime() {
        this.hours = 0;
        this.minutes = 0;
        this.seconds = 0;
        this.tseconds = 0;
        if (this.st == null) {
            this.st = new ShowTime(this);
            this.st.start();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    /* access modifiers changed from: protected */
    public void drawTime(Canvas canvas) {
        int value = this.hours / 10;
        if (value > 9) {
            value = 0;
        }
        Bitmap bitmap = Bitmap.createBitmap(this.sawNumbers[value], 0, 0, this.sawNumbers[value].getWidth(), this.sawNumbers[value].getHeight(), this.matrix, true);
        canvas.drawBitmap(bitmap, 0.0f, 0.0f, (Paint) null);
        float left = 0.0f + ((float) bitmap.getWidth());
        int value2 = this.hours % 10;
        Bitmap bitmap2 = Bitmap.createBitmap(this.sawNumbers[value2], 0, 0, this.sawNumbers[value2].getWidth(), this.sawNumbers[value2].getHeight(), this.matrix, true);
        canvas.drawBitmap(bitmap2, left, 0.0f, (Paint) null);
        float left2 = left + ((float) bitmap2.getWidth());
        Bitmap bitmap3 = Bitmap.createBitmap(this.sawColon, 0, 0, this.sawColon.getWidth(), this.sawColon.getHeight(), this.matrix, true);
        canvas.drawBitmap(bitmap3, left2, 0.0f, (Paint) null);
        float left3 = left2 + ((float) bitmap3.getWidth());
        int value3 = this.minutes / 10;
        Bitmap bitmap4 = Bitmap.createBitmap(this.sawNumbers[value3], 0, 0, this.sawNumbers[value3].getWidth(), this.sawNumbers[value3].getHeight(), this.matrix, true);
        canvas.drawBitmap(bitmap4, left3, 0.0f, (Paint) null);
        float left4 = left3 + ((float) bitmap4.getWidth());
        int value4 = this.minutes % 10;
        Bitmap bitmap5 = Bitmap.createBitmap(this.sawNumbers[value4], 0, 0, this.sawNumbers[value4].getWidth(), this.sawNumbers[value4].getHeight(), this.matrix, true);
        canvas.drawBitmap(bitmap5, left4, 0.0f, (Paint) null);
        float left5 = left4 + ((float) bitmap5.getWidth());
        Bitmap bitmap6 = Bitmap.createBitmap(this.sawColon, 0, 0, this.sawColon.getWidth(), this.sawColon.getHeight(), this.matrix, true);
        canvas.drawBitmap(bitmap6, left5, 0.0f, (Paint) null);
        float left6 = left5 + ((float) bitmap6.getWidth());
        int value5 = this.seconds / 10;
        Bitmap bitmap7 = Bitmap.createBitmap(this.sawNumbers[value5], 0, 0, this.sawNumbers[value5].getWidth(), this.sawNumbers[value5].getHeight(), this.matrix, true);
        canvas.drawBitmap(bitmap7, left6, 0.0f, (Paint) null);
        float left7 = left6 + ((float) bitmap7.getWidth());
        int value6 = this.seconds % 10;
        Bitmap bitmap8 = Bitmap.createBitmap(this.sawNumbers[value6], 0, 0, this.sawNumbers[value6].getWidth(), this.sawNumbers[value6].getHeight(), this.matrix, true);
        canvas.drawBitmap(bitmap8, left7, 0.0f, (Paint) null);
        float left8 = left7 + ((float) bitmap8.getWidth());
        Bitmap bitmap9 = Bitmap.createBitmap(this.sawDot, 0, 0, this.sawDot.getWidth(), this.sawDot.getHeight(), this.matrix, true);
        canvas.drawBitmap(bitmap9, left8, 0.0f, (Paint) null);
        float left9 = left8 + ((float) bitmap9.getWidth());
        int value7 = this.tseconds;
        Bitmap bitmap10 = Bitmap.createBitmap(this.sawNumbers[value7], 0, 0, this.sawNumbers[value7].getWidth(), this.sawNumbers[value7].getHeight(), this.matrix, true);
        canvas.drawBitmap(bitmap10, left9, 0.0f, (Paint) null);
        float left10 = left9 + ((float) bitmap10.getWidth());
    }

    protected static class ShowTime extends Thread {
        private StopWatch parent = null;
        private final int sleepTime = 100;
        SurfaceHolder surfaceHolder;

        public ShowTime(StopWatch p) {
            this.parent = p;
            this.surfaceHolder = this.parent.holder;
        }

        public void run() {
            Canvas canvas = null;
            boolean flag = true;
            while (flag) {
                flag = this.parent.parent.runFlag;
                try {
                    canvas = this.surfaceHolder.lockCanvas(null);
                    synchronized (this.surfaceHolder) {
                        this.parent.drawTime(canvas);
                        if (this.parent.parent.runFlag) {
                            this.parent.tseconds++;
                            if (this.parent.tseconds == 10) {
                                this.parent.seconds++;
                                this.parent.tseconds = 0;
                                if (this.parent.seconds == 60) {
                                    this.parent.minutes++;
                                    this.parent.seconds = 0;
                                    if (this.parent.minutes == 60) {
                                        this.parent.hours++;
                                        this.parent.minutes = 0;
                                    }
                                }
                            }
                        }
                    }
                    if (canvas != null) {
                        this.surfaceHolder.unlockCanvasAndPost(canvas);
                    }
                } catch (Exception e) {
                    try {
                        e.printStackTrace();
                        if (canvas != null) {
                            this.surfaceHolder.unlockCanvasAndPost(canvas);
                        }
                    } catch (Throwable th) {
                        if (canvas != null) {
                            this.surfaceHolder.unlockCanvasAndPost(canvas);
                        }
                        throw th;
                    }
                }
                try {
                    sleep(100);
                } catch (InterruptedException e2) {
                    e2.printStackTrace();
                }
            }
        }
    }
}
