package com.saubcy.games.maze.market;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.saubcy.util.date.CYConverter;

public class ScoreManager extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "ClassicMaze.db";
    private static final String DATABASE_TABLE_SCORE = "t_score";
    private static final int DATABASE_VERSION = 1;
    private final String CREATE_TABLE_SCORES = "CREATE TABLE IF NOT EXISTS t_score(id INTEGER PRIMARY KEY, mode INTEGER NOT NULL , blocksize INTEGER NOT NULL , timeconsume INTEGER NOT NULL , timestamp INTEGER NOT NULL)";

    public ScoreManager(Context context) {
        super(context, DATABASE_NAME, (SQLiteDatabase.CursorFactory) null, 1);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS t_score(id INTEGER PRIMARY KEY, mode INTEGER NOT NULL , blocksize INTEGER NOT NULL , timeconsume INTEGER NOT NULL , timestamp INTEGER NOT NULL)");
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    public int getBestTime(int mode, int blocksize) {
        SQLiteDatabase db = getWritableDatabase();
        Cursor c = db.rawQuery("SELECT timeconsume FROM t_score WHERE mode = ? AND blocksize = ? ", new String[]{new StringBuilder(String.valueOf(mode)).toString(), new StringBuilder(String.valueOf(blocksize)).toString()});
        int BestTime = -1;
        c.moveToFirst();
        if (!c.isAfterLast()) {
            BestTime = c.getInt(0);
        }
        c.close();
        db.close();
        return BestTime;
    }

    private void insert(ScoreStructure ss, SQLiteDatabase db) {
        if (!db.isOpen()) {
            db = getWritableDatabase();
        }
        ContentValues values = new ContentValues();
        values.put("mode", Integer.valueOf(ss.mode));
        values.put("blocksize", Integer.valueOf(ss.blocksize));
        values.put("timeconsume", Integer.valueOf(ss.timeconsume));
        values.put("timestamp", Long.valueOf(ss.timestamp));
        db.insert(DATABASE_TABLE_SCORE, null, values);
    }

    private void update(ScoreStructure ss, SQLiteDatabase db) {
        if (getBestTime(ss.mode, ss.blocksize) < 0) {
            insert(ss, db);
            return;
        }
        if (!db.isOpen()) {
            db = getWritableDatabase();
        }
        db.execSQL("UPDATE t_score SET timeconsume = ?, timestamp = ? WHERE mode = ? AND blocksize = ?", new Object[]{Integer.valueOf(ss.timeconsume), Long.valueOf(ss.timestamp), Integer.valueOf(ss.mode), Integer.valueOf(ss.blocksize)});
    }

    /* access modifiers changed from: protected */
    public void addNewRecord(int mode, int blocksize, int timeconsume) {
        ScoreStructure ss = new ScoreStructure();
        ss.mode = mode;
        ss.blocksize = blocksize;
        ss.timeconsume = timeconsume;
        ss.timestamp = CYConverter.getTimestamp();
        SQLiteDatabase db = getWritableDatabase();
        update(ss, db);
        db.close();
    }

    protected static class ScoreStructure implements Comparable<ScoreStructure> {
        public int blocksize;
        public int id;
        public int mode;
        public int timeconsume;
        public long timestamp;

        protected ScoreStructure() {
        }

        public int compareTo(ScoreStructure arg0) {
            return this.timeconsume - arg0.timeconsume;
        }
    }
}
