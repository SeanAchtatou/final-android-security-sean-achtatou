package com.wiyun.game;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.webkit.CookieManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.Toast;
import com.wiyun.game.b.b;
import com.wiyun.game.b.d;
import com.wiyun.game.b.e;

public class OAuth extends Activity implements View.OnClickListener, com.wiyun.game.b.a, b {
    /* access modifiers changed from: private */
    public View a;
    /* access modifiers changed from: private */
    public ViewGroup b;
    private WebView c;
    private String d;
    private String e;
    private String f;
    private String g;
    private String h;
    private boolean i;
    private boolean j;
    private boolean k;
    private boolean l;
    private long m;
    private String n;
    private boolean o;
    private Intent p;

    private final class a extends WebViewClient {
        private a() {
        }

        /* synthetic */ a(OAuth oAuth, a aVar) {
            this();
        }

        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            OAuth.this.runOnUiThread(new Runnable() {
                public void run() {
                    OAuth.this.a.setVisibility(4);
                    h.b(OAuth.this.b);
                }
            });
        }

        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            OAuth.this.runOnUiThread(new Runnable() {
                public void run() {
                    OAuth.this.a.setVisibility(0);
                    h.a(OAuth.this.b);
                }
            });
        }
    }

    private void a() {
        Intent intent = getIntent();
        this.h = intent.getStringExtra("type");
        this.j = intent.getBooleanExtra("prompt_binding", false);
        this.l = intent.getBooleanExtra("from_use_another_account", false);
        this.i = intent.getBooleanExtra("login", false);
        this.d = intent.getStringExtra("url");
        this.e = intent.getStringExtra("action");
        if (this.e == null) {
            this.e = "";
        }
        this.f = intent.getStringExtra("tag");
        this.g = intent.getStringExtra("params");
        if (this.g == null) {
            this.g = "{}";
        }
        d.a().a((b) this);
        d.a().a((com.wiyun.game.b.a) this);
        this.p = new Intent();
        this.p.putExtra("tag", this.f);
        this.p.putExtra("action", this.e);
        this.p.putExtra("params", this.g);
        setResult(0, this.p);
        CookieManager instance = CookieManager.getInstance();
        instance.setCookie("http://wiyun.com", "wigame_config_apiserver=/wiapi;domain=wiyun.com");
        instance.setCookie("http://wiyun.com", "wigame_config_platform=android;domain=wiyun.com");
        instance.setCookie("http://wiyun.com", "wigame_config_system=android 1.5;domain=wiyun.com");
        instance.setCookie("http://wiyun.com", "wigame_config_shell=wigame_android;domain=wiyun.com");
        instance.setCookie("http://wiyun.com", String.format("wigame_config_channel=%s;domain=wiyun.com", WiGame.getChannel()));
        Object[] objArr = new Object[1];
        objArr[0] = Integer.valueOf(WiGame.H() >= 240 ? 2 : 1);
        instance.setCookie("http://wiyun.com", String.format("wigame_config_hidpi=%d;domain=wiyun.com", objArr));
        instance.setCookie("http://wiyun.com", String.format("wigame_config_dpi=%d;domain=wiyun.com", Integer.valueOf(WiGame.H())));
        instance.setCookie("http://wiyun.com", String.format("wigame_config_location=%f,%f;domain=%s", Double.valueOf(WiGame.getLatitude()), Double.valueOf(WiGame.getLongitude()), "wiyun.com"));
    }

    private void b() {
        this.a = findViewById(t.d("wy_ll_progress_panel"));
        this.b = (ViewGroup) findViewById(t.d("wy_ll_main_panel"));
        ((Button) findViewById(t.d("wy_b_close"))).setOnClickListener(this);
        ((Button) findViewById(t.d("wy_b_back"))).setVisibility(4);
        this.c = (WebView) findViewById(t.d("wy_webview"));
        this.c.setScrollBarStyle(33554432);
        this.c.setMapTrackballToArrowKeys(false);
        this.c.getSettings().setJavaScriptEnabled(true);
        this.c.setWebViewClient(new a(this, null));
        this.c.addJavascriptInterface(this, "wigame");
    }

    private void c() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService("input_method");
        if (inputMethodManager != null) {
            inputMethodManager.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void a(e eVar) {
        switch (eVar.a) {
            case 14:
                if (this.o) {
                    if (!this.j || WiGame.t().d()) {
                        WiGame.y();
                    } else {
                        Intent intent = new Intent(this, AccountRetrieval.class);
                        intent.putExtra("process_pending", true);
                        startActivity(intent);
                    }
                    finish();
                    return;
                }
                return;
            case 89:
                if (this.m == eVar.j) {
                    eVar.d = false;
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void ajax(String str, String str2, String str3, String str4) {
        if ("/doneauth".equals(str)) {
            String[] split = str3.split("&");
            String[] strArr = TextUtils.isEmpty(str3) ? null : new String[(split.length * 2)];
            if (strArr != null) {
                int i2 = 0;
                for (String split2 : split) {
                    String[] split3 = split2.split("=");
                    int i3 = i2 + 1;
                    strArr[i2] = split3[0];
                    if (split3.length == 1) {
                        i2 = i3 + 1;
                        strArr[i3] = "";
                    } else {
                        String j2 = h.j(split3[1]);
                        int i4 = i3 + 1;
                        strArr[i3] = j2;
                        if ("user_id".equals(strArr[0])) {
                            this.n = j2;
                        }
                        i2 = i4;
                    }
                }
            }
            runOnUiThread(new Runnable() {
                public void run() {
                    OAuth.this.a.setVisibility(0);
                    h.a(OAuth.this.b);
                }
            });
            if (WiGame.getMyId().equals(this.n)) {
                setResult(-1, this.p);
                finish();
                return;
            }
            f.b(this.n);
        } else if ("/autherror".equals(str)) {
            if (this.l) {
                Intent intent = new Intent(this, UseAnotherAccount.class);
                intent.putExtra("prompt_binding", this.j);
                startActivity(intent);
            }
            finish();
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public void b(final e e2) {
        switch (e2.a) {
            case 6:
                if (e2.c) {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            OAuth.this.a.setVisibility(4);
                            h.b(OAuth.this.b);
                            Toast.makeText(OAuth.this, (String) e2.e, 0).show();
                        }
                    });
                } else {
                    this.o = true;
                }
                e2.d = false;
                return;
            case 91:
                if (e2.c) {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            OAuth.this.a.setVisibility(4);
                            h.b(OAuth.this.b);
                            Toast.makeText(OAuth.this, (String) e2.e, 0).show();
                        }
                    });
                    return;
                } else {
                    this.c.loadUrl((String) e2.e);
                    return;
                }
            default:
                return;
        }
    }

    public void onClick(View view) {
        if (view.getId() == t.d("wy_b_close")) {
            if (this.l) {
                Intent intent = new Intent(this, UseAnotherAccount.class);
                intent.putExtra("prompt_binding", this.j);
                startActivity(intent);
            }
            c();
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(WiGame.i);
        requestWindowFeature(1);
        setContentView(t.e("wy_view_embed_browser"));
        a();
        b();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        d.a().b((b) this);
        d.a().b((com.wiyun.game.b.a) this);
        this.c.stopLoading();
        this.c.setWebViewClient(null);
        this.c.setWebChromeClient(null);
        this.b.removeView(this.c);
        this.c.destroy();
        super.onDestroy();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || event.getRepeatCount() != 0) {
            return super.onKeyDown(keyCode, event);
        }
        this.k = true;
        return true;
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode != 4 || !this.k) {
            return super.onKeyUp(keyCode, event);
        }
        this.k = false;
        finish();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        this.a.setVisibility(0);
        h.a(this.b);
        if (TextUtils.isEmpty(this.d)) {
            f.a(this.h, this.i);
        } else {
            this.c.loadUrl(this.d);
        }
    }
}
