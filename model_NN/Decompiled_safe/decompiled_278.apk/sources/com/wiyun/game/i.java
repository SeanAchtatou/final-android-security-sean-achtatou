package com.wiyun.game;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.google.ads.AdActivity;
import com.wiyun.game.WiGame;
import com.wiyun.game.b;
import com.wiyun.game.e.a;
import com.wiyun.game.model.ChallengeResult;
import com.wiyun.game.model.a.ab;
import java.io.File;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

class i {
    private static SQLiteDatabase a;
    private static int b;

    i() {
    }

    static Cursor a(String str, String str2, boolean z) {
        Calendar calendar;
        boolean equals = "A".equals(str2);
        if (!equals) {
            calendar = GregorianCalendar.getInstance();
            calendar.setTimeInMillis(System.currentTimeMillis());
            calendar.set(10, 0);
            calendar.set(12, 0);
            calendar.set(13, 0);
            calendar.set(14, 0);
        } else {
            calendar = null;
        }
        if ("W".equals(str2)) {
            calendar.add(5, calendar.getFirstDayOfWeek() - calendar.get(7));
        } else if ("M".equals(str2)) {
            calendar.add(5, (-calendar.get(5)) + 1);
        }
        StringBuilder sb = new StringBuilder();
        sb.append("lb_id").append("='").append(str).append("' AND ").append("uid").append("='").append(WiGame.getMyId()).append("'");
        if (!equals) {
            sb.append(" AND ").append("ct").append(">=").append(calendar.getTimeInMillis());
        }
        return a.query("score", b.g.a, sb.toString(), null, null, null, z ? "score ASC" : "score DESC", "10");
    }

    static String a(Cursor cursor, int i, int i2) {
        StringBuilder sb = new StringBuilder();
        sb.append(cursor.getString(1)).append(':').append(cursor.getString(2)).append(':').append(cursor.getString(3)).append(':').append(cursor.getString(10)).append(':').append(cursor.getString(4)).append(':').append(cursor.getInt(5)).append(':').append(i).append(':').append(i2).append(':').append(cursor.getString(7)).append(':').append(WiGame.q());
        com.wiyun.game.a.i iVar = new com.wiyun.game.a.i();
        iVar.a(true);
        return h.b(h.a(iVar, a.a(h.d(sb.toString())), AdActivity.INTENT_ACTION_PARAM));
    }

    static String a(ab abVar, int i, int i2) {
        StringBuilder sb = new StringBuilder();
        sb.append(WiGame.getMyId()).append(':').append(abVar.a()).append(':').append(abVar.b()).append(':').append(abVar.c()).append(':').append(abVar.e()).append(':').append(abVar.g() ? 1 : 0).append(':').append(i).append(':').append(i2).append(':').append(abVar.i()).append(':').append(WiGame.q());
        com.wiyun.game.a.i iVar = new com.wiyun.game.a.i();
        iVar.a(true);
        return h.b(h.a(iVar, a.a(h.d(sb.toString())), AdActivity.INTENT_ACTION_PARAM));
    }

    static synchronized void a() {
        synchronized (i.class) {
            b--;
            b = Math.max(0, b);
            if (b == 0 && a != null) {
                a.close();
                a = null;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void} */
    static void a(long j) {
        StringBuilder sb = new StringBuilder();
        sb.append("_id").append("=? AND ").append("uid").append("=?");
        ContentValues contentValues = new ContentValues();
        contentValues.put("done", (Boolean) true);
        a.update("pcr", contentValues, sb.toString(), new String[]{String.valueOf(j), WiGame.getMyId()});
    }

    static void a(long j, int i) {
        StringBuilder sb = new StringBuilder();
        sb.append("_id").append("=? AND ").append("uid").append("=?");
        ContentValues contentValues = new ContentValues();
        contentValues.put("fc", Integer.valueOf(i + 1));
        a.update("pcr", contentValues, sb.toString(), new String[]{String.valueOf(j), WiGame.getMyId()});
    }

    private static void a(ChallengeResult result, ContentValues values) {
        com.wiyun.game.a.i a2 = new com.wiyun.game.a.i();
        a2.a(true);
        values.put("uid", result.getUserId());
        values.put("ctu_id", result.getCtuId());
        values.put("score", Integer.valueOf(result.getScore()));
        values.put("result", Integer.valueOf(result.getResult()));
        if (result.getBlob() != null) {
            values.put("_blob", result.getBlob());
        }
        StringBuilder buf = new StringBuilder();
        buf.append(result.getCtuId()).append(':').append(result.getScore()).append(':').append(result).append(':').append(result.getBlob() == null ? "" : Arrays.toString(a.a(result.getBlob())));
        values.put("sig", h.a(a2, a.a(h.d(buf.toString())), "wiyun.db"));
    }

    static synchronized void a(File dir) {
        synchronized (i.class) {
            if (a == null) {
                File file = new File(dir, "wiyun.db");
                boolean exist = file.exists();
                a = SQLiteDatabase.openOrCreateDatabase(file, (SQLiteDatabase.CursorFactory) null);
                a.setLockingEnabled(true);
                b = 1;
                if (!exist) {
                    a.setVersion(4);
                    b.j.a(a);
                    b.g.a(a);
                    b.C0002b.a(a);
                    b.a.a(a);
                    b.h.a(a);
                    b.e.a(a);
                } else {
                    for (int oldVersion = a.getVersion(); 4 > oldVersion; oldVersion++) {
                        switch (oldVersion) {
                            case 1:
                                o();
                                break;
                            case 2:
                                n();
                                break;
                            case 3:
                                m();
                                break;
                        }
                    }
                    a.setVersion(4);
                }
            } else {
                b++;
            }
        }
    }

    static void a(String str, int i) {
        StringBuilder sb = new StringBuilder();
        sb.append("uid").append("='").append(WiGame.getMyId()).append("' AND ").append("lb_id").append("='").append(str).append('\'');
        ContentValues contentValues = new ContentValues();
        contentValues.put("score", Integer.valueOf(i));
        if (a.update("hscore", contentValues, sb.toString(), null) != 1) {
            contentValues.put("lb_id", str);
            contentValues.put("uid", WiGame.getMyId());
            a.insert("hscore", null, contentValues);
        }
    }

    static void a(List<ab> list) {
        Object[] i = i();
        Map map = (Map) i[0];
        Map map2 = (Map) i[1];
        a.beginTransaction();
        try {
            StringBuilder sb = new StringBuilder();
            ContentValues contentValues = new ContentValues();
            for (ab next : list) {
                contentValues.clear();
                contentValues.put("n", next.b());
                contentValues.put("a", next.c());
                contentValues.put("d", next.e());
                contentValues.put("c", Integer.valueOf(next.g() ? 1 : 0));
                contentValues.put("p", next.i());
                contentValues.put("iu", next.d());
                contentValues.put("did", next.h());
                if (map.containsKey(next.a())) {
                    sb.setLength(0);
                    sb.append("uid").append("='").append(WiGame.getMyId()).append("' AND ").append("iid").append("='").append(next.a()).append('\'');
                    String sb2 = sb.toString();
                    int intValue = ((Integer) map.get(next.a())).intValue();
                    int intValue2 = ((Integer) map2.get(next.a())).intValue();
                    int max = Math.max(0, intValue + next.f());
                    contentValues.put("ct", Integer.valueOf(max));
                    contentValues.put("sig", a(next, max, intValue2));
                    a.update(AdActivity.INTENT_ACTION_PARAM, contentValues, sb2, null);
                } else {
                    contentValues.put("uid", WiGame.getMyId());
                    contentValues.put("iid", next.a());
                    contentValues.put("ct", Integer.valueOf(next.f()));
                    contentValues.put("sig", a(next, next.f(), 0));
                    a.insert(AdActivity.INTENT_ACTION_PARAM, null, contentValues);
                }
            }
            a.setTransactionSuccessful();
        } catch (Exception e) {
        } finally {
            a.endTransaction();
        }
    }

    static boolean a(WiGame.b achievement) {
        if (achievement == null) {
            return false;
        }
        StringBuilder buf = new StringBuilder();
        buf.append("ach_id").append("=? AND ").append("uid").append("=?");
        ContentValues values = new ContentValues();
        achievement.a(values);
        a.delete("ach", buf.toString(), new String[]{achievement.b, WiGame.getMyId()});
        return a.insert("ach", null, values) != -1;
    }

    static boolean a(WiGame.d pendingScore) {
        if (pendingScore == null) {
            return false;
        }
        ContentValues values = new ContentValues();
        pendingScore.a(values);
        return a.insert("score", null, values) != -1;
    }

    static boolean a(ChallengeResult result) {
        if (result == null) {
            return false;
        }
        ContentValues values = new ContentValues();
        a(result, values);
        return a.insert("pcr", null, values) != -1;
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x006e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static boolean a(java.lang.String r12) {
        /*
            r10 = 0
            r9 = 1
            r8 = 0
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0061, all -> 0x006a }
            r3.<init>()     // Catch:{ Exception -> 0x0061, all -> 0x006a }
            java.lang.String r0 = "ach_id"
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ Exception -> 0x0061, all -> 0x006a }
            java.lang.String r1 = "=? AND "
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x0061, all -> 0x006a }
            java.lang.String r1 = "uid"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x0061, all -> 0x006a }
            java.lang.String r1 = "=?"
            r0.append(r1)     // Catch:{ Exception -> 0x0061, all -> 0x006a }
            android.database.sqlite.SQLiteDatabase r0 = com.wiyun.game.i.a     // Catch:{ Exception -> 0x0061, all -> 0x006a }
            java.lang.String r1 = "ach"
            r2 = 1
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ Exception -> 0x0061, all -> 0x006a }
            r4 = 0
            java.lang.String r5 = "count()"
            r2[r4] = r5     // Catch:{ Exception -> 0x0061, all -> 0x006a }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0061, all -> 0x006a }
            r4 = 2
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x0061, all -> 0x006a }
            r5 = 0
            r4[r5] = r12     // Catch:{ Exception -> 0x0061, all -> 0x006a }
            r5 = 1
            java.lang.String r6 = com.wiyun.game.WiGame.getMyId()     // Catch:{ Exception -> 0x0061, all -> 0x006a }
            r4[r5] = r6     // Catch:{ Exception -> 0x0061, all -> 0x006a }
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x0061, all -> 0x006a }
            boolean r1 = r0.moveToFirst()     // Catch:{ Exception -> 0x0077, all -> 0x0072 }
            if (r1 == 0) goto L_0x005a
            r1 = 0
            int r1 = r0.getInt(r1)     // Catch:{ Exception -> 0x0077, all -> 0x0072 }
            if (r1 != r9) goto L_0x0058
            r1 = r9
        L_0x0051:
            if (r0 == 0) goto L_0x0056
            r0.close()
        L_0x0056:
            r0 = r1
        L_0x0057:
            return r0
        L_0x0058:
            r1 = r8
            goto L_0x0051
        L_0x005a:
            if (r0 == 0) goto L_0x005f
            r0.close()
        L_0x005f:
            r0 = r8
            goto L_0x0057
        L_0x0061:
            r0 = move-exception
            r0 = r10
        L_0x0063:
            if (r0 == 0) goto L_0x0068
            r0.close()
        L_0x0068:
            r0 = r8
            goto L_0x0057
        L_0x006a:
            r0 = move-exception
            r1 = r10
        L_0x006c:
            if (r1 == 0) goto L_0x0071
            r1.close()
        L_0x0071:
            throw r0
        L_0x0072:
            r1 = move-exception
            r11 = r1
            r1 = r0
            r0 = r11
            goto L_0x006c
        L_0x0077:
            r1 = move-exception
            goto L_0x0063
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wiyun.game.i.a(java.lang.String):boolean");
    }

    static Cursor b() {
        return a.query("pcr", b.j.a, "uid=?", new String[]{WiGame.getMyId()}, null, null, null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void} */
    static void b(long j) {
        StringBuilder sb = new StringBuilder();
        sb.append("_id").append("=? AND ").append("uid").append("=?");
        ContentValues contentValues = new ContentValues();
        contentValues.put("done", (Boolean) true);
        a.update("score", contentValues, sb.toString(), new String[]{String.valueOf(j), WiGame.getMyId()});
    }

    static void b(long j, int i) {
        StringBuilder sb = new StringBuilder();
        sb.append("_id").append("=? AND ").append("uid").append("=?");
        ContentValues contentValues = new ContentValues();
        contentValues.put("fc", Integer.valueOf(i + 1));
        a.update("score", contentValues, sb.toString(), new String[]{String.valueOf(j), WiGame.getMyId()});
    }

    static void b(String str) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("uid", WiGame.getMyId());
        contentValues.put("sig", str);
        StringBuilder sb = new StringBuilder();
        sb.append("uid").append("='").append(WiGame.getMyId()).append('\'');
        a.delete("itsig", sb.toString(), null);
        a.insert("itsig", null, contentValues);
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x0074  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static int c(java.lang.String r11) {
        /*
            r9 = 0
            r8 = 0
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r0 = "uid"
            java.lang.StringBuilder r0 = r3.append(r0)
            java.lang.String r1 = "='"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = com.wiyun.game.WiGame.getMyId()
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "' AND "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "lb_id"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "='"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r11)
            r1 = 39
            r0.append(r1)
            android.database.sqlite.SQLiteDatabase r0 = com.wiyun.game.i.a     // Catch:{ Exception -> 0x0067, all -> 0x0070 }
            java.lang.String r1 = "hscore"
            r2 = 1
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ Exception -> 0x0067, all -> 0x0070 }
            r4 = 0
            java.lang.String r5 = "score"
            r2[r4] = r5     // Catch:{ Exception -> 0x0067, all -> 0x0070 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0067, all -> 0x0070 }
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x0067, all -> 0x0070 }
            boolean r1 = r0.moveToFirst()     // Catch:{ Exception -> 0x007d, all -> 0x0078 }
            if (r1 == 0) goto L_0x0060
            r1 = 0
            int r1 = r0.getInt(r1)     // Catch:{ Exception -> 0x007d, all -> 0x0078 }
            if (r0 == 0) goto L_0x005e
            r0.close()
        L_0x005e:
            r0 = r1
        L_0x005f:
            return r0
        L_0x0060:
            if (r0 == 0) goto L_0x0065
            r0.close()
        L_0x0065:
            r0 = r9
            goto L_0x005f
        L_0x0067:
            r0 = move-exception
            r0 = r8
        L_0x0069:
            if (r0 == 0) goto L_0x006e
            r0.close()
        L_0x006e:
            r0 = r9
            goto L_0x005f
        L_0x0070:
            r0 = move-exception
            r1 = r8
        L_0x0072:
            if (r1 == 0) goto L_0x0077
            r1.close()
        L_0x0077:
            throw r0
        L_0x0078:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
            goto L_0x0072
        L_0x007d:
            r1 = move-exception
            goto L_0x0069
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wiyun.game.i.c(java.lang.String):int");
    }

    static Cursor c() {
        StringBuilder sb = new StringBuilder();
        sb.append("done").append("=0 AND ").append("uid").append("=?");
        return a.query("score", b.g.a, sb.toString(), new String[]{WiGame.getMyId()}, null, null, null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void} */
    static void c(long j) {
        StringBuilder sb = new StringBuilder();
        sb.append("_id").append("=? AND ").append("uid").append("=?");
        ContentValues contentValues = new ContentValues();
        contentValues.put("done", (Boolean) true);
        a.update("ach", contentValues, sb.toString(), new String[]{String.valueOf(j), WiGame.getMyId()});
    }

    static void c(long j, int i) {
        StringBuilder sb = new StringBuilder();
        sb.append("_id").append("=? AND ").append("uid").append("=?");
        ContentValues contentValues = new ContentValues();
        contentValues.put("fc", Integer.valueOf(i + 1));
        a.update("ach", contentValues, sb.toString(), new String[]{String.valueOf(j), WiGame.getMyId()});
    }

    static Cursor d() {
        StringBuilder sb = new StringBuilder();
        sb.append("done").append("=0 AND ").append("uid").append("=?");
        return a.query("ach", b.C0002b.a, sb.toString(), new String[]{WiGame.getMyId()}, null, null, null);
    }

    static Cursor e() {
        return a.query("ach", b.C0002b.a, "uid=?", new String[]{WiGame.getMyId()}, null, null, null);
    }

    static void f() {
        a.delete("pcr", "done=1", null);
    }

    static void g() {
        ContentValues contentValues = new ContentValues();
        contentValues.put("uid", WiGame.getMyId());
        a.update("ach", contentValues, "uid=''", null);
        a.update("score", contentValues, "uid=''", null);
    }

    static void h() {
        StringBuilder sb = new StringBuilder();
        sb.append("uid").append("='").append(WiGame.getMyId()).append("' AND ").append("ct").append("=0 AND ").append("uc").append("=0");
        a.delete(AdActivity.INTENT_ACTION_PARAM, sb.toString(), null);
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x0092  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static java.lang.Object[] i() {
        /*
            r2 = 2
            r11 = 0
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r0 = "uid"
            java.lang.StringBuilder r0 = r3.append(r0)
            java.lang.String r1 = "='"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = com.wiyun.game.WiGame.getMyId()
            java.lang.StringBuilder r0 = r0.append(r1)
            r1 = 39
            r0.append(r1)
            java.lang.Object[] r8 = new java.lang.Object[r2]
            java.util.HashMap r9 = new java.util.HashMap
            r9.<init>()
            java.util.HashMap r10 = new java.util.HashMap
            r10.<init>()
            r0 = 0
            r8[r0] = r9
            r0 = 1
            r8[r0] = r10
            android.database.sqlite.SQLiteDatabase r0 = com.wiyun.game.i.a     // Catch:{ Exception -> 0x009d, all -> 0x008e }
            java.lang.String r1 = "i"
            java.lang.String[] r2 = com.wiyun.game.b.a.a     // Catch:{ Exception -> 0x009d, all -> 0x008e }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x009d, all -> 0x008e }
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x009d, all -> 0x008e }
            boolean r1 = r0.moveToFirst()     // Catch:{ Exception -> 0x007d, all -> 0x0096 }
            if (r1 == 0) goto L_0x0050
        L_0x004a:
            boolean r1 = r0.isAfterLast()     // Catch:{ Exception -> 0x007d, all -> 0x0096 }
            if (r1 == 0) goto L_0x0056
        L_0x0050:
            if (r0 == 0) goto L_0x0055
            r0.close()
        L_0x0055:
            return r8
        L_0x0056:
            r1 = 2
            java.lang.String r1 = r0.getString(r1)     // Catch:{ Exception -> 0x007d, all -> 0x0096 }
            r2 = 6
            int r2 = r0.getInt(r2)     // Catch:{ Exception -> 0x007d, all -> 0x0096 }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ Exception -> 0x007d, all -> 0x0096 }
            r9.put(r1, r2)     // Catch:{ Exception -> 0x007d, all -> 0x0096 }
            r1 = 2
            java.lang.String r1 = r0.getString(r1)     // Catch:{ Exception -> 0x007d, all -> 0x0096 }
            r2 = 9
            int r2 = r0.getInt(r2)     // Catch:{ Exception -> 0x007d, all -> 0x0096 }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ Exception -> 0x007d, all -> 0x0096 }
            r10.put(r1, r2)     // Catch:{ Exception -> 0x007d, all -> 0x0096 }
            r0.moveToNext()     // Catch:{ Exception -> 0x007d, all -> 0x0096 }
            goto L_0x004a
        L_0x007d:
            r1 = move-exception
            r12 = r1
            r1 = r0
            r0 = r12
        L_0x0081:
            java.lang.String r2 = "WiYun"
            java.lang.String r3 = "failed to get item id map"
            android.util.Log.w(r2, r3, r0)     // Catch:{ all -> 0x009b }
            if (r1 == 0) goto L_0x0055
            r1.close()
            goto L_0x0055
        L_0x008e:
            r0 = move-exception
            r1 = r11
        L_0x0090:
            if (r1 == 0) goto L_0x0095
            r1.close()
        L_0x0095:
            throw r0
        L_0x0096:
            r1 = move-exception
            r12 = r1
            r1 = r0
            r0 = r12
            goto L_0x0090
        L_0x009b:
            r0 = move-exception
            goto L_0x0090
        L_0x009d:
            r0 = move-exception
            r1 = r11
            goto L_0x0081
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wiyun.game.i.i():java.lang.Object[]");
    }

    static String j() {
        Cursor cursor;
        StringBuilder sb = new StringBuilder();
        sb.append("uid").append("='").append(WiGame.getMyId()).append('\'');
        try {
            Cursor query = a.query("itsig", new String[]{"sig"}, sb.toString(), null, null, null, null);
            if (query != null) {
                try {
                    if (query.moveToFirst()) {
                        String string = query.getString(0);
                        if (query != null) {
                            query.close();
                        }
                        return string;
                    }
                } catch (Throwable th) {
                    Throwable th2 = th;
                    cursor = query;
                    th = th2;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
            if (query != null) {
                query.close();
            }
            return "";
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
        }
    }

    static Cursor k() {
        StringBuilder sb = new StringBuilder();
        sb.append("uid").append("='").append(WiGame.getMyId()).append("' AND ").append("uc").append(">0");
        return a.query(AdActivity.INTENT_ACTION_PARAM, b.a.a, sb.toString(), null, null, null, null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0059  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x009d  */
    /* JADX WARNING: Removed duplicated region for block: B:42:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static void l() {
        /*
            r10 = 0
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r0 = "uid"
            java.lang.StringBuilder r0 = r8.append(r0)
            java.lang.String r1 = "='"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = com.wiyun.game.WiGame.getMyId()
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "' AND "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "uc"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = ">0"
            r0.append(r1)
            java.util.HashMap r9 = new java.util.HashMap
            r9.<init>()
            android.database.sqlite.SQLiteDatabase r0 = com.wiyun.game.i.a     // Catch:{ Exception -> 0x010c, all -> 0x0099 }
            java.lang.String r1 = "i"
            java.lang.String[] r2 = com.wiyun.game.b.a.a     // Catch:{ Exception -> 0x010c, all -> 0x0099 }
            java.lang.String r3 = r8.toString()     // Catch:{ Exception -> 0x010c, all -> 0x0099 }
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x010c, all -> 0x0099 }
            boolean r1 = r0.moveToFirst()     // Catch:{ Exception -> 0x0092, all -> 0x0107 }
            if (r1 == 0) goto L_0x004e
        L_0x0048:
            boolean r1 = r0.isAfterLast()     // Catch:{ Exception -> 0x0092, all -> 0x0107 }
            if (r1 == 0) goto L_0x007c
        L_0x004e:
            if (r0 == 0) goto L_0x0053
            r0.close()
        L_0x0053:
            boolean r0 = r9.isEmpty()
            if (r0 != 0) goto L_0x007b
            android.database.sqlite.SQLiteDatabase r0 = com.wiyun.game.i.a
            r0.beginTransaction()
            android.content.ContentValues r1 = new android.content.ContentValues     // Catch:{ all -> 0x0100 }
            r1.<init>()     // Catch:{ all -> 0x0100 }
            java.util.Set r0 = r9.keySet()     // Catch:{ all -> 0x0100 }
            java.util.Iterator r2 = r0.iterator()     // Catch:{ all -> 0x0100 }
        L_0x006b:
            boolean r0 = r2.hasNext()     // Catch:{ all -> 0x0100 }
            if (r0 != 0) goto L_0x00a1
            android.database.sqlite.SQLiteDatabase r0 = com.wiyun.game.i.a     // Catch:{ all -> 0x0100 }
            r0.setTransactionSuccessful()     // Catch:{ all -> 0x0100 }
            android.database.sqlite.SQLiteDatabase r0 = com.wiyun.game.i.a
            r0.endTransaction()
        L_0x007b:
            return
        L_0x007c:
            r1 = 2
            java.lang.String r1 = r0.getString(r1)     // Catch:{ Exception -> 0x0092, all -> 0x0107 }
            r2 = 6
            int r2 = r0.getInt(r2)     // Catch:{ Exception -> 0x0092, all -> 0x0107 }
            r3 = 0
            java.lang.String r2 = a(r0, r2, r3)     // Catch:{ Exception -> 0x0092, all -> 0x0107 }
            r9.put(r1, r2)     // Catch:{ Exception -> 0x0092, all -> 0x0107 }
            r0.moveToNext()     // Catch:{ Exception -> 0x0092, all -> 0x0107 }
            goto L_0x0048
        L_0x0092:
            r1 = move-exception
        L_0x0093:
            if (r0 == 0) goto L_0x0053
            r0.close()
            goto L_0x0053
        L_0x0099:
            r0 = move-exception
            r1 = r10
        L_0x009b:
            if (r1 == 0) goto L_0x00a0
            r1.close()
        L_0x00a0:
            throw r0
        L_0x00a1:
            java.lang.Object r0 = r2.next()     // Catch:{ all -> 0x0100 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ all -> 0x0100 }
            r3 = 0
            r8.setLength(r3)     // Catch:{ all -> 0x0100 }
            java.lang.String r3 = "uid"
            java.lang.StringBuilder r3 = r8.append(r3)     // Catch:{ all -> 0x0100 }
            java.lang.String r4 = "='"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0100 }
            java.lang.String r4 = com.wiyun.game.WiGame.getMyId()     // Catch:{ all -> 0x0100 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0100 }
            java.lang.String r4 = "' AND "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0100 }
            java.lang.String r4 = "iid"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0100 }
            java.lang.String r4 = "='"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0100 }
            java.lang.StringBuilder r3 = r3.append(r0)     // Catch:{ all -> 0x0100 }
            r4 = 39
            r3.append(r4)     // Catch:{ all -> 0x0100 }
            r1.clear()     // Catch:{ all -> 0x0100 }
            java.lang.String r3 = "uc"
            r4 = 0
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ all -> 0x0100 }
            r1.put(r3, r4)     // Catch:{ all -> 0x0100 }
            java.lang.String r3 = "sig"
            java.lang.Object r0 = r9.get(r0)     // Catch:{ all -> 0x0100 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ all -> 0x0100 }
            r1.put(r3, r0)     // Catch:{ all -> 0x0100 }
            android.database.sqlite.SQLiteDatabase r0 = com.wiyun.game.i.a     // Catch:{ all -> 0x0100 }
            java.lang.String r3 = "i"
            java.lang.String r4 = r8.toString()     // Catch:{ all -> 0x0100 }
            r5 = 0
            r0.update(r3, r1, r4, r5)     // Catch:{ all -> 0x0100 }
            goto L_0x006b
        L_0x0100:
            r0 = move-exception
            android.database.sqlite.SQLiteDatabase r1 = com.wiyun.game.i.a
            r1.endTransaction()
            throw r0
        L_0x0107:
            r1 = move-exception
            r11 = r1
            r1 = r0
            r0 = r11
            goto L_0x009b
        L_0x010c:
            r0 = move-exception
            r0 = r10
            goto L_0x0093
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wiyun.game.i.l():void");
    }

    private static void m() {
        b.e.a(a);
    }

    private static void n() {
        b.a.a(a);
        b.h.a(a);
    }

    private static void o() {
        b.j.b(a);
        b.g.b(a);
        b.C0002b.b(a);
        b.j.a(a);
        b.g.a(a);
        b.C0002b.a(a);
    }
}
