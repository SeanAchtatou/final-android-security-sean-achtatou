package com.wiyun.game.b;

import com.saubcy.games.maze.market.MazeGame;
import com.saubcy.games.maze.market.R;
import com.wiyun.game.model.a.a;
import com.wiyun.game.model.a.aa;
import com.wiyun.game.model.a.ab;
import com.wiyun.game.model.a.ac;
import com.wiyun.game.model.a.ad;
import com.wiyun.game.model.a.ae;
import com.wiyun.game.model.a.ag;
import com.wiyun.game.model.a.ah;
import com.wiyun.game.model.a.ai;
import com.wiyun.game.model.a.b;
import com.wiyun.game.model.a.d;
import com.wiyun.game.model.a.f;
import com.wiyun.game.model.a.g;
import com.wiyun.game.model.a.h;
import com.wiyun.game.model.a.i;
import com.wiyun.game.model.a.j;
import com.wiyun.game.model.a.l;
import com.wiyun.game.model.a.m;
import com.wiyun.game.model.a.o;
import com.wiyun.game.model.a.p;
import com.wiyun.game.model.a.q;
import com.wiyun.game.model.a.r;
import com.wiyun.game.model.a.s;
import com.wiyun.game.model.a.v;
import com.wiyun.game.model.a.x;
import com.wiyun.game.model.a.y;
import com.wiyun.game.model.a.z;
import com.wiyun.game.t;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class c {
    private static void A(String body, e e) throws JSONException {
        JSONObject json = a(body);
        e.g = json.optInt("total_count");
        List topics = new ArrayList();
        JSONArray topicArray = json.optJSONArray("items");
        if (topicArray != null) {
            int count = topicArray.length();
            for (int i = 0; i < count; i++) {
                q t = q.a(topicArray.getJSONObject(i));
                if (t != null) {
                    topics.add(t);
                }
            }
        }
        e.e = topics;
    }

    private static void B(String body, e e) throws JSONException {
        JSONArray challengeArray;
        JSONObject json = a(body);
        a ci = a.a(json);
        if (!(ci == null || (challengeArray = json.optJSONArray("items")) == null)) {
            int count = challengeArray.length();
            for (int i = 0; i < count; i++) {
                v u = v.c(challengeArray.getJSONObject(i));
                if (u != null) {
                    ci.a(u);
                }
            }
        }
        e.e = ci;
        if (e.e == null) {
            e.c = true;
        }
    }

    private static void C(String body, e e) throws JSONException {
        JSONObject json = a(body);
        e.g = json.optInt("total_count");
        JSONArray challengeArray = json.optJSONArray("items");
        List challenges = new ArrayList();
        if (challengeArray != null) {
            int count = challengeArray.length();
            for (int i = 0; i < count; i++) {
                h c = h.a(challengeArray.getJSONObject(i));
                if (c != null) {
                    challenges.add(c);
                }
            }
        }
        e.e = challenges;
    }

    private static void D(String body, e e) throws JSONException {
        JSONObject json = a(body);
        e.g = json.optInt("total_count");
        JSONArray challengeArray = json.optJSONArray("items");
        List challenges = new ArrayList();
        if (challengeArray != null) {
            int count = challengeArray.length();
            for (int i = 0; i < count; i++) {
                z c = z.a(challengeArray.getJSONObject(i));
                if (c != null) {
                    challenges.add(c);
                }
            }
        }
        e.e = challenges;
    }

    private static void E(String body, e e) throws JSONException {
        JSONObject json = a(body);
        e.g = json.optInt("total_count");
        JSONArray challengeArray = json.optJSONArray("items");
        List challenges = new ArrayList();
        if (challengeArray != null) {
            int count = challengeArray.length();
            for (int i = 0; i < count; i++) {
                g h = g.a(challengeArray.getJSONObject(i));
                if (h != null) {
                    challenges.add(h);
                }
            }
        }
        e.e = challenges;
    }

    private static void F(String body, e e) throws JSONException {
        JSONObject json = a(body);
        e.g = json.optInt("total_count");
        JSONArray challengeArray = json.optJSONArray("items");
        List challenges = new ArrayList();
        if (challengeArray != null) {
            int count = challengeArray.length();
            for (int i = 0; i < count; i++) {
                d a = d.a(challengeArray.getJSONObject(i));
                if (a != null) {
                    challenges.add(a);
                }
            }
        }
        e.e = challenges;
    }

    private static void G(String body, e e) throws JSONException {
        e.e = ai.a(a(body));
        e.f = body;
        if (e.e == null) {
            e.c = true;
        }
    }

    private static void H(String body, e e) throws JSONException {
        JSONObject json = a(body);
        e.g = json.optInt("total_count");
        JSONArray array = json.optJSONArray("items");
        List accomplishers = new ArrayList();
        if (array != null) {
            int count = array.length();
            for (int i = 0; i < count; i++) {
                s a = s.a(array.getJSONObject(i));
                if (a != null) {
                    accomplishers.add(a);
                }
            }
        }
        e.e = accomplishers;
    }

    private static void I(String body, e e) throws JSONException {
        JSONObject json = a(body);
        e.g = json.optInt("total_count");
        JSONArray achievementArray = json.optJSONArray("items");
        List achievements = new ArrayList();
        if (achievementArray != null) {
            int count = achievementArray.length();
            for (int i = 0; i < count; i++) {
                f a = f.a(achievementArray.getJSONObject(i));
                if (a != null) {
                    achievements.add(a);
                }
            }
        }
        e.e = achievements;
    }

    private static void J(String body, e e) throws JSONException {
        JSONObject json = a(body);
        e.g = json.optInt("total_count");
        JSONArray friendArray = json.optJSONArray("items");
        List neighbors = new ArrayList();
        if (friendArray != null) {
            int count = friendArray.length();
            for (int i = 0; i < count; i++) {
                ag u = ag.a(friendArray.getJSONObject(i));
                if (u != null) {
                    neighbors.add(u);
                }
            }
        }
        e.e = neighbors;
    }

    private static void K(String body, e e) throws JSONException {
        JSONArray friendArray = a(body).optJSONArray("items");
        List friends = new ArrayList();
        if (friendArray != null) {
            int count = friendArray.length();
            for (int i = 0; i < count; i++) {
                ag u = ag.a(friendArray.getJSONObject(i));
                if (u != null) {
                    friends.add(u);
                }
            }
        }
        e.e = friends;
    }

    private static void L(String body, e e) throws JSONException {
        JSONObject json = a(body);
        e.g = json.optInt("total_count");
        JSONArray inviteArray = json.optJSONArray("items");
        List invitations = new ArrayList();
        if (inviteArray != null) {
            int count = inviteArray.length();
            for (int i = 0; i < count; i++) {
                j u = j.a(inviteArray.getJSONObject(i));
                if (u != null) {
                    invitations.add(u);
                }
            }
        }
        e.e = invitations;
    }

    private static void M(String body, e e) throws JSONException {
        JSONObject json = a(body);
        e.g = json.optInt("total_count");
        JSONArray appArray = json.optJSONArray("items");
        List apps = new ArrayList();
        if (appArray != null) {
            int count = appArray.length();
            for (int i = 0; i < count; i++) {
                b u = b.a(appArray.getJSONObject(i));
                if (u != null) {
                    apps.add(u);
                }
            }
        }
        e.e = apps;
    }

    private static void N(String body, e e) throws JSONException {
        JSONObject json = a(body);
        e.g = json.optInt("total_count");
        JSONArray inviteArray = json.optJSONArray("items");
        List pending = new ArrayList();
        if (inviteArray != null) {
            int count = inviteArray.length();
            for (int i = 0; i < count; i++) {
                aa fi = aa.a(inviteArray.getJSONObject(i));
                if (fi != null) {
                    pending.add(fi);
                }
            }
        }
        e.e = pending;
    }

    private static void O(String body, e e) throws JSONException {
        e.e = p.a(a(body));
    }

    private static void P(String body, e e) throws JSONException {
        JSONObject json = a(body);
        e.g = json.optInt("total_count");
        List scores = new ArrayList();
        JSONArray scoreArray = json.optJSONArray("items");
        if (scoreArray != null) {
            int length = scoreArray.length();
            for (int i = 0; i < length; i++) {
                p score = p.a(scoreArray.getJSONObject(i));
                if (score != null) {
                    score.c(i + 1);
                    scores.add(score);
                }
            }
        }
        e.e = scores;
    }

    private static void Q(String body, e e) throws JSONException {
        e.g = a(body).optInt("rank");
    }

    private static void R(String body, e e) throws JSONException {
        e.e = ag.a(a(body));
        if (e.e == null) {
            e.c = true;
        }
    }

    private static void S(String body, e e) throws JSONException {
        JSONObject json = a(body);
        List boards = new ArrayList();
        JSONArray boardArray = json.optJSONArray("items");
        if (boardArray != null) {
            int count = boardArray.length();
            for (int i = 0; i < count; i++) {
                com.wiyun.game.model.a.c board = com.wiyun.game.model.a.c.a(boardArray.getJSONObject(i));
                if (board != null) {
                    boards.add(board);
                }
            }
        }
        if (boards.isEmpty()) {
            e.c = true;
            e.e = t.h("wy_toast_server_error");
            return;
        }
        e.e = boards;
    }

    private static void T(String body, e e) throws JSONException {
        e.e = ag.a(a(body));
        if (e.e == null) {
            e.c = true;
        }
    }

    private static void U(String body, e e) throws JSONException {
        e.e = ah.a(a(body));
        if (e.e == null) {
            e.c = true;
        }
    }

    private static void V(String str, e eVar) throws JSONException {
        eVar.e = a(str).optString("user_id");
        if (eVar.e == null) {
            eVar.c = true;
        }
    }

    private static void W(String str, e eVar) throws JSONException {
        JSONObject a = a(str);
        if (a.optBoolean("new")) {
            eVar.e = a.optString("username", "");
            return;
        }
        ArrayList arrayList = new ArrayList();
        JSONArray optJSONArray = a.optJSONArray("items");
        if (optJSONArray != null) {
            int length = optJSONArray.length();
            for (int i = 0; i < length; i++) {
                ag a2 = ag.a(optJSONArray.getJSONObject(i));
                if (a2 != null) {
                    arrayList.add(a2);
                }
            }
        }
        if (arrayList.isEmpty()) {
            eVar.c = true;
            eVar.e = t.h("wy_toast_server_error");
            return;
        }
        eVar.e = arrayList;
    }

    private static JSONObject a(String body) throws JSONException {
        if (!body.startsWith("[")) {
            return new JSONObject(body);
        }
        StringBuilder buf = new StringBuilder();
        buf.append("{\"").append("items").append("\":");
        buf.append(body);
        buf.append('}');
        return new JSONObject(buf.toString());
    }

    public static void a(String body, e e) {
        try {
            if (e.b < 300) {
                switch (e.a) {
                    case 1:
                        V(body, e);
                        break;
                    case 3:
                    case 4:
                        W(body, e);
                        break;
                    case 5:
                        T(body, e);
                        break;
                    case 6:
                        U(body, e);
                        break;
                    case R.styleable.com_wiyun_ad_AdView_testAdType:
                        G(body, e);
                        break;
                    case R.styleable.com_wiyun_ad_AdView_transition:
                        S(body, e);
                        break;
                    case MazeGame.WIDTHREDUCTION:
                        P(body, e);
                        break;
                    case 11:
                        O(body, e);
                        break;
                    case 12:
                        Q(body, e);
                        break;
                    case 14:
                        R(body, e);
                        break;
                    case 21:
                        J(body, e);
                        break;
                    case 22:
                        o(body, e);
                        break;
                    case 23:
                        K(body, e);
                        break;
                    case 24:
                        N(body, e);
                        break;
                    case 31:
                    case 72:
                        M(body, e);
                        break;
                    case 32:
                        p(body, e);
                        break;
                    case 33:
                        L(body, e);
                        break;
                    case 38:
                    case 39:
                        I(body, e);
                        break;
                    case 40:
                        H(body, e);
                        break;
                    case 43:
                        F(body, e);
                        break;
                    case 45:
                        E(body, e);
                        break;
                    case 47:
                        D(body, e);
                        break;
                    case 48:
                        C(body, e);
                        break;
                    case 49:
                        B(body, e);
                        break;
                    case MazeGame.HIGHTREDUCTION:
                        q(body, e);
                        break;
                    case 57:
                        A(body, e);
                        break;
                    case 59:
                        z(body, e);
                        break;
                    case 63:
                        y(body, e);
                        break;
                    case 64:
                        w(body, e);
                        break;
                    case 65:
                        v(body, e);
                        break;
                    case 66:
                        u(body, e);
                        break;
                    case 68:
                        x(body, e);
                        break;
                    case 69:
                        t(body, e);
                        break;
                    case 71:
                        s(body, e);
                        break;
                    case 73:
                        r(body, e);
                        break;
                    case 74:
                        n(body, e);
                        break;
                    case 77:
                        e(body, e);
                        break;
                    case 78:
                    case 79:
                    case 80:
                        d(body, e);
                        break;
                    case 81:
                        h(body, e);
                        break;
                    case 82:
                        f(body, e);
                        break;
                    case 83:
                        g(body, e);
                        break;
                    case 84:
                        i(body, e);
                        break;
                    case 85:
                        m(body, e);
                        break;
                    case 86:
                        l(body, e);
                        break;
                    case 87:
                        k(body, e);
                        break;
                    case 88:
                        j(body, e);
                        break;
                    case 90:
                        c(body, e);
                        break;
                    case 91:
                        b(body, e);
                        break;
                }
            } else {
                e.e = body;
                e.c = true;
            }
        } catch (JSONException e2) {
        } catch (Exception e3) {
            e.c = true;
        }
        if (!e.c) {
            return;
        }
        if (e.e == null || e.b == 500) {
            e.e = t.h("wy_toast_server_error");
            return;
        }
        try {
            e.e = a(body).optString("msg");
        } catch (JSONException e4) {
        }
    }

    private static void b(String body, e e) throws JSONException {
        e.e = body;
    }

    private static void c(String body, e e) throws JSONException {
        JSONArray array = a(body).optJSONArray("items");
        List ret = new ArrayList();
        if (array != null) {
            int count = array.length();
            for (int i = 0; i < count; i++) {
                ac c = ac.a(array.getJSONObject(i));
                if (c != null) {
                    ret.add(c);
                }
            }
        }
        e.e = ret;
    }

    private static void d(String body, e e) throws JSONException {
        e.e = a(body).optString("redirect_url");
    }

    private static void e(String body, e e) throws JSONException {
        e.e = a(body).optString("order_id");
    }

    private static void f(String body, e e) throws JSONException {
        e.e = l.a(a(body));
    }

    private static void g(String body, e e) throws JSONException {
        JSONArray cardArray = a(body).optJSONArray("items");
        List cards = new ArrayList();
        if (cardArray != null) {
            int count = cardArray.length();
            for (int i = 0; i < count; i++) {
                com.wiyun.game.model.a.t c = com.wiyun.game.model.a.t.a(cardArray.getJSONObject(i));
                if (c != null) {
                    cards.add(c);
                }
            }
        }
        e.e = cards;
    }

    private static void h(String str, e eVar) throws JSONException {
        JSONObject a = a(str);
        eVar.g = a.optInt("total_count");
        eVar.f = a.optString("items_sig");
        JSONArray optJSONArray = a.optJSONArray("items");
        ArrayList arrayList = new ArrayList();
        if (optJSONArray != null) {
            int length = optJSONArray.length();
            for (int i = 0; i < length; i++) {
                l a2 = l.a(optJSONArray.getJSONObject(i));
                if (a2 != null) {
                    arrayList.add(a2);
                }
            }
        }
        eVar.e = arrayList;
    }

    private static void i(String body, e e) throws JSONException {
        e.e = ab.a(a(body));
    }

    private static void j(String str, e eVar) throws JSONException {
        eVar.e = a(str).optString("items_sig");
    }

    private static void k(String str, e eVar) throws JSONException {
        eVar.e = a(str).optString("items_sig");
    }

    private static void l(String str, e eVar) throws JSONException {
        JSONObject a = a(str);
        eVar.g = a.optInt("total_count");
        eVar.f = a.optString("items_sig");
        JSONArray optJSONArray = a.optJSONArray("items");
        ArrayList arrayList = new ArrayList();
        if (optJSONArray != null) {
            int length = optJSONArray.length();
            for (int i = 0; i < length; i++) {
                ab a2 = ab.a(optJSONArray.getJSONObject(i));
                if (a2 != null) {
                    arrayList.add(a2);
                }
            }
        }
        eVar.e = arrayList;
    }

    private static void m(String body, e e) throws JSONException {
        JSONObject json = a(body);
        e.g = json.optInt("total_count");
        JSONArray itemArray = json.optJSONArray("items");
        List items = new ArrayList();
        if (itemArray != null) {
            int count = itemArray.length();
            for (int i = 0; i < count; i++) {
                ab item = ab.a(itemArray.getJSONObject(i));
                if (item != null) {
                    items.add(item);
                }
            }
        }
        e.e = items;
    }

    private static void n(String body, e e) throws JSONException {
        JSONObject json = a(body);
        e.g = json.optInt("total_count");
        JSONArray slotArray = json.optJSONArray("items");
        List slots = new ArrayList();
        if (slotArray != null) {
            int count = slotArray.length();
            for (int i = 0; i < count; i++) {
                i s = i.a(slotArray.getJSONObject(i));
                if (s != null) {
                    slots.add(s);
                }
            }
        }
        e.e = slots;
    }

    private static void o(String body, e e) throws JSONException {
        JSONArray friendArray = a(body).optJSONArray("items");
        List users = new ArrayList();
        if (friendArray != null) {
            int count = friendArray.length();
            for (int i = 0; i < count; i++) {
                ag u = ag.a(friendArray.getJSONObject(i));
                if (u != null) {
                    users.add(u);
                }
            }
        }
        e.e = users;
    }

    private static void p(String body, e e) throws JSONException {
        JSONArray appArray = a(body).optJSONArray("items");
        if (appArray != null) {
            int count = appArray.length();
            for (int i = 0; i < count; i++) {
                b u = b.a(appArray.getJSONObject(i));
                if (u != null) {
                    e.e = u;
                    return;
                }
            }
        }
    }

    private static void q(String body, e e) throws JSONException {
        e.e = r.f(a(body));
        if (e.e == null) {
            e.c = true;
        }
    }

    private static void r(String body, e e) throws JSONException {
        e.e = y.a(a(body));
        if (e.e == null) {
            e.c = true;
        }
    }

    private static void s(String body, e e) throws JSONException {
        e.e = ad.a(a(body));
        if (e.e == null) {
            e.c = true;
        }
    }

    private static void t(String body, e e) throws JSONException {
        JSONObject json = a(body);
        e.g = json.optInt("total_count");
        JSONArray noticeArray = json.optJSONArray("items");
        List notices = new ArrayList();
        if (noticeArray != null) {
            int count = noticeArray.length();
            for (int i = 0; i < count; i++) {
                o n = o.a(noticeArray.getJSONObject(i));
                if (n != null) {
                    notices.add(n);
                }
            }
        }
        e.e = notices;
    }

    private static void u(String body, e e) throws JSONException {
        JSONObject json = a(body);
        e.g = json.optInt("total_count");
        JSONArray msgArray = json.optJSONArray("items");
        List messages = new ArrayList();
        if (msgArray != null) {
            int count = msgArray.length();
            for (int i = 0; i < count; i++) {
                x m = x.a(msgArray.getJSONObject(i));
                if (m != null) {
                    messages.add(m);
                }
            }
        }
        e.e = messages;
    }

    private static void v(String body, e e) throws JSONException {
        JSONObject json = a(body);
        e.g = json.optInt("total_count");
        JSONArray threadArray = json.optJSONArray("items");
        List threads = new ArrayList();
        if (threadArray != null) {
            int count = threadArray.length();
            for (int i = 0; i < count; i++) {
                ae t = ae.a(threadArray.getJSONObject(i));
                if (t != null) {
                    threads.add(t);
                }
            }
        }
        e.e = threads;
    }

    private static void w(String body, e e) throws JSONException {
        JSONObject json = a(body);
        e.g = json.optInt("total_count");
        JSONArray userArray = json.optJSONArray("items");
        List users = new ArrayList();
        if (userArray != null) {
            int count = userArray.length();
            for (int i = 0; i < count; i++) {
                ag u = ag.a(userArray.getJSONObject(i));
                if (u != null) {
                    users.add(u);
                }
            }
        }
        e.e = users;
    }

    private static void x(String body, e e) throws JSONException {
        e.e = x.a(a(body));
        if (e.e == null) {
            e.c = true;
        }
    }

    private static void y(String str, e eVar) throws JSONException {
        JSONObject a = a(str);
        eVar.g = a.optInt("total_count");
        eVar.h = a.optInt("app_point");
        JSONArray optJSONArray = a.optJSONArray("items");
        ArrayList arrayList = new ArrayList();
        if (optJSONArray != null) {
            int length = optJSONArray.length();
            for (int i = 0; i < length; i++) {
                ag a2 = ag.a(optJSONArray.getJSONObject(i));
                if (a2 != null) {
                    arrayList.add(a2);
                }
            }
        }
        eVar.e = arrayList;
    }

    private static void z(String body, e e) throws JSONException {
        m r;
        JSONObject json = a(body);
        List replies = new ArrayList();
        JSONObject topic = json.optJSONObject("topic");
        if (!(topic == null || (r = m.a(topic)) == null)) {
            replies.add(r);
        }
        e.g = json.optInt("total_count");
        JSONArray replyArray = json.optJSONArray("items");
        if (replyArray != null) {
            int count = replyArray.length();
            for (int i = 0; i < count; i++) {
                m r2 = m.a(replyArray.getJSONObject(i));
                if (r2 != null) {
                    replies.add(r2);
                }
            }
        }
        e.e = replies;
    }
}
