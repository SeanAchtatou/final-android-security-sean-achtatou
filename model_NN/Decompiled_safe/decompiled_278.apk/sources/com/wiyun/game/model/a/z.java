package com.wiyun.game.model.a;

import org.json.JSONObject;

public class z {
    private r a;
    private u b;
    private v c;

    public static z a(JSONObject dict) {
        u c2;
        v toUser;
        r d = r.c(dict);
        if (d == null || (c2 = u.b(dict)) == null || (toUser = v.b(dict)) == null) {
            return null;
        }
        z ch = new z();
        ch.a(d);
        ch.a(c2);
        ch.a(toUser);
        return ch;
    }

    public void a(r challengeDefinition) {
        this.a = challengeDefinition;
    }

    public void a(u challenge) {
        this.b = challenge;
    }

    public void a(v challengeToUser) {
        this.c = challengeToUser;
    }
}
