package com.wiyun.game.model.a;

import org.json.JSONObject;

public class h {
    private int a;
    private String b;
    private String c;
    private int d;
    private r e;
    private u f;

    public static h a(JSONObject dict) {
        u c2;
        r d2 = r.d(dict);
        if (d2 == null || (c2 = u.c(dict)) == null) {
            return null;
        }
        h h = new h();
        h.a(d2);
        h.a(c2);
        h.a(dict.optInt("user_count"));
        h.a(dict.optString("username"));
        h.b(dict.optInt("result"));
        h.b(dict.optString("avatar"));
        return h;
    }

    public void a(int recevierCount) {
        this.a = recevierCount;
    }

    public void a(r challengeDefinition) {
        this.e = challengeDefinition;
    }

    public void a(u challenge) {
        this.f = challenge;
    }

    public void a(String toUsername) {
        this.b = toUsername;
    }

    public void b(int result) {
        this.d = result;
    }

    public void b(String avatarUrl) {
        this.c = avatarUrl;
    }
}
