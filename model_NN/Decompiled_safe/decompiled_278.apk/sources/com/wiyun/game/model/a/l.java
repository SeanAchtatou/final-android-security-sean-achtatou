package com.wiyun.game.model.a;

import android.text.TextUtils;
import org.json.JSONObject;

public class l {
    private String a;
    private int b;
    private String c;
    private String d;
    private float e;
    private long f;
    private String g;

    public static l a(JSONObject dict) {
        String id = dict.optString("id");
        if (TextUtils.isEmpty(id)) {
            return null;
        }
        l i = new l();
        i.a(id);
        i.a(dict.optInt("status"));
        i.b(dict.optString("detail"));
        i.c(dict.optString("name"));
        i.a((float) dict.optDouble("amount"));
        i.a(dict.optLong("create_time"));
        i.d(dict.optString("comment"));
        return i;
    }

    public void a(float amount) {
        this.e = amount;
    }

    public void a(int mStatus) {
        this.b = mStatus;
    }

    public void a(long mRechargeTime) {
        this.f = mRechargeTime;
    }

    public void a(String mId) {
        this.a = mId;
    }

    public void b(String detail) {
        this.c = detail;
    }

    public void c(String name) {
        this.d = name;
    }

    public void d(String comment) {
        this.g = comment;
    }
}
