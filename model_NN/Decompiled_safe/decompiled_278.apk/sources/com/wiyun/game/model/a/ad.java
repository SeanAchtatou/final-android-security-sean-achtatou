package com.wiyun.game.model.a;

import org.json.JSONObject;

public class ad {
    private long a;
    private long b;
    private int c;
    private String d;
    private String e;
    private String f;
    private String g;
    private String h;

    public static final ad a(JSONObject dict) {
        ad s = new ad();
        s.a(dict.optLong("featured_game_update_time"));
        s.b(dict.optLong("recommended_game_update_time"));
        s.a(dict.optInt("friend_game_count"));
        s.a(dict.optString("featured_game_icon"));
        s.d(dict.optString("recommended_game_icon"));
        s.c(dict.optString("friend_game_icon"));
        s.b(dict.optString("popular_game_icon"));
        s.e(dict.optString("developer_app_icon"));
        return s;
    }

    public void a(int friendGameCount) {
        this.c = friendGameCount;
    }

    public void a(long featuredGameUpdateTime) {
        this.a = featuredGameUpdateTime;
    }

    public void a(String featuredGameIconUrl) {
        this.d = featuredGameIconUrl;
    }

    public void b(long recommendedGameUpdateTime) {
        this.b = recommendedGameUpdateTime;
    }

    public void b(String popularGameIconUrl) {
        this.f = popularGameIconUrl;
    }

    public void c(String frienddGameIconUrl) {
        this.g = frienddGameIconUrl;
    }

    public void d(String recommendedGameIconUrl) {
        this.e = recommendedGameIconUrl;
    }

    public void e(String developerAppIconUrl) {
        this.h = developerAppIconUrl;
    }
}
