package com.wiyun.game.model.a;

import android.text.TextUtils;
import org.json.JSONObject;

public class m {
    private String a;
    private String b;
    private String c;
    private long d;
    private String e;
    private String f;
    private String g;
    private boolean h;

    public static m a(JSONObject dict) {
        m reply = new m();
        String id = dict.optString("id");
        if (TextUtils.isEmpty(id)) {
            return null;
        }
        reply.d(id);
        reply.f(dict.optString("title"));
        reply.a(dict.optString("content"));
        reply.a(dict.optLong("create_time"));
        reply.b(dict.optString("username"));
        reply.c(dict.optString("user_id"));
        reply.e(dict.optString("avatar"));
        reply.a("F".equalsIgnoreCase(dict.optString("gender")));
        return reply;
    }

    public void a(long createTime) {
        this.d = createTime;
    }

    public void a(String content) {
        this.c = content;
    }

    public void a(boolean female) {
        this.h = female;
    }

    public void b(String username) {
        this.e = username;
    }

    public void c(String id) {
        this.f = id;
    }

    public void d(String id) {
        this.a = id;
    }

    public void e(String avatarUrl) {
        this.g = avatarUrl;
    }

    public void f(String title) {
        this.b = title;
    }
}
