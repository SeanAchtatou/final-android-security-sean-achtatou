package com.wiyun.game.model.a;

import android.text.TextUtils;
import org.json.JSONObject;

public class ah {
    private String a;
    private String b;
    private String c;
    private String d;
    private int e;
    private String f;
    private String g;
    private String h;
    private String[] i;
    private String j;
    private String k;
    private String l;
    private String m;

    public static ah a(JSONObject dict) {
        ah r = new ah();
        String sessionKey = dict.optString("session_key");
        if (TextUtils.isEmpty(sessionKey)) {
            return null;
        }
        r.b(dict.optString("user_id"));
        r.c(dict.optString("username"));
        r.a(dict.optInt("point"));
        r.d(dict.optString("avatar"));
        r.f(dict.optString("last_app_id"));
        r.e(dict.optString("last_app_name"));
        r.a(sessionKey);
        r.h(dict.optString("bind_email"));
        r.g(dict.optString("bind_mobile"));
        r.i(dict.optString("items_sig"));
        r.j(dict.optString("platform"));
        r.k(dict.optString("brand", "unknown"));
        String platforms = dict.optString("last_app_platform", "android");
        if (platforms.indexOf(44) == -1) {
            r.a(new String[]{platforms});
        } else {
            r.a(platforms.split(","));
        }
        return r;
    }

    public String a() {
        return this.j;
    }

    public void a(int point) {
        this.e = point;
    }

    public void a(String sessionKey) {
        this.j = sessionKey;
    }

    public void a(String[] lastAppPlatforms) {
        this.i = lastAppPlatforms;
    }

    public String b() {
        return this.a;
    }

    public void b(String id) {
        this.a = id;
    }

    public String c() {
        return this.b;
    }

    public void c(String name) {
        this.b = name;
    }

    public int d() {
        return this.e;
    }

    public void d(String avatarUrl) {
        this.f = avatarUrl;
    }

    public String e() {
        return this.f;
    }

    public void e(String lastAppName) {
        this.g = lastAppName;
    }

    public String f() {
        return this.g;
    }

    public void f(String lastAppId) {
        this.h = lastAppId;
    }

    public String g() {
        return this.h;
    }

    public void g(String boundMobile) {
        this.k = boundMobile;
    }

    public String h() {
        return this.k;
    }

    public void h(String boundEmail) {
        this.l = boundEmail;
    }

    public String i() {
        return this.l;
    }

    public void i(String itemSig) {
        this.m = itemSig;
    }

    public String j() {
        return this.m;
    }

    public void j(String platform) {
        this.c = platform;
    }

    public String k() {
        return this.c;
    }

    public void k(String brand) {
        this.d = brand;
    }

    public String l() {
        return this.d;
    }
}
