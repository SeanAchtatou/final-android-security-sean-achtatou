package com.wiyun.game.model.a;

import android.text.TextUtils;
import org.json.JSONObject;

public class x {
    private String a;
    private String b;
    private long c;
    private String d;

    public static x a(JSONObject dict) {
        x m = new x();
        String id = dict.optString("id");
        String userId = dict.optString("user_id");
        if (TextUtils.isEmpty(id) || TextUtils.isEmpty(userId)) {
            return null;
        }
        m.a(id);
        m.c(userId);
        m.b(dict.optString("body"));
        m.a(dict.optLong("sent_time"));
        return m;
    }

    public void a(long sentTime) {
        this.c = sentTime;
    }

    public void a(String id) {
        this.a = id;
    }

    public void b(String content) {
        this.b = content;
    }

    public void c(String userId) {
        this.d = userId;
    }
}
