package com.wiyun.game.model.a;

import org.json.JSONObject;

public class ac {
    private String a;
    private String b;
    private String c;

    public static ac a(JSONObject dict) {
        ac c2 = new ac();
        c2.a(dict.optString("type"));
        c2.b(dict.optString("name"));
        c2.c(dict.optString("auth_method"));
        return c2;
    }

    public String a() {
        return this.a;
    }

    public void a(String mType) {
        this.a = mType;
    }

    public void b(String mName) {
        this.b = mName;
    }

    public void c(String mAuthMethod) {
        this.c = mAuthMethod;
    }
}
