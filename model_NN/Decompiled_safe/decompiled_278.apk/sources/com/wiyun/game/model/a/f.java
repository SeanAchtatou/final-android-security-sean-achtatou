package com.wiyun.game.model.a;

import android.text.TextUtils;
import org.json.JSONObject;

public class f {
    private String a;
    private String b;
    private String c;
    private int d;
    private String e;
    private boolean f;
    private long g;
    private boolean h;

    public static f a(JSONObject dict) {
        f a2 = new f();
        String id = dict.optString("id");
        if (TextUtils.isEmpty(id)) {
            return null;
        }
        a2.a(id);
        a2.b(dict.optString("name"));
        a2.c(dict.optString("description"));
        a2.a(dict.optInt("point"));
        a2.d(dict.optString("icon"));
        a2.a(dict.optBoolean("completed"));
        a2.b(dict.optBoolean("secret"));
        a2.a(dict.optLong("complete_time"));
        return a2;
    }

    public String a() {
        return this.a;
    }

    public void a(int point) {
        this.d = point;
    }

    public void a(long unlockTime) {
        this.g = unlockTime;
    }

    public void a(String id) {
        this.a = id;
    }

    public void a(boolean complete) {
        this.f = complete;
    }

    public String b() {
        return this.b;
    }

    public void b(String name) {
        this.b = name;
    }

    public void b(boolean secret) {
        this.h = secret;
    }

    public String c() {
        return this.c;
    }

    public void c(String description) {
        this.c = description;
    }

    public int d() {
        return this.d;
    }

    public void d(String iconUrl) {
        this.e = iconUrl;
    }

    public String e() {
        return this.e;
    }

    public boolean f() {
        return this.f;
    }

    public boolean g() {
        return this.h;
    }

    public long h() {
        return this.g;
    }
}
