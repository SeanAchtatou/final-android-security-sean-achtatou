package com.wiyun.game.model.a;

import android.text.TextUtils;
import java.io.Serializable;
import org.json.JSONObject;

public class n implements Serializable {
    private String a;
    private String b;
    private String c;

    public static final n a(JSONObject dict) {
        n s = new n();
        String smallUrl = dict.optString("small");
        String bigUrl = dict.optString("big");
        if (TextUtils.isEmpty(smallUrl) || TextUtils.isEmpty(bigUrl)) {
            return null;
        }
        s.a(smallUrl);
        s.b(bigUrl);
        s.c(dict.optString("name"));
        return s;
    }

    public String a() {
        return this.b;
    }

    public void a(String smallImageUrl) {
        this.a = smallImageUrl;
    }

    public void b(String bigImageUrl) {
        this.b = bigImageUrl;
    }

    public void c(String name) {
        this.c = name;
    }
}
