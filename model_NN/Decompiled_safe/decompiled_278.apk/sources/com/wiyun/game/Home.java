package com.wiyun.game;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.webkit.CookieManager;
import android.webkit.DownloadListener;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.SimpleCursorAdapter;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.Toast;
import com.saubcy.games.maze.market.MazeGame;
import com.saubcy.games.maze.market.R;
import com.wiyun.game.a.c;
import com.wiyun.game.model.a.ag;
import com.wiyun.game.model.a.n;
import com.wiyun.game.model.a.u;
import com.wiyun.game.model.a.v;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Home extends Activity implements TextWatcher, View.OnClickListener, View.OnTouchListener, DownloadListener, CompoundButton.OnCheckedChangeListener, SimpleCursorAdapter.ViewBinder, c.a, com.wiyun.game.b.a, com.wiyun.game.b.b {
    /* access modifiers changed from: private */
    public static d V;
    static boolean a;
    private Button A;
    private Button B;
    private Button C;
    private Button D;
    /* access modifiers changed from: private */
    public EditText E;
    private ImageButton F;
    /* access modifiers changed from: private */
    public Button G;
    private TextView H;
    private TextView I;
    private TextView J;
    private ImageView K;
    private View L;
    /* access modifiers changed from: private */
    public ListView[] M;
    /* access modifiers changed from: private */
    public int N;
    private String[] O;
    private boolean P;
    private int Q;
    /* access modifiers changed from: private */
    public int R;
    /* access modifiers changed from: private */
    public int S;
    /* access modifiers changed from: private */
    public int T;
    /* access modifiers changed from: private */
    public int U;
    private boolean W;
    /* access modifiers changed from: private */
    public Map<String, String> X;
    private boolean Y;
    /* access modifiers changed from: private */
    public Map<String, Bitmap> Z;
    /* access modifiers changed from: private */
    public List<com.wiyun.game.model.a.f> aa;
    /* access modifiers changed from: private */
    public com.wiyun.game.a.h ab;
    /* access modifiers changed from: private */
    public Map<String, String> ac;
    /* access modifiers changed from: private */
    public byte[] ad;
    /* access modifiers changed from: private */
    public Geocoder ae;
    /* access modifiers changed from: private */
    public g af;
    private boolean ag;
    private BroadcastReceiver ah = new u(this);
    private int b;
    /* access modifiers changed from: private */
    public WebView c;
    /* access modifiers changed from: private */
    public ViewGroup d;
    /* access modifiers changed from: private */
    public View e;
    private View f;
    /* access modifiers changed from: private */
    public View g;
    /* access modifiers changed from: private */
    public View h;
    private View i;
    private TableLayout j;
    private ScrollView k;
    private ScrollView l;
    private LinearLayout m;
    /* access modifiers changed from: private */
    public TextView n;
    private Button[] o;
    /* access modifiers changed from: private */
    public TextSwitcher p;
    private ImageView q;
    private TextView r;
    private TextView s;
    private TextView t;
    private TextView u;
    private TextView v;
    private TextView w;
    private TextView x;
    private CheckBox y;
    private Button z;

    private final class a extends BaseAdapter {
        private a() {
        }

        /* synthetic */ a(Home home, a aVar) {
            this();
        }

        public int getCount() {
            return Home.this.aa.size();
        }

        public Object getItem(int position) {
            return Home.this.aa.get(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.wiyun.game.h.a(java.util.Map<java.lang.String, android.graphics.Bitmap>, boolean, java.lang.String, java.lang.String):android.graphics.Bitmap
         arg types: [java.util.Map, int, java.lang.String, java.lang.String]
         candidates:
          com.wiyun.game.h.a(double, double, double, double):double
          com.wiyun.game.h.a(android.content.Context, java.lang.String, boolean, int):void
          com.wiyun.game.h.a(java.util.Map<java.lang.String, android.graphics.Bitmap>, boolean, java.lang.String, java.lang.String):android.graphics.Bitmap */
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(Home.this).inflate(t.e("wy_list_item_achievement"), (ViewGroup) null);
                e vh = new e(Home.this, null);
                vh.a = (TextView) convertView.findViewById(t.d("wy_tv_time"));
                vh.b = (TextView) convertView.findViewById(t.d("wy_tv_name"));
                vh.c = (TextView) convertView.findViewById(t.d("wy_tv_desc"));
                vh.d = (TextView) convertView.findViewById(t.d("wy_tv_honor"));
                vh.e = (ImageView) convertView.findViewById(t.d("wy_iv_icon"));
                convertView.setTag(vh);
            }
            e vh2 = (e) convertView.getTag();
            com.wiyun.game.model.a.f a2 = (com.wiyun.game.model.a.f) getItem(position);
            if (a2.f()) {
                vh2.a.setVisibility(0);
                vh2.a.setText(String.format(t.h("wy_label_achieve_at_x"), Home.this.ab.a(Home.this, a2.h())));
            } else {
                vh2.a.setVisibility(8);
            }
            vh2.b.setText(a2.b());
            if (!a2.g() || a2.f()) {
                vh2.c.setText(a2.c());
            } else {
                vh2.c.setText(t.f("wy_label_secret_achievement_desc"));
            }
            vh2.d.setText(String.valueOf(a2.d()));
            if (a2.f()) {
                vh2.e.setImageBitmap(h.a((Map<String, Bitmap>) Home.this.Z, false, "ach_", a2.e()));
            } else {
                vh2.e.setImageResource(t.c("wy_icon_locked"));
            }
            return convertView;
        }
    }

    private final class b extends Thread {
        private b() {
        }

        /* synthetic */ b(Home home, b bVar) {
            this();
        }

        public void run() {
            Map map = new HashMap();
            for (com.wiyun.game.model.a.f a2 : Home.this.aa) {
                map.put(a2.a(), a2);
            }
            Cursor c = null;
            try {
                c = i.e();
                if (c.moveToFirst()) {
                    while (!c.isAfterLast()) {
                        com.wiyun.game.model.a.f achievement = (com.wiyun.game.model.a.f) map.get(c.getString(1));
                        if (achievement != null) {
                            achievement.a(true);
                            achievement.a(c.getLong(3));
                        }
                        c.moveToNext();
                    }
                }
                if (c != null) {
                    c.close();
                }
            } catch (Exception e) {
                if (c != null) {
                    c.close();
                }
            } catch (Throwable th) {
                if (c != null) {
                    c.close();
                }
                throw th;
            }
            Home.this.runOnUiThread(new Runnable() {
                public void run() {
                    Home.this.M[Home.this.M.length - 1].setAdapter((ListAdapter) new a(Home.this, null));
                    Home.this.a(Home.this.M[Home.this.M.length - 1]);
                    Home a2 = Home.this;
                    a2.N = a2.N - 1;
                    if (Home.this.N <= 0) {
                        Home.this.e.setVisibility(4);
                    }
                }
            });
        }
    }

    private final class c extends WebViewClient {
        private c() {
        }

        /* synthetic */ c(Home home, c cVar) {
            this();
        }

        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            if (Home.this.X.isEmpty()) {
                Home.this.e.setVisibility(4);
            }
        }

        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            if (Home.this.d.getVisibility() == 0) {
                Home.this.e.setVisibility(0);
            }
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Home.this.e();
            return super.shouldOverrideUrlLoading(view, url);
        }
    }

    private final class d extends Thread {
        private double b;
        private double c;
        private String d;

        d(double lat, double lon, String tag) {
            this.b = lat;
            this.c = lon;
            this.d = tag;
        }

        public void run() {
            try {
                List addresses = Home.this.ae.getFromLocation(this.b, this.c, 1);
                if (addresses != null && !addresses.isEmpty()) {
                    Address addr = addresses.get(0);
                    StringBuffer buf = new StringBuffer();
                    String part = addr.getAddressLine(0);
                    if (part != null) {
                        buf.append(part);
                    }
                    String part2 = addr.getAddressLine(1);
                    if (part2 != null) {
                        buf.append(part2);
                    }
                    StringBuffer buffer = new StringBuffer();
                    buffer.append("javascript:wigame_response(").append('\'').append(this.d).append("',200,").append('\'').append(buf.toString()).append("')");
                    Home.this.c.loadUrl(buffer.toString());
                }
            } catch (Exception e) {
            }
        }
    }

    private final class e {
        TextView a;
        TextView b;
        TextView c;
        TextView d;
        ImageView e;

        private e() {
        }

        /* synthetic */ e(Home home, e eVar) {
            this();
        }
    }

    private final class f extends Thread {
        /* access modifiers changed from: private */
        public int b;
        private String c;

        public f(int listViewIndex, String leaderboardId) {
            this.b = listViewIndex;
            this.c = leaderboardId;
        }

        public void run() {
            i.a(Home.this.getFilesDir());
            com.wiyun.game.model.a.c lb = x.b(this.c);
            final Cursor c2 = i.a(this.c, "A", lb == null ? false : lb.d());
            i.a();
            Home.this.runOnUiThread(new Runnable() {
                public void run() {
                    if (c2.getCount() > 0) {
                        SimpleCursorAdapter sca = new SimpleCursorAdapter(Home.this, t.e("wy_list_item_score"), c2, new String[]{"_id"}, new int[]{t.d("wy_ll_score_item")});
                        sca.setViewBinder(Home.this);
                        Home.this.M[f.this.b].setVisibility(0);
                        Home.this.M[f.this.b].setAdapter((ListAdapter) sca);
                        ((TextView) Home.this.M[f.this.b].getTag()).setVisibility(8);
                        Home.this.a(Home.this.M[f.this.b]);
                    } else {
                        ((TextView) Home.this.M[f.this.b].getTag()).setVisibility(0);
                        Home.this.M[f.this.b].setVisibility(8);
                    }
                    Home b2 = Home.this;
                    b2.N = b2.N - 1;
                    if (Home.this.N <= 0) {
                        Home.this.e.setVisibility(4);
                    }
                }
            });
        }
    }

    private final class g {
        String a;
        int b;
        boolean c;
        boolean d;
        String e;
        String f;
        String[] g;
        String h;
        long i;

        private g() {
        }

        /* synthetic */ g(Home home, g gVar) {
            this();
        }
    }

    private final class h extends WebChromeClient {
        private h() {
        }

        /* synthetic */ h(Home home, h hVar) {
            this();
        }

        public void onReceivedTitle(WebView view, String title) {
            super.onReceivedTitle(view, title);
            Home.this.n.setText(title);
        }
    }

    private final class i implements Runnable {
        private String b;
        private boolean c;

        public i(String msg, boolean fail) {
            this.b = msg;
            this.c = fail;
        }

        public void run() {
            h.a(Home.this, this.b, this.c);
        }
    }

    /* access modifiers changed from: private */
    public void a(Intent intent) {
        switch (intent.getIntExtra("pending_action", 0)) {
            case 2:
                a("leaderboard?leaderboard_id=" + intent.getStringExtra("lb_id"));
                return;
            case 3:
                a("leaderboards");
                return;
            case 4:
            case 5:
            case 6:
            case 7:
            case R.styleable.com_wiyun_ad_AdView_testAdType:
            case R.styleable.com_wiyun_ad_AdView_transition:
            default:
                return;
            case MazeGame.WIDTHREDUCTION:
                a("topics");
                return;
            case 11:
                a("achievements");
                return;
            case 12:
                a("moregame");
                return;
            case 13:
                a("challenge");
                return;
            case 14:
                c(2);
                return;
            case MazeGame.MAXBLOCKSIZE:
                a("myfriends");
                return;
            case 16:
                a("threads");
                return;
            case 17:
                a("notices");
                return;
            case 18:
                a("messages?user_id=" + intent.getStringExtra("lb_id"));
                return;
            case 19:
                a("shares");
                return;
            case 20:
                a("news");
                return;
        }
    }

    private void a(String str) {
        String str2 = "http://" + f.d + ":" + f.a + (f.d.endsWith(".wiyun.com") ? "/m/" : "/wigame/");
        e();
        this.c.loadUrl(String.valueOf(str2) + str);
    }

    /* access modifiers changed from: private */
    public void a(final String tag, final int statusCode, final String data) {
        runOnUiThread(new Runnable() {
            public void run() {
                StringBuffer buf = new StringBuffer("javascript:wigame_response('");
                buf.append(tag).append("',").append(statusCode).append(",'").append(data).append("')");
                Home.this.c.loadUrl(buf.toString());
            }
        });
    }

    private Map<String, String> b(String str) {
        this.ac.clear();
        for (String split : str.split("&")) {
            String[] split2 = split.split("=");
            if (split2.length == 2) {
                this.ac.put(split2[0], split2[1]);
            }
        }
        return this.ac;
    }

    private void b() {
        a = true;
        this.b = 0;
        this.X = new HashMap();
        this.Z = new HashMap();
        this.ac = new HashMap();
        this.ab = new com.wiyun.game.a.h(false);
        this.aa = new ArrayList();
        this.ae = new Geocoder(this);
        this.O = getResources().getStringArray(t.b("wy_smily_array"));
        if (x.b != null) {
            this.aa.addAll(x.b);
        }
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.wiyun.game.RESET");
        intentFilter.addAction("com.wiyun.game.EXIT");
        intentFilter.addAction("com.wiyun.game.CACHE_CLEARED");
        intentFilter.addAction("com.wiyun.game.RELOAD");
        intentFilter.addAction("com.wiyun.game.BLOB_DOWNLOADED");
        intentFilter.addAction("com.wiyun.game.IMAGE_DOWNLOADED");
        registerReceiver(this.ah, intentFilter);
        com.wiyun.game.b.d.a().a((com.wiyun.game.b.a) this);
        com.wiyun.game.b.d.a().a((com.wiyun.game.b.b) this);
        CookieManager instance = CookieManager.getInstance();
        instance.setCookie("http://wiyun.com", "wigame_config_apiserver=/wiapi;domain=wiyun.com");
        instance.setCookie("http://wiyun.com", "wigame_config_platform=android;domain=wiyun.com");
        instance.setCookie("http://wiyun.com", "wigame_config_system=android 1.5;domain=wiyun.com");
        instance.setCookie("http://wiyun.com", "wigame_config_shell=wigame_android;domain=wiyun.com");
        instance.setCookie("http://wiyun.com", String.format("wigame_config_channel=%s;domain=wiyun.com", WiGame.getChannel()));
        Object[] objArr = new Object[1];
        objArr[0] = Integer.valueOf(WiGame.H() >= 240 ? 2 : 1);
        instance.setCookie("http://wiyun.com", String.format("wigame_config_hidpi=%d;domain=wiyun.com", objArr));
        instance.setCookie("http://wiyun.com", String.format("wigame_config_dpi=%d;domain=wiyun.com", Integer.valueOf(WiGame.H())));
        instance.setCookie("http://wiyun.com", String.format("wigame_config_location=%f,%f;domain=%s", Double.valueOf(WiGame.getLatitude()), Double.valueOf(WiGame.getLongitude()), "wiyun.com"));
    }

    private void c() {
        this.c = (WebView) findViewById(t.d("wy_wv_web"));
        this.d = (ViewGroup) findViewById(t.d("wy_fl_main_panel"));
        this.e = findViewById(t.d("wy_ll_progress_panel"));
        this.f = findViewById(t.d("wy_fl_popup_panel"));
        this.g = findViewById(t.d("wy_ll_input_panel"));
        this.i = findViewById(t.d("wy_ll_switch_account_panel"));
        this.j = (TableLayout) findViewById(t.d("wy_tl_smily_panel"));
        this.L = findViewById(t.d("wy_ll_empty_hint_panel"));
        this.E = (EditText) findViewById(t.d("wy_et_reply"));
        this.G = (Button) findViewById(t.d("wy_b_send"));
        this.h = findViewById(t.d("wy_ll_footer"));
        this.k = (ScrollView) findViewById(t.d("wy_sv_local_scroller"));
        this.l = (ScrollView) findViewById(t.d("wy_sv_settings_scroller"));
        this.m = (LinearLayout) findViewById(t.d("wy_ll_local_panel"));
        this.n = (TextView) findViewById(t.d("wy_tv_title"));
        this.q = (ImageView) findViewById(t.d("wy_iv_popup_portrait"));
        this.r = (TextView) findViewById(t.d("wy_tv_popup_name"));
        this.s = (TextView) findViewById(t.d("wy_tv_popup_coins"));
        this.t = (TextView) findViewById(t.d("wy_tv_popup_honor"));
        this.u = (TextView) findViewById(t.d("wy_tv_challenge_badge"));
        this.v = (TextView) findViewById(t.d("wy_tv_message_badge"));
        this.w = (TextView) findViewById(t.d("wy_tv_notice_badge"));
        this.x = (TextView) findViewById(t.d("wy_tv_friends_badge"));
        this.o = new Button[4];
        this.o[0] = (Button) findViewById(t.d("wy_b_activity"));
        this.o[1] = (Button) findViewById(t.d("wy_b_game"));
        this.o[2] = (Button) findViewById(t.d("wy_b_profile"));
        this.o[3] = (Button) findViewById(t.d("wy_b_wibox"));
        this.y = (CheckBox) findViewById(t.d("wy_cb_share_location"));
        this.H = (TextView) this.l.findViewById(t.d("wy_tv_settings_name"));
        this.J = (TextView) this.l.findViewById(t.d("wy_tv_settings_honor"));
        this.I = (TextView) this.l.findViewById(t.d("wy_tv_settings_playing"));
        this.K = (ImageView) this.l.findViewById(t.d("wy_iv_settings_portrait"));
        this.c.setScrollBarStyle(33554432);
        this.c.setMapTrackballToArrowKeys(false);
        this.c.getSettings().setJavaScriptEnabled(true);
        this.c.getSettings().setBuiltInZoomControls(false);
        this.c.setWebViewClient(new c(this, null));
        this.c.setWebChromeClient(new h(this, null));
        this.c.addJavascriptInterface(this, "wigame");
        this.c.setDownloadListener(this);
        this.c.setOnTouchListener(this);
        this.p = (TextSwitcher) findViewById(t.d("wy_text_switcher"));
        V = new d(this.p);
        this.p.setInAnimation(this, t.a("wy_push_up_in"));
        this.p.setOutAnimation(this, t.a("wy_push_up_out"));
        this.p.setCurrentText(String.format(t.h("wy_label_x_welcome_to_wiyun"), WiGame.getMyName()));
        this.e.setVisibility(0);
        this.i.setVisibility(WiGame.isSwitchAccountForbidden() ? 8 : 0);
        this.b = 0;
        for (Button selected : this.o) {
            selected.setSelected(false);
        }
        this.o[this.b].setSelected(true);
        ((Button) findViewById(t.d("wy_b_activity"))).setOnClickListener(this);
        ((Button) findViewById(t.d("wy_b_game"))).setOnClickListener(this);
        ((Button) findViewById(t.d("wy_b_profile"))).setOnClickListener(this);
        ((Button) findViewById(t.d("wy_b_wibox"))).setOnClickListener(this);
        ((Button) findViewById(t.d("wy_b_back"))).setOnClickListener(this);
        ((Button) findViewById(t.d("wy_b_close"))).setOnClickListener(this);
        this.F = (ImageButton) findViewById(t.d("wy_ib_smily"));
        this.F.setOnClickListener(this);
        ((Button) findViewById(t.d("wy_b_send"))).setOnClickListener(this);
        ((Button) findViewById(t.d("wy_b_switch_account"))).setOnClickListener(this);
        ((Button) findViewById(t.d("wy_b_clear_cache"))).setOnClickListener(this);
        ((Button) findViewById(t.d("wy_b_logout_account"))).setOnClickListener(this);
        findViewById(t.d("wy_ll_header_center")).setOnClickListener(this);
        findViewById(t.d("wy_fl_popup_panel")).setOnClickListener(this);
        this.q.setOnClickListener(this);
        this.z = (Button) findViewById(t.d("wy_b_challenge"));
        this.z.setOnClickListener(this);
        this.A = (Button) findViewById(t.d("wy_b_message"));
        this.A.setOnClickListener(this);
        this.B = (Button) findViewById(t.d("wy_b_notice"));
        this.B.setOnClickListener(this);
        ((Button) findViewById(t.d("wy_b_local_score"))).setOnClickListener(this);
        this.D = (Button) findViewById(t.d("wy_b_settings"));
        this.D.setOnClickListener(this);
        this.C = (Button) findViewById(t.d("wy_b_friends"));
        this.C.setOnClickListener(this);
        this.y.setOnCheckedChangeListener(this);
        d();
        this.E.addTextChangedListener(this);
        this.E.setOnTouchListener(this);
    }

    /* access modifiers changed from: private */
    public void c(int i2) {
        if (this.b == i2) {
            com.wiyun.game.a.f.b();
        }
        this.b = i2;
        for (Button selected : this.o) {
            selected.setSelected(false);
        }
        this.o[i2].setSelected(true);
        this.e.setVisibility(0);
        switch (this.b) {
            case 0:
                a("news");
                return;
            case 1:
                a("dashboard");
                return;
            case 2:
                a("me");
                f.d(WiGame.getMyId());
                return;
            case 3:
                a("wibox");
                return;
            default:
                return;
        }
    }

    private void d() {
        int i2;
        for (int i3 = 0; i3 < this.O.length; i3 = i2) {
            TableRow tableRow = new TableRow(this);
            this.j.addView(tableRow);
            i2 = i3;
            int i4 = 0;
            while (i4 < 5 && i2 < this.O.length) {
                ImageButton imageButton = new ImageButton(this);
                imageButton.setBackgroundResource(t.c("wy_smily_bg"));
                imageButton.setImageResource(t.c(String.format("wy_smily_%d", Integer.valueOf(i2 + 1))));
                imageButton.setId(i2 + 10000);
                imageButton.setOnClickListener(this);
                tableRow.addView(imageButton);
                if (i4 < 4) {
                    ImageView imageView = new ImageView(this);
                    imageView.setImageResource(t.c("wy_separator_v_style1"));
                    imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                    tableRow.addView(imageView, new TableRow.LayoutParams(2, -1));
                }
                i4++;
                i2++;
            }
            if (i2 < this.O.length) {
                TableRow tableRow2 = new TableRow(this);
                this.j.addView(tableRow2);
                ImageView imageView2 = new ImageView(this);
                imageView2.setImageResource(t.c("wy_separator_h_style1"));
                imageView2.setScaleType(ImageView.ScaleType.FIT_XY);
                TableRow.LayoutParams layoutParams = new TableRow.LayoutParams(-1, 2);
                layoutParams.span = 9;
                tableRow2.addView(imageView2, layoutParams);
            }
        }
    }

    /* access modifiers changed from: private */
    public void e() {
        this.X.clear();
        this.e.setVisibility(4);
        this.f.setVisibility(4);
    }

    private void f() {
        for (Bitmap bitmap : this.Z.values()) {
            if (bitmap != null && !bitmap.isRecycled()) {
                bitmap.recycle();
            }
        }
        this.Z.clear();
    }

    private void g() {
        if (this.M != null) {
            for (ListView lv : this.M) {
                ListAdapter adapter = lv.getAdapter();
                if (adapter instanceof CursorAdapter) {
                    ((CursorAdapter) adapter).changeCursor(null);
                }
                lv.setAdapter((ListAdapter) null);
            }
        }
    }

    private void h() {
        if (this.k.getVisibility() == 0) {
            this.k.setVisibility(4);
            this.l.setVisibility(4);
            this.d.setVisibility(0);
            this.n.setText((CharSequence) this.n.getTag());
            this.n.setTag(null);
            g();
        }
    }

    /* access modifiers changed from: private */
    public void i() {
        this.h.setVisibility(0);
        this.g.setVisibility(4);
        this.g.setTag(null);
    }

    private void j() {
        if (this.l.getVisibility() == 0) {
            this.k.setVisibility(4);
            this.l.setVisibility(4);
            this.d.setVisibility(0);
            this.n.setText((CharSequence) this.n.getTag());
            this.n.setTag(null);
        }
    }

    private void k() {
        if (this.f.getVisibility() == 0) {
            this.f.setVisibility(4);
        } else if (this.g.getTag() != null) {
            i();
        } else if (this.k.getVisibility() == 0) {
            if (!this.Y) {
                h();
            } else {
                finish();
            }
        } else if (this.l.getVisibility() == 0) {
            if (!this.Y) {
                j();
            } else {
                finish();
            }
        } else if (this.c.canGoBack()) {
            e();
            this.c.goBack();
        } else {
            finish();
        }
    }

    /* access modifiers changed from: private */
    public void l() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService("input_method");
        if (inputMethodManager != null) {
            inputMethodManager.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
        }
    }

    private void m() {
        this.f.setVisibility(0);
        o();
        if (this.R > 0) {
            this.u.setVisibility(0);
            this.u.setText(String.valueOf(this.R));
        } else {
            this.u.setVisibility(4);
        }
        if (this.S > 0) {
            this.v.setVisibility(0);
            this.v.setText(String.valueOf(this.S));
        } else {
            this.v.setVisibility(4);
        }
        if (this.T > 0) {
            this.w.setVisibility(0);
            this.w.setText(String.valueOf(this.T));
        } else {
            this.w.setVisibility(4);
        }
        if (this.U > 0) {
            this.x.setVisibility(0);
            this.x.setText(String.valueOf(this.U));
        } else {
            this.x.setVisibility(4);
        }
        boolean enabled = !this.Y && WiGame.isLoggedIn();
        this.z.setEnabled(enabled);
        this.A.setEnabled(enabled);
        this.B.setEnabled(enabled);
        this.D.setEnabled(enabled);
        this.C.setEnabled(enabled);
    }

    private void n() {
        this.f.setVisibility(4);
        if (this.l.getVisibility() != 0) {
            this.l.setTag(null);
            this.n.setTag(this.n.getText());
            this.n.setText(t.f("wy_button_settings"));
            this.e.setVisibility(4);
            this.d.setVisibility(4);
            this.k.setVisibility(4);
            this.l.setVisibility(0);
            p();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wiyun.game.h.a(java.util.Map<java.lang.String, android.graphics.Bitmap>, boolean, java.lang.String, java.lang.String, boolean):android.graphics.Bitmap
     arg types: [?[OBJECT, ARRAY], int, java.lang.String, java.lang.String, boolean]
     candidates:
      com.wiyun.game.h.a(int, int, int, int, android.net.Uri):android.content.Intent
      com.wiyun.game.h.a(android.content.Context, java.lang.String, int, android.view.View$OnClickListener, int[]):void
      com.wiyun.game.h.a(java.util.Map<java.lang.String, android.graphics.Bitmap>, boolean, java.lang.String, java.lang.String, boolean):android.graphics.Bitmap */
    /* access modifiers changed from: private */
    public void o() {
        ag t2 = WiGame.t();
        this.q.setImageBitmap(h.a((Map<String, Bitmap>) null, false, h.b("p_", t2.getId()), t2.getAvatarUrl(), t2.isFemale()));
        this.r.setText(WiGame.getMyName());
        this.s.setText(String.valueOf(t2.getCoins()));
        this.t.setText(String.valueOf(t2.getHonor()));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wiyun.game.h.a(java.util.Map<java.lang.String, android.graphics.Bitmap>, boolean, java.lang.String, java.lang.String, boolean):android.graphics.Bitmap
     arg types: [?[OBJECT, ARRAY], int, java.lang.String, java.lang.String, boolean]
     candidates:
      com.wiyun.game.h.a(int, int, int, int, android.net.Uri):android.content.Intent
      com.wiyun.game.h.a(android.content.Context, java.lang.String, int, android.view.View$OnClickListener, int[]):void
      com.wiyun.game.h.a(java.util.Map<java.lang.String, android.graphics.Bitmap>, boolean, java.lang.String, java.lang.String, boolean):android.graphics.Bitmap */
    /* access modifiers changed from: private */
    public void p() {
        ag t2 = WiGame.t();
        this.y.setChecked(t2.a());
        this.H.setText(WiGame.getMyName());
        this.J.setText(String.valueOf(t2.getHonor()));
        this.I.setText(String.format(t.h("wy_label_playing_x"), t2.getLastAppName()));
        this.K.setImageBitmap(h.a((Map<String, Bitmap>) null, false, h.b("p_", t2.getId()), t2.getAvatarUrl(), t2.isFemale()));
    }

    private void q() {
        this.f.setVisibility(4);
        if (this.k.getVisibility() != 0) {
            this.n.setTag(this.n.getText());
            this.n.setText(t.f("wy_button_local_scores"));
            this.e.setVisibility(0);
            this.d.setVisibility(4);
            this.k.setVisibility(0);
            this.l.setVisibility(4);
            r();
        }
    }

    private void r() {
        LayoutInflater from = LayoutInflater.from(this);
        boolean z2 = this.M == null;
        this.N = x.b() + (x.a() ? 1 : 0);
        if (this.N <= 0) {
            this.e.setVisibility(4);
            this.L.setVisibility(0);
            return;
        }
        this.L.setVisibility(8);
        if (z2) {
            this.M = new ListView[this.N];
        }
        if (x.a != null) {
            int i2 = 0;
            for (com.wiyun.game.model.a.c next : x.a) {
                if (z2) {
                    TextView textView = (TextView) from.inflate(t.e("wy_view_small_shadow_label"), (ViewGroup) null);
                    textView.setText(next.a());
                    this.m.addView(textView);
                    FrameLayout frameLayout = (FrameLayout) from.inflate(t.e("wy_view_listview_block"), (ViewGroup) null);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
                    layoutParams.bottomMargin = 10;
                    this.m.addView(frameLayout, layoutParams);
                    this.M[i2] = (ListView) frameLayout.findViewById(16908298);
                    this.M[i2].setTag((TextView) frameLayout.findViewById(t.d("wy_tv_empty_view")));
                }
                f fVar = new f(i2, next.b());
                fVar.setName("Cached Score Query Thread");
                fVar.setPriority(1);
                fVar.setDaemon(true);
                fVar.start();
                i2++;
            }
        }
        if (x.b != null) {
            if (z2) {
                TextView textView2 = (TextView) from.inflate(t.e("wy_view_small_shadow_label"), (ViewGroup) null);
                textView2.setText(t.f("wy_label_achievements"));
                this.m.addView(textView2);
                FrameLayout frameLayout2 = (FrameLayout) from.inflate(t.e("wy_view_listview_block"), (ViewGroup) null);
                this.m.addView(frameLayout2);
                this.M[this.M.length - 1] = (ListView) frameLayout2.findViewById(16908298);
            }
            b bVar = new b(this, null);
            bVar.setName("Cached Achievement Query Thread");
            bVar.setPriority(1);
            bVar.setDaemon(true);
            bVar.start();
        }
        if (this.N <= 0) {
            this.e.setVisibility(4);
        }
    }

    private void s() {
        this.c.stopLoading();
        this.c.reload();
    }

    private void t() {
        finish();
    }

    private void u() {
        a("myfriends");
    }

    private void v() {
        a("searchuser");
    }

    /* access modifiers changed from: private */
    public void w() {
        this.Y = true;
        this.p.setCurrentText(t.h("wy_label_offline_mode"));
        this.n.setText(t.h("wy_button_local_scores"));
        q();
    }

    /* access modifiers changed from: private */
    public void x() {
        if (this.g.getVisibility() != 0) {
            this.E.setText((CharSequence) null);
            this.g.setVisibility(0);
            this.j.setVisibility(this.af.d ? 8 : 0);
            this.F.setVisibility(this.af.d ? 8 : 0);
            Animation loadAnimation = AnimationUtils.loadAnimation(this, t.a("wy_push_up_in"));
            loadAnimation.setAnimationListener(new Animation.AnimationListener() {
                public void onAnimationEnd(Animation arg0) {
                    Home.this.h.setVisibility(4);
                    Home.this.g.setTag(true);
                }

                public void onAnimationRepeat(Animation arg0) {
                }

                public void onAnimationStart(Animation arg0) {
                }
            });
            this.g.startAnimation(loadAnimation);
        }
    }

    /* access modifiers changed from: private */
    public void y() {
        u uVar = new u();
        uVar.a(h.j(this.ac.get("challenge_id")));
        uVar.c(h.j(this.ac.get("user_name")));
        uVar.b(h.c(this.ac.get("score")));
        uVar.a(h.c(this.ac.get("bid_count")));
        uVar.e(h.j(this.ac.get("user_id")));
        uVar.d(h.j(this.ac.get("user_avatar")));
        v vVar = new v();
        vVar.a(h.j(this.ac.get("challenge_to_user_id")));
        vVar.c(h.c(this.ac.get("score")));
        WiGame.a(h.j(this.ac.get("challenge_definition_id")), vVar, uVar, this.ad);
        f.g(vVar.a());
    }

    public void a(int id) {
    }

    public void a(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter != null) {
            int totalHeight = 0;
            for (int i2 = 0; i2 < listAdapter.getCount(); i2++) {
                View listItem = listAdapter.getView(i2, null, null);
                listItem.measure(0, 0);
                totalHeight += listItem.getMeasuredHeight();
            }
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = (listView.getDividerHeight() * (listAdapter.getCount() - 1)) + totalHeight;
            listView.setLayoutParams(params);
        }
    }

    public void a(final com.wiyun.game.b.e e2) {
        switch (e2.a) {
            case 6:
                if (e2.c) {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            if (e2.b == 403) {
                                Intent i = new Intent(Home.this, SwitchAccount.class);
                                i.setFlags(67108864);
                                Home.this.startActivity(i);
                                Home.this.finish();
                                return;
                            }
                            Home.this.w();
                        }
                    });
                    return;
                } else {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            Home.this.o();
                            Home.this.p.setCurrentText(String.format(t.h("wy_label_x_welcome_to_wiyun"), WiGame.getMyName()));
                            Home.this.c(1);
                            Home.this.a(Home.this.getIntent());
                        }
                    });
                    return;
                }
            case 14:
                if (!e2.c && ((ag) e2.e).getId().equals(WiGame.getMyId())) {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            Home.this.p();
                        }
                    });
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void afterTextChanged(Editable s2) {
        if (this.P) {
            this.P = false;
            int index = s2.toString().lastIndexOf(91);
            if (index != -1) {
                s2.delete(index, this.Q);
            }
        }
    }

    public void ajax(String str, String str2, String str3, String str4) {
        ajax(str, str2, str3, str4, "false");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void ajax(String str, String str2, String str3, final String str4, String str5) {
        boolean z2;
        boolean z3;
        if ("/posttopic".equals(str)) {
            Intent intent = new Intent(this, ComposeTopic.class);
            b(str3);
            intent.putExtra("app_id", h.j(this.ac.get("app_id")));
            intent.putExtra("post_mode", 1);
            startActivity(intent);
        } else if ("/replytopic".equals(str)) {
            b(str3);
            Intent intent2 = new Intent(this, ComposeTopic.class);
            intent2.putExtra("app_id", h.j(this.ac.get("app_id")));
            intent2.putExtra("topic_id", h.j(this.ac.get("topic_id")));
            intent2.putExtra("prefill", h.j(this.ac.get("heading")));
            intent2.putExtra("title", h.j(this.ac.get("title")));
            intent2.putExtra("post_mode", 2);
            startActivity(intent2);
        } else if ("/postfeeds".equals(str)) {
            Intent intent3 = new Intent(this, ComposeTopic.class);
            intent3.putExtra("post_mode", true);
            intent3.putExtra("post_mode", 3);
            startActivity(intent3);
        } else if ("/inputmessage".equals(str)) {
            b(str3);
            if (this.af == null) {
                this.af = new g(this, null);
            }
            this.af.a = h.j(this.ac.remove("im_api"));
            this.af.b = h.c(h.j(this.ac.remove("im_limits")));
            this.af.e = h.j(this.ac.remove("im_fill"));
            this.af.c = "true".equals(h.j(this.ac.remove("im_alwayson")));
            this.af.d = "true".equals(h.j(this.ac.remove("im_noemotion")));
            this.af.h = h.j(this.ac.remove("im_btn_text"));
            this.af.f = str4;
            String j2 = h.j(this.ac.remove("im_field"));
            this.af.g = new String[((this.ac.size() * 2) + 2)];
            this.af.g[0] = j2;
            int i2 = 0 + 1 + 1;
            for (Map.Entry next : this.ac.entrySet()) {
                int i3 = i2 + 1;
                this.af.g[i2] = (String) next.getKey();
                i2 = i3 + 1;
                this.af.g[i3] = h.j((String) next.getValue());
            }
            runOnUiThread(new Runnable() {
                public void run() {
                    if (!TextUtils.isEmpty(Home.this.af.e)) {
                        Home.this.E.setHint(Home.this.af.e);
                    }
                    if (!TextUtils.isEmpty(Home.this.af.h)) {
                        Home.this.G.setText(Home.this.af.h);
                    } else {
                        Home.this.G.setText(t.f("wy_button_send"));
                    }
                    Home.this.x();
                }
            });
        } else if ("/reversegeocode".equals(str)) {
            b(str3);
            String[] split = h.j(this.ac.get("location")).split(",");
            new d((double) h.a(split[0]), (double) h.a(split[1]), str4).start();
        } else if ("/showlocation".equals(str)) {
            b(str3);
            String j3 = h.j(this.ac.get("location"));
            String[] split2 = j3.split(",");
            double a2 = (double) h.a(split2[0]);
            double a3 = (double) h.a(split2[1]);
            if (WiGame.G()) {
                Intent intent4 = new Intent(this, UserMap.class);
                intent4.putExtra("mode", 1);
                intent4.putExtra("latitude", a2);
                intent4.putExtra("longitude", a3);
                startActivity(intent4);
                return;
            }
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://maps.google.com/maps?q=" + j3)));
        } else if ("/user/location".equals(str)) {
            b(str3);
            String j4 = h.j(this.ac.get("user_id"));
            if (WiGame.G()) {
                Intent intent5 = new Intent(this, UserMap.class);
                intent5.putExtra("mode", 2);
                intent5.putExtra("user_id", j4);
                startActivity(intent5);
                return;
            }
            runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(Home.this, t.f("wy_toast_no_mapview"), 0).show();
                }
            });
        } else if ("/user/nearby".equals(str)) {
            if (WiGame.G()) {
                Intent intent6 = new Intent(this, UserMap.class);
                intent6.putExtra("mode", 3);
                startActivity(intent6);
                return;
            }
            runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(Home.this, t.f("wy_toast_no_mapview"), 0).show();
                }
            });
        } else if ("/score/nearby".equals(str)) {
            if (WiGame.G()) {
                b(str3);
                String j5 = h.j(this.ac.get("leaderboard_id"));
                String j6 = h.j(this.ac.get("timespan"));
                String str6 = this.ac.get("app_id");
                if (str6 != null) {
                    str6 = h.j(str6);
                }
                Intent intent7 = new Intent(this, UserMap.class);
                intent7.putExtra("mode", 4);
                intent7.putExtra("leaderboard_id", j5);
                intent7.putExtra("timespan", j6);
                intent7.putExtra("app_id", str6);
                startActivity(intent7);
                return;
            }
            runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(Home.this, t.f("wy_toast_no_mapview"), 0).show();
                }
            });
        } else if ("/imagegallary".equals(str)) {
            Intent intent8 = new Intent(this, FullImageGallery.class);
            b(str3);
            String str7 = this.ac.get("images");
            if (!TextUtils.isEmpty(str7)) {
                ArrayList arrayList = new ArrayList();
                for (String b2 : h.j(str7).split(";")) {
                    n nVar = new n();
                    nVar.b(b2);
                    arrayList.add(nVar);
                }
                intent8.putExtra("screenshots", arrayList);
            }
            intent8.putExtra("start", h.c(this.ac.get("index")));
            startActivity(intent8);
        } else if ("/showinfo".equals(str)) {
            b(str3);
            runOnUiThread(new i(h.j(this.ac.get("msg")), false));
        } else if ("/showerror".equals(str)) {
            b(str3);
            runOnUiThread(new i(h.j(this.ac.get("msg")), false));
        } else if ("/updatetitle".equals(str)) {
            b(str3);
            runOnUiThread(new Runnable() {
                public void run() {
                    Home.this.n.setText(h.j((String) Home.this.ac.get("title")));
                }
            });
        } else if ("/updatenotify".equals(str)) {
            b(str3);
            this.U = h.c(h.j(this.ac.get("pending_friend_count")));
            this.R = h.c(h.j(this.ac.get("pending_challenge_count")));
            this.S = h.c(h.j(this.ac.get("unread_message_count")));
            this.T = h.c(h.j(this.ac.get("notice_count")));
        } else if ("/refreshuser".equals(str)) {
            f.d(WiGame.getMyId());
        } else if ("/showdialog".equals(str)) {
            b(str3);
            runOnUiThread(new Runnable() {
                public void run() {
                    String positive = h.j((String) Home.this.ac.get("btn_ok"));
                    AlertDialog.Builder builder = new AlertDialog.Builder(Home.this);
                    builder.setTitle(h.j((String) Home.this.ac.get("title"))).setMessage(h.j((String) Home.this.ac.get("message"))).setNegativeButton(h.j((String) Home.this.ac.get("btn_cancel")), (DialogInterface.OnClickListener) null);
                    if (!TextUtils.isEmpty(positive)) {
                        final String str = str4;
                        builder.setPositiveButton(positive, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface arg0, int arg1) {
                                Home.this.a(str, 200, "");
                            }
                        });
                    }
                    builder.show();
                }
            });
        } else if ("/updateavatar".equals(str)) {
            startActivity(new Intent(this, ChangeMyPortrait.class));
        } else if ("/acceptchallenge".equals(str)) {
            b(str3);
            showDialog(1000);
        } else if ("/requireauth".equals(str)) {
            b(str3);
            Intent intent9 = new Intent(this, OAuth.class);
            intent9.putExtra("tag", str4);
            intent9.putExtra("url", h.j(this.ac.remove("ra_url")));
            intent9.putExtra("action", h.j(this.ac.remove("ra_action")));
            intent9.putExtra("params", h.a(this.ac));
            startActivityForResult(intent9, 1);
        } else if ("/hasapp".equals(str)) {
            b(str3);
            String[] split3 = h.j(this.ac.get("packages")).split(",");
            if (split3 != null) {
                StringBuffer stringBuffer = new StringBuffer("javascript:wigame_response('");
                stringBuffer.append(str4).append("',200,'");
                PackageManager packageManager = getPackageManager();
                for (String packageInfo : split3) {
                    try {
                        if (packageManager.getPackageInfo(packageInfo, 0) == null) {
                            stringBuffer.append("false,");
                        } else {
                            stringBuffer.append("true,");
                        }
                    } catch (PackageManager.NameNotFoundException e2) {
                        stringBuffer.append("false,");
                    }
                }
                stringBuffer.deleteCharAt(stringBuffer.length() - 1);
                stringBuffer.append("')");
                this.c.loadUrl(stringBuffer.toString());
            }
        } else if ("/startapp".equals(str)) {
            b(str3);
            String j7 = h.j(this.ac.get("uri"));
            String j8 = h.j(this.ac.get("type"));
            String j9 = h.j(this.ac.get("pn"));
            String j10 = h.j(this.ac.get("action"));
            String j11 = h.j(this.ac.get("category"));
            Intent intent10 = new Intent();
            if (!TextUtils.isEmpty(j10)) {
                try {
                    intent10.setAction((String) Intent.class.getField(j10).get(null));
                    z2 = false;
                } catch (Exception e3) {
                    z2 = true;
                }
            } else {
                z2 = false;
            }
            if (z2 || TextUtils.isEmpty(j11)) {
                z3 = z2;
            } else {
                try {
                    intent10.addCategory((String) Intent.class.getField(j11).get(null));
                    z3 = z2;
                } catch (Exception e4) {
                    z3 = true;
                }
            }
            if (!z3 && !TextUtils.isEmpty(j7)) {
                intent10.setData(Uri.parse(j7));
            }
            if (!z3 && !TextUtils.isEmpty(j8)) {
                intent10.setType(j8);
            }
            if (!z3 && !TextUtils.isEmpty(j9)) {
                intent10.setPackage(j9);
            }
            if (!z3) {
                try {
                    startActivity(intent10);
                } catch (Exception e5) {
                }
            }
        } else if ("/clearapidata".equals(str)) {
            com.wiyun.game.a.f.b();
        } else {
            String[] split4 = str3.split("&");
            String[] strArr = TextUtils.isEmpty(str3) ? null : new String[(split4.length * 2)];
            if (strArr != null) {
                int i4 = 0;
                for (String split5 : split4) {
                    String[] split6 = split5.split("=");
                    int i5 = i4 + 1;
                    strArr[i4] = split6[0];
                    if (split6.length == 1) {
                        i4 = i5 + 1;
                        strArr[i5] = "";
                    } else {
                        i4 = i5 + 1;
                        strArr[i5] = h.j(split6[1]);
                    }
                }
            }
            f.b(str, str2.toUpperCase(), str4, strArr);
            if ("true".equals(str5)) {
                this.X.put(str4, str4);
            }
        }
    }

    public void b(int i2) {
        switch (i2) {
            case 1000:
                String j2 = h.j(this.ac.get("challenge_id"));
                if ("true".equals(h.j(this.ac.get("has_blob")))) {
                    try {
                        String a2 = f.a("/challenge/blob", "GET", "challenge_id", j2);
                        Intent intent = new Intent(this, DownloadBlob.class);
                        intent.putExtra("blob_url", a2);
                        intent.putExtra("message", t.h("wy_label_downloading_challenge_blob"));
                        startActivity(intent);
                        return;
                    } catch (UnsupportedEncodingException e2) {
                        return;
                    }
                } else {
                    y();
                    return;
                }
            case 1001:
                this.e.setVisibility(0);
                f.a(WiGame.getMyId());
                return;
            case 1002:
                j();
                a("account");
                return;
            default:
                return;
        }
    }

    public void b(final com.wiyun.game.b.e eVar) {
        switch (eVar.a) {
            case 2:
                if (!eVar.c) {
                    finish();
                    return;
                }
                return;
            case 14:
                if (!eVar.c) {
                    if (TextUtils.equals(WiGame.getMyId(), eVar.a("user_id"))) {
                        runOnUiThread(new Runnable() {
                            public void run() {
                                List notices = new ArrayList();
                                ag u = WiGame.t();
                                Home.this.U = u.getPendingFriendCount();
                                if (Home.this.U > 0) {
                                    notices.add(String.format(t.h("wy_label_my_friends_hint2"), Integer.valueOf(Home.this.U)));
                                }
                                Home.this.S = u.getUnreadMessageCount();
                                if (Home.this.S > 0) {
                                    notices.add(String.format(t.h("wy_label_my_message_hint2"), Integer.valueOf(Home.this.S)));
                                }
                                Home.this.T = u.getNoticeCount();
                                if (Home.this.T > 0) {
                                    notices.add(String.format(t.h("wy_label_system_notice_hint2"), Integer.valueOf(Home.this.T)));
                                }
                                Home.this.R = u.getPendingChallengeCount();
                                if (Home.this.R > 0) {
                                    notices.add(String.format(t.h("wy_label_pending_challenge_hint"), Integer.valueOf(Home.this.R)));
                                }
                                Home.this.p.setCurrentText(String.format(t.h("wy_label_x_welcome_to_wiyun"), WiGame.getMyName()));
                                if (Home.V != null) {
                                    Home.V.a((String[]) notices.toArray(new String[notices.size()]));
                                }
                            }
                        });
                        return;
                    }
                    return;
                }
                return;
            case 56:
            case 60:
            case 68:
                if (eVar.c) {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            Home.this.e.setVisibility(4);
                            Toast.makeText(Home.this, (String) eVar.e, 0).show();
                        }
                    });
                    return;
                } else {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            Home.this.e.setVisibility(4);
                            Home.this.i();
                            Home.this.l();
                            Home.this.e();
                            Home.this.c.reload();
                        }
                    });
                    return;
                }
            case 89:
                if (!this.ag) {
                    final StringBuffer stringBuffer = new StringBuffer();
                    stringBuffer.append("javascript:wigame_response(").append('\'').append((String) eVar.f).append("',").append(eVar.b).append(',');
                    String str = (String) eVar.e;
                    if (eVar.c || (!str.startsWith("{") && !str.startsWith("["))) {
                        stringBuffer.append('\'').append(h.j((String) eVar.e).replace("'", "\\'")).append('\'');
                    } else {
                        stringBuffer.append(str);
                    }
                    if (this.af != null && this.af.i == eVar.j) {
                        stringBuffer.append(",'").append(this.af.g[1].replace("'", "\\'")).append('\'');
                        if (!this.af.c) {
                            runOnUiThread(new Runnable() {
                                public void run() {
                                    Home.this.i();
                                    Home.this.l();
                                    Home.this.af = (g) null;
                                }
                            });
                        }
                    }
                    stringBuffer.append(")");
                    if (eVar.b >= 300) {
                        runOnUiThread(new Runnable() {
                            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                             method: com.wiyun.game.h.a(android.content.Context, java.lang.String, boolean):void
                             arg types: [com.wiyun.game.Home, java.lang.String, int]
                             candidates:
                              com.wiyun.game.h.a(int, java.lang.String, int):int
                              com.wiyun.game.h.a(java.lang.String, java.lang.String, int):int
                              com.wiyun.game.h.a(android.graphics.Bitmap, int, int):android.graphics.Bitmap
                              com.wiyun.game.h.a(byte[], int, int):java.lang.String
                              com.wiyun.game.h.a(java.lang.String, boolean, int):void
                              com.wiyun.game.h.a(java.lang.StringBuilder, java.lang.String, java.lang.String):void
                              com.wiyun.game.h.a(byte[], byte[], boolean):boolean
                              com.wiyun.game.h.a(com.wiyun.game.a.i, java.lang.String, java.lang.String):byte[]
                              com.wiyun.game.h.a(com.wiyun.game.a.i, byte[], java.lang.String):byte[]
                              com.wiyun.game.h.a(android.content.Context, java.lang.String, boolean):void */
                            public void run() {
                                h.a((Context) Home.this, (String) eVar.e, false);
                            }
                        });
                    }
                    runOnUiThread(new Runnable() {
                        public void run() {
                            Home.this.E.setText((CharSequence) null);
                            Home.this.c.loadUrl(stringBuffer.toString());
                        }
                    });
                    this.X.remove((String) eVar.f);
                    if (this.X.isEmpty()) {
                        runOnUiThread(new Runnable() {
                            public void run() {
                                Home.this.e.setVisibility(4);
                            }
                        });
                        return;
                    }
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void beforeTextChanged(CharSequence s2, int start, int count, int after) {
        if (s2.length() > start && s2.charAt(start) == ']' && count == 1 && after == 0) {
            this.P = true;
            this.Q = start;
            return;
        }
        this.P = false;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        switch (i2) {
            case 1:
                if (i3 == -1) {
                    String stringExtra = intent.getStringExtra("tag");
                    String stringExtra2 = intent.getStringExtra("action");
                    String stringExtra3 = intent.getStringExtra("params");
                    StringBuffer stringBuffer = new StringBuffer();
                    stringBuffer.append("javascript:wigame_response('").append(stringExtra).append("',").append(200).append(",'").append(stringExtra2).append("',").append(stringExtra3).append(")");
                    this.c.loadUrl(stringBuffer.toString());
                    return;
                }
                return;
            default:
                super.onActivityResult(i2, i3, intent);
                return;
        }
    }

    public void onCheckedChanged(CompoundButton cb, boolean checked) {
        if (cb == this.y) {
            WiGame.t().a(checked);
            f.a(WiGame.t().a(), WiGame.t().b());
        }
    }

    public void onClick(View view) {
        int id = view.getId();
        if (id == t.d("wy_b_back")) {
            k();
        } else if (id == t.d("wy_b_activity")) {
            c(0);
        } else if (id == t.d("wy_b_game")) {
            c(1);
        } else if (id == t.d("wy_b_profile")) {
            c(2);
        } else if (id == t.d("wy_b_wibox")) {
            c(3);
        } else if (id == t.d("wy_b_close")) {
            finish();
        } else if (id == t.d("wy_ib_smily")) {
            if (this.j.getVisibility() == 0) {
                this.j.setVisibility(8);
                return;
            }
            this.j.setVisibility(0);
            this.E.clearFocus();
            l();
        } else if (id == t.d("wy_b_send")) {
            String trim = this.E.getText().toString().trim();
            if (TextUtils.isEmpty(trim)) {
                Toast.makeText(this, t.f("wy_toast_please_input_reply"), 0).show();
            } else if (this.af.b <= 0 || trim.length() <= this.af.b) {
                this.e.setVisibility(0);
                this.af.g[1] = trim;
                this.af.i = f.b(this.af.a, "POST", this.af.f, this.af.g);
            } else {
                Toast.makeText(this, String.format(t.h("wy_toast_content_too_long"), Integer.valueOf(this.af.b)), 0).show();
            }
        } else if (id == t.d("wy_ll_header_center")) {
            m();
        } else if (id == t.d("wy_fl_popup_panel")) {
            this.f.setVisibility(4);
        } else if (id == t.d("wy_iv_popup_portrait")) {
            startActivity(new Intent(this, ChangeMyPortrait.class));
            this.f.setVisibility(4);
        } else if (id == t.d("wy_b_challenge")) {
            this.R = 0;
            h();
            j();
            a("challenge");
        } else if (id == t.d("wy_b_message")) {
            this.S = 0;
            h();
            j();
            a("threads");
        } else if (id == t.d("wy_b_notice")) {
            this.T = 0;
            h();
            j();
            a("notices");
        } else if (id == t.d("wy_b_local_score")) {
            q();
        } else if (id == t.d("wy_b_settings")) {
            n();
        } else if (id == t.d("wy_b_friends")) {
            this.U = 0;
            h();
            j();
            a("myfriends");
        } else if (id == t.d("wy_b_switch_account")) {
            Intent intent = new Intent(this, SwitchAccount.class);
            if (WiGame.a != null) {
                intent.putParcelableArrayListExtra("bound_users", WiGame.a);
            }
            startActivity(intent);
        } else if (id == t.d("wy_b_clear_cache")) {
            if (h.a(getCacheDir())) {
                Toast.makeText(this, t.f("wy_toast_clear_cache_ok"), 0).show();
            } else {
                Toast.makeText(this, t.f("wy_toast_clear_cache_fail"), 0).show();
            }
            Intent intent2 = new Intent("com.wiyun.game.CACHE_CLEARED");
            intent2.setFlags(1073741824);
            sendBroadcast(intent2);
        } else if (id == t.d("wy_b_logout_account")) {
            if (WiGame.t().d()) {
                showDialog(1001);
            } else {
                showDialog(1002);
            }
        } else if (id >= 10000) {
            int selectionEnd = this.E.getSelectionEnd();
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("[").append(this.O[id - 10000]).append("]");
            this.E.getEditableText().insert(selectionEnd, stringBuffer.toString());
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WiGame.d = this;
        if (savedInstanceState != null) {
            WiGame.b(savedInstanceState);
            WiGame.a(this);
        }
        if (WiGame.j) {
            getWindow().addFlags(1024);
        }
        requestWindowFeature(1);
        setContentView(t.e("wy_activity_home"));
        b();
        c();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i2) {
        switch (i2) {
            case 1000:
                return com.wiyun.game.a.c.a(this, i2, 17301659, t.h("wy_title_confirm_accept_challenge"), t.h("wy_label_confirm_accept_challenge"), t.f("wy_button_start_challenge"), t.f("wy_button_later"), this);
            case 1001:
                return com.wiyun.game.a.c.a(this, i2, 17301543, t.h("wy_title_confirm_logout"), t.h("wy_label_confirm_logout"), t.f("wy_button_ok"), t.f("wy_button_cancel"), this);
            case 1002:
                return com.wiyun.game.a.c.a(this, i2, 17301659, t.h("wy_title_please_bind_first"), t.h("wy_label_please_bind_first"), t.f("wy_button_ok"), t.f("wy_button_cancel"), this);
            default:
                return super.onCreateDialog(i2);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        if (!WiGame.F()) {
            menu.add(1, 1, 0, t.f("wy_option_item_search_user")).setIcon(t.c("wy_menu_search_user"));
            menu.add(1, 2, 0, t.f("wy_option_item_my_friends")).setIcon(t.c("wy_menu_my_friends"));
            menu.add(1, 3, 0, t.f("wy_option_item_exit")).setIcon(t.c("wy_menu_exit"));
            menu.add(1, 4, 0, t.f("wy_option_item_reload")).setIcon(t.c("wy_menu_reload"));
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.ag = true;
        WiGame.d = null;
        com.wiyun.game.b.d.a().b((com.wiyun.game.b.b) this);
        com.wiyun.game.b.d.a().b((com.wiyun.game.b.a) this);
        a = false;
        if (V != null) {
            V.a();
            V = null;
        }
        this.c.stopLoading();
        this.c.setWebViewClient(null);
        this.c.setWebChromeClient(null);
        this.d.removeView(this.c);
        this.c.destroy();
        this.c = null;
        g();
        f();
        unregisterReceiver(this.ah);
        super.onDestroy();
    }

    public void onDownloadStart(String str, String str2, String str3, String str4, long j2) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(Uri.parse(str));
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException e2) {
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || event.getRepeatCount() != 0) {
            return super.onKeyDown(keyCode, event);
        }
        this.W = true;
        return true;
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode != 4 || !this.W) {
            return super.onKeyUp(keyCode, event);
        }
        this.W = false;
        k();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        a(intent);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                v();
                break;
            case 2:
                u();
                break;
            case 3:
                t();
                break;
            case 4:
                s();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPostCreate(Bundle icicle) {
        super.onPostCreate(icicle);
        if (WiGame.v()) {
            return;
        }
        if (!WiGame.isLoggedIn()) {
            f.b(WiGame.getMyId());
            return;
        }
        c(1);
        a(getIntent());
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.setGroupVisible(1, WiGame.isLoggedIn());
        return true;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle state) {
        super.onRestoreInstanceState(state);
        c(state.getInt("tab_id"));
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("tab_id", this.b);
        WiGame.a(outState);
    }

    public void onTextChanged(CharSequence s2, int start, int before, int count) {
    }

    public boolean onTouch(View v2, MotionEvent e2) {
        if (v2 == this.E) {
            switch (e2.getAction()) {
                case 1:
                    this.j.setVisibility(8);
                    return false;
                default:
                    return false;
            }
        } else if (v2 != this.c || this.g.getTag() == null) {
            return false;
        } else {
            k();
            return false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wiyun.game.h.a(java.util.Map<java.lang.String, android.graphics.Bitmap>, boolean, java.lang.String, java.lang.String, boolean):android.graphics.Bitmap
     arg types: [java.util.Map<java.lang.String, android.graphics.Bitmap>, int, java.lang.String, java.lang.String, boolean]
     candidates:
      com.wiyun.game.h.a(int, int, int, int, android.net.Uri):android.content.Intent
      com.wiyun.game.h.a(android.content.Context, java.lang.String, int, android.view.View$OnClickListener, int[]):void
      com.wiyun.game.h.a(java.util.Map<java.lang.String, android.graphics.Bitmap>, boolean, java.lang.String, java.lang.String, boolean):android.graphics.Bitmap */
    public boolean setViewValue(View view, Cursor cursor, int i2) {
        TextView textView = (TextView) view.findViewById(t.d("wy_tv_name"));
        TextView textView2 = (TextView) view.findViewById(t.d("wy_tv_time"));
        TextView textView3 = (TextView) view.findViewById(t.d("wy_tv_score"));
        ((ImageView) view.findViewById(t.d("wy_iv_portrait"))).setImageBitmap(h.a(this.Z, false, h.b("p_", WiGame.getMyId()), WiGame.t().getAvatarUrl(), WiGame.t().isFemale()));
        String name = WiGame.t().getName();
        if (TextUtils.isEmpty(name)) {
            name = t.h("wy_label_me");
        }
        textView.setText((cursor.getPosition() + 1) + ". " + name);
        textView2.setText(String.format(t.h("wy_label_achieve_at_x"), this.ab.a(this, cursor.getLong(4))));
        int i3 = cursor.getInt(3);
        com.wiyun.game.model.a.c b2 = x.b(cursor.getString(1));
        if (b2 == null) {
            textView3.setText(String.valueOf(i3));
        } else if (b2.c()) {
            int i4 = i3 % 1000;
            int i5 = i3 / 1000;
            textView3.setText(String.format("%d:%d.%d", Integer.valueOf(i5 / 60), Integer.valueOf(i5 % 60), Integer.valueOf(i4)));
        } else {
            textView3.setText(String.valueOf(i3));
        }
        return true;
    }
}
