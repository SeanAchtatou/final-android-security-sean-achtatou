package com.wiyun.game;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.wiyun.game.a.d;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpProtocolParams;

public class DownloadBlob extends Activity implements View.OnClickListener {
    /* access modifiers changed from: private */
    public ProgressBar a;
    /* access modifiers changed from: private */
    public TextView b;
    /* access modifiers changed from: private */
    public boolean c;
    private boolean d;
    /* access modifiers changed from: private */
    public int e;
    /* access modifiers changed from: private */
    public int f;
    /* access modifiers changed from: private */
    public String g;
    /* access modifiers changed from: private */
    public boolean h;
    /* access modifiers changed from: private */
    public boolean i;
    /* access modifiers changed from: private */
    public String j;

    private void a() {
        Intent intent = getIntent();
        this.g = intent.getStringExtra("blob_url");
        this.j = intent.getStringExtra("message");
        this.h = intent.getBooleanExtra("use_tmp_file", false);
    }

    /* access modifiers changed from: private */
    public boolean a(HttpResponse response, ByteArrayOutputStream baos, File file) throws IOException {
        byte[] buf = new byte[4096];
        InputStream is = response.getEntity().getContent();
        if (is == null) {
            return false;
        }
        FileOutputStream fos = null;
        if (file != null) {
            try {
                if (file.getParentFile().exists()) {
                    file.getParentFile().mkdirs();
                }
                fos = new FileOutputStream(file);
            } catch (Exception e2) {
                if (fos != null) {
                    try {
                        fos.close();
                    } catch (Exception e3) {
                    }
                }
                return false;
            } catch (Throwable th) {
                if (fos != null) {
                    try {
                        fos.close();
                    } catch (Exception e4) {
                    }
                }
                throw th;
            }
        }
        for (int i2 = 0; i2 != -1; i2 = is.read(buf)) {
            if (baos != null) {
                baos.write(buf, 0, i2);
            }
            if (fos != null) {
                fos.write(buf, 0, i2);
            }
            if (this.d || this.c) {
                if (fos != null) {
                    try {
                        fos.close();
                    } catch (Exception e5) {
                    }
                }
                return false;
            }
            if (!this.i) {
                this.f += i2;
                runOnUiThread(new Runnable() {
                    public void run() {
                        TextView h = DownloadBlob.this.b;
                        Object[] objArr = new Object[2];
                        objArr[0] = DownloadBlob.this.j;
                        objArr[1] = Integer.valueOf(DownloadBlob.this.e == 0 ? 0 : (DownloadBlob.this.f * 100) / DownloadBlob.this.e);
                        h.setText(String.format("%s%d%%", objArr));
                        DownloadBlob.this.a.setProgress(DownloadBlob.this.f);
                    }
                });
            }
        }
        if (fos != null) {
            try {
                fos.close();
            } catch (Exception e6) {
            }
        }
        return true;
    }

    private void b() {
        this.a = (ProgressBar) findViewById(t.d("wy_pb_download"));
        this.b = (TextView) findViewById(t.d("wy_tv_progress_hint"));
        this.b.setText(this.j);
        ((Button) findViewById(t.d("wy_b_cancel"))).setOnClickListener(this);
    }

    /* access modifiers changed from: private */
    public void c() {
        runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(DownloadBlob.this, t.f(DownloadBlob.this.c ? "wy_toast_cancelled_download_blob" : "wy_toast_failed_to_download_blob"), 0).show();
                DownloadBlob.this.finish();
            }
        });
    }

    /* access modifiers changed from: private */
    public DefaultHttpClient d() {
        HttpHost d2;
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        HttpProtocolParams.setVersion(basicHttpParams, HttpVersion.HTTP_1_0);
        HttpProtocolParams.setContentCharset(basicHttpParams, "UTF-8");
        HttpProtocolParams.setUseExpectContinue(basicHttpParams, false);
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, 10000);
        HttpConnectionParams.setSoTimeout(basicHttpParams, 10000);
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient(basicHttpParams);
        if (!d.b() && d.c() && (d2 = d.d()) != null) {
            defaultHttpClient.getParams().setParameter("http.route.default-proxy", d2);
        }
        return defaultHttpClient;
    }

    public void onClick(View v) {
        if (v.getId() == t.d("wy_b_cancel")) {
            this.c = true;
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView(t.e("wy_activity_download_blob"));
        a();
        b();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.d = true;
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        Thread t = new Thread() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.wiyun.game.DownloadBlob.a(com.wiyun.game.DownloadBlob, boolean):void
             arg types: [com.wiyun.game.DownloadBlob, int]
             candidates:
              com.wiyun.game.DownloadBlob.a(com.wiyun.game.DownloadBlob, int):void
              com.wiyun.game.DownloadBlob.a(com.wiyun.game.DownloadBlob, boolean):void */
            public void run() {
                DefaultHttpClient client = DownloadBlob.this.d();
                try {
                    HttpResponse response = client.execute(new HttpGet(h.e(DownloadBlob.this.g)));
                    if (response.getStatusLine().getStatusCode() < 300) {
                        Header header = response.getFirstHeader("Content-Length");
                        if (header == null) {
                            DownloadBlob.this.i = true;
                            DownloadBlob.this.a.setIndeterminate(true);
                        } else {
                            DownloadBlob.this.e = h.c(header.getValue());
                            DownloadBlob.this.runOnUiThread(new Runnable() {
                                public void run() {
                                    DownloadBlob.this.a.setMax(DownloadBlob.this.e);
                                }
                            });
                        }
                        ByteArrayOutputStream baos = DownloadBlob.this.h ? null : new ByteArrayOutputStream();
                        File file = DownloadBlob.this.h ? new File(DownloadBlob.this.getCacheDir(), "wy_tmp_blob") : null;
                        if (DownloadBlob.this.a(response, baos, file)) {
                            Intent intent = new Intent("com.wiyun.game.BLOB_DOWNLOADED");
                            if (baos != null) {
                                intent.putExtra("blob", baos.toByteArray());
                            }
                            if (file != null) {
                                intent.putExtra("tmp_path", file.getAbsolutePath());
                            }
                            intent.setFlags(1073741824);
                            DownloadBlob.this.sendBroadcast(intent);
                            DownloadBlob.this.runOnUiThread(new Runnable() {
                                public void run() {
                                    DownloadBlob.this.finish();
                                }
                            });
                        } else {
                            DownloadBlob.this.c();
                        }
                    } else {
                        DownloadBlob.this.c();
                    }
                } catch (Exception e) {
                    DownloadBlob.this.c();
                } finally {
                    client.getConnectionManager().shutdown();
                }
            }
        };
        t.setDaemon(true);
        t.setName("Blob Download Thread");
        t.setPriority(1);
        t.start();
    }
}
