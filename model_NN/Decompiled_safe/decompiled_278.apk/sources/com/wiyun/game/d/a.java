package com.wiyun.game.d;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Handler;
import com.wiyun.game.MonitoredActivity;
import java.io.Closeable;

public class a {

    /* renamed from: com.wiyun.game.d.a$a  reason: collision with other inner class name */
    private static class C0003a extends MonitoredActivity.b implements Runnable {
        /* access modifiers changed from: private */
        public final MonitoredActivity a;
        /* access modifiers changed from: private */
        public final ProgressDialog b;
        private final Runnable c;
        private final Handler d;
        private final Runnable e = new f(this);

        public C0003a(MonitoredActivity activity, Runnable job, ProgressDialog dialog, Handler handler) {
            this.a = activity;
            this.b = dialog;
            this.c = job;
            this.a.a(this);
            this.d = handler;
        }

        public void a(MonitoredActivity activity) {
            this.e.run();
            this.d.removeCallbacks(this.e);
        }

        public void b(MonitoredActivity activity) {
            this.b.hide();
        }

        public void c(MonitoredActivity activity) {
            this.b.show();
        }

        public void run() {
            try {
                this.c.run();
            } finally {
                this.d.post(this.e);
            }
        }
    }

    private a() {
    }

    /* JADX INFO: Multiple debug info for r1v2 float: [D('bitmapWidthF' float), D('deltaY' int)] */
    /* JADX INFO: Multiple debug info for r0v2 float: [D('deltaX' int), D('bitmapHeightF' float)] */
    /* JADX INFO: Multiple debug info for r9v1 android.graphics.Bitmap: [D('b2' android.graphics.Bitmap), D('targetWidth' int)] */
    /* JADX INFO: Multiple debug info for r7v5 android.graphics.Bitmap: [D('b1' android.graphics.Bitmap), D('b2' android.graphics.Bitmap)] */
    /* JADX INFO: Multiple debug info for r7v9 android.graphics.Bitmap: [D('scaler' android.graphics.Matrix), D('b2' android.graphics.Bitmap)] */
    /* JADX INFO: Multiple debug info for r11v12 android.graphics.Canvas: [D('scaleUp' boolean), D('c' android.graphics.Canvas)] */
    /* JADX INFO: Multiple debug info for r0v18 int: [D('deltaXHalf' int), D('deltaX' int)] */
    /* JADX INFO: Multiple debug info for r1v7 int: [D('deltaYHalf' int), D('deltaY' int)] */
    /* JADX INFO: Multiple debug info for r1v8 int: [D('deltaYHalf' int), D('dstX' int)] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static Bitmap a(Matrix scaler, Bitmap source, int targetWidth, int targetHeight, boolean scaleUp) {
        Bitmap b1;
        int deltaX = source.getWidth() - targetWidth;
        int deltaY = source.getHeight() - targetHeight;
        if (scaleUp || (deltaX >= 0 && deltaY >= 0)) {
            float bitmapWidthF = (float) source.getWidth();
            float bitmapHeightF = (float) source.getHeight();
            if (bitmapWidthF / bitmapHeightF > ((float) targetWidth) / ((float) targetHeight)) {
                float scale = ((float) targetHeight) / bitmapHeightF;
                if (scale < 0.9f || scale > 1.0f) {
                    scaler.setScale(scale, scale);
                } else {
                    scaler = null;
                }
            } else {
                float scale2 = ((float) targetWidth) / bitmapWidthF;
                if (scale2 < 0.9f || scale2 > 1.0f) {
                    scaler.setScale(scale2, scale2);
                } else {
                    scaler = null;
                }
            }
            if (scaler != null) {
                b1 = Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), scaler, true);
            } else {
                b1 = source;
            }
            Bitmap b2 = Bitmap.createBitmap(b1, Math.max(0, b1.getWidth() - targetWidth) / 2, Math.max(0, b1.getHeight() - targetHeight) / 2, targetWidth, targetHeight);
            if (b1 != source) {
                b1.recycle();
            }
            return b2;
        }
        Bitmap b22 = Bitmap.createBitmap(targetWidth, targetHeight, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b22);
        int deltaX2 = Math.max(0, deltaX / 2);
        int deltaY2 = Math.max(0, deltaY / 2);
        Rect src = new Rect(deltaX2, deltaY2, Math.min(targetWidth, source.getWidth()) + deltaX2, Math.min(targetHeight, source.getHeight()) + deltaY2);
        int dstX = (targetWidth - src.width()) / 2;
        int dstY = (targetHeight - src.height()) / 2;
        c.drawBitmap(source, src, new Rect(dstX, dstY, targetWidth - dstX, targetHeight - dstY), (Paint) null);
        return b22;
    }

    public static void a(MonitoredActivity activity, String title, String message, Runnable job, Handler handler) {
        new Thread(new C0003a(activity, job, ProgressDialog.show(activity, title, message, true, false), handler)).start();
    }

    public static void a(Closeable c) {
        if (c != null) {
            try {
                c.close();
            } catch (Throwable th) {
            }
        }
    }
}
