package com.wiyun.game;

import android.app.ListActivity;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.saubcy.games.maze.market.MazeGame;
import com.wiyun.game.b.d;
import com.wiyun.game.b.e;
import com.wiyun.game.model.a.ag;
import com.wiyun.game.model.a.r;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SendChallenge extends ListActivity implements View.OnClickListener, View.OnFocusChangeListener, com.wiyun.game.b.b {
    private String a;
    /* access modifiers changed from: private */
    public int b;
    private byte[] c;
    private String d;
    /* access modifiers changed from: private */
    public int e;
    /* access modifiers changed from: private */
    public int f;
    /* access modifiers changed from: private */
    public int g;
    private boolean h;
    private boolean i;
    /* access modifiers changed from: private */
    public r j;
    /* access modifiers changed from: private */
    public View k;
    /* access modifiers changed from: private */
    public ViewGroup l;
    /* access modifiers changed from: private */
    public TextView m;
    /* access modifiers changed from: private */
    public TextView n;
    private EditText o;
    private Button p;
    private Button q;
    private Button r;
    /* access modifiers changed from: private */
    public List<ag> s;
    /* access modifiers changed from: private */
    public List<ag> t;
    /* access modifiers changed from: private */
    public List<ag> u;
    /* access modifiers changed from: private */
    public Map<String, Bitmap> v;
    /* access modifiers changed from: private */
    public Map<String, ag> w;
    /* access modifiers changed from: private */
    public long x;
    private BroadcastReceiver y = new w(this);

    private static final class a {
        int a;
        TextView b;
        TextView c;
        TextView d;
        TextView e;
        CheckBox f;
        ImageView g;

        private a() {
        }

        /* synthetic */ a(a aVar) {
            this();
        }
    }

    private final class b extends BaseAdapter {
        private b() {
        }

        /* synthetic */ b(SendChallenge sendChallenge, b bVar) {
            this();
        }

        private View a(View view, ViewGroup viewGroup) {
            View view2;
            if (view == null || ((a) view.getTag()).a != 5) {
                view2 = LayoutInflater.from(SendChallenge.this).inflate(t.e("wy_list_item_load_hint"), (ViewGroup) null);
                a aVar = new a(null);
                aVar.a = 5;
                aVar.b = (TextView) view2.findViewById(t.d("wy_text"));
                aVar.c = (TextView) view2.findViewById(t.d("wy_text2"));
                view2.setTag(aVar);
            } else {
                view2 = view;
            }
            a aVar2 = (a) view2.getTag();
            int h = SendChallenge.this.g;
            int size = SendChallenge.this.u.size();
            if (size < h) {
                int min = Math.min(h - size, 25);
                aVar2.b.setText(String.format(t.h("wy_send_challenge_label_load_next_x_more"), Integer.valueOf(min)));
            } else {
                aVar2.b.setText(t.f("wy_send_challenge_label_no_more_to_load"));
            }
            aVar2.c.setText(String.format(SendChallenge.this.getString(t.f("wy_send_challenge_label_showing_x_of_y")), Integer.valueOf(size), Integer.valueOf(h)));
            return view2;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.wiyun.game.h.a(java.util.Map<java.lang.String, android.graphics.Bitmap>, boolean, java.lang.String, java.lang.String, boolean):android.graphics.Bitmap
         arg types: [java.util.Map, int, java.lang.String, java.lang.String, boolean]
         candidates:
          com.wiyun.game.h.a(int, int, int, int, android.net.Uri):android.content.Intent
          com.wiyun.game.h.a(android.content.Context, java.lang.String, int, android.view.View$OnClickListener, int[]):void
          com.wiyun.game.h.a(java.util.Map<java.lang.String, android.graphics.Bitmap>, boolean, java.lang.String, java.lang.String, boolean):android.graphics.Bitmap */
        private View a(View convertView, ViewGroup parent, ag user) {
            if (convertView == null || ((a) convertView.getTag()).a != 4) {
                convertView = LayoutInflater.from(SendChallenge.this).inflate(t.e("wy_list_item_checkbox_score"), (ViewGroup) null);
                a vh = new a(null);
                vh.a = 4;
                vh.b = (TextView) convertView.findViewById(t.d("wy_text"));
                vh.c = (TextView) convertView.findViewById(t.d("wy_text2"));
                vh.d = (TextView) convertView.findViewById(t.d("wy_text3"));
                vh.g = (ImageView) convertView.findViewById(t.d("wy_image"));
                vh.f = (CheckBox) convertView.findViewById(t.d("wy_checkbox"));
                convertView.setTag(vh);
            }
            a vh2 = (a) convertView.getTag();
            vh2.b.setText(user.getName());
            vh2.d.setText(String.format(t.h("wy_label_best_score_is_x"), String.valueOf(user.e())));
            vh2.c.setVisibility(user.c() == 1 ? 0 : 8);
            vh2.f.setChecked(SendChallenge.this.w.containsKey(user.getId()));
            vh2.g.setImageBitmap(h.a((Map<String, Bitmap>) SendChallenge.this.v, false, h.b("p_", user.getId()), user.getAvatarUrl(), user.isFemale()));
            return convertView;
        }

        private View a(View view, ViewGroup viewGroup, String str) {
            View view2;
            if (view == null || ((a) view.getTag()).a != 2) {
                view2 = LayoutInflater.from(SendChallenge.this).inflate(t.e("wy_list_item_help"), (ViewGroup) null);
                a aVar = new a(null);
                aVar.a = 2;
                aVar.b = (TextView) view2.findViewById(t.d("wy_text"));
                view2.setTag(aVar);
            } else {
                view2 = view;
            }
            ((a) view2.getTag()).b.setText(str);
            return view2;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.wiyun.game.h.a(java.util.Map<java.lang.String, android.graphics.Bitmap>, boolean, java.lang.String, java.lang.String, boolean):android.graphics.Bitmap
         arg types: [java.util.Map, int, java.lang.String, java.lang.String, boolean]
         candidates:
          com.wiyun.game.h.a(int, int, int, int, android.net.Uri):android.content.Intent
          com.wiyun.game.h.a(android.content.Context, java.lang.String, int, android.view.View$OnClickListener, int[]):void
          com.wiyun.game.h.a(java.util.Map<java.lang.String, android.graphics.Bitmap>, boolean, java.lang.String, java.lang.String, boolean):android.graphics.Bitmap */
        private View b(View view, ViewGroup viewGroup, ag agVar) {
            View view2;
            if (view == null || ((a) view.getTag()).a != 3) {
                View inflate = LayoutInflater.from(SendChallenge.this).inflate(t.e("wy_list_item_checkbox_neighbor"), (ViewGroup) null);
                a aVar = new a(null);
                aVar.a = 3;
                aVar.b = (TextView) inflate.findViewById(t.d("wy_text"));
                aVar.c = (TextView) inflate.findViewById(t.d("wy_text2"));
                aVar.d = (TextView) inflate.findViewById(t.d("wy_text3"));
                aVar.e = (TextView) inflate.findViewById(t.d("wy_text4"));
                aVar.f = (CheckBox) inflate.findViewById(t.d("wy_checkbox"));
                aVar.g = (ImageView) inflate.findViewById(t.d("wy_image"));
                inflate.setTag(aVar);
                view2 = inflate;
            } else {
                view2 = view;
            }
            a aVar2 = (a) view2.getTag();
            aVar2.b.setText(agVar.getName());
            aVar2.c.setVisibility(agVar.c() == 1 ? 0 : 8);
            if (TextUtils.isEmpty(agVar.getLastAppName())) {
                aVar2.d.setVisibility(8);
            } else {
                aVar2.d.setText(String.format(t.h("wy_send_challenge_label_last_played_x"), agVar.getLastAppName()));
            }
            if (WiGame.h()) {
                boolean z = agVar.c() == 1;
                double a2 = h.a(WiGame.getLatitude(), WiGame.getLongitude(), agVar.getLatitude(), agVar.getLongitude());
                aVar2.e.setVisibility(0);
                aVar2.e.setText(String.format(SendChallenge.this.getString(z ? t.f("wy_send_challenge_label_now_from_me_x_kilometers") : t.f("wy_send_challenge_label_recently_from_me_x_kilometers")), Double.valueOf(a2)));
            } else {
                aVar2.e.setVisibility(8);
            }
            aVar2.f.setChecked(SendChallenge.this.w.containsKey(agVar.getId()));
            aVar2.g.setImageBitmap(h.a((Map<String, Bitmap>) SendChallenge.this.v, false, h.b("p_", agVar.getId()), agVar.getAvatarUrl(), agVar.isFemale()));
            return view2;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.wiyun.game.h.a(java.util.Map<java.lang.String, android.graphics.Bitmap>, boolean, java.lang.String, java.lang.String, boolean):android.graphics.Bitmap
         arg types: [java.util.Map, int, java.lang.String, java.lang.String, boolean]
         candidates:
          com.wiyun.game.h.a(int, int, int, int, android.net.Uri):android.content.Intent
          com.wiyun.game.h.a(android.content.Context, java.lang.String, int, android.view.View$OnClickListener, int[]):void
          com.wiyun.game.h.a(java.util.Map<java.lang.String, android.graphics.Bitmap>, boolean, java.lang.String, java.lang.String, boolean):android.graphics.Bitmap */
        private View c(View view, ViewGroup viewGroup, ag agVar) {
            View view2;
            if (view == null || ((a) view.getTag()).a != 1) {
                view2 = LayoutInflater.from(SendChallenge.this).inflate(t.e("wy_list_item_checkbox_friend"), (ViewGroup) null);
                a aVar = new a(null);
                aVar.a = 1;
                aVar.b = (TextView) view2.findViewById(t.d("wy_text"));
                aVar.c = (TextView) view2.findViewById(t.d("wy_text2"));
                aVar.d = (TextView) view2.findViewById(t.d("wy_text3"));
                aVar.f = (CheckBox) view2.findViewById(t.d("wy_checkbox"));
                aVar.g = (ImageView) view2.findViewById(t.d("wy_image"));
                view2.setTag(aVar);
            } else {
                view2 = view;
            }
            a aVar2 = (a) view2.getTag();
            aVar2.b.setText(agVar.getName());
            aVar2.c.setVisibility(agVar.c() == 1 ? 0 : 8);
            if (TextUtils.isEmpty(agVar.getLastAppName())) {
                aVar2.d.setVisibility(8);
            } else {
                aVar2.d.setText(String.format(t.h("wy_send_challenge_label_last_played_x"), agVar.getLastAppName()));
            }
            aVar2.f.setChecked(SendChallenge.this.w.containsKey(agVar.getId()));
            aVar2.g.setImageBitmap(h.a((Map<String, Bitmap>) SendChallenge.this.v, false, h.b("p_", agVar.getId()), agVar.getAvatarUrl(), agVar.isFemale()));
            return view2;
        }

        public int getCount() {
            switch (SendChallenge.this.f) {
                case 1:
                    if (SendChallenge.this.t.isEmpty()) {
                        return 1;
                    }
                    return SendChallenge.this.t.size();
                case 2:
                    return SendChallenge.this.u.size() + 1;
                case 3:
                    if (SendChallenge.this.s.isEmpty()) {
                        return 1;
                    }
                    return SendChallenge.this.s.size();
                default:
                    return 0;
            }
        }

        public Object getItem(int position) {
            return null;
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            switch (SendChallenge.this.a(position)) {
                case 1:
                    return c(convertView, parent, (ag) SendChallenge.this.b(position));
                case 2:
                    return a(convertView, parent, (String) SendChallenge.this.b(position));
                case 3:
                    return b(convertView, parent, (ag) SendChallenge.this.b(position));
                case 4:
                    return a(convertView, parent, (ag) SendChallenge.this.b(position));
                case 5:
                    return a(convertView, parent);
                default:
                    throw new IllegalArgumentException("Unknown cell tag!");
            }
        }
    }

    /* access modifiers changed from: private */
    public int a(int position) {
        if (this.f == 1) {
            return this.t.isEmpty() ? 2 : 1;
        }
        if (this.f != 2) {
            return this.s.isEmpty() ? 2 : 4;
        }
        if (this.u.isEmpty()) {
            return 2;
        }
        return position < this.u.size() ? 3 : 5;
    }

    private void a() {
        Intent intent = getIntent();
        this.a = intent.getStringExtra("def_id");
        this.b = intent.getIntExtra("score", 0);
        this.c = intent.getByteArrayExtra("blob");
        this.d = intent.getStringExtra("leaderboard_id");
        this.s = new ArrayList();
        this.t = new ArrayList();
        this.u = new ArrayList();
        this.w = new HashMap();
        this.v = new HashMap();
        this.f = 1;
        registerReceiver(this.y, new IntentFilter("com.wiyun.game.IMAGE_DOWNLOADED"));
        d.a().a(this);
    }

    /* access modifiers changed from: private */
    public Object b(int i2) {
        if (this.f == 1) {
            return this.t.isEmpty() ? t.h("wy_send_challenge_label_you_have_no_friends") : this.t.get(i2);
        }
        if (this.f != 2) {
            return this.s.isEmpty() ? t.h("wy_label_no_competitor_found") : this.s.get(i2);
        }
        if (this.u.isEmpty()) {
            return getString(WiGame.h() ? t.f("wy_send_challenge_label_no_neighbors") : t.f("wy_send_challenge_label_cannot_locate_myself"));
        } else if (i2 < this.u.size()) {
            return this.u.get(i2);
        } else {
            return null;
        }
    }

    private void b() {
        this.k = findViewById(t.d("wy_ll_progress_panel"));
        this.l = (ViewGroup) findViewById(t.d("wy_ll_main_panel"));
        this.m = (TextView) findViewById(t.d("wy_tv_challenge_name"));
        this.n = (TextView) findViewById(t.d("wy_tv_score_and_bet"));
        this.o = (EditText) findViewById(t.d("wy_et_challenge_message"));
        this.o.setOnFocusChangeListener(this);
        ((Button) findViewById(t.d("wy_b_send"))).setOnClickListener(this);
        ((Button) findViewById(t.d("wy_b_close"))).setOnClickListener(this);
        ((Button) findViewById(t.d("wy_b_bet"))).setOnClickListener(this);
        this.p = (Button) findViewById(t.d("wy_tab"));
        this.p.setOnClickListener(this);
        this.p.setSelected(true);
        this.q = (Button) findViewById(t.d("wy_tab2"));
        this.q.setOnClickListener(this);
        this.r = (Button) findViewById(t.d("wy_tab3"));
        this.r.setOnClickListener(this);
        this.r.setVisibility(TextUtils.isEmpty(this.d) ? 8 : 0);
        getListView().setOnCreateContextMenuListener(this);
        setListAdapter(new b(this, null));
    }

    private void c() {
        for (Bitmap bitmap : this.v.values()) {
            if (bitmap != null && !bitmap.isRecycled()) {
                bitmap.recycle();
            }
        }
        this.v.clear();
    }

    /* access modifiers changed from: private */
    public void d() {
        ListAdapter adapter = getListAdapter();
        if (adapter != null) {
            ((BaseAdapter) adapter).notifyDataSetChanged();
        }
    }

    private void e() {
        if (this.g > this.u.size() && WiGame.h()) {
            int start = this.u.size();
            this.k.setVisibility(0);
            h.a(this.l);
            this.x = f.a(WiGame.getLatitude(), WiGame.getLongitude(), start, 25);
        }
    }

    private void f() {
        List<ag> users = null;
        switch (this.f) {
            case 1:
                users = this.t;
                break;
            case 2:
                users = this.u;
                break;
            case 3:
                users = this.s;
                break;
        }
        for (ag u2 : users) {
            this.w.remove(u2.getId());
        }
        d();
    }

    private void g() {
        List users = null;
        switch (this.f) {
            case 1:
                users = this.t;
                break;
            case 2:
                users = this.u;
                break;
            case 3:
                users = this.s;
                break;
        }
        for (ag u2 : users) {
            this.w.put(u2.getId(), u2);
        }
        d();
    }

    public void b(final e e2) {
        switch (e2.a) {
            case 14:
                if (this.x == e2.j) {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            SendChallenge.this.k.setVisibility(4);
                            h.b(SendChallenge.this.l);
                        }
                    });
                    e2.d = false;
                    return;
                }
                return;
            case 21:
                if (e2.j != this.x) {
                    return;
                }
                if (e2.c) {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(SendChallenge.this, (String) e2.e, 0).show();
                            SendChallenge.this.finish();
                        }
                    });
                    return;
                }
                this.h = true;
                this.g = e2.g;
                this.u.addAll((List) e2.e);
                runOnUiThread(new Runnable() {
                    public void run() {
                        SendChallenge.this.d();
                        if (WiGame.c()) {
                            SendChallenge.this.k.setVisibility(4);
                            h.b(SendChallenge.this.l);
                            return;
                        }
                        SendChallenge.this.x = f.d(WiGame.getMyId());
                    }
                });
                return;
            case 22:
                if (e2.j != this.x) {
                    return;
                }
                if (e2.c) {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(SendChallenge.this, (String) e2.e, 0).show();
                            SendChallenge.this.finish();
                        }
                    });
                    return;
                }
                this.i = true;
                this.s.addAll((List) e2.e);
                runOnUiThread(new Runnable() {
                    public void run() {
                        SendChallenge.this.d();
                        if (WiGame.c()) {
                            SendChallenge.this.k.setVisibility(4);
                            h.b(SendChallenge.this.l);
                            return;
                        }
                        SendChallenge.this.x = f.d(WiGame.getMyId());
                    }
                });
                return;
            case 23:
                if (e2.j != this.x) {
                    return;
                }
                if (e2.c) {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(SendChallenge.this, (String) e2.e, 0).show();
                            SendChallenge.this.finish();
                        }
                    });
                    return;
                }
                this.t.addAll((List) e2.e);
                runOnUiThread(new Runnable() {
                    public void run() {
                        SendChallenge.this.d();
                        if (WiGame.c()) {
                            SendChallenge.this.k.setVisibility(4);
                            h.b(SendChallenge.this.l);
                            return;
                        }
                        SendChallenge.this.x = f.d(WiGame.getMyId());
                    }
                });
                return;
            case MazeGame.HIGHTREDUCTION:
                if (e2.c) {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(SendChallenge.this, (String) e2.e, 0).show();
                            SendChallenge.this.finish();
                        }
                    });
                    return;
                }
                this.j = (r) e2.e;
                this.x = f.a(WiGame.getMyId(), 0, 50);
                runOnUiThread(new Runnable() {
                    public void run() {
                        SendChallenge.this.m.setText(SendChallenge.this.j.b());
                        SendChallenge.this.n.setText(String.format(t.h("wy_label_score_is_x_bet_is_y"), String.valueOf(SendChallenge.this.b), Integer.valueOf(SendChallenge.this.e)));
                    }
                });
                return;
            case 51:
                if (e2.c) {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            SendChallenge.this.k.setVisibility(4);
                            h.b(SendChallenge.this.l);
                            Toast.makeText(SendChallenge.this, (String) e2.e, 0).show();
                        }
                    });
                    return;
                } else {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(SendChallenge.this, t.f("wy_toast_challenge_is_sent"), 0).show();
                            SendChallenge.this.finish();
                        }
                    });
                    return;
                }
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        switch (i2) {
            case 1:
                if (i3 == -1) {
                    this.e = intent.getIntExtra("value", 0);
                    this.n.setText(String.format(t.h("wy_label_score_is_x_bet_is_y"), String.valueOf(this.b), Integer.valueOf(this.e)));
                    return;
                }
                return;
            default:
                super.onActivityResult(i2, i3, intent);
                return;
        }
    }

    public void onClick(View view) {
        int id = view.getId();
        if (id == t.d("wy_b_send")) {
            if (this.w.isEmpty()) {
                Toast.makeText(this, t.f("wy_toast_select_one_friend_at_least"), 0).show();
            } else if (this.e < this.j.c()) {
                Toast.makeText(this, t.f("wy_toast_challenge_require_bid"), 0).show();
            } else if (this.e <= 0 || this.e * this.w.size() <= WiGame.t().getHonor()) {
                StringBuilder sb = new StringBuilder();
                for (Map.Entry<String, ag> key : this.w.entrySet()) {
                    sb.append((String) key.getKey()).append(',');
                }
                if (sb.length() > 0) {
                    sb.deleteCharAt(sb.length() - 1);
                }
                this.k.setVisibility(0);
                h.a(this.l);
                f.a(this.j.a(), this.o.getText().toString().trim(), sb.toString(), this.b, this.e, this.c);
            } else {
                Toast.makeText(this, String.format(t.h("wy_toast_point_is_not_enough"), Integer.valueOf(this.e * this.w.size())), 0).show();
            }
        } else if (id == t.d("wy_b_bet")) {
            Intent intent = new Intent(this, BidPicker.class);
            intent.putExtra("min", this.j.c());
            intent.putExtra("max", this.j.d());
            intent.putExtra("init", this.e);
            startActivityForResult(intent, 1);
        } else if (id == t.d("wy_b_close")) {
            finish();
        } else if ((id == t.d("wy_tab") || id == t.d("wy_tab2") || id == t.d("wy_tab3")) && !view.isSelected()) {
            this.p.setSelected(false);
            this.q.setSelected(false);
            this.r.setSelected(false);
            view.setSelected(true);
            if (id == t.d("wy_tab")) {
                this.f = 1;
                d();
            } else if (id == t.d("wy_tab2")) {
                if (!this.h && WiGame.h()) {
                    this.k.setVisibility(0);
                    h.a(this.l);
                    this.x = f.a(WiGame.getLatitude(), WiGame.getLongitude(), 0, 25);
                }
                this.f = 2;
                d();
            } else {
                if (!this.i) {
                    this.k.setVisibility(0);
                    h.a(this.l);
                    this.x = f.a(this.d, this.b);
                }
                this.f = 3;
                d();
            }
        }
    }

    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                g();
                break;
            case 2:
                f();
                break;
            default:
                return super.onContextItemSelected(item);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(WiGame.i);
        if (WiGame.j) {
            getWindow().addFlags(1024);
        }
        requestWindowFeature(1);
        setContentView(t.e("wy_activity_send_challenge"));
        a();
        b();
    }

    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        contextMenu.add(0, 1, 0, t.f("wy_context_item_select_all"));
        contextMenu.add(0, 2, 0, t.f("wy_context_item_deselect_all"));
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        setListAdapter(null);
        unregisterReceiver(this.y);
        d.a().b(this);
        c();
        WiGame.x();
        super.onDestroy();
    }

    public void onFocusChange(View v2, boolean hasFocus) {
        if (hasFocus) {
            this.o.selectAll();
        }
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l2, View v2, int position, long id) {
        switch (a(position)) {
            case 1:
            case 3:
            case 4:
                ag u2 = (ag) b(position);
                if (this.w.containsKey(u2.getId())) {
                    this.w.remove(u2.getId());
                } else {
                    this.w.put(u2.getId(), u2);
                }
                d();
                return;
            case 2:
            default:
                return;
            case 5:
                e();
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        this.k.setVisibility(0);
        h.a(this.l);
        f.h(this.a);
    }
}
