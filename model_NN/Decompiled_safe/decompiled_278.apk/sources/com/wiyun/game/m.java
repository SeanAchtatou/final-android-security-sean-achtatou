package com.wiyun.game;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

class m implements LocationListener {
    m() {
    }

    public void onLocationChanged(Location location) {
        WiGame.N = location;
        if (location.hasAccuracy() && location.getAccuracy() < 1000.0f && WiGame.R != null) {
            ((LocationManager) WiGame.R.getSystemService("location")).removeUpdates(this);
        }
        WiGame.ae();
    }

    public void onProviderDisabled(String provider) {
    }

    public void onProviderEnabled(String provider) {
    }

    public void onStatusChanged(String provider, int status, Bundle extras) {
    }
}
