package com.wiyun.game;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.widget.Toast;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;
import com.google.android.maps.Projection;
import com.saubcy.games.maze.market.MazeGame;
import com.wiyun.game.model.a.ag;
import com.wiyun.game.model.a.p;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class UserMap extends MapActivity implements com.wiyun.game.b.b {
    /* access modifiers changed from: private */
    public MapView a;
    /* access modifiers changed from: private */
    public View b;
    private int c;
    private int d;
    private double e;
    private double f;
    private String g;
    private String h;
    private String i;
    private String j;
    private long k;
    /* access modifiers changed from: private */
    public Geocoder l;
    private List<Overlay> m;
    /* access modifiers changed from: private */
    public b n;
    /* access modifiers changed from: private */
    public d o;
    private BroadcastReceiver p = new r(this);

    private final class a extends f {
        private p c;

        public a(GeoPoint point, String title, p s) {
            super(point, title);
            this.c = s;
        }

        /* access modifiers changed from: package-private */
        public p a() {
            return this.c;
        }
    }

    private final class b extends ItemizedOverlay<OverlayItem> {
        private ArrayList<OverlayItem> b = new ArrayList<>();
        private Paint c = new Paint(1);
        private boolean d;

        public b(Drawable defaultMarker) {
            super(boundCenterBottom(defaultMarker));
            this.c.setColor(-65536);
            this.c.setTextSize(TypedValue.applyDimension(2, 14.0f, UserMap.this.getResources().getDisplayMetrics()));
            populate();
        }

        /* access modifiers changed from: package-private */
        public ArrayList<OverlayItem> a() {
            return this.b;
        }

        /* access modifiers changed from: package-private */
        public void a(OverlayItem overlay) {
            this.b.add(overlay);
            populate();
        }

        /* access modifiers changed from: package-private */
        public void b() {
            this.d = true;
        }

        /* access modifiers changed from: package-private */
        public void c() {
            this.d = false;
        }

        /* access modifiers changed from: protected */
        public OverlayItem createItem(int i) {
            return this.b.get(i);
        }

        public void draw(Canvas canvas, MapView mapView, boolean z) {
            Bitmap b2;
            if (!this.d) {
                Projection projection = mapView.getProjection();
                Point point = new Point();
                Paint.FontMetrics fontMetrics = this.c.getFontMetrics();
                Iterator<OverlayItem> it = this.b.iterator();
                while (it.hasNext()) {
                    c cVar = (OverlayItem) it.next();
                    projection.toPixels(cVar.getPoint(), point);
                    if (cVar instanceof c) {
                        Bitmap b3 = cVar.b();
                        if (b3 != null) {
                            canvas.drawBitmap(b3, (float) (point.x - (b3.getWidth() / 2)), (float) (point.y - b3.getHeight()), this.c);
                        }
                    } else if (cVar instanceof d) {
                        d dVar = (d) cVar;
                        this.c.setColor(-1);
                        Drawable drawable = UserMap.this.getResources().getDrawable(t.c("wy_location_marker"));
                        String title = cVar.getTitle();
                        float measureText = this.c.measureText(title);
                        drawable.setBounds((int) ((((float) point.x) - (measureText / 2.0f)) - 15.0f), (point.y - drawable.getIntrinsicHeight()) + dVar.a(), (int) (((float) point.x) + (measureText / 2.0f) + 15.0f), point.y + dVar.a());
                        drawable.draw(canvas);
                        canvas.drawText(title, ((float) point.x) - (measureText / 2.0f), (((float) (point.y - drawable.getIntrinsicHeight())) - fontMetrics.ascent) + 10.0f + ((float) dVar.a()), this.c);
                    } else if ((cVar instanceof a) && (b2 = ((a) cVar).b()) != null) {
                        canvas.drawBitmap(b2, (float) (point.x - (b2.getWidth() / 2)), (float) (point.y - b2.getHeight()), this.c);
                    }
                }
            }
        }

        /* access modifiers changed from: protected */
        public boolean onTap(int index) {
            c item = getItem(index);
            if (item instanceof c) {
                c uoi = item;
                Bitmap portrait = uoi.b();
                int offset = portrait == null ? 0 : -portrait.getHeight();
                if (UserMap.this.o != null) {
                    this.b.remove(UserMap.this.o);
                    UserMap.this.o = (d) null;
                }
                UserMap.this.o = new d(uoi.getPoint(), uoi.getTitle(), offset);
                a(UserMap.this.o);
            } else if (item instanceof a) {
                a soi = item;
                Bitmap portrait2 = soi.b();
                int offset2 = portrait2 == null ? 0 : -portrait2.getHeight();
                if (UserMap.this.o != null) {
                    this.b.remove(UserMap.this.o);
                    UserMap.this.o = (d) null;
                }
                UserMap.this.o = new d(soi.getPoint(), String.format(t.h("wy_name_x_rank_y"), soi.getTitle(), Integer.valueOf(soi.a().f())), offset2);
                a(UserMap.this.o);
            }
            return UserMap.super.onTap(index);
        }

        public int size() {
            return this.b.size();
        }
    }

    private final class c extends f {
        private ag c;

        public c(GeoPoint point, String title, ag u) {
            super(point, title);
            this.c = u;
        }
    }

    private final class d extends OverlayItem {
        private int b;

        public d(GeoPoint point, String title, int offset) {
            super(point, title, "");
            this.b = offset;
        }

        /* access modifiers changed from: package-private */
        public int a() {
            return this.b;
        }
    }

    private final class e extends Thread {
        private double b;
        private double c;

        e(double lat, double lon) {
            this.b = lat;
            this.c = lon;
        }

        public void run() {
            boolean success = false;
            try {
                List addresses = UserMap.this.l.getFromLocation(this.b, this.c, 1);
                if (addresses != null && !addresses.isEmpty()) {
                    Address addr = addresses.get(0);
                    StringBuffer buf = new StringBuffer();
                    int i = 0;
                    while (true) {
                        String part = addr.getAddressLine(i);
                        if (part == null) {
                            break;
                        }
                        buf.append(part);
                        i++;
                    }
                    UserMap.this.n.a(new d(new GeoPoint((int) (this.b * 1000000.0d), (int) (this.c * 1000000.0d)), buf.toString(), 0));
                    success = true;
                }
            } catch (Exception e) {
            }
            if (!success) {
                UserMap.this.n.a(new d(new GeoPoint((int) (this.b * 1000000.0d), (int) (this.c * 1000000.0d)), String.format("%f,%f", Float.valueOf((float) this.b), Float.valueOf((float) this.c)), 0));
            }
            UserMap.this.runOnUiThread(new Runnable() {
                public void run() {
                    UserMap.this.b.setVisibility(4);
                    UserMap.this.a.invalidate();
                }
            });
        }
    }

    private class f extends OverlayItem {
        private Bitmap a;
        private String c;

        public f(GeoPoint point, String title) {
            super(point, title, "");
        }

        /* access modifiers changed from: package-private */
        public void a(Bitmap b2) {
            if (this.a != null && !this.a.isRecycled()) {
                this.a.recycle();
            }
            this.a = b2;
        }

        /* access modifiers changed from: package-private */
        public void a(String id) {
            this.c = id;
        }

        /* access modifiers changed from: package-private */
        public Bitmap b() {
            return this.a;
        }

        /* access modifiers changed from: package-private */
        public String c() {
            return this.c;
        }
    }

    private void a() {
        Intent intent = getIntent();
        this.d = intent.getIntExtra("mode", 0);
        switch (this.d) {
            case 1:
                this.e = intent.getDoubleExtra("latitude", 0.0d);
                this.f = intent.getDoubleExtra("longitude", 0.0d);
                break;
            case 2:
                this.i = intent.getStringExtra("user_id");
                break;
            case 3:
                break;
            case 4:
                this.g = intent.getStringExtra("leaderboard_id");
                this.h = intent.getStringExtra("timespan");
                this.j = intent.getStringExtra("app_id");
                break;
            default:
                finish();
                break;
        }
        registerReceiver(this.p, new IntentFilter("com.wiyun.game.IMAGE_DOWNLOADED"));
        com.wiyun.game.b.d.a().a(this);
    }

    /* access modifiers changed from: private */
    public void a(Bitmap bitmap, String id) {
        if (bitmap != null) {
            Iterator<OverlayItem> it = this.n.a().iterator();
            while (it.hasNext()) {
                f fVar = (OverlayItem) it.next();
                if (fVar instanceof f) {
                    f pi = fVar;
                    if (id.equals(pi.c())) {
                        pi.a(bitmap);
                        return;
                    }
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wiyun.game.h.a(java.util.Map<java.lang.String, android.graphics.Bitmap>, boolean, java.lang.String, java.lang.String, boolean):android.graphics.Bitmap
     arg types: [?[OBJECT, ARRAY], int, java.lang.String, java.lang.String, boolean]
     candidates:
      com.wiyun.game.h.a(int, int, int, int, android.net.Uri):android.content.Intent
      com.wiyun.game.h.a(android.content.Context, java.lang.String, int, android.view.View$OnClickListener, int[]):void
      com.wiyun.game.h.a(java.util.Map<java.lang.String, android.graphics.Bitmap>, boolean, java.lang.String, java.lang.String, boolean):android.graphics.Bitmap */
    private void a(ag agVar) {
        c cVar = new c(new GeoPoint((int) (agVar.getLatitude() * 1000000.0d), (int) (agVar.getLongitude() * 1000000.0d)), agVar.getName(), agVar);
        Bitmap a2 = h.a((Map<String, Bitmap>) null, false, h.b("p_", agVar.getId()), agVar.getAvatarUrl(), agVar.isFemale());
        if (a2 != null) {
            cVar.a(a2);
        }
        cVar.a(h.b("p_", agVar.getId()));
        this.n.a(cVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wiyun.game.h.a(java.util.Map<java.lang.String, android.graphics.Bitmap>, boolean, java.lang.String, java.lang.String, boolean):android.graphics.Bitmap
     arg types: [?[OBJECT, ARRAY], int, java.lang.String, java.lang.String, boolean]
     candidates:
      com.wiyun.game.h.a(int, int, int, int, android.net.Uri):android.content.Intent
      com.wiyun.game.h.a(android.content.Context, java.lang.String, int, android.view.View$OnClickListener, int[]):void
      com.wiyun.game.h.a(java.util.Map<java.lang.String, android.graphics.Bitmap>, boolean, java.lang.String, java.lang.String, boolean):android.graphics.Bitmap */
    private void a(p pVar) {
        a aVar = new a(new GeoPoint((int) (pVar.d() * 1000000.0d), (int) (pVar.e() * 1000000.0d)), pVar.a(), pVar);
        Bitmap a2 = h.a((Map<String, Bitmap>) null, false, h.b("p_", pVar.c()), pVar.b(), pVar.g());
        if (a2 != null) {
            aVar.a(a2);
        }
        aVar.a(h.b("p_", pVar.c()));
        this.n.a(aVar);
    }

    private void b() {
        this.a = findViewById(t.d("wy_mapview"));
        this.b = findViewById(t.d("wy_ll_progress_panel"));
        this.a.setBuiltInZoomControls(true);
        MapController mc = this.a.getController();
        this.c = (this.a.getMaxZoomLevel() * 2) / 3;
        mc.setZoom(this.c);
        this.m = this.a.getOverlays();
        this.n = new b(getResources().getDrawable(t.c("wy_screen_indicator_on")));
        this.m.add(this.n);
    }

    private void c() {
        Iterator<OverlayItem> it = this.n.a().iterator();
        while (it.hasNext()) {
            f fVar = (OverlayItem) it.next();
            if (fVar instanceof f) {
                f pi = fVar;
                Bitmap portrait = pi.b();
                if (portrait != null && !portrait.isRecycled()) {
                    portrait.recycle();
                }
                pi.a((Bitmap) null);
            }
        }
    }

    public void b(final com.wiyun.game.b.e e2) {
        switch (e2.a) {
            case MazeGame.WIDTHREDUCTION:
                if (this.k != e2.j) {
                    return;
                }
                if (e2.c) {
                    runOnUiThread(new Runnable() {
                        /* JADX WARN: Type inference failed for: r1v0, types: [android.content.Context, com.wiyun.game.UserMap] */
                        public void run() {
                            Toast.makeText((Context) UserMap.this, (String) e2.e, 0).show();
                            UserMap.this.finish();
                        }
                    });
                    return;
                }
                this.n.b();
                Map userIds = new HashMap();
                for (p s : (List) e2.e) {
                    if (!userIds.containsKey(s.c())) {
                        a(s);
                        userIds.put(s.c(), s.c());
                    }
                }
                this.n.c();
                runOnUiThread(new Runnable() {
                    public void run() {
                        UserMap.this.b.setVisibility(4);
                        UserMap.this.a.invalidate();
                    }
                });
                return;
            case 14:
                if (this.k != e2.j) {
                    return;
                }
                if (e2.c) {
                    runOnUiThread(new Runnable() {
                        /* JADX WARN: Type inference failed for: r1v0, types: [android.content.Context, com.wiyun.game.UserMap] */
                        public void run() {
                            Toast.makeText((Context) UserMap.this, (String) e2.e, 0).show();
                            UserMap.this.finish();
                        }
                    });
                    return;
                }
                ag u = (ag) e2.e;
                a(u);
                MapController mc = this.a.getController();
                mc.setZoom(this.a.getMaxZoomLevel());
                mc.animateTo(new GeoPoint((int) (u.getLatitude() * 1000000.0d), (int) (u.getLongitude() * 1000000.0d)));
                runOnUiThread(new Runnable() {
                    public void run() {
                        UserMap.this.b.setVisibility(4);
                    }
                });
                return;
            case 21:
                if (this.k != e2.j) {
                    return;
                }
                if (e2.c) {
                    runOnUiThread(new Runnable() {
                        /* JADX WARN: Type inference failed for: r1v0, types: [android.content.Context, com.wiyun.game.UserMap] */
                        public void run() {
                            Toast.makeText((Context) UserMap.this, (String) e2.e, 0).show();
                            UserMap.this.finish();
                        }
                    });
                    return;
                }
                this.n.b();
                for (ag u2 : (List) e2.e) {
                    a(u2);
                }
                this.n.c();
                runOnUiThread(new Runnable() {
                    public void run() {
                        UserMap.this.b.setVisibility(4);
                        UserMap.this.a.invalidate();
                    }
                });
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public boolean isRouteDisplayed() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle icicle) {
        UserMap.super.onCreate(icicle);
        requestWindowFeature(1);
        if (WiGame.j) {
            getWindow().addFlags(1024);
        }
        setContentView(t.e("wy_activity_usermap"));
        a();
        b();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        unregisterReceiver(this.p);
        this.m.remove(this.n);
        c();
        com.wiyun.game.b.d.a().b(this);
        UserMap.super.onDestroy();
    }

    /* JADX WARN: Type inference failed for: r12v0, types: [android.content.Context, com.wiyun.game.UserMap, com.google.android.maps.MapActivity] */
    /* access modifiers changed from: protected */
    public void onPostCreate(Bundle savedInstanceState) {
        UserMap.super.onPostCreate(savedInstanceState);
        MapController mc = this.a.getController();
        switch (this.d) {
            case 1:
                this.b.setVisibility(0);
                this.l = new Geocoder(this);
                new e(this.e, this.f).start();
                mc.animateTo(new GeoPoint((int) (this.e * 1000000.0d), (int) (this.f * 1000000.0d)));
                return;
            case 2:
                this.b.setVisibility(0);
                this.k = f.d(this.i);
                return;
            case 3:
                this.b.setVisibility(0);
                this.k = f.a(WiGame.getLatitude(), WiGame.getLongitude(), 0, 25);
                mc.animateTo(new GeoPoint((int) (WiGame.getLatitude() * 1000000.0d), (int) (WiGame.getLongitude() * 1000000.0d)));
                return;
            case 4:
                this.b.setVisibility(0);
                this.k = f.a(this.j, this.g, this.h, WiGame.getLatitude(), WiGame.getLongitude(), 0, 25);
                mc.animateTo(new GeoPoint((int) (WiGame.getLatitude() * 1000000.0d), (int) (WiGame.getLongitude() * 1000000.0d)));
                return;
            default:
                return;
        }
    }
}
