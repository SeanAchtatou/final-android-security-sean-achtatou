package com.wiyun.game.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.Scroller;
import com.wiyun.game.t;
import java.util.ArrayList;
import java.util.List;

public class Workspace extends ViewGroup {
    private boolean a;
    private int b;
    private int c;
    private Scroller d;
    private VelocityTracker e;
    private float f;
    private float g;
    private int h;
    private boolean i;
    private boolean j;
    private List<ScreenListener> k;

    public interface ScreenListener {
        void a();

        void a(int i);

        void b(int i);
    }

    public Workspace(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public Workspace(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.a = true;
        this.c = -1;
        this.h = 0;
        f();
    }

    private void f() {
        this.k = new ArrayList();
        this.d = new Scroller(getContext());
        this.b = 0;
        a();
    }

    private void g() {
        if (this.k != null) {
            for (ScreenListener l : this.k) {
                l.a();
            }
        }
    }

    private void h() {
        if (this.k != null) {
            for (ScreenListener l : this.k) {
                l.a(getChildCount());
            }
        }
    }

    private void i() {
        if (this.k != null) {
            for (ScreenListener l : this.k) {
                l.b(this.b);
            }
        }
    }

    private void j() {
        int screenWidth = getWidth();
        b((getScrollX() + (screenWidth / 2)) / screenWidth);
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public CellLayout a(int index) {
        if (index < 0 || index >= b()) {
            return null;
        }
        return (CellLayout) getChildAt(index);
    }

    public void a() {
        addView((CellLayout) LayoutInflater.from(getContext()).inflate(t.e("wy_view_workspace_screen"), (ViewGroup) null));
        h();
    }

    public void a(int whichScreen, boolean animate) {
        g();
        int whichScreen2 = Math.max(0, Math.min(whichScreen, b() - 1));
        boolean changingScreens = whichScreen2 != this.b;
        View focusedChild = getFocusedChild();
        if (focusedChild != null && changingScreens && focusedChild == c()) {
            focusedChild.clearFocus();
        }
        if (this.a) {
            this.b = whichScreen2;
            i();
            return;
        }
        this.c = whichScreen2;
        int newX = whichScreen2 * getWidth();
        if (animate) {
            int delta = newX - getScrollX();
            this.d.startScroll(getScrollX(), 0, delta, 0, Math.abs(delta) * 2);
        } else {
            scrollTo(newX, 0);
        }
        invalidate();
    }

    public boolean a(boolean loop) {
        if (this.b > 0) {
            b(this.b - 1);
            return true;
        } else if (!loop) {
            return false;
        } else {
            b(getChildCount() - 1);
            return true;
        }
    }

    public void addFocusables(ArrayList<View> views, int direction) {
        CellLayout screen = c();
        if (screen != null) {
            screen.addFocusables(views, direction);
            if (direction == 17) {
                if (this.b > 0) {
                    a(this.b - 1).addFocusables(views, direction);
                }
            } else if (direction == 66 && this.b < b() - 1) {
                a(this.b + 1).addFocusables(views, direction);
            }
        }
    }

    public int b() {
        return getChildCount();
    }

    public void b(int whichScreen) {
        a(whichScreen, true);
    }

    public boolean b(boolean loop) {
        if (this.b < getChildCount() - 1) {
            b(this.b + 1);
            return true;
        } else if (!loop) {
            return false;
        } else {
            b(0);
            return true;
        }
    }

    public CellLayout c() {
        return a(this.b);
    }

    public void computeScroll() {
        if (this.d.computeScrollOffset()) {
            scrollTo(this.d.getCurrX(), this.d.getCurrY());
            postInvalidate();
        } else if (this.c != -1) {
            this.b = Math.max(0, Math.min(this.c, b() - 1));
            this.c = -1;
            i();
        }
    }

    public int d() {
        return this.b;
    }

    /* access modifiers changed from: protected */
    public void dispatchDraw(Canvas canvas) {
        int count = b();
        if (this.h != 1 && this.c == -1) {
            drawChild(canvas, c(), getDrawingTime());
            return;
        }
        long drawingTime = getDrawingTime();
        if (this.c < 0 || this.c >= getChildCount() || Math.abs(this.b - this.c) != 1) {
            for (int i2 = 0; i2 < count; i2++) {
                drawChild(canvas, a(i2), drawingTime);
            }
            return;
        }
        drawChild(canvas, c(), drawingTime);
        drawChild(canvas, a(this.c), drawingTime);
    }

    public boolean dispatchUnhandledMove(View focused, int direction) {
        if (direction == 17 || direction == 33 || direction == 2) {
            if (d() > 0) {
                b(d() - 1);
                return true;
            }
        } else if ((direction == 66 || direction == 130 || direction == 1) && d() < b() - 1) {
            b(d() + 1);
            return true;
        }
        return super.dispatchUnhandledMove(focused, direction);
    }

    public void e() {
        this.k.clear();
    }

    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (this.j) {
            return true;
        }
        int action = ev.getAction();
        if (action == 2 && this.h != 0) {
            return true;
        }
        float x = ev.getX();
        float y = ev.getY();
        switch (action) {
            case 0:
                this.f = x;
                this.g = y;
                this.i = true;
                this.h = this.d.isFinished() ? 0 : 1;
                break;
            case 1:
            case 3:
                this.h = 0;
                break;
            case 2:
                int xDiff = (int) Math.abs(x - this.f);
                int yDiff = (int) Math.abs(y - this.g);
                int touchSlop = ViewConfiguration.getTouchSlop();
                boolean xMoved = xDiff > touchSlop;
                boolean yMoved = yDiff > touchSlop;
                if (xMoved || yMoved) {
                    if (xMoved) {
                        this.h = 1;
                    }
                    if (this.i) {
                        this.i = false;
                        c().cancelLongPress();
                        break;
                    }
                }
                break;
        }
        return this.h != 0;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean changed, int left, int top, int right, int bottom) {
        int childStart = 0;
        int count = b();
        for (int i2 = 0; i2 < count; i2++) {
            View child = a(i2);
            if (child.getVisibility() != 8) {
                int childWidth = child.getMeasuredWidth();
                child.layout(childStart, 0, childStart + childWidth, child.getMeasuredHeight());
                childStart += childWidth;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int width = View.MeasureSpec.getSize(widthMeasureSpec);
        if (View.MeasureSpec.getMode(widthMeasureSpec) != 1073741824) {
            throw new IllegalStateException("Workspace can only be used in EXACTLY mode.");
        }
        int height = View.MeasureSpec.getSize(heightMeasureSpec);
        int heightMode = View.MeasureSpec.getMode(heightMeasureSpec);
        if (heightMode != 1073741824) {
            throw new IllegalStateException("Workspace can only be used in EXACTLY mode.");
        }
        int count = b();
        for (int i2 = 0; i2 < count; i2++) {
            a(i2).measure(widthMeasureSpec, View.MeasureSpec.makeMeasureSpec(height, heightMode));
        }
        if (this.a) {
            scrollTo(this.b * width, 0);
            this.a = false;
        }
    }

    /* access modifiers changed from: protected */
    public boolean onRequestFocusInDescendants(int direction, Rect previouslyFocusedRect) {
        a(this.c != -1 ? this.c : this.b).requestFocus(direction, previouslyFocusedRect);
        return false;
    }

    public boolean onTouchEvent(MotionEvent ev) {
        int availableToScroll;
        if (this.j) {
            return true;
        }
        if (this.e == null) {
            this.e = VelocityTracker.obtain();
        }
        this.e.addMovement(ev);
        int action = ev.getAction();
        float x = ev.getX();
        switch (action) {
            case 0:
                if (!this.d.isFinished()) {
                    this.d.abortAnimation();
                }
                this.f = x;
                break;
            case 1:
                if (this.h == 1) {
                    VelocityTracker velocityTracker = this.e;
                    velocityTracker.computeCurrentVelocity(1000);
                    int velocity = (int) velocityTracker.getXVelocity();
                    if (velocity > 1000 && this.b > 0) {
                        b(this.b - 1);
                    } else if (velocity >= -1000 || this.b >= b() - 1) {
                        j();
                    } else {
                        b(this.b + 1);
                    }
                    if (this.e != null) {
                        this.e.recycle();
                        this.e = null;
                    }
                }
                this.h = 0;
                break;
            case 2:
                if (this.h == 1) {
                    int deltaX = (int) (this.f - x);
                    this.f = x;
                    if (deltaX >= 0) {
                        if (deltaX > 0 && (availableToScroll = (a(b() - 1).getRight() - getScrollX()) - getWidth()) > 0) {
                            scrollBy(Math.min(availableToScroll, deltaX), 0);
                            break;
                        }
                    } else if (getScrollX() > 0) {
                        scrollBy(Math.max(-getScrollX(), deltaX), 0);
                        break;
                    }
                }
                break;
            case 3:
                this.h = 0;
                break;
        }
        return true;
    }

    public boolean requestChildRectangleOnScreen(View child, Rect rectangle, boolean immediate) {
        int screen = indexOfChild(child);
        if (screen == this.b && this.d.isFinished()) {
            return false;
        }
        b(screen);
        return true;
    }
}
