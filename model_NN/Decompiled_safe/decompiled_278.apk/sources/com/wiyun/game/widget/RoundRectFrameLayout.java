package com.wiyun.game.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import com.wiyun.game.t;

public class RoundRectFrameLayout extends FrameLayout {
    private RectF a;
    private float b;
    private Path c;
    private int d;
    private int e;
    private Paint f;
    private boolean g;

    public RoundRectFrameLayout(Context context) {
        this(context, null);
    }

    public RoundRectFrameLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        a();
    }

    private void a() {
        this.a = new RectF();
        this.b = t.j("wy_roundrect_radius");
        this.e = t.i("wy_roundrect_container_inner_border");
        this.d = t.i("wy_roundrect_container_outter_border");
        this.g = true;
        this.f = new Paint();
        this.f.setStrokeWidth(1.0f);
        this.f.setStyle(Paint.Style.STROKE);
    }

    /* access modifiers changed from: protected */
    public void dispatchDraw(Canvas canvas) {
        this.a.set(0.0f, 0.0f, (float) getWidth(), (float) getHeight());
        this.a.left += (float) getPaddingLeft();
        this.a.top += (float) getPaddingTop();
        this.a.right -= (float) getPaddingRight();
        this.a.bottom -= (float) getPaddingBottom();
        if (this.c == null) {
            this.a.right += 1.0f;
            this.a.bottom += 1.0f;
            this.c = new Path();
            this.c.addRoundRect(this.a, this.b, this.b, Path.Direction.CCW);
            this.a.right -= 1.0f;
            this.a.bottom -= 1.0f;
        }
        canvas.save();
        canvas.clipPath(this.c);
        super.dispatchDraw(canvas);
        if (this.g) {
            this.f.setColor(this.d);
            canvas.drawRoundRect(this.a, this.b, this.b, this.f);
            this.a.inset(1.0f, 1.0f);
            this.f.setColor(this.e);
            canvas.drawRoundRect(this.a, this.b - 1.0f, this.b - 1.0f, this.f);
        }
        canvas.restore();
    }

    public void draw(Canvas canvas) {
        this.a.set(0.0f, 0.0f, (float) getWidth(), (float) getHeight());
        this.a.left += (float) getPaddingLeft();
        this.a.top += (float) getPaddingTop();
        this.a.right -= (float) getPaddingRight();
        this.a.bottom -= (float) getPaddingBottom();
        if (this.c == null) {
            this.a.right += 1.0f;
            this.a.bottom += 1.0f;
            this.c = new Path();
            this.c.addRoundRect(this.a, this.b, this.b, Path.Direction.CCW);
            this.a.right -= 1.0f;
            this.a.bottom -= 1.0f;
        }
        canvas.save();
        canvas.clipPath(this.c);
        super.draw(canvas);
        if (this.g) {
            this.f.setColor(this.d);
            canvas.drawRoundRect(this.a, this.b, this.b, this.f);
            this.a.inset(1.0f, 1.0f);
            this.f.setColor(this.e);
            canvas.drawRoundRect(this.a, this.b - 1.0f, this.b - 1.0f, this.f);
        }
        canvas.restore();
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        this.c = null;
    }
}
