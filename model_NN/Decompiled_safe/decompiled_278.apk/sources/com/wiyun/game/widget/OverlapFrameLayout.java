package com.wiyun.game.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import com.wiyun.game.t;

public class OverlapFrameLayout extends FrameLayout {
    public OverlapFrameLayout(Context context) {
        super(context);
    }

    public OverlapFrameLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public OverlapFrameLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        int count = getChildCount();
        for (int i = 0; i < count; i++) {
            View child = getChildAt(i);
            if (child.getId() == t.d("wy_ll_progress_panel")) {
                child.layout(left, top, right, bottom);
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(getMeasuredWidth() + 14, getMeasuredHeight() + 8);
    }
}
