package com.waps;

import android.os.AsyncTask;

class g extends AsyncTask {
    final /* synthetic */ AppConnect a;

    private g(AppConnect appConnect) {
        this.a = appConnect;
    }

    /* synthetic */ g(AppConnect appConnect, d dVar) {
        this(appConnect);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Boolean doInBackground(Void... voidArr) {
        boolean z = false;
        String a2 = AppConnect.v.a("http://app.waps.cn/action/" + AppConnect.ab, this.a.L + "&" + AppConnect.ac + "=" + this.a.N);
        if (a2 != null) {
            z = this.a.handleConnectResponse(a2);
        }
        return Boolean.valueOf(z);
    }
}
