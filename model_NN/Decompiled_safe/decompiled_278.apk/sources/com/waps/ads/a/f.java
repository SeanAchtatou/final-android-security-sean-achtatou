package com.waps.ads.a;

import android.util.Log;
import com.waps.ads.AdGroupLayout;
import com.waps.ads.b.c;

public class f extends a {
    public f(AdGroupLayout adGroupLayout, c cVar) {
        super(adGroupLayout, cVar);
    }

    public void handle() {
        Log.d("AdGroup_SDK", "Generic notification request initiated");
        AdGroupLayout adGroupLayout = (AdGroupLayout) this.c.get();
        if (adGroupLayout != null) {
            if (adGroupLayout.i != null) {
                adGroupLayout.i.a();
            } else {
                Log.w("AdGroup_SDK", "Generic notification sent, but no interface is listening");
            }
            adGroupLayout.j.resetRollover();
            adGroupLayout.rotateThreadedDelayed();
        }
    }
}
