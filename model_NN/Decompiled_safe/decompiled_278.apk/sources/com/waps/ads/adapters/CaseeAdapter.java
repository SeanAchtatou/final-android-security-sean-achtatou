package com.waps.ads.adapters;

import android.graphics.Color;
import android.util.AttributeSet;
import com.casee.adsdk.CaseeAdView;
import com.waps.ads.AdGroupLayout;
import com.waps.ads.AdGroupTargeting;
import com.waps.ads.a.a;
import com.waps.ads.b.b;
import com.waps.ads.b.c;

public class CaseeAdapter extends a implements CaseeAdView.AdListener {
    public CaseeAdapter(AdGroupLayout adGroupLayout, c cVar) {
        super(adGroupLayout, cVar);
    }

    public void handle() {
        AdGroupLayout adGroupLayout = (AdGroupLayout) this.c.get();
        if (adGroupLayout != null) {
            b bVar = adGroupLayout.d;
            new CaseeAdView(adGroupLayout.getContext(), (AttributeSet) null, 0, this.d.e, AdGroupTargeting.getTestMode(), bVar.i * 1000, Color.rgb(bVar.e, bVar.f, bVar.g), Color.rgb(bVar.a, bVar.b, bVar.c));
        }
    }

    public void onFailedToReceiveAd(CaseeAdView caseeAdView) {
    }

    public void onFailedToReceiveRefreshAd(CaseeAdView caseeAdView) {
    }

    public void onReceiveAd(CaseeAdView caseeAdView) {
    }

    public void onReceiveRefreshAd(CaseeAdView caseeAdView) {
    }
}
