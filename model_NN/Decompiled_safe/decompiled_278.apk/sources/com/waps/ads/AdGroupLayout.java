package com.waps.ads;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import com.waps.AppConnect;
import com.waps.ads.b.a;
import com.waps.ads.b.b;
import com.waps.ads.b.c;
import java.lang.ref.WeakReference;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class AdGroupLayout extends RelativeLayout {
    public static String k = "";
    public static String l = "";
    /* access modifiers changed from: private */
    public static Activity r;
    /* access modifiers changed from: private */
    public static int s;
    public WeakReference a;
    public final Handler b = new Handler();
    public final ScheduledExecutorService c = Executors.newScheduledThreadPool(1);
    public b d;
    public a e;
    public WeakReference f;
    public c g;
    public c h;
    public a i;
    public AdGroupManager j;
    private String m;
    /* access modifiers changed from: private */
    public boolean n;
    /* access modifiers changed from: private */
    public boolean o;
    private int p;
    private String q = "";
    private int t;

    public AdGroupLayout(Activity activity) {
        super(activity);
        init(activity, getAdGroupKey(activity));
        r = activity;
    }

    public AdGroupLayout(Activity activity, String str) {
        super(activity);
        init(activity, str);
    }

    public AdGroupLayout(Activity activity, int[] iArr) {
        super(activity);
        init(activity, getAdGroupKey(activity));
        r = activity;
    }

    public AdGroupLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init((Activity) context, getAdGroupKey(context));
    }

    private void countClick() {
        if (this.g != null) {
            this.q = "nid=" + this.g.a + "&";
            this.q += new AppConnect().getParams((Context) this.a.get());
            StringBuilder append = new StringBuilder().append(this.q);
            String str = "&ad_type=" + this.g.b;
            l = str;
            this.q = append.append(str).toString();
            String str2 = "http://ads.waps.cn/action/adgroup/ad_click?" + this.q;
            this.c.schedule(new d(str2), 0, TimeUnit.SECONDS);
            Log.i("AdGroup", "countClick: " + str2);
        }
    }

    private void countImpression() {
        Log.i("AdGroup", "activeRation: " + this.g);
        if (this.g != null) {
            this.q = "nid=" + this.g.a + "&";
            this.q += new AppConnect().getParams((Context) this.a.get());
            StringBuilder append = new StringBuilder().append(this.q);
            String str = "&ad_type=" + this.g.b;
            l = str;
            this.q = append.append(str).toString();
            String str2 = "http://ads.waps.cn/action/adgroup/ad_impression?" + this.q;
            Log.i("AdGroup", "countImpression: " + str2);
            this.c.schedule(new d(str2), 0, TimeUnit.SECONDS);
        }
    }

    /* access modifiers changed from: private */
    public void handleAd() {
        if (this.h == null) {
            Log.e("AdGroup_SDK", "nextRation is null!");
            rotateThreadedDelayed();
            return;
        }
        Log.d("AdGroup_SDK", String.format("Showing ad:\n\tnid: %s\n\tname: %s\n\ttype: %d\n\tkey: %s\n\tkey2: %s", this.h.a, this.h.c, Integer.valueOf(this.h.b), this.h.e, this.h.f));
        try {
            com.waps.ads.a.a.handle(this, this.h);
        } catch (Throwable th) {
            Log.w("AdGroup_SDK", "Caught an exception in adapter:", th);
            rollover();
        }
    }

    /* access modifiers changed from: private */
    public void rotateAd() {
        if (!this.n) {
            this.o = false;
            return;
        }
        Log.i("AdGroup_SDK", "Rotating Ad");
        this.h = this.j.getRation();
        this.b.post(new b(this));
    }

    /* access modifiers changed from: protected */
    public String getAdGroupKey(Context context) {
        String packageName = context.getPackageName();
        String name = context.getClass().getName();
        PackageManager packageManager = context.getPackageManager();
        try {
            Bundle bundle = packageManager.getActivityInfo(new ComponentName(packageName, name), 128).metaData;
            if (bundle != null) {
                return bundle.getString("WAPS_ID");
            }
            try {
                Bundle bundle2 = packageManager.getApplicationInfo(packageName, 128).metaData;
                if (bundle2 != null) {
                    return bundle2.getString("WAPS_ID");
                }
                return null;
            } catch (PackageManager.NameNotFoundException e2) {
                return null;
            }
        } catch (PackageManager.NameNotFoundException e3) {
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void init(Activity activity, String str) {
        this.a = new WeakReference(activity);
        this.f = new WeakReference(this);
        this.m = str;
        this.n = true;
        this.o = true;
        this.c.schedule(new c(this, str), 0, TimeUnit.SECONDS);
        setHorizontalScrollBarEnabled(false);
        setVerticalScrollBarEnabled(false);
        this.p = 0;
        this.t = 0;
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                Log.d("AdGroup_SDK", "Intercepted ACTION_DOWN event");
                if (this.g != null) {
                    countClick();
                    if (this.g.b == 9) {
                        if (this.e != null && this.e.b != null) {
                            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(this.e.b));
                            intent.addFlags(268435456);
                            try {
                                if (this.a != null) {
                                    Activity activity = (Activity) this.a.get();
                                    if (activity != null) {
                                        activity.startActivity(intent);
                                        break;
                                    } else {
                                        return false;
                                    }
                                } else {
                                    return false;
                                }
                            } catch (Exception e2) {
                                Log.w("AdGroup_SDK", "Could not handle click to " + this.e.b, e2);
                                break;
                            }
                        } else {
                            Log.w("AdGroup_SDK", "In onInterceptTouchEvent(), but custom or custom.link is null");
                            break;
                        }
                    }
                }
                break;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        super.onMeasure((this.p <= 0 || View.MeasureSpec.getSize(i2) <= this.p) ? i2 : View.MeasureSpec.makeMeasureSpec(this.p, Integer.MIN_VALUE), (this.t <= 0 || View.MeasureSpec.getSize(i3) <= this.t) ? i3 : View.MeasureSpec.makeMeasureSpec(this.t, Integer.MIN_VALUE));
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int i2) {
        if (i2 == 0) {
            this.n = true;
            if (!this.o) {
                this.o = true;
                if (this.d != null) {
                    rotateThreadedNow();
                } else {
                    this.c.schedule(new c(this, this.m), 0, TimeUnit.SECONDS);
                }
            }
        } else {
            this.n = false;
        }
    }

    public void pushSubView(ViewGroup viewGroup) {
        RelativeLayout relativeLayout = (RelativeLayout) this.f.get();
        if (relativeLayout != null) {
            relativeLayout.removeAllViews();
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams.addRule(13);
            relativeLayout.addView(viewGroup, layoutParams);
            Log.d("AdGroup_SDK", "Added subview");
            this.g = this.h;
            countImpression();
        }
    }

    public void rollover() {
        this.h = this.j.getRollover();
        this.b.post(new b(this));
    }

    public void rotateThreadedDelayed() {
        Log.d("AdGroup_SDK", "Will call rotateAd() in " + this.d.i + " seconds");
        this.c.schedule(new e(this), (long) this.d.i, TimeUnit.SECONDS);
    }

    public void rotateThreadedNow() {
        this.c.schedule(new e(this), 0, TimeUnit.SECONDS);
    }

    public void setAdGroupInterface(a aVar) {
        this.i = aVar;
    }

    public void setMaxHeight(int i2) {
        this.t = i2;
    }

    public void setMaxWidth(int i2) {
        this.p = i2;
    }

    public void updateResultsInUi(View view) {
        RelativeLayout relativeLayout = (RelativeLayout) this.f.get();
        if (relativeLayout != null) {
            relativeLayout.removeAllViews();
            ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
            double d2 = ((double) layoutParams.width) / ((double) layoutParams.height);
            int width = getRootView().getWidth();
            int height = getRootView().getHeight();
            if (height != 0 && width > height) {
                width = height;
            }
            RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(width, (int) (((double) width) / d2));
            layoutParams2.addRule(13);
            relativeLayout.addView(view, layoutParams2);
            Log.d("AdGroup_SDK", "update image view");
            this.g = this.h;
            countImpression();
        }
    }
}
