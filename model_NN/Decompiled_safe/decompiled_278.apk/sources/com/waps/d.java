package com.waps;

import android.content.DialogInterface;
import android.content.Intent;

class d implements DialogInterface.OnClickListener {
    final /* synthetic */ String a;
    final /* synthetic */ AppConnect b;

    d(AppConnect appConnect, String str) {
        this.b = appConnect;
        this.a = str;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        Intent intent = new Intent(this.b.t, OffersWebView.class);
        intent.putExtra("URL", this.a);
        intent.putExtra("isFinshClose", "true");
        intent.putExtra("CLIENT_PACKAGE", this.b.K);
        this.b.t.startActivity(intent);
        boolean unused = AppConnect.S = false;
    }
}
