package com.vodone.caibo.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.vodone.caibo.b.b;
import com.vodone.caibo.b.c;
import com.vodone.caibo.b.j;
import com.vodone.caibo.b.m;
import com.vodone.caibo.b.n;
import com.windo.a.a.a.d;

public final class a {
    private static final String[] a = {"_id", "tweetid", "timeline"};
    private static a b;

    public static int a(Context context, String str) {
        return context.getContentResolver().delete(CaiboProvider.b, "username = '" + str + "'", null);
    }

    public static int a(Context context, String str, int i, String str2, String str3) {
        String str4 = "belongto = '" + str + "'";
        if (i >= 0) {
            str4 = str4 + " AND type = " + i;
        }
        if (str3 != null) {
            str4 = str4 + " AND tag = " + str3;
        }
        if (str2 != null) {
            str4 = str4 + " AND tweetid = " + str2;
        }
        return context.getContentResolver().delete(CaiboProvider.a, str4, null);
    }

    public static Cursor a(Context context, String str, int i) {
        String str2 = "belongto = '" + str + "'";
        Cursor query = context.getContentResolver().query(CaiboProvider.c, null, i >= 0 ? str2 + " AND friendtype = " + i : str2, null, "_id ASC");
        query.moveToFirst();
        return query;
    }

    public static Cursor a(Context context, String str, int i, String str2) {
        return b(context, str, i, null, str2);
    }

    public static Cursor a(Context context, String str, String str2) {
        String str3 = ("belongto = '" + str + "'") + " AND friendtype = " + 7;
        Cursor query = context.getContentResolver().query(CaiboProvider.c, null, str2 != null ? str3 + " AND nickname LIKE '" + str2 + "%'" : str3, null, "_id ASC");
        query.moveToFirst();
        return query;
    }

    public static Cursor a(Context context, String str, String str2, int i) {
        return b(context, str, i, str2, null);
    }

    public static b a(Cursor cursor, b bVar) {
        if (cursor.getPosition() < 0) {
            return null;
        }
        b bVar2 = bVar == null ? new b() : bVar;
        try {
            b.a(new d(cursor.getString(cursor.getColumnIndex("timeline"))), bVar2);
            return bVar2;
        } catch (com.windo.a.a.a.a e) {
            e.printStackTrace();
            return bVar2;
        }
    }

    public static com.vodone.caibo.b.d a(Cursor cursor, com.vodone.caibo.b.d dVar) {
        if (cursor.getPosition() < 0 || cursor.getCount() <= 0) {
            return null;
        }
        com.vodone.caibo.b.d dVar2 = dVar == null ? new com.vodone.caibo.b.d() : dVar;
        try {
            com.vodone.caibo.b.d.a(new d(cursor.getString(cursor.getColumnIndex("timeline"))), dVar2);
            return dVar2;
        } catch (com.windo.a.a.a.a e) {
            e.printStackTrace();
            return dVar2;
        }
    }

    public static j a(Cursor cursor, j jVar) {
        if (cursor.getPosition() < 0) {
            return null;
        }
        j jVar2 = jVar == null ? new j() : jVar;
        try {
            j.a(new d(cursor.getString(cursor.getColumnIndex("timeline"))), jVar2);
            return jVar2;
        } catch (com.windo.a.a.a.a e) {
            e.printStackTrace();
            return jVar2;
        }
    }

    public static m a(Cursor cursor, m mVar) {
        if (cursor.getPosition() < 0) {
            return null;
        }
        m mVar2 = mVar == null ? new m() : mVar;
        mVar2.c = cursor.getString(cursor.getColumnIndex("nickname"));
        mVar2.b = cursor.getString(cursor.getColumnIndex("head"));
        mVar2.a = cursor.getString(cursor.getColumnIndex("userid"));
        mVar2.e = cursor.getInt(cursor.getColumnIndex("usertype"));
        mVar2.f = (byte) cursor.getInt(cursor.getColumnIndex("sex"));
        mVar2.d = cursor.getString(cursor.getColumnIndex("newest"));
        return mVar2;
    }

    public static n a(Cursor cursor, n nVar) {
        if (cursor.getPosition() < 0) {
            return null;
        }
        n nVar2 = nVar == null ? new n() : nVar;
        try {
            n.a(new d(cursor.getString(cursor.getColumnIndex("timeline"))), nVar2);
            return nVar2;
        } catch (com.windo.a.a.a.a e) {
            e.printStackTrace();
            return nVar2;
        }
    }

    public static a a() {
        if (b == null) {
            b = new a();
        }
        return b;
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0097  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00b5  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00b9  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean a(android.content.Context r10, com.vodone.caibo.b.c r11) {
        /*
            r7 = -1
            r6 = 1
            r5 = 0
            java.lang.String r3 = ","
            com.vodone.caibo.database.b r0 = com.vodone.caibo.database.b.a(r10)
            android.database.sqlite.SQLiteDatabase r0 = r0.getWritableDatabase()
            r0.beginTransaction()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r5)
            java.lang.String r2 = "INSERT OR ROLLBACK INTO "
            r1.append(r2)
            java.lang.String r2 = "accounts"
            r1.append(r2)
            java.lang.String r2 = "("
            r1.append(r2)
            java.lang.String r2 = "accountid"
            r1.append(r2)
            java.lang.String r2 = ","
            r1.append(r3)
            java.lang.String r2 = "password"
            r1.append(r2)
            java.lang.String r2 = ","
            r1.append(r3)
            java.lang.String r2 = "username"
            r1.append(r2)
            java.lang.String r2 = ","
            r1.append(r3)
            java.lang.String r2 = "savepwd"
            r1.append(r2)
            java.lang.String r2 = ")"
            r1.append(r2)
            java.lang.String r2 = " VALUES("
            r1.append(r2)
            java.lang.String r2 = "?,?,?,?"
            r1.append(r2)
            java.lang.String r2 = ");"
            r1.append(r2)
            r2 = 0
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x00c0, all -> 0x00b1 }
            android.database.sqlite.SQLiteStatement r1 = r0.compileStatement(r1)     // Catch:{ Exception -> 0x00c0, all -> 0x00b1 }
            r2 = 1
            java.lang.String r3 = r11.a     // Catch:{ Exception -> 0x00a0, all -> 0x00bb }
            r1.bindString(r2, r3)     // Catch:{ Exception -> 0x00a0, all -> 0x00bb }
            java.lang.String r2 = r11.c     // Catch:{ Exception -> 0x00a0, all -> 0x00bb }
            if (r2 != 0) goto L_0x0099
            r2 = 2
            r1.bindNull(r2)     // Catch:{ Exception -> 0x00a0, all -> 0x00bb }
        L_0x0073:
            r2 = 3
            java.lang.String r3 = r11.b     // Catch:{ Exception -> 0x00a0, all -> 0x00bb }
            r1.bindString(r2, r3)     // Catch:{ Exception -> 0x00a0, all -> 0x00bb }
            r2 = 4
            boolean r3 = r11.d     // Catch:{ Exception -> 0x00a0, all -> 0x00bb }
            if (r3 == 0) goto L_0x00ae
            r3 = 0
        L_0x0080:
            r1.bindLong(r2, r3)     // Catch:{ Exception -> 0x00a0, all -> 0x00bb }
            long r2 = r1.executeInsert()     // Catch:{ Exception -> 0x00a0, all -> 0x00bb }
            if (r1 == 0) goto L_0x00c4
            r1.close()
            r1 = r2
        L_0x008d:
            r0.setTransactionSuccessful()
            r0.endTransaction()
            int r0 = (r1 > r7 ? 1 : (r1 == r7 ? 0 : -1))
            if (r0 != 0) goto L_0x00b9
            r0 = r5
        L_0x0098:
            return r0
        L_0x0099:
            r2 = 2
            java.lang.String r3 = r11.c     // Catch:{ Exception -> 0x00a0, all -> 0x00bb }
            r1.bindString(r2, r3)     // Catch:{ Exception -> 0x00a0, all -> 0x00bb }
            goto L_0x0073
        L_0x00a0:
            r2 = move-exception
            r9 = r2
            r2 = r1
            r1 = r9
        L_0x00a4:
            r1.printStackTrace()     // Catch:{ all -> 0x00bd }
            if (r2 == 0) goto L_0x00c2
            r2.close()
            r1 = r7
            goto L_0x008d
        L_0x00ae:
            r3 = 1
            goto L_0x0080
        L_0x00b1:
            r0 = move-exception
            r1 = r2
        L_0x00b3:
            if (r1 == 0) goto L_0x00b8
            r1.close()
        L_0x00b8:
            throw r0
        L_0x00b9:
            r0 = r6
            goto L_0x0098
        L_0x00bb:
            r0 = move-exception
            goto L_0x00b3
        L_0x00bd:
            r0 = move-exception
            r1 = r2
            goto L_0x00b3
        L_0x00c0:
            r1 = move-exception
            goto L_0x00a4
        L_0x00c2:
            r1 = r7
            goto L_0x008d
        L_0x00c4:
            r1 = r2
            goto L_0x008d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vodone.caibo.database.a.a(android.content.Context, com.vodone.caibo.b.c):boolean");
    }

    public static boolean a(Context context, String str, byte b2, j[] jVarArr) {
        SQLiteDatabase writableDatabase = b.a(context).getWritableDatabase();
        writableDatabase.beginTransaction();
        for (int i = 0; i < jVarArr.length; i++) {
            j jVar = jVarArr[i];
            if (jVar == null) {
                writableDatabase.endTransaction();
                return false;
            }
            if (!a(writableDatabase, str, b2, jVar.a, null, jVarArr[i].a())) {
                writableDatabase.endTransaction();
                return false;
            }
        }
        writableDatabase.setTransactionSuccessful();
        writableDatabase.endTransaction();
        return true;
    }

    public static boolean a(Context context, String str, int i, com.vodone.caibo.b.d dVar) {
        String str2 = "tweetid = '" + str + "'";
        if (i >= 0) {
            str2 = str2 + " AND type = " + i;
        }
        ContentValues contentValues = new ContentValues();
        contentValues.clear();
        contentValues.put("timeline", dVar.a());
        return context.getContentResolver().update(CaiboProvider.a, contentValues, str2, null) == 1;
    }

    public static boolean a(Context context, String str, int i, String str2, b[] bVarArr) {
        SQLiteDatabase writableDatabase = b.a(context).getWritableDatabase();
        writableDatabase.beginTransaction();
        for (int i2 = 0; i2 < bVarArr.length; i2++) {
            b bVar = bVarArr[i2];
            if (bVar == null) {
                writableDatabase.endTransaction();
                return false;
            }
            if (str2 != null) {
                bVar.b = str2;
                if (!a(writableDatabase, str, i, bVar.b, null, bVar.a())) {
                    writableDatabase.endTransaction();
                    return false;
                }
            } else {
                if (!a(writableDatabase, str, i, bVar.b, null, bVarArr[i2].a())) {
                    writableDatabase.endTransaction();
                    return false;
                }
            }
        }
        writableDatabase.setTransactionSuccessful();
        writableDatabase.endTransaction();
        return true;
    }

    public static boolean a(Context context, String str, int i, String str2, com.vodone.caibo.b.d[] dVarArr) {
        SQLiteDatabase writableDatabase = b.a(context).getWritableDatabase();
        writableDatabase.beginTransaction();
        for (int i2 = 0; i2 < dVarArr.length; i2++) {
            com.vodone.caibo.b.d dVar = dVarArr[i2];
            if (dVar == null) {
                writableDatabase.endTransaction();
                return false;
            }
            if (!a(writableDatabase, str, i, dVar.a, str2, dVarArr[i2].a())) {
                writableDatabase.endTransaction();
                return false;
            }
        }
        writableDatabase.setTransactionSuccessful();
        writableDatabase.endTransaction();
        return true;
    }

    public static boolean a(Context context, String str, int i, m[] mVarArr) {
        SQLiteDatabase writableDatabase = b.a(context).getWritableDatabase();
        writableDatabase.beginTransaction();
        int i2 = 0;
        while (i2 < mVarArr.length) {
            m mVar = mVarArr[i2];
            if (mVar == null) {
                writableDatabase.endTransaction();
                return false;
            } else if (!a(writableDatabase, str, i, mVar)) {
                writableDatabase.endTransaction();
                return false;
            } else {
                i2++;
            }
        }
        writableDatabase.setTransactionSuccessful();
        writableDatabase.endTransaction();
        return true;
    }

    public static boolean a(Context context, String str, int i, n[] nVarArr) {
        SQLiteDatabase writableDatabase = b.a(context).getWritableDatabase();
        writableDatabase.beginTransaction();
        for (n nVar : nVarArr) {
            if (nVar == null) {
                writableDatabase.endTransaction();
                return false;
            }
            if (!a(writableDatabase, str, i, nVar.a, null, nVar.a())) {
                writableDatabase.endTransaction();
                return false;
            }
        }
        writableDatabase.setTransactionSuccessful();
        writableDatabase.endTransaction();
        return true;
    }

    public static boolean a(Context context, String str, c cVar) {
        String str2 = "username = '" + str + "'";
        ContentValues contentValues = new ContentValues();
        contentValues.clear();
        if (cVar.c == null) {
            contentValues.putNull("password");
        } else {
            contentValues.put("password", cVar.c);
        }
        contentValues.put("savepwd", Integer.valueOf(cVar.d ? 0 : 1));
        return context.getContentResolver().update(CaiboProvider.b, contentValues, str2, null) == 1;
    }

    /* JADX WARNING: Removed duplicated region for block: B:42:0x00f5  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean a(android.database.sqlite.SQLiteDatabase r8, java.lang.String r9, int r10, com.vodone.caibo.b.m r11) {
        /*
            r6 = 1
            r5 = 0
            java.lang.String r2 = ","
            if (r8 == 0) goto L_0x0008
            if (r11 != 0) goto L_0x000a
        L_0x0008:
            r0 = r5
        L_0x0009:
            return r0
        L_0x000a:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r5)
            java.lang.String r1 = " INSERT OR ROLLBACK INTO "
            r0.append(r1)
            java.lang.String r1 = "friends"
            r0.append(r1)
            java.lang.String r1 = "("
            r0.append(r1)
            java.lang.String r1 = "belongto"
            r0.append(r1)
            java.lang.String r1 = ","
            r0.append(r2)
            java.lang.String r1 = "userid"
            r0.append(r1)
            java.lang.String r1 = ","
            r0.append(r2)
            java.lang.String r1 = "friendtype"
            r0.append(r1)
            java.lang.String r1 = ","
            r0.append(r2)
            java.lang.String r1 = "head"
            r0.append(r1)
            java.lang.String r1 = ","
            r0.append(r2)
            java.lang.String r1 = "nickname"
            r0.append(r1)
            java.lang.String r1 = ","
            r0.append(r2)
            java.lang.String r1 = "newest"
            r0.append(r1)
            java.lang.String r1 = ","
            r0.append(r2)
            java.lang.String r1 = "sex"
            r0.append(r1)
            java.lang.String r1 = ","
            r0.append(r2)
            java.lang.String r1 = "usertype"
            r0.append(r1)
            java.lang.String r1 = ")"
            r0.append(r1)
            java.lang.String r1 = " VALUES("
            r0.append(r1)
            java.lang.String r1 = "?,?,?,?,?,?,?,?"
            r0.append(r1)
            java.lang.String r1 = ");"
            r0.append(r1)
            r1 = 0
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x010d }
            android.database.sqlite.SQLiteStatement r0 = r8.compileStatement(r0)     // Catch:{ Exception -> 0x010d }
            if (r9 == 0) goto L_0x00d6
            r1 = 1
            r0.bindString(r1, r9)     // Catch:{ Exception -> 0x00db, all -> 0x00ef }
        L_0x008c:
            r1 = 2
            java.lang.String r2 = r11.a     // Catch:{ Exception -> 0x00db, all -> 0x00ef }
            r0.bindString(r1, r2)     // Catch:{ Exception -> 0x00db, all -> 0x00ef }
            r1 = 3
            long r2 = (long) r10     // Catch:{ Exception -> 0x00db, all -> 0x00ef }
            r0.bindLong(r1, r2)     // Catch:{ Exception -> 0x00db, all -> 0x00ef }
            java.lang.String r1 = r11.b     // Catch:{ Exception -> 0x00db, all -> 0x00ef }
            if (r1 == 0) goto L_0x00ea
            r1 = 4
            java.lang.String r2 = r11.b     // Catch:{ Exception -> 0x00db, all -> 0x00ef }
            r0.bindString(r1, r2)     // Catch:{ Exception -> 0x00db, all -> 0x00ef }
        L_0x00a1:
            java.lang.String r1 = r11.c     // Catch:{ Exception -> 0x00db, all -> 0x00ef }
            if (r1 == 0) goto L_0x00f9
            r1 = 5
            java.lang.String r2 = r11.c     // Catch:{ Exception -> 0x00db, all -> 0x00ef }
            r0.bindString(r1, r2)     // Catch:{ Exception -> 0x00db, all -> 0x00ef }
        L_0x00ab:
            java.lang.String r1 = r11.d     // Catch:{ Exception -> 0x00db, all -> 0x00ef }
            if (r1 == 0) goto L_0x00fe
            r1 = 6
            java.lang.String r2 = r11.d     // Catch:{ Exception -> 0x00db, all -> 0x00ef }
            r0.bindString(r1, r2)     // Catch:{ Exception -> 0x00db, all -> 0x00ef }
        L_0x00b5:
            r1 = 7
            byte r2 = r11.f     // Catch:{ Exception -> 0x00db, all -> 0x00ef }
            long r2 = (long) r2     // Catch:{ Exception -> 0x00db, all -> 0x00ef }
            r0.bindLong(r1, r2)     // Catch:{ Exception -> 0x00db, all -> 0x00ef }
            r1 = 8
            int r2 = r11.e     // Catch:{ Exception -> 0x00db, all -> 0x00ef }
            long r2 = (long) r2     // Catch:{ Exception -> 0x00db, all -> 0x00ef }
            r0.bindLong(r1, r2)     // Catch:{ Exception -> 0x00db, all -> 0x00ef }
            long r1 = r0.executeInsert()     // Catch:{ Exception -> 0x00db, all -> 0x00ef }
            r3 = -1
            int r1 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r1 != 0) goto L_0x0103
            if (r0 == 0) goto L_0x00d3
            r0.close()
        L_0x00d3:
            r0 = r5
            goto L_0x0009
        L_0x00d6:
            r1 = 1
            r0.bindNull(r1)     // Catch:{ Exception -> 0x00db, all -> 0x00ef }
            goto L_0x008c
        L_0x00db:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
        L_0x00df:
            r0.printStackTrace()     // Catch:{ all -> 0x010b }
            if (r1 == 0) goto L_0x00e7
            r1.close()
        L_0x00e7:
            r0 = r5
            goto L_0x0009
        L_0x00ea:
            r1 = 4
            r0.bindNull(r1)     // Catch:{ Exception -> 0x00db, all -> 0x00ef }
            goto L_0x00a1
        L_0x00ef:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
        L_0x00f3:
            if (r1 == 0) goto L_0x00f8
            r1.close()
        L_0x00f8:
            throw r0
        L_0x00f9:
            r1 = 5
            r0.bindNull(r1)     // Catch:{ Exception -> 0x00db, all -> 0x00ef }
            goto L_0x00ab
        L_0x00fe:
            r1 = 6
            r0.bindNull(r1)     // Catch:{ Exception -> 0x00db, all -> 0x00ef }
            goto L_0x00b5
        L_0x0103:
            if (r0 == 0) goto L_0x0108
            r0.close()
        L_0x0108:
            r0 = r6
            goto L_0x0009
        L_0x010b:
            r0 = move-exception
            goto L_0x00f3
        L_0x010d:
            r0 = move-exception
            goto L_0x00df
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vodone.caibo.database.a.a(android.database.sqlite.SQLiteDatabase, java.lang.String, int, com.vodone.caibo.b.m):boolean");
    }

    /* JADX WARNING: Removed duplicated region for block: B:37:0x00b2  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean a(android.database.sqlite.SQLiteDatabase r8, java.lang.String r9, int r10, java.lang.String r11, java.lang.String r12, java.lang.String r13) {
        /*
            r6 = 1
            r5 = 0
            java.lang.String r2 = ","
            if (r8 != 0) goto L_0x0008
            r0 = r5
        L_0x0007:
            return r0
        L_0x0008:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r5)
            java.lang.String r1 = " INSERT OR ROLLBACK INTO "
            r0.append(r1)
            java.lang.String r1 = "caibo"
            r0.append(r1)
            java.lang.String r1 = "("
            r0.append(r1)
            java.lang.String r1 = "belongto"
            r0.append(r1)
            java.lang.String r1 = ","
            r0.append(r2)
            java.lang.String r1 = "tweetid"
            r0.append(r1)
            java.lang.String r1 = ","
            r0.append(r2)
            java.lang.String r1 = "type"
            r0.append(r1)
            java.lang.String r1 = ","
            r0.append(r2)
            java.lang.String r1 = "tag"
            r0.append(r1)
            java.lang.String r1 = ","
            r0.append(r2)
            java.lang.String r1 = "timeline"
            r0.append(r1)
            java.lang.String r1 = ")"
            r0.append(r1)
            java.lang.String r1 = " VALUES("
            r0.append(r1)
            java.lang.String r1 = "?,?,?,?,?"
            r0.append(r1)
            java.lang.String r1 = ");"
            r0.append(r1)
            r1 = 0
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x00c5 }
            android.database.sqlite.SQLiteStatement r0 = r8.compileStatement(r0)     // Catch:{ Exception -> 0x00c5 }
            if (r9 == 0) goto L_0x0093
            r1 = 1
            r0.bindString(r1, r9)     // Catch:{ Exception -> 0x0098, all -> 0x00ac }
        L_0x006c:
            if (r11 == 0) goto L_0x00a7
            r1 = 2
            r0.bindString(r1, r11)     // Catch:{ Exception -> 0x0098, all -> 0x00ac }
        L_0x0072:
            r1 = 3
            long r2 = (long) r10     // Catch:{ Exception -> 0x0098, all -> 0x00ac }
            r0.bindLong(r1, r2)     // Catch:{ Exception -> 0x0098, all -> 0x00ac }
            if (r12 == 0) goto L_0x00b6
            r1 = 4
            r0.bindString(r1, r12)     // Catch:{ Exception -> 0x0098, all -> 0x00ac }
        L_0x007d:
            r1 = 5
            r0.bindString(r1, r13)     // Catch:{ Exception -> 0x0098, all -> 0x00ac }
            long r1 = r0.executeInsert()     // Catch:{ Exception -> 0x0098, all -> 0x00ac }
            r3 = -1
            int r1 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r1 != 0) goto L_0x00bb
            if (r0 == 0) goto L_0x0090
            r0.close()
        L_0x0090:
            r0 = r5
            goto L_0x0007
        L_0x0093:
            r1 = 1
            r0.bindNull(r1)     // Catch:{ Exception -> 0x0098, all -> 0x00ac }
            goto L_0x006c
        L_0x0098:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
        L_0x009c:
            r0.printStackTrace()     // Catch:{ all -> 0x00c3 }
            if (r1 == 0) goto L_0x00a4
            r1.close()
        L_0x00a4:
            r0 = r5
            goto L_0x0007
        L_0x00a7:
            r1 = 2
            r0.bindNull(r1)     // Catch:{ Exception -> 0x0098, all -> 0x00ac }
            goto L_0x0072
        L_0x00ac:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
        L_0x00b0:
            if (r1 == 0) goto L_0x00b5
            r1.close()
        L_0x00b5:
            throw r0
        L_0x00b6:
            r1 = 4
            r0.bindNull(r1)     // Catch:{ Exception -> 0x0098, all -> 0x00ac }
            goto L_0x007d
        L_0x00bb:
            if (r0 == 0) goto L_0x00c0
            r0.close()
        L_0x00c0:
            r0 = r6
            goto L_0x0007
        L_0x00c3:
            r0 = move-exception
            goto L_0x00b0
        L_0x00c5:
            r0 = move-exception
            goto L_0x009c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vodone.caibo.database.a.a(android.database.sqlite.SQLiteDatabase, java.lang.String, int, java.lang.String, java.lang.String, java.lang.String):boolean");
    }

    public static c[] a(Context context) {
        c cVar;
        Cursor query = context.getContentResolver().query(CaiboProvider.b, null, null, null, "_id ASC");
        query.moveToFirst();
        int count = query.getCount();
        c[] cVarArr = new c[count];
        for (int i = 0; i < count; i++) {
            if (query.getPosition() < 0) {
                cVar = null;
            } else {
                cVar = new c();
                cVar.a = query.getString(query.getColumnIndex("accountid"));
                cVar.b = query.getString(query.getColumnIndex("username"));
                cVar.c = query.getString(query.getColumnIndex("password"));
                cVar.d = query.getInt(query.getColumnIndex("savepwd")) == 0;
            }
            cVarArr[i] = cVar;
            query.moveToNext();
        }
        query.close();
        return cVarArr;
    }

    public static int b(Context context, String str) {
        Cursor b2 = b(context, str, 20, null, null);
        int count = b2.getCount();
        b2.close();
        return count;
    }

    public static int b(Context context, String str, int i) {
        String str2 = "belongto = '" + str + "'";
        if (i >= 0) {
            str2 = str2 + " AND friendtype = " + i;
        }
        return context.getContentResolver().delete(CaiboProvider.c, str2, null);
    }

    public static int b(Context context, String str, int i, String str2) {
        return a(context, str, i, (String) null, str2);
    }

    public static Cursor b(Context context, String str, int i, String str2, String str3) {
        String str4 = "belongto = '" + str + "'";
        if (str2 != null) {
            str4 = str4 + " AND tweetid = '" + str2 + "'";
        }
        if (str3 != null) {
            str4 = str4 + " AND tag = '" + str3 + "'";
        }
        Cursor query = context.getContentResolver().query(CaiboProvider.a, a, i >= 0 ? str4 + " AND type = " + i : str4, null, "_id ASC");
        query.moveToFirst();
        return query;
    }

    public static Cursor b(Context context, String str, String str2, int i) {
        return b(context, str, i, str2, null);
    }

    public static boolean b(Context context, String str, String str2) {
        SQLiteDatabase writableDatabase = b.a(context).getWritableDatabase();
        writableDatabase.beginTransaction();
        if (str2.equals(" ")) {
            writableDatabase.endTransaction();
            return false;
        } else if (!a(writableDatabase, str, 23, "aaaaa", null, str2)) {
            writableDatabase.endTransaction();
            return false;
        } else {
            writableDatabase.setTransactionSuccessful();
            writableDatabase.endTransaction();
            return true;
        }
    }

    public static int c(Context context, String str, int i, String str2) {
        return a(context, str, i, str2, (String) null);
    }
}
