package com.vodone.caibo.database;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

public class CaiboProvider extends ContentProvider {
    public static final Uri a = Uri.parse("content://com.vodone.caibo.sqlite.provider/caibo");
    public static final Uri b = Uri.parse("content://com.vodone.caibo.sqlite.provider/accounts");
    public static final Uri c = Uri.parse("content://com.vodone.caibo.sqlite.provider/friends");
    private static final UriMatcher d;
    private b e;

    static {
        UriMatcher uriMatcher = new UriMatcher(-1);
        d = uriMatcher;
        uriMatcher.addURI("com.vodone.caibo.sqlite.provider", "caibo", 1);
        d.addURI("com.vodone.caibo.sqlite.provider", "accounts", 2);
        d.addURI("com.vodone.caibo.sqlite.provider", "friends", 3);
    }

    private static String a(Uri uri) {
        switch (d.match(uri)) {
            case 1:
                return "caibo";
            case 2:
                return "accounts";
            case 3:
                return "friends";
            default:
                return null;
        }
    }

    public int delete(Uri uri, String str, String[] strArr) {
        SQLiteDatabase writableDatabase = this.e.getWritableDatabase();
        try {
            writableDatabase.beginTransaction();
            int delete = writableDatabase.delete(a(uri), str, strArr);
            writableDatabase.setTransactionSuccessful();
            return delete;
        } finally {
            writableDatabase.endTransaction();
        }
    }

    public String getType(Uri uri) {
        return null;
    }

    public Uri insert(Uri uri, ContentValues contentValues) {
        SQLiteDatabase writableDatabase = this.e.getWritableDatabase();
        try {
            writableDatabase.beginTransaction();
            writableDatabase.insert("caibo", null, contentValues);
            writableDatabase.setTransactionSuccessful();
            return uri;
        } finally {
            writableDatabase.endTransaction();
        }
    }

    public boolean onCreate() {
        try {
            this.e = b.a(getContext());
            this.e.getWritableDatabase();
            return true;
        } catch (SQLiteException e2) {
            e2.printStackTrace();
            this.e.close();
            this.e = null;
            return true;
        }
    }

    public Cursor query(Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        SQLiteQueryBuilder sQLiteQueryBuilder = new SQLiteQueryBuilder();
        sQLiteQueryBuilder.setTables(a(uri));
        return sQLiteQueryBuilder.query(this.e.getReadableDatabase(), strArr, str, strArr2, null, null, str2);
    }

    public int update(Uri uri, ContentValues contentValues, String str, String[] strArr) {
        SQLiteDatabase writableDatabase = this.e.getWritableDatabase();
        try {
            writableDatabase.beginTransaction();
            int update = writableDatabase.update(a(uri), contentValues, str, strArr);
            writableDatabase.setTransactionSuccessful();
            return update;
        } finally {
            writableDatabase.endTransaction();
        }
    }
}
