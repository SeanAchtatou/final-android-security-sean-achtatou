package com.vodone.caibo.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public final class b extends SQLiteOpenHelper {
    private static b a;
    private Context b;

    private b(Context context) {
        super(context, "caibo.db", (SQLiteDatabase.CursorFactory) null, 1);
        this.b = context;
    }

    public static synchronized b a(Context context) {
        b bVar;
        synchronized (b.class) {
            if (a == null) {
                a = new b(context);
            }
            bVar = a;
        }
        return bVar;
    }

    public final void onCreate(SQLiteDatabase sQLiteDatabase) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("CREATE TABLE ");
            sb.append("caibo");
            sb.append(" (");
            sb.append("_id");
            sb.append(" INTEGER PRIMARY KEY AUTOINCREMENT,");
            sb.append("belongto");
            sb.append(" TEXT NOT NULL,");
            sb.append("type");
            sb.append(" INTEGER,");
            sb.append("tag");
            sb.append(" TEXT,");
            sb.append("tweetid");
            sb.append(" TEXT NOT NULL,");
            sb.append("read");
            sb.append(" INTEGER,");
            sb.append("timeline");
            sb.append(" TEXT");
            sb.append(");");
            sQLiteDatabase.execSQL(sb.toString());
            sb.setLength(0);
            sb.append("CREATE TABLE ");
            sb.append("accounts");
            sb.append(" (");
            sb.append("_id");
            sb.append(" INTEGER PRIMARY KEY AUTOINCREMENT,");
            sb.append("accountid");
            sb.append(" TEXT NOT NULL,");
            sb.append("username");
            sb.append(" TEXT NOT NULL,");
            sb.append("password");
            sb.append(" TEXT,");
            sb.append("savepwd");
            sb.append(" INTEGER");
            sb.append(");");
            sQLiteDatabase.execSQL(sb.toString());
            sb.setLength(0);
            sb.append("CREATE TABLE ");
            sb.append("friends");
            sb.append(" (");
            sb.append("_id");
            sb.append(" INTEGER PRIMARY KEY AUTOINCREMENT,");
            sb.append("belongto");
            sb.append(" TEXT NOT NULL,");
            sb.append("userid");
            sb.append(" TEXT NOT NULL,");
            sb.append("friendtype");
            sb.append(" INTEGER,");
            sb.append("nickname");
            sb.append(" TEXT,");
            sb.append("head");
            sb.append(" TEXT,");
            sb.append("newest");
            sb.append(" TEXT,");
            sb.append("usertype");
            sb.append(" INTEGER,");
            sb.append("sex");
            sb.append(" INTEGER");
            sb.append(");");
            sQLiteDatabase.execSQL(sb.toString());
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
    }

    public final void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        Log.v("accounts", "onUpgrade");
    }
}
