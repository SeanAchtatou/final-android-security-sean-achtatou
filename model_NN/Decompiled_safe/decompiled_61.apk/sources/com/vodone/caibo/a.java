package com.vodone.caibo;

import java.util.List;
import java.util.StringTokenizer;

public final class a {
    private String a;
    private String b;
    private String c;
    private String[] d;

    private String a(int i, a aVar) {
        this.d = aVar.b();
        switch (i) {
            case 0:
                return this.d[0];
            case 1:
                return this.d[1];
            case 2:
                return this.d[2];
            case 3:
                return this.d[3];
            case 4:
                return this.d[4];
            case 5:
                return this.d[5];
            case 6:
                return this.d[6];
            case 7:
                return this.d[7];
            case 8:
                return this.d[8];
            case 9:
                return this.d[9];
            case 10:
                return this.d[10];
            case 11:
                return this.d[11];
            case 12:
                return this.d[12];
            case 13:
                return this.d[13];
            case 14:
                return this.d[14];
            case 15:
                return this.d[15];
            case 16:
                return this.d[16];
            case 17:
                return this.d[17];
            case 18:
                return this.d[18];
            case 19:
                return this.d[19];
            case 20:
                return this.d[20];
            case 21:
                return this.d[21];
            case 22:
                return this.d[22];
            default:
                return null;
        }
    }

    public final String a() {
        return this.a;
    }

    public final String a(int i, int i2, List list) {
        a aVar = (a) list.get(i - 1);
        switch (i) {
            case 1:
                this.c = this.b;
                break;
            case 2:
                this.c = this.b;
                break;
            case 3:
                this.c = this.b;
                break;
            case 4:
                this.c = this.b;
                break;
            case 5:
                this.c = a(i2 - 5, aVar);
                break;
            case 6:
                this.c = a(i2 - 16, aVar);
                break;
            case 7:
                this.c = a(i2 - 27, aVar);
                break;
            case 8:
                this.c = a(i2 - 50, aVar);
                break;
            case 9:
                this.c = a(i2 - 64, aVar);
                break;
            case 10:
                this.c = a(i2 - 73, aVar);
                break;
            case 11:
                this.c = a(i2 - 86, aVar);
                break;
            case 12:
                this.c = a(i2 - 99, aVar);
                break;
            case 13:
                this.c = a(i2 - 110, aVar);
                break;
            case 14:
                this.c = a(i2 - 127, aVar);
                break;
            case 15:
                this.c = a(i2 - 136, aVar);
                break;
            case 16:
                this.c = a(i2 - 147, aVar);
                break;
            case 17:
                this.c = a(i2 - 164, aVar);
                break;
            case 18:
                this.c = a(i2 - 182, aVar);
                break;
            case 19:
                this.c = a(i2 - 199, aVar);
                break;
            case 20:
                this.c = a(i2 - 213, aVar);
                break;
            case 21:
                this.c = a(i2 - 234, aVar);
                break;
            case 22:
                this.c = a(i2 - 248, aVar);
                break;
            case 23:
                this.c = a(i2 - 269, aVar);
                break;
            case 24:
                this.c = a(i2 - 278, aVar);
                break;
            case 25:
                this.c = a(i2 - 296, aVar);
                break;
            case 26:
                this.c = a(i2 - 312, aVar);
                break;
            case 27:
                this.c = a(i2 - 320, aVar);
                break;
            case 28:
                this.c = a(i2 - 330, aVar);
                break;
            case 29:
                this.c = a(i2 - 344, aVar);
                break;
            case 30:
                this.c = a(i2 - 351, aVar);
                break;
            case 31:
                this.c = a(i2 - 356, aVar);
                break;
            case 32:
                this.c = a(i2 - 378, aVar);
                break;
            case 33:
                this.c = a(i2 - 390, aVar);
                break;
            case 34:
                this.c = a(i2 - 391, aVar);
                break;
        }
        return this.c;
    }

    public final void a(String str) {
        this.a = str;
    }

    public final void b(String str) {
        this.b = str;
    }

    public final String[] b() {
        StringTokenizer stringTokenizer = new StringTokenizer(this.b, "\n");
        String[] strArr = new String[stringTokenizer.countTokens()];
        for (int i = 0; i < strArr.length; i++) {
            strArr[i] = stringTokenizer.nextToken().trim();
        }
        return strArr;
    }
}
