package com.vodone.caibo.b;

import com.windo.a.a.a.d;
import com.windo.a.a.c;

public final class m {
    public String a;
    public String b;
    public String c;
    public String d;
    public int e;
    public byte f = 2;

    public static m a(d dVar) {
        m mVar = new m();
        a(dVar, mVar);
        return mVar;
    }

    public static void a(d dVar, m mVar) {
        if (mVar != null) {
            if (dVar.a("userId", (String) null) != null) {
                mVar.a = dVar.e("userId");
            } else {
                mVar.a = dVar.e("id");
            }
            if (dVar.a("mid_image", (String) null) != null) {
                mVar.b = dVar.e("mid_image");
            } else {
                mVar.b = dVar.e("image");
            }
            mVar.c = dVar.e("nick_name");
            mVar.e = dVar.d("usertype");
            mVar.f = (byte) dVar.d("sex");
            String a2 = dVar.a("newTopic", (String) null);
            if (a2 != null) {
                mVar.d = c.a(a2);
            }
        }
    }
}
