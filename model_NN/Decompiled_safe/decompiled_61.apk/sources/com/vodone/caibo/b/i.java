package com.vodone.caibo.b;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.windo.a.b.a;
import com.windo.a.d.b;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

final class i implements b {
    private /* synthetic */ g a;

    i(g gVar) {
        this.a = gVar;
    }

    public final void a(int i, int i2) {
        String str = null;
        if (this.a.e.containsKey(Integer.valueOf(i))) {
            str = (String) this.a.e.get(Integer.valueOf(i));
            this.a.e.remove(Integer.valueOf(i));
        } else if (this.a.f.containsKey(Integer.valueOf(i))) {
            str = (String) this.a.f.get(Integer.valueOf(i));
            this.a.f.remove(Integer.valueOf(i));
        }
        this.a.i.obtainMessage(-1, str);
    }

    public final void a(int i, InputStream inputStream) {
        File b = g.b(this.a.h);
        File file = new File(b, (System.currentTimeMillis() + ((long) this.a.g.nextInt())) + ".tmp");
        try {
            if (file.exists()) {
                file.delete();
            }
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            byte[] bArr = new byte[1024];
            while (true) {
                int read = inputStream.read(bArr);
                if (read == -1) {
                    break;
                }
                fileOutputStream.write(bArr, 0, read);
                fileOutputStream.flush();
            }
            fileOutputStream.close();
            String str = null;
            if (this.a.e.containsKey(Integer.valueOf(i))) {
                str = (String) this.a.e.get(Integer.valueOf(i));
                this.a.e.remove(Integer.valueOf(i));
                File file2 = new File(b, str.hashCode() + ".jpg");
                file.renameTo(file2);
                Bitmap unused = this.a.a(str, BitmapFactory.decodeFile(file2.getPath()));
            } else if (this.a.f.containsKey(Integer.valueOf(i))) {
                str = (String) this.a.f.get(Integer.valueOf(i));
                this.a.f.remove(Integer.valueOf(i));
                File file3 = new File(b, str.hashCode() + ".jpg");
                file.renameTo(file3);
                Bitmap decodeFile = BitmapFactory.decodeFile(file3.getPath());
                if (this.a.c.contains(str)) {
                    this.a.c.remove(str);
                    Bitmap unused2 = this.a.c(str, decodeFile);
                } else {
                    Bitmap unused3 = this.a.b(str, decodeFile);
                }
            }
            this.a.i.sendMessage(this.a.i.obtainMessage(0, str));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public final void a(int i, byte[] bArr, a aVar) {
    }
}
