package com.vodone.caibo.b;

import com.windo.a.a.a.a;
import com.windo.a.a.c;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class d {
    private static Pattern q = Pattern.compile("<img src=\"http://t.diyicai.com/style/images/faces/F[0-9]+.gif\"/>");
    public String a;
    public byte b;
    public String c;
    public String d;
    public String e;
    public int f;
    public int g;
    public String h;
    public String i;
    public String j;
    public String k;
    public String l;
    public String m;
    public d n;
    public boolean o;
    public int p = -1;
    private String r;
    private int s;
    private String t;

    public static d a(com.windo.a.a.a.d dVar) {
        d dVar2 = new d();
        a(dVar, dVar2);
        return dVar2;
    }

    public static String a(String str) {
        StringBuffer stringBuffer = new StringBuffer();
        Matcher matcher = q.matcher(str);
        int i2 = 0;
        while (matcher.find()) {
            int start = matcher.start();
            int end = matcher.end();
            if (end > start && start >= 0) {
                stringBuffer.append(str.substring(i2, start));
                String substring = str.substring(start, end);
                int length = "<img src=\"http://t.diyicai.com/style/images/faces/".length();
                String substring2 = substring.substring(length, length + 3);
                stringBuffer.append("[");
                stringBuffer.append(substring2);
                stringBuffer.append("]");
                i2 = end;
            }
        }
        if (i2 < str.length()) {
            stringBuffer.append(str.subSequence(i2, str.length()));
        }
        return stringBuffer.toString();
    }

    public static void a(com.windo.a.a.a.d dVar, d dVar2) {
        if (dVar2 != null) {
            try {
                if (dVar.a("topicid", (String) null) != null) {
                    dVar2.a = dVar.c("topicid");
                } else {
                    dVar2.a = dVar.c("id");
                }
                dVar2.b = (byte) dVar.a("attach_type", 0);
                if (dVar.a("title", (String) null) != null) {
                    dVar2.r = dVar.a("title", (String) null);
                } else {
                    dVar2.r = dVar.a("rec_title", (String) null);
                }
                dVar2.l = dVar.a("attach", (String) null);
                dVar2.k = dVar.a("attach_small", (String) null);
                dVar2.t = dVar.a("site", (String) null);
                if (dVar.a("count_zf", -1) != -1) {
                    dVar2.f = dVar.d("count_zf");
                } else {
                    dVar2.f = dVar.d("count_zt");
                }
                dVar2.g = dVar.d("count_pl");
                dVar2.s = dVar.d("count_sc");
                if (dVar.a("userid", (String) null) != null) {
                    dVar2.h = dVar.e("userid");
                } else {
                    dVar2.h = dVar.e("userId");
                }
                dVar2.e = dVar.a("source", (String) null);
                dVar2.o = dVar.a("isCc", 0) != 0;
                if (dVar.a("content", (String) null) != null) {
                    dVar2.c = dVar.e("content");
                } else {
                    dVar2.c = dVar.e("rec_content");
                }
                dVar2.c = a(dVar2.c);
                dVar2.c = c.a(dVar2.c);
                dVar2.j = dVar.a("mid_image", "");
                dVar2.i = dVar.e("nick_name");
                dVar2.d = dVar.a("timeformate", null) != null ? dVar.c("timeformate") : dVar.e("date_created");
                dVar2.p = dVar.a("type", -1);
                dVar2.m = dVar.a("orignal_id", (String) null);
                if (dVar2.m == null || dVar2.m.length() <= 0 || dVar2.m.equals("0")) {
                    dVar2.n = null;
                    return;
                }
                d dVar3 = new d();
                if (dVar.a("topicid_ref", (String) null) != null) {
                    dVar3.a = dVar.e("topicid_ref");
                    dVar3.b = (byte) dVar.a("attach_type_ref", 0);
                    dVar3.r = dVar.a("title_ref", (String) null);
                    dVar3.l = dVar.a("attach_ref", (String) null);
                    dVar3.k = dVar.a("attach_small_ref", (String) null);
                    dVar3.t = dVar.a("site_ref", (String) null);
                    dVar3.f = dVar.d("count_zf_ref");
                    dVar3.g = dVar.d("count_pl_ref");
                    dVar3.s = dVar.d("count_sc_ref");
                    dVar3.h = dVar.e("userid_ref");
                    dVar3.e = dVar.a("source_ref", (String) null);
                    dVar3.c = dVar.c("content_ref");
                    dVar3.c = c.a(dVar3.c);
                    dVar3.j = dVar.c("mid_image_ref");
                    dVar3.i = dVar.c("nick_name_ref");
                    dVar2.n = dVar3;
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    public static d b(com.windo.a.a.a.d dVar) {
        d dVar2 = new d();
        try {
            if (dVar.a("count_zt", -1) != -1) {
                dVar2.f = dVar.d("count_zt");
            } else {
                dVar2.f = dVar.d("count_zf");
            }
            dVar2.g = dVar.d("count_pl");
            dVar2.s = dVar.d("count_sc");
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return dVar2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.windo.a.a.a.d.a(java.lang.String, java.lang.Object):com.windo.a.a.a.d
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.windo.a.a.a.d.a(java.lang.String, int):int
      com.windo.a.a.a.d.a(java.lang.String, boolean):com.windo.a.a.a.d
      com.windo.a.a.a.d.a(java.lang.String, java.lang.String):java.lang.String
      com.windo.a.a.a.d.a(java.lang.String, java.lang.Object):com.windo.a.a.a.d */
    public final String a() {
        com.windo.a.a.a.d dVar = new com.windo.a.a.a.d();
        try {
            dVar.a("topicid", (Object) this.a);
            dVar.b("attach_type", this.b);
            dVar.a("title", (Object) this.r);
            if (this.l != null) {
                dVar.a("attach", (Object) this.l);
            }
            if (this.k != null) {
                dVar.a("attach_small", (Object) this.k);
            }
            dVar.a("site", (Object) this.t);
            dVar.b("count_zt", this.f);
            dVar.b("count_pl", this.g);
            dVar.b("count_sc", this.s);
            dVar.a("userid", (Object) this.h);
            dVar.a("content", (Object) this.c);
            dVar.a("source", (Object) this.e);
            dVar.a("timeformate", (Object) this.d);
            dVar.a("mid_image", (Object) this.j);
            dVar.a("nick_name", (Object) this.i);
            dVar.b("isCc", this.o ? 1 : 0);
            dVar.a("type", (Object) String.valueOf(this.p));
            if (this.n != null) {
                dVar.a("orignal_id", (Object) this.m);
                dVar.a("topicid_ref", (Object) this.n.a);
                dVar.b("attach_type_ref", this.n.b);
                dVar.a("title_ref", (Object) this.n.r);
                dVar.a("attach_ref", (Object) this.n.l);
                if (this.n.l != null) {
                    dVar.a("attach_ref", (Object) this.n.l);
                }
                if (this.n.k != null) {
                    dVar.a("attach_small_ref", (Object) this.n.k);
                }
                dVar.a("site_ref", (Object) this.n.t);
                dVar.b("count_zf_ref", this.n.f);
                dVar.b("count_pl_ref", this.n.g);
                dVar.b("count_sc_ref", this.n.s);
                dVar.a("userid_ref", (Object) this.n.h);
                dVar.a("content_ref", (Object) this.n.c);
                dVar.a("source_ref", (Object) this.n.e);
                dVar.a("timeformate", (Object) this.d);
                dVar.a("mid_image_ref", (Object) this.n.j);
                dVar.a("nick_name_ref", (Object) this.n.i);
            }
            return dVar.toString();
        } catch (a e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public final boolean b() {
        return (this.l == null || this.l.length() == 0 || this.k == null || this.k.length() == 0) ? false : true;
    }

    public final boolean c() {
        return this.n != null;
    }
}
