package com.vodone.caibo.b;

import android.content.Context;
import com.vodone.caibo.database.a;
import java.util.Hashtable;
import java.util.Vector;

public final class l {
    private static l d;
    private Vector a = new Vector();
    private Hashtable b = new Hashtable();
    private Context c;

    private l(Context context) {
        this.c = context;
        a.a();
        c[] a2 = a.a(this.c);
        if (a2 != null && a2.length > 0) {
            for (c cVar : a2) {
                this.b.put(cVar.b, cVar);
                this.a.addElement(cVar.b);
            }
        }
    }

    public static l a(Context context) {
        if (d == null) {
            d = new l(context);
        }
        return d;
    }

    public static void a() {
        if (d != null) {
            d.b.clear();
            d = null;
        }
    }

    public final void a(c cVar) {
        if (!this.b.containsKey(cVar.b)) {
            this.b.put(cVar.b, cVar);
            this.a.addElement(cVar.b);
            a.a();
            a.a(this.c, cVar);
        }
    }

    public final void a(String str) {
        if (this.b.containsKey(str)) {
            this.b.remove(str);
            this.a.removeElement(str);
            a.a();
            a.a(this.c, str);
        }
    }

    public final boolean a(String str, c cVar) {
        if (!this.b.containsKey(str)) {
            return false;
        }
        this.b.put(str, cVar);
        a.a();
        return a.a(this.c, str, cVar);
    }

    public final c b(String str) {
        if (this.b.containsKey(str)) {
            return (c) this.b.get(str);
        }
        return null;
    }

    public final String[] b() {
        String[] strArr = new String[this.a.size()];
        this.a.toArray(strArr);
        return strArr;
    }

    public final boolean c(String str) {
        return this.b.containsKey(str);
    }
}
