package com.vodone.caibo.b;

import com.windo.a.a.a.a;
import com.windo.a.a.a.d;
import com.windo.a.a.c;

public final class b {
    public String a;
    public String b;
    public String c;
    public String d;
    public String e;
    public String f;
    public String g;
    public String h;
    public String i;
    public String j;

    public static b a(d dVar) {
        b bVar = new b();
        a(dVar, bVar);
        return bVar;
    }

    public static void a(d dVar, b bVar) {
        if (bVar != null) {
            try {
                bVar.f = dVar.c("nick_name");
                bVar.g = dVar.c("mid_image");
                bVar.b = dVar.e("topicid");
                if (dVar.a("content", (String) null) != null) {
                    bVar.a = dVar.e("content");
                }
                bVar.a = d.a(bVar.a);
                bVar.a = c.a(bVar.a);
                bVar.d = dVar.e("content_text");
                if (bVar.d != null) {
                    bVar.d = d.a(bVar.d);
                    bVar.d = c.a(bVar.d);
                }
                bVar.c = dVar.e("timeformate") != null ? dVar.c("timeformate") : dVar.e("time") != null ? dVar.c("time") : null;
                bVar.h = dVar.a("ycid", (String) null);
                bVar.i = dVar.a("replyid", (String) null);
                if (bVar.i != null && !bVar.i.equals("0")) {
                    if (dVar.e("content_ref") != null) {
                        bVar.d = c.a(d.a(dVar.e("content_ref")));
                    }
                    bVar.j = dVar.a("nick_name_ref", (String) null);
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.windo.a.a.a.d.a(java.lang.String, java.lang.Object):com.windo.a.a.a.d
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.windo.a.a.a.d.a(java.lang.String, int):int
      com.windo.a.a.a.d.a(java.lang.String, boolean):com.windo.a.a.a.d
      com.windo.a.a.a.d.a(java.lang.String, java.lang.String):java.lang.String
      com.windo.a.a.a.d.a(java.lang.String, java.lang.Object):com.windo.a.a.a.d */
    public final String a() {
        d dVar = new d();
        try {
            dVar.a("nick_name", (Object) this.f);
            dVar.a("mid_image", (Object) this.g);
            dVar.a("content", (Object) this.a);
            dVar.a("topicid", (Object) this.b);
            if (this.d != null && this.d.length() > 0) {
                dVar.a("content_text", (Object) this.d);
            }
            dVar.a("timeformate", (Object) this.c);
            if (this.h != null) {
                dVar.a("ycid", (Object) this.h);
            }
            if (this.i != null) {
                dVar.a("replyid", (Object) this.i);
                dVar.a("nick_name_ref", (Object) this.j);
                dVar.a("content_ref", (Object) this.d);
            }
            return dVar.toString();
        } catch (a e2) {
            e2.printStackTrace();
            return null;
        }
    }
}
