package com.vodone.caibo.b;

import com.windo.a.a.a.d;

public final class n {
    public String a;
    public String b;
    private int c;
    private boolean d;

    public static n a(d dVar) {
        n nVar = new n();
        a(dVar, nVar);
        return nVar;
    }

    public static void a(d dVar, n nVar) {
        if (nVar != null) {
            try {
                nVar.b = dVar.c("name");
                nVar.a = dVar.e("id");
                nVar.c = dVar.d("count_all");
                nVar.d = dVar.a("result", 0) != 0;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.windo.a.a.a.d.a(java.lang.String, java.lang.Object):com.windo.a.a.a.d
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.windo.a.a.a.d.a(java.lang.String, int):int
      com.windo.a.a.a.d.a(java.lang.String, boolean):com.windo.a.a.a.d
      com.windo.a.a.a.d.a(java.lang.String, java.lang.String):java.lang.String
      com.windo.a.a.a.d.a(java.lang.String, java.lang.Object):com.windo.a.a.a.d */
    public final String a() {
        try {
            d dVar = new d();
            dVar.a("name", (Object) this.b);
            dVar.a("id", (Object) this.a);
            dVar.b("count_all", this.c);
            dVar.a("result", this.d);
            return dVar.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public final String toString() {
        return "#" + this.b + "#";
    }
}
