package com.vodone.caibo.b;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import java.util.List;

public final class k extends Handler {
    private /* synthetic */ g a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public k(g gVar, Looper looper) {
        super(looper);
        this.a = gVar;
    }

    public final void handleMessage(Message message) {
        List list;
        String str = (String) message.obj;
        synchronized (this.a.d) {
            list = (List) this.a.d.get(str);
            this.a.d.remove(str);
        }
        if (list != null && list.size() > 0) {
            if (message.what == 0) {
                Bitmap bitmap = null;
                if (this.a.a.containsKey(str)) {
                    bitmap = (Bitmap) this.a.a.get(str);
                } else if (this.a.b.containsKey(str)) {
                    bitmap = (Bitmap) this.a.b.get(str);
                    this.a.b.remove(str);
                }
                for (int i = 0; i < list.size(); i++) {
                    h hVar = (h) list.get(i);
                    if (hVar != null) {
                        hVar.a(bitmap);
                    }
                }
            } else {
                for (int i2 = 0; i2 < list.size(); i2++) {
                    ((h) list.get(i2)).a();
                }
            }
        }
        super.handleMessage(message);
    }
}
