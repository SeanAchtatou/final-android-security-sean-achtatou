package com.vodone.caibo.b;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.os.Looper;
import com.vodone.caibo.R;
import com.windo.a.d.a;
import com.windo.a.d.b;
import com.windo.a.d.c;
import java.io.File;
import java.util.Hashtable;
import java.util.List;
import java.util.Random;
import java.util.Vector;

public final class g {
    private static g m;
    Hashtable a;
    Hashtable b;
    Vector c;
    Hashtable d;
    Hashtable e;
    Hashtable f;
    Random g = new Random();
    Context h;
    public k i;
    private List j;
    private List k;
    private a l;
    private b n = new i(this);

    private g(Context context) {
        this.h = context;
        this.a = new Hashtable();
        this.b = new Hashtable();
        this.j = new Vector();
        this.k = new Vector();
        this.c = new Vector();
        this.d = new Hashtable();
        this.e = new Hashtable();
        this.f = new Hashtable();
        if (Looper.myLooper() == null) {
            this.i = new k(this, Looper.getMainLooper());
        } else {
            this.i = new k(this, Looper.myLooper());
        }
    }

    /* access modifiers changed from: private */
    public Bitmap a(String str, Bitmap bitmap) {
        Bitmap a2;
        synchronized (this.a) {
            if (this.j.size() > 20) {
                String str2 = (String) this.j.get(0);
                this.a.remove(str2);
                this.j.remove(str2);
            }
            a2 = e.a(bitmap);
            this.a.put(str, a2);
            this.j.add(str);
        }
        return a2;
    }

    public static g a(Context context) {
        if (m == null) {
            m = new g(context);
        }
        return m;
    }

    public static void a() {
        if (m != null) {
            m.d.clear();
            m.a.clear();
            if (m.l != null) {
                m.l.a();
            }
            m.l = null;
            m = null;
        }
    }

    private void a(String str, h hVar, byte b2) {
        String str2 = !str.startsWith("http://") ? "http://t.diyicai.com" + str : str;
        if (this.l == null) {
            this.l = new a();
        }
        synchronized (this.d) {
            if (this.d.containsKey(str2)) {
                List list = (List) this.d.get(str2);
                if (!list.contains(hVar)) {
                    list.add(hVar);
                }
            } else {
                c cVar = new c(str2);
                cVar.a(this.n);
                cVar.c();
                Vector vector = new Vector();
                vector.add(hVar);
                if (b2 == 2) {
                    this.e.put(Integer.valueOf(cVar.a()), str2);
                } else if (b2 == 0 || b2 == 1) {
                    this.f.put(Integer.valueOf(cVar.a()), str2);
                    if (b2 == 1) {
                        this.c.add(str2);
                    }
                }
                this.d.put(str2, vector);
                this.l.a(cVar);
            }
        }
    }

    /* access modifiers changed from: private */
    public Bitmap b(String str, Bitmap bitmap) {
        synchronized (this.a) {
            if (this.k.size() > 5) {
                String str2 = (String) this.k.get(0);
                this.a.remove(str2);
                this.k.remove(str2);
            }
            if (bitmap != null) {
                this.a.put(str, bitmap);
                this.k.add(str);
            }
        }
        return bitmap;
    }

    public static synchronized File b(Context context) {
        File cacheDir;
        File file;
        synchronized (g.class) {
            if (Environment.getExternalStorageState().equals("mounted")) {
                File file2 = new File(Environment.getExternalStorageDirectory(), "/vodone/caibo");
                if (!file2.exists()) {
                    file2.mkdirs();
                }
                cacheDir = file2;
            } else {
                cacheDir = context.getCacheDir();
            }
            file = new File(cacheDir, "/cache");
            if (!file.exists()) {
                file.mkdirs();
            }
        }
        return file;
    }

    /* access modifiers changed from: private */
    public Bitmap c(String str, Bitmap bitmap) {
        synchronized (this.a) {
            this.b.put(str, bitmap);
        }
        return bitmap;
    }

    public static synchronized File c(Context context) {
        File file;
        synchronized (g.class) {
            file = new File(context.getCacheDir(), "/cache");
            if (!file.exists()) {
                file = null;
            }
        }
        return file;
    }

    private Bitmap d(String str) {
        String str2 = str.hashCode() + ".jpg";
        File d2 = d();
        if (d2 != null) {
            File file = new File(d2, str2);
            if (file.exists()) {
                return BitmapFactory.decodeFile(file.getPath());
            }
        }
        File c2 = c(this.h);
        if (c2 != null) {
            File file2 = new File(c2, str2);
            if (file2.exists()) {
                return BitmapFactory.decodeFile(file2.getPath());
            }
        }
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x001e, code lost:
        if (r1.exists() != false) goto L_0x0020;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized java.io.File d() {
        /*
            java.lang.Class<com.vodone.caibo.b.g> r0 = com.vodone.caibo.b.g.class
            monitor-enter(r0)
            java.lang.String r1 = android.os.Environment.getExternalStorageState()     // Catch:{ all -> 0x0024 }
            java.lang.String r2 = "mounted"
            boolean r1 = r1.equals(r2)     // Catch:{ all -> 0x0024 }
            if (r1 == 0) goto L_0x0022
            java.io.File r1 = new java.io.File     // Catch:{ all -> 0x0024 }
            java.io.File r2 = android.os.Environment.getExternalStorageDirectory()     // Catch:{ all -> 0x0024 }
            java.lang.String r3 = "/vodone/caibo/cache"
            r1.<init>(r2, r3)     // Catch:{ all -> 0x0024 }
            boolean r2 = r1.exists()     // Catch:{ all -> 0x0024 }
            if (r2 == 0) goto L_0x0022
        L_0x0020:
            monitor-exit(r0)
            return r1
        L_0x0022:
            r1 = 0
            goto L_0x0020
        L_0x0024:
            r1 = move-exception
            monitor-exit(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vodone.caibo.b.g.d():java.io.File");
    }

    public final Bitmap a(String str) {
        if (str == null || str.length() == 0) {
            return b();
        }
        String str2 = !str.startsWith("http://") ? "http://t.diyicai.com" + str : str;
        synchronized (this.a) {
            if (this.a.containsKey(str2)) {
                Bitmap bitmap = (Bitmap) this.a.get(str2);
                return bitmap;
            }
            Bitmap d2 = d(str2);
            if (d2 == null) {
                return null;
            }
            Bitmap a2 = a(str2, d2);
            return a2;
        }
    }

    public final void a(String str, h hVar) {
        if (str != null && str.length() != 0) {
            a(str, hVar, (byte) 2);
        }
    }

    public final Bitmap b() {
        if (this.a.containsKey("default_userhead_system")) {
            return (Bitmap) this.a.get("default_userhead_system");
        }
        Bitmap decodeResource = BitmapFactory.decodeResource(this.h.getResources(), R.drawable.default_portrait);
        this.a.put("default_userhead_system", decodeResource);
        return decodeResource;
    }

    public final Bitmap b(String str) {
        String str2;
        if (!str.startsWith("http://")) {
            str2 = "http://t.diyicai.com" + (!str.startsWith("/") ? "/" + str : str);
        } else {
            str2 = str;
        }
        synchronized (this.a) {
            if (this.a.containsKey(str2)) {
                Bitmap bitmap = (Bitmap) this.a.get(str2);
                return bitmap;
            }
            Bitmap d2 = d(str2);
            if (d2 == null) {
                return null;
            }
            Bitmap b2 = b(str2, d2);
            return b2;
        }
    }

    public final void b(String str, h hVar) {
        if (str != null && str.length() != 0) {
            a(str, hVar, (byte) 0);
        }
    }

    public final Bitmap c() {
        if (this.a.containsKey("default_preview_system")) {
            return (Bitmap) this.a.get("default_preview_system");
        }
        Bitmap decodeResource = BitmapFactory.decodeResource(this.h.getResources(), R.drawable.default_preview);
        this.a.put("default_preview_system", decodeResource);
        return decodeResource;
    }

    public final Bitmap c(String str) {
        String str2;
        if (!str.startsWith("http://")) {
            str2 = "http://t.diyicai.com" + (!str.startsWith("/") ? "/" + str : str);
        } else {
            str2 = str;
        }
        synchronized (this.a) {
            if (this.a.containsKey(str2)) {
                Bitmap bitmap = (Bitmap) this.a.get(str2);
                return bitmap;
            }
            Bitmap d2 = d(str2);
            return d2;
        }
    }

    public final void c(String str, h hVar) {
        if (str != null && str.length() != 0) {
            a(str, hVar, (byte) 1);
        }
    }
}
