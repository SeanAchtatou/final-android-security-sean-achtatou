package com.vodone.caibo.a;

import com.vodone.caibo.b.a;
import com.vodone.caibo.b.m;
import com.vodone.caibo.service.b;
import com.windo.a.c.d;
import com.windo.a.d.c;
import java.util.Hashtable;

public final class h extends b {
    private int b = 0;
    private String c;
    private String d;
    private String e;
    private String f;
    private Hashtable g;

    private h(d dVar, int i) {
        super(dVar, i);
    }

    public static h a(d dVar, String str, String str2) {
        h hVar = new h(dVar, 114);
        hVar.c = str;
        hVar.d = str2;
        return hVar;
    }

    public static h a(d dVar, String str, String str2, String str3) {
        h hVar = new h(dVar, 116);
        hVar.f = str;
        hVar.c = str2;
        hVar.d = str3;
        return hVar;
    }

    public static h a(d dVar, String str, Hashtable hashtable) {
        h hVar = new h(dVar, 115);
        hVar.c = str;
        hVar.g = hashtable;
        if (hVar.g.get("sma_image") != null) {
            hVar.b = 1;
        } else {
            hVar.b = 2;
        }
        return hVar;
    }

    public final void a() {
        c cVar = null;
        b.a();
        switch (d()) {
            case 113:
                cVar = b.o(this.d, this.e);
                break;
            case 114:
                cVar = b.n(this.c, this.d);
                break;
            case 116:
                cVar = b.c(this.f, this.c, this.d);
            case 115:
                if (this.b != 1) {
                    if (this.b == 2) {
                        cVar = b.a(this.c, this.g);
                        break;
                    }
                } else {
                    cVar = b.c(this.g.get("sma_image").toString());
                    break;
                }
                break;
        }
        if (cVar != null) {
            a(cVar);
        }
    }

    public final void a(int i) {
    }

    public final void a(Exception exc) {
    }

    public final void a(String str) {
        switch (d()) {
            case 113:
                try {
                    String c2 = new com.windo.a.a.a.d(str).c("userid");
                    if (c2 != "-1") {
                        a(0, d(), -1, c2);
                        return;
                    } else {
                        a(-1, d(), (Object) null);
                        return;
                    }
                } catch (Exception e2) {
                    return;
                }
            case 114:
            case 116:
                if (str.equals("fail")) {
                    a(-1, d(), (Object) null);
                    return;
                }
                try {
                    com.windo.a.a.a.d dVar = new com.windo.a.a.a.d(str);
                    a aVar = new a();
                    if (aVar.a != null) {
                        m.a(dVar, aVar.a);
                    } else {
                        aVar.a = m.a(dVar);
                    }
                    aVar.b = dVar.e("signatures");
                    aVar.e = dVar.e("instruction");
                    aVar.f = dVar.e("address");
                    aVar.g = dVar.b("fans");
                    aVar.h = dVar.b("topicsize");
                    aVar.i = dVar.b("attention");
                    aVar.l = dVar.b("favoritesize");
                    aVar.k = dVar.d("themesize");
                    aVar.j = dVar.d("blacksize");
                    aVar.m = dVar.d("province");
                    aVar.n = dVar.d("city");
                    aVar.o = dVar.b("isAtt");
                    aVar.p = dVar.b("isBlack");
                    a(0, d(), -1, aVar);
                    return;
                } catch (Exception e3) {
                    e3.printStackTrace();
                    return;
                }
            case 115:
                if (this.b == 1) {
                    this.g.put("sma_image", str);
                    this.b = 2;
                    b().b(c());
                    return;
                } else if (this.b == 2) {
                    try {
                        if (new com.windo.a.a.a.d(str).c("result").equals("succ")) {
                            a(0, d(), -1, (Object) null);
                            return;
                        } else {
                            a(-1, d(), (Object) null);
                            return;
                        }
                    } catch (Exception e4) {
                        e4.printStackTrace();
                        return;
                    }
                } else {
                    return;
                }
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public final void b(byte[] bArr) {
        if (d() == 115 && this.b == 1) {
            a(bArr);
        } else {
            super.b(bArr);
        }
    }
}
