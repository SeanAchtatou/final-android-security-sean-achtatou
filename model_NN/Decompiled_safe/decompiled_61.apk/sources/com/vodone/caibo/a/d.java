package com.vodone.caibo.a;

import com.vodone.caibo.b.m;
import com.vodone.caibo.service.b;
import com.windo.a.a.a.a;
import com.windo.a.a.a.e;
import com.windo.a.d.c;

public final class d extends b {
    private int b;
    private int c;
    private String d;
    private String e;

    private d(com.windo.a.c.d dVar, int i) {
        super(dVar, i);
    }

    public static d a(com.windo.a.c.d dVar, int i, String str, int i2) {
        d dVar2 = new d(dVar, i);
        dVar2.d = str;
        dVar2.b = 1;
        dVar2.c = i2;
        return dVar2;
    }

    public static d a(com.windo.a.c.d dVar, int i, String str, String str2) {
        d dVar2 = new d(dVar, i);
        dVar2.d = str;
        dVar2.e = str2;
        return dVar2;
    }

    public static d b(com.windo.a.c.d dVar, int i, String str, int i2) {
        d dVar2 = new d(dVar, i);
        dVar2.d = str;
        dVar2.b = i2;
        dVar2.c = 30;
        return dVar2;
    }

    public final void a() {
        c cVar = null;
        b.a();
        switch (d()) {
            case 129:
            case 130:
                cVar = b.i(this.d, this.b, this.c);
                break;
            case 131:
            case 132:
                cVar = b.j(this.d, this.b, this.c);
                break;
            case 133:
            case 134:
                cVar = b.k(this.d, this.b, this.c);
                break;
            case 135:
                cVar = b.d(this.d, this.e);
                break;
            case 136:
                cVar = b.e(this.d, this.e);
                break;
            case 137:
                cVar = b.f(this.d, this.e);
                break;
            case 144:
                cVar = b.g(this.d, this.e);
                break;
            case 145:
                cVar = b.h(this.d, this.e);
                break;
            case 146:
                cVar = b.i(this.d, this.e);
                break;
            case 147:
            case 148:
                cVar = b.l(this.d, this.b, this.c);
                break;
        }
        if (cVar != null) {
            a(cVar);
        }
    }

    public final void a(int i) {
    }

    public final void a(Exception exc) {
    }

    public final void a(String str) {
        int i = 0;
        switch (d()) {
            case 129:
            case 130:
            case 131:
            case 132:
            case 147:
            case 148:
                try {
                    e eVar = new e(str);
                    int a = eVar.a();
                    m[] mVarArr = new m[a];
                    while (i < a) {
                        mVarArr[i] = m.a(eVar.b(i));
                        i++;
                    }
                    a(0, d(), -1, mVarArr);
                    return;
                } catch (a e2) {
                    e2.printStackTrace();
                    return;
                }
            case 133:
            case 134:
                if (!str.equals("{}")) {
                    try {
                        e eVar2 = new e(new com.windo.a.a.a.d(str).e("retList"));
                        int a2 = eVar2.a();
                        m[] mVarArr2 = new m[a2];
                        while (i < a2) {
                            mVarArr2[i] = m.a(eVar2.b(i));
                            i++;
                        }
                        a(0, d(), -1, mVarArr2);
                        return;
                    } catch (a e3) {
                        e3.printStackTrace();
                        return;
                    }
                } else {
                    a(0, d(), -1, (Object) null);
                    return;
                }
            case 135:
            case 136:
            case 137:
            case 144:
                try {
                    String c2 = new com.windo.a.a.a.d(str).c("result");
                    if (c2.equals("succ")) {
                        a(0, d(), -1, (Object) null);
                        return;
                    } else if (c2.equals("fail")) {
                        a(-1, d(), null);
                        return;
                    } else {
                        return;
                    }
                } catch (Exception e4) {
                    return;
                }
            case 138:
            case 139:
            case 140:
            case 141:
            case 142:
            case 143:
            default:
                return;
            case 145:
                try {
                    a(0, d(), new com.windo.a.a.a.d(str).b("relation"), (Object) null);
                    return;
                } catch (Exception e5) {
                    e5.printStackTrace();
                    return;
                }
            case 146:
                try {
                    Object a3 = new com.windo.a.a.a.d(str).a("result");
                    String obj = a3.toString();
                    if (!a3.equals("fail")) {
                        a(0, d(), Integer.parseInt(obj), (Object) null);
                        return;
                    }
                    return;
                } catch (Exception e6) {
                    e6.printStackTrace();
                    return;
                }
        }
    }
}
