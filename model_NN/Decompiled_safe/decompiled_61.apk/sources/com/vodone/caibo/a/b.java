package com.vodone.caibo.a;

import com.windo.a.c.d;
import com.windo.a.c.f;
import java.io.UnsupportedEncodingException;

public abstract class b extends f {
    protected b(d dVar, int i) {
        super(dVar, i);
    }

    public abstract void a(String str);

    public final void a(byte[] bArr) {
        try {
            a(new String(bArr, "utf-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
}
