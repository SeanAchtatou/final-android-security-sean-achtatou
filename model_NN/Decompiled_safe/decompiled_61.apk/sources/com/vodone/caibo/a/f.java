package com.vodone.caibo.a;

import com.vodone.caibo.b.j;
import com.vodone.caibo.b.n;
import com.vodone.caibo.service.b;
import com.windo.a.a.a.a;
import com.windo.a.a.a.e;
import com.windo.a.c.d;
import com.windo.a.d.c;

public final class f extends b {
    public String b;
    public String c;
    private int d;
    private int e;
    private String f;
    private String g;
    private String h;
    private String i;
    private String k;

    private f(d dVar, int i2) {
        super(dVar, i2);
    }

    public static f a(d dVar, int i2, String str, int i3) {
        f fVar = new f(dVar, i2);
        fVar.h = str;
        fVar.d = 1;
        fVar.e = i3;
        return fVar;
    }

    public static f a(d dVar, int i2, String str, int i3, int i4, String str2) {
        f fVar = new f(dVar, i2);
        fVar.h = str;
        fVar.e = i3;
        fVar.d = i4;
        fVar.f = str2;
        return fVar;
    }

    public static f a(d dVar, int i2, String str, String str2, int i3, int i4) {
        f fVar = new f(dVar, i2);
        fVar.h = str;
        fVar.i = str2;
        fVar.d = i4;
        fVar.e = i3;
        return fVar;
    }

    public static f a(d dVar, String str) {
        f fVar = new f(dVar, 52);
        fVar.h = str;
        return fVar;
    }

    public static f a(d dVar, String str, String str2) {
        f fVar = new f(dVar, 65);
        fVar.h = str;
        fVar.i = str2;
        return fVar;
    }

    public static f b(d dVar, String str) {
        f fVar = new f(dVar, 288);
        fVar.h = str;
        fVar.d = 1;
        fVar.e = 50;
        return fVar;
    }

    public static f b(d dVar, String str, String str2) {
        f fVar = new f(dVar, 289);
        fVar.h = str;
        fVar.g = str2;
        return fVar;
    }

    public final void a() {
        c b2;
        int d2 = d();
        b.a();
        switch (d2) {
            case 17:
                b2 = b.b(this.h, 1, this.e);
                break;
            case 18:
                b2 = b.b(this.h, this.d, this.e);
                break;
            case 19:
                b2 = b.a(this.h, this.e, this.f);
                break;
            case 20:
                b2 = b.c(this.h, this.d, this.e);
                break;
            case 21:
            case 22:
                b2 = b.d(this.h, this.d, this.e);
                break;
            case 23:
            case 24:
                b2 = b.e(this.h, this.d, this.e);
                break;
            case 25:
            case 32:
                b2 = b.f(this.h, this.d, this.e);
                break;
            case 33:
            case 34:
                b2 = b.g(this.h, this.d, this.e);
                break;
            case 35:
            case 36:
                b2 = b.h(this.h, this.d, this.e);
                break;
            case 37:
            case 38:
                b2 = b.e(this.h, this.k, this.d, this.e);
                break;
            case 39:
            case 40:
                b2 = b.c(this.h, this.c, this.d, this.e);
                break;
            case 41:
            case 48:
                b2 = b.b();
                break;
            case 49:
            case 50:
                b2 = b.c();
                break;
            case 51:
                b2 = b.d();
                break;
            case 52:
                b2 = b.a(this.h);
                break;
            case 53:
                b2 = b.d(this.h, this.f, this.d, this.e);
                break;
            case 54:
            case 55:
                b2 = b.a(this.d, this.e);
                break;
            case 56:
            case 57:
                b2 = b.a(this.h, this.b, this.d, this.e);
                break;
            case 64:
            case 66:
            case 67:
                b2 = b.b(this.h, this.i, this.d, this.e);
                break;
            case 65:
                b2 = b.a(this.h, this.i);
                break;
            case 288:
                b2 = b.a(this.h, this.d, this.e);
                break;
            case 289:
                b2 = b.b(this.h, this.g);
                break;
            default:
                b2 = null;
                break;
        }
        if (b2 != null) {
            a(b2);
        }
    }

    public final void a(int i2) {
        a(i2, d(), (Object) null);
    }

    public final void a(Exception exc) {
        exc.printStackTrace();
    }

    public final void a(String str) {
        int i2 = 0;
        switch (d()) {
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
            case 33:
            case 34:
            case 35:
            case 36:
            case 37:
            case 38:
            case 39:
            case 40:
            case 41:
            case 48:
            case 49:
            case 50:
            case 54:
            case 55:
            case 56:
            case 57:
                try {
                    e eVar = new e(str);
                    int a = eVar.a();
                    com.vodone.caibo.b.d[] dVarArr = new com.vodone.caibo.b.d[a];
                    while (i2 < a) {
                        dVarArr[i2] = com.vodone.caibo.b.d.a(eVar.b(i2));
                        if (d() == 33 || d() == 34) {
                            dVarArr[i2].o = true;
                        }
                        i2++;
                    }
                    a(0, d(), -1, dVarArr);
                    return;
                } catch (a e2) {
                    e2.printStackTrace();
                    return;
                }
            case 23:
            case 24:
            case 64:
            case 66:
            case 67:
                try {
                    if (str.equals("[]")) {
                        a(0, d(), -1, (Object) null);
                        return;
                    }
                    e eVar2 = new e(str);
                    int a2 = eVar2.a();
                    j[] jVarArr = new j[a2];
                    while (i2 < a2) {
                        com.windo.a.a.a.d b2 = eVar2.b(i2);
                        j jVar = new j();
                        j.a(b2, jVar);
                        jVarArr[i2] = jVar;
                        i2++;
                    }
                    a(0, d(), -1, jVarArr);
                    return;
                } catch (a e3) {
                    e3.printStackTrace();
                    return;
                }
            case 25:
            case 32:
            case 53:
                try {
                    e eVar3 = new e(str);
                    int a3 = eVar3.a();
                    com.vodone.caibo.b.b[] bVarArr = new com.vodone.caibo.b.b[a3];
                    while (i2 < a3) {
                        bVarArr[i2] = com.vodone.caibo.b.b.a(eVar3.b(i2));
                        i2++;
                    }
                    a(0, d(), -1, bVarArr);
                    return;
                } catch (a e4) {
                    e4.printStackTrace();
                    return;
                }
            case 51:
                try {
                    e eVar4 = new e(str);
                    n[] nVarArr = new n[eVar4.a()];
                    while (i2 < nVarArr.length) {
                        nVarArr[i2] = n.a((com.windo.a.a.a.d) eVar4.a(i2));
                        i2++;
                    }
                    a(0, d(), -1, nVarArr);
                    return;
                } catch (Exception e5) {
                    e5.printStackTrace();
                    return;
                }
            case 52:
                if (str.charAt(0) == '{') {
                    try {
                        com.windo.a.a.a.d dVar = new com.windo.a.a.a.d(str);
                        Integer.parseInt(dVar.c("result"));
                        a(-1, d(), dVar.c("msg"));
                        return;
                    } catch (Exception e6) {
                        e6.printStackTrace();
                        return;
                    }
                } else {
                    try {
                        e eVar5 = new e(str);
                        n[] nVarArr2 = new n[eVar5.a()];
                        while (i2 < nVarArr2.length) {
                            nVarArr2[i2] = n.a((com.windo.a.a.a.d) eVar5.a(i2));
                            i2++;
                        }
                        a(0, d(), -1, nVarArr2);
                        return;
                    } catch (Exception e7) {
                        e7.printStackTrace();
                        return;
                    }
                }
            case 65:
            case 289:
                try {
                    if (new com.windo.a.a.a.d(str).c("result").equals("succ")) {
                        a(0, d(), -1, (Object) null);
                        return;
                    } else {
                        a(-1, d(), (Object) null);
                        return;
                    }
                } catch (Exception e8) {
                    e8.printStackTrace();
                    return;
                }
            case 288:
                try {
                    String a4 = new com.windo.a.a.a.d(str).a("retList", (String) null);
                    if (a4 != null) {
                        e eVar6 = new e(a4);
                        int a5 = eVar6.a();
                        com.vodone.caibo.b.f[] fVarArr = new com.vodone.caibo.b.f[a5];
                        while (i2 < a5) {
                            fVarArr[i2] = com.vodone.caibo.b.f.a(eVar6.b(i2));
                            i2++;
                        }
                        a(0, d(), -1, fVarArr);
                        return;
                    }
                    a(0, d(), -1, (Object) null);
                    return;
                } catch (a e9) {
                    e9.printStackTrace();
                    return;
                }
            default:
                return;
        }
    }

    public final void b(String str) {
        this.k = str;
    }
}
