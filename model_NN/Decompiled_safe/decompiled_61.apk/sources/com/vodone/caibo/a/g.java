package com.vodone.caibo.a;

import com.vodone.caibo.CaiboApp;
import com.vodone.caibo.b.d;
import com.vodone.caibo.service.b;
import com.windo.a.a.a.e;
import com.windo.a.d.c;

public final class g extends b {
    public String b;
    public String c;
    public String d;
    public String e;
    public String f;
    public String g;
    public String h;
    public String i;
    private int k = 0;
    private d l = null;
    private int m;
    private boolean n;

    public g(com.windo.a.c.d dVar, int i2) {
        super(dVar, i2);
    }

    public static g a(com.windo.a.c.d dVar, d dVar2) {
        g gVar = new g(dVar, 112);
        gVar.l = dVar2;
        if (dVar2.l != null) {
            gVar.k = 1;
        } else {
            gVar.k = 2;
        }
        return gVar;
    }

    public static g a(com.windo.a.c.d dVar, String str, int i2, d dVar2) {
        g gVar = new g(dVar, 97);
        gVar.l = dVar2;
        gVar.b = str;
        gVar.m = i2;
        if (dVar2.l != null) {
            gVar.k = 1;
        } else {
            gVar.k = 2;
        }
        return gVar;
    }

    public static g a(com.windo.a.c.d dVar, String str, String str2, boolean z) {
        g gVar = new g(dVar, 50);
        gVar.b = str;
        gVar.c = str2;
        gVar.n = z;
        return gVar;
    }

    public static g b(com.windo.a.c.d dVar, String str, String str2, boolean z) {
        g gVar = new g(dVar, 104);
        gVar.b = str;
        gVar.c = str2;
        gVar.n = z;
        return gVar;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final void a() {
        c a;
        b.a();
        if (d() == 291) {
            a = b.m(this.h, this.i);
        } else if (CaiboApp.a().b() != null) {
            String str = CaiboApp.a().b().a;
            switch (d()) {
                case 50:
                    a = b.a(str, this.b, this.c, this.n ? 1 : 0);
                    break;
                case 51:
                    a = b.d();
                    break;
                case 97:
                    if (this.l != null) {
                        if (this.k != 1) {
                            if (this.k == 2) {
                                if (this.l.l != null) {
                                    this.l.b = 1;
                                } else {
                                    this.l.b = 0;
                                }
                                a = b.a(str, this.b, this.l.c, this.l.b, this.l.l, this.l.m, this.m);
                                break;
                            }
                        } else {
                            a = b.c(this.l.l);
                            break;
                        }
                    }
                    a = null;
                    break;
                case 99:
                    a = b.a(str, this.f, this.c);
                    break;
                case 101:
                    a = b.j(str, this.d);
                    break;
                case 102:
                    a = b.k(str, this.d);
                    break;
                case 103:
                    a = b.b(this.b);
                    break;
                case 104:
                    a = b.b(str, this.b, this.c, this.n ? 1 : 0);
                    break;
                case 105:
                    a = b.b(str, this.c, this.b);
                    break;
                case 112:
                    if (this.l != null) {
                        if (this.k != 1) {
                            if (this.k == 2) {
                                if (this.l.l != null) {
                                    this.l.b = 1;
                                } else {
                                    this.l.b = 0;
                                }
                                a = b.a(str, this.l.c, this.l.b, this.l.l);
                                break;
                            }
                        } else {
                            a = b.c(this.l.l);
                            break;
                        }
                    }
                    a = null;
                    break;
                case 257:
                    a = b.d(str);
                    break;
                case 290:
                    a = b.l(str, this.b);
                    break;
                case 1793:
                    a = b.c(this.e, this.b);
                    break;
                default:
                    a = null;
                    break;
            }
        } else {
            return;
        }
        if (a != null) {
            a(a);
        }
    }

    public final void a(int i2) {
        a(i2, d(), null);
    }

    public final void a(Exception exc) {
        exc.printStackTrace();
    }

    public final void a(String str) {
        int i2 = 0;
        switch (d()) {
            case 50:
            case 99:
            case 102:
            case 104:
            case 105:
            case 290:
                try {
                    if (new com.windo.a.a.a.d(str).c("result").equals("succ")) {
                        a(0, d(), -1, (Object) null);
                        return;
                    } else {
                        a(-1, d(), null);
                        return;
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                    return;
                }
            case 97:
            case 112:
                if (this.l == null) {
                    return;
                }
                if (this.k == 1) {
                    this.l.l = str;
                    this.k = 2;
                    b().b(c());
                    return;
                } else if (this.k == 2) {
                    try {
                        if (new com.windo.a.a.a.d(str).c("result").equals("succ")) {
                            a(0, d(), -1, (Object) null);
                            return;
                        } else {
                            a(-1, d(), null);
                            return;
                        }
                    } catch (Exception e3) {
                        e3.printStackTrace();
                        return;
                    }
                } else {
                    return;
                }
            case 101:
                if (str.toLowerCase().equals("succ")) {
                    a(0, d(), -1, (Object) null);
                    return;
                } else {
                    a(-1, d(), null);
                    return;
                }
            case 103:
                try {
                    if (str.equals("[]")) {
                        a(26369, d(), null);
                        return;
                    }
                    e eVar = new e(str);
                    d[] dVarArr = new d[eVar.a()];
                    while (i2 < dVarArr.length) {
                        dVarArr[i2] = d.a((com.windo.a.a.a.d) eVar.a(i2));
                        i2++;
                    }
                    a(0, d(), -1, dVarArr[0]);
                    return;
                } catch (Exception e4) {
                    e4.printStackTrace();
                    return;
                }
            case 257:
                try {
                    int[] iArr = new int[12];
                    com.windo.a.a.a.d dVar = new com.windo.a.a.a.d(str);
                    iArr[0] = dVar.b("zxdt");
                    iArr[1] = dVar.b("zxkj");
                    iArr[2] = dVar.b("zjsc");
                    iArr[3] = dVar.b("gfzz");
                    iArr[4] = dVar.b("mrcb");
                    iArr[5] = dVar.b("atme");
                    iArr[6] = dVar.b("pl");
                    iArr[7] = dVar.b("sx");
                    iArr[8] = dVar.b("gzrft");
                    iArr[9] = dVar.b("gz");
                    iArr[10] = dVar.b("xttz");
                    a(0, d(), -1, iArr);
                    return;
                } catch (Exception e5) {
                    e5.printStackTrace();
                    return;
                }
            case 291:
                try {
                    com.windo.a.a.a.d dVar2 = new com.windo.a.a.a.d(str);
                    if (dVar2.a("isUpdate", -1) == 1) {
                        a(0, d(), -1, dVar2.c("add"));
                        return;
                    }
                    a(74497, d(), -1, (Object) null);
                    return;
                } catch (Exception e6) {
                    e6.printStackTrace();
                    return;
                }
            case 1793:
                try {
                    e eVar2 = new e(str);
                    d[] dVarArr2 = new d[eVar2.a()];
                    while (i2 < dVarArr2.length) {
                        dVarArr2[i2] = d.b((com.windo.a.a.a.d) eVar2.a(i2));
                        i2++;
                    }
                    a(0, d(), -1, dVarArr2[0]);
                    return;
                } catch (Exception e7) {
                    e7.printStackTrace();
                    return;
                }
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public final void b(byte[] bArr) {
        if (d() == 97 && this.k == 1) {
            a(bArr);
        } else if (d() == 112 && this.k == 1) {
            a(bArr);
        } else {
            super.b(bArr);
        }
    }
}
