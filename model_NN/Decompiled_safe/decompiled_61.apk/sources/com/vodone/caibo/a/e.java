package com.vodone.caibo.a;

import android.telephony.TelephonyManager;
import com.vodone.caibo.CaiboApp;
import com.vodone.caibo.b.a;
import com.vodone.caibo.b.m;
import com.vodone.caibo.service.b;
import com.windo.a.c.d;
import com.windo.a.d.c;

public final class e extends b {
    private c b = null;

    private e(d dVar, int i) {
        super(dVar, i);
    }

    public static e a(d dVar, int i, String str) {
        e eVar = new e(dVar, 9);
        b.a();
        eVar.b = b.a(i, str);
        return eVar;
    }

    public static e a(d dVar, String str) {
        e eVar = new e(dVar, 8);
        b.a();
        eVar.b = b.e(str);
        return eVar;
    }

    public static e a(d dVar, String str, String str2) {
        e eVar = new e(dVar, 1);
        b.a();
        eVar.b = b.p(str, str2);
        return eVar;
    }

    public static e a(d dVar, String str, String str2, String str3, String str4) {
        e eVar = new e(dVar, 6);
        b.a();
        eVar.b = b.a(str, str2, str3, str4);
        return eVar;
    }

    public static e b(d dVar, String str, String str2) {
        e eVar = new e(dVar, 2);
        b.a();
        TelephonyManager telephonyManager = (TelephonyManager) CaiboApp.a().getSystemService("phone");
        eVar.b = b.d(str, str2, telephonyManager != null ? telephonyManager.getDeviceId() : null);
        return eVar;
    }

    public static e c(d dVar, String str, String str2) {
        e eVar = new e(dVar, 7);
        b.a();
        eVar.b = b.q(str, str2);
        return eVar;
    }

    public static e d(d dVar, String str, String str2) {
        e eVar = new e(dVar, 3);
        b.a();
        eVar.b = b.r(str, str2);
        return eVar;
    }

    public static e e(d dVar, String str, String str2) {
        e eVar = new e(dVar, 4);
        b.a();
        eVar.b = b.s(str, str2);
        return eVar;
    }

    public final void a() {
        switch (d()) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
                if (this.b != null) {
                    a(this.b);
                    return;
                }
                return;
            default:
                return;
        }
    }

    public final void a(int i) {
        a(i, d(), (Object) null);
    }

    public final void a(Exception exc) {
    }

    public final void a(String str) {
        switch (d()) {
            case 1:
                if (str.toLowerCase().equals("fail")) {
                    a(257, d(), (Object) null);
                    return;
                }
                try {
                    a(0, d(), -1, m.a(new com.windo.a.a.a.d(str)));
                    return;
                } catch (Exception e) {
                    e.printStackTrace();
                    return;
                }
            case 2:
                try {
                    com.windo.a.a.a.d dVar = new com.windo.a.a.a.d(str);
                    int b2 = dVar.b("result");
                    if (b2 == 0) {
                        com.vodone.caibo.b.c cVar = new com.vodone.caibo.b.c();
                        cVar.a = dVar.c("id");
                        cVar.b = dVar.c("nickname");
                        a(0, d(), -1, cVar);
                        return;
                    }
                    a((d() << 8) | b2, d(), (Object) null);
                    return;
                } catch (Exception e2) {
                    e2.printStackTrace();
                    return;
                }
            case 3:
            case 7:
                try {
                    com.windo.a.a.a.d dVar2 = new com.windo.a.a.a.d(str);
                    if (dVar2.b("result") == 0) {
                        a(0, d(), -1, null);
                        return;
                    }
                    a(-1, d(), dVar2.c("msg"));
                    return;
                } catch (Exception e3) {
                    return;
                }
            case 4:
                if (str.toLowerCase().equals("fail")) {
                    a(1025, d(), (Object) null);
                    return;
                }
                try {
                    com.windo.a.a.a.d dVar3 = new com.windo.a.a.a.d(str);
                    if (dVar3.b("result") == 0) {
                        a(0, d(), -1, null);
                        return;
                    }
                    a(-1, d(), dVar3.c("msg"));
                    return;
                } catch (Exception e4) {
                    e4.printStackTrace();
                    return;
                }
            case 5:
            case 6:
                try {
                    int b3 = new com.windo.a.a.a.d(str).b("result");
                    if (b3 == 0) {
                        a(0, d(), -1, null);
                        return;
                    } else {
                        a(b3 | (d() << 8), d(), (Object) null);
                        return;
                    }
                } catch (Exception e5) {
                    e5.printStackTrace();
                    return;
                }
            case 8:
                if (str.toLowerCase().equals("fail")) {
                    a(2052, d(), (Object) null);
                    return;
                }
                try {
                    com.windo.a.a.a.d dVar4 = new com.windo.a.a.a.d(str);
                    if (dVar4.e("result").equals("4")) {
                        a(-1, d(), dVar4.e("msg"));
                        return;
                    }
                    a aVar = new a();
                    aVar.c = dVar4.e("mail");
                    aVar.d = dVar4.e("phone");
                    if (aVar.c != "" && aVar.d != "") {
                        a(2051, d(), -1, aVar);
                        return;
                    } else if (aVar.c != "") {
                        a(2050, d(), -1, aVar);
                        return;
                    } else if (aVar.d != "") {
                        a(2049, d(), -1, aVar);
                        return;
                    } else {
                        a(8, d(), -1, null);
                        return;
                    }
                } catch (Exception e6) {
                    e6.printStackTrace();
                    return;
                }
            case 9:
                if (str.toLowerCase().equals("fail")) {
                    a(-1, d(), (Object) null);
                    return;
                }
                try {
                    com.windo.a.a.a.d dVar5 = new com.windo.a.a.a.d(str);
                    if (dVar5.b("result") == 0) {
                        a(0, d(), -1, null);
                        return;
                    }
                    a(-1, d(), dVar5.c("msg"));
                    return;
                } catch (Exception e7) {
                    e7.printStackTrace();
                    return;
                }
            default:
                return;
        }
    }
}
