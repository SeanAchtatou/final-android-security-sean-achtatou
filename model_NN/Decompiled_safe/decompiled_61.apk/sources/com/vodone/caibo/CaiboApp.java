package com.vodone.caibo;

import android.app.AlarmManager;
import android.app.Application;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import com.vodone.caibo.activity.af;
import com.vodone.caibo.b.c;
import com.vodone.caibo.b.l;
import com.vodone.caibo.service.CheckLatestService;
import com.vodone.caibo.service.d;
import com.windo.a.b.a.b;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

public class CaiboApp extends Application {
    private static CaiboApp a;
    private NotificationManager b;
    private Hashtable c;
    private boolean d = false;
    private int[] e = null;
    private c f;

    public static CaiboApp a() {
        if (a != null) {
            return a;
        }
        throw new NullPointerException("app not create or be terminated!");
    }

    private PendingIntent g() {
        Intent intent = new Intent(this, CheckLatestService.class);
        intent.setAction("repeating");
        return PendingIntent.getService(this, 0, intent, 134217728);
    }

    public final int a(int i) {
        if (i < 0 || i >= this.e.length) {
            return 0;
        }
        return this.e[i];
    }

    public final void a(int i, int i2) {
        if (i >= 0 && i < this.e.length) {
            this.e[i] = i2;
            if (this.c.containsKey(Integer.valueOf(i))) {
                List list = (List) this.c.get(Integer.valueOf(i));
                int size = list.size();
                for (int i3 = 0; i3 < size; i3++) {
                    Handler handler = (Handler) list.get(i3);
                    handler.sendMessage(Message.obtain(handler, 100001, i, i2));
                }
            }
        }
    }

    public final void a(int i, Handler handler) {
        if (this.c.containsKey(Integer.valueOf(i))) {
            List list = (List) this.c.get(Integer.valueOf(i));
            if (!list.contains(handler)) {
                list.add(handler);
                return;
            }
            return;
        }
        Vector vector = new Vector();
        vector.addElement(handler);
        this.c.put(Integer.valueOf(i), vector);
    }

    public final void a(c cVar) {
        this.f = cVar;
        if (cVar != null) {
            af.a(this, "current_account", cVar.b);
        } else {
            af.a(this, "current_account", (String) null);
        }
    }

    public final c b() {
        String c2;
        if (this.f == null && (c2 = af.c(this, "current_account")) != null) {
            this.f = l.a(this).b(c2);
        }
        return this.f;
    }

    public final void b(int i) {
        if (i >= 0 && i < this.e.length) {
            switch (i) {
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                    a(i, 0);
                    a(12, this.e[2] + this.e[0] + this.e[1] + this.e[3]);
                    return;
                case 5:
                case 6:
                case 7:
                    a(i, 0);
                    a(11, this.e[5] + this.e[6] + this.e[7]);
                    return;
                case 8:
                    a(i, 0);
                    return;
                default:
                    return;
            }
        }
    }

    public final void b(int i, Handler handler) {
        if (this.c.containsKey(Integer.valueOf(i))) {
            ((List) this.c.get(Integer.valueOf(i))).remove(handler);
        }
    }

    public final void c() {
        ((AlarmManager) getSystemService("alarm")).cancel(g());
        this.b.cancel(0);
        this.b.cancel(1);
        this.b.cancel(2);
        this.b.cancel(3);
        this.b.cancel(4);
        this.b.cancel(5);
        this.b.cancel(6);
        this.b.cancel(7);
        this.b.cancel(8);
        this.b.cancel(9);
        this.d = false;
        com.vodone.caibo.service.c.a().b(this);
        a = null;
        this.b = null;
        d.b();
        d.a();
        af.a(this, "current_account", (String) null);
        b.a();
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0040  */
    /* JADX WARNING: Removed duplicated region for block: B:5:0x002c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void d() {
        /*
            r11 = this;
            r10 = 120(0x78, float:1.68E-43)
            r1 = 2
            r9 = 1
            r8 = 0
            r7 = -1
            android.app.PendingIntent r6 = r11.g()
            long r2 = android.os.SystemClock.elapsedRealtime()
            java.lang.String r0 = "alarm"
            java.lang.Object r0 = r11.getSystemService(r0)
            android.app.AlarmManager r0 = (android.app.AlarmManager) r0
            r0.cancel(r6)
            java.lang.String r4 = "com.vodone.caibo.setting"
            android.content.SharedPreferences r4 = r11.getSharedPreferences(r4, r8)
            java.lang.String r5 = "mMessageAlert"
            int r4 = r4.getInt(r5, r7)
            if (r4 == r7) goto L_0x003e
            if (r4 != 0) goto L_0x002f
            r4 = r7
        L_0x002a:
            if (r4 >= 0) goto L_0x0040
            r11.d = r8
        L_0x002e:
            return
        L_0x002f:
            if (r4 != r9) goto L_0x0034
            r4 = 30
            goto L_0x002a
        L_0x0034:
            if (r4 == r1) goto L_0x003e
            r5 = 3
            if (r4 != r5) goto L_0x003c
            r4 = 300(0x12c, float:4.2E-43)
            goto L_0x002a
        L_0x003c:
            r4 = r10
            goto L_0x002a
        L_0x003e:
            r4 = r10
            goto L_0x002a
        L_0x0040:
            int r4 = r4 * 1000
            long r7 = (long) r4
            long r2 = r2 + r7
            long r4 = (long) r4
            r0.setRepeating(r1, r2, r4, r6)
            r11.d = r9
            goto L_0x002e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vodone.caibo.CaiboApp.d():void");
    }

    public final boolean e() {
        return this.d;
    }

    public final NotificationManager f() {
        return this.b;
    }

    public void onCreate() {
        super.onCreate();
        a = this;
        this.b = (NotificationManager) getSystemService("notification");
        this.c = new Hashtable();
        this.e = new int[14];
        b.a(getPackageName(), "vodone");
    }
}
