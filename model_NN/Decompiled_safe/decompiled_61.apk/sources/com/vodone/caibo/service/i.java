package com.vodone.caibo.service;

import android.content.Intent;
import android.net.Uri;
import com.vodone.caibo.activity.cn;

final class i implements Runnable {
    private /* synthetic */ d a;

    i(d dVar) {
        this.a = dVar;
    }

    public final void run() {
        this.a.a.cancel();
        d dVar = this.a;
        if (dVar.b != null) {
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setDataAndType(Uri.parse("file://" + dVar.b), "application/vnd.android.package-archive");
            cn.a().b().startActivity(intent);
        }
    }
}
