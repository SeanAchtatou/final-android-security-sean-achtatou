package com.vodone.caibo.service;

import com.windo.a.c.a;
import com.windo.a.c.c;
import com.windo.a.c.d;
import com.windo.a.d.b;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.Hashtable;
import java.util.zip.GZIPInputStream;

public final class e extends a implements b {
    private Hashtable b = new Hashtable();
    private Hashtable c = new Hashtable();
    private com.windo.a.d.a d;

    public e(d dVar, com.windo.a.d.a aVar) {
        super(dVar);
        this.d = aVar;
    }

    public final void a() {
        if (this.d != null) {
            this.d.a();
            this.d = null;
        }
    }

    public final void a(int i, int i2) {
        int i3;
        switch (i2) {
            case 6002:
                i3 = 602;
                break;
            case 6003:
                i3 = 603;
                break;
            case 6004:
                i3 = 601;
                break;
            default:
                i3 = 604;
                break;
        }
        synchronized (this.b) {
            Short sh = (Short) this.b.remove(new Integer(i));
            if (sh != null) {
                this.c.remove(sh);
                this.a.a(sh, i3);
            }
        }
    }

    public final void a(int i, InputStream inputStream) {
    }

    public final void a(int i, byte[] bArr, com.windo.a.b.a aVar) {
        byte[] bArr2;
        try {
            String a = aVar.a("Content-Encoding");
            if (a == null || !a.equals("gzip")) {
                bArr2 = bArr;
            } else {
                GZIPInputStream gZIPInputStream = new GZIPInputStream(new ByteArrayInputStream(bArr));
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                byte[] bArr3 = new byte[1024];
                while (true) {
                    int read = gZIPInputStream.read(bArr3);
                    if (read <= 0) {
                        break;
                    }
                    byteArrayOutputStream.write(bArr3, 0, read);
                }
                bArr2 = byteArrayOutputStream.toByteArray();
            }
            synchronized (this.b) {
                Integer num = new Integer(i);
                if (this.b.containsKey(num)) {
                    Short sh = (Short) this.b.remove(num);
                    this.c.remove(sh);
                    this.a.a(new com.windo.a.c.b(this.a, sh, bArr2));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            a(i, 603);
        }
    }

    public final void a(c cVar) {
        synchronized (this.b) {
            Short sh = new Short(cVar.c());
            if (this.c.containsKey(sh)) {
                com.windo.a.d.c cVar2 = (com.windo.a.d.c) this.c.remove(sh);
                cVar2.b();
                this.b.remove(new Integer(cVar2.a()));
            }
        }
    }

    public final void a(Object obj, c cVar) {
        if (obj instanceof com.windo.a.d.c) {
            com.windo.a.d.c cVar2 = (com.windo.a.d.c) obj;
            synchronized (this.b) {
                Short sh = new Short(cVar.c());
                this.b.put(new Integer(cVar2.a()), sh);
                this.c.put(sh, cVar2);
                cVar2.a(this);
                this.d.a(cVar2);
            }
            return;
        }
        cVar.a(new IllegalArgumentException());
    }
}
