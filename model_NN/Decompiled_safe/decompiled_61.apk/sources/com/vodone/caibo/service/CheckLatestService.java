package com.vodone.caibo.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import com.vodone.caibo.activity.af;
import com.vodone.caibo.activity.bb;
import java.util.Date;

public class CheckLatestService extends Service implements Runnable {
    private Thread a = null;
    private bb b = new a(this);

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
        if (this.a == null) {
            this.a = new Thread(this);
            this.a.start();
        }
    }

    public void onDestroy() {
        this.a = null;
        super.onDestroy();
    }

    public void onStart(Intent intent, int i) {
        if (intent == null) {
            stopSelf(i);
        } else {
            super.onStart(intent, i);
        }
    }

    public void run() {
        boolean z;
        Date date = new Date(System.currentTimeMillis());
        Date date2 = new Date(System.currentTimeMillis());
        String c = af.c(this, "StartTime");
        if (c == null) {
            date2.setHours(9);
        } else {
            String[] split = c.split(":");
            if (split != null && split.length == 2) {
                date2.setHours(Integer.parseInt(split[0]));
                date2.setMinutes(Integer.parseInt(split[1]));
            }
        }
        if (date.before(date2)) {
            z = false;
        } else {
            String c2 = af.c(this, "EndTime");
            if (c2 == null) {
                date2.setHours(19);
            } else {
                String[] split2 = c2.split(":");
                if (split2 != null && split2.length == 2) {
                    date2.setHours(Integer.parseInt(split2[0]));
                    date2.setMinutes(Integer.parseInt(split2[1]));
                }
            }
            z = !date.after(date2);
        }
        if (z) {
            c.a().l(this.b);
        } else {
            stopSelf();
        }
    }
}
