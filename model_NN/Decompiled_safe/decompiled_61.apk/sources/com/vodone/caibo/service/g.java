package com.vodone.caibo.service;

import android.os.Environment;
import com.vodone.caibo.CaiboApp;
import com.windo.a.b.a.e;
import java.io.File;

final class g extends Thread {
    private /* synthetic */ String a;
    private /* synthetic */ d b;

    g(d dVar, String str) {
        this.b = dVar;
        this.a = str;
    }

    public final void run() {
        File file = new File(Environment.getExternalStorageDirectory(), this.a.substring(this.a.lastIndexOf("/")));
        if (file.exists()) {
            file.delete();
        }
        this.b.b = file.toString();
        if (e.a(CaiboApp.a())) {
            this.b.b(this.a, file);
        } else {
            this.b.a(this.a, file);
        }
    }
}
