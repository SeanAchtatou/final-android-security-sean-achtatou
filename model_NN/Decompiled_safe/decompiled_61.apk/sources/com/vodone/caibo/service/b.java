package com.vodone.caibo.service;

import com.vodone.caibo.CaiboApp;
import com.vodone.caibo.R;
import com.windo.a.a.a;
import com.windo.a.d.c;
import java.util.Enumeration;
import java.util.Hashtable;

public final class b {
    private static b a;

    public static b a() {
        if (a != null) {
            return a;
        }
        b bVar = new b();
        a = bVar;
        return bVar;
    }

    public static c a(int i, int i2) {
        return a(null, i, i2, null, "listYtTopic");
    }

    public static c a(int i, String str) {
        Hashtable hashtable = new Hashtable();
        hashtable.put("type", String.valueOf(i));
        hashtable.put("nickName", str);
        return b("retrievePwd", hashtable);
    }

    public static c a(String str) {
        Hashtable hashtable = new Hashtable();
        hashtable.put("userId", str);
        return b("listYtThemeGz", hashtable);
    }

    public static c a(String str, int i, int i2) {
        Hashtable hashtable = new Hashtable();
        if (str != null) {
            hashtable.put("userId", str);
        }
        hashtable.put("pageNum", String.valueOf(i));
        hashtable.put("pageSize", String.valueOf(i2));
        return b("getUserGroupList", hashtable);
    }

    private static c a(String str, int i, int i2, String str2, String str3) {
        Hashtable hashtable = new Hashtable();
        if (str != null) {
            hashtable.put("userId", str);
        }
        hashtable.put("pageNum", String.valueOf(i));
        hashtable.put("pageSize", String.valueOf(i2));
        if (str2 != null) {
            hashtable.put("maxTopicId", str2);
        }
        return b(str3, hashtable);
    }

    public static c a(String str, int i, String str2) {
        return a(str, 1, i, str2, "listYtTopicByUserId");
    }

    public static c a(String str, String str2) {
        Hashtable hashtable = new Hashtable();
        hashtable.put("userId1", str);
        hashtable.put("userId2", str2);
        return b("delUsersMailList", hashtable);
    }

    public static c a(String str, String str2, int i, int i2) {
        Hashtable hashtable = new Hashtable();
        if (str != null) {
            hashtable.put("userId", str);
        }
        hashtable.put("pageNum", String.valueOf(i));
        hashtable.put("pageSize", String.valueOf(i2));
        if (str2 != null) {
            hashtable.put("groupId", str2);
        }
        return b("listYtTopicByUserId", hashtable);
    }

    public static c a(String str, String str2, int i, String str3) {
        Hashtable hashtable = new Hashtable();
        hashtable.put("type", "2");
        hashtable.put("userId", str);
        hashtable.put("content", str2);
        hashtable.put("attachType", String.valueOf(i));
        if (str3 == null || str3.length() == 0) {
            hashtable.put("attach", "");
        } else {
            hashtable.put("attach", str3);
        }
        return b("qA1", hashtable);
    }

    public static c a(String str, String str2, String str3) {
        Hashtable hashtable = new Hashtable();
        hashtable.put("userId", str);
        hashtable.put("taUserId", str2);
        hashtable.put("content", str3);
        return b("sendMail", hashtable);
    }

    public static c a(String str, String str2, String str3, byte b, String str4, String str5, int i) {
        Hashtable hashtable = new Hashtable();
        hashtable.put("userId", str);
        hashtable.put("content", str3);
        hashtable.put("attachType", String.valueOf((int) b));
        hashtable.put("source", "android 手机客户端");
        if (str2 == null) {
            hashtable.put("fwTopicId", "");
        } else {
            hashtable.put("fwTopicId", str2);
        }
        if (str4 == null) {
            hashtable.put("attach", "");
        } else {
            hashtable.put("attach", str4);
            hashtable.put("type", "1");
        }
        if (str5 == null) {
            hashtable.put("orignalId", "");
        } else {
            hashtable.put("orignalId", str5);
        }
        hashtable.put("is_comment", String.valueOf(i));
        return b("saveYtTopic", hashtable);
    }

    public static c a(String str, String str2, String str3, int i) {
        Hashtable hashtable = new Hashtable();
        hashtable.put("userId", str);
        hashtable.put("topicId", str2);
        hashtable.put("content", str3);
        hashtable.put("is_trans_topic", String.valueOf(i));
        hashtable.put("source", "android 手机客户端");
        return b("saveYtComment", hashtable);
    }

    public static c a(String str, String str2, String str3, String str4) {
        Hashtable hashtable = new Hashtable();
        hashtable.put("phone", str);
        hashtable.put("passCode", str2);
        hashtable.put("password", str3);
        hashtable.put("repassword", str4);
        return b("resetPasswd", hashtable);
    }

    public static c a(String str, Hashtable hashtable) {
        if (hashtable == null) {
            return null;
        }
        hashtable.put("userId", str);
        return b("updateUser", hashtable);
    }

    private static void a(Hashtable hashtable, StringBuffer stringBuffer) {
        if (hashtable != null) {
            Enumeration keys = hashtable.keys();
            boolean z = true;
            while (keys.hasMoreElements()) {
                String str = (String) keys.nextElement();
                String str2 = (String) hashtable.get(str);
                if (z) {
                    z = false;
                } else {
                    stringBuffer.append('&');
                }
                stringBuffer.append(f(str));
                stringBuffer.append("=");
                stringBuffer.append(f(str2));
            }
        }
    }

    public static c b() {
        return b("listHotYtTopic", (Hashtable) null);
    }

    public static c b(String str) {
        Hashtable hashtable = new Hashtable();
        hashtable.put("topicId", str);
        return b("getTopicListById", hashtable);
    }

    public static c b(String str, int i, int i2) {
        return a(str, i, i2, null, "pushMessageList");
    }

    public static c b(String str, String str2) {
        Hashtable hashtable = new Hashtable();
        hashtable.put("userId", str);
        hashtable.put("topicId", str2);
        return b("delTopic", hashtable);
    }

    public static c b(String str, String str2, int i, int i2) {
        Hashtable hashtable = new Hashtable();
        hashtable.put("userId1", str);
        hashtable.put("userId2", str2);
        hashtable.put("pageNum", String.valueOf(i));
        hashtable.put("pageSize", String.valueOf(i2));
        return b("usersMailList", hashtable);
    }

    public static c b(String str, String str2, String str3) {
        Hashtable hashtable = new Hashtable();
        hashtable.put("type", String.valueOf(2));
        hashtable.put("userId", str);
        hashtable.put("content", str2);
        hashtable.put("topicId", str3);
        return b("report1", hashtable);
    }

    public static c b(String str, String str2, String str3, int i) {
        Hashtable hashtable = new Hashtable();
        hashtable.put("userId", str);
        hashtable.put("content", str3);
        hashtable.put("topicId", str2);
        hashtable.put("source", "android 手机客户端");
        hashtable.put("totop", String.valueOf(i));
        return b("sendComment", hashtable);
    }

    private static c b(String str, Hashtable hashtable) {
        com.vodone.caibo.b.c b;
        StringBuffer stringBuffer = new StringBuffer("http://tapi.diyicai.com/api/mobileClientApi.action?");
        Hashtable hashtable2 = hashtable == null ? new Hashtable() : hashtable;
        if (!hashtable2.containsKey("userId") && (b = CaiboApp.a().b()) != null) {
            hashtable2.put("userId", b.a);
        }
        hashtable2.put("function", str);
        a(hashtable2, stringBuffer);
        return new c(stringBuffer.toString());
    }

    public static c c() {
        return b("listHotYtComment", (Hashtable) null);
    }

    public static c c(String str) {
        c cVar = new c("http://t.diyicai.com/servlet/UploadGroupPic");
        cVar.a(str);
        return cVar;
    }

    public static c c(String str, int i, int i2) {
        return a(str, i, i2, null, "listYtTopicByUserId");
    }

    public static c c(String str, String str2) {
        Hashtable hashtable = new Hashtable();
        if (str != null) {
            hashtable.put("userId", str);
        }
        hashtable.put("topicId", str2);
        return b("getTopicStat", hashtable);
    }

    public static c c(String str, String str2, int i, int i2) {
        Hashtable hashtable = new Hashtable();
        hashtable.put("userId", str);
        hashtable.put("pageNum", String.valueOf(i));
        hashtable.put("pageSize", String.valueOf(i2));
        hashtable.put("themeName", str2);
        return b("getThemetYtTipic", hashtable);
    }

    public static c c(String str, String str2, String str3) {
        Hashtable hashtable = new Hashtable();
        hashtable.put("loginId", str);
        if (str2 != null) {
            hashtable.put("userId", str2);
        } else if (str3 != null) {
            hashtable.put("nickName", str3);
        }
        return b("getUserInfo", hashtable);
    }

    public static c d() {
        Hashtable hashtable = new Hashtable();
        hashtable.put("pageNum", String.valueOf(1));
        hashtable.put("pageSize", String.valueOf(100));
        return b("listYtTheme", hashtable);
    }

    public static c d(String str) {
        Hashtable hashtable = new Hashtable();
        hashtable.put("userId", str);
        return b("checkNewMsg", hashtable);
    }

    public static c d(String str, int i, int i2) {
        return a(str, i, i2, null, "getAtmeTopicList");
    }

    public static c d(String str, String str2) {
        Hashtable hashtable = new Hashtable();
        hashtable.put("userId", str);
        hashtable.put("attUserId", str2);
        return b("saveAttention", hashtable);
    }

    public static c d(String str, String str2, int i, int i2) {
        Hashtable hashtable = new Hashtable();
        hashtable.put("userId", str);
        hashtable.put("topicId", str2);
        hashtable.put("pageNum", String.valueOf(i));
        hashtable.put("pageSize", String.valueOf(i2));
        return b("getYtTopicCommentList", hashtable);
    }

    public static c d(String str, String str2, String str3) {
        Hashtable hashtable = new Hashtable();
        hashtable.put("nickName", str);
        hashtable.put("password", str2);
        hashtable.put("sid", CaiboApp.a().getString(R.string.SID));
        hashtable.put("mobleId", str3);
        return b("newregister", hashtable);
    }

    public static c e(String str) {
        Hashtable hashtable = new Hashtable();
        hashtable.put("nickName", str);
        return b("selectKeepUserStatus", hashtable);
    }

    public static c e(String str, int i, int i2) {
        return a(str, i, i2, null, "newMailList");
    }

    public static c e(String str, String str2) {
        Hashtable hashtable = new Hashtable();
        hashtable.put("userId", str);
        hashtable.put("attUserId", str2);
        return b("cancelAttention", hashtable);
    }

    public static c e(String str, String str2, int i, int i2) {
        Hashtable hashtable = new Hashtable();
        hashtable.put("para", str2);
        hashtable.put("pageNum", String.valueOf(i));
        hashtable.put("pageSize", String.valueOf(i2));
        if (str != null) {
            hashtable.put("userId", str);
        } else {
            hashtable.put("userId", "");
        }
        return b("listYtTopicByOther", hashtable);
    }

    public static c f(String str, int i, int i2) {
        return a(str, i, i2, null, "getCommentList");
    }

    public static c f(String str, String str2) {
        Hashtable hashtable = new Hashtable();
        hashtable.put("userId", str);
        hashtable.put("taUserId", str2);
        return b("saveBlackUser", hashtable);
    }

    private static c f(String str, String str2, int i, int i2) {
        Hashtable hashtable = new Hashtable();
        hashtable.put("keywords", str2);
        hashtable.put("pageNum", String.valueOf(i));
        hashtable.put("pageSize", String.valueOf(i2));
        return b(str, hashtable);
    }

    private static String f(String str) {
        return str == null ? "" : a.a(str, "utf-8");
    }

    public static c g(String str, int i, int i2) {
        return a(str, i, i2, null, "listFavoYtTopicByUserId");
    }

    public static c g(String str, String str2) {
        Hashtable hashtable = new Hashtable();
        hashtable.put("userId", str);
        hashtable.put("taUserId", str2);
        return b("deleteBlackUser", hashtable);
    }

    public static c h(String str, int i, int i2) {
        return a(str == null ? "" : str, i, i2, null, "listYtTopicByZhuanjia");
    }

    public static c h(String str, String str2) {
        Hashtable hashtable = new Hashtable();
        hashtable.put("userId", str);
        hashtable.put("himId", str2);
        return b("getRelationbyuserid", hashtable);
    }

    public static c i(String str, int i, int i2) {
        return a(str, i, i2, null, "getMyFansList");
    }

    public static c i(String str, String str2) {
        Hashtable hashtable = new Hashtable();
        hashtable.put("userId", str);
        hashtable.put("attUserId", str2);
        return b("checkBlackUser", hashtable);
    }

    public static c j(String str, int i, int i2) {
        return a(str, i, i2, null, "getMyAttenList");
    }

    public static c j(String str, String str2) {
        Hashtable hashtable = new Hashtable();
        hashtable.put("userId", str);
        hashtable.put("topicId", str2);
        return b("saveFavorite", hashtable);
    }

    public static c k(String str, int i, int i2) {
        return a(str, i, i2, null, "getBlackUserList");
    }

    public static c k(String str, String str2) {
        Hashtable hashtable = new Hashtable();
        hashtable.put("userId", str);
        hashtable.put("topicId", str2);
        return b("deleteUserTopicById", hashtable);
    }

    public static c l(String str, int i, int i2) {
        return a(str, i, i2, null, "getMailList1");
    }

    public static c l(String str, String str2) {
        Hashtable hashtable = new Hashtable();
        hashtable.put("id", str2);
        hashtable.put("userId", str);
        return b("delUsersMailById", hashtable);
    }

    public static c m(String str, int i, int i2) {
        return f("searchTopicList", str, i, i2);
    }

    public static c m(String str, String str2) {
        Hashtable hashtable = new Hashtable();
        hashtable.put("version", str);
        hashtable.put("identity", "02");
        hashtable.put("sid", str2);
        return b("checkUpdate", hashtable);
    }

    public static c n(String str, int i, int i2) {
        return f("searchUserList", str, i, i2);
    }

    public static c n(String str, String str2) {
        Hashtable hashtable = new Hashtable();
        if (str != null) {
            hashtable.put("userId", str);
        } else if (str2 != null) {
            hashtable.put("nickName", str2);
        }
        return b("getUserInfo", hashtable);
    }

    public static c o(String str, String str2) {
        Hashtable hashtable = new Hashtable();
        hashtable.put("userName", str);
        hashtable.put("password", str2);
        return b("getUserIdByName", hashtable);
    }

    public static c p(String str, String str2) {
        Hashtable hashtable = new Hashtable();
        hashtable.put("userName", str);
        hashtable.put("password", str2);
        hashtable.put("source", "android 手机客户端");
        hashtable.put("vision", CaiboApp.a().getString(R.string.VERSION));
        hashtable.put("sid", CaiboApp.a().getString(R.string.SID));
        return b("login", hashtable);
    }

    public static c q(String str, String str2) {
        Hashtable hashtable = new Hashtable();
        hashtable.put("nickName", str2);
        hashtable.put("phone", str);
        return b("keepMobileUser", hashtable);
    }

    public static c r(String str, String str2) {
        Hashtable hashtable = new Hashtable();
        hashtable.put("passCode", str);
        hashtable.put("nickName", str2);
        return b("keepMobileUser2", hashtable);
    }

    public static c s(String str, String str2) {
        Hashtable hashtable = new Hashtable();
        hashtable.put("nickName", str);
        hashtable.put("mail", str2);
        return b("keepMailUser", hashtable);
    }
}
