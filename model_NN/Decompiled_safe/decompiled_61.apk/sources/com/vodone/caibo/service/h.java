package com.vodone.caibo.service;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import com.vodone.caibo.activity.cn;

final class h implements DialogInterface.OnClickListener {
    final /* synthetic */ d a;
    private /* synthetic */ String b;

    h(d dVar, String str) {
        this.a = dVar;
        this.b = str;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        Activity b2 = cn.a().b();
        this.a.a = new ProgressDialog(b2);
        this.a.a.setTitle("正在下载");
        this.a.a.setMessage("请稍候...");
        this.a.a.setProgressStyle(0);
        this.a.a.show();
        this.a.a.setCancelable(true);
        this.a.a.setOnCancelListener(new f(this));
        d dVar = this.a;
        String str = this.b;
        dVar.c = false;
        new g(dVar, str).start();
    }
}
