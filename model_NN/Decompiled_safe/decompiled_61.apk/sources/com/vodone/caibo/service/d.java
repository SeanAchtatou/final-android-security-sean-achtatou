package com.vodone.caibo.service;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Message;
import android.widget.Toast;
import com.vodone.caibo.CaiboApp;
import com.vodone.caibo.R;
import com.vodone.caibo.activity.bb;
import com.vodone.caibo.activity.cn;
import com.vodone.caibo.activity.l;
import com.windo.a.b.a;
import com.windo.a.b.a.b;
import com.windo.a.b.a.e;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

public final class d extends bb {
    private static d f;
    ProgressDialog a;
    String b;
    boolean c;
    private boolean d = false;
    private boolean e;

    private d() {
    }

    private a a(String str, long j, long j2) {
        a b2 = e.b(str, "GET");
        String str2 = "bytes=" + j + "-";
        if (j2 > j) {
            str2 = str2 + j2;
        }
        b2.a("RANGE", str2);
        int i = 0;
        while (i < 3) {
            b.b("UpdateService", "------------Request-Range count:" + i + "---------");
            try {
                int b3 = b2.b();
                if (b3 == 200 || b3 == 206) {
                    return b2;
                }
                Thread.sleep(5000);
                i++;
                i++;
            } catch (Exception e2) {
                try {
                    Thread.sleep(5000);
                } catch (Exception e3) {
                }
            }
        }
        sendMessage(Message.obtain(this, 36866));
        return null;
    }

    public static void a() {
        f = null;
    }

    public static d b() {
        if (f == null) {
            f = new d();
        }
        return f;
    }

    private void c() {
        post(new i(this));
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0095, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0096, code lost:
        r0.printStackTrace();
        sendMessage(android.os.Message.obtain(r12, 36866));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00c1, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00c2, code lost:
        r1.printStackTrace();
        com.windo.a.b.a.b.b("UpdateService", "------------Request-Range:" + r8 + "---------");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:?, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:?, code lost:
        r0 = a(r13, (long) r8, r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00ef, code lost:
        if (r0 == null) goto L_0x00f1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00f1, code lost:
        r12.c = true;
        r0 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00f9, code lost:
        r1 = r0.d();
        r0 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0095 A[ExcHandler: ClientProtocolException (r0v6 'e' org.apache.http.client.ClientProtocolException A[CUSTOM_DECLARE]), Splitter:B:1:0x0006] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(java.lang.String r13, java.io.File r14) {
        /*
            r12 = this;
            java.lang.String r0 = "GET"
            com.windo.a.b.a r0 = com.windo.a.b.a.e.b(r13, r0)
            int r1 = r0.b()     // Catch:{ ClientProtocolException -> 0x0095, IOException -> 0x00fd }
            java.lang.String r2 = "UpdateService"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ ClientProtocolException -> 0x0095, IOException -> 0x00fd }
            r3.<init>()     // Catch:{ ClientProtocolException -> 0x0095, IOException -> 0x00fd }
            java.lang.String r4 = "------------first request code:"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ ClientProtocolException -> 0x0095, IOException -> 0x00fd }
            java.lang.StringBuilder r3 = r3.append(r1)     // Catch:{ ClientProtocolException -> 0x0095, IOException -> 0x00fd }
            java.lang.String r4 = "---------"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ ClientProtocolException -> 0x0095, IOException -> 0x00fd }
            java.lang.String r3 = r3.toString()     // Catch:{ ClientProtocolException -> 0x0095, IOException -> 0x00fd }
            com.windo.a.b.a.b.b(r2, r3)     // Catch:{ ClientProtocolException -> 0x0095, IOException -> 0x00fd }
            r2 = 200(0xc8, float:2.8E-43)
            if (r1 == r2) goto L_0x003b
            r2 = 206(0xce, float:2.89E-43)
            if (r1 == r2) goto L_0x003b
            r0 = 36866(0x9002, float:5.166E-41)
            android.os.Message r0 = android.os.Message.obtain(r12, r0)     // Catch:{ ClientProtocolException -> 0x0095, IOException -> 0x00fd }
            r12.sendMessage(r0)     // Catch:{ ClientProtocolException -> 0x0095, IOException -> 0x00fd }
        L_0x003a:
            return
        L_0x003b:
            long r4 = r0.e()     // Catch:{ ClientProtocolException -> 0x0095, IOException -> 0x00fd }
            java.io.InputStream r0 = r0.d()     // Catch:{ ClientProtocolException -> 0x0095, IOException -> 0x00fd }
            r1 = 0
            if (r0 == 0) goto L_0x011b
            java.lang.String r1 = "UpdateService"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ ClientProtocolException -> 0x0095, IOException -> 0x00fd }
            r2.<init>()     // Catch:{ ClientProtocolException -> 0x0095, IOException -> 0x00fd }
            java.lang.String r3 = "------------file size :"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ ClientProtocolException -> 0x0095, IOException -> 0x00fd }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ ClientProtocolException -> 0x0095, IOException -> 0x00fd }
            java.lang.String r3 = "---------"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ ClientProtocolException -> 0x0095, IOException -> 0x00fd }
            java.lang.String r2 = r2.toString()     // Catch:{ ClientProtocolException -> 0x0095, IOException -> 0x00fd }
            com.windo.a.b.a.b.b(r1, r2)     // Catch:{ ClientProtocolException -> 0x0095, IOException -> 0x00fd }
            java.io.FileOutputStream r6 = new java.io.FileOutputStream     // Catch:{ ClientProtocolException -> 0x0095, IOException -> 0x00fd }
            r6.<init>(r14)     // Catch:{ ClientProtocolException -> 0x0095, IOException -> 0x00fd }
            r1 = 1024(0x400, float:1.435E-42)
            byte[] r7 = new byte[r1]     // Catch:{ ClientProtocolException -> 0x0095, IOException -> 0x00fd }
            r1 = 0
            r8 = r1
        L_0x006f:
            long r1 = (long) r8     // Catch:{ ClientProtocolException -> 0x0095, IOException -> 0x00fd }
            int r1 = (r1 > r4 ? 1 : (r1 == r4 ? 0 : -1))
            if (r1 >= 0) goto L_0x007f
            boolean r1 = r12.c     // Catch:{ ClientProtocolException -> 0x0095, IOException -> 0x00fd }
            if (r1 != 0) goto L_0x007f
            int r1 = r0.read(r7)     // Catch:{ IOException -> 0x00c1, ClientProtocolException -> 0x0095 }
            r2 = -1
            if (r1 != r2) goto L_0x00a4
        L_0x007f:
            if (r0 == 0) goto L_0x0084
            r0.close()     // Catch:{ ClientProtocolException -> 0x0095, IOException -> 0x00fd }
        L_0x0084:
            r0 = r6
        L_0x0085:
            if (r0 == 0) goto L_0x008d
            r0.flush()     // Catch:{ ClientProtocolException -> 0x0095, IOException -> 0x00fd }
            r0.close()     // Catch:{ ClientProtocolException -> 0x0095, IOException -> 0x00fd }
        L_0x008d:
            boolean r0 = r12.c     // Catch:{ ClientProtocolException -> 0x0095, IOException -> 0x00fd }
            if (r0 != 0) goto L_0x003a
            r12.c()     // Catch:{ ClientProtocolException -> 0x0095, IOException -> 0x00fd }
            goto L_0x003a
        L_0x0095:
            r0 = move-exception
            r0.printStackTrace()
            r0 = 36866(0x9002, float:5.166E-41)
            android.os.Message r0 = android.os.Message.obtain(r12, r0)
            r12.sendMessage(r0)
            goto L_0x003a
        L_0x00a4:
            r11 = r1
            r1 = r0
            r0 = r11
        L_0x00a7:
            if (r0 <= 0) goto L_0x0118
            r2 = 0
            r6.write(r7, r2, r0)     // Catch:{ ClientProtocolException -> 0x0095, IOException -> 0x00fd }
            int r0 = r0 + r8
            int r2 = r0 * 100
            long r2 = (long) r2     // Catch:{ ClientProtocolException -> 0x0095, IOException -> 0x00fd }
            long r2 = r2 / r4
            int r2 = (int) r2     // Catch:{ ClientProtocolException -> 0x0095, IOException -> 0x00fd }
            r3 = 36865(0x9001, float:5.1659E-41)
            r8 = -1
            android.os.Message r2 = android.os.Message.obtain(r12, r3, r2, r8)     // Catch:{ ClientProtocolException -> 0x0095, IOException -> 0x00fd }
            r12.sendMessage(r2)     // Catch:{ ClientProtocolException -> 0x0095, IOException -> 0x00fd }
            r8 = r0
            r0 = r1
            goto L_0x006f
        L_0x00c1:
            r1 = move-exception
            r1.printStackTrace()     // Catch:{ ClientProtocolException -> 0x0095, IOException -> 0x00fd }
            r9 = 0
            java.lang.String r1 = "UpdateService"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ ClientProtocolException -> 0x0095, IOException -> 0x00fd }
            r2.<init>()     // Catch:{ ClientProtocolException -> 0x0095, IOException -> 0x00fd }
            java.lang.String r3 = "------------Request-Range:"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ ClientProtocolException -> 0x0095, IOException -> 0x00fd }
            java.lang.StringBuilder r2 = r2.append(r8)     // Catch:{ ClientProtocolException -> 0x0095, IOException -> 0x00fd }
            java.lang.String r3 = "---------"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ ClientProtocolException -> 0x0095, IOException -> 0x00fd }
            java.lang.String r2 = r2.toString()     // Catch:{ ClientProtocolException -> 0x0095, IOException -> 0x00fd }
            com.windo.a.b.a.b.b(r1, r2)     // Catch:{ ClientProtocolException -> 0x0095, IOException -> 0x00fd }
            r0.close()     // Catch:{ Exception -> 0x0116 }
        L_0x00e7:
            r10 = 0
            long r2 = (long) r8
            r0 = r12
            r1 = r13
            com.windo.a.b.a r0 = r0.a(r1, r2, r4)     // Catch:{ ClientProtocolException -> 0x0095, IOException -> 0x00fd }
            if (r0 != 0) goto L_0x00f6
            r0 = 1
            r12.c = r0     // Catch:{ ClientProtocolException -> 0x0095, IOException -> 0x00fd }
            r0 = r10
            goto L_0x007f
        L_0x00f6:
            java.io.InputStream r0 = r0.d()     // Catch:{ ClientProtocolException -> 0x0095, IOException -> 0x00fd }
            r1 = r0
            r0 = r9
            goto L_0x00a7
        L_0x00fd:
            r0 = move-exception
            r0.printStackTrace()
            java.lang.String r1 = "UpdateService"
            java.lang.String r0 = r0.toString()
            com.windo.a.b.a.b.b(r1, r0)
            r0 = 36866(0x9002, float:5.166E-41)
            android.os.Message r0 = android.os.Message.obtain(r12, r0)
            r12.sendMessage(r0)
            goto L_0x003a
        L_0x0116:
            r0 = move-exception
            goto L_0x00e7
        L_0x0118:
            r0 = r1
            goto L_0x006f
        L_0x011b:
            r0 = r1
            goto L_0x0085
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vodone.caibo.service.d.a(java.lang.String, java.io.File):void");
    }

    public final void a(boolean z) {
        Activity b2;
        if (!this.d) {
            this.e = z;
            this.d = true;
            c.a().a(this, CaiboApp.a().getString(R.string.VERSION), CaiboApp.a().getString(R.string.SID));
            if (!z && (b2 = cn.a().b()) != null) {
                Toast.makeText(b2, "正在检测新版本，请稍等", 0).show();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void b(String str, File file) {
        int i;
        int i2 = 0;
        a a2 = a(str, 0, 256000);
        if (a2 != null) {
            int c2 = a2.c();
            if (c2 == 206 || c2 == 200) {
                try {
                    String a3 = a2.a("Content-Range");
                    int intValue = Integer.valueOf(a3.substring(a3.lastIndexOf("/") + 1)).intValue();
                    InputStream d2 = a2.d();
                    FileOutputStream fileOutputStream = new FileOutputStream(file);
                    byte[] bArr = new byte[1024];
                    while (i2 < intValue && !this.c) {
                        try {
                            i = d2.read(bArr);
                        } catch (Exception e2) {
                            i = -1;
                        }
                        if (i > 0) {
                            fileOutputStream.write(bArr, 0, i);
                            int i3 = i + i2;
                            sendMessage(Message.obtain(this, 36865, (i3 * 100) / intValue, -1));
                            i2 = i3;
                        } else if (i == -1 && i2 < intValue) {
                            d2.close();
                            a a4 = a(str, (long) i2, (long) ((intValue - i2 > 256000 ? 256000 : intValue - i2) + i2));
                            if (a4 != null) {
                                d2 = a4.d();
                            } else {
                                this.c = true;
                                return;
                            }
                        }
                    }
                    if (d2 != null) {
                        d2.close();
                    }
                    fileOutputStream.flush();
                    fileOutputStream.close();
                    if (!this.c) {
                        c();
                    }
                } catch (Exception e3) {
                    e3.printStackTrace();
                    b.b("UpdateService", e3.toString());
                    sendMessage(Message.obtain(this, 36866));
                }
            }
        }
    }

    public final void handleMessage(Message message) {
        this.d = false;
        if (message.what == 36865) {
            this.a.setMessage("已下载 " + message.arg1 + "%");
        } else if (message.what == 36866) {
            this.a.cancel();
            Toast.makeText(cn.a().b(), "更新包下载失败", 0).show();
        } else if (message.what == 0) {
            Activity b2 = cn.a().b();
            String string = CaiboApp.a().getString(R.string.VERSION);
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("当前版本:");
            stringBuffer.append(string);
            stringBuffer.append(", 发现有新版本.");
            stringBuffer.append(", 是否更新?");
            new AlertDialog.Builder(b2).setTitle("软件更新").setMessage(stringBuffer.toString()).setPositiveButton("更新", new h(this, (String) message.obj)).setNegativeButton("暂不更新", (DialogInterface.OnClickListener) null).create().show();
        } else if (this.e) {
        } else {
            if (message.what == 74497) {
                Activity b3 = cn.a().b();
                String string2 = CaiboApp.a().getString(R.string.VERSION);
                StringBuffer stringBuffer2 = new StringBuffer();
                stringBuffer2.append("当前版本:");
                stringBuffer2.append(string2);
                stringBuffer2.append(",\n已是最新版,无需更新!");
                new AlertDialog.Builder(b3).setTitle("软件更新").setMessage(stringBuffer2.toString()).setPositiveButton("确定", (DialogInterface.OnClickListener) null).create().show();
                return;
            }
            int a2 = l.a(message.what);
            Activity b4 = cn.a().b();
            if (a2 != 0 && b4 != null) {
                Toast.makeText(b4, a2, 0).show();
            }
        }
    }
}
