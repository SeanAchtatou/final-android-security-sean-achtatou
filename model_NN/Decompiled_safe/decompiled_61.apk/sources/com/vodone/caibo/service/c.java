package com.vodone.caibo.service;

import android.content.Context;
import com.vodone.caibo.CaiboApp;
import com.vodone.caibo.a.g;
import com.vodone.caibo.a.h;
import com.vodone.caibo.activity.af;
import com.vodone.caibo.activity.bb;
import com.vodone.caibo.b.f;
import com.vodone.caibo.b.l;
import com.windo.a.c.d;
import com.windo.a.c.e;
import com.windo.a.d.a;
import java.util.Hashtable;

public final class c {
    private static c a;
    private d b = new d();
    private f[] c;

    private c() {
        this.b.a(new e(this.b, new a()));
    }

    public static com.vodone.caibo.b.c a(Context context) {
        com.vodone.caibo.b.c b2 = CaiboApp.a().b();
        if (b2 != null) {
            return b2;
        }
        com.vodone.caibo.b.c b3 = l.a(context).b(af.c(context, "lastAccount_nickname"));
        CaiboApp.a().a(b3);
        return b3;
    }

    public static c a() {
        if (a == null) {
            a = new c();
        }
        return a;
    }

    private short a(com.windo.a.c.f fVar, e eVar) {
        fVar.a(eVar);
        this.b.a(fVar);
        return fVar.c();
    }

    public final short a(int i) {
        return com.vodone.caibo.a.f.a(this.b, 55, (String) null, 20, i, (String) null).c();
    }

    public final short a(int i, e eVar) {
        com.vodone.caibo.b.c b2 = CaiboApp.a().b();
        if (b2 == null) {
            return -1;
        }
        com.vodone.caibo.a.f a2 = com.vodone.caibo.a.f.a(this.b, 67, b2.a, String.valueOf(0), 20, i);
        a(a2, eVar);
        return a2.c();
    }

    public final short a(int i, String str, e eVar) {
        com.vodone.caibo.a.e a2 = com.vodone.caibo.a.e.a(this.b, i, str);
        a(a2, eVar);
        return a2.c();
    }

    public final short a(com.vodone.caibo.b.d dVar, e eVar) {
        g a2 = g.a(this.b, dVar);
        a(a2, eVar);
        return a2.c();
    }

    public final short a(com.vodone.caibo.b.d dVar, String str, int i, e eVar) {
        g a2 = g.a(this.b, str, i, dVar);
        a(a2, eVar);
        return a2.c();
    }

    public final short a(e eVar) {
        com.vodone.caibo.b.c b2 = CaiboApp.a().b();
        if (b2 == null) {
            return -1;
        }
        com.vodone.caibo.a.f b3 = com.vodone.caibo.a.f.b(this.b, b2.a);
        a(b3, eVar);
        return b3.c();
    }

    public final short a(e eVar, String str, String str2) {
        g gVar = new g(this.b, 291);
        gVar.h = str;
        gVar.i = str2;
        a(gVar, eVar);
        return gVar.c();
    }

    public final short a(String str, int i, e eVar) {
        com.vodone.caibo.b.c b2 = CaiboApp.a().b();
        if (b2 == null) {
            return -1;
        }
        com.vodone.caibo.a.f a2 = com.vodone.caibo.a.f.a(this.b, 18, b2.a, 20, i, str);
        a(a2, eVar);
        return a2.c();
    }

    public final short a(String str, bb bbVar) {
        com.vodone.caibo.a.e a2 = com.vodone.caibo.a.e.a(this.b, str);
        a(a2, bbVar);
        return a2.c();
    }

    public final short a(String str, e eVar) {
        com.vodone.caibo.b.c b2 = CaiboApp.a().b();
        if (b2 == null) {
            return -1;
        }
        com.vodone.caibo.a.f a2 = com.vodone.caibo.a.f.a(this.b, 56, b2.a, 20);
        a2.b = str;
        a(a2, eVar);
        return a2.c();
    }

    public final short a(String str, String str2, int i, e eVar) {
        com.vodone.caibo.b.c b2 = CaiboApp.a().b();
        if (b2 == null) {
            return -1;
        }
        com.vodone.caibo.a.f a2 = com.vodone.caibo.a.f.a(this.b, 57, b2.a, 20, i, str);
        a2.b = str2;
        a(a2, eVar);
        return a2.c();
    }

    public final short a(String str, String str2, e eVar) {
        com.vodone.caibo.a.e a2 = com.vodone.caibo.a.e.a(this.b, str, str2);
        a(a2, eVar);
        return a2.c();
    }

    public final short a(String str, String str2, String str3, String str4, e eVar) {
        com.vodone.caibo.a.e a2 = com.vodone.caibo.a.e.a(this.b, str, str2, str3, str4);
        a(a2, eVar);
        return a2.c();
    }

    public final short a(String str, String str2, boolean z, e eVar) {
        g a2 = g.a(this.b, str, str2, z);
        a(a2, eVar);
        return a2.c();
    }

    public final short a(Hashtable hashtable, e eVar) {
        com.vodone.caibo.b.c b2 = CaiboApp.a().b();
        if (b2 == null) {
            return -1;
        }
        h a2 = h.a(this.b, b2.a, hashtable);
        a(a2, eVar);
        return a2.c();
    }

    public final void a(f[] fVarArr) {
        this.c = fVarArr;
    }

    public final d b() {
        return this.b;
    }

    public final short b(int i, e eVar) {
        com.vodone.caibo.b.c b2 = CaiboApp.a().b();
        com.vodone.caibo.a.f a2 = com.vodone.caibo.a.f.a(this.b, 36, b2 != null ? b2.a : null, 20, i, (String) null);
        a(a2, eVar);
        return a2.c();
    }

    public final short b(int i, String str, e eVar) {
        com.vodone.caibo.b.c b2 = CaiboApp.a().b();
        if (b2 == null) {
            return -1;
        }
        com.vodone.caibo.a.f a2 = com.vodone.caibo.a.f.a(this.b, 22, b2.a, 20, i, str);
        a(a2, eVar);
        return a2.c();
    }

    public final short b(e eVar) {
        com.vodone.caibo.b.c b2 = CaiboApp.a().b();
        if (b2 == null) {
            return -1;
        }
        com.vodone.caibo.a.f a2 = com.vodone.caibo.a.f.a(this.b, 17, b2.a, 20);
        a(a2, eVar);
        return a2.c();
    }

    public final short b(String str, int i, e eVar) {
        com.vodone.caibo.a.f a2 = com.vodone.caibo.a.f.a(this.b, 40, (String) null, i, 20, (String) null);
        a2.c = str;
        a(a2, eVar);
        return a2.c();
    }

    public final short b(String str, e eVar) {
        if (CaiboApp.a().b() == null) {
            return -1;
        }
        com.vodone.caibo.a.f a2 = com.vodone.caibo.a.f.a(this.b, 19, str, 20);
        a(a2, eVar);
        return a2.c();
    }

    public final short b(String str, String str2, int i, e eVar) {
        com.vodone.caibo.a.f a2 = com.vodone.caibo.a.f.a(this.b, 20, str, 20, i, str2);
        a(a2, eVar);
        return a2.c();
    }

    public final short b(String str, String str2, e eVar) {
        com.vodone.caibo.a.e b2 = com.vodone.caibo.a.e.b(this.b, str, str2);
        a(b2, eVar);
        return b2.c();
    }

    public final short b(String str, String str2, String str3, String str4, e eVar) {
        g gVar = new g(this.b, 105);
        gVar.e = str;
        gVar.g = str2;
        gVar.c = str3;
        gVar.b = str4;
        a(gVar, eVar);
        return gVar.c();
    }

    public final short b(String str, String str2, boolean z, e eVar) {
        g b2 = g.b(this.b, str, str2, z);
        a(b2, eVar);
        return b2.c();
    }

    public final void b(Context context) {
        this.c = null;
        this.b.a();
        this.b = null;
        l.a(context);
        l.a();
        com.vodone.caibo.b.g.a(context);
        com.vodone.caibo.b.g.a();
        a = null;
    }

    public final short c(int i, e eVar) {
        com.vodone.caibo.a.f a2 = com.vodone.caibo.a.f.a(this.b, 48, (String) null, 20, i, (String) null);
        a(a2, eVar);
        return a2.c();
    }

    public final short c(int i, String str, e eVar) {
        com.vodone.caibo.b.c b2 = CaiboApp.a().b();
        if (b2 == null) {
            return -1;
        }
        com.vodone.caibo.a.f a2 = com.vodone.caibo.a.f.a(this.b, 32, b2.a, 20, i, str);
        a(a2, eVar);
        return a2.c();
    }

    public final short c(e eVar) {
        com.vodone.caibo.b.c b2 = CaiboApp.a().b();
        if (b2 == null) {
            return -1;
        }
        com.vodone.caibo.a.f a2 = com.vodone.caibo.a.f.a(this.b, 66, b2.a, String.valueOf(0), 20, 1);
        a(a2, eVar);
        return a2.c();
    }

    public final short c(String str, int i, e eVar) {
        com.vodone.caibo.b.c b2 = CaiboApp.a().b();
        if (b2 == null) {
            return -1;
        }
        com.vodone.caibo.a.f a2 = com.vodone.caibo.a.f.a(this.b, 53, b2.a, 20, i, str);
        a(a2, eVar);
        return a2.c();
    }

    public final short c(String str, e eVar) {
        com.vodone.caibo.a.f a2 = com.vodone.caibo.a.f.a(this.b, 33, str, 20);
        a(a2, eVar);
        return a2.c();
    }

    public final short c(String str, String str2, int i, e eVar) {
        com.vodone.caibo.a.f a2 = com.vodone.caibo.a.f.a(this.b, 34, str, 20, i, str2);
        a(a2, eVar);
        return a2.c();
    }

    public final short c(String str, String str2, e eVar) {
        com.vodone.caibo.a.e c2 = com.vodone.caibo.a.e.c(this.b, str, str2);
        a(c2, eVar);
        return c2.c();
    }

    public final f[] c() {
        return this.c;
    }

    public final short d(int i, e eVar) {
        com.vodone.caibo.a.f a2 = com.vodone.caibo.a.f.a(this.b, 50, (String) null, 20, i, (String) null);
        a(a2, eVar);
        return a2.c();
    }

    public final short d(int i, String str, e eVar) {
        com.vodone.caibo.b.c b2 = CaiboApp.a().b();
        if (b2 == null) {
            return -1;
        }
        com.vodone.caibo.a.f a2 = com.vodone.caibo.a.f.a(this.b, 24, b2.a, 20, i, str);
        a(a2, eVar);
        return a2.c();
    }

    public final short d(e eVar) {
        com.vodone.caibo.b.c b2 = CaiboApp.a().b();
        if (b2 == null) {
            return -1;
        }
        com.vodone.caibo.a.f a2 = com.vodone.caibo.a.f.a(this.b, 21, b2.a, 20);
        a(a2, eVar);
        return a2.c();
    }

    public final short d(String str, int i, e eVar) {
        com.vodone.caibo.a.d b2 = com.vodone.caibo.a.d.b(this.b, 148, str, i);
        a(b2, eVar);
        return b2.c();
    }

    public final short d(String str, e eVar) {
        com.vodone.caibo.b.c b2 = CaiboApp.a().b();
        if (b2 == null) {
            return -1;
        }
        com.vodone.caibo.a.f a2 = com.vodone.caibo.a.f.a(this.b, 64, b2.a, str, 50, 1);
        a(a2, eVar);
        return a2.c();
    }

    public final short d(String str, String str2, int i, e eVar) {
        com.vodone.caibo.b.c b2 = CaiboApp.a().b();
        com.vodone.caibo.a.f a2 = com.vodone.caibo.a.f.a(this.b, 38, b2 != null ? b2.a : null, 20, i, str2);
        a2.b(str);
        a(a2, eVar);
        return a2.c();
    }

    public final short d(String str, String str2, e eVar) {
        com.vodone.caibo.a.e d = com.vodone.caibo.a.e.d(this.b, str, str2);
        a(d, eVar);
        return d.c();
    }

    public final short e(e eVar) {
        com.vodone.caibo.b.c b2 = CaiboApp.a().b();
        if (b2 == null) {
            return -1;
        }
        com.vodone.caibo.a.f a2 = com.vodone.caibo.a.f.a(this.b, 25, b2.a, 20);
        a(a2, eVar);
        return a2.c();
    }

    public final short e(String str, int i, e eVar) {
        com.vodone.caibo.a.d b2 = com.vodone.caibo.a.d.b(this.b, 130, str, i);
        a(b2, eVar);
        return b2.c();
    }

    public final short e(String str, e eVar) {
        com.vodone.caibo.b.c b2 = CaiboApp.a().b();
        if (b2 == null) {
            return -1;
        }
        com.vodone.caibo.a.f a2 = com.vodone.caibo.a.f.a(this.b, b2.a, str);
        a(a2, eVar);
        return a2.c();
    }

    public final short e(String str, String str2, e eVar) {
        com.vodone.caibo.a.e e = com.vodone.caibo.a.e.e(this.b, str, str2);
        a(e, eVar);
        return e.c();
    }

    public final short f(e eVar) {
        com.vodone.caibo.b.c b2 = CaiboApp.a().b();
        if (b2 == null) {
            return -1;
        }
        com.vodone.caibo.a.f a2 = com.vodone.caibo.a.f.a(this.b, 23, b2.a, 20);
        a(a2, eVar);
        return a2.c();
    }

    public final short f(String str, int i, e eVar) {
        com.vodone.caibo.a.d b2 = com.vodone.caibo.a.d.b(this.b, 132, str, i);
        a(b2, eVar);
        return b2.c();
    }

    public final short f(String str, e eVar) {
        com.vodone.caibo.b.c b2 = CaiboApp.a().b();
        com.vodone.caibo.a.f a2 = com.vodone.caibo.a.f.a(this.b, 37, b2 != null ? b2.a : null, 20);
        a2.b(str);
        a(a2, eVar);
        return a2.c();
    }

    public final short f(String str, String str2, e eVar) {
        g gVar = new g(this.b, 1793);
        gVar.e = str;
        gVar.b = str2;
        a(gVar, eVar);
        return gVar.c();
    }

    public final short g(e eVar) {
        com.vodone.caibo.b.c b2 = CaiboApp.a().b();
        com.vodone.caibo.a.f a2 = com.vodone.caibo.a.f.a(this.b, 35, b2 != null ? b2.a : null, 20);
        a(a2, eVar);
        return a2.c();
    }

    public final short g(String str, int i, e eVar) {
        com.vodone.caibo.a.d b2 = com.vodone.caibo.a.d.b(this.b, 134, str, i);
        a(b2, eVar);
        return b2.c();
    }

    public final short g(String str, e eVar) {
        com.vodone.caibo.b.c b2 = CaiboApp.a().b();
        if (b2 == null) {
            return -1;
        }
        com.vodone.caibo.a.f a2 = com.vodone.caibo.a.f.a(this.b, 39, b2.a, 20);
        a2.c = str;
        a(a2, eVar);
        return a2.c();
    }

    public final short g(String str, String str2, e eVar) {
        g gVar = new g(this.b, 99);
        gVar.f = str;
        gVar.c = str2;
        a(gVar, eVar);
        return gVar.c();
    }

    public final short h(e eVar) {
        com.vodone.caibo.a.f a2 = com.vodone.caibo.a.f.a(this.b, 41, (String) null, 20);
        a(a2, eVar);
        return a2.c();
    }

    public final short h(String str, int i, e eVar) {
        com.vodone.caibo.a.a a2 = com.vodone.caibo.a.a.a(this.b, 81, str, i, 20);
        a(a2, eVar);
        return a2.c();
    }

    public final short h(String str, e eVar) {
        com.vodone.caibo.a.f a2 = com.vodone.caibo.a.f.a(this.b, str);
        a(a2, eVar);
        return a2.c();
    }

    public final short h(String str, String str2, e eVar) {
        com.vodone.caibo.a.f b2 = com.vodone.caibo.a.f.b(this.b, str, str2);
        a(b2, eVar);
        return b2.c();
    }

    public final short i(e eVar) {
        com.vodone.caibo.a.f a2 = com.vodone.caibo.a.f.a(this.b, 49, (String) null, 20);
        a(a2, eVar);
        return a2.c();
    }

    public final short i(String str, int i, e eVar) {
        com.vodone.caibo.a.a a2 = com.vodone.caibo.a.a.a(this.b, 82, str, i, 30);
        a(a2, eVar);
        return a2.c();
    }

    public final short i(String str, e eVar) {
        com.vodone.caibo.a.d a2 = com.vodone.caibo.a.d.a(this.b, 147, str, 20);
        a(a2, eVar);
        return a2.c();
    }

    public final short i(String str, String str2, e eVar) {
        h a2 = h.a(this.b, str, str2);
        a(a2, eVar);
        return a2.c();
    }

    public final short j(e eVar) {
        com.vodone.caibo.a.f a2 = com.vodone.caibo.a.f.a(this.b, 54, (String) null, 20);
        a(a2, eVar);
        return a2.c();
    }

    public final short j(String str, e eVar) {
        com.vodone.caibo.a.d a2 = com.vodone.caibo.a.d.a(this.b, 129, str, 30);
        a(a2, eVar);
        return a2.c();
    }

    public final short j(String str, String str2, e eVar) {
        h a2 = h.a(this.b, CaiboApp.a().b().a, str, str2);
        a(a2, eVar);
        return a2.c();
    }

    public final short k(e eVar) {
        com.vodone.caibo.a.f a2 = com.vodone.caibo.a.f.a(this.b, 51, (String) null, -1);
        a(a2, eVar);
        return a2.c();
    }

    public final short k(String str, e eVar) {
        com.vodone.caibo.a.d a2 = com.vodone.caibo.a.d.a(this.b, 131, str, 30);
        a(a2, eVar);
        return a2.c();
    }

    public final short l(e eVar) {
        g gVar = new g(this.b, 257);
        a(gVar, eVar);
        return gVar.c();
    }

    public final short l(String str, e eVar) {
        com.vodone.caibo.a.d a2 = com.vodone.caibo.a.d.a(this.b, 133, str, 30);
        a(a2, eVar);
        return a2.c();
    }

    public final short m(String str, e eVar) {
        com.vodone.caibo.b.c b2 = CaiboApp.a().b();
        if (b2 == null) {
            return -1;
        }
        com.vodone.caibo.a.d a2 = com.vodone.caibo.a.d.a(this.b, 135, b2.a, str);
        a(a2, eVar);
        return a2.c();
    }

    public final short n(String str, e eVar) {
        com.vodone.caibo.b.c b2 = CaiboApp.a().b();
        if (b2 == null) {
            return -1;
        }
        com.vodone.caibo.a.d a2 = com.vodone.caibo.a.d.a(this.b, 136, b2.a, str);
        a(a2, eVar);
        return a2.c();
    }

    public final short o(String str, e eVar) {
        com.vodone.caibo.b.c b2 = CaiboApp.a().b();
        if (b2 == null) {
            return -1;
        }
        com.vodone.caibo.a.d a2 = com.vodone.caibo.a.d.a(this.b, 137, b2.a, str);
        a(a2, eVar);
        return a2.c();
    }

    public final short p(String str, e eVar) {
        com.vodone.caibo.b.c b2 = CaiboApp.a().b();
        if (b2 == null) {
            return -1;
        }
        com.vodone.caibo.a.d a2 = com.vodone.caibo.a.d.a(this.b, 144, b2.a, str);
        a(a2, eVar);
        return a2.c();
    }

    public final short q(String str, e eVar) {
        g gVar = new g(this.b, 103);
        gVar.b = str;
        a(gVar, eVar);
        return gVar.c();
    }

    public final short r(String str, e eVar) {
        g gVar = new g(this.b, 101);
        gVar.d = str;
        a(gVar, eVar);
        return gVar.c();
    }

    public final short s(String str, e eVar) {
        g gVar = new g(this.b, 102);
        gVar.d = str;
        a(gVar, eVar);
        return gVar.c();
    }

    public final short t(String str, e eVar) {
        g gVar = new g(this.b, 290);
        gVar.b = str;
        a(gVar, eVar);
        return gVar.c();
    }

    public final short u(String str, e eVar) {
        com.vodone.caibo.a.c cVar = new com.vodone.caibo.a.c(this.b, str);
        a(cVar, eVar);
        return cVar.c();
    }
}
