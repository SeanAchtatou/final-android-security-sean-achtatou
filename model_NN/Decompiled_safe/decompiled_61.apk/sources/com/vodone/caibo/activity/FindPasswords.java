package com.vodone.caibo.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.vodone.caibo.R;

public class FindPasswords extends BaseActivity implements View.OnClickListener {
    private Button a;
    private View b;
    private View c;
    private RelativeLayout d;
    private RelativeLayout e;
    private TextView f;
    private TextView k;

    public void onClick(View view) {
        Class<MobileAccount> cls = MobileAccount.class;
        Bundle bundle = new Bundle();
        if (view.equals(this.a)) {
            startActivity(new Intent(this, LoginActivity.class));
        } else if (view.equals(this.b)) {
            bundle.putString("findpass", "mobileaccount");
            Class<MobileAccount> cls2 = MobileAccount.class;
            Intent intent = new Intent(this, cls);
            intent.putExtras(bundle);
            startActivity(intent);
        } else if (view.equals(this.c)) {
            bundle.putString("findpass", "emailaccount");
            Class<MobileAccount> cls3 = MobileAccount.class;
            Intent intent2 = new Intent(this, cls);
            intent2.putExtras(bundle);
            startActivity(intent2);
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.findpasswd);
        a((byte) 0, (int) R.string.cancel, this.i);
        b((byte) 2, -1, null);
        setTitle((int) R.string.findpasswd);
        this.a = (Button) findViewById(R.id.titleBtnLeft);
        this.a.setOnClickListener(this);
        this.b = findViewById(R.id.phoneaccount);
        this.b.setOnClickListener(this);
        this.c = findViewById(R.id.emailaccount);
        this.c.setOnClickListener(this);
        this.d = (RelativeLayout) findViewById(R.id.emailaccount);
        this.d.setOnClickListener(this);
        this.e = (RelativeLayout) findViewById(R.id.phoneaccount);
        this.e.setOnClickListener(this);
        this.f = (TextView) findViewById(R.id.phonefindtext);
        this.k = (TextView) findViewById(R.id.emailfindtext);
        String string = getIntent().getExtras().getString("isnull");
        if (string.equals("nothingbind")) {
            this.f.setTextColor((int) R.color.yellow);
            this.k.setTextColor((int) R.color.yellow);
            this.d.setClickable(false);
            this.e.setClickable(false);
        } else if (string.equals("notemail")) {
            this.d.setClickable(false);
            this.k.setTextColor((int) R.color.yellow);
        } else if (string.equals("notphone")) {
            this.f.setTextColor((int) R.color.yellow);
            this.e.setClickable(false);
        }
    }
}
