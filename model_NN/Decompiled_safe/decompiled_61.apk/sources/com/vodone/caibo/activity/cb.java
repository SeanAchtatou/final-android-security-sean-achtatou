package com.vodone.caibo.activity;

import android.app.AlertDialog;
import android.content.Intent;
import android.database.Cursor;
import android.view.View;
import com.vodone.caibo.CaiboApp;
import com.vodone.caibo.R;
import com.vodone.caibo.b.d;
import com.vodone.caibo.b.n;
import com.vodone.caibo.database.a;

final class cb implements dd {
    private /* synthetic */ TimeLineActivity a;

    cb(TimeLineActivity timeLineActivity) {
        this.a = timeLineActivity;
    }

    public final void a() {
        this.a.a();
    }

    public final void a(g gVar, View view, int i) {
        if (CaiboApp.a().b() == null) {
            TimeLineActivity timeLineActivity = this.a;
            new AlertDialog.Builder(timeLineActivity).setTitle((int) R.string.choose_operation).setItems((int) R.array.message_warningLogin, new ce(timeLineActivity)).show();
        } else if (this.a.a == 0 || 1 == this.a.a || 2 == this.a.a || 18 == this.a.a || 16 == this.a.a || 17 == this.a.a || 8 == this.a.a || 19 == this.a.a || 7 == this.a.a || 24 == this.a.a) {
            Cursor a2 = this.a.e.a(i - 1);
            a.a();
            d a3 = a.a(a2, (d) null);
            if (a3.p == 0) {
                this.a.startActivity(BlogDetailsActivity.a(this.a, a3.a, 2, this.a.a));
                return;
            }
            this.a.startActivity(BlogDetailsActivity.a(this.a, a3.a, 1, this.a.a));
        } else if (9 == this.a.a || 25 == this.a.a) {
            Cursor a4 = this.a.e.a(i - 1);
            a.a();
            n a5 = a.a(a4, (n) null);
            if (this.a.l) {
                Intent intent = new Intent(this.a, RearchActivity.class);
                intent.putExtra("topic", "#" + a5.b + "#");
                this.a.setResult(-1, intent);
                this.a.finish();
                return;
            }
            this.a.startActivity(TimeLineActivity.a(this.a, a5.b));
        } else if (6 == this.a.a) {
            this.a.a(i);
        }
    }

    public final void b() {
        TimeLineActivity.b(this.a);
    }
}
