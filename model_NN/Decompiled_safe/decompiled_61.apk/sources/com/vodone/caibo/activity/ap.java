package com.vodone.caibo.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.widget.ImageView;
import android.widget.ProgressBar;
import com.vodone.caibo.R;
import com.vodone.caibo.b.g;

public final class ap extends AlertDialog {
    private String a;
    /* access modifiers changed from: private */
    public Context b;
    private ImageView c;
    /* access modifiers changed from: private */
    public ProgressBar d;

    private ap(Context context, String str) {
        super(context);
        this.a = str;
        this.b = context;
    }

    public static void a(Context context, String str) {
        new ap(context, str).show();
    }

    /* access modifiers changed from: protected */
    public final void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (this.a == null) {
            dismiss();
        }
        setContentView((int) R.layout.image_preview_dialog);
        this.d = (ProgressBar) findViewById(R.id.progressBar);
        this.c = (ImageView) findViewById(R.id.imageView);
        Bitmap b2 = g.a(this.b).b(this.a);
        if (b2 != null) {
            this.d.setVisibility(8);
            this.c.setImageBitmap(b2);
            return;
        }
        this.d.setVisibility(0);
        g.a(this.b).b(this.a, new i(this, this.c));
    }

    public final boolean onKeyDown(int i, KeyEvent keyEvent) {
        dismiss();
        return true;
    }

    public final boolean onTouchEvent(MotionEvent motionEvent) {
        dismiss();
        return true;
    }
}
