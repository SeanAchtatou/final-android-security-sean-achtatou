package com.vodone.caibo.activity;

import android.content.DialogInterface;

final class cc implements DialogInterface.OnClickListener {
    private /* synthetic */ TimeLineActivity a;

    cc(TimeLineActivity timeLineActivity) {
        this.a = timeLineActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        if (i == 0) {
            this.a.startActivity(PersonalInformationActivity.a(this.a, this.a.x.e, this.a.x.f, 0));
        } else if (i == 1) {
            this.a.startActivity(SendblogActivity.a(this.a, this.a.x.h));
        }
    }
}
