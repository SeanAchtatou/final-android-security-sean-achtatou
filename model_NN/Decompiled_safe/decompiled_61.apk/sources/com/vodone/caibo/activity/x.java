package com.vodone.caibo.activity;

import android.text.Editable;
import android.text.TextWatcher;

final class x implements TextWatcher {
    private /* synthetic */ LoginActivity a;

    x(LoginActivity loginActivity) {
        this.a = loginActivity;
    }

    public final void afterTextChanged(Editable editable) {
    }

    public final void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public final void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        String valueOf = String.valueOf(charSequence);
        if (i3 == 0 || i3 == 1) {
            this.a.f.setText("");
            this.a.k.setChecked(false);
            return;
        }
        this.a.c(valueOf);
    }
}
