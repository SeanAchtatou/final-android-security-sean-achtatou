package com.vodone.caibo.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Message;
import android.widget.Toast;
import com.vodone.caibo.CaiboApp;
import com.vodone.caibo.R;
import com.vodone.caibo.b.l;
import com.vodone.caibo.b.m;
import com.vodone.caibo.service.c;

public final class ar extends bb {
    private Context a;
    private ProgressDialog b;
    private String c;
    private String d;
    private boolean e;
    private String f;

    public ar(Context context, ProgressDialog progressDialog, String str, String str2, boolean z) {
        this.a = context;
        this.b = progressDialog;
        this.c = str2;
        this.d = str;
        this.e = z;
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        c.a().a(new f(this, str));
    }

    public final void a(int i, int i2, int i3, Object obj) {
        if (i == 0) {
            m mVar = (m) obj;
            this.f = mVar.b;
            af.a(this.a, "headUrl", this.f);
            com.vodone.caibo.b.c cVar = new com.vodone.caibo.b.c();
            cVar.b = mVar.c;
            cVar.a = mVar.a;
            cVar.d = this.e;
            l a2 = l.a(this.a);
            cVar.c = this.c;
            if (a2.c(cVar.b)) {
                a2.a(cVar.b, cVar);
            } else {
                a2.a(cVar);
            }
            af.a(this.a, "lastAccout_loginname", this.d);
            af.a(this.a, "lastAccount_nickname", cVar.b);
            super.a(i, i2, i3, cVar);
        }
    }

    public final void b(int i, int i2, int i3, Object obj) {
        super.a(i, i2, i3, obj);
    }

    public final void handleMessage(Message message) {
        int i = message.what;
        if (this.b != null && this.b.isShowing()) {
            this.b.dismiss();
            this.b = null;
        }
        if (i == 0) {
            CaiboApp.a().a((com.vodone.caibo.b.c) message.obj);
            cn.a().d();
            this.a.startActivity(new Intent(this.a, MainTabActivity.class));
            Toast.makeText(this.a, (int) R.string.login_succeed, 1).show();
            a(((com.vodone.caibo.b.c) message.obj).a);
            return;
        }
        int a2 = l.a(i);
        if (a2 != 0) {
            Toast.makeText(this.a, a2, 0).show();
        }
    }
}
