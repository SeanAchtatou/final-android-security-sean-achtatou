package com.vodone.caibo.activity;

import android.content.Intent;
import android.os.Message;
import android.widget.Toast;
import com.vodone.caibo.CaiboApp;
import com.vodone.caibo.R;
import com.vodone.caibo.b.c;
import com.vodone.caibo.b.l;

final class av extends bb {
    private /* synthetic */ RegisterAcitivity a;

    av(RegisterAcitivity registerAcitivity) {
        this.a = registerAcitivity;
    }

    public final void a(int i, int i2, int i3, Object obj) {
        c cVar = (c) obj;
        if (i == 0) {
            this.a.a.dismiss();
            if (cVar != null) {
                String obj2 = this.a.d.getText().toString();
                cVar.d = false;
                cVar.c = obj2;
                l a2 = l.a(this.a);
                if (a2.c(cVar.b)) {
                    a2.a(cVar.b, cVar);
                } else {
                    a2.a(cVar);
                }
            }
            super.a(i, i2, i3, obj);
        }
    }

    public final void handleMessage(Message message) {
        int a2;
        int i = message.what;
        this.a.a.dismiss();
        if (i == 0) {
            CaiboApp.a().a((c) message.obj);
            this.a.startActivityForResult(new Intent(this.a, BindAccountActivity.class), 3);
            Toast.makeText(this.a, (int) R.string.register_succeed, 1).show();
            return;
        }
        switch (i) {
            case 513:
                a2 = l.a(513);
                break;
            case 514:
            default:
                a2 = l.a(message.what);
                break;
            case 515:
                this.a.c.selectAll();
                a2 = l.a(515);
                break;
            case 516:
                this.a.c.selectAll();
                a2 = l.a(516);
                break;
            case 517:
                a2 = l.a(517);
                break;
        }
        if (a2 != 0) {
            Toast.makeText(this.a, a2, 1).show();
        }
    }
}
