package com.vodone.caibo.activity.zoom;

import java.util.Observable;

public final class c extends Observable {
    private float a;
    private float b;
    private float c;

    public c() {
        a(0.5f);
        b(0.5f);
        c(1.0f);
        notifyObservers();
    }

    public final float a() {
        return this.b;
    }

    public final void a(float f) {
        if (f != this.b) {
            this.b = f;
            setChanged();
        }
    }

    public final float b() {
        return this.c;
    }

    public final void b(float f) {
        if (f != this.c) {
            this.c = f;
            setChanged();
        }
    }

    public final float c() {
        return this.a;
    }

    public final void c(float f) {
        if (f != this.a) {
            this.a = f;
            setChanged();
        }
    }

    public final float d(float f) {
        return Math.min(this.a, this.a * f);
    }

    public final float e(float f) {
        return Math.min(this.a, this.a / f);
    }
}
