package com.vodone.caibo.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import com.vodone.caibo.CaiboApp;
import com.vodone.caibo.R;
import com.vodone.caibo.b.c;
import com.vodone.caibo.b.l;
import com.vodone.caibo.service.d;

public class LoginActivity extends BaseActivity implements View.OnClickListener {
    ArrayAdapter a;
    private Button b;
    private Button c;
    private Button d;
    private EditText e;
    /* access modifiers changed from: private */
    public EditText f;
    /* access modifiers changed from: private */
    public CheckBox k;
    private AutoCompleteTextView l;
    private View m;
    private View n;

    /* access modifiers changed from: private */
    public void c(String str) {
        l a2 = l.a(this);
        if (a2.c(str)) {
            c b2 = a2.b(str);
            this.k.setChecked(b2.d);
            if (b2.d) {
                this.f.setText(b2.c);
                this.f.selectAll();
                return;
            }
            this.f.setText("");
        }
    }

    public void onClick(View view) {
        boolean z;
        if (view.equals(this.c)) {
            startActivity(new Intent(this, RegisterAcitivity.class));
        } else if (view.equals(this.b)) {
            String obj = this.e.getText().toString();
            String obj2 = this.f.getText().toString();
            if (obj == null || obj.length() == 0 || obj2 == null || obj2.length() == 0) {
                b((int) R.string.notnull);
                z = false;
            } else {
                z = true;
            }
            if (z) {
                String obj3 = this.e.getText().toString();
                String obj4 = this.f.getText().toString();
                ProgressDialog show = ProgressDialog.show(this, getString(R.string.waiting), getString(R.string.loding));
                short a2 = com.vodone.caibo.service.c.a().a(obj3, obj4, new ar(this, show, obj3, obj4, this.k.isChecked()));
                show.setCancelable(true);
                show.setOnCancelListener(new ao(a2));
            }
        } else if (view.equals(this.d)) {
            startActivity(new Intent(this, FindPasswordActivity.class));
        } else if (view.equals(this.n)) {
            startActivity(TimeLineActivity.b(this));
        } else if (view.equals(this.m)) {
            startActivity(TimeLineActivity.b(this, "mrcb"));
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.login);
        a((byte) 0, (int) R.string.findpasswd, this);
        b((byte) 2, -1, null);
        setTitle((int) R.string.app_name);
        this.b = (Button) findViewById(R.id.login);
        this.b.setOnClickListener(this);
        this.c = (Button) findViewById(R.id.regist);
        this.c.setOnClickListener(this);
        findViewById(R.id.blogpzza).setOnClickListener(this);
        this.k = (CheckBox) findViewById(R.id.remeberpass);
        this.e = (EditText) findViewById(R.id.UserName);
        this.e.addTextChangedListener(new x(this));
        this.d = (Button) findViewById(R.id.titleBtnLeft);
        this.d.setOnClickListener(this);
        this.f = (EditText) findViewById(R.id.PassWord);
        this.f.setOnClickListener(this);
        this.l = (AutoCompleteTextView) findViewById(R.id.UserName);
        this.l.setOnItemClickListener(new y(this));
        this.n = findViewById(R.id.blogpzza);
        this.m = findViewById(R.id.famousblog);
        this.n.setOnClickListener(this);
        this.m.setOnClickListener(this);
        Bundle extras = getIntent().getExtras();
        if (extras == null || extras.getString("username") == null) {
            String c2 = af.c(this, "lastAccout_loginname");
            String c3 = af.c(this, "lastAccount_nickname");
            if (c3 != null) {
                this.e.setText(c2);
                c(c3);
            }
        } else {
            this.e.setText(extras.getString("username"));
            if (extras.getString("password") != null) {
                this.f.setText(extras.getString("password"));
            }
        }
        String[] b2 = l.a(this).b();
        if (b2 != null && b2.length > 0) {
            this.a = new ArrayAdapter(this, 17367050, b2);
            this.l.setAdapter(this.a);
        }
        d.b().a(true);
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 4 && cn.a().a.size() <= 1) {
            CaiboApp.a().c();
            System.exit(0);
        }
        return super.onKeyDown(i, keyEvent);
    }
}
