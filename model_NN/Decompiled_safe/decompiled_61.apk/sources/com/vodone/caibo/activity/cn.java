package com.vodone.caibo.activity;

import android.app.Activity;
import java.util.ArrayList;

public final class cn {
    private static cn b;
    ArrayList a = new ArrayList();

    private cn() {
    }

    public static cn a() {
        if (b == null) {
            b = new cn();
        }
        return b;
    }

    public final Activity b() {
        if (this.a.size() > 0) {
            return (Activity) this.a.get(this.a.size() - 1);
        }
        return null;
    }

    public final MessageGroup c() {
        for (int i = 0; i < this.a.size(); i++) {
            if (this.a.get(i) instanceof MessageGroup) {
                return (MessageGroup) this.a.get(i);
            }
        }
        return null;
    }

    public final void d() {
        while (this.a.size() > 0) {
            ((Activity) this.a.remove(this.a.size() - 1)).finish();
        }
    }
}
