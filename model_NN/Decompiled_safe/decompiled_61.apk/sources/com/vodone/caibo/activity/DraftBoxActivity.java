package com.vodone.caibo.activity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;
import com.vodone.caibo.CaiboApp;
import com.vodone.caibo.R;
import com.vodone.caibo.b.d;
import com.vodone.caibo.database.a;

public class DraftBoxActivity extends BaseActivity implements View.OnClickListener, AdapterView.OnItemClickListener {
    private ListView a;
    private Button b;
    private dp c;
    private boolean d;

    public void onClick(View view) {
        if (view.equals(this.b)) {
            if (this.d) {
                this.c.a(false);
                this.b.setText((int) R.string.edit);
            } else {
                this.c.a(true);
                this.b.setText((int) R.string.finish);
            }
            this.d = !this.d;
        } else if ((view instanceof Button) && view.getTag() != null) {
            dp dpVar = this.c;
            String str = CaiboApp.a().b().a;
            a.a();
            a.a(dpVar.a, str, 20, (String) view.getTag(), (String) null);
            dpVar.getCursor().requery();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.timeline1);
        a((byte) 0, (int) R.string.back, this.i);
        b((byte) 0, R.string.edit, this);
        this.g.a.setBackgroundResource(R.drawable.title_left_button);
        this.b = (Button) findViewById(R.id.titleBtnRight);
        setTitle((int) R.string.mdraft);
        this.a = (ListView) findViewById(R.id.timeline_ListView01);
        this.a.setOnItemClickListener(this);
        String str = CaiboApp.a().b().a;
        a.a();
        Cursor b2 = a.b(this, str, 20, null, null);
        dp dpVar = new dp(this, this, b2);
        this.a.setAdapter((ListAdapter) dpVar);
        this.c = dpVar;
        if (b2.getCount() == 0) {
            Toast.makeText(this, "暂无数据", 1).show();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.c.getCursor().close();
        super.onDestroy();
    }

    public void onItemClick(AdapterView adapterView, View view, int i, long j) {
        a.a();
        d a2 = a.a((Cursor) this.c.getItem(i), (d) null);
        String str = a2.c;
        String str2 = a2.l;
        String str3 = a2.a;
        Intent intent = new Intent(this, SendblogActivity.class);
        Bundle bundle = new Bundle();
        bundle.putByte("type", (byte) 7);
        bundle.putString("content", str);
        if (str2 != null) {
            bundle.putString("attach", str2);
        }
        bundle.putString("draft_id", str3);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    public void onResume() {
        super.onResume();
        if (this.c != null) {
            this.c.getCursor().requery();
        }
    }
}
