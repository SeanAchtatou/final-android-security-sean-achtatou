package com.vodone.caibo.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.vodone.caibo.CaiboApp;
import com.vodone.caibo.R;
import com.vodone.caibo.b.b;
import com.vodone.caibo.b.f;
import com.vodone.caibo.database.a;
import com.vodone.caibo.service.c;
import com.windo.a.c.e;

public class TimeLineActivity extends BaseActivity implements View.OnClickListener {
    private String A;
    private bb B = new cd(this);
    private dd C = new cb(this);
    byte a;
    String b;
    String c;
    int d = 0;
    g e;
    short f = -1;
    short k = -1;
    boolean l;
    ProgressDialog m;
    private String n;
    private String o;
    private String p;
    private String q;
    private String r = null;
    private ListView s;
    private TextView t;
    private RelativeLayout u;
    private ImageButton v;
    private ImageButton w;
    /* access modifiers changed from: private */
    public b x;
    private View y;
    private String z;

    public static Intent a(Context context) {
        Intent intent = new Intent(context, TimeLineActivity.class);
        Bundle bundle = new Bundle();
        bundle.putByte("timeline", (byte) 19);
        intent.putExtras(bundle);
        return intent;
    }

    public static Intent a(Context context, String str) {
        Intent intent = new Intent(context, TimeLineActivity.class);
        Bundle bundle = new Bundle();
        bundle.putByte("timeline", (byte) 7);
        bundle.putString("topic_title", str);
        intent.putExtras(bundle);
        return intent;
    }

    public static Intent a(Context context, String str, String str2) {
        Intent intent = new Intent(context, TimeLineActivity.class);
        Bundle bundle = new Bundle();
        bundle.putByte("timeline", (byte) 6);
        bundle.putString("topicid", str);
        bundle.putString("userid", str2);
        intent.putExtras(bundle);
        return intent;
    }

    public static Intent a(Context context, boolean z2) {
        Intent intent = new Intent(context, TimeLineActivity.class);
        Bundle bundle = new Bundle();
        bundle.putByte("timeline", (byte) 9);
        bundle.putBoolean("select", z2);
        intent.putExtras(bundle);
        return intent;
    }

    public static g a(ListView listView, Cursor cursor, byte b2) {
        return new g(listView, new cg(listView.getContext(), cursor, c(b2)));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.vodone.caibo.service.c.a(java.lang.String, com.windo.a.c.e):short
     arg types: [java.lang.String, com.vodone.caibo.activity.bb]
     candidates:
      com.vodone.caibo.service.c.a(com.windo.a.c.f, com.windo.a.c.e):short
      com.vodone.caibo.service.c.a(int, com.windo.a.c.e):short
      com.vodone.caibo.service.c.a(com.vodone.caibo.b.d, com.windo.a.c.e):short
      com.vodone.caibo.service.c.a(java.lang.String, com.vodone.caibo.activity.bb):short
      com.vodone.caibo.service.c.a(java.util.Hashtable, com.windo.a.c.e):short
      com.vodone.caibo.service.c.a(java.lang.String, com.windo.a.c.e):short */
    /* access modifiers changed from: private */
    public void a() {
        c a2 = c.a();
        switch (this.a) {
            case 0:
                CaiboApp.a().b(8);
                if (this.r == null) {
                    this.f = a2.b(this.B);
                    b(true);
                    break;
                } else {
                    this.f = a2.a(this.r, (e) this.B);
                    b(true);
                    break;
                }
            case 1:
                this.f = a2.b(this.b, this.B);
                break;
            case 2:
                this.f = a2.c(this.b, this.B);
                break;
            case 6:
                this.f = a2.c(this.c, 1, this.B);
                break;
            case 7:
                this.f = a2.g(this.o, this.B);
                break;
            case 8:
                this.f = a2.f(this.q, this.B);
                String str = this.q;
                CaiboApp a3 = CaiboApp.a();
                if (!str.equals("mrcb")) {
                    if (!str.equals("zxdt")) {
                        if (!str.equals("zxkj")) {
                            if (str.equals("gfzz")) {
                                a3.b(3);
                                break;
                            }
                        } else {
                            a3.b(1);
                            break;
                        }
                    } else {
                        a3.b(0);
                        break;
                    }
                } else {
                    a3.b(4);
                    break;
                }
                break;
            case 9:
                this.f = a2.k(this.B);
                break;
            case 16:
                this.f = a2.h(this.B);
                break;
            case 17:
                this.f = a2.i(this.B);
                break;
            case 18:
                this.f = a2.j(this.B);
                break;
            case 19:
                this.f = a2.g(this.B);
                CaiboApp.a().b(2);
                break;
            case 24:
                this.f = a2.h(this.z, 1, this.B);
                break;
            case 25:
                this.f = a2.h(this.b, this.B);
                break;
        }
        if (this.f > 0) {
            this.e.a(true);
        }
    }

    public static Intent b(Context context) {
        Intent intent = new Intent(context, TimeLineActivity.class);
        Bundle bundle = new Bundle();
        bundle.putByte("timeline", (byte) 18);
        intent.putExtras(bundle);
        return intent;
    }

    public static Intent b(Context context, String str) {
        Intent intent = new Intent(context, TimeLineActivity.class);
        Bundle bundle = new Bundle();
        bundle.putByte("timeline", (byte) 8);
        bundle.putString("other_key", str);
        intent.putExtras(bundle);
        return intent;
    }

    static /* synthetic */ void b(TimeLineActivity timeLineActivity) {
        timeLineActivity.e.b(true);
        c a2 = c.a();
        switch (timeLineActivity.a) {
            case 0:
                if (timeLineActivity.r != null) {
                    timeLineActivity.f = a2.a(timeLineActivity.p, timeLineActivity.r, timeLineActivity.d + 1, timeLineActivity.B);
                    timeLineActivity.b(true);
                    return;
                }
                timeLineActivity.k = a2.a(timeLineActivity.p, timeLineActivity.d + 1, timeLineActivity.B);
                timeLineActivity.b(true);
                return;
            case 1:
                timeLineActivity.k = a2.b(timeLineActivity.b, timeLineActivity.p, timeLineActivity.d + 1, timeLineActivity.B);
                return;
            case 2:
                timeLineActivity.f = a2.c(timeLineActivity.b, timeLineActivity.p, timeLineActivity.d + 1, timeLineActivity.B);
                return;
            case 3:
            case 4:
            case 5:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 20:
            case 21:
            case 22:
            case 23:
            default:
                return;
            case 6:
                timeLineActivity.f = c.a().c(timeLineActivity.c, timeLineActivity.d + 1, timeLineActivity.B);
                return;
            case 7:
                timeLineActivity.f = a2.b(timeLineActivity.o, timeLineActivity.d + 1, timeLineActivity.B);
                return;
            case 8:
                timeLineActivity.f = a2.d(timeLineActivity.q, timeLineActivity.p, timeLineActivity.d + 1, timeLineActivity.B);
                return;
            case 16:
                timeLineActivity.f = a2.c(timeLineActivity.d + 1, timeLineActivity.B);
                return;
            case 17:
                timeLineActivity.f = a2.d(timeLineActivity.d + 1, timeLineActivity.B);
                return;
            case 18:
                timeLineActivity.f = a2.a(timeLineActivity.d + 1);
                return;
            case 19:
                timeLineActivity.f = a2.b(timeLineActivity.d + 1, timeLineActivity.B);
                return;
            case 24:
                timeLineActivity.f = a2.h(timeLineActivity.z, timeLineActivity.d + 1, timeLineActivity.B);
                return;
        }
    }

    private static byte c(int i) {
        switch (i) {
            case 3:
                return 2;
            case 4:
                return 3;
            case 5:
                return 1;
            case 6:
                return 4;
            case 9:
                return 8;
            case 21:
                return 19;
            case 23:
                return 17;
            case 25:
                return 18;
            default:
                return 0;
        }
    }

    private void c(String str) {
        String str2 = str == null ? "listtheme" : str;
        byte c2 = c(this.a);
        if (c2 != 0 && c2 != 16) {
            return;
        }
        if (this.A == null || !this.A.equals(str2)) {
            this.A = str2;
            this.e.a(str2);
        }
    }

    public final void a(int i) {
        Cursor a2 = this.e.a(i - 1);
        a.a();
        this.x = a.a(a2, (b) null);
        new AlertDialog.Builder(this).setTitle("请选择需要的操作").setItems((int) R.array.blogComment, new cc(this)).show();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 2) {
            a();
        }
    }

    public void onClick(View view) {
        switch (this.a) {
            case 0:
                if (view.equals(this.g.a)) {
                    Intent intent = new Intent(this, SendblogActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putByte("type", (byte) 1);
                    intent.putExtras(bundle);
                    startActivity(intent);
                    return;
                } else if (view.equals(this.g.d)) {
                    a();
                    return;
                } else if (view.equals(this.t)) {
                    openContextMenu(this.t);
                    return;
                } else {
                    return;
                }
            case 6:
                if (view.equals(this.g.d)) {
                    startActivity(SendblogActivity.b(this, this.c));
                    return;
                }
                return;
            default:
                return;
        }
    }

    public boolean onContextItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 0:
                a(this.n);
                this.r = null;
                a();
                break;
            default:
                int itemId = menuItem.getItemId() - 1;
                f[] c2 = c.a().c();
                if (itemId >= 0 && itemId <= c2.length) {
                    f fVar = c2[itemId];
                    this.r = fVar.a;
                    a(getString(R.string.group) + fVar.b);
                    a();
                    break;
                }
        }
        return super.onContextItemSelected(menuItem);
    }

    public void onCreate(Bundle bundle) {
        Cursor cursor;
        super.onCreate(bundle);
        Bundle extras = getIntent().getExtras();
        this.a = extras.getByte("timeline");
        this.n = extras.getString("username");
        this.b = extras.getString("userid");
        this.o = extras.getString("topic_title");
        this.c = extras.getString("topicid");
        this.q = extras.getString("other_key");
        this.r = extras.getString("gruopid");
        this.z = extras.getString("keywords");
        this.l = extras.getBoolean("select");
        setContentView((int) R.layout.timeline);
        this.u = (RelativeLayout) findViewById(R.id.RelativeLayout_hottopic);
        this.v = (ImageButton) findViewById(R.id.hottopic_write);
        this.v.setOnClickListener(this);
        this.w = (ImageButton) findViewById(R.id.hottopic_favorited);
        this.w.setOnClickListener(this);
        this.s = (ListView) findViewById(R.id.timeline_ListView01);
        this.t = (TextView) findViewById(R.id.titleText);
        this.t.setOnClickListener(this);
        if (this.a == 0) {
            registerForContextMenu(this.t);
            this.y = findViewById(R.id.group_down);
            this.y.setVisibility(0);
        } else {
            this.y = findViewById(R.id.group_down);
            this.y.setVisibility(8);
        }
        com.vodone.caibo.b.c b2 = CaiboApp.a().b();
        String str = b2 != null ? b2.a : "99999999999999999";
        a.a();
        if (this.a != 0) {
            if (this.a == 6) {
                a.c(this, str, this.a, this.c);
            } else if (this.a == 25) {
                a.a(this, str, this.a, (String) null, (String) null);
            } else {
                a.a(this, str, this.a, (String) null, this.b);
            }
        }
        if (this.a == 9 || this.a == 25) {
            cursor = a.b(this, str, this.a, null, null);
        } else if (this.a == 6) {
            a.a();
            cursor = a.b(this, str, this.c, this.a);
        } else {
            a.a();
            cursor = a.a(this, str, this.a, this.b);
        }
        this.e = new g(this.s, new cg(this, cursor, c(this.a)));
        this.e.c = this.C;
        switch (this.a) {
            case 0:
                a(this.n);
                a((byte) 1, (int) R.drawable.writeblog_icon, this);
                b((byte) 1, R.drawable.refresh_icon, this);
                break;
            case 1:
            case 2:
            case 7:
                switch (this.a) {
                    case 1:
                        setTitle((int) R.string.blog);
                        a((byte) 0, (int) R.string.back, this.i);
                        this.g.a.setBackgroundResource(R.drawable.title_left_button);
                        b((byte) 1, R.drawable.home_icon, this.j);
                        this.g.b.setOnClickListener(this);
                        break;
                    case 2:
                        setTitle((int) R.string.fav);
                        break;
                    case 6:
                        setTitle((int) R.string.main_comment);
                        a((byte) 0, (int) R.string.back, this.i);
                        this.g.a.setBackgroundResource(R.drawable.title_left_button);
                        b((byte) 0, R.string.publishcomment, this);
                        break;
                    case 7:
                        a("#" + this.o + "#");
                        a((byte) 0, (int) R.string.back, this.i);
                        this.g.a.setBackgroundResource(R.drawable.title_left_button);
                        b((byte) 1, R.drawable.home_icon, this.j);
                        break;
                }
                a((byte) 0, (int) R.string.back, this.i);
                this.g.a.setBackgroundResource(R.drawable.title_left_button);
                b((byte) 1, R.drawable.home_icon, this.j);
                break;
            case 6:
                setTitle((int) R.string.main_comment);
                a((byte) 0, (int) R.string.back, this.i);
                this.g.a.setBackgroundResource(R.drawable.title_left_button);
                b((byte) 0, R.string.publishcomment, this);
                break;
            case 8:
            case 9:
            case 16:
            case 17:
            case 18:
            case 19:
                byte b3 = this.a;
                a((byte) 0, (int) R.string.back, this.i);
                this.g.a.setBackgroundResource(R.drawable.title_left_button);
                b((byte) 2, -1, null);
                this.g.a.setClickable(true);
                this.g.a.setEnabled(true);
                switch (b3) {
                    case 8:
                        if (!this.q.equals("mrcb")) {
                            if (!this.q.equals("zxkj")) {
                                if (!this.q.equals("zxdt")) {
                                    if (this.q.equals("gfzz")) {
                                        setTitle((int) R.string.f0square_officialgroup);
                                        break;
                                    }
                                } else {
                                    setTitle((int) R.string.square_newtrends);
                                    break;
                                }
                            } else {
                                setTitle((int) R.string.square_newlottery);
                                break;
                            }
                        } else {
                            setTitle((int) R.string.square_celebrityblog);
                            break;
                        }
                        break;
                    case 9:
                        setTitle((int) R.string.square_hottopic);
                        break;
                    case 16:
                        setTitle((int) R.string.square_hotretweet);
                        break;
                    case 17:
                        setTitle((int) R.string.square_hotcomment);
                        break;
                    case 18:
                        setTitle((int) R.string.square_freelook);
                        break;
                    case 19:
                        setTitle((int) R.string.square_specialisttalk);
                        break;
                }
            case 24:
                a(this.z);
                a((byte) 0, (int) R.string.back, this.i);
                b((byte) 2, -1, null);
                this.g.a.setBackgroundResource(R.drawable.title_left_button);
                break;
            case 25:
                setTitle((int) R.string.mtopic);
                a((byte) 0, (int) R.string.back, this.i);
                this.g.a.setBackgroundResource(R.drawable.title_left_button);
                b((byte) 1, R.drawable.home_icon, this.j);
                break;
        }
        a();
        c(af.c(this, "theme"));
    }

    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        f[] c2 = c.a().c();
        contextMenu.add(0, 0, 0, getString(R.string.group_all));
        if (c2 != null && c2.length > 0) {
            for (int i = 0; i < c2.length; i++) {
                contextMenu.add(0, i + 1, 0, c2[i].b);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.f != -1) {
            c.a().b().a(this.f);
            this.f = -1;
        }
        if (this.k != -1) {
            c.a().b().a(this.k);
            this.k = -1;
        }
        this.e.b.getCursor().close();
        String str = "99999999999999999";
        com.vodone.caibo.b.c b2 = CaiboApp.a().b();
        if (b2 != null) {
            str = b2.a;
        }
        if (this.a == 6) {
            a.a();
            a.c(this, str, this.a, this.c);
        } else if (this.a != 0) {
            a.a();
            a.a(this, str, this.a, (String) null, this.b);
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        c(af.c(this, "theme"));
        int a2 = CaiboApp.a().a(8);
        this.e.b.notifyDataSetChanged();
        if (this.a != 0 || a2 <= 0) {
            this.e.b.getCursor().requery();
        } else {
            a();
        }
    }
}
