package com.vodone.caibo.activity;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.vodone.caibo.R;

public final class g {
    ListView a;
    cg b;
    dd c;
    /* access modifiers changed from: private */
    public View d;
    private ProgressBar e;
    private TextView f;
    /* access modifiers changed from: private */
    public View g;
    private ProgressBar h;
    private TextView i;
    private boolean j;
    private boolean k;
    private AdapterView.OnItemClickListener l = new dn(this);

    public g(ListView listView, cg cgVar) {
        this.a = listView;
        this.b = cgVar;
        Context context = listView.getContext();
        this.d = LayoutInflater.from(context).inflate((int) R.layout.head_loading, (ViewGroup) null);
        this.e = (ProgressBar) this.d.findViewById(R.id.headProgress);
        this.f = (TextView) this.d.findViewById(R.id.headText);
        this.a.addHeaderView(this.d);
        this.g = LayoutInflater.from(context).inflate((int) R.layout.foot_loading, (ViewGroup) null);
        this.h = (ProgressBar) this.g.findViewById(R.id.foot_progress);
        this.i = (TextView) this.g.findViewById(R.id.foot_text);
        if (cgVar.getCount() > 0) {
            this.a.addFooterView(this.g);
        }
        this.a.setAdapter((ListAdapter) this.b);
        this.a.setOnItemClickListener(this.l);
        cgVar.b = new dl(this);
    }

    static /* synthetic */ void a(g gVar) {
        if (!gVar.k && gVar.a.getFooterViewsCount() > 0) {
            gVar.b(true);
            if (gVar.c != null) {
                gVar.c.b();
            }
        }
    }

    static /* synthetic */ void c(g gVar) {
        if (!gVar.j) {
            gVar.a(true);
            if (gVar.c != null) {
                gVar.c.a();
            }
        }
    }

    public final Cursor a(int i2) {
        return (Cursor) this.b.getItem(i2);
    }

    public final void a() {
        if (this.a.getHeaderViewsCount() != 0) {
            this.a.removeHeaderView(this.d);
        }
    }

    public final void a(String str) {
        String str2 = str == null ? "listtheme" : str;
        if (str2.equals("bubbletheme")) {
            this.a.setBackgroundResource(R.drawable.timeline_bg);
            this.a.setDividerHeight(0);
        } else if (str2.equals("listtheme")) {
            this.a.setBackgroundColor(this.a.getResources().getColor(R.color.timeline_bg));
            this.a.setDividerHeight(1);
        }
        this.b.notifyDataSetChanged();
    }

    public final void a(boolean z) {
        this.j = z;
        if (z) {
            this.e.setVisibility(0);
            this.f.setText((int) R.string.querying);
            return;
        }
        this.e.setVisibility(8);
        this.f.setText((int) R.string.refresh);
    }

    public final void b(boolean z) {
        this.k = z;
        if (this.g == null) {
            return;
        }
        if (this.b == null || this.b.getCount() != 0) {
            this.g.setVisibility(0);
            this.a.setFooterDividersEnabled(true);
            if (z) {
                this.h.setVisibility(0);
                this.i.setText((int) R.string.acquiring_data_in_progress);
                return;
            }
            this.h.setVisibility(8);
            this.i.setText((int) R.string.see_more);
            return;
        }
        this.g.setVisibility(8);
        this.a.setFooterDividersEnabled(false);
    }

    public final void c(boolean z) {
        if (z) {
            if (this.a.getFooterViewsCount() == 0) {
                this.a.addFooterView(this.g);
            }
        } else if (this.a.getFooterViewsCount() != 0) {
            this.a.removeFooterView(this.g);
        }
    }

    public final void d(boolean z) {
        this.b.getCursor().requery();
        if (z && this.a.getFooterViewsCount() == 0) {
            this.a.addFooterView(this.g);
        } else if (!z && this.a.getFooterViewsCount() > 0) {
            this.a.removeFooterView(this.g);
        }
    }
}
