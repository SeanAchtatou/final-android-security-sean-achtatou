package com.vodone.caibo.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.text.ClipboardManager;
import android.widget.Toast;
import com.vodone.caibo.R;

final class al implements DialogInterface.OnClickListener {
    private /* synthetic */ b a;

    al(b bVar) {
        this.a = bVar;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        if (i == 0) {
            SendLetterActivity sendLetterActivity = this.a.a;
            new AlertDialog.Builder(sendLetterActivity).setMessage((int) R.string.affirm_delect).setPositiveButton((int) R.string.ok, new da(sendLetterActivity)).setNegativeButton((int) R.string.cancel, new cz(sendLetterActivity)).show();
        } else if (i == 1) {
            ((ClipboardManager) this.a.a.getSystemService("clipboard")).setText(this.a.a.b.d);
            Toast.makeText(this.a.a, "已复制到剪切板中", 0).show();
        } else if (i == 2) {
            dialogInterface.dismiss();
        }
    }
}
