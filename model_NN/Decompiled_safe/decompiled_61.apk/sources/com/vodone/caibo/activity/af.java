package com.vodone.caibo.activity;

import android.content.Context;
import android.content.SharedPreferences;

public final class af {
    public static int a(Context context, String str) {
        return context.getSharedPreferences("com.vodone.caibo.setting", 0).getInt(str, -1);
    }

    public static void a(Context context, String str, int i) {
        SharedPreferences.Editor edit = context.getSharedPreferences("com.vodone.caibo.setting", 0).edit();
        edit.putInt(str, i);
        edit.commit();
    }

    public static void a(Context context, String str, String str2) {
        SharedPreferences.Editor edit = context.getSharedPreferences("com.vodone.caibo.setting", 0).edit();
        if (str2 == null) {
            edit.remove(str);
        } else {
            edit.putString(str, str2);
        }
        edit.commit();
    }

    public static void a(Context context, String str, boolean z) {
        SharedPreferences.Editor edit = context.getSharedPreferences("com.vodone.caibo.setting", 0).edit();
        edit.putBoolean(str, z);
        edit.commit();
    }

    public static boolean b(Context context, String str) {
        return context.getSharedPreferences("com.vodone.caibo.setting", 0).getBoolean(str, true);
    }

    public static String c(Context context, String str) {
        return context.getSharedPreferences("com.vodone.caibo.setting", 0).getString(str, null);
    }
}
