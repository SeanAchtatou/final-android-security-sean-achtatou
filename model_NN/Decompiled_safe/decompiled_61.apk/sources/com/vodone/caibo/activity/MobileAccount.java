package com.vodone.caibo.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.vodone.caibo.R;
import com.vodone.caibo.service.c;

public class MobileAccount extends BaseActivity implements View.OnClickListener {
    public int a;
    ProgressDialog b;
    private String c;
    private String d;
    private String e;
    private String f;
    private String k;
    /* access modifiers changed from: private */
    public Button l;
    /* access modifiers changed from: private */
    public Button m;
    private TextView n;
    private EditText o;
    private EditText p;
    private TextView q;
    /* access modifiers changed from: private */
    public Button r;
    private EditText s;
    private String t = null;
    private String u;
    private bb v = new cu(this);

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
    }

    public void onClick(View view) {
        if (this.t.equals("mobileaccount")) {
            if (view.equals(this.l)) {
                Bundle bundle = new Bundle();
                this.e = this.p.getText().toString();
                bundle.putString("phone", this.d);
                bundle.putString("phonecode", this.e);
                if (this.e.length() != 0) {
                    Intent intent = new Intent(this, ReSetPasswd.class);
                    intent.putExtras(bundle);
                    startActivity(intent);
                    return;
                }
                b((int) R.string.checkcode_null);
            } else if (view.equals(this.m)) {
                this.d = this.o.getText().toString();
                if (this.d.equals("")) {
                    b((int) R.string.numbernull);
                } else if (!this.d.equals(this.c)) {
                    b((int) R.string.phone_fail);
                } else {
                    this.a = 0;
                    this.b = ProgressDialog.show(this, getString(R.string.waiting), getString(R.string.loding));
                    this.b.setOnCancelListener(new ao(c.a().a(this.a, this.u, this.v)));
                    this.b.setCancelable(true);
                }
            }
        } else if (!this.t.equals("emailaccount")) {
        } else {
            if (view.equals(this.g.d)) {
                startActivity(new Intent(this, EmailForgotPassword.class));
            } else if (view.equals(this.r)) {
                this.k = this.s.getText().toString();
                if (this.k.equals("")) {
                    b((int) R.string.emailnull);
                } else if (!this.k.equals(this.f)) {
                    b((int) R.string.emailmismaching);
                } else {
                    this.a = 1;
                    this.b = ProgressDialog.show(this, getString(R.string.waiting), getString(R.string.loding));
                    this.b.setOnCancelListener(new ao(c.a().a(this.a, this.u, this.v)));
                    this.b.setCancelable(true);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Bundle extras = getIntent().getExtras();
        this.u = af.c(this, "findpasswd_nickName");
        this.t = extras.getString("findpass");
        if (this.t.equals("mobileaccount")) {
            setContentView((int) R.layout.oldtype);
            a((byte) 0, (int) R.string.back, this.i);
            this.g.a.setBackgroundResource(R.drawable.title_left_button);
            b((byte) 0, R.string.next, this);
            setTitle((int) R.string.findpasswd);
            this.l = (Button) findViewById(R.id.titleBtnRight);
            this.l.setOnClickListener(this);
            this.m = (Button) findViewById(R.id.findpassok);
            this.m.setOnClickListener(this);
            this.n = (TextView) findViewById(R.id.phonetext1);
            this.o = (EditText) findViewById(R.id.passwdEdit3);
            this.o.setOnClickListener(this);
            this.p = (EditText) findViewById(R.id.passcodeEdit3);
            this.p.setOnClickListener(this);
            this.c = af.c(this, "phone");
            String str = this.c;
            StringBuffer stringBuffer = new StringBuffer();
            String substring = str.substring(0, 3);
            stringBuffer.append(substring + "-XXXX-" + str.substring(7, 11));
            this.n.setText(stringBuffer);
            this.l.setClickable(false);
        } else if (this.t.equals("emailaccount")) {
            setContentView((int) R.layout.emailaccount);
            a((byte) 0, (int) R.string.back, this.i);
            this.g.a.setBackgroundResource(R.drawable.title_left_button);
            b((byte) 0, R.string.next, this);
            setTitle((int) R.string.vipaccount);
            this.q = (TextView) findViewById(R.id.emailtexts1);
            this.r = (Button) findViewById(R.id.emailbindBut);
            this.r.setOnClickListener(this);
            this.s = (EditText) findViewById(R.id.EmailEdit1);
            this.f = af.c(this, "emailss");
            String str2 = this.f;
            StringBuffer stringBuffer2 = new StringBuffer();
            String substring2 = str2.substring(0, 5);
            stringBuffer2.append(substring2 + "*****" + str2.substring(10, str2.length()));
            this.q.setText(stringBuffer2);
            this.g.d.setClickable(false);
        }
    }
}
