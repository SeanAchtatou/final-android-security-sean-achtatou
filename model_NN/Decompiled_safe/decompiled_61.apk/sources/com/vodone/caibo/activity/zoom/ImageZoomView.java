package com.vodone.caibo.activity.zoom;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.widget.ImageView;
import java.util.Observable;
import java.util.Observer;

public class ImageZoomView extends ImageView implements Observer {
    private c a;
    private final Paint b = new Paint(2);
    private final Rect c = new Rect();
    private final Rect d = new Rect();
    private e e = new e();

    public ImageZoomView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(new c());
    }

    private void b() {
        Bitmap bitmap;
        if (getDrawable() != null && (bitmap = ((BitmapDrawable) getDrawable()).getBitmap()) != null) {
            this.e.a((float) getWidth(), (float) getHeight(), (float) bitmap.getWidth(), (float) bitmap.getHeight());
            this.e.notifyObservers();
        }
    }

    public final e a() {
        return this.e;
    }

    public final void a(c cVar) {
        if (this.a != null) {
            this.a.deleteObserver(this);
        }
        this.a = cVar;
        if (this.a != null) {
            this.a.addObserver(this);
        }
        invalidate();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        Bitmap bitmap = ((BitmapDrawable) getDrawable()).getBitmap();
        if (this.a == null) {
            super.draw(canvas);
        } else if (bitmap != null) {
            int width = getWidth();
            int height = getHeight();
            int width2 = bitmap.getWidth();
            int height2 = bitmap.getHeight();
            float a2 = this.a.a();
            float b2 = this.a.b();
            float d2 = (this.a.d(this.e.a()) * ((float) width)) / ((float) width2);
            float e2 = (this.a.e(this.e.a()) * ((float) height)) / ((float) height2);
            this.c.left = (int) ((a2 * ((float) width2)) - (((float) width) / (d2 * 2.0f)));
            this.c.top = (int) ((b2 * ((float) height2)) - (((float) height) / (e2 * 2.0f)));
            this.c.right = (int) ((((float) width) / d2) + ((float) this.c.left));
            this.c.bottom = (int) ((((float) height) / e2) + ((float) this.c.top));
            this.d.left = getLeft();
            this.d.top = getTop();
            this.d.right = getRight();
            this.d.bottom = getBottom();
            if (this.c.left < 0) {
                Rect rect = this.d;
                rect.left = (int) (((float) rect.left) + (((float) (-this.c.left)) * d2));
                this.c.left = 0;
            }
            if (this.c.right > width2) {
                Rect rect2 = this.d;
                rect2.right = (int) (((float) rect2.right) - (((float) (this.c.right - width2)) * d2));
                this.c.right = width2;
            }
            if (this.c.top < 0) {
                Rect rect3 = this.d;
                rect3.top = (int) (((float) rect3.top) + (((float) (-this.c.top)) * e2));
                this.c.top = 0;
            }
            if (this.c.bottom > height2) {
                Rect rect4 = this.d;
                rect4.bottom = (int) (((float) rect4.bottom) - (((float) (this.c.bottom - height2)) * e2));
                this.c.bottom = height2;
            }
            canvas.drawBitmap(bitmap, this.c, this.d, this.b);
            System.out.println("mRectDst" + this.d.toString());
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        b();
    }

    public void setImageBitmap(Bitmap bitmap) {
        super.setImageBitmap(bitmap);
        b();
        invalidate();
    }

    public void update(Observable observable, Object obj) {
        invalidate();
    }
}
