package com.vodone.caibo.activity;

import android.view.View;

final class ak implements View.OnClickListener {
    private /* synthetic */ PreviewActivity a;

    ak(PreviewActivity previewActivity) {
        this.a = previewActivity;
    }

    public final void onClick(View view) {
        this.a.k.c(this.a.k.c() * 2.0f);
        Boolean bool = true;
        this.a.k.notifyObservers(bool);
        if (this.a.k.c() >= 6.0f) {
            this.a.d.setIsZoomInEnabled(!bool.booleanValue());
            this.a.d.setIsZoomOutEnabled(bool.booleanValue());
            return;
        }
        this.a.d.setIsZoomOutEnabled(bool.booleanValue());
        this.a.d.setIsZoomInEnabled(bool.booleanValue());
    }
}
