package com.vodone.caibo.activity;

import android.os.Message;
import android.widget.Toast;
import com.vodone.caibo.R;

final class cu extends bb {
    private /* synthetic */ MobileAccount a;

    cu(MobileAccount mobileAccount) {
        this.a = mobileAccount;
    }

    public final void handleMessage(Message message) {
        int i = message.what;
        if (this.a.b != null && this.a.b.isShowing()) {
            this.a.b.dismiss();
            this.a.b = null;
        }
        if (i != 0) {
            int a2 = l.a(message.what);
            if (a2 != 0) {
                Toast.makeText(this.a, a2, 1).show();
            } else if (message.obj != null) {
                Toast.makeText(this.a, (String) message.obj, 0).show();
            }
        } else if (this.a.a == 0) {
            this.a.m.setClickable(false);
            this.a.m.setTextColor((int) R.color.yellow);
            this.a.m.setText((int) R.string.codesend);
            this.a.b((int) R.string.getauth_code_succeed);
            this.a.l.setClickable(true);
        } else {
            this.a.r.setTextColor((int) R.color.yellow);
            this.a.r.setText((int) R.string.emailsend);
            this.a.r.setClickable(false);
            Toast.makeText(this.a, (int) R.string.sendemail_rev, 0).show();
            this.a.g.d.setClickable(true);
        }
    }
}
