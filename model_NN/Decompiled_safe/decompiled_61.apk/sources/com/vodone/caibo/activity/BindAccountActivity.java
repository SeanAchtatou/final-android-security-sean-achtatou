package com.vodone.caibo.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.vodone.caibo.CaiboApp;
import com.vodone.caibo.R;

public class BindAccountActivity extends BaseActivity implements View.OnClickListener {
    private View a;
    private View b;
    private Button c;
    private String d;

    public void onClick(View view) {
        Class<BindActivity> cls = BindActivity.class;
        Bundle bundle = new Bundle();
        if (view.equals(this.a)) {
            bundle.putString("bind", "mobilebind");
            bundle.putString("usernamee", this.d);
            Class<BindActivity> cls2 = BindActivity.class;
            Intent intent = new Intent(this, cls);
            intent.putExtras(bundle);
            startActivity(intent);
        } else if (view.equals(this.b)) {
            bundle.putString("bind", "emailbind");
            bundle.putString("usernamee", this.d);
            Class<BindActivity> cls3 = BindActivity.class;
            Intent intent2 = new Intent(this, cls);
            intent2.putExtras(bundle);
            startActivity(intent2);
        } else if (view.equals(this.c)) {
            startActivity(new Intent(this, PerfectActivity.class));
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.accountbind);
        a((byte) 0, (int) R.string.back, this.i);
        this.g.a.setBackgroundResource(R.drawable.title_left_button);
        b((byte) 0, R.string.jump, this);
        setTitle((int) R.string.accountbind);
        this.a = findViewById(R.id.mMobileBind);
        this.a.setOnClickListener(this);
        this.b = findViewById(R.id.mEmailBind);
        this.b.setOnClickListener(this);
        this.c = (Button) findViewById(R.id.titleBtnRight);
        this.c.setOnClickListener(this);
        this.d = CaiboApp.a().b().b;
        af.a(this, "bindskip", 1);
    }
}
