package com.vodone.caibo.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.vodone.caibo.R;
import com.windo.widget.WithIconButton;

public class BaseActivity extends Activity {
    private View a;
    BaseLayout g;
    LayoutInflater h;
    public View.OnClickListener i = new dc(this);
    public View.OnClickListener j = new de(this);

    private static void a(byte b, int i2, WithIconButton withIconButton) {
        switch (b) {
            case 0:
                withIconButton.setVisibility(0);
                withIconButton.setText(i2);
                withIconButton.a((Drawable) null);
                return;
            case 1:
                withIconButton.setVisibility(0);
                withIconButton.a(i2);
                withIconButton.setText((CharSequence) null);
                return;
            case 2:
                withIconButton.setVisibility(8);
                return;
            default:
                return;
        }
    }

    public final void a(byte b, int i2, View.OnClickListener onClickListener) {
        a(b, i2, this.g.a);
        this.g.a.setOnClickListener(onClickListener);
        if (i2 == R.string.back) {
            WithIconButton withIconButton = this.g.a;
            this.g.a.setPadding(10, withIconButton.getPaddingTop(), withIconButton.getPaddingRight(), withIconButton.getPaddingBottom());
        }
    }

    public final void a(String str) {
        this.g.e.setText(str);
    }

    public final void b(byte b, int i2, View.OnClickListener onClickListener) {
        a(b, i2, this.g.d);
        this.g.d.setOnClickListener(onClickListener);
    }

    public final void b(int i2) {
        b(getString(i2));
    }

    public final void b(String str) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(str);
        builder.setPositiveButton((int) R.string.ok, (DialogInterface.OnClickListener) null);
        builder.show();
    }

    public final void b(boolean z) {
        if (z) {
            this.g.f.setVisibility(0);
            this.g.d.setVisibility(8);
            return;
        }
        this.g.f.setVisibility(8);
        this.g.d.setVisibility(0);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        this.g = new BaseLayout(this);
        super.setContentView(this.g);
        cn.a().a.add(this);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        cn a2 = cn.a();
        if (a2.a.contains(this)) {
            a2.a.remove(this);
        }
    }

    public void setContentView(int i2) {
        if (this.h == null) {
            this.h = LayoutInflater.from(this);
        }
        setContentView(this.h.inflate(i2, (ViewGroup) null));
    }

    public void setContentView(View view) {
        if (this.a != null) {
            this.g.removeView(this.a);
            this.a = null;
        }
        this.g.addView(view, new LinearLayout.LayoutParams(-1, -1));
        this.a = view;
    }

    public void setTitle(int i2) {
        a(getResources().getString(i2));
    }
}
