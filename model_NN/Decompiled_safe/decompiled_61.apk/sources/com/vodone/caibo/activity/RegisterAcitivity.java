package com.vodone.caibo.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.vodone.caibo.R;
import com.vodone.caibo.service.c;

public class RegisterAcitivity extends BaseActivity implements View.OnClickListener {
    ProgressDialog a;
    private Button b;
    /* access modifiers changed from: private */
    public EditText c;
    /* access modifiers changed from: private */
    public EditText d;
    private EditText e;
    private bb f = new av(this);

    private boolean a() {
        String obj = this.c.getText().toString();
        String obj2 = this.d.getText().toString();
        String obj3 = this.e.getText().toString();
        if (obj == null || obj.length() == 0 || obj2 == null || obj2.length() == 0 || obj3 == null || obj3.length() == 0) {
            b((int) R.string.fullinfo);
            return false;
        } else if (!obj2.equals(obj3)) {
            b((int) R.string.inputdifferent);
            return false;
        } else {
            try {
                int length = obj.getBytes("gb2312").length;
                if (length > 16 || length < 4) {
                    b((int) R.string.usrename_length_limit);
                    return false;
                }
            } catch (Exception e2) {
            }
            if (obj.matches("[0-9]+")) {
                b((int) R.string.username_number_limit);
                return false;
            } else if (!obj.matches("[a-zA-Z0-9一-鿿]+")) {
                b((int) R.string.username_char_limit);
                return false;
            } else {
                int c2 = c(obj2);
                if (c2 == 0) {
                    return true;
                }
                b(c2);
                return false;
            }
        }
    }

    private static boolean a(String str, int i) {
        char charAt = str.charAt(0);
        if (charAt < '0' || charAt > '9') {
            return false;
        }
        int i2 = charAt - '0';
        for (int i3 = 1; i3 < str.length(); i3++) {
            char charAt2 = str.charAt(i3);
            if (charAt2 >= '0' && charAt2 <= '9') {
                int i4 = charAt2 - '0';
                if (i4 != i2 + i) {
                    return false;
                }
                i2 = i4;
            }
        }
        return true;
    }

    public static int c(String str) {
        if (str.matches("([0-9])\\1{" + (str.length() - 1) + "}")) {
            return R.string.password_same_number;
        }
        if (str.matches(".*[A-Z].*")) {
            return R.string.password_upper_case;
        }
        if (!str.matches("[a-z0-9_]+")) {
            return R.string.password_char_limit;
        }
        if (a(str, -1) || a(str, 1)) {
            return R.string.password_consecutive_number;
        }
        if (str.length() < 6 || str.length() > 16) {
            return R.string.password_length_limit;
        }
        return 0;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i == 3) {
            this.c.setText((CharSequence) null);
            this.d.setText((CharSequence) null);
            this.e.setText((CharSequence) null);
        }
    }

    public void onClick(View view) {
        if (view.equals(this.b) && a()) {
            String obj = this.c.getText().toString();
            String obj2 = this.d.getText().toString();
            this.e.getText().toString();
            this.a = ProgressDialog.show(this, getString(R.string.wait), getString(R.string.registering));
            short b2 = c.a().b(obj, obj2, this.f);
            this.a.setCancelable(true);
            this.a.setOnCancelListener(new ao(b2));
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.register);
        a((byte) 0, (int) R.string.back, this.i);
        this.g.a.setBackgroundResource(R.drawable.title_left_button);
        b((byte) 0, R.string.finish, this);
        setTitle((int) R.string.register);
        this.b = (Button) findViewById(R.id.titleBtnRight);
        this.b.setOnClickListener(this);
        this.c = (EditText) findViewById(R.id.nameEdit1);
        this.c.setOnClickListener(this);
        this.d = (EditText) findViewById(R.id.passEdit1);
        this.d.setOnClickListener(this);
        this.e = (EditText) findViewById(R.id.passEditok);
        this.e.setOnClickListener(this);
    }
}
