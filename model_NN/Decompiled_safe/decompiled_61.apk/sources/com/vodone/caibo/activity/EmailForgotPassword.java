package com.vodone.caibo.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.vodone.caibo.R;

public class EmailForgotPassword extends BaseActivity implements View.OnClickListener {
    private Button a;

    public void onClick(View view) {
        if (view.equals(this.g.d)) {
            String c = af.c(this, "findpasswd_nickName");
            Bundle bundle = new Bundle();
            bundle.putString("username", c);
            bundle.putString("password", "");
            Intent intent = new Intent(this, LoginActivity.class);
            intent.putExtras(bundle);
            startActivity(intent);
            cn.a().d();
        } else if (view.equals(this.a)) {
            Bundle bundle2 = new Bundle();
            bundle2.putString("findpass", "emailaccount");
            Intent intent2 = new Intent(this, MobileAccount.class);
            intent2.putExtras(bundle2);
            startActivityForResult(intent2, 1);
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        a((byte) 0, (int) R.string.back, this.i);
        this.g.a.setBackgroundResource(R.drawable.title_left_button);
        b((byte) 0, R.string.finish, this);
        setTitle((int) R.string.findpasswd);
        setContentView((int) R.layout.emailfindpassword);
        this.a = (Button) findViewById(R.id.notrev);
        this.a.setOnClickListener(this);
    }
}
