package com.vodone.caibo.activity;

import android.app.TimePickerDialog;
import android.view.View;
import android.widget.TextView;
import android.widget.TimePicker;

final class ca implements TimePickerDialog.OnTimeSetListener, View.OnClickListener {
    private TextView a;
    private String b;
    private int c;
    private /* synthetic */ SettingActivity d;

    public ca(SettingActivity settingActivity, TextView textView, String str, int i) {
        this.d = settingActivity;
        this.a = textView;
        this.b = str;
        this.c = i;
    }

    public final void onClick(View view) {
        int indexOf = this.b.indexOf(":");
        new TimePickerDialog(this.d, this, Integer.parseInt(this.b.substring(0, indexOf)), Integer.parseInt(this.b.substring(indexOf + 1, this.b.length())), true).show();
    }

    public final void onTimeSet(TimePicker timePicker, int i, int i2) {
        this.b = SettingActivity.c(i) + ":" + SettingActivity.c(i2);
        this.a.setText(this.b);
        if (this.c == 0) {
            af.a(this.d, "StartTime", this.b);
        } else {
            af.a(this.d, "EndTime", this.b);
        }
    }
}
