package com.vodone.caibo.activity;

import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import com.vodone.caibo.CaiboApp;
import com.vodone.caibo.R;
import com.vodone.caibo.b.b;
import com.vodone.caibo.b.c;
import com.vodone.caibo.b.j;
import com.vodone.caibo.database.a;
import java.util.Enumeration;
import java.util.Hashtable;

public class MessageGroup extends BaseActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {
    byte a;
    int b = 1;
    String c;
    public Hashtable d;
    b e;
    public bb f = new e(this);
    private String k;
    private String l;
    private j m;
    private String n;
    private String o;
    private c p = CaiboApp.a().b();
    private String q;

    static /* synthetic */ void a(MessageGroup messageGroup, g gVar, int i) {
        Cursor a2 = gVar.a(i - 1);
        a.a();
        messageGroup.m = a.a(a2, (j) null);
        messageGroup.o = messageGroup.m.b;
        messageGroup.n = messageGroup.m.g;
        if (messageGroup.o.equals(messageGroup.p.a)) {
            messageGroup.startActivity(SendLetterActivity.a(messageGroup, messageGroup.m.c, messageGroup.n, 2));
        } else {
            messageGroup.startActivity(SendLetterActivity.a(messageGroup, messageGroup.o, messageGroup.n, 2));
        }
    }

    private void c(byte b2) {
        int i;
        if (this.a != b2) {
            this.a = b2;
            if (this.d.containsKey(Byte.valueOf(b2))) {
                g gVar = (g) this.d.get(Byte.valueOf(b2));
                setContentView(gVar.a);
                gVar.a(af.c(this, "theme"));
                switch (b2) {
                    case 3:
                        i = 7;
                        break;
                    case 4:
                        i = 5;
                        break;
                    case 5:
                        i = 6;
                        break;
                    case 21:
                        i = 10;
                        break;
                    default:
                        i = -1;
                        break;
                }
                if (CaiboApp.a().a(i) > 0) {
                    b(b2);
                    return;
                }
                return;
            }
            ListView listView = (ListView) this.h.inflate((int) R.layout.timeline1, (ViewGroup) null);
            setContentView(listView);
            String str = CaiboApp.a().b().a;
            a.a();
            a.a(this, str, b2, (String) null, (String) null);
            g a2 = TimeLineActivity.a(listView, a.b(this, str, b2, null, null), b2);
            a2.c = new o(this, b2);
            this.d.put(Byte.valueOf(b2), a2);
            b(b2);
            c(af.c(this, "theme"));
        }
    }

    private void c(String str) {
        String str2 = str == null ? "listtheme" : str;
        if (this.a != 4 && this.a != 5) {
            return;
        }
        if (this.q == null || !this.q.equals(str2)) {
            this.q = str2;
            ((g) this.d.get(Byte.valueOf(this.a))).a(this.q);
        }
    }

    public final void a(byte b2) {
        RadioGroup radioGroup = (RadioGroup) findViewById(R.id.title_radiogroup);
        switch (b2) {
            case 3:
                radioGroup.check(R.id.titlemessage);
                return;
            case 4:
                radioGroup.check(R.id.titleat);
                return;
            case 5:
                radioGroup.check(R.id.titlecomment);
                return;
            case 21:
                radioGroup.check(R.id.titlenotification);
                return;
            default:
                return;
        }
    }

    public final void b(byte b2) {
        ((g) this.d.get(Byte.valueOf(b2))).a(true);
        com.vodone.caibo.service.c a2 = com.vodone.caibo.service.c.a();
        CaiboApp a3 = CaiboApp.a();
        switch (b2) {
            case 3:
                a2.f(this.f);
                a3.b(7);
                return;
            case 4:
                a2.d(this.f);
                a3.b(5);
                return;
            case 5:
                a2.e(this.f);
                a3.b(6);
                return;
            case 21:
                a2.c(this.f);
                a3.b(10);
                return;
            default:
                return;
        }
    }

    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        switch (radioGroup.getCheckedRadioButtonId()) {
            case R.id.titlenotification /*2131427674*/:
                c((byte) 21);
                this.g.d.setVisibility(8);
                return;
            case R.id.titleat /*2131427675*/:
                c((byte) 4);
                this.g.d.setVisibility(8);
                return;
            case R.id.titlecomment /*2131427676*/:
                c((byte) 5);
                this.g.d.setVisibility(8);
                return;
            case R.id.titlemessage /*2131427677*/:
                c((byte) 3);
                b((byte) 1, R.drawable.writeblog_icon, this);
                return;
            default:
                return;
        }
    }

    public void onClick(View view) {
        if (view.equals(this.g.d)) {
            startActivity(SendLetterActivity.a(this, null, null, 1));
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Bundle extras = getIntent().getExtras();
        this.k = extras.getString("username");
        this.l = extras.getString("userid");
        setContentView((int) R.layout.timeline1);
        this.g.b.setVisibility(0);
        this.g.d.setVisibility(8);
        this.g.a.setVisibility(8);
        this.g.c.setOnCheckedChangeListener(this);
        this.d = new Hashtable();
        ((RadioButton) findViewById(R.id.titlenotification)).setChecked(true);
        CaiboApp a2 = CaiboApp.a();
        View findViewById = findViewById(R.id.titleat);
        m mVar = new m(this, R.string.main_at, (Button) findViewById, a2.a(5));
        findViewById.setTag(mVar);
        a2.a(5, mVar);
        View findViewById2 = findViewById(R.id.titlecomment);
        m mVar2 = new m(this, R.string.main_comment, (Button) findViewById2, a2.a(6));
        findViewById2.setTag(mVar2);
        a2.a(6, mVar2);
        View findViewById3 = findViewById(R.id.titlemessage);
        m mVar3 = new m(this, R.string.main_message, (Button) findViewById3, a2.a(7));
        findViewById3.setTag(mVar3);
        a2.a(7, mVar3);
        View findViewById4 = findViewById(R.id.titlenotification);
        m mVar4 = new m(this, R.string.main_message, (Button) findViewById4, a2.a(10));
        findViewById4.setTag(mVar4);
        a2.a(10, mVar4);
        c((byte) 21);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        CaiboApp a2 = CaiboApp.a();
        String str = a2.b().a;
        Enumeration keys = this.d.keys();
        while (keys.hasMoreElements()) {
            Byte b2 = (Byte) keys.nextElement();
            ((g) this.d.get(b2)).b.getCursor().close();
            a.a();
            a.a(this, str, b2.byteValue(), (String) null, (String) null);
        }
        a2.b(5, findViewById(R.id.titleat).getHandler());
        a2.b(6, findViewById(R.id.titlecomment).getHandler());
        a2.b(7, findViewById(R.id.titlemessage).getHandler());
        a2.b(10, findViewById(R.id.titlenotification).getHandler());
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        c(af.c(this, "theme"));
        ((g) this.d.get(Byte.valueOf(this.a))).b.getCursor().requery();
        if (this.a == 3) {
            b(this.a);
        }
    }
}
