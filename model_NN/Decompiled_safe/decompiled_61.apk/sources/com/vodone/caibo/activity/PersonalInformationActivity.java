package com.vodone.caibo.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.vodone.caibo.CaiboApp;
import com.vodone.caibo.R;
import com.vodone.caibo.b.a;
import com.vodone.caibo.b.c;
import com.vodone.caibo.b.g;
import java.util.ArrayList;
import java.util.List;

public class PersonalInformationActivity extends BaseActivity implements View.OnClickListener {
    private TextView A;
    private RelativeLayout B;
    private TextView C;
    private LinearLayout D;
    private ImageView E;
    private ImageView F;
    private ImageView G;
    private ImageView H;
    private ImageView I;
    /* access modifiers changed from: private */
    public byte J;
    /* access modifiers changed from: private */
    public String K;
    private String L;
    private c M = null;
    ProgressDialog a;
    short b;
    a c;
    public bb d = new bz(this);
    private ImageView e;
    private Button f;
    private TextView k;
    private TextView l;
    private TextView m;
    private TextView n;
    private RelativeLayout o;
    private TextView p;
    private RelativeLayout q;
    private TextView r;
    private RelativeLayout s;
    private TextView t;
    private RelativeLayout u;
    private TextView v;
    private LinearLayout w;
    private RelativeLayout x;
    private TextView y;
    private RelativeLayout z;

    public static Intent a(Context context, String str, String str2, int i) {
        Intent intent = new Intent(context, PersonalInformationActivity.class);
        Bundle bundle = new Bundle();
        if (str != null) {
            bundle.putString("user_id", str);
        }
        if (str2 != null) {
            bundle.putString("user_name", str2);
        }
        bundle.putByte("activity", (byte) i);
        intent.putExtras(bundle);
        return intent;
    }

    private void a() {
        if (this.J == 2) {
            a((byte) 0, (int) R.string.back, this.i);
            this.g.a.setBackgroundResource(R.drawable.title_left_button);
            setTitle((int) R.string.mydata);
            b((byte) 1, R.drawable.refresh_icon, this);
            return;
        }
        a((byte) 0, (int) R.string.back, this.i);
        this.g.a.setBackgroundResource(R.drawable.title_left_button);
        setTitle((int) R.string.data);
        b((byte) 1, R.drawable.home_icon, this.j);
    }

    /* access modifiers changed from: private */
    public void a(int i) {
        switch (i) {
            case 0:
            case 2:
                this.f.setText((int) R.string.mattention);
                this.G.setImageResource(R.drawable.message_unclicked_icon);
                return;
            case 1:
                this.f.setText((int) R.string.mCanelAttention);
                this.G.setImageResource(R.drawable.message_unclicked_icon);
                return;
            case 3:
                this.f.setText((int) R.string.mCanelAttention);
                this.G.setImageResource(R.drawable.message_icon);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public void a(int i, int i2) {
        switch (i) {
            case 0:
            case 1:
                a(i2);
                return;
            case 2:
            case 3:
                this.f.setText((int) R.string.remove_black_user);
                this.G.setImageResource(R.drawable.message_unclicked_icon);
                return;
            default:
                return;
        }
    }

    private void b() {
        if (this.a != null && this.a.isShowing()) {
            this.a.dismiss();
        }
        this.a = null;
        if (this.J == 2) {
            this.b = com.vodone.caibo.service.c.a().i(this.K, this.L, this.d);
        } else if (this.J == 1 || this.J == 3 || this.J == 0) {
            this.b = com.vodone.caibo.service.c.a().j(this.K, this.L, this.d);
        }
        this.a = ProgressDialog.show(this, null, "正在获取个人资料.....");
        this.a.setCancelable(true);
        this.a.setOnCancelListener(new bo(this));
        b(true);
    }

    /* access modifiers changed from: private */
    public void c() {
        if (this.a != null && this.a.isShowing()) {
            this.a.dismiss();
        }
        this.a = null;
        b(false);
    }

    /* access modifiers changed from: private */
    public void d() {
        if (this.a != null && this.a.isShowing()) {
            this.a.dismiss();
        }
        this.a = null;
        this.a = ProgressDialog.show(this, null, "正在处理关系.....");
        this.a.setCancelable(true);
        this.a.setOnCancelListener(new br(this));
    }

    public final void a(a aVar) {
        if (aVar != null) {
            g a2 = g.a(this);
            Bitmap a3 = a2.a(aVar.a.b);
            if (a3 == null) {
                a3 = a2.b();
                a2.a(aVar.a.b, new cr(this.e));
            }
            this.e.setImageBitmap(a3);
            if (aVar.a.a.equals(this.M.a)) {
                af.a(this, "headUrl", aVar.a.b);
            }
            this.k.setText(aVar.a.c);
            if (!(aVar.m == 0 || aVar.n == 0)) {
                int i = aVar.m;
                new SetLocationActivity();
                new ArrayList();
                String a4 = ((com.vodone.caibo.a) SetLocationActivity.a(this).get(i - 1)).a();
                int i2 = aVar.n;
                int i3 = aVar.m;
                new SetLocationActivity();
                new ArrayList();
                List a5 = SetLocationActivity.a(this);
                this.l.setText(a4 + " " + ((com.vodone.caibo.a) a5.get(i3 - 1)).a(i3, i2, a5));
            }
            this.p.setText(String.valueOf(aVar.i));
            this.r.setText(String.valueOf(aVar.h));
            this.t.setText(String.valueOf(aVar.g));
            this.v.setText(String.valueOf(aVar.k));
            if (aVar.a.a.equals(this.K)) {
                this.n.setText(aVar.a.c);
                this.y.setText(String.valueOf(aVar.l));
                String str = CaiboApp.a().b().a;
                com.vodone.caibo.database.a.a();
                this.A.setText(String.valueOf(com.vodone.caibo.database.a.b(this, str)));
                this.C.setText(String.valueOf(aVar.j));
            } else {
                this.n.setText(aVar.e);
            }
            if (this.J == 1 || this.J == 3 || this.J == 0) {
                String str2 = this.M.b;
                String str3 = this.M.a;
                a(aVar.p, aVar.o);
                if (aVar.a.c.equals(str2) || aVar.a.a.equals(str3)) {
                    if (this.K == null) {
                        this.K = aVar.a.a;
                    }
                    this.f.setVisibility(8);
                    this.G.setClickable(false);
                    this.H.setClickable(false);
                    this.I.setClickable(false);
                    c();
                } else if (this.K == null) {
                    this.K = aVar.a.a;
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i2 == -1) {
            switch (i) {
                case 1:
                    this.K = intent.getStringExtra("userid");
                    b();
                    return;
                default:
                    return;
            }
        }
    }

    public void onClick(View view) {
        if (view.equals(this.o)) {
            startActivity(PersonMainTabActivity.a(this, this.K, this.L, 3));
        } else if (view.equals(this.s)) {
            startActivity(PersonMainTabActivity.a(this, this.K, this.L, 2));
        } else if (view.equals(this.q)) {
            startActivityForResult(PersonMainTabActivity.a(this, this.K, this.L, 0), 2);
        } else if (view.equals(this.u)) {
            startActivity(PersonMainTabActivity.a(this, this.K, this.L, 1));
        } else if (view.equals(this.f)) {
            String str = (String) this.f.getText();
            if (str.equals(getText(R.string.edit))) {
                startActivityForResult(EditInfoActivity.a(this, this.c.a.a, this.c.a.c, this.c.b, this.c.m, this.c.n, this.c.a.f, this.c.a.b), 1);
            } else if (str.equals(getText(R.string.mattention))) {
                com.vodone.caibo.service.c.a().m(this.K, this.d);
                d();
            } else if (str.equals(getText(R.string.mCanelAttention))) {
                com.vodone.caibo.service.c.a().n(this.K, this.d);
                d();
            } else if (str.equals(getText(R.string.remove_black_user))) {
                com.vodone.caibo.service.c.a().p(this.K, this.d);
                d();
            } else if (str.equals(getText(R.string.add_black_user))) {
                com.vodone.caibo.service.c.a().o(this.K, this.d);
                d();
            }
        } else if (view.equals(this.g.a)) {
            finish();
        } else if (view.equals(this.g.d)) {
            if (this.J == 2) {
                b();
            } else {
                startActivity(new Intent(this, MainTabActivity.class));
            }
        } else if (view.equals(this.E)) {
            b();
        } else if (view.equals(this.F)) {
            Intent intent = new Intent(this, SendblogActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("content", "@" + this.k.getText().toString() + " ");
            bundle.putByte("type", (byte) 8);
            intent.putExtras(bundle);
            startActivity(intent);
        } else if (view.equals(this.G)) {
            if (this.c.o == 3) {
                startActivity(SendLetterActivity.a(this, this.K, this.L, 1));
            }
        } else if (view.equals(this.H)) {
        } else {
            if (view.equals(this.I)) {
                if (this.c.p == 0 || this.c.p == 1) {
                    new AlertDialog.Builder(this).setTitle((int) R.string.addBlackTitle).setMessage((int) R.string.alertContext_addblack).setPositiveButton((int) R.string.add_black_user, new bu(this)).setNegativeButton((int) R.string.cancel, (DialogInterface.OnClickListener) null).show();
                }
            } else if (view.equals(this.B)) {
                String str2 = this.K;
                Intent intent2 = new Intent(this, FriendActivity.class);
                Bundle bundle2 = new Bundle();
                bundle2.putByte("friend_key", (byte) 3);
                bundle2.putString("friend_user_id", str2);
                intent2.putExtras(bundle2);
                startActivity(intent2);
            } else if (view.equals(this.x)) {
                String str3 = this.K;
                String str4 = this.L;
                Intent intent3 = new Intent(this, TimeLineActivity.class);
                Bundle bundle3 = new Bundle();
                bundle3.putByte("timeline", (byte) 2);
                bundle3.putString("userid", str3);
                bundle3.putString("username", str4);
                intent3.putExtras(bundle3);
                startActivity(intent3);
            } else if (view.equals(this.z)) {
                startActivity(new Intent(this, DraftBoxActivity.class));
            }
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.personal_info);
        this.M = CaiboApp.a().b();
        this.e = (ImageView) findViewById(R.id.user_header_image);
        this.k = (TextView) findViewById(R.id.nickname);
        this.f = (Button) findViewById(R.id.edit_or_attention_button);
        this.f.setOnClickListener(this);
        this.l = (TextView) findViewById(R.id.addressContext);
        this.m = (TextView) findViewById(R.id.introduceOrLoginname);
        this.n = (TextView) findViewById(R.id.introduceOrLoginnameContext);
        this.o = (RelativeLayout) findViewById(R.id.attention);
        this.o.setOnClickListener(this);
        this.p = (TextView) findViewById(R.id.attention_count);
        this.q = (RelativeLayout) findViewById(R.id.blog);
        this.q.setOnClickListener(this);
        this.r = (TextView) findViewById(R.id.blogCount);
        this.s = (RelativeLayout) findViewById(R.id.fans);
        this.s.setOnClickListener(this);
        this.t = (TextView) findViewById(R.id.FansCount);
        this.u = (RelativeLayout) findViewById(R.id.topic);
        this.u.setOnClickListener(this);
        this.v = (TextView) findViewById(R.id.TopicCount);
        this.w = (LinearLayout) findViewById(R.id.otherInfor);
        this.x = (RelativeLayout) findViewById(R.id.collectRelation);
        this.x.setOnClickListener(this);
        this.y = (TextView) findViewById(R.id.collectCount);
        this.z = (RelativeLayout) findViewById(R.id.DraftBoxRelative);
        this.z.setOnClickListener(this);
        this.A = (TextView) findViewById(R.id.drafCountText);
        this.B = (RelativeLayout) findViewById(R.id.blackListRelative);
        this.B.setOnClickListener(this);
        this.C = (TextView) findViewById(R.id.blackListCount);
        this.D = (LinearLayout) findViewById(R.id.linearLayout2);
        this.E = (ImageView) findViewById(R.id.refresh);
        this.E.setOnClickListener(this);
        this.F = (ImageView) findViewById(R.id.blogAtmo);
        this.F.setOnClickListener(this);
        this.G = (ImageView) findViewById(R.id.message);
        this.G.setOnClickListener(this);
        this.H = (ImageView) findViewById(R.id.grouping);
        this.H.setOnClickListener(this);
        this.I = (ImageView) findViewById(R.id.addBlack);
        this.I.setOnClickListener(this);
        Bundle extras = getIntent().getExtras();
        this.K = extras.getString("user_id");
        this.L = extras.getString("user_name");
        this.J = extras.getByte("activity");
        if (this.J == 2) {
            a();
            this.f.setText((int) R.string.edit);
            this.m.setText((int) R.string.loginname);
            this.w.setVisibility(0);
            this.D.setVisibility(8);
        } else {
            a();
            this.m.setText((int) R.string.introduce);
        }
        b();
    }

    public void onResume() {
        super.onResume();
        if (this.c != null && this.J == 2) {
            a(this.c);
        }
    }
}
