package com.vodone.caibo.activity;

import android.content.Intent;
import android.database.Cursor;
import android.view.View;
import com.vodone.caibo.b.m;
import com.vodone.caibo.database.a;
import com.vodone.caibo.service.c;

final class ah implements dd {
    private /* synthetic */ SearchContactActivity a;

    ah(SearchContactActivity searchContactActivity) {
        this.a = searchContactActivity;
    }

    public final void a() {
        c.a().k(this.a.m, this.a.d);
    }

    public final void a(g gVar, View view, int i) {
        if (view.equals(this.a.a)) {
            Intent intent = new Intent();
            intent.putExtra("friend_nickname_key", this.a.a.getText());
            this.a.setResult(-1, intent);
            this.a.finish();
        } else if (view.equals(this.a.b)) {
            this.a.startActivityForResult(FriendActivity.a(this.a, this.a.e.getText().toString(), this.a.m, true), 1);
        } else {
            Cursor a2 = this.a.k.a(i);
            a.a();
            String str = a.a(a2, (m) null).c;
            Intent intent2 = new Intent();
            intent2.putExtra("friend_nickname_key", "@" + str);
            this.a.setResult(-1, intent2);
            this.a.finish();
        }
    }

    public final void b() {
        c.a().f(this.a.m, this.a.n + 1, this.a.d);
    }
}
