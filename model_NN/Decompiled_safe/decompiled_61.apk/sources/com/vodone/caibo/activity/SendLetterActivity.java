package com.vodone.caibo.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.vodone.caibo.CaiboApp;
import com.vodone.caibo.R;
import com.vodone.caibo.b.c;
import com.vodone.caibo.b.j;
import com.vodone.caibo.database.a;
import java.util.Calendar;
import java.util.Hashtable;

public class SendLetterActivity extends BaseActivity implements View.OnClickListener {
    private InputMethodManager A;
    private int B = 1;
    private int C = 2;
    private int D = 3;
    private c E = CaiboApp.a().b();
    private AdapterView.OnItemClickListener F = new dh(this);
    g a;
    j b;
    short c;
    public bb d = new cy(this);
    private Hashtable e = new Hashtable();
    /* access modifiers changed from: private */
    public String f;
    private String k;
    /* access modifiers changed from: private */
    public int l;
    private String m;
    private RelativeLayout n;
    private ListView o;
    private ImageButton p;
    private ImageButton q;
    private ImageView r;
    private ImageView s;
    private LinearLayout t;
    private Button u;
    private TextView v;
    /* access modifiers changed from: private */
    public EditText w;
    /* access modifiers changed from: private */
    public AlertDialog x;
    private GridView y;
    private DisplayMetrics z;

    public static Intent a(Context context, String str, String str2, int i) {
        Intent intent = new Intent(context, SendLetterActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("toUserId", str);
        bundle.putString("toUserName", str2);
        bundle.putInt("style", i);
        intent.putExtras(bundle);
        return intent;
    }

    private static String a(int i) {
        String str = "" + i;
        return str.length() == 1 ? "0" + str : str;
    }

    /* access modifiers changed from: private */
    public void a(boolean z2) {
        if (this.A != null) {
            if (z2) {
                c(false);
                this.A.showSoftInput(this.w, 0);
                return;
            }
            this.A.hideSoftInputFromWindow(this.w.getWindowToken(), 0);
        }
    }

    /* access modifiers changed from: private */
    public void c(String str) {
        this.w.getText().insert(this.w.getSelectionStart(), str + " ");
    }

    private void c(boolean z2) {
        if (z2) {
            if (this.z == null) {
                this.z = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(this.z);
            }
            this.t.setVisibility(0);
            this.y.setLayoutParams(new LinearLayout.LayoutParams(-1, this.z.heightPixels / 3));
            return;
        }
        this.t.setVisibility(8);
    }

    public final void a() {
        com.vodone.caibo.service.c.a().d(this.f, this.d);
        b(true);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        a(false);
        c(false);
        super.onActivityResult(i, i2, intent);
        if (i2 == -1) {
            switch (i) {
                case 1:
                    c(intent.getStringExtra("topic"));
                    return;
                case 2:
                    this.v.setVisibility(0);
                    this.v.setText(intent.getStringExtra("friend_nickname_key"));
                    this.f = intent.getStringExtra("friend_user_id");
                    return;
                case 3:
                    c(intent.getStringExtra("friend_nickname_key"));
                    return;
                default:
                    return;
            }
        }
    }

    public void onClick(View view) {
        if (view.equals(this.g.a)) {
            if (!this.w.getText().toString().equals("")) {
                this.x = new AlertDialog.Builder(this).setItems((int) R.array.send_cancel_dialog, new cx(this)).show();
            } else {
                finish();
            }
        } else if (view.equals(this.g.d)) {
            a.a();
            a.a(this, this.f, 23, (String) null, (String) null);
            a();
        } else if (view.equals(this.r)) {
            String str = CaiboApp.a().b().a;
            Intent intent = new Intent(this, FriendActivity.class);
            Bundle bundle = new Bundle();
            bundle.putByte("friend_key", (byte) 5);
            bundle.putString("friend_user_id", str);
            intent.putExtras(bundle);
            startActivityForResult(intent, 2);
        } else if (view.equals(this.s)) {
            if (this.t.getVisibility() == 8) {
                c(true);
                a(false);
                return;
            }
            c(false);
            a(true);
        } else if (view.equals(this.u)) {
            if (!this.w.getText().toString().equals("") && this.f != null) {
                String obj = this.w.getText().toString();
                if (this.x != null && this.x.isShowing()) {
                    this.x.dismiss();
                }
                this.x = null;
                this.c = com.vodone.caibo.service.c.a().g(this.f, obj, this.d);
                this.x = ProgressDialog.show(this, null, "正在发送中.....");
                this.x.setCancelable(true);
                this.x.setOnCancelListener(new dg(this));
                a.a();
                String str2 = this.f;
                Calendar instance = Calendar.getInstance();
                String str3 = a(instance.get(11)) + ":" + a(instance.get(12)) + ":" + a(instance.get(13));
                String str4 = this.E.a;
                this.m = af.c(this, "headUrl");
                a.b(this, str2, "{\"recieverId\":\"" + this.f + "\",\"status\":0,\"content\":\"" + this.w.getText().toString() + "\",\"senderHead\":\"" + this.m + "\",\"type\":0,\"recieverHead\":\"http://t.diyicai.com//face/36/mid425847.jpg\",\"ytMailId\":\"" + str4 + "\",\"nickName\":\"" + this.k + "\",\"senderId\":\"" + str4 + "\",\"createDate\":\"2011-07-11 " + str3 + "\"}");
            } else if (this.w.getText().toString().equals("")) {
                Toast.makeText(this, (int) R.string.send_warn_context, 0).show();
            } else if (this.f == null) {
                Toast.makeText(this, (int) R.string.send_warn_linkman, 0).show();
            }
        } else if (view.equals(this.p)) {
            startActivityForResult(SearchContactActivity.a(this, CaiboApp.a().b().a, 2), 3);
        } else if (view.equals(this.q)) {
            startActivityForResult(RearchActivity.a(this, 2), 1);
        } else if (view.equals(this.w)) {
            c(false);
            a(true);
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.private_letter);
        Bundle extras = getIntent().getExtras();
        this.f = extras.getString("toUserId");
        this.k = extras.getString("toUserName");
        this.l = extras.getInt("style");
        if (this.l == 1) {
            a((byte) 0, (int) R.string.cancel, this);
            setTitle((int) R.string.send_letter);
            b((byte) 2, -1, null);
        } else if (this.l == 2) {
            a((byte) 0, (int) R.string.back, this.i);
            a(this.k);
            b((byte) 1, R.drawable.refresh_icon, this);
            this.g.a.setBackgroundResource(R.drawable.title_left_button);
        }
        this.n = (RelativeLayout) findViewById(R.id.ReceiverRelativeLayout);
        this.o = (ListView) findViewById(R.id.letterList);
        this.v = (TextView) findViewById(R.id.RecvicerName);
        this.r = (ImageView) findViewById(R.id.addImage);
        this.r.setOnClickListener(this);
        this.w = (EditText) findViewById(R.id.letterContext);
        this.w.setOnClickListener(this);
        this.s = (ImageView) findViewById(R.id.addcontent);
        this.s.setOnClickListener(this);
        this.t = (LinearLayout) findViewById(R.id.phiz_and_other);
        this.p = (ImageButton) findViewById(R.id.atpersonImageButton);
        this.p.setOnClickListener(this);
        this.q = (ImageButton) findViewById(R.id.topicImageButton);
        this.q.setOnClickListener(this);
        this.u = (Button) findViewById(R.id.sendButton);
        this.u.setOnClickListener(this);
        this.y = (GridView) findViewById(R.id.Grid);
        this.y.setAdapter((ListAdapter) new cs(this));
        this.y.setOnItemClickListener(this.F);
        if (this.l == 1) {
            if (this.k != null) {
                this.v.setText(this.k);
            } else {
                this.v.setVisibility(8);
            }
            this.A = (InputMethodManager) getSystemService("input_method");
            this.w.requestFocus();
            new Handler().postDelayed(new df(this), 500);
        } else if (this.l == 2) {
            this.n.setVisibility(8);
            this.o.setVisibility(0);
            a.a();
            a.a(this, this.f, 23, (String) null, (String) null);
            this.a = TimeLineActivity.a(this.o, a.b(this, this.f, 23, null, null), (byte) 23);
            this.a.c = new b(this);
            this.e.put((byte) 23, this.a);
            this.a.b(false);
            this.a.c(false);
            this.a.a();
            this.a.a(false);
            a();
            this.A = (InputMethodManager) getSystemService("input_method");
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.l == 2) {
            this.a.b.getCursor().close();
            a.a();
            a.a(this, this.f, 23, (String) null, (String) null);
        }
        super.onDestroy();
    }
}
