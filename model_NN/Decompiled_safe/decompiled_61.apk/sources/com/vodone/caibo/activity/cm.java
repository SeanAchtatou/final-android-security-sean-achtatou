package com.vodone.caibo.activity;

import android.app.ProgressDialog;
import android.view.View;
import com.vodone.caibo.service.c;

final class cm implements View.OnClickListener {
    final /* synthetic */ SettingActivity a;

    cm(SettingActivity settingActivity) {
        this.a = settingActivity;
    }

    public final void onClick(View view) {
        if (this.a.c != null && this.a.c.isShowing()) {
            this.a.c.dismiss();
        }
        this.a.c = null;
        this.a.b = c.a().l(this.a.d);
        this.a.c = ProgressDialog.show(this.a, null, "正在检测推送服务.....");
        this.a.c.setCancelable(true);
        this.a.c.setOnCancelListener(new p(this));
    }
}
