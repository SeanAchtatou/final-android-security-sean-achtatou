package com.vodone.caibo.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import com.vodone.caibo.CaiboApp;
import com.vodone.caibo.R;

public class AboutCaiboActivity extends BaseActivity implements View.OnClickListener {
    public void onClick(View view) {
        if (view.equals(this.g.a)) {
            finish();
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.about);
        a((byte) 0, (int) R.string.back, this.i);
        this.g.a.setBackgroundResource(R.drawable.title_left_button);
        b((byte) 2, -1, null);
        setTitle((int) R.string.about_theme);
        ((TextView) findViewById(R.id.VersionNumberContext)).setText(CaiboApp.a().getString(R.string.VERSION));
    }
}
