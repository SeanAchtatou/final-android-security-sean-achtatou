package com.vodone.caibo.activity;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.vodone.caibo.CaiboApp;
import com.vodone.caibo.R;
import com.vodone.caibo.b.b;
import com.vodone.caibo.b.c;
import com.vodone.caibo.b.d;
import com.vodone.caibo.b.g;
import com.vodone.caibo.b.j;
import com.vodone.caibo.b.m;
import com.vodone.caibo.b.n;
import com.vodone.caibo.database.a;
import com.windo.widget.RichTextView;
import java.util.Date;

public final class cg extends CursorAdapter {
    Context a;
    dd b;
    View.OnClickListener c = null;
    private c d;
    private byte e;
    private LayoutInflater f;
    private Date g = new Date();
    private Date h = new Date();
    private StringBuffer i = new StringBuffer();
    private d j;
    private b k;
    private m l;
    private j m;
    private n n;

    public cg(Context context, Cursor cursor, byte b2) {
        super(context, cursor);
        this.f = LayoutInflater.from(context);
        this.e = b2;
        this.a = context;
    }

    public final void bindView(View view, Context context, Cursor cursor) {
        String str;
        String str2;
        if (cursor.getPosition() == cursor.getCount() - 1 && cursor.getCount() > 0 && this.b != null) {
            this.b.b();
        }
        switch (this.e) {
            case 0:
            case 3:
            case 16:
                a.a();
                this.j = a.a(cursor, this.j);
                d dVar = this.j;
                dm dmVar = (dm) view.getTag();
                c b2 = CaiboApp.a().b();
                Boolean valueOf = Boolean.valueOf(af.b(context, "showpreivewweibo"));
                if (valueOf == null) {
                    valueOf = false;
                }
                String c2 = af.c(context, "theme");
                if (c2 == null || c2.length() == 0) {
                    c2 = "listtheme";
                }
                String c3 = af.c(context, "readmode");
                if (c3 == null) {
                    c3 = "readmode_img";
                }
                if (dVar.p == 0) {
                    dmVar.b.setVisibility(8);
                    dmVar.q.setVisibility(8);
                    dmVar.r.setVisibility(0);
                    dmVar.r.setText(dVar.c);
                    if (!c3.equals("readmode_txt")) {
                        dmVar.a.setVisibility(0);
                        dmVar.a.setImageResource(R.drawable.pushmessage_cion);
                        return;
                    }
                    dmVar.a.setVisibility(8);
                    return;
                }
                dmVar.a.setVisibility(8);
                dmVar.q.setVisibility(0);
                dmVar.r.setVisibility(8);
                if (c2.equals("bubbletheme")) {
                    dmVar.q.setBackgroundResource(R.drawable.bubble_bg);
                } else {
                    dmVar.q.setBackgroundDrawable(null);
                }
                if (valueOf.booleanValue()) {
                    dmVar.g.setFilters(new InputFilter[]{new InputFilter.LengthFilter(100)});
                    dmVar.g.setEllipsize(TextUtils.TruncateAt.END);
                    dmVar.j.setFilters(new InputFilter[]{new InputFilter.LengthFilter(50)});
                    dmVar.j.setEllipsize(TextUtils.TruncateAt.END);
                } else {
                    dmVar.g.setFilters(new InputFilter[]{new InputFilter.LengthFilter(100000)});
                    dmVar.g.setEllipsize(null);
                    dmVar.j.setFilters(new InputFilter[]{new InputFilter.LengthFilter(100000)});
                    dmVar.j.setEllipsize(null);
                }
                g a2 = g.a(context);
                if (!c3.equals("readmode_txt")) {
                    dmVar.b.setVisibility(0);
                    Bitmap a3 = a2.a(dVar.j);
                    if (a3 == null) {
                        a3 = a2.b();
                        a2.a(dVar.j, new q(this, dmVar.b));
                    }
                    dmVar.b.setImageBitmap(a3);
                } else {
                    dmVar.b.setVisibility(8);
                }
                if (b2 == null || !dVar.i.equals(b2.b)) {
                    dmVar.c.setText(dVar.i);
                } else {
                    dmVar.c.setText("我");
                }
                dmVar.f.setText(dVar.d);
                dmVar.g.setText(dVar.c);
                if (dVar.b()) {
                    dmVar.e.setVisibility(0);
                    if (c3.equals("readmode_img")) {
                        dmVar.h.setVisibility(0);
                        Bitmap b3 = a2.b(dVar.k);
                        if (b3 == null) {
                            b3 = a2.c();
                            a2.b(dVar.k, new q(this, dmVar.h));
                        }
                        dmVar.h.setImageBitmap(b3);
                        dmVar.h.setOnClickListener(new cf(this, dVar.k));
                    } else {
                        dmVar.h.setVisibility(8);
                    }
                } else {
                    dmVar.e.setVisibility(8);
                    dmVar.h.setVisibility(8);
                }
                if (dVar.c()) {
                    dmVar.i.setVisibility(0);
                    dmVar.j.setText(dVar.n.i + ":" + dVar.n.c);
                    if (!dVar.n.b() || !c3.equals("readmode_img")) {
                        dmVar.k.setVisibility(8);
                    } else {
                        dmVar.k.setVisibility(0);
                        Bitmap b4 = a2.b(dVar.n.k);
                        if (b4 == null) {
                            b4 = a2.c();
                            a2.b(dVar.n.k, new q(this, dmVar.k));
                        }
                        dmVar.k.setImageBitmap(b4);
                        dmVar.k.setOnClickListener(new cf(this, dVar.n.k));
                    }
                    if (c2.equals("bubbletheme")) {
                        dmVar.i.setBackgroundDrawable(null);
                        dmVar.s.setVisibility(0);
                    } else {
                        dmVar.i.setBackgroundResource(R.drawable.reply_bg);
                        dmVar.s.setVisibility(8);
                    }
                } else {
                    dmVar.i.setVisibility(8);
                }
                if (this.e == 3) {
                    dmVar.m.setText(dVar.e);
                    dmVar.n.setVisibility(8);
                    return;
                }
                dmVar.m.setText(dVar.e);
                dmVar.o.setText(String.valueOf(dVar.g));
                dmVar.p.setText(String.valueOf(dVar.f));
                return;
            case 1:
                a.a();
                this.k = a.a(cursor, this.k);
                b bVar = this.k;
                dm dmVar2 = (dm) view.getTag();
                String c4 = af.c(context, "readmode");
                if (Boolean.valueOf(af.b(context, "showpreivewweibo")).booleanValue()) {
                    dmVar2.j.setFilters(new InputFilter[]{new InputFilter.LengthFilter(50)});
                    dmVar2.j.setEllipsize(TextUtils.TruncateAt.END);
                } else {
                    dmVar2.j.setFilters(new InputFilter[]{new InputFilter.LengthFilter(100000)});
                    dmVar2.j.setEllipsize(null);
                }
                if (c4 == null) {
                    c4 = "readmode_img";
                }
                if (c4.equals("readmode_txt")) {
                    dmVar2.b.setVisibility(8);
                } else {
                    dmVar2.b.setVisibility(0);
                    g a4 = g.a(context);
                    Bitmap a5 = a4.a(bVar.g);
                    if (a5 == null) {
                        a5 = a4.b();
                        a4.a(bVar.g, new q(this, dmVar2.b));
                    }
                    dmVar2.b.setImageBitmap(a5);
                }
                if (bVar.f.equals(CaiboApp.a().b().b)) {
                    dmVar2.c.setText("我");
                } else {
                    dmVar2.c.setText(bVar.f);
                }
                dmVar2.f.setText(bVar.c);
                dmVar2.g.setText(bVar.a);
                dmVar2.i.setVisibility(0);
                if (bVar.i == null || bVar.i.equals("0")) {
                    dmVar2.j.setText(context.getString(R.string.reply_of_meblog) + bVar.d);
                } else {
                    dmVar2.j.setText(context.getString(R.string.reply_of_mecomment) + bVar.d);
                }
                dmVar2.m.setVisibility(8);
                dmVar2.n.setVisibility(8);
                String c5 = af.c(context, "theme");
                if (c5 == null || c5.equals("")) {
                    c5 = "listtheme";
                }
                if (c5.equals("bubbletheme")) {
                    dmVar2.s.setVisibility(0);
                    dmVar2.i.setBackgroundDrawable(null);
                    dmVar2.q.setBackgroundResource(R.drawable.bubble_bg);
                    return;
                }
                dmVar2.s.setVisibility(8);
                dmVar2.i.setBackgroundResource(R.drawable.reply_bg);
                dmVar2.q.setBackgroundDrawable(null);
                return;
            case 2:
                a.a();
                this.m = a.a(cursor, this.m);
                j jVar = this.m;
                dm dmVar3 = (dm) view.getTag();
                this.d = CaiboApp.a().b();
                String str3 = jVar.f;
                String str4 = jVar.g;
                if (jVar.b.equals(this.d.a)) {
                    String str5 = jVar.h;
                    str = str5;
                    str2 = jVar.g;
                } else {
                    String str6 = str4;
                    str = str3;
                    str2 = str6;
                }
                String c6 = af.c(context, "readmode");
                if (c6 == null) {
                    c6 = "readmode_img";
                }
                if (c6.equals("readmode_txt")) {
                    dmVar3.b.setVisibility(8);
                } else {
                    dmVar3.b.setVisibility(0);
                    g a6 = g.a(context);
                    Bitmap a7 = a6.a(str);
                    if (a7 == null) {
                        a7 = a6.b();
                        a6.a(str, new q(this, dmVar3.b));
                    }
                    dmVar3.b.setImageBitmap(a7);
                }
                dmVar3.c.setText(str2);
                dmVar3.f.setText(jVar.e);
                dmVar3.g.setSingleLine(true);
                dmVar3.g.setEllipsize(TextUtils.TruncateAt.END);
                dmVar3.g.setText(jVar.d);
                dmVar3.l.setVisibility(8);
                dmVar3.h.setVisibility(8);
                dmVar3.i.setVisibility(8);
                return;
            case 4:
                a.a();
                this.k = a.a(cursor, this.k);
                b bVar2 = this.k;
                dm dmVar4 = (dm) view.getTag();
                c b5 = CaiboApp.a().b();
                if (bVar2.f.equals(b5.b)) {
                    dmVar4.c.setText("我");
                } else {
                    dmVar4.c.setText(bVar2.f);
                }
                dmVar4.f.setText(bVar2.c);
                if (bVar2.i == null || bVar2.i.equals("0")) {
                    dmVar4.g.setText(bVar2.a);
                } else if (bVar2.j == null) {
                    dmVar4.g.setText(bVar2.a);
                } else if (bVar2.j.equals(b5.b)) {
                    dmVar4.g.setText("回复 我:" + bVar2.a);
                } else {
                    dmVar4.g.setText("回复 " + bVar2.j + ":" + bVar2.a);
                }
                dmVar4.b.setVisibility(8);
                dmVar4.i.setVisibility(8);
                dmVar4.m.setVisibility(8);
                dmVar4.n.setVisibility(8);
                return;
            case 5:
            case 6:
            case 9:
                a.a();
                this.l = a.a(cursor, this.l);
                m mVar = this.l;
                k kVar = (k) view.getTag();
                g a8 = g.a(context);
                Bitmap a9 = a8.a(mVar.b);
                if (a9 == null) {
                    a9 = a8.b();
                    a8.a(mVar.b, new q(this, kVar.a));
                }
                kVar.a.setImageBitmap(a9);
                kVar.b.setText(mVar.c);
                kVar.c.setText(mVar.d);
                if (6 == this.e) {
                    kVar.d.setVisibility(8);
                    return;
                } else {
                    kVar.d.setVisibility(0);
                    return;
                }
            case 7:
                a.a();
                this.l = a.a(cursor, this.l);
                m mVar2 = this.l;
                k kVar2 = (k) view.getTag();
                g a10 = g.a(context);
                Bitmap a11 = a10.a(mVar2.b);
                if (a11 == null) {
                    a11 = a10.b();
                    a10.a(mVar2.b, new q(this, kVar2.a));
                }
                kVar2.a.setImageBitmap(a11);
                kVar2.b.setText(mVar2.c);
                kVar2.e.setOnClickListener(this.c);
                kVar2.e.setTag(mVar2.a);
                return;
            case 8:
                a.a();
                this.n = a.a(cursor, this.n);
                ((TextView) view).setText("#" + this.n.b + "#");
                return;
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            default:
                return;
            case 17:
                a.a();
                this.m = a.a(cursor, this.m);
                j jVar2 = this.m;
                this.d = CaiboApp.a().b();
                Cdo doVar = (Cdo) view.getTag();
                if (jVar2.b.equals(this.d.a)) {
                    g a12 = g.a(context);
                    Bitmap a13 = a12.a(jVar2.f);
                    if (a13 == null) {
                        a13 = a12.b();
                        a12.a(jVar2.f, new q(this, doVar.a));
                    }
                    doVar.c.setText(jVar2.e);
                    doVar.a.setImageBitmap(a13);
                    doVar.a.setVisibility(0);
                    doVar.d.setText(jVar2.d);
                    doVar.d.setVisibility(0);
                    doVar.e.setVisibility(8);
                    doVar.b.setVisibility(8);
                    return;
                }
                g a14 = g.a(context);
                Bitmap a15 = a14.a(jVar2.f);
                if (a15 == null) {
                    a15 = a14.b();
                    a14.a(jVar2.f, new q(this, doVar.b));
                }
                doVar.b.setImageBitmap(a15);
                doVar.b.setVisibility(0);
                doVar.e.setText(jVar2.d);
                doVar.e.setVisibility(0);
                doVar.c.setText(jVar2.e);
                doVar.d.setVisibility(8);
                doVar.a.setVisibility(8);
                return;
            case 18:
                a.a();
                this.n = a.a(cursor, this.n);
                ((n) view.getTag()).a.setText("#" + this.n.b + "#");
                return;
            case 19:
                a.a();
                this.m = a.a(cursor, this.m);
                j jVar3 = this.m;
                dm dmVar5 = (dm) view.getTag();
                dmVar5.b.setVisibility(8);
                dmVar5.q.setVisibility(8);
                dmVar5.r.setVisibility(0);
                dmVar5.a.setVisibility(0);
                dmVar5.r.setText(jVar3.d);
                dmVar5.a.setImageResource(R.drawable.pushmessage_cion);
                return;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        switch (this.e) {
            case 5:
            case 9:
                View inflate = this.f.inflate((int) R.layout.friend_item, viewGroup, false);
                k kVar = new k(this);
                kVar.a = (ImageView) inflate.findViewById(R.id.friendhead_ImgVew);
                kVar.c = (TextView) inflate.findViewById(R.id.friend_newest);
                kVar.b = (TextView) inflate.findViewById(R.id.friend_usernameView);
                kVar.d = (ImageView) inflate.findViewById(R.id.friend_more);
                inflate.setTag(kVar);
                return inflate;
            case 6:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            default:
                View inflate2 = this.f.inflate((int) R.layout.timeline_item, viewGroup, false);
                dm dmVar = new dm(this);
                dmVar.a = (ImageView) inflate2.findViewById(R.id.newsimgview);
                dmVar.b = (ImageView) inflate2.findViewById(R.id.headImgView);
                dmVar.c = (TextView) inflate2.findViewById(R.id.usernameView);
                dmVar.d = (ImageView) inflate2.findViewById(R.id.locationImgView);
                dmVar.e = (ImageView) inflate2.findViewById(R.id.isImgView);
                dmVar.f = (TextView) inflate2.findViewById(R.id.time);
                dmVar.g = (RichTextView) inflate2.findViewById(R.id.tweetcontext);
                dmVar.g.a(false);
                dmVar.h = (ImageView) inflate2.findViewById(R.id.preimage_statues);
                dmVar.i = (LinearLayout) inflate2.findViewById(R.id.root_layout);
                dmVar.j = (RichTextView) inflate2.findViewById(R.id.root_blogContextView);
                dmVar.j.a(false);
                dmVar.k = (ImageView) inflate2.findViewById(R.id.root_preimage_view);
                dmVar.l = (RelativeLayout) inflate2.findViewById(R.id.source_layout);
                dmVar.n = (LinearLayout) inflate2.findViewById(R.id.number_layout);
                dmVar.m = (TextView) inflate2.findViewById(R.id.txt_source);
                dmVar.o = (TextView) inflate2.findViewById(R.id.comment);
                dmVar.p = (TextView) inflate2.findViewById(R.id.retweet);
                dmVar.q = (LinearLayout) inflate2.findViewById(R.id.tweet_layout);
                dmVar.r = (RichTextView) inflate2.findViewById(R.id.pushtweetcontext);
                dmVar.s = (TextView) inflate2.findViewById(R.id.replyDivider);
                inflate2.setTag(dmVar);
                return inflate2;
            case 7:
                View inflate3 = this.f.inflate((int) R.layout.black_item, viewGroup, false);
                k kVar2 = new k(this);
                kVar2.a = (ImageView) inflate3.findViewById(R.id.blackfriendhead_ImgVew);
                kVar2.b = (TextView) inflate3.findViewById(R.id.blackfriend_usernameView);
                kVar2.e = (Button) inflate3.findViewById(R.id.blackfriend_relation);
                inflate3.setTag(kVar2);
                return inflate3;
            case 8:
                return this.f.inflate((int) R.layout.topic_list, viewGroup, false);
            case 17:
                View inflate4 = this.f.inflate((int) R.layout.letter_list_row, viewGroup, false);
                Cdo doVar = new Cdo(this);
                doVar.a = (ImageView) inflate4.findViewById(R.id.mine_head_image);
                doVar.d = (RichTextView) inflate4.findViewById(R.id.mine_talk);
                doVar.b = (ImageView) inflate4.findViewById(R.id.sender_head_image);
                doVar.e = (RichTextView) inflate4.findViewById(R.id.sender_talk);
                doVar.c = (TextView) inflate4.findViewById(R.id.date_time);
                inflate4.setTag(doVar);
                return inflate4;
            case 18:
                View inflate5 = this.f.inflate((int) R.layout.attention_topic_row, viewGroup, false);
                n nVar = new n(this);
                nVar.a = (TextView) inflate5.findViewById(R.id.TopicName);
                inflate5.setTag(nVar);
                return inflate5;
        }
    }
}
