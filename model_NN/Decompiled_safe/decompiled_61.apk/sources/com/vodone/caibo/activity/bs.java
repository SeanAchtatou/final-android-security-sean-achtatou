package com.vodone.caibo.activity;

import android.os.Message;
import android.widget.Toast;
import com.vodone.caibo.CaiboApp;
import com.vodone.caibo.R;
import com.vodone.caibo.database.a;

final class bs extends bb {
    private /* synthetic */ SendblogActivity a;

    bs(SendblogActivity sendblogActivity) {
        this.a = sendblogActivity;
    }

    public final void handleMessage(Message message) {
        super.handleMessage(message);
        if (this.a.e != null) {
            this.a.e.dismiss();
            this.a.e = null;
            int i = message.what;
            if (i == 0) {
                this.a.finish();
                if (this.a.a == 2 || this.a.a == 3) {
                    Toast.makeText(this.a, this.a.getString(R.string.comment_succeed), 0).show();
                } else if (this.a.a == 6) {
                    Toast.makeText(this.a, this.a.getString(R.string.report_succeed), 0).show();
                } else if (this.a.a == 4) {
                    Toast.makeText(this.a, this.a.getString(R.string.feedback_succeed), 0).show();
                } else {
                    Toast.makeText(this.a, this.a.getString(R.string.send_succeed), 0).show();
                }
                String str = CaiboApp.a().b().a;
                if (this.a.b != null) {
                    a.a();
                    a.a(this.a, str, 20, this.a.b, (String) null);
                }
            } else if (i != -1) {
                int a2 = l.a(i);
                if (a2 != 0) {
                    Toast.makeText(this.a, a2, 1).show();
                }
            } else if (this.a.a == 6) {
                Toast.makeText(this.a, this.a.getString(R.string.report_failed), 0).show();
            } else {
                Toast.makeText(this.a, this.a.getString(R.string.sendfail), 0).show();
            }
        }
    }
}
