package com.vodone.caibo.activity;

import android.app.AlertDialog;
import android.view.View;
import com.vodone.caibo.R;

final class aw implements View.OnClickListener {
    private /* synthetic */ FriendActivity a;

    aw(FriendActivity friendActivity) {
        this.a = friendActivity;
    }

    public final void onClick(View view) {
        FriendActivity friendActivity = this.a;
        new AlertDialog.Builder(friendActivity).setTitle((int) R.string.message_alert).setMessage((int) R.string.makesure_remove_black_user).setPositiveButton((int) R.string.ok, new z(friendActivity, (String) view.getTag())).setNegativeButton((int) R.string.cancel, new aa(friendActivity)).show();
    }
}
