package com.vodone.caibo.activity;

import android.app.AlertDialog;
import android.database.Cursor;
import android.view.View;
import com.vodone.caibo.R;
import com.vodone.caibo.b.j;
import com.vodone.caibo.database.a;

final class b implements dd {
    final /* synthetic */ SendLetterActivity a;
    private byte b = 23;

    public b(SendLetterActivity sendLetterActivity) {
        this.a = sendLetterActivity;
    }

    public final void a() {
        this.a.a();
    }

    public final void a(g gVar, View view, int i) {
        if (2 == this.a.l) {
            Cursor a2 = this.a.a.a(i);
            SendLetterActivity sendLetterActivity = this.a;
            a.a();
            sendLetterActivity.b = a.a(a2, (j) null);
            new AlertDialog.Builder(this.a).setTitle((int) R.string.choose_operation).setItems((int) R.array.send_letter_choice, new al(this)).show();
        }
    }

    public final void b() {
    }
}
