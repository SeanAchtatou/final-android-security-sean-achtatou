package com.vodone.caibo.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import com.vodone.caibo.R;
import com.vodone.caibo.database.a;
import com.vodone.caibo.service.c;

public class FriendActivity extends BaseActivity implements View.OnClickListener {
    int a = 0;
    short b;
    short c = -1;
    public bb d = new ax(this);
    /* access modifiers changed from: private */
    public String e;
    private String f;
    /* access modifiers changed from: private */
    public byte k;
    /* access modifiers changed from: private */
    public ProgressDialog l;
    /* access modifiers changed from: private */
    public g m;
    /* access modifiers changed from: private */
    public boolean n;
    private View.OnClickListener o = new aw(this);
    private dd p = new az(this);

    public static Intent a(Context context, String str) {
        Intent intent = new Intent(context, FriendActivity.class);
        Bundle bundle = new Bundle();
        bundle.putByte("friend_key", (byte) 1);
        bundle.putString("friend_user_id", str);
        intent.putExtras(bundle);
        return intent;
    }

    public static Intent a(Context context, String str, String str2, boolean z) {
        Intent intent = new Intent(context, FriendActivity.class);
        Bundle bundle = new Bundle();
        bundle.putByte("friend_key", (byte) 6);
        bundle.putString("keywords", str);
        bundle.putString("friend_user_id", str2);
        bundle.putBoolean("select_friend", z);
        intent.putExtras(bundle);
        return intent;
    }

    public final void a() {
        short s = -1;
        c a2 = c.a();
        switch (this.k) {
            case 1:
                s = a2.j(this.e, this.d);
                break;
            case 2:
                s = a2.k(this.e, this.d);
                break;
            case 3:
                s = a2.l(this.e, this.d);
                break;
            case 5:
                s = a2.i(this.e, this.d);
                break;
            case 6:
                s = a2.i(this.f, 1, this.d);
                break;
        }
        if (s >= 0) {
            if (this.l != null) {
                this.l.dismiss();
                this.l = null;
            }
            this.l = ProgressDialog.show(this, getString(R.string.wait), getString(R.string.loding));
            this.l.setCancelable(true);
            this.l.setOnCancelListener(new ao(s));
        }
    }

    public final void b() {
        c a2 = c.a();
        switch (this.k) {
            case 1:
                a2.e(this.e, this.a + 1, this.d);
                return;
            case 2:
                a2.f(this.e, this.a + 1, this.d);
                return;
            case 3:
                a2.g(this.e, this.a + 1, this.d);
                return;
            case 4:
            default:
                return;
            case 5:
                a2.d(this.e, this.a + 1, this.d);
                return;
            case 6:
                a2.i(this.f, this.a + 1, this.d);
                return;
        }
    }

    public void onClick(View view) {
        switch (this.k) {
            case 3:
                if (view.equals(this.g.d)) {
                    a.a();
                    a.b(this, this.e, this.k);
                    a();
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Bundle extras = getIntent().getExtras();
        this.k = extras.getByte("friend_key");
        this.e = extras.getString("friend_user_id");
        this.n = extras.getBoolean("select_friend");
        this.f = extras.getString("keywords");
        setContentView((int) R.layout.timeline1);
        ListView listView = (ListView) findViewById(R.id.timeline_ListView01);
        a.a();
        a.b(this, this.e, this.k);
        a.a();
        Cursor a2 = a.a(this, this.e, this.k);
        if (this.k == 3) {
            a.a();
            a.b(this, this.e, this.k);
            this.m = new g(listView, new cg(this, a2, (byte) 7));
            this.m.b.c = this.o;
            this.m.a();
            this.m.c = this.p;
        } else if (this.k == 6) {
            this.m = new g(listView, new cg(this, a2, (byte) 9));
            this.m.a();
            this.m.c = this.p;
        } else {
            this.m = new g(listView, new cg(this, a2, (byte) 5));
            this.m.a();
            this.m.c = this.p;
        }
        a((byte) 0, (int) R.string.back, this.i);
        b((byte) 1, R.drawable.home_icon, this.j);
        this.g.a.setBackgroundResource(R.drawable.title_left_button);
        switch (this.k) {
            case 1:
                setTitle((int) R.string.mfans);
                break;
            case 2:
                setTitle((int) R.string.attention_str);
                break;
            case 3:
                setTitle((int) R.string.blacklist);
                b((byte) 1, R.drawable.refresh_icon, this);
                break;
            case 5:
                setTitle((int) R.string.mutual_atten);
                break;
            case 6:
                a(this.f);
                b((byte) 2, -1, null);
                break;
        }
        a();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.m.b.getCursor().close();
        a.a();
        a.b(this, this.e, this.k);
    }
}
