package com.vodone.caibo.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.widget.Toast;
import com.vodone.caibo.R;

final class ab extends bb {
    private /* synthetic */ ReSetPasswd a;

    ab(ReSetPasswd reSetPasswd) {
        this.a = reSetPasswd;
    }

    public final void handleMessage(Message message) {
        if (message.what == 0) {
            String c = af.c(this.a, "findpasswd_nickName");
            Toast.makeText(this.a, (int) R.string.update_succeed, 1).show();
            Bundle bundle = new Bundle();
            bundle.putString("username", c);
            bundle.putString("password", "");
            Intent intent = new Intent(this.a, LoginActivity.class);
            intent.putExtras(bundle);
            this.a.startActivity(intent);
            return;
        }
        switch (message.what) {
            case 1538:
                this.a.b("修改失败");
                return;
            case 1539:
                this.a.b((int) R.string.code_wrong);
                return;
            case 1540:
            case 1541:
            default:
                return;
            case 1542:
                this.a.b("新密码过于简单");
                return;
            case 1543:
                this.a.b("新密码不合法");
                return;
        }
    }
}
