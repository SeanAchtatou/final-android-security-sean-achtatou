package com.vodone.caibo.activity;

import android.content.Intent;
import android.database.Cursor;
import android.view.View;
import com.vodone.caibo.b.m;
import com.vodone.caibo.database.a;

final class az implements dd {
    private /* synthetic */ FriendActivity a;

    az(FriendActivity friendActivity) {
        this.a = friendActivity;
    }

    public final void a() {
        this.a.a();
    }

    public final void a(g gVar, View view, int i) {
        if (1 == this.a.k || 2 == this.a.k || 3 == this.a.k || 5 == this.a.k || 6 == this.a.k) {
            Cursor a2 = this.a.m.a(i);
            a.a();
            m a3 = a.a(a2, (m) null);
            String str = a3.a;
            String str2 = a3.c;
            if (3 == this.a.k) {
                this.a.startActivity(PersonalInformationActivity.a(this.a, str, str2, 3));
            } else if (1 == this.a.k) {
                this.a.startActivity(PersonalInformationActivity.a(this.a, str, str2, 0));
            } else if (6 == this.a.k) {
                if (this.a.n) {
                    Intent intent = new Intent(this.a, SearchContactActivity.class);
                    intent.putExtra("friend_nickname_key", "@" + str2);
                    this.a.setResult(-1, intent);
                    this.a.finish();
                    return;
                }
                this.a.startActivity(PersonalInformationActivity.a(this.a, str, str2, 0));
            } else if (2 == this.a.k) {
                this.a.startActivity(PersonalInformationActivity.a(this.a, str, str2, 1));
            } else if (5 == this.a.k) {
                Intent intent2 = new Intent();
                intent2.putExtra("friend_nickname_key", str2);
                intent2.putExtra("friend_user_id", str);
                this.a.setResult(-1, intent2);
                this.a.finish();
            }
        }
    }

    public final void b() {
        this.a.b();
    }
}
