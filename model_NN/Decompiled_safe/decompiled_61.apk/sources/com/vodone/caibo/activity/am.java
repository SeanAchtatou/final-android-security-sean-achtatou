package com.vodone.caibo.activity;

import android.view.View;

final class am implements View.OnClickListener {
    private /* synthetic */ PreviewActivity a;

    am(PreviewActivity previewActivity) {
        this.a = previewActivity;
    }

    public final void onClick(View view) {
        this.a.k.c(this.a.k.c() * 0.5f);
        Boolean bool = true;
        this.a.k.notifyObservers(bool);
        if (this.a.k.c() <= 1.0f) {
            this.a.d.setIsZoomOutEnabled(!bool.booleanValue());
            this.a.d.setIsZoomInEnabled(bool.booleanValue());
            return;
        }
        this.a.d.setIsZoomOutEnabled(bool.booleanValue());
        this.a.d.setIsZoomInEnabled(bool.booleanValue());
    }
}
