package com.vodone.caibo.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.widget.ZoomControls;
import com.vodone.caibo.R;
import com.vodone.caibo.activity.zoom.ImageZoomView;
import com.vodone.caibo.activity.zoom.a;
import com.vodone.caibo.activity.zoom.b;
import com.vodone.caibo.activity.zoom.c;
import com.vodone.caibo.activity.zoom.d;
import com.vodone.caibo.b.e;
import com.vodone.caibo.b.g;
import java.io.File;
import java.util.Timer;
import java.util.TimerTask;

public class PreviewActivity extends BaseActivity implements View.OnClickListener {
    /* access modifiers changed from: private */
    public ProgressBar a;
    private String b;
    /* access modifiers changed from: private */
    public Bitmap c;
    /* access modifiers changed from: private */
    public ZoomControls d;
    private d e;
    private a f;
    /* access modifiers changed from: private */
    public c k;
    private ImageZoomView l;
    private View.OnClickListener m = new ak(this);
    private View.OnClickListener n = new am(this);
    private final long o = 5000;
    private Timer p = new Timer();
    private TimerTask q = null;
    private final int r = 100;
    /* access modifiers changed from: private */
    public Handler s = new ag(this);

    public static Intent a(Context context, String str) {
        Intent intent = new Intent(context, PreviewActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("imageurl", str);
        intent.putExtras(bundle);
        return intent;
    }

    public final void a(boolean z) {
        if (z) {
            if (this.d.getVisibility() != 0) {
                AlphaAnimation alphaAnimation = new AlphaAnimation(0.1f, 1.0f);
                alphaAnimation.setDuration(500);
                this.d.setAnimation(alphaAnimation);
                this.d.setVisibility(0);
                if (this.q != null) {
                    this.q.cancel();
                }
                this.q = null;
                this.q = new an(this);
                this.p.schedule(this.q, 5000);
            }
        } else if (this.d.getVisibility() != 8) {
            AlphaAnimation alphaAnimation2 = new AlphaAnimation(1.0f, 0.1f);
            alphaAnimation2.setDuration(500);
            this.d.setAnimation(alphaAnimation2);
            this.d.setVisibility(8);
        }
    }

    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            a(true);
        }
        return super.dispatchTouchEvent(motionEvent);
    }

    public void onClick(View view) {
        File cacheDir;
        if (view.equals(this.g.d)) {
            if (Environment.getExternalStorageState().equals("mounted")) {
                cacheDir = new File(new File(Environment.getExternalStorageDirectory(), "/vodone/caibo"), "/save");
                if (!cacheDir.exists()) {
                    cacheDir.mkdirs();
                }
            } else {
                cacheDir = getCacheDir();
            }
            String str = cacheDir.getPath() + "/" + this.b.hashCode() + ".jpg";
            e.a(this.c, str, Bitmap.CompressFormat.JPEG);
            Toast.makeText(this, getString(R.string.image_saved_toast_text) + str, 1).show();
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.view_image_layout);
        this.e = new d();
        this.k = this.e.a();
        this.l = (ImageZoomView) findViewById(R.id.imagezoomview_img);
        this.d = (ZoomControls) findViewById(R.id.imageview_control);
        this.l.a(this.k);
        this.f = new a();
        this.f.a(b.PAN);
        this.f.a(this.e);
        this.e.a(this.l.a());
        this.a = (ProgressBar) findViewById(R.id.loadingphoto);
        this.a.setPadding(0, 0, 15, 0);
        this.a.setVisibility(0);
        this.d.setVisibility(8);
        Intent intent = getIntent();
        if (intent != null) {
            this.b = intent.getExtras().getString("imageurl");
        }
        this.l.setImageBitmap(null);
        this.d.setOnZoomInClickListener(this.m);
        this.d.setOnZoomOutClickListener(this.n);
        this.k.a(0.5f);
        this.k.b(0.5f);
        this.k.c(1.0f);
        this.k.notifyObservers();
        this.d.setIsZoomOutEnabled(false);
        a((byte) 0, (int) R.string.back, this.i);
        b((byte) 0, R.string.save, this);
        this.g.a.setBackgroundResource(R.drawable.title_left_button);
        this.l.setOnTouchListener(this.f);
        String str = this.b;
        Bitmap c2 = g.a(this).c(str);
        if (c2 != null) {
            this.c = c2;
            this.l.setImageBitmap(this.c);
            this.a.setVisibility(8);
            a(true);
            return;
        }
        this.a.setVisibility(0);
        g.a(this).c(str, new r(this, this.l));
    }
}
