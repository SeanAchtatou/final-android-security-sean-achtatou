package com.vodone.caibo.activity;

import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.TextView;
import com.vodone.caibo.R;
import com.vodone.caibo.b.d;
import com.vodone.caibo.database.a;

final class dp extends CursorAdapter {
    final /* synthetic */ DraftBoxActivity a;
    private boolean b;
    private LayoutInflater c;
    private d d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public dp(DraftBoxActivity draftBoxActivity, Context context, Cursor cursor) {
        super(context, cursor);
        this.a = draftBoxActivity;
        this.c = LayoutInflater.from(context);
    }

    public final void a(boolean z) {
        boolean z2;
        dp dpVar;
        if (this.b != z) {
            z2 = z;
            dpVar = this;
        } else if (!z) {
            z2 = true;
            dpVar = this;
        } else {
            z2 = false;
            dpVar = this;
        }
        dpVar.b = z2;
        notifyDataSetChanged();
    }

    public final void bindView(View view, Context context, Cursor cursor) {
        a.a();
        this.d = a.a(cursor, this.d);
        dk dkVar = (dk) view.getTag();
        if (this.d.c != null) {
            dkVar.a.setText(this.d.c);
        }
        if (this.b) {
            dkVar.b.setVisibility(0);
        } else {
            dkVar.b.setVisibility(8);
        }
        dkVar.b.setTag(this.d.a);
        if (cursor.getPosition() % 2 != 0) {
            view.setBackgroundColor(-723724);
        } else {
            view.setBackgroundColor(-1);
        }
    }

    public final View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        View inflate = this.c.inflate((int) R.layout.accountmanagelist, (ViewGroup) null);
        inflate.findViewById(R.id.account_tag).setVisibility(8);
        inflate.findViewById(R.id.account_triangle).setVisibility(8);
        dk dkVar = new dk(this);
        dkVar.a = (TextView) inflate.findViewById(R.id.account_user_text);
        dkVar.a.setTextSize(20.0f);
        dkVar.a.setPadding(10, 10, 10, 10);
        dkVar.a.setSingleLine();
        dkVar.a.setEllipsize(TextUtils.TruncateAt.END);
        dkVar.b = (Button) inflate.findViewById(R.id.accmgrdelete);
        dkVar.b.setOnClickListener(this.a);
        inflate.setTag(dkVar);
        return inflate;
    }
}
