package com.vodone.caibo.activity;

import android.graphics.Bitmap;
import android.widget.ImageView;
import android.widget.Toast;
import com.vodone.caibo.R;
import com.vodone.caibo.b.h;

final class r implements h {
    private ImageView a;
    private /* synthetic */ PreviewActivity b;

    public r(PreviewActivity previewActivity, ImageView imageView) {
        this.b = previewActivity;
        this.a = imageView;
    }

    public final void a() {
        if (this.a.isShown()) {
            this.b.a.setVisibility(8);
            Toast.makeText(this.b, this.b.getString(R.string.image_loaded_error_text), 0).show();
        }
    }

    public final void a(Bitmap bitmap) {
        this.b.a.setVisibility(8);
        if (bitmap != null) {
            Bitmap unused = this.b.c = bitmap;
            this.a.setImageBitmap(bitmap);
            this.b.a(true);
        } else if (this.a.isShown()) {
            Toast.makeText(this.b, this.b.getString(R.string.image_loaded_error_text), 0).show();
        }
    }
}
