package com.vodone.caibo.activity;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SimpleAdapter;
import com.vodone.caibo.CaiboApp;
import com.vodone.caibo.R;
import com.windo.widget.WithNewIconButton;
import java.util.List;

final class v extends SimpleAdapter {
    private /* synthetic */ PlazaActivity a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    v(PlazaActivity plazaActivity, Context context, List list, String[] strArr, int[] iArr) {
        super(context, list, R.layout.blogsquare_stylebox, strArr, iArr);
        this.a = plazaActivity;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        View view2 = super.getView(i, view, viewGroup);
        WithNewIconButton withNewIconButton = (WithNewIconButton) view2.findViewById(R.id.ItemImage);
        this.a.e[i] = withNewIconButton;
        int i2 = -1;
        switch (i) {
            case 4:
                i2 = 0;
                break;
            case 5:
                i2 = 1;
                break;
            case 6:
                i2 = 2;
                break;
            case 7:
                i2 = 3;
                break;
            case 8:
                i2 = 4;
                break;
        }
        int a2 = CaiboApp.a().a(i2);
        if (a2 > 0) {
            withNewIconButton.a(a2);
        }
        c cVar = new c(this.a, withNewIconButton);
        CaiboApp.a().a(i2, cVar);
        withNewIconButton.setTag(cVar);
        return view2;
    }
}
