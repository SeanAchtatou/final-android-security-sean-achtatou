package com.vodone.caibo.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.vodone.caibo.R;
import com.vodone.caibo.b.c;
import com.vodone.caibo.b.l;
import java.util.ArrayList;

public class AccountManageActivity extends BaseActivity implements View.OnClickListener, AdapterView.OnItemClickListener {
    boolean a = true;
    j b;
    private ListView c;
    /* access modifiers changed from: private */
    public Button d;
    private View e;
    private ProgressDialog f;
    private boolean k = false;

    public void onClick(View view) {
        if (view.equals(this.e)) {
            startActivity(new Intent(this, AddAccountActitvity.class));
        } else if (view.equals(this.d)) {
            this.b.a(this.a);
            if (this.k) {
                this.b.a(false);
                this.d.setText((int) R.string.edit);
            } else {
                this.b.a(true);
                this.d.setText((int) R.string.finish);
            }
            this.k = !this.k;
        } else if ((view instanceof Button) && view.getTag() != null) {
            new AlertDialog.Builder(this).setTitle(getString(R.string.prompt)).setMessage(getString(R.string.confirm_delete)).setPositiveButton((int) R.string.ok, new bp(this, (String) view.getTag())).setNegativeButton((int) R.string.cancel, new bw(this)).show();
        }
    }

    public boolean onContextItemSelected(MenuItem menuItem) {
        return super.onContextItemSelected(menuItem);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.accoutmanage);
        this.g.a.setBackgroundResource(R.drawable.title_left_button);
        a((byte) 0, (int) R.string.back, this.i);
        b((byte) 0, R.string.edit, this);
        setTitle((int) R.string.accountmanage);
        this.c = (ListView) findViewById(R.id.listview);
        this.c.setOnItemClickListener(this);
        this.e = findViewById(R.id.addaccount);
        this.e.setOnClickListener(this);
        this.d = (Button) findViewById(R.id.titleBtnRight);
        this.d.setOnClickListener(this);
        if (this.b == null) {
            String[] b2 = l.a(this).b();
            ArrayList arrayList = new ArrayList();
            for (String add : b2) {
                arrayList.add(add);
            }
            this.b = new j(this, this, arrayList);
            this.c.setAdapter((ListAdapter) this.b);
            this.c.setDividerHeight(0);
            this.c.setOnItemClickListener(this);
        }
    }

    public void onItemClick(AdapterView adapterView, View view, int i, long j) {
        String str = (String) this.b.getItem(i);
        if (str != null) {
            c b2 = l.a(this).b(str);
            String str2 = b2.b;
            String str3 = b2.c;
            boolean z = b2.d;
            if (str3 != null) {
                if (this.f != null && this.f.isShowing()) {
                    this.f.dismiss();
                }
                this.f = null;
                this.f = ProgressDialog.show(this, getString(R.string.waiting), getString(R.string.loding));
                this.f.setOnCancelListener(new ao(com.vodone.caibo.service.c.a().a(str2, str3, new ar(this, this.f, str2, str3, z))));
                this.f.setCancelable(true);
                return;
            }
            Intent intent = new Intent(this, LoginActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("username", str2);
            bundle.putString("password", null);
            intent.putExtras(bundle);
            startActivity(intent);
        }
    }
}
