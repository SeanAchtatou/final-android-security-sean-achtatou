package com.vodone.caibo.activity;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.vodone.caibo.R;
import com.windo.widget.WithIconButton;

public class BaseLayout extends LinearLayout {
    public WithIconButton a = ((WithIconButton) findViewById(R.id.titleBtnLeft));
    public LinearLayout b = ((LinearLayout) findViewById(R.id.titleembededButtons));
    public RadioGroup c = ((RadioGroup) findViewById(R.id.title_radiogroup));
    public WithIconButton d = ((WithIconButton) findViewById(R.id.titleBtnRight));
    public TextView e = ((TextView) findViewById(R.id.titleText));
    public ProgressBar f = ((ProgressBar) findViewById(R.id.refreshData));
    private Button g = ((Button) findViewById(R.id.titleat));
    private Button h = ((Button) findViewById(R.id.titlecomment));
    private Button i = ((Button) findViewById(R.id.titlemessage));

    public BaseLayout(Context context) {
        super(context);
        View inflate = ((LayoutInflater) context.getSystemService("layout_inflater")).inflate((int) R.layout.titlebar, (ViewGroup) null);
        setOrientation(1);
        addView(inflate, new LinearLayout.LayoutParams(-1, -2));
    }
}
