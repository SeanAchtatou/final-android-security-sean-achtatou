package com.vodone.caibo.activity;

import android.text.Editable;
import android.text.TextWatcher;
import com.vodone.caibo.database.a;

final class ai implements TextWatcher {
    private boolean a = false;
    private /* synthetic */ SearchContactActivity b;

    ai(SearchContactActivity searchContactActivity) {
        this.b = searchContactActivity;
    }

    public final void afterTextChanged(Editable editable) {
    }

    public final void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public final void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        if (charSequence.length() > 0) {
            if (!this.a) {
                this.b.k.c(false);
                this.b.f.addFooterView(this.b.a);
                this.b.f.addFooterView(this.b.b);
                this.a = true;
            }
            a.a();
            this.b.l.changeCursor(a.a(this.b, this.b.m, charSequence.toString()));
            this.b.a.setText("@" + ((Object) charSequence));
            return;
        }
        this.b.f.removeFooterView(this.b.a);
        this.b.f.removeFooterView(this.b.b);
        this.b.k.c(true);
        a.a();
        this.b.l.changeCursor(a.a(this.b, this.b.m, 7));
        this.a = false;
    }
}
