package com.vodone.caibo.activity;

import android.view.View;
import android.widget.AdapterView;

final class dn implements AdapterView.OnItemClickListener {
    private /* synthetic */ g a;

    dn(g gVar) {
        this.a = gVar;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        if (view.equals(this.a.d)) {
            g.c(this.a);
        } else if (view.equals(this.a.g)) {
            g.a(this.a);
        } else if (this.a.c != null) {
            this.a.c.a(this.a, view, i);
        }
    }
}
