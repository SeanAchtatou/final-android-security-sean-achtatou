package com.vodone.caibo.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import com.vodone.caibo.CaiboApp;
import com.vodone.caibo.R;
import com.vodone.caibo.b.c;
import com.vodone.caibo.b.l;
import com.vodone.caibo.service.d;

public class MoreItemsActivity extends BaseActivity implements View.OnClickListener {
    private static String q = "MyInformation";
    String a = null;
    private View b;
    private View c;
    private View d;
    private RelativeLayout e;
    private RelativeLayout f;
    private RelativeLayout k;
    private RelativeLayout l;
    private RelativeLayout m;
    private RelativeLayout n;
    private RelativeLayout o;
    private RelativeLayout p;

    public final void a() {
        c b2 = l.a(this).b(this.a);
        String str = b2.b;
        String str2 = b2.c;
        ProgressDialog show = ProgressDialog.show(this, getString(R.string.waiting), getString(R.string.loding));
        short a2 = com.vodone.caibo.service.c.a().a(this.a, str2, new ar(this, show, str, str2, true));
        show.setCancelable(true);
        show.setOnCancelListener(new ao(a2));
    }

    public final void b() {
        cn.a().d();
        startActivity(new Intent(this, LoginActivity.class));
    }

    public void onClick(View view) {
        Class<BindActivity> cls = BindActivity.class;
        Bundle bundle = new Bundle();
        c b2 = CaiboApp.a().b();
        String str = b2.b;
        if (view.equals(this.e)) {
            Intent intent = new Intent();
            intent.setClass(this, SettingActivity.class);
            startActivity(intent);
        } else if (view.equals(this.f)) {
            startActivity(PersonalInformationActivity.a(this, b2.a, b2.b, 2));
        } else if (view.equals(this.k)) {
            Intent intent2 = new Intent();
            intent2.setClass(this, AboutCaiboActivity.class);
            startActivity(intent2);
        } else if (view.equals(this.b)) {
            startActivity(new Intent(this, AccountManageActivity.class));
        } else if (view.equals(this.l)) {
            new AlertDialog.Builder(this).setMessage(getString(R.string.exitcurrents)).setPositiveButton((int) R.string.ok, new dr(this)).setNegativeButton((int) R.string.cancel, new ds(this)).show();
        } else if (view.equals(this.c)) {
            bundle.putString("bind", "mobilebind");
            bundle.putString("usernamee", str);
            Class<BindActivity> cls2 = BindActivity.class;
            Intent intent3 = new Intent(this, cls);
            intent3.putExtras(bundle);
            startActivity(intent3);
        } else if (view.equals(this.d)) {
            bundle.putString("bind", "emailbind");
            bundle.putString("usernamee", str);
            Class<BindActivity> cls3 = BindActivity.class;
            Intent intent4 = new Intent(this, cls);
            intent4.putExtras(bundle);
            startActivity(intent4);
        } else if (view.equals(this.m)) {
            startActivity(new Intent(this, ReadingModelActivity.class));
        } else if (view.equals(this.n)) {
            startActivity(new Intent(this, SetThemeActivity.class));
        } else if (view.equals(this.o)) {
            Intent intent5 = new Intent(this, SendblogActivity.class);
            Bundle bundle2 = new Bundle();
            bundle2.putByte("type", (byte) 4);
            intent5.putExtras(bundle2);
            startActivity(intent5);
        } else if (view.equals(this.p)) {
            d.b().a(false);
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.morelist);
        a((byte) 2, -1, (View.OnClickListener) null);
        b((byte) 2, -1, null);
        setTitle((int) R.string.more);
        this.b = findViewById(R.id.accoutmanagess);
        this.b.setOnClickListener(this);
        this.c = findViewById(R.id.bindphones);
        this.c.setOnClickListener(this);
        this.d = findViewById(R.id.bindemails);
        this.d.setOnClickListener(this);
        this.e = (RelativeLayout) findViewById(R.id.sets);
        this.e.setOnClickListener(this);
        this.f = (RelativeLayout) findViewById(R.id.myinfos);
        this.f.setOnClickListener(this);
        this.k = (RelativeLayout) findViewById(R.id.abouts);
        this.k.setOnClickListener(this);
        this.l = (RelativeLayout) findViewById(R.id.exitcurrunts);
        this.l.setOnClickListener(this);
        this.m = (RelativeLayout) findViewById(R.id.readmodelayout);
        this.m.setOnClickListener(this);
        this.n = (RelativeLayout) findViewById(R.id.themelayout);
        this.n.setOnClickListener(this);
        this.o = (RelativeLayout) findViewById(R.id.feekbacks);
        this.o.setOnClickListener(this);
        this.p = (RelativeLayout) findViewById(R.id.checkupdate);
        this.p.setOnClickListener(this);
        af.a(this, "bindskip", 2);
    }
}
