package com.vodone.caibo.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.vodone.caibo.CaiboApp;
import com.vodone.caibo.R;
import com.vodone.caibo.a;
import com.vodone.caibo.b.c;
import com.vodone.caibo.b.l;
import java.util.ArrayList;
import java.util.List;

public class AddInfoAcitivity extends BaseActivity implements View.OnClickListener {
    private Button a;
    private Button b;
    private TextView c;
    private TextView d;
    private TextView e;
    private RelativeLayout f;
    private String k;
    private int l;
    private int m;

    private String a(int i) {
        new SetLocationActivity();
        new ArrayList();
        return ((a) SetLocationActivity.a(this).get(i - 1)).a();
    }

    private String a(int i, int i2) {
        new SetLocationActivity();
        new ArrayList();
        List a2 = SetLocationActivity.a(this);
        return ((a) a2.get(i2 - 1)).a(i2, i, a2);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 1) {
            this.l = intent.getIntExtra("province", this.l);
            this.m = intent.getIntExtra("city", this.m);
            String a2 = a(this.l);
            this.e.setText(a2 + " " + a(this.m, this.l));
        }
    }

    public void onClick(View view) {
        if (view.equals(this.a)) {
            CaiboApp.a().a(l.a(this).b(this.k));
            af.a(this, "province", this.l);
            af.a(this, "city", this.m);
            Intent intent = new Intent(this, PerfectActivity.class);
            intent.addFlags(67108864);
            startActivity(intent);
            finish();
        } else if (view.equals(this.b)) {
            startActivity(new Intent(this, BindAccountActivity.class));
        } else if (view.equals(this.f)) {
            startActivityForResult(new Intent(this, SetLocationActivity.class), 1);
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.addinfo);
        a((byte) 0, (int) R.string.cancel, this);
        b((byte) 0, R.string.save, this);
        setTitle((int) R.string.add_userifo);
        this.b = (Button) findViewById(R.id.titleBtnLeft);
        this.b.setOnClickListener(this);
        this.a = (Button) findViewById(R.id.titleBtnRight);
        this.a.setOnClickListener(this);
        this.c = (TextView) findViewById(R.id.nicknameText);
        this.d = (TextView) findViewById(R.id.nicknameText);
        this.f = (RelativeLayout) findViewById(R.id.adresslayout);
        this.f.setOnClickListener(this);
        this.e = (TextView) findViewById(R.id.addresstext1);
        if (!(this.l == 0 || this.m == 0)) {
            this.e.setText(a(this.l) + "   " + a(this.m, this.l));
        }
        com.vodone.caibo.database.a.a();
        c[] a2 = com.vodone.caibo.database.a.a(this);
        this.k = a2[a2.length - 1].b;
        this.c.setText(this.k);
    }
}
