package com.vodone.caibo.activity;

import android.app.TabActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TabHost;
import com.vodone.caibo.R;

public class PersonMainTabActivity extends TabActivity implements CompoundButton.OnCheckedChangeListener {
    private TabHost a;
    private Intent b;
    private Intent c;
    private Intent d;
    private Intent e;
    private String f;
    private String g;

    public static Intent a(Context context, String str, String str2, int i) {
        Intent intent = new Intent(context, PersonMainTabActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("user_id", str);
        bundle.putString("user_name", str2);
        bundle.putByte("activity", (byte) i);
        intent.putExtras(bundle);
        return intent;
    }

    private TabHost.TabSpec a(String str, int i, int i2, Intent intent) {
        return this.a.newTabSpec(str).setIndicator(getString(i), getResources().getDrawable(i2)).setContent(intent);
    }

    private void a(int i) {
        this.a = getTabHost();
        TabHost tabHost = this.a;
        TabHost.TabSpec[] tabSpecArr = {a("mblog_tab", (int) R.string.mblog, (int) R.drawable.icon_person_blog, this.b), a("topic_tab", (int) R.string.mtopic, (int) R.drawable.icon_person_topic, this.c), a("fans_tab", (int) R.string.mfans, (int) R.drawable.icon_person_fans, this.d), a("attent_tab", (int) R.string.mattention, (int) R.drawable.icon_person_atten, this.e)};
        TabHost.TabSpec tabSpec = tabSpecArr[0];
        switch (i) {
            case 1:
                tabSpecArr[0] = tabSpecArr[1];
                tabSpecArr[1] = tabSpec;
                break;
            case 2:
                tabSpecArr[0] = tabSpecArr[2];
                tabSpecArr[2] = tabSpec;
                break;
            case 3:
                tabSpecArr[0] = tabSpecArr[3];
                tabSpecArr[3] = tabSpec;
                break;
        }
        for (TabHost.TabSpec addTab : tabSpecArr) {
            tabHost.addTab(addTab);
        }
    }

    public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
        if (z) {
            switch (compoundButton.getId()) {
                case R.id.person_radio_button0 /*2131427519*/:
                    this.a.setCurrentTabByTag("mblog_tab");
                    return;
                case R.id.person_radio_button1 /*2131427520*/:
                    this.a.setCurrentTabByTag("topic_tab");
                    return;
                case R.id.person_radio_button2 /*2131427521*/:
                    this.a.setCurrentTabByTag("fans_tab");
                    return;
                case R.id.person_radio_button3 /*2131427522*/:
                    this.a.setCurrentTabByTag("attent_tab");
                    return;
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        Bundle extras = getIntent().getExtras();
        this.f = extras.getString("user_id");
        this.g = extras.getString("user_name");
        byte b2 = extras.getByte("activity");
        setContentView((int) R.layout.person_maintabs);
        String str = this.f;
        String str2 = this.g;
        Intent intent = new Intent(this, TimeLineActivity.class);
        Bundle bundle2 = new Bundle();
        bundle2.putByte("timeline", (byte) 1);
        bundle2.putString("userid", str);
        bundle2.putString("username", str2);
        intent.putExtras(bundle2);
        this.b = intent;
        String str3 = this.f;
        Intent intent2 = new Intent(this, TimeLineActivity.class);
        Bundle bundle3 = new Bundle();
        bundle3.putByte("timeline", (byte) 25);
        bundle3.putString("userid", str3);
        intent2.putExtras(bundle3);
        this.c = intent2;
        this.d = FriendActivity.a(this, this.f);
        String str4 = this.f;
        Intent intent3 = new Intent(this, FriendActivity.class);
        Bundle bundle4 = new Bundle();
        bundle4.putByte("friend_key", (byte) 2);
        bundle4.putString("friend_user_id", str4);
        intent3.putExtras(bundle4);
        this.e = intent3;
        ((RadioButton) findViewById(R.id.person_radio_button0)).setOnCheckedChangeListener(this);
        ((RadioButton) findViewById(R.id.person_radio_button1)).setOnCheckedChangeListener(this);
        ((RadioButton) findViewById(R.id.person_radio_button2)).setOnCheckedChangeListener(this);
        ((RadioButton) findViewById(R.id.person_radio_button3)).setOnCheckedChangeListener(this);
        a(b2);
        RadioButton radioButton = null;
        switch (b2) {
            case 0:
                radioButton = (RadioButton) findViewById(R.id.person_radio_button0);
                break;
            case 1:
                radioButton = (RadioButton) findViewById(R.id.person_radio_button1);
                break;
            case 2:
                radioButton = (RadioButton) findViewById(R.id.person_radio_button2);
                break;
            case 3:
                radioButton = (RadioButton) findViewById(R.id.person_radio_button3);
                break;
        }
        if (radioButton != null) {
            radioButton.setChecked(true);
        }
    }
}
