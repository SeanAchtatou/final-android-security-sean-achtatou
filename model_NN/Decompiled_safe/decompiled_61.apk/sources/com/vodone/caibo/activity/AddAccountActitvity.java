package com.vodone.caibo.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.vodone.caibo.R;
import com.vodone.caibo.b.l;
import com.vodone.caibo.service.c;

public class AddAccountActitvity extends BaseActivity implements View.OnClickListener {
    private LinearLayout a;
    private LinearLayout b;
    private LinearLayout c;
    private Button d;
    private Button e;
    private EditText f;
    private EditText k;
    private ProgressDialog l;

    public void onClick(View view) {
        boolean z;
        if (view.equals(this.d)) {
            String obj = this.f.getText().toString();
            String obj2 = this.k.getText().toString();
            if (obj == null || obj.length() == 0 || obj2 == null || obj2.length() == 0) {
                b((int) R.string.fullinfo);
                z = false;
            } else {
                z = true;
            }
            if (z) {
                String obj3 = this.f.getText().toString();
                String obj4 = this.k.getText().toString();
                if (!l.a(this).c(obj3)) {
                    this.l = ProgressDialog.show(this, getString(R.string.waiting), getString(R.string.loding));
                    short a2 = c.a().a(obj3, obj4, new ar(this, this.l, obj3, obj4, true));
                    this.l.setCancelable(true);
                    this.l.setOnCancelListener(new ao(a2));
                    return;
                }
                Toast.makeText(this, (int) R.string.accountexist, 1).show();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.login);
        a((byte) 0, (int) R.string.back, this.i);
        this.g.a.setBackgroundResource(R.drawable.title_left_button);
        b((byte) 2, -1, null);
        setTitle((int) R.string.addaccounts);
        this.a = (LinearLayout) findViewById(R.id.checkbox1);
        this.b = (LinearLayout) findViewById(R.id.bloglayout);
        this.c = (LinearLayout) findViewById(R.id.buttonlayout1);
        this.d = (Button) findViewById(R.id.loginss);
        this.d.setOnClickListener(this);
        this.e = (Button) findViewById(R.id.regist);
        this.f = (EditText) findViewById(R.id.UserName);
        this.k = (EditText) findViewById(R.id.PassWord);
        this.d.setVisibility(0);
        this.a.setVisibility(8);
        this.b.setVisibility(8);
        this.e.setVisibility(8);
        this.c.setVisibility(8);
    }
}
