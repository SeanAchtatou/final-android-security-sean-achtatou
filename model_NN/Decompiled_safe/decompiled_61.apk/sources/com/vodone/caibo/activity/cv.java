package com.vodone.caibo.activity;

import android.os.Message;
import android.widget.Toast;
import com.vodone.caibo.CaiboApp;
import com.vodone.caibo.R;
import com.vodone.caibo.b.d;
import com.vodone.caibo.database.a;

final class cv extends bb {
    private /* synthetic */ BlogDetailsActivity a;

    cv(BlogDetailsActivity blogDetailsActivity) {
        this.a = blogDetailsActivity;
    }

    public final void handleMessage(Message message) {
        int i = message.what;
        int i2 = message.arg1;
        if (this.a.b != null && this.a.b.isShowing()) {
            this.a.b.dismiss();
        }
        this.a.b = null;
        if (i == 0) {
            switch (i2) {
                case 101:
                    this.a.D.setImageResource(R.drawable.blogbottom_cancelfav);
                    this.a.k.o = true;
                    if (this.a.a == 1) {
                        a.a();
                        a.a(this.a, this.a.e, this.a.f, this.a.k);
                    }
                    Toast.makeText(this.a, (int) R.string.blogdetails_favoritedSuccess, 0).show();
                    return;
                case 102:
                    this.a.D.setImageResource(R.drawable.blogbottom_addfav);
                    this.a.k.o = false;
                    if (this.a.a == 1) {
                        a.a();
                        a.a(this.a, this.a.e, this.a.f, this.a.k);
                    }
                    Toast.makeText(this.a, (int) R.string.blogdetails_CancelfavoritedSuccess, 0).show();
                    return;
                case 103:
                    this.a.k = (d) message.obj;
                    this.a.a(this.a.k);
                    return;
                case 289:
                case 290:
                    String str = CaiboApp.a().b().a;
                    a.a();
                    a.a(this.a, str, this.a.f, this.a.e, (String) null);
                    if (289 == i2) {
                        Toast.makeText(this.a, "删除博文成功", 0).show();
                    } else if (290 == i2) {
                        Toast.makeText(this.a, "删除通知成功", 0).show();
                    }
                    this.a.finish();
                    return;
                case 1793:
                    d dVar = (d) message.obj;
                    this.a.o.setText(this.a.getString(R.string.main_comment) + String.valueOf(dVar.g));
                    this.a.p.setText(this.a.getString(R.string.transpond) + String.valueOf(dVar.f));
                    return;
                default:
                    return;
            }
        } else {
            switch (i2) {
                case 101:
                    Toast.makeText(this.a, "已添加收藏", 0).show();
                    this.a.D.setImageResource(R.drawable.blogbottom_cancelfav);
                    this.a.k.o = true;
                    if (this.a.a == 1) {
                        a.a();
                        a.a(this.a, this.a.e, this.a.f, this.a.k);
                        return;
                    }
                    return;
                case 102:
                    Toast.makeText(this.a, "已取消收藏", 0).show();
                    this.a.D.setImageResource(R.drawable.blogbottom_addfav);
                    this.a.k.o = false;
                    if (this.a.a == 1) {
                        a.a();
                        a.a(this.a, this.a.e, this.a.f, this.a.k);
                        return;
                    }
                    return;
                case 103:
                    int a2 = l.a(i);
                    if (a2 != 0) {
                        Toast.makeText(this.a, a2, 1).show();
                    }
                    this.a.y.setEnabled(false);
                    this.a.B.setEnabled(false);
                    this.a.C.setEnabled(false);
                    this.a.D.setEnabled(false);
                    this.a.E.setEnabled(false);
                    return;
                case 289:
                    Toast.makeText(this.a, "删除博文失败", 1).show();
                    return;
                default:
                    int a3 = l.a(i);
                    if (a3 != 0) {
                        Toast.makeText(this.a, a3, 1).show();
                        return;
                    }
                    return;
            }
        }
    }
}
