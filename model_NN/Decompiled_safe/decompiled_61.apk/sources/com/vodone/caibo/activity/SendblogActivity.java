package com.vodone.caibo.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.vodone.caibo.CaiboApp;
import com.vodone.caibo.R;
import com.vodone.caibo.b.d;
import com.vodone.caibo.b.e;
import com.vodone.caibo.b.g;
import com.vodone.caibo.database.a;
import com.vodone.caibo.service.c;
import java.io.File;

public class SendblogActivity extends BaseActivity implements View.OnClickListener {
    private CheckBox A;
    private LinearLayout B;
    private String C;
    private String D;
    private String E;
    private String F;
    private String G;
    private String H;
    private String I;
    private String J;
    private boolean K;
    private InputMethodManager L;
    private DisplayMetrics M;
    private AdapterView.OnItemClickListener N = new bh(this);
    private bb O = new bs(this);
    byte a;
    String b;
    String c;
    boolean d = false;
    ProgressDialog e;
    private int f = 1;
    private int k = 3;
    private int l = 4;
    private Button m;
    private Button n;
    private EditText o;
    /* access modifiers changed from: private */
    public ImageButton p;
    private ImageButton q;
    private ImageButton r;
    private ImageButton s;
    private ImageButton t;
    private ImageButton u;
    private ImageView v;
    private GridView w;
    /* access modifiers changed from: private */
    public TextView x;
    private CheckBox y;
    private CheckBox z;

    public static Intent a(Context context, String str) {
        Intent intent = new Intent(context, SendblogActivity.class);
        Bundle bundle = new Bundle();
        bundle.putByte("type", (byte) 3);
        bundle.putString("comment_id", str);
        intent.putExtras(bundle);
        return intent;
    }

    public static Intent a(Context context, String str, String str2, String str3, String str4, String str5) {
        Intent intent = new Intent(context, SendblogActivity.class);
        Bundle bundle = new Bundle();
        bundle.putByte("type", (byte) 5);
        if (str3 != null) {
            bundle.putString("comment_original_author", str3);
        }
        if (str2 != null) {
            bundle.putString("comment_author", str2);
        }
        if (str5 != null) {
            bundle.putString("orignal_id", str5);
        }
        if (str4 != null) {
            bundle.putString("forward_id", str4);
        }
        if (str != null) {
            bundle.putString("content", str);
        }
        intent.putExtras(bundle);
        return intent;
    }

    /* access modifiers changed from: private */
    public void a() {
        boolean a2;
        String str = CaiboApp.a().b().a;
        d dVar = new d();
        dVar.c = this.o.getText().toString().trim();
        if (this.c != null && this.c.length() > 0) {
            dVar.l = this.c;
        }
        if (this.a == 7) {
            dVar.a = this.b;
            a.a();
            a2 = a.a(this, this.b, dVar.p, dVar);
        } else {
            dVar.a = String.valueOf(System.currentTimeMillis());
            a.a();
            a2 = a.a(this, str, 20, (String) null, new d[]{dVar});
        }
        if (a2) {
            this.o.setText("");
            c((String) null);
            Toast.makeText(this, (int) R.string.save_draft_succ, 0).show();
            this.p.setImageResource(R.drawable.save_disable);
            this.d = false;
            return;
        }
        Toast.makeText(this, (int) R.string.save_draft_fail, 0).show();
    }

    /* access modifiers changed from: private */
    public void a(boolean z2) {
        if (this.L != null) {
            if (z2) {
                c(false);
                this.L.showSoftInput(this.o, 0);
            } else {
                this.L.hideSoftInputFromWindow(this.o.getWindowToken(), 0);
            }
            this.K = z2;
        }
    }

    public static Intent b(Context context, String str) {
        Intent intent = new Intent(context, SendblogActivity.class);
        Bundle bundle = new Bundle();
        bundle.putByte("type", (byte) 2);
        bundle.putString("comment_id", str);
        intent.putExtras(bundle);
        return intent;
    }

    /* access modifiers changed from: private */
    public void c(String str) {
        this.c = str;
        this.c = str;
        if (str == null) {
            this.v.setVisibility(8);
            return;
        }
        Bitmap a2 = e.a(str, e.a(this, 100.0f), e.a(this, 100.0f));
        if (a2 == null) {
            this.v.setVisibility(8);
            return;
        }
        this.v.setVisibility(0);
        this.v.setImageBitmap(a2);
    }

    /* access modifiers changed from: private */
    public void c(boolean z2) {
        if (z2) {
            if (this.M == null) {
                this.M = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(this.M);
            }
            this.w.setVisibility(0);
            this.w.setLayoutParams(new LinearLayout.LayoutParams(-1, this.M.heightPixels / 3));
            return;
        }
        this.w.setVisibility(8);
    }

    /* access modifiers changed from: private */
    public void d(String str) {
        this.o.getText().insert(this.o.getSelectionStart(), str + " ");
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i2 == -1) {
            c.a();
            c.a(this);
            switch (i) {
                case 1:
                    d(intent.getStringExtra("topic"));
                    return;
                case 2:
                    d(intent.getStringExtra("friend_nickname_key"));
                    return;
                case 3:
                    File d2 = g.d();
                    if (d2 != null) {
                        this.c = d2 + "/image_tmpPhoto.jpg";
                    } else {
                        Bitmap bitmap = (Bitmap) intent.getExtras().get("data");
                        String str = getCacheDir() + "/image_tmpPhoto.jpg";
                        e.a(bitmap, str, Bitmap.CompressFormat.JPEG);
                        bitmap.recycle();
                        this.c = str;
                    }
                    c(this.c);
                    return;
                case 4:
                    Cursor query = getContentResolver().query(intent.getData(), new String[]{"_data"}, null, null, null);
                    if (query != null && query.moveToFirst()) {
                        this.c = query.getString(0);
                    }
                    if (query != null) {
                        query.close();
                    }
                    c(this.c);
                    return;
                default:
                    return;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.vodone.caibo.service.c.a(java.lang.String, java.lang.String, boolean, com.windo.a.c.e):short
     arg types: [java.lang.String, java.lang.String, boolean, com.vodone.caibo.activity.bb]
     candidates:
      com.vodone.caibo.service.c.a(com.vodone.caibo.b.d, java.lang.String, int, com.windo.a.c.e):short
      com.vodone.caibo.service.c.a(java.lang.String, java.lang.String, int, com.windo.a.c.e):short
      com.vodone.caibo.service.c.a(java.lang.String, java.lang.String, boolean, com.windo.a.c.e):short */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.vodone.caibo.service.c.b(java.lang.String, java.lang.String, boolean, com.windo.a.c.e):short
     arg types: [java.lang.String, java.lang.String, boolean, com.vodone.caibo.activity.bb]
     candidates:
      com.vodone.caibo.service.c.b(java.lang.String, java.lang.String, int, com.windo.a.c.e):short
      com.vodone.caibo.service.c.b(java.lang.String, java.lang.String, boolean, com.windo.a.c.e):short */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x014b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onClick(android.view.View r11) {
        /*
            r10 = this;
            r9 = 320(0x140, float:4.48E-43)
            r8 = 480(0x1e0, float:6.73E-43)
            r7 = 0
            r6 = 1
            r5 = 0
            android.widget.ImageButton r0 = r10.p
            boolean r0 = r11.equals(r0)
            if (r0 == 0) goto L_0x0013
            r10.a()
        L_0x0012:
            return
        L_0x0013:
            android.widget.Button r0 = r10.n
            boolean r0 = r11.equals(r0)
            if (r0 == 0) goto L_0x01fa
            android.widget.EditText r0 = r10.o
            android.text.Editable r0 = r0.getText()
            java.lang.String r0 = r0.toString()
            java.lang.String r3 = r0.trim()
            if (r3 == 0) goto L_0x0031
            int r0 = r3.length()
            if (r0 != 0) goto L_0x0045
        L_0x0031:
            java.lang.String r0 = r10.c
            if (r0 != 0) goto L_0x0045
            r0 = 5
            byte r1 = r10.a
            if (r0 == r1) goto L_0x0045
            r0 = 2131296471(0x7f0900d7, float:1.821086E38)
            android.widget.Toast r0 = android.widget.Toast.makeText(r10, r0, r5)
            r0.show()
            goto L_0x0012
        L_0x0045:
            java.lang.String r0 = "gb2312"
            byte[] r0 = r3.getBytes(r0)     // Catch:{ Exception -> 0x0066 }
            int r0 = r0.length     // Catch:{ Exception -> 0x0066 }
            int r1 = r0 % 2
            if (r1 != 0) goto L_0x0061
            int r0 = r0 / 2
        L_0x0052:
            r1 = 140(0x8c, float:1.96E-43)
            if (r0 <= r1) goto L_0x006c
            r0 = 2131296472(0x7f0900d8, float:1.8210862E38)
            android.widget.Toast r0 = android.widget.Toast.makeText(r10, r0, r5)
            r0.show()
            goto L_0x0012
        L_0x0061:
            int r0 = r0 / 2
            int r0 = r0 + 1
            goto L_0x0052
        L_0x0066:
            r0 = move-exception
            int r0 = r3.length()
            goto L_0x0052
        L_0x006c:
            android.app.ProgressDialog r0 = r10.e
            if (r0 == 0) goto L_0x0077
            android.app.ProgressDialog r0 = r10.e
            r0.dismiss()
            r10.e = r7
        L_0x0077:
            r0 = -1
            byte r1 = r10.a
            switch(r1) {
                case 1: goto L_0x010a;
                case 2: goto L_0x01c0;
                case 3: goto L_0x01d4;
                case 4: goto L_0x00a4;
                case 5: goto L_0x018b;
                case 6: goto L_0x01e8;
                case 7: goto L_0x010a;
                case 8: goto L_0x010a;
                default: goto L_0x007d;
            }
        L_0x007d:
            if (r0 < 0) goto L_0x0012
            r1 = 2131296290(0x7f090022, float:1.8210493E38)
            java.lang.String r1 = r10.getString(r1)
            r2 = 2131296441(0x7f0900b9, float:1.8210799E38)
            java.lang.String r2 = r10.getString(r2)
            android.app.ProgressDialog r1 = android.app.ProgressDialog.show(r10, r1, r2)
            r10.e = r1
            android.app.ProgressDialog r1 = r10.e
            r1.setCancelable(r6)
            android.app.ProgressDialog r1 = r10.e
            com.vodone.caibo.activity.ao r2 = new com.vodone.caibo.activity.ao
            r2.<init>(r0)
            r1.setOnCancelListener(r2)
            goto L_0x0012
        L_0x00a4:
            com.vodone.caibo.b.d r0 = new com.vodone.caibo.b.d
            r0.<init>()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.StringBuffer r2 = new java.lang.StringBuffer
            java.lang.String r4 = "#android客户端意见反馈#,版本"
            r2.<init>(r4)
            com.vodone.caibo.CaiboApp r4 = com.vodone.caibo.CaiboApp.a()
            r5 = 2131296259(0x7f090003, float:1.821043E38)
            java.lang.String r4 = r4.getString(r5)
            r2.append(r4)
            java.lang.String r4 = "型号:"
            r2.append(r4)
            java.lang.String r4 = android.os.Build.MODEL
            r2.append(r4)
            java.lang.String r4 = " android "
            r2.append(r4)
            java.lang.String r4 = android.os.Build.VERSION.RELEASE
            r2.append(r4)
            java.lang.String r4 = "\r\n"
            r2.append(r4)
            java.lang.String r2 = r2.toString()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r1 = r1.toString()
            r0.c = r1
            java.lang.String r1 = r10.c
            if (r1 == 0) goto L_0x00fe
            java.lang.String r1 = r10.c
            int r1 = r1.length()
            if (r1 <= 0) goto L_0x00fe
            java.lang.String r1 = r10.c
            r0.l = r1
        L_0x00fe:
            com.vodone.caibo.service.c r1 = com.vodone.caibo.service.c.a()
            com.vodone.caibo.activity.bb r2 = r10.O
            short r0 = r1.a(r0, r2)
            goto L_0x007d
        L_0x010a:
            com.vodone.caibo.b.d r0 = new com.vodone.caibo.b.d
            r0.<init>()
            r0.c = r3
            java.lang.String r1 = r10.c
            if (r1 == 0) goto L_0x0124
            java.lang.String r1 = r10.c
            int r1 = r1.length()
            if (r1 <= 0) goto L_0x0124
            java.lang.String r1 = r10.c
            if (r1 != 0) goto L_0x0130
            r1 = r7
        L_0x0122:
            r0.l = r1
        L_0x0124:
            com.vodone.caibo.service.c r1 = com.vodone.caibo.service.c.a()
            com.vodone.caibo.activity.bb r2 = r10.O
            short r0 = r1.a(r0, r7, r5, r2)
            goto L_0x007d
        L_0x0130:
            r2 = 640(0x280, float:8.97E-43)
            java.lang.String r3 = "ImagedefauQualityStr"
            java.lang.String r3 = com.vodone.caibo.activity.af.c(r10, r3)
            if (r3 == 0) goto L_0x02a4
            java.lang.String r4 = "high_image"
            boolean r4 = r3.equals(r4)
            if (r4 == 0) goto L_0x016e
            r2 = 640(0x280, float:8.97E-43)
            r3 = r8
        L_0x0145:
            android.graphics.Bitmap r2 = com.vodone.caibo.b.e.a(r1, r3, r2)
            if (r2 == 0) goto L_0x0122
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.io.File r4 = r10.getCacheDir()
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r4 = "/image_tobesend.jpg"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            android.graphics.Bitmap$CompressFormat r4 = android.graphics.Bitmap.CompressFormat.JPEG
            boolean r4 = com.vodone.caibo.b.e.a(r2, r3, r4)
            if (r4 != 0) goto L_0x0186
            r2.recycle()
            goto L_0x0122
        L_0x016e:
            java.lang.String r4 = "middle_image"
            boolean r4 = r3.equals(r4)
            if (r4 == 0) goto L_0x0179
            r2 = r8
            r3 = r9
            goto L_0x0145
        L_0x0179:
            java.lang.String r4 = "loe_image"
            boolean r3 = r3.equals(r4)
            if (r3 == 0) goto L_0x02a4
            r2 = 240(0xf0, float:3.36E-43)
            r3 = r2
            r2 = r9
            goto L_0x0145
        L_0x0186:
            r2.recycle()
            r1 = r3
            goto L_0x0122
        L_0x018b:
            com.vodone.caibo.b.d r0 = new com.vodone.caibo.b.d
            r0.<init>()
            java.lang.String r1 = r10.E
            r0.m = r1
            android.widget.CheckBox r1 = r10.y
            boolean r1 = r1.isChecked()
            if (r1 == 0) goto L_0x02a1
            int r1 = r5 + 1
        L_0x019e:
            android.widget.CheckBox r2 = r10.z
            boolean r2 = r2.isChecked()
            if (r2 == 0) goto L_0x01a8
            int r1 = r1 + 2
        L_0x01a8:
            r0.c = r3
            java.lang.String r2 = r0.c
            if (r2 != 0) goto L_0x01b2
            java.lang.String r2 = ""
            r0.c = r2
        L_0x01b2:
            com.vodone.caibo.service.c r2 = com.vodone.caibo.service.c.a()
            java.lang.String r3 = r10.F
            com.vodone.caibo.activity.bb r4 = r10.O
            short r0 = r2.a(r0, r3, r1, r4)
            goto L_0x007d
        L_0x01c0:
            com.vodone.caibo.service.c r0 = com.vodone.caibo.service.c.a()
            java.lang.String r1 = r10.G
            android.widget.CheckBox r2 = r10.A
            boolean r2 = r2.isChecked()
            com.vodone.caibo.activity.bb r4 = r10.O
            short r0 = r0.a(r1, r3, r2, r4)
            goto L_0x007d
        L_0x01d4:
            com.vodone.caibo.service.c r0 = com.vodone.caibo.service.c.a()
            java.lang.String r1 = r10.G
            android.widget.CheckBox r2 = r10.A
            boolean r2 = r2.isChecked()
            com.vodone.caibo.activity.bb r4 = r10.O
            short r0 = r0.b(r1, r3, r2, r4)
            goto L_0x007d
        L_0x01e8:
            com.vodone.caibo.service.c r0 = com.vodone.caibo.service.c.a()
            java.lang.String r1 = r10.H
            java.lang.String r2 = r10.I
            java.lang.String r4 = r10.J
            com.vodone.caibo.activity.bb r5 = r10.O
            short r0 = r0.b(r1, r2, r3, r4, r5)
            goto L_0x007d
        L_0x01fa:
            android.widget.Button r0 = r10.m
            boolean r0 = r0.equals(r11)
            if (r0 != 0) goto L_0x0012
            android.widget.ImageButton r0 = r10.q
            boolean r0 = r11.equals(r0)
            if (r0 == 0) goto L_0x0213
            android.content.Intent r0 = com.vodone.caibo.activity.RearchActivity.a(r10, r6)
            r10.startActivityForResult(r0, r6)
            goto L_0x0012
        L_0x0213:
            android.widget.ImageButton r0 = r10.s
            boolean r0 = r11.equals(r0)
            if (r0 == 0) goto L_0x022f
            com.vodone.caibo.CaiboApp r0 = com.vodone.caibo.CaiboApp.a()
            com.vodone.caibo.b.c r0 = r0.b()
            java.lang.String r0 = r0.a
            android.content.Intent r0 = com.vodone.caibo.activity.SearchContactActivity.a(r10, r0, r6)
            r1 = 2
            r10.startActivityForResult(r0, r1)
            goto L_0x0012
        L_0x022f:
            android.widget.ImageButton r0 = r10.u
            boolean r0 = r11.equals(r0)
            if (r0 == 0) goto L_0x023e
            android.widget.ImageButton r0 = r10.u
            r10.openContextMenu(r0)
            goto L_0x0012
        L_0x023e:
            android.widget.ImageButton r0 = r10.t
            boolean r0 = r11.equals(r0)
            if (r0 == 0) goto L_0x0260
            android.widget.GridView r0 = r10.w
            int r0 = r0.getVisibility()
            r1 = 8
            if (r0 != r1) goto L_0x0258
            r10.c(r6)
            r10.a(r5)
            goto L_0x0012
        L_0x0258:
            r10.c(r5)
            r10.a(r6)
            goto L_0x0012
        L_0x0260:
            android.widget.EditText r0 = r10.o
            boolean r0 = r11.equals(r0)
            if (r0 == 0) goto L_0x0270
            r10.c(r5)
            r10.a(r6)
            goto L_0x0012
        L_0x0270:
            android.widget.ImageView r0 = r10.v
            boolean r0 = r11.equals(r0)
            if (r0 == 0) goto L_0x0012
            java.lang.String r0 = r10.c
            if (r0 == 0) goto L_0x0012
            android.app.AlertDialog$Builder r0 = new android.app.AlertDialog$Builder
            r0.<init>(r10)
            r1 = 2131296470(0x7f0900d6, float:1.8210858E38)
            java.lang.String r1 = r10.getString(r1)
            android.app.AlertDialog$Builder r0 = r0.setMessage(r1)
            r1 = 2131296451(0x7f0900c3, float:1.821082E38)
            com.vodone.caibo.activity.bj r2 = new com.vodone.caibo.activity.bj
            r2.<init>(r10)
            r0.setPositiveButton(r1, r2)
            r1 = 17039360(0x1040000, float:2.424457E-38)
            r0.setNegativeButton(r1, r7)
            r0.show()
            goto L_0x0012
        L_0x02a1:
            r1 = r5
            goto L_0x019e
        L_0x02a4:
            r3 = r8
            goto L_0x0145
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vodone.caibo.activity.SendblogActivity.onClick(android.view.View):void");
    }

    public boolean onContextItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 3:
                Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                File d2 = g.d();
                if (d2 != null) {
                    intent.putExtra("output", Uri.fromFile(new File(d2 + "/image_tmpPhoto.jpg")));
                }
                startActivityForResult(intent, 3);
                break;
            case 4:
                startActivityForResult(new Intent("android.intent.action.PICK", MediaStore.Images.Media.INTERNAL_CONTENT_URI), 4);
                break;
        }
        return super.onContextItemSelected(menuItem);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.writeblog);
        Bundle extras = getIntent().getExtras();
        this.p = (ImageButton) findViewById(R.id.save);
        this.p.setOnClickListener(this);
        this.q = (ImageButton) findViewById(R.id.topic);
        this.q.setOnClickListener(this);
        this.r = (ImageButton) findViewById(R.id.location);
        this.r.setOnClickListener(this);
        this.s = (ImageButton) findViewById(R.id.contact);
        this.s.setOnClickListener(this);
        this.t = (ImageButton) findViewById(R.id.phiz);
        this.t.setOnClickListener(this);
        this.o = (EditText) findViewById(R.id.lookorsay);
        this.o.setOnClickListener(this);
        this.n = (Button) findViewById(R.id.titleBtnRight);
        this.n.setOnClickListener(this);
        this.m = (Button) findViewById(R.id.titleBtnLeft);
        this.m.setOnClickListener(this);
        this.u = (ImageButton) findViewById(R.id.photo);
        this.u.setOnClickListener(this);
        registerForContextMenu(this.u);
        this.v = (ImageView) findViewById(R.id.photoview);
        this.v.setOnClickListener(this);
        this.x = (TextView) findViewById(R.id.count);
        this.y = (CheckBox) findViewById(R.id.comment_cbx);
        this.z = (CheckBox) findViewById(R.id.comment_root_cbx);
        this.A = (CheckBox) findViewById(R.id.fwto_me_cbx);
        this.B = (LinearLayout) findViewById(R.id.checklayout);
        this.w = (GridView) findViewById(R.id.grid);
        this.w.setAdapter((ListAdapter) new cs(this));
        this.w.setOnItemClickListener(this.N);
        this.o = (EditText) findViewById(R.id.lookorsay);
        this.o.setOnClickListener(this);
        this.o.addTextChangedListener(new bl(this));
        this.o.setOnFocusChangeListener(new bk(this));
        this.x.setText(String.valueOf(140));
        a((byte) 0, (int) R.string.back, this.i);
        this.g.a.setBackgroundResource(R.drawable.title_left_button);
        b((byte) 0, R.string.send, this);
        this.a = extras.getByte("type");
        this.G = extras.getString("comment_id");
        this.C = extras.getString("comment_original_author");
        this.D = extras.getString("comment_author");
        this.E = extras.getString("orignal_id");
        this.F = extras.getString("forward_id");
        this.H = extras.getString("userid");
        this.I = extras.getString("jbuserid");
        this.J = extras.getString("topicid");
        this.b = extras.getString("draft_id");
        if (1 == this.a || 4 == this.a || 7 == this.a || 8 == this.a) {
            this.p.setVisibility(0);
            this.p.setEnabled(false);
        } else {
            this.p.setVisibility(8);
        }
        if (extras.getString("content") != null) {
            this.o.setText(extras.getString("content"));
        }
        if (extras.getString("attach") != null) {
            c(extras.getString("attach"));
        }
        switch (this.a) {
            case 1:
            case 7:
            case 8:
                setTitle((int) R.string.newblog);
                break;
            case 2:
            case 3:
                if (this.a == 2) {
                    setTitle((int) R.string.publishcomment);
                } else {
                    setTitle((int) R.string.replycomment);
                }
                this.u.setVisibility(8);
                this.B.setVisibility(0);
                this.y.setVisibility(8);
                this.z.setVisibility(8);
                break;
            case 4:
                setTitle((int) R.string.feedbackidea);
                break;
            case 5:
                setTitle((int) R.string.forwarding);
                this.u.setVisibility(8);
                this.B.setVisibility(0);
                this.A.setVisibility(8);
                String string = getString(R.string.samecomment);
                this.y.setText(string + this.D);
                if (this.C == null) {
                    this.z.setVisibility(8);
                    break;
                } else {
                    this.z.setText(string + this.C);
                    break;
                }
            case 6:
                setTitle((int) R.string.report);
                this.p.setVisibility(8);
                this.q.setVisibility(8);
                this.r.setVisibility(8);
                this.s.setVisibility(8);
                this.t.setVisibility(8);
                this.u.setVisibility(8);
                break;
        }
        this.L = (InputMethodManager) getSystemService("input_method");
        this.o.requestFocus();
        new Handler().postDelayed(new bd(this), 500);
    }

    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        contextMenu.setHeaderTitle(getString(R.string.insert_image));
        contextMenu.add(0, 3, 0, getString(R.string.new_image));
        contextMenu.add(0, 4, 0, getString(R.string.old_image));
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4) {
            return super.onKeyDown(i, keyEvent);
        }
        if (this.w.isShown()) {
            c(false);
            return true;
        } else if (this.o.getText().toString().length() <= 0 || !this.d) {
            return super.onKeyDown(i, keyEvent);
        } else {
            new AlertDialog.Builder(this).setMessage(getString(R.string.save_exit_dialog)).setPositiveButton((int) R.string.ok, new bf(this)).setNegativeButton((int) R.string.cancel, new bg(this)).show();
            return true;
        }
    }
}
