package com.vodone.caibo.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.ClipboardManager;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.vodone.caibo.CaiboApp;
import com.vodone.caibo.R;
import com.vodone.caibo.b.d;
import com.vodone.caibo.b.g;
import com.vodone.caibo.b.j;
import com.vodone.caibo.database.a;
import com.windo.widget.RichTextView;
import com.windo.widget.c;

public class BlogDetailsActivity extends BaseActivity implements View.OnClickListener, c {
    private ImageButton A;
    /* access modifiers changed from: private */
    public ImageButton B;
    /* access modifiers changed from: private */
    public ImageButton C;
    /* access modifiers changed from: private */
    public ImageButton D;
    /* access modifiers changed from: private */
    public ImageButton E;
    private LinearLayout F;
    private LinearLayout G;
    private LinearLayout H;
    private RelativeLayout I;
    private String J;
    private String K;
    private com.vodone.caibo.b.c L;
    int a;
    ProgressDialog b;
    short c;
    String d;
    String e;
    byte f;
    d k;
    public bb l = new cv(this);
    private Bundle m;
    private Button n;
    /* access modifiers changed from: private */
    public Button o;
    /* access modifiers changed from: private */
    public Button p;
    private ImageView q;
    private ImageView r;
    private ImageView s;
    private TextView t;
    private RichTextView u;
    private RichTextView v;
    private TextView w;
    private TextView x;
    /* access modifiers changed from: private */
    public ImageButton y;
    private ImageButton z;

    public static Intent a(Context context, String str, int i, int i2) {
        Intent intent = new Intent(context, BlogDetailsActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("topicid", str);
        bundle.putByte("activity_type", (byte) i);
        bundle.putByte("list_type", (byte) i2);
        intent.putExtras(bundle);
        return intent;
    }

    private void a(View view, int i) {
        if (i == 4) {
            view.setTag(4);
            openContextMenu(view);
        } else if (i == 5) {
            view.setTag(5);
            openContextMenu(view);
        }
    }

    private void a(String str, String str2) {
        startActivity(PersonalInformationActivity.a(this, str, str2, 1));
    }

    public final void a(d dVar) {
        if (dVar != null) {
            this.d = dVar.h;
            this.J = dVar.i;
            this.e = dVar.a;
            g a2 = g.a(this);
            Bitmap a3 = a2.a(dVar.j);
            if (a3 == null) {
                a3 = a2.b();
                a2.a(dVar.j, new cr(this.q));
            }
            this.q.setImageBitmap(a3);
            if (this.k.o) {
                this.D.setImageResource(R.drawable.blogbottom_cancelfav);
            } else {
                this.D.setImageResource(R.drawable.blogbottom_addfav);
            }
            this.t.setText(dVar.i);
            this.u.setText(dVar.c);
            this.o.setText(getString(R.string.main_comment) + String.valueOf(dVar.g));
            this.p.setText(getString(R.string.transpond) + String.valueOf(dVar.f));
            if (dVar.b()) {
                this.r.setVisibility(0);
                Bitmap b2 = a2.b(dVar.k);
                if (b2 == null) {
                    b2 = a2.c();
                    a2.b(dVar.k, new cr(this.r));
                }
                this.r.setImageBitmap(b2);
            } else {
                this.r.setVisibility(8);
            }
            if (dVar.c()) {
                this.F.setVisibility(0);
                this.K = "@" + dVar.n.i + ":" + dVar.n.c;
                this.v.setText(this.K);
                this.w.setText(String.valueOf(dVar.n.g));
                this.x.setText(String.valueOf(dVar.n.f));
                if (dVar.n.b()) {
                    this.s.setVisibility(0);
                    Bitmap b3 = a2.b(dVar.n.k);
                    if (b3 == null) {
                        b3 = a2.c();
                        a2.b(dVar.n.k, new cr(this.s));
                    }
                    this.s.setImageBitmap(b3);
                    return;
                }
                this.s.setVisibility(8);
                return;
            }
            this.F.setVisibility(8);
        }
    }

    public final boolean a(String str, int i) {
        switch (i) {
            case 1:
                a((String) null, str.substring(1));
                break;
            case 2:
                String substring = str.startsWith("#") ? str.substring(1) : str;
                if (substring.endsWith("#")) {
                    substring = substring.substring(0, substring.length() - 1);
                }
                startActivity(TimeLineActivity.a(this, substring));
                break;
            case 3:
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
                break;
        }
        return false;
    }

    public void onClick(View view) {
        if (view.equals(this.I)) {
            a(this.d, this.J);
        } else if (view.equals(this.G) || view.equals(this.y)) {
            startActivity(TimeLineActivity.a(this, this.k.n.a, this.k.n.h));
        } else if (view.equals(this.H) || view.equals(this.z)) {
            startActivity(SendblogActivity.a(this, null, this.k.n.i, null, this.k.n.a, null));
        } else if (view.equals(this.r)) {
            startActivity(PreviewActivity.a(this, this.k.l));
        } else if (view.equals(this.s)) {
            startActivity(PreviewActivity.a(this, this.k.n.l));
        } else if (view.equals(this.A)) {
            this.c = com.vodone.caibo.service.c.a().q(this.e, this.l);
            this.b = ProgressDialog.show(this, null, "获取信息中.....");
            this.b.setCancelable(true);
            this.b.setOnCancelListener(new bv(this));
        } else if (view.equals(this.o)) {
            startActivity(TimeLineActivity.a(this, this.e, this.d));
        } else if (view.equals(this.B)) {
            startActivity(SendblogActivity.b(this, this.e));
        } else if (view.equals(this.C) || view.equals(this.p)) {
            if (this.k.c()) {
                StringBuffer stringBuffer = new StringBuffer("//@");
                stringBuffer.append(this.k.i);
                stringBuffer.append(":");
                stringBuffer.append(this.k.c);
                startActivity(SendblogActivity.a(this, stringBuffer.toString(), this.J, this.k.n.i, this.e, this.k.n.a));
                return;
            }
            startActivity(SendblogActivity.a(this, null, this.J, null, this.e, null));
        } else if (view.equals(this.D)) {
            if (this.k.o) {
                new AlertDialog.Builder(this).setTitle((int) R.string.main_news).setMessage((int) R.string.blogdetails_IsCancelfavoritedFailed).setPositiveButton((int) R.string.ok, new bq(this)).setNegativeButton((int) R.string.cancel, (DialogInterface.OnClickListener) null).show();
            } else {
                new AlertDialog.Builder(this).setTitle((int) R.string.main_news).setMessage((int) R.string.blogdetails_IsAdd_favorited).setPositiveButton((int) R.string.ok, new bx(this)).setNegativeButton((int) R.string.cancel, (DialogInterface.OnClickListener) null).show();
            }
        } else if (view.equals(this.E)) {
            if (this.L.a.equals(this.k.h)) {
                a(this.E, 5);
            } else {
                a(this.E, 4);
            }
        } else if (view.equals(this.g.d)) {
            new AlertDialog.Builder(this).setTitle((int) R.string.choose_operation).setItems(new String[]{getString(R.string.delete), getString(R.string.cancel)}, new by(this)).show();
        }
    }

    public boolean onContextItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 7:
                ClipboardManager clipboardManager = (ClipboardManager) getSystemService("clipboard");
                String str = (!this.k.b() || !this.k.c()) ? (this.k.b() || !this.k.c()) ? (!this.k.b() || this.k.c()) ? getString(R.string.copyto_sms_msg) + this.k.c + "\n" + getString(R.string.copyto_sms_source) : getString(R.string.copyto_sms_msg) + this.k.c + "\n" + getString(R.string.copyto_sms_pictureurl) + this.k.l + "\n" + getString(R.string.copyto_sms_source) : getString(R.string.copyto_sms_msg) + this.k.c + "\n" + getString(R.string.copyto_sms_originalblog) + this.k.n.c + "\n" + getString(R.string.copyto_sms_source) : getString(R.string.copyto_sms_msg) + this.k.c + "\n" + getString(R.string.copyto_sms_originalblog) + this.k.n.c + "\n" + getString(R.string.copyto_sms_pictureurl) + this.k.l + "\n" + getString(R.string.copyto_sms_source);
                clipboardManager.setText(str);
                new AlertDialog.Builder(this).setTitle((int) R.string.str_sms_share).setMessage((int) R.string.sms_share_msg).setPositiveButton((int) R.string.ok, new bm(this, str)).setNegativeButton((int) R.string.cancel, new bt(this)).show();
                break;
            case 8:
                ((ClipboardManager) getSystemService("clipboard")).setText(this.k.c);
                new AlertDialog.Builder(this).setTitle((int) R.string.str_copy).setMessage((int) R.string.copy_success_msg).setPositiveButton((int) R.string.ok, new ay(this)).setNegativeButton((int) R.string.cancel, new bn(this)).show();
                break;
            case 9:
                String str2 = this.L.a;
                String str3 = this.d;
                String str4 = this.e;
                Intent intent = new Intent(this, SendblogActivity.class);
                Bundle bundle = new Bundle();
                bundle.putByte("type", (byte) 6);
                bundle.putString("userid", str2);
                bundle.putString("jbuserid", str3);
                bundle.putString("topicid", str4);
                intent.putExtras(bundle);
                startActivity(intent);
                break;
            case 10:
                new AlertDialog.Builder(this).setTitle("是否确定删除此博文?").setPositiveButton((int) R.string.ok, new cp(this)).setNegativeButton((int) R.string.cancel, new ba(this)).show();
                com.vodone.caibo.service.c.a().i(this.d, this.J, this.l);
                break;
        }
        return super.onContextItemSelected(menuItem);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.blogdetails);
        this.m = getIntent().getExtras();
        this.e = this.m.getString("topicid");
        this.a = this.m.getByte("activity_type");
        this.f = this.m.getByte("list_type");
        a((byte) 0, (int) R.string.back, this.i);
        this.g.a.setBackgroundResource(R.drawable.title_left_button);
        if (this.a == 4) {
            b((byte) 0, R.string.delete, this);
            a("通知");
        } else {
            b((byte) 1, R.drawable.home_icon, this.j);
            setTitle((int) R.string.weibodetails);
        }
        this.n = (Button) findViewById(R.id.mBlogdetails_nextBtn);
        this.n.setOnClickListener(this);
        this.o = (Button) findViewById(R.id.mBlog_comment_numBtn);
        this.o.setOnClickListener(this);
        this.p = (Button) findViewById(R.id.mBlog_retweet_numBtn);
        this.p.setOnClickListener(this);
        this.q = (ImageView) findViewById(R.id.mUserImage);
        this.r = (ImageView) findViewById(R.id.mBlogContentImage);
        this.r.setOnClickListener(this);
        this.s = (ImageView) findViewById(R.id.mOriginalBlogContentImage);
        this.s.setOnClickListener(this);
        this.t = (TextView) findViewById(R.id.mUsernameText);
        this.u = (RichTextView) findViewById(R.id.mBlogContentText);
        this.v = (RichTextView) findViewById(R.id.mOriginalBlogContentText);
        this.w = (TextView) findViewById(R.id.mOriginalBlog_comment_numText);
        this.x = (TextView) findViewById(R.id.mOriginalBlog_retweet_numText);
        this.y = (ImageButton) findViewById(R.id.ImageButton_comment);
        this.y.setOnClickListener(this);
        this.z = (ImageButton) findViewById(R.id.ImageButton_retweet);
        this.z.setOnClickListener(this);
        this.A = (ImageButton) findViewById(R.id.blogdetails_refresh);
        this.A.setOnClickListener(this);
        this.B = (ImageButton) findViewById(R.id.blogdetails_comment);
        this.B.setOnClickListener(this);
        this.C = (ImageButton) findViewById(R.id.blogdetails_retweet);
        this.C.setOnClickListener(this);
        this.D = (ImageButton) findViewById(R.id.blogdetails_favorited);
        this.D.setOnClickListener(this);
        this.E = (ImageButton) findViewById(R.id.blogdetails_more);
        this.E.setOnClickListener(this);
        registerForContextMenu(this.E);
        this.F = (LinearLayout) findViewById(R.id.LinearLayout_blogoriginalcontent);
        this.G = (LinearLayout) findViewById(R.id.LinearLayout_OriginalBlog_comment_num);
        this.G.setOnClickListener(this);
        this.H = (LinearLayout) findViewById(R.id.LinearLayout_OriginalBlog_teweet_num);
        this.H.setOnClickListener(this);
        this.I = (RelativeLayout) findViewById(R.id.RelativeLayout_blogdetails_userInfoBar);
        this.I.setOnClickListener(this);
        this.u.a(true);
        this.v.a(true);
        this.u.a(this);
        this.v.a(this);
        this.L = CaiboApp.a().b();
        if (this.a == 1) {
            a.a();
            Cursor a2 = a.a(this, this.L.a, this.e, this.f);
            a.a();
            this.k = a.a(a2, (d) null);
            a(this.k);
            a2.close();
            com.vodone.caibo.service.c.a().f(this.L.a, this.e, this.l);
        } else if (this.a == 2) {
            this.b = ProgressDialog.show(this, null, "获取微博信息中.....");
            this.b.setCancelable(true);
            com.vodone.caibo.service.c.a().q(this.e, this.l);
        } else if (this.a == 4) {
            this.I.setVisibility(8);
            findViewById(R.id.RelativeLayout_bottom_bar).setVisibility(8);
            this.o.setVisibility(8);
            this.p.setVisibility(8);
            findViewById(R.id.bloginfobar).setVisibility(8);
            a.a();
            Cursor a3 = a.a(this, this.L.a, this.e, this.f);
            a.a();
            j a4 = a.a(a3, (j) null);
            a3.close();
            this.u.setText(a4.d);
        }
    }

    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        if (view.getTag() != null && view != null && contextMenu != null) {
            boolean equals = view.getTag().equals(4);
            contextMenu.setHeaderTitle(getString(R.string.more));
            contextMenu.add(0, 7, 0, getString(R.string.str_sms_share));
            contextMenu.add(0, 8, 0, getString(R.string.str_copy));
            if (equals) {
                contextMenu.add(0, 9, 0, getString(R.string.str_blog_report));
                contextMenu.add(0, 11, 0, getString(R.string.cancel));
                return;
            }
            contextMenu.add(0, 10, 0, getString(R.string.deleteblog));
            contextMenu.add(0, 11, 0, getString(R.string.cancel));
        }
    }
}
