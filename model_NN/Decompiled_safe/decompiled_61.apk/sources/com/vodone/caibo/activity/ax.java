package com.vodone.caibo.activity;

import android.app.ProgressDialog;
import android.os.Message;
import android.widget.Toast;
import com.vodone.caibo.b.m;
import com.vodone.caibo.database.a;

final class ax extends bb {
    private /* synthetic */ FriendActivity a;

    ax(FriendActivity friendActivity) {
        this.a = friendActivity;
    }

    public final void a(int i, int i2, int i3, Object obj) {
        m[] mVarArr = (m[]) obj;
        int i4 = 0;
        if (mVarArr != null && mVarArr.length > 0) {
            i4 = mVarArr.length;
            a.a();
            a.a(this.a, this.a.e, this.a.k, mVarArr);
        }
        if (i4 > 0) {
            this.a.a++;
        }
        super.a(i, i2, i4, null);
    }

    public final void handleMessage(Message message) {
        int i = message.what;
        int i2 = message.arg1;
        if (this.a.l != null && this.a.l.isShowing()) {
            this.a.l.dismiss();
            ProgressDialog unused = this.a.l = null;
        }
        if (i == 0) {
            if (message.arg2 == 0 && this.a.a == 0) {
                Toast.makeText(this.a, "暂无数据", 1).show();
            } else {
                this.a.m.d(message.arg2 == 30);
            }
            switch (i2) {
                case 82:
                case 130:
                case 132:
                case 134:
                case 148:
                    this.a.m.b(false);
                    this.a.b = -1;
                    return;
                case 129:
                case 131:
                case 133:
                case 147:
                    this.a.m.a(false);
                    this.a.c = -1;
                    return;
                default:
                    return;
            }
        } else {
            int a2 = l.a(i);
            if (a2 != 0) {
                Toast.makeText(this.a, a2, 1).show();
            }
        }
    }
}
