package com.vodone.caibo.activity;

import com.vodone.caibo.R;

public final class l {
    public static int a(int i) {
        switch (i) {
            case -1:
                return R.string._fail;
            case 257:
                return R.string.login_fail;
            case 513:
                return R.string.register_fail;
            case 515:
                return R.string.renincknam;
            case 516:
                return R.string.nicknamewrongful;
            case 517:
                return R.string.passwodwrongful;
            case 601:
                return R.string.netfail;
            case 602:
            case 603:
                return R.string.netnotconnection;
            case 604:
                return R.string.server_fail;
            case 769:
                return R.string.bindmobilfail;
            case 1025:
                return R.string.bindemailfail;
            case 1281:
                return R.string.trytimeout;
            case 1282:
                return R.string.getfail;
            case 1283:
                return R.string.usernothing;
            case 1284:
                return R.string.mismatching;
            case 1285:
                return R.string.usernull;
            case 1286:
                return R.string.numbernull;
            case 26369:
                return R.string.blogid_not_exist;
            default:
                return 0;
        }
    }
}
