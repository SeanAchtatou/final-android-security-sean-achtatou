package com.vodone.caibo.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.vodone.caibo.R;
import com.vodone.caibo.b.n;
import com.vodone.caibo.service.c;
import java.util.ArrayList;

public class RearchActivity extends BaseActivity implements View.OnClickListener {
    n[] a;
    ListView b;
    ArrayAdapter c;
    TextView d;
    TextView e;
    /* access modifiers changed from: private */
    public ProgressDialog f;
    private Button k;
    /* access modifiers changed from: private */
    public AutoCompleteTextView l;
    private int m;
    private bb n = new s(this);

    public static Intent a(Context context, int i) {
        Intent intent = new Intent(context, RearchActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt("from", i);
        intent.putExtras(bundle);
        return intent;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i2 == -1 && i == 0) {
            Intent intent2 = this.m == 1 ? new Intent(this, SendblogActivity.class) : new Intent(this, SendLetterActivity.class);
            intent2.putExtra("topic", intent.getStringExtra("topic"));
            setResult(-1, intent2);
            finish();
        }
    }

    public void onClick(View view) {
        if (view.equals(this.k)) {
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.inserttheme);
        this.g.a.setBackgroundResource(R.drawable.title_left_button);
        this.g.a.setText((int) R.string.back);
        b((byte) 2, -1, null);
        setTitle((int) R.string.inserttheme);
        this.k = (Button) findViewById(R.id.titleBtnLeft);
        this.k.setOnClickListener(this);
        this.l = (AutoCompleteTextView) findViewById(R.id.auto);
        this.l.addTextChangedListener(new u(this));
        this.b = (ListView) findViewById(R.id.listView1);
        this.c = new ArrayAdapter(this, (int) R.layout.topic_list, new ArrayList());
        this.b.setOnItemClickListener(new t(this));
        this.d = (TextView) LayoutInflater.from(this).inflate((int) R.layout.topic_list, (ViewGroup) null);
        this.e = (TextView) LayoutInflater.from(this).inflate((int) R.layout.topic_list, (ViewGroup) null);
        this.e.setText((int) R.string.square_hottopic);
        this.b.addFooterView(this.e);
        this.b.setAdapter((ListAdapter) this.c);
        this.b.removeFooterView(this.e);
        getWindow().setSoftInputMode(18);
        if (this.f != null && this.f.isShowing()) {
            this.f.dismiss();
            this.f = null;
        }
        this.f = ProgressDialog.show(this, getString(R.string.wait), getString(R.string.loding));
        this.f.setCancelable(true);
        this.f.setOnCancelListener(new ao(c.a().k(this.n)));
    }
}
