package com.vodone.caibo.activity;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import com.windo.widget.a;

public final class cs extends BaseAdapter {
    private Context a;

    public cs(Context context) {
        this.a = context;
    }

    public final int getCount() {
        return a.a(this.a).a();
    }

    public final Object getItem(int i) {
        return a.a(this.a).a(i);
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        ImageView imageView;
        if (view == null) {
            imageView = new ImageView(this.a);
            imageView.setPadding(10, 10, 10, 10);
            imageView.setScaleType(ImageView.ScaleType.CENTER);
            imageView.setLayoutParams(new AbsListView.LayoutParams(-1, -1));
        } else {
            imageView = (ImageView) view;
        }
        imageView.setImageResource(a.a(this.a).b(i));
        imageView.setTag(a.a(this.a).a(i));
        return imageView;
    }
}
