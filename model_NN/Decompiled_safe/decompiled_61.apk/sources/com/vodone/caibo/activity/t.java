package com.vodone.caibo.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import com.vodone.caibo.b.n;

final class t implements AdapterView.OnItemClickListener {
    private /* synthetic */ RearchActivity a;

    t(RearchActivity rearchActivity) {
        this.a = rearchActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.vodone.caibo.activity.TimeLineActivity.a(android.content.Context, boolean):android.content.Intent
     arg types: [com.vodone.caibo.activity.RearchActivity, int]
     candidates:
      com.vodone.caibo.activity.TimeLineActivity.a(android.content.Context, java.lang.String):android.content.Intent
      com.vodone.caibo.activity.TimeLineActivity.a(android.content.Context, boolean):android.content.Intent */
    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        if (view.equals(this.a.d)) {
            String obj = this.a.l.getText().toString();
            Intent intent = new Intent();
            intent.putExtra("topic", "#" + obj + "#");
            this.a.setResult(-1, intent);
            this.a.finish();
            this.a.getWindow().setSoftInputMode(18);
        } else if (view.equals(this.a.e)) {
            this.a.startActivityForResult(TimeLineActivity.a((Context) this.a, true), 0);
        } else {
            Intent intent2 = new Intent();
            intent2.putExtra("topic", ((n) this.a.b.getItemAtPosition(i)).toString());
            this.a.setResult(-1, intent2);
            this.a.finish();
            this.a.getWindow().setSoftInputMode(18);
        }
    }
}
