package com.vodone.caibo.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.vodone.caibo.R;
import com.vodone.caibo.service.c;

public class BindActivity extends BaseActivity implements View.OnClickListener {
    ProgressDialog a;
    public Button b;
    private String c = null;
    private Button d;
    private Button e;
    private Button f;
    private EditText k;
    private EditText l;
    private EditText m;
    private TextView n;
    /* access modifiers changed from: private */
    public TextView o;
    private String p;
    /* access modifiers changed from: private */
    public Boolean q = true;
    private bb r = new be(this);

    public void onClick(View view) {
        boolean z;
        if (this.c.equals("mobilebind")) {
            String obj = this.k.getText().toString();
            if (view.equals(this.b)) {
                if (obj.length() != 0) {
                    this.a = ProgressDialog.show(this, getString(R.string.waiting), getString(R.string.sendcode));
                    this.a.setCancelable(true);
                    this.a.setOnCancelListener(new ao(c.a().c(obj, this.p, this.r)));
                    return;
                }
                b((int) R.string.phonenumnull);
            } else if (view.equals(this.f)) {
                String obj2 = this.m.getText().toString();
                if (this.q.booleanValue()) {
                    b((int) R.string.notgetcode);
                } else if (!obj2.equals("")) {
                    this.a = ProgressDialog.show(this, getString(R.string.waiting), getString(R.string.binding));
                    this.a.setCancelable(true);
                    this.a.setOnCancelListener(new ao(c.a().d(obj2, this.p, this.r)));
                } else {
                    b((int) R.string.inputcode);
                }
            }
        } else if (!this.c.equals("emailbind")) {
        } else {
            if (view.equals(this.e)) {
                startActivity(new Intent(this, BindAccountActivity.class));
            } else if (view.equals(this.d)) {
                String obj3 = this.l.getText().toString();
                if (obj3.length() == 0) {
                    b((int) R.string.emailnull);
                    z = false;
                } else if (obj3.indexOf(64) == -1 || obj3.indexOf(46) == -1) {
                    b((int) R.string.emailwrong);
                    z = false;
                } else {
                    z = true;
                }
                if (z) {
                    this.a = ProgressDialog.show(this, getString(R.string.waiting), getString(R.string.sendemailing));
                    this.a.setCancelable(true);
                    this.a.setOnCancelListener(new ao(c.a().e(this.p, obj3, this.r)));
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Bundle extras = getIntent().getExtras();
        this.c = extras.getString("bind");
        this.p = extras.getString("usernamee");
        if (this.c.equals("mobilebind")) {
            setContentView((int) R.layout.mobileaccount);
            a((byte) 0, (int) R.string.back, this.i);
            this.g.a.setBackgroundResource(R.drawable.title_left_button);
            b((byte) 0, R.string.finish, this);
            setTitle((int) R.string.bind_mobile);
            this.b = (Button) findViewById(R.id.mobilebindBut);
            this.b.setOnClickListener(this);
            this.f = (Button) findViewById(R.id.titleBtnRight);
            this.f.setOnClickListener(this);
            this.k = (EditText) findViewById(R.id.PhoneEdit1);
            this.k.setOnClickListener(this);
            this.m = (EditText) findViewById(R.id.checkCode);
            this.m.setOnClickListener(this);
            return;
        }
        setContentView((int) R.layout.emailbind);
        a((byte) 0, (int) R.string.back, this.i);
        this.g.a.setBackgroundResource(R.drawable.title_left_button);
        b((byte) 2, -1, null);
        setTitle((int) R.string.bind_email);
        this.d = (Button) findViewById(R.id.sendButs);
        this.d.setOnClickListener(this);
        this.l = (EditText) findViewById(R.id.emailEdit1);
        this.l.setOnClickListener(this);
        this.n = (TextView) findViewById(R.id.emailbindpointtext);
    }
}
