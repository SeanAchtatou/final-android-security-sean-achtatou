package com.vodone.caibo.activity;

import android.graphics.Bitmap;
import android.widget.ImageView;
import android.widget.Toast;
import com.vodone.caibo.R;
import com.vodone.caibo.b.h;

final class i implements h {
    private ImageView a;
    private /* synthetic */ ap b;

    public i(ap apVar, ImageView imageView) {
        this.b = apVar;
        this.a = imageView;
    }

    public final void a() {
        this.b.d.setVisibility(8);
        this.b.dismiss();
        Toast.makeText(this.b.b, (int) R.string.image_loaded_error_text, 0).show();
    }

    public final void a(Bitmap bitmap) {
        this.b.d.setVisibility(8);
        this.a.setImageBitmap(bitmap);
    }
}
