package com.vodone.caibo.activity;

import android.os.Handler;
import android.os.Message;
import android.widget.Button;

public final class m extends Handler {
    private Button a = null;
    private int b = -1;
    private /* synthetic */ MessageGroup c;

    public m(MessageGroup messageGroup, int i, Button button, int i2) {
        this.c = messageGroup;
        this.a = button;
        this.b = i;
        if (i2 > 0) {
            this.a.setText(messageGroup.getString(this.b) + "(" + i2 + ")");
        }
    }

    public final void handleMessage(Message message) {
        if (message.what == 100001) {
            int i = message.arg2;
            if (i == 0) {
                this.a.setText(this.b);
                return;
            }
            this.a.setText(this.c.getString(this.b) + "(" + i + ")");
        }
    }
}
