package com.vodone.caibo.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.vodone.caibo.R;
import com.vodone.caibo.a;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.SAXException;

public class SetLocationActivity extends BaseActivity implements AdapterView.OnItemClickListener {
    private ListView a;
    /* access modifiers changed from: private */
    public int b;
    /* access modifiers changed from: private */
    public int c;
    private List d = new ArrayList();
    private String[] e;
    private a f;

    public static List a(Context context) {
        ArrayList arrayList = new ArrayList();
        InputStream openRawResource = context.getResources().openRawResource(R.raw.city);
        try {
            dj djVar = new dj();
            SAXParserFactory.newInstance().newSAXParser().parse(openRawResource, djVar);
            return djVar.a();
        } catch (SAXException e2) {
            e2.printStackTrace();
            return arrayList;
        } catch (IOException e3) {
            e3.printStackTrace();
            return arrayList;
        } catch (ParserConfigurationException e4) {
            e4.printStackTrace();
            return arrayList;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.location);
        this.d = a((Context) this);
        this.a = (ListView) findViewById(16908298);
        this.a.setAdapter((ListAdapter) new a(this, this.d));
        this.a.setOnItemClickListener(this);
        a((byte) 0, (int) R.string.back, this.i);
        this.g.a.setBackgroundResource(R.drawable.title_left_button);
        setTitle((int) R.string.edit_location_setting);
        b((byte) 2, -1, null);
    }

    public void onItemClick(AdapterView adapterView, View view, int i, long j) {
        this.f = (a) this.d.get(i);
        this.b = i + 1;
        this.e = this.f.b();
        new AlertDialog.Builder(this).setItems(this.e, new aq(this)).show();
    }
}
