package com.vodone.caibo.activity;

import android.os.Bundle;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import com.vodone.caibo.R;

public class SetThemeActivity extends BaseActivity {
    /* access modifiers changed from: private */
    public RadioButton a;
    /* access modifiers changed from: private */
    public RadioButton b;
    private RelativeLayout c;
    private RelativeLayout d;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.settheme);
        a((byte) 0, (int) R.string.back, this.i);
        this.g.a.setBackgroundResource(R.drawable.title_left_button);
        b((byte) 2, -1, null);
        setTitle((int) R.string.title_theme);
        this.a = (RadioButton) findViewById(R.id.radioButton_listTheme);
        this.b = (RadioButton) findViewById(R.id.radioButton_bubbleTheme);
        this.a.setClickable(false);
        this.b.setClickable(false);
        String c2 = af.c(this, "theme");
        if (c2 == null || c2.equals("")) {
            c2 = "listtheme";
        }
        if (c2.equals("listtheme")) {
            this.a.setChecked(true);
            this.b.setChecked(false);
        } else {
            this.a.setChecked(false);
            this.b.setChecked(true);
        }
        this.c = (RelativeLayout) findViewById(R.id.RelativeLayout_listTheme);
        this.c.setOnClickListener(new cw(this));
        this.d = (RelativeLayout) findViewById(R.id.RelativeLayout_bubbleTheme);
        this.d.setOnClickListener(new ct(this));
    }
}
