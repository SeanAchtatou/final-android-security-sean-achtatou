package com.vodone.caibo.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.widget.Toast;
import com.vodone.caibo.R;
import com.vodone.caibo.b.a;

final class di extends bb {
    private /* synthetic */ FindPasswordActivity a;

    di(FindPasswordActivity findPasswordActivity) {
        this.a = findPasswordActivity;
    }

    public final void handleMessage(Message message) {
        int i = message.what;
        if (i == -1) {
            Toast.makeText(this.a, (int) R.string.nameinexist, 1).show();
            return;
        }
        Bundle bundle = new Bundle();
        a aVar = (a) message.obj;
        if (aVar == null) {
            bundle.putString("isnull", "nothingbind");
        } else if (aVar.c.length() != 0 && aVar.d.length() != 0) {
            af.a(this.a, "phone", aVar.d);
            af.a(this.a, "emailss", aVar.c);
            bundle.putString("isnull", "");
        } else if (aVar.d.length() != 0 && aVar.c.length() == 0) {
            af.a(this.a, "phone", aVar.d);
            bundle.putString("isnull", "notemail");
        } else if (aVar.c.length() != 0 && aVar.d.length() == 0) {
            af.a(this.a, "emailss", aVar.c);
            bundle.putString("isnull", "notphone");
        }
        Intent intent = new Intent(this.a, FindPasswords.class);
        intent.putExtras(bundle);
        if (i == 8) {
            Toast.makeText(this.a, (int) R.string.bindstatus_nothing, 1).show();
        } else if (i == 2051) {
            Toast.makeText(this.a, (int) R.string.bindstatux_all, 1).show();
        } else if (i == 2050) {
            Toast.makeText(this.a, (int) R.string.bindstatus_mail, 1).show();
        } else if (i == 2049) {
            Toast.makeText(this.a, (int) R.string.bindstatus_phone, 1).show();
        } else if (i == -1) {
            Toast.makeText(this.a, (int) R.string.bindstatus_fail, 1).show();
        } else {
            int a2 = l.a(i);
            if (a2 != 0) {
                Toast.makeText(this.a, a2, 1).show();
            }
        }
        af.a(this.a, "findpasswd_nickName", this.a.c);
        this.a.startActivity(intent);
    }
}
