package com.vodone.caibo.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import com.vodone.caibo.R;

public class TopicListActivity extends BaseActivity implements View.OnClickListener {
    public void onClick(View view) {
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        TextView textView = new TextView(this);
        textView.setBackgroundColor(-657931);
        textView.setText((int) R.string.wait_topic);
        textView.setGravity(17);
        setContentView(textView);
        setTitle((int) R.string.mtopic);
        a((byte) 0, (int) R.string.back, this.i);
        b((byte) 1, R.drawable.home_icon, this.j);
        this.g.a.setBackgroundResource(R.drawable.title_left_button);
    }
}
