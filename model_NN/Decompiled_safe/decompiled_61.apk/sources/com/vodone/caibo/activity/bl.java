package com.vodone.caibo.activity;

import android.text.Editable;
import android.text.TextWatcher;
import com.vodone.caibo.R;

final class bl implements TextWatcher {
    private /* synthetic */ SendblogActivity a;

    bl(SendblogActivity sendblogActivity) {
        this.a = sendblogActivity;
    }

    public final void afterTextChanged(Editable editable) {
    }

    public final void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public final void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        int length;
        try {
            int length2 = charSequence.toString().getBytes("gb2312").length;
            length = length2 % 2 == 0 ? length2 / 2 : (length2 / 2) + 1;
        } catch (Exception e) {
            length = charSequence.toString().length();
        }
        int i4 = 140 - length;
        this.a.x.setText(i4 + "");
        if (i4 > 140) {
            this.a.x.setTextColor(-65536);
        } else {
            this.a.x.setTextColor(-6898470);
        }
        if (charSequence.length() > 0) {
            this.a.p.setImageResource(R.drawable.save);
            this.a.p.setEnabled(true);
            this.a.d = true;
            return;
        }
        this.a.p.setImageResource(R.drawable.save_disable);
        this.a.p.setEnabled(false);
    }
}
