package com.vodone.caibo.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import com.vodone.caibo.R;

public class ReSetPasswd extends BaseActivity implements View.OnClickListener {
    private EditText a;
    private EditText b;
    private String c;
    private String d;
    private bb e = new ab(this);

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0037  */
    /* JADX WARNING: Removed duplicated region for block: B:28:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onClick(android.view.View r7) {
        /*
            r6 = this;
            com.vodone.caibo.activity.BaseLayout r0 = r6.g
            com.windo.widget.WithIconButton r0 = r0.d
            boolean r0 = r7.equals(r0)
            if (r0 == 0) goto L_0x0087
            android.widget.EditText r0 = r6.a
            android.text.Editable r0 = r0.getText()
            java.lang.String r0 = r0.toString()
            android.widget.EditText r1 = r6.b
            android.text.Editable r1 = r1.getText()
            java.lang.String r1 = r1.toString()
            if (r0 == 0) goto L_0x002e
            int r2 = r0.length()
            if (r2 == 0) goto L_0x002e
            if (r1 == 0) goto L_0x002e
            int r2 = r0.length()
            if (r2 != 0) goto L_0x0059
        L_0x002e:
            r0 = 2131296394(0x7f09008a, float:1.8210703E38)
            r6.b(r0)
        L_0x0034:
            r0 = 0
        L_0x0035:
            if (r0 == 0) goto L_0x0058
            android.widget.EditText r0 = r6.a
            android.text.Editable r0 = r0.getText()
            java.lang.String r3 = r0.toString()
            android.widget.EditText r0 = r6.b
            android.text.Editable r0 = r0.getText()
            java.lang.String r4 = r0.toString()
            com.vodone.caibo.service.c r0 = com.vodone.caibo.service.c.a()
            java.lang.String r1 = r6.c
            java.lang.String r2 = r6.d
            com.vodone.caibo.activity.bb r5 = r6.e
            r0.a(r1, r2, r3, r4, r5)
        L_0x0058:
            return
        L_0x0059:
            boolean r1 = r0.equals(r1)
            if (r1 != 0) goto L_0x0066
            r0 = 2131296395(0x7f09008b, float:1.8210705E38)
            r6.b(r0)
            goto L_0x0034
        L_0x0066:
            int r1 = r0.length()
            r2 = 6
            if (r1 < r2) goto L_0x0075
            int r1 = r0.length()
            r2 = 16
            if (r1 <= r2) goto L_0x007c
        L_0x0075:
            r0 = 2131296278(0x7f090016, float:1.8210468E38)
            r6.b(r0)
            goto L_0x0034
        L_0x007c:
            int r0 = com.vodone.caibo.activity.RegisterAcitivity.c(r0)
            if (r0 == 0) goto L_0x0034
            r6.b(r0)
            r0 = 1
            goto L_0x0035
        L_0x0087:
            com.vodone.caibo.activity.BaseLayout r0 = r6.g
            com.windo.widget.WithIconButton r0 = r0.a
            boolean r0 = r7.equals(r0)
            if (r0 == 0) goto L_0x0058
            android.os.Bundle r0 = new android.os.Bundle
            r0.<init>()
            java.lang.String r1 = "findpass"
            java.lang.String r2 = "mobileaccount"
            r0.putString(r1, r2)
            android.content.Intent r1 = new android.content.Intent
            java.lang.Class<com.vodone.caibo.activity.MobileAccount> r2 = com.vodone.caibo.activity.MobileAccount.class
            r1.<init>(r6, r2)
            r1.putExtras(r0)
            r6.startActivity(r1)
            goto L_0x0058
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vodone.caibo.activity.ReSetPasswd.onClick(android.view.View):void");
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.resetpasswd);
        a((byte) 0, (int) R.string.back, this.i);
        this.g.a.setBackgroundResource(R.drawable.title_left_button);
        b((byte) 0, R.string.finish, this);
        setTitle((int) R.string.setnewpasswd);
        this.a = (EditText) findViewById(R.id.nameEdit2);
        this.a.setOnClickListener(this);
        this.b = (EditText) findViewById(R.id.passEdit2);
        this.b.setOnClickListener(this);
        Bundle extras = getIntent().getExtras();
        this.c = extras.getString("phone");
        this.d = extras.getString("phonecode");
    }
}
