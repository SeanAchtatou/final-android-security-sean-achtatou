package com.vodone.caibo.activity;

import android.os.Message;
import android.widget.Toast;
import com.vodone.caibo.CaiboApp;
import com.vodone.caibo.b.b;
import com.vodone.caibo.b.c;
import com.vodone.caibo.b.d;
import com.vodone.caibo.b.n;
import com.vodone.caibo.database.a;

final class cd extends bb {
    private /* synthetic */ TimeLineActivity a;

    cd(TimeLineActivity timeLineActivity) {
        this.a = timeLineActivity;
    }

    public final void a(int i, int i2, int i3, Object obj) {
        int i4;
        c b = CaiboApp.a().b();
        String str = b != null ? b.a : "99999999999999999";
        if (i2 == 17 || i2 == 19 || i2 == 33 || i2 == 35 || i2 == 37 || i2 == 39 || i2 == 41 || i2 == 49 || i2 == 54 || i2 == 51 || i2 == 56 || i2 == 81) {
            this.a.d = 0;
            a.a();
            a.b(this.a, str, this.a.a, this.a.b);
        } else if (i2 == 52) {
            this.a.d = 0;
            a.a();
            a.b(this.a, str, this.a.a, (String) null);
        } else if (i2 == 53) {
            this.a.d = 0;
            a.a();
            a.c(this.a, str, this.a.a, this.a.c);
        }
        if (obj == null) {
            i4 = 0;
        } else if (i2 == 51) {
            n[] nVarArr = (n[]) obj;
            int length = nVarArr.length;
            if (nVarArr != null && nVarArr.length > 0) {
                this.a.d++;
                a.a();
                a.a(this.a, str, this.a.a, nVarArr);
            }
            i4 = length;
        } else if (i2 == 53) {
            b[] bVarArr = (b[]) obj;
            int length2 = bVarArr.length;
            if (bVarArr != null && bVarArr.length > 0) {
                this.a.d++;
                a.a();
                a.a(this.a, str, this.a.a, this.a.c, bVarArr);
            }
            i4 = length2;
        } else if (i2 == 52) {
            n[] nVarArr2 = (n[]) obj;
            int length3 = nVarArr2.length;
            if (nVarArr2 != null && nVarArr2.length > 0) {
                this.a.d++;
                a.a();
                a.a(this.a, str, this.a.a, nVarArr2);
            }
            i4 = length3;
        } else {
            d[] dVarArr = (d[]) obj;
            int length4 = dVarArr.length;
            if (dVarArr != null && dVarArr.length > 0) {
                this.a.d++;
                a.a();
                a.a(this.a, str, this.a.a, this.a.b, dVarArr);
            }
            i4 = length4;
        }
        super.a(i, i2, i4, null);
    }

    public final void handleMessage(Message message) {
        int i = message.what;
        int i2 = message.arg1;
        if (this.a.m != null && this.a.m.isShowing()) {
            this.a.m.dismiss();
        }
        this.a.m = null;
        if (18 == i2 || 17 == i2 || 57 == i2 || 56 == i2) {
            this.a.b(false);
        }
        if (i == 0) {
            if (message.arg2 == 0 && this.a.d == 0) {
                Toast.makeText(this.a, "暂无数据！", 1).show();
                this.a.e.d(false);
            } else {
                this.a.e.d(message.arg2 >= 20);
            }
            switch (i2) {
                case 17:
                case 19:
                case 33:
                case 35:
                case 37:
                case 39:
                case 41:
                case 49:
                case 51:
                case 52:
                case 54:
                case 56:
                case 81:
                    this.a.f = -1;
                    this.a.e.a(false);
                    return;
                case 18:
                case 20:
                case 34:
                case 36:
                case 38:
                case 40:
                case 48:
                case 50:
                case 55:
                case 57:
                    this.a.e.b(false);
                    this.a.k = -1;
                    return;
                case 53:
                    this.a.e.b(false);
                    this.a.f = -1;
                    this.a.e.a(false);
                    this.a.k = -1;
                    return;
                default:
                    return;
            }
        } else {
            this.a.e.b(false);
            this.a.e.a(false);
            this.a.f = -1;
            this.a.k = -1;
            if (i2 == 52) {
                Toast.makeText(this.a, (String) message.obj, 0).show();
                return;
            }
            int a2 = l.a(i);
            if (a2 != 0) {
                Toast.makeText(this.a, a2, 1).show();
            }
        }
    }
}
