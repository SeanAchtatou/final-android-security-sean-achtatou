package com.vodone.caibo.activity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import com.vodone.caibo.R;

public class ReadingModelActivity extends BaseActivity implements View.OnClickListener {
    private RadioButton a;
    private RadioButton b;
    private RadioButton c;
    private RelativeLayout d;
    private RelativeLayout e;
    private RelativeLayout f;
    private RelativeLayout k;
    private CheckBox l;

    private void c(String str) {
        this.a.setChecked(false);
        this.c.setChecked(false);
        this.b.setChecked(false);
        if (str.equals("readmode_classical")) {
            this.b.setChecked(true);
        } else if (str.equals("readmode_img")) {
            this.a.setChecked(true);
        } else if (str.equals("readmode_txt")) {
            this.c.setChecked(true);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.vodone.caibo.activity.af.a(android.content.Context, java.lang.String, boolean):void
     arg types: [com.vodone.caibo.activity.ReadingModelActivity, java.lang.String, boolean]
     candidates:
      com.vodone.caibo.activity.af.a(android.content.Context, java.lang.String, int):void
      com.vodone.caibo.activity.af.a(android.content.Context, java.lang.String, java.lang.String):void
      com.vodone.caibo.activity.af.a(android.content.Context, java.lang.String, boolean):void */
    public void onClick(View view) {
        if (view.equals(this.d)) {
            c("readmode_img");
            af.a(this, "readmode", "readmode_img");
        }
        if (view.equals(this.e)) {
            c("readmode_classical");
            af.a(this, "readmode", "readmode_classical");
        }
        if (view.equals(this.f)) {
            c("readmode_txt");
            af.a(this, "readmode", "readmode_txt");
        }
        if (view.equals(this.k)) {
            this.l.setChecked(!this.l.isChecked());
            af.a((Context) this, "showpreivewweibo", this.l.isChecked());
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.readingmodel);
        a((byte) 0, (int) R.string.back, this.i);
        this.g.a.setBackgroundResource(R.drawable.title_left_button);
        b((byte) 2, -1, null);
        setTitle((int) R.string.reading_model);
        this.a = (RadioButton) findViewById(R.id.previewradiobtn);
        this.b = (RadioButton) findViewById(R.id.classicsradiobtn);
        this.c = (RadioButton) findViewById(R.id.textradiobtn);
        this.a.setClickable(false);
        this.b.setClickable(false);
        this.c.setClickable(false);
        this.l = (CheckBox) findViewById(R.id.preview_on_off_choice);
        this.l.setClickable(false);
        this.d = (RelativeLayout) findViewById(R.id.previewlayout);
        this.d.setOnClickListener(this);
        this.e = (RelativeLayout) findViewById(R.id.classicslayout);
        this.e.setOnClickListener(this);
        this.f = (RelativeLayout) findViewById(R.id.textlayout);
        this.f.setOnClickListener(this);
        this.k = (RelativeLayout) findViewById(R.id.show_preview_layout);
        this.k.setOnClickListener(this);
        this.l.setChecked(af.b(this, "showpreivewweibo"));
        String c2 = af.c(this, "readmode");
        if (c2 == null) {
            c2 = "readmode_img";
        }
        c(c2);
    }
}
