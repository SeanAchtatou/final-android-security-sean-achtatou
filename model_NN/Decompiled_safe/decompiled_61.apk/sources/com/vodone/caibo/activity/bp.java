package com.vodone.caibo.activity;

import android.content.DialogInterface;
import android.content.Intent;
import com.vodone.caibo.CaiboApp;
import com.vodone.caibo.b.l;

final class bp implements DialogInterface.OnClickListener {
    private /* synthetic */ String a;
    private /* synthetic */ AccountManageActivity b;

    bp(AccountManageActivity accountManageActivity, String str) {
        this.b = accountManageActivity;
        this.a = str;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        if (CaiboApp.a().b().b.equals(this.a)) {
            l.a(this.b).a(this.a);
            af.a(this.b, "lastAccout_loginname", (String) null);
            af.a(this.b, "lastAccount_nickname", (String) null);
            AccountManageActivity accountManageActivity = this.b;
            cn.a().d();
            accountManageActivity.startActivity(new Intent(accountManageActivity, LoginActivity.class));
            return;
        }
        l.a(this.b).a(this.a);
        j jVar = this.b.b;
        String str = this.a;
        if (jVar.a.contains(str)) {
            jVar.a.remove(str);
            jVar.notifyDataSetChanged();
        }
    }
}
