package com.vodone.caibo.activity;

import android.widget.RadioGroup;
import com.vodone.caibo.R;

final class cq implements RadioGroup.OnCheckedChangeListener {
    private /* synthetic */ SettingActivity a;

    cq(SettingActivity settingActivity) {
        this.a = settingActivity;
    }

    public final void onCheckedChanged(RadioGroup radioGroup, int i) {
        switch (i) {
            case R.id.high /*2131427630*/:
                af.a(this.a, "ImagedefauQualityStr", "high_image");
                return;
            case R.id.middle /*2131427631*/:
                af.a(this.a, "ImagedefauQualityStr", "middle_image");
                return;
            case R.id.low /*2131427632*/:
                af.a(this.a, "ImagedefauQualityStr", "loe_image");
                return;
            default:
                return;
        }
    }
}
