package com.vodone.caibo.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.widget.Toast;
import com.vodone.caibo.b.g;
import com.vodone.caibo.service.c;
import java.io.File;

final class cj implements DialogInterface.OnClickListener {
    private /* synthetic */ SettingActivity a;

    cj(SettingActivity settingActivity) {
        this.a = settingActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        short s;
        SettingActivity settingActivity = this.a;
        if (settingActivity.a != null) {
            settingActivity.a.dismiss();
            settingActivity.a = null;
        }
        g.a(settingActivity);
        ck ckVar = new ck(settingActivity);
        File d = g.d();
        if (d == null) {
            d = g.c(settingActivity);
        }
        if (d != null) {
            s = c.a().u(d.getPath(), ckVar);
        } else {
            s = -1;
        }
        if (s != -1) {
            settingActivity.a = ProgressDialog.show(settingActivity, "请稍后", "正在清空缓存");
            settingActivity.a.setCancelable(true);
            settingActivity.a.setOnCancelListener(new ao(s));
            return;
        }
        Toast.makeText(settingActivity, "缓存清空完毕", 1).show();
    }
}
