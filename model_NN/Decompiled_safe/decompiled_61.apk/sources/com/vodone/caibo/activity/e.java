package com.vodone.caibo.activity;

import android.os.Message;
import android.widget.Toast;
import com.vodone.caibo.CaiboApp;
import com.vodone.caibo.b.b;
import com.vodone.caibo.b.d;
import com.vodone.caibo.b.j;
import com.vodone.caibo.database.a;

final class e extends bb {
    private /* synthetic */ MessageGroup a;

    e(MessageGroup messageGroup) {
        this.a = messageGroup;
    }

    private static byte a(int i) {
        switch (i) {
            case 21:
            case 22:
                return 4;
            case 23:
            case 24:
                return 3;
            case 25:
            case 32:
                return 5;
            case 66:
            case 67:
                return 21;
            default:
                return -1;
        }
    }

    public final void a(int i, int i2, int i3, Object obj) {
        int i4;
        String str = CaiboApp.a().b().a;
        byte a2 = a(i2);
        int i5 = 0;
        if (i2 == 21 || i2 == 25 || i2 == 23 || i2 == 66) {
            a.a();
            a.a(this.a, str, a2, (String) null, (String) null);
            this.a.b = 1;
        }
        if (obj != null) {
            switch (i2) {
                case 21:
                case 22:
                    d[] dVarArr = (d[]) obj;
                    int length = dVarArr.length;
                    a.a();
                    a.a(this.a, str, a2, (String) null, dVarArr);
                    i4 = length;
                    break;
                case 23:
                case 24:
                    j[] jVarArr = (j[]) obj;
                    int length2 = jVarArr.length;
                    a.a();
                    a.a(this.a, str, a2, jVarArr);
                    i4 = length2;
                    break;
                case 25:
                case 32:
                    b[] bVarArr = (b[]) obj;
                    int length3 = bVarArr.length;
                    a.a();
                    a.a(this.a, str, a2, (String) null, bVarArr);
                    i4 = length3;
                    break;
                case 66:
                case 67:
                    j[] jVarArr2 = (j[]) obj;
                    i5 = jVarArr2.length;
                    a.a();
                    a.a(this.a, str, a2, jVarArr2);
                default:
                    i4 = i5;
                    break;
            }
            super.a(i, i2, i4, null);
        }
        i4 = i5;
        super.a(i, i2, i4, null);
    }

    public final void handleMessage(Message message) {
        int i = message.what;
        int i2 = message.arg1;
        int i3 = message.arg2;
        g gVar = (g) this.a.d.get(Byte.valueOf(a(i2)));
        if (i == 0) {
            if (i3 == 0 && this.a.b == 0) {
                Toast.makeText(this.a, "暂无数据！", 1).show();
                gVar.d(false);
            } else {
                gVar.d(message.arg2 >= 20);
            }
            switch (i2) {
                case 21:
                case 23:
                case 25:
                case 66:
                    gVar.a(false);
                    return;
                case 22:
                case 24:
                case 32:
                case 67:
                    gVar.b(false);
                    this.a.b++;
                    return;
                default:
                    return;
            }
        } else {
            gVar.a(false);
            gVar.b(false);
            int a2 = l.a(message.what);
            if (a2 != 0) {
                Toast.makeText(this.a, a2, 1).show();
            }
        }
    }
}
