package com.vodone.caibo.activity;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.vodone.caibo.R;
import java.util.List;

public final class a extends BaseAdapter {
    private LayoutInflater a;
    private List b;

    public a(Context context, List list) {
        this.a = LayoutInflater.from(context);
        this.b = list;
    }

    public final int getCount() {
        return this.b.size();
    }

    public final Object getItem(int i) {
        return this.b.get(i);
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        ae aeVar;
        View view2;
        if (view == null) {
            view2 = this.a.inflate((int) R.layout.location_row, (ViewGroup) null);
            ae aeVar2 = new ae(this);
            aeVar2.a = (TextView) view2.findViewById(R.id.text);
            view2.setTag(aeVar2);
            aeVar = aeVar2;
        } else {
            aeVar = (ae) view.getTag();
            view2 = view;
        }
        aeVar.a.setText(((com.vodone.caibo.a) this.b.get(i)).a());
        return view2;
    }
}
