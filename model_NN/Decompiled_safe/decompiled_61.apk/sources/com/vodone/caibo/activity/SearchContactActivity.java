package com.vodone.caibo.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.ListView;
import android.widget.TextView;
import com.vodone.caibo.R;
import com.vodone.caibo.database.a;
import com.vodone.caibo.service.c;

public class SearchContactActivity extends BaseActivity implements View.OnClickListener {
    public static ProgressDialog c;
    TextView a;
    TextView b;
    public bb d = new aj(this);
    /* access modifiers changed from: private */
    public AutoCompleteTextView e;
    /* access modifiers changed from: private */
    public ListView f;
    /* access modifiers changed from: private */
    public g k;
    /* access modifiers changed from: private */
    public cg l;
    /* access modifiers changed from: private */
    public String m;
    /* access modifiers changed from: private */
    public int n;
    private int o;
    private TextWatcher p = new ai(this);
    private dd q = new ah(this);

    public static Intent a(Context context, String str, int i) {
        Intent intent = new Intent(context, SearchContactActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("user_id", str);
        bundle.putInt("from_activity", i);
        intent.putExtras(bundle);
        return intent;
    }

    static /* synthetic */ int c(SearchContactActivity searchContactActivity) {
        int i = searchContactActivity.n + 1;
        searchContactActivity.n = i;
        return i;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i2 == -1 && i == 1) {
            Intent intent2 = this.o == 1 ? new Intent(this, SendblogActivity.class) : new Intent(this, SendLetterActivity.class);
            intent2.putExtra("friend_nickname_key", intent.getStringExtra("friend_nickname_key"));
            setResult(-1, intent2);
            finish();
        }
    }

    public void onClick(View view) {
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Bundle extras = getIntent().getExtras();
        this.m = extras.getString("user_id");
        this.o = extras.getInt("from_activity");
        setContentView((int) R.layout.searchfrequentcontact);
        a((byte) 0, (int) R.string.back, this.i);
        this.g.a.setBackgroundResource(R.drawable.title_left_button);
        b((byte) 2, -1, null);
        setTitle((int) R.string.commonconcat);
        this.e = (AutoCompleteTextView) findViewById(R.id.EditText_SearchFrequentContact);
        this.e.addTextChangedListener(this.p);
        a.a();
        a.b(this, this.m, 7);
        a.a();
        Cursor a2 = a.a(this, this.m, 7);
        this.f = (ListView) findViewById(R.id.ListView_SearchFrequentContact);
        this.a = (TextView) LayoutInflater.from(this).inflate((int) R.layout.topic_list, (ViewGroup) null);
        this.b = (TextView) LayoutInflater.from(this).inflate((int) R.layout.topic_list, (ViewGroup) null);
        this.b.setText((int) R.string.search_online);
        this.l = new cg(this, a2, (byte) 9);
        this.k = new g(this.f, this.l);
        this.k.c = this.q;
        short k2 = c.a().k(this.m, this.d);
        ProgressDialog show = ProgressDialog.show(this, "请稍后", "正在加载数据");
        c = show;
        show.setCancelable(true);
        c.setOnCancelListener(new ao(k2));
        this.k.a();
        getWindow().setSoftInputMode(18);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.k.b.getCursor().close();
        a.a();
        a.b(this, this.m, 7);
    }
}
