package com.vodone.caibo.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.vodone.caibo.R;
import com.vodone.caibo.b.a;
import com.vodone.caibo.service.c;

public class PerfectActivity extends BaseActivity implements View.OnClickListener {
    private Button a;
    private View b;
    private View c;
    private View d;
    /* access modifiers changed from: private */
    public TextView e;
    /* access modifiers changed from: private */
    public String f = "";
    private String k;
    private String l;
    private String m;
    /* access modifiers changed from: private */
    public String n = "";
    /* access modifiers changed from: private */
    public int o;
    /* access modifiers changed from: private */
    public byte p;
    /* access modifiers changed from: private */
    public int q;
    /* access modifiers changed from: private */
    public int r;
    /* access modifiers changed from: private */
    public a s;
    private bb t = new bi(this);

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i == 1) {
            c.a().i(this.m, this.k, this.t);
        }
    }

    public void onClick(View view) {
        if (view.equals(this.a)) {
            c.a().a(this.k, this.l, new ar(this, ProgressDialog.show(this, getString(R.string.waiting), getString(R.string.loding)), this.k, this.l, true));
        } else if (view.equals(this.b)) {
            startActivityForResult(EditInfoActivity.a(this, this.m, this.k, this.f, this.q, this.r, this.p, this.n), 1);
        } else if (view.equals(this.c)) {
            startActivity(new Intent(this, PlazaActivity.class));
        } else if (view.equals(this.d)) {
            startActivity(TimeLineActivity.b(this, "mrcb"));
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.welcome);
        a((byte) 0, (int) R.string.back, this.i);
        this.g.a.setBackgroundResource(R.drawable.title_left_button);
        b((byte) 0, R.string.finish, this);
        setTitle((int) R.string.welcome);
        com.vodone.caibo.database.a.a();
        com.vodone.caibo.b.c[] a2 = com.vodone.caibo.database.a.a(this);
        int length = a2.length;
        this.k = a2[length - 1].b;
        this.l = a2[length - 1].c;
        this.m = a2[length - 1].a;
        c.a().i(this.m, this.k, this.t);
        this.a = (Button) findViewById(R.id.titleBtnRight);
        this.a.setOnClickListener(this);
        this.e = (TextView) findViewById(R.id.perfectinfo2);
        this.b = findViewById(R.id.perfectinfos);
        this.b.setOnClickListener(this);
        this.c = findViewById(R.id.blogpzzas);
        this.c.setOnClickListener(this);
        this.d = findViewById(R.id.famousblog);
        this.d.setOnClickListener(this);
    }
}
