package com.vodone.caibo.activity;

import android.content.Intent;
import android.os.Message;
import android.widget.TextView;
import android.widget.Toast;
import com.vodone.caibo.R;

final class be extends bb {
    private /* synthetic */ BindActivity a;

    be(BindActivity bindActivity) {
        this.a = bindActivity;
    }

    public final void a(int i, int i2, int i3, Object obj) {
        super.a(i, i2, i3, obj);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.vodone.caibo.activity.BindActivity.a(com.vodone.caibo.activity.BindActivity, java.lang.Boolean):java.lang.Boolean
     arg types: [com.vodone.caibo.activity.BindActivity, int]
     candidates:
      com.vodone.caibo.activity.BindActivity.a(com.vodone.caibo.activity.BindActivity, android.widget.TextView):android.widget.TextView
      com.vodone.caibo.activity.BindActivity.a(com.vodone.caibo.activity.BindActivity, java.lang.Boolean):java.lang.Boolean */
    public final void handleMessage(Message message) {
        int i = message.arg1;
        int i2 = message.what;
        if (this.a.a != null && this.a.a.isShowing()) {
            this.a.a.dismiss();
            this.a.a = null;
        }
        if (i2 == 0) {
            switch (i) {
                case 3:
                    Toast.makeText(this.a, (int) R.string.mobilebind_succeed, 1).show();
                    if (af.a(this.a, "bindskip") == 1) {
                        this.a.startActivity(new Intent(this.a, BindAccountActivity.class));
                        return;
                    }
                    new Intent(this.a, MoreItemsActivity.class);
                    this.a.finish();
                    return;
                case 4:
                    Toast.makeText(this.a, (int) R.string.emailbind_succeed, 1).show();
                    int a2 = af.a(this.a, "bindskip");
                    if (a2 == 1) {
                        this.a.startActivity(new Intent(this.a, BindAccountActivity.class));
                        return;
                    } else if (a2 == 2) {
                        new Intent(this.a, MoreItemsActivity.class);
                        this.a.finish();
                        return;
                    } else {
                        return;
                    }
                case 5:
                case 6:
                default:
                    return;
                case 7:
                    Boolean unused = this.a.q = (Boolean) false;
                    this.a.b.setClickable(this.a.q.booleanValue());
                    this.a.b.setTextColor((int) R.color.yellow);
                    this.a.b.setText((int) R.string.codesend);
                    TextView unused2 = this.a.o = (TextView) this.a.findViewById(R.id.captchatext);
                    this.a.o.setVisibility(8);
                    Toast.makeText(this.a, (int) R.string.passcode_secceed, 1).show();
                    return;
            }
        } else if (i2 == -1) {
            String str = (String) message.obj;
            switch (i) {
                case 3:
                    Toast.makeText(this.a, str, 1).show();
                    return;
                case 4:
                    if (str.equals("邮箱不可用")) {
                        Toast.makeText(this.a, "此邮箱已绑定", 1).show();
                        return;
                    }
                    break;
                case 5:
                case 6:
                default:
                    return;
                case 7:
                    break;
            }
            Toast.makeText(this.a, str, 1).show();
            TextView unused3 = this.a.o = (TextView) this.a.findViewById(R.id.captchatext);
            this.a.o.setVisibility(0);
        } else if (i2 == 1281) {
            Toast.makeText(this.a, (int) R.string.toomore, 1).show();
        } else if (i2 == 1) {
            Toast.makeText(this.a, (int) R.string.toomore, 1).show();
        } else {
            int a3 = l.a(i2);
            if (a3 != 0) {
                Toast.makeText(this.a, a3, 1).show();
            }
        }
    }
}
