package com.vodone.caibo.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Message;
import com.vodone.caibo.R;

final class cl extends bb {
    private /* synthetic */ SettingActivity a;

    cl(SettingActivity settingActivity) {
        this.a = settingActivity;
    }

    public final void handleMessage(Message message) {
        if (message.what == 0) {
            if (this.a.c != null && this.a.c.isShowing()) {
                this.a.c.dismiss();
            }
            this.a.c = null;
            new AlertDialog.Builder(this.a).setTitle("提示").setMessage("您的账号已经激活推送服务").setNegativeButton((int) R.string.ok, (DialogInterface.OnClickListener) null).show();
        }
    }
}
