package com.vodone.caibo.activity;

import com.vodone.caibo.a;
import java.util.ArrayList;
import java.util.List;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

public final class dj extends DefaultHandler {
    private int a = 0;
    private boolean b = false;
    private boolean c = false;
    private boolean d = false;
    private boolean e = false;
    private List f;
    private a g;
    private String h;
    private StringBuffer i = new StringBuffer();

    public final List a() {
        return this.f;
    }

    public final void characters(char[] cArr, int i2, int i3) {
        if (this.b) {
            this.i.append(cArr, i2, i3);
        }
    }

    public final void endDocument() {
        super.endDocument();
    }

    public final void endElement(String str, String str2, String str3) {
        if (str2.equals("city")) {
            this.b = false;
            this.f.add(this.g);
        } else if (str2.equals("key")) {
            if (this.b) {
                this.g.a(this.i.toString().trim());
                this.h = this.i.toString().trim();
                this.i.setLength(0);
                this.c = false;
            }
        } else if (str2.equals("array")) {
            if (this.b) {
                this.d = false;
                this.i.setLength(0);
            }
        } else if (str2.equals("string") && this.b && this.d) {
            this.g.b(this.i.toString().trim());
            this.e = false;
        }
    }

    public final void startDocument() {
        this.f = new ArrayList();
    }

    public final void startElement(String str, String str2, String str3, Attributes attributes) {
        if (str2.equals("city")) {
            this.b = true;
            this.g = new a();
        } else if (str2.equals("key")) {
            if (this.b) {
                this.c = true;
            }
        } else if (str2.equals("array")) {
            if (this.b) {
                this.d = true;
            }
        } else if (str2.equals("string") && this.b && this.d) {
            this.e = true;
        }
    }
}
