package com.vodone.caibo.activity;

import android.os.Message;
import android.widget.Toast;
import com.vodone.caibo.b.a;
import com.vodone.caibo.service.c;

final class bz extends bb {
    private c a = c.a();
    private /* synthetic */ PersonalInformationActivity b;

    bz(PersonalInformationActivity personalInformationActivity) {
        this.b = personalInformationActivity;
    }

    public final void handleMessage(Message message) {
        int i = message.what;
        int i2 = message.arg1;
        if (this.b.J == 2) {
            if (this.b.a != null && this.b.a.isShowing()) {
                this.b.a.dismiss();
            }
            this.b.a = null;
            this.b.b(false);
        }
        if (i == 0) {
            switch (i2) {
                case 114:
                    this.b.c = (a) message.obj;
                    this.b.a(this.b.c);
                    return;
                case 116:
                    this.b.c = (a) message.obj;
                    this.b.a(this.b.c);
                    this.b.c();
                    return;
                case 135:
                    this.b.c();
                    if (this.b.c.o == 0 || this.b.c.o == 2) {
                        a aVar = this.b.c;
                        int i3 = aVar.o + 1;
                        aVar.o = i3;
                        this.b.a(i3);
                    }
                    Toast.makeText(this.b, "添加关注成功", 0).show();
                    return;
                case 136:
                    this.b.c();
                    if (this.b.c.o == 3 || this.b.c.o == 1) {
                        a aVar2 = this.b.c;
                        int i4 = aVar2.o - 1;
                        aVar2.o = i4;
                        this.b.a(i4);
                    }
                    Toast.makeText(this.b, "成功解除关注", 0).show();
                    return;
                case 137:
                    this.b.c();
                    if (this.b.c.p == 1 || this.b.c.p == 0) {
                        if (this.b.c.o == 3 || this.b.c.o == 1) {
                            this.b.c.o = 0;
                        }
                        this.b.c.p += 2;
                        this.b.a(this.b.c.p, this.b.c.o);
                    }
                    Toast.makeText(this.b, "已添加黑名单", 0).show();
                    return;
                case 144:
                    this.b.c();
                    if (this.b.c.p == 3 || this.b.c.p == 2) {
                        this.b.c.p -= 2;
                        this.b.a(this.b.c.p, this.b.c.o);
                    }
                    Toast.makeText(this.b, "解除成功", 0).show();
                    return;
                case 289:
                    a aVar3 = this.b.c;
                    int i5 = aVar3.h - 1;
                    aVar3.h = i5;
                    System.out.println("blognum--->  " + i5);
                    return;
                default:
                    return;
            }
        } else {
            if (114 == i2) {
                this.b.b(false);
                Toast.makeText(this.b, "获取用户数据失败", 0).show();
            } else {
                int a2 = l.a(message.what);
                if (a2 != 0) {
                    Toast.makeText(this.b, a2, 1).show();
                }
            }
            this.b.finish();
        }
    }
}
