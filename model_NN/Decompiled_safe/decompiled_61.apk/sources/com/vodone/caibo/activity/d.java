package com.vodone.caibo.activity;

import android.content.DialogInterface;

final class d implements DialogInterface.OnClickListener {
    private /* synthetic */ MessageGroup a;

    d(MessageGroup messageGroup) {
        this.a = messageGroup;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        if (i == 0) {
            this.a.startActivity(SendblogActivity.a(this.a, this.a.e.h));
        } else if (i == 1) {
            this.a.startActivity(BlogDetailsActivity.a(this.a, this.a.e.b, 2, 5));
        }
    }
}
