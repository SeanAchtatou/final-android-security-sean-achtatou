package com.vodone.caibo.activity;

import android.content.Intent;
import android.os.Message;
import android.widget.Toast;
import com.vodone.caibo.R;

final class at extends bb {
    private /* synthetic */ EditInfoActivity a;

    at(EditInfoActivity editInfoActivity) {
        this.a = editInfoActivity;
    }

    public final void handleMessage(Message message) {
        super.handleMessage(message);
        if (this.a.a != null) {
            this.a.a.dismiss();
            this.a.a = null;
        }
        int i = message.what;
        if (i == 0) {
            Toast.makeText(this.a, (int) R.string.save_succ, 0).show();
            Intent intent = new Intent();
            intent.putExtra("userid", this.a.f);
            this.a.setResult(-1, intent);
            this.a.finish();
            return;
        }
        int a2 = l.a(i);
        if (a2 != 0) {
            Toast.makeText(this.a, a2, 0).show();
        }
    }
}
