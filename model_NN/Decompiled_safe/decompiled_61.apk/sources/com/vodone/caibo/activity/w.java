package com.vodone.caibo.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;

final class w implements AdapterView.OnItemClickListener {
    private /* synthetic */ PlazaActivity a;

    w(PlazaActivity plazaActivity) {
        this.a = plazaActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.vodone.caibo.activity.TimeLineActivity.a(android.content.Context, boolean):android.content.Intent
     arg types: [com.vodone.caibo.activity.PlazaActivity, int]
     candidates:
      com.vodone.caibo.activity.TimeLineActivity.a(android.content.Context, java.lang.String):android.content.Intent
      com.vodone.caibo.activity.TimeLineActivity.a(android.content.Context, boolean):android.content.Intent */
    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        Class<TimeLineActivity> cls = TimeLineActivity.class;
        PlazaActivity plazaActivity = this.a;
        switch (i) {
            case 0:
                plazaActivity.startActivity(TimeLineActivity.a((Context) plazaActivity, false));
                return;
            case 1:
                Class<TimeLineActivity> cls2 = TimeLineActivity.class;
                Intent intent = new Intent(plazaActivity, cls);
                Bundle bundle = new Bundle();
                bundle.putByte("timeline", (byte) 16);
                intent.putExtras(bundle);
                plazaActivity.startActivity(intent);
                return;
            case 2:
                Class<TimeLineActivity> cls3 = TimeLineActivity.class;
                Intent intent2 = new Intent(plazaActivity, cls);
                Bundle bundle2 = new Bundle();
                bundle2.putByte("timeline", (byte) 17);
                intent2.putExtras(bundle2);
                plazaActivity.startActivity(intent2);
                return;
            case 3:
                plazaActivity.startActivity(TimeLineActivity.b(plazaActivity));
                return;
            case 4:
                plazaActivity.startActivity(TimeLineActivity.b(plazaActivity, "zxdt"));
                return;
            case 5:
                plazaActivity.startActivity(TimeLineActivity.b(plazaActivity, "zxkj"));
                return;
            case 6:
                plazaActivity.startActivity(TimeLineActivity.a(plazaActivity));
                return;
            case 7:
                plazaActivity.startActivity(TimeLineActivity.b(plazaActivity, "gfzz"));
                return;
            case 8:
                plazaActivity.startActivity(TimeLineActivity.b(plazaActivity, "mrcb"));
                return;
            default:
                return;
        }
    }
}
