package com.vodone.caibo.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import com.vodone.caibo.R;

public class InformationEditActivity extends BaseActivity implements View.OnClickListener {
    private String a;
    private EditText b;

    public void onClick(View view) {
        if (view.equals(this.g.a)) {
            finish();
        } else if (view.equals(this.g.d)) {
            String obj = this.b.getText().toString();
            Intent intent = new Intent();
            intent.putExtra("introduce", obj);
            setResult(-1, intent);
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.introduce);
        this.a = getIntent().getExtras().getString("introduce");
        a((byte) 0, (int) R.string.cancel, this);
        b((byte) 0, R.string.save, this);
        setTitle((int) R.string.editinfo);
        this.b = (EditText) findViewById(R.id.introEdits);
        if (this.a != null) {
            this.b.setText(this.a);
            this.b.selectAll();
        }
    }
}
