package com.vodone.caibo.activity;

import android.text.Editable;
import android.text.TextWatcher;

final class u implements TextWatcher {
    private /* synthetic */ RearchActivity a;

    u(RearchActivity rearchActivity) {
        this.a = rearchActivity;
    }

    public final void afterTextChanged(Editable editable) {
    }

    public final void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public final void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        if (charSequence.length() == 0) {
            this.a.c.getFilter().filter("");
            if (this.a.b.getFooterViewsCount() > 0) {
                this.a.b.removeFooterView(this.a.d);
                this.a.b.removeFooterView(this.a.e);
                return;
            }
            return;
        }
        this.a.c.getFilter().filter("#" + ((Object) charSequence));
        if (this.a.b.getFooterViewsCount() == 0) {
            this.a.b.addFooterView(this.a.d);
            this.a.b.addFooterView(this.a.e);
        }
        this.a.d.setText("#" + ((Object) charSequence) + "#");
    }
}
