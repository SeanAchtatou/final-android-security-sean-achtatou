package com.vodone.caibo.activity;

import android.app.AlertDialog;
import android.database.Cursor;
import android.view.View;
import com.vodone.caibo.R;
import com.vodone.caibo.b.b;
import com.vodone.caibo.b.d;
import com.vodone.caibo.b.j;
import com.vodone.caibo.database.a;
import com.vodone.caibo.service.c;

final class o implements dd {
    private byte a;
    private /* synthetic */ MessageGroup b;

    public o(MessageGroup messageGroup, byte b2) {
        this.b = messageGroup;
        this.a = b2;
    }

    public final void a() {
        this.b.b(this.a);
    }

    public final void a(g gVar, View view, int i) {
        if (3 == this.a) {
            MessageGroup.a(this.b, gVar, i);
        } else if (4 == this.a) {
            Cursor a2 = gVar.a(i - 1);
            a.a();
            this.b.startActivity(BlogDetailsActivity.a(this.b, a.a(a2, (d) null).a, 2, this.b.a));
        } else if (5 == this.a) {
            MessageGroup messageGroup = this.b;
            Cursor a3 = gVar.a(i - 1);
            a.a();
            messageGroup.e = a.a(a3, (b) null);
            new AlertDialog.Builder(messageGroup).setTitle("请选择").setItems((int) R.array.comment_dialog, new d(messageGroup)).show();
        } else if (21 == this.a) {
            Cursor a4 = gVar.a(i - 1);
            a.a();
            this.b.startActivity(BlogDetailsActivity.a(this.b, a.a(a4, (j) null).a, 4, this.b.a));
        }
    }

    public final void b() {
        MessageGroup messageGroup = this.b;
        byte b2 = this.a;
        ((g) messageGroup.d.get(Byte.valueOf(b2))).b(true);
        c a2 = c.a();
        switch (b2) {
            case 3:
                a2.d(messageGroup.b + 1, messageGroup.c, messageGroup.f);
                return;
            case 4:
                a2.b(messageGroup.b + 1, messageGroup.c, messageGroup.f);
                return;
            case 5:
                a2.c(messageGroup.b + 1, messageGroup.c, messageGroup.f);
                return;
            case 21:
                a2.a(messageGroup.b + 1, messageGroup.f);
                return;
            default:
                return;
        }
    }
}
