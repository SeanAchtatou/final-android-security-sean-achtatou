package com.vodone.caibo.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import com.vodone.caibo.CaiboApp;
import com.vodone.caibo.R;
import com.windo.widget.WithNewIconButton;
import java.util.ArrayList;
import java.util.HashMap;

public class PlazaActivity extends BaseActivity implements View.OnClickListener {
    private Integer[] a = null;
    private String[] b = null;
    private GridView c;
    private LinearLayout d;
    /* access modifiers changed from: private */
    public WithNewIconButton[] e;

    public void onClick(View view) {
        if (view.equals(this.d)) {
            startActivity(new Intent(this, SearchEditActivity.class));
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.blogsquare);
        a((byte) 2, -1, (View.OnClickListener) null);
        b((byte) 2, -1, null);
        setTitle((int) R.string.main_plaza);
        this.c = (GridView) findViewById(R.id.gridview);
        this.d = (LinearLayout) findViewById(R.id.LinearLayout_search);
        this.d.setOnClickListener(this);
        ArrayList arrayList = new ArrayList();
        this.a = new Integer[]{Integer.valueOf((int) R.drawable.square_hottopic), Integer.valueOf((int) R.drawable.square_hotretweet), Integer.valueOf((int) R.drawable.square_hotcomment), Integer.valueOf((int) R.drawable.square_freelook), Integer.valueOf((int) R.drawable.square_newtrends), Integer.valueOf((int) R.drawable.square_newlottery), Integer.valueOf((int) R.drawable.square_specialisttalk), Integer.valueOf((int) R.drawable.square_officialgroup), Integer.valueOf((int) R.drawable.square_celebrityblog)};
        this.b = new String[]{getString(R.string.square_hottopic), getString(R.string.square_hotretweet), getString(R.string.square_hotcomment), getString(R.string.square_freelook), getString(R.string.square_newtrends), getString(R.string.square_newlottery), getString(R.string.square_specialisttalk), getString(R.string.f0square_officialgroup), getString(R.string.square_celebrityblog)};
        for (int i = 0; i <= 8; i++) {
            HashMap hashMap = new HashMap();
            hashMap.put("ItemImage", this.a[i]);
            hashMap.put("ItemText", this.b[i]);
            arrayList.add(hashMap);
        }
        this.e = new WithNewIconButton[9];
        this.c.setAdapter((ListAdapter) new v(this, this, arrayList, new String[]{"ItemImage", "ItemText"}, new int[]{R.id.ItemImage, R.id.ItemText}));
        this.c.setOnItemClickListener(new w(this));
    }

    public void onDestroy() {
        int i;
        super.onDestroy();
        if (this.e != null) {
            CaiboApp a2 = CaiboApp.a();
            for (int i2 = 0; i2 < this.e.length; i2++) {
                if (!(this.e[i2] == null || this.e[i2].getTag() == null)) {
                    switch (i2) {
                        case 4:
                            i = 0;
                            break;
                        case 5:
                            i = 1;
                            break;
                        case 6:
                            i = 2;
                            break;
                        case 7:
                            i = 3;
                            break;
                        case 8:
                            i = 4;
                            break;
                        default:
                            i = -1;
                            break;
                    }
                    if (i != -1) {
                        a2.b(i, (Handler) this.e[i2].getTag());
                    }
                }
            }
        }
    }
}
