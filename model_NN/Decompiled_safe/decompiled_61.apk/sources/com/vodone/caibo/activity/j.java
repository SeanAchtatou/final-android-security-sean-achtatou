package com.vodone.caibo.activity;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.vodone.caibo.R;
import java.util.ArrayList;

public final class j extends BaseAdapter {
    ArrayList a;
    private boolean b;
    private Context c;
    private LayoutInflater d;
    private /* synthetic */ AccountManageActivity e;

    public j(AccountManageActivity accountManageActivity, Context context, ArrayList arrayList) {
        this.e = accountManageActivity;
        this.a = arrayList;
        this.c = context;
        this.d = LayoutInflater.from(context);
    }

    public final void a(boolean z) {
        if (this.b != z) {
            this.b = z;
            notifyDataSetChanged();
        }
    }

    public final int getCount() {
        if (this.a == null) {
            return 0;
        }
        return this.a.size();
    }

    public final Object getItem(int i) {
        if (this.a == null || i < 0 || i >= this.a.size()) {
            return null;
        }
        return this.a.get(i);
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        View view2;
        db dbVar;
        if (view != null) {
            dbVar = (db) view.getTag();
            view2 = view;
        } else {
            View inflate = this.d.inflate((int) R.layout.accountmanagelist, (ViewGroup) null);
            db dbVar2 = new db(this);
            dbVar2.a = (RelativeLayout) inflate.findViewById(R.id.account_layout);
            dbVar2.b = (TextView) inflate.findViewById(R.id.account_user_text);
            dbVar2.c = (Button) inflate.findViewById(R.id.accmgrdelete);
            dbVar2.d = (ImageView) inflate.findViewById(R.id.account_triangle);
            inflate.setTag(dbVar2);
            db dbVar3 = dbVar2;
            view2 = inflate;
            dbVar = dbVar3;
        }
        if (getCount() == 1) {
            dbVar.a.setBackgroundResource(R.drawable.round_button_bg);
        } else if (i == 0) {
            dbVar.a.setBackgroundResource(R.drawable.list_above_bg);
        } else if (i == getCount() - 1) {
            dbVar.a.setBackgroundResource(R.drawable.list_below_bg);
        } else {
            dbVar.a.setBackgroundResource(R.drawable.list_middle_bg);
        }
        String str = (String) getItem(i);
        dbVar.b.setText(str);
        if (this.b) {
            dbVar.d.setVisibility(8);
            dbVar.c.setVisibility(0);
            this.e.d.setText((int) R.string.finish);
            this.e.a = false;
        } else {
            dbVar.d.setVisibility(0);
            dbVar.c.setVisibility(8);
            this.e.d.setText((int) R.string.edit);
            this.e.a = true;
        }
        dbVar.c.setTag(str);
        dbVar.c.setOnClickListener(this.e);
        return view2;
    }
}
