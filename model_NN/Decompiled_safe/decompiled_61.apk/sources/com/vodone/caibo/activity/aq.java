package com.vodone.caibo.activity;

import android.content.DialogInterface;
import android.content.Intent;

final class aq implements DialogInterface.OnClickListener {
    private /* synthetic */ SetLocationActivity a;

    aq(SetLocationActivity setLocationActivity) {
        this.a = setLocationActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        SetLocationActivity setLocationActivity = this.a;
        int a2 = this.a.b;
        switch (a2) {
            case 1:
            case 2:
            case 3:
            case 4:
                break;
            case 5:
                a2 = i + 5;
                break;
            case 6:
                a2 = i + 16;
                break;
            case 7:
                a2 = i + 27;
                break;
            case 8:
                a2 = i + 50;
                break;
            case 9:
                a2 = i + 64;
                break;
            case 10:
                a2 = i + 73;
                break;
            case 11:
                a2 = i + 86;
                break;
            case 12:
                a2 = i + 99;
                break;
            case 13:
                a2 = i + 110;
                break;
            case 14:
                a2 = i + 127;
                break;
            case 15:
                a2 = i + 136;
                break;
            case 16:
                a2 = i + 147;
                break;
            case 17:
                a2 = i + 164;
                break;
            case 18:
                a2 = i + 182;
                break;
            case 19:
                a2 = i + 199;
                break;
            case 20:
                a2 = i + 213;
                break;
            case 21:
                a2 = i + 234;
                break;
            case 22:
                a2 = i + 248;
                break;
            case 23:
                a2 = i + 269;
                break;
            case 24:
                a2 = i + 278;
                break;
            case 25:
                a2 = i + 296;
                break;
            case 26:
                a2 = i + 312;
                break;
            case 27:
                a2 = i + 320;
                break;
            case 28:
                a2 = i + 330;
                break;
            case 29:
                a2 = i + 344;
                break;
            case 30:
                a2 = i + 351;
                break;
            case 31:
                a2 = i + 356;
                break;
            case 32:
                a2 = i + 378;
                break;
            case 33:
                a2 = i + 390;
                break;
            case 34:
                a2 = i + 391;
                break;
            default:
                a2 = 0;
                break;
        }
        int unused = setLocationActivity.c = a2;
        Intent intent = new Intent();
        intent.putExtra("province", this.a.b);
        intent.putExtra("city", this.a.c);
        this.a.setResult(-1, intent);
        this.a.finish();
    }
}
