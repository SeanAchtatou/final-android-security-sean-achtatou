package com.vodone.caibo.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.vodone.caibo.R;
import com.vodone.caibo.service.c;

public class FindPasswordActivity extends BaseActivity implements View.OnClickListener {
    private Button a;
    private EditText b;
    /* access modifiers changed from: private */
    public String c;
    private bb d = new di(this);

    public void onClick(View view) {
        boolean z;
        if (view.equals(this.a)) {
            this.c = this.b.getText().toString();
            if (this.c.length() == 0 || this.c == null) {
                b((int) R.string.nullusername);
                z = false;
            } else {
                z = true;
            }
            if (z) {
                c.a().a(this.c, this.d);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.findpass1);
        a((byte) 0, (int) R.string.back, this.i);
        this.g.a.setBackgroundResource(R.drawable.title_left_button);
        b((byte) 2, -1, null);
        setTitle((int) R.string.findpasswd);
        this.a = (Button) findViewById(R.id.button1);
        this.a.setOnClickListener(this);
        this.b = (EditText) findViewById(R.id.editText1);
        this.b.setOnClickListener(this);
    }
}
