package com.vodone.caibo.activity;

import android.app.AlertDialog;
import android.os.Message;
import android.widget.Toast;
import com.vodone.caibo.b.j;
import com.vodone.caibo.database.a;

final class cy extends bb {
    private /* synthetic */ SendLetterActivity a;

    cy(SendLetterActivity sendLetterActivity) {
        this.a = sendLetterActivity;
    }

    public final void a(int i, int i2, int i3, Object obj) {
        int i4;
        if (obj != null) {
            switch (i2) {
                case 64:
                    j[] jVarArr = (j[]) obj;
                    new j();
                    int length = jVarArr.length;
                    for (int i5 = 0; i5 < length / 2; i5++) {
                        j jVar = jVarArr[i5];
                        jVarArr[i5] = jVarArr[(length - 1) - i5];
                        jVarArr[(length - 1) - i5] = jVar;
                    }
                    i4 = jVarArr.length;
                    a.a();
                    a.a(this.a, this.a.f, (byte) 23, jVarArr);
                    break;
            }
            super.a(i, i2, i4, null);
        }
        i4 = 0;
        super.a(i, i2, i4, null);
    }

    public final void handleMessage(Message message) {
        int i = message.what;
        int i2 = message.arg1;
        if (i == 0) {
            if (2 == this.a.l) {
                this.a.a.d(false);
            }
            switch (i2) {
                case 64:
                    this.a.b(false);
                    return;
                case 65:
                    Toast.makeText(this.a, "以全部删除与该好友的短信", 0).show();
                    this.a.finish();
                    return;
                case 99:
                    if (this.a.x != null && this.a.x.isShowing()) {
                        this.a.x.dismiss();
                    }
                    AlertDialog unused = this.a.x = null;
                    if (this.a.l == 1) {
                        Toast.makeText(this.a, "私信以成功发送！", 0).show();
                        this.a.finish();
                        return;
                    } else if (this.a.l == 2) {
                        this.a.w.setText((CharSequence) null);
                        Toast.makeText(this.a, "私信以成功发送！", 0).show();
                        this.a.a.d(false);
                        return;
                    } else {
                        return;
                    }
                default:
                    return;
            }
        } else {
            int a2 = l.a(i);
            if (a2 != 0) {
                Toast.makeText(this.a, a2, 1).show();
            }
        }
    }
}
