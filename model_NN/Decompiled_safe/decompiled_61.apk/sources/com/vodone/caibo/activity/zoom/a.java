package com.vodone.caibo.activity.zoom;

import android.view.MotionEvent;
import android.view.View;

public final class a implements View.OnTouchListener {
    private b a = b.ZOOM;
    private float b;
    private float c;
    private d d;
    private float e;
    private float f;

    public final void a(b bVar) {
        this.a = bVar;
    }

    public final void a(d dVar) {
        this.d = dVar;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        float x = motionEvent.getX();
        float y = motionEvent.getY();
        switch (action) {
            case 1:
            default:
                return true;
            case 0:
                this.e = x;
                this.f = y;
                this.b = x;
                this.c = y;
                break;
            case 2:
                break;
        }
        float width = (x - this.b) / ((float) view.getWidth());
        float height = (y - this.c) / ((float) view.getHeight());
        if (this.a == b.ZOOM) {
            this.d.a((float) Math.pow(20.0d, (double) (-height)), this.e / ((float) view.getWidth()), this.f / ((float) view.getHeight()));
        } else {
            this.d.a(-width, -height);
        }
        this.b = x;
        this.c = y;
        return true;
    }
}
