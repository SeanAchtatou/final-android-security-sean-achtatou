package com.vodone.caibo.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.vodone.caibo.R;

public class SettingActivity extends BaseActivity implements View.OnClickListener {
    private CheckBox A;
    private CheckBox B;
    private TextView C;
    private TextView D;
    private Button E;
    private Button F;
    private RadioGroup.OnCheckedChangeListener G = new cq(this);
    ProgressDialog a;
    short b;
    ProgressDialog c;
    public bb d = new cl(this);
    private RadioGroup e;
    private RadioButton f;
    private RadioButton k;
    private RadioButton l;
    private RelativeLayout m;
    private RelativeLayout n;
    private RelativeLayout o;
    private RelativeLayout p;
    private RelativeLayout q;
    private CheckBox r;
    private RelativeLayout s;
    private RelativeLayout t;
    private RelativeLayout u;
    private RelativeLayout v;
    private RelativeLayout w;
    private CheckBox x;
    private CheckBox y;
    private CheckBox z;

    /* access modifiers changed from: private */
    public static String c(int i) {
        String str = "" + i;
        return str.length() == 1 ? "0" + str : str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.vodone.caibo.activity.af.a(android.content.Context, java.lang.String, boolean):void
     arg types: [com.vodone.caibo.activity.SettingActivity, java.lang.String, boolean]
     candidates:
      com.vodone.caibo.activity.af.a(android.content.Context, java.lang.String, int):void
      com.vodone.caibo.activity.af.a(android.content.Context, java.lang.String, java.lang.String):void
      com.vodone.caibo.activity.af.a(android.content.Context, java.lang.String, boolean):void */
    public void onClick(View view) {
        if (view.equals(this.m)) {
            this.r.setChecked(!this.r.isChecked());
            af.a((Context) this, "WarningTone", this.r.isChecked());
        } else if (view.equals(this.n)) {
            View inflate = ((LayoutInflater) getSystemService("layout_inflater")).inflate((int) R.layout.send_notice_setting, (ViewGroup) null);
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle((int) R.string.send_setting);
            builder.setView(inflate);
            AlertDialog create = builder.create();
            create.show();
            this.x = (CheckBox) inflate.findViewById(R.id.HomeCheckBox);
            this.s = (RelativeLayout) inflate.findViewById(R.id.SetHomeRelativeLayout);
            this.s.setOnClickListener(this);
            this.y = (CheckBox) inflate.findViewById(R.id.CommentCheckBox);
            this.t = (RelativeLayout) inflate.findViewById(R.id.SetCommentRelativeLayout);
            this.t.setOnClickListener(this);
            this.z = (CheckBox) inflate.findViewById(R.id.AboutMeCheckBox);
            this.u = (RelativeLayout) inflate.findViewById(R.id.SetAboutMeRelativeLayout);
            this.u.setOnClickListener(this);
            this.A = (CheckBox) inflate.findViewById(R.id.PrivateLetterCheckBox);
            this.v = (RelativeLayout) inflate.findViewById(R.id.PrivateLetterRelativeLayout);
            this.v.setOnClickListener(this);
            this.B = (CheckBox) inflate.findViewById(R.id.NewFansCheckBox);
            this.w = (RelativeLayout) inflate.findViewById(R.id.NewFansRelativeLayout);
            this.w.setOnClickListener(this);
            this.x.setChecked(af.b(this, "Home"));
            this.y.setChecked(af.b(this, "Comment"));
            this.z.setChecked(af.b(this, "AboutMe"));
            this.A.setChecked(af.b(this, "PrivateLetter"));
            this.B.setChecked(af.b(this, "NewFans"));
            this.C = (TextView) inflate.findViewById(R.id.startTime);
            this.D = (TextView) inflate.findViewById(R.id.endTime);
            String c2 = af.c(this, "StartTime");
            String c3 = af.c(this, "EndTime");
            if (c2 == null) {
                c2 = c(9) + ":" + c(0);
            }
            if (c3 == null) {
                c3 = c(21) + ":" + c(0);
            }
            this.C.setText(c2);
            this.C.setOnClickListener(new ca(this, this.C, c2, 0));
            this.D.setText(c3);
            this.D.setOnClickListener(new ca(this, this.D, c3, 1));
            this.E = (Button) inflate.findViewById(R.id.checkButton);
            this.E.setOnClickListener(new cm(this));
            this.F = (Button) inflate.findViewById(R.id.backButton);
            this.F.setOnClickListener(new co(this, create));
        } else if (view.equals(this.o)) {
            int a2 = af.a(this, "mMessageAlert");
            if (a2 == -1) {
                af.a(this, "mMessageAlert", 2);
                a2 = 2;
            }
            new AlertDialog.Builder(this).setSingleChoiceItems((int) R.array.message_setting_chioce_item, a2, new ci(this)).setNegativeButton((int) R.string.cancel, (DialogInterface.OnClickListener) null).show();
        } else if (view.equals(this.p)) {
            new AlertDialog.Builder(this).setMessage((int) R.string.clean_image_context).setPositiveButton((int) R.string.ok, new cj(this)).setNegativeButton((int) R.string.cancel, (DialogInterface.OnClickListener) null).show();
        } else if (view.equals(this.q)) {
            new AlertDialog.Builder(this).setMessage((int) R.string.clean_search_history_context).setPositiveButton((int) R.string.ok, new ch(this)).setNegativeButton((int) R.string.cancel, (DialogInterface.OnClickListener) null).show();
        } else if (view.equals(this.t)) {
            this.y.setChecked(!this.y.isChecked());
            af.a((Context) this, "Comment", this.y.isChecked());
        } else if (view.equals(this.u)) {
            this.z.setChecked(!this.z.isChecked());
            af.a((Context) this, "AboutMe", this.z.isChecked());
        } else if (view.equals(this.v)) {
            this.A.setChecked(!this.A.isChecked());
            af.a((Context) this, "PrivateLetter", this.A.isChecked());
        } else if (view.equals(this.w)) {
            this.B.setChecked(!this.B.isChecked());
            af.a((Context) this, "NewFans", this.B.isChecked());
        } else if (view.equals(this.s)) {
            this.x.setChecked(!this.x.isChecked());
            af.a((Context) this, "Home", this.x.isChecked());
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.setting);
        a((byte) 0, (int) R.string.back, this.i);
        this.g.a.setBackgroundResource(R.drawable.title_left_button);
        b((byte) 2, -1, this);
        setTitle((int) R.string.set_title);
        this.e = (RadioGroup) findViewById(R.id.qualityRadioGroup);
        this.e.setOnCheckedChangeListener(this.G);
        this.f = (RadioButton) findViewById(R.id.high);
        this.k = (RadioButton) findViewById(R.id.middle);
        this.l = (RadioButton) findViewById(R.id.low);
        this.m = (RelativeLayout) findViewById(R.id.warningToneRelativeLayout);
        this.r = (CheckBox) findViewById(R.id.tone_on_off_choice);
        this.n = (RelativeLayout) findViewById(R.id.SendNoticRelativeLayout);
        this.o = (RelativeLayout) findViewById(R.id.MessageAlertRelativeLayout);
        this.p = (RelativeLayout) findViewById(R.id.ClearImageRelativeLayout);
        this.q = (RelativeLayout) findViewById(R.id.CleanSearchHistoryRelativeLayout);
        this.m.setOnClickListener(this);
        this.n.setOnClickListener(this);
        this.o.setOnClickListener(this);
        this.p.setOnClickListener(this);
        this.q.setOnClickListener(this);
        String c2 = af.c(this, "ImagedefauQualityStr");
        if (c2 == null || c2.equals("high_image")) {
            this.f.setChecked(true);
        } else if (c2.equals("middle_image")) {
            this.k.setChecked(true);
        } else if (c2.equals("loe_image")) {
            this.l.setChecked(true);
        }
        this.r.setChecked(af.b(this, "WarningTone"));
    }
}
