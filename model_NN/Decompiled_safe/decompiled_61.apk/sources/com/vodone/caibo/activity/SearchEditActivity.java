package com.vodone.caibo.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import com.vodone.caibo.CaiboApp;
import com.vodone.caibo.R;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;
import java.util.Vector;

public class SearchEditActivity extends BaseActivity implements View.OnClickListener {
    private Button a;
    private Button b;
    /* access modifiers changed from: private */
    public AutoCompleteTextView c;
    private ListView d;
    private SimpleAdapter e;
    private ArrayList f;

    private static Vector a(String str, String str2) {
        StringTokenizer stringTokenizer = new StringTokenizer(str, str2);
        Vector vector = new Vector();
        while (stringTokenizer.hasMoreElements()) {
            vector.addElement((String) stringTokenizer.nextElement());
        }
        return vector;
    }

    private static void a(SimpleAdapter simpleAdapter, ArrayList arrayList, Vector vector) {
        arrayList.clear();
        for (int i = 0; i < vector.size(); i++) {
            HashMap hashMap = new HashMap();
            hashMap.put("content", vector.elementAt(i));
            arrayList.add(hashMap);
        }
        simpleAdapter.notifyDataSetChanged();
    }

    public void onClick(View view) {
        Vector vector;
        if (view.equals(this.b) || view.equals(this.a)) {
            String trim = this.c.getText().toString().trim();
            String c2 = af.c(this, "search_history");
            if (c2 != null) {
                Vector a2 = a(c2, "!#@");
                if (a2.contains(trim)) {
                    a2.remove(trim);
                } else if (a2.size() > 10) {
                    a2.remove(a2.size() - 1);
                }
                a2.insertElementAt(trim, 0);
                StringBuffer stringBuffer = new StringBuffer();
                for (int i = 0; i < a2.size(); i++) {
                    stringBuffer.append((String) a2.elementAt(i));
                    if (i < a2.size() - 1) {
                        stringBuffer.append("!#@");
                    }
                }
                af.a(this, "search_history", stringBuffer.toString());
                vector = a2;
            } else {
                af.a(this, "search_history", trim);
                vector = new Vector();
                vector.add(trim);
            }
            a(this.e, this.f, vector);
            String str = CaiboApp.a().b().a;
            if (view.equals(this.b)) {
                Intent intent = new Intent(this, TimeLineActivity.class);
                Bundle bundle = new Bundle();
                bundle.putByte("timeline", (byte) 24);
                bundle.putString("keywords", trim);
                intent.putExtras(bundle);
                startActivity(intent);
            } else if (view.equals(this.a)) {
                startActivity(FriendActivity.a(this, trim, str, false));
            }
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.editsearch);
        a((byte) 0, (int) R.string.back, this.i);
        this.g.a.setBackgroundResource(R.drawable.title_left_button);
        b((byte) 2, -1, null);
        setTitle((int) R.string.square_search);
        this.a = (Button) findViewById(R.id.searchUser);
        this.a.setOnClickListener(this);
        this.b = (Button) findViewById(R.id.searchBlog);
        this.b.setOnClickListener(this);
        this.c = (AutoCompleteTextView) findViewById(R.id.EditText_searchedit);
        this.d = (ListView) findViewById(R.id.ListViewsearch_result);
        this.f = new ArrayList();
        this.e = new SimpleAdapter(this, this.f, R.layout.search_item, new String[]{"content"}, new int[]{R.id.ItemContent});
        this.d.setAdapter((ListAdapter) this.e);
        String c2 = af.c(this, "search_history");
        if (c2 != null) {
            a(this.e, this.f, a(c2, "!#@"));
        }
        this.d.setOnItemClickListener(new h(this));
    }
}
