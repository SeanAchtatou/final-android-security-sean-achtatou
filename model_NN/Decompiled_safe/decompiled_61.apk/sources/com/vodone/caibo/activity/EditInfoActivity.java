package com.vodone.caibo.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.ContextMenu;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.vodone.caibo.R;
import com.vodone.caibo.a;
import com.vodone.caibo.b.e;
import com.vodone.caibo.b.g;
import com.vodone.caibo.service.c;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

public class EditInfoActivity extends BaseActivity implements View.OnClickListener {
    /* access modifiers changed from: private */
    public RadioButton A;
    /* access modifiers changed from: private */
    public RadioButton B;
    private RadioGroup.OnCheckedChangeListener C = new au(this);
    private bb D = new at(this);
    ProgressDialog a;
    short b;
    private int c = 1;
    private int d = 2;
    private int e = 3;
    /* access modifiers changed from: private */
    public String f;
    private String k;
    private String l;
    /* access modifiers changed from: private */
    public byte m;
    private String n = null;
    private int o;
    private int p;
    private String q;
    private ImageView r;
    private LinearLayout s;
    private RelativeLayout t;
    private RelativeLayout u;
    private RelativeLayout v;
    private TextView w;
    private TextView x;
    private TextView y;
    private RadioGroup z;

    public static Intent a(Context context, String str, String str2, String str3, int i, int i2, byte b2, String str4) {
        Intent intent = new Intent(context, EditInfoActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("userid", str);
        bundle.putString("nickname", str2);
        bundle.putString("instruction", str3);
        bundle.putInt("province", i);
        bundle.putInt("city", i2);
        bundle.putByte("sex", b2);
        bundle.putString("headURL", str4);
        intent.putExtras(bundle);
        return intent;
    }

    private String a(int i) {
        new SetLocationActivity();
        new ArrayList();
        return ((a) SetLocationActivity.a(this).get(i - 1)).a();
    }

    private String a(int i, int i2) {
        new SetLocationActivity();
        new ArrayList();
        List a2 = SetLocationActivity.a(this);
        return ((a) a2.get(i2 - 1)).a(i2, i, a2);
    }

    private void c(String str) {
        Display defaultDisplay = getWindowManager().getDefaultDisplay();
        Bitmap a2 = e.a(str, e.a(this, (float) (defaultDisplay.getWidth() / 8)), e.a(this, (float) (defaultDisplay.getHeight() / 10)));
        this.r.setVisibility(0);
        this.r.setImageBitmap(a2);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        c.a();
        c.a(this);
        super.onActivityResult(i, i2, intent);
        if (i2 == -1) {
            switch (i) {
                case 1:
                    Bitmap bitmap = (Bitmap) intent.getExtras().get("data");
                    int width = bitmap.getWidth();
                    if (width > bitmap.getHeight()) {
                        width = bitmap.getHeight();
                    }
                    Bitmap createBitmap = Bitmap.createBitmap(bitmap, 0, 0, width, width);
                    bitmap.recycle();
                    this.q = getCacheDir().getPath() + "/image_tmpPhoto.jpg";
                    if (!e.a(createBitmap, this.q, Bitmap.CompressFormat.JPEG)) {
                        this.q = null;
                    }
                    c(this.q);
                    createBitmap.recycle();
                    return;
                case 2:
                    Cursor query = getContentResolver().query(intent.getData(), new String[]{"_data"}, null, null, null);
                    if (query != null && query.moveToFirst()) {
                        this.q = query.getString(0);
                    }
                    if (query != null) {
                        query.close();
                    }
                    c(this.q);
                    return;
                case 3:
                    this.n = intent.getStringExtra("introduce");
                    this.x.setText(this.n);
                    return;
                case 4:
                    this.o = intent.getIntExtra("province", this.o);
                    this.p = intent.getIntExtra("city", this.p);
                    String a2 = a(this.o);
                    this.y.setText(a2 + "  " + a(this.p, this.o));
                    return;
                default:
                    return;
            }
        }
    }

    public void onClick(View view) {
        if (view.equals(this.g.a)) {
            finish();
        } else if (view.equals(this.g.d)) {
            String valueOf = String.valueOf(this.o);
            String valueOf2 = String.valueOf(this.p);
            String valueOf3 = String.valueOf((int) this.m);
            Hashtable hashtable = new Hashtable();
            hashtable.put("province", valueOf);
            hashtable.put("city", valueOf2);
            hashtable.put("sex", valueOf3);
            hashtable.put("signatures", this.n);
            if (this.q != null) {
                hashtable.put("sma_image", this.q);
            } else {
                hashtable.put("sma_image", "");
            }
            if (this.a != null && this.a.isShowing()) {
                this.a.dismiss();
            }
            this.a = null;
            this.b = c.a().a(hashtable, this.D);
            this.a = ProgressDialog.show(this, null, "保存信息中.....");
            this.a.setCancelable(true);
            this.a.setOnCancelListener(new as(this));
            c.a().a(hashtable, this.D);
        } else if (view.equals(this.u)) {
            String str = this.n;
            Intent intent = new Intent(this, InformationEditActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("introduce", str);
            intent.putExtras(bundle);
            startActivityForResult(intent, 3);
        } else if (view.equals(this.s)) {
            openContextMenu(this.s);
        } else if (view.equals(this.v)) {
            startActivityForResult(new Intent(this, SetLocationActivity.class), 4);
        }
    }

    public boolean onContextItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 1:
                startActivityForResult(new Intent("android.media.action.IMAGE_CAPTURE"), 1);
                break;
            case 2:
                startActivityForResult(new Intent("android.intent.action.PICK", MediaStore.Images.Media.INTERNAL_CONTENT_URI), 2);
                break;
        }
        return super.onContextItemSelected(menuItem);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.editinfo);
        Bundle extras = getIntent().getExtras();
        this.f = extras.getString("userid");
        this.l = extras.getString("nickname");
        this.k = extras.getString("headURL");
        this.m = extras.getByte("sex");
        this.n = extras.getString("instruction");
        this.o = extras.getInt("province");
        this.p = extras.getInt("city");
        this.r = (ImageView) findViewById(R.id.userHead);
        this.s = (LinearLayout) findViewById(R.id.UploadingHeadImage);
        registerForContextMenu(this.s);
        this.u = (RelativeLayout) findViewById(R.id.intrduceRelativeLayout);
        this.v = (RelativeLayout) findViewById(R.id.LocationRelativeLayout);
        this.t = (RelativeLayout) findViewById(R.id.NickNameRelativeLayout);
        this.w = (TextView) findViewById(R.id.NicknameTextView);
        this.x = (TextView) findViewById(R.id.IntroduceTextView);
        this.y = (TextView) findViewById(R.id.LocationTextView);
        this.A = (RadioButton) findViewById(R.id.famel);
        this.B = (RadioButton) findViewById(R.id.man);
        this.z = (RadioGroup) findViewById(R.id.sex);
        this.z.setOnCheckedChangeListener(this.C);
        this.s.setOnClickListener(this);
        this.u.setOnClickListener(this);
        this.v.setOnClickListener(this);
        g a2 = g.a(this);
        Bitmap a3 = a2.a(this.k);
        if (a3 == null) {
            a3 = a2.b();
            a2.a(this.k, new cr(this.r));
        }
        this.r.setImageBitmap(a3);
        if (this.l != null) {
            this.w.setTextColor((int) R.color.grey);
            this.w.setText(this.l);
        } else {
            this.t.setOnClickListener(this);
        }
        this.x.setText(this.n);
        if (!(this.o == 0 || this.p == 0)) {
            this.y.setText(a(this.o) + "   " + a(this.p, this.o));
        }
        if (this.m == 0 || this.m == 2) {
            this.B.setChecked(true);
            this.A.setChecked(false);
        } else if (this.m == 1) {
            this.A.setChecked(true);
            this.B.setChecked(false);
        }
        a((byte) 0, (int) R.string.cancel, this);
        b((byte) 0, R.string.save, this);
        setTitle((int) R.string.editinfo);
    }

    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        contextMenu.setHeaderTitle(getString(R.string.insert_image));
        contextMenu.add(0, 1, 0, getString(R.string.new_image));
        contextMenu.add(0, 2, 0, getString(R.string.old_image));
    }
}
