package com.vodone.caibo.activity.zoom;

import java.util.Observable;

public final class e extends Observable {
    private float a;

    public final float a() {
        return this.a;
    }

    public final void a(float f, float f2, float f3, float f4) {
        float f5 = (f3 / f4) / (f / f2);
        if (f5 != this.a) {
            this.a = f5;
            setChanged();
        }
    }
}
