package com.vodone.caibo.activity;

import android.app.AlertDialog;
import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.widget.RadioGroup;
import android.widget.TabHost;
import com.vodone.caibo.CaiboApp;
import com.vodone.caibo.R;
import com.vodone.caibo.b.c;
import com.windo.widget.WithNewIconRadioButton;

public class MainTabActivity extends TabActivity implements RadioGroup.OnCheckedChangeListener {
    private TabHost a;
    private Intent b;
    private Intent c;
    private Intent d;
    private Intent e;
    private Intent f;
    private String g;
    private String h;
    private WithNewIconRadioButton i;
    private WithNewIconRadioButton j;
    private WithNewIconRadioButton k;
    private WithNewIconRadioButton l;
    private RadioGroup m;

    private TabHost.TabSpec a(String str, int i2, int i3, Intent intent) {
        return this.a.newTabSpec(str).setIndicator(getString(i2), getResources().getDrawable(i3)).setContent(intent);
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        if (keyEvent.getKeyCode() != 4 || keyEvent.getAction() != 0) {
            return super.dispatchKeyEvent(keyEvent);
        }
        new AlertDialog.Builder(this).setMessage(getString(R.string.confirm_exit)).setPositiveButton((int) R.string.ok, new ac(this)).setNegativeButton((int) R.string.cancel, new ad(this)).show();
        return true;
    }

    public void onCheckedChanged(RadioGroup radioGroup, int i2) {
        MessageGroup c2;
        switch (radioGroup.getCheckedRadioButtonId()) {
            case R.id.radio_button0 /*2131427490*/:
                this.a.setCurrentTabByTag("mblog_tab");
                return;
            case R.id.radio_button1 /*2131427491*/:
                boolean z = true;
                if (cn.a().c() == null && CaiboApp.a().a(10) > 0) {
                    z = false;
                }
                this.a.setCurrentTabByTag("message_tab");
                if (z && (c2 = cn.a().c()) != null) {
                    if (CaiboApp.a().a(10) > 0) {
                        c2.a((byte) 21);
                        return;
                    } else if (CaiboApp.a().a(5) > 0) {
                        c2.a((byte) 4);
                        return;
                    } else if (CaiboApp.a().a(6) > 0) {
                        c2.a((byte) 5);
                        return;
                    } else if (CaiboApp.a().a(7) > 0) {
                        c2.a((byte) 3);
                        return;
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            case R.id.radio_button2 /*2131427492*/:
                this.a.setCurrentTabByTag("plaza_tab");
                return;
            case R.id.radio_button3 /*2131427493*/:
                this.a.setCurrentTabByTag("search_tab");
                return;
            case R.id.radio_button4 /*2131427494*/:
                this.a.setCurrentTabByTag("more_tab");
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        Intent intent;
        super.onCreate(bundle);
        requestWindowFeature(1);
        setContentView((int) R.layout.maintabs);
        c b2 = CaiboApp.a().b();
        this.g = b2.a;
        this.h = b2.b;
        c b3 = CaiboApp.a().b();
        if (b3 == null) {
            intent = null;
        } else {
            Intent intent2 = new Intent(this, TimeLineActivity.class);
            Bundle bundle2 = new Bundle();
            bundle2.putByte("timeline", (byte) 0);
            bundle2.putString("userid", b3.a);
            bundle2.putString("username", b3.b);
            intent2.putExtras(bundle2);
            intent = intent2;
        }
        this.b = intent;
        String str = this.g;
        String str2 = this.h;
        Intent intent3 = new Intent(this, MessageGroup.class);
        Bundle bundle3 = new Bundle();
        bundle3.putString("userid", str);
        bundle3.putString("username", str2);
        intent3.putExtras(bundle3);
        this.d = intent3;
        this.e = new Intent(this, PlazaActivity.class);
        this.f = new Intent(this, LotteryActiviy.class);
        this.c = new Intent(this, MoreItemsActivity.class);
        this.i = (WithNewIconRadioButton) findViewById(R.id.radio_button0);
        this.k = (WithNewIconRadioButton) findViewById(R.id.radio_button1);
        this.l = (WithNewIconRadioButton) findViewById(R.id.radio_button2);
        this.j = (WithNewIconRadioButton) findViewById(R.id.radio_button4);
        ((RadioGroup) findViewById(R.id.main_radio)).setOnCheckedChangeListener(this);
        CaiboApp a2 = CaiboApp.a();
        this.i.a();
        if (a2.a(8) > 0) {
            this.i.a(a2.a(8));
        }
        if (a2.a(11) > 0) {
            this.k.a(a2.a(11));
        }
        if (a2.a(12) > 0) {
            this.l.a(a2.a(12));
        }
        CaiboApp a3 = CaiboApp.a();
        bc bcVar = new bc(this, this.i);
        a3.a(8, bcVar);
        this.i.setTag(bcVar);
        bc bcVar2 = new bc(this, this.k);
        a3.a(11, bcVar2);
        this.k.setTag(bcVar2);
        bc bcVar3 = new bc(this, this.l);
        a3.a(12, bcVar3);
        this.l.setTag(bcVar3);
        this.a = getTabHost();
        TabHost tabHost = this.a;
        tabHost.addTab(a("mblog_tab", R.string.main_home, R.drawable.icon_1_n, this.b));
        tabHost.addTab(a("message_tab", R.string.main_news, R.drawable.icon_2_n, this.d));
        tabHost.addTab(a("plaza_tab", R.string.main_plaza, R.drawable.icon_4_n, this.e));
        tabHost.addTab(a("lottery_tab", R.string.main_lottery, R.drawable.icon_3_n, this.f));
        tabHost.addTab(a("more_tab", R.string.more, R.drawable.icon_5_n, this.c));
        this.m = (RadioGroup) findViewById(R.id.main_radio);
        this.m.check(R.id.radio_button0);
        if (!CaiboApp.a().e()) {
            CaiboApp.a().d();
        }
    }

    public void onDestroy() {
        super.onDestroy();
        CaiboApp a2 = CaiboApp.a();
        a2.b(8, (Handler) this.i.getTag());
        a2.b(11, (Handler) this.k.getTag());
        a2.b(12, (Handler) this.l.getTag());
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        String action = intent.getAction();
        if (action != null) {
            if ("home_list".equals(action)) {
                this.m.check(R.id.radio_button0);
                return;
            }
            this.m.check(R.id.radio_button1);
            MessageGroup c2 = cn.a().c();
            if (action.equals("atme_list")) {
                c2.a((byte) 4);
            } else if (action.equals("comment_list")) {
                c2.a((byte) 5);
            } else if (action.equals("mail_list")) {
                c2.a((byte) 3);
            } else if (action.equals("notification_list")) {
                c2.a((byte) 21);
            }
        }
    }
}
