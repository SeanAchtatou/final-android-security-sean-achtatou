package com.vodone.caibo.activity.zoom;

import java.util.Observable;
import java.util.Observer;

public final class d implements Observer {
    private e a;
    private final c b = new c();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    private static float a(float f) {
        return Math.max(0.0f, 0.5f * ((f - 1.0f) / f));
    }

    private void b() {
        float a2 = this.a.a();
        float d = this.b.d(a2);
        float e = this.b.e(a2);
        float a3 = 0.5f - a(d);
        float a4 = a(d) + 0.5f;
        float a5 = 0.5f - a(e);
        float a6 = a(e) + 0.5f;
        if (this.b.a() > a4) {
            this.b.a(a4);
        }
        if (this.b.a() < a3) {
            this.b.a(a3);
        }
        if (this.b.b() > a6) {
            this.b.b(a6);
        }
        if (this.b.b() < a5) {
            this.b.b(a5);
        }
    }

    private void c() {
        if (this.b.c() > 16.0f) {
            this.b.c(16.0f);
        }
        if (this.b.c() < 1.0f) {
            this.b.c(1.0f);
        }
    }

    public final c a() {
        return this.b;
    }

    public final void a(float f, float f2) {
        float a2 = this.a.a();
        this.b.a(this.b.a() + (f / this.b.d(a2)));
        this.b.b((f2 / this.b.e(a2)) + this.b.b());
        b();
        this.b.notifyObservers();
    }

    public final void a(float f, float f2, float f3) {
        float a2 = this.a.a();
        float d = this.b.d(a2);
        float e = this.b.e(a2);
        this.b.c(this.b.c() * f);
        c();
        float d2 = this.b.d(a2);
        float e2 = this.b.e(a2);
        this.b.a((((1.0f / d) - (1.0f / d2)) * (f2 - 0.5f)) + this.b.a());
        this.b.b((((1.0f / e) - (1.0f / e2)) * (f3 - 0.5f)) + this.b.b());
        b();
        this.b.notifyObservers();
    }

    public final void a(e eVar) {
        if (this.a != null) {
            this.a.deleteObserver(this);
        }
        this.a = eVar;
        this.a.addObserver(this);
    }

    public final void update(Observable observable, Object obj) {
        c();
        b();
    }
}
