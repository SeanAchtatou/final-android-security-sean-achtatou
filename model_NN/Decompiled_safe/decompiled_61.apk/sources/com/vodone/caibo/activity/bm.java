package com.vodone.caibo.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.view.KeyEvent;

final class bm implements DialogInterface.OnClickListener {
    private /* synthetic */ String a;
    private /* synthetic */ BlogDetailsActivity b;

    bm(BlogDetailsActivity blogDetailsActivity, String str) {
        this.b = blogDetailsActivity;
        this.a = str;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        try {
            new KeyEvent(0, 0, 0, 59, 0, 0).dispatch(this.b.g);
        } catch (Exception e) {
        }
        Intent intent = new Intent("android.intent.action.SENDTO", Uri.parse("smsto:"));
        intent.putExtra("sms_body", this.a);
        this.b.startActivity(intent);
    }
}
