package com.vodone.caibo;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;

public class CaiboActivity extends Activity {
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        TextView textView = new TextView(this);
        textView.setBackgroundDrawable(getResources().getDrawable(R.drawable.loading));
        setContentView(textView);
        new Handler().postDelayed(new b(this), 2000);
    }
}
