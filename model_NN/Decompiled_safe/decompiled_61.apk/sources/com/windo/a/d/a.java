package com.windo.a.d;

import com.windo.a.a.d;
import com.windo.a.b.a.b;
import com.windo.a.b.a.e;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.Hashtable;

public final class a implements Runnable {
    private static int d = 0;
    private d a = new d();
    private Thread b;
    private boolean c = false;
    private com.windo.a.b.a e;

    private static void a(c cVar, int i) {
        if (cVar.h != null) {
            cVar.h.a(cVar.a, i);
        }
    }

    private void a(c cVar, InputStream inputStream, OutputStream outputStream, int i) {
        byte[] bArr = new byte[1024];
        int i2 = 0;
        while (!d(cVar) && i2 < i) {
            int read = inputStream.read(bArr, 0, Math.min(bArr.length, i - i2));
            if (read == -1) {
                throw new IOException();
            }
            outputStream.write(bArr, 0, read);
            i2 += read;
            try {
                Thread.sleep(10);
            } catch (Exception e2) {
            }
        }
    }

    private byte[] a(com.windo.a.b.a aVar, c cVar) {
        InputStream inputStream;
        Throwable th;
        try {
            InputStream d2 = aVar.d();
            try {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                if (aVar.a("Content-Length") != null) {
                    a(cVar, d2, byteArrayOutputStream, (int) aVar.e());
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    if (d2 != null) {
                        d2.close();
                    }
                    return byteArray;
                }
                long currentTimeMillis = System.currentTimeMillis();
                byte[] bArr = new byte[1024];
                while (true) {
                    if (d(cVar)) {
                        break;
                    }
                    int read = d2.read(bArr, 0, bArr.length);
                    long currentTimeMillis2 = System.currentTimeMillis();
                    if (read < 0) {
                        break;
                    } else if (read != 0) {
                        byteArrayOutputStream.write(bArr, 0, read);
                        currentTimeMillis = currentTimeMillis2;
                    } else if (currentTimeMillis2 - currentTimeMillis > 5000) {
                        System.out.println("time out  > 5000");
                        break;
                    } else {
                        try {
                            Thread.sleep(50);
                        } catch (Exception e2) {
                        }
                    }
                }
                byte[] byteArray2 = byteArrayOutputStream.toByteArray();
                if (d2 != null) {
                    d2.close();
                }
                return byteArray2;
            } catch (Throwable th2) {
                Throwable th3 = th2;
                inputStream = d2;
                th = th3;
                if (inputStream != null) {
                    inputStream.close();
                }
                throw th;
            }
        } catch (Throwable th4) {
            Throwable th5 = th4;
            inputStream = null;
            th = th5;
        }
    }

    public static synchronized int b() {
        int i;
        synchronized (a.class) {
            i = d;
            d = i + 1;
        }
        return i;
    }

    private int b(c cVar) {
        InputStream inputStream;
        c(cVar);
        int c2 = this.e.c();
        if (d(cVar)) {
            c();
            return c2;
        }
        String a2 = this.e.a("Content-Type");
        if (a2 == null || a2.indexOf("vnd.wap.wml") < 0) {
            try {
                if (cVar.i && c2 == 200) {
                    inputStream = null;
                    InputStream d2 = this.e.d();
                    this.e.e();
                    if (cVar.h != null) {
                        cVar.h.a(cVar.a, d2);
                    }
                    if (d2 != null) {
                        d2.close();
                    }
                } else if (!d(cVar)) {
                    if (c2 == 200) {
                        byte[] a3 = a(this.e, cVar);
                        com.windo.a.b.a aVar = this.e;
                        if (cVar.h != null) {
                            cVar.h.a(cVar.a, a3, aVar);
                        }
                    } else {
                        a(cVar, c2);
                    }
                }
                c();
                return c2;
            } catch (IOException e2) {
                try {
                    throw e2;
                } catch (Throwable th) {
                    c();
                    throw th;
                }
            } catch (Throwable th2) {
                if (inputStream != null) {
                    inputStream.close();
                }
                throw th2;
            }
        } else {
            b.b("HttpEngine", "Content-Type:" + a2);
            c();
            return -1;
        }
    }

    private com.windo.a.b.a c(c cVar) {
        String str = cVar.c;
        if (!str.startsWith("http://")) {
            "http://" + str;
        }
        this.e = e.b(cVar.c, cVar.d);
        this.e.f();
        try {
            Hashtable hashtable = cVar.e;
            if (hashtable != null && hashtable.size() > 0) {
                Enumeration keys = hashtable.keys();
                while (keys.hasMoreElements()) {
                    String str2 = (String) keys.nextElement();
                    this.e.a(str2, (String) hashtable.get(str2));
                }
            }
            if (cVar.d.equals("POST")) {
                if (cVar.f != null) {
                    this.e.a(cVar.f);
                }
                if (cVar.g != null) {
                    this.e.a(cVar.g, cVar.d());
                }
            }
            this.e.b();
            return this.e;
        } catch (SecurityException e2) {
            this.e.a();
            this.e = null;
            throw e2;
        } catch (IOException e3) {
            e3.printStackTrace();
            this.e.a();
            this.e = null;
            b.c("[Error]HttpEngine", "HttpEngine trySend IOException");
            throw e3;
        }
    }

    private void c() {
        if (this.e != null) {
            try {
                this.e.a();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
            this.e = null;
        }
    }

    private boolean d(c cVar) {
        return this.c || cVar.b;
    }

    public final void a() {
        if (this.b != null) {
            this.c = true;
            c();
            this.a.b();
            this.b.interrupt();
            this.b = null;
        }
    }

    public final void a(c cVar) {
        this.a.a(cVar);
        if (this.b == null) {
            this.b = new Thread(this);
            this.b.start();
        }
    }

    public final void run() {
        b.b("HttpEngine", "Http Engine start");
        while (!this.c) {
            Object a2 = this.a.a();
            if (a2 != null) {
                c cVar = (c) a2;
                int i = 0;
                while (!this.c && i < 3 && !cVar.b) {
                    try {
                        if (b(cVar) != -1) {
                            break;
                        }
                        i++;
                    } catch (IOException e2) {
                        e2.printStackTrace();
                        b.c("[Error]HttpEngine", "Http Engine Catch IOException : " + e2.toString());
                        a(cVar, 6002);
                    } catch (SecurityException e3) {
                        e3.printStackTrace();
                        b.c("[Error]HttpEngine", "Http Engine Catch SecurityException : " + e3.toString());
                        a(cVar, 6001);
                    } catch (Exception e4) {
                        e4.printStackTrace();
                        b.c("[Error]HttpEngine", "Http Engine Catch Exception : " + e4.toString());
                        a(cVar, 6003);
                    }
                }
                if (d(cVar)) {
                    a(cVar, 6004);
                } else if (i >= 3) {
                    a(cVar, 6002);
                }
            }
        }
        b.b("HttpEngine", "Http Engine End");
    }
}
