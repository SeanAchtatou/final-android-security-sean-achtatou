package com.windo.a.d;

import java.util.Hashtable;

public final class c {
    int a;
    boolean b;
    String c;
    String d;
    Hashtable e;
    byte[] f;
    String g;
    b h;
    boolean i;
    private Hashtable j;

    public c(String str) {
        this(str, "GET");
    }

    private c(String str, String str2) {
        this.b = false;
        this.c = str;
        this.d = str2;
        this.a = a.b();
        this.b = false;
    }

    public final int a() {
        return this.a;
    }

    public final void a(b bVar) {
        this.h = bVar;
    }

    public final void a(String str) {
        this.g = str;
        if (str != null) {
            this.d = "POST";
        }
    }

    public final void b() {
        this.b = true;
    }

    public final void c() {
        this.i = true;
    }

    public final Hashtable d() {
        return this.j;
    }
}
