package com.windo.a.b.a;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

public final class f {
    private File a;
    private boolean b = true;

    public f(String str) {
        this.a = new File(str);
    }

    public final String a() {
        return this.a.getName();
    }

    public final long b() {
        if (this.a.isFile()) {
            return this.a.length();
        }
        return 0;
    }

    public final InputStream c() {
        return new FileInputStream(this.a);
    }
}
