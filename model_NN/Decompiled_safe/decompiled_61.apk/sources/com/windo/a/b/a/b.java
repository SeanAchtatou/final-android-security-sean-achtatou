package com.windo.a.b.a;

import android.os.Environment;
import android.util.Log;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Calendar;

public final class b {
    static String a;
    public static d b = null;
    private static boolean c = true;
    private static String d;
    private static Object e = new Object();
    private static Calendar f = Calendar.getInstance();
    private static StringBuffer g = new StringBuffer();
    private static OutputStream h;
    private static long i;

    private static File a(String str) {
        if (a == null || a.length() == 0) {
            return null;
        }
        return new File(a + d + "_" + str);
    }

    public static void a() {
        if (b != null) {
            d dVar = b;
            a("PaintLogThread:", "shutdown", 4);
            dVar.b = true;
            if (dVar.a != null) {
                dVar.a.destroy();
                dVar.a = null;
            }
            b = null;
        }
    }

    public static void a(String str, String str2) {
        synchronized (e) {
            if (Environment.getExternalStorageState().equals("mounted")) {
                a = Environment.getExternalStorageDirectory() + "/vodone/caibo" + "/";
            } else {
                a = "/data/data/%packetname%/files/".replaceFirst("%packetname%", str);
            }
            d = str2;
            Log.d("iniAppPath", a);
            File file = new File(a);
            if (!file.exists()) {
                file.mkdirs();
            }
        }
    }

    private static void a(String str, String str2, int i2) {
        if (c) {
            String str3 = str == null ? "TAG_NULL" : str;
            String str4 = str2 == null ? "MSG_NULL" : str2;
            if (i2 >= 2) {
                switch (i2) {
                    case 2:
                        Log.v(str3, str4);
                        break;
                    case 3:
                        Log.d(str3, str4);
                        break;
                    case 4:
                        Log.i(str3, str4);
                        break;
                    case 5:
                        Log.w(str3, str4);
                        break;
                    case 6:
                        Log.e(str3, str4);
                        break;
                }
                b(str3, str4, i2);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    private static OutputStream b() {
        if (h == null) {
            try {
                if (a == null || a.length() == 0) {
                    return null;
                }
                File a2 = a("log.temp");
                if (a2 == null) {
                    return null;
                }
                if (a2.exists()) {
                    h = new FileOutputStream(a2, true);
                    i = a2.length();
                } else {
                    h = new FileOutputStream(a2);
                    i = 0;
                }
            } catch (FileNotFoundException e2) {
                e2.printStackTrace();
            } catch (IOException e3) {
                e3.printStackTrace();
            }
        }
        return h;
    }

    public static void b(String str, String str2) {
        a(str, str2, 3);
    }

    /* access modifiers changed from: private */
    public static void b(String str, String str2, int i2) {
        synchronized (e) {
            OutputStream b2 = b();
            if (b2 != null) {
                try {
                    f.setTimeInMillis(System.currentTimeMillis());
                    g.setLength(0);
                    g.append("[");
                    g.append(str);
                    g.append(" : ");
                    g.append(f.get(2) + 1);
                    g.append("-");
                    g.append(f.get(5));
                    g.append(" ");
                    g.append(f.get(11));
                    g.append(":");
                    g.append(f.get(12));
                    g.append(":");
                    g.append(f.get(13));
                    g.append(":");
                    g.append(f.get(14));
                    g.append("] ");
                    g.append(str2);
                    byte[] bytes = g.toString().getBytes("utf-8");
                    if (i < 1048576) {
                        b2.write(bytes);
                        b2.write("\r\n".getBytes());
                        b2.flush();
                        i = ((long) bytes.length) + i;
                    } else {
                        try {
                            if (h != null) {
                                h.close();
                                h = null;
                                i = 0;
                            }
                        } catch (Exception e2) {
                            e2.printStackTrace();
                        }
                        synchronized (e) {
                            File a2 = a("log.temp");
                            File a3 = a("log_last.txt");
                            if (a3.exists()) {
                                a3.delete();
                            }
                            a2.renameTo(a3);
                        }
                        b(str, str2, i2);
                    }
                } catch (Exception e3) {
                    e3.printStackTrace();
                }
            } else {
                Log.w("vodone Log", "Log File open fail: [AppPath]=" + a + ",[LogName]:" + d);
            }
        }
    }

    public static void c(String str, String str2) {
        a(str, str2, 6);
    }

    public static void d(String str, String str2) {
        a(str, str2, 4);
    }
}
