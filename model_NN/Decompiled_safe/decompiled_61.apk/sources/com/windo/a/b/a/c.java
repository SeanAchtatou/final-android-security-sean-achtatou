package com.windo.a.b.a;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.LinkedList;
import java.util.List;
import org.apache.http.entity.AbstractHttpEntity;

public final class c extends AbstractHttpEntity {
    private List a = new LinkedList();

    public final void a(InputStream inputStream, int i) {
        if (inputStream != null) {
            a aVar = new a(this, 2);
            Object unused = aVar.b = inputStream;
            int unused2 = aVar.c = i;
            this.a.add(aVar);
        }
    }

    public final void a(byte[] bArr) {
        if (bArr != null) {
            a aVar = new a(this, 1);
            Object unused = aVar.b = bArr;
            int unused2 = aVar.c = -1;
            this.a.add(aVar);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    public final InputStream getContent() {
        int read;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        for (int i = 0; i < this.a.size(); i++) {
            a aVar = (a) this.a.get(i);
            if (aVar.a == 1) {
                byteArrayOutputStream.write((byte[]) aVar.b, 0, aVar.c > 0 ? aVar.c : ((byte[]) aVar.b).length);
            } else if (aVar.a == 2) {
                InputStream inputStream = (InputStream) aVar.b;
                byte[] bArr = new byte[2048];
                if (aVar.c < 0) {
                    while (true) {
                        int read2 = inputStream.read(bArr);
                        if (read2 == -1) {
                            break;
                        }
                        byteArrayOutputStream.write(bArr, 0, read2);
                    }
                } else {
                    long c = (long) aVar.c;
                    while (c > 0 && (read = inputStream.read(bArr, 0, (int) Math.min(2048L, c))) != -1) {
                        byteArrayOutputStream.write(bArr, 0, read);
                        c -= (long) read;
                    }
                }
            }
        }
        return new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
    }

    public final long getContentLength() {
        int i = 0;
        int i2 = 0;
        while (true) {
            int i3 = i;
            if (i2 >= this.a.size()) {
                return (long) i3;
            }
            a aVar = (a) this.a.get(i2);
            i = ((aVar.c >= 0 || aVar.a != 1) ? aVar.c : ((byte[]) aVar.b).length) + i3;
            i2++;
        }
    }

    public final boolean isRepeatable() {
        return false;
    }

    public final boolean isStreaming() {
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    public final void writeTo(OutputStream outputStream) {
        InputStream inputStream;
        int read;
        if (outputStream == null) {
            throw new IllegalArgumentException("Output stream may not be null");
        }
        for (int i = 0; i < this.a.size(); i++) {
            a aVar = (a) this.a.get(i);
            if (aVar.a == 1) {
                outputStream.write((byte[]) aVar.b, 0, aVar.c > 0 ? aVar.c : ((byte[]) aVar.b).length);
            } else if (aVar.a == 2 && (inputStream = (InputStream) aVar.b) != null) {
                byte[] bArr = new byte[2048];
                if (aVar.c < 0) {
                    int unused = aVar.c = 0;
                    while (true) {
                        int read2 = inputStream.read(bArr);
                        if (read2 == -1) {
                            break;
                        }
                        outputStream.write(bArr, 0, read2);
                        a.b(aVar, read2);
                    }
                } else {
                    long c = (long) aVar.c;
                    while (c > 0 && (read = inputStream.read(bArr, 0, (int) Math.min(2048L, c))) != -1) {
                        outputStream.write(bArr, 0, read);
                        c -= (long) read;
                    }
                }
                inputStream.close();
            }
        }
        outputStream.flush();
    }
}
