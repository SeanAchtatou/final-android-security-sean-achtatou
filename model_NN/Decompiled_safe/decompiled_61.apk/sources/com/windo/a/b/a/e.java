package com.windo.a.b.a;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.vodone.caibo.CaiboApp;
import com.windo.a.b.a;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Random;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpProtocolParams;

public final class e implements a {
    private static Random b = new Random();
    private static Hashtable l = new Hashtable();
    private final String a = "PAL_HTTP";
    private String c;
    private String d;
    private byte[] e;
    private String f;
    private Hashtable g;
    private HttpClient h;
    private Hashtable i;
    private HttpResponse j;
    private int k;

    public static boolean a(Context context) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo == null || activeNetworkInfo.getType() != 0) {
            return false;
        }
        String extraInfo = activeNetworkInfo.getExtraInfo();
        if (extraInfo == null || extraInfo.length() < 3) {
            return false;
        }
        return extraInfo.toLowerCase().contains("wap");
    }

    public static a b(String str, String str2) {
        e eVar = new e();
        eVar.c = str;
        eVar.d = str2;
        return eVar;
    }

    private void g() {
        if (this.h == null) {
            BasicHttpParams basicHttpParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(basicHttpParams, 20000);
            HttpConnectionParams.setSoTimeout(basicHttpParams, 20000);
            HttpConnectionParams.setSocketBufferSize(basicHttpParams, 102400);
            HttpProtocolParams.setUseExpectContinue(basicHttpParams, true);
            SchemeRegistry schemeRegistry = new SchemeRegistry();
            schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            this.h = new DefaultHttpClient(new ThreadSafeClientConnManager(basicHttpParams, schemeRegistry), basicHttpParams);
            if (a(CaiboApp.a())) {
                b.b("PALHttp ", "buildHttpClient isWap");
                this.h.getParams().setParameter("http.route.default-proxy", new HttpHost("10.0.0.172", 80));
            }
        }
    }

    private HttpResponse h() {
        HttpEntity httpEntity;
        b.d("PAL_HTTP", "[http post url:]" + this.c);
        g();
        HttpPost httpPost = new HttpPost(this.c);
        if (this.i != null && this.i.size() > 0) {
            Enumeration keys = this.i.keys();
            while (keys.hasMoreElements()) {
                String str = (String) keys.nextElement();
                httpPost.setHeader(str, (String) this.i.get(str));
            }
        }
        if (this.e != null) {
            httpEntity = new ByteArrayEntity(this.e);
        } else if (this.f != null) {
            String str2 = String.valueOf(System.currentTimeMillis()) + String.valueOf(Math.abs(b.nextLong()));
            f fVar = new f(this.f);
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("--");
            stringBuffer.append(str2);
            stringBuffer.append("\r\n");
            stringBuffer.append("Content-Disposition: form-data; ");
            if (this.g == null || this.g.size() <= 0) {
                stringBuffer.append("name=\"pic\"; ");
            } else {
                Enumeration keys2 = this.g.keys();
                while (keys2.hasMoreElements()) {
                    String str3 = (String) keys2.nextElement();
                    stringBuffer.append(str3 + "=" + ((String) this.g.get(str3)) + "; ");
                }
            }
            stringBuffer.append("filename=\"" + fVar.a() + "\"\r\n");
            stringBuffer.append("Content-Type: application/octet-stream;\r\n");
            stringBuffer.append("Content-Transfer-Encoding: binary\r\n");
            stringBuffer.append("\r\n");
            try {
                byte[] bytes = stringBuffer.toString().getBytes();
                byte[] bytes2 = ("\r\n--" + str2 + "--\r\n").getBytes();
                httpPost.setHeader("Content-Type", "multipart/form-data; boundary=" + str2);
                InputStream c2 = fVar.c();
                HttpEntity cVar = new c();
                cVar.a(bytes);
                cVar.a(c2, (int) fVar.b());
                cVar.a(bytes2);
                httpEntity = cVar;
            } catch (IOException e2) {
                e2.printStackTrace();
                throw e2;
            }
        } else {
            httpEntity = null;
        }
        httpPost.setEntity(httpEntity);
        return this.h.execute(httpPost);
    }

    public final String a(String str) {
        Header firstHeader;
        if (this.j == null || (firstHeader = this.j.getFirstHeader(str)) == null) {
            return null;
        }
        return firstHeader.getValue();
    }

    public final void a() {
        if (this.h != null) {
            this.h.getConnectionManager().shutdown();
            this.h = null;
        }
    }

    public final void a(String str, String str2) {
        if (this.i == null) {
            this.i = new Hashtable();
        }
        this.i.put(str, str2);
    }

    public final void a(String str, Hashtable hashtable) {
        this.f = str;
        this.g = hashtable;
    }

    public final void a(byte[] bArr) {
        this.e = bArr;
    }

    public final int b() {
        g();
        if (this.d.equals("POST")) {
            this.j = h();
        } else {
            b.d("PAL_HTTP", "[http get url:]" + this.c);
            g();
            HttpGet httpGet = new HttpGet(this.c);
            if (this.i != null && this.i.size() > 0) {
                Enumeration keys = this.i.keys();
                while (keys.hasMoreElements()) {
                    String str = (String) keys.nextElement();
                    httpGet.setHeader(str, (String) this.i.get(str));
                }
            }
            this.j = this.h.execute(httpGet);
        }
        if (this.j == null || this.j.getStatusLine() == null) {
            b.c("PAL_HTTP", "mHttpResponse = null");
            return -1;
        }
        b.d("PAL_HTTP", "ResponseCode" + this.j.getStatusLine().getStatusCode());
        return this.j.getStatusLine().getStatusCode();
    }

    public final int c() {
        if (this.j == null || this.j.getStatusLine() == null) {
            return -1;
        }
        return this.j.getStatusLine().getStatusCode();
    }

    public final InputStream d() {
        if (this.j == null || this.j.getEntity() == null) {
            return null;
        }
        return this.j.getEntity().getContent();
    }

    public final long e() {
        HttpEntity entity;
        if (this.j == null || (entity = this.j.getEntity()) == null) {
            return 0;
        }
        return entity.getContentLength();
    }

    public final void f() {
        this.k = 20000;
    }
}
