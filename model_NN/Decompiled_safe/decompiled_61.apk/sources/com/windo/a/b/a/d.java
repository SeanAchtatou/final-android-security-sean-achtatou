package com.windo.a.b.a;

final class d extends Thread {
    Process a;
    boolean b;

    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        android.util.Log.i("PaintLogThread:", "readLine==null");
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r4 = this;
            java.lang.String r0 = "PaintLogThread:"
            java.lang.String r0 = "PaintLogThread:"
            java.lang.String r1 = "start"
            com.windo.a.b.a.b.d(r0, r1)     // Catch:{ Exception -> 0x0057 }
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ Exception -> 0x0057 }
            r0.<init>()     // Catch:{ Exception -> 0x0057 }
            java.lang.String r1 = "logcat"
            r0.add(r1)     // Catch:{ Exception -> 0x0057 }
            java.lang.String r1 = "-v"
            r0.add(r1)     // Catch:{ Exception -> 0x0057 }
            java.lang.String r1 = "time"
            r0.add(r1)     // Catch:{ Exception -> 0x0057 }
            java.lang.Runtime r1 = java.lang.Runtime.getRuntime()     // Catch:{ Exception -> 0x0057 }
            int r2 = r0.size()     // Catch:{ Exception -> 0x0057 }
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ Exception -> 0x0057 }
            java.lang.Object[] r0 = r0.toArray(r2)     // Catch:{ Exception -> 0x0057 }
            java.lang.String[] r0 = (java.lang.String[]) r0     // Catch:{ Exception -> 0x0057 }
            java.lang.Process r0 = r1.exec(r0)     // Catch:{ Exception -> 0x0057 }
            r4.a = r0     // Catch:{ Exception -> 0x0057 }
            java.io.BufferedReader r0 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0057 }
            java.io.InputStreamReader r1 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x0057 }
            java.lang.Process r2 = r4.a     // Catch:{ Exception -> 0x0057 }
            java.io.InputStream r2 = r2.getInputStream()     // Catch:{ Exception -> 0x0057 }
            r1.<init>(r2)     // Catch:{ Exception -> 0x0057 }
            r0.<init>(r1)     // Catch:{ Exception -> 0x0057 }
        L_0x0043:
            boolean r1 = r4.b     // Catch:{ Exception -> 0x0057 }
            if (r1 != 0) goto L_0x0081
            java.lang.String r1 = r0.readLine()     // Catch:{ Exception -> 0x0057 }
            if (r1 == 0) goto L_0x0078
            java.lang.String r2 = com.windo.a.b.a.b.a     // Catch:{ Exception -> 0x0057 }
            if (r2 == 0) goto L_0x0078
            java.lang.String r2 = "SysLog"
            com.windo.a.b.a.b.b(r2, r1, 2)     // Catch:{ Exception -> 0x0057 }
            goto L_0x0043
        L_0x0057:
            r0 = move-exception
            r0.printStackTrace()
            java.lang.String r1 = "NeteaseLog"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "logcatToFile Exception:"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r0 = r0.toString()
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            android.util.Log.d(r1, r0)
        L_0x0077:
            return
        L_0x0078:
            if (r1 != 0) goto L_0x0043
            java.lang.String r1 = "PaintLogThread:"
            java.lang.String r2 = "readLine==null"
            android.util.Log.i(r1, r2)     // Catch:{ Exception -> 0x0057 }
        L_0x0081:
            r0.close()     // Catch:{ Exception -> 0x0057 }
            java.lang.Process r0 = r4.a     // Catch:{ Exception -> 0x0057 }
            if (r0 == 0) goto L_0x008d
            java.lang.Process r0 = r4.a     // Catch:{ Exception -> 0x0057 }
            r0.destroy()     // Catch:{ Exception -> 0x0057 }
        L_0x008d:
            r0 = 0
            r4.a = r0     // Catch:{ Exception -> 0x0057 }
            r0 = 0
            com.windo.a.b.a.b.b = r0     // Catch:{ Exception -> 0x0057 }
            java.lang.String r0 = "PaintLogThread:"
            java.lang.String r1 = "end PaintLogThread:"
            android.util.Log.i(r0, r1)     // Catch:{ Exception -> 0x0057 }
            goto L_0x0077
        */
        throw new UnsupportedOperationException("Method not decompiled: com.windo.a.b.a.d.run():void");
    }
}
