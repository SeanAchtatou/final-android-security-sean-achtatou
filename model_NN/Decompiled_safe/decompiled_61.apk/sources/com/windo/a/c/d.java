package com.windo.a.c;

import com.windo.a.b.a.b;
import java.util.Hashtable;

public final class d implements Runnable {
    private Thread a;
    private boolean b;
    private Hashtable c = new Hashtable();
    private com.windo.a.a.d d = new com.windo.a.a.d();
    private c e;
    private boolean f;
    private a g;

    public final void a() {
        if (this.a != null) {
            this.b = true;
            this.a = null;
            this.d.b();
            if (this.f && this.e != null) {
                this.e.f();
                this.e = null;
            }
            if (this.g != null) {
                this.g.a();
                this.g = null;
            }
        }
    }

    public final void a(a aVar) {
        this.g = aVar;
    }

    public final void a(c cVar) {
        if (cVar != null) {
            cVar.a(this);
            synchronized (this.c) {
                if (cVar instanceof f) {
                    ((f) cVar).j = this.g;
                }
                this.d.a(cVar);
                this.c.put(new Short(cVar.c()), cVar);
                if (this.a == null && this.a == null) {
                    this.a = new Thread(this);
                    this.b = false;
                    this.a.start();
                }
            }
        }
    }

    public final void a(Short sh, int i) {
        f fVar = null;
        synchronized (this.c) {
            if (this.c.containsKey(sh)) {
                fVar = (f) this.c.get(sh);
            }
        }
        if (fVar != null) {
            fVar.a(i);
            fVar.a.b(fVar);
        }
    }

    public final void a(Short sh, byte[] bArr) {
        f fVar = null;
        synchronized (this.c) {
            if (this.c.containsKey(sh)) {
                fVar = (f) this.c.get(sh);
            }
        }
        if (fVar != null) {
            fVar.b(bArr);
        }
    }

    public final void a(short s) {
        synchronized (this.c) {
            Short sh = new Short(s);
            if (this.c.containsKey(sh)) {
                c cVar = (c) this.c.remove(sh);
                cVar.f();
                this.d.b(cVar);
            }
        }
    }

    public final void b(c cVar) {
        synchronized (this.c) {
            Short sh = new Short(cVar.c());
            if (this.c.containsKey(sh)) {
                this.d.b((c) this.c.remove(sh));
            }
        }
    }

    public final void b(short s) {
        synchronized (this.c) {
            Short sh = new Short(s);
            if (this.c.containsKey(sh)) {
                this.d.a((c) this.c.get(sh));
            }
        }
    }

    public final void run() {
        while (!this.b) {
            Object a2 = this.d.a();
            if (a2 != null) {
                c cVar = (c) a2;
                this.e = cVar;
                this.f = true;
                try {
                    cVar.g();
                } catch (Exception e2) {
                    e2.printStackTrace();
                    b.b("[Error]TransactionEngine Thread", e2.toString());
                    cVar.f();
                    cVar.a(e2);
                    b(cVar);
                }
                this.f = false;
                this.e = null;
            }
        }
        this.a = null;
    }
}
