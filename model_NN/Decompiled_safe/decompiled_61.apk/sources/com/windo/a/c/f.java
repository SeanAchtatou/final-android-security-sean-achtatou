package com.windo.a.c;

public abstract class f extends c {
    a j;

    protected f(d dVar, int i) {
        super(dVar, i);
    }

    public abstract void a(int i);

    /* access modifiers changed from: protected */
    public final void a(Object obj) {
        if (this.j != null) {
            this.j.a(obj, this);
        }
    }

    public abstract void a(byte[] bArr);

    /* access modifiers changed from: protected */
    public void b(byte[] bArr) {
        a(bArr);
        this.a.b(this);
    }

    public final void f() {
        super.f();
        if (this.j != null) {
            this.j.a(this);
        }
    }

    public final void g() {
        a();
    }
}
