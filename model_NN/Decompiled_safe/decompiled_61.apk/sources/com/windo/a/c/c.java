package com.windo.a.c;

public abstract class c {
    private static short f = 0;
    d a;
    private int b;
    private short c = h();
    private boolean d;
    private e e;

    protected c(d dVar, int i) {
        this.b = i;
        this.a = dVar;
    }

    private static synchronized short h() {
        short s;
        synchronized (c.class) {
            if (f >= 999) {
                f = 0;
            }
            s = f;
            f = (short) (s + 1);
        }
        return s;
    }

    public abstract void a();

    /* access modifiers changed from: protected */
    public final void a(int i, int i2, int i3, Object obj) {
        if (this.e != null && !this.d) {
            this.e.a(i, i2, i3, obj);
        }
    }

    public final void a(int i, int i2, Object obj) {
        if (this.e != null && !this.d) {
            this.e.b(i, i2, -1, obj);
        }
    }

    public final void a(d dVar) {
        this.a = dVar;
    }

    public final void a(e eVar) {
        this.e = eVar;
    }

    public abstract void a(Exception exc);

    public final d b() {
        return this.a;
    }

    public final short c() {
        return this.c;
    }

    public final int d() {
        return this.b;
    }

    public final boolean e() {
        return this.d;
    }

    public void f() {
        this.d = true;
    }

    public void g() {
        a();
        this.a.b(this);
    }
}
