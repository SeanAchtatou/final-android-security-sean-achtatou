package com.windo.a.a.a;

import java.util.Enumeration;
import java.util.Hashtable;

public final class d {
    public static final Object a = new c();
    private Hashtable b;

    public d() {
        this.b = new Hashtable();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x004f A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0069 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001c  */
    public d(com.windo.a.a.a.b r4) {
        /*
            r3 = this;
            r3.<init>()
            char r0 = r4.c()
            r1 = 123(0x7b, float:1.72E-43)
            if (r0 == r1) goto L_0x0015
            java.lang.String r0 = "A JSONObject text must begin with '{'"
            com.windo.a.a.a.a r0 = r4.a(r0)
            throw r0
        L_0x0012:
            r4.a()
        L_0x0015:
            char r0 = r4.c()
            switch(r0) {
                case 0: goto L_0x004f;
                case 125: goto L_0x0069;
                default: goto L_0x001c;
            }
        L_0x001c:
            r4.a()
            java.lang.Object r0 = r4.d()
            java.lang.String r0 = r0.toString()
            char r1 = r4.c()
            r2 = 61
            if (r1 != r2) goto L_0x0056
            char r1 = r4.b()
            r2 = 62
            if (r1 == r2) goto L_0x003a
            r4.a()
        L_0x003a:
            java.lang.Object r1 = r4.d()
            r3.a(r0, r1)
            char r0 = r4.c()
            switch(r0) {
                case 44: goto L_0x0061;
                case 59: goto L_0x0061;
                case 125: goto L_0x0069;
                default: goto L_0x0048;
            }
        L_0x0048:
            java.lang.String r0 = "Expected a ',' or '}'"
            com.windo.a.a.a.a r0 = r4.a(r0)
            throw r0
        L_0x004f:
            java.lang.String r0 = "A JSONObject text must end with '}'"
            com.windo.a.a.a.a r0 = r4.a(r0)
            throw r0
        L_0x0056:
            r2 = 58
            if (r1 == r2) goto L_0x003a
            java.lang.String r0 = "Expected a ':' after a key"
            com.windo.a.a.a.a r0 = r4.a(r0)
            throw r0
        L_0x0061:
            char r0 = r4.c()
            r1 = 125(0x7d, float:1.75E-43)
            if (r0 != r1) goto L_0x0012
        L_0x0069:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.windo.a.a.a.d.<init>(com.windo.a.a.a.b):void");
    }

    public d(String str) {
        this(new b(str));
    }

    static String a(Object obj) {
        if (obj == null || obj.equals(null)) {
            return "null";
        }
        if (!(obj instanceof Byte) && !(obj instanceof Short) && !(obj instanceof Integer) && !(obj instanceof Long)) {
            return ((obj instanceof Boolean) || (obj instanceof d) || (obj instanceof e)) ? obj.toString() : h(obj.toString());
        }
        if (obj == null) {
            throw new a("Null pointer");
        }
        String obj2 = obj.toString();
        if (obj2.indexOf(46) <= 0 || obj2.indexOf(101) >= 0 || obj2.indexOf(69) >= 0) {
            return obj2;
        }
        while (obj2.endsWith("0")) {
            obj2 = obj2.substring(0, obj2.length() - 1);
        }
        return obj2.endsWith(".") ? obj2.substring(0, obj2.length() - 1) : obj2;
    }

    private long f(String str) {
        Object a2 = a(str);
        if (a2 instanceof Byte) {
            return (long) ((Byte) a2).byteValue();
        }
        if (a2 instanceof Short) {
            return (long) ((Short) a2).shortValue();
        }
        if (a2 instanceof Integer) {
            return (long) ((Integer) a2).intValue();
        }
        if (a2 instanceof Long) {
            return ((Long) a2).longValue();
        }
        if (a2 instanceof String) {
            try {
                return Long.parseLong((String) a2);
            } catch (Exception e) {
                throw new a("JSONObject[" + h(str) + "] is not a number.");
            }
        } else {
            throw new a("JSONObject[" + h(str) + "] is not a number.");
        }
    }

    private Object g(String str) {
        if (str == null) {
            return null;
        }
        Object obj = this.b.get(str);
        if (obj == a) {
            return null;
        }
        return obj;
    }

    private static String h(String str) {
        char c = 0;
        if (str == null || str.length() == 0) {
            return "\"\"";
        }
        int length = str.length();
        StringBuffer stringBuffer = new StringBuffer(length + 4);
        stringBuffer.append('\"');
        int i = 0;
        while (i < length) {
            char charAt = str.charAt(i);
            switch (charAt) {
                case 8:
                    stringBuffer.append("\\b");
                    break;
                case 9:
                    stringBuffer.append("\\t");
                    break;
                case 10:
                    stringBuffer.append("\\n");
                    break;
                case 12:
                    stringBuffer.append("\\f");
                    break;
                case 13:
                    stringBuffer.append("\\r");
                    break;
                case '\"':
                case '\\':
                    stringBuffer.append('\\');
                    stringBuffer.append(charAt);
                    break;
                case '/':
                    if (c == '<') {
                        stringBuffer.append('\\');
                    }
                    stringBuffer.append(charAt);
                    break;
                default:
                    if (charAt >= ' ') {
                        stringBuffer.append(charAt);
                        break;
                    } else {
                        String str2 = "000" + Integer.toHexString(charAt);
                        stringBuffer.append("\\u" + str2.substring(str2.length() - 4));
                        break;
                    }
            }
            i++;
            c = charAt;
        }
        stringBuffer.append('\"');
        return stringBuffer.toString();
    }

    public final int a(String str, int i) {
        try {
            return b(str);
        } catch (Exception e) {
            return i;
        }
    }

    public final d a(String str, Object obj) {
        if (str == null) {
            throw new a("Null key.");
        }
        if (obj != null) {
            this.b.put(str, obj);
        } else {
            this.b.remove(str);
        }
        return this;
    }

    public final d a(String str, boolean z) {
        a(str, z ? Boolean.TRUE : Boolean.FALSE);
        return this;
    }

    public final Object a(String str) {
        Object g = g(str);
        if (g != null) {
            return g;
        }
        throw new a("JSONObject[" + h(str) + "] not found.");
    }

    public final String a(String str, String str2) {
        Object g = g(str);
        return g != null ? g.toString() : str2;
    }

    public final int b(String str) {
        Object a2 = a(str);
        if (a2 instanceof Byte) {
            return ((Byte) a2).byteValue();
        }
        if (a2 instanceof Short) {
            return ((Short) a2).shortValue();
        }
        if (a2 instanceof Integer) {
            return ((Integer) a2).intValue();
        }
        if (a2 instanceof Long) {
            return (int) ((Long) a2).longValue();
        }
        if (a2 instanceof String) {
            return (int) f(str);
        }
        throw new a("JSONObject[" + h(str) + "] is not a number.");
    }

    public final d b(String str, int i) {
        a(str, new Integer(i));
        return this;
    }

    public final String c(String str) {
        return a(str).toString();
    }

    public final int d(String str) {
        return a(str, 0);
    }

    public final String e(String str) {
        return a(str, "");
    }

    public final String toString() {
        try {
            Enumeration keys = this.b.keys();
            StringBuffer stringBuffer = new StringBuffer("{");
            while (keys.hasMoreElements()) {
                if (stringBuffer.length() > 1) {
                    stringBuffer.append(',');
                }
                Object nextElement = keys.nextElement();
                stringBuffer.append(h(nextElement.toString()));
                stringBuffer.append(':');
                stringBuffer.append(a(this.b.get(nextElement)));
            }
            stringBuffer.append('}');
            return stringBuffer.toString();
        } catch (Exception e) {
            return null;
        }
    }
}
