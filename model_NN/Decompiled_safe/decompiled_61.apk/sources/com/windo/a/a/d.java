package com.windo.a.a;

import java.util.Vector;

public final class d {
    private Vector a = new Vector();

    private Object c() {
        synchronized (this.a) {
            if (this.a.size() <= 0) {
                return null;
            }
            Object elementAt = this.a.elementAt(0);
            this.a.removeElementAt(0);
            return elementAt;
        }
    }

    public final Object a() {
        synchronized (this.a) {
            if (this.a.size() > 0) {
                Object c = c();
                return c;
            }
            try {
                this.a.wait();
            } catch (Exception e) {
            }
            Object c2 = c();
            return c2;
        }
    }

    public final void a(Object obj) {
        if (obj != null) {
            synchronized (this.a) {
                this.a.addElement(obj);
                this.a.notifyAll();
            }
        }
    }

    public final void b() {
        synchronized (this.a) {
            this.a.notifyAll();
        }
    }

    public final boolean b(Object obj) {
        boolean removeElement;
        synchronized (this.a) {
            removeElement = this.a.removeElement(obj);
        }
        return removeElement;
    }
}
