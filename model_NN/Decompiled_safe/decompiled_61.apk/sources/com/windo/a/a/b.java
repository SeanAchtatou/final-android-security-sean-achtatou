package com.windo.a.a;

import java.util.Hashtable;

final class b {
    private static final char[] a = "<br".toCharArray();
    private static final char[] b = "<p".toCharArray();
    private static final char[] c = "<li".toCharArray();
    private static final char[] d = "<pre".toCharArray();
    private static final char[] e = "<hr".toCharArray();
    private static final char[] f = "</td>".toCharArray();
    private static final char[] g = "</tr>".toCharArray();
    private static final char[] h = "</li>".toCharArray();
    private static final Hashtable i;
    private int j;
    private String k;
    private String l = null;
    private int m = 0;
    private boolean n = false;

    static {
        Hashtable hashtable = new Hashtable();
        i = hashtable;
        hashtable.put("<", "<");
        i.put(">", ">");
        i.put("&", "&");
        i.put("?", "(r)");
        i.put("?", "(c)");
        i.put(" ", " ");
        i.put("￡", "?");
    }

    public b(int i2, char[] cArr, int i3, int i4, boolean z) {
        this.j = i2;
        this.m = i4 - i3;
        this.k = new String(cArr, i3, this.m);
        if (this.j == 2) {
            char[] charArray = this.k.toCharArray();
            if (a(a, charArray) || a(b, charArray)) {
                this.l = "\n";
            } else if (a(c, charArray)) {
                this.l = "\n* ";
            } else if (a(d, charArray)) {
                this.n = true;
            } else if (a(e, charArray)) {
                this.l = "\n--------\n";
            } else if (b(f, charArray)) {
                this.l = "\t";
            } else if (b(g, charArray) || b(h, charArray)) {
                this.l = "\n";
            }
        } else if (this.j == 0) {
            this.l = a(this.k, z);
        }
    }

    private static int a(char[] cArr, int i2) {
        int i3 = i2 + 10;
        if (i3 > cArr.length) {
            i3 = cArr.length;
        }
        for (int i4 = i2; i4 < i3; i4++) {
            if (cArr[i4] == ';') {
                return (i4 - i2) + 1;
            }
        }
        return -1;
    }

    private static String a(String str, boolean z) {
        char[] charArray = str.toCharArray();
        int i2 = 0;
        StringBuffer stringBuffer = new StringBuffer(charArray.length);
        boolean z2 = false;
        while (i2 < charArray.length) {
            char c2 = charArray[i2];
            char c3 = i2 + 1 < charArray.length ? charArray[i2 + 1] : 0;
            if (c2 == ' ') {
                i2++;
                stringBuffer = (z || !z2) ? stringBuffer.append(' ') : stringBuffer;
                z2 = true;
            } else if (c2 == 13 && c3 == 10) {
                if (z) {
                    stringBuffer = stringBuffer.append(10);
                }
                i2 += 2;
            } else if (c2 == 10 || c2 == 13) {
                if (z) {
                    stringBuffer = stringBuffer.append(10);
                }
                i2++;
            } else if (c2 == '&') {
                int a2 = a(charArray, i2);
                if (a2 == -1) {
                    i2++;
                    stringBuffer = stringBuffer.append('&');
                    z2 = false;
                } else {
                    String str2 = (String) i.get(new String(charArray, i2, a2));
                    if (str2 != null) {
                        stringBuffer = stringBuffer.append(str2);
                        i2 = a2 + i2;
                        z2 = false;
                    } else if (c3 == '#') {
                        try {
                            int parseInt = Integer.parseInt(new String(charArray, i2 + 2, a2 - 3));
                            if (parseInt > 0 && parseInt < 65536) {
                                i2++;
                                stringBuffer = stringBuffer.append((char) parseInt);
                                z2 = false;
                            }
                        } catch (Exception e2) {
                            e2.printStackTrace();
                        }
                        i2 += 2;
                        stringBuffer = stringBuffer.append("&#");
                        z2 = false;
                    } else {
                        i2++;
                        stringBuffer = stringBuffer.append('&');
                        z2 = false;
                    }
                }
            } else {
                i2++;
                stringBuffer = stringBuffer.append(c2);
                z2 = false;
            }
        }
        return c.b(stringBuffer.toString());
    }

    private static boolean a(char[] cArr, char[] cArr2) {
        if (cArr.length >= cArr2.length) {
            return false;
        }
        for (int i2 = 0; i2 < cArr.length; i2++) {
            if (Character.toLowerCase(cArr2[i2]) != cArr[i2]) {
                return false;
            }
        }
        if (cArr2.length <= cArr.length) {
            return true;
        }
        char lowerCase = Character.toLowerCase(cArr2[cArr.length]);
        return lowerCase < 'a' || lowerCase > 'z';
    }

    private static boolean b(char[] cArr, char[] cArr2) {
        if (cArr.length > cArr2.length) {
            return false;
        }
        for (int i2 = 0; i2 < cArr.length; i2++) {
            if (Character.toLowerCase(cArr2[i2]) != cArr[i2]) {
                return false;
            }
        }
        return true;
    }

    public final int a() {
        return this.m;
    }

    public final boolean b() {
        return this.n;
    }

    public final String c() {
        return this.l == null ? "" : this.l;
    }

    public final String toString() {
        return this.k;
    }
}
