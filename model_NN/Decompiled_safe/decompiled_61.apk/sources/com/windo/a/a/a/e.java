package com.windo.a.a.a;

import java.util.Vector;

public final class e {
    private Vector a;

    public e() {
        this.a = new Vector();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x004a  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x001a A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0036 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0026  */
    public e(com.windo.a.a.a.b r4) {
        /*
            r3 = this;
            r2 = 93
            r3.<init>()
            char r0 = r4.c()
            r1 = 91
            if (r0 == r1) goto L_0x0014
            java.lang.String r0 = "A JSONArray text must start with '['"
            com.windo.a.a.a.a r0 = r4.a(r0)
            throw r0
        L_0x0014:
            char r0 = r4.c()
            if (r0 != r2) goto L_0x001b
        L_0x001a:
            return
        L_0x001b:
            r4.a()
        L_0x001e:
            char r0 = r4.c()
            r1 = 44
            if (r0 != r1) goto L_0x003d
            r4.a()
            java.util.Vector r0 = r3.a
            r1 = 0
            r0.addElement(r1)
        L_0x002f:
            char r0 = r4.c()
            switch(r0) {
                case 44: goto L_0x004a;
                case 59: goto L_0x004a;
                case 93: goto L_0x001a;
                default: goto L_0x0036;
            }
        L_0x0036:
            java.lang.String r0 = "Expected a ',' or ']'"
            com.windo.a.a.a.a r0 = r4.a(r0)
            throw r0
        L_0x003d:
            r4.a()
            java.util.Vector r0 = r3.a
            java.lang.Object r1 = r4.d()
            r0.addElement(r1)
            goto L_0x002f
        L_0x004a:
            char r0 = r4.c()
            if (r0 == r2) goto L_0x001a
            r4.a()
            goto L_0x001e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.windo.a.a.a.e.<init>(com.windo.a.a.a.b):void");
    }

    public e(String str) {
        this(new b(str));
    }

    private String a(String str) {
        int size = this.a.size();
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < size; i++) {
            if (i > 0) {
                stringBuffer.append(str);
            }
            stringBuffer.append(d.a(this.a.elementAt(i)));
        }
        return stringBuffer.toString();
    }

    public final int a() {
        return this.a.size();
    }

    public final Object a(int i) {
        Object elementAt = (i < 0 || i >= this.a.size()) ? null : this.a.elementAt(i);
        if (elementAt != null && elementAt != d.a) {
            return elementAt;
        }
        throw new a("JSONArray[" + i + "] not found.");
    }

    public final d b(int i) {
        Object a2 = a(i);
        if (a2 instanceof d) {
            return (d) a2;
        }
        throw new a("JSONArray[" + i + "] is not a JSONObject.");
    }

    public final String toString() {
        try {
            return '[' + a(",") + ']';
        } catch (Exception e) {
            return null;
        }
    }
}
