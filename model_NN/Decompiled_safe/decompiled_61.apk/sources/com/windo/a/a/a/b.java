package com.windo.a.a.a;

public final class b {
    private int a = 0;
    private String b;

    public b(String str) {
        this.b = str;
    }

    private String a(int i) {
        int i2 = this.a;
        int i3 = i2 + i;
        if (i3 >= this.b.length()) {
            throw a("Substring bounds error");
        }
        this.a += i;
        return this.b.substring(i2, i3);
    }

    public final a a(String str) {
        return new a(str + toString());
    }

    public final void a() {
        if (this.a > 0) {
            this.a--;
        }
    }

    public final char b() {
        if (!(this.a < this.b.length())) {
            return 0;
        }
        char charAt = this.b.charAt(this.a);
        this.a++;
        return charAt;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: CFG modification limit reached, blocks count: 141 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final char c() {
        /*
            r5 = this;
            r4 = 13
            r3 = 10
            r2 = 47
        L_0x0006:
            char r0 = r5.b()
            if (r0 != r2) goto L_0x003d
            char r0 = r5.b()
            switch(r0) {
                case 42: goto L_0x0030;
                case 47: goto L_0x0018;
                default: goto L_0x0013;
            }
        L_0x0013:
            r5.a()
            r0 = r2
        L_0x0017:
            return r0
        L_0x0018:
            char r0 = r5.b()
            if (r0 == r3) goto L_0x0006
            if (r0 == r4) goto L_0x0006
            if (r0 != 0) goto L_0x0018
            goto L_0x0006
        L_0x0023:
            r1 = 42
            if (r0 != r1) goto L_0x0030
            char r0 = r5.b()
            if (r0 == r2) goto L_0x0006
            r5.a()
        L_0x0030:
            char r0 = r5.b()
            if (r0 != 0) goto L_0x0023
            java.lang.String r0 = "Unclosed comment."
            com.windo.a.a.a.a r0 = r5.a(r0)
            throw r0
        L_0x003d:
            r1 = 35
            if (r0 != r1) goto L_0x004c
        L_0x0041:
            char r0 = r5.b()
            if (r0 == r3) goto L_0x0006
            if (r0 == r4) goto L_0x0006
            if (r0 != 0) goto L_0x0041
            goto L_0x0006
        L_0x004c:
            if (r0 == 0) goto L_0x0017
            r1 = 32
            if (r0 <= r1) goto L_0x0006
            goto L_0x0017
        */
        throw new UnsupportedOperationException("Method not decompiled: com.windo.a.a.a.b.c():char");
    }

    public final Object d() {
        char c = c();
        switch (c) {
            case '\"':
            case '\'':
                StringBuffer stringBuffer = new StringBuffer();
                while (true) {
                    char b2 = b();
                    switch (b2) {
                        case 0:
                        case 10:
                        case 13:
                            throw a("Unterminated string");
                        case '\\':
                            char b3 = b();
                            switch (b3) {
                                case 'b':
                                    stringBuffer.append(8);
                                    continue;
                                case 'f':
                                    stringBuffer.append(12);
                                    continue;
                                case 'n':
                                    stringBuffer.append(10);
                                    continue;
                                case 'r':
                                    stringBuffer.append(13);
                                    continue;
                                case 't':
                                    stringBuffer.append(9);
                                    continue;
                                case 'u':
                                    stringBuffer.append((char) Integer.parseInt(a(4), 16));
                                    continue;
                                case 'x':
                                    stringBuffer.append((char) Integer.parseInt(a(2), 16));
                                    continue;
                                default:
                                    stringBuffer.append(b3);
                                    continue;
                            }
                        default:
                            if (b2 != c) {
                                stringBuffer.append(b2);
                                break;
                            } else {
                                return stringBuffer.toString();
                            }
                    }
                }
            case '[':
                a();
                return new e(this);
            case '{':
                a();
                return new d(this);
            default:
                StringBuffer stringBuffer2 = new StringBuffer();
                char c2 = c;
                while (c2 >= ' ' && ",:]}/\\\"[{;=#".indexOf(c2) < 0) {
                    stringBuffer2.append(c2);
                    c2 = b();
                }
                a();
                String trim = stringBuffer2.toString().trim();
                trim.toLowerCase();
                if (trim.equals("")) {
                    throw a("Missing value.");
                } else if (trim.equals("true")) {
                    return Boolean.TRUE;
                } else {
                    if (trim.equals("false")) {
                        return Boolean.FALSE;
                    }
                    if (trim.equals("null")) {
                        return d.a;
                    }
                    if ((c < '0' || c > '9') && c != '.' && c != '-' && c != '+') {
                        return trim;
                    }
                    if (c == '0') {
                        if (trim.length() <= 2 || !(trim.charAt(1) == 'x' || trim.charAt(1) == 'X')) {
                            try {
                                return new Integer(Integer.parseInt(trim, 8));
                            } catch (Exception e) {
                            }
                        } else {
                            try {
                                return new Integer(Integer.parseInt(trim.substring(2), 16));
                            } catch (Exception e2) {
                            }
                        }
                    }
                    try {
                        return Integer.valueOf(trim);
                    } catch (Exception e3) {
                        try {
                            return new Long(Long.parseLong(trim));
                        } catch (Exception e4) {
                            return trim;
                        }
                    }
                }
        }
    }

    public final String toString() {
        return " at character " + this.a + " of " + this.b;
    }
}
