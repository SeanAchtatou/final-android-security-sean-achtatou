package com.windo.a.a;

import java.util.Enumeration;
import java.util.Hashtable;

public final class c {
    private static Hashtable a;

    static {
        Hashtable hashtable = new Hashtable();
        a = hashtable;
        hashtable.put("&lt;", "<");
        a.put("&gt;", ">");
        a.put("&amp;", "&");
        a.put("&ldquo;", "\"");
        a.put("&rdquo;", "\"");
        a.put("&quot;", "\"");
        a.put("&nbsp;", " ");
        a.put("&emsp;", " ");
        a.put("&ensp;", " ");
        a.put("&times;", "×");
        a.put("&divide;", "÷");
        a.put("&copy;", "??");
        a.put("&reg;", "??");
        a.put("??", "??");
    }

    private static int a(char[] cArr, int i, char c) {
        for (int i2 = i; i2 < cArr.length; i2++) {
            if (cArr[i2] == c) {
                return i2;
            }
        }
        return -1;
    }

    private static int a(char[] cArr, int i, String str) {
        char[] charArray = str.toCharArray();
        for (int i2 = i; i2 < cArr.length - charArray.length; i2++) {
            boolean z = true;
            int i3 = 0;
            while (true) {
                if (i3 >= charArray.length) {
                    break;
                } else if (cArr[i2 + i3] != charArray[i3]) {
                    z = false;
                    break;
                } else {
                    i3++;
                }
            }
            if (z) {
                return i2;
            }
        }
        return -1;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.windo.a.a.c.a(char[], int, char):int
     arg types: [char[], int, int]
     candidates:
      com.windo.a.a.c.a(char[], int, java.lang.String):int
      com.windo.a.a.c.a(char[], int, char):int */
    public static String a(String str) {
        b bVar;
        StringBuffer stringBuffer = new StringBuffer(str.length());
        char[] charArray = str.toCharArray();
        boolean z = false;
        int i = 0;
        StringBuffer stringBuffer2 = stringBuffer;
        while (true) {
            if (i >= charArray.length) {
                bVar = null;
            } else if (charArray[i] == '<') {
                int a2 = a(charArray, i + 1, '>');
                if (a2 == -1) {
                    bVar = new b(0, charArray, i, charArray.length, z);
                } else {
                    String str2 = new String(charArray, i, (a2 - i) + 1);
                    if (str2.startsWith("<!--")) {
                        int a3 = a(charArray, i + 1, "-->");
                        bVar = a3 == -1 ? new b(1, charArray, i, charArray.length, z) : new b(1, charArray, i, a3 + 3, z);
                    } else {
                        String lowerCase = str2.toLowerCase();
                        if (lowerCase.startsWith("<style")) {
                            int a4 = a(charArray, i + 1, "</style>");
                            bVar = a4 == -1 ? new b(4, charArray, i, charArray.length, z) : new b(4, charArray, i, a4 + 9, z);
                        } else if (lowerCase.startsWith("<script")) {
                            int a5 = a(charArray, i + 1, "</script>");
                            bVar = a5 == -1 ? new b(3, charArray, i, charArray.length, z) : new b(3, charArray, i, a5 + 9, z);
                        } else {
                            bVar = new b(2, charArray, i, str2.length() + i, z);
                        }
                    }
                }
            } else {
                int a6 = a(charArray, i + 1, '<');
                bVar = a6 == -1 ? new b(0, charArray, i, charArray.length, z) : new b(0, charArray, i, a6, z);
            }
            if (bVar == null) {
                return stringBuffer2.toString();
            }
            boolean b = bVar.b();
            stringBuffer2.append(bVar.c());
            if (bVar.toString().startsWith("</p") || bVar.toString().startsWith("</br") || bVar.toString().startsWith("</div")) {
                stringBuffer2 = stringBuffer2.append("\n");
            }
            z = b;
            i = bVar.a() + i;
        }
    }

    public static String b(String str) {
        Enumeration keys = a.keys();
        String str2 = str;
        while (keys.hasMoreElements()) {
            String str3 = (String) keys.nextElement();
            String str4 = (String) a.get(str3);
            StringBuffer stringBuffer = new StringBuffer();
            int indexOf = str2.indexOf(str3, 0);
            int i = 0;
            while (indexOf >= 0) {
                stringBuffer.append(str2.substring(i, indexOf));
                stringBuffer.append(str4);
                int length = indexOf + str3.length();
                i = length;
                indexOf = str2.indexOf(str3, length);
            }
            if (i < str2.length()) {
                stringBuffer.append(str2.substring(i));
            }
            str2 = stringBuffer.toString();
        }
        return str2;
    }
}
