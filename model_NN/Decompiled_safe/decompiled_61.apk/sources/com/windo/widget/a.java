package com.windo.widget;

import android.content.Context;
import android.text.Spannable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class a {
    private static a e;
    private Pattern a = Pattern.compile("\\[(\\S+?)\\]");
    private LinkedHashMap b;
    private List c;
    private Context d;

    private a(Context context) {
        this.d = context;
        this.b = new LinkedHashMap();
        this.c = new Vector();
        for (int i = 1; i <= 33; i++) {
            String str = "f" + i;
            if (i < 10) {
                str = "f0" + i;
            }
            String str2 = "[" + str.toUpperCase() + "]";
            this.c.add(str2);
            this.b.put(str2, Integer.valueOf(context.getResources().getIdentifier(str, "drawable", context.getPackageName())));
        }
    }

    public static a a(Context context) {
        if (e == null) {
            e = new a(context);
        }
        return e;
    }

    public final int a() {
        return this.c.size();
    }

    public final int a(Spannable spannable) {
        return b(spannable);
    }

    public final String a(int i) {
        return (String) this.c.get(i);
    }

    public final int b(int i) {
        return ((Integer) this.b.get(a(i))).intValue();
    }

    public final int b(Spannable spannable) {
        int i;
        int intValue;
        if (spannable == null || spannable.length() < 2) {
            return 0;
        }
        Matcher matcher = this.a.matcher(spannable);
        for (b removeSpan : (b[]) spannable.getSpans(0, spannable.length(), b.class)) {
            spannable.removeSpan(removeSpan);
        }
        int i2 = 0;
        while (matcher.find()) {
            int start = matcher.start();
            int end = matcher.end();
            if (start < end) {
                String obj = spannable.subSequence(start, end).toString();
                if (this.b.containsKey(obj) && (intValue = ((Integer) this.b.get(obj)).intValue()) != 0) {
                    spannable.setSpan(new b(this, this.d, intValue), start, end, 33);
                    i = i2 + 1;
                    i2 = i;
                }
            }
            i = i2;
            i2 = i;
        }
        return i2;
    }
}
