package com.windo.widget;

import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;
import com.vodone.caibo.R;

final class d extends ClickableSpan {
    private String a;
    private byte b;
    private /* synthetic */ RichTextView c;

    public d(RichTextView richTextView, String str, byte b2) {
        this.c = richTextView;
        this.a = str;
        this.b = b2;
    }

    public final void onClick(View view) {
        if (this.c.a != null) {
            this.c.a.a(this.a, this.b);
        }
    }

    public final void updateDrawState(TextPaint textPaint) {
        textPaint.setColor(this.c.getResources().getColor(R.color.rich_text));
        if (this.c.f) {
            textPaint.setUnderlineText(true);
        } else {
            textPaint.setUnderlineText(false);
        }
    }
}
