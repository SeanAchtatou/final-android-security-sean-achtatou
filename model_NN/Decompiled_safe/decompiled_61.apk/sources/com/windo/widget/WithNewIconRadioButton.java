package com.windo.widget;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.RadioButton;
import com.vodone.caibo.R;

public class WithNewIconRadioButton extends RadioButton {
    private int a = -1;
    private Bitmap b;
    private Bitmap c;
    private Paint d;
    private String e;
    private int f = 0;

    public WithNewIconRadioButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Resources resources = context.getResources();
        this.b = BitmapFactory.decodeResource(resources, R.drawable.new_icon);
        this.c = BitmapFactory.decodeResource(resources, R.drawable.number_icon);
    }

    private Paint b() {
        if (this.d == null) {
            this.d = new Paint();
            this.d.setColor(-1);
            this.d.setTypeface(Typeface.DEFAULT_BOLD);
        }
        return this.d;
    }

    public final void a() {
        this.a = 0;
        invalidate();
    }

    public final void a(int i) {
        if (i <= 0) {
            this.e = null;
        } else {
            this.e = "" + i;
        }
        invalidate();
    }

    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.e != null) {
            int scrollX = getScrollX();
            int scrollY = getScrollY();
            canvas.save();
            canvas.translate((float) scrollX, (float) scrollY);
            if (this.a == 0) {
                canvas.drawBitmap(this.b, (float) (getWidth() - this.b.getWidth()), 0.0f, (Paint) null);
            } else {
                int width = getWidth() - this.b.getWidth();
                canvas.drawBitmap(this.c, (float) width, 0.0f, (Paint) null);
                Paint.FontMetrics fontMetrics = b().getFontMetrics();
                float[] fArr = new float[this.e.length()];
                b().getTextWidths(this.e, fArr);
                this.f = 0;
                for (int i = 0; i < fArr.length; i++) {
                    this.f += (int) fArr[0];
                }
                int i2 = (int) (fontMetrics.leading - fontMetrics.top);
                canvas.drawText(this.e, (float) (width + ((this.c.getWidth() - this.f) >> 1)), (float) (i2 + ((this.c.getHeight() - i2) >> 1)), b());
            }
            canvas.restore();
        }
    }
}
