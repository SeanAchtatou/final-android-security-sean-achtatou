package com.windo.widget;

import android.content.Context;
import android.text.Html;
import android.text.Spannable;
import android.util.AttributeSet;
import android.widget.TextView;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RichTextView extends TextView {
    private static Pattern b = Pattern.compile("@([一-龥_a-zA-Z0-9]+)");
    private static Pattern c = Pattern.compile("[#＃]([^#＃\\s]+)[#＃\\s]?");
    private static Pattern d = Pattern.compile("http://[A-Za-z0-9]+(\\.[A-Za-z0-9]+)+(/([A-Za-z0-9]|[\\.\\+\\*\\(\\(\\?\\$\\-,;:@&=/~#%_!])*)*");
    private static Pattern e = Pattern.compile("http://126.fm/[A-Za-z0-9]+");
    public c a;
    /* access modifiers changed from: private */
    public boolean f = false;

    public RichTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    private void a(Pattern pattern, Spannable spannable, byte b2) {
        Matcher matcher = pattern.matcher(spannable);
        while (matcher.find()) {
            int start = matcher.start();
            int end = matcher.end();
            if (start < end) {
                spannable.setSpan(new d(this, spannable.subSequence(start, end).toString(), b2), start, end, 33);
            }
        }
    }

    public final void a(c cVar) {
        this.a = cVar;
    }

    public final void a(boolean z) {
        this.f = z;
        if (this.f) {
            setMovementMethod(e.a());
            setFocusable(true);
            setClickable(true);
            setLongClickable(true);
            return;
        }
        setMovementMethod(null);
        setFocusable(false);
        setClickable(false);
        setLongClickable(false);
    }

    public void setText(CharSequence charSequence, TextView.BufferType bufferType) {
        Spannable spannable = (Spannable) Html.fromHtml((charSequence == null ? "" : charSequence).toString());
        a(b, spannable, (byte) 1);
        a(c, spannable, (byte) 2);
        a(d, spannable, (byte) 3);
        a.a(getContext()).a(spannable);
        super.setText(spannable, TextView.BufferType.SPANNABLE);
    }
}
