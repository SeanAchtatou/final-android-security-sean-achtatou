package com.windo.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.ImageView;
import com.vodone.caibo.R;

public class WithNewIconButton extends ImageView {
    private String a;
    private Bitmap b;
    private Paint c;

    public WithNewIconButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.b = BitmapFactory.decodeResource(context.getResources(), R.drawable.number_icon);
    }

    private Paint a() {
        if (this.c == null) {
            this.c = new Paint();
            this.c.setColor(-1);
            this.c.setTypeface(Typeface.DEFAULT_BOLD);
        }
        return this.c;
    }

    public final void a(int i) {
        if (i > 0) {
            this.a = "" + i;
        } else {
            this.a = null;
        }
        invalidate();
    }

    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.a != null) {
            int scrollX = getScrollX();
            int scrollY = getScrollY();
            canvas.save();
            canvas.translate((float) scrollX, (float) scrollY);
            int width = getWidth() - this.b.getWidth();
            canvas.drawBitmap(this.b, (float) width, 0.0f, (Paint) null);
            Paint.FontMetrics fontMetrics = a().getFontMetrics();
            float[] fArr = new float[this.a.length()];
            a().getTextWidths(this.a, fArr);
            int i = 0;
            for (int i2 = 0; i2 < fArr.length; i2++) {
                i += (int) fArr[0];
            }
            int i3 = (int) (fontMetrics.leading - fontMetrics.top);
            canvas.drawText(this.a, (float) (width + ((this.b.getWidth() - i) >> 1)), (float) (i3 + ((this.b.getHeight() - i3) >> 1)), a());
            canvas.restore();
        }
    }
}
