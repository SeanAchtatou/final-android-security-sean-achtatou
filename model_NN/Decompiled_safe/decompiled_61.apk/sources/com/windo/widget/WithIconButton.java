package com.windo.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.Button;

public class WithIconButton extends Button {
    private int a = -1;
    private Drawable b = null;
    private int c = 0;
    private int d = 0;
    private Matrix e;

    public WithIconButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public WithIconButton(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public final void a(int i) {
        this.a = i;
        a(getContext().getResources().getDrawable(i));
    }

    public final void a(Drawable drawable) {
        if (this.b != drawable) {
            if (this.b != null) {
                this.b.setCallback(null);
                unscheduleDrawable(this.b);
            }
            this.b = drawable;
            if (drawable != null) {
                drawable.setCallback(this);
                if (drawable.isStateful()) {
                    drawable.setState(getDrawableState());
                }
                this.c = drawable.getIntrinsicWidth();
                this.d = drawable.getIntrinsicHeight();
            }
            requestLayout();
            invalidate();
        }
    }

    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.b != null && this.c != 0 && this.d != 0) {
            int paddingTop = getPaddingTop();
            int paddingLeft = getPaddingLeft();
            if (this.e == null && paddingTop == 0 && paddingLeft == 0) {
                this.b.draw(canvas);
                return;
            }
            int saveCount = canvas.getSaveCount();
            canvas.save();
            canvas.translate((float) paddingLeft, (float) paddingTop);
            if (this.e != null) {
                canvas.concat(this.e);
            }
            this.b.draw(canvas);
            canvas.restoreToCount(saveCount);
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        if (this.b != null) {
            int i5 = this.c;
            int i6 = this.d;
            this.b.setBounds(0, 0, i5, i6);
            int width = (getWidth() - getPaddingLeft()) - getPaddingRight();
            int height = (getHeight() - getPaddingTop()) - getPaddingBottom();
            if (i5 <= 0 || i6 <= 0) {
                this.b.setBounds(0, 0, width, height);
                this.e = null;
                return;
            }
            this.b.setBounds(0, 0, i5, i6);
            if (this.e == null) {
                this.e = new Matrix();
            } else {
                this.e.reset();
            }
            this.e.setTranslate(((float) (width - i5)) * 0.5f, ((float) (height - i6)) * 0.5f);
        }
    }
}
