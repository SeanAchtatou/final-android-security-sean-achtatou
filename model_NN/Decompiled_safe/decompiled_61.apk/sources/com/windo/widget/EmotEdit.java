package com.windo.widget;

import android.content.Context;
import android.text.Selection;
import android.text.SpannableStringBuilder;
import android.util.AttributeSet;
import android.widget.EditText;

public class EmotEdit extends EditText {
    private boolean a = false;

    public EmotEdit(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public EmotEdit(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.text.SpannableStringBuilder.replace(int, int, java.lang.CharSequence):android.text.SpannableStringBuilder}
     arg types: [int, int, android.text.SpannableStringBuilder]
     candidates:
      ClspMth{android.text.SpannableStringBuilder.replace(int, int, java.lang.CharSequence):android.text.Editable}
      ClspMth{android.text.SpannableStringBuilder.replace(int, int, java.lang.CharSequence):android.text.SpannableStringBuilder} */
    /* access modifiers changed from: protected */
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        super.onTextChanged(charSequence, i, i2, i3);
        if (!this.a) {
            SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(charSequence.toString());
            int[] iArr = {getSelectionStart(), getSelectionEnd()};
            if (a.a(getContext()).b(spannableStringBuilder) > 0) {
                this.a = true;
                if (charSequence instanceof SpannableStringBuilder) {
                    SpannableStringBuilder spannableStringBuilder2 = (SpannableStringBuilder) charSequence;
                    spannableStringBuilder2.clear();
                    spannableStringBuilder2.replace(0, charSequence.length(), (CharSequence) spannableStringBuilder);
                    Selection.setSelection(spannableStringBuilder2, iArr[0], iArr[1]);
                } else {
                    setText(spannableStringBuilder);
                    setSelection(iArr[0], iArr[1]);
                }
                this.a = false;
            }
        }
    }
}
