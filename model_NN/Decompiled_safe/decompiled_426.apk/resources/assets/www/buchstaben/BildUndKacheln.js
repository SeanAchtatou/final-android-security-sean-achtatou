dojo.provide("buchstaben.BildUndKacheln");

dojo
		.declare(
				"buchstaben.BildUndKacheln",
				null,
				{

					constructor : function(args) {
						this.main = args.main;
						console.debug("Drin");
					},

					init : function() {
						this.model = this.main.load("model.json");
						this.container = dojo.byId("tileContainerStartletter");
						this.imageStrip = dojo.byId("imageStrip");
						this.input = dojo.byId("input");
						var self = this;
						dojo.connect(this.input, "onkeydown", this, function(
								evt) {
							self.handleKeyEvent(evt);
						});
						// var snd = new Audio("buchstaben/Techno.mp3");
						// snd.loop = true;
						// snd.play();

						this.cycle();
					},

					handleKeyEvent : function(event) {
						if (event.keyCode == dojo.keys.ENTER) {
							dojo.stopEvent(event);
							this.cycle();
						}
					},

					cycle : function() {
						// clear field
						while (this.imageStrip.firstChild) {
							this.imageStrip
									.removeChild(this.imageStrip.firstChild);
						}
						var category = "Tiere";
						var names = this.model[category];
						var index = Math.floor(Math.random() * names.length);
						var name = names[index];
						this.result = name;
						var image = this.main.getImage(category, name);
						this.imageStrip.appendChild(image);

						this.showKeys(name);

					},

					showKeys : function(name) {

						this.main.removeAllTiles(this.container);

						var chars = this.getDifferentChars(4);

						var styles = [ "TL", "TR", "BL", "BR" ];
						for ( var i in styles) {
							var tile = this.createTile(chars[i], "tile"
									+ styles[i]);
							if (i == 2) {
								tile.style.clear = "both";
							}
						}
					},

					getDifferentChars : function(size) {
						var name = this.result;
						var chars = [];
						var correctChar = name.substr(0, 1);

						// create array of different chars
						var set = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
						for ( var i = 0; chars.length < size; i++) {
							var index = Math.floor(Math.random() * 52);
							var char = set.substring(index, index + 1);
							if (chars.indexOf(char.toUpperCase()) == -1
									&& chars.indexOf(char.toLowerCase()) == -1
									&& char.toLowerCase() != correctChar
											.toLowerCase()) {
								chars.push(char);
							}
						}

						// set the correct char in a field
						var index = Math.floor(Math.random() * 4);
						chars[index] = correctChar;
						// switch between upper/lower for the correct char
						if (Math.random() > 0.5) {
							chars[index] = correctChar.toLowerCase();
						}
						return chars;
					},

					createTile : function(text, className, colorSet) {
						var container = this.container;
						if (!colorSet) {
							colorSet = this.makeRandomColor();
						}
						if (className) {
							className = "tile " + className;
						} else {
							className = "tile";
						}
						var tile = dojo.create("DIV", {
							className : className,
							style : "background-color: " + colorSet.color
						}, container);
						var textBox = dojo.create("DIV", {
							className : "tileText",
							innerHTML : text
						}, tile);
						this.setEventsOnTile(text, tile, colorSet);
						return tile;
					},

					setEventsOnTile : function(char, tile, colorSet) {
						var self = this;
						dojo.connect(tile, "onmouseover", function() {
							tile.style.backgroundColor = colorSet.colorHover
						});
						dojo.connect(tile, "onmouseout", function() {
							tile.style.backgroundColor = colorSet.color
						});
						dojo.connect(tile, "onmousedown", function() {
							tile.style.backgroundColor = colorSet.colorClick
						});
						dojo.connect(tile, "onclick", function() {
							self.tileClicked(char, tile);
						});
					},

					tileClicked : function(char, tile) {
						var name = this.result;
						if (name.substr(0, 1).toLowerCase() == char
								.toLowerCase()) {

							var container = tile.parentNode;
							var children = container.childNodes;
							for ( var i = 0; i < children.length; i++) {
								this.makeTilesFix(children[i]);
								if (children[i] != tile) {
									children[i].style.opacity = 0;
								}
							}
							for ( var i = 0; i < children.length; i++) {
								children[i].style.position = "absolute";
							}

							dojo.addClass(tile, "showTile");
							tile.style.opacity = 0;
							var self = this;
							var snd = new Audio("buchstaben/Tusch.mp3");
							snd.play();
							setTimeout(function() {
								self.showResult(name);
							}, 2000);
						} else {
							tile.style.opacity = 0;
						}
					},

					showResult : function(name) {
						var container = this.container;
						dojo.create("DIV", {
							innerHTML : name,
							className : "result"
						}, container);
						var self = this;
						setTimeout(function() {
							container.removeChild(container.firstChild);
							self.cycle();
						}, 2000);
					},

					makeTilesFix : function(tile) {
						var height = tile.offsetHeight;
						var width = tile.offsetWidth;
						var x = tile.offsetLeft;
						var y = tile.offsetTop;

						tile.style.height = height + "px";
						tile.style.width = width + "px";
						tile.style.left = x + "px";
						tile.style.top = y + "px";
					},

					makeRandomColor : function() {
						var color = "rgb(";
						var colorHover = "rgb(";
						var colorClick = "rgb(";
						for ( var i = 0; i < 3; i++) {
							var random = Math.floor(Math.random() * 150) + 55;
							if (i > 0) {
								color += ",";
								colorHover += ",";
								colorClick += ",";
							}
							color += random;
							colorHover += (random + 30);
							colorClick += (random + 50);
						}
						color += ")";
						colorHover += ")";
						colorClick += ")";

						return {
							color : color,
							colorHover : colorHover,
							colorClick : colorClick
						};
					}
				});
