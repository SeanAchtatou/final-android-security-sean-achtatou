dojo.provide("buchstaben.Main");

//dojo.require("buchstaben.BildUndKacheln");
dojo.require("buchstaben.Memory");

dojo
		.declare(
				"buchstaben.Main",
				null,
				{
					saymap: {
						A: "A",
						B: "B",
						C: "C",
						D: "D",
						E: "E",
						F: "F",
						G: "G",
						H: "H",
						I: "I",
						J: "J",
						K: "K",
						L: "L",
						M: "M",
						N: "N",
						O: "Jodd",
						P: "P",
						Q: "Q",
						R: "R",
						S: "S",
						T: "T",
						U: "U",
						V: "V",
						W: "W",
						X: "X",
						Y: "�psilon",
						Z: "Z"
					},
					
					wins: ["Super!", "Klasse!", "Ja toll!", "Hammer!", "Das war spitze!", "Toll!", "Spitze", "Weiter so!", "Gut gemacht!", "Suppi!", "Du kannst es!", "Klasse!", "Du hast wohl ge�bt!", "Jawoll!", "Bingo!"],
					
					constructor : function(args) {
					},

					init : function() {
						var self = this;
						this.memory = new buchstaben.Memory({main: self});
						dojo.connect(dijit.byId("memoryLetterCapitalMenu").domNode, "onclick", function() {
							self.memory.init("letterCapital");
						});
						dojo.connect(dijit.byId("memoryLetterMixedMenu").domNode, "onclick", function() {
							self.memory.init("letterMixed");
						});
						dojo.connect(dijit.byId("memoryLetterMatchMenu").domNode, "onclick", function() {
							self.memory.init("letterMatch");
						});
						dojo.connect(dijit.byId("memoryLetterBlindMenu").domNode, "onclick", function() {
							self.memory.init("letterBlind");
						});
						dojo.connect(dijit.byId("cartoonMemoryMenu").domNode, "onclick", function() {
							self.memory.init("cartoon");
						});
						dojo.connect(dijit.byId("bugsMemoryMenu").domNode, "onclick", function() {
							self.memory.init("bugs");
						});
						dojo.connect(dijit.byId("butterflyMemoryMenu").domNode, "onclick", function() {
							self.memory.init("butterflies");
						});
						dojo.connect(dijit.byId("flagsMemoryMenu").domNode, "onclick", function() {
							self.memory.init("flags");
						});
						dojo.connect(dijit.byId("facesMemoryMenu").domNode, "onclick", function() {
							self.memory.init("faces");
						});
						dojo.connect(dijit.byId("smileysMemoryMenu").domNode, "onclick", function() {
							self.memory.init("smileys");
						});
						dojo.connect(dijit.byId("stickmanMemoryMenu").domNode, "onclick", function() {
							self.memory.init("stickman");
						});
//						dojo.connect(dijit.byId("startletterMenu").domNode, "onclick", function() {
//							var spiel = new buchstaben.BildUndKacheln({main: self});
//							spiel.init();
//						});
						dojo.connect(dijit.byId("mathMemory20Menu").domNode, "onclick", function() {
							self.memory.init("math20");
						});
						dojo.connect(dijit.byId("mathMemory100Menu").domNode, "onclick", function() {
							self.memory.init("math100");
						});
						dojo.connect(dijit.byId("mathMemoryBlindMenu").domNode, "onclick", function() {
							self.memory.init("math100Blind");
						});
						dojo.connect(dijit.byId("mathIcon10MemoryMenu").domNode, "onclick", function() {
							self.memory.init("mathIcon10");
						});
						dojo.connect(dijit.byId("mathIcon20MemoryMenu").domNode, "onclick", function() {
							self.memory.init("mathIcon20");
						});
						dojo.connect(dijit.byId("math20MemoryMenu").domNode, "onclick", function() {
							self.memory.init("mathCalc20");
						});
						dojo.connect(dijit.byId("math100MemoryMenu").domNode, "onclick", function() {
							self.memory.init("mathCalc100");
						});
						dojo.connect(dijit.byId("math20SubMemoryMenu").domNode, "onclick", function() {
							self.memory.init("mathCalc20Sub");
						});
						dojo.connect(dijit.byId("math100SubMemoryMenu").domNode, "onclick", function() {
							self.memory.init("mathCalc100Sub");
						});
//						dojo.connect(dijit.byId("info").domNode, "onclick", function() {
//							aler(navigator.userAgent.toLowerCase());
//						});
						var agent = navigator.userAgent.toLowerCase();
						agent = "android";
						if (agent.search("android") > -1) {
							this.agent = "android";
							dojo.addClass(dojo.doc.body, "androidDevice");
						} else {
							this.agent = "iphone";
							dojo.addClass(dojo.doc.body, "iphoneDevice");
						}

					},
					
					addLang: function(loc, lang) {
						console.debug(loc + " " + lang);
					},

					changeLang: function(lang) {
				        window.plugins.tts.setLanguage(lang, this.ttsWin, this.ttsFail);
					},

					ttsWin: function(result) {
						console.debug(result);
					},
				    
				    ttsFail: function(result) {
				        console.debug("Error = " + result);
				    },
				    
				    speak: function(text) {
				    	if (window.plugins && window.plugins.tts) {
				    		if (this.saymap[text]) {
				    			text = this.saymap[text];
				    		}
					        window.plugins.tts.speak(text);
				    	} else {
				    		console.debug("Say: " + text);
				    	}
				    },
				    
				    vibrateLong: function() {
				    	if (this.phonegapUsed) {
					    	navigator.notification.vibrate(500);				    	
				    	}
				    },
				    
				    vibrate: function() {
				    	if (this.phonegapUsed) {
				    		navigator.notification.vibrate(150);
				    	}
				    },
				    
				    initPhoneGap: function() {
				    	this.phonegapUsed = true;
				    	this.prepareSounds();
				    },
				    
				    prepareSounds: function() {
				    	this.sounds = {
				    			swooshOpen1: this.createSound("buchstaben/swoosh.mp3"),
				    			swooshOpen2: this.createSound("buchstaben/swoosh.mp3"),
				    			swooshClose: this.createSound("buchstaben/swoosh.mp3"),
				    			fail: this.createSound("buchstaben/Buzzer.mp3"),
				    			win: this.createSound("buchstaben/Fanfare.wav"),
				    			tusch: this.createSound("buchstaben/Tusch2.mp3")
				    	}
				    },
				    
				    createSound: function(url) {
				    	if (this.phonegapUsed) {
					        url = "/android_asset/www/" + url;
					        var my_media = new Media(url,
						            function() {
					        			console.log("playAudio():Audio Success: "+err);
						            },
						            function(err) {
						                console.log("playAudio():Audio Error: "+err);
						        });
					        return my_media;
				    	} else {
					        url = url;
							 var snd = new Audio(url);
							 return snd;
				    	}
				    },
				    
				    playSwooshOpen: function(count) {
				    	if (!this.sounds) {
				    		this.prepareSounds();
				    	}
				    	if (this.swooshCount > 0) {
					    	this.sounds.swooshOpen2.play();
					    	this.swooshCount = 0;
				    	} else {
					    	this.sounds.swooshOpen1.play();
					    	this.swooshCount = 1;
				    	}
				    },
				    
				    playSwooshClose: function(count) {
				    	if (!this.sounds) {
				    		this.prepareSounds();
				    	}
				    	this.sounds.swooshClose.play();
				    },
				    
				    playFail: function() {
				    	if (!this.sounds) {
				    		this.prepareSounds();
				    	}
				    	//this.sounds.fail.play();
				    },
				    
				    playWin: function() {
				    	if (!this.sounds) {
				    		this.prepareSounds();
				    	}
				    	this.sounds.win.play();
				    	this.sayWin();
				    },
				    
				    sayWin: function() {
				    	var index = Math.floor(Math.random() * this.wins.length);
				    	this.speak(this.wins[index]);
				    },
				    
				    playTusch: function() {
				    	if (!this.sounds) {
				    		this.prepareSounds();
				    	}
				    	this.sounds.tusch.play();
				    }, 
				    
					load : function(name) {
						var text = dojo.trim(dojo._getText(name));
						return eval("(" + text + ")");
					},

					getImage : function(category, name) {
						var url = "https://ajax.googleapis.com/ajax/services/search/images?v=1.0&imgc=color&imgtype=photo&rsz=8&q="
								+ name + "%20" + category;

						var response = dojo._getText(url);
						var data = dojo.fromJson(response);
						var imgUrl = data.responseData.results[0].tbUrl;
						var image = dojo.create("IMG", {
							src : imgUrl,
							className : "image"
						});
						return image;
					},
					
					removeAllTiles : function(container) {
						while (container.firstChild) {
							container.removeChild(container.firstChild);
						}
					},
					
					getDifferentChars : function(size, notThisChar) {
						var chars = [];

						// create array of different chars
						var set = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
						for ( var i = 0; chars.length < size; i++) {
							var index = Math.floor(Math.random() * 52);
							var char = set.substring(index, index + 1);
							if (chars.indexOf(char.toUpperCase()) == -1
									&& chars.indexOf(char.toLowerCase()) == -1
									&& (!notThisChar || char.toLowerCase() != notThisChar
											.toLowerCase())) {
								chars.push(char);
							}
						}
						return chars;
					}
					
					
					
				});

buchstaben.Main.getInstance = function() {
	if (!buchstaben.Main.instance) {
		buchstaben.Main.instance = new buchstaben.Main();
	}
	return buchstaben.Main.instance;
}