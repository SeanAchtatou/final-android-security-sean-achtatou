dojo.provide("buchstaben.Memory");

/*
 * Memory Arten:
 * Bilder: Cartoon, Bugs, Butterflys
 * Zeichensprache: Zeichen und Buchstaben
 * Buchstaben: case sensitive, case ignore
 * Tiere: Anfangsbuchstaben und Tier, zwei verschiedene Bilder f�r Tiere
 * Mathe: Rechnung und Ergebnis (Addition, Subtraktion), Menge und Zahl
 * 
 * Uhrzeiten Analog=Digital
 * Farben
 * Lebensmittel
 */
dojo
		.declare(
				"buchstaben.Memory",
				null,
				{

					levels: [{h: 3, v: 4},
							 {h: 4, v: 4},
							 {h: 4, v: 5},
							 {h: 4, v: 6},
							 {h: 5, v: 6},
							 {h: 6, v: 6},
							 {h: 6, v: 7},
							 {h: 6, v: 8},
							 {h: 7, v: 8},
							 {h: 8, v: 8},
							 {h: 8, v: 9},
							 {h: 9, v: 10},
							 {h: 10, v: 10}
					         ],
					         
					constructor : function(args) {
						var self = this;
						var view = dijit.byId("memory");
						dojo.connect(dijit.byId("memory"), "onAfterTransitionIn", function() {self.afterTransition()});
						dojo.connect(dijit.byId("memory"), "onBeforeTransitionIn", function() {self.beforeTransition()});
						this.main = args.main;
						this.events = [];
						this.header = dojo.byId("memoryHeader");
						this.footer = dojo.byId("footer");
						this.width = dojo.doc.body.offsetWidth;
						this.height = dojo.doc.body.offsetHeight - this.header.offsetHeight - this.footer.offsetHeight;
						this.outerCanvas = dojo.byId("outerCanvas");
						this.outerCanvas.style.height = this.height + 'px';
						this.container = dojo.byId("tileContainerMemory");
						this.imageDir = "res/selectedImages/";
						this.level = 0;
						this.model = this.main.load("buchstaben/model.json");
					},

					removeAllTiles : function() {
						while (this.container.firstChild) {
							this.container.removeChild(this.container.firstChild);
						}
					},

					afterTransition: function() {
						console.debug("nach memory");
						this.play();
					},
					
					beforeTransition: function() {
						console.debug("vor memory");
						this.removeAllTiles();
					},
					
					init : function(type) {
						this.type = type;
						this.level = 0;
					},

					clearOldGame: function() {
						this.removeAllTiles();
						if (this.events && this.events.length > 0) {
							for (var i in this.events) {
								dojo.disconnect(this.events[i]);
							}
						}
						this.events = [];
						this.tiles = null;
					},
					
					play : function() {
						this.clearOldGame();
						if (this.level > this.levels.length - 1) {
							this.level = this.levels.length - 1;
						} 
						var level = this.levels[this.level];
						var h = level.h; 
						var v = level.v;
						
						this.calculateTileSize(h, v);
						this.initTiles(h, v);

						for (var i = 0; i < h; i++) {
							for (var t = 0; t < v; t++) {
								this.createTile(i, t);
								this.connectEvents(this.tiles[i][t]);
							}
						}
						
						if (this.type == "letterCapital") {
							this.main.speak("Finde zwei gleiche Gro�buchstaben");
							this.createLetterTiles(h, v, this.type);
						} else if (this.type == "letterMixed") {
							this.main.speak("Finde zwei gleiche Buchstaben");
							this.createLetterTiles(h, v, this.type);
						} else if (this.type == "letterMatch") {
							this.main.speak("Finde die Gro� und Kleinbuchstaben die zusammengeh�ren");
							this.createLetterTiles(h, v, this.type);
						} else if (this.type == "letterBlind") {
							this.main.speak("Finde zwei gleiche Buchstaben nur durch zuh�ren");
							this.createLetterTiles(h, v, this.type);
						} else if (this.type == "math20") {
							this.main.speak("Finde zwei gleiche Zahlen von 1 bis 20");
							this.createMathTiles(h, v, 20);
						} else if (this.type == "math100") {
							this.main.speak("Finde zwei gleiche Zahlen von 1 bis 100");
							this.createMathTiles(h, v, 100);
						} else if (this.type == "math100Blind") {
							this.main.speak("Finde zwei gleiche Zahlen von 1 bis 100 nur durch zuh�ren");
							this.createMathTiles(h, v, 100, true);
						} else if (this.type == "mathIcon10") {
							this.main.speak("Finde die Zahl die zur Menge geh�rt");
							this.createMathIconTiles(h, v, 10);
						} else if (this.type == "mathIcon20") {
							this.main.speak("Finde die Zahl die zur Menge geh�rt");
							this.createMathIconTiles(h, v, 20);
						} else if (this.type == "mathCalc20") {
							this.main.speak("Finde die das Ergebnis zur Rechnung im Zahlenraum bis 20");
							this.createMathCalcTiles(h, v, 20);
						} else if (this.type == "mathCalc100") {
							this.main.speak("Finde die das Ergebnis zur Rechnung im Zahlenraum bis 100");
							this.createMathCalcTiles(h, v, 100);
						} else if (this.type == "mathCalc20Sub") {
							this.main.speak("Finde die das Ergebnis zur Rechnung im Zahlenraum bis 20 mit plus und minus");
							this.createMathCalcTiles(h, v, 20, true);
						} else if (this.type == "mathCalc100Sub") {
							this.main.speak("Finde die das Ergebnis zur Rechnung im Zahlenraum bis 100 mit plus und minus");
							this.createMathCalcTiles(h, v, 100, true);
						} else {
							this.main.speak("Finde zwei gleiche Bilder");
							this.createImageTiles(h, v);
						}
						
						this.fadeInTiles();
					},
					
					fadeInTiles: function() {
						var count = 0;
						for (var i = 0; i < this.tiles.length; i++) {
							for (var t = 0; t < this.tiles[0].length; t++) {
								var obj = this.tiles[i][t];
								var fadeIn = function(obj) {return function() {dojo.removeClass(obj.tile, "invisible")}};
								setTimeout(fadeIn(obj), 100 * count);
								count++;
							}
						}
					},
					
					calculateTileSize: function(sizeH, sizeV) {
						this.tileDistance = Math.min((this.width - 20)/sizeH ,(this.height - 20)/sizeV);
						this.tileSize = this.tileDistance - 5;
						this.container.style.width = (this.tileDistance * sizeH) + "px";
						this.container.style.height = (this.tileDistance * sizeV) + "px";
					},
					
					initTiles: function(sizeH, sizeV) {
						this.tiles = [];
						for (var i = 0; i < sizeH; i++) {
							this.tiles[i] = [];
						}
					},
					
					createTile: function(i, t) {
						var perpectiveContainer = dojo.create("DIV", {className: "perspectiveTileContainer", style: "top: " + (t*this.tileDistance + 10)+ "px; left: " + (i*this.tileDistance) + "px; width: " + this.tileSize + "px; height: " + this.tileSize + "px"}, this.container);
						var tile = dojo.create("DIV", {className: "flipMemoryTile memoryTile invisible"}, perpectiveContainer)
						var frontTile = dojo.create("DIV", {className: "flipFrontTile frontTile", style: "line-height: " + this.tileSize + "px;font-size: " + this.tileSize*0.70 + "px"}, perpectiveContainer)
						this.tiles[i][t] = {frontTile: frontTile,
											tile: tile};
					},
					
					getDifferentImages: function(size) {
						var allImages = dojo.clone(this.model.memory[this.type]);
						var images = [];
						for (var i = 0; i < size; i++) {
							var imageIndex = Math.floor(Math.random()*allImages.length);
							var imageName = allImages[imageIndex];
							allImages.splice(imageIndex, 1);
							if (allImages.length == 0) {
								// handle underflow
								allImages = dojo.clone(this.model.memory[this.type]);
							}
							images.push(imageName);
						}
						return images;
					},
					
					getDifferentChars : function(size, notThisChar, type) {
						var chars = [];

						// create array of different chars
						var set = null;
						if (type == "letterCapital" || type == "letterMatch" || type=="letterBlind") {
							set = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
						} else {
							set = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
						}
						for ( var i = 0; chars.length < size; i++) {
							var index = Math.floor(Math.random() * set.length);
							var char = set.substring(index, index + 1);
							if (size >= set.length - 3 || // handle underflow
									(chars.indexOf(char.toUpperCase()) == -1
									&& chars.indexOf(char.toLowerCase()) == -1
									&& (!notThisChar || char.toLowerCase() != notThisChar
											.toLowerCase()))) {
								chars.push(char);
							}
						}
						return chars;
					},

					getDifferentNumbers: function(size, max) {
						var chars = [];

						// create array of different chars
						for ( var i = 0; chars.length < size; i++) {
							var index = Math.floor(Math.random() * max) + 1;
							chars.push(index);
						}
						return chars; 
					},

					getDifferentCalculations: function(size, max, subtraction) {
						var chars = [];

						// create array of different chars
						for ( var i = 0; chars.length < size; i++) {
							var index = Math.floor(Math.random() * max) + 1;
							// find calculation
							var operand1, operand2, operator;
							if (subtraction) {
								 operand1 = Math.floor(Math.random() * max) + 1;
								 operand2 = index - operand1;
								 operator = operand2 < 0?"":"+";
								 calc = "" + operand1 + operator + operand2;
							} else {
								 operand1 = Math.floor(Math.random() * index) + 1;
								 operand2 = index - operand1;
								 operator = "+";
								 calc = "" + operand1 + operator + operand2;
							}
							
							chars.push({number: index, calculation: calc});
						}
						return chars;
					},

					createImageTiles: function(sizeH, sizeV) {
						var images = this.getDifferentImages(sizeH*sizeV/2);

						for (var i in images) {
							var image = images[i];
							var foundTiles = 0;
							while (foundTiles < 2) {
								var h = Math.floor(Math.random()*sizeH);
								var v = Math.floor(Math.random()*sizeV);
								if (!this.tiles[h][v].value) {
									this.tiles[h][v].value = image;
									this.tiles[h][v].frontTile.style.backgroundColor = "white";
									this.tiles[h][v].frontTile.style.backgroundImage = "none";
									this.tiles[h][v].frontTile.appendChild(dojo.create("IMG", {style: "max-width: " + this.tileSize + "px; max-height: " + this.tileSize + "px", src: this.imageDir + image, className: "imageTile"}, null));
									foundTiles++;
								}
							}
						}
					},
					
					createLetterTiles: function(sizeH, sizeV, type) {
						var chars = this.getDifferentChars(sizeH*sizeV/2, null, type);

						for (var i in chars) {
							var char = chars[i];
							var foundTiles = 0;
							while (foundTiles < 2) {
								var h = Math.floor(Math.random()*sizeH);
								var v = Math.floor(Math.random()*sizeV);
								if (!this.tiles[h][v].value) {
									this.tiles[h][v].value = char;
									this.tiles[h][v].say = char;
									if (type == "letterMatch" && foundTiles == 1) {
										this.tiles[h][v].frontTile.innerHTML = char.toLowerCase();
									} else if (type == "letterBlind") {
										this.tiles[h][v].frontTile.innerHTML = "";
									} else {
										this.tiles[h][v].frontTile.innerHTML = char;
									} 
									foundTiles++;
								}
							}
						}
					},
					
					createMathIconTiles: function(sizeH, sizeV, max) {
						var chars = this.getDifferentNumbers(sizeH*sizeV/2, max);
						var rows = 2;
						if (max == 20) {
							rows = 4;
						}
						
						for (var i in chars) {
							var char = chars[i];
							var foundTiles = 0;
							while (foundTiles < 2) {
								var h = Math.floor(Math.random()*sizeH);
								var v = Math.floor(Math.random()*sizeV);
								if (!this.tiles[h][v].value) {
									if (foundTiles == 1) {
										this.tiles[h][v].value = char;
										this.tiles[h][v].say = char;
										this.createNumberIcon(this.tiles[h][v].frontTile, char, rows);
									} else {
										this.tiles[h][v].value = char;
										this.tiles[h][v].say = char;
										this.tiles[h][v].frontTile.innerHTML = char;
									}
									foundTiles++;
								}
							}
						}
					},
					
					createMathTiles: function(sizeH, sizeV, max, blind) {
						var chars = this.getDifferentNumbers(sizeH*sizeV/2, max);

						for (var i in chars) {
							var char = chars[i];
							var foundTiles = 0;
							while (foundTiles < 2) {
								var h = Math.floor(Math.random()*sizeH);
								var v = Math.floor(Math.random()*sizeV);
								if (!this.tiles[h][v].value) {
									this.tiles[h][v].value = char;
									this.tiles[h][v].say = char;
									if (blind) {
										this.tiles[h][v].frontTile.innerHTML = "";
									} else {
										this.tiles[h][v].frontTile.innerHTML = char;
									}
									foundTiles++;
								}
							}
						}
					},
					
					createMathCalcTiles: function(sizeH, sizeV, max, sub) {
						var chars = this.getDifferentCalculations(sizeH*sizeV/2, max, sub);

						for (var i in chars) {
							var char = chars[i];
							var foundTiles = 0;
							while (foundTiles < 2) {
								var h = Math.floor(Math.random()*sizeH);
								var v = Math.floor(Math.random()*sizeV);
								if (!this.tiles[h][v].value) {
									if (foundTiles == 1) {
										this.tiles[h][v].value = char.number;
										this.tiles[h][v].say = char.number;
										this.tiles[h][v].frontTile.innerHTML = char.number;
									} else {
										this.tiles[h][v].value = char.number;
										this.tiles[h][v].say = char.calculation;
										this.tiles[h][v].frontTile.innerHTML = char.calculation;
										this.tiles[h][v].frontTile.style.fontSize = this.tileSize*0.30 + "px";
									}
									foundTiles++;
								}
							}
						}
					},
					
					createNumberIcon: function(node, number, rows) {
						var color = "red";
						var width = node.offsetWidth;
						var height = node.offsetHeight;
						var distance = Math.floor(width/5*0.9);
						var size = Math.floor(distance*0.8);
						var leftStart = (width - distance*5)/2;
						var topStart = (height - distance*rows)/2;
						
						for (var i = 0; i < rows; i++) {
							for (var t = 0; t < 5; t++) {
								if (i*5 + t >= number) {
									color = "silver";
								}
								dojo.create("div", {className: "numberIcon", style: "background-color: " + color + ";width: " + size + "px; height: " + size + "px; left: " + (leftStart + t*distance) + "px; top: " + (topStart + i*distance) + "px"}, node);
							}
						}
						
					},
					
					connectEvents: function(obj) {
						var self = this;
						this.events.push(dojo.connect(obj.tile, "onclick", function() {
							if (!obj.done && !self.lock) {
								self.showTile(obj);
							}
						}));
//						dojo.connect(obj.frontTile, "onclick", function() {
//							if (!obj.done) {
//								self.hideTile(obj);
//							}
//						});
					},
					
					checkTiles: function() {
						var foundVisible = null;
						for (var i = 0; i < this.tiles.length; i++) {
							for (var t = 0; t < this.tiles[0].length; t++) {
								var obj = this.tiles[i][t];
								if (obj.visible) {
									if (foundVisible) {
										var self = this;
										this.lock = true;
										if (obj.value == foundVisible.value) {
											// Bingo!
											obj.visible = false;
											obj.done = true;
											var self = this;
											setTimeout(function() {
												self.main.vibrateLong();
												 self.main.playWin();
												self.removeTile(obj);
												self.removeTile(foundVisible);
												self.checkIfDone();
												self.lock = false;
											}, 1000);
										} else {
											obj.visible = false;
											var self = this;
											setTimeout(function() {
												self.hideTile(obj);
												self.hideTile(foundVisible);
												self.lock = false;
											}, 1000);
											setTimeout(function() {
												 self.main.playFail();
											}, 500);
										}
										return;
									} else {
										foundVisible = obj;
									}
								}
							}
						}
					},
					
					checkIfDone: function() {
						var foundHidden = null;
						for (var i = 0; i < this.tiles.length; i++) {
							for (var t = 0; t < this.tiles[0].length; t++) {
								var obj = this.tiles[i][t];
								if (!obj.done) {
									return;
								}
							}
						}
					    this.main.playTusch();
					    var self = this;
						setTimeout(function() {
							self.level++;
							self.play();
						}, 3000);
					},
					
					hideTile: function(obj) {
						 this.main.playSwooshClose();
						obj.frontTile.className = "flipFrontTile frontTile";
						obj.tile.className = "flipMemoryTile memoryTile";
						 obj.visible = false;
					},
					
					showTile: function(obj) {
						if (obj.say) {
							 this.main.speak(obj.say);
						}
						if (!obj.say && !this.main.phonegapUsed) {
							 this.main.playSwooshOpen();
						} else {
							this.main.vibrate();
						}
						obj.frontTile.className = "frontTile";
						obj.tile.className = "memoryTile";
						 obj.visible = true;
						 this.checkTiles();
					},
					
					removeTile: function(obj) {
						//dojo.addClass(obj.frontTile, "showTile");
						obj.frontTile.className = "tileSucceeded frontTile";
						obj.visible = false;
						obj.done = true;
					}
					

				});
