package de.mymielke.lernchef;

import android.os.Bundle;
import com.phonegap.DroidGap;

public class LernChef extends DroidGap {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.loadUrl("file:///android_asset/www/index.html");
    }
}
