package com.phonegap;

import android.net.Uri;
import android.util.Log;
import android.webkit.CookieManager;
import com.phonegap.api.Plugin;
import com.phonegap.api.PluginResult;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Iterator;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class FileTransfer extends Plugin {
    private static final String BOUNDRY = "*****";
    public static int CONNECTION_ERR = 3;
    static final HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };
    public static int FILE_NOT_FOUND_ERR = 1;
    public static int INVALID_URL_ERR = 2;
    private static final String LINE_END = "\r\n";
    private static final String LINE_START = "--";
    private static final String LOG_TAG = "FileUploader";
    private HostnameVerifier defaultHostnameVerifier = null;
    private SSLSocketFactory defaultSSLSocketFactory = null;

    public PluginResult execute(String action, JSONArray args, String callbackId) {
        try {
            String file = args.getString(0);
            String server = args.getString(1);
            String fileKey = getArgument(args, 2, "file");
            String fileName = getArgument(args, 3, "image.jpg");
            String mimeType = getArgument(args, 4, "image/jpeg");
            try {
                JSONObject params = args.optJSONObject(5);
                boolean trustEveryone = args.optBoolean(6);
                if (!action.equals("upload")) {
                    return new PluginResult(PluginResult.Status.INVALID_ACTION);
                }
                FileUploadResult r = upload(file, server, fileKey, fileName, mimeType, params, trustEveryone);
                Log.d(LOG_TAG, "****** About to return a result from upload");
                return new PluginResult(PluginResult.Status.OK, r.toJSONObject());
            } catch (FileNotFoundException e) {
                FileNotFoundException e2 = e;
                Log.e(LOG_TAG, e2.getMessage(), e2);
                return new PluginResult(PluginResult.Status.IO_EXCEPTION, createFileUploadError(FILE_NOT_FOUND_ERR));
            } catch (IllegalArgumentException e3) {
                IllegalArgumentException e4 = e3;
                Log.e(LOG_TAG, e4.getMessage(), e4);
                return new PluginResult(PluginResult.Status.IO_EXCEPTION, createFileUploadError(INVALID_URL_ERR));
            } catch (SSLException e5) {
                SSLException e6 = e5;
                Log.e(LOG_TAG, e6.getMessage(), e6);
                Log.d(LOG_TAG, "Got my ssl exception!!!");
                return new PluginResult(PluginResult.Status.IO_EXCEPTION, createFileUploadError(CONNECTION_ERR));
            } catch (IOException e7) {
                IOException e8 = e7;
                Log.e(LOG_TAG, e8.getMessage(), e8);
                return new PluginResult(PluginResult.Status.IO_EXCEPTION, createFileUploadError(CONNECTION_ERR));
            } catch (JSONException e9) {
                JSONException e10 = e9;
                Log.e(LOG_TAG, e10.getMessage(), e10);
                return new PluginResult(PluginResult.Status.JSON_EXCEPTION);
            }
        } catch (JSONException e11) {
            Log.d(LOG_TAG, "Missing filename or server name");
            return new PluginResult(PluginResult.Status.JSON_EXCEPTION, "Missing filename or server name");
        }
    }

    private void trustAllHosts() {
        TrustManager[] trustAllCerts = {new X509TrustManager() {
            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[0];
            }

            public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
            }

            public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
            }
        }};
        try {
            this.defaultSSLSocketFactory = HttpsURLConnection.getDefaultSSLSocketFactory();
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, trustAllCerts, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
            Exception e2 = e;
            Log.e(LOG_TAG, e2.getMessage(), e2);
        }
    }

    private JSONObject createFileUploadError(int errorCode) {
        JSONException e;
        JSONObject error = null;
        try {
            JSONObject error2 = new JSONObject();
            try {
                error2.put("code", errorCode);
                return error2;
            } catch (JSONException e2) {
                e = e2;
                error = error2;
                Log.e(LOG_TAG, e.getMessage(), e);
                return error;
            }
        } catch (JSONException e3) {
            e = e3;
            Log.e(LOG_TAG, e.getMessage(), e);
            return error;
        }
    }

    private String getArgument(JSONArray args, int position, String defaultString) {
        String arg = defaultString;
        if (args.length() < position) {
            return arg;
        }
        String arg2 = args.optString(position);
        if (arg2 == null || "null".equals(arg2)) {
            return defaultString;
        }
        return arg2;
    }

    public FileUploadResult upload(String file, String server, String fileKey, String fileName, String mimeType, JSONObject params, boolean trustEveryone) throws IOException, SSLException {
        HttpURLConnection conn;
        FileUploadResult result = new FileUploadResult();
        InputStream fileInputStream = getPathFromUri(file);
        URL url = new URL(server);
        if (!url.getProtocol().toLowerCase().equals("https")) {
            conn = (HttpURLConnection) url.openConnection();
        } else if (!trustEveryone) {
            conn = (HttpsURLConnection) url.openConnection();
        } else {
            trustAllHosts();
            HttpsURLConnection https = (HttpsURLConnection) url.openConnection();
            this.defaultHostnameVerifier = https.getHostnameVerifier();
            https.setHostnameVerifier(DO_NOT_VERIFY);
            conn = https;
        }
        conn.setDoInput(true);
        conn.setDoOutput(true);
        conn.setUseCaches(false);
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Connection", "Keep-Alive");
        conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=*****");
        String cookie = CookieManager.getInstance().getCookie(server);
        if (cookie != null) {
            conn.setRequestProperty("Cookie", cookie);
        }
        DataOutputStream dataOutputStream = new DataOutputStream(conn.getOutputStream());
        try {
            Iterator iter = params.keys();
            while (iter.hasNext()) {
                Object key = iter.next();
                dataOutputStream.writeBytes("--*****\r\n");
                dataOutputStream.writeBytes("Content-Disposition: form-data; name=\"" + key.toString() + "\";");
                dataOutputStream.writeBytes("\r\n\r\n");
                dataOutputStream.writeBytes(params.getString(key.toString()));
                dataOutputStream.writeBytes(LINE_END);
            }
        } catch (JSONException e) {
            JSONException e2 = e;
            Log.e(LOG_TAG, e2.getMessage(), e2);
        }
        dataOutputStream.writeBytes("--*****\r\n");
        dataOutputStream.writeBytes("Content-Disposition: form-data; name=\"" + fileKey + "\";" + " filename=\"" + fileName + "\"" + LINE_END);
        dataOutputStream.writeBytes("Content-Type: " + mimeType + LINE_END);
        dataOutputStream.writeBytes(LINE_END);
        int bufferSize = Math.min(fileInputStream.available(), 8096);
        byte[] buffer = new byte[bufferSize];
        int bytesRead = fileInputStream.read(buffer, 0, bufferSize);
        long totalBytes = 0;
        while (bytesRead > 0) {
            totalBytes += (long) bytesRead;
            result.setBytesSent(totalBytes);
            dataOutputStream.write(buffer, 0, bufferSize);
            bufferSize = Math.min(fileInputStream.available(), 8096);
            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
        }
        dataOutputStream.writeBytes(LINE_END);
        dataOutputStream.writeBytes("--*****--\r\n");
        fileInputStream.close();
        dataOutputStream.flush();
        dataOutputStream.close();
        StringBuffer stringBuffer = new StringBuffer("");
        try {
            DataInputStream dataInputStream = new DataInputStream(conn.getInputStream());
            while (true) {
                String line = dataInputStream.readLine();
                if (line == null) {
                    break;
                }
                stringBuffer.append(line);
            }
            Log.d(LOG_TAG, "got response from server");
            Log.d(LOG_TAG, stringBuffer.toString());
            result.setResponseCode(conn.getResponseCode());
            result.setResponse(stringBuffer.toString());
            dataInputStream.close();
            conn.disconnect();
            if (trustEveryone && url.getProtocol().toLowerCase().equals("https")) {
                ((HttpsURLConnection) conn).setHostnameVerifier(this.defaultHostnameVerifier);
                HttpsURLConnection.setDefaultSSLSocketFactory(this.defaultSSLSocketFactory);
            }
            return result;
        } catch (FileNotFoundException e3) {
            throw new IOException("Received error from server");
        }
    }

    private InputStream getPathFromUri(String path) throws FileNotFoundException {
        if (!path.startsWith("content:")) {
            return new FileInputStream(path);
        }
        return this.ctx.getContentResolver().openInputStream(Uri.parse(path));
    }
}
