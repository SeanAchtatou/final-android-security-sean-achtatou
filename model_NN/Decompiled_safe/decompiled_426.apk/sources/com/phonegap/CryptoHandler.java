package com.phonegap;

import com.phonegap.api.Plugin;
import com.phonegap.api.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;

public class CryptoHandler extends Plugin {
    public PluginResult execute(String action, JSONArray args, String callbackId) {
        PluginResult.Status status = PluginResult.Status.OK;
        try {
            if (action.equals("encrypt")) {
                encrypt(args.getString(0), args.getString(1));
            } else if (action.equals("decrypt")) {
                decrypt(args.getString(0), args.getString(1));
            }
            return new PluginResult(status, "");
        } catch (JSONException e) {
            return new PluginResult(PluginResult.Status.JSON_EXCEPTION);
        }
    }

    public void encrypt(String pass, String text) {
        try {
            String encrypt = SimpleCrypto.encrypt(pass, text);
            sendJavascript("Crypto.gotCryptedString('" + text + "')");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void decrypt(String pass, String text) {
        try {
            String decrypt = SimpleCrypto.decrypt(pass, text);
            sendJavascript("Crypto.gotPlainString('" + text + "')");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
