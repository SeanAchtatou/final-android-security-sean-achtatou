package com.phonegap;

import android.util.Log;
import com.phonegap.api.Plugin;
import com.phonegap.api.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ContactManager extends Plugin {
    private static final String LOG_TAG = "Contact Query";
    private static ContactAccessor contactAccessor;

    public PluginResult execute(String action, JSONArray args, String callbackId) {
        if (contactAccessor == null) {
            contactAccessor = ContactAccessor.getInstance(this.webView, this.ctx);
        }
        PluginResult.Status status = PluginResult.Status.OK;
        try {
            if (action.equals("search")) {
                return new PluginResult(status, contactAccessor.search(args.getJSONArray(0), args.optJSONObject(1)), "navigator.service.contacts.cast");
            }
            if (action.equals("save")) {
                if (contactAccessor.save(args.getJSONObject(0))) {
                    return new PluginResult(status, "");
                }
                JSONObject r = new JSONObject();
                r.put("code", 0);
                return new PluginResult(PluginResult.Status.ERROR, r);
            } else if (!action.equals("remove")) {
                return new PluginResult(status, "");
            } else {
                if (contactAccessor.remove(args.getString(0))) {
                    return new PluginResult(status, "");
                }
                JSONObject r2 = new JSONObject();
                r2.put("code", 2);
                return new PluginResult(PluginResult.Status.ERROR, r2);
            }
        } catch (JSONException e) {
            JSONException e2 = e;
            Log.e(LOG_TAG, e2.getMessage(), e2);
            return new PluginResult(PluginResult.Status.JSON_EXCEPTION);
        }
    }
}
