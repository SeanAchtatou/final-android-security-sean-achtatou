package com.phonegap;

import android.net.Uri;
import android.os.Environment;
import android.webkit.MimeTypeMap;
import com.phonegap.api.Plugin;
import com.phonegap.api.PluginResult;
import com.phonegap.file.EncodingException;
import com.phonegap.file.FileExistsException;
import com.phonegap.file.InvalidModificationException;
import com.phonegap.file.NoModificationAllowedException;
import com.phonegap.file.TypeMismatchException;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.channels.FileChannel;
import org.apache.commons.codec.binary.Base64;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class FileUtils extends Plugin {
    public static int ABORT_ERR = 3;
    public static int APPLICATION = 3;
    public static int ENCODING_ERR = 5;
    public static int INVALID_MODIFICATION_ERR = 9;
    public static int INVALID_STATE_ERR = 7;
    private static final String LOG_TAG = "FileUtils";
    public static int NOT_FOUND_ERR = 1;
    public static int NOT_READABLE_ERR = 4;
    public static int NO_MODIFICATION_ALLOWED_ERR = 6;
    public static int PATH_EXISTS_ERR = 12;
    public static int PERSISTENT = 1;
    public static int QUOTA_EXCEEDED_ERR = 10;
    public static int RESOURCE = 2;
    public static int SECURITY_ERR = 2;
    public static int SYNTAX_ERR = 8;
    public static int TEMPORARY = 0;
    public static int TYPE_MISMATCH_ERR = 11;
    FileReader f_in;
    FileWriter f_out;

    public PluginResult execute(String action, JSONArray args, String callbackId) {
        PluginResult.Status status = PluginResult.Status.OK;
        try {
            if (action.equals("testSaveLocationExists")) {
                return new PluginResult(status, DirectoryManager.testSaveLocationExists());
            }
            if (action.equals("getFreeDiskSpace")) {
                return new PluginResult(status, (float) DirectoryManager.getFreeDiskSpace());
            }
            if (action.equals("testFileExists")) {
                return new PluginResult(status, DirectoryManager.testFileExists(args.getString(0)));
            }
            if (action.equals("testDirectoryExists")) {
                return new PluginResult(status, DirectoryManager.testFileExists(args.getString(0)));
            }
            if (action.equals("readAsText")) {
                return new PluginResult(status, readAsText(args.getString(0), args.getString(1)));
            } else if (action.equals("readAsDataURL")) {
                return new PluginResult(status, readAsDataURL(args.getString(0)));
            } else {
                if (action.equals("writeAsText")) {
                    writeAsText(args.getString(0), args.getString(1), args.getBoolean(2));
                } else if (action.equals("write")) {
                    return new PluginResult(status, (float) write(args.getString(0), args.getString(1), args.getInt(2)));
                } else if (action.equals("truncate")) {
                    return new PluginResult(status, (float) truncateFile(args.getString(0), args.getLong(1)));
                } else if (action.equals("requestFileSystem")) {
                    long size = args.optLong(1);
                    if (size == 0 || size <= DirectoryManager.getFreeDiskSpace()) {
                        return new PluginResult(status, requestFileSystem(args.getInt(0)), "window.localFileSystem._castFS");
                    }
                    return new PluginResult(PluginResult.Status.ERROR, new JSONObject().put("code", QUOTA_EXCEEDED_ERR));
                } else if (action.equals("resolveLocalFileSystemURI")) {
                    return new PluginResult(status, resolveLocalFileSystemURI(args.getString(0)), "window.localFileSystem._castEntry");
                } else if (action.equals("getMetadata")) {
                    return new PluginResult(status, getMetadata(args.getString(0)), "window.localFileSystem._castDate");
                } else if (action.equals("getFileMetadata")) {
                    return new PluginResult(status, getFileMetadata(args.getString(0)), "window.localFileSystem._castDate");
                } else if (action.equals("getParent")) {
                    return new PluginResult(status, getParent(args.getString(0)), "window.localFileSystem._castEntry");
                } else if (action.equals("getDirectory")) {
                    return new PluginResult(status, getFile(args.getString(0), args.getString(1), args.optJSONObject(2), true), "window.localFileSystem._castEntry");
                } else if (action.equals("getFile")) {
                    return new PluginResult(status, getFile(args.getString(0), args.getString(1), args.optJSONObject(2), false), "window.localFileSystem._castEntry");
                } else if (action.equals("remove")) {
                    if (remove(args.getString(0))) {
                        return new PluginResult(status);
                    }
                    return new PluginResult(PluginResult.Status.ERROR, new JSONObject().put("code", NO_MODIFICATION_ALLOWED_ERR));
                } else if (action.equals("removeRecursively")) {
                    if (removeRecursively(args.getString(0))) {
                        return new PluginResult(status);
                    }
                    return new PluginResult(PluginResult.Status.ERROR, new JSONObject().put("code", NO_MODIFICATION_ALLOWED_ERR));
                } else if (action.equals("moveTo")) {
                    return new PluginResult(status, transferTo(args.getString(0), args.getJSONObject(1), args.optString(2), true), "window.localFileSystem._castEntry");
                } else if (action.equals("copyTo")) {
                    return new PluginResult(status, transferTo(args.getString(0), args.getJSONObject(1), args.optString(2), false), "window.localFileSystem._castEntry");
                } else if (action.equals("readEntries")) {
                    return new PluginResult(status, readEntries(args.getString(0)), "window.localFileSystem._castEntries");
                }
                return new PluginResult(status, "");
            }
        } catch (FileNotFoundException e) {
            try {
                return new PluginResult(PluginResult.Status.ERROR, new JSONObject().put("code", NOT_FOUND_ERR));
            } catch (JSONException e2) {
                e2.printStackTrace();
                return new PluginResult(PluginResult.Status.JSON_EXCEPTION);
            }
        } catch (FileExistsException e3) {
            return new PluginResult(PluginResult.Status.ERROR, new JSONObject().put("code", PATH_EXISTS_ERR));
        } catch (NoModificationAllowedException e4) {
            return new PluginResult(PluginResult.Status.ERROR, new JSONObject().put("code", NO_MODIFICATION_ALLOWED_ERR));
        } catch (JSONException e5) {
            return new PluginResult(PluginResult.Status.ERROR, new JSONObject().put("code", NO_MODIFICATION_ALLOWED_ERR));
        } catch (InvalidModificationException e6) {
            return new PluginResult(PluginResult.Status.ERROR, new JSONObject().put("code", INVALID_MODIFICATION_ERR));
        } catch (MalformedURLException e7) {
            return new PluginResult(PluginResult.Status.ERROR, new JSONObject().put("code", ENCODING_ERR));
        } catch (IOException e8) {
            return new PluginResult(PluginResult.Status.ERROR, new JSONObject().put("code", INVALID_MODIFICATION_ERR));
        } catch (EncodingException e9) {
            return new PluginResult(PluginResult.Status.ERROR, new JSONObject().put("code", ENCODING_ERR));
        } catch (TypeMismatchException e10) {
            return new PluginResult(PluginResult.Status.ERROR, new JSONObject().put("code", TYPE_MISMATCH_ERR));
        }
    }

    private JSONObject resolveLocalFileSystemURI(String url) throws IOException, JSONException {
        File fp;
        String decoded = URLDecoder.decode(url, "UTF-8");
        new URL(decoded);
        if (decoded.startsWith("file://")) {
            fp = new File(decoded.substring(7, decoded.length()));
        } else {
            fp = new File(decoded);
        }
        if (!fp.exists()) {
            throw new FileNotFoundException();
        } else if (fp.canRead()) {
            return getEntry(fp);
        } else {
            throw new IOException();
        }
    }

    private JSONArray readEntries(String fileName) throws FileNotFoundException, JSONException {
        File fp = new File(fileName);
        if (!fp.exists()) {
            throw new FileNotFoundException();
        }
        JSONArray entries = new JSONArray();
        if (fp.isDirectory()) {
            File[] files = fp.listFiles();
            for (File entry : files) {
                entries.put(getEntry(entry));
            }
        }
        return entries;
    }

    private JSONObject transferTo(String fileName, JSONObject newParent, String newName, boolean move) throws JSONException, FileExistsException, NoModificationAllowedException, IOException, InvalidModificationException, EncodingException {
        if (newName == null || !newName.contains(":")) {
            File source = new File(fileName);
            if (!source.exists()) {
                throw new FileNotFoundException("The source does not exist");
            }
            File destinationDir = new File(newParent.getString("fullPath"));
            if (!destinationDir.exists()) {
                throw new FileNotFoundException("The source does not exist");
            }
            File destination = createDestination(newName, source, destinationDir);
            if (source.getAbsolutePath().equals(destination.getAbsolutePath())) {
                throw new InvalidModificationException("Can't copy a file onto itself");
            } else if (source.isDirectory()) {
                if (move) {
                    return moveDirectory(source, destination);
                }
                return copyDirectory(source, destination);
            } else if (move) {
                return moveFile(source, destination);
            } else {
                return copyFile(source, destination);
            }
        } else {
            throw new EncodingException("Bad file name");
        }
    }

    private File createDestination(String newName, File fp, File destination) {
        if ("null".equals(newName) || "".equals(newName)) {
            newName = null;
        }
        if (newName != null) {
            return new File(destination.getAbsolutePath() + File.separator + newName);
        }
        return new File(destination.getAbsolutePath() + File.separator + fp.getName());
    }

    private JSONObject copyFile(File srcFile, File destFile) throws IOException, InvalidModificationException, JSONException {
        if (!destFile.exists() || !destFile.isDirectory()) {
            FileChannel input = new FileInputStream(srcFile).getChannel();
            FileChannel output = new FileOutputStream(destFile).getChannel();
            input.transferTo(0, input.size(), output);
            input.close();
            output.close();
            return getEntry(destFile);
        }
        throw new InvalidModificationException("Can't rename a file to a directory");
    }

    private JSONObject copyDirectory(File srcDir, File destinationDir) throws JSONException, IOException, NoModificationAllowedException, InvalidModificationException {
        if (destinationDir.exists() && destinationDir.isFile()) {
            throw new InvalidModificationException("Can't rename a file to a directory");
        } else if (isCopyOnItself(srcDir.getAbsolutePath(), destinationDir.getAbsolutePath())) {
            throw new InvalidModificationException("Can't copy itself into itself");
        } else if (destinationDir.exists() || destinationDir.mkdir()) {
            for (File file : srcDir.listFiles()) {
                if (file.isDirectory()) {
                    copyDirectory(file, destinationDir);
                } else {
                    copyFile(file, new File(destinationDir.getAbsoluteFile() + File.separator + file.getName()));
                }
            }
            return getEntry(destinationDir);
        } else {
            throw new NoModificationAllowedException("Couldn't create the destination direcotry");
        }
    }

    private boolean isCopyOnItself(String src, String dest) {
        if (!dest.startsWith(src) || dest.indexOf(File.separator, src.length() - 1) == -1) {
            return false;
        }
        return true;
    }

    private JSONObject moveFile(File srcFile, File destFile) throws JSONException, InvalidModificationException {
        if (!destFile.exists() || !destFile.isDirectory()) {
            if (!srcFile.renameTo(destFile)) {
            }
            return getEntry(destFile);
        }
        throw new InvalidModificationException("Can't rename a file to a directory");
    }

    private JSONObject moveDirectory(File srcDir, File destinationDir) throws JSONException, FileExistsException, NoModificationAllowedException, InvalidModificationException {
        if (destinationDir.exists() && destinationDir.isFile()) {
            throw new InvalidModificationException("Can't rename a file to a directory");
        } else if (isCopyOnItself(srcDir.getAbsolutePath(), destinationDir.getAbsolutePath())) {
            throw new InvalidModificationException("Can't move itself into itself");
        } else if (!destinationDir.exists() || destinationDir.list().length <= 0) {
            if (!srcDir.renameTo(destinationDir)) {
            }
            return getEntry(destinationDir);
        } else {
            throw new InvalidModificationException("directory is not empty");
        }
    }

    private boolean removeRecursively(String filePath) throws FileExistsException {
        File fp = new File(filePath);
        if (atRootDirectory(filePath)) {
            return false;
        }
        return removeDirRecursively(fp);
    }

    private boolean removeDirRecursively(File directory) throws FileExistsException {
        if (directory.isDirectory()) {
            for (File file : directory.listFiles()) {
                removeDirRecursively(file);
            }
        }
        if (directory.delete()) {
            return true;
        }
        throw new FileExistsException("could not delete: " + directory.getName());
    }

    private boolean remove(String filePath) throws NoModificationAllowedException, InvalidModificationException {
        File fp = new File(filePath);
        if (atRootDirectory(filePath)) {
            throw new NoModificationAllowedException("You can't delete the root directory");
        } else if (!fp.isDirectory() || fp.list().length <= 0) {
            return fp.delete();
        } else {
            throw new InvalidModificationException("You can't delete a directory that is not empty.");
        }
    }

    private JSONObject getFile(String dirPath, String fileName, JSONObject options, boolean directory) throws FileExistsException, IOException, TypeMismatchException, EncodingException, JSONException {
        boolean create = false;
        boolean exclusive = false;
        if (options != null && (create = options.optBoolean("create"))) {
            exclusive = options.optBoolean("exclusive");
        }
        if (fileName.contains(":")) {
            throw new EncodingException("This file has a : in it's name");
        }
        File fp = createFileObject(dirPath, fileName);
        if (create) {
            if (!exclusive || !fp.exists()) {
                if (directory) {
                    fp.mkdir();
                } else {
                    fp.createNewFile();
                }
                if (!fp.exists()) {
                    throw new FileExistsException("create fails");
                }
            } else {
                throw new FileExistsException("create/exclusive fails");
            }
        } else if (!fp.exists()) {
            throw new FileNotFoundException("path does not exist");
        } else if (directory) {
            if (fp.isFile()) {
                throw new TypeMismatchException("path doesn't exist or is file");
            }
        } else if (fp.isDirectory()) {
            throw new TypeMismatchException("path doesn't exist or is directory");
        }
        return getEntry(fp);
    }

    private File createFileObject(String dirPath, String fileName) {
        if (fileName.startsWith("/")) {
            return new File(fileName);
        }
        return new File(dirPath + File.separator + fileName);
    }

    private JSONObject getParent(String filePath) throws JSONException {
        if (atRootDirectory(filePath)) {
            return getEntry(filePath);
        }
        return getEntry(new File(filePath).getParent());
    }

    private boolean atRootDirectory(String filePath) {
        if (filePath.equals(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Android/data/" + this.ctx.getPackageName() + "/cache") || filePath.equals(Environment.getExternalStorageDirectory().getAbsolutePath())) {
            return true;
        }
        return false;
    }

    private JSONObject getMetadata(String filePath) throws FileNotFoundException, JSONException {
        File file = new File(filePath);
        if (!file.exists()) {
            throw new FileNotFoundException("Failed to find file in getMetadata");
        }
        JSONObject metadata = new JSONObject();
        metadata.put("modificationTime", file.lastModified());
        return metadata;
    }

    private JSONObject getFileMetadata(String filePath) throws FileNotFoundException, JSONException {
        File file = new File(filePath);
        if (!file.exists()) {
            throw new FileNotFoundException("File: " + filePath + " does not exist.");
        }
        JSONObject metadata = new JSONObject();
        metadata.put("size", file.length());
        metadata.put("type", getMimeType(filePath));
        metadata.put("name", file.getName());
        metadata.put("fullPath", file.getAbsolutePath());
        metadata.put("lastModifiedDate", file.lastModified());
        return metadata;
    }

    private JSONObject requestFileSystem(int type) throws IOException, JSONException {
        JSONObject fs = new JSONObject();
        if (type == TEMPORARY) {
            if (Environment.getExternalStorageState().equals("mounted")) {
                fs.put("name", "temporary");
                fs.put("root", getEntry(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Android/data/" + this.ctx.getPackageName() + "/cache/"));
                new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Android/data/" + this.ctx.getPackageName() + "/cache/").mkdirs();
            } else {
                throw new IOException("SD Card not mounted");
            }
        } else if (type == PERSISTENT) {
            if (Environment.getExternalStorageState().equals("mounted")) {
                fs.put("name", "persistent");
                fs.put("root", getEntry(Environment.getExternalStorageDirectory()));
            } else {
                throw new IOException("SD Card not mounted");
            }
        } else if (type == RESOURCE) {
            fs.put("name", "resource");
        } else if (type == APPLICATION) {
            fs.put("name", "application");
        } else {
            throw new IOException("No filesystem of type requested");
        }
        return fs;
    }

    private JSONObject getEntry(File file) throws JSONException {
        JSONObject entry = new JSONObject();
        entry.put("isFile", file.isFile());
        entry.put("isDirectory", file.isDirectory());
        entry.put("name", file.getName());
        entry.put("fullPath", file.getAbsolutePath());
        return entry;
    }

    private JSONObject getEntry(String path) throws JSONException {
        return getEntry(new File(path));
    }

    public boolean isSynch(String action) {
        if (action.equals("testSaveLocationExists")) {
            return true;
        }
        if (action.equals("getFreeDiskSpace")) {
            return true;
        }
        if (action.equals("testFileExists")) {
            return true;
        }
        if (action.equals("testDirectoryExists")) {
            return true;
        }
        return false;
    }

    public String readAsText(String filename, String encoding) throws FileNotFoundException, IOException {
        byte[] bytes = new byte[1000];
        BufferedInputStream bis = new BufferedInputStream(getPathFromUri(filename), 1024);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        while (true) {
            int numRead = bis.read(bytes, 0, 1000);
            if (numRead < 0) {
                return new String(bos.toByteArray(), encoding);
            }
            bos.write(bytes, 0, numRead);
        }
    }

    public String readAsDataURL(String filename) throws FileNotFoundException, IOException {
        String contentType;
        byte[] bytes = new byte[1000];
        BufferedInputStream bis = new BufferedInputStream(getPathFromUri(filename), 1024);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        while (true) {
            int numRead = bis.read(bytes, 0, 1000);
            if (numRead < 0) {
                break;
            }
            bos.write(bytes, 0, numRead);
        }
        if (filename.startsWith("content:")) {
            contentType = this.ctx.getContentResolver().getType(Uri.parse(filename));
        } else {
            contentType = getMimeType(filename);
        }
        return "data:" + contentType + ";base64," + new String(Base64.encodeBase64(bos.toByteArray()));
    }

    public static String getMimeType(String filename) {
        return MimeTypeMap.getSingleton().getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(filename));
    }

    public void writeAsText(String filename, String data, boolean append) throws FileNotFoundException, IOException {
        byte[] rawData = data.getBytes();
        ByteArrayInputStream in = new ByteArrayInputStream(rawData);
        FileOutputStream out = new FileOutputStream(filename, append);
        byte[] buff = new byte[rawData.length];
        in.read(buff, 0, buff.length);
        out.write(buff, 0, rawData.length);
        out.flush();
        out.close();
    }

    public long write(String filename, String data, int offset) throws FileNotFoundException, IOException {
        boolean append = false;
        if (offset > 0) {
            truncateFile(filename, (long) offset);
            append = true;
        }
        byte[] rawData = data.getBytes();
        ByteArrayInputStream in = new ByteArrayInputStream(rawData);
        FileOutputStream out = new FileOutputStream(filename, append);
        byte[] buff = new byte[rawData.length];
        in.read(buff, 0, buff.length);
        out.write(buff, 0, rawData.length);
        out.flush();
        out.close();
        return (long) data.length();
    }

    private long truncateFile(String filename, long size) throws FileNotFoundException, IOException {
        RandomAccessFile raf = new RandomAccessFile(filename, "rw");
        if (raf.length() < size) {
            return raf.length();
        }
        raf.getChannel().truncate(size);
        return size;
    }

    private InputStream getPathFromUri(String path) throws FileNotFoundException {
        if (!path.startsWith("content")) {
            return new FileInputStream(path);
        }
        return this.ctx.getContentResolver().openInputStream(Uri.parse(path));
    }
}
