package com.phonegap.api;

import android.content.Intent;
import android.webkit.WebView;
import com.phonegap.api.PluginResult;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;

public final class PluginManager {
    private final WebView app;
    private final PhonegapActivity ctx;
    private HashMap<String, Plugin> plugins = new HashMap<>();
    private HashMap<String, String> services = new HashMap<>();

    public PluginManager(WebView app2, PhonegapActivity ctx2) {
        this.ctx = ctx2;
        this.app = app2;
    }

    public String exec(String service, String action, String callbackId, String jsonArgs, boolean async) {
        PluginResult cr = null;
        boolean runAsync = async;
        try {
            final JSONArray jSONArray = new JSONArray(jsonArgs);
            String clazz = this.services.get(service);
            Class c = null;
            if (clazz != null) {
                c = getClassByName(clazz);
            }
            if (isPhoneGapPlugin(c)) {
                final Plugin plugin = addPlugin(clazz, c);
                final PhonegapActivity ctx2 = this.ctx;
                if (!async || plugin.isSynch(action)) {
                    runAsync = false;
                } else {
                    runAsync = true;
                }
                if (runAsync) {
                    final String str = action;
                    final String str2 = callbackId;
                    new Thread(new Runnable() {
                        public void run() {
                            try {
                                PluginResult cr = plugin.execute(str, jSONArray, str2);
                                int status = cr.getStatus();
                                if (status == PluginResult.Status.NO_RESULT.ordinal() && cr.getKeepCallback()) {
                                    return;
                                }
                                if (status == PluginResult.Status.OK.ordinal() || status == PluginResult.Status.NO_RESULT.ordinal()) {
                                    ctx2.sendJavascript(cr.toSuccessCallbackString(str2));
                                } else {
                                    ctx2.sendJavascript(cr.toErrorCallbackString(str2));
                                }
                            } catch (Exception e) {
                                ctx2.sendJavascript(new PluginResult(PluginResult.Status.ERROR).toErrorCallbackString(str2));
                            }
                        }
                    }).start();
                    return "";
                }
                cr = plugin.execute(action, jSONArray, callbackId);
                if (cr.getStatus() == PluginResult.Status.NO_RESULT.ordinal() && cr.getKeepCallback()) {
                    return "";
                }
            }
        } catch (ClassNotFoundException e) {
            cr = new PluginResult(PluginResult.Status.CLASS_NOT_FOUND_EXCEPTION);
        } catch (JSONException e2) {
            System.out.println("ERROR: " + e2.toString());
            cr = new PluginResult(PluginResult.Status.JSON_EXCEPTION);
        }
        if (runAsync) {
            if (cr == null) {
                cr = new PluginResult(PluginResult.Status.CLASS_NOT_FOUND_EXCEPTION);
            }
            this.ctx.sendJavascript(cr.toErrorCallbackString(callbackId));
        }
        if (cr != null) {
            return cr.getJSONString();
        }
        return "{ status: 0, message: 'all good' }";
    }

    private Class getClassByName(String clazz) throws ClassNotFoundException {
        return Class.forName(clazz);
    }

    private boolean isPhoneGapPlugin(Class c) {
        if (c == null) {
            return false;
        }
        if (Plugin.class.isAssignableFrom(c) || IPlugin.class.isAssignableFrom(c)) {
            return true;
        }
        return false;
    }

    public Plugin addPlugin(String className) {
        try {
            return addPlugin(className, getClassByName(className));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            System.out.println("Error adding plugin " + className + ".");
            return null;
        }
    }

    private Plugin addPlugin(String className, Class clazz) {
        if (this.plugins.containsKey(className)) {
            return getPlugin(className);
        }
        try {
            Plugin plugin = (Plugin) clazz.newInstance();
            this.plugins.put(className, plugin);
            plugin.setContext(this.ctx);
            plugin.setView(this.app);
            return plugin;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error adding plugin " + className + ".");
            return null;
        }
    }

    private Plugin getPlugin(String className) {
        return this.plugins.get(className);
    }

    public void addService(String serviceType, String className) {
        this.services.put(serviceType, className);
    }

    public void onPause(boolean multitasking) {
        for (Map.Entry<String, Plugin> entry : this.plugins.entrySet()) {
            ((Plugin) entry.getValue()).onPause(multitasking);
        }
    }

    public void onResume(boolean multitasking) {
        for (Map.Entry<String, Plugin> entry : this.plugins.entrySet()) {
            ((Plugin) entry.getValue()).onResume(multitasking);
        }
    }

    public void onDestroy() {
        for (Map.Entry<String, Plugin> entry : this.plugins.entrySet()) {
            ((Plugin) entry.getValue()).onDestroy();
        }
    }

    public void onNewIntent(Intent intent) {
        for (Map.Entry<String, Plugin> entry : this.plugins.entrySet()) {
            ((Plugin) entry.getValue()).onNewIntent(intent);
        }
    }
}
