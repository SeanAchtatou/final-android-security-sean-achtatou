package com.phonegap.api;

import android.content.Intent;
import android.webkit.WebView;
import org.json.JSONArray;

public abstract class Plugin implements IPlugin {
    public PhonegapActivity ctx;
    public WebView webView;

    public abstract PluginResult execute(String str, JSONArray jSONArray, String str2);

    public boolean isSynch(String action) {
        return false;
    }

    public void setContext(PhonegapActivity ctx2) {
        this.ctx = ctx2;
    }

    public void setView(WebView webView2) {
        this.webView = webView2;
    }

    public void onPause(boolean multitasking) {
    }

    public void onResume(boolean multitasking) {
    }

    public void onNewIntent(Intent intent) {
    }

    public void onDestroy() {
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
    }

    public void sendJavascript(String statement) {
        this.ctx.sendJavascript(statement);
    }

    public void success(PluginResult pluginResult, String callbackId) {
        this.ctx.sendJavascript(pluginResult.toSuccessCallbackString(callbackId));
    }

    public void error(PluginResult pluginResult, String callbackId) {
        this.ctx.sendJavascript(pluginResult.toErrorCallbackString(callbackId));
    }
}
