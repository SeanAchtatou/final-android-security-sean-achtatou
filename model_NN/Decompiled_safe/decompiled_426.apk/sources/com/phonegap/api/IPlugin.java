package com.phonegap.api;

import android.content.Intent;
import android.webkit.WebView;
import org.json.JSONArray;

public interface IPlugin {
    PluginResult execute(String str, JSONArray jSONArray, String str2);

    boolean isSynch(String str);

    void onActivityResult(int i, int i2, Intent intent);

    void onDestroy();

    void onPause(boolean z);

    void onResume(boolean z);

    void setContext(PhonegapActivity phonegapActivity);

    void setView(WebView webView);
}
