package com.phonegap.api;

import android.app.Activity;
import android.content.Intent;

public abstract class PhonegapActivity extends Activity {
    public abstract void addService(String str, String str2);

    public abstract void sendJavascript(String str);

    public abstract void startActivityForResult(Plugin plugin, Intent intent, int i);
}
