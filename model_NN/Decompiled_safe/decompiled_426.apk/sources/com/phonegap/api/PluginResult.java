package com.phonegap.api;

import org.json.JSONArray;
import org.json.JSONObject;

public class PluginResult {
    public static String[] StatusMessages = {"No result", "OK", "Class not found", "Illegal access", "Instantiation error", "Malformed url", "IO error", "Invalid action", "JSON error", "Error"};
    private String cast = null;
    private boolean keepCallback = false;
    private final String message;
    private final int status;

    public enum Status {
        NO_RESULT,
        OK,
        CLASS_NOT_FOUND_EXCEPTION,
        ILLEGAL_ACCESS_EXCEPTION,
        INSTANTIATION_EXCEPTION,
        MALFORMED_URL_EXCEPTION,
        IO_EXCEPTION,
        INVALID_ACTION,
        JSON_EXCEPTION,
        ERROR
    }

    public PluginResult(Status status2) {
        this.status = status2.ordinal();
        this.message = "'" + StatusMessages[this.status] + "'";
    }

    public PluginResult(Status status2, String message2) {
        this.status = status2.ordinal();
        this.message = JSONObject.quote(message2);
    }

    public PluginResult(Status status2, JSONArray message2, String cast2) {
        this.status = status2.ordinal();
        this.message = message2.toString();
        this.cast = cast2;
    }

    public PluginResult(Status status2, JSONObject message2, String cast2) {
        this.status = status2.ordinal();
        this.message = message2.toString();
        this.cast = cast2;
    }

    public PluginResult(Status status2, JSONArray message2) {
        this.status = status2.ordinal();
        this.message = message2.toString();
    }

    public PluginResult(Status status2, JSONObject message2) {
        this.status = status2.ordinal();
        this.message = message2.toString();
    }

    public PluginResult(Status status2, int i) {
        this.status = status2.ordinal();
        this.message = "" + i;
    }

    public PluginResult(Status status2, float f) {
        this.status = status2.ordinal();
        this.message = "" + f;
    }

    public PluginResult(Status status2, boolean b) {
        this.status = status2.ordinal();
        this.message = "" + b;
    }

    public void setKeepCallback(boolean b) {
        this.keepCallback = b;
    }

    public int getStatus() {
        return this.status;
    }

    public String getMessage() {
        return this.message;
    }

    public boolean getKeepCallback() {
        return this.keepCallback;
    }

    public String getJSONString() {
        return "{status:" + this.status + ",message:" + this.message + ",keepCallback:" + this.keepCallback + "}";
    }

    public String toSuccessCallbackString(String callbackId) {
        StringBuffer buf = new StringBuffer("");
        if (this.cast != null) {
            buf.append("var temp = " + this.cast + "(" + getJSONString() + ");\n");
            buf.append("PhoneGap.callbackSuccess('" + callbackId + "',temp);");
        } else {
            buf.append("PhoneGap.callbackSuccess('" + callbackId + "'," + getJSONString() + ");");
        }
        return buf.toString();
    }

    public String toErrorCallbackString(String callbackId) {
        return "PhoneGap.callbackError('" + callbackId + "', " + getJSONString() + ");";
    }
}
