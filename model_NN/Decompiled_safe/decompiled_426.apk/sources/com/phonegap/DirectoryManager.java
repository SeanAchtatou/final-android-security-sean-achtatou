package com.phonegap;

import android.os.Environment;
import android.os.StatFs;
import java.io.File;

public class DirectoryManager {
    private static final String LOG_TAG = "DirectoryManager";

    protected static boolean testFileExists(String name) {
        if (!testSaveLocationExists() || name.equals("")) {
            return false;
        }
        return constructFilePaths(Environment.getExternalStorageDirectory().toString(), name).exists();
    }

    protected static long getFreeDiskSpace() {
        long freeSpace = 0;
        if (!Environment.getExternalStorageState().equals("mounted")) {
            return -1;
        }
        try {
            StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
            freeSpace = (((long) stat.getAvailableBlocks()) * ((long) stat.getBlockSize())) / 1024;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return freeSpace;
    }

    protected static boolean testSaveLocationExists() {
        if (Environment.getExternalStorageState().equals("mounted")) {
            return true;
        }
        return false;
    }

    private static File constructFilePaths(String file1, String file2) {
        if (file2.startsWith(file1)) {
            return new File(file2);
        }
        return new File(file1 + "/" + file2);
    }
}
