package com.phonegap;

import com.phonegap.api.Plugin;
import com.phonegap.api.PluginResult;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;

public class GeoBroker extends Plugin {
    private HashMap<String, GeoListener> geoListeners = new HashMap<>();
    private GeoListener global;

    public PluginResult execute(String action, JSONArray args, String callbackId) {
        PluginResult.Status status = PluginResult.Status.OK;
        try {
            if (action.equals("getCurrentLocation")) {
                getCurrentLocation(args.getBoolean(0), args.getInt(1), args.getInt(2));
            } else if (action.equals("start")) {
                return new PluginResult(status, start(args.getString(0), args.getBoolean(1), args.getInt(2), args.getInt(3)));
            } else {
                if (action.equals("stop")) {
                    stop(args.getString(0));
                }
            }
            return new PluginResult(status, "");
        } catch (JSONException e) {
            return new PluginResult(PluginResult.Status.JSON_EXCEPTION);
        }
    }

    public boolean isSynch(String action) {
        return true;
    }

    public void onDestroy() {
        for (Map.Entry<String, GeoListener> entry : this.geoListeners.entrySet()) {
            ((GeoListener) entry.getValue()).destroy();
        }
        this.geoListeners.clear();
        if (this.global != null) {
            this.global.destroy();
        }
        this.global = null;
    }

    public void getCurrentLocation(boolean enableHighAccuracy, int timeout, int maximumAge) {
        if (this.global == null) {
            this.global = new GeoListener(this, "global", maximumAge);
        } else {
            this.global.start(maximumAge);
        }
    }

    public String start(String key, boolean enableHighAccuracy, int timeout, int maximumAge) {
        GeoListener listener = this.geoListeners.get(key);
        if (listener == null) {
            listener = new GeoListener(this, key, maximumAge);
            this.geoListeners.put(key, listener);
        }
        listener.start(maximumAge);
        return key;
    }

    public void stop(String key) {
        GeoListener listener = this.geoListeners.remove(key);
        if (listener != null) {
            listener.stop();
        }
    }
}
