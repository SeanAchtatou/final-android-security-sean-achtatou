package com.phonegap;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import com.phonegap.api.PhonegapActivity;
import com.phonegap.api.Plugin;
import com.phonegap.api.PluginResult;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;

public class CompassListener extends Plugin implements SensorEventListener {
    public static int ERROR_FAILED_TO_START = 3;
    public static int RUNNING = 2;
    public static int STARTING = 1;
    public static int STOPPED = 0;
    public long TIMEOUT = 30000;
    float heading;
    long lastAccessTime;
    Sensor mSensor;
    private SensorManager sensorManager;
    int status;
    long timeStamp = 0;

    public CompassListener() {
        setStatus(STOPPED);
    }

    public void setContext(PhonegapActivity ctx) {
        super.setContext(ctx);
        this.sensorManager = (SensorManager) ctx.getSystemService("sensor");
    }

    public PluginResult execute(String action, JSONArray args, String callbackId) {
        PluginResult.Status status2 = PluginResult.Status.OK;
        try {
            if (action.equals("start")) {
                start();
            } else if (action.equals("stop")) {
                stop();
            } else if (action.equals("getStatus")) {
                return new PluginResult(status2, getStatus());
            } else {
                if (action.equals("getHeading")) {
                    if (this.status != RUNNING) {
                        if (start() == ERROR_FAILED_TO_START) {
                            return new PluginResult(PluginResult.Status.IO_EXCEPTION, ERROR_FAILED_TO_START);
                        }
                        long timeout = 2000;
                        while (this.status == STARTING && timeout > 0) {
                            timeout -= 100;
                            try {
                                Thread.sleep(100);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                        if (timeout == 0) {
                            return new PluginResult(PluginResult.Status.IO_EXCEPTION, AccelListener.ERROR_FAILED_TO_START);
                        }
                    }
                    return new PluginResult(status2, getHeading());
                } else if (action.equals("setTimeout")) {
                    setTimeout(args.getLong(0));
                } else if (action.equals("getTimeout")) {
                    return new PluginResult(status2, (float) getTimeout());
                }
            }
            return new PluginResult(status2, "");
        } catch (JSONException e2) {
            e2.printStackTrace();
            return new PluginResult(PluginResult.Status.JSON_EXCEPTION);
        }
    }

    public boolean isSynch(String action) {
        if (action.equals("getStatus")) {
            return true;
        }
        if (action.equals("getHeading")) {
            if (this.status == RUNNING) {
                return true;
            }
        } else if (action.equals("getTimeout")) {
            return true;
        }
        return false;
    }

    public void onDestroy() {
        stop();
    }

    public int start() {
        if (this.status == RUNNING || this.status == STARTING) {
            return this.status;
        }
        List<Sensor> list = this.sensorManager.getSensorList(3);
        if (list.size() > 0) {
            this.mSensor = list.get(0);
            this.sensorManager.registerListener(this, this.mSensor, 3);
            this.lastAccessTime = System.currentTimeMillis();
            setStatus(STARTING);
        } else {
            setStatus(ERROR_FAILED_TO_START);
        }
        return this.status;
    }

    public void stop() {
        if (this.status != STOPPED) {
            this.sensorManager.unregisterListener(this);
        }
        setStatus(STOPPED);
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    public void onSensorChanged(SensorEvent event) {
        float heading2 = event.values[0];
        this.timeStamp = System.currentTimeMillis();
        this.heading = heading2;
        setStatus(RUNNING);
        if (this.timeStamp - this.lastAccessTime > this.TIMEOUT) {
            stop();
        }
    }

    public int getStatus() {
        return this.status;
    }

    public float getHeading() {
        this.lastAccessTime = System.currentTimeMillis();
        return this.heading;
    }

    public void setTimeout(long timeout) {
        this.TIMEOUT = timeout;
    }

    public long getTimeout() {
        return this.TIMEOUT;
    }

    private void setStatus(int status2) {
        this.status = status2;
    }
}
