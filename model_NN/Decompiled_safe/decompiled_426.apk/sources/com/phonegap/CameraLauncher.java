package com.phonegap;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import com.phonegap.api.Plugin;
import com.phonegap.api.PluginResult;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import org.apache.commons.codec.binary.Base64;
import org.json.JSONArray;
import org.json.JSONException;

public class CameraLauncher extends Plugin {
    private static final int CAMERA = 1;
    private static final int DATA_URL = 0;
    private static final int FILE_URI = 1;
    private static final int PHOTOLIBRARY = 0;
    private static final int SAVEDPHOTOALBUM = 2;
    public String callbackId;
    private Uri imageUri;
    private int mQuality;

    public PluginResult execute(String action, JSONArray args, String callbackId2) {
        PluginResult.Status status = PluginResult.Status.OK;
        this.callbackId = callbackId2;
        try {
            if (!action.equals("takePicture")) {
                return new PluginResult(status, "");
            }
            int destType = 0;
            if (args.length() > 1) {
                destType = args.getInt(1);
            }
            int srcType = 1;
            if (args.length() > SAVEDPHOTOALBUM) {
                srcType = args.getInt(SAVEDPHOTOALBUM);
            }
            if (srcType == 1) {
                takePicture(args.getInt(0), destType);
            } else if (srcType == 0 || srcType == SAVEDPHOTOALBUM) {
                getImage(args.getInt(0), srcType, destType);
            }
            PluginResult r = new PluginResult(PluginResult.Status.NO_RESULT);
            r.setKeepCallback(true);
            return r;
        } catch (JSONException e) {
            e.printStackTrace();
            return new PluginResult(PluginResult.Status.JSON_EXCEPTION);
        }
    }

    public void takePicture(int quality, int returnType) {
        this.mQuality = quality;
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        File photo = new File(Environment.getExternalStorageDirectory(), "Pic.jpg");
        intent.putExtra("output", Uri.fromFile(photo));
        this.imageUri = Uri.fromFile(photo);
        this.ctx.startActivityForResult(this, intent, returnType + 32 + 1);
    }

    public void getImage(int quality, int srcType, int returnType) {
        this.mQuality = quality;
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction("android.intent.action.GET_CONTENT");
        intent.addCategory("android.intent.category.OPENABLE");
        this.ctx.startActivityForResult(this, Intent.createChooser(intent, new String("Get Picture")), ((srcType + 1) * 16) + returnType + 1);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        Bitmap bitmap;
        Uri uri;
        int srcType = (requestCode / 16) - 1;
        int destType = (requestCode % 16) - 1;
        if (srcType == 1) {
            if (resultCode == -1) {
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(this.ctx.getContentResolver(), this.imageUri);
                } catch (FileNotFoundException e) {
                    FileNotFoundException fileNotFoundException = e;
                    bitmap = BitmapFactory.decodeStream(this.ctx.getContentResolver().openInputStream(intent.getData()));
                }
                if (destType == 0) {
                    try {
                        processPicture(bitmap);
                    } catch (IOException e2) {
                        e2.printStackTrace();
                        failPicture("Error capturing image.");
                        return;
                    }
                } else if (destType == 1) {
                    ContentValues values = new ContentValues();
                    values.put("mime_type", "image/jpeg");
                    try {
                        uri = this.ctx.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                    } catch (UnsupportedOperationException e3) {
                        System.out.println("Can't write to external media storage.");
                        try {
                            uri = this.ctx.getContentResolver().insert(MediaStore.Images.Media.INTERNAL_CONTENT_URI, values);
                        } catch (UnsupportedOperationException e4) {
                            System.out.println("Can't write to internal media storage.");
                            failPicture("Error capturing image - no media storage found.");
                            return;
                        }
                    }
                    OutputStream os = this.ctx.getContentResolver().openOutputStream(uri);
                    bitmap.compress(Bitmap.CompressFormat.JPEG, this.mQuality, os);
                    os.close();
                    success(new PluginResult(PluginResult.Status.OK, uri.toString()), this.callbackId);
                }
                bitmap.recycle();
                System.gc();
            } else if (resultCode == 0) {
                failPicture("Camera cancelled.");
            } else {
                failPicture("Did not complete!");
            }
        } else if (srcType != 0 && srcType != SAVEDPHOTOALBUM) {
        } else {
            if (resultCode == -1) {
                Uri uri2 = intent.getData();
                ContentResolver resolver = this.ctx.getContentResolver();
                if (destType == 0) {
                    try {
                        Bitmap bitmap2 = BitmapFactory.decodeStream(resolver.openInputStream(uri2));
                        processPicture(bitmap2);
                        bitmap2.recycle();
                        System.gc();
                    } catch (FileNotFoundException e5) {
                        e5.printStackTrace();
                        failPicture("Error retrieving image.");
                    }
                } else if (destType == 1) {
                    success(new PluginResult(PluginResult.Status.OK, uri2.toString()), this.callbackId);
                }
            } else if (resultCode == 0) {
                failPicture("Selection cancelled.");
            } else {
                failPicture("Selection did not complete!");
            }
        }
    }

    public void processPicture(Bitmap bitmap) {
        ByteArrayOutputStream jpeg_data = new ByteArrayOutputStream();
        try {
            if (bitmap.compress(Bitmap.CompressFormat.JPEG, this.mQuality, jpeg_data)) {
                success(new PluginResult(PluginResult.Status.OK, new String(Base64.encodeBase64(jpeg_data.toByteArray()))), this.callbackId);
            }
        } catch (Exception e) {
            failPicture("Error compressing image.");
        }
    }

    public void failPicture(String err) {
        error(new PluginResult(PluginResult.Status.ERROR, err), this.callbackId);
    }
}
