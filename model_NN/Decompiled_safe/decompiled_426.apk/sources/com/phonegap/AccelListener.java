package com.phonegap;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import com.phonegap.api.PhonegapActivity;
import com.phonegap.api.Plugin;
import com.phonegap.api.PluginResult;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AccelListener extends Plugin implements SensorEventListener {
    public static int ERROR_FAILED_TO_START = 3;
    public static int RUNNING = 2;
    public static int STARTING = 1;
    public static int STOPPED = 0;
    public float TIMEOUT = 30000.0f;
    long lastAccessTime;
    Sensor mSensor;
    private SensorManager sensorManager;
    int status;
    long timestamp = 0;
    float x = 0.0f;
    float y = 0.0f;
    float z = 0.0f;

    public AccelListener() {
        setStatus(STOPPED);
    }

    public void setContext(PhonegapActivity ctx) {
        super.setContext(ctx);
        this.sensorManager = (SensorManager) ctx.getSystemService("sensor");
    }

    public PluginResult execute(String action, JSONArray args, String callbackId) {
        PluginResult.Status status2 = PluginResult.Status.OK;
        try {
            if (action.equals("getStatus")) {
                return new PluginResult(status2, getStatus());
            }
            if (action.equals("start")) {
                return new PluginResult(status2, start());
            }
            if (action.equals("stop")) {
                stop();
                return new PluginResult(status2, 0);
            } else if (action.equals("getAcceleration")) {
                if (this.status != RUNNING) {
                    if (start() == ERROR_FAILED_TO_START) {
                        return new PluginResult(PluginResult.Status.IO_EXCEPTION, ERROR_FAILED_TO_START);
                    }
                    long timeout = 2000;
                    while (this.status == STARTING && timeout > 0) {
                        timeout -= 100;
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    if (timeout == 0) {
                        return new PluginResult(PluginResult.Status.IO_EXCEPTION, ERROR_FAILED_TO_START);
                    }
                }
                this.lastAccessTime = System.currentTimeMillis();
                JSONObject r = new JSONObject();
                r.put("x", (double) this.x);
                r.put("y", (double) this.y);
                r.put("z", (double) this.z);
                r.put("timestamp", this.timestamp);
                return new PluginResult(status2, r);
            } else if (action.equals("setTimeout")) {
                try {
                    setTimeout(Float.parseFloat(args.getString(0)));
                    return new PluginResult(status2, 0);
                } catch (NumberFormatException e2) {
                    NumberFormatException e3 = e2;
                    status2 = PluginResult.Status.INVALID_ACTION;
                    e3.printStackTrace();
                } catch (JSONException e4) {
                    status2 = PluginResult.Status.JSON_EXCEPTION;
                    e4.printStackTrace();
                }
            } else {
                if (action.equals("getTimeout")) {
                    return new PluginResult(status2, getTimeout());
                }
                return new PluginResult(status2, "");
            }
        } catch (JSONException e5) {
            return new PluginResult(PluginResult.Status.JSON_EXCEPTION);
        }
    }

    public boolean isSynch(String action) {
        if (action.equals("getStatus")) {
            return true;
        }
        if (action.equals("getAcceleration")) {
            if (this.status == RUNNING) {
                return true;
            }
        } else if (action.equals("getTimeout")) {
            return true;
        }
        return false;
    }

    public void onDestroy() {
        stop();
    }

    public int start() {
        if (this.status == RUNNING || this.status == STARTING) {
            return this.status;
        }
        List<Sensor> list = this.sensorManager.getSensorList(1);
        if (list == null || list.size() <= 0) {
            setStatus(ERROR_FAILED_TO_START);
        } else {
            this.mSensor = list.get(0);
            this.sensorManager.registerListener(this, this.mSensor, 0);
            setStatus(STARTING);
            this.lastAccessTime = System.currentTimeMillis();
        }
        return this.status;
    }

    public void stop() {
        if (this.status != STOPPED) {
            this.sensorManager.unregisterListener(this);
        }
        setStatus(STOPPED);
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == 1 && this.status != STOPPED) {
            this.timestamp = System.currentTimeMillis();
            this.x = event.values[0];
            this.y = event.values[1];
            this.z = event.values[2];
            setStatus(RUNNING);
            if (((float) (this.timestamp - this.lastAccessTime)) > this.TIMEOUT) {
                stop();
            }
        }
    }

    public int getStatus() {
        return this.status;
    }

    public void setTimeout(float timeout) {
        this.TIMEOUT = timeout;
    }

    public float getTimeout() {
        return this.TIMEOUT;
    }

    private void setStatus(int status2) {
        this.status = status2;
    }
}
