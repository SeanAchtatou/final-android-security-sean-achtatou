package com.phonegap;

import android.app.Activity;
import android.os.Build;
import android.util.Log;
import android.webkit.WebView;
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class ContactAccessor {
    private static ContactAccessor sInstance;
    protected final String LOG_TAG = "ContactsAccessor";
    protected Activity mApp;
    protected WebView mView;

    public abstract boolean remove(String str);

    public abstract boolean save(JSONObject jSONObject);

    public abstract JSONArray search(JSONArray jSONArray, JSONObject jSONObject);

    public static ContactAccessor getInstance(WebView view, Activity app) {
        String className;
        if (sInstance == null) {
            if (Build.VERSION.RELEASE.startsWith("1.")) {
                className = "com.phonegap.ContactAccessorSdk3_4";
            } else {
                className = "com.phonegap.ContactAccessorSdk5";
            }
            try {
                sInstance = (ContactAccessor) Class.forName(className).asSubclass(ContactAccessor.class).getConstructor(Class.forName("android.webkit.WebView"), Class.forName("android.app.Activity")).newInstance(view, app);
            } catch (Exception e) {
                throw new IllegalStateException(e);
            }
        }
        return sInstance;
    }

    /* access modifiers changed from: protected */
    public boolean isRequired(String key, HashMap<String, Boolean> map) {
        Boolean retVal = map.get(key);
        if (retVal == null) {
            return false;
        }
        return retVal.booleanValue();
    }

    /* access modifiers changed from: protected */
    public HashMap<String, Boolean> buildPopulationSet(JSONArray fields) {
        HashMap<String, Boolean> map = new HashMap<>();
        int i = 0;
        while (i < fields.length()) {
            try {
                String key = fields.getString(i);
                if (key.startsWith("displayName")) {
                    map.put("displayName", true);
                } else if (key.startsWith("name")) {
                    map.put("name", true);
                } else if (key.startsWith("nickname")) {
                    map.put("nickname", true);
                } else if (key.startsWith("phoneNumbers")) {
                    map.put("phoneNumbers", true);
                } else if (key.startsWith("emails")) {
                    map.put("emails", true);
                } else if (key.startsWith("addresses")) {
                    map.put("addresses", true);
                } else if (key.startsWith("ims")) {
                    map.put("ims", true);
                } else if (key.startsWith("organizations")) {
                    map.put("organizations", true);
                } else if (key.startsWith("birthday")) {
                    map.put("birthday", true);
                } else if (key.startsWith("anniversary")) {
                    map.put("anniversary", true);
                } else if (key.startsWith("note")) {
                    map.put("note", true);
                } else if (key.startsWith("relationships")) {
                    map.put("relationships", true);
                } else if (key.startsWith("urls")) {
                    map.put("urls", true);
                } else if (key.startsWith("photos")) {
                    map.put("photos", true);
                }
                i++;
            } catch (JSONException e) {
                JSONException e2 = e;
                Log.e("ContactsAccessor", e2.getMessage(), e2);
            }
        }
        return map;
    }

    /* access modifiers changed from: protected */
    public String getJsonString(JSONObject obj, String property) {
        if (obj == null) {
            return null;
        }
        try {
            String value = obj.getString(property);
            if (!value.equals("null")) {
                return value;
            }
            Log.d("ContactsAccessor", property + " is string called 'null'");
            return null;
        } catch (JSONException e) {
            Log.d("ContactsAccessor", "Could not get = " + e.getMessage());
            return null;
        }
    }

    class WhereOptions {
        private String where;
        private String[] whereArgs;

        WhereOptions() {
        }

        public void setWhere(String where2) {
            this.where = where2;
        }

        public String getWhere() {
            return this.where;
        }

        public void setWhereArgs(String[] whereArgs2) {
            this.whereArgs = whereArgs2;
        }

        public String[] getWhereArgs() {
            return this.whereArgs;
        }
    }
}
