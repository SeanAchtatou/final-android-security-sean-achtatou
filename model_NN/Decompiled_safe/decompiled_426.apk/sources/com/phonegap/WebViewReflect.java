package com.phonegap;

import android.webkit.WebSettings;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class WebViewReflect {
    private static Method mWebSettings_setDatabaseEnabled;
    private static Method mWebSettings_setDatabasePath;
    private static Method mWebSettings_setDomStorageEnabled;
    private static Method mWebSettings_setGeolocationEnabled;

    static {
        checkCompatibility();
    }

    private static void setDatabaseEnabled(boolean e) throws IOException {
        try {
            mWebSettings_setDatabaseEnabled.invoke(Boolean.valueOf(e), new Object[0]);
        } catch (InvocationTargetException e2) {
            InvocationTargetException ite = e2;
            Throwable cause = ite.getCause();
            if (cause instanceof IOException) {
                throw ((IOException) cause);
            } else if (cause instanceof RuntimeException) {
                throw ((RuntimeException) cause);
            } else if (cause instanceof Error) {
                throw ((Error) cause);
            } else {
                throw new RuntimeException(ite);
            }
        } catch (IllegalAccessException e3) {
            System.err.println("unexpected " + e3);
        }
    }

    public static void checkCompatibility() {
        Class<WebSettings> cls = WebSettings.class;
        Class<WebSettings> cls2 = WebSettings.class;
        try {
            mWebSettings_setDatabaseEnabled = cls2.getMethod("setDatabaseEnabled", Boolean.TYPE);
            mWebSettings_setDatabasePath = WebSettings.class.getMethod("setDatabasePath", String.class);
            mWebSettings_setDomStorageEnabled = WebSettings.class.getMethod("setDomStorageEnabled", Boolean.TYPE);
            mWebSettings_setGeolocationEnabled = WebSettings.class.getMethod("setGeolocationEnabled", Boolean.TYPE);
        } catch (NoSuchMethodException e) {
        }
    }

    public static void setStorage(WebSettings setting, boolean enable, String path) {
        if (mWebSettings_setDatabaseEnabled != null) {
            try {
                mWebSettings_setDatabaseEnabled.invoke(setting, Boolean.valueOf(enable));
                mWebSettings_setDatabasePath.invoke(setting, path);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e2) {
                e2.printStackTrace();
            } catch (InvocationTargetException e3) {
                e3.printStackTrace();
            }
        }
    }

    public static void setGeolocationEnabled(WebSettings setting, boolean enable) {
        if (mWebSettings_setGeolocationEnabled != null) {
            try {
                mWebSettings_setGeolocationEnabled.invoke(setting, Boolean.valueOf(enable));
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e2) {
                e2.printStackTrace();
            } catch (InvocationTargetException e3) {
                e3.printStackTrace();
            }
        } else {
            System.out.println("Native Geolocation not supported - we're ok");
        }
    }

    public static void setDomStorage(WebSettings setting) {
        if (mWebSettings_setDomStorageEnabled != null) {
            try {
                mWebSettings_setDomStorageEnabled.invoke(setting, true);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e2) {
                e2.printStackTrace();
            } catch (InvocationTargetException e3) {
                e3.printStackTrace();
            }
        }
    }
}
