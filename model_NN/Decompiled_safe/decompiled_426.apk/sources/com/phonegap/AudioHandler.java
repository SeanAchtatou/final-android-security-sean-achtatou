package com.phonegap;

import android.media.AudioManager;
import com.phonegap.api.Plugin;
import com.phonegap.api.PluginResult;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;

public class AudioHandler extends Plugin {
    HashMap<String, AudioPlayer> players = new HashMap<>();

    public PluginResult execute(String action, JSONArray args, String callbackId) {
        PluginResult.Status status = PluginResult.Status.OK;
        try {
            if (action.equals("startRecordingAudio")) {
                startRecordingAudio(args.getString(0), args.getString(1));
            } else if (action.equals("stopRecordingAudio")) {
                stopRecordingAudio(args.getString(0));
            } else if (action.equals("startPlayingAudio")) {
                startPlayingAudio(args.getString(0), args.getString(1));
            } else if (action.equals("seekToAudio")) {
                seekToAudio(args.getString(0), args.getInt(1));
            } else if (action.equals("pausePlayingAudio")) {
                pausePlayingAudio(args.getString(0));
            } else if (action.equals("stopPlayingAudio")) {
                stopPlayingAudio(args.getString(0));
            } else if (action.equals("getCurrentPositionAudio")) {
                return new PluginResult(status, getCurrentPositionAudio(args.getString(0)));
            } else {
                if (action.equals("getDurationAudio")) {
                    return new PluginResult(status, getDurationAudio(args.getString(0), args.getString(1)));
                }
                if (action.equals("release")) {
                    return new PluginResult(status, release(args.getString(0)));
                }
            }
            return new PluginResult(status, "");
        } catch (JSONException e) {
            e.printStackTrace();
            return new PluginResult(PluginResult.Status.JSON_EXCEPTION);
        }
    }

    public boolean isSynch(String action) {
        if (action.equals("getCurrentPositionAudio")) {
            return true;
        }
        if (action.equals("getDurationAudio")) {
            return true;
        }
        return false;
    }

    public void onDestroy() {
        for (Map.Entry<String, AudioPlayer> entry : this.players.entrySet()) {
            ((AudioPlayer) entry.getValue()).destroy();
        }
        this.players.clear();
    }

    private boolean release(String id) {
        if (!this.players.containsKey(id)) {
            return false;
        }
        this.players.remove(id);
        this.players.get(id).destroy();
        return true;
    }

    public void startRecordingAudio(String id, String file) {
        if (!this.players.containsKey(id)) {
            AudioPlayer audio = new AudioPlayer(this, id);
            this.players.put(id, audio);
            audio.startRecording(file);
        }
    }

    public void stopRecordingAudio(String id) {
        AudioPlayer audio = this.players.get(id);
        if (audio != null) {
            audio.stopRecording();
            this.players.remove(id);
        }
    }

    public void startPlayingAudio(String id, String file) {
        AudioPlayer audio = this.players.get(id);
        if (audio == null) {
            audio = new AudioPlayer(this, id);
            this.players.put(id, audio);
        }
        audio.startPlaying(file);
    }

    public void seekToAudio(String id, int milliseconds) {
        AudioPlayer audio = this.players.get(id);
        if (audio != null) {
            audio.seekToPlaying(milliseconds);
        }
    }

    public void pausePlayingAudio(String id) {
        AudioPlayer audio = this.players.get(id);
        if (audio != null) {
            audio.pausePlaying();
        }
    }

    public void stopPlayingAudio(String id) {
        AudioPlayer audio = this.players.get(id);
        if (audio != null) {
            audio.stopPlaying();
        }
    }

    public float getCurrentPositionAudio(String id) {
        AudioPlayer audio = this.players.get(id);
        if (audio != null) {
            return ((float) audio.getCurrentPosition()) / 1000.0f;
        }
        return -1.0f;
    }

    public float getDurationAudio(String id, String file) {
        AudioPlayer audio = this.players.get(id);
        if (audio != null) {
            return audio.getDuration(file);
        }
        AudioPlayer audio2 = new AudioPlayer(this, id);
        this.players.put(id, audio2);
        return audio2.getDuration(file);
    }

    public void setAudioOutputDevice(int output) {
        AudioManager audiMgr = (AudioManager) this.ctx.getSystemService("audio");
        if (output == 2) {
            audiMgr.setRouting(0, 2, -1);
        } else if (output == 1) {
            audiMgr.setRouting(0, 1, -1);
        } else {
            System.out.println("AudioHandler.setAudioOutputDevice() Error: Unknown output device.");
        }
    }

    public int getAudioOutputDevice() {
        AudioManager audiMgr = (AudioManager) this.ctx.getSystemService("audio");
        if (audiMgr.getRouting(0) == 1) {
            return 1;
        }
        if (audiMgr.getRouting(0) == 2) {
            return 2;
        }
        return -1;
    }
}
