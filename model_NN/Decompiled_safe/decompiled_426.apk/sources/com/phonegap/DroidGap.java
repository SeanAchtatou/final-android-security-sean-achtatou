package com.phonegap;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.GeolocationPermissions;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebStorage;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.LinearLayout;
import com.phonegap.api.PhonegapActivity;
import com.phonegap.api.Plugin;
import com.phonegap.api.PluginManager;
import org.json.JSONArray;
import org.json.JSONException;

public class DroidGap extends PhonegapActivity {
    protected Plugin activityResultCallback = null;
    protected boolean activityResultKeepRunning;
    protected WebView appView;
    /* access modifiers changed from: private */
    public String baseUrl;
    public boolean bound = false;
    public CallbackServer callbackServer;
    protected boolean cancelLoadUrl = false;
    protected boolean clearHistory = false;
    protected boolean hideLoadingDialogOnPageLoad = false;
    protected boolean keepRunning = true;
    protected boolean loadInWebView = false;
    /* access modifiers changed from: private */
    public int loadUrlTimeout = 0;
    protected int loadUrlTimeoutValue = 20000;
    protected PluginManager pluginManager;
    protected LinearLayout root;
    protected int splashscreen = 0;
    private String url;
    /* access modifiers changed from: private */
    public String urlFile;
    protected WebViewClient webViewClient;

    static /* synthetic */ int access$108(DroidGap x0) {
        int i = x0.loadUrlTimeout;
        x0.loadUrlTimeout = i + 1;
        return i;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(1);
        getWindow().setFlags(2048, 2048);
        Display display = getWindowManager().getDefaultDisplay();
        this.root = new LinearLayoutSoftKeyboardDetect(this, display.getWidth(), display.getHeight());
        this.root.setOrientation(1);
        this.root.setBackgroundColor(-16777216);
        this.root.setLayoutParams(new LinearLayout.LayoutParams(-1, -1, 0.0f));
        Bundle bundle = getIntent().getExtras();
        if (!(bundle == null || bundle.getString("url") == null)) {
            init();
        }
        setVolumeControlStream(3);
    }

    public void init() {
        this.appView = new WebView(this);
        this.appView.setId(100);
        this.appView.setLayoutParams(new LinearLayout.LayoutParams(-1, -1, 1.0f));
        WebViewReflect.checkCompatibility();
        if (Build.VERSION.RELEASE.startsWith("1.")) {
            this.appView.setWebChromeClient(new GapClient(this));
        } else {
            this.appView.setWebChromeClient(new EclairClient(this));
        }
        setWebViewClient(this.appView, new GapViewClient(this));
        this.appView.setInitialScale(100);
        this.appView.setVerticalScrollBarEnabled(false);
        this.appView.requestFocusFromTouch();
        WebSettings settings = this.appView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NORMAL);
        WebViewReflect.setStorage(settings, true, "/data/data/" + getClass().getPackage().getName() + "/app_database/");
        WebViewReflect.setDomStorage(settings);
        WebViewReflect.setGeolocationEnabled(settings, true);
        bindBrowser(this.appView);
        this.appView.setVisibility(4);
        this.root.addView(this.appView);
        setContentView(this.root);
        this.cancelLoadUrl = false;
        String url2 = getStringProperty("url", null);
        if (url2 != null) {
            System.out.println("Loading initial URL=" + url2);
            loadUrl(url2);
        }
    }

    /* access modifiers changed from: protected */
    public void setWebViewClient(WebView appView2, WebViewClient client) {
        this.webViewClient = client;
        appView2.setWebViewClient(client);
    }

    private void bindBrowser(WebView appView2) {
        this.callbackServer = new CallbackServer();
        this.pluginManager = new PluginManager(appView2, this);
        addService("App", "com.phonegap.App");
        addService("Geolocation", "com.phonegap.GeoBroker");
        addService("Device", "com.phonegap.Device");
        addService("Accelerometer", "com.phonegap.AccelListener");
        addService("Compass", "com.phonegap.CompassListener");
        addService("Media", "com.phonegap.AudioHandler");
        addService("Camera", "com.phonegap.CameraLauncher");
        addService("Contacts", "com.phonegap.ContactManager");
        addService("Crypto", "com.phonegap.CryptoHandler");
        addService("File", "com.phonegap.FileUtils");
        addService("Location", "com.phonegap.GeoBroker");
        addService("Network Status", "com.phonegap.NetworkManager");
        addService("Notification", "com.phonegap.Notification");
        addService("Storage", "com.phonegap.Storage");
        addService("Temperature", "com.phonegap.TempListener");
        addService("FileTransfer", "com.phonegap.FileTransfer");
        addService("Capture", "com.phonegap.Capture");
    }

    /* access modifiers changed from: private */
    public void handleActivityParameters() {
        if (this.appView == null) {
            init();
        }
        this.splashscreen = getIntegerProperty("splashscreen", 0);
        if (this.splashscreen != 0) {
            this.root.setBackgroundResource(this.splashscreen);
        }
        this.hideLoadingDialogOnPageLoad = getBooleanProperty("hideLoadingDialogOnPageLoad", false);
        this.loadInWebView = getBooleanProperty("loadInWebView", false);
        int timeout = getIntegerProperty("loadUrlTimeoutValue", 0);
        if (timeout > 0) {
            this.loadUrlTimeoutValue = timeout;
        }
        this.keepRunning = getBooleanProperty("keepRunning", true);
    }

    public void loadUrl(final String url2) {
        System.out.println("loadUrl(" + url2 + ")");
        this.urlFile = getUrlFile(url2);
        this.url = url2;
        int i = url2.lastIndexOf(47);
        if (i > 0) {
            this.baseUrl = url2.substring(0, i);
        } else {
            this.baseUrl = this.url;
        }
        System.out.println("url=" + url2 + " baseUrl=" + this.baseUrl);
        runOnUiThread(new Runnable() {
            public void run() {
                this.handleActivityParameters();
                this.callbackServer.init(url2);
                String loading = this.getStringProperty("loadingDialog", null);
                if (loading != null) {
                    String title = "";
                    String message = "Loading Application...";
                    if (loading.length() > 0) {
                        int comma = loading.indexOf(44);
                        if (comma > 0) {
                            title = loading.substring(0, comma);
                            message = loading.substring(comma + 1);
                        } else {
                            title = "";
                            message = loading;
                        }
                    }
                    JSONArray parm = new JSONArray();
                    parm.put(title);
                    parm.put(message);
                    this.pluginManager.exec("Notification", "activityStart", null, parm.toString(), false);
                }
                final int currentLoadUrlTimeout = this.loadUrlTimeout;
                new Thread(new Runnable() {
                    public void run() {
                        try {
                            synchronized (this) {
                                wait((long) this.loadUrlTimeoutValue);
                            }
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        if (this.loadUrlTimeout == currentLoadUrlTimeout) {
                            this.appView.stopLoading();
                            this.webViewClient.onReceivedError(this.appView, -6, "The connection to the server was unsuccessful.", url2);
                        }
                    }
                }).start();
                this.appView.loadUrl(url2);
            }
        });
    }

    public void loadUrl(final String url2, final int time) {
        System.out.println("loadUrl(" + url2 + "," + time + ")");
        runOnUiThread(new Runnable() {
            public void run() {
                this.handleActivityParameters();
            }
        });
        new Thread(new Runnable() {
            public void run() {
                try {
                    synchronized (this) {
                        wait((long) time);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (!this.cancelLoadUrl) {
                    this.loadUrl(url2);
                    return;
                }
                this.cancelLoadUrl = false;
                System.out.println("Aborting loadUrl(" + url2 + "): Another URL was loaded before timer expired.");
            }
        }).start();
    }

    public void cancelLoadUrl() {
        this.cancelLoadUrl = true;
    }

    public void clearCache() {
        if (this.appView == null) {
            init();
        }
        this.appView.clearCache(true);
    }

    public void clearHistory() {
        this.clearHistory = true;
        if (this.appView != null) {
            this.appView.clearHistory();
        }
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    public boolean getBooleanProperty(String name, boolean defaultValue) {
        Bundle bundle = getIntent().getExtras();
        if (bundle == null) {
            return defaultValue;
        }
        Boolean p = (Boolean) bundle.get(name);
        if (p == null) {
            return defaultValue;
        }
        return p.booleanValue();
    }

    public int getIntegerProperty(String name, int defaultValue) {
        Bundle bundle = getIntent().getExtras();
        if (bundle == null) {
            return defaultValue;
        }
        Integer p = (Integer) bundle.get(name);
        if (p == null) {
            return defaultValue;
        }
        return p.intValue();
    }

    public String getStringProperty(String name, String defaultValue) {
        Bundle bundle = getIntent().getExtras();
        if (bundle == null) {
            return defaultValue;
        }
        String p = bundle.getString(name);
        if (p == null) {
            return defaultValue;
        }
        return p;
    }

    public double getDoubleProperty(String name, double defaultValue) {
        Bundle bundle = getIntent().getExtras();
        if (bundle == null) {
            return defaultValue;
        }
        Double p = (Double) bundle.get(name);
        if (p == null) {
            return defaultValue;
        }
        return p.doubleValue();
    }

    public void setBooleanProperty(String name, boolean value) {
        getIntent().putExtra(name, value);
    }

    public void setIntegerProperty(String name, int value) {
        getIntent().putExtra(name, value);
    }

    public void setStringProperty(String name, String value) {
        getIntent().putExtra(name, value);
    }

    public void setDoubleProperty(String name, double value) {
        getIntent().putExtra(name, value);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.appView != null) {
            this.appView.loadUrl("javascript:try{PhoneGap.onPause.fire();}catch(e){};");
            this.pluginManager.onPause(this.keepRunning);
            if (!this.keepRunning) {
                this.appView.pauseTimers();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        this.pluginManager.onNewIntent(intent);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.appView != null) {
            this.appView.loadUrl("javascript:try{PhoneGap.onResume.fire();}catch(e){};");
            this.pluginManager.onResume(this.keepRunning || this.activityResultKeepRunning);
            if (!this.keepRunning || this.activityResultKeepRunning) {
                if (this.activityResultKeepRunning) {
                    this.keepRunning = this.activityResultKeepRunning;
                    this.activityResultKeepRunning = false;
                }
                this.appView.resumeTimers();
            }
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.appView != null) {
            this.appView.loadUrl("javascript:try{PhoneGap.onPause.fire();}catch(e){};");
            this.appView.loadUrl("javascript:try{PhoneGap.onDestroy.fire();}catch(e){};");
            this.appView.loadUrl("about:blank");
            this.pluginManager.onDestroy();
        }
    }

    public void addService(String serviceType, String className) {
        this.pluginManager.addService(serviceType, className);
    }

    public void sendJavascript(String statement) {
        this.callbackServer.sendJavascript(statement);
    }

    /* access modifiers changed from: private */
    public String getUrlFile(String url2) {
        int p3;
        int p1 = url2.indexOf("#");
        int p2 = url2.indexOf("?");
        if (p1 < 0) {
            p1 = url2.length();
        }
        if (p2 < 0) {
            p2 = url2.length();
        }
        if (p1 < p2) {
            p3 = p1;
        } else {
            p3 = p2;
        }
        return url2.substring(0, p3);
    }

    public class GapClient extends WebChromeClient {
        private DroidGap ctx;

        public GapClient(Context ctx2) {
            this.ctx = (DroidGap) ctx2;
        }

        public boolean onJsAlert(WebView view, String url, String message, final JsResult result) {
            AlertDialog.Builder dlg = new AlertDialog.Builder(this.ctx);
            dlg.setMessage(message);
            dlg.setTitle("Alert");
            dlg.setCancelable(false);
            dlg.setPositiveButton(17039370, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    result.confirm();
                }
            });
            dlg.create();
            dlg.show();
            return true;
        }

        public boolean onJsConfirm(WebView view, String url, String message, final JsResult result) {
            AlertDialog.Builder dlg = new AlertDialog.Builder(this.ctx);
            dlg.setMessage(message);
            dlg.setTitle("Confirm");
            dlg.setCancelable(false);
            dlg.setPositiveButton(17039370, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    result.confirm();
                }
            });
            dlg.setNegativeButton(17039360, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    result.cancel();
                }
            });
            dlg.create();
            dlg.show();
            return true;
        }

        public boolean onJsPrompt(WebView view, String url, String message, String defaultValue, JsPromptResult result) {
            boolean reqOk = false;
            if (this.ctx.urlFile.equals(this.ctx.getUrlFile(url))) {
                reqOk = true;
            }
            if (reqOk && defaultValue != null && defaultValue.length() > 3 && defaultValue.substring(0, 4).equals("gap:")) {
                try {
                    JSONArray array = new JSONArray(defaultValue.substring(4));
                    result.confirm(DroidGap.this.pluginManager.exec(array.getString(0), array.getString(1), array.getString(2), message, array.getBoolean(3)));
                    return true;
                } catch (JSONException e) {
                    e.printStackTrace();
                    return true;
                }
            } else if (reqOk && defaultValue.equals("gap_poll:")) {
                result.confirm(DroidGap.this.callbackServer.getJavascript());
                return true;
            } else if (!reqOk || !defaultValue.equals("gap_callbackServer:")) {
                JsPromptResult res = result;
                AlertDialog.Builder dlg = new AlertDialog.Builder(this.ctx);
                dlg.setMessage(message);
                EditText input = new EditText(this.ctx);
                dlg.setView(input);
                dlg.setCancelable(false);
                final EditText editText = input;
                final JsPromptResult jsPromptResult = res;
                dlg.setPositiveButton(17039370, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        jsPromptResult.confirm(editText.getText().toString());
                    }
                });
                final JsPromptResult jsPromptResult2 = res;
                dlg.setNegativeButton(17039360, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        jsPromptResult2.cancel();
                    }
                });
                dlg.create();
                dlg.show();
                return true;
            } else {
                String r = "";
                if (message.equals("usePolling")) {
                    r = "" + DroidGap.this.callbackServer.usePolling();
                } else if (message.equals("restartServer")) {
                    DroidGap.this.callbackServer.restartServer();
                } else if (message.equals("getPort")) {
                    r = Integer.toString(DroidGap.this.callbackServer.getPort());
                } else if (message.equals("getToken")) {
                    r = DroidGap.this.callbackServer.getToken();
                }
                result.confirm(r);
                return true;
            }
        }
    }

    public class EclairClient extends GapClient {
        private long MAX_QUOTA = 104857600;
        private String TAG = "PhoneGapLog";

        public EclairClient(Context ctx) {
            super(ctx);
        }

        public void onExceededDatabaseQuota(String url, String databaseIdentifier, long currentQuota, long estimatedSize, long totalUsedQuota, WebStorage.QuotaUpdater quotaUpdater) {
            Log.d(this.TAG, "event raised onExceededDatabaseQuota estimatedSize: " + Long.toString(estimatedSize) + " currentQuota: " + Long.toString(currentQuota) + " totalUsedQuota: " + Long.toString(totalUsedQuota));
            if (estimatedSize < this.MAX_QUOTA) {
                long newQuota = estimatedSize;
                Log.d(this.TAG, "calling quotaUpdater.updateQuota newQuota: " + Long.toString(newQuota));
                quotaUpdater.updateQuota(newQuota);
                return;
            }
            quotaUpdater.updateQuota(currentQuota);
        }

        public void onConsoleMessage(String message, int lineNumber, String sourceID) {
            Log.d(this.TAG, sourceID + ": Line " + Integer.toString(lineNumber) + " : " + message);
        }

        public void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissions.Callback callback) {
            super.onGeolocationPermissionsShowPrompt(origin, callback);
            callback.invoke(origin, true, false);
        }
    }

    public class GapViewClient extends WebViewClient {
        DroidGap ctx;

        public GapViewClient(DroidGap ctx2) {
            this.ctx = ctx2;
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            String address;
            if (url.startsWith("tel:")) {
                try {
                    Intent intent = new Intent("android.intent.action.DIAL");
                    intent.setData(Uri.parse(url));
                    DroidGap.this.startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    System.out.println("Error dialing " + url + ": " + e.toString());
                }
                return true;
            } else if (url.startsWith("geo:")) {
                try {
                    Intent intent2 = new Intent("android.intent.action.VIEW");
                    intent2.setData(Uri.parse(url));
                    DroidGap.this.startActivity(intent2);
                } catch (ActivityNotFoundException e2) {
                    System.out.println("Error showing map " + url + ": " + e2.toString());
                }
                return true;
            } else if (url.startsWith("mailto:")) {
                try {
                    Intent intent3 = new Intent("android.intent.action.VIEW");
                    intent3.setData(Uri.parse(url));
                    DroidGap.this.startActivity(intent3);
                } catch (ActivityNotFoundException e3) {
                    System.out.println("Error sending email " + url + ": " + e3.toString());
                }
                return true;
            } else if (url.startsWith("sms:")) {
                try {
                    Intent intent4 = new Intent("android.intent.action.VIEW");
                    int parmIndex = url.indexOf(63);
                    if (parmIndex == -1) {
                        address = url.substring(4);
                    } else {
                        address = url.substring(4, parmIndex);
                        String query = Uri.parse(url).getQuery();
                        if (query != null && query.startsWith("body=")) {
                            intent4.putExtra("sms_body", query.substring(5));
                        }
                    }
                    intent4.setData(Uri.parse("sms:" + address));
                    intent4.putExtra("address", address);
                    intent4.setType("vnd.android-dir/mms-sms");
                    DroidGap.this.startActivity(intent4);
                } catch (ActivityNotFoundException e4) {
                    System.out.println("Error sending sms " + url + ":" + e4.toString());
                }
                return true;
            } else {
                int i = url.lastIndexOf(47);
                String newBaseUrl = url;
                if (i > 0) {
                    newBaseUrl = url.substring(0, i);
                }
                if (this.ctx.loadInWebView || url.startsWith("file://") || this.ctx.baseUrl.equals(newBaseUrl)) {
                    this.ctx.appView.loadUrl(url);
                } else {
                    try {
                        Intent intent5 = new Intent("android.intent.action.VIEW");
                        intent5.setData(Uri.parse(url));
                        DroidGap.this.startActivity(intent5);
                    } catch (ActivityNotFoundException e5) {
                        System.out.println("Error loading url " + url + ":" + e5.toString());
                    }
                }
                return true;
            }
        }

        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            DroidGap.access$108(this.ctx);
            if (!url.equals("about:blank")) {
                DroidGap.this.appView.loadUrl("javascript:try{ PhoneGap.onNativeReady.fire();}catch(e){_nativeReady = true;}");
            }
            DroidGap.this.appView.setVisibility(0);
            if (this.ctx.hideLoadingDialogOnPageLoad) {
                this.ctx.hideLoadingDialogOnPageLoad = false;
                this.ctx.pluginManager.exec("Notification", "activityStop", null, "[]", false);
            }
            if (this.ctx.clearHistory) {
                this.ctx.clearHistory = false;
                this.ctx.appView.clearHistory();
            }
            if (url.equals("about:blank") && this.ctx.callbackServer != null) {
                this.ctx.callbackServer.destroy();
            }
        }

        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            System.out.println("onReceivedError: Error code=" + errorCode + " Description=" + description + " URL=" + failingUrl);
            DroidGap.access$108(this.ctx);
            this.ctx.pluginManager.exec("Notification", "activityStop", null, "[]", false);
            this.ctx.onReceivedError(errorCode, description, failingUrl);
        }

        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
            try {
                if ((this.ctx.getPackageManager().getApplicationInfo(this.ctx.getPackageName(), 128).flags & 2) != 0) {
                    handler.proceed();
                } else {
                    super.onReceivedSslError(view, handler, error);
                }
            } catch (PackageManager.NameNotFoundException e) {
                super.onReceivedSslError(view, handler, error);
            }
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (this.appView == null) {
            return super.onKeyDown(keyCode, event);
        }
        if (keyCode == 4) {
            if (this.bound) {
                this.appView.loadUrl("javascript:PhoneGap.fireEvent('backbutton');");
                return true;
            } else if (!this.appView.canGoBack()) {
                return super.onKeyDown(keyCode, event);
            } else {
                this.appView.goBack();
                return true;
            }
        } else if (keyCode == 82) {
            this.appView.loadUrl("javascript:PhoneGap.fireEvent('menubutton');");
            return true;
        } else if (keyCode != 84) {
            return false;
        } else {
            this.appView.loadUrl("javascript:PhoneGap.fireEvent('searchbutton');");
            return true;
        }
    }

    public void startActivityForResult(Intent intent, int requestCode) throws RuntimeException {
        System.out.println("startActivityForResult(intent," + requestCode + ")");
        if (requestCode == -1) {
            super.startActivityForResult(intent, requestCode);
            return;
        }
        throw new RuntimeException("PhoneGap Exception: Call startActivityForResult(Command, Intent) instead.");
    }

    public void startActivityForResult(Plugin command, Intent intent, int requestCode) {
        this.activityResultCallback = command;
        this.activityResultKeepRunning = this.keepRunning;
        if (command != null) {
            this.keepRunning = false;
        }
        super.startActivityForResult(intent, requestCode);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        Plugin callback = this.activityResultCallback;
        if (callback != null) {
            callback.onActivityResult(requestCode, resultCode, intent);
        }
    }

    public void onReceivedError(int errorCode, String description, String failingUrl) {
        final String errorUrl = getStringProperty("errorUrl", null);
        if (errorUrl == null || !errorUrl.startsWith("file://") || failingUrl.equals(errorUrl)) {
            this.appView.loadUrl("about:blank");
            displayError("Application Error", description + " (" + failingUrl + ")", "OK", true);
            return;
        }
        runOnUiThread(new Runnable() {
            public void run() {
                this.appView.loadUrl(errorUrl);
            }
        });
    }

    public void displayError(String title, String message, String button, boolean exit) {
        final String str = message;
        final String str2 = title;
        final String str3 = button;
        final boolean z = exit;
        runOnUiThread(new Runnable() {
            public void run() {
                AlertDialog.Builder dlg = new AlertDialog.Builder(this);
                dlg.setMessage(str);
                dlg.setTitle(str2);
                dlg.setCancelable(false);
                dlg.setPositiveButton(str3, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        if (z) {
                            this.finish();
                        }
                    }
                });
                dlg.create();
                dlg.show();
            }
        });
    }

    class LinearLayoutSoftKeyboardDetect extends LinearLayout {
        private static final String LOG_TAG = "SoftKeyboardDetect";
        private int oldHeight = 0;
        private int oldWidth = 0;
        private int screenHeight = 0;
        private int screenWidth = 0;

        public LinearLayoutSoftKeyboardDetect(Context context, int width, int height) {
            super(context);
            this.screenWidth = width;
            this.screenHeight = height;
        }

        /* access modifiers changed from: protected */
        public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
            Log.d(LOG_TAG, "We are in our onMeasure method");
            int height = View.MeasureSpec.getSize(heightMeasureSpec);
            int width = View.MeasureSpec.getSize(widthMeasureSpec);
            Log.d(LOG_TAG, "Old Height = " + this.oldHeight);
            Log.d(LOG_TAG, "Height = " + height);
            Log.d(LOG_TAG, "Old Width = " + this.oldWidth);
            Log.d(LOG_TAG, "Width = " + width);
            if (this.oldHeight == 0 || this.oldHeight == height) {
                Log.d(LOG_TAG, "Ignore this event");
            } else if (this.screenHeight == width) {
                int tmp_var = this.screenHeight;
                this.screenHeight = this.screenWidth;
                this.screenWidth = tmp_var;
                Log.d(LOG_TAG, "Orientation Change");
            } else if (height > this.oldHeight) {
                Log.d(LOG_TAG, "Throw hide keyboard event");
                DroidGap.this.callbackServer.sendJavascript("PhoneGap.fireEvent('hidekeyboard');");
            } else if (height < this.oldHeight) {
                Log.d(LOG_TAG, "Throw show keyboard event");
                DroidGap.this.callbackServer.sendJavascript("PhoneGap.fireEvent('showkeyboard');");
            }
            this.oldHeight = height;
            this.oldWidth = width;
        }
    }
}
