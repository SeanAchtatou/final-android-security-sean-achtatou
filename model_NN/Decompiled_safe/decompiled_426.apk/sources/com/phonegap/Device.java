package com.phonegap;

import android.os.Build;
import android.provider.Settings;
import com.phonegap.api.PhonegapActivity;
import com.phonegap.api.Plugin;
import com.phonegap.api.PluginResult;
import java.util.TimeZone;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Device extends Plugin {
    public static String phonegapVersion = "0.9.5";
    public static String platform = "Android";
    public static String uuid;

    public void setContext(PhonegapActivity ctx) {
        super.setContext(ctx);
        uuid = getUuid();
    }

    public PluginResult execute(String action, JSONArray args, String callbackId) {
        PluginResult.Status status = PluginResult.Status.OK;
        try {
            if (!action.equals("getDeviceInfo")) {
                return new PluginResult(status, "");
            }
            JSONObject r = new JSONObject();
            r.put("uuid", uuid);
            r.put("version", getOSVersion());
            r.put("platform", platform);
            r.put("name", getProductName());
            r.put("phonegap", phonegapVersion);
            return new PluginResult(status, r);
        } catch (JSONException e) {
            return new PluginResult(PluginResult.Status.JSON_EXCEPTION);
        }
    }

    public boolean isSynch(String action) {
        if (action.equals("getDeviceInfo")) {
            return true;
        }
        return false;
    }

    public String getPlatform() {
        return platform;
    }

    public String getUuid() {
        return Settings.Secure.getString(this.ctx.getContentResolver(), "android_id");
    }

    public String getPhonegapVersion() {
        return phonegapVersion;
    }

    public String getModel() {
        return Build.MODEL;
    }

    public String getProductName() {
        return Build.PRODUCT;
    }

    public String getOSVersion() {
        return Build.VERSION.RELEASE;
    }

    public String getSDKVersion() {
        return Build.VERSION.SDK;
    }

    public String getTimeZoneID() {
        return TimeZone.getDefault().getID();
    }
}
