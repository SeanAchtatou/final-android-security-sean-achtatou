package com.phonegap.plugins.speech;

import android.speech.tts.TextToSpeech;
import com.phonegap.api.Plugin;
import com.phonegap.api.PluginResult;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class TTS extends Plugin implements TextToSpeech.OnInitListener {
    private static final int INITIALIZING = 1;
    private static final String LOG_TAG = "TTS";
    private static final int STARTED = 2;
    private static final int STOPPED = 0;
    private TextToSpeech mTts = null;
    private String startupCallbackId = "";
    private int state = 0;

    public PluginResult execute(String action, JSONArray args, String callbackId) {
        PluginResult.Status status = PluginResult.Status.OK;
        try {
            if (action.equals("speak")) {
                String text = args.getString(0);
                if (isReady()) {
                    this.mTts.speak(text, INITIALIZING, null);
                    return new PluginResult(status, "");
                }
                JSONObject error = new JSONObject();
                error.put("message", "TTS service is still initialzing.");
                error.put("code", (int) INITIALIZING);
                return new PluginResult(PluginResult.Status.ERROR, error);
            } else if (action.equals("silence")) {
                if (isReady()) {
                    this.mTts.playSilence(args.getLong(0), INITIALIZING, null);
                    return new PluginResult(status, "");
                }
                JSONObject error2 = new JSONObject();
                error2.put("message", "TTS service is still initialzing.");
                error2.put("code", (int) INITIALIZING);
                return new PluginResult(PluginResult.Status.ERROR, error2);
            } else if (action.equals("startup")) {
                if (this.mTts == null) {
                    this.startupCallbackId = callbackId;
                    this.state = INITIALIZING;
                    this.mTts = new TextToSpeech(this.ctx, this);
                }
                PluginResult pluginResult = new PluginResult(status, (int) INITIALIZING);
                pluginResult.setKeepCallback(true);
                return pluginResult;
            } else if (action.equals("shutdown")) {
                if (this.mTts != null) {
                    this.mTts.shutdown();
                }
                return new PluginResult(status, "");
            } else {
                if (action.equals("getLanguage")) {
                    if (this.mTts != null) {
                        return new PluginResult(status, this.mTts.getLanguage().toString());
                    }
                } else if (action.equals("isLanguageAvailable")) {
                    if (this.mTts != null) {
                        return new PluginResult(status, this.mTts.isLanguageAvailable(new Locale(args.getString(0))) < 0 ? "false" : "true");
                    }
                } else if (action.equals("setLanguage") && this.mTts != null) {
                    return new PluginResult(status, this.mTts.setLanguage(new Locale(args.getString(0))) < 0 ? "false" : "true");
                }
                return new PluginResult(status, "");
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return new PluginResult(PluginResult.Status.JSON_EXCEPTION);
        }
    }

    private boolean isReady() {
        return this.state == STARTED;
    }

    public void onInit(int status) {
        if (status == 0) {
            this.state = STARTED;
            PluginResult result = new PluginResult(PluginResult.Status.OK, (int) STARTED);
            result.setKeepCallback(false);
            success(result, this.startupCallbackId);
        } else if (status == -1) {
            this.state = 0;
            PluginResult result2 = new PluginResult(PluginResult.Status.ERROR, 0);
            result2.setKeepCallback(false);
            error(result2, this.startupCallbackId);
        }
    }

    public void onDestroy() {
        if (this.mTts != null) {
            this.mTts.shutdown();
        }
    }
}
