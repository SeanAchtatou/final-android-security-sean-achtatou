package com.phonegap;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.provider.Contacts;
import android.util.Log;
import android.webkit.WebView;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ContactAccessorSdk3_4 extends ContactAccessor {
    private static final String PEOPLE_ID_EQUALS = "people._id = ?";
    private static final Map<String, String> dbMap = new HashMap();

    static {
        dbMap.put("id", "_id");
        dbMap.put("displayName", "display_name");
        dbMap.put("phoneNumbers", "number");
        dbMap.put("phoneNumbers.value", "number");
        dbMap.put("emails", "data");
        dbMap.put("emails.value", "data");
        dbMap.put("addresses", "data");
        dbMap.put("addresses.formatted", "data");
        dbMap.put("ims", "data");
        dbMap.put("ims.value", "data");
        dbMap.put("organizations", "company");
        dbMap.put("organizations.name", "company");
        dbMap.put("organizations.title", "title");
        dbMap.put("note", "notes");
    }

    public ContactAccessorSdk3_4(WebView view, Activity app) {
        this.mApp = app;
        this.mView = view;
    }

    public JSONArray search(JSONArray fields, JSONObject options) {
        String searchTerm;
        int limit = Integer.MAX_VALUE;
        if (options != null) {
            String searchTerm2 = options.optString("filter");
            if (searchTerm2.length() == 0) {
                searchTerm = "%";
            } else {
                searchTerm = "%" + searchTerm2 + "%";
            }
            try {
                if (!options.getBoolean("multiple")) {
                    limit = 1;
                }
            } catch (JSONException e) {
            }
        } else {
            searchTerm = "%";
        }
        ContentResolver cr = this.mApp.getContentResolver();
        Set<String> contactIds = buildSetOfContactIds(fields, searchTerm);
        HashMap<String, Boolean> populate = buildPopulationSet(fields);
        Iterator<String> it = contactIds.iterator();
        JSONArray contacts = new JSONArray();
        int pos = 0;
        while (it.hasNext() && pos < limit) {
            JSONObject contact = new JSONObject();
            try {
                String contactId = it.next();
                contact.put("id", contactId);
                Cursor cur = cr.query(Contacts.People.CONTENT_URI, new String[]{"display_name", "notes"}, PEOPLE_ID_EQUALS, new String[]{contactId}, null);
                cur.moveToFirst();
                if (isRequired("displayName", populate)) {
                    contact.put("displayName", cur.getString(cur.getColumnIndex("display_name")));
                }
                if (isRequired("phoneNumbers", populate)) {
                    contact.put("phoneNumbers", phoneQuery(cr, contactId));
                }
                if (isRequired("emails", populate)) {
                    contact.put("emails", emailQuery(cr, contactId));
                }
                if (isRequired("addresses", populate)) {
                    contact.put("addresses", addressQuery(cr, contactId));
                }
                if (isRequired("organizations", populate)) {
                    contact.put("organizations", organizationQuery(cr, contactId));
                }
                if (isRequired("ims", populate)) {
                    contact.put("ims", imQuery(cr, contactId));
                }
                if (isRequired("note", populate)) {
                    contact.put("note", cur.getString(cur.getColumnIndex("notes")));
                }
                pos++;
                cur.close();
            } catch (JSONException e2) {
                JSONException e3 = e2;
                Log.e("ContactsAccessor", e3.getMessage(), e3);
            }
            contacts.put(contact);
        }
        return contacts;
    }

    private Set<String> buildSetOfContactIds(JSONArray fields, String searchTerm) {
        Set<String> contactIds = new HashSet<>();
        int i = 0;
        while (i < fields.length()) {
            try {
                String key = fields.getString(i);
                if (key.startsWith("displayName")) {
                    doQuery(searchTerm, contactIds, Contacts.People.CONTENT_URI, "_id", dbMap.get(key) + " LIKE ?", new String[]{searchTerm});
                } else if (key.startsWith("phoneNumbers")) {
                    doQuery(searchTerm, contactIds, Contacts.Phones.CONTENT_URI, "person", dbMap.get(key) + " LIKE ?", new String[]{searchTerm});
                } else if (key.startsWith("emails")) {
                    doQuery(searchTerm, contactIds, Contacts.ContactMethods.CONTENT_EMAIL_URI, "person", dbMap.get(key) + " LIKE ? AND " + "kind" + " = ?", new String[]{searchTerm, "vnd.android.cursor.item/email"});
                } else if (key.startsWith("addresses")) {
                    doQuery(searchTerm, contactIds, Contacts.ContactMethods.CONTENT_URI, "person", dbMap.get(key) + " LIKE ? AND " + "kind" + " = ?", new String[]{searchTerm, "vnd.android.cursor.item/postal-address"});
                } else if (key.startsWith("ims")) {
                    doQuery(searchTerm, contactIds, Contacts.ContactMethods.CONTENT_URI, "person", dbMap.get(key) + " LIKE ? AND " + "kind" + " = ?", new String[]{searchTerm, "vnd.android.cursor.item/jabber-im"});
                } else if (key.startsWith("organizations")) {
                    doQuery(searchTerm, contactIds, Contacts.Organizations.CONTENT_URI, "person", dbMap.get(key) + " LIKE ?", new String[]{searchTerm});
                } else if (key.startsWith("note")) {
                    doQuery(searchTerm, contactIds, Contacts.People.CONTENT_URI, "_id", dbMap.get(key) + " LIKE ?", new String[]{searchTerm});
                }
                i++;
            } catch (JSONException e) {
                JSONException e2 = e;
                Log.e("ContactsAccessor", e2.getMessage(), e2);
            }
        }
        return contactIds;
    }

    private void doQuery(String searchTerm, Set<String> contactIds, Uri uri, String projection, String selection, String[] selectionArgs) {
        Cursor cursor = this.mApp.getContentResolver().query(uri, null, selection, selectionArgs, null);
        while (cursor.moveToNext()) {
            contactIds.add(cursor.getString(cursor.getColumnIndex(projection)));
        }
        cursor.close();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
    private JSONArray imQuery(ContentResolver cr, String contactId) {
        ContentResolver contentResolver = cr;
        Cursor cursor = contentResolver.query(Contacts.ContactMethods.CONTENT_URI, null, "person = ? AND kind = ?", new String[]{contactId, "vnd.android.cursor.item/jabber-im"}, null);
        JSONArray ims = new JSONArray();
        while (cursor.moveToNext()) {
            JSONObject im = new JSONObject();
            try {
                im.put("id", cursor.getString(cursor.getColumnIndex("_id")));
                im.put("perf", false);
                im.put("value", cursor.getString(cursor.getColumnIndex("data")));
                im.put("type", getContactType(cursor.getInt(cursor.getColumnIndex("type"))));
                ims.put(im);
            } catch (JSONException e) {
                JSONException e2 = e;
                Log.e("ContactsAccessor", e2.getMessage(), e2);
            }
        }
        cursor.close();
        return null;
    }

    private JSONArray organizationQuery(ContentResolver cr, String contactId) {
        ContentResolver contentResolver = cr;
        Cursor cursor = contentResolver.query(Contacts.Organizations.CONTENT_URI, null, "person = ?", new String[]{contactId}, null);
        JSONArray organizations = new JSONArray();
        while (cursor.moveToNext()) {
            JSONObject organization = new JSONObject();
            try {
                organization.put("id", cursor.getString(cursor.getColumnIndex("_id")));
                organization.put("name", cursor.getString(cursor.getColumnIndex("company")));
                organization.put("title", cursor.getString(cursor.getColumnIndex("title")));
                organizations.put(organization);
            } catch (JSONException e) {
                JSONException e2 = e;
                Log.e("ContactsAccessor", e2.getMessage(), e2);
            }
        }
        return organizations;
    }

    private JSONArray addressQuery(ContentResolver cr, String contactId) {
        ContentResolver contentResolver = cr;
        Cursor cursor = contentResolver.query(Contacts.ContactMethods.CONTENT_URI, null, "person = ? AND kind = ?", new String[]{contactId, "vnd.android.cursor.item/postal-address"}, null);
        JSONArray addresses = new JSONArray();
        while (cursor.moveToNext()) {
            JSONObject address = new JSONObject();
            try {
                address.put("id", cursor.getString(cursor.getColumnIndex("_id")));
                address.put("formatted", cursor.getString(cursor.getColumnIndex("data")));
                addresses.put(address);
            } catch (JSONException e) {
                JSONException e2 = e;
                Log.e("ContactsAccessor", e2.getMessage(), e2);
            }
        }
        return addresses;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
    private JSONArray phoneQuery(ContentResolver cr, String contactId) {
        Cursor cursor = cr.query(Contacts.Phones.CONTENT_URI, null, "person = ?", new String[]{contactId}, null);
        JSONArray phones = new JSONArray();
        while (cursor.moveToNext()) {
            JSONObject phone = new JSONObject();
            try {
                phone.put("id", cursor.getString(cursor.getColumnIndex("_id")));
                phone.put("perf", false);
                phone.put("value", cursor.getString(cursor.getColumnIndex("number")));
                phone.put("type", getPhoneType(cursor.getInt(cursor.getColumnIndex("type"))));
                phones.put(phone);
            } catch (JSONException e) {
                JSONException e2 = e;
                Log.e("ContactsAccessor", e2.getMessage(), e2);
            }
        }
        return phones;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
    private JSONArray emailQuery(ContentResolver cr, String contactId) {
        Cursor cursor = cr.query(Contacts.ContactMethods.CONTENT_EMAIL_URI, null, "person = ?", new String[]{contactId}, null);
        JSONArray emails = new JSONArray();
        while (cursor.moveToNext()) {
            JSONObject email = new JSONObject();
            try {
                email.put("id", cursor.getString(cursor.getColumnIndex("_id")));
                email.put("perf", false);
                email.put("value", cursor.getString(cursor.getColumnIndex("data")));
                emails.put(email);
            } catch (JSONException e) {
                JSONException e2 = e;
                Log.e("ContactsAccessor", e2.getMessage(), e2);
            }
        }
        return emails;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public boolean save(JSONObject contact) {
        Uri newPersonUri;
        ContentValues personValues = new ContentValues();
        String id = getJsonString(contact, "id");
        String name = getJsonString(contact, "displayName");
        if (name != null) {
            personValues.put("name", name);
        }
        String note = getJsonString(contact, "note");
        if (note != null) {
            personValues.put("notes", note);
        }
        personValues.put("starred", (Integer) 0);
        if (id == null) {
            newPersonUri = Contacts.People.createPersonInMyContactsGroup(this.mApp.getContentResolver(), personValues);
        } else {
            newPersonUri = Uri.withAppendedPath(Contacts.People.CONTENT_URI, id);
            this.mApp.getContentResolver().update(newPersonUri, personValues, PEOPLE_ID_EQUALS, new String[]{id});
        }
        if (newPersonUri == null) {
            return false;
        }
        savePhoneNumbers(contact, newPersonUri);
        saveEntries(contact, newPersonUri, "emails", 1);
        saveAddresses(contact, newPersonUri);
        saveOrganizations(contact, newPersonUri);
        saveEntries(contact, newPersonUri, "ims", 3);
        return true;
    }

    private void saveOrganizations(JSONObject contact, Uri newPersonUri) {
        ContentValues values = new ContentValues();
        Uri orgUri = Uri.withAppendedPath(newPersonUri, "organizations");
        try {
            JSONArray orgs = contact.getJSONArray("organizations");
            if (orgs != null && orgs.length() > 0) {
                for (int i = 0; i < orgs.length(); i++) {
                    JSONObject org = orgs.getJSONObject(i);
                    String id = getJsonString(org, "id");
                    values.put("company", getJsonString(org, "name"));
                    values.put("title", getJsonString(org, "title"));
                    if (id == null) {
                        this.mApp.getContentResolver().insert(orgUri, values);
                    } else {
                        this.mApp.getContentResolver().update(Uri.withAppendedPath(orgUri, id), values, null, null);
                    }
                }
            }
        } catch (JSONException e) {
            Log.d("ContactsAccessor", "Could not save organizations = " + e.getMessage());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    private void saveAddresses(JSONObject contact, Uri uri) {
        ContentValues values = new ContentValues();
        Uri newUri = Uri.withAppendedPath(uri, "contact_methods");
        try {
            JSONArray entries = contact.getJSONArray("addresses");
            if (entries != null && entries.length() > 0) {
                values.put("kind", (Integer) 2);
                for (int i = 0; i < entries.length(); i++) {
                    JSONObject entry = entries.getJSONObject(i);
                    String id = getJsonString(entry, "id");
                    String address = getJsonString(entry, "formatted");
                    if (address != null) {
                        values.put("data", address);
                    } else {
                        values.put("data", createAddressString(entry));
                    }
                    if (id == null) {
                        this.mApp.getContentResolver().insert(newUri, values);
                    } else {
                        this.mApp.getContentResolver().update(Uri.withAppendedPath(newUri, id), values, null, null);
                    }
                }
            }
        } catch (JSONException e) {
            Log.d("ContactsAccessor", "Could not save address = " + e.getMessage());
        }
    }

    private String createAddressString(JSONObject entry) {
        StringBuffer buffer = new StringBuffer("");
        if (getJsonString(entry, "locality") != null) {
            buffer.append(getJsonString(entry, "locality"));
        }
        if (getJsonString(entry, "region") != null) {
            if (buffer.length() > 0) {
                buffer.append(", ");
            }
            buffer.append(getJsonString(entry, "region"));
        }
        if (getJsonString(entry, "postalCode") != null) {
            if (buffer.length() > 0) {
                buffer.append(", ");
            }
            buffer.append(getJsonString(entry, "postalCode"));
        }
        if (getJsonString(entry, "country") != null) {
            if (buffer.length() > 0) {
                buffer.append(", ");
            }
            buffer.append(getJsonString(entry, "country"));
        }
        return buffer.toString();
    }

    private void saveEntries(JSONObject contact, Uri uri, String dataType, int contactKind) {
        ContentValues values = new ContentValues();
        Uri newUri = Uri.withAppendedPath(uri, "contact_methods");
        try {
            JSONArray entries = contact.getJSONArray(dataType);
            if (entries != null && entries.length() > 0) {
                values.put("kind", Integer.valueOf(contactKind));
                for (int i = 0; i < entries.length(); i++) {
                    JSONObject entry = entries.getJSONObject(i);
                    String id = getJsonString(entry, "id");
                    values.put("data", getJsonString(entry, "value"));
                    values.put("type", Integer.valueOf(getContactType(getJsonString(entry, "type"))));
                    if (id == null) {
                        this.mApp.getContentResolver().insert(newUri, values);
                    } else {
                        this.mApp.getContentResolver().update(Uri.withAppendedPath(newUri, id), values, null, null);
                    }
                }
            }
        } catch (JSONException e) {
            Log.d("ContactsAccessor", "Could not save " + dataType + " = " + e.getMessage());
        }
    }

    private int getContactType(String string) {
        if (string != null) {
            if ("home".equals(string.toLowerCase())) {
                return 1;
            }
            if ("work".equals(string.toLowerCase())) {
                return 2;
            }
            if ("other".equals(string.toLowerCase())) {
                return 3;
            }
            if ("custom".equals(string.toLowerCase())) {
                return 0;
            }
        }
        return 3;
    }

    private String getContactType(int type) {
        switch (type) {
            case 0:
                return "custom";
            case 1:
                return "home";
            case 2:
                return "work";
            default:
                return "other";
        }
    }

    private void savePhoneNumbers(JSONObject contact, Uri uri) {
        ContentValues values = new ContentValues();
        Uri phonesUri = Uri.withAppendedPath(uri, "phones");
        try {
            JSONArray phones = contact.getJSONArray("phoneNumbers");
            if (phones != null && phones.length() > 0) {
                for (int i = 0; i < phones.length(); i++) {
                    JSONObject phone = phones.getJSONObject(i);
                    String id = getJsonString(phone, "id");
                    values.put("number", getJsonString(phone, "value"));
                    values.put("type", Integer.valueOf(getPhoneType(getJsonString(phone, "type"))));
                    if (id == null) {
                        this.mApp.getContentResolver().insert(phonesUri, values);
                    } else {
                        this.mApp.getContentResolver().update(Uri.withAppendedPath(phonesUri, id), values, null, null);
                    }
                }
            }
        } catch (JSONException e) {
            Log.d("ContactsAccessor", "Could not save phones = " + e.getMessage());
        }
    }

    private int getPhoneType(String string) {
        if ("home".equals(string.toLowerCase())) {
            return 1;
        }
        if (NetworkManager.MOBILE.equals(string.toLowerCase())) {
            return 2;
        }
        if ("work".equals(string.toLowerCase())) {
            return 3;
        }
        if ("work fax".equals(string.toLowerCase())) {
            return 4;
        }
        if ("home fax".equals(string.toLowerCase())) {
            return 5;
        }
        if ("fax".equals(string.toLowerCase())) {
            return 4;
        }
        if ("pager".equals(string.toLowerCase())) {
            return 6;
        }
        if ("other".equals(string.toLowerCase())) {
            return 7;
        }
        return "custom".equals(string.toLowerCase()) ? 0 : 7;
    }

    private String getPhoneType(int type) {
        switch (type) {
            case 0:
                return "custom";
            case 1:
                return "home";
            case 2:
                return NetworkManager.MOBILE;
            case 3:
                return "work";
            case 4:
                return "work fax";
            case 5:
                return "home fax";
            case 6:
                return "pager";
            default:
                return "custom";
        }
    }

    public boolean remove(String id) {
        return this.mApp.getContentResolver().delete(Contacts.People.CONTENT_URI, PEOPLE_ID_EQUALS, new String[]{id}) > 0;
    }
}
