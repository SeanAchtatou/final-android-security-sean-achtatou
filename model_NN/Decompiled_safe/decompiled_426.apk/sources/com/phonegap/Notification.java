package com.phonegap;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.os.Vibrator;
import com.phonegap.api.PhonegapActivity;
import com.phonegap.api.Plugin;
import com.phonegap.api.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;

public class Notification extends Plugin {
    public int confirmResult = -1;
    public ProgressDialog progressDialog = null;
    public ProgressDialog spinnerDialog = null;

    public PluginResult execute(String action, JSONArray args, String callbackId) {
        PluginResult.Status status = PluginResult.Status.OK;
        try {
            if (action.equals("beep")) {
                beep(args.getLong(0));
            } else if (action.equals("vibrate")) {
                vibrate(args.getLong(0));
            } else if (action.equals("alert")) {
                alert(args.getString(0), args.getString(1), args.getString(2), callbackId);
                PluginResult r = new PluginResult(PluginResult.Status.NO_RESULT);
                r.setKeepCallback(true);
                return r;
            } else if (action.equals("confirm")) {
                confirm(args.getString(0), args.getString(1), args.getString(2), callbackId);
                PluginResult r2 = new PluginResult(PluginResult.Status.NO_RESULT);
                r2.setKeepCallback(true);
                return r2;
            } else if (action.equals("activityStart")) {
                activityStart(args.getString(0), args.getString(1));
            } else if (action.equals("activityStop")) {
                activityStop();
            } else if (action.equals("progressStart")) {
                progressStart(args.getString(0), args.getString(1));
            } else if (action.equals("progressValue")) {
                progressValue(args.getInt(0));
            } else if (action.equals("progressStop")) {
                progressStop();
            }
            return new PluginResult(status, "");
        } catch (JSONException e) {
            return new PluginResult(PluginResult.Status.JSON_EXCEPTION);
        }
    }

    public boolean isSynch(String action) {
        if (action.equals("alert")) {
            return true;
        }
        if (action.equals("confirm")) {
            return true;
        }
        if (action.equals("activityStart")) {
            return true;
        }
        if (action.equals("activityStop")) {
            return true;
        }
        if (action.equals("progressStart")) {
            return true;
        }
        if (action.equals("progressValue")) {
            return true;
        }
        if (action.equals("progressStop")) {
            return true;
        }
        return false;
    }

    public void beep(long count) {
        Ringtone notification = RingtoneManager.getRingtone(this.ctx, RingtoneManager.getDefaultUri(2));
        if (notification != null) {
            for (long i = 0; i < count; i++) {
                notification.play();
                long timeout = 5000;
                while (notification.isPlaying() && timeout > 0) {
                    timeout -= 100;
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                    }
                }
            }
        }
    }

    public void vibrate(long time) {
        if (time == 0) {
            time = 500;
        }
        ((Vibrator) this.ctx.getSystemService("vibrator")).vibrate(time);
    }

    public synchronized void alert(String message, String title, String buttonLabel, String callbackId) {
        final PhonegapActivity ctx = this.ctx;
        final String str = message;
        final String str2 = title;
        final String str3 = buttonLabel;
        final String str4 = callbackId;
        this.ctx.runOnUiThread(new Runnable() {
            public void run() {
                AlertDialog.Builder dlg = new AlertDialog.Builder(ctx);
                dlg.setMessage(str);
                dlg.setTitle(str2);
                dlg.setCancelable(false);
                dlg.setPositiveButton(str3, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        this.success(new PluginResult(PluginResult.Status.OK, 0), str4);
                    }
                });
                dlg.create();
                dlg.show();
            }
        });
    }

    public synchronized void confirm(String message, String title, String buttonLabels, String callbackId) {
        final PhonegapActivity ctx = this.ctx;
        final String[] fButtons = buttonLabels.split(",");
        final String str = message;
        final String str2 = title;
        final String str3 = callbackId;
        this.ctx.runOnUiThread(new Runnable() {
            public void run() {
                AlertDialog.Builder dlg = new AlertDialog.Builder(ctx);
                dlg.setMessage(str);
                dlg.setTitle(str2);
                dlg.setCancelable(false);
                if (fButtons.length > 0) {
                    dlg.setPositiveButton(fButtons[0], new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            this.success(new PluginResult(PluginResult.Status.OK, 1), str3);
                        }
                    });
                }
                if (fButtons.length > 1) {
                    dlg.setNeutralButton(fButtons[1], new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            this.success(new PluginResult(PluginResult.Status.OK, 2), str3);
                        }
                    });
                }
                if (fButtons.length > 2) {
                    dlg.setNegativeButton(fButtons[2], new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            this.success(new PluginResult(PluginResult.Status.OK, 3), str3);
                        }
                    });
                }
                dlg.create();
                dlg.show();
            }
        });
    }

    public synchronized void activityStart(String title, String message) {
        if (this.spinnerDialog != null) {
            this.spinnerDialog.dismiss();
            this.spinnerDialog = null;
        }
        final PhonegapActivity ctx = this.ctx;
        final String str = title;
        final String str2 = message;
        this.ctx.runOnUiThread(new Runnable() {
            public void run() {
                this.spinnerDialog = ProgressDialog.show(ctx, str, str2, true, true, new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialog) {
                        this.spinnerDialog = null;
                    }
                });
            }
        });
    }

    public synchronized void activityStop() {
        if (this.spinnerDialog != null) {
            this.spinnerDialog.dismiss();
            this.spinnerDialog = null;
        }
    }

    public synchronized void progressStart(String title, String message) {
        if (this.progressDialog != null) {
            this.progressDialog.dismiss();
            this.progressDialog = null;
        }
        final PhonegapActivity ctx = this.ctx;
        final String str = title;
        final String str2 = message;
        this.ctx.runOnUiThread(new Runnable() {
            public void run() {
                this.progressDialog = new ProgressDialog(ctx);
                this.progressDialog.setProgressStyle(1);
                this.progressDialog.setTitle(str);
                this.progressDialog.setMessage(str2);
                this.progressDialog.setCancelable(true);
                this.progressDialog.setMax(100);
                this.progressDialog.setProgress(0);
                this.progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialog) {
                        this.progressDialog = null;
                    }
                });
                this.progressDialog.show();
            }
        });
    }

    public synchronized void progressValue(int value) {
        if (this.progressDialog != null) {
            this.progressDialog.setProgress(value);
        }
    }

    public synchronized void progressStop() {
        if (this.progressDialog != null) {
            this.progressDialog.dismiss();
            this.progressDialog = null;
        }
    }
}
