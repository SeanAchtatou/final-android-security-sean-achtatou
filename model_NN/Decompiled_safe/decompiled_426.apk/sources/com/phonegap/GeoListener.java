package com.phonegap;

import android.location.Location;
import android.location.LocationManager;

public class GeoListener {
    public static int PERMISSION_DENIED = 1;
    public static int POSITION_UNAVAILABLE = 2;
    public static int TIMEOUT = 3;
    private GeoBroker broker;
    String failCallback;
    String id;
    int interval;
    GpsListener mGps = null;
    LocationManager mLocMan;
    NetworkListener mNetwork = null;
    String successCallback;

    GeoListener(GeoBroker broker2, String id2, int time) {
        this.id = id2;
        this.interval = time;
        this.broker = broker2;
        this.mLocMan = (LocationManager) broker2.ctx.getSystemService("location");
        if (this.mLocMan.getProvider("gps") != null) {
            this.mGps = new GpsListener(broker2.ctx, time, this);
        }
        if (this.mLocMan.getProvider("network") != null) {
            this.mNetwork = new NetworkListener(broker2.ctx, time, this);
        }
    }

    public void destroy() {
        stop();
    }

    /* access modifiers changed from: package-private */
    public void success(Location loc) {
        String params = loc.getLatitude() + "," + loc.getLongitude() + ", " + loc.getAltitude() + "," + loc.getAccuracy() + "," + loc.getBearing() + "," + loc.getSpeed() + "," + loc.getTime();
        if (this.id == "global") {
            stop();
        }
        this.broker.sendJavascript("navigator._geo.success('" + this.id + "'," + params + ");");
    }

    /* access modifiers changed from: package-private */
    public void fail(int code, String msg) {
        this.broker.sendJavascript("navigator._geo.fail('" + this.id + "', '" + code + "', '" + msg + "');");
        stop();
    }

    /* access modifiers changed from: package-private */
    public void start(int interval2) {
        if (this.mGps != null) {
            this.mGps.start(interval2);
        }
        if (this.mNetwork != null) {
            this.mNetwork.start(interval2);
        }
        if (this.mNetwork == null && this.mGps == null) {
            fail(POSITION_UNAVAILABLE, "No location providers available.");
        }
    }

    /* access modifiers changed from: package-private */
    public void stop() {
        if (this.mGps != null) {
            this.mGps.stop();
        }
        if (this.mNetwork != null) {
            this.mNetwork.stop();
        }
    }
}
