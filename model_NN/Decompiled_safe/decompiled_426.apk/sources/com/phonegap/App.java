package com.phonegap;

import com.phonegap.api.Plugin;
import com.phonegap.api.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class App extends Plugin {
    public PluginResult execute(String action, JSONArray args, String callbackId) {
        PluginResult.Status status = PluginResult.Status.OK;
        try {
            if (action.equals("clearCache")) {
                clearCache();
            } else if (action.equals("loadUrl")) {
                loadUrl(args.getString(0), args.optJSONObject(1));
            } else if (action.equals("cancelLoadUrl")) {
                cancelLoadUrl();
            } else if (action.equals("clearHistory")) {
                clearHistory();
            } else if (action.equals("addService")) {
                addService(args.getString(0), args.getString(1));
            } else if (action.equals("overrideBackbutton")) {
                overrideBackbutton(args.getBoolean(0));
            } else if (action.equals("isBackbuttonOverridden")) {
                return new PluginResult(status, isBackbuttonOverridden());
            } else {
                if (action.equals("exitApp")) {
                    exitApp();
                }
            }
            return new PluginResult(status, "");
        } catch (JSONException e) {
            return new PluginResult(PluginResult.Status.JSON_EXCEPTION);
        }
    }

    public void clearCache() {
        ((DroidGap) this.ctx).clearCache();
    }

    /* Debug info: failed to restart local var, previous not found, register: 8 */
    public void loadUrl(String url, JSONObject props) throws JSONException {
        System.out.println("App.loadUrl(" + url + "," + props + ")");
        int wait = 0;
        if (props != null) {
            JSONArray keys = props.names();
            for (int i = 0; i < keys.length(); i++) {
                String key = keys.getString(i);
                if (key.equals("wait")) {
                    wait = props.getInt(key);
                } else {
                    Object value = props.get(key);
                    if (value != null) {
                        if (value.getClass().equals(String.class)) {
                            this.ctx.getIntent().putExtra(key, (String) value);
                        } else if (value.getClass().equals(Boolean.class)) {
                            this.ctx.getIntent().putExtra(key, (Boolean) value);
                        } else if (value.getClass().equals(Integer.class)) {
                            this.ctx.getIntent().putExtra(key, (Integer) value);
                        }
                    }
                }
            }
        }
        if (wait > 0) {
            ((DroidGap) this.ctx).loadUrl(url, wait);
        } else {
            ((DroidGap) this.ctx).loadUrl(url);
        }
    }

    public void cancelLoadUrl() {
        ((DroidGap) this.ctx).cancelLoadUrl();
    }

    public void clearHistory() {
        ((DroidGap) this.ctx).clearHistory();
    }

    public void addService(String serviceType, String className) {
        this.ctx.addService(serviceType, className);
    }

    public void overrideBackbutton(boolean override) {
        System.out.println("WARNING: Back Button Default Behaviour will be overridden.  The backbutton event will be fired!");
        ((DroidGap) this.ctx).bound = override;
    }

    public boolean isBackbuttonOverridden() {
        return ((DroidGap) this.ctx).bound;
    }

    public void exitApp() {
        ((DroidGap) this.ctx).finish();
    }
}
