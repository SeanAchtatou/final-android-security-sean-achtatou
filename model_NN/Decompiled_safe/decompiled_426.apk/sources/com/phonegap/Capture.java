package com.phonegap;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import com.phonegap.api.Plugin;
import com.phonegap.api.PluginResult;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Capture extends Plugin {
    private static final int CAPTURE_AUDIO = 0;
    private static final int CAPTURE_IMAGE = 1;
    private static final int CAPTURE_VIDEO = 2;
    private static final String LOG_TAG = "Capture";
    private static final String _DATA = "_data";
    private String callbackId;
    private double duration;
    private Uri imageUri;
    private long limit;
    private JSONArray results;

    public PluginResult execute(String action, JSONArray args, String callbackId2) {
        this.callbackId = callbackId2;
        this.limit = 1;
        this.duration = 0.0d;
        this.results = new JSONArray();
        JSONObject options = args.optJSONObject(CAPTURE_AUDIO);
        if (options != null) {
            this.limit = options.optLong("limit", 1);
            this.duration = options.optDouble("duration", 0.0d);
        }
        if (action.equals("getFormatData")) {
            try {
                return new PluginResult(PluginResult.Status.OK, getFormatData(args.getString(CAPTURE_AUDIO), args.getString(CAPTURE_IMAGE)));
            } catch (JSONException e) {
                return new PluginResult(PluginResult.Status.ERROR);
            }
        } else {
            if (action.equals("captureAudio")) {
                captureAudio();
            } else if (action.equals("captureImage")) {
                captureImage();
            } else if (action.equals("captureVideo")) {
                captureVideo(this.duration);
            }
            PluginResult r = new PluginResult(PluginResult.Status.NO_RESULT);
            r.setKeepCallback(true);
            return r;
        }
    }

    private JSONObject getFormatData(String filePath, String mimeType) {
        JSONObject obj = new JSONObject();
        try {
            obj.put("height", (int) CAPTURE_AUDIO);
            obj.put("width", (int) CAPTURE_AUDIO);
            obj.put("bitrate", (int) CAPTURE_AUDIO);
            obj.put("duration", (int) CAPTURE_AUDIO);
            obj.put("codecs", "");
            if (mimeType.equals("image/jpeg")) {
                return getImageData(filePath, obj);
            }
            if (filePath.endsWith("audio/3gpp")) {
                return getAudioVideoData(filePath, obj, false);
            }
            if (mimeType.equals("video/3gpp")) {
                return getAudioVideoData(filePath, obj, true);
            }
            return obj;
        } catch (JSONException e) {
            Log.d(LOG_TAG, "Error: setting media file data object");
            return obj;
        }
    }

    private JSONObject getImageData(String filePath, JSONObject obj) throws JSONException {
        Bitmap bitmap = BitmapFactory.decodeFile(filePath);
        obj.put("height", bitmap.getHeight());
        obj.put("width", bitmap.getWidth());
        return obj;
    }

    private JSONObject getAudioVideoData(String filePath, JSONObject obj, boolean video) throws JSONException {
        MediaPlayer player = new MediaPlayer();
        try {
            player.setDataSource(filePath);
            player.prepare();
            obj.put("duration", player.getDuration());
            if (video) {
                obj.put("height", player.getVideoHeight());
                obj.put("width", player.getVideoWidth());
            }
        } catch (IOException e) {
            Log.d(LOG_TAG, "Error: loading video file");
        }
        return obj;
    }

    private void captureAudio() {
        this.ctx.startActivityForResult(this, new Intent("android.provider.MediaStore.RECORD_SOUND"), CAPTURE_AUDIO);
    }

    private void captureImage() {
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        File photo = new File(Environment.getExternalStorageDirectory(), "Capture.jpg");
        intent.putExtra("output", Uri.fromFile(photo));
        this.imageUri = Uri.fromFile(photo);
        this.ctx.startActivityForResult(this, intent, CAPTURE_IMAGE);
    }

    private void captureVideo(double duration2) {
        this.ctx.startActivityForResult(this, new Intent("android.media.action.VIDEO_CAPTURE"), CAPTURE_VIDEO);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        Uri uri;
        if (resultCode == -1) {
            if (requestCode == 0) {
                this.results.put(createMediaFile(intent.getData()));
                if (((long) this.results.length()) >= this.limit) {
                    success(new PluginResult(PluginResult.Status.OK, this.results, "navigator.device.capture._castMediaFile"), this.callbackId);
                } else {
                    captureAudio();
                }
            } else if (requestCode == CAPTURE_IMAGE) {
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.ctx.getContentResolver(), this.imageUri);
                    ContentValues values = new ContentValues();
                    values.put("mime_type", "image/jpeg");
                    try {
                        uri = this.ctx.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                    } catch (UnsupportedOperationException e) {
                        System.out.println("Can't write to external media storage.");
                        try {
                            uri = this.ctx.getContentResolver().insert(MediaStore.Images.Media.INTERNAL_CONTENT_URI, values);
                        } catch (UnsupportedOperationException e2) {
                            System.out.println("Can't write to internal media storage.");
                            fail("Error capturing image - no media storage found.");
                            return;
                        }
                    }
                    OutputStream os = this.ctx.getContentResolver().openOutputStream(uri);
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
                    os.close();
                    bitmap.recycle();
                    System.gc();
                    this.results.put(createMediaFile(uri));
                    if (((long) this.results.length()) >= this.limit) {
                        success(new PluginResult(PluginResult.Status.OK, this.results, "navigator.device.capture._castMediaFile"), this.callbackId);
                    } else {
                        captureImage();
                    }
                } catch (IOException e3) {
                    e3.printStackTrace();
                    fail("Error capturing image.");
                }
            } else if (requestCode == CAPTURE_VIDEO) {
                this.results.put(createMediaFile(intent.getData()));
                if (((long) this.results.length()) >= this.limit) {
                    success(new PluginResult(PluginResult.Status.OK, this.results, "navigator.device.capture._castMediaFile"), this.callbackId);
                } else {
                    captureVideo(this.duration);
                }
            }
        } else if (resultCode == 0) {
            if (this.results.length() > 0) {
                success(new PluginResult(PluginResult.Status.OK, this.results, "navigator.device.capture._castMediaFile"), this.callbackId);
            } else {
                fail("Canceled.");
            }
        } else if (this.results.length() > 0) {
            success(new PluginResult(PluginResult.Status.OK, this.results, "navigator.device.capture._castMediaFile"), this.callbackId);
        } else {
            fail("Did not complete!");
        }
    }

    private JSONObject createMediaFile(Uri data) {
        File fp = new File(getRealPathFromURI(data));
        JSONObject obj = new JSONObject();
        try {
            obj.put("name", fp.getName());
            obj.put("fullPath", fp.getAbsolutePath());
            obj.put("type", FileUtils.getMimeType(fp.getAbsolutePath()));
            obj.put("lastModifiedDate", fp.lastModified());
            obj.put("size", fp.length());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return obj;
    }

    private String getRealPathFromURI(Uri contentUri) {
        String[] proj = new String[CAPTURE_IMAGE];
        proj[CAPTURE_AUDIO] = _DATA;
        Cursor cursor = this.ctx.managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(_DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public void fail(String err) {
        error(new PluginResult(PluginResult.Status.ERROR, err), this.callbackId);
    }
}
