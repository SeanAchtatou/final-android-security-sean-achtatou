package com.phonegap;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import com.phonegap.api.PhonegapActivity;

public class NetworkListener implements LocationListener {
    private Location cLoc;
    private boolean hasData = false;
    private PhonegapActivity mCtx;
    private LocationManager mLocMan;
    private GeoListener owner;
    private boolean running = false;

    public NetworkListener(PhonegapActivity ctx, int interval, GeoListener m) {
        this.owner = m;
        this.mCtx = ctx;
        this.mLocMan = (LocationManager) this.mCtx.getSystemService("location");
        this.running = false;
        start(interval);
    }

    public Location getLocation() {
        this.cLoc = this.mLocMan.getLastKnownLocation("network");
        if (this.cLoc != null) {
            this.hasData = true;
        }
        return this.cLoc;
    }

    public void onProviderDisabled(String provider) {
        System.out.println("NetworkListener: The provider " + provider + " is disabled");
    }

    public void onProviderEnabled(String provider) {
        System.out.println("NetworkListener: The provider " + provider + " is enabled");
    }

    public void onStatusChanged(String provider, int status, Bundle extras) {
        System.out.println("NetworkListener: The status of the provider " + provider + " has changed");
        if (status == 0) {
            System.out.println("NetworkListener: " + provider + " is OUT OF SERVICE");
        } else if (status == 1) {
            System.out.println("NetworkListener: " + provider + " is TEMPORARILY_UNAVAILABLE");
        } else {
            System.out.println("NetworkListener: " + provider + " is Available");
        }
    }

    public void onLocationChanged(Location location) {
        System.out.println("NetworkListener: The location has been updated!");
        this.hasData = true;
        this.cLoc = location;
        if (!this.owner.mGps.hasLocation()) {
            this.owner.success(location);
        }
    }

    public void start(int interval) {
        if (!this.running) {
            this.running = true;
            this.mLocMan.requestLocationUpdates("network", (long) interval, 0.0f, this);
            getLocation();
            if (this.hasData && !this.owner.mGps.hasLocation()) {
                this.owner.success(this.cLoc);
            }
        }
    }

    public void stop() {
        if (this.running) {
            this.mLocMan.removeUpdates(this);
        }
        this.running = false;
    }
}
