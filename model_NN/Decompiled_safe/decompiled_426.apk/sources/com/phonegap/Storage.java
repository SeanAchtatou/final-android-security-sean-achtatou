package com.phonegap;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import com.phonegap.api.Plugin;
import com.phonegap.api.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Storage extends Plugin {
    private static final String ALTER = "alter";
    private static final String CREATE = "create";
    private static final String DROP = "drop";
    private static final String TRUNCATE = "truncate";
    String dbName = null;
    SQLiteDatabase myDb = null;
    String path = null;

    public PluginResult execute(String action, JSONArray args, String callbackId) {
        String[] s;
        PluginResult.Status status = PluginResult.Status.OK;
        try {
            if (action.equals("setStorage")) {
                setStorage(args.getString(0));
            } else if (action.equals("openDatabase")) {
                openDatabase(args.getString(0), args.getString(1), args.getString(2), args.getLong(3));
            } else if (action.equals("executeSql")) {
                if (args.isNull(1)) {
                    s = new String[0];
                } else {
                    JSONArray a = args.getJSONArray(1);
                    int len = a.length();
                    s = new String[len];
                    for (int i = 0; i < len; i++) {
                        s[i] = a.getString(i);
                    }
                }
                executeSql(args.getString(0), s, args.getString(2));
            }
            return new PluginResult(status, "");
        } catch (JSONException e) {
            return new PluginResult(PluginResult.Status.JSON_EXCEPTION);
        }
    }

    public boolean isSynch(String action) {
        return true;
    }

    public void onDestroy() {
        if (this.myDb != null) {
            this.myDb.close();
            this.myDb = null;
        }
    }

    public void setStorage(String appPackage) {
        this.path = "/data/data/" + appPackage + "/databases/";
    }

    public void openDatabase(String db, String version, String display_name, long size) {
        if (this.myDb != null) {
            this.myDb.close();
        }
        if (this.path == null) {
            setStorage(this.ctx.getClass().getPackage().getName());
        }
        this.dbName = this.path + db + ".db";
        this.myDb = SQLiteDatabase.openOrCreateDatabase(this.dbName, (SQLiteDatabase.CursorFactory) null);
    }

    public void executeSql(String query, String[] params, String tx_id) {
        try {
            if (isDDL(query)) {
                this.myDb.execSQL(query);
                sendJavascript("droiddb.completeQuery('" + tx_id + "', '');");
                return;
            }
            Cursor myCursor = this.myDb.rawQuery(query, params);
            processResults(myCursor, tx_id);
            myCursor.close();
        } catch (SQLiteException e) {
            SQLiteException ex = e;
            ex.printStackTrace();
            System.out.println("Storage.executeSql(): Error=" + ex.getMessage());
            sendJavascript("droiddb.fail('" + ex.getMessage() + "','" + tx_id + "');");
        }
    }

    private boolean isDDL(String query) {
        String cmd = query.toLowerCase();
        if (cmd.startsWith(DROP) || cmd.startsWith(CREATE) || cmd.startsWith(ALTER) || cmd.startsWith(TRUNCATE)) {
            return true;
        }
        return false;
    }

    public void processResults(Cursor cur, String tx_id) {
        String result = "[]";
        if (cur.moveToFirst()) {
            JSONArray fullresult = new JSONArray();
            int colCount = cur.getColumnCount();
            do {
                JSONObject row = new JSONObject();
                int i = 0;
                while (i < colCount) {
                    try {
                        row.put(cur.getColumnName(i), cur.getString(i));
                        i++;
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                fullresult.put(row);
            } while (cur.moveToNext());
            result = fullresult.toString();
        }
        sendJavascript("droiddb.completeQuery('" + tx_id + "', " + result + ");");
    }
}
