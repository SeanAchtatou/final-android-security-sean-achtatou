package com.phonegap;

import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Environment;
import java.io.File;
import java.io.IOException;

public class AudioPlayer implements MediaPlayer.OnCompletionListener, MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener {
    private static int MEDIA_DURATION = 2;
    private static int MEDIA_ERROR = 9;
    private static int MEDIA_ERROR_ALREADY_RECORDING = 2;
    private static int MEDIA_ERROR_PAUSE_STATE = 7;
    private static int MEDIA_ERROR_PLAY_MODE_SET = 1;
    private static int MEDIA_ERROR_RECORD_MODE_SET = 4;
    private static int MEDIA_ERROR_RESUME_STATE = 6;
    private static int MEDIA_ERROR_STARTING_PLAYBACK = 5;
    private static int MEDIA_ERROR_STARTING_RECORDING = 3;
    private static int MEDIA_ERROR_STOP_STATE = 8;
    private static int MEDIA_NONE = 0;
    private static int MEDIA_PAUSED = 3;
    private static int MEDIA_RUNNING = 2;
    private static int MEDIA_STARTING = 1;
    private static int MEDIA_STATE = 1;
    private static int MEDIA_STOPPED = 4;
    private String audioFile = null;
    private float duration = -1.0f;
    private AudioHandler handler;
    private String id;
    private MediaPlayer mPlayer = null;
    private boolean prepareOnly = false;
    private MediaRecorder recorder = null;
    private int state = MEDIA_NONE;
    private String tempFile = null;

    public AudioPlayer(AudioHandler handler2, String id2) {
        this.handler = handler2;
        this.id = id2;
        this.tempFile = Environment.getExternalStorageDirectory().getAbsolutePath() + "/tmprecording.mp3";
    }

    public void destroy() {
        if (this.mPlayer != null) {
            stopPlaying();
            this.mPlayer.release();
            this.mPlayer = null;
        }
        if (this.recorder != null) {
            stopRecording();
            this.recorder.release();
            this.recorder = null;
        }
    }

    public void startRecording(String file) {
        if (this.mPlayer != null) {
            System.out.println("AudioPlayer Error: Can't record in play mode.");
            this.handler.sendJavascript("PhoneGap.Media.onStatus('" + this.id + "', " + MEDIA_ERROR + ", " + MEDIA_ERROR_PLAY_MODE_SET + ");");
            return;
        } else if (this.recorder == null) {
            this.audioFile = file;
            this.recorder = new MediaRecorder();
            this.recorder.setAudioSource(1);
            this.recorder.setOutputFormat(0);
            this.recorder.setAudioEncoder(0);
            this.recorder.setOutputFile(this.tempFile);
            try {
                this.recorder.prepare();
                this.recorder.start();
                setState(MEDIA_RUNNING);
                return;
            } catch (IllegalStateException e) {
                e.printStackTrace();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        } else {
            System.out.println("AudioPlayer Error: Already recording.");
            this.handler.sendJavascript("PhoneGap.Media.onStatus('" + this.id + "', " + MEDIA_ERROR + ", " + MEDIA_ERROR_ALREADY_RECORDING + ");");
            return;
        }
        this.handler.sendJavascript("PhoneGap.Media.onStatus('" + this.id + "', " + MEDIA_ERROR + ", " + MEDIA_ERROR_STARTING_RECORDING + ");");
    }

    public void moveFile(String file) {
        new File(this.tempFile).renameTo(new File("/sdcard/" + file));
    }

    public void stopRecording() {
        if (this.recorder != null) {
            try {
                if (this.state == MEDIA_RUNNING) {
                    this.recorder.stop();
                    setState(MEDIA_STOPPED);
                }
                moveFile(this.audioFile);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void startPlaying(String file) {
        if (this.recorder != null) {
            System.out.println("AudioPlayer Error: Can't play in record mode.");
            this.handler.sendJavascript("PhoneGap.Media.onStatus('" + this.id + "', " + MEDIA_ERROR + ", " + MEDIA_ERROR_RECORD_MODE_SET + ");");
        } else if (this.mPlayer == null || this.state == MEDIA_STOPPED) {
            try {
                if (this.mPlayer != null) {
                    this.mPlayer.reset();
                } else {
                    this.mPlayer = new MediaPlayer();
                }
                this.audioFile = file;
                if (isStreaming(file)) {
                    this.mPlayer.setDataSource(file);
                    this.mPlayer.setAudioStreamType(3);
                    setState(MEDIA_STARTING);
                    this.mPlayer.setOnPreparedListener(this);
                    this.mPlayer.prepareAsync();
                    return;
                }
                if (file.startsWith("/android_asset/")) {
                    AssetFileDescriptor fd = this.handler.ctx.getBaseContext().getAssets().openFd(file.substring(15));
                    this.mPlayer.setDataSource(fd.getFileDescriptor(), fd.getStartOffset(), fd.getLength());
                } else {
                    this.mPlayer.setDataSource("/sdcard/" + file);
                }
                setState(MEDIA_STARTING);
                this.mPlayer.setOnPreparedListener(this);
                this.mPlayer.prepare();
                this.duration = getDurationInSeconds();
            } catch (Exception e) {
                e.printStackTrace();
                this.handler.sendJavascript("PhoneGap.Media.onStatus('" + this.id + "', " + MEDIA_ERROR + ", " + MEDIA_ERROR_STARTING_PLAYBACK + ");");
            }
        } else if (this.state == MEDIA_PAUSED || this.state == MEDIA_STARTING) {
            this.mPlayer.start();
            setState(MEDIA_RUNNING);
        } else {
            System.out.println("AudioPlayer Error: startPlaying() called during invalid state: " + this.state);
            this.handler.sendJavascript("PhoneGap.Media.onStatus('" + this.id + "', " + MEDIA_ERROR + ", " + MEDIA_ERROR_RESUME_STATE + ");");
        }
    }

    public void seekToPlaying(int milliseconds) {
        if (this.mPlayer != null) {
            this.mPlayer.seekTo(milliseconds);
        }
    }

    public void pausePlaying() {
        if (this.state == MEDIA_RUNNING) {
            this.mPlayer.pause();
            setState(MEDIA_PAUSED);
            return;
        }
        System.out.println("AudioPlayer Error: pausePlaying() called during invalid state: " + this.state);
        this.handler.sendJavascript("PhoneGap.Media.onStatus('" + this.id + "', " + MEDIA_ERROR + ", " + MEDIA_ERROR_PAUSE_STATE + ");");
    }

    public void stopPlaying() {
        if (this.state == MEDIA_RUNNING || this.state == MEDIA_PAUSED) {
            this.mPlayer.stop();
            setState(MEDIA_STOPPED);
            return;
        }
        System.out.println("AudioPlayer Error: stopPlaying() called during invalid state: " + this.state);
        this.handler.sendJavascript("PhoneGap.Media.onStatus('" + this.id + "', " + MEDIA_ERROR + ", " + MEDIA_ERROR_STOP_STATE + ");");
    }

    public void onCompletion(MediaPlayer mPlayer2) {
        setState(MEDIA_STOPPED);
    }

    public long getCurrentPosition() {
        if (this.state == MEDIA_RUNNING || this.state == MEDIA_PAUSED) {
            return (long) this.mPlayer.getCurrentPosition();
        }
        return -1;
    }

    public boolean isStreaming(String file) {
        if (file.contains("http://")) {
            return true;
        }
        return false;
    }

    public float getDuration(String file) {
        if (this.recorder != null) {
            return -2.0f;
        }
        if (this.mPlayer != null) {
            return this.duration;
        }
        this.prepareOnly = true;
        startPlaying(file);
        return this.duration;
    }

    public void onPrepared(MediaPlayer mPlayer2) {
        this.mPlayer.setOnCompletionListener(this);
        if (!this.prepareOnly) {
            this.mPlayer.start();
            setState(MEDIA_RUNNING);
        }
        this.duration = getDurationInSeconds();
        this.prepareOnly = false;
        this.handler.sendJavascript("PhoneGap.Media.onStatus('" + this.id + "', " + MEDIA_DURATION + "," + this.duration + ");");
    }

    private float getDurationInSeconds() {
        return ((float) this.mPlayer.getDuration()) / 1000.0f;
    }

    public boolean onError(MediaPlayer mPlayer2, int arg1, int arg2) {
        System.out.println("AudioPlayer.onError(" + arg1 + ", " + arg2 + ")");
        this.mPlayer.stop();
        this.mPlayer.release();
        this.handler.sendJavascript("PhoneGap.Media.onStatus('" + this.id + "', " + MEDIA_ERROR + ", " + arg1 + ");");
        return false;
    }

    private void setState(int state2) {
        if (this.state != state2) {
            this.handler.sendJavascript("PhoneGap.Media.onStatus('" + this.id + "', " + MEDIA_STATE + ", " + state2 + ");");
        }
        this.state = state2;
    }
}
