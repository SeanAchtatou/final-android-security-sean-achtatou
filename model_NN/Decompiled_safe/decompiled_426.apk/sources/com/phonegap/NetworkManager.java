package com.phonegap;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import com.phonegap.api.PhonegapActivity;
import com.phonegap.api.Plugin;
import com.phonegap.api.PluginResult;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;

public class NetworkManager extends Plugin {
    public static final String CDMA = "cdma";
    public static final String EDGE = "edge";
    public static final String GPRS = "gprs";
    public static final String GSM = "gsm";
    private static final String LOG_TAG = "NetworkManager";
    public static final String LTE = "lte";
    public static final String MOBILE = "mobile";
    public static int NOT_REACHABLE = 0;
    public static int REACHABLE_VIA_CARRIER_DATA_NETWORK = 1;
    public static int REACHABLE_VIA_WIFI_NETWORK = 2;
    public static final String TYPE_2G = "2g";
    public static final String TYPE_3G = "3g";
    public static final String TYPE_4G = "4g";
    public static final String TYPE_ETHERNET = "ethernet";
    public static final String TYPE_NONE = "none";
    public static final String TYPE_UNKNOWN = "unknown";
    public static final String TYPE_WIFI = "wifi";
    public static final String UMB = "umb";
    public static final String UMTS = "umts";
    public static final String WIFI = "wifi";
    public static final String WIMAX = "wimax";
    private String connectionCallbackId;
    BroadcastReceiver receiver = null;
    ConnectivityManager sockMan;

    public void setContext(PhonegapActivity ctx) {
        super.setContext(ctx);
        this.sockMan = (ConnectivityManager) ctx.getSystemService("connectivity");
        this.connectionCallbackId = null;
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        if (this.receiver == null) {
            this.receiver = new BroadcastReceiver() {
                public void onReceive(Context context, Intent intent) {
                    NetworkManager.this.updateConnectionInfo((NetworkInfo) intent.getParcelableExtra("networkInfo"));
                }
            };
            ctx.registerReceiver(this.receiver, intentFilter);
        }
    }

    public PluginResult execute(String action, JSONArray args, String callbackId) {
        PluginResult.Status status = PluginResult.Status.OK;
        try {
            if (action.equals("isAvailable")) {
                return new PluginResult(status, isAvailable());
            }
            if (action.equals("isWifiActive")) {
                return new PluginResult(status, isWifiActive());
            }
            if (action.equals("isReachable")) {
                return new PluginResult(status, isReachable(args.getString(0), args.getBoolean(1)));
            }
            if (!action.equals("getConnectionInfo")) {
                return new PluginResult(status, "");
            }
            this.connectionCallbackId = callbackId;
            PluginResult pluginResult = new PluginResult(status, getConnectionInfo(this.sockMan.getActiveNetworkInfo()));
            pluginResult.setKeepCallback(true);
            return pluginResult;
        } catch (JSONException e) {
            return new PluginResult(PluginResult.Status.JSON_EXCEPTION);
        }
    }

    public boolean isSynch(String action) {
        return false;
    }

    public void onDestroy() {
        if (this.receiver != null) {
            try {
                this.ctx.unregisterReceiver(this.receiver);
            } catch (Exception e) {
                Exception e2 = e;
                Log.e(LOG_TAG, "Error unregistering network receiver: " + e2.getMessage(), e2);
            }
        }
    }

    /* access modifiers changed from: private */
    public void updateConnectionInfo(NetworkInfo info) {
        sendUpdate(getConnectionInfo(info));
    }

    private String getConnectionInfo(NetworkInfo info) {
        if (info == null || !info.isConnected()) {
            return TYPE_NONE;
        }
        return getType(info);
    }

    private void sendUpdate(String type) {
        PluginResult result = new PluginResult(PluginResult.Status.OK, type);
        result.setKeepCallback(true);
        success(result, this.connectionCallbackId);
    }

    private String getType(NetworkInfo info) {
        if (info == null) {
            return TYPE_NONE;
        }
        String type = info.getTypeName();
        if (type.toLowerCase().equals("wifi")) {
            return "wifi";
        }
        if (type.toLowerCase().equals(MOBILE)) {
            String type2 = info.getSubtypeName();
            if (type2.toLowerCase().equals(GSM) || type2.toLowerCase().equals(GPRS) || type2.toLowerCase().equals(EDGE)) {
                return TYPE_2G;
            }
            if (type2.toLowerCase().equals(CDMA) || type2.toLowerCase().equals(UMTS)) {
                return TYPE_3G;
            }
            if (type2.toLowerCase().equals(LTE) || type2.toLowerCase().equals(UMB)) {
                return TYPE_4G;
            }
        }
        return TYPE_UNKNOWN;
    }

    public boolean isAvailable() {
        NetworkInfo info = this.sockMan.getActiveNetworkInfo();
        if (info != null) {
            return info.isConnected();
        }
        return false;
    }

    public boolean isWifiActive() {
        NetworkInfo info = this.sockMan.getActiveNetworkInfo();
        if (info != null) {
            return info.getTypeName().equals("WIFI");
        }
        return false;
    }

    public int isReachable(String uri, boolean isIpAddress) {
        int reachable = NOT_REACHABLE;
        if (uri.indexOf("http://") == -1 && uri.indexOf("https://") == -1) {
            uri = "http://" + uri;
        }
        if (!isAvailable()) {
            return reachable;
        }
        try {
            new DefaultHttpClient().execute(new HttpGet(uri));
            if (isWifiActive()) {
                return REACHABLE_VIA_WIFI_NETWORK;
            }
            return REACHABLE_VIA_CARRIER_DATA_NETWORK;
        } catch (Exception e) {
            return NOT_REACHABLE;
        }
    }
}
