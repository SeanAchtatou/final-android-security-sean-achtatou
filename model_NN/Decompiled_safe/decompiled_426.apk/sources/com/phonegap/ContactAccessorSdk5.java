package com.phonegap;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.ContentProviderOperation;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.net.Uri;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.util.Log;
import android.webkit.WebView;
import com.phonegap.ContactAccessor;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ContactAccessorSdk5 extends ContactAccessor {
    private static final String EMAIL_REGEXP = ".+@.+\\.+.+";
    private static final long MAX_PHOTO_SIZE = 1048576;
    private static final Map<String, String> dbMap = new HashMap();

    static {
        dbMap.put("id", "_id");
        dbMap.put("displayName", "display_name");
        dbMap.put("name", "data1");
        dbMap.put("name.formatted", "data1");
        dbMap.put("name.familyName", "data3");
        dbMap.put("name.givenName", "data2");
        dbMap.put("name.middleName", "data5");
        dbMap.put("name.honorificPrefix", "data4");
        dbMap.put("name.honorificSuffix", "data6");
        dbMap.put("nickname", "data1");
        dbMap.put("phoneNumbers", "data1");
        dbMap.put("phoneNumbers.value", "data1");
        dbMap.put("emails", "data1");
        dbMap.put("emails.value", "data1");
        dbMap.put("addresses", "data1");
        dbMap.put("addresses.formatted", "data1");
        dbMap.put("addresses.streetAddress", "data4");
        dbMap.put("addresses.locality", "data7");
        dbMap.put("addresses.region", "data8");
        dbMap.put("addresses.postalCode", "data9");
        dbMap.put("addresses.country", "data10");
        dbMap.put("ims", "data1");
        dbMap.put("ims.value", "data1");
        dbMap.put("organizations", "data1");
        dbMap.put("organizations.name", "data1");
        dbMap.put("organizations.department", "data5");
        dbMap.put("organizations.title", "data4");
        dbMap.put("birthday", "vnd.android.cursor.item/contact_event");
        dbMap.put("note", "data1");
        dbMap.put("photos.value", "vnd.android.cursor.item/photo");
        dbMap.put("urls", "data1");
        dbMap.put("urls.value", "data1");
    }

    public ContactAccessorSdk5(WebView view, Activity app) {
        this.mApp = app;
        this.mView = view;
    }

    public JSONArray search(JSONArray fields, JSONObject options) {
        String searchTerm;
        JSONException e;
        JSONArray organizations;
        JSONArray addresses;
        JSONArray phones;
        JSONArray emails;
        JSONArray ims;
        JSONArray websites;
        long totalStart = System.currentTimeMillis();
        int limit = Integer.MAX_VALUE;
        if (options != null) {
            String searchTerm2 = options.optString("filter");
            if (searchTerm2.length() == 0) {
                searchTerm = "%";
            } else {
                searchTerm = "%" + searchTerm2 + "%";
            }
            try {
                if (!options.getBoolean("multiple")) {
                    limit = 1;
                }
            } catch (JSONException e2) {
            }
        } else {
            searchTerm = "%";
        }
        HashMap<String, Boolean> populate = buildPopulationSet(fields);
        ContactAccessor.WhereOptions whereOptions = buildWhereClause(fields, searchTerm);
        Cursor idCursor = this.mApp.getContentResolver().query(ContactsContract.Data.CONTENT_URI, new String[]{"contact_id"}, whereOptions.getWhere(), whereOptions.getWhereArgs(), "contact_id ASC");
        HashSet hashSet = new HashSet();
        while (idCursor.moveToNext()) {
            hashSet.add(idCursor.getString(idCursor.getColumnIndex("contact_id")));
        }
        idCursor.close();
        ContactAccessor.WhereOptions idOptions = buildIdClause(hashSet, searchTerm);
        Cursor c = this.mApp.getContentResolver().query(ContactsContract.Data.CONTENT_URI, null, idOptions.getWhere(), idOptions.getWhereArgs(), "contact_id ASC");
        String contactId = "";
        String oldContactId = "";
        boolean newContact = true;
        JSONArray contacts = new JSONArray();
        JSONObject contact = new JSONObject();
        JSONArray organizations2 = new JSONArray();
        JSONArray addresses2 = new JSONArray();
        JSONArray phones2 = new JSONArray();
        JSONArray emails2 = new JSONArray();
        JSONArray ims2 = new JSONArray();
        JSONArray websites2 = new JSONArray();
        JSONArray photos = new JSONArray();
        if (c.getCount() > 0) {
            while (c.moveToNext() && contacts.length() <= limit - 1) {
                try {
                    contactId = c.getString(c.getColumnIndex("contact_id"));
                    String rawId = c.getString(c.getColumnIndex("raw_contact_id"));
                    if (c.getPosition() == 0) {
                        oldContactId = contactId;
                    }
                    if (!oldContactId.equals(contactId)) {
                        contacts.put(populateContact(contact, organizations2, addresses2, phones2, emails2, ims2, websites2, photos));
                        JSONObject contact2 = new JSONObject();
                        try {
                            organizations = new JSONArray();
                            try {
                                addresses = new JSONArray();
                                try {
                                    phones = new JSONArray();
                                } catch (JSONException e3) {
                                    e = e3;
                                    addresses2 = addresses;
                                    organizations2 = organizations;
                                    contact = contact2;
                                    Log.e("ContactsAccessor", e.getMessage(), e);
                                    oldContactId = contactId;
                                }
                            } catch (JSONException e4) {
                                e = e4;
                                organizations2 = organizations;
                                contact = contact2;
                                Log.e("ContactsAccessor", e.getMessage(), e);
                                oldContactId = contactId;
                            }
                        } catch (JSONException e5) {
                            e = e5;
                            contact = contact2;
                            Log.e("ContactsAccessor", e.getMessage(), e);
                            oldContactId = contactId;
                        }
                        try {
                            emails = new JSONArray();
                            try {
                                ims = new JSONArray();
                                try {
                                    websites = new JSONArray();
                                } catch (JSONException e6) {
                                    e = e6;
                                    ims2 = ims;
                                    emails2 = emails;
                                    phones2 = phones;
                                    addresses2 = addresses;
                                    organizations2 = organizations;
                                    contact = contact2;
                                    Log.e("ContactsAccessor", e.getMessage(), e);
                                    oldContactId = contactId;
                                }
                            } catch (JSONException e7) {
                                e = e7;
                                emails2 = emails;
                                phones2 = phones;
                                addresses2 = addresses;
                                organizations2 = organizations;
                                contact = contact2;
                                Log.e("ContactsAccessor", e.getMessage(), e);
                                oldContactId = contactId;
                            }
                        } catch (JSONException e8) {
                            e = e8;
                            phones2 = phones;
                            addresses2 = addresses;
                            organizations2 = organizations;
                            contact = contact2;
                            Log.e("ContactsAccessor", e.getMessage(), e);
                            oldContactId = contactId;
                        }
                        try {
                            newContact = true;
                            photos = new JSONArray();
                            websites2 = websites;
                            ims2 = ims;
                            emails2 = emails;
                            phones2 = phones;
                            addresses2 = addresses;
                            organizations2 = organizations;
                            contact = contact2;
                        } catch (JSONException e9) {
                            e = e9;
                            websites2 = websites;
                            ims2 = ims;
                            emails2 = emails;
                            phones2 = phones;
                            addresses2 = addresses;
                            organizations2 = organizations;
                            contact = contact2;
                            Log.e("ContactsAccessor", e.getMessage(), e);
                            oldContactId = contactId;
                        }
                    }
                    if (newContact) {
                        newContact = false;
                        contact.put("id", contactId);
                        contact.put("rawId", rawId);
                        contact.put("displayName", c.getString(c.getColumnIndex("data1")));
                    }
                    String mimetype = c.getString(c.getColumnIndex("mimetype"));
                    if (mimetype.equals("vnd.android.cursor.item/name") && isRequired("name", populate)) {
                        contact.put("name", nameQuery(c));
                        oldContactId = contactId;
                    } else if (mimetype.equals("vnd.android.cursor.item/phone_v2") && isRequired("phoneNumbers", populate)) {
                        phones2.put(phoneQuery(c));
                        oldContactId = contactId;
                    } else if (mimetype.equals("vnd.android.cursor.item/email_v2") && isRequired("emails", populate)) {
                        emails2.put(emailQuery(c));
                        oldContactId = contactId;
                    } else if (mimetype.equals("vnd.android.cursor.item/postal-address_v2") && isRequired("addresses", populate)) {
                        addresses2.put(addressQuery(c));
                        oldContactId = contactId;
                    } else if (mimetype.equals("vnd.android.cursor.item/organization") && isRequired("organizations", populate)) {
                        organizations2.put(organizationQuery(c));
                        oldContactId = contactId;
                    } else if (mimetype.equals("vnd.android.cursor.item/im") && isRequired("ims", populate)) {
                        ims2.put(imQuery(c));
                        oldContactId = contactId;
                    } else if (mimetype.equals("vnd.android.cursor.item/note") && isRequired("note", populate)) {
                        contact.put("note", c.getString(c.getColumnIndex("data1")));
                        oldContactId = contactId;
                    } else if (mimetype.equals("vnd.android.cursor.item/nickname") && isRequired("nickname", populate)) {
                        contact.put("nickname", c.getString(c.getColumnIndex("data1")));
                        oldContactId = contactId;
                    } else if (!mimetype.equals("vnd.android.cursor.item/website") || !isRequired("urls", populate)) {
                        if (mimetype.equals("vnd.android.cursor.item/contact_event")) {
                            if (3 == c.getInt(c.getColumnIndex("data2")) && isRequired("birthday", populate)) {
                                contact.put("birthday", c.getString(c.getColumnIndex("data1")));
                            }
                        } else if (mimetype.equals("vnd.android.cursor.item/photo") && isRequired("photos", populate)) {
                            photos.put(photoQuery(c, contactId));
                        }
                        oldContactId = contactId;
                    } else {
                        websites2.put(websiteQuery(c));
                        oldContactId = contactId;
                    }
                } catch (JSONException e10) {
                    e = e10;
                    Log.e("ContactsAccessor", e.getMessage(), e);
                    oldContactId = contactId;
                }
            }
            if (contacts.length() < limit) {
                contacts.put(populateContact(contact, organizations2, addresses2, phones2, emails2, ims2, websites2, photos));
            }
        }
        c.close();
        Log.d("ContactsAccessor", "Total time = " + (System.currentTimeMillis() - totalStart));
        return contacts;
    }

    /* Debug info: failed to restart local var, previous not found, register: 6 */
    private ContactAccessor.WhereOptions buildIdClause(Set<String> contactIds, String searchTerm) {
        ContactAccessor.WhereOptions options = new ContactAccessor.WhereOptions();
        if (searchTerm.equals("%")) {
            options.setWhere("(contact_id LIKE ? )");
            options.setWhereArgs(new String[]{searchTerm});
        } else {
            Iterator<String> it = contactIds.iterator();
            StringBuffer buffer = new StringBuffer("(");
            while (it.hasNext()) {
                buffer.append("'" + it.next() + "'");
                if (it.hasNext()) {
                    buffer.append(",");
                }
            }
            buffer.append(")");
            options.setWhere("contact_id IN " + buffer.toString());
            options.setWhereArgs(null);
        }
        return options;
    }

    private JSONObject populateContact(JSONObject contact, JSONArray organizations, JSONArray addresses, JSONArray phones, JSONArray emails, JSONArray ims, JSONArray websites, JSONArray photos) {
        try {
            contact.put("organizations", organizations);
            contact.put("addresses", addresses);
            contact.put("phoneNumbers", phones);
            contact.put("emails", emails);
            contact.put("ims", ims);
            contact.put("websites", websites);
            contact.put("photos", photos);
        } catch (JSONException e) {
            JSONException e2 = e;
            Log.e("ContactsAccessor", e2.getMessage(), e2);
        }
        return contact;
    }

    /* Debug info: failed to restart local var, previous not found, register: 11 */
    private ContactAccessor.WhereOptions buildWhereClause(JSONArray fields, String searchTerm) {
        ArrayList<String> where = new ArrayList<>();
        ArrayList<String> whereArgs = new ArrayList<>();
        ContactAccessor.WhereOptions options = new ContactAccessor.WhereOptions();
        if ("%".equals(searchTerm)) {
            options.setWhere("(display_name LIKE ? )");
            options.setWhereArgs(new String[]{searchTerm});
        } else {
            int i = 0;
            while (i < fields.length()) {
                try {
                    String key = fields.getString(i);
                    if (key.startsWith("displayName")) {
                        where.add("(" + dbMap.get(key) + " LIKE ? )");
                        whereArgs.add(searchTerm);
                    } else if (key.startsWith("name")) {
                        where.add("(" + dbMap.get(key) + " LIKE ? AND " + "mimetype" + " = ? )");
                        whereArgs.add(searchTerm);
                        whereArgs.add("vnd.android.cursor.item/name");
                    } else if (key.startsWith("nickname")) {
                        where.add("(" + dbMap.get(key) + " LIKE ? AND " + "mimetype" + " = ? )");
                        whereArgs.add(searchTerm);
                        whereArgs.add("vnd.android.cursor.item/nickname");
                    } else if (key.startsWith("phoneNumbers")) {
                        where.add("(" + dbMap.get(key) + " LIKE ? AND " + "mimetype" + " = ? )");
                        whereArgs.add(searchTerm);
                        whereArgs.add("vnd.android.cursor.item/phone_v2");
                    } else if (key.startsWith("emails")) {
                        where.add("(" + dbMap.get(key) + " LIKE ? AND " + "mimetype" + " = ? )");
                        whereArgs.add(searchTerm);
                        whereArgs.add("vnd.android.cursor.item/email_v2");
                    } else if (key.startsWith("addresses")) {
                        where.add("(" + dbMap.get(key) + " LIKE ? AND " + "mimetype" + " = ? )");
                        whereArgs.add(searchTerm);
                        whereArgs.add("vnd.android.cursor.item/postal-address_v2");
                    } else if (key.startsWith("ims")) {
                        where.add("(" + dbMap.get(key) + " LIKE ? AND " + "mimetype" + " = ? )");
                        whereArgs.add(searchTerm);
                        whereArgs.add("vnd.android.cursor.item/im");
                    } else if (key.startsWith("organizations")) {
                        where.add("(" + dbMap.get(key) + " LIKE ? AND " + "mimetype" + " = ? )");
                        whereArgs.add(searchTerm);
                        whereArgs.add("vnd.android.cursor.item/organization");
                    } else if (key.startsWith("note")) {
                        where.add("(" + dbMap.get(key) + " LIKE ? AND " + "mimetype" + " = ? )");
                        whereArgs.add(searchTerm);
                        whereArgs.add("vnd.android.cursor.item/note");
                    } else if (key.startsWith("urls")) {
                        where.add("(" + dbMap.get(key) + " LIKE ? AND " + "mimetype" + " = ? )");
                        whereArgs.add(searchTerm);
                        whereArgs.add("vnd.android.cursor.item/website");
                    }
                    i++;
                } catch (JSONException e) {
                    JSONException e2 = e;
                    Log.e("ContactsAccessor", e2.getMessage(), e2);
                }
            }
            StringBuffer selection = new StringBuffer();
            for (int i2 = 0; i2 < where.size(); i2++) {
                selection.append((String) where.get(i2));
                if (i2 != where.size() - 1) {
                    selection.append(" OR ");
                }
            }
            options.setWhere(selection.toString());
            String[] selectionArgs = new String[whereArgs.size()];
            for (int i3 = 0; i3 < whereArgs.size(); i3++) {
                selectionArgs[i3] = (String) whereArgs.get(i3);
            }
            options.setWhereArgs(selectionArgs);
        }
        return options;
    }

    private JSONObject organizationQuery(Cursor cursor) {
        JSONObject organization = new JSONObject();
        try {
            organization.put("id", cursor.getString(cursor.getColumnIndex("_id")));
            organization.put("department", cursor.getString(cursor.getColumnIndex("data5")));
            organization.put("name", cursor.getString(cursor.getColumnIndex("data1")));
            organization.put("title", cursor.getString(cursor.getColumnIndex("data4")));
        } catch (JSONException e) {
            JSONException e2 = e;
            Log.e("ContactsAccessor", e2.getMessage(), e2);
        }
        return organization;
    }

    private JSONObject addressQuery(Cursor cursor) {
        JSONObject address = new JSONObject();
        try {
            address.put("id", cursor.getString(cursor.getColumnIndex("_id")));
            address.put("formatted", cursor.getString(cursor.getColumnIndex("data1")));
            address.put("streetAddress", cursor.getString(cursor.getColumnIndex("data4")));
            address.put("locality", cursor.getString(cursor.getColumnIndex("data7")));
            address.put("region", cursor.getString(cursor.getColumnIndex("data8")));
            address.put("postalCode", cursor.getString(cursor.getColumnIndex("data9")));
            address.put("country", cursor.getString(cursor.getColumnIndex("data10")));
        } catch (JSONException e) {
            JSONException e2 = e;
            Log.e("ContactsAccessor", e2.getMessage(), e2);
        }
        return address;
    }

    private JSONObject nameQuery(Cursor cursor) {
        JSONObject contactName = new JSONObject();
        try {
            String familyName = cursor.getString(cursor.getColumnIndex("data3"));
            String givenName = cursor.getString(cursor.getColumnIndex("data2"));
            String middleName = cursor.getString(cursor.getColumnIndex("data5"));
            String honorificPrefix = cursor.getString(cursor.getColumnIndex("data4"));
            String honorificSuffix = cursor.getString(cursor.getColumnIndex("data6"));
            StringBuffer formatted = new StringBuffer("");
            if (honorificPrefix != null) {
                formatted.append(honorificPrefix + " ");
            }
            if (givenName != null) {
                formatted.append(givenName + " ");
            }
            if (middleName != null) {
                formatted.append(middleName + " ");
            }
            if (familyName != null) {
                formatted.append(familyName + " ");
            }
            if (honorificSuffix != null) {
                formatted.append(honorificSuffix + " ");
            }
            contactName.put("familyName", familyName);
            contactName.put("givenName", givenName);
            contactName.put("middleName", middleName);
            contactName.put("honorificPrefix", honorificPrefix);
            contactName.put("honorificSuffix", honorificSuffix);
            contactName.put("formatted", formatted);
        } catch (JSONException e) {
            JSONException e2 = e;
            Log.e("ContactsAccessor", e2.getMessage(), e2);
        }
        return contactName;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
    private JSONObject phoneQuery(Cursor cursor) {
        JSONObject phoneNumber = new JSONObject();
        try {
            phoneNumber.put("id", cursor.getString(cursor.getColumnIndex("_id")));
            phoneNumber.put("pref", false);
            phoneNumber.put("value", cursor.getString(cursor.getColumnIndex("data1")));
            phoneNumber.put("type", getPhoneType(cursor.getInt(cursor.getColumnIndex("data2"))));
        } catch (JSONException e) {
            JSONException e2 = e;
            Log.e("ContactsAccessor", e2.getMessage(), e2);
        } catch (Exception e3) {
            Exception excp = e3;
            Log.e("ContactsAccessor", excp.getMessage(), excp);
        }
        return phoneNumber;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
    private JSONObject emailQuery(Cursor cursor) {
        JSONObject email = new JSONObject();
        try {
            email.put("id", cursor.getString(cursor.getColumnIndex("_id")));
            email.put("pref", false);
            email.put("value", cursor.getString(cursor.getColumnIndex("data1")));
            email.put("type", getContactType(cursor.getInt(cursor.getColumnIndex("data2"))));
        } catch (JSONException e) {
            JSONException e2 = e;
            Log.e("ContactsAccessor", e2.getMessage(), e2);
        }
        return email;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
    private JSONObject imQuery(Cursor cursor) {
        JSONObject im = new JSONObject();
        try {
            im.put("id", cursor.getString(cursor.getColumnIndex("_id")));
            im.put("pref", false);
            im.put("value", cursor.getString(cursor.getColumnIndex("data1")));
            im.put("type", getContactType(cursor.getInt(cursor.getColumnIndex("data2"))));
        } catch (JSONException e) {
            JSONException e2 = e;
            Log.e("ContactsAccessor", e2.getMessage(), e2);
        }
        return im;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
    private JSONObject websiteQuery(Cursor cursor) {
        JSONObject website = new JSONObject();
        try {
            website.put("id", cursor.getString(cursor.getColumnIndex("_id")));
            website.put("pref", false);
            website.put("value", cursor.getString(cursor.getColumnIndex("data1")));
            website.put("type", getContactType(cursor.getInt(cursor.getColumnIndex("data2"))));
        } catch (JSONException e) {
            JSONException e2 = e;
            Log.e("ContactsAccessor", e2.getMessage(), e2);
        }
        return website;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
    private JSONObject photoQuery(Cursor cursor, String contactId) {
        JSONObject photo = new JSONObject();
        try {
            photo.put("id", cursor.getString(cursor.getColumnIndex("_id")));
            photo.put("pref", false);
            photo.put("type", "url");
            photo.put("value", Uri.withAppendedPath(ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, new Long(contactId).longValue()), "photo").toString());
        } catch (JSONException e) {
            JSONException e2 = e;
            Log.e("ContactsAccessor", e2.getMessage(), e2);
        }
        return photo;
    }

    public boolean save(JSONObject contact) {
        Account[] accounts = AccountManager.get(this.mApp).getAccounts();
        Account account = null;
        if (accounts.length == 1) {
            account = accounts[0];
        } else if (accounts.length > 1) {
            Account[] arr$ = accounts;
            int len$ = arr$.length;
            int i$ = 0;
            while (true) {
                if (i$ >= len$) {
                    break;
                }
                Account a = arr$[i$];
                if (a.type.contains("eas") && a.name.matches(EMAIL_REGEXP)) {
                    account = a;
                    break;
                }
                i$++;
            }
            if (account == null) {
                Account[] arr$2 = accounts;
                int len$2 = arr$2.length;
                int i$2 = 0;
                while (true) {
                    if (i$2 >= len$2) {
                        break;
                    }
                    Account a2 = arr$2[i$2];
                    if (a2.type.contains("com.google") && a2.name.matches(EMAIL_REGEXP)) {
                        account = a2;
                        break;
                    }
                    i$2++;
                }
            }
            if (account == null) {
                Account[] arr$3 = accounts;
                int len$3 = arr$3.length;
                int i$3 = 0;
                while (true) {
                    if (i$3 >= len$3) {
                        break;
                    }
                    Account a3 = arr$3[i$3];
                    if (a3.name.matches(EMAIL_REGEXP)) {
                        account = a3;
                        break;
                    }
                    i$3++;
                }
            }
        }
        if (account == null) {
            return false;
        }
        String id = getJsonString(contact, "id");
        if (id == null) {
            return createNewContact(contact, account);
        }
        return modifyContact(id, contact, account);
    }

    private boolean modifyContact(String id, JSONObject contact, Account account) {
        int rawId = new Integer(getJsonString(contact, "rawId")).intValue();
        ArrayList<ContentProviderOperation> ops = new ArrayList<>();
        ops.add(ContentProviderOperation.newUpdate(ContactsContract.RawContacts.CONTENT_URI).withValue("account_type", account.type).withValue("account_name", account.name).build());
        try {
            String displayName = getJsonString(contact, "displayName");
            JSONObject name = contact.getJSONObject("name");
            if (!(displayName == null && name == null)) {
                ContentProviderOperation.Builder builder = ContentProviderOperation.newUpdate(ContactsContract.Data.CONTENT_URI).withSelection("contact_id=? AND mimetype=?", new String[]{id, "vnd.android.cursor.item/name"});
                if (displayName != null) {
                    builder.withValue("data1", displayName);
                }
                String familyName = getJsonString(name, "familyName");
                if (familyName != null) {
                    builder.withValue("data3", familyName);
                }
                String middleName = getJsonString(name, "middleName");
                if (middleName != null) {
                    builder.withValue("data5", middleName);
                }
                String givenName = getJsonString(name, "givenName");
                if (givenName != null) {
                    builder.withValue("data2", givenName);
                }
                String honorificPrefix = getJsonString(name, "honorificPrefix");
                if (honorificPrefix != null) {
                    builder.withValue("data4", honorificPrefix);
                }
                String honorificSuffix = getJsonString(name, "honorificSuffix");
                if (honorificSuffix != null) {
                    builder.withValue("data6", honorificSuffix);
                }
                ops.add(builder.build());
            }
        } catch (JSONException e) {
            Log.d("ContactsAccessor", "Could not get name");
        }
        try {
            JSONArray phones = contact.getJSONArray("phoneNumbers");
            if (phones != null) {
                for (int i = 0; i < phones.length(); i++) {
                    JSONObject phone = (JSONObject) phones.get(i);
                    String phoneId = getJsonString(phone, "id");
                    if (phoneId == null) {
                        ContentValues contentValues = new ContentValues();
                        contentValues.put("raw_contact_id", Integer.valueOf(rawId));
                        contentValues.put("mimetype", "vnd.android.cursor.item/phone_v2");
                        contentValues.put("data1", getJsonString(phone, "value"));
                        contentValues.put("data2", Integer.valueOf(getPhoneType(getJsonString(phone, "type"))));
                        ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI).withValues(contentValues).build());
                    } else {
                        ops.add(ContentProviderOperation.newUpdate(ContactsContract.Data.CONTENT_URI).withSelection("_id=? AND mimetype=?", new String[]{phoneId, "vnd.android.cursor.item/phone_v2"}).withValue("data1", getJsonString(phone, "value")).withValue("data2", Integer.valueOf(getPhoneType(getJsonString(phone, "type")))).build());
                    }
                }
            }
        } catch (JSONException e2) {
            Log.d("ContactsAccessor", "Could not get phone numbers");
        }
        try {
            JSONArray emails = contact.getJSONArray("emails");
            if (emails != null) {
                for (int i2 = 0; i2 < emails.length(); i2++) {
                    JSONObject email = (JSONObject) emails.get(i2);
                    String emailId = getJsonString(email, "id");
                    if (emailId == null) {
                        ContentValues contentValues2 = new ContentValues();
                        contentValues2.put("raw_contact_id", Integer.valueOf(rawId));
                        contentValues2.put("mimetype", "vnd.android.cursor.item/email_v2");
                        contentValues2.put("data1", getJsonString(email, "value"));
                        contentValues2.put("data2", Integer.valueOf(getContactType(getJsonString(email, "type"))));
                        ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI).withValues(contentValues2).build());
                    } else {
                        ops.add(ContentProviderOperation.newUpdate(ContactsContract.Data.CONTENT_URI).withSelection("_id=? AND mimetype=?", new String[]{emailId, "vnd.android.cursor.item/email_v2"}).withValue("data1", getJsonString(email, "value")).withValue("data2", Integer.valueOf(getContactType(getJsonString(email, "type")))).build());
                    }
                }
            }
        } catch (JSONException e3) {
            Log.d("ContactsAccessor", "Could not get emails");
        }
        try {
            JSONArray addresses = contact.getJSONArray("addresses");
            if (addresses != null) {
                for (int i3 = 0; i3 < addresses.length(); i3++) {
                    JSONObject address = (JSONObject) addresses.get(i3);
                    String addressId = getJsonString(address, "id");
                    if (addressId == null) {
                        ContentValues contentValues3 = new ContentValues();
                        contentValues3.put("raw_contact_id", Integer.valueOf(rawId));
                        contentValues3.put("mimetype", "vnd.android.cursor.item/postal-address_v2");
                        contentValues3.put("data1", getJsonString(address, "formatted"));
                        contentValues3.put("data4", getJsonString(address, "streetAddress"));
                        contentValues3.put("data7", getJsonString(address, "locality"));
                        contentValues3.put("data8", getJsonString(address, "region"));
                        contentValues3.put("data9", getJsonString(address, "postalCode"));
                        contentValues3.put("data10", getJsonString(address, "country"));
                        ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI).withValues(contentValues3).build());
                    } else {
                        ops.add(ContentProviderOperation.newUpdate(ContactsContract.Data.CONTENT_URI).withSelection("_id=? AND mimetype=?", new String[]{addressId, "vnd.android.cursor.item/postal-address_v2"}).withValue("data1", getJsonString(address, "formatted")).withValue("data4", getJsonString(address, "streetAddress")).withValue("data7", getJsonString(address, "locality")).withValue("data8", getJsonString(address, "region")).withValue("data9", getJsonString(address, "postalCode")).withValue("data10", getJsonString(address, "country")).build());
                    }
                }
            }
        } catch (JSONException e4) {
            Log.d("ContactsAccessor", "Could not get addresses");
        }
        try {
            JSONArray organizations = contact.getJSONArray("organizations");
            if (organizations != null) {
                for (int i4 = 0; i4 < organizations.length(); i4++) {
                    JSONObject org = (JSONObject) organizations.get(i4);
                    String orgId = getJsonString(org, "id");
                    if (orgId == null) {
                        ContentValues contentValues4 = new ContentValues();
                        contentValues4.put("raw_contact_id", Integer.valueOf(rawId));
                        contentValues4.put("mimetype", "vnd.android.cursor.item/organization");
                        contentValues4.put("data5", getJsonString(org, "department"));
                        contentValues4.put("data1", getJsonString(org, "name"));
                        contentValues4.put("data4", getJsonString(org, "title"));
                        ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI).withValues(contentValues4).build());
                    } else {
                        ops.add(ContentProviderOperation.newUpdate(ContactsContract.Data.CONTENT_URI).withSelection("_id=? AND mimetype=?", new String[]{orgId, "vnd.android.cursor.item/organization"}).withValue("data5", getJsonString(org, "department")).withValue("data1", getJsonString(org, "name")).withValue("data4", getJsonString(org, "title")).build());
                    }
                }
            }
        } catch (JSONException e5) {
            Log.d("ContactsAccessor", "Could not get organizations");
        }
        try {
            JSONArray ims = contact.getJSONArray("ims");
            if (ims != null) {
                for (int i5 = 0; i5 < ims.length(); i5++) {
                    JSONObject im = (JSONObject) ims.get(i5);
                    String imId = getJsonString(im, "id");
                    if (imId == null) {
                        ContentValues contentValues5 = new ContentValues();
                        contentValues5.put("raw_contact_id", Integer.valueOf(rawId));
                        contentValues5.put("mimetype", "vnd.android.cursor.item/im");
                        contentValues5.put("data1", getJsonString(im, "value"));
                        contentValues5.put("data2", Integer.valueOf(getContactType(getJsonString(im, "type"))));
                        ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI).withValues(contentValues5).build());
                    } else {
                        ops.add(ContentProviderOperation.newUpdate(ContactsContract.Data.CONTENT_URI).withSelection("_id=? AND mimetype=?", new String[]{imId, "vnd.android.cursor.item/im"}).withValue("data1", getJsonString(im, "value")).withValue("data2", Integer.valueOf(getContactType(getJsonString(im, "type")))).build());
                    }
                }
            }
        } catch (JSONException e6) {
            Log.d("ContactsAccessor", "Could not get emails");
        }
        ops.add(ContentProviderOperation.newUpdate(ContactsContract.Data.CONTENT_URI).withSelection("contact_id=? AND mimetype=?", new String[]{id, "vnd.android.cursor.item/note"}).withValue("data1", getJsonString(contact, "note")).build());
        String nickname = getJsonString(contact, "nickname");
        if (nickname != null) {
            ops.add(ContentProviderOperation.newUpdate(ContactsContract.Data.CONTENT_URI).withSelection("contact_id=? AND mimetype=?", new String[]{id, "vnd.android.cursor.item/nickname"}).withValue("data1", nickname).build());
        }
        try {
            JSONArray websites = contact.getJSONArray("websites");
            if (websites != null) {
                for (int i6 = 0; i6 < websites.length(); i6++) {
                    JSONObject website = (JSONObject) websites.get(i6);
                    String websiteId = getJsonString(website, "id");
                    if (websiteId == null) {
                        ContentValues contentValues6 = new ContentValues();
                        contentValues6.put("raw_contact_id", Integer.valueOf(rawId));
                        contentValues6.put("mimetype", "vnd.android.cursor.item/website");
                        contentValues6.put("data1", getJsonString(website, "value"));
                        contentValues6.put("data2", Integer.valueOf(getContactType(getJsonString(website, "type"))));
                        ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI).withValues(contentValues6).build());
                    } else {
                        ops.add(ContentProviderOperation.newUpdate(ContactsContract.Data.CONTENT_URI).withSelection("_id=? AND mimetype=?", new String[]{websiteId, "vnd.android.cursor.item/website"}).withValue("data1", getJsonString(website, "value")).withValue("data2", Integer.valueOf(getContactType(getJsonString(website, "type")))).build());
                    }
                }
            }
        } catch (JSONException e7) {
            Log.d("ContactsAccessor", "Could not get websites");
        }
        String birthday = getJsonString(contact, "birthday");
        if (birthday != null) {
            ops.add(ContentProviderOperation.newUpdate(ContactsContract.Data.CONTENT_URI).withSelection("contact_id=? AND mimetype=? AND data2=?", new String[]{id, "vnd.android.cursor.item/contact_event", new String("3")}).withValue("data2", 3).withValue("data1", birthday).build());
        }
        try {
            JSONArray photos = contact.getJSONArray("photos");
            if (photos != null) {
                for (int i7 = 0; i7 < photos.length(); i7++) {
                    JSONObject photo = (JSONObject) photos.get(i7);
                    String photoId = getJsonString(photo, "id");
                    byte[] bytes = getPhotoBytes(getJsonString(photo, "value"));
                    if (photoId == null) {
                        ContentValues contentValues7 = new ContentValues();
                        contentValues7.put("raw_contact_id", Integer.valueOf(rawId));
                        contentValues7.put("mimetype", "vnd.android.cursor.item/photo");
                        contentValues7.put("is_super_primary", (Integer) 1);
                        contentValues7.put("data15", bytes);
                        ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI).withValues(contentValues7).build());
                    } else {
                        ops.add(ContentProviderOperation.newUpdate(ContactsContract.Data.CONTENT_URI).withSelection("_id=? AND mimetype=?", new String[]{photoId, "vnd.android.cursor.item/photo"}).withValue("is_super_primary", 1).withValue("data15", bytes).build());
                    }
                }
            }
        } catch (JSONException e8) {
            Log.d("ContactsAccessor", "Could not get photos");
        }
        try {
            this.mApp.getContentResolver().applyBatch("com.android.contacts", ops);
            return true;
        } catch (RemoteException e9) {
            RemoteException e10 = e9;
            Log.e("ContactsAccessor", e10.getMessage(), e10);
            Log.e("ContactsAccessor", Log.getStackTraceString(e10), e10);
            return false;
        } catch (OperationApplicationException e11) {
            OperationApplicationException e12 = e11;
            Log.e("ContactsAccessor", e12.getMessage(), e12);
            Log.e("ContactsAccessor", Log.getStackTraceString(e12), e12);
            return false;
        }
    }

    private void insertWebsite(ArrayList<ContentProviderOperation> ops, JSONObject website) {
        ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI).withValueBackReference("raw_contact_id", 0).withValue("mimetype", "vnd.android.cursor.item/website").withValue("data1", getJsonString(website, "value")).withValue("data2", Integer.valueOf(getContactType(getJsonString(website, "type")))).build());
    }

    private void insertIm(ArrayList<ContentProviderOperation> ops, JSONObject im) {
        ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI).withValueBackReference("raw_contact_id", 0).withValue("mimetype", "vnd.android.cursor.item/im").withValue("data1", getJsonString(im, "value")).withValue("data2", Integer.valueOf(getContactType(getJsonString(im, "type")))).build());
    }

    private void insertOrganization(ArrayList<ContentProviderOperation> ops, JSONObject org) {
        ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI).withValueBackReference("raw_contact_id", 0).withValue("mimetype", "vnd.android.cursor.item/organization").withValue("data5", getJsonString(org, "department")).withValue("data1", getJsonString(org, "name")).withValue("data4", getJsonString(org, "title")).build());
    }

    private void insertAddress(ArrayList<ContentProviderOperation> ops, JSONObject address) {
        ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI).withValueBackReference("raw_contact_id", 0).withValue("mimetype", "vnd.android.cursor.item/postal-address_v2").withValue("data1", getJsonString(address, "formatted")).withValue("data4", getJsonString(address, "streetAddress")).withValue("data7", getJsonString(address, "locality")).withValue("data8", getJsonString(address, "region")).withValue("data9", getJsonString(address, "postalCode")).withValue("data10", getJsonString(address, "country")).build());
    }

    private void insertEmail(ArrayList<ContentProviderOperation> ops, JSONObject email) {
        ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI).withValueBackReference("raw_contact_id", 0).withValue("mimetype", "vnd.android.cursor.item/email_v2").withValue("data1", getJsonString(email, "value")).withValue("data2", Integer.valueOf(getPhoneType(getJsonString(email, "type")))).build());
    }

    private void insertPhone(ArrayList<ContentProviderOperation> ops, JSONObject phone) {
        ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI).withValueBackReference("raw_contact_id", 0).withValue("mimetype", "vnd.android.cursor.item/phone_v2").withValue("data1", getJsonString(phone, "value")).withValue("data2", Integer.valueOf(getPhoneType(getJsonString(phone, "type")))).build());
    }

    private void insertPhoto(ArrayList<ContentProviderOperation> ops, JSONObject photo) {
        ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI).withValueBackReference("raw_contact_id", 0).withValue("is_super_primary", 1).withValue("mimetype", "vnd.android.cursor.item/photo").withValue("data15", getPhotoBytes(getJsonString(photo, "value"))).build());
    }

    private byte[] getPhotoBytes(String filename) {
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        long totalBytesRead = 0;
        try {
            byte[] data = new byte[8192];
            InputStream in = getPathFromUri(filename);
            while (true) {
                int bytesRead = in.read(data, 0, data.length);
                if (bytesRead == -1 || totalBytesRead > MAX_PHOTO_SIZE) {
                    in.close();
                    buffer.flush();
                } else {
                    buffer.write(data, 0, bytesRead);
                    totalBytesRead += (long) bytesRead;
                }
            }
            in.close();
            buffer.flush();
        } catch (FileNotFoundException e) {
            FileNotFoundException e2 = e;
            Log.e("ContactsAccessor", e2.getMessage(), e2);
        } catch (IOException e3) {
            IOException e4 = e3;
            Log.e("ContactsAccessor", e4.getMessage(), e4);
        }
        return buffer.toByteArray();
    }

    private InputStream getPathFromUri(String path) throws IOException {
        if (path.startsWith("content:")) {
            return this.mApp.getContentResolver().openInputStream(Uri.parse(path));
        } else if (path.startsWith("http:") || path.startsWith("file:")) {
            return new URL(path).openStream();
        } else {
            return new FileInputStream(path);
        }
    }

    private boolean createNewContact(JSONObject contact, Account account) {
        ArrayList<ContentProviderOperation> ops = new ArrayList<>();
        ops.add(ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI).withValue("account_type", account.type).withValue("account_name", account.name).build());
        try {
            JSONObject name = contact.optJSONObject("name");
            String displayName = contact.getString("displayName");
            if (!(displayName == null && name == null)) {
                ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI).withValueBackReference("raw_contact_id", 0).withValue("mimetype", "vnd.android.cursor.item/name").withValue("data1", displayName).withValue("data3", getJsonString(name, "familyName")).withValue("data5", getJsonString(name, "middleName")).withValue("data2", getJsonString(name, "givenName")).withValue("data4", getJsonString(name, "honorificPrefix")).withValue("data6", getJsonString(name, "honorificSuffix")).build());
            }
        } catch (JSONException e) {
            Log.d("ContactsAccessor", "Could not get name object");
        }
        try {
            JSONArray phones = contact.getJSONArray("phoneNumbers");
            if (phones != null) {
                for (int i = 0; i < phones.length(); i++) {
                    insertPhone(ops, (JSONObject) phones.get(i));
                }
            }
        } catch (JSONException e2) {
            Log.d("ContactsAccessor", "Could not get phone numbers");
        }
        try {
            JSONArray emails = contact.getJSONArray("emails");
            if (emails != null) {
                for (int i2 = 0; i2 < emails.length(); i2++) {
                    insertEmail(ops, (JSONObject) emails.get(i2));
                }
            }
        } catch (JSONException e3) {
            Log.d("ContactsAccessor", "Could not get emails");
        }
        try {
            JSONArray addresses = contact.getJSONArray("addresses");
            if (addresses != null) {
                for (int i3 = 0; i3 < addresses.length(); i3++) {
                    insertAddress(ops, (JSONObject) addresses.get(i3));
                }
            }
        } catch (JSONException e4) {
            Log.d("ContactsAccessor", "Could not get addresses");
        }
        try {
            JSONArray organizations = contact.getJSONArray("organizations");
            if (organizations != null) {
                for (int i4 = 0; i4 < organizations.length(); i4++) {
                    insertOrganization(ops, (JSONObject) organizations.get(i4));
                }
            }
        } catch (JSONException e5) {
            Log.d("ContactsAccessor", "Could not get organizations");
        }
        try {
            JSONArray ims = contact.getJSONArray("ims");
            if (ims != null) {
                for (int i5 = 0; i5 < ims.length(); i5++) {
                    insertIm(ops, (JSONObject) ims.get(i5));
                }
            }
        } catch (JSONException e6) {
            Log.d("ContactsAccessor", "Could not get emails");
        }
        String note = getJsonString(contact, "note");
        if (note != null) {
            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI).withValueBackReference("raw_contact_id", 0).withValue("mimetype", "vnd.android.cursor.item/note").withValue("data1", note).build());
        }
        String nickname = getJsonString(contact, "nickname");
        if (nickname != null) {
            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI).withValueBackReference("raw_contact_id", 0).withValue("mimetype", "vnd.android.cursor.item/nickname").withValue("data1", nickname).build());
        }
        try {
            JSONArray websites = contact.getJSONArray("websites");
            if (websites != null) {
                for (int i6 = 0; i6 < websites.length(); i6++) {
                    insertWebsite(ops, (JSONObject) websites.get(i6));
                }
            }
        } catch (JSONException e7) {
            Log.d("ContactsAccessor", "Could not get websites");
        }
        String birthday = getJsonString(contact, "birthday");
        if (birthday != null) {
            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI).withValueBackReference("raw_contact_id", 0).withValue("mimetype", "vnd.android.cursor.item/contact_event").withValue("data2", 3).withValue("data1", birthday).build());
        }
        try {
            JSONArray photos = contact.getJSONArray("photos");
            if (photos != null) {
                for (int i7 = 0; i7 < photos.length(); i7++) {
                    insertPhoto(ops, (JSONObject) photos.get(i7));
                }
            }
        } catch (JSONException e8) {
            Log.d("ContactsAccessor", "Could not get photos");
        }
        try {
            this.mApp.getContentResolver().applyBatch("com.android.contacts", ops);
            return true;
        } catch (RemoteException e9) {
            RemoteException e10 = e9;
            Log.e("ContactsAccessor", e10.getMessage(), e10);
            return false;
        } catch (OperationApplicationException e11) {
            OperationApplicationException e12 = e11;
            Log.e("ContactsAccessor", e12.getMessage(), e12);
            return false;
        }
    }

    public boolean remove(String id) {
        return this.mApp.getContentResolver().delete(ContactsContract.Data.CONTENT_URI, "contact_id = ?", new String[]{id}) > 0;
    }

    private int getPhoneType(String string) {
        if ("home".equals(string.toLowerCase())) {
            return 1;
        }
        if (NetworkManager.MOBILE.equals(string.toLowerCase())) {
            return 2;
        }
        if ("work".equals(string.toLowerCase())) {
            return 3;
        }
        if ("work fax".equals(string.toLowerCase())) {
            return 4;
        }
        if ("home fax".equals(string.toLowerCase())) {
            return 5;
        }
        if ("fax".equals(string.toLowerCase())) {
            return 4;
        }
        if ("pager".equals(string.toLowerCase())) {
            return 6;
        }
        if ("other".equals(string.toLowerCase())) {
            return 7;
        }
        if ("car".equals(string.toLowerCase())) {
            return 9;
        }
        if ("company main".equals(string.toLowerCase())) {
            return 10;
        }
        if ("isdn".equals(string.toLowerCase())) {
            return 11;
        }
        if ("main".equals(string.toLowerCase())) {
            return 12;
        }
        if ("other fax".equals(string.toLowerCase())) {
            return 13;
        }
        if ("radio".equals(string.toLowerCase())) {
            return 14;
        }
        if ("telex".equals(string.toLowerCase())) {
            return 15;
        }
        if ("work mobile".equals(string.toLowerCase())) {
            return 17;
        }
        if ("work pager".equals(string.toLowerCase())) {
            return 18;
        }
        if ("assistant".equals(string.toLowerCase())) {
            return 19;
        }
        if ("mms".equals(string.toLowerCase())) {
            return 20;
        }
        if ("callback".equals(string.toLowerCase())) {
            return 8;
        }
        if ("tty ttd".equals(string.toLowerCase())) {
            return 16;
        }
        return "custom".equals(string.toLowerCase()) ? 0 : 7;
    }

    private String getPhoneType(int type) {
        switch (type) {
            case 0:
                return "custom";
            case 1:
                return "home";
            case 2:
                return NetworkManager.MOBILE;
            case 3:
                return "work";
            case 4:
                return "work fax";
            case 5:
                return "home fax";
            case 6:
                return "pager";
            case 7:
            case 12:
            default:
                return "other";
            case 8:
                return "callback";
            case 9:
                return "car";
            case 10:
                return "company main";
            case 11:
                return "isdn";
            case 13:
                return "other fax";
            case 14:
                return "radio";
            case 15:
                return "telex";
            case 16:
                return "tty tdd";
            case 17:
                return "work mobile";
            case 18:
                return "work pager";
            case 19:
                return "assistant";
            case 20:
                return "mms";
        }
    }

    private int getContactType(String string) {
        if (string != null) {
            if ("home".equals(string.toLowerCase())) {
                return 1;
            }
            if ("work".equals(string.toLowerCase())) {
                return 2;
            }
            if ("other".equals(string.toLowerCase())) {
                return 3;
            }
            if (NetworkManager.MOBILE.equals(string.toLowerCase())) {
                return 4;
            }
            if ("custom".equals(string.toLowerCase())) {
                return 0;
            }
        }
        return 3;
    }

    private String getContactType(int type) {
        switch (type) {
            case 0:
                return "custom";
            case 1:
                return "home";
            case 2:
                return "work";
            case 3:
            default:
                return "other";
            case 4:
                return NetworkManager.MOBILE;
        }
    }
}
