package com.phonegap;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import com.phonegap.api.PhonegapActivity;
import com.phonegap.api.Plugin;
import com.phonegap.api.PluginResult;
import java.util.List;
import org.json.JSONArray;

public class TempListener extends Plugin implements SensorEventListener {
    Sensor mSensor;
    private SensorManager sensorManager;

    public void setContext(PhonegapActivity ctx) {
        super.setContext(ctx);
        this.sensorManager = (SensorManager) ctx.getSystemService("sensor");
    }

    public PluginResult execute(String action, JSONArray args, String callbackId) {
        PluginResult.Status status = PluginResult.Status.OK;
        if (action.equals("start")) {
            start();
        } else if (action.equals("stop")) {
            stop();
        }
        return new PluginResult(status, "");
    }

    public void onDestroy() {
        stop();
    }

    public void start() {
        List<Sensor> list = this.sensorManager.getSensorList(7);
        if (list.size() > 0) {
            this.mSensor = list.get(0);
            this.sensorManager.registerListener(this, this.mSensor, 3);
        }
    }

    public void stop() {
        this.sensorManager.unregisterListener(this);
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    public void onSensorChanged(SensorEvent event) {
        sendJavascript("gotTemp(" + event.values[0] + ");");
    }
}
