package com.minitw.android;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int icon = 2130837504;
    }

    public static final class id {
        public static final int datePicker_end = 2131034115;
        public static final int datePicker_start = 2131034113;
        public static final int textView1 = 2131034112;
        public static final int textView2 = 2131034114;
        public static final int textView_Result = 2131034116;
    }

    public static final class layout {
        public static final int main = 2130903040;
    }

    public static final class string {
        public static final int EndDateDesc = 2130968578;
        public static final int StartDateDesc = 2130968577;
        public static final int app_name = 2130968576;
    }
}
