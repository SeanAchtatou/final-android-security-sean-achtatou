package com.minitw.android;

import android.app.Activity;
import android.os.Bundle;
import android.widget.DatePicker;
import android.widget.TextView;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Months;
import org.joda.time.Period;
import org.joda.time.Weeks;
import org.joda.time.format.DateTimeFormat;

public class Init extends Activity {
    private DatePicker endDatePicker;
    private TextView resultTextView;
    /* access modifiers changed from: private */
    public DateTime selectEndDate;
    /* access modifiers changed from: private */
    public DateTime selectStartDate;
    private DatePicker startDatePicker;

    public Init() {
        this.startDatePicker = null;
        this.endDatePicker = null;
        this.resultTextView = null;
        this.selectStartDate = null;
        this.selectEndDate = null;
        this.selectStartDate = DateTime.now();
        this.selectEndDate = DateTime.now().plus(Period.days(10));
    }

    public String format(int x) {
        String s = new StringBuilder().append(x).toString();
        if (s.length() == 1) {
            return "0" + s;
        }
        return s;
    }

    public void displayMessage() {
        this.resultTextView.setText(String.format(" %s Days\r\n %s Weeks\r\n %s Months", Integer.valueOf(Days.daysBetween(this.selectStartDate, this.selectEndDate).getDays()), Integer.valueOf(Weeks.weeksBetween(this.selectStartDate, this.selectEndDate).getWeeks()), Integer.valueOf(Months.monthsBetween(this.selectStartDate, this.selectEndDate).getMonths())));
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        this.startDatePicker = (DatePicker) findViewById(R.id.datePicker_start);
        this.endDatePicker = (DatePicker) findViewById(R.id.datePicker_end);
        this.resultTextView = (TextView) findViewById(R.id.textView_Result);
        this.startDatePicker.init(this.selectStartDate.getYear(), this.selectStartDate.getMonthOfYear(), this.selectStartDate.getDayOfMonth(), new DatePicker.OnDateChangedListener() {
            public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                try {
                    Init.this.selectStartDate = DateTimeFormat.forPattern("yyyyMMdd").parseDateTime(String.valueOf(year) + Init.this.format(monthOfYear + 1) + Init.this.format(dayOfMonth));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Init.this.displayMessage();
            }
        });
        this.endDatePicker.init(this.selectEndDate.getYear(), this.selectEndDate.getMonthOfYear(), this.selectEndDate.getDayOfMonth(), new DatePicker.OnDateChangedListener() {
            public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                try {
                    Init.this.selectEndDate = DateTimeFormat.forPattern("yyyyMMdd").parseDateTime(String.valueOf(year) + Init.this.format(monthOfYear) + Init.this.format(dayOfMonth));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Init.this.displayMessage();
            }
        });
        displayMessage();
    }
}
