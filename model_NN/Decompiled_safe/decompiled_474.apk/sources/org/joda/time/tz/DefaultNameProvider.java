package org.joda.time.tz;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import org.joda.time.DateTimeUtils;

public class DefaultNameProvider implements NameProvider {
    private HashMap<Locale, Map<String, Map<String, Object>>> iByLocaleCache = createCache();

    public String getShortName(Locale locale, String str, String str2) {
        String[] nameSet = getNameSet(locale, str, str2);
        if (nameSet == null) {
            return null;
        }
        return nameSet[0];
    }

    public String getName(Locale locale, String str, String str2) {
        String[] nameSet = getNameSet(locale, str, str2);
        if (nameSet == null) {
            return null;
        }
        return nameSet[1];
    }

    private synchronized String[] getNameSet(Locale locale, String str, String str2) {
        String[] strArr;
        Map map;
        int i = 0;
        synchronized (this) {
            if (locale == null || str == null || str2 == null) {
                strArr = null;
            } else {
                Map map2 = this.iByLocaleCache.get(locale);
                if (map2 == null) {
                    HashMap<Locale, Map<String, Map<String, Object>>> hashMap = this.iByLocaleCache;
                    map = createCache();
                    hashMap.put(locale, map);
                } else {
                    map = map2;
                }
                Map map3 = (Map) map.get(str);
                if (map3 == null) {
                    map3 = createCache();
                    map.put(str, map3);
                    String[][] zoneStrings = DateTimeUtils.getDateFormatSymbols(locale).getZoneStrings();
                    while (true) {
                        if (i >= zoneStrings.length) {
                            break;
                        }
                        String[] strArr2 = zoneStrings[i];
                        if (strArr2 == null || strArr2.length != 5 || !str.equals(strArr2[0])) {
                            i++;
                        } else {
                            map3.put(strArr2[2], new String[]{strArr2[2], strArr2[1]});
                            if (strArr2[2].equals(strArr2[4])) {
                                map3.put(strArr2[4] + "-Summer", new String[]{strArr2[4], strArr2[3]});
                            } else {
                                map3.put(strArr2[4], new String[]{strArr2[4], strArr2[3]});
                            }
                        }
                    }
                }
                strArr = (String[]) map3.get(str2);
            }
        }
        return strArr;
    }

    private HashMap createCache() {
        return new HashMap(7);
    }
}
