package org.joda.time.tz;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.SoftReference;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import org.joda.time.DateTimeZone;

public class ZoneInfoProvider implements Provider {
    private final File iFileDir;
    private final ClassLoader iLoader;
    private final String iResourcePath;
    private final Map<String, Object> iZoneInfoMap;

    public ZoneInfoProvider(File file) throws IOException {
        if (file == null) {
            throw new IllegalArgumentException("No file directory provided");
        } else if (!file.exists()) {
            throw new IOException("File directory doesn't exist: " + file);
        } else if (!file.isDirectory()) {
            throw new IOException("File doesn't refer to a directory: " + file);
        } else {
            this.iFileDir = file;
            this.iResourcePath = null;
            this.iLoader = null;
            this.iZoneInfoMap = loadZoneInfoMap(openResource("ZoneInfoMap"));
        }
    }

    public ZoneInfoProvider(String str) throws IOException {
        this(str, null, false);
    }

    public ZoneInfoProvider(String str, ClassLoader classLoader) throws IOException {
        this(str, classLoader, true);
    }

    private ZoneInfoProvider(String str, ClassLoader classLoader, boolean z) throws IOException {
        String str2;
        ClassLoader classLoader2;
        if (str == null) {
            throw new IllegalArgumentException("No resource path provided");
        }
        if (!str.endsWith("/")) {
            str2 = str + '/';
        } else {
            str2 = str;
        }
        this.iFileDir = null;
        this.iResourcePath = str2;
        if (classLoader != null || z) {
            classLoader2 = classLoader;
        } else {
            classLoader2 = getClass().getClassLoader();
        }
        this.iLoader = classLoader2;
        this.iZoneInfoMap = loadZoneInfoMap(openResource("ZoneInfoMap"));
    }

    public DateTimeZone getZone(String str) {
        if (str == null) {
            return null;
        }
        Object obj = this.iZoneInfoMap.get(str);
        if (obj == null) {
            return null;
        }
        if (str.equals(obj)) {
            return loadZoneData(str);
        }
        if (!(obj instanceof SoftReference)) {
            return getZone((String) obj);
        }
        DateTimeZone dateTimeZone = (DateTimeZone) ((SoftReference) obj).get();
        if (dateTimeZone == null) {
            return loadZoneData(str);
        }
        return dateTimeZone;
    }

    public Set<String> getAvailableIDs() {
        return new TreeSet(this.iZoneInfoMap.keySet());
    }

    /* access modifiers changed from: protected */
    public void uncaughtException(Exception exc) {
        Thread currentThread = Thread.currentThread();
        currentThread.getThreadGroup().uncaughtException(currentThread, exc);
    }

    private InputStream openResource(String str) throws IOException {
        InputStream systemResourceAsStream;
        if (this.iFileDir != null) {
            return new FileInputStream(new File(this.iFileDir, str));
        }
        String concat = this.iResourcePath.concat(str);
        if (this.iLoader != null) {
            systemResourceAsStream = this.iLoader.getResourceAsStream(concat);
        } else {
            systemResourceAsStream = ClassLoader.getSystemResourceAsStream(concat);
        }
        if (systemResourceAsStream != null) {
            return systemResourceAsStream;
        }
        throw new IOException(new StringBuffer(40).append("Resource not found: \"").append(concat).append("\" ClassLoader: ").append(this.iLoader != null ? this.iLoader.toString() : "system").toString());
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x002f A[SYNTHETIC, Splitter:B:20:0x002f] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private org.joda.time.DateTimeZone loadZoneData(java.lang.String r7) {
        /*
            r6 = this;
            r4 = 0
            java.io.InputStream r0 = r6.openResource(r7)     // Catch:{ IOException -> 0x001a, all -> 0x002b }
            org.joda.time.DateTimeZone r1 = org.joda.time.tz.DateTimeZoneBuilder.readFrom(r0, r7)     // Catch:{ IOException -> 0x0040, all -> 0x0039 }
            java.util.Map<java.lang.String, java.lang.Object> r2 = r6.iZoneInfoMap     // Catch:{ IOException -> 0x0040, all -> 0x0039 }
            java.lang.ref.SoftReference r3 = new java.lang.ref.SoftReference     // Catch:{ IOException -> 0x0040, all -> 0x0039 }
            r3.<init>(r1)     // Catch:{ IOException -> 0x0040, all -> 0x0039 }
            r2.put(r7, r3)     // Catch:{ IOException -> 0x0040, all -> 0x0039 }
            if (r0 == 0) goto L_0x0018
            r0.close()     // Catch:{ IOException -> 0x0033 }
        L_0x0018:
            r0 = r1
        L_0x0019:
            return r0
        L_0x001a:
            r0 = move-exception
            r1 = r4
        L_0x001c:
            r6.uncaughtException(r0)     // Catch:{ all -> 0x003e }
            java.util.Map<java.lang.String, java.lang.Object> r0 = r6.iZoneInfoMap     // Catch:{ all -> 0x003e }
            r0.remove(r7)     // Catch:{ all -> 0x003e }
            if (r1 == 0) goto L_0x0029
            r1.close()     // Catch:{ IOException -> 0x0035 }
        L_0x0029:
            r0 = r4
            goto L_0x0019
        L_0x002b:
            r0 = move-exception
            r1 = r4
        L_0x002d:
            if (r1 == 0) goto L_0x0032
            r1.close()     // Catch:{ IOException -> 0x0037 }
        L_0x0032:
            throw r0
        L_0x0033:
            r0 = move-exception
            goto L_0x0018
        L_0x0035:
            r0 = move-exception
            goto L_0x0029
        L_0x0037:
            r1 = move-exception
            goto L_0x0032
        L_0x0039:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x002d
        L_0x003e:
            r0 = move-exception
            goto L_0x002d
        L_0x0040:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x001c
        */
        throw new UnsupportedOperationException("Method not decompiled: org.joda.time.tz.ZoneInfoProvider.loadZoneData(java.lang.String):org.joda.time.DateTimeZone");
    }

    private static Map<String, Object> loadZoneInfoMap(InputStream inputStream) throws IOException {
        ConcurrentHashMap concurrentHashMap = new ConcurrentHashMap();
        DataInputStream dataInputStream = new DataInputStream(inputStream);
        try {
            readZoneInfoMap(dataInputStream, concurrentHashMap);
            concurrentHashMap.put("UTC", new SoftReference(DateTimeZone.UTC));
            return concurrentHashMap;
        } finally {
            try {
                dataInputStream.close();
            } catch (IOException e) {
            }
        }
    }

    private static void readZoneInfoMap(DataInputStream dataInputStream, Map<String, Object> map) throws IOException {
        int readUnsignedShort = dataInputStream.readUnsignedShort();
        String[] strArr = new String[readUnsignedShort];
        for (int i = 0; i < readUnsignedShort; i++) {
            strArr[i] = dataInputStream.readUTF().intern();
        }
        int readUnsignedShort2 = dataInputStream.readUnsignedShort();
        int i2 = 0;
        while (i2 < readUnsignedShort2) {
            try {
                map.put(strArr[dataInputStream.readUnsignedShort()], strArr[dataInputStream.readUnsignedShort()]);
                i2++;
            } catch (ArrayIndexOutOfBoundsException e) {
                throw new IOException("Corrupt zone info map");
            }
        }
    }
}
