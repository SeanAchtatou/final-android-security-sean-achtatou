package org.joda.time.tz;

import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import org.joda.time.Chronology;
import org.joda.time.DateTimeUtils;
import org.joda.time.DateTimeZone;
import org.joda.time.chrono.ISOChronology;

public class DateTimeZoneBuilder {
    private final ArrayList<RuleSet> iRuleSets = new ArrayList<>(10);

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.joda.time.tz.DateTimeZoneBuilder.readFrom(java.io.DataInput, java.lang.String):org.joda.time.DateTimeZone
     arg types: [java.io.DataInputStream, java.lang.String]
     candidates:
      org.joda.time.tz.DateTimeZoneBuilder.readFrom(java.io.InputStream, java.lang.String):org.joda.time.DateTimeZone
      org.joda.time.tz.DateTimeZoneBuilder.readFrom(java.io.DataInput, java.lang.String):org.joda.time.DateTimeZone */
    public static DateTimeZone readFrom(InputStream inputStream, String str) throws IOException {
        if (inputStream instanceof DataInput) {
            return readFrom((DataInput) inputStream, str);
        }
        return readFrom((DataInput) new DataInputStream(inputStream), str);
    }

    public static DateTimeZone readFrom(DataInput dataInput, String str) throws IOException {
        switch (dataInput.readUnsignedByte()) {
            case 67:
                return CachedDateTimeZone.forZone(PrecalculatedZone.readFrom(dataInput, str));
            case 70:
                FixedDateTimeZone fixedDateTimeZone = new FixedDateTimeZone(str, dataInput.readUTF(), (int) readMillis(dataInput), (int) readMillis(dataInput));
                if (fixedDateTimeZone.equals(DateTimeZone.UTC)) {
                    return DateTimeZone.UTC;
                }
                return fixedDateTimeZone;
            case 80:
                return PrecalculatedZone.readFrom(dataInput, str);
            default:
                throw new IOException("Invalid encoding");
        }
    }

    static void writeMillis(DataOutput dataOutput, long j) throws IOException {
        if (j % 1800000 == 0) {
            long j2 = j / 1800000;
            if (((j2 << 58) >> 58) == j2) {
                dataOutput.writeByte((int) (j2 & 63));
                return;
            }
        }
        if (j % 60000 == 0) {
            long j3 = j / 60000;
            if (((j3 << 34) >> 34) == j3) {
                dataOutput.writeInt(((int) (j3 & 1073741823)) | 1073741824);
                return;
            }
        }
        if (j % 1000 == 0) {
            long j4 = j / 1000;
            if (((j4 << 26) >> 26) == j4) {
                dataOutput.writeByte(((int) ((j4 >> 32) & 63)) | 128);
                dataOutput.writeInt((int) (j4 & -1));
                return;
            }
        }
        dataOutput.writeByte(j < 0 ? 255 : 192);
        dataOutput.writeLong(j);
    }

    static long readMillis(DataInput dataInput) throws IOException {
        int readUnsignedByte = dataInput.readUnsignedByte();
        switch (readUnsignedByte >> 6) {
            case 1:
                return ((long) (((readUnsignedByte << 26) >> 2) | (dataInput.readUnsignedByte() << 16) | (dataInput.readUnsignedByte() << 8) | dataInput.readUnsignedByte())) * 60000;
            case 2:
                return (((((long) readUnsignedByte) << 58) >> 26) | ((long) (dataInput.readUnsignedByte() << 24)) | ((long) (dataInput.readUnsignedByte() << 16)) | ((long) (dataInput.readUnsignedByte() << 8)) | ((long) dataInput.readUnsignedByte())) * 1000;
            case 3:
                return dataInput.readLong();
            default:
                return ((long) ((readUnsignedByte << 26) >> 26)) * 1800000;
        }
    }

    private static DateTimeZone buildFixedZone(String str, String str2, int i, int i2) {
        if (!"UTC".equals(str) || !str.equals(str2) || i != 0 || i2 != 0) {
            return new FixedDateTimeZone(str, str2, i, i2);
        }
        return DateTimeZone.UTC;
    }

    public DateTimeZoneBuilder addCutover(int i, char c, int i2, int i3, int i4, boolean z, int i5) {
        if (this.iRuleSets.size() > 0) {
            this.iRuleSets.get(this.iRuleSets.size() - 1).setUpperLimit(i, new OfYear(c, i2, i3, i4, z, i5));
        }
        this.iRuleSets.add(new RuleSet());
        return this;
    }

    public DateTimeZoneBuilder setStandardOffset(int i) {
        getLastRuleSet().setStandardOffset(i);
        return this;
    }

    public DateTimeZoneBuilder setFixedSavings(String str, int i) {
        getLastRuleSet().setFixedSavings(str, i);
        return this;
    }

    public DateTimeZoneBuilder addRecurringSavings(String str, int i, int i2, int i3, char c, int i4, int i5, int i6, boolean z, int i7) {
        if (i2 <= i3) {
            getLastRuleSet().addRule(new Rule(new Recurrence(new OfYear(c, i4, i5, i6, z, i7), str, i), i2, i3));
        }
        return this;
    }

    private RuleSet getLastRuleSet() {
        if (this.iRuleSets.size() == 0) {
            addCutover(Integer.MIN_VALUE, 'w', 1, 1, 0, false, 0);
        }
        return this.iRuleSets.get(this.iRuleSets.size() - 1);
    }

    public DateTimeZone toDateTimeZone(String str, boolean z) {
        if (str == null) {
            throw new IllegalArgumentException();
        }
        ArrayList arrayList = new ArrayList();
        int size = this.iRuleSets.size();
        long j = Long.MIN_VALUE;
        int i = 0;
        DSTZone dSTZone = null;
        while (i < size) {
            RuleSet ruleSet = this.iRuleSets.get(i);
            Transition firstTransition = ruleSet.firstTransition(j);
            if (firstTransition != null) {
                addTransition(arrayList, firstTransition);
                long millis = firstTransition.getMillis();
                int saveMillis = firstTransition.getSaveMillis();
                RuleSet ruleSet2 = new RuleSet(ruleSet);
                int i2 = saveMillis;
                while (true) {
                    Transition nextTransition = ruleSet2.nextTransition(millis, i2);
                    if (nextTransition == null || (addTransition(arrayList, nextTransition) && dSTZone != null)) {
                        j = ruleSet2.getUpperLimit(i2);
                    } else {
                        long millis2 = nextTransition.getMillis();
                        i2 = nextTransition.getSaveMillis();
                        if (dSTZone == null && i == size - 1) {
                            dSTZone = ruleSet2.buildTailZone(str);
                            millis = millis2;
                        } else {
                            millis = millis2;
                        }
                    }
                }
                j = ruleSet2.getUpperLimit(i2);
            }
            i++;
            dSTZone = dSTZone;
        }
        if (arrayList.size() == 0) {
            if (dSTZone != null) {
                return dSTZone;
            }
            return buildFixedZone(str, "UTC", 0, 0);
        } else if (arrayList.size() == 1 && dSTZone == null) {
            Transition transition = (Transition) arrayList.get(0);
            return buildFixedZone(str, transition.getNameKey(), transition.getWallOffset(), transition.getStandardOffset());
        } else {
            PrecalculatedZone create = PrecalculatedZone.create(str, z, arrayList, dSTZone);
            if (create.isCachable()) {
                return CachedDateTimeZone.forZone(create);
            }
            return create;
        }
    }

    private boolean addTransition(ArrayList<Transition> arrayList, Transition transition) {
        int i;
        int size = arrayList.size();
        if (size == 0) {
            arrayList.add(transition);
            return true;
        }
        Transition transition2 = arrayList.get(size - 1);
        if (!transition.isTransitionFrom(transition2)) {
            return false;
        }
        if (size >= 2) {
            i = arrayList.get(size - 2).getWallOffset();
        } else {
            i = 0;
        }
        if (transition.getMillis() + ((long) transition2.getWallOffset()) != ((long) i) + transition2.getMillis()) {
            arrayList.add(transition);
            return true;
        }
        arrayList.remove(size - 1);
        return addTransition(arrayList, transition);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.joda.time.tz.DateTimeZoneBuilder.writeTo(java.lang.String, java.io.DataOutput):void
     arg types: [java.lang.String, java.io.DataOutputStream]
     candidates:
      org.joda.time.tz.DateTimeZoneBuilder.writeTo(java.lang.String, java.io.OutputStream):void
      org.joda.time.tz.DateTimeZoneBuilder.writeTo(java.lang.String, java.io.DataOutput):void */
    public void writeTo(String str, OutputStream outputStream) throws IOException {
        if (outputStream instanceof DataOutput) {
            writeTo(str, (DataOutput) outputStream);
        } else {
            writeTo(str, (DataOutput) new DataOutputStream(outputStream));
        }
    }

    public void writeTo(String str, DataOutput dataOutput) throws IOException {
        DateTimeZone dateTimeZone = toDateTimeZone(str, false);
        if (dateTimeZone instanceof FixedDateTimeZone) {
            dataOutput.writeByte(70);
            dataOutput.writeUTF(dateTimeZone.getNameKey(0));
            writeMillis(dataOutput, (long) dateTimeZone.getOffset(0));
            writeMillis(dataOutput, (long) dateTimeZone.getStandardOffset(0));
            return;
        }
        if (dateTimeZone instanceof CachedDateTimeZone) {
            dataOutput.writeByte(67);
            dateTimeZone = ((CachedDateTimeZone) dateTimeZone).getUncachedZone();
        } else {
            dataOutput.writeByte(80);
        }
        ((PrecalculatedZone) dateTimeZone).writeTo(dataOutput);
    }

    private static final class OfYear {
        final boolean iAdvance;
        final int iDayOfMonth;
        final int iDayOfWeek;
        final int iMillisOfDay;
        final char iMode;
        final int iMonthOfYear;

        static OfYear readFrom(DataInput dataInput) throws IOException {
            return new OfYear((char) dataInput.readUnsignedByte(), dataInput.readUnsignedByte(), dataInput.readByte(), dataInput.readUnsignedByte(), dataInput.readBoolean(), (int) DateTimeZoneBuilder.readMillis(dataInput));
        }

        OfYear(char c, int i, int i2, int i3, boolean z, int i4) {
            if (c == 'u' || c == 'w' || c == 's') {
                this.iMode = c;
                this.iMonthOfYear = i;
                this.iDayOfMonth = i2;
                this.iDayOfWeek = i3;
                this.iAdvance = z;
                this.iMillisOfDay = i4;
                return;
            }
            throw new IllegalArgumentException("Unknown mode: " + c);
        }

        public long setInstant(int i, int i2, int i3) {
            int i4;
            long j;
            if (this.iMode == 'w') {
                i4 = i2 + i3;
            } else if (this.iMode == 's') {
                i4 = i2;
            } else {
                i4 = 0;
            }
            ISOChronology instanceUTC = ISOChronology.getInstanceUTC();
            long dayOfMonth = setDayOfMonth(instanceUTC, instanceUTC.millisOfDay().set(instanceUTC.monthOfYear().set(instanceUTC.year().set(0, i), this.iMonthOfYear), this.iMillisOfDay));
            if (this.iDayOfWeek != 0) {
                j = setDayOfWeek(instanceUTC, dayOfMonth);
            } else {
                j = dayOfMonth;
            }
            return j - ((long) i4);
        }

        public long next(long j, int i, int i2) {
            int i3;
            long j2;
            if (this.iMode == 'w') {
                i3 = i + i2;
            } else if (this.iMode == 's') {
                i3 = i;
            } else {
                i3 = 0;
            }
            long j3 = ((long) i3) + j;
            ISOChronology instanceUTC = ISOChronology.getInstanceUTC();
            long dayOfMonthNext = setDayOfMonthNext(instanceUTC, instanceUTC.millisOfDay().add(instanceUTC.millisOfDay().set(instanceUTC.monthOfYear().set(j3, this.iMonthOfYear), 0), this.iMillisOfDay));
            if (this.iDayOfWeek == 0) {
                if (dayOfMonthNext <= j3) {
                    j2 = setDayOfMonthNext(instanceUTC, instanceUTC.year().add(dayOfMonthNext, 1));
                }
                j2 = dayOfMonthNext;
            } else {
                dayOfMonthNext = setDayOfWeek(instanceUTC, dayOfMonthNext);
                if (dayOfMonthNext <= j3) {
                    j2 = setDayOfWeek(instanceUTC, setDayOfMonthNext(instanceUTC, instanceUTC.monthOfYear().set(instanceUTC.year().add(dayOfMonthNext, 1), this.iMonthOfYear)));
                }
                j2 = dayOfMonthNext;
            }
            return j2 - ((long) i3);
        }

        public long previous(long j, int i, int i2) {
            int i3;
            long j2;
            if (this.iMode == 'w') {
                i3 = i + i2;
            } else if (this.iMode == 's') {
                i3 = i;
            } else {
                i3 = 0;
            }
            long j3 = ((long) i3) + j;
            ISOChronology instanceUTC = ISOChronology.getInstanceUTC();
            long dayOfMonthPrevious = setDayOfMonthPrevious(instanceUTC, instanceUTC.millisOfDay().add(instanceUTC.millisOfDay().set(instanceUTC.monthOfYear().set(j3, this.iMonthOfYear), 0), this.iMillisOfDay));
            if (this.iDayOfWeek == 0) {
                if (dayOfMonthPrevious >= j3) {
                    j2 = setDayOfMonthPrevious(instanceUTC, instanceUTC.year().add(dayOfMonthPrevious, -1));
                }
                j2 = dayOfMonthPrevious;
            } else {
                dayOfMonthPrevious = setDayOfWeek(instanceUTC, dayOfMonthPrevious);
                if (dayOfMonthPrevious >= j3) {
                    j2 = setDayOfWeek(instanceUTC, setDayOfMonthPrevious(instanceUTC, instanceUTC.monthOfYear().set(instanceUTC.year().add(dayOfMonthPrevious, -1), this.iMonthOfYear)));
                }
                j2 = dayOfMonthPrevious;
            }
            return j2 - ((long) i3);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof OfYear)) {
                return false;
            }
            OfYear ofYear = (OfYear) obj;
            return this.iMode == ofYear.iMode && this.iMonthOfYear == ofYear.iMonthOfYear && this.iDayOfMonth == ofYear.iDayOfMonth && this.iDayOfWeek == ofYear.iDayOfWeek && this.iAdvance == ofYear.iAdvance && this.iMillisOfDay == ofYear.iMillisOfDay;
        }

        public void writeTo(DataOutput dataOutput) throws IOException {
            dataOutput.writeByte(this.iMode);
            dataOutput.writeByte(this.iMonthOfYear);
            dataOutput.writeByte(this.iDayOfMonth);
            dataOutput.writeByte(this.iDayOfWeek);
            dataOutput.writeBoolean(this.iAdvance);
            DateTimeZoneBuilder.writeMillis(dataOutput, (long) this.iMillisOfDay);
        }

        private long setDayOfMonthNext(Chronology chronology, long j) {
            try {
                return setDayOfMonth(chronology, j);
            } catch (IllegalArgumentException e) {
                if (this.iMonthOfYear == 2 && this.iDayOfMonth == 29) {
                    long j2 = j;
                    while (!chronology.year().isLeap(j2)) {
                        j2 = chronology.year().add(j2, 1);
                    }
                    return setDayOfMonth(chronology, j2);
                }
                throw e;
            }
        }

        private long setDayOfMonthPrevious(Chronology chronology, long j) {
            try {
                return setDayOfMonth(chronology, j);
            } catch (IllegalArgumentException e) {
                if (this.iMonthOfYear == 2 && this.iDayOfMonth == 29) {
                    long j2 = j;
                    while (!chronology.year().isLeap(j2)) {
                        j2 = chronology.year().add(j2, -1);
                    }
                    return setDayOfMonth(chronology, j2);
                }
                throw e;
            }
        }

        private long setDayOfMonth(Chronology chronology, long j) {
            if (this.iDayOfMonth >= 0) {
                return chronology.dayOfMonth().set(j, this.iDayOfMonth);
            }
            return chronology.dayOfMonth().add(chronology.monthOfYear().add(chronology.dayOfMonth().set(j, 1), 1), this.iDayOfMonth);
        }

        private long setDayOfWeek(Chronology chronology, long j) {
            int i = this.iDayOfWeek - chronology.dayOfWeek().get(j);
            if (i == 0) {
                return j;
            }
            if (this.iAdvance) {
                if (i < 0) {
                    i += 7;
                }
            } else if (i > 0) {
                i -= 7;
            }
            return chronology.dayOfWeek().add(j, i);
        }
    }

    private static final class Recurrence {
        final String iNameKey;
        final OfYear iOfYear;
        final int iSaveMillis;

        static Recurrence readFrom(DataInput dataInput) throws IOException {
            return new Recurrence(OfYear.readFrom(dataInput), dataInput.readUTF(), (int) DateTimeZoneBuilder.readMillis(dataInput));
        }

        Recurrence(OfYear ofYear, String str, int i) {
            this.iOfYear = ofYear;
            this.iNameKey = str;
            this.iSaveMillis = i;
        }

        public OfYear getOfYear() {
            return this.iOfYear;
        }

        public long next(long j, int i, int i2) {
            return this.iOfYear.next(j, i, i2);
        }

        public long previous(long j, int i, int i2) {
            return this.iOfYear.previous(j, i, i2);
        }

        public String getNameKey() {
            return this.iNameKey;
        }

        public int getSaveMillis() {
            return this.iSaveMillis;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Recurrence)) {
                return false;
            }
            Recurrence recurrence = (Recurrence) obj;
            return this.iSaveMillis == recurrence.iSaveMillis && this.iNameKey.equals(recurrence.iNameKey) && this.iOfYear.equals(recurrence.iOfYear);
        }

        public void writeTo(DataOutput dataOutput) throws IOException {
            this.iOfYear.writeTo(dataOutput);
            dataOutput.writeUTF(this.iNameKey);
            DateTimeZoneBuilder.writeMillis(dataOutput, (long) this.iSaveMillis);
        }

        /* access modifiers changed from: package-private */
        public Recurrence rename(String str) {
            return new Recurrence(this.iOfYear, str, this.iSaveMillis);
        }

        /* access modifiers changed from: package-private */
        public Recurrence renameAppend(String str) {
            return rename((this.iNameKey + str).intern());
        }
    }

    private static final class Rule {
        final int iFromYear;
        final Recurrence iRecurrence;
        final int iToYear;

        Rule(Recurrence recurrence, int i, int i2) {
            this.iRecurrence = recurrence;
            this.iFromYear = i;
            this.iToYear = i2;
        }

        public int getFromYear() {
            return this.iFromYear;
        }

        public int getToYear() {
            return this.iToYear;
        }

        public OfYear getOfYear() {
            return this.iRecurrence.getOfYear();
        }

        public String getNameKey() {
            return this.iRecurrence.getNameKey();
        }

        public int getSaveMillis() {
            return this.iRecurrence.getSaveMillis();
        }

        public long next(long j, int i, int i2) {
            int i3;
            long j2;
            ISOChronology instanceUTC = ISOChronology.getInstanceUTC();
            int i4 = i + i2;
            if (j == Long.MIN_VALUE) {
                i3 = Integer.MIN_VALUE;
            } else {
                i3 = instanceUTC.year().get(((long) i4) + j);
            }
            if (i3 < this.iFromYear) {
                j2 = (instanceUTC.year().set(0, this.iFromYear) - ((long) i4)) - 1;
            } else {
                j2 = j;
            }
            long next = this.iRecurrence.next(j2, i, i2);
            if (next <= j || instanceUTC.year().get(((long) i4) + next) <= this.iToYear) {
                return next;
            }
            return j;
        }
    }

    private static final class Transition {
        private final long iMillis;
        private final String iNameKey;
        private final int iStandardOffset;
        private final int iWallOffset;

        Transition(long j, Transition transition) {
            this.iMillis = j;
            this.iNameKey = transition.iNameKey;
            this.iWallOffset = transition.iWallOffset;
            this.iStandardOffset = transition.iStandardOffset;
        }

        Transition(long j, Rule rule, int i) {
            this.iMillis = j;
            this.iNameKey = rule.getNameKey();
            this.iWallOffset = rule.getSaveMillis() + i;
            this.iStandardOffset = i;
        }

        Transition(long j, String str, int i, int i2) {
            this.iMillis = j;
            this.iNameKey = str;
            this.iWallOffset = i;
            this.iStandardOffset = i2;
        }

        public long getMillis() {
            return this.iMillis;
        }

        public String getNameKey() {
            return this.iNameKey;
        }

        public int getWallOffset() {
            return this.iWallOffset;
        }

        public int getStandardOffset() {
            return this.iStandardOffset;
        }

        public int getSaveMillis() {
            return this.iWallOffset - this.iStandardOffset;
        }

        public boolean isTransitionFrom(Transition transition) {
            if (transition == null) {
                return true;
            }
            return this.iMillis > transition.iMillis && (this.iWallOffset != transition.iWallOffset || !this.iNameKey.equals(transition.iNameKey));
        }
    }

    private static final class RuleSet {
        private static final int YEAR_LIMIT = (ISOChronology.getInstanceUTC().year().get(DateTimeUtils.currentTimeMillis()) + 100);
        private String iInitialNameKey;
        private int iInitialSaveMillis;
        private ArrayList<Rule> iRules;
        private int iStandardOffset;
        private OfYear iUpperOfYear;
        private int iUpperYear;

        RuleSet() {
            this.iRules = new ArrayList<>(10);
            this.iUpperYear = Integer.MAX_VALUE;
        }

        RuleSet(RuleSet ruleSet) {
            this.iStandardOffset = ruleSet.iStandardOffset;
            this.iRules = new ArrayList<>(ruleSet.iRules);
            this.iInitialNameKey = ruleSet.iInitialNameKey;
            this.iInitialSaveMillis = ruleSet.iInitialSaveMillis;
            this.iUpperYear = ruleSet.iUpperYear;
            this.iUpperOfYear = ruleSet.iUpperOfYear;
        }

        public int getStandardOffset() {
            return this.iStandardOffset;
        }

        public void setStandardOffset(int i) {
            this.iStandardOffset = i;
        }

        public void setFixedSavings(String str, int i) {
            this.iInitialNameKey = str;
            this.iInitialSaveMillis = i;
        }

        public void addRule(Rule rule) {
            if (!this.iRules.contains(rule)) {
                this.iRules.add(rule);
            }
        }

        public void setUpperLimit(int i, OfYear ofYear) {
            this.iUpperYear = i;
            this.iUpperOfYear = ofYear;
        }

        public Transition firstTransition(long j) {
            Transition transition;
            if (this.iInitialNameKey != null) {
                return new Transition(j, this.iInitialNameKey, this.iStandardOffset + this.iInitialSaveMillis, this.iStandardOffset);
            }
            ArrayList<Rule> arrayList = new ArrayList<>(this.iRules);
            long j2 = Long.MIN_VALUE;
            Transition transition2 = null;
            int i = 0;
            while (true) {
                Transition nextTransition = nextTransition(j2, i);
                if (nextTransition == null) {
                    transition = transition2;
                    break;
                }
                long millis = nextTransition.getMillis();
                if (millis == j) {
                    transition = new Transition(j, nextTransition);
                    break;
                } else if (millis > j) {
                    if (transition2 == null) {
                        Iterator<Rule> it = arrayList.iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                break;
                            }
                            Rule next = it.next();
                            if (next.getSaveMillis() == 0) {
                                transition = new Transition(j, next, this.iStandardOffset);
                                break;
                            }
                        }
                    }
                    transition = transition2;
                    if (transition == null) {
                        transition = new Transition(j, nextTransition.getNameKey(), this.iStandardOffset, this.iStandardOffset);
                    }
                } else {
                    Transition transition3 = new Transition(j, nextTransition);
                    i = nextTransition.getSaveMillis();
                    transition2 = transition3;
                    j2 = millis;
                }
            }
            this.iRules = arrayList;
            return transition;
        }

        public Transition nextTransition(long j, int i) {
            ISOChronology instanceUTC = ISOChronology.getInstanceUTC();
            long j2 = Long.MAX_VALUE;
            Iterator<Rule> it = this.iRules.iterator();
            Rule rule = null;
            while (it.hasNext()) {
                Rule next = it.next();
                long next2 = next.next(j, this.iStandardOffset, i);
                if (next2 <= j) {
                    it.remove();
                } else {
                    if (next2 <= j2) {
                        j2 = next2;
                    } else {
                        next = rule;
                    }
                    rule = next;
                }
            }
            if (rule == null) {
                return null;
            }
            if (instanceUTC.year().get(j2) >= YEAR_LIMIT) {
                return null;
            }
            if (this.iUpperYear >= Integer.MAX_VALUE || j2 < this.iUpperOfYear.setInstant(this.iUpperYear, this.iStandardOffset, i)) {
                return new Transition(j2, rule, this.iStandardOffset);
            }
            return null;
        }

        public long getUpperLimit(int i) {
            if (this.iUpperYear == Integer.MAX_VALUE) {
                return Long.MAX_VALUE;
            }
            return this.iUpperOfYear.setInstant(this.iUpperYear, this.iStandardOffset, i);
        }

        public DSTZone buildTailZone(String str) {
            if (this.iRules.size() == 2) {
                Rule rule = this.iRules.get(0);
                Rule rule2 = this.iRules.get(1);
                if (rule.getToYear() == Integer.MAX_VALUE && rule2.getToYear() == Integer.MAX_VALUE) {
                    return new DSTZone(str, this.iStandardOffset, rule.iRecurrence, rule2.iRecurrence);
                }
            }
            return null;
        }
    }

    private static final class DSTZone extends DateTimeZone {
        private static final long serialVersionUID = 6941492635554961361L;
        final Recurrence iEndRecurrence;
        final int iStandardOffset;
        final Recurrence iStartRecurrence;

        static DSTZone readFrom(DataInput dataInput, String str) throws IOException {
            return new DSTZone(str, (int) DateTimeZoneBuilder.readMillis(dataInput), Recurrence.readFrom(dataInput), Recurrence.readFrom(dataInput));
        }

        DSTZone(String str, int i, Recurrence recurrence, Recurrence recurrence2) {
            super(str);
            this.iStandardOffset = i;
            this.iStartRecurrence = recurrence;
            this.iEndRecurrence = recurrence2;
        }

        public String getNameKey(long j) {
            return findMatchingRecurrence(j).getNameKey();
        }

        public int getOffset(long j) {
            return this.iStandardOffset + findMatchingRecurrence(j).getSaveMillis();
        }

        public int getStandardOffset(long j) {
            return this.iStandardOffset;
        }

        public boolean isFixed() {
            return false;
        }

        public long nextTransition(long j) {
            long j2;
            long j3;
            int i = this.iStandardOffset;
            Recurrence recurrence = this.iStartRecurrence;
            Recurrence recurrence2 = this.iEndRecurrence;
            try {
                j2 = recurrence.next(j, i, recurrence2.getSaveMillis());
                if (j > 0 && j2 < 0) {
                    j2 = j;
                }
            } catch (IllegalArgumentException e) {
                j2 = j;
            } catch (ArithmeticException e2) {
                j2 = j;
            }
            try {
                j3 = recurrence2.next(j, i, recurrence.getSaveMillis());
                if (j > 0 && j3 < 0) {
                    j3 = j;
                }
            } catch (IllegalArgumentException e3) {
                j3 = j;
            } catch (ArithmeticException e4) {
                j3 = j;
            }
            if (j2 > j3) {
                return j3;
            }
            return j2;
        }

        public long previousTransition(long j) {
            long j2;
            long j3 = j + 1;
            int i = this.iStandardOffset;
            Recurrence recurrence = this.iStartRecurrence;
            Recurrence recurrence2 = this.iEndRecurrence;
            try {
                j2 = recurrence.previous(j3, i, recurrence2.getSaveMillis());
                if (j3 < 0 && j2 > 0) {
                    j2 = j3;
                }
            } catch (IllegalArgumentException e) {
                j2 = j3;
            } catch (ArithmeticException e2) {
                j2 = j3;
            }
            try {
                long previous = recurrence2.previous(j3, i, recurrence.getSaveMillis());
                if (j3 >= 0 || previous <= 0) {
                    j3 = previous;
                }
            } catch (ArithmeticException | IllegalArgumentException e3) {
            }
            if (j2 > j3) {
                j3 = j2;
            }
            return j3 - 1;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof DSTZone)) {
                return false;
            }
            DSTZone dSTZone = (DSTZone) obj;
            return getID().equals(dSTZone.getID()) && this.iStandardOffset == dSTZone.iStandardOffset && this.iStartRecurrence.equals(dSTZone.iStartRecurrence) && this.iEndRecurrence.equals(dSTZone.iEndRecurrence);
        }

        public void writeTo(DataOutput dataOutput) throws IOException {
            DateTimeZoneBuilder.writeMillis(dataOutput, (long) this.iStandardOffset);
            this.iStartRecurrence.writeTo(dataOutput);
            this.iEndRecurrence.writeTo(dataOutput);
        }

        private Recurrence findMatchingRecurrence(long j) {
            long j2;
            long j3;
            int i = this.iStandardOffset;
            Recurrence recurrence = this.iStartRecurrence;
            Recurrence recurrence2 = this.iEndRecurrence;
            try {
                j2 = recurrence.next(j, i, recurrence2.getSaveMillis());
            } catch (IllegalArgumentException e) {
                j2 = j;
            } catch (ArithmeticException e2) {
                j2 = j;
            }
            try {
                j3 = recurrence2.next(j, i, recurrence.getSaveMillis());
            } catch (IllegalArgumentException e3) {
                j3 = j;
            } catch (ArithmeticException e4) {
                j3 = j;
            }
            if (j2 > j3) {
                return recurrence;
            }
            return recurrence2;
        }
    }

    private static final class PrecalculatedZone extends DateTimeZone {
        private static final long serialVersionUID = 7811976468055766265L;
        private final String[] iNameKeys;
        private final int[] iStandardOffsets;
        private final DSTZone iTailZone;
        private final long[] iTransitions;
        private final int[] iWallOffsets;

        static PrecalculatedZone readFrom(DataInput dataInput, String str) throws IOException {
            DSTZone dSTZone;
            int readUnsignedShort;
            int readUnsignedShort2 = dataInput.readUnsignedShort();
            String[] strArr = new String[readUnsignedShort2];
            for (int i = 0; i < readUnsignedShort2; i++) {
                strArr[i] = dataInput.readUTF();
            }
            int readInt = dataInput.readInt();
            long[] jArr = new long[readInt];
            int[] iArr = new int[readInt];
            int[] iArr2 = new int[readInt];
            String[] strArr2 = new String[readInt];
            for (int i2 = 0; i2 < readInt; i2++) {
                jArr[i2] = DateTimeZoneBuilder.readMillis(dataInput);
                iArr[i2] = (int) DateTimeZoneBuilder.readMillis(dataInput);
                iArr2[i2] = (int) DateTimeZoneBuilder.readMillis(dataInput);
                if (readUnsignedShort2 < 256) {
                    try {
                        readUnsignedShort = dataInput.readUnsignedByte();
                    } catch (ArrayIndexOutOfBoundsException e) {
                        throw new IOException("Invalid encoding");
                    }
                } else {
                    readUnsignedShort = dataInput.readUnsignedShort();
                }
                strArr2[i2] = strArr[readUnsignedShort];
            }
            if (dataInput.readBoolean()) {
                dSTZone = DSTZone.readFrom(dataInput, str);
            } else {
                dSTZone = null;
            }
            return new PrecalculatedZone(str, jArr, iArr, iArr2, strArr2, dSTZone);
        }

        /* JADX WARNING: Removed duplicated region for block: B:61:0x021b  */
        /* JADX WARNING: Removed duplicated region for block: B:65:0x0251  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        static org.joda.time.tz.DateTimeZoneBuilder.PrecalculatedZone create(java.lang.String r27, boolean r28, java.util.ArrayList<org.joda.time.tz.DateTimeZoneBuilder.Transition> r29, org.joda.time.tz.DateTimeZoneBuilder.DSTZone r30) {
            /*
                int r6 = r29.size()
                if (r6 != 0) goto L_0x000c
                java.lang.IllegalArgumentException r27 = new java.lang.IllegalArgumentException
                r27.<init>()
                throw r27
            L_0x000c:
                long[] r12 = new long[r6]
                int[] r13 = new int[r6]
                int[] r14 = new int[r6]
                java.lang.String[] r15 = new java.lang.String[r6]
                r5 = 0
                r7 = 0
                r8 = r5
            L_0x0017:
                if (r7 >= r6) goto L_0x004e
                r0 = r29
                r1 = r7
                java.lang.Object r5 = r0.get(r1)
                org.joda.time.tz.DateTimeZoneBuilder$Transition r5 = (org.joda.time.tz.DateTimeZoneBuilder.Transition) r5
                boolean r8 = r5.isTransitionFrom(r8)
                if (r8 != 0) goto L_0x0032
                java.lang.IllegalArgumentException r28 = new java.lang.IllegalArgumentException
                r0 = r28
                r1 = r27
                r0.<init>(r1)
                throw r28
            L_0x0032:
                long r8 = r5.getMillis()
                r12[r7] = r8
                int r8 = r5.getWallOffset()
                r13[r7] = r8
                int r8 = r5.getStandardOffset()
                r14[r7] = r8
                java.lang.String r8 = r5.getNameKey()
                r15[r7] = r8
                int r7 = r7 + 1
                r8 = r5
                goto L_0x0017
            L_0x004e:
                r29 = 5
                r0 = r29
                java.lang.String[] r0 = new java.lang.String[r0]
                r29 = r0
                java.text.DateFormatSymbols r5 = new java.text.DateFormatSymbols
                java.util.Locale r6 = java.util.Locale.ENGLISH
                r5.<init>(r6)
                java.lang.String[][] r5 = r5.getZoneStrings()
                r6 = 0
                r16 = r29
                r29 = r6
            L_0x0066:
                int r6 = r5.length
                r0 = r29
                r1 = r6
                if (r0 >= r1) goto L_0x0085
                r6 = r5[r29]
                if (r6 == 0) goto L_0x0259
                int r7 = r6.length
                r8 = 5
                if (r7 != r8) goto L_0x0259
                r7 = 0
                r7 = r6[r7]
                r0 = r27
                r1 = r7
                boolean r7 = r0.equals(r1)
                if (r7 == 0) goto L_0x0259
            L_0x0080:
                int r29 = r29 + 1
                r16 = r6
                goto L_0x0066
            L_0x0085:
                org.joda.time.chrono.ISOChronology r11 = org.joda.time.chrono.ISOChronology.getInstanceUTC()
                r29 = 0
            L_0x008b:
                int r5 = r15.length
                r6 = 1
                int r5 = r5 - r6
                r0 = r29
                r1 = r5
                if (r0 >= r1) goto L_0x019b
                r17 = r15[r29]
                int r5 = r29 + 1
                r18 = r15[r5]
                r5 = r13[r29]
                r0 = r5
                long r0 = (long) r0
                r19 = r0
                int r5 = r29 + 1
                r5 = r13[r5]
                r0 = r5
                long r0 = (long) r0
                r21 = r0
                r5 = r14[r29]
                r0 = r5
                long r0 = (long) r0
                r23 = r0
                int r5 = r29 + 1
                r5 = r14[r5]
                r0 = r5
                long r0 = (long) r0
                r25 = r0
                org.joda.time.Period r5 = new org.joda.time.Period
                r6 = r12[r29]
                int r8 = r29 + 1
                r8 = r12[r8]
                org.joda.time.PeriodType r10 = org.joda.time.PeriodType.yearMonthDay()
                r5.<init>(r6, r8, r10, r11)
                int r6 = (r19 > r21 ? 1 : (r19 == r21 ? 0 : -1))
                if (r6 == 0) goto L_0x0172
                int r6 = (r23 > r25 ? 1 : (r23 == r25 ? 0 : -1))
                if (r6 != 0) goto L_0x0172
                boolean r6 = r17.equals(r18)
                if (r6 == 0) goto L_0x0172
                int r6 = r5.getYears()
                if (r6 != 0) goto L_0x0172
                int r6 = r5.getMonths()
                r7 = 4
                if (r6 <= r7) goto L_0x0172
                int r5 = r5.getMonths()
                r6 = 8
                if (r5 >= r6) goto L_0x0172
                r5 = 2
                r5 = r16[r5]
                r0 = r17
                r1 = r5
                boolean r5 = r0.equals(r1)
                if (r5 == 0) goto L_0x0172
                r5 = 4
                r5 = r16[r5]
                r0 = r17
                r1 = r5
                boolean r5 = r0.equals(r1)
                if (r5 == 0) goto L_0x0172
                boolean r5 = org.joda.time.tz.ZoneInfoCompiler.verbose()
                if (r5 == 0) goto L_0x0152
                java.io.PrintStream r5 = java.lang.System.out
                java.lang.StringBuilder r6 = new java.lang.StringBuilder
                r6.<init>()
                java.lang.String r7 = "Fixing duplicate name key - "
                java.lang.StringBuilder r6 = r6.append(r7)
                r0 = r6
                r1 = r18
                java.lang.StringBuilder r6 = r0.append(r1)
                java.lang.String r6 = r6.toString()
                r5.println(r6)
                java.io.PrintStream r5 = java.lang.System.out
                java.lang.StringBuilder r6 = new java.lang.StringBuilder
                r6.<init>()
                java.lang.String r7 = "     - "
                java.lang.StringBuilder r6 = r6.append(r7)
                org.joda.time.DateTime r7 = new org.joda.time.DateTime
                r8 = r12[r29]
                r7.<init>(r8, r11)
                java.lang.StringBuilder r6 = r6.append(r7)
                java.lang.String r7 = " - "
                java.lang.StringBuilder r6 = r6.append(r7)
                org.joda.time.DateTime r7 = new org.joda.time.DateTime
                int r8 = r29 + 1
                r8 = r12[r8]
                r7.<init>(r8, r11)
                java.lang.StringBuilder r6 = r6.append(r7)
                java.lang.String r6 = r6.toString()
                r5.println(r6)
            L_0x0152:
                int r5 = (r19 > r21 ? 1 : (r19 == r21 ? 0 : -1))
                if (r5 <= 0) goto L_0x0176
                java.lang.StringBuilder r5 = new java.lang.StringBuilder
                r5.<init>()
                r0 = r5
                r1 = r17
                java.lang.StringBuilder r5 = r0.append(r1)
                java.lang.String r6 = "-Summer"
                java.lang.StringBuilder r5 = r5.append(r6)
                java.lang.String r5 = r5.toString()
                java.lang.String r5 = r5.intern()
                r15[r29] = r5
            L_0x0172:
                int r29 = r29 + 1
                goto L_0x008b
            L_0x0176:
                int r5 = (r19 > r21 ? 1 : (r19 == r21 ? 0 : -1))
                if (r5 >= 0) goto L_0x0172
                int r5 = r29 + 1
                java.lang.StringBuilder r6 = new java.lang.StringBuilder
                r6.<init>()
                r0 = r6
                r1 = r18
                java.lang.StringBuilder r6 = r0.append(r1)
                java.lang.String r7 = "-Summer"
                java.lang.StringBuilder r6 = r6.append(r7)
                java.lang.String r6 = r6.toString()
                java.lang.String r6 = r6.intern()
                r15[r5] = r6
                int r29 = r29 + 1
                goto L_0x0172
            L_0x019b:
                if (r30 == 0) goto L_0x0256
                r0 = r30
                org.joda.time.tz.DateTimeZoneBuilder$Recurrence r0 = r0.iStartRecurrence
                r29 = r0
                java.lang.String r29 = r29.getNameKey()
                r0 = r30
                org.joda.time.tz.DateTimeZoneBuilder$Recurrence r0 = r0.iEndRecurrence
                r5 = r0
                java.lang.String r5 = r5.getNameKey()
                r0 = r29
                r1 = r5
                boolean r29 = r0.equals(r1)
                if (r29 == 0) goto L_0x0256
                boolean r29 = org.joda.time.tz.ZoneInfoCompiler.verbose()
                if (r29 == 0) goto L_0x01e3
                java.io.PrintStream r29 = java.lang.System.out
                java.lang.StringBuilder r5 = new java.lang.StringBuilder
                r5.<init>()
                java.lang.String r6 = "Fixing duplicate recurrent name key - "
                java.lang.StringBuilder r5 = r5.append(r6)
                r0 = r30
                org.joda.time.tz.DateTimeZoneBuilder$Recurrence r0 = r0.iStartRecurrence
                r6 = r0
                java.lang.String r6 = r6.getNameKey()
                java.lang.StringBuilder r5 = r5.append(r6)
                java.lang.String r5 = r5.toString()
                r0 = r29
                r1 = r5
                r0.println(r1)
            L_0x01e3:
                r0 = r30
                org.joda.time.tz.DateTimeZoneBuilder$Recurrence r0 = r0.iStartRecurrence
                r29 = r0
                int r29 = r29.getSaveMillis()
                if (r29 <= 0) goto L_0x0225
                org.joda.time.tz.DateTimeZoneBuilder$DSTZone r29 = new org.joda.time.tz.DateTimeZoneBuilder$DSTZone
                java.lang.String r5 = r30.getID()
                r0 = r30
                int r0 = r0.iStandardOffset
                r6 = r0
                r0 = r30
                org.joda.time.tz.DateTimeZoneBuilder$Recurrence r0 = r0.iStartRecurrence
                r7 = r0
                java.lang.String r8 = "-Summer"
                org.joda.time.tz.DateTimeZoneBuilder$Recurrence r7 = r7.renameAppend(r8)
                r0 = r30
                org.joda.time.tz.DateTimeZoneBuilder$Recurrence r0 = r0.iEndRecurrence
                r30 = r0
                r0 = r29
                r1 = r5
                r2 = r6
                r3 = r7
                r4 = r30
                r0.<init>(r1, r2, r3, r4)
                r11 = r29
            L_0x0217:
                org.joda.time.tz.DateTimeZoneBuilder$PrecalculatedZone r5 = new org.joda.time.tz.DateTimeZoneBuilder$PrecalculatedZone
                if (r28 == 0) goto L_0x0251
                r6 = r27
            L_0x021d:
                r7 = r12
                r8 = r13
                r9 = r14
                r10 = r15
                r5.<init>(r6, r7, r8, r9, r10, r11)
                return r5
            L_0x0225:
                org.joda.time.tz.DateTimeZoneBuilder$DSTZone r29 = new org.joda.time.tz.DateTimeZoneBuilder$DSTZone
                java.lang.String r5 = r30.getID()
                r0 = r30
                int r0 = r0.iStandardOffset
                r6 = r0
                r0 = r30
                org.joda.time.tz.DateTimeZoneBuilder$Recurrence r0 = r0.iStartRecurrence
                r7 = r0
                r0 = r30
                org.joda.time.tz.DateTimeZoneBuilder$Recurrence r0 = r0.iEndRecurrence
                r30 = r0
                java.lang.String r8 = "-Summer"
                r0 = r30
                r1 = r8
                org.joda.time.tz.DateTimeZoneBuilder$Recurrence r30 = r0.renameAppend(r1)
                r0 = r29
                r1 = r5
                r2 = r6
                r3 = r7
                r4 = r30
                r0.<init>(r1, r2, r3, r4)
                r11 = r29
                goto L_0x0217
            L_0x0251:
                java.lang.String r27 = ""
                r6 = r27
                goto L_0x021d
            L_0x0256:
                r11 = r30
                goto L_0x0217
            L_0x0259:
                r6 = r16
                goto L_0x0080
            */
            throw new UnsupportedOperationException("Method not decompiled: org.joda.time.tz.DateTimeZoneBuilder.PrecalculatedZone.create(java.lang.String, boolean, java.util.ArrayList, org.joda.time.tz.DateTimeZoneBuilder$DSTZone):org.joda.time.tz.DateTimeZoneBuilder$PrecalculatedZone");
        }

        private PrecalculatedZone(String str, long[] jArr, int[] iArr, int[] iArr2, String[] strArr, DSTZone dSTZone) {
            super(str);
            this.iTransitions = jArr;
            this.iWallOffsets = iArr;
            this.iStandardOffsets = iArr2;
            this.iNameKeys = strArr;
            this.iTailZone = dSTZone;
        }

        public String getNameKey(long j) {
            long[] jArr = this.iTransitions;
            int binarySearch = Arrays.binarySearch(jArr, j);
            if (binarySearch >= 0) {
                return this.iNameKeys[binarySearch];
            }
            int i = binarySearch ^ -1;
            if (i < jArr.length) {
                if (i > 0) {
                    return this.iNameKeys[i - 1];
                }
                return "UTC";
            } else if (this.iTailZone == null) {
                return this.iNameKeys[i - 1];
            } else {
                return this.iTailZone.getNameKey(j);
            }
        }

        public int getOffset(long j) {
            long[] jArr = this.iTransitions;
            int binarySearch = Arrays.binarySearch(jArr, j);
            if (binarySearch >= 0) {
                return this.iWallOffsets[binarySearch];
            }
            int i = binarySearch ^ -1;
            if (i < jArr.length) {
                if (i > 0) {
                    return this.iWallOffsets[i - 1];
                }
                return 0;
            } else if (this.iTailZone == null) {
                return this.iWallOffsets[i - 1];
            } else {
                return this.iTailZone.getOffset(j);
            }
        }

        public int getStandardOffset(long j) {
            long[] jArr = this.iTransitions;
            int binarySearch = Arrays.binarySearch(jArr, j);
            if (binarySearch >= 0) {
                return this.iStandardOffsets[binarySearch];
            }
            int i = binarySearch ^ -1;
            if (i < jArr.length) {
                if (i > 0) {
                    return this.iStandardOffsets[i - 1];
                }
                return 0;
            } else if (this.iTailZone == null) {
                return this.iStandardOffsets[i - 1];
            } else {
                return this.iTailZone.getStandardOffset(j);
            }
        }

        public boolean isFixed() {
            return false;
        }

        public long nextTransition(long j) {
            long[] jArr = this.iTransitions;
            int binarySearch = Arrays.binarySearch(jArr, j);
            int i = binarySearch >= 0 ? binarySearch + 1 : binarySearch ^ -1;
            if (i < jArr.length) {
                return jArr[i];
            }
            if (this.iTailZone == null) {
                return j;
            }
            long j2 = jArr[jArr.length - 1];
            if (j >= j2) {
                j2 = j;
            }
            return this.iTailZone.nextTransition(j2);
        }

        public long previousTransition(long j) {
            long[] jArr = this.iTransitions;
            int binarySearch = Arrays.binarySearch(jArr, j);
            if (binarySearch < 0) {
                int i = binarySearch ^ -1;
                if (i < jArr.length) {
                    if (i > 0) {
                        long j2 = jArr[i - 1];
                        if (j2 > Long.MIN_VALUE) {
                            return j2 - 1;
                        }
                    }
                    return j;
                }
                if (this.iTailZone != null) {
                    long previousTransition = this.iTailZone.previousTransition(j);
                    if (previousTransition < j) {
                        return previousTransition;
                    }
                }
                long j3 = jArr[i - 1];
                return j3 > Long.MIN_VALUE ? j3 - 1 : j;
            } else if (j > Long.MIN_VALUE) {
                return j - 1;
            } else {
                return j;
            }
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof PrecalculatedZone)) {
                return false;
            }
            PrecalculatedZone precalculatedZone = (PrecalculatedZone) obj;
            return getID().equals(precalculatedZone.getID()) && Arrays.equals(this.iTransitions, precalculatedZone.iTransitions) && Arrays.equals(this.iNameKeys, precalculatedZone.iNameKeys) && Arrays.equals(this.iWallOffsets, precalculatedZone.iWallOffsets) && Arrays.equals(this.iStandardOffsets, precalculatedZone.iStandardOffsets) && (this.iTailZone != null ? this.iTailZone.equals(precalculatedZone.iTailZone) : precalculatedZone.iTailZone == null);
        }

        public void writeTo(DataOutput dataOutput) throws IOException {
            boolean z;
            int length = this.iTransitions.length;
            HashSet<String> hashSet = new HashSet<>();
            for (int i = 0; i < length; i++) {
                hashSet.add(this.iNameKeys[i]);
            }
            int size = hashSet.size();
            if (size > 65535) {
                throw new UnsupportedOperationException("String pool is too large");
            }
            String[] strArr = new String[size];
            int i2 = 0;
            for (String str : hashSet) {
                strArr[i2] = str;
                i2++;
            }
            dataOutput.writeShort(size);
            for (int i3 = 0; i3 < size; i3++) {
                dataOutput.writeUTF(strArr[i3]);
            }
            dataOutput.writeInt(length);
            for (int i4 = 0; i4 < length; i4++) {
                DateTimeZoneBuilder.writeMillis(dataOutput, this.iTransitions[i4]);
                DateTimeZoneBuilder.writeMillis(dataOutput, (long) this.iWallOffsets[i4]);
                DateTimeZoneBuilder.writeMillis(dataOutput, (long) this.iStandardOffsets[i4]);
                String str2 = this.iNameKeys[i4];
                int i5 = 0;
                while (true) {
                    if (i5 >= size) {
                        break;
                    } else if (!strArr[i5].equals(str2)) {
                        i5++;
                    } else if (size < 256) {
                        dataOutput.writeByte(i5);
                    } else {
                        dataOutput.writeShort(i5);
                    }
                }
            }
            if (this.iTailZone != null) {
                z = true;
            } else {
                z = false;
            }
            dataOutput.writeBoolean(z);
            if (this.iTailZone != null) {
                this.iTailZone.writeTo(dataOutput);
            }
        }

        public boolean isCachable() {
            if (this.iTailZone != null) {
                return true;
            }
            long[] jArr = this.iTransitions;
            if (jArr.length <= 1) {
                return false;
            }
            double d = 0.0d;
            int i = 0;
            for (int i2 = 1; i2 < jArr.length; i2++) {
                long j = jArr[i2] - jArr[i2 - 1];
                if (j < 63158400000L) {
                    d += (double) j;
                    i++;
                }
            }
            if (i <= 0 || (d / ((double) i)) / 8.64E7d < 25.0d) {
                return false;
            }
            return true;
        }
    }
}
