package org.joda.time.format;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import org.joda.time.Chronology;
import org.joda.time.DateTimeConstants;
import org.joda.time.DateTimeField;
import org.joda.time.DateTimeFieldType;
import org.joda.time.DateTimeZone;
import org.joda.time.ReadablePartial;
import org.joda.time.field.MillisDurationField;
import org.joda.time.field.PreciseDateTimeField;

public class DateTimeFormatterBuilder {
    private ArrayList<Object> iElementPairs = new ArrayList<>();
    private Object iFormatter;

    public DateTimeFormatter toFormatter() {
        DateTimePrinter dateTimePrinter;
        DateTimeParser dateTimeParser;
        Object formatter = getFormatter();
        if (isPrinter(formatter)) {
            dateTimePrinter = (DateTimePrinter) formatter;
        } else {
            dateTimePrinter = null;
        }
        if (isParser(formatter)) {
            dateTimeParser = (DateTimeParser) formatter;
        } else {
            dateTimeParser = null;
        }
        if (dateTimePrinter != null || dateTimeParser != null) {
            return new DateTimeFormatter(dateTimePrinter, dateTimeParser);
        }
        throw new UnsupportedOperationException("Both printing and parsing not supported");
    }

    public DateTimePrinter toPrinter() {
        Object formatter = getFormatter();
        if (isPrinter(formatter)) {
            return (DateTimePrinter) formatter;
        }
        throw new UnsupportedOperationException("Printing is not supported");
    }

    public DateTimeParser toParser() {
        Object formatter = getFormatter();
        if (isParser(formatter)) {
            return (DateTimeParser) formatter;
        }
        throw new UnsupportedOperationException("Parsing is not supported");
    }

    public boolean canBuildFormatter() {
        return isFormatter(getFormatter());
    }

    public boolean canBuildPrinter() {
        return isPrinter(getFormatter());
    }

    public boolean canBuildParser() {
        return isParser(getFormatter());
    }

    public void clear() {
        this.iFormatter = null;
        this.iElementPairs.clear();
    }

    public DateTimeFormatterBuilder append(DateTimeFormatter dateTimeFormatter) {
        if (dateTimeFormatter != null) {
            return append0(dateTimeFormatter.getPrinter(), dateTimeFormatter.getParser());
        }
        throw new IllegalArgumentException("No formatter supplied");
    }

    public DateTimeFormatterBuilder append(DateTimePrinter dateTimePrinter) {
        checkPrinter(dateTimePrinter);
        return append0(dateTimePrinter, null);
    }

    public DateTimeFormatterBuilder append(DateTimeParser dateTimeParser) {
        checkParser(dateTimeParser);
        return append0(null, dateTimeParser);
    }

    public DateTimeFormatterBuilder append(DateTimePrinter dateTimePrinter, DateTimeParser dateTimeParser) {
        checkPrinter(dateTimePrinter);
        checkParser(dateTimeParser);
        return append0(dateTimePrinter, dateTimeParser);
    }

    public DateTimeFormatterBuilder append(DateTimePrinter dateTimePrinter, DateTimeParser[] dateTimeParserArr) {
        int i = 0;
        if (dateTimePrinter != null) {
            checkPrinter(dateTimePrinter);
        }
        if (dateTimeParserArr == null) {
            throw new IllegalArgumentException("No parsers supplied");
        }
        int length = dateTimeParserArr.length;
        if (length != 1) {
            DateTimeParser[] dateTimeParserArr2 = new DateTimeParser[length];
            while (i < length - 1) {
                DateTimeParser dateTimeParser = dateTimeParserArr[i];
                dateTimeParserArr2[i] = dateTimeParser;
                if (dateTimeParser == null) {
                    throw new IllegalArgumentException("Incomplete parser array");
                }
                i++;
            }
            dateTimeParserArr2[i] = dateTimeParserArr[i];
            return append0(dateTimePrinter, new MatchingParser(dateTimeParserArr2));
        } else if (dateTimeParserArr[0] != null) {
            return append0(dateTimePrinter, dateTimeParserArr[0]);
        } else {
            throw new IllegalArgumentException("No parser supplied");
        }
    }

    public DateTimeFormatterBuilder appendOptional(DateTimeParser dateTimeParser) {
        checkParser(dateTimeParser);
        return append0(null, new MatchingParser(new DateTimeParser[]{dateTimeParser, null}));
    }

    private void checkParser(DateTimeParser dateTimeParser) {
        if (dateTimeParser == null) {
            throw new IllegalArgumentException("No parser supplied");
        }
    }

    private void checkPrinter(DateTimePrinter dateTimePrinter) {
        if (dateTimePrinter == null) {
            throw new IllegalArgumentException("No printer supplied");
        }
    }

    private DateTimeFormatterBuilder append0(Object obj) {
        this.iFormatter = null;
        this.iElementPairs.add(obj);
        this.iElementPairs.add(obj);
        return this;
    }

    private DateTimeFormatterBuilder append0(DateTimePrinter dateTimePrinter, DateTimeParser dateTimeParser) {
        this.iFormatter = null;
        this.iElementPairs.add(dateTimePrinter);
        this.iElementPairs.add(dateTimeParser);
        return this;
    }

    public DateTimeFormatterBuilder appendLiteral(char c) {
        return append0(new CharacterLiteral(c));
    }

    public DateTimeFormatterBuilder appendLiteral(String str) {
        if (str == null) {
            throw new IllegalArgumentException("Literal must not be null");
        }
        switch (str.length()) {
            case 0:
                return this;
            case 1:
                return append0(new CharacterLiteral(str.charAt(0)));
            default:
                return append0(new StringLiteral(str));
        }
    }

    public DateTimeFormatterBuilder appendDecimal(DateTimeFieldType dateTimeFieldType, int i, int i2) {
        int i3;
        if (dateTimeFieldType == null) {
            throw new IllegalArgumentException("Field type must not be null");
        }
        if (i2 < i) {
            i3 = i;
        } else {
            i3 = i2;
        }
        if (i < 0 || i3 <= 0) {
            throw new IllegalArgumentException();
        } else if (i <= 1) {
            return append0(new UnpaddedNumber(dateTimeFieldType, i3, false));
        } else {
            return append0(new PaddedNumber(dateTimeFieldType, i3, false, i));
        }
    }

    public DateTimeFormatterBuilder appendFixedDecimal(DateTimeFieldType dateTimeFieldType, int i) {
        if (dateTimeFieldType == null) {
            throw new IllegalArgumentException("Field type must not be null");
        } else if (i > 0) {
            return append0(new FixedNumber(dateTimeFieldType, i, false));
        } else {
            throw new IllegalArgumentException("Illegal number of digits: " + i);
        }
    }

    public DateTimeFormatterBuilder appendSignedDecimal(DateTimeFieldType dateTimeFieldType, int i, int i2) {
        int i3;
        if (dateTimeFieldType == null) {
            throw new IllegalArgumentException("Field type must not be null");
        }
        if (i2 < i) {
            i3 = i;
        } else {
            i3 = i2;
        }
        if (i < 0 || i3 <= 0) {
            throw new IllegalArgumentException();
        } else if (i <= 1) {
            return append0(new UnpaddedNumber(dateTimeFieldType, i3, true));
        } else {
            return append0(new PaddedNumber(dateTimeFieldType, i3, true, i));
        }
    }

    public DateTimeFormatterBuilder appendFixedSignedDecimal(DateTimeFieldType dateTimeFieldType, int i) {
        if (dateTimeFieldType == null) {
            throw new IllegalArgumentException("Field type must not be null");
        } else if (i > 0) {
            return append0(new FixedNumber(dateTimeFieldType, i, true));
        } else {
            throw new IllegalArgumentException("Illegal number of digits: " + i);
        }
    }

    public DateTimeFormatterBuilder appendText(DateTimeFieldType dateTimeFieldType) {
        if (dateTimeFieldType != null) {
            return append0(new TextField(dateTimeFieldType, false));
        }
        throw new IllegalArgumentException("Field type must not be null");
    }

    public DateTimeFormatterBuilder appendShortText(DateTimeFieldType dateTimeFieldType) {
        if (dateTimeFieldType != null) {
            return append0(new TextField(dateTimeFieldType, true));
        }
        throw new IllegalArgumentException("Field type must not be null");
    }

    public DateTimeFormatterBuilder appendFraction(DateTimeFieldType dateTimeFieldType, int i, int i2) {
        int i3;
        if (dateTimeFieldType == null) {
            throw new IllegalArgumentException("Field type must not be null");
        }
        if (i2 < i) {
            i3 = i;
        } else {
            i3 = i2;
        }
        if (i >= 0 && i3 > 0) {
            return append0(new Fraction(dateTimeFieldType, i, i3));
        }
        throw new IllegalArgumentException();
    }

    public DateTimeFormatterBuilder appendFractionOfSecond(int i, int i2) {
        return appendFraction(DateTimeFieldType.secondOfDay(), i, i2);
    }

    public DateTimeFormatterBuilder appendFractionOfMinute(int i, int i2) {
        return appendFraction(DateTimeFieldType.minuteOfDay(), i, i2);
    }

    public DateTimeFormatterBuilder appendFractionOfHour(int i, int i2) {
        return appendFraction(DateTimeFieldType.hourOfDay(), i, i2);
    }

    public DateTimeFormatterBuilder appendFractionOfDay(int i, int i2) {
        return appendFraction(DateTimeFieldType.dayOfYear(), i, i2);
    }

    public DateTimeFormatterBuilder appendMillisOfSecond(int i) {
        return appendDecimal(DateTimeFieldType.millisOfSecond(), i, 3);
    }

    public DateTimeFormatterBuilder appendMillisOfDay(int i) {
        return appendDecimal(DateTimeFieldType.millisOfDay(), i, 8);
    }

    public DateTimeFormatterBuilder appendSecondOfMinute(int i) {
        return appendDecimal(DateTimeFieldType.secondOfMinute(), i, 2);
    }

    public DateTimeFormatterBuilder appendSecondOfDay(int i) {
        return appendDecimal(DateTimeFieldType.secondOfDay(), i, 5);
    }

    public DateTimeFormatterBuilder appendMinuteOfHour(int i) {
        return appendDecimal(DateTimeFieldType.minuteOfHour(), i, 2);
    }

    public DateTimeFormatterBuilder appendMinuteOfDay(int i) {
        return appendDecimal(DateTimeFieldType.minuteOfDay(), i, 4);
    }

    public DateTimeFormatterBuilder appendHourOfDay(int i) {
        return appendDecimal(DateTimeFieldType.hourOfDay(), i, 2);
    }

    public DateTimeFormatterBuilder appendClockhourOfDay(int i) {
        return appendDecimal(DateTimeFieldType.clockhourOfDay(), i, 2);
    }

    public DateTimeFormatterBuilder appendHourOfHalfday(int i) {
        return appendDecimal(DateTimeFieldType.hourOfHalfday(), i, 2);
    }

    public DateTimeFormatterBuilder appendClockhourOfHalfday(int i) {
        return appendDecimal(DateTimeFieldType.clockhourOfHalfday(), i, 2);
    }

    public DateTimeFormatterBuilder appendDayOfWeek(int i) {
        return appendDecimal(DateTimeFieldType.dayOfWeek(), i, 1);
    }

    public DateTimeFormatterBuilder appendDayOfMonth(int i) {
        return appendDecimal(DateTimeFieldType.dayOfMonth(), i, 2);
    }

    public DateTimeFormatterBuilder appendDayOfYear(int i) {
        return appendDecimal(DateTimeFieldType.dayOfYear(), i, 3);
    }

    public DateTimeFormatterBuilder appendWeekOfWeekyear(int i) {
        return appendDecimal(DateTimeFieldType.weekOfWeekyear(), i, 2);
    }

    public DateTimeFormatterBuilder appendWeekyear(int i, int i2) {
        return appendSignedDecimal(DateTimeFieldType.weekyear(), i, i2);
    }

    public DateTimeFormatterBuilder appendMonthOfYear(int i) {
        return appendDecimal(DateTimeFieldType.monthOfYear(), i, 2);
    }

    public DateTimeFormatterBuilder appendYear(int i, int i2) {
        return appendSignedDecimal(DateTimeFieldType.year(), i, i2);
    }

    public DateTimeFormatterBuilder appendTwoDigitYear(int i) {
        return appendTwoDigitYear(i, false);
    }

    public DateTimeFormatterBuilder appendTwoDigitYear(int i, boolean z) {
        return append0(new TwoDigitYear(DateTimeFieldType.year(), i, z));
    }

    public DateTimeFormatterBuilder appendTwoDigitWeekyear(int i) {
        return appendTwoDigitWeekyear(i, false);
    }

    public DateTimeFormatterBuilder appendTwoDigitWeekyear(int i, boolean z) {
        return append0(new TwoDigitYear(DateTimeFieldType.weekyear(), i, z));
    }

    public DateTimeFormatterBuilder appendYearOfEra(int i, int i2) {
        return appendDecimal(DateTimeFieldType.yearOfEra(), i, i2);
    }

    public DateTimeFormatterBuilder appendYearOfCentury(int i, int i2) {
        return appendDecimal(DateTimeFieldType.yearOfCentury(), i, i2);
    }

    public DateTimeFormatterBuilder appendCenturyOfEra(int i, int i2) {
        return appendSignedDecimal(DateTimeFieldType.centuryOfEra(), i, i2);
    }

    public DateTimeFormatterBuilder appendHalfdayOfDayText() {
        return appendText(DateTimeFieldType.halfdayOfDay());
    }

    public DateTimeFormatterBuilder appendDayOfWeekText() {
        return appendText(DateTimeFieldType.dayOfWeek());
    }

    public DateTimeFormatterBuilder appendDayOfWeekShortText() {
        return appendShortText(DateTimeFieldType.dayOfWeek());
    }

    public DateTimeFormatterBuilder appendMonthOfYearText() {
        return appendText(DateTimeFieldType.monthOfYear());
    }

    public DateTimeFormatterBuilder appendMonthOfYearShortText() {
        return appendShortText(DateTimeFieldType.monthOfYear());
    }

    public DateTimeFormatterBuilder appendEraText() {
        return appendText(DateTimeFieldType.era());
    }

    public DateTimeFormatterBuilder appendTimeZoneName() {
        return append0(new TimeZoneName(0, null), null);
    }

    public DateTimeFormatterBuilder appendTimeZoneName(Map<String, DateTimeZone> map) {
        TimeZoneName timeZoneName = new TimeZoneName(0, map);
        return append0(timeZoneName, timeZoneName);
    }

    public DateTimeFormatterBuilder appendTimeZoneShortName() {
        return append0(new TimeZoneName(1, null), null);
    }

    public DateTimeFormatterBuilder appendTimeZoneShortName(Map<String, DateTimeZone> map) {
        TimeZoneName timeZoneName = new TimeZoneName(1, map);
        return append0(timeZoneName, timeZoneName);
    }

    public DateTimeFormatterBuilder appendTimeZoneId() {
        return append0(TimeZoneId.INSTANCE, TimeZoneId.INSTANCE);
    }

    public DateTimeFormatterBuilder appendTimeZoneOffset(String str, boolean z, int i, int i2) {
        return append0(new TimeZoneOffset(str, str, z, i, i2));
    }

    public DateTimeFormatterBuilder appendTimeZoneOffset(String str, String str2, boolean z, int i, int i2) {
        return append0(new TimeZoneOffset(str, str2, z, i, i2));
    }

    public DateTimeFormatterBuilder appendPattern(String str) {
        DateTimeFormat.appendPatternTo(this, str);
        return this;
    }

    private Object getFormatter() {
        Object obj = this.iFormatter;
        if (obj == null) {
            if (this.iElementPairs.size() == 2) {
                Object obj2 = this.iElementPairs.get(0);
                Object obj3 = this.iElementPairs.get(1);
                if (obj2 == null) {
                    obj = obj3;
                } else if (obj2 == obj3 || obj3 == null) {
                    obj = obj2;
                }
            }
            if (obj == null) {
                obj = new Composite(this.iElementPairs);
            }
            this.iFormatter = obj;
        }
        return obj;
    }

    private boolean isPrinter(Object obj) {
        if (!(obj instanceof DateTimePrinter)) {
            return false;
        }
        if (obj instanceof Composite) {
            return ((Composite) obj).isPrinter();
        }
        return true;
    }

    private boolean isParser(Object obj) {
        if (!(obj instanceof DateTimeParser)) {
            return false;
        }
        if (obj instanceof Composite) {
            return ((Composite) obj).isParser();
        }
        return true;
    }

    private boolean isFormatter(Object obj) {
        return isPrinter(obj) || isParser(obj);
    }

    static void appendUnknownString(StringBuffer stringBuffer, int i) {
        int i2 = i;
        while (true) {
            i2--;
            if (i2 >= 0) {
                stringBuffer.append(65533);
            } else {
                return;
            }
        }
    }

    static void printUnknownString(Writer writer, int i) throws IOException {
        int i2 = i;
        while (true) {
            i2--;
            if (i2 >= 0) {
                writer.write(65533);
            } else {
                return;
            }
        }
    }

    static class CharacterLiteral implements DateTimePrinter, DateTimeParser {
        private final char iValue;

        CharacterLiteral(char c) {
            this.iValue = c;
        }

        public int estimatePrintedLength() {
            return 1;
        }

        public void printTo(StringBuffer stringBuffer, long j, Chronology chronology, int i, DateTimeZone dateTimeZone, Locale locale) {
            stringBuffer.append(this.iValue);
        }

        public void printTo(Writer writer, long j, Chronology chronology, int i, DateTimeZone dateTimeZone, Locale locale) throws IOException {
            writer.write(this.iValue);
        }

        public void printTo(StringBuffer stringBuffer, ReadablePartial readablePartial, Locale locale) {
            stringBuffer.append(this.iValue);
        }

        public void printTo(Writer writer, ReadablePartial readablePartial, Locale locale) throws IOException {
            writer.write(this.iValue);
        }

        public int estimateParsedLength() {
            return 1;
        }

        public int parseInto(DateTimeParserBucket dateTimeParserBucket, String str, int i) {
            char upperCase;
            char upperCase2;
            if (i >= str.length()) {
                return i ^ -1;
            }
            char charAt = str.charAt(i);
            char c = this.iValue;
            if (charAt == c || (upperCase = Character.toUpperCase(charAt)) == (upperCase2 = Character.toUpperCase(c)) || Character.toLowerCase(upperCase) == Character.toLowerCase(upperCase2)) {
                return i + 1;
            }
            return i ^ -1;
        }
    }

    static class StringLiteral implements DateTimePrinter, DateTimeParser {
        private final String iValue;

        StringLiteral(String str) {
            this.iValue = str;
        }

        public int estimatePrintedLength() {
            return this.iValue.length();
        }

        public void printTo(StringBuffer stringBuffer, long j, Chronology chronology, int i, DateTimeZone dateTimeZone, Locale locale) {
            stringBuffer.append(this.iValue);
        }

        public void printTo(Writer writer, long j, Chronology chronology, int i, DateTimeZone dateTimeZone, Locale locale) throws IOException {
            writer.write(this.iValue);
        }

        public void printTo(StringBuffer stringBuffer, ReadablePartial readablePartial, Locale locale) {
            stringBuffer.append(this.iValue);
        }

        public void printTo(Writer writer, ReadablePartial readablePartial, Locale locale) throws IOException {
            writer.write(this.iValue);
        }

        public int estimateParsedLength() {
            return this.iValue.length();
        }

        public int parseInto(DateTimeParserBucket dateTimeParserBucket, String str, int i) {
            if (str.regionMatches(true, i, this.iValue, 0, this.iValue.length())) {
                return this.iValue.length() + i;
            }
            return i ^ -1;
        }
    }

    static abstract class NumberFormatter implements DateTimePrinter, DateTimeParser {
        protected final DateTimeFieldType iFieldType;
        protected final int iMaxParsedDigits;
        protected final boolean iSigned;

        NumberFormatter(DateTimeFieldType dateTimeFieldType, int i, boolean z) {
            this.iFieldType = dateTimeFieldType;
            this.iMaxParsedDigits = i;
            this.iSigned = z;
        }

        public int estimateParsedLength() {
            return this.iMaxParsedDigits;
        }

        public int parseInto(DateTimeParserBucket dateTimeParserBucket, String str, int i) {
            int i2;
            int i3;
            int i4;
            char charAt;
            boolean z = false;
            int min = Math.min(this.iMaxParsedDigits, str.length() - i);
            int i5 = i;
            int i6 = 0;
            while (i6 < min) {
                char charAt2 = str.charAt(i5 + i6);
                if (i6 != 0 || ((charAt2 != '-' && charAt2 != '+') || !this.iSigned)) {
                    if (charAt2 < '0' || charAt2 > '9') {
                        break;
                    }
                    i6++;
                } else {
                    z = charAt2 == '-';
                    if (i6 + 1 >= min || (charAt = str.charAt(i5 + i6 + 1)) < '0' || charAt > '9') {
                        break;
                    }
                    if (z) {
                        i6++;
                    } else {
                        i5++;
                    }
                    min = Math.min(min + 1, str.length() - i5);
                }
            }
            if (i6 == 0) {
                return i5 ^ -1;
            }
            if (i6 >= 9) {
                int i7 = i6 + i5;
                i3 = i7;
                i4 = Integer.parseInt(str.substring(i5, i7));
            } else {
                if (z) {
                    i2 = i5 + 1;
                } else {
                    i2 = i5;
                }
                int i8 = i2 + 1;
                try {
                    int i9 = i6 + i5;
                    int charAt3 = str.charAt(i2) - '0';
                    for (int i10 = i8; i10 < i9; i10++) {
                        charAt3 = (str.charAt(i10) + ((charAt3 << 1) + (charAt3 << 3))) - 48;
                    }
                    if (z) {
                        i3 = i9;
                        i4 = -charAt3;
                    } else {
                        i3 = i9;
                        i4 = charAt3;
                    }
                } catch (StringIndexOutOfBoundsException e) {
                    return i5 ^ -1;
                }
            }
            dateTimeParserBucket.saveField(this.iFieldType, i4);
            return i3;
        }
    }

    static class UnpaddedNumber extends NumberFormatter {
        protected UnpaddedNumber(DateTimeFieldType dateTimeFieldType, int i, boolean z) {
            super(dateTimeFieldType, i, z);
        }

        public int estimatePrintedLength() {
            return this.iMaxParsedDigits;
        }

        public void printTo(StringBuffer stringBuffer, long j, Chronology chronology, int i, DateTimeZone dateTimeZone, Locale locale) {
            try {
                FormatUtils.appendUnpaddedInteger(stringBuffer, this.iFieldType.getField(chronology).get(j));
            } catch (RuntimeException e) {
                stringBuffer.append(65533);
            }
        }

        public void printTo(Writer writer, long j, Chronology chronology, int i, DateTimeZone dateTimeZone, Locale locale) throws IOException {
            try {
                FormatUtils.writeUnpaddedInteger(writer, this.iFieldType.getField(chronology).get(j));
            } catch (RuntimeException e) {
                writer.write(65533);
            }
        }

        public void printTo(StringBuffer stringBuffer, ReadablePartial readablePartial, Locale locale) {
            if (readablePartial.isSupported(this.iFieldType)) {
                try {
                    FormatUtils.appendUnpaddedInteger(stringBuffer, readablePartial.get(this.iFieldType));
                } catch (RuntimeException e) {
                    stringBuffer.append(65533);
                }
            } else {
                stringBuffer.append(65533);
            }
        }

        public void printTo(Writer writer, ReadablePartial readablePartial, Locale locale) throws IOException {
            if (readablePartial.isSupported(this.iFieldType)) {
                try {
                    FormatUtils.writeUnpaddedInteger(writer, readablePartial.get(this.iFieldType));
                } catch (RuntimeException e) {
                    writer.write(65533);
                }
            } else {
                writer.write(65533);
            }
        }
    }

    static class PaddedNumber extends NumberFormatter {
        protected final int iMinPrintedDigits;

        protected PaddedNumber(DateTimeFieldType dateTimeFieldType, int i, boolean z, int i2) {
            super(dateTimeFieldType, i, z);
            this.iMinPrintedDigits = i2;
        }

        public int estimatePrintedLength() {
            return this.iMaxParsedDigits;
        }

        public void printTo(StringBuffer stringBuffer, long j, Chronology chronology, int i, DateTimeZone dateTimeZone, Locale locale) {
            try {
                FormatUtils.appendPaddedInteger(stringBuffer, this.iFieldType.getField(chronology).get(j), this.iMinPrintedDigits);
            } catch (RuntimeException e) {
                DateTimeFormatterBuilder.appendUnknownString(stringBuffer, this.iMinPrintedDigits);
            }
        }

        public void printTo(Writer writer, long j, Chronology chronology, int i, DateTimeZone dateTimeZone, Locale locale) throws IOException {
            try {
                FormatUtils.writePaddedInteger(writer, this.iFieldType.getField(chronology).get(j), this.iMinPrintedDigits);
            } catch (RuntimeException e) {
                DateTimeFormatterBuilder.printUnknownString(writer, this.iMinPrintedDigits);
            }
        }

        public void printTo(StringBuffer stringBuffer, ReadablePartial readablePartial, Locale locale) {
            if (readablePartial.isSupported(this.iFieldType)) {
                try {
                    FormatUtils.appendPaddedInteger(stringBuffer, readablePartial.get(this.iFieldType), this.iMinPrintedDigits);
                } catch (RuntimeException e) {
                    DateTimeFormatterBuilder.appendUnknownString(stringBuffer, this.iMinPrintedDigits);
                }
            } else {
                DateTimeFormatterBuilder.appendUnknownString(stringBuffer, this.iMinPrintedDigits);
            }
        }

        public void printTo(Writer writer, ReadablePartial readablePartial, Locale locale) throws IOException {
            if (readablePartial.isSupported(this.iFieldType)) {
                try {
                    FormatUtils.writePaddedInteger(writer, readablePartial.get(this.iFieldType), this.iMinPrintedDigits);
                } catch (RuntimeException e) {
                    DateTimeFormatterBuilder.printUnknownString(writer, this.iMinPrintedDigits);
                }
            } else {
                DateTimeFormatterBuilder.printUnknownString(writer, this.iMinPrintedDigits);
            }
        }
    }

    static class FixedNumber extends PaddedNumber {
        protected FixedNumber(DateTimeFieldType dateTimeFieldType, int i, boolean z) {
            super(dateTimeFieldType, i, z, i);
        }

        public int parseInto(DateTimeParserBucket dateTimeParserBucket, String str, int i) {
            int i2;
            char charAt;
            int parseInto = super.parseInto(dateTimeParserBucket, str, i);
            if (parseInto < 0 || parseInto == (i2 = this.iMaxParsedDigits + i)) {
                return parseInto;
            }
            if (this.iSigned && ((charAt = str.charAt(i)) == '-' || charAt == '+')) {
                i2++;
            }
            if (parseInto > i2) {
                return (i2 + 1) ^ -1;
            }
            if (parseInto < i2) {
                return parseInto ^ -1;
            }
            return parseInto;
        }
    }

    static class TwoDigitYear implements DateTimePrinter, DateTimeParser {
        private final boolean iLenientParse;
        private final int iPivot;
        private final DateTimeFieldType iType;

        TwoDigitYear(DateTimeFieldType dateTimeFieldType, int i, boolean z) {
            this.iType = dateTimeFieldType;
            this.iPivot = i;
            this.iLenientParse = z;
        }

        public int estimateParsedLength() {
            return this.iLenientParse ? 4 : 2;
        }

        public int parseInto(DateTimeParserBucket dateTimeParserBucket, String str, int i) {
            int i2;
            int i3;
            int i4;
            int i5;
            int i6;
            int length = str.length() - i;
            if (this.iLenientParse) {
                int i7 = i;
                boolean z = false;
                boolean z2 = false;
                int i8 = length;
                int i9 = 0;
                int i10 = i8;
                while (i9 < i10) {
                    char charAt = str.charAt(i7 + i9);
                    if (i9 != 0 || (charAt != '-' && charAt != '+')) {
                        if (charAt < '0' || charAt > '9') {
                            break;
                        }
                        i9++;
                    } else {
                        boolean z3 = charAt == '-';
                        if (z3) {
                            i9++;
                            boolean z4 = z3;
                            z = true;
                            z2 = z4;
                        } else {
                            i7++;
                            i10--;
                            boolean z5 = z3;
                            z = true;
                            z2 = z5;
                        }
                    }
                }
                if (i9 == 0) {
                    return i7 ^ -1;
                }
                if (z || i9 != 2) {
                    if (i9 >= 9) {
                        int i11 = i9 + i7;
                        i5 = i11;
                        i6 = Integer.parseInt(str.substring(i7, i11));
                    } else {
                        if (z2) {
                            i4 = i7 + 1;
                        } else {
                            i4 = i7;
                        }
                        try {
                            int i12 = i9 + i7;
                            int charAt2 = str.charAt(i4) - '0';
                            for (int i13 = i4 + 1; i13 < i12; i13++) {
                                charAt2 = (str.charAt(i13) + ((charAt2 << 1) + (charAt2 << 3))) - 48;
                            }
                            if (z2) {
                                i5 = i12;
                                i6 = -charAt2;
                            } else {
                                i5 = i12;
                                i6 = charAt2;
                            }
                        } catch (StringIndexOutOfBoundsException e) {
                            return i7 ^ -1;
                        }
                    }
                    dateTimeParserBucket.saveField(this.iType, i6);
                    return i5;
                }
                i2 = i7;
            } else if (Math.min(2, length) < 2) {
                return i ^ -1;
            } else {
                i2 = i;
            }
            char charAt3 = str.charAt(i2);
            if (charAt3 < '0' || charAt3 > '9') {
                return i2 ^ -1;
            }
            int i14 = charAt3 - '0';
            char charAt4 = str.charAt(i2 + 1);
            if (charAt4 < '0' || charAt4 > '9') {
                return i2 ^ -1;
            }
            int i15 = (((i14 << 1) + (i14 << 3)) + charAt4) - 48;
            int i16 = this.iPivot;
            if (dateTimeParserBucket.getPivotYear() != null) {
                i16 = dateTimeParserBucket.getPivotYear().intValue();
            }
            int i17 = i16 - 50;
            if (i17 >= 0) {
                i3 = i17 % 100;
            } else {
                i3 = ((i17 + 1) % 100) + 99;
            }
            dateTimeParserBucket.saveField(this.iType, i15 + ((i17 + (i15 < i3 ? 100 : 0)) - i3));
            return i2 + 2;
        }

        public int estimatePrintedLength() {
            return 2;
        }

        public void printTo(StringBuffer stringBuffer, long j, Chronology chronology, int i, DateTimeZone dateTimeZone, Locale locale) {
            int twoDigitYear = getTwoDigitYear(j, chronology);
            if (twoDigitYear < 0) {
                stringBuffer.append(65533);
                stringBuffer.append(65533);
                return;
            }
            FormatUtils.appendPaddedInteger(stringBuffer, twoDigitYear, 2);
        }

        public void printTo(Writer writer, long j, Chronology chronology, int i, DateTimeZone dateTimeZone, Locale locale) throws IOException {
            int twoDigitYear = getTwoDigitYear(j, chronology);
            if (twoDigitYear < 0) {
                writer.write(65533);
                writer.write(65533);
                return;
            }
            FormatUtils.writePaddedInteger(writer, twoDigitYear, 2);
        }

        private int getTwoDigitYear(long j, Chronology chronology) {
            try {
                int i = this.iType.getField(chronology).get(j);
                if (i < 0) {
                    i = -i;
                }
                return i % 100;
            } catch (RuntimeException e) {
                return -1;
            }
        }

        public void printTo(StringBuffer stringBuffer, ReadablePartial readablePartial, Locale locale) {
            int twoDigitYear = getTwoDigitYear(readablePartial);
            if (twoDigitYear < 0) {
                stringBuffer.append(65533);
                stringBuffer.append(65533);
                return;
            }
            FormatUtils.appendPaddedInteger(stringBuffer, twoDigitYear, 2);
        }

        public void printTo(Writer writer, ReadablePartial readablePartial, Locale locale) throws IOException {
            int twoDigitYear = getTwoDigitYear(readablePartial);
            if (twoDigitYear < 0) {
                writer.write(65533);
                writer.write(65533);
                return;
            }
            FormatUtils.writePaddedInteger(writer, twoDigitYear, 2);
        }

        private int getTwoDigitYear(ReadablePartial readablePartial) {
            if (readablePartial.isSupported(this.iType)) {
                try {
                    int i = readablePartial.get(this.iType);
                    if (i < 0) {
                        i = -i;
                    }
                    return i % 100;
                } catch (RuntimeException e) {
                }
            }
            return -1;
        }
    }

    static class TextField implements DateTimePrinter, DateTimeParser {
        private static Map<Locale, Map<DateTimeFieldType, Object[]>> cParseCache = new HashMap();
        private final DateTimeFieldType iFieldType;
        private final boolean iShort;

        TextField(DateTimeFieldType dateTimeFieldType, boolean z) {
            this.iFieldType = dateTimeFieldType;
            this.iShort = z;
        }

        public int estimatePrintedLength() {
            return this.iShort ? 6 : 20;
        }

        public void printTo(StringBuffer stringBuffer, long j, Chronology chronology, int i, DateTimeZone dateTimeZone, Locale locale) {
            try {
                stringBuffer.append(print(j, chronology, locale));
            } catch (RuntimeException e) {
                stringBuffer.append(65533);
            }
        }

        public void printTo(Writer writer, long j, Chronology chronology, int i, DateTimeZone dateTimeZone, Locale locale) throws IOException {
            try {
                writer.write(print(j, chronology, locale));
            } catch (RuntimeException e) {
                writer.write(65533);
            }
        }

        public void printTo(StringBuffer stringBuffer, ReadablePartial readablePartial, Locale locale) {
            try {
                stringBuffer.append(print(readablePartial, locale));
            } catch (RuntimeException e) {
                stringBuffer.append(65533);
            }
        }

        public void printTo(Writer writer, ReadablePartial readablePartial, Locale locale) throws IOException {
            try {
                writer.write(print(readablePartial, locale));
            } catch (RuntimeException e) {
                writer.write(65533);
            }
        }

        private String print(long j, Chronology chronology, Locale locale) {
            DateTimeField field = this.iFieldType.getField(chronology);
            if (this.iShort) {
                return field.getAsShortText(j, locale);
            }
            return field.getAsText(j, locale);
        }

        private String print(ReadablePartial readablePartial, Locale locale) {
            if (!readablePartial.isSupported(this.iFieldType)) {
                return "�";
            }
            DateTimeField field = this.iFieldType.getField(readablePartial.getChronology());
            if (this.iShort) {
                return field.getAsShortText(readablePartial, locale);
            }
            return field.getAsText(readablePartial, locale);
        }

        public int estimateParsedLength() {
            return estimatePrintedLength();
        }

        /* JADX WARNING: Code restructure failed: missing block: B:22:0x00d2, code lost:
            r0 = java.lang.Math.min(r11.length(), r0 + r12);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:23:0x00db, code lost:
            if (r0 <= r12) goto L_0x0103;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:24:0x00dd, code lost:
            r3 = r11.substring(r12, r0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:25:0x00e5, code lost:
            if (r1.contains(r3) == false) goto L_0x0100;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:26:0x00e7, code lost:
            r10.saveField(r9.iFieldType, r3, r2);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:33:0x0100, code lost:
            r0 = r0 - 1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:41:?, code lost:
            return r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:42:?, code lost:
            return r12 ^ -1;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public int parseInto(org.joda.time.format.DateTimeParserBucket r10, java.lang.String r11, int r12) {
            /*
                r9 = this;
                r8 = 32
                java.util.Locale r2 = r10.getLocale()
                java.util.Map<java.util.Locale, java.util.Map<org.joda.time.DateTimeFieldType, java.lang.Object[]>> r3 = org.joda.time.format.DateTimeFormatterBuilder.TextField.cParseCache
                monitor-enter(r3)
                java.util.Map<java.util.Locale, java.util.Map<org.joda.time.DateTimeFieldType, java.lang.Object[]>> r0 = org.joda.time.format.DateTimeFormatterBuilder.TextField.cParseCache     // Catch:{ all -> 0x00fd }
                java.lang.Object r0 = r0.get(r2)     // Catch:{ all -> 0x00fd }
                java.util.Map r0 = (java.util.Map) r0     // Catch:{ all -> 0x00fd }
                if (r0 != 0) goto L_0x0109
                java.util.HashMap r0 = new java.util.HashMap     // Catch:{ all -> 0x00fd }
                r0.<init>()     // Catch:{ all -> 0x00fd }
                java.util.Map<java.util.Locale, java.util.Map<org.joda.time.DateTimeFieldType, java.lang.Object[]>> r1 = org.joda.time.format.DateTimeFormatterBuilder.TextField.cParseCache     // Catch:{ all -> 0x00fd }
                r1.put(r2, r0)     // Catch:{ all -> 0x00fd }
                r1 = r0
            L_0x001e:
                org.joda.time.DateTimeFieldType r0 = r9.iFieldType     // Catch:{ all -> 0x00fd }
                java.lang.Object r0 = r1.get(r0)     // Catch:{ all -> 0x00fd }
                java.lang.Object[] r0 = (java.lang.Object[]) r0     // Catch:{ all -> 0x00fd }
                if (r0 != 0) goto L_0x00ee
                java.util.HashSet r0 = new java.util.HashSet     // Catch:{ all -> 0x00fd }
                r4 = 32
                r0.<init>(r4)     // Catch:{ all -> 0x00fd }
                org.joda.time.MutableDateTime r4 = new org.joda.time.MutableDateTime     // Catch:{ all -> 0x00fd }
                r5 = 0
                org.joda.time.DateTimeZone r7 = org.joda.time.DateTimeZone.UTC     // Catch:{ all -> 0x00fd }
                r4.<init>(r5, r7)     // Catch:{ all -> 0x00fd }
                org.joda.time.DateTimeFieldType r5 = r9.iFieldType     // Catch:{ all -> 0x00fd }
                org.joda.time.MutableDateTime$Property r4 = r4.property(r5)     // Catch:{ all -> 0x00fd }
                int r5 = r4.getMinimumValueOverall()     // Catch:{ all -> 0x00fd }
                int r6 = r4.getMaximumValueOverall()     // Catch:{ all -> 0x00fd }
                int r7 = r6 - r5
                if (r7 <= r8) goto L_0x004e
                r0 = r12 ^ -1
                monitor-exit(r3)     // Catch:{ all -> 0x00fd }
            L_0x004d:
                return r0
            L_0x004e:
                int r7 = r4.getMaximumTextLength(r2)     // Catch:{ all -> 0x00fd }
            L_0x0052:
                if (r5 > r6) goto L_0x0094
                r4.set(r5)     // Catch:{ all -> 0x00fd }
                java.lang.String r8 = r4.getAsShortText(r2)     // Catch:{ all -> 0x00fd }
                r0.add(r8)     // Catch:{ all -> 0x00fd }
                java.lang.String r8 = r4.getAsShortText(r2)     // Catch:{ all -> 0x00fd }
                java.lang.String r8 = r8.toLowerCase(r2)     // Catch:{ all -> 0x00fd }
                r0.add(r8)     // Catch:{ all -> 0x00fd }
                java.lang.String r8 = r4.getAsShortText(r2)     // Catch:{ all -> 0x00fd }
                java.lang.String r8 = r8.toUpperCase(r2)     // Catch:{ all -> 0x00fd }
                r0.add(r8)     // Catch:{ all -> 0x00fd }
                java.lang.String r8 = r4.getAsText(r2)     // Catch:{ all -> 0x00fd }
                r0.add(r8)     // Catch:{ all -> 0x00fd }
                java.lang.String r8 = r4.getAsText(r2)     // Catch:{ all -> 0x00fd }
                java.lang.String r8 = r8.toLowerCase(r2)     // Catch:{ all -> 0x00fd }
                r0.add(r8)     // Catch:{ all -> 0x00fd }
                java.lang.String r8 = r4.getAsText(r2)     // Catch:{ all -> 0x00fd }
                java.lang.String r8 = r8.toUpperCase(r2)     // Catch:{ all -> 0x00fd }
                r0.add(r8)     // Catch:{ all -> 0x00fd }
                int r5 = r5 + 1
                goto L_0x0052
            L_0x0094:
                java.lang.String r4 = "en"
                java.lang.String r5 = r2.getLanguage()     // Catch:{ all -> 0x00fd }
                boolean r4 = r4.equals(r5)     // Catch:{ all -> 0x00fd }
                if (r4 == 0) goto L_0x0107
                org.joda.time.DateTimeFieldType r4 = r9.iFieldType     // Catch:{ all -> 0x00fd }
                org.joda.time.DateTimeFieldType r5 = org.joda.time.DateTimeFieldType.era()     // Catch:{ all -> 0x00fd }
                if (r4 != r5) goto L_0x0107
                java.lang.String r4 = "BCE"
                r0.add(r4)     // Catch:{ all -> 0x00fd }
                java.lang.String r4 = "bce"
                r0.add(r4)     // Catch:{ all -> 0x00fd }
                java.lang.String r4 = "CE"
                r0.add(r4)     // Catch:{ all -> 0x00fd }
                java.lang.String r4 = "ce"
                r0.add(r4)     // Catch:{ all -> 0x00fd }
                r4 = 3
            L_0x00bd:
                r5 = 2
                java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ all -> 0x00fd }
                r6 = 0
                r5[r6] = r0     // Catch:{ all -> 0x00fd }
                r6 = 1
                java.lang.Integer r7 = java.lang.Integer.valueOf(r4)     // Catch:{ all -> 0x00fd }
                r5[r6] = r7     // Catch:{ all -> 0x00fd }
                org.joda.time.DateTimeFieldType r6 = r9.iFieldType     // Catch:{ all -> 0x00fd }
                r1.put(r6, r5)     // Catch:{ all -> 0x00fd }
                r1 = r0
                r0 = r4
            L_0x00d1:
                monitor-exit(r3)     // Catch:{ all -> 0x00fd }
                int r3 = r11.length()
                int r0 = r0 + r12
                int r0 = java.lang.Math.min(r3, r0)
            L_0x00db:
                if (r0 <= r12) goto L_0x0103
                java.lang.String r3 = r11.substring(r12, r0)
                boolean r4 = r1.contains(r3)
                if (r4 == 0) goto L_0x0100
                org.joda.time.DateTimeFieldType r1 = r9.iFieldType
                r10.saveField(r1, r3, r2)
                goto L_0x004d
            L_0x00ee:
                r1 = 0
                r1 = r0[r1]     // Catch:{ all -> 0x00fd }
                java.util.Set r1 = (java.util.Set) r1     // Catch:{ all -> 0x00fd }
                r4 = 1
                r0 = r0[r4]     // Catch:{ all -> 0x00fd }
                java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ all -> 0x00fd }
                int r0 = r0.intValue()     // Catch:{ all -> 0x00fd }
                goto L_0x00d1
            L_0x00fd:
                r0 = move-exception
                monitor-exit(r3)     // Catch:{ all -> 0x00fd }
                throw r0
            L_0x0100:
                int r0 = r0 + -1
                goto L_0x00db
            L_0x0103:
                r0 = r12 ^ -1
                goto L_0x004d
            L_0x0107:
                r4 = r7
                goto L_0x00bd
            L_0x0109:
                r1 = r0
                goto L_0x001e
            */
            throw new UnsupportedOperationException("Method not decompiled: org.joda.time.format.DateTimeFormatterBuilder.TextField.parseInto(org.joda.time.format.DateTimeParserBucket, java.lang.String, int):int");
        }
    }

    static class Fraction implements DateTimePrinter, DateTimeParser {
        private final DateTimeFieldType iFieldType;
        protected int iMaxDigits;
        protected int iMinDigits;

        protected Fraction(DateTimeFieldType dateTimeFieldType, int i, int i2) {
            int i3 = 18;
            this.iFieldType = dateTimeFieldType;
            i3 = i2 <= 18 ? i2 : i3;
            this.iMinDigits = i;
            this.iMaxDigits = i3;
        }

        public int estimatePrintedLength() {
            return this.iMaxDigits;
        }

        public void printTo(StringBuffer stringBuffer, long j, Chronology chronology, int i, DateTimeZone dateTimeZone, Locale locale) {
            try {
                printTo(stringBuffer, null, j, chronology);
            } catch (IOException e) {
            }
        }

        public void printTo(Writer writer, long j, Chronology chronology, int i, DateTimeZone dateTimeZone, Locale locale) throws IOException {
            printTo(null, writer, j, chronology);
        }

        public void printTo(StringBuffer stringBuffer, ReadablePartial readablePartial, Locale locale) {
            try {
                printTo(stringBuffer, null, readablePartial.getChronology().set(readablePartial, 0), readablePartial.getChronology());
            } catch (IOException e) {
            }
        }

        public void printTo(Writer writer, ReadablePartial readablePartial, Locale locale) throws IOException {
            printTo(null, writer, readablePartial.getChronology().set(readablePartial, 0), readablePartial.getChronology());
        }

        /* access modifiers changed from: protected */
        public void printTo(StringBuffer stringBuffer, Writer writer, long j, Chronology chronology) throws IOException {
            String l;
            DateTimeField field = this.iFieldType.getField(chronology);
            int i = this.iMinDigits;
            try {
                long remainder = field.remainder(j);
                if (remainder != 0) {
                    long[] fractionData = getFractionData(remainder, field);
                    long j2 = fractionData[0];
                    int i2 = (int) fractionData[1];
                    if ((2147483647L & j2) == j2) {
                        l = Integer.toString((int) j2);
                    } else {
                        l = Long.toString(j2);
                    }
                    int length = l.length();
                    while (length < i2) {
                        if (stringBuffer != null) {
                            stringBuffer.append('0');
                        } else {
                            writer.write(48);
                        }
                        i--;
                        i2--;
                    }
                    if (i < i2) {
                        while (i < i2 && length > 1 && l.charAt(length - 1) == '0') {
                            i2--;
                            length--;
                        }
                        if (length < l.length()) {
                            if (stringBuffer != null) {
                                for (int i3 = 0; i3 < length; i3++) {
                                    stringBuffer.append(l.charAt(i3));
                                }
                                return;
                            }
                            for (int i4 = 0; i4 < length; i4++) {
                                writer.write(l.charAt(i4));
                            }
                            return;
                        }
                    }
                    if (stringBuffer != null) {
                        stringBuffer.append(l);
                    } else {
                        writer.write(l);
                    }
                } else if (stringBuffer != null) {
                    int i5 = i;
                    while (true) {
                        i5--;
                        if (i5 >= 0) {
                            stringBuffer.append('0');
                        } else {
                            return;
                        }
                    }
                } else {
                    int i6 = i;
                    while (true) {
                        i6--;
                        if (i6 >= 0) {
                            writer.write(48);
                        } else {
                            return;
                        }
                    }
                }
            } catch (RuntimeException e) {
                if (stringBuffer != null) {
                    DateTimeFormatterBuilder.appendUnknownString(stringBuffer, i);
                } else {
                    DateTimeFormatterBuilder.printUnknownString(writer, i);
                }
            }
        }

        private long[] getFractionData(long j, DateTimeField dateTimeField) {
            long j2;
            long unitMillis = dateTimeField.getDurationField().getUnitMillis();
            int i = this.iMaxDigits;
            while (true) {
                switch (i) {
                    case 1:
                        j2 = 10;
                        break;
                    case 2:
                        j2 = 100;
                        break;
                    case 3:
                        j2 = 1000;
                        break;
                    case 4:
                        j2 = 10000;
                        break;
                    case 5:
                        j2 = 100000;
                        break;
                    case 6:
                        j2 = 1000000;
                        break;
                    case 7:
                        j2 = 10000000;
                        break;
                    case DateTimeConstants.AUGUST:
                        j2 = 100000000;
                        break;
                    case DateTimeConstants.SEPTEMBER:
                        j2 = 1000000000;
                        break;
                    case DateTimeConstants.OCTOBER:
                        j2 = 10000000000L;
                        break;
                    case DateTimeConstants.NOVEMBER:
                        j2 = 100000000000L;
                        break;
                    case DateTimeConstants.DECEMBER:
                        j2 = 1000000000000L;
                        break;
                    case 13:
                        j2 = 10000000000000L;
                        break;
                    case 14:
                        j2 = 100000000000000L;
                        break;
                    case 15:
                        j2 = 1000000000000000L;
                        break;
                    case 16:
                        j2 = 10000000000000000L;
                        break;
                    case 17:
                        j2 = 100000000000000000L;
                        break;
                    case 18:
                        j2 = 1000000000000000000L;
                        break;
                    default:
                        j2 = 1;
                        break;
                }
                if ((unitMillis * j2) / j2 == unitMillis) {
                    return new long[]{(j2 * j) / unitMillis, (long) i};
                }
                i--;
            }
        }

        public int estimateParsedLength() {
            return this.iMaxDigits;
        }

        public int parseInto(DateTimeParserBucket dateTimeParserBucket, String str, int i) {
            DateTimeField field = this.iFieldType.getField(dateTimeParserBucket.getChronology());
            int min = Math.min(this.iMaxDigits, str.length() - i);
            long j = 0;
            int i2 = 0;
            long unitMillis = field.getDurationField().getUnitMillis() * 10;
            while (i2 < min) {
                char charAt = str.charAt(i + i2);
                if (charAt < '0' || charAt > '9') {
                    break;
                }
                i2++;
                unitMillis /= 10;
                j += ((long) (charAt - '0')) * unitMillis;
            }
            long j2 = j / 10;
            if (i2 == 0) {
                return i ^ -1;
            }
            if (j2 > 2147483647L) {
                return i ^ -1;
            }
            dateTimeParserBucket.saveField(new PreciseDateTimeField(DateTimeFieldType.millisOfSecond(), MillisDurationField.INSTANCE, field.getDurationField()), (int) j2);
            return i + i2;
        }
    }

    static class TimeZoneOffset implements DateTimePrinter, DateTimeParser {
        private final int iMaxFields;
        private final int iMinFields;
        private final boolean iShowSeparators;
        private final String iZeroOffsetParseText;
        private final String iZeroOffsetPrintText;

        TimeZoneOffset(String str, String str2, boolean z, int i, int i2) {
            int i3;
            int i4 = 4;
            this.iZeroOffsetPrintText = str;
            this.iZeroOffsetParseText = str2;
            this.iShowSeparators = z;
            if (i <= 0 || i2 < i) {
                throw new IllegalArgumentException();
            }
            if (i > 4) {
                i3 = 4;
            } else {
                i4 = i2;
                i3 = i;
            }
            this.iMinFields = i3;
            this.iMaxFields = i4;
        }

        public int estimatePrintedLength() {
            int i = (this.iMinFields + 1) << 1;
            if (this.iShowSeparators) {
                i += this.iMinFields - 1;
            }
            if (this.iZeroOffsetPrintText == null || this.iZeroOffsetPrintText.length() <= i) {
                return i;
            }
            return this.iZeroOffsetPrintText.length();
        }

        public void printTo(StringBuffer stringBuffer, long j, Chronology chronology, int i, DateTimeZone dateTimeZone, Locale locale) {
            int i2;
            if (dateTimeZone != null) {
                if (i != 0 || this.iZeroOffsetPrintText == null) {
                    if (i >= 0) {
                        stringBuffer.append('+');
                        i2 = i;
                    } else {
                        stringBuffer.append('-');
                        i2 = -i;
                    }
                    int i3 = i2 / DateTimeConstants.MILLIS_PER_HOUR;
                    FormatUtils.appendPaddedInteger(stringBuffer, i3, 2);
                    if (this.iMaxFields != 1) {
                        int i4 = i2 - (i3 * DateTimeConstants.MILLIS_PER_HOUR);
                        if (i4 != 0 || this.iMinFields > 1) {
                            int i5 = i4 / DateTimeConstants.MILLIS_PER_MINUTE;
                            if (this.iShowSeparators) {
                                stringBuffer.append(':');
                            }
                            FormatUtils.appendPaddedInteger(stringBuffer, i5, 2);
                            if (this.iMaxFields != 2) {
                                int i6 = i4 - (i5 * DateTimeConstants.MILLIS_PER_MINUTE);
                                if (i6 != 0 || this.iMinFields > 2) {
                                    int i7 = i6 / DateTimeConstants.MILLIS_PER_SECOND;
                                    if (this.iShowSeparators) {
                                        stringBuffer.append(':');
                                    }
                                    FormatUtils.appendPaddedInteger(stringBuffer, i7, 2);
                                    if (this.iMaxFields != 3) {
                                        int i8 = i6 - (i7 * DateTimeConstants.MILLIS_PER_SECOND);
                                        if (i8 != 0 || this.iMinFields > 3) {
                                            if (this.iShowSeparators) {
                                                stringBuffer.append('.');
                                            }
                                            FormatUtils.appendPaddedInteger(stringBuffer, i8, 3);
                                            return;
                                        }
                                        return;
                                    }
                                    return;
                                }
                                return;
                            }
                            return;
                        }
                        return;
                    }
                    return;
                }
                stringBuffer.append(this.iZeroOffsetPrintText);
            }
        }

        public void printTo(Writer writer, long j, Chronology chronology, int i, DateTimeZone dateTimeZone, Locale locale) throws IOException {
            int i2;
            if (dateTimeZone != null) {
                if (i != 0 || this.iZeroOffsetPrintText == null) {
                    if (i >= 0) {
                        writer.write(43);
                        i2 = i;
                    } else {
                        writer.write(45);
                        i2 = -i;
                    }
                    int i3 = i2 / DateTimeConstants.MILLIS_PER_HOUR;
                    FormatUtils.writePaddedInteger(writer, i3, 2);
                    if (this.iMaxFields != 1) {
                        int i4 = i2 - (i3 * DateTimeConstants.MILLIS_PER_HOUR);
                        if (i4 != 0 || this.iMinFields != 1) {
                            int i5 = i4 / DateTimeConstants.MILLIS_PER_MINUTE;
                            if (this.iShowSeparators) {
                                writer.write(58);
                            }
                            FormatUtils.writePaddedInteger(writer, i5, 2);
                            if (this.iMaxFields != 2) {
                                int i6 = i4 - (i5 * DateTimeConstants.MILLIS_PER_MINUTE);
                                if (i6 != 0 || this.iMinFields != 2) {
                                    int i7 = i6 / DateTimeConstants.MILLIS_PER_SECOND;
                                    if (this.iShowSeparators) {
                                        writer.write(58);
                                    }
                                    FormatUtils.writePaddedInteger(writer, i7, 2);
                                    if (this.iMaxFields != 3) {
                                        int i8 = i6 - (i7 * DateTimeConstants.MILLIS_PER_SECOND);
                                        if (i8 != 0 || this.iMinFields != 3) {
                                            if (this.iShowSeparators) {
                                                writer.write(46);
                                            }
                                            FormatUtils.writePaddedInteger(writer, i8, 3);
                                            return;
                                        }
                                        return;
                                    }
                                    return;
                                }
                                return;
                            }
                            return;
                        }
                        return;
                    }
                    return;
                }
                writer.write(this.iZeroOffsetPrintText);
            }
        }

        public void printTo(StringBuffer stringBuffer, ReadablePartial readablePartial, Locale locale) {
        }

        public void printTo(Writer writer, ReadablePartial readablePartial, Locale locale) throws IOException {
        }

        public int estimateParsedLength() {
            return estimatePrintedLength();
        }

        public int parseInto(DateTimeParserBucket dateTimeParserBucket, String str, int i) {
            boolean z;
            int i2;
            int i3;
            int i4;
            int i5;
            boolean z2;
            int i6;
            int i7;
            char charAt;
            int length = str.length() - i;
            if (this.iZeroOffsetParseText != null) {
                if (this.iZeroOffsetParseText.length() == 0) {
                    if (length <= 0 || !((charAt = str.charAt(i)) == '-' || charAt == '+')) {
                        dateTimeParserBucket.setOffset((Integer) 0);
                        return i;
                    }
                } else if (str.regionMatches(true, i, this.iZeroOffsetParseText, 0, this.iZeroOffsetParseText.length())) {
                    dateTimeParserBucket.setOffset((Integer) 0);
                    return this.iZeroOffsetParseText.length() + i;
                }
            }
            if (length <= 1) {
                return i ^ -1;
            }
            char charAt2 = str.charAt(i);
            if (charAt2 == '-') {
                z = true;
            } else if (charAt2 != '+') {
                return i ^ -1;
            } else {
                z = false;
            }
            int i8 = length - 1;
            int i9 = i + 1;
            if (digitCount(str, i9, 2) < 2) {
                return i9 ^ -1;
            }
            int parseTwoDigits = FormatUtils.parseTwoDigits(str, i9);
            if (parseTwoDigits > 23) {
                return i9 ^ -1;
            }
            int i10 = parseTwoDigits * DateTimeConstants.MILLIS_PER_HOUR;
            int i11 = i8 - 2;
            int i12 = i9 + 2;
            if (i11 <= 0) {
                i2 = i10;
                i3 = i12;
            } else {
                char charAt3 = str.charAt(i12);
                if (charAt3 == ':') {
                    i4 = i12 + 1;
                    i5 = i11 - 1;
                    z2 = true;
                } else if (charAt3 < '0' || charAt3 > '9') {
                    i2 = i10;
                    i3 = i12;
                } else {
                    i4 = i12;
                    i5 = i11;
                    z2 = false;
                }
                int digitCount = digitCount(str, i4, 2);
                if (digitCount == 0 && !z2) {
                    i2 = i10;
                    i3 = i4;
                } else if (digitCount < 2) {
                    return i4 ^ -1;
                } else {
                    int parseTwoDigits2 = FormatUtils.parseTwoDigits(str, i4);
                    if (parseTwoDigits2 > 59) {
                        return i4 ^ -1;
                    }
                    int i13 = i10 + (parseTwoDigits2 * DateTimeConstants.MILLIS_PER_MINUTE);
                    int i14 = i5 - 2;
                    int i15 = i4 + 2;
                    if (i14 <= 0) {
                        i2 = i13;
                        i3 = i15;
                    } else {
                        if (z2) {
                            if (str.charAt(i15) != ':') {
                                i2 = i13;
                                i3 = i15;
                            } else {
                                i14--;
                                i15++;
                            }
                        }
                        int digitCount2 = digitCount(str, i15, 2);
                        if (digitCount2 == 0 && !z2) {
                            i2 = i13;
                            i3 = i15;
                        } else if (digitCount2 < 2) {
                            return i15 ^ -1;
                        } else {
                            int parseTwoDigits3 = FormatUtils.parseTwoDigits(str, i15);
                            if (parseTwoDigits3 > 59) {
                                return i15 ^ -1;
                            }
                            int i16 = i13 + (parseTwoDigits3 * DateTimeConstants.MILLIS_PER_SECOND);
                            int i17 = i14 - 2;
                            int i18 = i15 + 2;
                            if (i17 <= 0) {
                                i2 = i16;
                                i3 = i18;
                            } else {
                                if (!z2) {
                                    i6 = i18;
                                } else if (str.charAt(i18) == '.' || str.charAt(i18) == ',') {
                                    int i19 = i17 - 1;
                                    i6 = i18 + 1;
                                } else {
                                    i2 = i16;
                                    i3 = i18;
                                }
                                int digitCount3 = digitCount(str, i6, 3);
                                if (digitCount3 == 0 && !z2) {
                                    i2 = i16;
                                    i3 = i6;
                                } else if (digitCount3 < 1) {
                                    return i6 ^ -1;
                                } else {
                                    i3 = i6 + 1;
                                    int charAt4 = ((str.charAt(i6) - '0') * 100) + i16;
                                    if (digitCount3 > 1) {
                                        int i20 = i3 + 1;
                                        int charAt5 = ((str.charAt(i3) - '0') * 10) + charAt4;
                                        if (digitCount3 > 2) {
                                            int i21 = i20 + 1;
                                            i2 = (str.charAt(i20) - '0') + charAt5;
                                            i3 = i21;
                                        } else {
                                            int i22 = charAt5;
                                            i3 = i20;
                                            i2 = i22;
                                        }
                                    } else {
                                        i2 = charAt4;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (z) {
                i7 = -i2;
            } else {
                i7 = i2;
            }
            dateTimeParserBucket.setOffset(Integer.valueOf(i7));
            return i3;
        }

        private int digitCount(String str, int i, int i2) {
            int i3 = 0;
            for (int min = Math.min(str.length() - i, i2); min > 0; min--) {
                char charAt = str.charAt(i + i3);
                if (charAt < '0' || charAt > '9') {
                    break;
                }
                i3++;
            }
            return i3;
        }
    }

    static class TimeZoneName implements DateTimePrinter, DateTimeParser {
        static final int LONG_NAME = 0;
        static final int SHORT_NAME = 1;
        private final Map<String, DateTimeZone> iParseLookup;
        private final int iType;

        TimeZoneName(int i, Map<String, DateTimeZone> map) {
            this.iType = i;
            this.iParseLookup = map;
        }

        public int estimatePrintedLength() {
            return this.iType == 1 ? 4 : 20;
        }

        public void printTo(StringBuffer stringBuffer, long j, Chronology chronology, int i, DateTimeZone dateTimeZone, Locale locale) {
            stringBuffer.append(print(j - ((long) i), dateTimeZone, locale));
        }

        public void printTo(Writer writer, long j, Chronology chronology, int i, DateTimeZone dateTimeZone, Locale locale) throws IOException {
            writer.write(print(j - ((long) i), dateTimeZone, locale));
        }

        private String print(long j, DateTimeZone dateTimeZone, Locale locale) {
            if (dateTimeZone == null) {
                return "";
            }
            switch (this.iType) {
                case 0:
                    return dateTimeZone.getName(j, locale);
                case 1:
                    return dateTimeZone.getShortName(j, locale);
                default:
                    return "";
            }
        }

        public void printTo(StringBuffer stringBuffer, ReadablePartial readablePartial, Locale locale) {
        }

        public void printTo(Writer writer, ReadablePartial readablePartial, Locale locale) throws IOException {
        }

        public int estimateParsedLength() {
            return this.iType == 1 ? 4 : 20;
        }

        public int parseInto(DateTimeParserBucket dateTimeParserBucket, String str, int i) {
            String substring = str.substring(i);
            for (String next : this.iParseLookup.keySet()) {
                if (substring.startsWith(next)) {
                    dateTimeParserBucket.setZone(this.iParseLookup.get(next));
                    return next.length() + i;
                }
            }
            return i ^ -1;
        }
    }

    enum TimeZoneId implements DateTimePrinter, DateTimeParser {
        INSTANCE;
        
        static final Set<String> ALL_IDS = DateTimeZone.getAvailableIDs();
        static final int MAX_LENGTH;

        static {
            int i = 0;
            for (String length : ALL_IDS) {
                i = Math.max(i, length.length());
            }
            MAX_LENGTH = i;
        }

        public int estimatePrintedLength() {
            return MAX_LENGTH;
        }

        public void printTo(StringBuffer stringBuffer, long j, Chronology chronology, int i, DateTimeZone dateTimeZone, Locale locale) {
            stringBuffer.append(dateTimeZone != null ? dateTimeZone.getID() : "");
        }

        public void printTo(Writer writer, long j, Chronology chronology, int i, DateTimeZone dateTimeZone, Locale locale) throws IOException {
            writer.write(dateTimeZone != null ? dateTimeZone.getID() : "");
        }

        public void printTo(StringBuffer stringBuffer, ReadablePartial readablePartial, Locale locale) {
        }

        public void printTo(Writer writer, ReadablePartial readablePartial, Locale locale) throws IOException {
        }

        public int estimateParsedLength() {
            return MAX_LENGTH;
        }

        public int parseInto(DateTimeParserBucket dateTimeParserBucket, String str, int i) {
            String substring = str.substring(i);
            for (String next : ALL_IDS) {
                if (substring.startsWith(next)) {
                    dateTimeParserBucket.setZone(DateTimeZone.forID(next));
                    return next.length() + i;
                }
            }
            return i ^ -1;
        }
    }

    static class Composite implements DateTimePrinter, DateTimeParser {
        private final int iParsedLengthEstimate;
        private final DateTimeParser[] iParsers;
        private final int iPrintedLengthEstimate;
        private final DateTimePrinter[] iPrinters;

        Composite(List<Object> list) {
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            decompose(list, arrayList, arrayList2);
            if (arrayList.size() <= 0) {
                this.iPrinters = null;
                this.iPrintedLengthEstimate = 0;
            } else {
                int size = arrayList.size();
                this.iPrinters = new DateTimePrinter[size];
                int i = 0;
                for (int i2 = 0; i2 < size; i2++) {
                    DateTimePrinter dateTimePrinter = (DateTimePrinter) arrayList.get(i2);
                    i += dateTimePrinter.estimatePrintedLength();
                    this.iPrinters[i2] = dateTimePrinter;
                }
                this.iPrintedLengthEstimate = i;
            }
            if (arrayList2.size() <= 0) {
                this.iParsers = null;
                this.iParsedLengthEstimate = 0;
                return;
            }
            int size2 = arrayList2.size();
            this.iParsers = new DateTimeParser[size2];
            int i3 = 0;
            for (int i4 = 0; i4 < size2; i4++) {
                DateTimeParser dateTimeParser = (DateTimeParser) arrayList2.get(i4);
                i3 += dateTimeParser.estimateParsedLength();
                this.iParsers[i4] = dateTimeParser;
            }
            this.iParsedLengthEstimate = i3;
        }

        public int estimatePrintedLength() {
            return this.iPrintedLengthEstimate;
        }

        public void printTo(StringBuffer stringBuffer, long j, Chronology chronology, int i, DateTimeZone dateTimeZone, Locale locale) {
            Locale locale2;
            DateTimePrinter[] dateTimePrinterArr = this.iPrinters;
            if (dateTimePrinterArr == null) {
                throw new UnsupportedOperationException();
            }
            if (locale == null) {
                locale2 = Locale.getDefault();
            } else {
                locale2 = locale;
            }
            for (DateTimePrinter printTo : dateTimePrinterArr) {
                printTo.printTo(stringBuffer, j, chronology, i, dateTimeZone, locale2);
            }
        }

        public void printTo(Writer writer, long j, Chronology chronology, int i, DateTimeZone dateTimeZone, Locale locale) throws IOException {
            Locale locale2;
            DateTimePrinter[] dateTimePrinterArr = this.iPrinters;
            if (dateTimePrinterArr == null) {
                throw new UnsupportedOperationException();
            }
            if (locale == null) {
                locale2 = Locale.getDefault();
            } else {
                locale2 = locale;
            }
            for (DateTimePrinter printTo : dateTimePrinterArr) {
                printTo.printTo(writer, j, chronology, i, dateTimeZone, locale2);
            }
        }

        public void printTo(StringBuffer stringBuffer, ReadablePartial readablePartial, Locale locale) {
            Locale locale2;
            DateTimePrinter[] dateTimePrinterArr = this.iPrinters;
            if (dateTimePrinterArr == null) {
                throw new UnsupportedOperationException();
            }
            if (locale == null) {
                locale2 = Locale.getDefault();
            } else {
                locale2 = locale;
            }
            for (DateTimePrinter printTo : dateTimePrinterArr) {
                printTo.printTo(stringBuffer, readablePartial, locale2);
            }
        }

        public void printTo(Writer writer, ReadablePartial readablePartial, Locale locale) throws IOException {
            Locale locale2;
            DateTimePrinter[] dateTimePrinterArr = this.iPrinters;
            if (dateTimePrinterArr == null) {
                throw new UnsupportedOperationException();
            }
            if (locale == null) {
                locale2 = Locale.getDefault();
            } else {
                locale2 = locale;
            }
            for (DateTimePrinter printTo : dateTimePrinterArr) {
                printTo.printTo(writer, readablePartial, locale2);
            }
        }

        public int estimateParsedLength() {
            return this.iParsedLengthEstimate;
        }

        public int parseInto(DateTimeParserBucket dateTimeParserBucket, String str, int i) {
            DateTimeParser[] dateTimeParserArr = this.iParsers;
            if (dateTimeParserArr == null) {
                throw new UnsupportedOperationException();
            }
            int length = dateTimeParserArr.length;
            int i2 = i;
            for (int i3 = 0; i3 < length && i2 >= 0; i3++) {
                i2 = dateTimeParserArr[i3].parseInto(dateTimeParserBucket, str, i2);
            }
            return i2;
        }

        /* access modifiers changed from: package-private */
        public boolean isPrinter() {
            return this.iPrinters != null;
        }

        /* access modifiers changed from: package-private */
        public boolean isParser() {
            return this.iParsers != null;
        }

        private void decompose(List<Object> list, List<Object> list2, List<Object> list3) {
            int size = list.size();
            for (int i = 0; i < size; i += 2) {
                Object obj = list.get(i);
                if (obj instanceof DateTimePrinter) {
                    if (obj instanceof Composite) {
                        addArrayToList(list2, ((Composite) obj).iPrinters);
                    } else {
                        list2.add(obj);
                    }
                }
                Object obj2 = list.get(i + 1);
                if (obj2 instanceof DateTimeParser) {
                    if (obj2 instanceof Composite) {
                        addArrayToList(list3, ((Composite) obj2).iParsers);
                    } else {
                        list3.add(obj2);
                    }
                }
            }
        }

        private void addArrayToList(List<Object> list, Object[] objArr) {
            if (objArr != null) {
                for (Object add : objArr) {
                    list.add(add);
                }
            }
        }
    }

    static class MatchingParser implements DateTimeParser {
        private final int iParsedLengthEstimate;
        private final DateTimeParser[] iParsers;

        MatchingParser(DateTimeParser[] dateTimeParserArr) {
            int estimateParsedLength;
            this.iParsers = dateTimeParserArr;
            int i = 0;
            int length = dateTimeParserArr.length;
            while (true) {
                length--;
                if (length >= 0) {
                    DateTimeParser dateTimeParser = dateTimeParserArr[length];
                    if (dateTimeParser != null && (estimateParsedLength = dateTimeParser.estimateParsedLength()) > i) {
                        i = estimateParsedLength;
                    }
                } else {
                    this.iParsedLengthEstimate = i;
                    return;
                }
            }
        }

        public int estimateParsedLength() {
            return this.iParsedLengthEstimate;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:10:0x001e, code lost:
            if (r5 == null) goto L_0x0023;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:11:0x0020, code lost:
            r10.restoreState(r5);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:42:?, code lost:
            return r6;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:44:?, code lost:
            return r4 ^ -1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:7:0x0018, code lost:
            if (r6 > r12) goto L_0x001e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:8:0x001a, code lost:
            if (r6 != r12) goto L_0x0052;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:9:0x001c, code lost:
            if (r0 == false) goto L_0x0052;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public int parseInto(org.joda.time.format.DateTimeParserBucket r10, java.lang.String r11, int r12) {
            /*
                r9 = this;
                r8 = 0
                org.joda.time.format.DateTimeParser[] r0 = r9.iParsers
                int r1 = r0.length
                java.lang.Object r2 = r10.saveState()
                r3 = 0
                r4 = r12
                r5 = r3
                r6 = r12
                r3 = r8
            L_0x000d:
                if (r3 >= r1) goto L_0x0055
                r7 = r0[r3]
                if (r7 != 0) goto L_0x0025
                if (r6 > r12) goto L_0x0017
                r0 = r12
            L_0x0016:
                return r0
            L_0x0017:
                r0 = 1
            L_0x0018:
                if (r6 > r12) goto L_0x001e
                if (r6 != r12) goto L_0x0052
                if (r0 == 0) goto L_0x0052
            L_0x001e:
                if (r5 == 0) goto L_0x0023
                r10.restoreState(r5)
            L_0x0023:
                r0 = r6
                goto L_0x0016
            L_0x0025:
                int r7 = r7.parseInto(r10, r11, r12)
                if (r7 < r12) goto L_0x004a
                if (r7 <= r6) goto L_0x0044
                int r5 = r11.length()
                if (r7 >= r5) goto L_0x003d
                int r5 = r3 + 1
                if (r5 >= r1) goto L_0x003d
                int r5 = r3 + 1
                r5 = r0[r5]
                if (r5 != 0) goto L_0x003f
            L_0x003d:
                r0 = r7
                goto L_0x0016
            L_0x003f:
                java.lang.Object r5 = r10.saveState()
                r6 = r7
            L_0x0044:
                r10.restoreState(r2)
                int r3 = r3 + 1
                goto L_0x000d
            L_0x004a:
                if (r7 >= 0) goto L_0x0044
                r7 = r7 ^ -1
                if (r7 <= r4) goto L_0x0044
                r4 = r7
                goto L_0x0044
            L_0x0052:
                r0 = r4 ^ -1
                goto L_0x0016
            L_0x0055:
                r0 = r8
                goto L_0x0018
            */
            throw new UnsupportedOperationException("Method not decompiled: org.joda.time.format.DateTimeFormatterBuilder.MatchingParser.parseInto(org.joda.time.format.DateTimeParserBucket, java.lang.String, int):int");
        }
    }
}
