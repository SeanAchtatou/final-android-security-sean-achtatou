package org.joda.time.format;

import java.io.IOException;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import org.joda.time.Chronology;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.ReadablePartial;

public class DateTimeFormat {
    static final int DATE = 0;
    static final int DATETIME = 2;
    static final int FULL = 0;
    static final int LONG = 1;
    static final int MEDIUM = 2;
    static final int NONE = 4;
    static final int SHORT = 3;
    static final int TIME = 1;
    private static final Map<String, DateTimeFormatter> cPatternedCache = new HashMap(7);
    private static final DateTimeFormatter[] cStyleCache = new DateTimeFormatter[25];

    public static DateTimeFormatter forPattern(String str) {
        return createFormatterForPattern(str);
    }

    public static DateTimeFormatter forStyle(String str) {
        return createFormatterForStyle(str);
    }

    public static String patternForStyle(String str, Locale locale) {
        Locale locale2;
        DateTimeFormatter createFormatterForStyle = createFormatterForStyle(str);
        if (locale == null) {
            locale2 = Locale.getDefault();
        } else {
            locale2 = locale;
        }
        return ((StyleFormatter) createFormatterForStyle.getPrinter()).getPattern(locale2);
    }

    public static DateTimeFormatter shortDate() {
        return createFormatterForStyleIndex(3, 4);
    }

    public static DateTimeFormatter shortTime() {
        return createFormatterForStyleIndex(4, 3);
    }

    public static DateTimeFormatter shortDateTime() {
        return createFormatterForStyleIndex(3, 3);
    }

    public static DateTimeFormatter mediumDate() {
        return createFormatterForStyleIndex(2, 4);
    }

    public static DateTimeFormatter mediumTime() {
        return createFormatterForStyleIndex(4, 2);
    }

    public static DateTimeFormatter mediumDateTime() {
        return createFormatterForStyleIndex(2, 2);
    }

    public static DateTimeFormatter longDate() {
        return createFormatterForStyleIndex(1, 4);
    }

    public static DateTimeFormatter longTime() {
        return createFormatterForStyleIndex(4, 1);
    }

    public static DateTimeFormatter longDateTime() {
        return createFormatterForStyleIndex(1, 1);
    }

    public static DateTimeFormatter fullDate() {
        return createFormatterForStyleIndex(0, 4);
    }

    public static DateTimeFormatter fullTime() {
        return createFormatterForStyleIndex(4, 0);
    }

    public static DateTimeFormatter fullDateTime() {
        return createFormatterForStyleIndex(0, 0);
    }

    static void appendPatternTo(DateTimeFormatterBuilder dateTimeFormatterBuilder, String str) {
        parsePatternTo(dateTimeFormatterBuilder, str);
    }

    protected DateTimeFormat() {
    }

    private static void parsePatternTo(DateTimeFormatterBuilder dateTimeFormatterBuilder, String str) {
        int length = str.length();
        int[] iArr = new int[1];
        int i = 0;
        while (i < length) {
            iArr[0] = i;
            String parseToken = parseToken(str, iArr);
            int i2 = iArr[0];
            int length2 = parseToken.length();
            if (length2 != 0) {
                char charAt = parseToken.charAt(0);
                switch (charAt) {
                    case '\'':
                        String substring = parseToken.substring(1);
                        if (substring.length() != 1) {
                            dateTimeFormatterBuilder.appendLiteral(new String(substring));
                            break;
                        } else {
                            dateTimeFormatterBuilder.appendLiteral(substring.charAt(0));
                            break;
                        }
                    case 'C':
                        dateTimeFormatterBuilder.appendCenturyOfEra(length2, length2);
                        break;
                    case 'D':
                        dateTimeFormatterBuilder.appendDayOfYear(length2);
                        break;
                    case 'E':
                        if (length2 < 4) {
                            dateTimeFormatterBuilder.appendDayOfWeekShortText();
                            break;
                        } else {
                            dateTimeFormatterBuilder.appendDayOfWeekText();
                            break;
                        }
                    case 'G':
                        dateTimeFormatterBuilder.appendEraText();
                        break;
                    case 'H':
                        dateTimeFormatterBuilder.appendHourOfDay(length2);
                        break;
                    case 'K':
                        dateTimeFormatterBuilder.appendHourOfHalfday(length2);
                        break;
                    case 'M':
                        if (length2 >= 3) {
                            if (length2 < 4) {
                                dateTimeFormatterBuilder.appendMonthOfYearShortText();
                                break;
                            } else {
                                dateTimeFormatterBuilder.appendMonthOfYearText();
                                break;
                            }
                        } else {
                            dateTimeFormatterBuilder.appendMonthOfYear(length2);
                            break;
                        }
                    case 'S':
                        dateTimeFormatterBuilder.appendFractionOfSecond(length2, length2);
                        break;
                    case 'Y':
                    case 'x':
                    case 'y':
                        if (length2 == 2) {
                            boolean z = true;
                            if (i2 + 1 < length) {
                                iArr[0] = iArr[0] + 1;
                                if (isNumericToken(parseToken(str, iArr))) {
                                    z = false;
                                }
                                iArr[0] = iArr[0] - 1;
                            }
                            switch (charAt) {
                                case 'x':
                                    dateTimeFormatterBuilder.appendTwoDigitWeekyear(new DateTime().getWeekyear() - 30, z);
                                    continue;
                                default:
                                    dateTimeFormatterBuilder.appendTwoDigitYear(new DateTime().getYear() - 30, z);
                                    continue;
                            }
                        } else {
                            int i3 = 9;
                            if (i2 + 1 < length) {
                                iArr[0] = iArr[0] + 1;
                                if (isNumericToken(parseToken(str, iArr))) {
                                    i3 = length2;
                                }
                                iArr[0] = iArr[0] - 1;
                            }
                            switch (charAt) {
                                case 'Y':
                                    dateTimeFormatterBuilder.appendYearOfEra(length2, i3);
                                    continue;
                                case 'x':
                                    dateTimeFormatterBuilder.appendWeekyear(length2, i3);
                                    continue;
                                case 'y':
                                    dateTimeFormatterBuilder.appendYear(length2, i3);
                                    continue;
                            }
                        }
                    case 'Z':
                        if (length2 != 1) {
                            if (length2 != 2) {
                                dateTimeFormatterBuilder.appendTimeZoneId();
                                break;
                            } else {
                                dateTimeFormatterBuilder.appendTimeZoneOffset(null, "Z", true, 2, 2);
                                break;
                            }
                        } else {
                            dateTimeFormatterBuilder.appendTimeZoneOffset(null, "Z", false, 2, 2);
                            break;
                        }
                    case 'a':
                        dateTimeFormatterBuilder.appendHalfdayOfDayText();
                        break;
                    case 'd':
                        dateTimeFormatterBuilder.appendDayOfMonth(length2);
                        break;
                    case 'e':
                        dateTimeFormatterBuilder.appendDayOfWeek(length2);
                        break;
                    case 'h':
                        dateTimeFormatterBuilder.appendClockhourOfHalfday(length2);
                        break;
                    case 'k':
                        dateTimeFormatterBuilder.appendClockhourOfDay(length2);
                        break;
                    case 'm':
                        dateTimeFormatterBuilder.appendMinuteOfHour(length2);
                        break;
                    case 's':
                        dateTimeFormatterBuilder.appendSecondOfMinute(length2);
                        break;
                    case 'w':
                        dateTimeFormatterBuilder.appendWeekOfWeekyear(length2);
                        break;
                    case 'z':
                        if (length2 < 4) {
                            dateTimeFormatterBuilder.appendTimeZoneShortName();
                            break;
                        } else {
                            dateTimeFormatterBuilder.appendTimeZoneName();
                            break;
                        }
                    default:
                        throw new IllegalArgumentException("Illegal pattern component: " + parseToken);
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:31:0x006a, code lost:
        r1 = r3 - 1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String parseToken(java.lang.String r11, int[] r12) {
        /*
            r10 = 97
            r9 = 90
            r8 = 65
            r7 = 39
            r6 = 0
            java.lang.StringBuffer r0 = new java.lang.StringBuffer
            r0.<init>()
            r1 = r12[r6]
            int r2 = r11.length()
            char r3 = r11.charAt(r1)
            if (r3 < r8) goto L_0x001c
            if (r3 <= r9) goto L_0x0022
        L_0x001c:
            if (r3 < r10) goto L_0x0037
            r4 = 122(0x7a, float:1.71E-43)
            if (r3 > r4) goto L_0x0037
        L_0x0022:
            r0.append(r3)
        L_0x0025:
            int r4 = r1 + 1
            if (r4 >= r2) goto L_0x006c
            int r4 = r1 + 1
            char r4 = r11.charAt(r4)
            if (r4 != r3) goto L_0x006c
            r0.append(r3)
            int r1 = r1 + 1
            goto L_0x0025
        L_0x0037:
            r0.append(r7)
            r3 = r1
            r1 = r6
        L_0x003c:
            if (r3 >= r2) goto L_0x0077
            char r4 = r11.charAt(r3)
            if (r4 != r7) goto L_0x005e
            int r5 = r3 + 1
            if (r5 >= r2) goto L_0x0058
            int r5 = r3 + 1
            char r5 = r11.charAt(r5)
            if (r5 != r7) goto L_0x0058
            int r3 = r3 + 1
            r0.append(r4)
        L_0x0055:
            int r3 = r3 + 1
            goto L_0x003c
        L_0x0058:
            if (r1 != 0) goto L_0x005c
            r1 = 1
            goto L_0x0055
        L_0x005c:
            r1 = r6
            goto L_0x0055
        L_0x005e:
            if (r1 != 0) goto L_0x0073
            if (r4 < r8) goto L_0x0064
            if (r4 <= r9) goto L_0x006a
        L_0x0064:
            if (r4 < r10) goto L_0x0073
            r5 = 122(0x7a, float:1.71E-43)
            if (r4 > r5) goto L_0x0073
        L_0x006a:
            int r1 = r3 + -1
        L_0x006c:
            r12[r6] = r1
            java.lang.String r0 = r0.toString()
            return r0
        L_0x0073:
            r0.append(r4)
            goto L_0x0055
        L_0x0077:
            r1 = r3
            goto L_0x006c
        */
        throw new UnsupportedOperationException("Method not decompiled: org.joda.time.format.DateTimeFormat.parseToken(java.lang.String, int[]):java.lang.String");
    }

    private static boolean isNumericToken(String str) {
        int length = str.length();
        if (length > 0) {
            switch (str.charAt(0)) {
                case 'C':
                case 'D':
                case 'F':
                case 'H':
                case 'K':
                case 'S':
                case 'W':
                case 'Y':
                case 'c':
                case 'd':
                case 'e':
                case 'h':
                case 'k':
                case 'm':
                case 's':
                case 'w':
                case 'x':
                case 'y':
                    return true;
                case 'M':
                    if (length <= 2) {
                        return true;
                    }
                    break;
            }
        }
        return false;
    }

    private static DateTimeFormatter createFormatterForPattern(String str) {
        DateTimeFormatter dateTimeFormatter;
        if (str == null || str.length() == 0) {
            throw new IllegalArgumentException("Invalid pattern specification");
        }
        synchronized (cPatternedCache) {
            dateTimeFormatter = cPatternedCache.get(str);
            if (dateTimeFormatter == null) {
                DateTimeFormatterBuilder dateTimeFormatterBuilder = new DateTimeFormatterBuilder();
                parsePatternTo(dateTimeFormatterBuilder, str);
                dateTimeFormatter = dateTimeFormatterBuilder.toFormatter();
                cPatternedCache.put(str, dateTimeFormatter);
            }
        }
        return dateTimeFormatter;
    }

    private static DateTimeFormatter createFormatterForStyle(String str) {
        if (str == null || str.length() != 2) {
            throw new IllegalArgumentException("Invalid style specification: " + str);
        }
        int selectStyle = selectStyle(str.charAt(0));
        int selectStyle2 = selectStyle(str.charAt(1));
        if (selectStyle != 4 || selectStyle2 != 4) {
            return createFormatterForStyleIndex(selectStyle, selectStyle2);
        }
        throw new IllegalArgumentException("Style '--' is invalid");
    }

    private static DateTimeFormatter createFormatterForStyleIndex(int i, int i2) {
        DateTimeFormatter dateTimeFormatter;
        int i3 = (i << 2) + i + i2;
        synchronized (cStyleCache) {
            DateTimeFormatter dateTimeFormatter2 = cStyleCache[i3];
            if (dateTimeFormatter2 == null) {
                int i4 = 2;
                if (i == 4) {
                    i4 = 1;
                } else if (i2 == 4) {
                    i4 = 0;
                }
                StyleFormatter styleFormatter = new StyleFormatter(i, i2, i4);
                DateTimeFormatter dateTimeFormatter3 = new DateTimeFormatter(styleFormatter, styleFormatter);
                cStyleCache[i3] = dateTimeFormatter3;
                dateTimeFormatter = dateTimeFormatter3;
            } else {
                dateTimeFormatter = dateTimeFormatter2;
            }
        }
        return dateTimeFormatter;
    }

    private static int selectStyle(char c) {
        switch (c) {
            case '-':
                return 4;
            case 'F':
                return 0;
            case 'L':
                return 1;
            case 'M':
                return 2;
            case 'S':
                return 3;
            default:
                throw new IllegalArgumentException("Invalid style character: " + c);
        }
    }

    static class StyleFormatter implements DateTimePrinter, DateTimeParser {
        private static final Map<String, DateTimeFormatter> cCache = new HashMap();
        private final int iDateStyle;
        private final int iTimeStyle;
        private final int iType;

        StyleFormatter(int i, int i2, int i3) {
            this.iDateStyle = i;
            this.iTimeStyle = i2;
            this.iType = i3;
        }

        public int estimatePrintedLength() {
            return 40;
        }

        public void printTo(StringBuffer stringBuffer, long j, Chronology chronology, int i, DateTimeZone dateTimeZone, Locale locale) {
            getFormatter(locale).getPrinter().printTo(stringBuffer, j, chronology, i, dateTimeZone, locale);
        }

        public void printTo(Writer writer, long j, Chronology chronology, int i, DateTimeZone dateTimeZone, Locale locale) throws IOException {
            getFormatter(locale).getPrinter().printTo(writer, j, chronology, i, dateTimeZone, locale);
        }

        public void printTo(StringBuffer stringBuffer, ReadablePartial readablePartial, Locale locale) {
            getFormatter(locale).getPrinter().printTo(stringBuffer, readablePartial, locale);
        }

        public void printTo(Writer writer, ReadablePartial readablePartial, Locale locale) throws IOException {
            getFormatter(locale).getPrinter().printTo(writer, readablePartial, locale);
        }

        public int estimateParsedLength() {
            return 40;
        }

        public int parseInto(DateTimeParserBucket dateTimeParserBucket, String str, int i) {
            return getFormatter(dateTimeParserBucket.getLocale()).getParser().parseInto(dateTimeParserBucket, str, i);
        }

        private DateTimeFormatter getFormatter(Locale locale) {
            DateTimeFormatter dateTimeFormatter;
            Locale locale2 = locale == null ? Locale.getDefault() : locale;
            String str = Integer.toString(this.iType + (this.iDateStyle << 4) + (this.iTimeStyle << 8)) + locale2.toString();
            synchronized (cCache) {
                dateTimeFormatter = cCache.get(str);
                if (dateTimeFormatter == null) {
                    dateTimeFormatter = DateTimeFormat.forPattern(getPattern(locale2));
                    cCache.put(str, dateTimeFormatter);
                }
            }
            return dateTimeFormatter;
        }

        /* access modifiers changed from: package-private */
        public String getPattern(Locale locale) {
            DateFormat dateTimeInstance;
            switch (this.iType) {
                case 0:
                    dateTimeInstance = DateFormat.getDateInstance(this.iDateStyle, locale);
                    break;
                case 1:
                    dateTimeInstance = DateFormat.getTimeInstance(this.iTimeStyle, locale);
                    break;
                case 2:
                    dateTimeInstance = DateFormat.getDateTimeInstance(this.iDateStyle, this.iTimeStyle, locale);
                    break;
                default:
                    dateTimeInstance = null;
                    break;
            }
            if (dateTimeInstance instanceof SimpleDateFormat) {
                return ((SimpleDateFormat) dateTimeInstance).toPattern();
            }
            throw new IllegalArgumentException("No datetime pattern for locale: " + locale);
        }
    }
}
