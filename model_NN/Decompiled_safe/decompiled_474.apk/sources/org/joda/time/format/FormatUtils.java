package org.joda.time.format;

import java.io.IOException;
import java.io.Writer;

public class FormatUtils {
    private static final double LOG_10 = Math.log(10.0d);

    private FormatUtils() {
    }

    public static void appendPaddedInteger(StringBuffer stringBuffer, int i, int i2) {
        int i3;
        int i4;
        int i5;
        if (i < 0) {
            stringBuffer.append('-');
            if (i != Integer.MIN_VALUE) {
                i3 = -i;
            } else {
                for (int i6 = i2; i6 > 10; i6--) {
                    stringBuffer.append('0');
                }
                stringBuffer.append("2147483648");
                return;
            }
        } else {
            i3 = i;
        }
        if (i3 < 10) {
            for (int i7 = i2; i7 > 1; i7--) {
                stringBuffer.append('0');
            }
            stringBuffer.append((char) (i3 + 48));
        } else if (i3 < 100) {
            for (int i8 = i2; i8 > 2; i8--) {
                stringBuffer.append('0');
            }
            int i9 = ((i3 + 1) * 13421772) >> 27;
            stringBuffer.append((char) (i9 + 48));
            stringBuffer.append((char) (((i3 - (i9 << 3)) - (i9 << 1)) + 48));
        } else {
            if (i3 < 1000) {
                i5 = 3;
                i4 = i2;
            } else if (i3 < 10000) {
                i5 = 4;
                i4 = i2;
            } else {
                i5 = ((int) (Math.log((double) i3) / LOG_10)) + 1;
                i4 = i2;
            }
            while (i4 > i5) {
                stringBuffer.append('0');
                i4--;
            }
            stringBuffer.append(Integer.toString(i3));
        }
    }

    public static void appendPaddedInteger(StringBuffer stringBuffer, long j, int i) {
        long j2;
        int i2 = (int) j;
        if (((long) i2) == j) {
            appendPaddedInteger(stringBuffer, i2, i);
        } else if (i <= 19) {
            stringBuffer.append(Long.toString(j));
        } else {
            if (j < 0) {
                stringBuffer.append('-');
                if (j != Long.MIN_VALUE) {
                    j2 = -j;
                } else {
                    for (int i3 = i; i3 > 19; i3--) {
                        stringBuffer.append('0');
                    }
                    stringBuffer.append("9223372036854775808");
                    return;
                }
            } else {
                j2 = j;
            }
            int log = ((int) (Math.log((double) j2) / LOG_10)) + 1;
            for (int i4 = i; i4 > log; i4--) {
                stringBuffer.append('0');
            }
            stringBuffer.append(Long.toString(j2));
        }
    }

    public static void writePaddedInteger(Writer writer, int i, int i2) throws IOException {
        int i3;
        int i4;
        int i5;
        if (i < 0) {
            writer.write(45);
            if (i != Integer.MIN_VALUE) {
                i3 = -i;
            } else {
                for (int i6 = i2; i6 > 10; i6--) {
                    writer.write(48);
                }
                writer.write("2147483648");
                return;
            }
        } else {
            i3 = i;
        }
        if (i3 < 10) {
            for (int i7 = i2; i7 > 1; i7--) {
                writer.write(48);
            }
            writer.write(i3 + 48);
        } else if (i3 < 100) {
            for (int i8 = i2; i8 > 2; i8--) {
                writer.write(48);
            }
            int i9 = ((i3 + 1) * 13421772) >> 27;
            writer.write(i9 + 48);
            writer.write(((i3 - (i9 << 3)) - (i9 << 1)) + 48);
        } else {
            if (i3 < 1000) {
                i5 = 3;
                i4 = i2;
            } else if (i3 < 10000) {
                i5 = 4;
                i4 = i2;
            } else {
                i5 = ((int) (Math.log((double) i3) / LOG_10)) + 1;
                i4 = i2;
            }
            while (i4 > i5) {
                writer.write(48);
                i4--;
            }
            writer.write(Integer.toString(i3));
        }
    }

    public static void writePaddedInteger(Writer writer, long j, int i) throws IOException {
        long j2;
        int i2 = (int) j;
        if (((long) i2) == j) {
            writePaddedInteger(writer, i2, i);
        } else if (i <= 19) {
            writer.write(Long.toString(j));
        } else {
            if (j < 0) {
                writer.write(45);
                if (j != Long.MIN_VALUE) {
                    j2 = -j;
                } else {
                    for (int i3 = i; i3 > 19; i3--) {
                        writer.write(48);
                    }
                    writer.write("9223372036854775808");
                    return;
                }
            } else {
                j2 = j;
            }
            int log = ((int) (Math.log((double) j2) / LOG_10)) + 1;
            for (int i4 = i; i4 > log; i4--) {
                writer.write(48);
            }
            writer.write(Long.toString(j2));
        }
    }

    public static void appendUnpaddedInteger(StringBuffer stringBuffer, int i) {
        int i2;
        if (i < 0) {
            stringBuffer.append('-');
            if (i != Integer.MIN_VALUE) {
                i2 = -i;
            } else {
                stringBuffer.append("2147483648");
                return;
            }
        } else {
            i2 = i;
        }
        if (i2 < 10) {
            stringBuffer.append((char) (i2 + 48));
        } else if (i2 < 100) {
            int i3 = ((i2 + 1) * 13421772) >> 27;
            stringBuffer.append((char) (i3 + 48));
            stringBuffer.append((char) (((i2 - (i3 << 3)) - (i3 << 1)) + 48));
        } else {
            stringBuffer.append(Integer.toString(i2));
        }
    }

    public static void appendUnpaddedInteger(StringBuffer stringBuffer, long j) {
        int i = (int) j;
        if (((long) i) == j) {
            appendUnpaddedInteger(stringBuffer, i);
        } else {
            stringBuffer.append(Long.toString(j));
        }
    }

    public static void writeUnpaddedInteger(Writer writer, int i) throws IOException {
        int i2;
        if (i < 0) {
            writer.write(45);
            if (i != Integer.MIN_VALUE) {
                i2 = -i;
            } else {
                writer.write("2147483648");
                return;
            }
        } else {
            i2 = i;
        }
        if (i2 < 10) {
            writer.write(i2 + 48);
        } else if (i2 < 100) {
            int i3 = ((i2 + 1) * 13421772) >> 27;
            writer.write(i3 + 48);
            writer.write(((i2 - (i3 << 3)) - (i3 << 1)) + 48);
        } else {
            writer.write(Integer.toString(i2));
        }
    }

    public static void writeUnpaddedInteger(Writer writer, long j) throws IOException {
        int i = (int) j;
        if (((long) i) == j) {
            writeUnpaddedInteger(writer, i);
        } else {
            writer.write(Long.toString(j));
        }
    }

    public static int calculateDigitCount(long j) {
        if (j < 0) {
            if (j != Long.MIN_VALUE) {
                return calculateDigitCount(-j) + 1;
            }
            return 20;
        } else if (j < 10) {
            return 1;
        } else {
            if (j < 100) {
                return 2;
            }
            if (j < 1000) {
                return 3;
            }
            if (j < 10000) {
                return 4;
            }
            return ((int) (Math.log((double) j) / LOG_10)) + 1;
        }
    }

    static int parseTwoDigits(String str, int i) {
        int charAt = str.charAt(i) - '0';
        return (((charAt << 1) + (charAt << 3)) + str.charAt(i + 1)) - 48;
    }

    static String createErrorMessage(String str, int i) {
        String concat;
        int i2 = i + 32;
        if (str.length() <= i2 + 3) {
            concat = str;
        } else {
            concat = str.substring(0, i2).concat("...");
        }
        if (i <= 0) {
            return "Invalid format: \"" + concat + '\"';
        }
        if (i >= str.length()) {
            return "Invalid format: \"" + concat + "\" is too short";
        }
        return "Invalid format: \"" + concat + "\" is malformed at \"" + concat.substring(i) + '\"';
    }
}
