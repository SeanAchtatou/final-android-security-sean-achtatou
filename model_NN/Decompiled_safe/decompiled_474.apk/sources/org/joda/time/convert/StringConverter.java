package org.joda.time.convert;

import org.joda.time.Chronology;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.Period;
import org.joda.time.ReadWritableInterval;
import org.joda.time.ReadWritablePeriod;
import org.joda.time.ReadablePartial;
import org.joda.time.field.FieldUtils;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.joda.time.format.ISOPeriodFormat;
import org.joda.time.format.PeriodFormatter;

class StringConverter extends AbstractConverter implements InstantConverter, PartialConverter, DurationConverter, PeriodConverter, IntervalConverter {
    static final StringConverter INSTANCE = new StringConverter();

    protected StringConverter() {
    }

    public long getInstantMillis(Object obj, Chronology chronology) {
        return ISODateTimeFormat.dateTimeParser().withChronology(chronology).parseMillis((String) obj);
    }

    public int[] getPartialValues(ReadablePartial readablePartial, Object obj, Chronology chronology, DateTimeFormatter dateTimeFormatter) {
        Chronology chronology2;
        if (dateTimeFormatter.getZone() != null) {
            chronology2 = chronology.withZone(dateTimeFormatter.getZone());
        } else {
            chronology2 = chronology;
        }
        return chronology2.get(readablePartial, dateTimeFormatter.withChronology(chronology2).parseMillis((String) obj));
    }

    public long getDurationMillis(Object obj) {
        long parseLong;
        long j;
        String str = (String) obj;
        int length = str.length();
        if (length < 4 || !((str.charAt(0) == 'P' || str.charAt(0) == 'p') && ((str.charAt(1) == 'T' || str.charAt(1) == 't') && (str.charAt(length - 1) == 'S' || str.charAt(length - 1) == 's')))) {
            throw new IllegalArgumentException("Invalid format: \"" + str + '\"');
        }
        String substring = str.substring(2, length - 1);
        int i = -1;
        for (int i2 = 0; i2 < substring.length(); i2++) {
            if ((substring.charAt(i2) < '0' || substring.charAt(i2) > '9') && !(i2 == 0 && substring.charAt(0) == '-')) {
                if (i2 > 0 && substring.charAt(i2) == '.' && i == -1) {
                    i = i2;
                } else {
                    throw new IllegalArgumentException("Invalid format: \"" + str + '\"');
                }
            }
        }
        if (i > 0) {
            long parseLong2 = Long.parseLong(substring.substring(0, i));
            String substring2 = substring.substring(i + 1);
            if (substring2.length() != 3) {
                substring2 = (substring2 + "000").substring(0, 3);
            }
            j = (long) Integer.parseInt(substring2);
            parseLong = parseLong2;
        } else {
            parseLong = Long.parseLong(substring);
            j = 0;
        }
        if (parseLong < 0) {
            return FieldUtils.safeAdd(FieldUtils.safeMultiply(parseLong, (int) DateTimeConstants.MILLIS_PER_SECOND), -j);
        }
        return FieldUtils.safeAdd(FieldUtils.safeMultiply(parseLong, (int) DateTimeConstants.MILLIS_PER_SECOND), j);
    }

    public void setInto(ReadWritablePeriod readWritablePeriod, Object obj, Chronology chronology) {
        String str = (String) obj;
        PeriodFormatter standard = ISOPeriodFormat.standard();
        readWritablePeriod.clear();
        int parseInto = standard.parseInto(readWritablePeriod, str, 0);
        if (parseInto < str.length()) {
            if (parseInto < 0) {
                standard.withParseType(readWritablePeriod.getPeriodType()).parseMutablePeriod(str);
            }
            throw new IllegalArgumentException("Invalid format: \"" + str + '\"');
        }
    }

    public void setInto(ReadWritableInterval readWritableInterval, Object obj, Chronology chronology) {
        long j;
        Period period;
        Chronology chronology2;
        Chronology chronology3;
        long j2;
        long j3;
        String str = (String) obj;
        int indexOf = str.indexOf(47);
        if (indexOf < 0) {
            throw new IllegalArgumentException("Format requires a '/' separator: " + str);
        }
        String substring = str.substring(0, indexOf);
        if (substring.length() <= 0) {
            throw new IllegalArgumentException("Format invalid: " + str);
        }
        String substring2 = str.substring(indexOf + 1);
        if (substring2.length() <= 0) {
            throw new IllegalArgumentException("Format invalid: " + str);
        }
        DateTimeFormatter withChronology = ISODateTimeFormat.dateTimeParser().withChronology(chronology);
        PeriodFormatter standard = ISOPeriodFormat.standard();
        char charAt = substring.charAt(0);
        if (charAt == 'P' || charAt == 'p') {
            Period parsePeriod = standard.withParseType(getPeriodType(substring)).parsePeriod(substring);
            chronology2 = null;
            j = 0;
            period = parsePeriod;
        } else {
            DateTime parseDateTime = withChronology.parseDateTime(substring);
            long millis = parseDateTime.getMillis();
            chronology2 = parseDateTime.getChronology();
            j = millis;
            period = null;
        }
        char charAt2 = substring2.charAt(0);
        if (charAt2 != 'P' && charAt2 != 'p') {
            DateTime parseDateTime2 = withChronology.parseDateTime(substring2);
            long millis2 = parseDateTime2.getMillis();
            Chronology chronology4 = chronology2 != null ? chronology2 : parseDateTime2.getChronology();
            if (chronology != null) {
                chronology4 = chronology;
            }
            if (period != null) {
                long j4 = millis2;
                j2 = chronology4.add(period, millis2, -1);
                chronology3 = chronology4;
                j3 = j4;
            } else {
                chronology3 = chronology4;
                j3 = millis2;
                j2 = j;
            }
        } else if (period != null) {
            throw new IllegalArgumentException("Interval composed of two durations: " + str);
        } else {
            Period parsePeriod2 = standard.withParseType(getPeriodType(substring2)).parsePeriod(substring2);
            if (chronology != null) {
                chronology2 = chronology;
            }
            chronology3 = chronology2;
            j3 = chronology2.add(parsePeriod2, j, 1);
            j2 = j;
        }
        readWritableInterval.setInterval(j2, j3);
        readWritableInterval.setChronology(chronology3);
    }

    public Class<?> getSupportedType() {
        return String.class;
    }
}
