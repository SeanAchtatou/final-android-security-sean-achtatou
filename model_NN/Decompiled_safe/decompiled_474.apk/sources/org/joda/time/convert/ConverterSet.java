package org.joda.time.convert;

class ConverterSet {
    private final Converter[] iConverters;
    private Entry[] iSelectEntries = new Entry[16];

    ConverterSet(Converter[] converterArr) {
        this.iConverters = converterArr;
    }

    /* access modifiers changed from: package-private */
    public Converter select(Class<?> cls) throws IllegalStateException {
        Entry[] entryArr = this.iSelectEntries;
        int length = entryArr.length;
        int hashCode = cls == null ? 0 : cls.hashCode() & (length - 1);
        while (true) {
            Entry entry = entryArr[hashCode];
            if (entry == null) {
                Converter selectSlow = selectSlow(this, cls);
                Entry entry2 = new Entry(cls, selectSlow);
                Entry[] entryArr2 = (Entry[]) entryArr.clone();
                entryArr2[hashCode] = entry2;
                for (int i = 0; i < length; i++) {
                    if (entryArr2[i] == null) {
                        this.iSelectEntries = entryArr2;
                        return selectSlow;
                    }
                }
                int i2 = length << 1;
                Entry[] entryArr3 = new Entry[i2];
                for (int i3 = 0; i3 < length; i3++) {
                    Entry entry3 = entryArr2[i3];
                    Class<?> cls2 = entry3.iType;
                    int hashCode2 = cls2 == null ? 0 : cls2.hashCode() & (i2 - 1);
                    while (entryArr3[hashCode2] != null) {
                        int i4 = hashCode2 + 1;
                        if (i4 >= i2) {
                            i4 = 0;
                        }
                    }
                    entryArr3[hashCode2] = entry3;
                }
                this.iSelectEntries = entryArr3;
                return selectSlow;
            } else if (entry.iType == cls) {
                return entry.iConverter;
            } else {
                int i5 = hashCode + 1;
                if (i5 >= length) {
                    i5 = 0;
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public int size() {
        return this.iConverters.length;
    }

    /* access modifiers changed from: package-private */
    public void copyInto(Converter[] converterArr) {
        System.arraycopy(this.iConverters, 0, converterArr, 0, this.iConverters.length);
    }

    /* access modifiers changed from: package-private */
    public ConverterSet add(Converter converter, Converter[] converterArr) {
        Converter[] converterArr2 = this.iConverters;
        int length = converterArr2.length;
        int i = 0;
        while (i < length) {
            Converter converter2 = converterArr2[i];
            if (converter.equals(converter2)) {
                if (converterArr != null) {
                    converterArr[0] = null;
                }
                return this;
            } else if (converter.getSupportedType() == converter2.getSupportedType()) {
                Converter[] converterArr3 = new Converter[length];
                for (int i2 = 0; i2 < length; i2++) {
                    if (i2 != i) {
                        converterArr3[i2] = converterArr2[i2];
                    } else {
                        converterArr3[i2] = converter;
                    }
                }
                if (converterArr != null) {
                    converterArr[0] = converter2;
                }
                return new ConverterSet(converterArr3);
            } else {
                i++;
            }
        }
        Converter[] converterArr4 = new Converter[(length + 1)];
        System.arraycopy(converterArr2, 0, converterArr4, 0, length);
        converterArr4[length] = converter;
        if (converterArr != null) {
            converterArr[0] = null;
        }
        return new ConverterSet(converterArr4);
    }

    /* access modifiers changed from: package-private */
    public ConverterSet remove(Converter converter, Converter[] converterArr) {
        Converter[] converterArr2 = this.iConverters;
        int length = converterArr2.length;
        for (int i = 0; i < length; i++) {
            if (converter.equals(converterArr2[i])) {
                return remove(i, converterArr);
            }
        }
        if (converterArr != null) {
            converterArr[0] = null;
        }
        return this;
    }

    /* access modifiers changed from: package-private */
    public ConverterSet remove(int i, Converter[] converterArr) {
        Converter[] converterArr2 = this.iConverters;
        int length = converterArr2.length;
        if (i >= length) {
            throw new IndexOutOfBoundsException();
        }
        if (converterArr != null) {
            converterArr[0] = converterArr2[i];
        }
        Converter[] converterArr3 = new Converter[(length - 1)];
        int i2 = 0;
        for (int i3 = 0; i3 < length; i3++) {
            if (i3 != i) {
                converterArr3[i2] = converterArr2[i3];
                i2++;
            }
        }
        return new ConverterSet(converterArr3);
    }

    private static Converter selectSlow(ConverterSet converterSet, Class<?> cls) {
        Converter[] converterArr = converterSet.iConverters;
        int length = converterArr.length;
        Converter[] converterArr2 = converterArr;
        ConverterSet converterSet2 = converterSet;
        int i = length;
        while (true) {
            i--;
            if (i >= 0) {
                Converter converter = converterArr2[i];
                Class<?> supportedType = converter.getSupportedType();
                if (supportedType == cls) {
                    return converter;
                }
                if (supportedType == null || (cls != null && !supportedType.isAssignableFrom(cls))) {
                    ConverterSet remove = converterSet2.remove(i, (Converter[]) null);
                    converterArr2 = remove.iConverters;
                    converterSet2 = remove;
                    length = converterArr2.length;
                }
            } else if (cls == null || length == 0) {
                return null;
            } else {
                if (length == 1) {
                    return converterArr2[0];
                }
                int i2 = length;
                while (true) {
                    int i3 = i2 - 1;
                    if (i3 < 0) {
                        break;
                    }
                    Class<?> supportedType2 = converterArr2[i3].getSupportedType();
                    ConverterSet converterSet3 = converterSet2;
                    Converter[] converterArr3 = converterArr2;
                    int i4 = length;
                    int i5 = i3;
                    int i6 = length;
                    int i7 = i5;
                    while (true) {
                        i6--;
                        if (i6 < 0) {
                            break;
                        } else if (i6 != i7 && converterArr3[i6].getSupportedType().isAssignableFrom(supportedType2)) {
                            ConverterSet remove2 = converterSet3.remove(i6, (Converter[]) null);
                            Converter[] converterArr4 = remove2.iConverters;
                            int length2 = converterArr4.length;
                            converterSet3 = remove2;
                            i7 = length2 - 1;
                            Converter[] converterArr5 = converterArr4;
                            i4 = length2;
                            converterArr3 = converterArr5;
                        }
                    }
                    i2 = i7;
                    length = i4;
                    converterArr2 = converterArr3;
                    converterSet2 = converterSet3;
                }
                if (length == 1) {
                    return converterArr2[0];
                }
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append("Unable to find best converter for type \"");
                stringBuffer.append(cls.getName());
                stringBuffer.append("\" from remaining set: ");
                for (int i8 = 0; i8 < length; i8++) {
                    Converter converter2 = converterArr2[i8];
                    Class<?> supportedType3 = converter2.getSupportedType();
                    stringBuffer.append(converter2.getClass().getName());
                    stringBuffer.append('[');
                    stringBuffer.append(supportedType3 == null ? null : supportedType3.getName());
                    stringBuffer.append("], ");
                }
                throw new IllegalStateException(stringBuffer.toString());
            }
        }
    }

    static class Entry {
        final Converter iConverter;
        final Class<?> iType;

        Entry(Class<?> cls, Converter converter) {
            this.iType = cls;
            this.iConverter = converter;
        }
    }
}
