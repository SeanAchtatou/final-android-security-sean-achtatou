package org.joda.time.field;

import org.joda.time.DateTimeField;

public class StrictDateTimeField extends DelegatedDateTimeField {
    private static final long serialVersionUID = 3154803964207950910L;

    public static DateTimeField getInstance(DateTimeField dateTimeField) {
        DateTimeField dateTimeField2;
        if (dateTimeField == null) {
            return null;
        }
        if (dateTimeField instanceof LenientDateTimeField) {
            dateTimeField2 = ((LenientDateTimeField) dateTimeField).getWrappedField();
        } else {
            dateTimeField2 = dateTimeField;
        }
        return dateTimeField2.isLenient() ? new StrictDateTimeField(dateTimeField2) : dateTimeField2;
    }

    protected StrictDateTimeField(DateTimeField dateTimeField) {
        super(dateTimeField);
    }

    public final boolean isLenient() {
        return false;
    }

    public long set(long j, int i) {
        FieldUtils.verifyValueBounds(this, i, getMinimumValue(j), getMaximumValue(j));
        return super.set(j, i);
    }
}
