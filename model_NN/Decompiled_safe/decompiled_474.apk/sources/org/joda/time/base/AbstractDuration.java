package org.joda.time.base;

import org.joda.convert.ToString;
import org.joda.time.Duration;
import org.joda.time.Period;
import org.joda.time.ReadableDuration;
import org.joda.time.format.FormatUtils;

public abstract class AbstractDuration implements ReadableDuration {
    protected AbstractDuration() {
    }

    public Duration toDuration() {
        return new Duration(getMillis());
    }

    public Period toPeriod() {
        return new Period(getMillis());
    }

    public int compareTo(ReadableDuration readableDuration) {
        long millis = getMillis();
        long millis2 = readableDuration.getMillis();
        if (millis < millis2) {
            return -1;
        }
        if (millis > millis2) {
            return 1;
        }
        return 0;
    }

    public boolean isEqual(ReadableDuration readableDuration) {
        ReadableDuration readableDuration2;
        if (readableDuration == null) {
            readableDuration2 = Duration.ZERO;
        } else {
            readableDuration2 = readableDuration;
        }
        return compareTo(readableDuration2) == 0;
    }

    public boolean isLongerThan(ReadableDuration readableDuration) {
        ReadableDuration readableDuration2;
        if (readableDuration == null) {
            readableDuration2 = Duration.ZERO;
        } else {
            readableDuration2 = readableDuration;
        }
        return compareTo(readableDuration2) > 0;
    }

    public boolean isShorterThan(ReadableDuration readableDuration) {
        ReadableDuration readableDuration2;
        if (readableDuration == null) {
            readableDuration2 = Duration.ZERO;
        } else {
            readableDuration2 = readableDuration;
        }
        return compareTo(readableDuration2) < 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ReadableDuration)) {
            return false;
        }
        return getMillis() == ((ReadableDuration) obj).getMillis();
    }

    public int hashCode() {
        long millis = getMillis();
        return (int) (millis ^ (millis >>> 32));
    }

    @ToString
    public String toString() {
        long millis = getMillis();
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("PT");
        boolean z = millis < 0;
        FormatUtils.appendUnpaddedInteger(stringBuffer, millis);
        while (true) {
            if (stringBuffer.length() >= (z ? 7 : 6)) {
                break;
            }
            stringBuffer.insert(z ? 3 : 2, "0");
        }
        if ((millis / 1000) * 1000 == millis) {
            stringBuffer.setLength(stringBuffer.length() - 3);
        } else {
            stringBuffer.insert(stringBuffer.length() - 3, ".");
        }
        stringBuffer.append('S');
        return stringBuffer.toString();
    }
}
