package com.omgbutton.anotherclockwidget;

import android.app.PendingIntent;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.IBinder;
import android.text.format.DateFormat;
import android.widget.RemoteViews;
import java.text.DateFormatSymbols;
import java.util.Calendar;

public class ClockWidget extends AppWidgetProvider {
    public void onDisabled(Context context) {
        super.onDisabled(context);
        context.stopService(new Intent(context, UpdateService.class));
    }

    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        if ("android.appwidget.action.APPWIDGET_UPDATE".equals(intent.getAction())) {
            RemoteViews views = new RemoteViews(context.getPackageName(), (int) R.layout.widget);
            views.setOnClickPendingIntent(R.id.time, PendingIntent.getActivity(context, 0, new Intent("android.intent.action.MAIN").addCategory("android.intent.category.LAUNCHER").setComponent(new ComponentName("com.android.alarmclock", "com.android.alarmclock.AlarmClock")), 0));
            AppWidgetManager.getInstance(context).updateAppWidget(intent.getIntArrayExtra("appWidgetIds"), views);
        }
    }

    public void onEnabled(Context context) {
        super.onEnabled(context);
        context.startService(new Intent("com.omgbutton.anotherclockwidget.action.UPDATE"));
    }

    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        super.onUpdate(context, appWidgetManager, appWidgetIds);
        context.startService(new Intent("com.omgbutton.anotherclockwidget.action.UPDATE"));
    }

    public static final class UpdateService extends Service {
        static final String ACTION_UPDATE = "com.omgbutton.anotherclockwidget.action.UPDATE";
        private static final String FORMAT_12_HOURS = "hh:mm";
        private static final String FORMAT_24_HOURS = "kk:mm";
        private static final IntentFilter sIntentFilter = new IntentFilter();
        private String amText;
        private String hoursFormat;
        private Calendar mCalendar;
        private final BroadcastReceiver mTimeChangedReceiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if (action.equals("android.intent.action.TIME_SET") || action.equals("android.intent.action.TIMEZONE_CHANGED")) {
                    UpdateService.this.setParams();
                }
                UpdateService.this.update();
            }
        };
        private String pmText;

        static {
            sIntentFilter.addAction("android.intent.action.TIME_TICK");
            sIntentFilter.addAction("android.intent.action.TIMEZONE_CHANGED");
            sIntentFilter.addAction("android.intent.action.TIME_SET");
        }

        public void onCreate() {
            super.onCreate();
            setParams();
            registerReceiver(this.mTimeChangedReceiver, sIntentFilter);
        }

        public void onDestroy() {
            super.onDestroy();
            unregisterReceiver(this.mTimeChangedReceiver);
        }

        public void onStart(Intent intent, int startId) {
            super.onStart(intent, startId);
            if (ACTION_UPDATE.equals(intent.getAction())) {
                update();
            }
        }

        public IBinder onBind(Intent intent) {
            return null;
        }

        /* access modifiers changed from: private */
        public void update() {
            this.mCalendar.setTimeInMillis(System.currentTimeMillis());
            RemoteViews views = new RemoteViews(getPackageName(), (int) R.layout.widget);
            String dayTime = "";
            if (!is24HourFormat(this)) {
                dayTime = this.mCalendar.get(9) == 0 ? this.amText : this.pmText;
            }
            views.setImageViewBitmap(R.id.time, buildImage(DateFormat.format(this.hoursFormat, this.mCalendar).toString(), dayTime, DateFormat.format("EEEE", this.mCalendar).toString(), DateFormat.format("dd", this.mCalendar).toString(), DateFormat.format("MMMM", this.mCalendar).toString(), DateFormat.format("yyyy", this.mCalendar).toString()));
            AppWidgetManager.getInstance(this).updateAppWidget(new ComponentName(this, ClockWidget.class), views);
        }

        /* access modifiers changed from: private */
        public void setParams() {
            this.hoursFormat = is24HourFormat(this) ? FORMAT_24_HOURS : FORMAT_12_HOURS;
            this.mCalendar = Calendar.getInstance();
            String[] ampm = new DateFormatSymbols().getAmPmStrings();
            this.amText = ampm[0].toUpperCase();
            this.pmText = ampm[1].toUpperCase();
        }

        private Bitmap buildImage(String time, String dayTime, String dayName, String day, String month, String year) {
            Bitmap myBitmap = Bitmap.createBitmap(380, 200, Bitmap.Config.ARGB_4444);
            Canvas myCanvas = new Canvas(myBitmap);
            Paint paint = new Paint();
            Typeface font = Typeface.createFromAsset(getAssets(), "fonts/YOXALL_B.TTF");
            paint.setAntiAlias(true);
            paint.setSubpixelText(true);
            paint.setTypeface(font);
            paint.setStyle(Paint.Style.FILL);
            paint.setColor(-1);
            paint.setTextSize(100.0f);
            paint.setTextAlign(Paint.Align.CENTER);
            myCanvas.drawText(time, 190.0f, 100.0f, paint);
            paint.setTextSize(20.0f);
            myCanvas.drawText(dayTime, 340.0f, 100.0f, paint);
            paint.setTextAlign(Paint.Align.LEFT);
            paint.setTextSize(60.0f);
            myCanvas.drawText(day, 190.0f, 150.0f, paint);
            paint.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/DejaVuSansCondensed.ttf"));
            paint.setTextAlign(Paint.Align.RIGHT);
            paint.setTextSize(30.0f);
            myCanvas.drawText(dayName, 170.0f, 130.0f, paint);
            paint.setTextAlign(Paint.Align.LEFT);
            paint.setTextSize(16.0f);
            myCanvas.drawText(month, 270.0f, 120.0f, paint);
            paint.setTextAlign(Paint.Align.LEFT);
            paint.setTextSize(30.0f);
            myCanvas.drawText(year, 270.0f, 150.0f, paint);
            return myBitmap;
        }

        private static boolean is24HourFormat(Context context) {
            return DateFormat.is24HourFormat(context);
        }
    }
}
