package com.omgbutton.anotherclockwidget;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int icon = 2130837504;
    }

    public static final class id {
        public static final int time = 2131099649;
        public static final int widget_layout = 2131099648;
    }

    public static final class layout {
        public static final int widget = 2130903040;
    }

    public static final class string {
        public static final int app_name = 2131034113;
        public static final int widget_name = 2131034112;
    }

    public static final class xml {
        public static final int clock_widget = 2130968576;
    }
}
