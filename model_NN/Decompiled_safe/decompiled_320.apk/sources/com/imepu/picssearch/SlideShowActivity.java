package com.imepu.picssearch;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.imepu.picssearch.base.PrcmSlideActivity;
import com.imepu.picssearch.base.PrcmToast;
import com.imepu.picssearch.constants.Constants;
import com.imepu.picssearch.hamasaki.R;
import com.imepu.picssearch.http.PrcmApi;
import com.imepu.picssearch.json.PrcmCommentsList;
import com.imepu.picssearch.json.PrcmDetail;
import com.imepu.picssearch.json.PrcmPic;
import com.imepu.picssearch.service.ImageDownloadService;
import com.imepu.picssearch.task.CommentRequest;
import com.imepu.picssearch.task.DetailTask;
import com.imepu.picssearch.utils.ImageCache;
import com.imepu.picssearch.view.PrcmImageView;
import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.Iterator;

public class SlideShowActivity extends PrcmSlideActivity {
    private View.OnClickListener commentBtn = new View.OnClickListener() {
        public void onClick(View arg0) {
            SlideShowActivity.this.showPicComments(SlideShowActivity.this.getActivePrcmPic());
        }
    };
    /* access modifiers changed from: private */
    public Button commentsBtn;
    private View.OnClickListener detailClick = new View.OnClickListener() {
        public void onClick(View v) {
            SlideShowActivity.this.detailDialog.showDialog();
        }
    };
    /* access modifiers changed from: private */
    public DetailDialog detailDialog;
    /* access modifiers changed from: private */
    public View.OnClickListener imageDownload = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent(Constants.IMAGE_DOWNLOAD_SERVICE);
            intent.setClass(SlideShowActivity.this, ImageDownloadService.class);
            PrcmPic pic = SlideShowActivity.this.getActivePrcmPic();
            intent.putExtra("image_url", pic.getOriginalThumbnail().getUrl());
            intent.putExtra("image_title", pic.getTitle());
            SlideShowActivity.this.startService(intent);
            SlideShowActivity.this.closeDialog();
            PrcmToast.show(SlideShowActivity.this, SlideShowActivity.this.getString(R.string.download_image_service_start));
        }
    };
    private View.OnClickListener imageDownloadConfirm = new View.OnClickListener() {
        public void onClick(View v) {
            SlideShowActivity.this.showOkCancelDialog(SlideShowActivity.this.getString(R.string.pic_download_confirm_message), SlideShowActivity.this.imageDownload);
        }
    };
    private ArrayList<PrcmPic> list;
    private final Runnable loadingEnd = new Runnable() {
        public void run() {
            SlideShowActivity.this.progressDialog.setVisibility(8);
        }
    };
    private final Runnable loadingStart = new Runnable() {
        public void run() {
            SlideShowActivity.this.progressDialog.setVisibility(0);
        }
    };
    /* access modifiers changed from: private */
    public View progressDialog;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.list = (ArrayList) getIntent().getSerializableExtra("list");
        Iterator<PrcmPic> it = this.list.iterator();
        while (it.hasNext()) {
            PrcmImageView imageView = new PrcmImageView(this);
            SoftReference<Bitmap> image = ImageCache.getImage(this, it.next().getSmallThumbnail().getUrl());
            if (image != null) {
                imageView.setImageBitmap(image.get());
            }
            addImage(imageView);
        }
        this.progressDialog = findViewById(R.id.loading_progress);
        this.progressDialog.setVisibility(8);
        this.commentsBtn = _findButtonClickListener(R.id.flick_menu_comments_btn, this.commentBtn);
        _findButtonClickListener(R.id.flick_menu_download_btn, this.imageDownloadConfirm);
        _findButtonClickListener(R.id.flick_menu_detail_btn, this.detailClick);
        setTitle(getTitleBarText());
        commentsLoad();
        largeImageLoad();
        detailLoad();
    }

    public void apiResult(PrcmDetail prcmDetail) {
        this.detailDialog.setDetail(prcmDetail);
    }

    private void commentsLoad() {
        CommentRequest request = new CommentRequest(30, -1);
        request.setPictureId(getActivePrcmPic().getPictureId());
        new CommentsTask().execute(request);
    }

    private void detailLoad() {
        this.detailDialog = this.detailDialog == null ? new DetailDialog(this) : this.detailDialog;
        this.detailDialog.setStatus(0);
        new DetailTask(this).execute(getActivePrcmPic().getPictureId());
    }

    private void largeImageLoad() {
        int page = getPageNo();
        setVisibilityArrowButton();
        PrcmImageView imageView = getPrcmImageView();
        imageView.setStartRunnable(this.loadingStart);
        imageView.setEndRunnable(this.loadingEnd);
        imageView.setImageUrl(getActivePrcmPic().getLargeThumbnail().getUrl());
        if (page + 1 < this.list.size()) {
            getPrcmImageView(page + 1).setImageUrl(this.list.get(page + 1).getLargeThumbnail().getUrl());
        }
    }

    private String getTitleBarText() {
        return getActivePrcmPic().getTitle().trim();
    }

    private class CommentsTask extends AsyncTask<CommentRequest, Void, PrcmCommentsList> {
        public CommentsTask() {
            SlideShowActivity.this.commentsBtn.setText(String.valueOf(SlideShowActivity.this.getString(R.string.flick_menu_comments)) + "\n" + "-");
        }

        /* access modifiers changed from: protected */
        public PrcmCommentsList doInBackground(CommentRequest... value) {
            try {
                return PrcmApi.comments(value[0]);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(PrcmCommentsList list) {
            SlideShowActivity.this.commentsBtn.setText(String.valueOf(SlideShowActivity.this.getString(R.string.flick_menu_comments)) + "\n" + String.valueOf(list != null ? list.getCount() : 0));
        }
    }

    /* access modifiers changed from: private */
    public PrcmPic getActivePrcmPic() {
        return this.list.get(getPageNo());
    }

    /* access modifiers changed from: protected */
    public void onItemChenge() {
        this.progressDialog.setVisibility(8);
        largeImageLoad();
        detailLoad();
        commentsLoad();
        setTitle(getTitleBarText());
    }
}
