package com.imepu.picssearch;

import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.TextView;
import com.imepu.picssearch.base.PrcmGridActivity;
import com.imepu.picssearch.hamasaki.R;
import com.imepu.picssearch.http.PrcmApi;
import com.imepu.picssearch.http.PrcmException;
import com.imepu.picssearch.json.PrcmPicsList;
import com.imepu.picssearch.task.ListRequest;
import com.imepu.picssearch.task.TaskResult;
import com.imepu.picssearch.utils.CacheUtils;
import java.text.NumberFormat;

public class SearchPicsGridActivity extends PrcmGridActivity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(getString(R.string.search_title));
    }

    public void onDestroy() {
        super.onDestroy();
        new RemoveImageCache(this, null).execute(new Void[0]);
    }

    /* access modifiers changed from: protected */
    public void startTask(ListRequest request) {
        new SearchListTask(this, null).execute(request);
    }

    private class SearchListTask extends AsyncTask<ListRequest, Void, TaskResult> {
        private SearchListTask() {
        }

        /* synthetic */ SearchListTask(SearchPicsGridActivity searchPicsGridActivity, SearchListTask searchListTask) {
            this();
        }

        /* access modifiers changed from: protected */
        public TaskResult doInBackground(ListRequest... vaule) {
            try {
                ListRequest request = vaule[0];
                int count = request.getCount().intValue();
                return new TaskResult().setResult(PrcmApi.searchList(SearchPicsGridActivity.this.getString(R.string.search_word), request.getSinceId().intValue(), count));
            } catch (Exception e) {
                Exception e2 = e;
                e2.printStackTrace();
                return new TaskResult().setError(e2);
            }
        }

        /* Debug info: failed to restart local var, previous not found, register: 7 */
        /* access modifiers changed from: protected */
        public void onPostExecute(TaskResult result) {
            SearchPicsGridActivity.this.setProgressVisibility(4);
            if (result.isNotError()) {
                SearchPicsGridActivity.this.apiResult((PrcmPicsList) result.getResult());
                SearchPicsGridActivity.this.setTitle(String.valueOf(NumberFormat.getNumberInstance().format((long) ((PrcmPicsList) result.getResult()).getTotal())) + " pics");
                return;
            }
            ((TextView) SearchPicsGridActivity.this.findViewById(R.id.loading_message)).setText(result.getException() instanceof PrcmException ? result.getException().getMessage() : SearchPicsGridActivity.this.getString(R.string.network_error));
        }
    }

    private class RemoveImageCache extends AsyncTask<Void, Void, Void> {
        private RemoveImageCache() {
        }

        /* synthetic */ RemoveImageCache(SearchPicsGridActivity searchPicsGridActivity, RemoveImageCache removeImageCache) {
            this();
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Void... params) {
            CacheUtils.deleteAll(SearchPicsGridActivity.this);
            return null;
        }
    }
}
