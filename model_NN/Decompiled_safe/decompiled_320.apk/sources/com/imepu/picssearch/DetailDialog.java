package com.imepu.picssearch;

import android.app.Dialog;
import android.content.Intent;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.imepu.picssearch.base.PrcmActivity;
import com.imepu.picssearch.base.PrcmDisplay;
import com.imepu.picssearch.base.PrcmToast;
import com.imepu.picssearch.constants.Constants;
import com.imepu.picssearch.hamasaki.R;
import com.imepu.picssearch.json.PrcmDetail;
import com.imepu.picssearch.service.ImageDownloadService;
import com.imepu.picssearch.utils.EmojiUtils;
import com.imepu.picssearch.utils.StringUtils;
import com.imepu.picssearch.view.PrcmImageView;

public class DetailDialog {
    public static final int STATUS_LOADING = 0;
    public static final int STATUS_YES = 1;
    /* access modifiers changed from: private */
    public PrcmActivity activity;
    private View.OnClickListener albumClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            DetailDialog.this.activity.openUrl(DetailDialog.this.detail.getAlbum().getAlbumUrl());
        }
    };
    /* access modifiers changed from: private */
    public PrcmDetail detail;
    /* access modifiers changed from: private */
    public Dialog dialog;
    private View dialogView;
    private View.OnClickListener imageDownload = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent(Constants.IMAGE_DOWNLOAD_SERVICE);
            intent.setClass(DetailDialog.this.activity, ImageDownloadService.class);
            intent.putExtra("image_url", DetailDialog.this.detail.getPic().getOriginalThumbnail().getUrl());
            intent.putExtra("image_title", DetailDialog.this.detail.getPic().getTitle());
            DetailDialog.this.activity.startService(intent);
            PrcmToast.show(DetailDialog.this.activity, DetailDialog.this.activity.getString(R.string.download_image_service_start));
            DetailDialog.this.dialog.dismiss();
        }
    };
    private View.OnClickListener shareClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent();
            intent.setType("text/plain");
            intent.setAction("android.intent.action.SEND");
            intent.addCategory("android.intent.category.DEFAULT");
            intent.putExtra("android.intent.extra.TEXT", String.valueOf(DetailDialog.this.detail.getPic().getTitle()) + " " + DetailDialog.this.detail.getPic().getWebUrl());
            DetailDialog.this.activity.startActivity(intent);
        }
    };
    private int status = 1;
    private View.OnClickListener webClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            DetailDialog.this.activity.openUrl(DetailDialog.this.detail.getPic().getWebUrl());
        }
    };

    public DetailDialog(PrcmActivity activity2) {
        this.activity = activity2;
        this.dialogView = LayoutInflater.from(activity2).inflate((int) R.layout.detail_dialog, (ViewGroup) null);
        this.dialog = new Dialog(activity2);
        this.dialog.setContentView(this.dialogView, new ViewGroup.LayoutParams((int) (((double) PrcmDisplay.getWidth(activity2)) * 0.9d), -1));
        _findButtonClickListener(R.id.share_btn, this.shareClickListener);
        _findButtonClickListener(R.id.open_web_btn, this.webClickListener);
        _findButtonClickListener(R.id.download_btn, this.imageDownload);
    }

    public void setStatus(int status2) {
        this.status = status2;
    }

    public void setDetail(PrcmDetail detail2) {
        if (detail2 != null) {
            this.detail = detail2;
            setStatus(1);
            _setCaption(detail2);
            Integer viewCount = Integer.valueOf(detail2.getEvaluate().getView());
            Integer commentCount = Integer.valueOf(detail2.getEvaluate().getComment());
            Integer likeCount = Integer.valueOf(detail2.getEvaluate().getLike());
            _dialogViewTextViewAndSetText(R.id.detail_view_count, viewCount.toString());
            _dialogViewTextViewAndSetText(R.id.detail_comments_count, commentCount.toString());
            _dialogViewTextViewAndSetText(R.id.detail_like_count, likeCount.toString());
            _dialogViewTextViewAndSetText(R.id.detail_like, EmojiUtils.deleteEmoji(detail2.getAlbum().getLikeComment().trim()));
            try {
                CharSequence spanned = Html.fromHtml("<a href=\"\">アルバム：" + EmojiUtils.convertTag(detail2.getAlbum().getTitle()) + "</a>", this.activity.emojiGetter, null);
                TextView album = (TextView) this.dialogView.findViewById(R.id.detail_album_name);
                album.setText(spanned);
                album.setVisibility(0);
                album.setClickable(true);
                album.setTextColor(-16776961);
                album.setOnClickListener(this.albumClickListener);
            } catch (Exception e) {
                e.printStackTrace();
            }
            ((PrcmImageView) this.dialogView.findViewById(R.id.detail_image)).setImageUrl(detail2.getPic().getLargeThumbnail().getUrl());
            this.dialog.setTitle(detail2.getPic().getTitle());
        }
    }

    public void showDialog() {
        if (this.status == 1) {
            this.dialog.show();
        }
    }

    private void _setCaption(PrcmDetail detail2) {
        TextView textView = (TextView) this.dialogView.findViewById(R.id.detail_caption);
        String caption = detail2.getCaption().trim();
        textView.setVisibility(StringUtils.isNotEmpty(caption) ? 0 : 8);
        if (StringUtils.isNotEmpty(caption)) {
            textView.setText(caption);
            String html = EmojiUtils.convertTag(detail2.getCaption().trim());
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(-1, -2);
            int margin = (int) (10.0f * PrcmDisplay.getDip(this.activity));
            params.setMargins(margin, 0, margin, margin);
            textView.setLayoutParams(params);
            textView.setText(Html.fromHtml(html, this.activity.emojiGetter, null));
        }
    }

    private TextView _dialogViewTextViewAndSetText(int id, String text) {
        TextView textView = (TextView) this.dialogView.findViewById(id);
        if (textView != null) {
            textView.setText(text);
            if (StringUtils.isNotEmpty(text)) {
                textView.setEnabled(true);
                textView.setVisibility(0);
            }
        }
        return textView;
    }

    private Button _findButtonClickListener(int id, View.OnClickListener listener) {
        try {
            Button button = (Button) this.dialogView.findViewById(id);
            button.setOnClickListener(listener);
            return button;
        } catch (Exception e) {
            return null;
        }
    }
}
