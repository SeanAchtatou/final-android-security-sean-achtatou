package com.imepu.picssearch.base;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.imepu.picssearch.PicCommentsListActivity;
import com.imepu.picssearch.SearchPicsGridActivity;
import com.imepu.picssearch.SlideShowActivity;
import com.imepu.picssearch.constants.Constants;
import com.imepu.picssearch.hamasaki.R;
import com.imepu.picssearch.json.PrcmPic;
import com.imepu.picssearch.utils.Analytics;
import com.imepu.picssearch.utils.ImageCache;
import com.imepu.picssearch.utils.ObjectCache;
import com.imepu.picssearch.utils.StringUtils;
import java.util.ArrayList;
import java.util.Random;

public abstract class PrcmActivity extends Activity {
    private static final int MENU_APPLI_LIST = 2;
    private static final int MENU_THIS_APP = 1;
    private AdView adView;
    public Html.ImageGetter emojiGetter = new Html.ImageGetter() {
        public Drawable getDrawable(String source) {
            Drawable emoji = PrcmActivity.this.getResources().getDrawable(PrcmActivity.this.getResources().getIdentifier(source, "drawable", PrcmActivity.this.getPackageName()));
            emoji.setBounds(0, 0, (int) (((double) emoji.getIntrinsicWidth()) * 1.25d), (int) (((double) emoji.getIntrinsicHeight()) * 1.25d));
            return emoji;
        }
    };
    private AlertDialog mAlertDialog;
    private TextView mTitleTextView;
    private GoogleAnalyticsTracker tracker;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
    }

    public void setContentView(int layoutResId) {
        super.setContentView(layoutResId);
        this.tracker = Analytics.getTracker(this);
        Analytics.Track(this.tracker, getString(R.string.package_name));
        this.mTitleTextView = (TextView) findViewById(R.id.title_bar_text);
        this.adView = new AdView(this, AdSize.BANNER, Constants.ADMOB_ID);
        LinearLayout layout = (LinearLayout) findViewById(R.id.mainLayout);
        if (layout != null) {
            layout.addView(this.adView);
            this.adView.loadAd(new AdRequest());
        }
    }

    public void onDestroy() {
        this.adView.destroy();
        Analytics.stop(this.tracker);
        super.onDestroy();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menuAdd(menu, 1, "about", 17301577);
        menuAdd(menu, 2, "othor app", 17301556);
        return super.onCreateOptionsMenu(menu);
    }

    /* access modifiers changed from: protected */
    public MenuItem menuAdd(Menu menu, int itemId, CharSequence title, int icon) {
        return menu.add(0, itemId, 0, title).setIcon(icon);
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        boolean ret = super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case 1:
                showOkCancelDialog(getString(R.string.this_application), new View.OnClickListener() {
                    public void onClick(View v) {
                        PrcmActivity.this.openUrl("market://details?id=jp.gmomedia.android.prcm");
                    }
                });
                return true;
            case 2:
                openUrl("market://search?q=pub:imepu");
                return true;
            default:
                return ret;
        }
    }

    /* access modifiers changed from: protected */
    public Button _findButtonClickListener(int id, View.OnClickListener listener) {
        try {
            Button button = (Button) findViewById(id);
            button.setOnClickListener(listener);
            return button;
        } catch (Exception e) {
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public ImageButton _findImageButtonClickListener(int id, View.OnClickListener listener) {
        try {
            ImageButton button = (ImageButton) findViewById(id);
            button.setOnClickListener(listener);
            return button;
        } catch (Exception e) {
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public TextView _findTextViewAndSetText(int id, String text) {
        TextView textView = (TextView) findViewById(id);
        if (textView != null) {
            textView.setText(text);
            if (StringUtils.isNotEmpty(text)) {
                textView.setEnabled(true);
                textView.setVisibility(0);
            }
        }
        return textView;
    }

    /* access modifiers changed from: protected */
    public TextView _findTextViewAndSetRandomText(int id, int stringArrayId) {
        String[] message = getResources().getStringArray(stringArrayId);
        return _findTextViewAndSetText(id, message[new Random().nextInt(message.length)]);
    }

    /* access modifiers changed from: protected */
    public ImageView _findImageViewAndSetRandomImage(int id, int[] resorceIds) {
        return _findImageViewAndImageResource(id, resorceIds[new Random().nextInt(resorceIds.length)]);
    }

    /* access modifiers changed from: protected */
    public ImageView _findImageViewAndImageResource(int id, int resorceId) {
        ImageView view = (ImageView) findViewById(id);
        if (view != null) {
            view.setImageResource(resorceId);
        }
        return view;
    }

    /* access modifiers changed from: protected */
    public EditText _findEditTextAndSetText(int id, String text) {
        EditText editText = (EditText) findViewById(id);
        if (editText != null && StringUtils.isNotEmpty(text)) {
            editText.setText(text);
        }
        return editText;
    }

    public void openUrl(String url) {
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse(url)));
    }

    /* access modifiers changed from: protected */
    public void showGrid() {
        startActivity(new Intent(this, SearchPicsGridActivity.class));
    }

    /* access modifiers changed from: protected */
    public void showDetail(PrcmPic pic) {
        ArrayList<PrcmPic> slideShowList = new ArrayList<>();
        slideShowList.add(pic);
        showSlideShow(slideShowList, 0);
    }

    /* access modifiers changed from: protected */
    public void showPicComments(PrcmPic pic) {
        Intent intent = new Intent(this, PicCommentsListActivity.class);
        intent.putExtra("picture_id", pic.getPictureId());
        intent.putExtra("title", pic.getTitle());
        intent.putExtra("web_url", pic.getWebUrl());
        startActivity(intent);
    }

    /* access modifiers changed from: protected */
    public void showSlideShow(ArrayList<PrcmPic> list, int position) {
        Intent intent = new Intent(this, SlideShowActivity.class);
        intent.putExtra("list", list);
        intent.putExtra("position", position);
        startActivity(intent);
    }

    public void showAlertDialog(String Message) {
        View view = LayoutInflater.from(this).inflate((int) R.layout.dialog_error, (ViewGroup) null);
        ((TextView) view.findViewById(R.id.message)).setText(Message);
        ((Button) view.findViewById(R.id.btnOk)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PrcmActivity.this.closeDialog();
            }
        });
        showDialog(view);
    }

    /* access modifiers changed from: protected */
    public void showOkCancelDialog(String Message, View.OnClickListener onClickListener) {
        View view = LayoutInflater.from(this).inflate((int) R.layout.dialog_ok_cancel, (ViewGroup) null);
        ((TextView) view.findViewById(R.id.message)).setText(Message);
        ((Button) view.findViewById(R.id.btnOk)).setOnClickListener(onClickListener);
        ((Button) view.findViewById(R.id.btnCancel)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PrcmActivity.this.closeDialog();
            }
        });
        showDialog(view);
    }

    /* access modifiers changed from: protected */
    public void showOkDialog(String Message, View.OnClickListener onClickListener) {
        View view = LayoutInflater.from(this).inflate((int) R.layout.dialog_info, (ViewGroup) null);
        ((TextView) view.findViewById(R.id.message)).setText(Message);
        ((Button) view.findViewById(R.id.btnOk)).setOnClickListener(onClickListener);
        showDialog(view);
    }

    private void showDialog(View view) {
        try {
            this.mAlertDialog = this.mAlertDialog == null ? new AlertDialog.Builder(this).create() : this.mAlertDialog;
            this.mAlertDialog.setView(view);
            this.mAlertDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void closeDialog() {
        if (this.mAlertDialog != null) {
            this.mAlertDialog.dismiss();
        }
    }

    public void setTitle(String title) {
        if (this.mTitleTextView != null) {
            this.mTitleTextView.setText(title);
        }
    }

    public void clearCache() {
        ImageCache.clear(this);
        ObjectCache.clear();
    }
}
