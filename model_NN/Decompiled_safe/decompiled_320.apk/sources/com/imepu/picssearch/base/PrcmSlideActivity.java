package com.imepu.picssearch.base;

import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import com.imepu.picssearch.hamasaki.R;
import com.imepu.picssearch.view.HSView;
import com.imepu.picssearch.view.PrcmImageView;
import java.util.ArrayList;

public abstract class PrcmSlideActivity extends PrcmActivity {
    private Animation animation;
    private LinearLayout horizontalLinearLayout;
    /* access modifiers changed from: private */
    public HSView hsvc;
    private ArrayList<PrcmImageView> imageViews = new ArrayList<>();
    private Runnable itemChenge = new Runnable() {
        public void run() {
            PrcmSlideActivity.this.onItemChenge();
        }
    };
    private ImageButton leftBtn;
    private View.OnClickListener leftButton = new View.OnClickListener() {
        public void onClick(View v) {
            PrcmSlideActivity.this.hsvc.moveLeftPage();
        }
    };
    private ImageButton rightBtn;
    private View.OnClickListener rightButton = new View.OnClickListener() {
        public void onClick(View v) {
            PrcmSlideActivity.this.hsvc.moveRightPage();
        }
    };

    /* access modifiers changed from: protected */
    public abstract void onItemChenge();

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.flick);
        this.hsvc = (HSView) findViewById(R.id.horizontal_view);
        this.rightBtn = (ImageButton) findViewById(R.id.arrow_right);
        this.leftBtn = (ImageButton) findViewById(R.id.arrow_left);
        this.leftBtn.setOnClickListener(this.leftButton);
        this.rightBtn.setOnClickListener(this.rightButton);
        this.hsvc.setPageChangeRunnable(this.itemChenge);
        this.horizontalLinearLayout = new LinearLayout(this);
        this.horizontalLinearLayout.setOrientation(0);
        this.hsvc.addView(this.horizontalLinearLayout);
        this.animation = AnimationUtils.loadAnimation(this, R.anim.fadeout);
    }

    /* access modifiers changed from: protected */
    public void addImage(PrcmImageView imageView) {
        this.imageViews.add(imageView);
        LinearLayout pageLinearLayout = new LinearLayout(this);
        pageLinearLayout.setOrientation(1);
        LinearLayout.LayoutParams pageLayoutParams = new LinearLayout.LayoutParams(-1, -1);
        pageLayoutParams.width = PrcmDisplay.getWidth(this);
        pageLayoutParams.height = PrcmDisplay.getHeight(this);
        pageLinearLayout.setLayoutParams(pageLayoutParams);
        imageView.setLayoutParams(pageLayoutParams);
        pageLinearLayout.addView(imageView);
        this.horizontalLinearLayout.addView(pageLinearLayout);
        this.hsvc.setPageSize(this.imageViews.size());
    }

    /* access modifiers changed from: protected */
    public void setVisibilityArrowButton() {
        setVisibilityAndAnimation(this.rightBtn, this.hsvc.hasNextPage());
        setVisibilityAndAnimation(this.leftBtn, this.hsvc.hasPrevPage());
    }

    /* access modifiers changed from: protected */
    public void setVisibilityAndAnimation(ImageButton btn, boolean visibility) {
        if (visibility) {
            btn.setVisibility(0);
            btn.startAnimation(this.animation);
            return;
        }
        btn.setVisibility(8);
        btn.clearAnimation();
    }

    /* access modifiers changed from: protected */
    public PrcmImageView getPrcmImageView() {
        return getPrcmImageView(getPageNo());
    }

    /* access modifiers changed from: protected */
    public PrcmImageView getPrcmImageView(int pageNo) {
        return this.imageViews.get(pageNo);
    }

    /* access modifiers changed from: protected */
    public int getPageNo() {
        return this.hsvc.getPageNo();
    }
}
