package com.imepu.picssearch.http;

import com.imepu.picssearch.utils.StringUtils;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import org.json.JSONObject;

public abstract class PrcmApiClient {
    protected static String addSinceId(String url, int sinceId) throws UnsupportedEncodingException {
        if (sinceId > 0) {
            return addParam(url, "since_id", String.valueOf(sinceId));
        }
        return url;
    }

    protected static String addParam(String url, String key, String param) throws UnsupportedEncodingException {
        return StringUtils.isNotEmpty(param) ? String.valueOf(url) + "&" + key + "=" + URLEncoder.encode(param, "UTF-8") : url;
    }

    protected static JSONObject doRequest(String url) throws Exception {
        return getResponse(PrcmHttpGet.doGetRequest(url));
    }

    protected static JSONObject doRequest(String url, String user, String password) throws Exception {
        return getResponse(PrcmHttpGet.doGetRequest(url, user, password));
    }

    private static JSONObject getResponse(HttpResult result) throws Exception {
        if (result.getStatusCode() == 200) {
            return new JSONObject(result.getHttpBody());
        }
        throw new PrcmException(result.getMessage());
    }
}
