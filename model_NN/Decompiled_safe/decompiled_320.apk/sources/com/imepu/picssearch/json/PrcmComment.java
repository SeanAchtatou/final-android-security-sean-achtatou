package com.imepu.picssearch.json;

import com.imepu.picssearch.utils.DateUtil;
import org.json.JSONException;
import org.json.JSONObject;

public class PrcmComment {
    private String UserId;
    private int commentId;
    private String contentFlag;
    private String createdAt;
    private String device;
    private PrcmPic pic;
    private int resNo;
    private String screenName;
    private int statusId;
    private String text;
    private int viewUserId;

    public PrcmComment(JSONObject json) throws JSONException {
        this.commentId = json.getInt("comment_id");
        this.resNo = json.getInt("res_no");
        this.text = json.getString("text");
        this.statusId = json.getInt("res_status_id");
        this.UserId = json.getString("user_id");
        this.viewUserId = json.getInt("view_user_id");
        this.screenName = json.getString("screen_name");
        this.createdAt = json.getString("created_at");
        this.device = json.getString("device");
        this.contentFlag = json.getString("content_flag");
        this.pic = new PrcmPic(json.getJSONObject("picture"));
    }

    public int getCommentId() {
        return this.commentId;
    }

    public int getResNo() {
        return this.resNo;
    }

    public String getText() {
        return this.text;
    }

    public int getStatusId() {
        return this.statusId;
    }

    public String getUserId() {
        return this.UserId;
    }

    public int getViewUserId() {
        return this.viewUserId;
    }

    public String getScreenName() {
        return this.screenName;
    }

    public String getCreatedAt() {
        return this.createdAt;
    }

    public String getCreatedAtScreen() {
        return DateUtil.toScreen(this.createdAt);
    }

    public String getDevice() {
        return this.device;
    }

    public boolean isLike() {
        return this.contentFlag.equals("LIKE");
    }

    public boolean isComment() {
        return this.contentFlag.equals("COMMENT");
    }

    public boolean isWarn() {
        return this.contentFlag.equals("WARN");
    }

    public PrcmPic getPic() {
        return this.pic;
    }
}
