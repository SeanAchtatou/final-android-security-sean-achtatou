package com.imepu.picssearch.json;

import java.io.Serializable;
import org.json.JSONException;
import org.json.JSONObject;

public class PrcmThumbnail implements Serializable {
    private static final long serialVersionUID = 359509101034584533L;
    int height;
    String url;
    int width;

    public PrcmThumbnail(JSONObject json) throws JSONException {
        this.url = json.getString("url");
        this.height = json.getInt("height");
        this.width = json.getInt("width");
    }

    public String getUrl() {
        return this.url;
    }

    public int getHeight() {
        return this.height;
    }

    public int getWidth() {
        return this.width;
    }
}
