package com.imepu.picssearch.task;

public class ListRequest {
    private Integer SinceId;
    private Integer count;

    public Integer getCount() {
        return this.count;
    }

    public void setCount(Integer count2) {
        this.count = count2;
    }

    public Integer getSinceId() {
        return this.SinceId;
    }

    public void setSinceId(Integer sinceId) {
        this.SinceId = sinceId;
    }
}
