package defpackage;

import android.webkit.WebView;
import com.google.ads.util.a;
import java.util.HashMap;

/* renamed from: l  reason: default package */
public final class l implements i {
    public final void a(d dVar, HashMap<String, String> hashMap, WebView webView) {
        if (webView instanceof g) {
            ((g) webView).a();
        } else {
            a.b("Trying to close WebView that isn't an AdWebView");
        }
    }
}
