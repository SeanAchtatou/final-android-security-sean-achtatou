package com.m41m41.naturedisaserts2;

import android.app.Activity;
import android.content.Intent;
import android.database.CursorJoiner;
import android.net.Uri;
import android.os.AsyncTask;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import org.apache.http.util.ByteArrayBuffer;
import org.apache.http.util.EncodingUtils;

public class LoadHotSearchListTask extends AsyncTask<Activity, Integer, CursorJoiner.Result> {
    public static final String app_url = "market://search?q=pname:com.m41m41.naturedisaserts2";
    public static final String english_name = "nature disaserts";
    private static final String refresh_err = "Refresh error,please retry";
    private final String RAN = "ran";
    private final String SEQ = "seq";
    private final String URL = "url";
    /* access modifiers changed from: private */
    public final activity activity;
    private ByteArrayBuffer baf;
    InputStream bis;
    /* access modifiers changed from: private */
    public String[] hotStrings = new String[(this.metaData.length / 3)];
    private String last_adress = "";
    private final String[] metaData = {"ran", "Avalanches", "Avalanches", "ran", "Earthquakes", "Earthquakes", "ran", "Volcanic eruptions", "Volcanic eruptions", "ran", "Floods", "Floods", "ran", "Limnic eruptions", "Limnic eruptions", "ran", "Blizzards", "Blizzards", "ran", "Cyclonic storms", "Cyclonic storms", "ran", "Droughts", "Droughts", "ran", "Hailstorms", "Hailstorms", "ran", "Heat waves", "Heat waves", "ran", "Tornadoes", "Tornadoes", "ran", "Fires", "Fires", "ran", "Impact events", "Impact events", "ran", "Solar flares", "Solar flares", "ran", "nature disasters", "nature disasters", "ran", "2001 Gujarat earthquake India", "2001 Gujarat earthquake India", "ran", "2003 Europe Heat Waves", "2003 Europe Heat Waves", "ran", "2003 Iran Earthquake", "2003 Iran Earthquake", "ran", "2004 Hurricane Jeanne", "2004 Hurricane Jeanne", "ran", "2004 Asian Tsunami", "2004 Asian Tsunami", "ran", "2005 Hurricane Katrina", "2005 Hurricane Katrina", "ran", "2005 Hurricane Stan", "2005 Hurricane Stan", "ran", "2005 Pakistan Earthquake", "2005 Pakistan Earthquake", "ran", "2008 Burma (Myanmar) Cyclone", "2008 Burma (Myanmar) Cyclone", "ran", "2008 China Earthquake", "2008 China Earthquake", "ran", "2009 Global Swine Flu", "2009 Global Swine Flu", "ran", "2010 Haiti Earthquake", "2010 Haiti Earthquake", "ran", "2010 Chile Earthquake", "2010 Chile Earthquake", "ran", "2010 Pakistan Flooding", "2010 Pakistan Flooding", "ran", "2010 Yushu earthquake", "2010 Yushu earthquake", "ran", "2011 Queensland floods", "2011 Queensland floods", "ran", "2011 Christchurch earthquake", "2011 Christchurch earthquake", "ran", "2011 Tōhoku earthquake and tsunami", "2011 Tōhoku earthquake and tsunami", "ran", "2011 Burma earthquake", "2011 Burma earthquake"};
    /* access modifiers changed from: private */
    public String[] searchOption = new String[(this.metaData.length / 3)];
    /* access modifiers changed from: private */
    public String[] stringToLink = new String[(this.metaData.length / 3)];
    URLConnection uconnection;
    URL uri;

    public String[] getHotStrings() {
        return this.hotStrings;
    }

    LoadHotSearchListTask(activity activity2) {
        this.activity = activity2;
        for (int i = 0; i < this.metaData.length; i += 3) {
            this.searchOption[i / 3] = this.metaData[i];
            this.hotStrings[i / 3] = this.metaData[i + 1];
            this.stringToLink[i / 3] = this.metaData[i + 2];
        }
    }

    /* access modifiers changed from: protected */
    public CursorJoiner.Result doInBackground(Activity... arg0) {
        this.activity.list_hot_search = getHotStrings();
        Intent intent = this.activity.getIntent();
        this.activity.getClass();
        this.activity.getClass();
        intent.putExtra("KEY_NET_status", "sucess");
        return null;
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(CursorJoiner.Result result) {
        activity activity2 = this.activity;
        this.activity.getClass();
        activity2.removeDialog(5);
        Intent intent = this.activity.getIntent();
        this.activity.getClass();
        String stringExtra = intent.getStringExtra("KEY_NET_status");
        this.activity.getClass();
        if (stringExtra.compareTo("fail") == 0) {
            Toast.makeText(this.activity, refresh_err, 1).show();
        } else if (!(this.activity.list_hot_search == null || this.activity.list_hot_search.length == 0)) {
            ListView list1 = (ListView) this.activity.findViewById(R.id.ListView01);
            list1.setAdapter((ListAdapter) new ArrayAdapter(this.activity, 17367043, this.activity.list_hot_search));
            list1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> adapterView, View arg1, int position, long arg3) {
                    if (LoadHotSearchListTask.this.searchOption[position].compareTo("url") == 0) {
                        Intent i = new Intent(LoadHotSearchListTask.this.activity, BrowserActivity.class);
                        i.putExtra(GoogleImageSearchParser.INTENT_STR_KEYWORD, LoadHotSearchListTask.this.hotStrings[position]);
                        i.setData(Uri.parse(LoadHotSearchListTask.this.stringToLink[position]));
                        LoadHotSearchListTask.this.activity.startActivity(i);
                    } else if (LoadHotSearchListTask.this.searchOption[position].compareTo("ran") == 0) {
                        LoadHotSearchListTask.this.activity.startSearch(LoadHotSearchListTask.this.stringToLink[position], false);
                    } else if (LoadHotSearchListTask.this.searchOption[position].compareTo("seq") == 0) {
                        LoadHotSearchListTask.this.activity.startSearch(LoadHotSearchListTask.this.stringToLink[position], true);
                    }
                }
            });
        }
        super.onPostExecute((Object) result);
    }

    public ByteArrayBuffer read(String address, int size) throws IOException {
        if (address.equalsIgnoreCase(this.last_adress)) {
            return this.baf;
        }
        this.uri = new URL(address);
        this.uconnection = this.uri.openConnection();
        this.uconnection.setUseCaches(true);
        this.bis = this.uconnection.getInputStream();
        this.baf = new ByteArrayBuffer(size);
        this.baf.clear();
        while (true) {
            int current = this.bis.read();
            if (current == -1) {
                this.last_adress = address;
                return this.baf;
            }
            this.baf.append((byte) current);
        }
    }

    public String decodeURL(ByteArrayBuffer buffer) {
        return EncodingUtils.getString(buffer.toByteArray(), "UTF-8");
    }
}
