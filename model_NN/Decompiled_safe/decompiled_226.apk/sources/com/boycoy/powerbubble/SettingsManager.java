package com.boycoy.powerbubble;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class SettingsManager {
    private static SettingsManager sInstance = null;
    private SharedPreferences mSharedPreferences = null;

    public enum Settings {
        FULLSCREEN {
            public String toString() {
                return "checkbox_fullscreen";
            }
        },
        SHOW_ADS {
            public String toString() {
                return "checkbox_show_ads";
            }
        },
        ALLOW_TRACKING {
            public String toString() {
                return "checkbox_allow_tracking";
            }
        },
        DONT_SLEEP {
            public String toString() {
                return "checkbox_dont_sleep";
            }
        },
        ENABLE_DECIMAL {
            public String toString() {
                return "checkbox_enable_decimal";
            }
        },
        ENABLE_SOUND {
            public String toString() {
                return "checkbox_enable_sound";
            }
        },
        ORIENTATION {
            public String toString() {
                return "orientation_value";
            }
        },
        FIRST_RUN {
            public String toString() {
                return "first_run";
            }
        },
        ENABLE_SPLASH {
            public String toString() {
                return "checkbox_enable_splash";
            }
        },
        CALIBRATION_0 {
            public String toString() {
                return "calibration_0";
            }
        },
        CALIBRATION_90 {
            public String toString() {
                return "calibration_90";
            }
        },
        CALIBRATION_180 {
            public String toString() {
                return "calibration_180";
            }
        },
        CALIBRATION_270 {
            public String toString() {
                return "calibration_270";
            }
        }
    }

    private SettingsManager(Context context) {
        PreferenceManager.setDefaultValues(context, R.xml.preferences, false);
        this.mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static void init(Context context) {
        sInstance = new SettingsManager(context);
    }

    public static boolean getBoolean(Settings setting) {
        return getBoolean(setting, false);
    }

    public static boolean getBoolean(Settings setting, boolean defaultValue) {
        return getInstance().mSharedPreferences.getBoolean(setting.toString(), defaultValue);
    }

    public static int getInt(Settings setting) {
        return getInt(setting, 0);
    }

    public static int getInt(Settings setting, int defaultValue) {
        return getInstance().mSharedPreferences.getInt(setting.toString(), defaultValue);
    }

    public static float getFloat(Settings setting) {
        return getFloat(setting, 0.0f);
    }

    public static float getFloat(Settings setting, float defaultValue) {
        return getInstance().mSharedPreferences.getFloat(setting.toString(), defaultValue);
    }

    public static void setBoolean(Settings setting, boolean value) {
        SharedPreferences.Editor editor = getInstance().mSharedPreferences.edit();
        editor.putBoolean(setting.toString(), value);
        editor.commit();
    }

    public static void setInt(Settings setting, int value) {
        SharedPreferences.Editor editor = getInstance().mSharedPreferences.edit();
        editor.putInt(setting.toString(), value);
        editor.commit();
    }

    public static void setFloat(Settings setting, float value) {
        SharedPreferences.Editor editor = getInstance().mSharedPreferences.edit();
        editor.putFloat(setting.toString(), value);
        editor.commit();
    }

    private static SettingsManager getInstance() {
        return sInstance;
    }
}
