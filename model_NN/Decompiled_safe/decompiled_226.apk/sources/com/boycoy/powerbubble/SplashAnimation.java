package com.boycoy.powerbubble;

import android.view.View;
import android.view.animation.Animation;

public class SplashAnimation {
    private Animation mHideAnimation;
    /* access modifiers changed from: private */
    public SplashAnimationListener mHideAnimationListener = null;
    private Animation mShowAnimation;
    /* access modifiers changed from: private */
    public SplashAnimationListener mShowAnimationListener = null;
    /* access modifiers changed from: private */
    public View mSplashView;

    public SplashAnimation(View splashView, Animation showAnimation, Animation hideAnimation) {
        this.mSplashView = splashView;
        this.mShowAnimation = showAnimation;
        this.mHideAnimation = hideAnimation;
        createListeners();
    }

    public void showSplash() {
        this.mSplashView.setVisibility(0);
        this.mSplashView.startAnimation(this.mShowAnimation);
    }

    public void hideSplash() {
        this.mSplashView.startAnimation(this.mHideAnimation);
    }

    public void setOnShowAnimationListener(SplashAnimationListener showAnimationListener) {
        this.mShowAnimationListener = showAnimationListener;
    }

    public void setOnHideAnimationListener(SplashAnimationListener hideAnimationListener) {
        this.mHideAnimationListener = hideAnimationListener;
    }

    private void createListeners() {
        createShowListener();
        createHideListener();
    }

    private void createShowListener() {
        this.mShowAnimation.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationEnd(Animation animation) {
                if (SplashAnimation.this.mShowAnimationListener != null) {
                    SplashAnimation.this.mShowAnimationListener.onAnimationEnd(animation);
                }
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }
        });
    }

    private void createHideListener() {
        this.mHideAnimation.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationEnd(Animation animation) {
                if (SplashAnimation.this.mHideAnimationListener != null) {
                    SplashAnimation.this.mHideAnimationListener.onAnimationEnd(animation);
                }
                SplashAnimation.this.mSplashView.setVisibility(4);
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }
        });
    }
}
