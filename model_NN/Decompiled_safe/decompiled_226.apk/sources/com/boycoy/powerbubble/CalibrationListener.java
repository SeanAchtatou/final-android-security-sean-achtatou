package com.boycoy.powerbubble;

public interface CalibrationListener {
    void onCalibrationFinished();
}
