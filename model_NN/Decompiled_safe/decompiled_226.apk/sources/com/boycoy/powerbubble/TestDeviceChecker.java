package com.boycoy.powerbubble;

import android.content.Context;
import android.os.Build;
import android.provider.Settings;

public class TestDeviceChecker {
    private static TestDeviceChecker sInstance;
    private Context mContext;
    private boolean mIsTestDevice = false;
    private String mTestDeviceId;

    private TestDeviceChecker(Context context) {
        this.mContext = context;
        generateTestDeviceId();
    }

    public static void init(Context context) {
        sInstance = new TestDeviceChecker(context);
    }

    public static TestDeviceChecker getInstance() {
        return sInstance;
    }

    public String getTestDeviceId() {
        return this.mTestDeviceId;
    }

    public boolean isTestDevice() {
        return this.mIsTestDevice;
    }

    public void addTestDevice(String id) {
        this.mIsTestDevice = this.mIsTestDevice || id.equals(this.mTestDeviceId);
    }

    private void generateTestDeviceId() {
        char[] result = new char[20];
        int i = 0;
        for (String s : new String[]{Build.BOARD, Build.BRAND, Build.CPU_ABI, Build.DEVICE, Build.DISPLAY, Build.HOST, Build.ID, Build.MANUFACTURER, Build.MODEL, Build.PRODUCT, Build.TAGS, Build.TYPE, Build.USER, Settings.Secure.getString(this.mContext.getContentResolver(), "android_id")}) {
            if (s != null) {
                for (char c : s.toCharArray()) {
                    result[i] = (char) (result[i] + (c % 10));
                    i = (i + 1) % 20;
                }
            }
        }
        int i2 = 0;
        for (char r : result) {
            result[i2] = Character.forDigit(r % 16, 16);
            i2++;
        }
        this.mTestDeviceId = String.valueOf(result);
    }
}
