package com.boycoy.powerbubble;

import android.content.Context;
import android.util.AttributeSet;
import com.boycoy.powerbubble.views.LcdSurfaceView;
import com.boycoy.powerbubble.views.OnLcdDrawListener;
import java.util.Timer;
import java.util.TimerTask;

public class CalibrateButton extends TouchableButtonImage implements CalibrationListener, LockListener {
    /* access modifiers changed from: private */
    public Calibrator mCalibrator = null;
    /* access modifiers changed from: private */
    public Context mContext;
    /* access modifiers changed from: private */
    public boolean mIsCalibrating = false;
    private boolean mIsHoldEnabled = false;
    /* access modifiers changed from: private */
    public LcdSurfaceView mLcdSurfaceView = null;
    private long mPressTime = 0;
    private Timer mTimer;

    public CalibrateButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
    }

    public void setLcdSurfaceView(LcdSurfaceView value) {
        this.mLcdSurfaceView = value;
        if (this.mLcdSurfaceView != null) {
            this.mLcdSurfaceView.setOnLcdDrawListener(new OnLcdDrawListener() {
                public void onTextRepeatCountChanged(int textRepeatCount) {
                    if (CalibrateButton.this.mCalibrator == null) {
                        return;
                    }
                    if (CalibrateButton.this.mIsCalibrating) {
                        CalibrateButton.this.mLcdSurfaceView.setMessage(CalibrateButton.this.mContext.getResources().getString(R.string.wait_lcd));
                        CalibrateButton.this.mCalibrator.startCalibration();
                        return;
                    }
                    CalibrateButton.this.mLcdSurfaceView.setMessage(null);
                }
            });
        }
    }

    public void setCalibrator(Calibrator value) {
        this.mCalibrator = value;
    }

    public void onPress() {
        this.mPressTime = System.currentTimeMillis();
        if (!this.mIsCalibrating && !this.mIsHoldEnabled) {
            this.mTimer = new Timer();
            this.mTimer.schedule(new TimerTask() {
                public void run() {
                    CalibrateButton.this.mIsCalibrating = true;
                    CalibrateButton.this.mLcdSurfaceView.setMessage(CalibrateButton.this.mContext.getResources().getString(R.string.calibration_lcd));
                }
            }, 1000);
        }
    }

    public void onRelease() {
        if (this.mTimer != null) {
            this.mTimer.cancel();
            this.mTimer = null;
        }
        if (!this.mIsCalibrating && System.currentTimeMillis() - this.mPressTime < 500) {
            if (this.mIsHoldEnabled) {
                this.mLcdSurfaceView.setMessage(this.mContext.getResources().getString(R.string.disable_lock_to_calibrate));
            } else {
                this.mLcdSurfaceView.setMessage(this.mContext.getResources().getString(R.string.hold_to_calibrate_lcd));
            }
        }
    }

    public void onCalibrationFinished() {
        this.mLcdSurfaceView.setMessage(null);
        this.mIsCalibrating = false;
        this.mTimer = null;
    }

    public void toggleLock() {
        this.mIsHoldEnabled = !this.mIsHoldEnabled;
    }

    public boolean isLockPossible() {
        return true;
    }

    public void setLockEnabled(boolean value) {
        this.mIsHoldEnabled = value;
    }
}
