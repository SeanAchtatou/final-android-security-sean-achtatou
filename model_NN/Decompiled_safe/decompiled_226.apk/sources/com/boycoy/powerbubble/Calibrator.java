package com.boycoy.powerbubble;

import com.boycoy.powerbubble.SettingsManager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class Calibrator {
    private static Calibrator sInstance = null;
    private HashMap<Integer, Float> mCalibratedValues;
    private CalibrationListener mCalibrationListener;
    private ArrayList<Float> mCalibrationSensorValues;
    private boolean mIsCalibrationEnabled;
    private Integer mLastRotation;

    public static Calibrator getInstance() {
        if (sInstance == null) {
            sInstance = new Calibrator();
        }
        return sInstance;
    }

    private Calibrator() {
        this.mCalibrationSensorValues = null;
        this.mIsCalibrationEnabled = false;
        this.mLastRotation = null;
        this.mCalibrationListener = null;
        this.mCalibrationSensorValues = new ArrayList<>();
        this.mCalibratedValues = new HashMap<>();
        this.mCalibratedValues.put(0, Float.valueOf(SettingsManager.getFloat(SettingsManager.Settings.CALIBRATION_0)));
        this.mCalibratedValues.put(1, Float.valueOf(SettingsManager.getFloat(SettingsManager.Settings.CALIBRATION_90)));
        this.mCalibratedValues.put(2, Float.valueOf(SettingsManager.getFloat(SettingsManager.Settings.CALIBRATION_180)));
        this.mCalibratedValues.put(3, Float.valueOf(SettingsManager.getFloat(SettingsManager.Settings.CALIBRATION_270)));
    }

    public void setCalibrationListener(CalibrationListener calibrationListener) {
        this.mCalibrationListener = calibrationListener;
    }

    public float getCorrection(int rotation) {
        Float value = this.mCalibratedValues.get(Integer.valueOf(rotation));
        if (value != null) {
            return value.floatValue();
        }
        return 0.0f;
    }

    public void resetCalibration() {
        commitCorrection(0, 0.0f);
        commitCorrection(1, 0.0f);
        commitCorrection(2, 0.0f);
        commitCorrection(3, 0.0f);
    }

    public void startCalibration() {
        this.mIsCalibrationEnabled = true;
    }

    public void setNotCalibratedAngleX(float angleNotCalibrated, int rotation) {
        if (!(this.mLastRotation == null || rotation == this.mLastRotation.intValue())) {
            this.mIsCalibrationEnabled = false;
        }
        if (this.mIsCalibrationEnabled) {
            this.mCalibrationSensorValues.add(Float.valueOf(angleNotCalibrated));
            if (this.mCalibrationSensorValues.size() >= 60) {
                this.mIsCalibrationEnabled = false;
                this.mLastRotation = null;
                Iterator<Float> iteratorSensor = this.mCalibrationSensorValues.iterator();
                float sensorAverage = 0.0f;
                while (iteratorSensor.hasNext()) {
                    sensorAverage += iteratorSensor.next().floatValue();
                }
                float sensorAverage2 = sensorAverage / ((float) this.mCalibrationSensorValues.size());
                this.mCalibrationSensorValues.clear();
                commitCorrection(rotation, sensorAverage2);
                if (this.mCalibrationListener != null) {
                    this.mCalibrationListener.onCalibrationFinished();
                }
            }
        }
    }

    private void commitCorrection(int rotation, float sensorAverage) {
        this.mCalibratedValues.put(Integer.valueOf(rotation), Float.valueOf(sensorAverage));
        SettingsManager.Settings setting = SettingsManager.Settings.CALIBRATION_0;
        switch (rotation) {
            case 1:
                setting = SettingsManager.Settings.CALIBRATION_90;
                break;
            case 2:
                setting = SettingsManager.Settings.CALIBRATION_180;
                break;
            case 3:
                setting = SettingsManager.Settings.CALIBRATION_270;
                break;
        }
        SettingsManager.setFloat(setting, sensorAverage);
    }
}
