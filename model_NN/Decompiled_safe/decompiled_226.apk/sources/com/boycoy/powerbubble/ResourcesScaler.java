package com.boycoy.powerbubble;

import android.app.Activity;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class ResourcesScaler {
    private Activity mActivity;
    private float mResolutionHeightMultipier = Commons.getInstance().getResolutionHeightMultiplier();
    private float mResolutionWidthMultipier = Commons.getInstance().getResolutionWidthMultiplier();

    public ResourcesScaler(Activity activity) {
        this.mActivity = activity;
    }

    public void scaleResources() {
        scalePowerBubbleResources();
        scaleButtonsResources();
        scaleLcdResources();
    }

    public void scaleSplashResources(int rootViewWidth, int rootViewHeight) {
        RelativeLayout splashPowerBubbleSpacer = (RelativeLayout) this.mActivity.findViewById(R.id.splash_powerbubble_spacer);
        if (splashPowerBubbleSpacer != null) {
            ViewGroup.LayoutParams layoutParams = splashPowerBubbleSpacer.getLayoutParams();
            layoutParams.height = (int) (((float) layoutParams.height) * this.mResolutionHeightMultipier);
        }
        ImageView splashBackground = (ImageView) this.mActivity.findViewById(R.id.splash_background);
        Matrix splashBackgroundMatrix = new Matrix();
        splashBackgroundMatrix.setScale(this.mResolutionWidthMultipier, this.mResolutionHeightMultipier);
        Drawable splashBackgroundDrawable = splashBackground.getDrawable();
        splashBackgroundMatrix.postTranslate((((float) rootViewWidth) - (this.mResolutionWidthMultipier * ((float) splashBackgroundDrawable.getIntrinsicWidth()))) / 2.0f, ((float) rootViewHeight) - (this.mResolutionHeightMultipier * ((float) splashBackgroundDrawable.getIntrinsicHeight())));
        splashBackground.setImageMatrix(splashBackgroundMatrix);
        ImageView splashPowerBubble = (ImageView) this.mActivity.findViewById(R.id.splash_powerbubble);
        Matrix splashPowerBubbleMatrix = new Matrix();
        splashPowerBubbleMatrix.setScale(this.mResolutionWidthMultipier, this.mResolutionHeightMultipier);
        Drawable splashPowerBubbleDrawable = splashPowerBubble.getDrawable();
        RelativeLayout.LayoutParams splashPowerBubbleLayoutParams = (RelativeLayout.LayoutParams) splashPowerBubble.getLayoutParams();
        splashPowerBubbleLayoutParams.width = (int) (this.mResolutionWidthMultipier * ((float) splashPowerBubbleDrawable.getIntrinsicWidth()));
        splashPowerBubbleLayoutParams.height = (int) (this.mResolutionHeightMultipier * ((float) (splashPowerBubbleDrawable.getIntrinsicHeight() + 90)));
        splashPowerBubbleLayoutParams.topMargin = (int) (((float) splashPowerBubbleLayoutParams.topMargin) * this.mResolutionHeightMultipier);
        splashPowerBubbleLayoutParams.bottomMargin = (int) (((float) splashPowerBubbleLayoutParams.bottomMargin) * this.mResolutionHeightMultipier);
        splashPowerBubble.setImageMatrix(splashPowerBubbleMatrix);
        ImageView splashBoyCoy = (ImageView) this.mActivity.findViewById(R.id.splash_boycoy);
        Matrix splashBoyCoyMatrix = new Matrix();
        splashBoyCoyMatrix.setScale(this.mResolutionWidthMultipier, this.mResolutionHeightMultipier);
        Drawable splashBoyCoyDrawable = splashBoyCoy.getDrawable();
        RelativeLayout.LayoutParams splashBoyCoyLayoutParams = (RelativeLayout.LayoutParams) splashBoyCoy.getLayoutParams();
        splashBoyCoyLayoutParams.width = (int) (this.mResolutionWidthMultipier * ((float) splashBoyCoyDrawable.getIntrinsicWidth()));
        splashBoyCoyLayoutParams.height = (int) (this.mResolutionWidthMultipier * ((float) splashBoyCoyDrawable.getIntrinsicHeight()));
        splashBoyCoyLayoutParams.leftMargin = (int) (((float) splashBoyCoyLayoutParams.leftMargin) * this.mResolutionWidthMultipier);
        splashBoyCoy.setImageMatrix(splashBoyCoyMatrix);
    }

    private void scalePowerBubbleResources() {
        this.mActivity.findViewById(R.id.logopowerbubble).setPadding(0, 0, 0, (int) (((float) this.mActivity.findViewById(R.id.logopowerbubble).getPaddingBottom()) * this.mResolutionHeightMultipier));
    }

    private void scaleButtonsResources() {
        View buttonsSpacerCenter = this.mActivity.findViewById(R.id.buttonsspacercenter);
        if (buttonsSpacerCenter != null) {
            ViewGroup.LayoutParams layoutParams = buttonsSpacerCenter.getLayoutParams();
            layoutParams.width = (int) (((float) layoutParams.width) * this.mResolutionWidthMultipier);
        }
        View buttonsLayoutPort = this.mActivity.findViewById(R.id.buttonslayoutport);
        if (buttonsLayoutPort != null) {
            RelativeLayout.LayoutParams buttonsLayoutPortLayoutParams = (RelativeLayout.LayoutParams) buttonsLayoutPort.getLayoutParams();
            buttonsLayoutPortLayoutParams.topMargin = (int) (((float) buttonsLayoutPortLayoutParams.topMargin) * this.mResolutionHeightMultipier);
        }
        View buttonCalibrateView = this.mActivity.findViewById(R.id.buttoncalibrate);
        if (buttonCalibrateView != null) {
            RelativeLayout.LayoutParams buttonCalibrateViewLayoutParams = (RelativeLayout.LayoutParams) buttonCalibrateView.getLayoutParams();
            buttonCalibrateViewLayoutParams.leftMargin = (int) (((float) buttonCalibrateViewLayoutParams.leftMargin) * this.mResolutionWidthMultipier);
            buttonCalibrateViewLayoutParams.topMargin = (int) (((float) buttonCalibrateViewLayoutParams.topMargin) * this.mResolutionHeightMultipier);
            buttonCalibrateViewLayoutParams.rightMargin = (int) (((float) buttonCalibrateViewLayoutParams.rightMargin) * this.mResolutionWidthMultipier);
            buttonCalibrateViewLayoutParams.bottomMargin = (int) (((float) buttonCalibrateViewLayoutParams.bottomMargin) * this.mResolutionHeightMultipier);
        }
        View buttonHoldView = this.mActivity.findViewById(R.id.buttonhold);
        if (buttonHoldView != null) {
            RelativeLayout.LayoutParams buttonHoldViewLayoutParams = (RelativeLayout.LayoutParams) buttonHoldView.getLayoutParams();
            buttonHoldViewLayoutParams.leftMargin = (int) (((float) buttonHoldViewLayoutParams.leftMargin) * this.mResolutionWidthMultipier);
            buttonHoldViewLayoutParams.topMargin = (int) (((float) buttonHoldViewLayoutParams.topMargin) * this.mResolutionHeightMultipier);
            buttonHoldViewLayoutParams.rightMargin = (int) (((float) buttonHoldViewLayoutParams.rightMargin) * this.mResolutionWidthMultipier);
            buttonHoldViewLayoutParams.bottomMargin = (int) (((float) buttonHoldViewLayoutParams.bottomMargin) * this.mResolutionHeightMultipier);
            buttonHoldView.setPadding(0, 0, 0, (int) (((float) buttonHoldView.getPaddingBottom()) * this.mResolutionHeightMultipier));
        }
        View buttonSettingsView = this.mActivity.findViewById(R.id.buttonsettings);
        if (buttonSettingsView != null) {
            RelativeLayout.LayoutParams buttonSettingsViewLayoutParams = (RelativeLayout.LayoutParams) buttonSettingsView.getLayoutParams();
            buttonSettingsViewLayoutParams.leftMargin = (int) (((float) buttonSettingsViewLayoutParams.leftMargin) * this.mResolutionWidthMultipier);
            buttonSettingsViewLayoutParams.topMargin = (int) (((float) buttonSettingsViewLayoutParams.topMargin) * this.mResolutionHeightMultipier);
            buttonSettingsViewLayoutParams.rightMargin = (int) (((float) buttonSettingsViewLayoutParams.rightMargin) * this.mResolutionWidthMultipier);
            buttonSettingsViewLayoutParams.bottomMargin = (int) (((float) buttonSettingsViewLayoutParams.bottomMargin) * this.mResolutionHeightMultipier);
        }
        View logoBoyCoy = this.mActivity.findViewById(R.id.logoboycoy);
        if (logoBoyCoy != null) {
            logoBoyCoy.setPadding(0, (int) (((float) this.mActivity.findViewById(R.id.logoboycoy).getPaddingTop()) * this.mResolutionHeightMultipier), 0, (int) (((float) this.mActivity.findViewById(R.id.logoboycoy).getPaddingBottom()) * this.mResolutionHeightMultipier));
        }
    }

    private void scaleLcdResources() {
        this.mActivity.findViewById(R.id.lcdlayout).setPadding(0, (int) (((float) this.mActivity.findViewById(R.id.lcdlayout).getPaddingTop()) * this.mResolutionHeightMultipier), 0, 0);
    }
}
