package com.boycoy.powerbubble;

import android.app.Activity;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;

public class Tracker {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$boycoy$powerbubble$Tracker$DonateValue;
    private boolean mIsStarted = false;
    private GoogleAnalyticsTracker mTracker;

    public enum DonateValue {
        DONATE,
        CONTINUE,
        BACK
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$boycoy$powerbubble$Tracker$DonateValue() {
        int[] iArr = $SWITCH_TABLE$com$boycoy$powerbubble$Tracker$DonateValue;
        if (iArr == null) {
            iArr = new int[DonateValue.values().length];
            try {
                iArr[DonateValue.BACK.ordinal()] = 3;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[DonateValue.CONTINUE.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[DonateValue.DONATE.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$com$boycoy$powerbubble$Tracker$DonateValue = iArr;
        }
        return iArr;
    }

    public void start(Activity activity) {
        if (!this.mIsStarted && !TestDeviceChecker.getInstance().isTestDevice()) {
            if (this.mTracker == null) {
                this.mTracker = GoogleAnalyticsTracker.getInstance();
            }
            this.mTracker.start("UA-22231731-4", 180, activity);
            this.mIsStarted = true;
        }
    }

    public void dispatch() {
        if (this.mIsStarted) {
            this.mTracker.dispatch();
        }
    }

    public void stop() {
        if (this.mIsStarted) {
            this.mIsStarted = false;
            this.mTracker.stop();
        }
    }

    public void trackPageView(String pagePath) {
        if (this.mIsStarted) {
            this.mTracker.trackPageView(pagePath);
        }
    }

    public void setDonationVariable(DonateValue donateValue) {
        if (this.mIsStarted) {
            String donateText = null;
            switch ($SWITCH_TABLE$com$boycoy$powerbubble$Tracker$DonateValue()[donateValue.ordinal()]) {
                case 1:
                    donateText = "donate";
                    break;
                case 3:
                    donateText = "back";
                    break;
            }
            this.mTracker.setCustomVar(1, "Donate", donateText, 1);
        }
    }
}
