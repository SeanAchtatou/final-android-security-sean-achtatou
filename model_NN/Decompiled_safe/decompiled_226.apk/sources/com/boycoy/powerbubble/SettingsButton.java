package com.boycoy.powerbubble;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;

public class SettingsButton extends TouchableButtonImage {
    private Context mContext = null;

    public SettingsButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
    }

    public void onRelease() {
        this.mContext.startActivity(new Intent(this.mContext, CustomPreferences.class));
    }
}
