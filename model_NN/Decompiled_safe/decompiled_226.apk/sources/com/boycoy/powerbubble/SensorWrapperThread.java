package com.boycoy.powerbubble;

import com.boycoy.powerbubble.SettingsManager;

public class SensorWrapperThread extends ConstantFpsThread {
    private static final int MAX_LISTENERS_COUNT = 10;
    private AngleXListener[] mAngleXListeners;
    private int mAngleXListenersCount = 0;
    private AngleYListener[] mAngleYListeners;
    private int mAngleYListenersCount = 0;
    private Calibrator mCalibrator;
    private int mRotation = 0;
    private float mSensorX = 0.0f;
    private Average mSensorXAverage;
    private float mSensorY = 0.0f;
    private Average mSensorYAverage;
    private boolean mShowDecimals;

    public SensorWrapperThread(Calibrator calibrator) {
        this.mCalibrator = calibrator;
        this.mSensorXAverage = new Average(15);
        this.mSensorYAverage = new Average(15);
        this.mShowDecimals = SettingsManager.getBoolean(SettingsManager.Settings.ENABLE_DECIMAL, true);
        this.mAngleXListeners = new AngleXListener[MAX_LISTENERS_COUNT];
        this.mAngleYListeners = new AngleYListener[MAX_LISTENERS_COUNT];
    }

    public void setSensors(float sensorX, float sensorY, int rotation) {
        synchronized (this) {
            this.mSensorX = sensorX;
            this.mSensorY = sensorY;
            this.mRotation = rotation;
        }
    }

    public void addAngleXListener(AngleXListener angleXListener) {
        AngleXListener[] angleXListenerArr = this.mAngleXListeners;
        int i = this.mAngleXListenersCount;
        this.mAngleXListenersCount = i + 1;
        angleXListenerArr[i] = angleXListener;
    }

    public void addAngleYListener(AngleYListener angleYListener) {
        AngleYListener[] angleYListenerArr = this.mAngleYListeners;
        int i = this.mAngleYListenersCount;
        this.mAngleYListenersCount = i + 1;
        angleYListenerArr[i] = angleYListener;
    }

    /* access modifiers changed from: protected */
    public void onFrameDraw(long elapsedTime) {
        int rotation;
        if (this.mAngleXListenersCount > 0 || this.mAngleYListenersCount > 0) {
            synchronized (this) {
                if (this.mSensorX <= 9.80665f) {
                    this.mSensorXAverage.add(this.mSensorX);
                }
                if (this.mSensorY <= 9.80665f) {
                    this.mSensorYAverage.add(this.mSensorY);
                }
                rotation = this.mRotation;
            }
            float notCalibratedAngleX = (this.mSensorXAverage.get() * 90.0f) / 9.80665f;
            float angleX = notCalibratedAngleX - this.mCalibrator.getCorrection(rotation);
            float angleY = (this.mSensorYAverage.get() * 90.0f) / 9.80665f;
            if (this.mAngleXListenersCount > 0) {
                for (int i = 0; i < this.mAngleXListenersCount; i++) {
                    this.mAngleXListeners[i].setAngleX(this.mShowDecimals ? angleX : round(angleX));
                }
            }
            if (this.mAngleYListenersCount > 0) {
                for (int i2 = 0; i2 < this.mAngleYListenersCount; i2++) {
                    this.mAngleYListeners[i2].setAngleY(-angleY);
                }
            }
            if (this.mCalibrator != null) {
                this.mCalibrator.setNotCalibratedAngleX(notCalibratedAngleX, rotation);
            }
        }
    }

    private float round(float angle) {
        float roundedAngle = angle;
        if (angle < 0.0f) {
            return (float) Math.ceil((double) angle);
        }
        if (angle > 0.0f) {
            return (float) Math.floor((double) angle);
        }
        return roundedAngle;
    }
}
