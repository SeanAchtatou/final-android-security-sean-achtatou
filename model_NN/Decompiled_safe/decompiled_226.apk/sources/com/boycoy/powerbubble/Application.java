package com.boycoy.powerbubble;

import com.boycoy.powerbubble.views.LcdCharactersCache;

public class Application {
    private LcdCharactersCache mLcdCharactersCache;

    public Application() {
        this.mLcdCharactersCache = null;
        this.mLcdCharactersCache = new LcdCharactersCache();
    }

    public LcdCharactersCache getLcdCharactersCache() {
        return this.mLcdCharactersCache;
    }
}
