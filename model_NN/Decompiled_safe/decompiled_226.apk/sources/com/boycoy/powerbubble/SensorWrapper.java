package com.boycoy.powerbubble;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import java.util.ArrayList;
import java.util.Iterator;

public class SensorWrapper implements SensorEventListener {
    private ArrayList<AngleXListener> mAngleXListeners;
    private ArrayList<AngleYListener> mAngleYListeners;
    private Calibrator mCalibrator;
    private SensorManager mSensorManager;
    private SensorWrapperThread mSensorWrapperThread = null;
    private float mSensorX = 0.0f;
    private float mSensorY = 0.0f;

    public SensorWrapper(Context context, Calibrator calibrator) {
        this.mSensorManager = (SensorManager) context.getSystemService("sensor");
        this.mCalibrator = calibrator;
        this.mAngleXListeners = new ArrayList<>();
        this.mAngleYListeners = new ArrayList<>();
    }

    public void addAngleXListener(AngleXListener angleXListener) {
        this.mAngleXListeners.add(angleXListener);
    }

    public void addAngleYListener(AngleYListener angleYListener) {
        this.mAngleYListeners.add(angleYListener);
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    public void onSensorChanged(SensorEvent sensorEvent) {
        if (sensorEvent.sensor.getType() == 1) {
            int rotation = Commons.getInstance().getRotation();
            switch (rotation) {
                case 0:
                    this.mSensorX = sensorEvent.values[0];
                    this.mSensorY = sensorEvent.values[1];
                    break;
                case 1:
                    this.mSensorX = -sensorEvent.values[1];
                    this.mSensorY = sensorEvent.values[0];
                    break;
                case 2:
                    this.mSensorX = -sensorEvent.values[0];
                    this.mSensorY = -sensorEvent.values[1];
                    break;
                case 3:
                    this.mSensorX = sensorEvent.values[1];
                    this.mSensorY = -sensorEvent.values[0];
                    break;
            }
            if (this.mSensorWrapperThread != null) {
                this.mSensorWrapperThread.setSensors(this.mSensorX, this.mSensorY, rotation);
            }
        }
    }

    public void registerSensors() {
        this.mSensorManager.registerListener(this, this.mSensorManager.getDefaultSensor(1), 0);
        startThread();
    }

    public void unregisterSensors() {
        stopThread();
        this.mSensorManager.unregisterListener(this);
    }

    private void startThread() {
        if (this.mSensorWrapperThread == null) {
            this.mSensorWrapperThread = new SensorWrapperThread(this.mCalibrator);
            Iterator<AngleXListener> it = this.mAngleXListeners.iterator();
            while (it.hasNext()) {
                this.mSensorWrapperThread.addAngleXListener(it.next());
            }
            Iterator<AngleYListener> it2 = this.mAngleYListeners.iterator();
            while (it2.hasNext()) {
                this.mSensorWrapperThread.addAngleYListener(it2.next());
            }
            this.mSensorWrapperThread.setFps(30);
            this.mSensorWrapperThread.setRunning(true);
            this.mSensorWrapperThread.start();
        }
    }

    private void stopThread() {
        if (this.mSensorWrapperThread != null) {
            this.mSensorWrapperThread.setRunning(false);
            boolean retry = true;
            while (retry) {
                try {
                    this.mSensorWrapperThread.join();
                    retry = false;
                } catch (InterruptedException e) {
                }
            }
            this.mSensorWrapperThread = null;
        }
    }
}
