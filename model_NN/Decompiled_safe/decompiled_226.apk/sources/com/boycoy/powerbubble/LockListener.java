package com.boycoy.powerbubble;

public interface LockListener {
    boolean isLockPossible();

    void setLockEnabled(boolean z);

    void toggleLock();
}
