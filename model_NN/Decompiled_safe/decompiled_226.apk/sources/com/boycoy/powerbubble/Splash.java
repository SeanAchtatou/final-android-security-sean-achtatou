package com.boycoy.powerbubble;

import android.app.Activity;
import android.view.animation.AnimationUtils;

public class Splash {
    private SplashAnimation mSplashAnimation;

    public Splash(Activity activity) {
        this.mSplashAnimation = new SplashAnimation(activity.findViewById(R.id.splash), AnimationUtils.loadAnimation(activity, R.anim.splash_in), AnimationUtils.loadAnimation(activity, R.anim.splash_out));
    }

    public void show() {
        this.mSplashAnimation.showSplash();
    }

    public void hide() {
        this.mSplashAnimation.hideSplash();
    }

    public void setOnShowAnimationListener(SplashAnimationListener splashAnimationListener) {
        this.mSplashAnimation.setOnShowAnimationListener(splashAnimationListener);
    }

    public void setOnHideAnimationListener(SplashAnimationListener splashAnimationListener) {
        this.mSplashAnimation.setOnHideAnimationListener(splashAnimationListener);
    }
}
