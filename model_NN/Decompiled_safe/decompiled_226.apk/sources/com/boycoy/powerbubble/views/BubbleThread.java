package com.boycoy.powerbubble.views;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.view.SurfaceHolder;
import com.boycoy.powerbubble.Commons;
import com.boycoy.powerbubble.ConstantFpsThread;
import com.boycoy.powerbubble.LockListener;
import com.boycoy.powerbubble.R;

public class BubbleThread extends ConstantFpsThread implements LockListener {
    private Bitmap[] mBubbleBitmaps;
    private Paint mBubblePaint = null;
    private BubbleParameters mBubbleParameters = null;
    private Bitmap mCroppedBackgroundBitmap;
    private Bitmap mCurrentBubbleBitmap;
    private float mCurrentX = -1.0f;
    private float mCurrentY = -1.0f;
    private Bitmap mLiquidBackgroundBitmap;
    private float mLiquidBackgroundGlassHeight;
    private Bitmap mLiquidBorderBackgroundBitmap;
    private Bitmap mLiquidForegroundBitmap;
    private int mLiquidOffsetY;
    private SurfaceHolder mSurfaceHolder;

    public BubbleThread(SurfaceHolder surfaceHolder, Resources resources, Bitmap fullBackgroundBitmap, int visibleBackgroundWidth, int visibleBackgroundHeight) {
        this.mSurfaceHolder = surfaceHolder;
        this.mBubbleBitmaps = new Bitmap[]{((BitmapDrawable) resources.getDrawable(R.drawable.bubble_10)).getBitmap(), ((BitmapDrawable) resources.getDrawable(R.drawable.bubble_9)).getBitmap(), ((BitmapDrawable) resources.getDrawable(R.drawable.bubble_8)).getBitmap(), ((BitmapDrawable) resources.getDrawable(R.drawable.bubble_7)).getBitmap(), ((BitmapDrawable) resources.getDrawable(R.drawable.bubble_6)).getBitmap(), ((BitmapDrawable) resources.getDrawable(R.drawable.bubble_5)).getBitmap(), ((BitmapDrawable) resources.getDrawable(R.drawable.bubble_4)).getBitmap(), ((BitmapDrawable) resources.getDrawable(R.drawable.bubble_3)).getBitmap(), ((BitmapDrawable) resources.getDrawable(R.drawable.bubble_2)).getBitmap(), ((BitmapDrawable) resources.getDrawable(R.drawable.bubble_1)).getBitmap()};
        this.mLiquidBorderBackgroundBitmap = ((BitmapDrawable) resources.getDrawable(R.drawable.liquid_border_background)).getBitmap();
        Bitmap scaledBackgroundBitmap = Bitmap.createScaledBitmap(fullBackgroundBitmap, visibleBackgroundWidth, visibleBackgroundHeight, true);
        this.mCroppedBackgroundBitmap = Bitmap.createBitmap(scaledBackgroundBitmap, (scaledBackgroundBitmap.getWidth() - this.mLiquidBorderBackgroundBitmap.getWidth()) / 2, (scaledBackgroundBitmap.getHeight() - this.mLiquidBorderBackgroundBitmap.getHeight()) / 2, this.mLiquidBorderBackgroundBitmap.getWidth(), this.mLiquidBorderBackgroundBitmap.getHeight());
        this.mLiquidBackgroundBitmap = ((BitmapDrawable) resources.getDrawable(R.drawable.liquid_background)).getBitmap();
        this.mCurrentBubbleBitmap = this.mBubbleBitmaps[0];
        this.mLiquidForegroundBitmap = ((BitmapDrawable) resources.getDrawable(R.drawable.liquid_foreground)).getBitmap();
        this.mLiquidBackgroundGlassHeight = 6.0f * Commons.getInstance().getResolutionHeightMultiplier();
        this.mBubblePaint = new Paint();
        this.mBubbleParameters = new BubbleParameters(this.mCurrentBubbleBitmap.getWidth(), this.mCurrentBubbleBitmap.getHeight(), this.mLiquidBackgroundBitmap.getWidth(), this.mLiquidBackgroundBitmap.getHeight() - ((int) (this.mLiquidBackgroundGlassHeight * 2.0f)));
        this.mBubbleParameters.setAnimationFramesCount(this.mBubbleBitmaps.length);
        this.mLiquidOffsetY = (this.mLiquidBorderBackgroundBitmap.getHeight() - this.mLiquidBackgroundBitmap.getHeight()) / 2;
    }

    public int getPostionX() {
        return this.mBubbleParameters.getX();
    }

    public void setPostionX(int value) {
        this.mBubbleParameters.setX(value);
        drawToSurfaceHolder();
    }

    public int getPostionY() {
        return this.mBubbleParameters.getY();
    }

    public void setPostionY(int value) {
        this.mBubbleParameters.setY(value);
        drawToSurfaceHolder();
    }

    public int getAnimationFrameY() {
        return this.mBubbleParameters.getAnimationFrame();
    }

    public void setAnimationFrameY(int value) {
        this.mBubbleParameters.setAnimationFrame(value);
        drawToSurfaceHolder();
    }

    public void setAngleX(float value) {
        this.mBubbleParameters.setAngleX((double) value);
    }

    public void setAngleY(float value) {
        this.mBubbleParameters.setAngleY((double) value);
    }

    public void setLockEnabled(boolean value) {
        setPaused(value);
    }

    public boolean isLockPossible() {
        return true;
    }

    public void toggleLock() {
        setPaused(!getPaused());
    }

    /* access modifiers changed from: protected */
    public void onFrameDraw(long elapsedTime) {
        this.mBubbleParameters.update(elapsedTime);
        if (this.mBubbleParameters.hasChanged()) {
            drawToSurfaceHolder();
        }
    }

    private void drawToSurfaceHolder() {
        this.mCurrentX = (float) this.mBubbleParameters.getX();
        this.mCurrentY = (float) this.mBubbleParameters.getY();
        Canvas canvas = null;
        try {
            canvas = this.mSurfaceHolder.lockCanvas(null);
            drawBubble(canvas);
            if (canvas != null) {
                this.mSurfaceHolder.unlockCanvasAndPost(canvas);
            }
        } catch (NullPointerException e) {
            if (canvas != null) {
                this.mSurfaceHolder.unlockCanvasAndPost(canvas);
            }
        } catch (Throwable th) {
            if (canvas != null) {
                this.mSurfaceHolder.unlockCanvasAndPost(canvas);
            }
            throw th;
        }
    }

    private synchronized void drawBubble(Canvas canvas) {
        canvas.drawBitmap(this.mCroppedBackgroundBitmap, 0.0f, 0.0f, this.mBubblePaint);
        canvas.drawBitmap(this.mLiquidBorderBackgroundBitmap, 0.0f, 0.0f, this.mBubblePaint);
        canvas.drawBitmap(this.mLiquidBackgroundBitmap, 0.0f, (float) this.mLiquidOffsetY, this.mBubblePaint);
        this.mCurrentBubbleBitmap = this.mBubbleBitmaps[this.mBubbleParameters.getAnimationFrame()];
        canvas.drawBitmap(this.mCurrentBubbleBitmap, this.mCurrentX, ((float) this.mLiquidOffsetY) + this.mLiquidBackgroundGlassHeight + this.mCurrentY, this.mBubblePaint);
        canvas.drawBitmap(this.mLiquidForegroundBitmap, 0.0f, (float) this.mLiquidOffsetY, this.mBubblePaint);
    }
}
