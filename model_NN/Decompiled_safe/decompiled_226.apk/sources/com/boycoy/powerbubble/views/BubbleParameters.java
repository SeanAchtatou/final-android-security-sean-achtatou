package com.boycoy.powerbubble.views;

import com.boycoy.powerbubble.Commons;

public class BubbleParameters {
    private static final double CONTAINER_MAX_ANGLE_X = 20.0d;
    private static final double CONTAINER_MAX_ANGLE_Y = 16.0d;
    private double mAngleX = 0.0d;
    private double mAngleY = 0.0d;
    private double mAnimationFrame = 0.0d;
    private int mAnimationFramesCount = 1;
    private BubblePhysics mBubblePhysicsX = null;
    private BubblePhysics mBubblePhysicsY = null;
    private BubblePosition mBubblePositionX = null;
    private BubblePosition mBubblePositionY = null;
    private double mLastAnimationFrame = 0.0d;
    private int mLastX = 0;
    private int mLastY = 0;
    private float mResolutionHeightMultiplier;

    public BubbleParameters(int bubbleSizeX, int bubbleSizeY, int conatinerWidth, int containerHeight) {
        this.mBubblePositionX = new BubblePosition(bubbleSizeX, conatinerWidth, CONTAINER_MAX_ANGLE_X);
        this.mBubblePositionY = new BubblePosition(bubbleSizeY, containerHeight, CONTAINER_MAX_ANGLE_Y);
        this.mResolutionHeightMultiplier = Commons.getInstance().getResolutionHeightMultiplier();
        this.mBubblePhysicsX = new BubblePhysics(CONTAINER_MAX_ANGLE_X, Commons.getInstance().getResolutionWidthMultiplier(), false);
        this.mBubblePhysicsY = new BubblePhysics(CONTAINER_MAX_ANGLE_Y, Commons.getInstance().getResolutionHeightMultiplier(), false);
    }

    public void setAngleX(double value) {
        this.mAngleX = value;
    }

    public void setAngleY(double value) {
        this.mAngleY = value;
    }

    public void update(long elapsedTime) {
        this.mBubblePositionX.calculatePositions(this.mBubblePhysicsX.getDelta(this.mAngleX, this.mBubblePositionX.getContainerAngleByCenterPostion(), elapsedTime));
        double deltaY = (this.mBubblePhysicsY.getDelta(this.mAngleY, this.mBubblePositionY.getContainerAngleByCenterPostion(), elapsedTime) * ((double) this.mBubblePositionY.getContainerSize())) / ((double) this.mBubblePositionX.getContainerSize());
        if (((int) this.mAnimationFrame) == 0) {
            this.mBubblePositionY.calculatePositions(deltaY);
        }
        if (getY() == 0) {
            this.mAnimationFrame -= deltaY;
            if (this.mAnimationFrame < 0.0d) {
                this.mAnimationFrame = 0.0d;
            } else if (this.mAnimationFrame > ((double) (((float) (this.mAnimationFramesCount - 1)) * this.mResolutionHeightMultiplier))) {
                this.mAnimationFrame = (double) (((float) (this.mAnimationFramesCount - 1)) * this.mResolutionHeightMultiplier);
            }
        }
    }

    public boolean hasChanged() {
        int currentX = getX();
        int currentY = getY();
        if (this.mLastX == currentX && this.mLastY == currentY && ((int) this.mLastAnimationFrame) == ((int) this.mAnimationFrame)) {
            return false;
        }
        this.mLastX = currentX;
        this.mLastY = currentY;
        this.mLastAnimationFrame = this.mAnimationFrame;
        return true;
    }

    public int getX() {
        return (int) this.mBubblePositionX.getPositionLeft();
    }

    public void setX(int value) {
        this.mBubblePositionX.setPostionLeft(value);
    }

    public int getY() {
        return (int) this.mBubblePositionY.getPositionLeft();
    }

    public void setY(int value) {
        this.mBubblePositionY.setPostionLeft(value);
    }

    public int getAnimationFrame() {
        return (int) (this.mAnimationFrame * ((double) (1.0f / this.mResolutionHeightMultiplier)));
    }

    public void setAnimationFrame(int value) {
        this.mAnimationFrame = (double) value;
    }

    public void setAnimationFramesCount(int length) {
        if (length >= 1) {
            this.mAnimationFramesCount = length;
        }
    }
}
