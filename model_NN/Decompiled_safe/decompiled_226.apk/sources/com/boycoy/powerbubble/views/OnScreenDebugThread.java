package com.boycoy.powerbubble.views;

import android.widget.TextView;
import com.boycoy.powerbubble.ConstantFpsThread;
import java.util.HashMap;
import java.util.Map;

public class OnScreenDebugThread extends ConstantFpsThread {
    private final HashMap<String, String> mLogLines;
    /* access modifiers changed from: private */
    public String mMessage;
    /* access modifiers changed from: private */
    public final TextView mTextView;
    private final Runnable mUpdateText = new Runnable() {
        public void run() {
            OnScreenDebugThread.this.mTextView.setText(OnScreenDebugThread.this.mMessage);
        }
    };

    public OnScreenDebugThread(TextView textView, HashMap<String, String> logLines) {
        this.mTextView = textView;
        this.mLogLines = logLines;
    }

    /* access modifiers changed from: protected */
    public void onFrameDraw(long elapsedTime) {
        this.mMessage = new String();
        int i = 0;
        for (Map.Entry<String, String> entry : this.mLogLines.entrySet()) {
            this.mMessage = String.valueOf(this.mMessage) + (String.valueOf(i) + ": [" + ((String) entry.getKey()) + "] " + ((String) entry.getValue())) + "\n";
            i++;
        }
        this.mTextView.post(this.mUpdateText);
    }
}
