package com.boycoy.powerbubble.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import java.util.HashMap;

public class OnScreenDebugView extends TextView {
    /* access modifiers changed from: private */
    public boolean mIsEnabled;
    private HashMap<String, String> mLogLines = new HashMap<>();
    private OnScreenDebugThread mOnScreenDebugThread;

    public OnScreenDebugView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setEnabled(false);
        setBackgroundColor(-16777216);
        setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (OnScreenDebugView.this.mIsEnabled) {
                    OnScreenDebugView.this.setEnabled(false);
                } else {
                    OnScreenDebugView.this.setEnabled(true);
                }
            }
        });
    }

    public boolean isEnabled() {
        return this.mIsEnabled;
    }

    public void setEnabled(boolean value) {
        this.mIsEnabled = value;
        setVisibility(value ? 0 : 8);
    }

    public synchronized void setLog(String id, int message) {
        if (this.mIsEnabled) {
            this.mLogLines.put(id, Integer.toString(message));
        }
    }

    public synchronized void setLog(String id, Float message) {
        if (this.mIsEnabled) {
            this.mLogLines.put(id, Float.toString(message.floatValue()));
        }
    }

    public synchronized void setLog(String id, Double message) {
        if (this.mIsEnabled) {
            this.mLogLines.put(id, Double.toString(message.doubleValue()));
        }
    }

    public synchronized void setLog(String id, String message) {
        if (this.mIsEnabled) {
            this.mLogLines.put(id, message);
        }
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int visibility) {
        super.onWindowVisibilityChanged(visibility);
        if (visibility != 0 || !this.mIsEnabled) {
            if (this.mOnScreenDebugThread != null) {
                this.mOnScreenDebugThread.setRunning(false);
            }
        } else if (this.mOnScreenDebugThread == null || !this.mOnScreenDebugThread.isAlive()) {
            this.mOnScreenDebugThread = new OnScreenDebugThread(this, this.mLogLines);
            this.mOnScreenDebugThread.setRunning(true);
            this.mOnScreenDebugThread.setFps(5);
            this.mOnScreenDebugThread.start();
        }
    }
}
