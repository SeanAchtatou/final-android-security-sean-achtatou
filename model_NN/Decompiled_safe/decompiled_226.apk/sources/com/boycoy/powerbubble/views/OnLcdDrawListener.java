package com.boycoy.powerbubble.views;

public interface OnLcdDrawListener {
    void onTextRepeatCountChanged(int i);
}
