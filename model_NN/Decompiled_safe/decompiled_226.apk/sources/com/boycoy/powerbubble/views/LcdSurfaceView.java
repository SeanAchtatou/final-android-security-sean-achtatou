package com.boycoy.powerbubble.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import com.boycoy.powerbubble.AngleXListener;
import com.boycoy.powerbubble.LockListener;
import com.boycoy.powerbubble.R;

public class LcdSurfaceView extends SurfaceView implements SurfaceHolder.Callback, LockListener, AngleXListener {
    private int mBackgroundHeight;
    private int mBackgroundWidth;
    private Context mContext;
    private float mInitialAngleX = 0.0f;
    private boolean mInitialHoldEnabled = false;
    private boolean mIsSoundEnabled = false;
    private LcdCharactersCache mLcdCharactersCache = null;
    private LcdThread mLcdThread = null;
    private OnLcdDrawListener mOnLcdDrawListener = null;

    public LcdSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        Bitmap backgroundSrcBitmap = ((BitmapDrawable) context.getResources().getDrawable(R.drawable.lcd_background)).getBitmap();
        this.mBackgroundWidth = backgroundSrcBitmap.getWidth();
        this.mBackgroundHeight = backgroundSrcBitmap.getHeight();
        getHolder().addCallback(this);
    }

    public void setLcdCharactersCache(LcdCharactersCache lcdCharactersCache) {
        this.mLcdCharactersCache = lcdCharactersCache;
    }

    public void setEnableSound(boolean value) {
        this.mIsSoundEnabled = value;
        if (this.mLcdThread != null) {
            this.mLcdThread.setEnableSound(this.mIsSoundEnabled);
        }
    }

    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        if (this.mLcdThread == null) {
            this.mLcdThread = new LcdThread(surfaceHolder, this.mContext, this.mLcdCharactersCache);
            this.mLcdThread.setOnLcdDrawListener(this.mOnLcdDrawListener);
            this.mLcdThread.setFps(40);
            this.mLcdThread.setEnableSound(this.mIsSoundEnabled);
            this.mLcdThread.setRunning(true);
            this.mLcdThread.setInitialAngleX(this.mInitialAngleX);
            this.mLcdThread.setLockEnabled(this.mInitialHoldEnabled);
            this.mLcdThread.start();
        }
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        if (this.mLcdThread != null) {
            this.mLcdThread.setRunning(false);
            boolean retry = true;
            while (retry) {
                try {
                    this.mLcdThread.join();
                    retry = false;
                } catch (InterruptedException e) {
                }
            }
            this.mLcdThread = null;
        }
    }

    public void setOnLcdDrawListener(OnLcdDrawListener onLcdDrawListener) {
        this.mOnLcdDrawListener = onLcdDrawListener;
        if (this.mLcdThread != null) {
            this.mLcdThread.setOnLcdDrawListener(onLcdDrawListener);
        }
    }

    public String getMessage() {
        return this.mLcdThread.getMessage();
    }

    public void setMessage(String message) {
        if (this.mLcdThread != null) {
            this.mLcdThread.setMessage(message);
        }
    }

    public float getAngleX() {
        float angle = this.mInitialAngleX;
        if (this.mLcdThread != null) {
            return this.mLcdThread.getAngleX();
        }
        return angle;
    }

    public void setAngleX(float angle) {
        if (this.mLcdThread != null) {
            this.mLcdThread.setAngleX(angle);
        }
    }

    public void setInitialAngleX(float angle) {
        this.mInitialAngleX = angle;
    }

    public void setLockEnabled(boolean value) {
        if (this.mLcdThread != null) {
            this.mLcdThread.setLockEnabled(value);
        } else {
            this.mInitialHoldEnabled = value;
        }
    }

    public boolean isLockPossible() {
        if (this.mLcdThread != null) {
            return this.mLcdThread.isLockPossible();
        }
        return false;
    }

    public void toggleLock() {
        if (this.mLcdThread != null) {
            this.mLcdThread.toggleLock();
        } else {
            this.mInitialHoldEnabled = !this.mInitialHoldEnabled;
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(this.mBackgroundWidth, this.mBackgroundHeight);
    }
}
