package com.boycoy.powerbubble.views;

import android.util.Log;

public class CopyOfBubblePhysics {
    private static final double AIR_DENSITY = 1.2000000476837158d;
    private static final double DRAG_COEFFICIENT = 0.10000000149011612d;
    private static final double VOLUME = 9.999999974752427E-7d;
    private static final double WATER_DENSITY = 998.0d;
    private boolean mIsDebugEnabled;
    private double mLastAcceleration = 0.0d;
    private final double mMaxAngle;
    private final float mResolutionMultipler;
    private final double mResultantForce;
    private double mSpeed = 0.0d;
    private double mTotalMass;

    public CopyOfBubblePhysics(double maxAngle, float resolutionMultipler, boolean isDebugEnabled) {
        this.mMaxAngle = maxAngle;
        this.mResolutionMultipler = resolutionMultipler;
        this.mIsDebugEnabled = isDebugEnabled;
        this.mResultantForce = 0.009787036836709785d - 1.1767980631998021E-5d;
        this.mTotalMass = 9.991999975249462E-4d;
    }

    public double getDelta(double angle, double containerAngle, long elapsedTime) {
        double currentAcceleration = (((double) this.mResolutionMultipler) * getAcceleration(angle, containerAngle)) / 50.0d;
        double acceleration = (this.mLastAcceleration + currentAcceleration) / 2.0d;
        double elapsedTimeSec = (double) elapsedTime;
        this.mSpeed += acceleration * elapsedTimeSec;
        if (containerAngle > this.mMaxAngle && this.mSpeed < 0.0d) {
            this.mSpeed = 0.0d;
        } else if (containerAngle < (-this.mMaxAngle) && this.mSpeed > 0.0d) {
            this.mSpeed = 0.0d;
        }
        double delta = (this.mSpeed * elapsedTimeSec) / 2.0d;
        if (delta >= -1.0E-4d && delta < 1.0E-4d) {
            delta = 0.0d;
        }
        this.mLastAcceleration = currentAcceleration;
        if (this.mIsDebugEnabled) {
            Log.d("LEVELAPP", "angle: " + angle + ", containerAngle: " + containerAngle + ", elapsedTimeSec: " + elapsedTimeSec + ", acceleration: " + acceleration + ", mSpeed: " + this.mSpeed + ", delta: " + delta);
        }
        return delta;
    }

    private double getAcceleration(double angle, double containerAngle) {
        return ((this.mResultantForce * Math.sin(0.017453292519943295d * (angle + containerAngle))) - (((DRAG_COEFFICIENT * this.mResultantForce) * Math.cos(0.017453292519943295d * (angle + containerAngle))) * this.mSpeed)) / this.mTotalMass;
    }
}
