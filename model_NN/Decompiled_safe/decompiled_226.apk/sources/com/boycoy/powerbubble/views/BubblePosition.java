package com.boycoy.powerbubble.views;

public class BubblePosition {
    private int mBubbleSize = -1;
    private int mContainerSize = -1;
    private double mMaxAngle = 0.0d;
    private double mPositionCenter = -1.0d;
    private double mPostionLeft = -1.0d;

    public BubblePosition(int bubbleSize, int containerSize, double maxAngle) {
        this.mBubbleSize = bubbleSize;
        this.mContainerSize = containerSize;
        this.mMaxAngle = maxAngle;
        calculatePositions(((double) this.mContainerSize) / 2.0d);
    }

    public float getBubbleSize() {
        return (float) this.mBubbleSize;
    }

    public float getContainerSize() {
        return (float) this.mContainerSize;
    }

    public double getPositionLeft() {
        return this.mPostionLeft;
    }

    public void setPostionLeft(int value) {
        this.mPositionCenter = (double) ((this.mBubbleSize / 2) + value);
        calculatePositions();
    }

    public double getPostionCenter() {
        return this.mPositionCenter - ((double) (this.mContainerSize / 2));
    }

    public double getContainerAngleByCenterPostion() {
        return ((-1.0d * this.mMaxAngle) * getPostionCenter()) / ((double) ((this.mContainerSize - this.mBubbleSize) / 2));
    }

    public void calculatePositions() {
        calculatePositions(0.0d);
    }

    public void calculatePositions(double delta) {
        double newLeft = (this.mPositionCenter + delta) - ((double) (this.mBubbleSize / 2));
        double newRight = this.mPositionCenter + delta + ((double) (this.mBubbleSize / 2));
        if (newLeft >= 0.0d && newRight <= ((double) this.mContainerSize)) {
            this.mPositionCenter += delta;
            this.mPostionLeft = newLeft;
        } else if (newLeft < 0.0d) {
            this.mPositionCenter = (double) (this.mBubbleSize / 2);
            this.mPostionLeft = 0.0d;
        } else if (newRight > ((double) this.mContainerSize)) {
            this.mPositionCenter = (double) (this.mContainerSize - (this.mBubbleSize / 2));
            this.mPostionLeft = (double) (this.mContainerSize - this.mBubbleSize);
        }
    }
}
