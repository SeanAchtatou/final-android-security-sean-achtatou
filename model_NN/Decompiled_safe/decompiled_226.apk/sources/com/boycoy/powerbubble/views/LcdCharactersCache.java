package com.boycoy.powerbubble.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import com.boycoy.powerbubble.R;
import java.util.HashMap;

public class LcdCharactersCache {
    private static int MAX_CHAR = 255;
    private Bitmap[] mBitmaps = new Bitmap[MAX_CHAR];
    private int mBitmapsHeight = 0;
    private int[] mBitmapsWidths = new int[MAX_CHAR];
    private boolean mIsCacheInitialized = false;

    public boolean containsKey(char key) {
        return this.mBitmaps[key] != null;
    }

    public Bitmap getCharNodpiBitmap(char key) {
        return this.mBitmaps[key];
    }

    public float getCharNodpiWidth(char key) {
        return (float) this.mBitmapsWidths[key];
    }

    public int getCharsNodpiHeight() {
        return this.mBitmapsHeight;
    }

    public boolean isCacheInitialized() {
        return this.mIsCacheInitialized;
    }

    public void initializeCache(Context context) {
        this.mIsCacheInitialized = true;
        this.mBitmaps[32] = null;
        this.mBitmapsWidths[32] = 15;
        HashMap<Character, Integer> charactersWidths = new HashMap<>();
        charactersWidths.put('.', 7);
        charactersWidths.put('*', 19);
        charactersWidths.put(',', 7);
        charactersWidths.put('-', 15);
        charactersWidths.put('+', 15);
        HashMap<Character, Integer> charactersPositions = new HashMap<>();
        charactersPositions.put('.', 2);
        charactersPositions.put('*', 10);
        charactersPositions.put(',', 30);
        charactersPositions.put('-', 38);
        charactersPositions.put('+', 54);
        Bitmap specialSrcBitmap = ((BitmapDrawable) context.getResources().getDrawable(R.drawable.lcd_characters_special)).getBitmap();
        generateBitmaps(charactersWidths, charactersPositions, specialSrcBitmap);
        this.mBitmapsHeight = specialSrcBitmap.getHeight();
        HashMap<Character, Integer> charactersWidths2 = new HashMap<>();
        for (int i = 0; i < 10; i++) {
            charactersWidths2.put(Character.valueOf((char) (i + 48)), 23);
        }
        HashMap<Character, Integer> charactersPositions2 = new HashMap<>();
        for (int i2 = 0; i2 < 10; i2++) {
            charactersPositions2.put(new Character((char) (i2 + 48)), Integer.valueOf((i2 * 24) + 1));
        }
        generateBitmaps(charactersWidths2, charactersPositions2, ((BitmapDrawable) context.getResources().getDrawable(R.drawable.lcd_characters_numbers)).getBitmap());
        HashMap<Character, Integer> charactersWidths3 = new HashMap<>();
        for (int i3 = 0; i3 < 26; i3++) {
            charactersWidths3.put(Character.valueOf((char) (i3 + 65)), 23);
        }
        HashMap<Character, Integer> charactersPositions3 = new HashMap<>();
        for (int i4 = 0; i4 < 26; i4++) {
            charactersPositions3.put(new Character((char) (i4 + 65)), Integer.valueOf((i4 * 24) + 2));
        }
        generateBitmaps(charactersWidths3, charactersPositions3, ((BitmapDrawable) context.getResources().getDrawable(R.drawable.lcd_characters_letters)).getBitmap());
    }

    private void generateBitmaps(HashMap<Character, Integer> charactersWidths, HashMap<Character, Integer> charactersPositions, Bitmap srcBitmap) {
        int i = 0;
        while (i < charactersWidths.size() && i < charactersPositions.size()) {
            char currentChar = ((Character) charactersWidths.keySet().toArray()[i]).charValue();
            if (charactersWidths.containsKey(Character.valueOf(currentChar)) && charactersPositions.containsKey(Character.valueOf(currentChar))) {
                int position = charactersPositions.get(Character.valueOf(currentChar)).intValue();
                int width = charactersWidths.get(Character.valueOf(currentChar)).intValue();
                Bitmap bitmap = null;
                if (position >= 0) {
                    bitmap = Bitmap.createBitmap(srcBitmap, position, 0, width, srcBitmap.getHeight());
                }
                this.mBitmaps[currentChar] = bitmap;
                this.mBitmapsWidths[currentChar] = width;
            }
            i++;
        }
    }
}
