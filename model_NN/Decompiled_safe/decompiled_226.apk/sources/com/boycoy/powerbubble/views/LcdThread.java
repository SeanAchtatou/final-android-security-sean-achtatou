package com.boycoy.powerbubble.views;

import android.content.Context;
import android.graphics.Canvas;
import android.media.MediaPlayer;
import android.view.SurfaceHolder;
import com.boycoy.powerbubble.ConstantFpsThread;
import com.boycoy.powerbubble.LockListener;
import com.boycoy.powerbubble.R;
import com.boycoy.powerbubble.SettingsManager;

public class LcdThread extends ConstantFpsThread implements LockListener {
    private static final int ANGLES_AVG_SIZE = 5;
    private float mCurrentAngleXValue = 0.0f;
    private int mCurrentTextRepeatCount = 0;
    private float mDesiredAngleXValue = 0.0f;
    private boolean mIsHoldPossible = true;
    private boolean mIsLockEnabled = false;
    private boolean mIsSoundEnabled = true;
    private float[] mLastAngelXValues;
    private int mLastAngleXAddIndex = 0;
    private float mLastAngleXValue = 0.0f;
    private boolean mLastFrameSoundPlayed = true;
    private int mLastTextRepeatCount = 0;
    private LcdDrawer mLcdDrawer;
    private char[] mLcdText;
    private boolean mLcdTextChanged = true;
    private int mLcdTextLength = 0;
    private MediaPlayer mMediaPlayer;
    private char[] mMessageCharArray = null;
    private OnLcdDrawListener mOnLcdDrawListener = null;
    private boolean mShowDecimals;
    private SurfaceHolder mSurfaceHolder;

    public LcdThread(SurfaceHolder surfaceHolder, Context context, LcdCharactersCache lcdCharactersCache) {
        this.mSurfaceHolder = surfaceHolder;
        this.mLcdDrawer = new LcdDrawer(context, lcdCharactersCache);
        this.mLcdText = new char[LcdDrawer.MAX_LCD_TEXT_LENGTH];
        this.mShowDecimals = SettingsManager.getBoolean(SettingsManager.Settings.ENABLE_DECIMAL, true);
        this.mLastAngelXValues = new float[5];
        this.mMediaPlayer = MediaPlayer.create(context, (int) R.raw.beep);
    }

    public String getMessage() {
        if (this.mMessageCharArray != null) {
            return new String(this.mMessageCharArray, 0, this.mMessageCharArray.length);
        }
        return null;
    }

    public void setMessage(String message) {
        this.mCurrentTextRepeatCount = 0;
        this.mLastTextRepeatCount = 0;
        synchronized (this) {
            if (message != null) {
                this.mMessageCharArray = message.toCharArray();
                this.mIsHoldPossible = false;
                setPaused(false);
            } else {
                this.mMessageCharArray = null;
            }
        }
    }

    public float getAngleX() {
        return this.mCurrentAngleXValue;
    }

    public void setAngleX(float value) {
        synchronized (this) {
            this.mDesiredAngleXValue = value;
        }
    }

    public void setInitialAngleX(float value) {
        synchronized (this) {
            this.mDesiredAngleXValue = value;
            this.mCurrentAngleXValue = value;
            drawToSurfaceHolder();
        }
    }

    public void setEnableSound(boolean value) {
        this.mIsSoundEnabled = value;
    }

    public void setLockEnabled(boolean value) {
        this.mIsLockEnabled = value;
        if (this.mMessageCharArray == null || !this.mIsLockEnabled) {
            setPaused(this.mIsLockEnabled);
        }
    }

    public boolean isLockPossible() {
        return this.mIsHoldPossible;
    }

    public void toggleLock() {
        setPaused(!getPaused());
    }

    public void setOnLcdDrawListener(OnLcdDrawListener onLcdDrawListener) {
        this.mOnLcdDrawListener = onLcdDrawListener;
    }

    /* access modifiers changed from: protected */
    public void onFrameDraw(long elapsedTime) {
        calculateAngleX(elapsedTime);
        checkChanges();
        if (this.mIsSoundEnabled && this.mMessageCharArray == null) {
            playSound();
        }
        if (this.mLcdTextChanged) {
            drawToSurfaceHolder();
        }
    }

    private void drawToSurfaceHolder() {
        Canvas canvas = null;
        try {
            canvas = this.mSurfaceHolder.lockCanvas(null);
            prepareString();
            this.mLcdDrawer.setText(this.mLcdText, this.mLcdTextLength);
            this.mLcdDrawer.draw(canvas);
            if (canvas != null) {
                this.mSurfaceHolder.unlockCanvasAndPost(canvas);
            }
        } catch (NullPointerException e) {
            if (canvas != null) {
                this.mSurfaceHolder.unlockCanvasAndPost(canvas);
            }
        } catch (Throwable th) {
            if (canvas != null) {
                this.mSurfaceHolder.unlockCanvasAndPost(canvas);
            }
            throw th;
        }
        if (this.mMessageCharArray == null && this.mIsLockEnabled) {
            setPaused(true);
        }
        this.mIsHoldPossible = this.mMessageCharArray == null;
        this.mCurrentTextRepeatCount = this.mLcdDrawer.getTextRepeatCount();
        if (this.mCurrentTextRepeatCount != this.mLastTextRepeatCount) {
            this.mOnLcdDrawListener.onTextRepeatCountChanged(this.mLcdDrawer.getTextRepeatCount());
            this.mLastTextRepeatCount = this.mLcdDrawer.getTextRepeatCount();
        }
    }

    private void calculateAngleX(long elapsedTime) {
        float stepUnsigned;
        float angleDiffSigned = this.mDesiredAngleXValue - this.mCurrentAngleXValue;
        float angleDiffUnsigned = Math.abs(this.mDesiredAngleXValue - this.mCurrentAngleXValue);
        if (!this.mShowDecimals || ((double) angleDiffUnsigned) <= 0.1d || this.mShowDecimals || angleDiffUnsigned <= 1.0f) {
            this.mCurrentAngleXValue = this.mDesiredAngleXValue;
            return;
        }
        if (angleDiffUnsigned > 10.0f) {
            stepUnsigned = (((float) (elapsedTime / this.mDesiredTime)) * angleDiffUnsigned) / 3.0f;
        } else if (angleDiffUnsigned > 4.0f) {
            stepUnsigned = (((float) (elapsedTime / this.mDesiredTime)) * angleDiffUnsigned) / 5.0f;
        } else if (angleDiffUnsigned > 1.0f) {
            stepUnsigned = (((float) (elapsedTime / this.mDesiredTime)) * angleDiffUnsigned) / 10.0f;
        } else if (((double) angleDiffUnsigned) > 0.7d) {
            stepUnsigned = ((float) (elapsedTime / this.mDesiredTime)) * 0.2f;
        } else {
            stepUnsigned = ((float) (elapsedTime / this.mDesiredTime)) * 0.1f;
        }
        if (angleDiffUnsigned - stepUnsigned < 0.0f) {
            stepUnsigned = angleDiffUnsigned;
        }
        this.mCurrentAngleXValue += Math.signum(angleDiffSigned) * stepUnsigned;
    }

    private void checkChanges() {
        this.mLcdTextChanged = false;
        if (this.mMessageCharArray != null) {
            this.mLcdTextChanged = true;
            this.mLastAngleXValue = Float.MAX_VALUE;
        } else if (this.mLcdTextLength == 0 || ((int) Math.floor((double) (this.mCurrentAngleXValue * 10.0f))) != ((int) Math.floor((double) (this.mLastAngleXValue * 10.0f)))) {
            this.mLastAngleXValue = this.mCurrentAngleXValue;
            this.mLcdTextChanged = true;
        }
    }

    private void prepareString() {
        int nextIndex;
        int nextIndex2;
        this.mLcdTextLength = 0;
        if (this.mMessageCharArray != null) {
            this.mLcdTextLength = this.mMessageCharArray.length;
            for (int i = 0; i < this.mLcdTextLength; i++) {
                this.mLcdText[i] = this.mMessageCharArray[i];
            }
            return;
        }
        if (((double) this.mCurrentAngleXValue) <= -0.1d || ((double) this.mCurrentAngleXValue) >= 0.1d) {
            int nextIndex3 = 0 + 1;
            this.mLcdText[0] = this.mCurrentAngleXValue >= 0.0f ? '+' : '-';
            nextIndex = nextIndex3;
        } else {
            this.mLcdText[0] = ' ';
            nextIndex = 0 + 1;
        }
        if (((int) Math.abs(this.mCurrentAngleXValue)) >= 10) {
            this.mLcdText[nextIndex] = Character.forDigit(((int) (Math.abs(this.mCurrentAngleXValue) / 10.0f)) % 10, 10);
            nextIndex2 = nextIndex + 1;
        } else {
            this.mLcdText[nextIndex] = '0';
            nextIndex2 = nextIndex + 1;
        }
        int nextIndex4 = nextIndex2 + 1;
        this.mLcdText[nextIndex2] = Character.forDigit(((int) Math.abs(this.mCurrentAngleXValue)) % 10, 10);
        if (this.mShowDecimals) {
            int nextIndex5 = nextIndex4 + 1;
            this.mLcdText[nextIndex4] = '.';
            nextIndex4 = nextIndex5 + 1;
            this.mLcdText[nextIndex5] = Character.forDigit(((int) (Math.abs(this.mCurrentAngleXValue) * 10.0f)) % 10, 10);
        }
        int nextIndex6 = nextIndex4;
        this.mLcdText[nextIndex6] = '*';
        this.mLcdTextLength = nextIndex6 + 1;
    }

    private void playSound() {
        float[] fArr = this.mLastAngelXValues;
        int i = this.mLastAngleXAddIndex;
        this.mLastAngleXAddIndex = i + 1;
        fArr[i] = this.mCurrentAngleXValue;
        this.mLastAngleXAddIndex %= 5;
        float min = Float.MAX_VALUE;
        float max = Float.MIN_VALUE;
        float bubblePostionXAvreage = 0.0f;
        for (int i2 = 0; i2 < 5; i2++) {
            float value = this.mLastAngelXValues[i2];
            bubblePostionXAvreage += value;
            if (value < min) {
                min = value;
            }
            if (value > max) {
                max = value;
            }
        }
        float bubblePostionXAvreage2 = bubblePostionXAvreage / 5.0f;
        float diff = Math.abs(max - min);
        if (((double) bubblePostionXAvreage2) <= -0.1d || ((double) bubblePostionXAvreage2) >= 0.1d || ((double) diff) >= 0.2d) {
            this.mLastFrameSoundPlayed = false;
        } else if (!this.mLastFrameSoundPlayed) {
            this.mLastFrameSoundPlayed = true;
            if (this.mMediaPlayer != null) {
                this.mMediaPlayer.start();
            }
        }
    }
}
