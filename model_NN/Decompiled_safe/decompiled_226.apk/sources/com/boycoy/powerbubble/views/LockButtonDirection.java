package com.boycoy.powerbubble.views;

public class LockButtonDirection {
    private static float MINIMUM_CHANGE_X = 0.1f;
    private Direction mDirection = Direction.NONE;
    private long mLastTime;
    private float mLastX = -1.0f;

    public enum Direction {
        NONE,
        LEFT,
        RIGHT
    }

    public Direction get() {
        return this.mDirection;
    }

    public void reset() {
        this.mLastX = -1.0f;
        this.mDirection = Direction.NONE;
    }

    public void calculate(float x) {
        if (this.mLastX == -1.0f) {
            this.mLastX = x;
            this.mLastTime = System.currentTimeMillis();
        }
        long elapsedTime = System.currentTimeMillis() - this.mLastTime;
        if (x - this.mLastX > MINIMUM_CHANGE_X * ((float) elapsedTime)) {
            this.mDirection = Direction.RIGHT;
        } else if (x - this.mLastX < (-MINIMUM_CHANGE_X) * ((float) elapsedTime)) {
            this.mDirection = Direction.LEFT;
        } else {
            this.mDirection = Direction.NONE;
        }
        this.mLastX = x;
        this.mLastTime = System.currentTimeMillis();
    }
}
