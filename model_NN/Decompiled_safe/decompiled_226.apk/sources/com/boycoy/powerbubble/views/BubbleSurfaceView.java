package com.boycoy.powerbubble.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.ImageView;
import com.boycoy.powerbubble.AngleXListener;
import com.boycoy.powerbubble.AngleYListener;
import com.boycoy.powerbubble.LockListener;
import com.boycoy.powerbubble.R;

public class BubbleSurfaceView extends SurfaceView implements SurfaceHolder.Callback, LockListener, AngleXListener, AngleYListener {
    private int mBackgroundHeight;
    private int mBackgroundWidth;
    private BubbleThread mBubbleThread = null;
    private Context mContext = null;
    private int mInitialAnimationFrameY = 0;
    private boolean mInitialHoldEnabled = false;
    private int mInitialPostionX = 0;
    private int mInitialPostionY = 0;

    public BubbleSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        Bitmap backgroundBitmap = ((BitmapDrawable) context.getResources().getDrawable(R.drawable.liquid_border_background)).getBitmap();
        this.mBackgroundWidth = backgroundBitmap.getWidth();
        this.mBackgroundHeight = backgroundBitmap.getHeight();
        getHolder().addCallback(this);
    }

    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        startThread(surfaceHolder);
    }

    public void surfaceChanged(SurfaceHolder surfaceHolder, int format, int width, int height) {
    }

    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        stopThread();
    }

    private void startThread(SurfaceHolder surfaceHolder) {
        if (this.mBubbleThread == null) {
            ImageView backgroundImageView = (ImageView) ((View) getParent()).findViewById(R.id.background);
            this.mBubbleThread = new BubbleThread(surfaceHolder, this.mContext.getResources(), ((BitmapDrawable) backgroundImageView.getDrawable()).getBitmap(), backgroundImageView.getWidth(), backgroundImageView.getHeight());
            this.mBubbleThread.setFps(30);
            this.mBubbleThread.setRunning(true);
            this.mBubbleThread.setLockEnabled(this.mInitialHoldEnabled);
            this.mBubbleThread.setPostionX(this.mInitialPostionX);
            this.mBubbleThread.setPostionY(this.mInitialPostionY);
            this.mBubbleThread.setAnimationFrameY(this.mInitialAnimationFrameY);
            this.mBubbleThread.start();
        }
    }

    public void stopThread() {
        if (this.mBubbleThread != null) {
            this.mBubbleThread.setRunning(false);
            boolean retry = true;
            while (retry) {
                try {
                    this.mBubbleThread.join();
                    retry = false;
                } catch (InterruptedException e) {
                }
            }
            this.mBubbleThread = null;
        }
    }

    public int getPostionX() {
        int postionX = this.mInitialPostionX;
        if (this.mBubbleThread != null) {
            return this.mBubbleThread.getPostionX();
        }
        return postionX;
    }

    public void setInitialPostionX(int value) {
        this.mInitialPostionX = value;
    }

    public int getPostionY() {
        int postionY = this.mInitialPostionY;
        if (this.mBubbleThread != null) {
            return this.mBubbleThread.getPostionY();
        }
        return postionY;
    }

    public void setInitialPostionY(int value) {
        this.mInitialPostionY = value;
    }

    public int getAnimationFrameY() {
        int animationFrame = this.mInitialAnimationFrameY;
        if (this.mBubbleThread != null) {
            return this.mBubbleThread.getAnimationFrameY();
        }
        return animationFrame;
    }

    public void setInitialAnimationFrameY(int value) {
        this.mInitialAnimationFrameY = value;
    }

    public void setLockEnabled(boolean value) {
        if (this.mBubbleThread != null) {
            this.mBubbleThread.setLockEnabled(value);
        } else {
            this.mInitialHoldEnabled = value;
        }
    }

    public boolean isLockPossible() {
        if (this.mBubbleThread != null) {
            return this.mBubbleThread.isLockPossible();
        }
        return false;
    }

    public void toggleLock() {
        if (this.mBubbleThread != null) {
            this.mBubbleThread.toggleLock();
        } else {
            this.mInitialHoldEnabled = !this.mInitialHoldEnabled;
        }
    }

    public void setAngleX(float angle) {
        if (this.mBubbleThread != null) {
            this.mBubbleThread.setAngleX(angle);
        }
    }

    public void setAngleY(float angle) {
        if (this.mBubbleThread != null) {
            this.mBubbleThread.setAngleY(angle);
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(this.mBackgroundWidth, this.mBackgroundHeight);
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        stopThread();
    }
}
