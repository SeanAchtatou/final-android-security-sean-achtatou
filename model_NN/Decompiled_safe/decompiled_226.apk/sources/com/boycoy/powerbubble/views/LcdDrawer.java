package com.boycoy.powerbubble.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import com.boycoy.powerbubble.Commons;
import com.boycoy.powerbubble.R;

public class LcdDrawer {
    private static final int LCD_HPADDING = 13;
    private static final int LCD_LINES_COUNT = 36;
    private static final int LINE_HWIDTH_WITH_SPACER = 4;
    private static final long LINE_TIME = 10;
    public static final int MAX_LCD_TEXT_LENGTH = 150;
    private int currentLine = 1;
    private Bitmap mBackgroundBitmap;
    private Bitmap mBackgroundBottomBitmap;
    private int mBackgroundHeight;
    private Canvas mCharsCanvas = null;
    private int mCurrentTextBitmapWidth = 0;
    private float mDensityMultiplier;
    private long mLastTime = 0;
    private LcdCharactersCache mLcdCharactersCache;
    private Paint mPaint;
    private char[] mText = null;
    private Bitmap mTextBitmap = null;
    private int mTextBitmapHeight;
    private int mTextLength = 0;
    private int mTextRepeatCount = 0;

    public LcdDrawer(Context context, LcdCharactersCache lcdCharactersCache) {
        this.mLcdCharactersCache = lcdCharactersCache;
        this.mDensityMultiplier = Commons.getInstance().getResolutionWidthMultiplier();
        this.mBackgroundBottomBitmap = ((BitmapDrawable) context.getResources().getDrawable(R.drawable.lcd_background_bottom)).getBitmap();
        this.mBackgroundBitmap = ((BitmapDrawable) context.getResources().getDrawable(R.drawable.lcd_background)).getBitmap();
        this.mPaint = new Paint();
        this.mBackgroundHeight = this.mBackgroundBitmap.getHeight();
        this.mTextBitmapHeight = this.mLcdCharactersCache.getCharsNodpiHeight();
    }

    public void setText(char[] value, int length) {
        this.mText = value;
        this.mTextLength = length;
        this.mTextRepeatCount = 0;
    }

    public int getTextRepeatCount() {
        return this.mTextRepeatCount;
    }

    public void draw(Canvas canvas) {
        canvas.drawBitmap(this.mBackgroundBottomBitmap, 0.0f, 0.0f, this.mPaint);
        canvas.drawBitmap(this.mBackgroundBitmap, 0.0f, 0.0f, this.mPaint);
        int textBitmapWidth = calculateTextBitmapWidth();
        drawTextToBitmap(textBitmapWidth);
        int charsLcdLines = (textBitmapWidth - 4) / 4;
        int textBitmapPositionLines = 0;
        int textBitmapOffsetLines = 0;
        int textBitmapRemainingLines = 0;
        if (charsLcdLines > LCD_LINES_COUNT) {
            int currentLine2 = calculateCurrentLine(charsLcdLines);
            if (currentLine2 < LCD_LINES_COUNT) {
                textBitmapPositionLines = LCD_LINES_COUNT - currentLine2;
                textBitmapOffsetLines = 0;
                textBitmapRemainingLines = currentLine2;
            } else if (currentLine2 >= LCD_LINES_COUNT && currentLine2 <= charsLcdLines + LCD_LINES_COUNT) {
                textBitmapPositionLines = 0;
                textBitmapOffsetLines = currentLine2 - LCD_LINES_COUNT;
                textBitmapRemainingLines = LCD_LINES_COUNT;
                if (currentLine2 >= (charsLcdLines + LCD_LINES_COUNT) - LCD_LINES_COUNT) {
                    textBitmapRemainingLines = LCD_LINES_COUNT - (currentLine2 - ((charsLcdLines + LCD_LINES_COUNT) - LCD_LINES_COUNT));
                }
            }
        } else {
            textBitmapPositionLines = 0 + ((LCD_LINES_COUNT - charsLcdLines) / 2);
            textBitmapRemainingLines = LCD_LINES_COUNT;
        }
        Bitmap textBitmapScaled = null;
        if (((double) Math.abs(this.mDensityMultiplier - 1.0f)) > 0.01d) {
            textBitmapScaled = Bitmap.createScaledBitmap(this.mTextBitmap, (int) (((float) this.mTextBitmap.getWidth()) * this.mDensityMultiplier), (int) (((float) this.mTextBitmapHeight) * this.mDensityMultiplier), true);
        }
        Bitmap textBitmapCropSource = textBitmapScaled != null ? textBitmapScaled : this.mTextBitmap;
        canvas.drawBitmap(Bitmap.createBitmap(textBitmapCropSource, (int) (((float) ((textBitmapOffsetLines * 4) + 1)) * this.mDensityMultiplier), 0, (int) Math.min(((float) ((textBitmapRemainingLines * 4) + 1)) * this.mDensityMultiplier, (float) (textBitmapCropSource.getWidth() - 2)), (int) (((float) this.mTextBitmapHeight) * this.mDensityMultiplier)), ((float) ((textBitmapPositionLines * 4) + LCD_HPADDING)) * this.mDensityMultiplier, (((float) this.mBackgroundHeight) - (((float) this.mTextBitmapHeight) * this.mDensityMultiplier)) / 2.0f, this.mPaint);
    }

    private int calculateCurrentLine(int charsLcdLines) {
        long currentTime = System.currentTimeMillis();
        long timeElapsed = currentTime - this.mLastTime;
        int position = 0;
        synchronized (this) {
            if (this.mText != null) {
                position = this.currentLine;
                if (timeElapsed > LINE_TIME) {
                    this.mLastTime = currentTime;
                    this.currentLine++;
                    if (this.currentLine >= (charsLcdLines + 72) - LCD_LINES_COUNT) {
                        this.currentLine = 1;
                        this.mTextRepeatCount++;
                    }
                }
            }
        }
        return position;
    }

    private int calculateTextBitmapWidth() {
        int charsBitmapWidth = 0;
        for (int i = 0; i < this.mTextLength; i++) {
            charsBitmapWidth = (int) (((float) charsBitmapWidth) + this.mLcdCharactersCache.getCharNodpiWidth(this.mText[i]) + 1.0f);
        }
        return charsBitmapWidth;
    }

    private Bitmap createTextBitmap(int textBitmapWidth) {
        return Bitmap.createBitmap(textBitmapWidth, this.mTextBitmapHeight, Bitmap.Config.ARGB_8888);
    }

    private void prepareTextBitmap(int textBitmapWidth) {
        if (this.mTextBitmap == null || this.mCurrentTextBitmapWidth < textBitmapWidth) {
            this.mCurrentTextBitmapWidth = textBitmapWidth;
            this.mTextBitmap = createTextBitmap(textBitmapWidth);
            this.mCharsCanvas = new Canvas(this.mTextBitmap);
            return;
        }
        this.mCharsCanvas.drawColor(0, PorterDuff.Mode.CLEAR);
    }

    private void drawTextToBitmap(int textBitmapWidth) {
        prepareTextBitmap(textBitmapWidth);
        int currentDrawingPostion = 0;
        for (int i = 0; i < this.mTextLength; i++) {
            char currentChar = this.mText[i];
            Bitmap charBitmap = this.mLcdCharactersCache.getCharNodpiBitmap(currentChar);
            if (charBitmap != null) {
                this.mCharsCanvas.drawBitmap(charBitmap, (float) currentDrawingPostion, 0.0f, this.mPaint);
            }
            currentDrawingPostion = (int) (((float) currentDrawingPostion) + this.mLcdCharactersCache.getCharNodpiWidth(currentChar) + 1.0f);
        }
    }
}
