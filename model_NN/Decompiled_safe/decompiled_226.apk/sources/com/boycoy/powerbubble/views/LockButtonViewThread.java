package com.boycoy.powerbubble.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.view.MotionEvent;
import android.view.View;
import com.boycoy.powerbubble.Commons;
import com.boycoy.powerbubble.ConstantFpsThread;
import com.boycoy.powerbubble.R;
import com.boycoy.powerbubble.views.LockButtonDirection;

public class LockButtonViewThread extends ConstantFpsThread {
    private long mActionDownTime = 0;
    private Bitmap mBackgroundBitmap;
    private Bitmap mButtonBitmap;
    private Canvas mCanvas;
    private LockButtonDirection.Direction mCurrentPostion = LockButtonDirection.Direction.LEFT;
    private float mCurrentStartX = -1.0f;
    private float mCurrentX = 0.0f;
    private Bitmap mDestBitmap;
    private float mGestureStartX = -1.0f;
    private LockButtonDirection mHoldButtonSpeed;
    private boolean mIsHoldPossible = true;
    private boolean mIsPressed = false;
    private Paint mPaint;
    private View mParentView;
    private float mResolutionMultiplier;
    private float mSpeed;

    public LockButtonViewThread(View parentView, Context context, boolean forcePostionRight) {
        this.mParentView = parentView;
        this.mResolutionMultiplier = Commons.getInstance().getResolutionWidthMultiplier();
        this.mBackgroundBitmap = ((BitmapDrawable) context.getResources().getDrawable(R.drawable.button_lock_background)).getBitmap();
        this.mButtonBitmap = ((BitmapDrawable) context.getResources().getDrawable(R.drawable.button_lock)).getBitmap();
        this.mDestBitmap = Bitmap.createBitmap(this.mBackgroundBitmap.getWidth(), this.mBackgroundBitmap.getHeight(), Bitmap.Config.ARGB_8888);
        this.mCanvas = new Canvas(this.mDestBitmap);
        this.mPaint = new Paint();
        this.mHoldButtonSpeed = new LockButtonDirection();
        if (forcePostionRight) {
            forcePostionRight();
        }
    }

    public LockButtonDirection.Direction getPostion() {
        return this.mCurrentPostion;
    }

    public void forcePostionRight() {
        setCurrentX((float) (this.mBackgroundBitmap.getWidth() - this.mButtonBitmap.getWidth()));
    }

    public void setIsHoldPossible(boolean isPossible) {
        this.mIsHoldPossible = isPossible;
    }

    public void onTouch(MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0 && !this.mIsPressed) {
            onActionDown(motionEvent.getX());
        }
        if (motionEvent.getAction() == 1 && this.mIsPressed) {
            onActionUp(motionEvent.getX());
        }
        if (this.mIsPressed) {
            this.mHoldButtonSpeed.calculate(motionEvent.getX());
            setCurrentX((this.mCurrentStartX + motionEvent.getX()) - this.mGestureStartX);
            updatePostionByCurrentX();
            draw();
            this.mParentView.postInvalidate();
        }
    }

    public Bitmap getBitmap() {
        return this.mDestBitmap;
    }

    /* access modifiers changed from: protected */
    public void onFrameDraw(long elapsedTime) {
        calculateCurrentX(elapsedTime);
        draw();
        this.mParentView.postInvalidate();
    }

    private void setCurrentX(float value) {
        float currentX = value;
        if (value < 0.0f) {
            currentX = 0.0f;
        }
        if (this.mIsHoldPossible) {
            if (value > ((float) (this.mBackgroundBitmap.getWidth() - this.mButtonBitmap.getWidth()))) {
                currentX = (float) (this.mBackgroundBitmap.getWidth() - this.mButtonBitmap.getWidth());
            }
        } else if (value > ((float) ((this.mBackgroundBitmap.getWidth() / 2) - (this.mButtonBitmap.getWidth() / 2)))) {
            currentX = (float) ((this.mBackgroundBitmap.getWidth() / 2) - (this.mButtonBitmap.getWidth() / 2));
        }
        this.mCurrentX = currentX;
    }

    private void onActionDown(float motionX) {
        this.mIsPressed = true;
        this.mActionDownTime = System.currentTimeMillis();
        this.mHoldButtonSpeed.reset();
        this.mGestureStartX = motionX;
        this.mCurrentStartX = this.mCurrentX;
    }

    private void onActionUp(float motionX) {
        setPaused(false);
        this.mIsPressed = false;
        this.mSpeed = 0.0f;
        if (Math.abs(this.mGestureStartX - motionX) >= 2.0f || this.mActionDownTime - System.currentTimeMillis() >= 150) {
            if (this.mHoldButtonSpeed.get() == LockButtonDirection.Direction.LEFT) {
                this.mSpeed = -1.0f;
            } else if (this.mHoldButtonSpeed.get() == LockButtonDirection.Direction.RIGHT) {
                if (this.mIsHoldPossible) {
                    this.mSpeed = 1.0f;
                } else {
                    this.mSpeed = -1.0f;
                }
            } else if (this.mCurrentX < ((float) ((this.mBackgroundBitmap.getWidth() - this.mButtonBitmap.getWidth()) / 2))) {
                this.mSpeed = -1.0f;
            } else if (this.mIsHoldPossible) {
                this.mSpeed = 1.0f;
            } else {
                this.mSpeed = -1.0f;
            }
            updatePostionBySpeed();
        } else if (this.mCurrentX >= ((float) ((this.mBackgroundBitmap.getWidth() - this.mButtonBitmap.getWidth()) / 2))) {
            this.mCurrentPostion = LockButtonDirection.Direction.LEFT;
            this.mSpeed = -1.0f;
        } else if (this.mIsHoldPossible) {
            this.mCurrentPostion = LockButtonDirection.Direction.RIGHT;
            this.mSpeed = 1.0f;
        } else {
            this.mCurrentPostion = LockButtonDirection.Direction.LEFT;
            this.mSpeed = -1.0f;
        }
        this.mHoldButtonSpeed.reset();
    }

    private void calculateCurrentX(long elapsedTime) {
        if (!this.mIsPressed) {
            this.mCurrentX += ((this.mResolutionMultiplier * this.mSpeed) * ((float) elapsedTime)) / 3.0f;
            if (this.mCurrentX <= 0.0f) {
                this.mCurrentX = 0.0f;
                this.mSpeed = 0.0f;
                setPaused(true);
            } else if (this.mCurrentX >= ((float) (this.mBackgroundBitmap.getWidth() - this.mButtonBitmap.getWidth()))) {
                this.mCurrentX = (float) (this.mBackgroundBitmap.getWidth() - this.mButtonBitmap.getWidth());
                this.mSpeed = 0.0f;
                setPaused(true);
            }
        }
    }

    private void updatePostionByCurrentX() {
        if (this.mCurrentX < ((float) ((this.mBackgroundBitmap.getWidth() - this.mButtonBitmap.getWidth()) / 2))) {
            this.mCurrentPostion = LockButtonDirection.Direction.LEFT;
        } else if (this.mIsHoldPossible) {
            this.mCurrentPostion = LockButtonDirection.Direction.RIGHT;
        }
    }

    private void updatePostionBySpeed() {
        if (this.mSpeed < 0.0f) {
            this.mCurrentPostion = LockButtonDirection.Direction.LEFT;
        } else if (this.mSpeed > 0.0f) {
            this.mCurrentPostion = LockButtonDirection.Direction.RIGHT;
        }
    }

    private void draw() {
        this.mCanvas.drawColor(0, PorterDuff.Mode.CLEAR);
        this.mCanvas.drawBitmap(this.mBackgroundBitmap, 0.0f, 0.0f, this.mPaint);
        this.mCanvas.drawBitmap(this.mButtonBitmap, this.mCurrentX, (float) (this.mBackgroundBitmap.getHeight() - this.mButtonBitmap.getHeight()), this.mPaint);
    }
}
