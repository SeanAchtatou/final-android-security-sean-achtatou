package com.boycoy.powerbubble.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import com.boycoy.powerbubble.LockListener;
import com.boycoy.powerbubble.R;
import com.boycoy.powerbubble.views.LockButtonDirection;
import java.util.ArrayList;
import java.util.Iterator;

public class LockButtonView extends View implements View.OnTouchListener {
    private int mBackgroundHeight;
    private int mBackgroundWidth;
    private Context mContext;
    private boolean mIsLockEnabled = false;
    private boolean mIsLockPossible = true;
    private LockButtonDirection.Direction mLastPostion;
    private LockButtonViewThread mLockButtonViewThread;
    private ArrayList<LockListener> mLockListeners;
    private Paint mPaint;

    public LockButtonView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        setOnTouchListener(this);
        this.mLockListeners = new ArrayList<>();
        Bitmap backgroundSrcBitmap = ((BitmapDrawable) context.getResources().getDrawable(R.drawable.button_lock_background)).getBitmap();
        this.mBackgroundWidth = backgroundSrcBitmap.getWidth();
        this.mBackgroundHeight = backgroundSrcBitmap.getHeight();
        this.mPaint = new Paint();
    }

    public boolean isLockEnabled() {
        return this.mIsLockEnabled;
    }

    public void forceHoldEnabled() {
        this.mIsLockEnabled = true;
        if (this.mLockButtonViewThread != null) {
            this.mLockButtonViewThread.forcePostionRight();
            dispatchLockListener(Boolean.valueOf(this.mIsLockEnabled));
        }
    }

    public void addLockListener(LockListener lockListener) {
        this.mLockListeners.add(lockListener);
    }

    public boolean onTouch(View v, MotionEvent motionEvent) {
        updateIsLockPossible();
        this.mLockButtonViewThread.setIsHoldPossible(this.mIsLockPossible);
        this.mLockButtonViewThread.onTouch(motionEvent);
        LockButtonDirection.Direction currentPostion = this.mLockButtonViewThread.getPostion();
        if (this.mLastPostion != currentPostion) {
            this.mIsLockEnabled = currentPostion == LockButtonDirection.Direction.RIGHT;
            dispatchLockListener(Boolean.valueOf(this.mIsLockEnabled));
        }
        this.mLastPostion = currentPostion;
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.mLockButtonViewThread != null) {
            canvas.drawBitmap(this.mLockButtonViewThread.getBitmap(), (float) getPaddingLeft(), (float) getPaddingTop(), this.mPaint);
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(getPaddingLeft() + this.mBackgroundWidth + getPaddingRight(), getPaddingTop() + this.mBackgroundHeight + getPaddingBottom());
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int visibility) {
        super.onWindowVisibilityChanged(visibility);
        if (visibility == 0) {
            startThread();
        } else {
            stopThread();
        }
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        stopThread();
        setOnTouchListener(null);
    }

    private void startThread() {
        if (this.mLockButtonViewThread == null) {
            this.mLockButtonViewThread = new LockButtonViewThread(this, this.mContext, this.mIsLockEnabled);
            this.mLockButtonViewThread.setRunning(true);
            this.mLockButtonViewThread.setFps(30);
            this.mLockButtonViewThread.start();
            dispatchLockListener(Boolean.valueOf(this.mIsLockEnabled));
        }
    }

    private void stopThread() {
        if (this.mLockButtonViewThread != null) {
            this.mLockButtonViewThread.setRunning(false);
            boolean retry = true;
            while (retry) {
                try {
                    this.mLockButtonViewThread.join();
                    retry = false;
                } catch (InterruptedException e) {
                }
            }
            this.mLockButtonViewThread = null;
        }
    }

    private void updateIsLockPossible() {
        this.mIsLockPossible = true;
        Iterator<LockListener> it = this.mLockListeners.iterator();
        while (it.hasNext()) {
            if (!it.next().isLockPossible()) {
                this.mIsLockPossible = false;
                return;
            }
        }
    }

    private void dispatchLockListener(Boolean isLockEnabled) {
        Iterator<LockListener> it = this.mLockListeners.iterator();
        while (it.hasNext()) {
            it.next().setLockEnabled(isLockEnabled.booleanValue());
        }
    }
}
