package com.boycoy.powerbubble;

public class Average {
    private int mAddIndex = 0;
    private float mAverage;
    private int mSize;
    private float[] mValues;

    public Average(int size) {
        this.mSize = size;
        this.mValues = new float[size];
    }

    public void add(float value) {
        float[] fArr = this.mValues;
        int i = this.mAddIndex;
        this.mAddIndex = i + 1;
        fArr[i] = value;
        this.mAddIndex %= this.mSize;
        calculate();
    }

    public float get() {
        return this.mAverage;
    }

    private void calculate() {
        this.mAverage = 0.0f;
        for (int i = 0; i < this.mSize; i++) {
            this.mAverage += this.mValues[i];
        }
        this.mAverage /= (float) this.mSize;
    }
}
