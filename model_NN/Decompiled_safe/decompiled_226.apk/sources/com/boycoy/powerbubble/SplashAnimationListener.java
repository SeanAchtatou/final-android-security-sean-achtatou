package com.boycoy.powerbubble;

import android.view.animation.Animation;

public interface SplashAnimationListener {
    void onAnimationEnd(Animation animation);
}
