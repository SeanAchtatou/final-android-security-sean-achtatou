package com.boycoy.powerbubble;

import android.app.Dialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import com.boycoy.powerbubble.dialogs.DialogManager;
import com.boycoy.powerbubble.dialogs.DonateDialog;
import com.boycoy.powerbubble.dialogs.OrientationDialog;
import com.boycoy.powerbubble.dialogs.ResetCalibrationDialog;

public class CustomPreferences extends PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener {
    private DialogManager mDialogManager = null;
    private DonateDialog mPleaseDonateDialog = null;
    private ResetCalibrationDialog mResetCalibrationDialog = null;
    private OrientationDialog mSetOrientationDialog = null;
    private SharedPreferences mSharedPreferences = null;
    private Tracker mTracker = null;

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals("checkbox_show_ads")) {
            if (!sharedPreferences.getBoolean("checkbox_show_ads", false)) {
                this.mDialogManager.showDialog(this.mPleaseDonateDialog);
            }
        } else if (!key.equals("checkbox_allow_tracking")) {
        } else {
            if (sharedPreferences.getBoolean("checkbox_allow_tracking", false)) {
                this.mTracker.start(this);
            } else {
                this.mTracker.stop();
            }
        }
    }

    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {
        boolean returnValue = super.onPreferenceTreeClick(preferenceScreen, preference);
        String key = preference.getKey();
        if (key == null) {
            return returnValue;
        }
        if (key.equals("button_why_donate")) {
            this.mDialogManager.showDialog(this.mPleaseDonateDialog);
            return true;
        } else if (key.equals("button_set_orientation")) {
            this.mDialogManager.showDialog(this.mSetOrientationDialog);
            return true;
        } else if (!key.equals("button_reset_calibration")) {
            return returnValue;
        } else {
            this.mDialogManager.showDialog(this.mResetCalibrationDialog);
            return true;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mTracker = new Tracker();
        addPreferencesFromResource(R.xml.preferences);
        this.mDialogManager = new DialogManager(this);
        this.mPleaseDonateDialog = new DonateDialog(this.mTracker);
        this.mDialogManager.addDialog(this.mPleaseDonateDialog);
        this.mSetOrientationDialog = new OrientationDialog(new ScreenOrientation(), this.mTracker);
        this.mDialogManager.addDialog(this.mSetOrientationDialog);
        this.mResetCalibrationDialog = new ResetCalibrationDialog(this.mTracker);
        this.mDialogManager.addDialog(this.mResetCalibrationDialog);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        this.mSharedPreferences.registerOnSharedPreferenceChangeListener(this);
        if (this.mSharedPreferences.getBoolean("checkbox_allow_tracking", false)) {
            this.mTracker.start(this);
        }
        this.mTracker.trackPageView("/preferences");
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.mTracker.dispatch();
        this.mTracker.stop();
        this.mSharedPreferences.unregisterOnSharedPreferenceChangeListener(this);
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        return this.mDialogManager.onCreateDialog(id, new Bundle());
    }
}
