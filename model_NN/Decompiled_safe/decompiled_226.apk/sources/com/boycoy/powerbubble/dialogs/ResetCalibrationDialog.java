package com.boycoy.powerbubble.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import com.boycoy.powerbubble.Calibrator;
import com.boycoy.powerbubble.R;
import com.boycoy.powerbubble.Tracker;

public class ResetCalibrationDialog extends DialogManageable {
    private static final int DIALOG_ID = 3;
    private Tracker mTracker;

    public ResetCalibrationDialog(Tracker tracker) {
        this.mTracker = tracker;
    }

    public int getDialogId() {
        return 3;
    }

    public void prepareDialog(Context context, Dialog dialog, Bundle args) {
    }

    public Dialog createDialog(Context context, Bundle args) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle((int) R.string.title_reset_calibration_dialog).setMessage((int) R.string.text_reset_calibration_dialog).setCancelable(true).setPositiveButton((int) R.string.positive_reset_calibration_dialog, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Calibrator.getInstance().resetCalibration();
            }
        }).setNegativeButton((int) R.string.negative_reset_calibration_dialog, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        this.mTracker.trackPageView("/preferences/reset_calibration");
        return builder.create();
    }
}
