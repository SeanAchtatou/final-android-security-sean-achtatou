package com.boycoy.powerbubble.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import com.boycoy.powerbubble.R;
import com.boycoy.powerbubble.Tracker;

public class DonateDialog extends DialogManageable {
    private static final int DIALOG_ID = 0;
    /* access modifiers changed from: private */
    public Tracker mTracker;

    public DonateDialog(Tracker tracker) {
        this.mTracker = tracker;
    }

    public int getDialogId() {
        return 0;
    }

    public void prepareDialog(Context context, Dialog dialog, Bundle args) {
    }

    public Dialog createDialog(final Context context, Bundle args) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle((int) R.string.title_donate_dialog).setMessage((int) R.string.text_donate_dialog).setCancelable(true).setPositiveButton((int) R.string.positive_donate_dialog, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                DonateDialog.this.mTracker.setDonationVariable(Tracker.DonateValue.DONATE);
                Intent intent = new Intent();
                intent.setAction("android.intent.action.VIEW");
                intent.setData(Uri.parse(context.getString(R.string.donate_address)));
                context.startActivity(intent);
            }
        }).setNegativeButton((int) R.string.negative_donate_dialog, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                DonateDialog.this.mTracker.setDonationVariable(Tracker.DonateValue.CONTINUE);
                dialog.cancel();
            }
        });
        this.mTracker.trackPageView("/preferences/please_donate");
        AlertDialog alertDialog = builder.create();
        alertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                DonateDialog.this.mTracker.setDonationVariable(Tracker.DonateValue.BACK);
            }
        });
        return alertDialog;
    }
}
