package com.boycoy.powerbubble.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;

public abstract class DialogManageable {
    public abstract Dialog createDialog(Context context, Bundle bundle);

    public abstract int getDialogId();

    public abstract void prepareDialog(Context context, Dialog dialog, Bundle bundle);
}
