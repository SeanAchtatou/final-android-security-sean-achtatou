package com.boycoy.powerbubble.dialogs;

import android.content.DialogInterface;

public interface TrackingDialogListener {
    void onClick(DialogInterface dialogInterface, int i);
}
