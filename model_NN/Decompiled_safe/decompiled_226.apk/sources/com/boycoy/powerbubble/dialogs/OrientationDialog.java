package com.boycoy.powerbubble.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import com.boycoy.powerbubble.R;
import com.boycoy.powerbubble.ScreenOrientation;
import com.boycoy.powerbubble.Tracker;

public class OrientationDialog extends DialogManageable {
    private static final int DIALOG_ID = 1;
    /* access modifiers changed from: private */
    public Activity mActivity = null;
    /* access modifiers changed from: private */
    public ScreenOrientation mScreenOrientation;
    private Tracker mTracker;

    public OrientationDialog(ScreenOrientation screenOrientation, Tracker tracker) {
        this.mScreenOrientation = screenOrientation;
        this.mTracker = tracker;
    }

    public int getDialogId() {
        return 1;
    }

    public void setActivityToUpdate(Activity activity) {
        this.mActivity = activity;
    }

    public void prepareDialog(Context context, Dialog dialog, Bundle args) {
        this.mTracker.trackPageView("/main/set_orientation");
        ((AlertDialog) dialog).getListView().setItemChecked(this.mScreenOrientation.get().ordinal(), true);
    }

    public Dialog createDialog(Context context, Bundle args) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle((int) R.string.title_orietation_dialog).setSingleChoiceItems(context.getResources().getStringArray(R.array.modes_orientation_dialog), this.mScreenOrientation.get().ordinal(), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
            }
        }).setPositiveButton((int) R.string.ok_general, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                int item = ((AlertDialog) dialog).getListView().getCheckedItemPosition();
                ScreenOrientation.Values screenOrientationValue = OrientationDialog.this.mScreenOrientation.get();
                switch (item) {
                    case 0:
                        screenOrientationValue = ScreenOrientation.Values.FULL_SENSOR;
                        break;
                    case 1:
                        screenOrientationValue = ScreenOrientation.Values.LANDSCAPE;
                        break;
                    case 2:
                        screenOrientationValue = ScreenOrientation.Values.PORTRAIT;
                        break;
                    case 3:
                        screenOrientationValue = ScreenOrientation.Values.REVERSE_LANDSCAPE;
                        break;
                    case R.styleable.com_admob_android_ads_AdView_refreshInterval:
                        screenOrientationValue = ScreenOrientation.Values.REVERSE_PORTRAIT;
                        break;
                }
                OrientationDialog.this.mScreenOrientation.set(screenOrientationValue);
                if (OrientationDialog.this.mActivity != null) {
                    OrientationDialog.this.mScreenOrientation.updateActivity(OrientationDialog.this.mActivity);
                }
            }
        }).setNegativeButton((int) R.string.cancel_general, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        });
        return builder.create();
    }
}
