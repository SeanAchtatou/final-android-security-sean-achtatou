package com.boycoy.powerbubble.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import com.boycoy.powerbubble.R;

public class TrackingDialog extends DialogManageable {
    private static final int DIALOG_ID = 2;
    /* access modifiers changed from: private */
    public TrackingDialogListener mTrackingDialogListener = null;

    public int getDialogId() {
        return 2;
    }

    public void prepareDialog(Context context, Dialog dialog, Bundle args) {
    }

    public Dialog createDialog(Context context, Bundle args) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle((int) R.string.title_tracking_dialog).setMessage((int) R.string.text_tracking_dialog).setNeutralButton((int) R.string.neutral_tracking_dialog, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int id) {
                if (TrackingDialog.this.mTrackingDialogListener != null) {
                    TrackingDialog.this.mTrackingDialogListener.onClick(dialogInterface, id);
                }
            }
        });
        return builder.create();
    }

    public void setTrackingDialogListener(TrackingDialogListener trackingDialogListener) {
        this.mTrackingDialogListener = trackingDialogListener;
    }
}
