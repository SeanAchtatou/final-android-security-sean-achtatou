package com.boycoy.powerbubble.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import java.util.ArrayList;
import java.util.Iterator;

public class DialogManager {
    private ArrayList<DialogManageable> dialogs = new ArrayList<>();
    private Activity mActivity;

    public DialogManager(Activity activity) {
        this.mActivity = activity;
    }

    public void addDialog(DialogManageable dialogManageable) {
        this.dialogs.add(dialogManageable);
    }

    public void showDialog(DialogManageable dialogManageable) {
        this.mActivity.showDialog(dialogManageable.getDialogId());
    }

    public void onPrepareDialog(int id, Dialog dialog, Bundle args) {
        findDialog(id).prepareDialog(this.mActivity, dialog, args);
    }

    public Dialog onCreateDialog(int id, Bundle args) {
        return findDialog(id).createDialog(this.mActivity, args);
    }

    private DialogManageable findDialog(int id) {
        Iterator<DialogManageable> it = this.dialogs.iterator();
        while (it.hasNext()) {
            DialogManageable currentDialog = it.next();
            if (id == currentDialog.getDialogId()) {
                return currentDialog;
            }
        }
        return null;
    }
}
