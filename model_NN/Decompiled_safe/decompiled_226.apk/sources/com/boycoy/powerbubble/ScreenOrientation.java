package com.boycoy.powerbubble;

import android.app.Activity;
import android.os.Build;
import com.boycoy.powerbubble.SettingsManager;

public class ScreenOrientation {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$boycoy$powerbubble$ScreenOrientation$Values;
    private Values mOrientationValue;

    public enum Values {
        FULL_SENSOR,
        LANDSCAPE,
        PORTRAIT,
        REVERSE_LANDSCAPE,
        REVERSE_PORTRAIT,
        SENSOR_LANDSCAPE,
        SENSOR_PORTRAIT
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$boycoy$powerbubble$ScreenOrientation$Values() {
        int[] iArr = $SWITCH_TABLE$com$boycoy$powerbubble$ScreenOrientation$Values;
        if (iArr == null) {
            iArr = new int[Values.values().length];
            try {
                iArr[Values.FULL_SENSOR.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[Values.LANDSCAPE.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[Values.PORTRAIT.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[Values.REVERSE_LANDSCAPE.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[Values.REVERSE_PORTRAIT.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[Values.SENSOR_LANDSCAPE.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[Values.SENSOR_PORTRAIT.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            $SWITCH_TABLE$com$boycoy$powerbubble$ScreenOrientation$Values = iArr;
        }
        return iArr;
    }

    public ScreenOrientation() {
        readSharedPreferences();
    }

    public void updateActivity(Activity activity) {
        readSharedPreferences();
        int activityInfoFullSensor = Build.VERSION.SDK_INT >= 9 ? 10 : 4;
        int activityOrientationValue = activityInfoFullSensor;
        switch ($SWITCH_TABLE$com$boycoy$powerbubble$ScreenOrientation$Values()[this.mOrientationValue.ordinal()]) {
            case 1:
                activityOrientationValue = activityInfoFullSensor;
                break;
            case 2:
                activityOrientationValue = 0;
                break;
            case 3:
                activityOrientationValue = 1;
                break;
            case R.styleable.com_admob_android_ads_AdView_refreshInterval:
                activityOrientationValue = 8;
                break;
            case CustomVariable.MAX_CUSTOM_VARIABLES /*5*/:
                activityOrientationValue = 9;
                break;
        }
        activity.setRequestedOrientation(activityOrientationValue);
    }

    public Values get() {
        return this.mOrientationValue;
    }

    public void set(Values value) {
        this.mOrientationValue = value;
        updateSharedPreferences();
    }

    private void updateSharedPreferences() {
        SettingsManager.setInt(SettingsManager.Settings.ORIENTATION, this.mOrientationValue.ordinal());
    }

    private void readSharedPreferences() {
        this.mOrientationValue = Values.values()[SettingsManager.getInt(SettingsManager.Settings.ORIENTATION, Values.FULL_SENSOR.ordinal())];
    }
}
