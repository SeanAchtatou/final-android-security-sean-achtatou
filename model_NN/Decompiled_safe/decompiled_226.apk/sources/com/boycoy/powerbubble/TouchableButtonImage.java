package com.boycoy.powerbubble;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

public class TouchableButtonImage extends FrameLayout implements View.OnTouchListener {
    private ImageView mPressedImageView;
    private ImageView mReleasedImageView;

    public TouchableButtonImage(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray arr = context.obtainStyledAttributes(attrs, R.styleable.TouchableButtonImage);
        int releasedImageResourceId = arr.getResourceId(0, 0);
        int pressedImageResourceId = arr.getResourceId(1, 0);
        arr.recycle();
        this.mPressedImageView = new ImageView(context);
        this.mPressedImageView.setImageResource(pressedImageResourceId);
        addView(this.mPressedImageView);
        this.mPressedImageView.setVisibility(4);
        this.mReleasedImageView = new ImageView(context);
        this.mReleasedImageView.setImageResource(releasedImageResourceId);
        addView(this.mReleasedImageView);
        setOnTouchListener(this);
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            this.mPressedImageView.setVisibility(0);
            this.mReleasedImageView.setVisibility(4);
            onPress();
        }
        if (motionEvent.getAction() == 1) {
            this.mReleasedImageView.setVisibility(0);
            this.mPressedImageView.setVisibility(4);
            onRelease();
        }
        return true;
    }

    public void onPress() {
    }

    public void onRelease() {
    }
}
