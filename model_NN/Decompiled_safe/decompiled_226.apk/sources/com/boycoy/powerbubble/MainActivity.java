package com.boycoy.powerbubble;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.boycoy.powerbubble.Commons;
import com.boycoy.powerbubble.SettingsManager;
import com.boycoy.powerbubble.Tracker;
import com.boycoy.powerbubble.dialogs.DialogManager;
import com.boycoy.powerbubble.dialogs.OrientationDialog;
import com.boycoy.powerbubble.dialogs.TrackingDialog;
import com.boycoy.powerbubble.dialogs.TrackingDialogListener;
import com.boycoy.powerbubble.views.BubbleSurfaceView;
import com.boycoy.powerbubble.views.LcdCharactersCache;
import com.boycoy.powerbubble.views.LcdSurfaceView;
import com.boycoy.powerbubble.views.LockButtonView;
import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdView;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends Activity {
    public static final String BUBBLE_ANIMATION_FRAME_Y = "BUBBLE_ANIMATION_FRAME_Y";
    public static final String BUBBLE_POSITION_X = "BUBBLE_POSTION_X";
    public static final String BUBBLE_POSITION_Y = "BUBBLE_POSTION_Y";
    public static final int DECIMAL_PRECISION = 1;
    public static final String HOLD_IS_ENABLED = "IS_HOLD_ENABLED";
    public static final String LCD_ANGLE_X = "LCD_VALUE";
    private static Application sApplication = null;
    /* access modifiers changed from: private */
    public static boolean sIsFirstLoad = true;
    /* access modifiers changed from: private */
    public Timer mAdTimer = null;
    /* access modifiers changed from: private */
    public boolean mAdVisible = false;
    /* access modifiers changed from: private */
    public TrackingDialog mAllowTrackingDialog = null;
    /* access modifiers changed from: private */
    public DialogManager mDialogManager = null;
    private ResourcesScaler mResourcesScaler = null;
    private ScreenOrientation mScreenOrientation = null;
    private SensorWrapper mSensorWrapper = null;
    private OrientationDialog mSetOrientationDialog = null;
    /* access modifiers changed from: private */
    public boolean mShowStickers = false;
    /* access modifiers changed from: private */
    public Splash mSplash = null;
    private Tracker mTracker = null;

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.set_orientation:
                this.mDialogManager.showDialog(this.mSetOrientationDialog);
                return true;
            case R.id.donate:
                this.mTracker.setDonationVariable(Tracker.DonateValue.DONATE);
                Intent donateIntent = new Intent();
                donateIntent.setAction("android.intent.action.VIEW");
                donateIntent.setData(Uri.parse(getString(R.string.donate_address)));
                startActivity(donateIntent);
                return true;
            case R.id.settings:
                startActivity(new Intent(this, CustomPreferences.class));
                return true;
            default:
                return true;
        }
    }

    /* access modifiers changed from: protected */
    public void onPrepareDialog(int id, Dialog dialog) {
        this.mDialogManager.onPrepareDialog(id, dialog, new Bundle());
        super.onPrepareDialog(id, dialog);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        return this.mDialogManager.onCreateDialog(id, new Bundle());
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        if (((LockButtonView) findViewById(R.id.buttonhold)).isLockEnabled()) {
            bundle.putBoolean(HOLD_IS_ENABLED, true);
            bundle.putFloat(LCD_ANGLE_X, ((LcdSurfaceView) findViewById(R.id.lcdnumberssurfaceview)).getAngleX());
            BubbleSurfaceView bubbleSurfaceView = (BubbleSurfaceView) findViewById(R.id.bubblesurfaceview);
            bundle.putInt(BUBBLE_POSITION_X, bubbleSurfaceView.getPostionX());
            bundle.putInt(BUBBLE_POSITION_Y, bubbleSurfaceView.getPostionY());
            bundle.putInt(BUBBLE_ANIMATION_FRAME_Y, bubbleSurfaceView.getAnimationFrameY());
        } else {
            bundle.putBoolean(HOLD_IS_ENABLED, false);
        }
        super.onSaveInstanceState(bundle);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        SettingsManager.init(this);
        this.mScreenOrientation = new ScreenOrientation();
        if (sApplication == null) {
            sApplication = new Application();
        }
        Commons.init(this);
        initTestDeviceChecker();
        initLcdCharactersCache(sApplication.getLcdCharactersCache());
        initLcdSurfaceView(sApplication.getLcdCharactersCache());
        this.mTracker = new Tracker();
        Calibrator calibrator = Calibrator.getInstance();
        initSensorWrapper(calibrator);
        CalibrateButton calibrateButton = (CalibrateButton) findViewById(R.id.buttoncalibrate);
        calibrateButton.setLcdSurfaceView((LcdSurfaceView) findViewById(R.id.lcdnumberssurfaceview));
        calibrateButton.setCalibrator(calibrator);
        calibrator.setCalibrationListener(calibrateButton);
        this.mDialogManager = new DialogManager(this);
        this.mSetOrientationDialog = new OrientationDialog(this.mScreenOrientation, this.mTracker);
        this.mSetOrientationDialog.setActivityToUpdate(this);
        this.mDialogManager.addDialog(this.mSetOrientationDialog);
        this.mAllowTrackingDialog = new TrackingDialog();
        this.mAllowTrackingDialog.setTrackingDialogListener(new TrackingDialogListener() {
            public void onClick(DialogInterface dialogInterface, int id) {
                if (MainActivity.this.mSplash != null) {
                    MainActivity.this.mSplash.hide();
                }
            }
        });
        this.mDialogManager.addDialog(this.mAllowTrackingDialog);
        this.mResourcesScaler = new ResourcesScaler(this);
        this.mResourcesScaler.scaleResources();
        restoreState(savedInstanceState);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.mScreenOrientation.updateActivity(this);
        this.mSensorWrapper.registerSensors();
        if (SettingsManager.getBoolean(SettingsManager.Settings.ALLOW_TRACKING)) {
            this.mTracker.start(this);
        }
        this.mTracker.trackPageView("/main");
        updateAdState();
        updateDontSleepState();
        updateFullscreenState();
        updateShowadsState();
        updateLcdSoundState();
        registerHoldListeners();
        RelativeLayout mainLayout = (RelativeLayout) findViewById(R.id.main);
        if (!SettingsManager.getBoolean(SettingsManager.Settings.ENABLE_SPLASH) || !sIsFirstLoad) {
            destroySplash();
            mainLayout.setVisibility(0);
            return;
        }
        this.mSplash = new Splash(this);
        setSplashAnimationListener(this.mSplash, (RelativeLayout) findViewById(R.id.main));
        this.mSplash.show();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        if (this.mTracker != null) {
            this.mTracker.dispatch();
            this.mTracker.stop();
        }
        if (this.mSensorWrapper != null) {
            this.mSensorWrapper.unregisterSensors();
        }
        SettingsManager.setBoolean(SettingsManager.Settings.FIRST_RUN, false);
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (isFinishing()) {
            sIsFirstLoad = true;
        }
        this.mSensorWrapper = null;
        this.mSetOrientationDialog = null;
        this.mDialogManager = null;
        this.mAllowTrackingDialog = null;
        this.mScreenOrientation = null;
        this.mTracker = null;
        if (this.mAdTimer != null) {
            this.mAdTimer.cancel();
            this.mAdTimer = null;
        }
        destroyBackground();
        destroyLcdSurfaceView();
        destroyBubbleSurfaceView();
        destroyLocdButtonView();
        AdView adView = (AdView) findViewById(R.id.ad);
        if (adView != null) {
            adView.destroy();
        }
        super.onDestroy();
    }

    public void onWindowFocusChanged(boolean hasFocus) {
        if (this.mSplash != null && hasFocus) {
            View mainLayout = findViewById(R.id.main);
            this.mResourcesScaler.scaleSplashResources(mainLayout.getWidth(), mainLayout.getHeight());
        }
    }

    private void updateAdState() {
        final AdView adView = (AdView) findViewById(R.id.ad);
        final ImageView stickerLeft = (ImageView) findViewById(R.id.sticker_left);
        final ImageView stickerRight = (ImageView) findViewById(R.id.sticker_right);
        adView.setVisibility(4);
        stickerLeft.setVisibility(4);
        stickerRight.setVisibility(4);
        if (SettingsManager.getBoolean(SettingsManager.Settings.SHOW_ADS)) {
            AdRequest adRequest = new AdRequest();
            adRequest.addTestDevice(AdRequest.TEST_EMULATOR);
            adRequest.addTestDevice("E853501765D5D9FE648436332DDC176D");
            adRequest.addTestDevice("7FEA8E150C93EE5E708E647DBE00BCD9");
            final Animation alphaAnimation = AnimationUtils.loadAnimation(this, R.anim.show_ads);
            this.mShowStickers = false;
            if (Commons.getInstance().getOrientation() == Commons.Orientation.LANDSCAPE) {
                this.mShowStickers = true;
            }
            alphaAnimation.setAnimationListener(new Animation.AnimationListener() {
                public void onAnimationEnd(Animation animation) {
                }

                public void onAnimationRepeat(Animation animation) {
                }

                public void onAnimationStart(Animation animation) {
                }
            });
            final Runnable showAd = new Runnable() {
                public void run() {
                    adView.startAnimation(alphaAnimation);
                    adView.setVisibility(0);
                    if (MainActivity.this.mShowStickers) {
                        stickerLeft.startAnimation(alphaAnimation);
                        stickerRight.startAnimation(alphaAnimation);
                        stickerLeft.setVisibility(0);
                        stickerRight.setVisibility(0);
                    }
                }
            };
            this.mAdTimer = null;
            this.mAdVisible = false;
            adView.setAdListener(new AdListener() {
                public void onDismissScreen(Ad arg0) {
                }

                public void onFailedToReceiveAd(Ad arg0, AdRequest.ErrorCode arg1) {
                }

                public void onLeaveApplication(Ad arg0) {
                }

                public void onPresentScreen(Ad arg0) {
                }

                public void onReceiveAd(Ad ad) {
                    if (!MainActivity.this.mAdVisible) {
                        MainActivity.this.mAdVisible = true;
                        adView.setVisibility(4);
                        MainActivity.this.mAdTimer = new Timer();
                        Timer access$5 = MainActivity.this.mAdTimer;
                        final AdView adView = adView;
                        final Runnable runnable = showAd;
                        access$5.schedule(new TimerTask() {
                            public void run() {
                                adView.post(runnable);
                                MainActivity.this.mAdTimer = null;
                            }
                        }, 5000);
                    }
                }
            });
            adView.loadAd(adRequest);
        }
    }

    private void updateDontSleepState() {
        if (SettingsManager.getBoolean(SettingsManager.Settings.DONT_SLEEP)) {
            getWindow().setFlags(128, 128);
        } else {
            getWindow().clearFlags(128);
        }
    }

    private void updateFullscreenState() {
        if (SettingsManager.getBoolean(SettingsManager.Settings.FULLSCREEN)) {
            getWindow().setFlags(1024, 1024);
        } else {
            getWindow().clearFlags(1024);
        }
    }

    private void updateShowadsState() {
        AdView adView = (AdView) findViewById(R.id.ad);
        if (SettingsManager.getBoolean(SettingsManager.Settings.SHOW_ADS)) {
            adView.setVisibility(0);
        } else {
            adView.setVisibility(8);
        }
    }

    private void updateLcdSoundState() {
        ((LcdSurfaceView) findViewById(R.id.lcdnumberssurfaceview)).setEnableSound(SettingsManager.getBoolean(SettingsManager.Settings.ENABLE_SOUND));
    }

    private void registerHoldListeners() {
        LockButtonView lockButtonView = (LockButtonView) findViewById(R.id.buttonhold);
        lockButtonView.addLockListener((LcdSurfaceView) findViewById(R.id.lcdnumberssurfaceview));
        lockButtonView.addLockListener((BubbleSurfaceView) findViewById(R.id.bubblesurfaceview));
        lockButtonView.addLockListener((CalibrateButton) findViewById(R.id.buttoncalibrate));
    }

    private void restoreState(Bundle savedInstanceState) {
        if (savedInstanceState != null && savedInstanceState.getBoolean(HOLD_IS_ENABLED, false)) {
            ((LockButtonView) findViewById(R.id.buttonhold)).forceHoldEnabled();
            float angleX = savedInstanceState.getFloat(LCD_ANGLE_X, 0.0f);
            LcdSurfaceView lcdSurfaceView = (LcdSurfaceView) findViewById(R.id.lcdnumberssurfaceview);
            lcdSurfaceView.setLockEnabled(true);
            lcdSurfaceView.setInitialAngleX(angleX);
            int bubblePostitionX = savedInstanceState.getInt(BUBBLE_POSITION_X, 0);
            int bubblePostitionY = savedInstanceState.getInt(BUBBLE_POSITION_Y, 0);
            int bubbleAnimationFrameY = savedInstanceState.getInt(BUBBLE_ANIMATION_FRAME_Y, 0);
            BubbleSurfaceView bubbleSurfaceView = (BubbleSurfaceView) findViewById(R.id.bubblesurfaceview);
            bubbleSurfaceView.setLockEnabled(true);
            bubbleSurfaceView.setInitialPostionX(bubblePostitionX);
            bubbleSurfaceView.setInitialPostionY(bubblePostitionY);
            bubbleSurfaceView.setInitialAnimationFrameY(bubbleAnimationFrameY);
        }
    }

    private void setSplashAnimationListener(final Splash splash, final RelativeLayout mainLayout) {
        splash.setOnShowAnimationListener(new SplashAnimationListener() {
            public void onAnimationEnd(Animation animation) {
                MainActivity.sIsFirstLoad = false;
                final RelativeLayout relativeLayout = mainLayout;
                final Splash splash = splash;
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        relativeLayout.setVisibility(0);
                        if (SettingsManager.getBoolean(SettingsManager.Settings.FIRST_RUN, true)) {
                            MainActivity.this.mDialogManager.showDialog(MainActivity.this.mAllowTrackingDialog);
                        } else {
                            splash.hide();
                        }
                    }
                }, 2000);
            }
        });
        splash.setOnHideAnimationListener(new SplashAnimationListener() {
            public void onAnimationEnd(Animation animation) {
                MainActivity.this.destroySplash();
            }
        });
    }

    private void initTestDeviceChecker() {
        TestDeviceChecker.init(this);
        TestDeviceChecker.getInstance().addTestDevice("1e3ef4ff3de92448d2a5");
        TestDeviceChecker.getInstance().addTestDevice("5d72a7a6dc6f17ce0ba3");
        TestDeviceChecker.getInstance().addTestDevice("13eb09892b16ac518b8d");
    }

    private void initLcdCharactersCache(LcdCharactersCache lcdCharactersCache) {
        if (!lcdCharactersCache.isCacheInitialized()) {
            lcdCharactersCache.initializeCache(this);
        }
    }

    private void initLcdSurfaceView(LcdCharactersCache lcdCharactersCache) {
        ((LcdSurfaceView) findViewById(R.id.lcdnumberssurfaceview)).setLcdCharactersCache(lcdCharactersCache);
    }

    private void initSensorWrapper(Calibrator calibrator) {
        this.mSensorWrapper = new SensorWrapper(this, calibrator);
        this.mSensorWrapper.addAngleXListener((BubbleSurfaceView) findViewById(R.id.bubblesurfaceview));
        this.mSensorWrapper.addAngleYListener((BubbleSurfaceView) findViewById(R.id.bubblesurfaceview));
        this.mSensorWrapper.addAngleXListener((LcdSurfaceView) findViewById(R.id.lcdnumberssurfaceview));
    }

    /* access modifiers changed from: private */
    public void destroySplash() {
        RelativeLayout mainLayout = (RelativeLayout) findViewById(R.id.main);
        RelativeLayout splashRelativeLayout = (RelativeLayout) findViewById(R.id.splash);
        if (mainLayout != null && splashRelativeLayout != null) {
            mainLayout.removeView(splashRelativeLayout);
            this.mSplash = null;
        }
    }

    private void destroyBackground() {
        RelativeLayout mainLayout = (RelativeLayout) findViewById(R.id.main);
        View backgroundRelativeLayout = findViewById(R.id.background);
        if (mainLayout != null && backgroundRelativeLayout != null) {
            mainLayout.removeView(backgroundRelativeLayout);
        }
    }

    private void destroyLcdSurfaceView() {
        RelativeLayout mainLayout = (RelativeLayout) findViewById(R.id.main);
        View lcdSurfaceView = findViewById(R.id.lcdnumberssurfaceview);
        if (mainLayout != null && lcdSurfaceView != null) {
            mainLayout.removeView(lcdSurfaceView);
        }
    }

    private void destroyBubbleSurfaceView() {
        RelativeLayout mainLayout = (RelativeLayout) findViewById(R.id.main);
        View bubbleSurfaceView = findViewById(R.id.bubblesurfaceview);
        if (mainLayout != null && bubbleSurfaceView != null) {
            mainLayout.removeView(bubbleSurfaceView);
        }
    }

    private void destroyLocdButtonView() {
        RelativeLayout mainLayout = (RelativeLayout) findViewById(R.id.main);
        View lockButtonView = findViewById(R.id.buttonhold);
        if (mainLayout != null && lockButtonView != null) {
            mainLayout.removeView(lockButtonView);
        }
    }
}
