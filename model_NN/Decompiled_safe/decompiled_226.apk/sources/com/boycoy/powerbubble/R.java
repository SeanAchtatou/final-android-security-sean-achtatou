package com.boycoy.powerbubble;

public final class R {

    public static final class anim {
        public static final int show_ads = 2130968576;
        public static final int splash_in = 2130968577;
        public static final int splash_out = 2130968578;
    }

    public static final class array {
        public static final int modes_orientation_dialog = 2131230720;
    }

    public static final class attr {
        public static final int backgroundColor = 2130771970;
        public static final int keywords = 2130771973;
        public static final int primaryTextColor = 2130771971;
        public static final int refreshInterval = 2130771974;
        public static final int secondaryTextColor = 2130771972;
        public static final int src_pressed = 2130771969;
        public static final int src_released = 2130771968;
    }

    public static final class drawable {
        public static final int background = 2130837504;
        public static final int background_port = 2130837505;
        public static final int bubble_1 = 2130837506;
        public static final int bubble_10 = 2130837507;
        public static final int bubble_2 = 2130837508;
        public static final int bubble_3 = 2130837509;
        public static final int bubble_4 = 2130837510;
        public static final int bubble_5 = 2130837511;
        public static final int bubble_6 = 2130837512;
        public static final int bubble_7 = 2130837513;
        public static final int bubble_8 = 2130837514;
        public static final int bubble_9 = 2130837515;
        public static final int bubble_border = 2130837516;
        public static final int button_calibrate_pressed = 2130837517;
        public static final int button_calibrate_released = 2130837518;
        public static final int button_lock = 2130837519;
        public static final int button_lock_background = 2130837520;
        public static final int button_settings_pressed = 2130837521;
        public static final int button_settings_released = 2130837522;
        public static final int icon = 2130837523;
        public static final int lcd_background = 2130837524;
        public static final int lcd_background_bottom = 2130837525;
        public static final int lcd_characters_letters = 2130837526;
        public static final int lcd_characters_numbers = 2130837527;
        public static final int lcd_characters_special = 2130837528;
        public static final int lcd_gloss = 2130837529;
        public static final int liquid_background = 2130837530;
        public static final int liquid_border_background = 2130837531;
        public static final int liquid_foreground = 2130837532;
        public static final int logo_boycoy = 2130837533;
        public static final int logo_powerbubble = 2130837534;
        public static final int menu_donate = 2130837535;
        public static final int menu_orientation = 2130837536;
        public static final int reference = 2130837537;
        public static final int splash_background_land = 2130837538;
        public static final int splash_background_port = 2130837539;
        public static final int splash_boycoy = 2130837540;
        public static final int splash_powerbubble = 2130837541;
        public static final int sticker_left = 2130837542;
        public static final int sticker_right = 2130837543;
    }

    public static final class id {
        public static final int ad = 2131361792;
        public static final int adspacercenter = 2131361793;
        public static final int background = 2131361796;
        public static final int bubble_border = 2131361809;
        public static final int bubblesurfaceview = 2131361797;
        public static final int buttoncalibrate = 2131361798;
        public static final int buttonhold = 2131361799;
        public static final int buttonsettings = 2131361801;
        public static final int buttonslayoutport = 2131361803;
        public static final int buttonsspacercenter = 2131361800;
        public static final int donate = 2131361816;
        public static final int lcdgloss = 2131361806;
        public static final int lcdlayout = 2131361804;
        public static final int lcdnumberssurfaceview = 2131361805;
        public static final int logoboycoy = 2131361802;
        public static final int logopowerbubble = 2131361807;
        public static final int main = 2131361808;
        public static final int set_orientation = 2131361815;
        public static final int settings = 2131361817;
        public static final int splash = 2131361810;
        public static final int splash_background = 2131361811;
        public static final int splash_boycoy = 2131361814;
        public static final int splash_powerbubble = 2131361812;
        public static final int splash_powerbubble_spacer = 2131361813;
        public static final int sticker_left = 2131361794;
        public static final int sticker_right = 2131361795;
    }

    public static final class layout {
        public static final int ad = 2130903040;
        public static final int background = 2130903041;
        public static final int bubble = 2130903042;
        public static final int buttons = 2130903043;
        public static final int lcd = 2130903044;
        public static final int logo_powerbubble = 2130903045;
        public static final int main = 2130903046;
        public static final int splash = 2130903047;
    }

    public static final class menu {
        public static final int menu = 2131296256;
    }

    public static final class raw {
        public static final int beep = 2131099648;
    }

    public static final class string {
        public static final int app_name = 2131165184;
        public static final int calibration_lcd = 2131165191;
        public static final int cancel_general = 2131165188;
        public static final int disable_lock_to_calibrate = 2131165189;
        public static final int donate_address = 2131165186;
        public static final int hold_to_calibrate_lcd = 2131165190;
        public static final int menu_donate = 2131165207;
        public static final int menu_donate_condensed = 2131165208;
        public static final int menu_set_orientation = 2131165205;
        public static final int menu_set_orientation_condensed = 2131165206;
        public static final int menu_settings = 2131165209;
        public static final int menu_settings_condensed = 2131165210;
        public static final int negative_donate_dialog = 2131165199;
        public static final int negative_reset_calibration_dialog = 2131165203;
        public static final int neutral_tracking_dialog = 2131165195;
        public static final int ok_general = 2131165187;
        public static final int positive_donate_dialog = 2131165198;
        public static final int positive_reset_calibration_dialog = 2131165202;
        public static final int summary_about_preference = 2131165252;
        public static final int summary_allow_tracking_preference = 2131165236;
        public static final int summary_calibrate_landscape_preference = 2131165242;
        public static final int summary_calibrate_portrait_preference = 2131165240;
        public static final int summary_calibrate_preference = 2131165238;
        public static final int summary_calibrate_reversed_landscape_preference = 2131165246;
        public static final int summary_calibrate_reversed_portrait_preference = 2131165244;
        public static final int summary_check_accelerometer_preference = 2131165248;
        public static final int summary_donate_preference = 2131165218;
        public static final int summary_dont_sleep_preference = 2131165226;
        public static final int summary_enable_decimal = 2131165228;
        public static final int summary_enable_sound = 2131165230;
        public static final int summary_enable_splash = 2131165232;
        public static final int summary_fullscreen_preference = 2131165224;
        public static final int summary_reset_calibration = 2131165250;
        public static final int summary_set_orientation = 2131165222;
        public static final int summary_show_ads_preference = 2131165234;
        public static final int summary_why_donate_preference = 2131165220;
        public static final int text_donate_dialog = 2131165197;
        public static final int text_reset_calibration_dialog = 2131165201;
        public static final int text_tracking_dialog = 2131165194;
        public static final int title_about_preference = 2131165251;
        public static final int title_about_preferencecategory = 2131165216;
        public static final int title_allow_tracking_preference = 2131165235;
        public static final int title_calibrate_landscape_preference = 2131165241;
        public static final int title_calibrate_portrait_preference = 2131165239;
        public static final int title_calibrate_preference = 2131165237;
        public static final int title_calibrate_reversed_landscape_preference = 2131165245;
        public static final int title_calibrate_reversed_portrait_preference = 2131165243;
        public static final int title_check_accelerometer_preference = 2131165247;
        public static final int title_display_settings_preferencecategory = 2131165213;
        public static final int title_donate_dialog = 2131165196;
        public static final int title_donate_preference = 2131165217;
        public static final int title_donate_preferencecategory = 2131165212;
        public static final int title_dont_sleep_preference = 2131165225;
        public static final int title_enable_decimal = 2131165227;
        public static final int title_enable_sound = 2131165229;
        public static final int title_enable_splash = 2131165231;
        public static final int title_fullscreen_preference = 2131165223;
        public static final int title_main_settings_preferencecategory = 2131165214;
        public static final int title_maintenance_preferencecategory = 2131165215;
        public static final int title_orietation_dialog = 2131165204;
        public static final int title_preferences = 2131165211;
        public static final int title_reset_calibration = 2131165249;
        public static final int title_reset_calibration_dialog = 2131165200;
        public static final int title_set_orientation = 2131165221;
        public static final int title_show_ads_preference = 2131165233;
        public static final int title_tracking_dialog = 2131165193;
        public static final int title_why_donate_preference = 2131165219;
        public static final int version_name = 2131165185;
        public static final int wait_lcd = 2131165192;
    }

    public static final class styleable {
        public static final int[] TouchableButtonImage = {R.attr.src_released, R.attr.src_pressed};
        public static final int TouchableButtonImage_src_pressed = 1;
        public static final int TouchableButtonImage_src_released = 0;
        public static final int[] com_admob_android_ads_AdView = {R.attr.backgroundColor, R.attr.primaryTextColor, R.attr.secondaryTextColor, R.attr.keywords, R.attr.refreshInterval};
        public static final int com_admob_android_ads_AdView_backgroundColor = 0;
        public static final int com_admob_android_ads_AdView_keywords = 3;
        public static final int com_admob_android_ads_AdView_primaryTextColor = 1;
        public static final int com_admob_android_ads_AdView_refreshInterval = 4;
        public static final int com_admob_android_ads_AdView_secondaryTextColor = 2;
    }

    public static final class xml {
        public static final int preferences = 2131034112;
    }
}
