package com.boycoy.powerbubble;

public abstract class ConstantFpsThread extends Thread {
    protected long mDesiredTime;
    private long mLastTime;
    private boolean mPause = false;
    private boolean mRun = false;

    /* access modifiers changed from: protected */
    public abstract void onFrameDraw(long j);

    public ConstantFpsThread() {
        setFps(30);
    }

    public boolean getRunning() {
        return this.mRun;
    }

    public boolean getPaused() {
        return this.mPause;
    }

    public void setRunning(boolean value) {
        this.mRun = value;
    }

    public void setPaused(boolean value) {
        this.mPause = value;
    }

    public void setFps(int value) {
        this.mDesiredTime = (long) (1000 / value);
    }

    public void run() {
        this.mLastTime = 0;
        while (this.mRun) {
            long currentTime = System.currentTimeMillis();
            long elapsedTime = currentTime - this.mLastTime;
            if (elapsedTime >= this.mDesiredTime) {
                if (this.mLastTime > 0 && !this.mPause) {
                    onFrameDraw(elapsedTime);
                }
                this.mLastTime = currentTime;
            } else {
                try {
                    Thread.sleep(this.mDesiredTime - elapsedTime);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
