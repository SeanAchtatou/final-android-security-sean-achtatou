package com.boycoy.powerbubble;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.view.Display;
import android.view.WindowManager;

public class Commons {
    private static Commons sInstance;
    private Display mDisplay;
    private float mScaleHeight;
    private float mScaleWidth;

    public enum Orientation {
        LANDSCAPE,
        PORTRAIT
    }

    private Commons(Context context) {
        Bitmap referenceBitmap = ((BitmapDrawable) context.getResources().getDrawable(R.drawable.reference)).getBitmap();
        this.mScaleWidth = ((float) referenceBitmap.getWidth()) / 400.0f;
        this.mScaleHeight = ((float) referenceBitmap.getHeight()) / 400.0f;
        this.mDisplay = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
    }

    public static void init(Context context) {
        sInstance = new Commons(context);
    }

    public static Commons getInstance() {
        return sInstance;
    }

    public float getResolutionWidthMultiplier() {
        return this.mScaleWidth;
    }

    public float getResolutionHeightMultiplier() {
        return this.mScaleHeight;
    }

    public int getRotation() {
        return Build.VERSION.SDK_INT >= 8 ? this.mDisplay.getRotation() : this.mDisplay.getOrientation();
    }

    public Orientation getOrientation() {
        return ((float) this.mDisplay.getWidth()) / ((float) this.mDisplay.getHeight()) > 1.0f ? Orientation.LANDSCAPE : Orientation.PORTRAIT;
    }
}
