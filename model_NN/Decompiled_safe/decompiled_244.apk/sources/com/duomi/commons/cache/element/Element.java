package com.duomi.commons.cache.element;

import java.io.IOException;
import java.io.Serializable;

public class Element implements Serializable {
    private long a;
    private long b;
    private int c;
    private String d;
    private Serializable e;

    public Element(String str, Serializable serializable) {
        this.d = str;
        this.e = serializable;
        d();
        try {
            this.c = ik.b(serializable);
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    public String a() {
        return this.d;
    }

    public Serializable b() {
        return this.e;
    }

    public long c() {
        return this.a;
    }

    public void d() {
        this.a = System.currentTimeMillis();
    }

    public long e() {
        return this.b;
    }

    public void f() {
        this.b = System.currentTimeMillis();
    }

    public int g() {
        return this.c;
    }
}
