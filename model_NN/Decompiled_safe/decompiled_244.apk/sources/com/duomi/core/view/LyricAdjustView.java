package com.duomi.core.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import com.duomi.android.R;
import java.util.ArrayList;

public class LyricAdjustView extends View {
    public int a = 0;
    private ArrayList b;
    private float c;
    private ju d;

    public LyricAdjustView(Context context) {
        super(context);
        a(context);
    }

    public LyricAdjustView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    private void a(Context context) {
        this.b = new ArrayList();
        this.b.add(BitmapFactory.decodeResource(getResources(), R.drawable.lyric1));
        this.b.add(BitmapFactory.decodeResource(getResources(), R.drawable.lyric2));
        this.b.add(BitmapFactory.decodeResource(getResources(), R.drawable.lyric3));
    }

    private void a(boolean z) {
        if (z) {
            this.a++;
            if (this.a >= 3) {
                this.a = 0;
                return;
            }
            return;
        }
        this.a--;
        if (this.a < 0) {
            this.a = 2;
        }
    }

    public void a(ju juVar) {
        this.d = juVar;
    }

    public void onDraw(Canvas canvas) {
        if (this.b != null) {
            canvas.drawBitmap((Bitmap) this.b.get(this.a), 0.0f, 0.0f, (Paint) null);
        }
    }

    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        float b2 = (float) (((double) en.b(getContext())) / 320.0d);
        setMeasuredDimension((int) (32.0f * b2), (int) (b2 * 210.0f));
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                this.c = motionEvent.getY();
                break;
            case 2:
                float y = motionEvent.getY();
                if (Math.abs(y - this.c) > 10.0f) {
                    if (y - this.c <= 0.0f) {
                        a(true);
                        if (this.d != null) {
                            this.d.a(true);
                        }
                        invalidate();
                        this.c = y;
                        break;
                    } else {
                        a(false);
                        if (this.d != null) {
                            this.d.a(false);
                        }
                        invalidate();
                        this.c = y;
                        break;
                    }
                }
                break;
        }
        return true;
    }
}
