package com.duomi.core.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.media.AudioManager;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import com.duomi.android.R;

public class VolumeView extends View {
    Bitmap a;
    Bitmap b;
    Bitmap c;
    Paint d;
    public int e;
    int f = 5;
    int g = 29;
    int h = 0;
    int i = 0;
    int j = 0;
    public AudioManager k;
    boolean l = false;
    public Handler m = new jv(this);
    /* access modifiers changed from: private */
    public jw n = null;

    public VolumeView(Context context) {
        super(context);
        a(context);
    }

    public VolumeView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    private void a(int i2) {
        this.e = i2;
        invalidate();
        Message obtainMessage = this.m.obtainMessage(0);
        this.m.removeMessages(0);
        this.m.sendMessageDelayed(obtainMessage, 20);
    }

    private void a(Context context) {
        this.k = (AudioManager) context.getSystemService("audio");
        this.a = BitmapFactory.decodeResource(getResources(), R.drawable.voicelayout_down_bg);
        this.b = BitmapFactory.decodeResource(getResources(), R.drawable.vol_enable);
        this.c = BitmapFactory.decodeResource(getResources(), R.drawable.vol_disable);
        this.d = new Paint();
        switch (en.b(getContext())) {
            case 240:
                this.h = 12;
                this.i = 5;
                this.j = 125;
                this.g = 22;
                return;
            case 320:
                this.h = 15;
                this.i = 7;
                this.j = 165;
                this.g = 23;
                return;
            case 480:
                this.h = 28;
                this.i = 10;
                this.j = 229;
                this.g = 29;
                return;
            default:
                this.h = 28;
                this.i = 10;
                this.j = 229;
                this.g = 29;
                return;
        }
    }

    private void a(MotionEvent motionEvent) {
        int i2 = (this.j - this.f) - this.g;
        int y = (int) motionEvent.getY();
        a((int) (((y < this.f ? 1.0f : y > this.j - this.g ? 0.0f : ((float) ((i2 - y) + this.f)) / ((float) i2)) * 20.0f) + 0.0f));
    }

    public void a() {
        this.e = (this.k.getStreamVolume(3) * 20) / 15;
    }

    public void a(jw jwVar) {
        this.n = jwVar;
    }

    public void b() {
        this.e++;
        if (this.e >= 20) {
            this.e = 20;
        }
        a(this.e);
    }

    public void c() {
        this.e--;
        if (this.e <= 0) {
            this.e = 0;
        }
        a(this.e);
    }

    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawBitmap(this.a, 0.0f, (float) 0, this.d);
        int i2 = this.f;
        for (int i3 = 0; i3 < 20; i3++) {
            if (i3 < 20 - this.e) {
                canvas.drawBitmap(this.c, (float) this.h, (float) i2, this.d);
            } else {
                canvas.drawBitmap(this.b, (float) this.h, (float) i2, this.d);
            }
            i2 += this.i;
        }
    }

    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        switch (en.b(getContext())) {
            case 240:
                setMeasuredDimension(40, 125);
                return;
            case 320:
                setMeasuredDimension(48, 165);
                return;
            case 480:
                setMeasuredDimension(68, 229);
                return;
            default:
                setMeasuredDimension(68, 229);
                return;
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.l) {
            return false;
        }
        switch (motionEvent.getAction()) {
            case 0:
                setPressed(true);
                a(motionEvent);
                break;
            case 1:
                a(motionEvent);
                setPressed(false);
                invalidate();
                break;
            case 2:
                a(motionEvent);
                break;
            case 3:
                a(motionEvent);
                setPressed(false);
                invalidate();
                break;
        }
        return true;
    }
}
