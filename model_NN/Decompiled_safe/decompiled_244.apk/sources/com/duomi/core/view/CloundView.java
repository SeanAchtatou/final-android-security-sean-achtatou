package com.duomi.core.view;

import android.content.Context;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.duomi.android.R;
import java.util.ArrayList;

public class CloundView extends LinearLayout {
    private static int f = 60;
    private static int g = 180;
    private static int h = 10;
    private static int i = 20;
    public ArrayList a;
    public ArrayList b;
    ArrayList c;
    ArrayList d;
    public LinearLayout.LayoutParams e;
    /* access modifiers changed from: private */
    public je j;
    private int k;
    private int l;
    private int m;
    private float n;
    private jf o = new jf(this, 6);
    private View.OnClickListener p = new jd(this);

    public CloundView(Context context) {
        super(context);
        setOrientation(1);
        a();
    }

    public CloundView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setOrientation(1);
        a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private TextView a(int i2, float f2, LayoutInflater layoutInflater) {
        TextView textView = (TextView) layoutInflater.inflate((int) R.layout.cloundtext, (ViewGroup) null, false);
        textView.setId(i2);
        textView.setText((CharSequence) this.a.get(i2));
        textView.setWidth(((int) f2) - (h * 2));
        a(textView);
        textView.setOnClickListener(this.p);
        return textView;
    }

    private void a() {
        this.n = getContext().getResources().getDisplayMetrics().density;
        this.k = (int) (this.n * 65.0f);
        this.l = (int) (this.n * 320.0f);
        this.m = (int) (this.n * 10.0f);
        f = (int) (this.n * ((float) f));
        g = (int) (this.n * ((float) g));
        h = (int) (this.n * ((float) h));
        i = (int) (this.n * ((float) i));
        setOrientation(1);
        this.e = new LinearLayout.LayoutParams(-1, -2);
        this.e.gravity = 1;
        this.e.topMargin = this.m;
        this.e.bottomMargin = this.m;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void a(LayoutInflater layoutInflater, int i2) {
        boolean z;
        Paint paint = new Paint();
        paint.setTextSize(((TextView) layoutInflater.inflate((int) R.layout.cloundtext, (ViewGroup) null, false)).getTextSize());
        ArrayList arrayList = new ArrayList();
        for (int i3 = 0; i3 < i2; i3++) {
            float measureText = paint.measureText((String) this.a.get(i3)) + ((float) i) + ((float) (h * 2));
            ad.b("CloundView", "strLength>>" + measureText);
            float f2 = measureText < ((float) f) ? (float) f : measureText;
            int i4 = 0;
            while (true) {
                if (i4 >= this.b.size()) {
                    z = true;
                    break;
                } else if (f2 <= ((Float) this.b.get(i4)).floatValue()) {
                    this.b.add(i4, Float.valueOf(f2));
                    arrayList.add(i4, this.a.get(i3));
                    z = false;
                    break;
                } else {
                    i4++;
                }
            }
            if (z) {
                this.b.add(Float.valueOf(f2));
                arrayList.add(this.a.get(i3));
            }
        }
        this.a = arrayList;
    }

    private void a(View view) {
        switch (this.o.a()) {
            case 0:
                view.setBackgroundResource(R.drawable.searchtext_bg_gray);
                return;
            case 1:
                view.setBackgroundResource(R.drawable.searchtext_bg_green);
                return;
            case 2:
                view.setBackgroundResource(R.drawable.searchtext_bg_red);
                return;
            case 3:
                view.setBackgroundResource(R.drawable.searchtext_bg_yellow);
                return;
            case 4:
                view.setBackgroundResource(R.drawable.searchtext_bg_orange);
                return;
            case 5:
                view.setBackgroundResource(R.drawable.searchtext_bg_blue);
                return;
            default:
                return;
        }
    }

    private void a(LinearLayout linearLayout, float f2) {
        boolean z;
        int size = this.c.size();
        int i2 = 0;
        while (true) {
            if (i2 >= size) {
                z = true;
                break;
            } else if (f2 <= ((Float) this.c.get(i2)).floatValue()) {
                this.c.add(i2, Float.valueOf(f2));
                this.d.add(i2, linearLayout);
                z = false;
                break;
            } else {
                i2++;
            }
        }
        if (z) {
            this.d.add(linearLayout);
            this.c.add(Float.valueOf(f2));
        }
    }

    private LinearLayout b() {
        LinearLayout linearLayout = new LinearLayout(getContext());
        linearLayout.setOrientation(0);
        linearLayout.setGravity(1);
        return linearLayout;
    }

    public void a(ArrayList arrayList) {
        long currentTimeMillis = System.currentTimeMillis();
        if (arrayList != null && arrayList.size() != 0) {
            if (getChildCount() > 0) {
                removeAllViews();
            }
            LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService("layout_inflater");
            this.a = arrayList;
            int size = this.a.size();
            this.b = new ArrayList();
            this.d = new ArrayList();
            this.c = new ArrayList();
            a(layoutInflater, size);
            LinearLayout b2 = b();
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
            layoutParams.leftMargin = h;
            layoutParams.rightMargin = h;
            float f2 = 0.0f;
            for (int i2 = 0; i2 < size; i2++) {
                float floatValue = ((Float) this.b.get(i2)).floatValue();
                TextView a2 = a(i2, floatValue, layoutInflater);
                if (floatValue >= ((float) ((this.l * 2) / 3))) {
                    if (f2 > 0.0f) {
                        a(b2, f2);
                        LinearLayout b3 = b();
                        b3.addView(a2, layoutParams);
                        a(b3, floatValue);
                        b2 = b();
                    } else {
                        LinearLayout b4 = b();
                        b4.addView(a2, layoutParams);
                        a(b4, floatValue);
                        b2 = b();
                    }
                    f2 = 0.0f;
                } else {
                    if (floatValue + f2 >= ((float) ((this.l * 3) / 4))) {
                        a(b2, f2);
                        b2 = b();
                        b2.addView(a2, layoutParams);
                        f2 = floatValue;
                    } else {
                        b2.addView(a2, layoutParams);
                        f2 += floatValue;
                    }
                    if (i2 == size - 1 && f2 > 0.0f) {
                        a(b2, f2);
                    }
                }
            }
            int size2 = this.d.size();
            for (int i3 = 0; i3 < size2; i3 += 2) {
                addView((View) this.d.get(i3), this.e);
            }
            for (int i4 = (size2 - 1) - (size2 % 2); i4 > 0; i4 -= 2) {
                addView((View) this.d.get(i4), this.e);
            }
            invalidate();
            ad.b("CloundView", "time>>" + (System.currentTimeMillis() - currentTimeMillis));
        }
    }

    public void a(je jeVar) {
        this.j = jeVar;
    }

    public void a(String[] strArr) {
        if (strArr != null && strArr.length > 0) {
            ArrayList arrayList = new ArrayList();
            for (int i2 = 0; i2 < strArr.length; i2++) {
                if (!en.c(strArr[i2]) && !arrayList.contains(strArr[i2])) {
                    arrayList.add(strArr[i2]);
                }
            }
            a(arrayList);
        }
    }
}
