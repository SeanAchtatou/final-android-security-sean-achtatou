package com.duomi.core.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Handler;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import com.duomi.android.R;

public class DrawerView extends ViewGroup {
    /* access modifiers changed from: private */
    public boolean A;
    private final int B;
    private final int C;
    private final int D;
    private final int E;
    private final int F;
    private final int G;
    private int H;
    private int I;
    private int J;
    private int K;
    private float L;
    private float M;
    private boolean N;
    private View a;
    private View b;
    private View c;
    private View d;
    private View e;
    private final Rect f;
    private final Rect g;
    private boolean h;
    /* access modifiers changed from: private */
    public boolean i;
    private boolean j;
    private int k;
    private int l;
    private int m;
    private VelocityTracker n;
    private jo o;
    private jn p;
    private jp q;
    private final Handler r;
    private float s;
    private float t;
    private float u;
    private long v;
    private long w;
    private int x;
    private boolean y;
    private boolean z;

    public DrawerView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        this.K = ViewConfiguration.get(getContext()).getScaledTouchSlop() * 2;
    }

    public DrawerView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet);
        this.f = new Rect();
        this.g = new Rect();
        this.r = new jq(this);
        this.J = 0;
        this.N = false;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.c);
        int resourceId = obtainStyledAttributes.getResourceId(0, 0);
        if (resourceId == 0) {
            throw new IllegalArgumentException("The handle attribute is required and must refer to a valid child.");
        }
        int resourceId2 = obtainStyledAttributes.getResourceId(1, 0);
        if (resourceId2 == 0) {
            throw new IllegalArgumentException("The content attribute is required and must refer to a valid child.");
        } else if (resourceId == resourceId2) {
            throw new IllegalArgumentException("The content and handle attributes must refer to different children.");
        } else {
            this.H = resourceId;
            this.I = resourceId2;
            this.k = 0;
            this.l = 5;
            this.z = true;
            this.A = true;
            this.J = en.a(en.b(getContext()));
            float f2 = getResources().getDisplayMetrics().density;
            this.B = (int) ((6.0f * f2) + 0.5f);
            this.C = (int) ((100.0f * f2) + 0.5f);
            this.D = (int) ((150.0f * f2) + 0.5f);
            this.E = (int) ((200.0f * f2) + 0.5f);
            this.F = (int) ((2000.0f * f2) + 0.5f);
            this.G = (int) ((f2 * 1000.0f) + 0.5f);
            obtainStyledAttributes.recycle();
            setAlwaysDrawnWithCacheEnabled(false);
            this.K = ViewConfiguration.get(getContext()).getScaledTouchSlop();
        }
    }

    private void a(int i2) {
        c(i2);
        a(i2, (float) this.F, true);
    }

    private void a(int i2, float f2, boolean z2) {
        this.u = (float) i2;
        this.t = f2;
        if (this.j) {
            if (z2 || f2 > ((float) this.E) || (i2 > this.l + this.m && f2 > ((float) (-this.E)))) {
                this.s = (float) this.F;
                if (f2 < 0.0f) {
                    this.t = 0.0f;
                }
            } else {
                this.s = (float) (-this.F);
                if (f2 > 0.0f) {
                    this.t = 0.0f;
                }
            }
        } else if (z2 || (f2 <= ((float) this.E) && (i2 <= getHeight() / 2 || f2 <= ((float) (-this.E))))) {
            this.s = (float) (-this.F);
            if (f2 > 0.0f) {
                this.t = 0.0f;
            }
        } else {
            this.s = (float) this.F;
            if (f2 < 0.0f) {
                this.t = 0.0f;
            }
        }
        long uptimeMillis = SystemClock.uptimeMillis();
        this.v = uptimeMillis;
        this.w = uptimeMillis + 16;
        this.y = true;
        this.r.removeMessages(1000);
        this.r.sendMessageAtTime(this.r.obtainMessage(1000), this.w);
        h();
    }

    private void b(int i2) {
        c(i2);
        a(i2, (float) (-this.F), true);
    }

    private void c(int i2) {
        this.h = true;
        this.n = VelocityTracker.obtain();
        if (!this.j) {
            this.s = (float) this.F;
            this.t = (float) this.E;
            this.u = (float) ((this.k + getHeight()) - this.m);
            d((int) this.u);
            this.y = true;
            this.r.removeMessages(1000);
            long uptimeMillis = SystemClock.uptimeMillis();
            this.v = uptimeMillis;
            this.w = uptimeMillis + 16;
            this.y = true;
            return;
        }
        if (this.y) {
            this.y = false;
            this.r.removeMessages(1000);
        }
        d(i2);
    }

    private void d(int i2) {
        View view = this.d;
        if (i2 == -10001) {
            view.offsetTopAndBottom(this.l - view.getTop());
            invalidate();
        } else if (i2 == -10002) {
            view.offsetTopAndBottom((((this.k + getBottom()) - getTop()) - this.m) - view.getTop());
            invalidate();
        } else {
            int top = view.getTop();
            int i3 = i2 - top;
            int bottom = i2 < this.l ? this.l - top : i3 > (((this.k + getBottom()) - getTop()) - this.m) - top ? (((this.k + getBottom()) - getTop()) - this.m) - top : i3;
            view.offsetTopAndBottom(bottom);
            Rect rect = this.f;
            Rect rect2 = this.g;
            view.getHitRect(rect);
            rect2.set(rect);
            rect2.union(rect.left, rect.top - bottom, rect.right, rect.bottom - bottom);
            rect2.union(0, rect.bottom - bottom, getWidth(), (rect.bottom - bottom) + this.e.getHeight());
            invalidate(rect2);
        }
    }

    private void g() {
        if (!this.y) {
            View view = this.e;
            if (view.isLayoutRequested()) {
                int i2 = this.m;
                view.measure(View.MeasureSpec.makeMeasureSpec(en.b(getContext()), 1073741824), View.MeasureSpec.makeMeasureSpec(((getBottom() - getTop()) - i2) - this.l, 1073741824));
                view.layout(0, this.l + i2, view.getMeasuredWidth(), i2 + this.l + view.getMeasuredHeight());
            }
            view.getViewTreeObserver().dispatchOnPreDraw();
            view.buildDrawingCache();
            view.setVisibility(8);
        }
    }

    private void h() {
        this.c.setPressed(false);
        this.h = false;
        if (this.q != null) {
            this.q.b();
        }
        if (this.n != null) {
            this.n.recycle();
            this.n = null;
        }
    }

    /* access modifiers changed from: private */
    public void i() {
        if (this.y) {
            j();
            if (this.u >= ((float) ((this.k + getHeight()) - 1))) {
                this.y = false;
                k();
            } else if (this.u < ((float) this.l)) {
                this.y = false;
                l();
            } else {
                d((int) this.u);
                this.w += 16;
                this.r.sendMessageAtTime(this.r.obtainMessage(1000), this.w);
            }
        }
    }

    private void j() {
        long uptimeMillis = SystemClock.uptimeMillis();
        float f2 = ((float) (uptimeMillis - this.v)) / 1000.0f;
        float f3 = this.u;
        float f4 = this.t;
        float f5 = this.s;
        this.u = f3 + (f4 * f2) + (0.5f * f5 * f2 * f2);
        this.t = (f2 * f5) + f4;
        this.v = uptimeMillis;
    }

    private void k() {
        d(-10002);
        this.e.setVisibility(8);
        this.e.destroyDrawingCache();
        if (this.j) {
            this.j = false;
            if (this.p != null) {
                this.p.a();
            }
        }
    }

    private void l() {
        d(-10001);
        this.e.setVisibility(0);
        if (!this.j) {
            this.j = true;
            if (this.o != null) {
                this.o.a();
            }
        } else if (this.o != null) {
            this.o.b();
        }
    }

    public void a() {
        if (!this.j) {
            l();
        } else {
            k();
        }
        invalidate();
        requestLayout();
    }

    public void a(jn jnVar) {
        this.p = jnVar;
    }

    public void a(jo joVar) {
        this.o = joVar;
    }

    public void a(jp jpVar) {
        this.q = jpVar;
    }

    public void b() {
        if (!this.j) {
            e();
        } else {
            d();
        }
    }

    public void c() {
        l();
        invalidate();
        requestLayout();
    }

    public void d() {
        g();
        jp jpVar = this.q;
        if (jpVar != null) {
            jpVar.a();
        }
        a(this.d.getTop());
        if (jpVar != null) {
            jpVar.b();
        }
    }

    /* access modifiers changed from: protected */
    public void dispatchDraw(Canvas canvas) {
        long drawingTime = getDrawingTime();
        View view = this.d;
        drawChild(canvas, view, drawingTime);
        if (this.h || this.y) {
            Bitmap drawingCache = this.e.getDrawingCache();
            if (drawingCache != null) {
                canvas.drawBitmap(drawingCache, 0.0f, (float) view.getBottom(), (Paint) null);
                return;
            }
            canvas.save();
            canvas.translate(0.0f, (float) (view.getTop() - this.l));
            drawChild(canvas, this.e, drawingTime);
            canvas.restore();
        } else if (this.j) {
            drawChild(canvas, this.e, drawingTime);
        }
    }

    public void e() {
        g();
        jp jpVar = this.q;
        if (jpVar != null) {
            jpVar.a();
        }
        b(this.d.getTop());
        if (jpVar != null) {
            this.N = true;
            jpVar.b();
            this.N = false;
        }
    }

    public boolean f() {
        return this.j;
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        this.d = findViewById(this.H);
        if (this.d == null) {
            throw new IllegalArgumentException("The handle attribute is must refer to an existing child.");
        }
        this.d.setOnClickListener(new jm(this));
        this.e = findViewById(this.I);
        if (this.e == null) {
            throw new IllegalArgumentException("The content attribute is must refer to an existing child.");
        }
        this.e.setVisibility(8);
        this.a = findViewById(R.id.handle_left);
        this.b = findViewById(R.id.handle_right);
        this.c = findViewById(R.id.handlebar);
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        if (this.i) {
            return false;
        }
        int action = motionEvent.getAction();
        int x2 = (int) motionEvent.getX();
        int y2 = (int) motionEvent.getY();
        Rect rect = this.f;
        Rect rect2 = new Rect();
        Rect rect3 = new Rect();
        this.d.getHitRect(rect);
        this.a.getGlobalVisibleRect(rect2);
        this.b.getGlobalVisibleRect(rect3);
        if (!this.h && ((rect2.contains(x2, this.J + y2) && !this.j) || rect3.contains(x2, this.J + y2) || !rect.contains(x2, y2))) {
            return false;
        }
        if (action == 0) {
            this.h = true;
            this.L = (float) x2;
            this.M = (float) y2;
            this.c.setPressed(true);
            g();
            if (this.q != null) {
                this.q.a();
            }
            int top = this.d.getTop();
            this.x = y2 - top;
            c(top);
            this.n.addMovement(motionEvent);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        if (!this.h) {
            int i6 = i5 - i3;
            View view = this.d;
            int measuredWidth = view.getMeasuredWidth();
            int measuredHeight = view.getMeasuredHeight();
            View view2 = this.e;
            int i7 = ((i4 - i2) - measuredWidth) / 2;
            int i8 = this.j ? this.l : (i6 - measuredHeight) + this.k;
            view2.layout(0, this.l + measuredHeight, view2.getMeasuredWidth(), this.l + measuredHeight + view2.getMeasuredHeight());
            view.layout(i7, i8, measuredWidth + i7, measuredHeight + i8);
            this.m = view.getHeight();
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        int mode2 = View.MeasureSpec.getMode(i3);
        int size2 = View.MeasureSpec.getSize(i3);
        if (mode == 0 || mode2 == 0) {
            throw new RuntimeException("SlidingDrawer cannot have UNSPECIFIED dimensions");
        }
        View view = this.d;
        measureChild(view, i2, i3);
        this.e.measure(View.MeasureSpec.makeMeasureSpec(size, 1073741824), View.MeasureSpec.makeMeasureSpec((size2 - view.getMeasuredHeight()) - this.l, 1073741824));
        setMeasuredDimension(size, size2);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.i) {
            return true;
        }
        if (this.h) {
            if (this.n == null) {
                this.n = VelocityTracker.obtain();
            }
            this.n.addMovement(motionEvent);
            switch (motionEvent.getAction()) {
                case 1:
                case 3:
                    VelocityTracker velocityTracker = this.n;
                    velocityTracker.computeCurrentVelocity(this.G);
                    float yVelocity = velocityTracker.getYVelocity();
                    float xVelocity = velocityTracker.getXVelocity();
                    boolean z2 = yVelocity < 0.0f;
                    if (xVelocity < 0.0f) {
                        xVelocity = -xVelocity;
                    }
                    if (xVelocity > ((float) this.D)) {
                        xVelocity = (float) this.D;
                    }
                    float hypot = (float) Math.hypot((double) xVelocity, (double) yVelocity);
                    if (z2) {
                        hypot = -hypot;
                    }
                    int top = this.d.getTop();
                    if (Math.abs(hypot) < ((float) this.C)) {
                        if ((this.j && top < this.B + this.l) || (!this.j && top > (((this.k + getBottom()) - getTop()) - this.m) - this.B)) {
                            if (!this.z) {
                                a(top, hypot, false);
                                break;
                            } else {
                                playSoundEffect(0);
                                if (!this.j) {
                                    b(top);
                                    break;
                                } else {
                                    a(top);
                                    break;
                                }
                            }
                        } else {
                            a(top, hypot, false);
                            break;
                        }
                    } else {
                        a(top, hypot, false);
                        break;
                    }
                case 2:
                    d(((int) motionEvent.getY()) - this.x);
                    break;
            }
        }
        return this.h || this.y || super.onTouchEvent(motionEvent);
    }
}
