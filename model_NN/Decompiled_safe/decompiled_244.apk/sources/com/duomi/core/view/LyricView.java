package com.duomi.core.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;
import java.util.ArrayList;

public class LyricView extends View {
    private static int k = 150;
    private static int l = 54;
    private static int q = Color.rgb(136, 136, 136);
    private static int r = Color.rgb(153, 234, 142);
    private static int s = Color.rgb(241, 240, 193);
    int a = 0;
    private float b;
    private float c = 0.0f;
    private float d = 0.0f;
    private int e = 0;
    private ArrayList f;
    private ArrayList g;
    private Paint h;
    private int i = -1;
    private int j;
    private int m = k;
    private int n = 1;
    private int o;
    private int p;
    private boolean t = false;
    private boolean u = false;
    private String v = null;
    private boolean w;
    private as x;

    public LyricView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.x = as.a(context);
    }

    private void a(String str, int i2, Canvas canvas) {
        String trim = str.trim();
        canvas.drawText(trim, (float) ((this.o - ((int) this.h.measureText(trim))) / 2), (float) i2, this.h);
    }

    public float a() {
        return this.c;
    }

    public void a(float f2) {
        this.d = f2;
    }

    public void a(float f2, float f3) {
        this.h = new Paint();
        this.h.setAntiAlias(true);
        this.h.setTextSize(f2);
        this.h.setColor(q);
        if (this.n == 1) {
            this.j = (int) (f2 + f3);
        } else {
            this.j = (int) (f2 + f3);
        }
        this.h.setTypeface(Typeface.create(Typeface.SANS_SERIF, 3));
    }

    public void a(int i2) {
        this.e = i2;
    }

    public void a(ArrayList arrayList) {
        this.g = arrayList;
    }

    public void b(float f2) {
        this.c = f2;
    }

    public void b(int i2) {
        this.o = i2;
        switch (i2) {
            case 240:
                l = 36;
                k = 120;
                return;
            case 320:
                l = 48;
                k = 170;
                return;
            case 480:
                l = 68;
                k = 330;
                return;
            default:
                l = 68;
                k = 150;
                return;
        }
    }

    public void b(ArrayList arrayList) {
        this.f = arrayList;
    }

    public void c(int i2) {
        this.p = i2;
    }

    public boolean c(float f2) {
        int i2;
        int i3;
        int i4;
        int i5;
        this.b = this.d + f2 + this.c;
        float f3 = this.b;
        if (f3 <= 0.0f) {
            this.i = 0;
        } else if (this.g != null && this.i < this.g.size()) {
            if (this.i < 0) {
                this.i = 0;
            } else {
                if (((float) ((Integer) this.g.get(this.i)).intValue()) > f3) {
                    int i6 = this.i - 1;
                    i4 = i6 / 2;
                    i3 = i6;
                    i2 = 0;
                } else {
                    int i7 = this.i;
                    int size = this.g.size() - 1;
                    i2 = i7;
                    int i8 = size;
                    i4 = (i7 + size) / 2;
                    i3 = i8;
                }
                while (i2 <= i3 && i4 != this.g.size() - 1 && (((float) ((Integer) this.g.get(i4)).intValue()) >= f3 || ((float) ((Integer) this.g.get(i4 + 1)).intValue()) < f3)) {
                    if (((float) ((Integer) this.g.get(i4)).intValue()) > f3) {
                        int i9 = i4 - 1;
                        i5 = (i2 + i9) / 2;
                        i3 = i9;
                    } else {
                        int i10 = i4 + 1;
                        i5 = (i10 + i3) / 2;
                        i2 = i10;
                    }
                }
                if (this.i != i4 || i4 == 0) {
                    if (this.i != i4) {
                    }
                    this.i = i4;
                }
            }
        }
        if (this.n == 1) {
            if (this.i <= 0) {
                if (f3 < ((float) ((Integer) this.g.get(0)).intValue())) {
                    this.m = k;
                } else if (this.g == null || this.g.size() <= 1) {
                    this.m = k;
                } else {
                    this.m = k - ((int) ((((float) this.j) * (f3 - ((float) ((Integer) this.g.get(0)).intValue()))) / ((float) (((Integer) this.g.get(1)).intValue() - ((Integer) this.g.get(0)).intValue()))));
                }
            } else if (this.i < this.g.size() - 1) {
                this.m = k - ((int) (((f3 - ((float) ((Integer) this.g.get(this.i)).intValue())) / ((float) (((Integer) this.g.get(this.i + 1)).intValue() - ((Integer) this.g.get(this.i)).intValue()))) * ((float) this.j)));
            } else if (this.e <= 0 || this.i >= this.g.size()) {
                this.m = k;
            } else {
                this.m = k - ((int) (((f3 - ((float) ((Integer) this.g.get(this.i)).intValue())) / ((float) (this.e - ((Integer) this.g.get(this.i)).intValue()))) * ((float) this.j)));
            }
            return true;
        }
        if (this.i <= 0) {
            if (f3 < ((float) ((Integer) this.g.get(0)).intValue())) {
                this.m = l;
            } else if (this.g == null || this.g.size() <= 1) {
                this.m = l;
            } else {
                this.m = l - ((int) ((((float) this.j) * (f3 - ((float) ((Integer) this.g.get(0)).intValue()))) / ((float) (((Integer) this.g.get(1)).intValue() - ((Integer) this.g.get(0)).intValue()))));
            }
        } else if (this.i < this.g.size() - 1) {
            this.m = l - ((int) (((f3 - ((float) ((Integer) this.g.get(this.i)).intValue())) / ((float) (((Integer) this.g.get(this.i + 1)).intValue() - ((Integer) this.g.get(this.i)).intValue()))) * ((float) this.j)));
        } else if (this.e <= 0 || this.i >= this.g.size()) {
            this.m = l;
        } else {
            this.m = l - ((int) (((f3 - ((float) ((Integer) this.g.get(this.i)).intValue())) / ((float) (this.e - ((Integer) this.g.get(this.i)).intValue()))) * ((float) this.j)));
        }
        return true;
    }

    public void d(int i2) {
        this.n = i2;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        this.w = this.x.L();
        try {
            switch (this.n) {
                case 1:
                    if (this.i <= 0) {
                        int i2 = this.m;
                        if (this.f != null) {
                            String str = (String) this.f.get(0);
                            int measureText = (int) this.h.measureText(str);
                            int i3 = (this.o - measureText) / 2;
                            if (this.b >= ((float) ((Integer) this.g.get(0)).intValue())) {
                                if (this.w) {
                                    this.h.setColor(s);
                                } else {
                                    this.h.setColor(r);
                                }
                                a(str, i2, canvas);
                                if (this.w) {
                                    this.h.setColor(r);
                                    canvas.clipRect((float) i3, ((float) i2) - this.h.getTextSize(), (float) (((int) (((this.b - ((float) ((Integer) this.g.get(0)).intValue())) * ((float) measureText)) / ((float) (0 < this.g.size() - 1 ? ((Integer) this.g.get(0 + 1)).intValue() - ((Integer) this.g.get(0)).intValue() : this.e > 0 ? this.e - ((Integer) this.g.get(0)).intValue() : (int) (this.b - ((float) ((Integer) this.g.get(0)).intValue())))))) + i3), (float) (this.j + i2));
                                    a(str, i2, canvas);
                                    canvas.restore();
                                    canvas.save(2);
                                    canvas.clipRect(0, 0, this.o, this.p);
                                }
                            } else {
                                this.h.setColor(q);
                                a(str, i2, canvas);
                            }
                            this.h.setColor(q);
                            int i4 = 1;
                            int i5 = this.j + i2;
                            while (i4 < this.f.size() && i5 < this.p + this.j) {
                                String str2 = (String) this.f.get(i4);
                                int measureText2 = (this.o - ((int) this.h.measureText(str2))) / 2;
                                a(str2, i5, canvas);
                                i4++;
                                i5 = this.j + i5;
                            }
                            return;
                        }
                        return;
                    } else if (this.i < this.g.size() && this.i < this.f.size()) {
                        int i6 = this.m;
                        int i7 = this.i;
                        if (this.w) {
                            this.h.setColor(s);
                        } else {
                            this.h.setColor(r);
                        }
                        String str3 = (String) this.f.get(i7);
                        int measureText3 = (int) this.h.measureText(str3);
                        int i8 = (this.o - measureText3) / 2;
                        a(str3, i6, canvas);
                        if (this.w) {
                            this.h.setColor(r);
                            canvas.clipRect((float) i8, ((float) i6) - this.h.getTextSize(), (float) (((int) (((this.b - ((float) ((Integer) this.g.get(i7)).intValue())) * ((float) measureText3)) / ((float) (i7 < this.g.size() - 1 ? ((Integer) this.g.get(i7 + 1)).intValue() - ((Integer) this.g.get(i7)).intValue() : this.e > 0 ? this.e - ((Integer) this.g.get(i7)).intValue() : (int) (this.b - ((float) ((Integer) this.g.get(i7)).intValue())))))) + i8), (float) (this.j + i6));
                            a(str3, i6, canvas);
                            canvas.restore();
                            canvas.save(2);
                            canvas.clipRect(0, 0, this.o, this.p);
                        }
                        int i9 = i6 - this.j;
                        int i10 = i7 - 1;
                        this.h.setColor(q);
                        int i11 = i9;
                        while (i10 >= 0 && i11 > 0) {
                            String str4 = (String) this.f.get(i10);
                            int measureText4 = (this.o - ((int) this.h.measureText(str4))) / 2;
                            a(str4, i11, canvas);
                            i10--;
                            i11 -= this.j;
                        }
                        int i12 = this.m + this.j;
                        int i13 = this.i;
                        while (true) {
                            i13++;
                            int i14 = i12;
                            if (i13 < this.f.size() && i14 < this.p + this.j) {
                                String str5 = (String) this.f.get(i13);
                                int measureText5 = (this.o - ((int) this.h.measureText(str5))) / 2;
                                a(str5, i14, canvas);
                                i12 = this.j + i14;
                            } else {
                                return;
                            }
                        }
                    } else {
                        return;
                    }
                    break;
                case 2:
                    if (this.i <= 0) {
                        int i15 = this.m;
                        if (this.f != null) {
                            int measureText6 = (int) this.h.measureText((String) this.f.get(0));
                            int i16 = (this.o - measureText6) / 2;
                            if (this.b >= ((float) ((Integer) this.g.get(0)).intValue())) {
                                if (this.w) {
                                    this.h.setColor(s);
                                } else {
                                    this.h.setColor(r);
                                }
                                canvas.drawText((String) this.f.get(0), (float) i16, (float) i15, this.h);
                                if (this.w) {
                                    this.h.setColor(r);
                                    canvas.clipRect((float) i16, ((float) i15) - this.h.getTextSize(), (float) (((int) (((this.b - ((float) ((Integer) this.g.get(0)).intValue())) * ((float) measureText6)) / ((float) (0 < this.g.size() - 1 ? ((Integer) this.g.get(0 + 1)).intValue() - ((Integer) this.g.get(0)).intValue() : this.e > 0 ? this.e - ((Integer) this.g.get(0)).intValue() : (int) (this.b - ((float) ((Integer) this.g.get(0)).intValue())))))) + i16), (float) (this.j + i15));
                                    canvas.drawText((String) this.f.get(0), (float) i16, (float) i15, this.h);
                                    canvas.restore();
                                    canvas.save(2);
                                    canvas.clipRect(0, 0, this.o, this.p);
                                }
                            } else {
                                this.h.setColor(q);
                                canvas.drawText((String) this.f.get(0), (float) i16, (float) i15, this.h);
                            }
                            this.h.setColor(q);
                            int i17 = this.j + i15;
                            int i18 = 1;
                            int i19 = i17;
                            while (i18 < this.f.size() && i19 < this.p + this.j) {
                                canvas.drawText((String) this.f.get(i18), (float) ((this.o - ((int) this.h.measureText((String) this.f.get(i18)))) / 2), (float) i19, this.h);
                                i18++;
                                i19 = this.j + i19;
                            }
                            return;
                        }
                        return;
                    } else if (this.i < this.g.size() && this.i < this.f.size()) {
                        int i20 = this.m;
                        int i21 = this.i;
                        if (this.w) {
                            this.h.setColor(s);
                        } else {
                            this.h.setColor(r);
                        }
                        int measureText7 = (int) this.h.measureText((String) this.f.get(i21));
                        int i22 = (this.o - measureText7) / 2;
                        canvas.drawText((String) this.f.get(i21), (float) i22, (float) i20, this.h);
                        if (this.w) {
                            this.h.setColor(r);
                            canvas.clipRect((float) i22, ((float) i20) - this.h.getTextSize(), (float) (((int) (((this.b - ((float) ((Integer) this.g.get(i21)).intValue())) * ((float) measureText7)) / ((float) (i21 < this.g.size() - 1 ? ((Integer) this.g.get(i21 + 1)).intValue() - ((Integer) this.g.get(i21)).intValue() : this.e > 0 ? this.e - ((Integer) this.g.get(i21)).intValue() : (int) (this.b - ((float) ((Integer) this.g.get(i21)).intValue())))))) + i22), (float) (this.j + i20));
                            canvas.drawText((String) this.f.get(i21), (float) i22, (float) i20, this.h);
                            canvas.restore();
                            canvas.save(2);
                            canvas.clipRect(0, 0, this.o, this.p);
                        }
                        int i23 = i20 - this.j;
                        int i24 = i21 - 1;
                        this.h.setColor(q);
                        int i25 = i23;
                        while (i24 >= 0 && i25 > 0) {
                            canvas.drawText((String) this.f.get(i24), (float) ((this.o - ((int) this.h.measureText((String) this.f.get(i24)))) / 2), (float) i25, this.h);
                            i24--;
                            i25 -= this.j;
                        }
                        int i26 = this.m + this.j;
                        int i27 = this.i + 1;
                        int i28 = i26;
                        while (i27 < this.f.size() && i28 < this.p + this.j) {
                            canvas.drawText((String) this.f.get(i27), (float) ((this.o - ((int) this.h.measureText((String) this.f.get(i27)))) / 2), (float) i28, this.h);
                            i27++;
                            i28 = this.j + i28;
                        }
                        return;
                    } else {
                        return;
                    }
                default:
                    return;
            }
        } catch (Exception e2) {
            ad.b("LyricView", e2.toString());
        }
        ad.b("LyricView", e2.toString());
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        if (this.n == 2) {
            switch (en.b(getContext())) {
                case 240:
                    setMeasuredDimension(i2, 45);
                    return;
                case 320:
                    setMeasuredDimension(i2, 56);
                    return;
                case 480:
                    setMeasuredDimension(i2, 86);
                    return;
                default:
                    setMeasuredDimension(i2, 86);
                    return;
            }
        } else {
            setMeasuredDimension(i2, i3);
        }
    }
}
