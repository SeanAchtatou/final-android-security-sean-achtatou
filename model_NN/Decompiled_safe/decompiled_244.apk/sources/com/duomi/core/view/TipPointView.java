package com.duomi.core.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import com.duomi.android.R;

public class TipPointView extends View {
    private int a;
    private int b;
    private int c;
    private int d;
    private Bitmap e;
    private Bitmap f;
    private Context g;

    public TipPointView(Context context) {
        this(context, null);
    }

    public TipPointView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.a = 0;
        this.b = 320;
        this.c = 24;
        this.d = 0;
        this.g = context;
        this.b = en.b(context);
        this.e = BitmapFactory.decodeResource(context.getResources(), R.drawable.point_normal);
        this.f = BitmapFactory.decodeResource(context.getResources(), R.drawable.point_f);
        switch (this.b) {
            case 240:
                this.c = 16;
                this.d = 182;
                return;
            case 320:
                this.c = 24;
                this.d = 238;
                return;
            case 480:
                this.c = 32;
                this.d = 357;
                return;
            case 640:
                this.c = 48;
                this.d = 450;
                return;
            default:
                this.c = 32;
                this.d = (int) (((double) ((float) this.b)) / 1.34d);
                return;
        }
    }

    public void a(int i) {
        this.a = i;
        invalidate();
    }

    public void onDraw(Canvas canvas) {
        int i = this.d;
        for (int i2 = 0; i2 < 3; i2++) {
            if (this.a == i2) {
                canvas.drawBitmap(this.f, (float) i, 0.0f, (Paint) null);
            } else {
                canvas.drawBitmap(this.e, (float) i, 0.0f, (Paint) null);
            }
            i = this.b > 480 ? i + 55 : this.b > 240 ? i + 24 : i + 16;
        }
    }
}
