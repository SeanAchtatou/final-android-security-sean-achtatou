package com.duomi.core.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;

public class DMAlertController {

    public class RecycleListView extends ListView {
        boolean a = true;

        public RecycleListView(Context context) {
            super(context);
        }

        public RecycleListView(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        public RecycleListView(Context context, AttributeSet attributeSet, int i) {
            super(context, attributeSet, i);
        }
    }
}
