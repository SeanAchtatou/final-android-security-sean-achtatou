package com.duomi.app.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.duomi.android.R;
import com.duomi.core.view.DMAlertController;
import java.util.ArrayList;

public class PlayListMusicEditView extends c implements View.OnClickListener {
    /* access modifiers changed from: private */
    public static int[] I;
    private static boolean K = false;
    private Button A;
    private Button B;
    private Button C;
    private Button D;
    private ImageView E;
    /* access modifiers changed from: private */
    public Context F;
    /* access modifiers changed from: private */
    public MyAdapter G;
    /* access modifiers changed from: private */
    public ListQueryHandler H;
    /* access modifiers changed from: private */
    public int J;
    private Message L;
    /* access modifiers changed from: private */
    public ar M;
    /* access modifiers changed from: private */
    public AlertDialog N;
    /* access modifiers changed from: private */
    public ProgressDialog O;
    /* access modifiers changed from: private */
    public int P = -1;
    /* access modifiers changed from: private */
    public Handler Q = new Handler() {
        public void handleMessage(Message message) {
            switch (message.what) {
                case 255:
                    bl d = PlayListMusicEditView.this.M.d(message.getData().getInt("listId"));
                    new AddTask().execute(d);
                    return;
                default:
                    return;
            }
        }
    };
    private AdapterView.OnItemClickListener R = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView adapterView, View view, int i, long j) {
            if (i != 3) {
                boolean unused = PlayListMusicEditView.this.a(i, PlayListMusicEditView.this.a);
                if (PlayListMusicEditView.this.a) {
                    switch (i) {
                        case 0:
                            fd.a().a(PlayListMusicEditView.this.F, (Handler) null);
                            return;
                        default:
                            return;
                    }
                } else {
                    switch (i) {
                        case 1:
                            fd.a().a(PlayListMusicEditView.this.F, (Handler) null);
                            return;
                        default:
                            return;
                    }
                }
            } else if (PlayListMusicEditView.this.g.getVisibility() == 0) {
                PlayListMusicEditView.this.j();
            } else {
                PlayListMusicEditView.this.i();
            }
        }
    };
    private AdapterView.OnItemClickListener S = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView adapterView, View view, int i, long j) {
            boolean unused = PlayListMusicEditView.this.b(i);
        }
    };
    private View T;
    public ContentValues[] x;
    private ListView y;
    private TextView z;

    class AddTask extends AsyncTask {
        private bl b;
        private boolean c;

        private AddTask() {
            this.b = null;
            this.c = false;
        }

        /* access modifiers changed from: protected */
        public Object doInBackground(Object... objArr) {
            this.b = (bl) objArr[0];
            PlayListMusicEditView.this.a(this.b);
            ar a2 = ar.a(PlayListMusicEditView.this.getContext());
            bl b2 = a2.b(bn.a().h(), 2);
            if (b2 == null || this.b == null || b2.g() != this.b.g() || gx.d == null) {
                return null;
            }
            try {
                bj k = gx.d.k();
                if (k == null || a2.a(b2.g(), k.f()) < 0) {
                    return null;
                }
                this.c = true;
                return null;
            } catch (RemoteException e) {
                e.printStackTrace();
                return null;
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Object obj) {
            PlayListMusicEditView.this.O.dismiss();
            jh.a(PlayListMusicEditView.this.F, PlayListMusicEditView.this.F.getString(R.string.playlist_song_add_sucess).replaceAll("%s", "\"" + this.b.h() + "\""));
            if (this.c) {
                PlayerLayoutView.H.a(true);
            }
            super.onPostExecute(obj);
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            PlayListMusicEditView.this.O.show();
            super.onPreExecute();
        }
    }

    class DeleteTask extends AsyncTask {
        private DeleteTask() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public Object doInBackground(Integer... numArr) {
            PlayListMusicEditView.this.M.b(numArr[0].intValue(), numArr[1].intValue(), PlayListMusicEditView.this.x);
            return null;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.duomi.app.ui.PlayListMusicEditView.a(com.duomi.app.ui.PlayListMusicEditView, android.content.AsyncQueryHandler):void
         arg types: [com.duomi.app.ui.PlayListMusicEditView, com.duomi.app.ui.PlayListMusicEditView$ListQueryHandler]
         candidates:
          com.duomi.app.ui.PlayListMusicEditView.a(com.duomi.app.ui.PlayListMusicEditView, int):int
          com.duomi.app.ui.PlayListMusicEditView.a(com.duomi.app.ui.PlayListMusicEditView, com.duomi.app.ui.PlayListMusicEditView$ListQueryHandler):com.duomi.app.ui.PlayListMusicEditView$ListQueryHandler
          com.duomi.app.ui.PlayListMusicEditView.a(com.duomi.app.ui.PlayListMusicEditView, android.database.Cursor):void
          com.duomi.app.ui.PlayListMusicEditView.a(com.duomi.app.ui.PlayListMusicEditView, bl):void
          com.duomi.app.ui.PlayListMusicEditView.a(int, android.view.KeyEvent):boolean
          c.a(java.lang.Class, android.os.Message):void
          c.a(boolean, android.os.Message):void
          c.a(int, android.view.KeyEvent):boolean
          c.a(int, boolean):boolean
          com.duomi.app.ui.PlayListMusicEditView.a(com.duomi.app.ui.PlayListMusicEditView, android.content.AsyncQueryHandler):void */
        /* access modifiers changed from: protected */
        public void onPostExecute(Object obj) {
            ListQueryHandler unused = PlayListMusicEditView.this.H = new ListQueryHandler(PlayListMusicEditView.this.g().getContentResolver());
            PlayListMusicEditView.this.a((AsyncQueryHandler) PlayListMusicEditView.this.H);
            PlayListMusicEditView.this.O.dismiss();
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            PlayListMusicEditView.this.O.show();
            super.onPreExecute();
        }
    }

    class ListQueryHandler extends AsyncQueryHandler {
        public ListQueryHandler(ContentResolver contentResolver) {
            super(contentResolver);
        }

        public void onQueryComplete(int i, Object obj, Cursor cursor) {
            PlayListMusicEditView.this.a(cursor);
            int[] unused = PlayListMusicEditView.I = new int[cursor.getCount()];
            for (int i2 = 0; i2 < cursor.getCount(); i2++) {
                PlayListMusicEditView.I[i2] = 0;
            }
            int length = PlayListMusicEditView.I.length;
            if (PlayListMusicEditView.this.P == 0 && gx.a(PlayListMusicEditView.this.J + "")) {
                try {
                    be j = gx.d.j();
                    if (j == null) {
                        return;
                    }
                    if (length == 0) {
                        gx.d.a((bj) null);
                        gx.d.a(new be("-1", 0, "", 0, 1, j.f(), j.g()), true);
                        return;
                    }
                    bj k = gx.d.k();
                    if (k != null) {
                        j.b(PlayListMusicEditView.this.M.a(PlayListMusicEditView.this.J, k.f()));
                        gx.d.a(new be(j.b(), j.c(), j.d(), length, 1, j.f(), j.g()), true);
                    }
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            } else if (PlayListMusicEditView.this.P > 0) {
                try {
                    be j2 = gx.d.j();
                    if (j2 != null && j2.a() != 0) {
                        if (!gx.a(PlayListMusicEditView.this.J + "")) {
                            int a2 = PlayListMusicEditView.this.M.a(en.d(j2.b()));
                            if (a2 != j2.e()) {
                                length = a2;
                            } else {
                                return;
                            }
                        }
                        if (length == 0) {
                            gx.d.a((bj) null);
                            gx.d.a(new be("-1", 0, "", 0, 1, j2.f(), j2.g()), true);
                            return;
                        }
                        bj k2 = gx.d.k();
                        if (k2 != null) {
                            j2.b(PlayListMusicEditView.this.M.a(en.d(j2.b()), k2.f()));
                        }
                        gx.d.a(new be(j2.b(), j2.c(), j2.d(), length, 1, j2.f(), j2.g()), true);
                    }
                } catch (RemoteException e2) {
                    e2.printStackTrace();
                }
            }
        }
    }

    class MyAdapter extends CursorAdapter {
        ar a;
        private LayoutInflater b;

        class ItemStruct {
            ImageView a;
            TextView b;
            TextView c;
            TextView d;
            ImageView e;

            private ItemStruct() {
            }
        }

        public MyAdapter(Context context, Cursor cursor) {
            super(context, cursor);
            this.b = (LayoutInflater) context.getSystemService("layout_inflater");
            this.a = ar.a(context);
        }

        public bj a(int i) {
            if (getCursor() != null) {
                return ar.a((Cursor) getItem(i));
            }
            return null;
        }

        public void bindView(View view, Context context, Cursor cursor) {
            ItemStruct itemStruct = (ItemStruct) view.getTag();
            bj a2 = ar.a(cursor);
            if (a2 != null) {
                if (PlayListMusicEditView.I[cursor.getPosition()] == 0) {
                    itemStruct.a.setImageResource(R.drawable.box_normal);
                } else {
                    itemStruct.a.setImageResource(R.drawable.box_f);
                }
                itemStruct.c.setText(en.l(new StringBuffer().append(String.valueOf(cursor.getPosition() + 1)).append(".").append(a2.h()).toString()));
                itemStruct.d.setText(en.d(a2.k()));
                if (a2.b() == 1) {
                    itemStruct.e.setVisibility(8);
                } else {
                    itemStruct.e.setVisibility(0);
                }
                itemStruct.b.setText(a2.j());
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
            View inflate = this.b.inflate((int) R.layout.playlist_music_edit_row, viewGroup, false);
            ItemStruct itemStruct = new ItemStruct();
            itemStruct.a = (ImageView) inflate.findViewById(R.id.playlist_local_select);
            itemStruct.c = (TextView) inflate.findViewById(R.id.playlist_local_song_name);
            itemStruct.d = (TextView) inflate.findViewById(R.id.playlist_local_duration);
            itemStruct.b = (TextView) inflate.findViewById(R.id.playlist_local_singer);
            itemStruct.e = (ImageView) inflate.findViewById(R.id.playlist_online_image);
            inflate.setTag(itemStruct);
            return inflate;
        }
    }

    public PlayListMusicEditView(Activity activity) {
        super(activity);
        this.F = activity;
    }

    /* access modifiers changed from: private */
    public void a(AsyncQueryHandler asyncQueryHandler) {
        if (asyncQueryHandler != null) {
            ad.b("PlayListMusicEditView", "querySongList:listid:" + this.J);
            asyncQueryHandler.startQuery(0, null, Uri.parse(ci.b + "/" + this.J), null, null, null, null);
        }
    }

    /* access modifiers changed from: private */
    public void a(Cursor cursor) {
        if (cursor != null) {
            ad.b("PlayListMusicEditView", "freshListViewNow()count:" + cursor.getCount());
            if (this.G == null) {
                ad.b("PlayListMusicEditView", "refresh---------------------1");
                this.G = new MyAdapter(g(), cursor);
                this.y.setAdapter((ListAdapter) this.G);
                return;
            }
            ad.b("PlayListMusicEditView", "refresh---------------------2");
            this.G.changeCursor(cursor);
        }
    }

    /* access modifiers changed from: private */
    public void a(bl blVar) {
        int i = 0;
        this.x = new ContentValues[u()];
        for (int i2 = 0; i2 < I.length; i2++) {
            if (I[i2] == 1) {
                ContentValues contentValues = new ContentValues();
                contentValues.put("position", Integer.valueOf(i2 + 1));
                this.x[i] = contentValues;
                i++;
            }
        }
        ad.b("PlayListMusicEditView", "size>>>>>>>>>>>" + i);
        this.M.a(this.J, blVar.g(), this.x);
        if (gx.a(blVar.g() + "")) {
            try {
                be j = gx.d.j();
                int a = this.M.a(blVar.g());
                if (j != null) {
                    gx.d.a(new be(blVar.g() + "", j.c(), blVar.h(), a, 1, j.f(), j.g()), false);
                }
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    private void r() {
        this.O = new ProgressDialog(this.F);
        this.O.setMessage(this.F.getString(R.string.app_scan_wait_moment));
        if (this.G != null) {
            this.G.changeCursor(null);
        }
        K = false;
        this.M = ar.a(this.F);
        this.E.setImageResource(R.drawable.list_return_icon);
        this.z.setText(((bl) this.L.obj).h());
        if (!fd.i) {
            this.T.setVisibility(0);
        } else {
            this.T.setVisibility(8);
        }
        this.J = ((bl) this.L.obj).g();
        this.E.setVisibility(8);
        this.A.setText((int) R.string.playlist_edit_complete);
        this.B.setText((int) R.string.playlist_edit_selectAll);
        this.C.setText((int) R.string.playlist_edit_deleteSelected);
        this.D.setText((int) R.string.playlist_edit_addTo);
        this.H = new ListQueryHandler(g().getContentResolver());
        a(this.H);
        this.A.setOnClickListener(this);
        this.B.setOnClickListener(this);
        this.C.setOnClickListener(this);
        this.D.setOnClickListener(this);
        this.y.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView adapterView, View view, int i, long j) {
                if (PlayListMusicEditView.I[i] == 0) {
                    PlayListMusicEditView.I[i] = 1;
                } else {
                    PlayListMusicEditView.I[i] = 0;
                }
                PlayListMusicEditView.this.G.notifyDataSetChanged();
            }
        });
    }

    private void s() {
        if (u() > 0) {
            this.N = new AlertDialog.Builder(this.F).create();
            this.N.setTitle(this.F.getString(R.string.playlist_song_bulk_add).replaceAll("%s", u() + ""));
            DMAlertController.RecycleListView recycleListView = (DMAlertController.RecycleListView) ((LayoutInflater) this.F.getSystemService("layout_inflater")).inflate((int) R.layout.select_dialog, (ViewGroup) null);
            ArrayList e = this.M.e(bn.a().h());
            final ArrayList arrayList = new ArrayList();
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < e.size()) {
                    if ((((bl) e.get(i2)).a() == 2 || ((bl) e.get(i2)).a() == 7) && ((bl) e.get(i2)).g() != this.J) {
                        arrayList.add(e.get(i2));
                    }
                    i = i2 + 1;
                } else {
                    arrayList.add(new bl(this.F.getString(R.string.playlist_create), -1, -1, null, null, null, null, -1, -1));
                    final iw iwVar = new iw(this.F, arrayList);
                    recycleListView.setAdapter((ListAdapter) iwVar);
                    this.N.setView(recycleListView);
                    this.N.show();
                    recycleListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                         method: bn.a(android.content.Context, boolean):void
                         arg types: [android.content.Context, int]
                         candidates:
                          bn.a(int, android.content.Context):int
                          bn.a(android.content.Context, boolean):void */
                        public void onItemClick(AdapterView adapterView, View view, final int i, long j) {
                            if (i >= iwVar.getCount() - 1) {
                                PopDialogView.a(PlayListMusicEditView.this.g(), PlayListMusicEditView.this.Q);
                            } else if (bn.a().b(PlayListMusicEditView.this.getContext()) || !kf.b || !kf.a(PlayListMusicEditView.this.F, 1) || as.a(PlayListMusicEditView.this.F).U()) {
                                new AddTask().execute(arrayList.get(i));
                            } else {
                                AlertDialog create = new AlertDialog.Builder(PlayListMusicEditView.this.getContext()).setPositiveButton(PlayListMusicEditView.this.getContext().getString(R.string.app_confirm), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        as.a(PlayListMusicEditView.this.getContext()).u(true);
                                    }
                                }).setNegativeButton(PlayListMusicEditView.this.getContext().getString(R.string.app_cancel), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                }).create();
                                create.setTitle(PlayListMusicEditView.this.getContext().getString(R.string.first_collection_title));
                                create.setMessage(PlayListMusicEditView.this.getContext().getString(R.string.first_collection_message));
                                create.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                    public void onDismiss(DialogInterface dialogInterface) {
                                        new AddTask().execute(arrayList.get(i));
                                    }
                                });
                                create.show();
                                bn.a().a(PlayListMusicEditView.this.getContext(), true);
                            }
                            PlayListMusicEditView.this.N.dismiss();
                        }
                    });
                    return;
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void t() {
        final int i;
        final int u = u();
        if (u > 0) {
            View inflate = LayoutInflater.from(this.F).inflate((int) R.layout.dialog_tips, (ViewGroup) null, false);
            TextView textView = (TextView) inflate.findViewById(R.id.tip1);
            final CheckBox checkBox = (CheckBox) inflate.findViewById(R.id.tiptext1);
            checkBox.setText((int) R.string.delete_check_title);
            checkBox.setChecked(false);
            if (u > 1) {
                textView.setText(this.F.getString(R.string.delete_number).replaceAll("%s", "" + u));
                i = 0;
            } else if (u == 1) {
                int i2 = 0;
                while (i2 < I.length && I[i2] != 1) {
                    i2++;
                }
                if (this.G.a(i2).b() == 0) {
                    checkBox.setClickable(false);
                } else {
                    checkBox.setClickable(true);
                }
                textView.setText(this.G.a(i2).h());
                i = i2;
            } else {
                i = 0;
            }
            new AlertDialog.Builder(this.F).setTitle((int) R.string.delete_title).setView(inflate).setPositiveButton((int) R.string.button_ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    PlayListMusicEditView.this.x = new ContentValues[u];
                    int i2 = 0;
                    for (int i3 = 0; i3 < PlayListMusicEditView.I.length; i3++) {
                        if (PlayListMusicEditView.I[i3] == 1) {
                            ContentValues contentValues = new ContentValues();
                            contentValues.put("position", Integer.valueOf(i3 + 1));
                            PlayListMusicEditView.this.x[i2] = contentValues;
                            i2++;
                        }
                    }
                    if (checkBox.isChecked()) {
                        int unused = PlayListMusicEditView.this.P = 1;
                        new DeleteTask().execute(Integer.valueOf(PlayListMusicEditView.this.J), 1);
                        if (u > 1) {
                            jh.a(PlayListMusicEditView.this.F, PlayListMusicEditView.this.F.getString(R.string.playlist_bulk_delete).replaceAll("%s", "" + u));
                        } else if (u == 1) {
                            jh.a(PlayListMusicEditView.this.F, PlayListMusicEditView.this.F.getString(R.string.playlist_song_delete).replaceAll("%s", PlayListMusicEditView.this.G.a(i).h()));
                        }
                    } else {
                        int unused2 = PlayListMusicEditView.this.P = 0;
                        new DeleteTask().execute(Integer.valueOf(PlayListMusicEditView.this.J), 0);
                        if (u > 1) {
                            jh.a(PlayListMusicEditView.this.F, PlayListMusicEditView.this.F.getString(R.string.playlist_bulk_clear).replaceAll("%s", "" + u));
                        } else if (u == 1) {
                            jh.a(PlayListMusicEditView.this.F, PlayListMusicEditView.this.F.getString(R.string.playlist_song_clear).replaceAll("%s", PlayListMusicEditView.this.G.a(i).h()));
                        }
                    }
                }
            }).setNegativeButton((int) R.string.button_cancel, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                }
            }).show();
        }
    }

    private int u() {
        int i = 0;
        for (int i2 : I) {
            if (i2 == 1) {
                i++;
            }
        }
        return i;
    }

    public void a(Message message) {
        this.L = message;
        this.P = -1;
        r();
        super.a(message);
    }

    public boolean a(int i, KeyEvent keyEvent) {
        b(i, keyEvent);
        if (i == 4 && this.f.getVisibility() == 0 && !this.u) {
            if (this.g.getVisibility() == 0) {
                this.g.setVisibility(8);
                this.i.requestFocus();
            } else {
                this.f.setVisibility(8);
            }
            return true;
        } else if (i != 4 || this.f.getVisibility() != 8 || this.u) {
            return false;
        } else {
            this.d.a(PlayListMusicView.class, this.L);
            return true;
        }
    }

    /* access modifiers changed from: protected */
    public void f() {
        Window window = g().getWindow();
        if (window != null) {
            window.clearFlags(1024);
        }
        inflate(g(), R.layout.playlist_music_edit, this);
        this.z = (TextView) findViewById(R.id.list_title);
        this.E = (ImageView) findViewById(R.id.go_back);
        this.A = (Button) findViewById(R.id.command);
        this.y = (ListView) findViewById(R.id.music_list);
        if (en.b() == 4) {
            this.y.setFastScrollEnabled(false);
        }
        this.B = (Button) findViewById(R.id.select);
        this.C = (Button) findViewById(R.id.delete);
        this.D = (Button) findViewById(R.id.add);
        this.T = findViewById(R.id.scanning_tip);
        h();
    }

    public void n() {
        this.i.setOnItemClickListener(this.R);
        this.j.setOnItemClickListener(this.S);
    }

    public void o() {
        if (this.a) {
            this.q = getResources().getStringArray(R.array.PlayListMusic_menu_without_download);
            this.s = new ArrayList();
            this.s.add(Integer.valueOf((int) R.drawable.meun_scan));
            this.s.add(Integer.valueOf((int) R.drawable.menu_bg));
        } else {
            this.q = getResources().getStringArray(R.array.PlayListMusic_menu);
            this.s = new ArrayList();
            this.s.add(Integer.valueOf((int) R.drawable.meun_downloadsetup));
            this.s.add(Integer.valueOf((int) R.drawable.meun_scan));
        }
        this.s.add(Integer.valueOf((int) R.drawable.menu_bg));
        this.s.add(Integer.valueOf((int) R.drawable.meun_more));
        this.s.add(Integer.valueOf((int) R.drawable.meun_setup));
        this.s.add(Integer.valueOf((int) R.drawable.meun_sleep));
        this.s.add(Integer.valueOf((int) R.drawable.meun_syc));
        this.s.add(Integer.valueOf((int) R.drawable.meun_exit));
        this.r = getResources().getStringArray(R.array.second_menu2);
        this.t = new ArrayList();
        this.t.add(Integer.valueOf((int) R.drawable.meun_help));
        this.t.add(Integer.valueOf((int) R.drawable.meun_advice));
        this.t.add(Integer.valueOf((int) R.drawable.meun_friends));
        this.t.add(Integer.valueOf((int) R.drawable.meun_about));
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.command:
                this.d.a(PlayListMusicView.class, this.L);
                return;
            case R.id.select:
                if (!K) {
                    this.B.setText((int) R.string.playlist_edit_selectNone);
                    for (int i = 0; i < I.length; i++) {
                        I[i] = 1;
                    }
                    K = true;
                    this.G.notifyDataSetChanged();
                    return;
                }
                this.B.setText((int) R.string.playlist_edit_selectAll);
                for (int i2 = 0; i2 < I.length; i2++) {
                    I[i2] = 0;
                }
                K = false;
                this.G.notifyDataSetChanged();
                return;
            case R.id.delete:
                t();
                return;
            case R.id.add:
                s();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void p() {
        super.p();
    }
}
