package com.duomi.app.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import com.duomi.android.R;

public class SinaReLoginView extends c {
    /* access modifiers changed from: private */
    public EditText A;
    private Button B;
    private Button C;
    private LinearLayout D;
    public ProgressDialog x;
    Handler y = new Handler() {
        public void handleMessage(Message message) {
            switch (message.what) {
                case 0:
                    jh.a(SinaReLoginView.this.g(), (String) message.obj);
                    break;
                case 1:
                    switch (message.arg1) {
                        case -3:
                            if (SinaReLoginView.this.x != null) {
                                SinaReLoginView.this.x.dismiss();
                            }
                            jh.a(SinaReLoginView.this.getContext(), (int) R.string.sina_username_pwd_error_prompt);
                            break;
                        case 0:
                            if (SinaReLoginView.this.x != null) {
                                SinaReLoginView.this.x.dismiss();
                            }
                            as.a(SinaReLoginView.this.g());
                            p a2 = p.a(SinaReLoginView.this.g());
                            a2.e();
                            a2.a(MultiView.class.getName());
                            break;
                    }
            }
            super.handleMessage(message);
        }
    };
    /* access modifiers changed from: private */
    public EditText z;

    class SinaLoginTask extends AsyncTask {
        private SinaLoginTask() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kf.a(android.content.Context, java.lang.String, java.lang.String, boolean):int
         arg types: [android.app.Activity, java.lang.String, java.lang.String, int]
         candidates:
          kf.a(android.content.Context, java.lang.String, java.lang.String, java.lang.String):int
          kf.a(android.content.Context, java.lang.String, java.lang.String, boolean):int */
        /* access modifiers changed from: protected */
        /* renamed from: a */
        public Integer doInBackground(Object... objArr) {
            String obj = objArr[0].toString();
            String obj2 = objArr[1].toString();
            Log.w("testcase", "weibo account >> " + obj + "," + obj2);
            return Integer.valueOf(kf.a((Context) SinaReLoginView.this.g(), obj, obj2, false));
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onPostExecute(Integer num) {
            Message obtainMessage = SinaReLoginView.this.y.obtainMessage(1);
            obtainMessage.arg1 = num.intValue();
            SinaReLoginView.this.y.sendMessage(obtainMessage);
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            try {
                if (!il.b(SinaReLoginView.this.getContext())) {
                    SinaReLoginView.this.y.obtainMessage(0, SinaReLoginView.this.getContext().getResources().getString(R.string.app_net_error)).sendToTarget();
                    return;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            SinaReLoginView.this.x = new ProgressDialog(SinaReLoginView.this.getContext());
            SinaReLoginView.this.x.setTitle((int) R.string.lg_title);
            SinaReLoginView.this.x.setMessage("" + SinaReLoginView.this.getContext().getString(R.string.lg_tips) + "    ");
            SinaReLoginView.this.x.show();
        }

        /* access modifiers changed from: protected */
        public void onProgressUpdate(Object... objArr) {
        }
    }

    public SinaReLoginView(Activity activity) {
        super(activity);
    }

    /* access modifiers changed from: protected */
    public void f() {
        inflate(g(), R.layout.sina_login, this);
        this.z = (EditText) findViewById(R.id.sina_login_user_name);
        this.A = (EditText) findViewById(R.id.sina_login_user_passwd);
        this.B = (Button) findViewById(R.id.sina_login_btn);
        this.C = (Button) findViewById(R.id.sina_cancle_btn);
        this.D = (LinearLayout) findViewById(R.id.sina_login_name_layout);
        q();
    }

    public void q() {
        this.D.setBackgroundResource(R.drawable.sina_login_edit_layout_background);
        this.B.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (en.c(SinaReLoginView.this.z.getText().toString()) || en.c(SinaReLoginView.this.A.getText().toString())) {
                    jh.a(SinaReLoginView.this.getContext(), "密码或用户名不能为空");
                    return;
                }
                new SinaLoginTask().execute(SinaReLoginView.this.z.getText(), SinaReLoginView.this.A.getText());
            }
        });
        this.C.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                p.a(SinaReLoginView.this.g()).e();
                p.a(SinaReLoginView.this.g()).b(LoginRegView.class.getName());
            }
        });
    }
}
