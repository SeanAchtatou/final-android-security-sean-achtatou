package com.duomi.app.ui;

import android.app.Activity;
import android.os.Message;
import android.view.MotionEvent;
import android.view.View;
import java.util.Stack;

public class PlayListGroupView extends c implements jt {
    PlayListOnLineSecondView A;
    PlayListOnLineMusicView B;
    PlayListMainView C;
    PlayListMusicView D;
    PlayListMusicEditView E;
    CategoryListView F;
    DialogView G;
    private boolean H;
    private b I;
    public Stack x;
    public Stack y;
    PlayListOnLineView z;

    public class OnLineInfo {
        cw a;
        int b;

        public OnLineInfo(cw cwVar, int i) {
            this.a = cwVar;
            this.b = i;
        }

        public cw a() {
            return this.a;
        }

        public int b() {
            return this.b;
        }
    }

    public PlayListGroupView(Activity activity) {
        super(activity);
        this.H = false;
        this.z = null;
        this.A = null;
        this.B = null;
        this.C = null;
        this.D = null;
        this.E = null;
        this.F = null;
        this.G = null;
        this.y = new Stack();
    }

    public PlayListGroupView(Activity activity, boolean z2) {
        super(activity);
        this.H = false;
        this.z = null;
        this.A = null;
        this.B = null;
        this.C = null;
        this.D = null;
        this.E = null;
        this.F = null;
        this.G = null;
        this.H = true;
        this.x = new Stack();
    }

    private void u() {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            if (childAt.getVisibility() != 8) {
                childAt.setVisibility(8);
                return;
            }
        }
    }

    public void a(OnLineInfo onLineInfo) {
        this.x.push(onLineInfo);
    }

    public void a(Class cls) {
        u();
        String name = cls.getName();
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            String name2 = childAt.getClass().getName();
            ad.b("PlayListGroupView", "showChild:cName:" + name + " tName:" + name2);
            if (name2.equals(name)) {
                ((b) childAt).a();
                ((b) childAt).c();
                this.I = (b) childAt;
                return;
            }
        }
    }

    public void a(Class cls, Message message) {
        u();
        String name = cls.getName();
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            String name2 = childAt.getClass().getName();
            ad.b("PlayListGroupView", "showChild:cName:" + name + " tName:" + name2);
            if (name2.equals(name)) {
                ((b) childAt).a();
                ((b) childAt).a(message);
                ((b) childAt).c();
                this.I = (b) childAt;
                return;
            }
        }
    }

    public boolean a(MotionEvent motionEvent) {
        int childCount = getChildCount();
        int i = 0;
        while (true) {
            if (i >= childCount) {
                break;
            }
            View childAt = getChildAt(i);
            if (childAt.getVisibility() == 8) {
                i++;
            } else if (childAt instanceof jt) {
                return ((jt) childAt).a(motionEvent);
            }
        }
        return false;
    }

    public void d() {
        super.d();
    }

    public void f() {
        if (this.H) {
            this.z = new PlayListOnLineView(g());
            this.z.setVisibility(8);
            this.z.a(this);
            addView(this.z);
            this.A = new PlayListOnLineSecondView(g());
            this.A.setVisibility(8);
            this.A.a(this);
            addView(this.A);
            this.B = new PlayListOnLineMusicView(g());
            this.B.setVisibility(8);
            this.B.a(this);
            addView(this.B);
            this.G = new DialogView(g());
            this.G.setVisibility(8);
            this.G.a(this);
            addView(this.G);
            a(PlayListOnLineView.class);
            return;
        }
        this.C = new PlayListMainView(g());
        this.C.setVisibility(8);
        this.C.a(this);
        addView(this.C);
        this.D = new PlayListMusicView(g());
        this.D.setVisibility(8);
        this.D.a(this);
        addView(this.D);
        this.E = new PlayListMusicEditView(g());
        this.E.setVisibility(8);
        this.E.a(this);
        addView(this.E);
        this.F = new CategoryListView(g());
        this.F.setVisibility(8);
        this.F.a(this);
        addView(this.F);
        a(PlayListMainView.class);
    }

    public void q() {
        if (this.H) {
            View childAt = getChildAt(3);
            if (childAt.getVisibility() != 8) {
                childAt.setVisibility(8);
            }
        }
    }

    public void r() {
        this.x.pop();
    }

    public OnLineInfo s() {
        return (OnLineInfo) this.x.peek();
    }

    public boolean t() {
        return this.x.isEmpty();
    }
}
