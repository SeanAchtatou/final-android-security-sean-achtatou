package com.duomi.app.ui;

import android.app.Activity;
import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.duomi.android.R;
import java.util.ArrayList;

public class CategoryListView extends c {
    private Message A;
    private ListView B;
    /* access modifiers changed from: private */
    public MyAdapter C;
    private RelativeLayout D;
    private int E;
    private ListQueryHandler F;
    private AdapterView.OnItemClickListener G = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView adapterView, View view, int i, long j) {
            if (i == 3) {
                if (CategoryListView.this.g.getVisibility() == 0) {
                    CategoryListView.this.j();
                } else {
                    CategoryListView.this.i();
                }
            } else if (CategoryListView.this.a) {
                boolean unused = CategoryListView.this.a(i, CategoryListView.this.a);
                switch (i) {
                    case 0:
                        fd.a().a(CategoryListView.this.getContext(), (Handler) null);
                        return;
                    default:
                        return;
                }
            } else {
                boolean unused2 = CategoryListView.this.a(i);
                switch (i) {
                    case 1:
                        fd.a().a(CategoryListView.this.getContext(), (Handler) null);
                        return;
                    default:
                        return;
                }
            }
        }
    };
    private AdapterView.OnItemClickListener H = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView adapterView, View view, int i, long j) {
            boolean unused = CategoryListView.this.b(i);
        }
    };
    private View I;
    private ImageView x;
    private Button y;
    private TextView z;

    class ListQueryHandler extends AsyncQueryHandler {
        public ListQueryHandler(ContentResolver contentResolver) {
            super(contentResolver);
        }

        public void onQueryComplete(int i, Object obj, Cursor cursor) {
            CategoryListView.this.a(cursor);
        }
    }

    class MyAdapter extends CursorAdapter {
        ar a;
        private LayoutInflater b;

        class ItemStruct {
            TextView a;
            ImageView b;

            ItemStruct() {
            }
        }

        public MyAdapter(Context context, Cursor cursor) {
            super(context, cursor);
            this.b = (LayoutInflater) context.getSystemService("layout_inflater");
            this.a = ar.a(context);
        }

        public bl a(int i) {
            if (getCursor() != null) {
                return this.a.c((Cursor) getItem(i));
            }
            return null;
        }

        public void bindView(View view, Context context, Cursor cursor) {
            ItemStruct itemStruct = (ItemStruct) view.getTag();
            bl c = this.a.c(cursor);
            if (c != null) {
                itemStruct.a.setText(c.h());
                itemStruct.b.setImageResource(R.drawable.list_arrow_icon);
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
            View inflate = this.b.inflate((int) R.layout.categorylist_row, viewGroup, false);
            ItemStruct itemStruct = new ItemStruct();
            itemStruct.a = (TextView) inflate.findViewById(R.id.list_title);
            itemStruct.b = (ImageView) inflate.findViewById(R.id.list_arrow);
            inflate.setTag(itemStruct);
            return inflate;
        }
    }

    public CategoryListView(Activity activity) {
        super(activity);
    }

    private void a(AsyncQueryHandler asyncQueryHandler) {
        if (asyncQueryHandler != null) {
            ad.b("CategoryListView", "querySongList()listid:" + this.E);
            asyncQueryHandler.startQuery(0, null, cf.a, null, "parent=? and exists(select 1 from songreflist where listid=songlist._id)", new String[]{"" + this.E}, null);
        }
    }

    /* access modifiers changed from: private */
    public void a(Cursor cursor) {
        if (cursor == null) {
            return;
        }
        if (this.C == null) {
            this.C = new MyAdapter(g(), cursor);
            this.B.setAdapter((ListAdapter) this.C);
            return;
        }
        this.C.changeCursor(cursor);
    }

    private void q() {
        if (this.C != null) {
            this.C.changeCursor(null);
        }
        this.y.setVisibility(8);
        this.x.setVisibility(0);
        this.x.setImageResource(R.drawable.list_return_icon);
        this.z.setText(((bl) this.A.obj).h());
        if (!fd.i) {
            this.I.setVisibility(0);
        } else {
            this.I.setVisibility(8);
        }
        this.E = ((bl) this.A.obj).g();
        this.F = new ListQueryHandler(g().getContentResolver());
        a(this.F);
        this.x.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                CategoryListView.this.d.a(PlayListMainView.class);
            }
        });
        this.D.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                CategoryListView.this.r();
            }
        });
        this.B.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView adapterView, View view, int i, long j) {
                Message message = new Message();
                message.obj = CategoryListView.this.C.a(i);
                ((PlayListGroupView) CategoryListView.this.d).y.push(1);
                CategoryListView.this.d.a(PlayListMusicView.class, message);
            }
        });
    }

    /* access modifiers changed from: private */
    public void r() {
        try {
            ((PlayListGroupView) this.d).y.pop();
            if (!((PlayListGroupView) this.d).y.isEmpty()) {
                this.d.a(CategoryListView.class, (Message) null);
            } else {
                this.d.a(PlayListMainView.class, (Message) null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void a(Message message) {
        this.A = message;
        if (message != null) {
            q();
        }
        super.a(message);
    }

    public boolean a(int i, KeyEvent keyEvent) {
        b(i, keyEvent);
        if (i == 4 && this.f.getVisibility() == 0 && !this.u) {
            if (this.g.getVisibility() == 0) {
                this.g.setVisibility(8);
                this.i.requestFocus();
            } else {
                this.f.setVisibility(8);
            }
            return true;
        } else if (i != 4 || this.f.getVisibility() != 8 || this.u) {
            return false;
        } else {
            r();
            return true;
        }
    }

    public void d() {
        if (this.C != null) {
            this.C.changeCursor(null);
        }
        super.d();
    }

    public void f() {
        Window window = g().getWindow();
        if (window != null) {
            window.clearFlags(1024);
        }
        inflate(g(), R.layout.categorylist, this);
        this.x = (ImageView) findViewById(R.id.go_back);
        this.y = (Button) findViewById(R.id.command);
        this.z = (TextView) findViewById(R.id.list_title);
        this.B = (ListView) findViewById(R.id.category_list);
        this.D = (RelativeLayout) findViewById(R.id.title_panel);
        this.I = findViewById(R.id.scanning_tip);
        h();
    }

    public void n() {
        this.i.setOnItemClickListener(this.G);
        this.j.setOnItemClickListener(this.H);
    }

    public void o() {
        if (this.a) {
            this.q = getResources().getStringArray(R.array.CategoryList_menu);
            this.s = new ArrayList();
            this.s.add(Integer.valueOf((int) R.drawable.meun_scan));
            this.s.add(Integer.valueOf((int) R.drawable.menu_bg));
        } else {
            this.q = getResources().getStringArray(R.array.CategoryList_menu);
            this.s = new ArrayList();
            this.s.add(Integer.valueOf((int) R.drawable.meun_downloadsetup));
            this.s.add(Integer.valueOf((int) R.drawable.meun_scan));
        }
        this.s.add(Integer.valueOf((int) R.drawable.menu_bg));
        this.s.add(Integer.valueOf((int) R.drawable.meun_more));
        this.s.add(Integer.valueOf((int) R.drawable.meun_setup));
        this.s.add(Integer.valueOf((int) R.drawable.meun_sleep));
        this.s.add(Integer.valueOf((int) R.drawable.meun_syc));
        this.s.add(Integer.valueOf((int) R.drawable.meun_exit));
        this.r = getResources().getStringArray(R.array.second_menu2);
        this.t = new ArrayList();
        this.t.add(Integer.valueOf((int) R.drawable.meun_help));
        this.t.add(Integer.valueOf((int) R.drawable.meun_advice));
        this.t.add(Integer.valueOf((int) R.drawable.meun_friends));
        this.t.add(Integer.valueOf((int) R.drawable.meun_about));
    }
}
