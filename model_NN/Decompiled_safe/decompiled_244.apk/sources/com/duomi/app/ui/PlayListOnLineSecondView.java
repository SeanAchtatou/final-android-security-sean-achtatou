package com.duomi.app.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.duomi.android.R;
import com.duomi.app.ui.PlayListGroupView;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PlayListOnLineSecondView extends c {
    private static int G = 0;
    /* access modifiers changed from: private */
    public static int H = 0;
    /* access modifiers changed from: private */
    public MyAdapter A;
    /* access modifiers changed from: private */
    public ListView B;
    private Message C;
    private RelativeLayout D;
    /* access modifiers changed from: private */
    public Context E;
    /* access modifiers changed from: private */
    public List F;
    /* access modifiers changed from: private */
    public hx I;
    /* access modifiers changed from: private */
    public Handler J = new Handler() {
        public void handleMessage(Message message) {
            Class<PlayListOnLineSecondView> cls = PlayListOnLineSecondView.class;
            switch (message.what) {
                case 0:
                    try {
                        if (!(PlayListOnLineSecondView.this.F.get(PlayListOnLineSecondView.H) instanceof cr)) {
                            Message message2 = new Message();
                            message2.obj = PlayListOnLineSecondView.this.F.get(PlayListOnLineSecondView.H);
                            try {
                                co.a().a(((ct) PlayListOnLineSecondView.this.I.a(en.k(((cw) PlayListOnLineSecondView.this.F.get(PlayListOnLineSecondView.H)).e())).b()).d());
                                if (gx.b(((cw) PlayListOnLineSecondView.this.F.get(PlayListOnLineSecondView.H)).e())) {
                                    ((PlayListGroupView) PlayListOnLineSecondView.this.d).a(new PlayListGroupView.OnLineInfo((cw) PlayListOnLineSecondView.this.F.get(PlayListOnLineSecondView.H), PlayListOnLineSecondView.this.B.getFirstVisiblePosition()));
                                    PlayListOnLineSecondView.this.d.a(PlayListOnLineMusicView.class, message2);
                                    return;
                                }
                                ((PlayListGroupView) PlayListOnLineSecondView.this.d).a(new PlayListGroupView.OnLineInfo((cw) PlayListOnLineSecondView.this.F.get(PlayListOnLineSecondView.H), PlayListOnLineSecondView.this.B.getFirstVisiblePosition()));
                                PlayListOnLineSecondView.this.d.a(PlayListOnLineSecondView.class, message2);
                                return;
                            } catch (Exception e) {
                                e.printStackTrace();
                                PlayListOnLineSecondView.this.s();
                                PlayListOnLineSecondView.this.J.sendEmptyMessage(252);
                                return;
                            }
                        } else {
                            return;
                        }
                    } catch (Exception e2) {
                        e2.printStackTrace();
                        PlayListOnLineSecondView.this.s();
                        PlayListOnLineSecondView.this.J.sendEmptyMessage(252);
                        return;
                    }
                case 1:
                    PlayListOnLineSecondView.this.A.notifyDataSetChanged();
                    return;
                case 2:
                    Class<PlayListOnLineSecondView> cls2 = PlayListOnLineSecondView.class;
                    PlayListOnLineSecondView.this.d.a(cls, (Message) null);
                    jh.a(PlayListOnLineSecondView.this.E, (int) R.string.app_net_error);
                    return;
                case 252:
                    Class<PlayListOnLineSecondView> cls3 = PlayListOnLineSecondView.class;
                    PlayListOnLineSecondView.this.d.a(cls, (Message) null);
                    jh.a(PlayListOnLineSecondView.this.E, (int) R.string.app_service_unable);
                    return;
                case 253:
                    Class<PlayListOnLineSecondView> cls4 = PlayListOnLineSecondView.class;
                    PlayListOnLineSecondView.this.d.a(cls, (Message) null);
                    jh.a(PlayListOnLineSecondView.this.E, (int) R.string.app_net_error);
                    return;
                case 254:
                    PopDialogView.a(PlayListOnLineSecondView.this.getContext());
                    return;
                default:
                    return;
            }
        }
    };
    private AdapterView.OnItemClickListener K = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView adapterView, View view, int i, long j) {
            if (i == 3) {
                if (PlayListOnLineSecondView.this.g.getVisibility() == 0) {
                    PlayListOnLineSecondView.this.j();
                } else {
                    PlayListOnLineSecondView.this.i();
                }
            } else if (PlayListOnLineSecondView.this.a) {
                boolean unused = PlayListOnLineSecondView.this.a(i, PlayListOnLineSecondView.this.a);
                switch (i) {
                    case 0:
                        fd.a().a(PlayListOnLineSecondView.this.E, (Handler) null);
                        return;
                    case 1:
                    default:
                        return;
                    case 2:
                        en.d(PlayListOnLineSecondView.this.E);
                        return;
                }
            } else {
                boolean unused2 = PlayListOnLineSecondView.this.a(i);
                switch (i) {
                    case 1:
                        fd.a().a(PlayListOnLineSecondView.this.E, (Handler) null);
                        return;
                    case 2:
                        en.d(PlayListOnLineSecondView.this.E);
                        return;
                    default:
                        return;
                }
            }
        }
    };
    private AdapterView.OnItemClickListener L = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView adapterView, View view, int i, long j) {
            boolean unused = PlayListOnLineSecondView.this.b(i);
        }
    };
    private View M;
    private ImageView x;
    private Button y;
    private TextView z;

    interface ASync {
    }

    public class ListThread extends Thread {
        String a;

        public ListThread(String str) {
            this.a = str;
        }

        public void run() {
            String a2 = cq.a(PlayListOnLineSecondView.this.E, this.a, 0, 30, PlayListOnLineSecondView.this.J);
            cp.c(PlayListOnLineSecondView.this.E, a2);
            if (!en.c(a2)) {
                PlayListOnLineSecondView.this.J.sendEmptyMessage(0);
            }
        }
    }

    class MyAdapter extends BaseAdapter {
        private LayoutInflater b;
        private List c;

        class ItemStruct {
            ImageView a;
            TextView b;
            TextView c;
            ImageView d;

            private ItemStruct() {
            }
        }

        public MyAdapter(Context context, List list) {
            this.c = list;
            this.b = LayoutInflater.from(context);
        }

        public int getCount() {
            if (this.c == null) {
                return 0;
            }
            return this.c.size();
        }

        public Object getItem(int i) {
            return null;
        }

        public long getItemId(int i) {
            return (long) i;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(int i, View view, ViewGroup viewGroup) {
            ItemStruct itemStruct;
            View view2;
            String str;
            if (view == null) {
                View inflate = this.b.inflate((int) R.layout.hall_list_row, viewGroup, false);
                ItemStruct itemStruct2 = new ItemStruct();
                itemStruct2.a = (ImageView) inflate.findViewById(R.id.hall_list_image);
                itemStruct2.b = (TextView) inflate.findViewById(R.id.hall_main_title);
                itemStruct2.c = (TextView) inflate.findViewById(R.id.hall_vice_title);
                itemStruct2.d = (ImageView) inflate.findViewById(R.id.hall_list_point);
                inflate.setTag(itemStruct2);
                ItemStruct itemStruct3 = itemStruct2;
                view2 = inflate;
                itemStruct = itemStruct3;
            } else {
                itemStruct = (ItemStruct) view.getTag();
                view2 = view;
            }
            try {
                str = this.c.get(i) instanceof cr ? ae.q + "/" + en.j(((cr) this.c.get(i)).c()) : ae.q + "/" + en.j(((cv) this.c.get(i)).b());
            } catch (Exception e) {
                e.printStackTrace();
                str = "";
            }
            if (new File(str).exists()) {
                itemStruct.a.setImageBitmap(BitmapFactory.decodeFile(str));
            } else {
                itemStruct.a.setImageResource(R.drawable.home_find);
            }
            try {
                if (this.c.get(i) instanceof cv) {
                    itemStruct.b.setText(((cv) this.c.get(i)).f());
                    itemStruct.c.setText(((cv) this.c.get(i)).a());
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            itemStruct.d.setImageResource(R.drawable.list_arrow_icon);
            return view2;
        }
    }

    public class PicHandler extends Handler {
        private List b;

        public PicHandler(Looper looper, List list) {
            super(looper);
            this.b = list;
        }

        public void handleMessage(Message message) {
            String str;
            String b2;
            int b3 = en.b(PlayListOnLineSecondView.this.E);
            switch (message.what) {
                case 0:
                    if (this.b != null) {
                        for (int i = 0; i < this.b.size(); i++) {
                            try {
                                if (this.b.get(i) instanceof cr) {
                                    str = ae.q + "/" + en.j(((cr) this.b.get(i)).c());
                                    b2 = ((cr) this.b.get(i)).c();
                                } else {
                                    str = ae.q + "/" + en.j(((cv) this.b.get(i)).b());
                                    b2 = ((cv) this.b.get(i)).b();
                                }
                                if (!new File(str).exists() && il.c(PlayListOnLineSecondView.this.E)) {
                                    il.a(en.b(PlayListOnLineSecondView.this.E, b2, b3), PlayListOnLineSecondView.this.E, false, b2);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            } catch (Exception e2) {
                                e2.printStackTrace();
                            }
                        }
                    }
                    PlayListOnLineSecondView.this.J.sendEmptyMessage(1);
                    return;
                default:
                    return;
            }
        }
    }

    public class PicThread implements Runnable {
        private final Object b = new Object();
        private Looper c;

        public PicThread(String str) {
            Thread thread = new Thread(null, this, str);
            thread.setPriority(10);
            thread.start();
            synchronized (this.b) {
                while (this.c == null) {
                    try {
                        this.b.wait();
                    } catch (InterruptedException e) {
                    }
                }
            }
        }

        public Looper a() {
            return this.c;
        }

        public void run() {
            synchronized (this.b) {
                Looper.prepare();
                this.c = Looper.myLooper();
                this.b.notifyAll();
            }
            Looper.loop();
        }
    }

    public PlayListOnLineSecondView(Activity activity) {
        super(activity);
        this.E = activity;
    }

    private void r() {
        this.y.setVisibility(8);
        this.x.setImageResource(R.drawable.list_return_icon);
        this.x.setVisibility(0);
        this.z.setText(((cw) this.C.obj).f());
        this.I = hx.a();
        this.I.a("duomi", (String) null);
        try {
            this.F = ((ct) this.I.a(en.k(((cw) this.C.obj).e())).b()).c();
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.A = new MyAdapter(g(), this.F);
        this.B.setAdapter((ListAdapter) this.A);
        PicHandler picHandler = new PicHandler(new PicThread("get icons").a(), this.F);
        picHandler.removeMessages(0);
        picHandler.obtainMessage(0).sendToTarget();
        if (G != 0) {
            this.B.setSelection(G);
        }
        this.B.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView adapterView, View view, int i, long j) {
                Class<DialogView> cls = DialogView.class;
                try {
                    int unused = PlayListOnLineSecondView.H = i;
                    if (PlayListOnLineSecondView.this.I.a(en.k(((cw) PlayListOnLineSecondView.this.F.get(i)).e())) == null) {
                        if (PlayListOnLineSecondView.this.F.get(i) instanceof cr) {
                            Intent intent = new Intent("android.intent.action.VIEW");
                            intent.setData(Uri.parse(((cr) PlayListOnLineSecondView.this.F.get(i)).a()));
                            PlayListOnLineSecondView.this.E.startActivity(intent);
                        } else if (!il.b(PlayListOnLineSecondView.this.E) || !il.c(PlayListOnLineSecondView.this.E)) {
                            PlayListOnLineSecondView.this.J.sendEmptyMessage(253);
                        } else {
                            PlayListOnLineSecondView.this.d.a(DialogView.class, (Message) null);
                            new ListThread(((cw) PlayListOnLineSecondView.this.F.get(i)).e()).start();
                        }
                    } else if (!((cv) PlayListOnLineSecondView.this.F.get(i)).d()) {
                        PlayListOnLineSecondView.this.J.sendEmptyMessage(0);
                    } else if (!il.b(PlayListOnLineSecondView.this.E) || !il.c(PlayListOnLineSecondView.this.E)) {
                        PlayListOnLineSecondView.this.J.sendEmptyMessage(253);
                    } else {
                        PlayListOnLineSecondView.this.d.a(DialogView.class, (Message) null);
                        PlayListOnLineSecondView.this.I.b(en.k(((cw) PlayListOnLineSecondView.this.F.get(i)).e()));
                        new ListThread(((cw) PlayListOnLineSecondView.this.F.get(i)).e()).start();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        this.D.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                PlayListOnLineSecondView.this.s();
            }
        });
    }

    /* access modifiers changed from: private */
    public void s() {
        try {
            ((PlayListGroupView) this.d).r();
            if (((PlayListGroupView) this.d).t()) {
                this.d.a(PlayListOnLineView.class, (Message) null);
            } else {
                this.C = new Message();
                this.C.obj = ((PlayListGroupView) this.d).s().a();
                G = ((PlayListGroupView) this.d).s().b();
                r();
            }
            co.a().b();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void a(Message message) {
        this.C = message;
        if (message != null) {
            r();
        }
        super.a(message);
    }

    public boolean a(int i, KeyEvent keyEvent) {
        b(i, keyEvent);
        if (i == 4 && this.f.getVisibility() == 0 && !this.u) {
            if (this.g.getVisibility() == 0) {
                this.g.setVisibility(8);
                this.i.requestFocus();
            } else {
                this.f.setVisibility(8);
            }
            return true;
        } else if (i != 4 || this.f.getVisibility() != 8 || this.u) {
            return false;
        } else {
            s();
            return true;
        }
    }

    public void f() {
        Window window = g().getWindow();
        if (window != null) {
            window.clearFlags(1024);
        }
        inflate(g(), R.layout.playlist_online_second, this);
        this.x = (ImageView) findViewById(R.id.go_back);
        this.y = (Button) findViewById(R.id.command);
        this.D = (RelativeLayout) findViewById(R.id.title_panel);
        this.z = (TextView) findViewById(R.id.list_title);
        this.B = (ListView) findViewById(R.id.playlist_online_second);
        this.M = findViewById(R.id.scanning_tip);
        h();
    }

    public void n() {
        this.i.setOnItemClickListener(this.K);
        this.j.setOnItemClickListener(this.L);
    }

    public void o() {
        if (this.a) {
            this.q = getResources().getStringArray(R.array.PlayListOnLine_menu_without_download);
            this.s = new ArrayList();
            this.s.add(Integer.valueOf((int) R.drawable.meun_scan));
            this.s.add(Integer.valueOf((int) R.drawable.menu_bg));
        } else {
            this.q = getResources().getStringArray(R.array.PlayListOnLine_menu);
            this.s = new ArrayList();
            this.s.add(Integer.valueOf((int) R.drawable.meun_downloadsetup));
            this.s.add(Integer.valueOf((int) R.drawable.meun_scan));
        }
        this.s.add(Integer.valueOf((int) R.drawable.menu_recommend));
        this.s.add(Integer.valueOf((int) R.drawable.meun_more));
        this.s.add(Integer.valueOf((int) R.drawable.meun_setup));
        this.s.add(Integer.valueOf((int) R.drawable.meun_sleep));
        this.s.add(Integer.valueOf((int) R.drawable.meun_syc));
        this.s.add(Integer.valueOf((int) R.drawable.meun_exit));
        this.r = getResources().getStringArray(R.array.second_menu2);
        this.t = new ArrayList();
        this.t.add(Integer.valueOf((int) R.drawable.meun_help));
        this.t.add(Integer.valueOf((int) R.drawable.meun_advice));
        this.t.add(Integer.valueOf((int) R.drawable.meun_friends));
        this.t.add(Integer.valueOf((int) R.drawable.meun_about));
    }
}
