package com.duomi.app.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.duomi.android.R;
import com.duomi.android.app.media.SongInfoReader;
import com.duomi.android.app.media.SongInfoTrans;
import com.duomi.android.appwidget.DuomiAppWidgetHandler;
import com.duomi.core.view.DrawerView;
import com.duomi.core.view.TipPointView;

public class MultiView extends c {
    /* access modifiers changed from: private */
    public static AlertDialog aS;
    public static DMGallery ac = null;
    public static DMGallery af = null;
    public static boolean ak = false;
    static boolean al = true;
    static boolean am = true;
    public DrawerView A;
    public ImageView B;
    public RelativeLayout C;
    public ImageView D;
    public ImageView E;
    public ImageView F;
    public ImageView G;
    public TextView H;
    public ProgressBar I;
    public RelativeLayout J;
    public ImageView K;
    public ImageView L;
    public ImageView M;
    public ImageView N;
    public ImageView O;
    public RelativeLayout P;
    public LinearLayout Q;
    public AnimationDrawable R;
    public Dialog S;
    public LinearLayout T;
    public LinearLayout U;
    public TextView V;
    public TextView W;
    public ImageView X;
    public TextView Y;
    public TextView Z;
    /* access modifiers changed from: private */
    public Boolean aA = true;
    private View.OnTouchListener aB = new View.OnTouchListener() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.duomi.app.ui.MultiView.a(com.duomi.app.ui.MultiView, boolean):void
         arg types: [com.duomi.app.ui.MultiView, int]
         candidates:
          com.duomi.app.ui.MultiView.a(com.duomi.app.ui.MultiView, android.app.AlertDialog):android.app.AlertDialog
          com.duomi.app.ui.MultiView.a(com.duomi.app.ui.MultiView, is):is
          com.duomi.app.ui.MultiView.a(com.duomi.app.ui.MultiView, java.lang.Boolean):java.lang.Boolean
          com.duomi.app.ui.MultiView.a(com.duomi.app.ui.MultiView, int):void
          com.duomi.app.ui.MultiView.a(com.duomi.app.ui.MultiView, com.duomi.android.app.media.SongInfoTrans):void
          com.duomi.app.ui.MultiView.a(int, android.view.KeyEvent):boolean
          c.a(java.lang.Class, android.os.Message):void
          c.a(boolean, android.os.Message):void
          c.a(int, android.view.KeyEvent):boolean
          c.a(int, boolean):boolean
          com.duomi.app.ui.MultiView.a(com.duomi.app.ui.MultiView, boolean):void */
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (MultiView.this.A.f() || MultiView.this.aA.booleanValue()) {
                if (motionEvent.getAction() == 0) {
                    if (!MultiView.this.A.f()) {
                        MultiView.this.ar.removeMessages(3);
                        MultiView.this.ar.sendEmptyMessageDelayed(3, 20);
                    } else if (MultiView.this.aa) {
                        MultiView.this.x();
                    } else {
                        MultiView.this.d(true);
                    }
                }
                return true;
            } else if (!MultiView.al) {
                return true;
            } else {
                new Thread() {
                    public void run() {
                        MultiView.al = false;
                        while (!MultiView.this.aA.booleanValue()) {
                            try {
                                Thread.sleep(20);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                        MultiView.al = true;
                        MultiView.this.ar.removeMessages(1);
                        MultiView.this.ar.sendEmptyMessageDelayed(1, 20);
                    }
                }.start();
                return true;
            }
        }
    };
    private View.OnTouchListener aC = new View.OnTouchListener() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: bn.a(android.content.Context, boolean):void
         arg types: [android.content.Context, int]
         candidates:
          bn.a(int, android.content.Context):int
          bn.a(android.content.Context, boolean):void */
        public boolean onTouch(View view, MotionEvent motionEvent) {
            switch (motionEvent.getAction()) {
                case 0:
                    view.setPressed(true);
                    break;
                case 1:
                case 3:
                    view.setPressed(false);
                    if (!MultiView.this.t()) {
                        bn a2 = bn.a();
                        AlertDialog create = new AlertDialog.Builder(MultiView.this.getContext()).setPositiveButton(MultiView.this.getContext().getString(R.string.app_confirm), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogInterface, int i) {
                                as.a(MultiView.this.getContext()).u(true);
                            }
                        }).setNegativeButton(MultiView.this.getContext().getString(R.string.app_cancel), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        }).create();
                        create.setTitle(MultiView.this.getContext().getString(R.string.first_collection_title));
                        create.setMessage(MultiView.this.getContext().getString(R.string.first_collection_message));
                        create.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            public void onDismiss(DialogInterface dialogInterface) {
                                switch (gx.d(MultiView.this.getContext())) {
                                    case 0:
                                    default:
                                        return;
                                    case 1:
                                        MultiView.this.K.setImageResource(R.drawable.like_disable);
                                        jh.a(MultiView.this.getContext(), (int) R.string.player_like_tip);
                                        return;
                                    case 2:
                                        MultiView.this.K.setImageResource(R.drawable.like);
                                        jh.a(MultiView.this.getContext(), (int) R.string.player_unlike_tip);
                                        return;
                                }
                            }
                        });
                        if (gx.c(MultiView.this.getContext()) != 2 || a2.b(MultiView.this.getContext()) || !kf.b || -1 == 0 || !kf.a(MultiView.this.getContext(), 1) || as.a(MultiView.this.getContext()).U()) {
                            switch (gx.d(MultiView.this.getContext())) {
                                case 1:
                                    MultiView.this.K.setImageResource(R.drawable.like_disable);
                                    jh.a(MultiView.this.getContext(), (int) R.string.player_like_tip);
                                    break;
                                case 2:
                                    MultiView.this.K.setImageResource(R.drawable.like);
                                    jh.a(MultiView.this.getContext(), (int) R.string.player_unlike_tip);
                                    break;
                            }
                        } else {
                            create.show();
                            bn.a().a(MultiView.this.getContext(), true);
                        }
                        MultiView.this.an.sendEmptyMessage(0);
                        break;
                    } else {
                        return true;
                    }
                    break;
            }
            return true;
        }
    };
    private Handler aD;
    private View.OnTouchListener aE = new View.OnTouchListener() {
        public boolean onTouch(View view, MotionEvent motionEvent) {
            switch (motionEvent.getAction()) {
                case 0:
                    view.setPressed(true);
                    break;
                case 1:
                case 3:
                    MultiView.this.ar.removeMessages(5);
                    MultiView.this.ar.sendEmptyMessageDelayed(5, 20);
                    view.setPressed(false);
                    break;
            }
            return true;
        }
    };
    private View.OnTouchListener aF = new View.OnTouchListener() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.duomi.app.ui.MultiView.a(com.duomi.app.ui.MultiView, boolean):void
         arg types: [com.duomi.app.ui.MultiView, int]
         candidates:
          com.duomi.app.ui.MultiView.a(com.duomi.app.ui.MultiView, android.app.AlertDialog):android.app.AlertDialog
          com.duomi.app.ui.MultiView.a(com.duomi.app.ui.MultiView, is):is
          com.duomi.app.ui.MultiView.a(com.duomi.app.ui.MultiView, java.lang.Boolean):java.lang.Boolean
          com.duomi.app.ui.MultiView.a(com.duomi.app.ui.MultiView, int):void
          com.duomi.app.ui.MultiView.a(com.duomi.app.ui.MultiView, com.duomi.android.app.media.SongInfoTrans):void
          com.duomi.app.ui.MultiView.a(int, android.view.KeyEvent):boolean
          c.a(java.lang.Class, android.os.Message):void
          c.a(boolean, android.os.Message):void
          c.a(int, android.view.KeyEvent):boolean
          c.a(int, boolean):boolean
          com.duomi.app.ui.MultiView.a(com.duomi.app.ui.MultiView, boolean):void */
        public boolean onTouch(View view, MotionEvent motionEvent) {
            switch (motionEvent.getAction()) {
                case 1:
                case 3:
                    MultiView.this.d(true);
                    break;
            }
            return true;
        }
    };
    private View.OnTouchListener aG = new View.OnTouchListener() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.duomi.app.ui.MultiView.a(com.duomi.app.ui.MultiView, boolean):void
         arg types: [com.duomi.app.ui.MultiView, int]
         candidates:
          com.duomi.app.ui.MultiView.a(com.duomi.app.ui.MultiView, android.app.AlertDialog):android.app.AlertDialog
          com.duomi.app.ui.MultiView.a(com.duomi.app.ui.MultiView, is):is
          com.duomi.app.ui.MultiView.a(com.duomi.app.ui.MultiView, java.lang.Boolean):java.lang.Boolean
          com.duomi.app.ui.MultiView.a(com.duomi.app.ui.MultiView, int):void
          com.duomi.app.ui.MultiView.a(com.duomi.app.ui.MultiView, com.duomi.android.app.media.SongInfoTrans):void
          com.duomi.app.ui.MultiView.a(int, android.view.KeyEvent):boolean
          c.a(java.lang.Class, android.os.Message):void
          c.a(boolean, android.os.Message):void
          c.a(int, android.view.KeyEvent):boolean
          c.a(int, boolean):boolean
          com.duomi.app.ui.MultiView.a(com.duomi.app.ui.MultiView, boolean):void */
        public boolean onTouch(View view, MotionEvent motionEvent) {
            switch (motionEvent.getAction()) {
                case 1:
                case 3:
                    MultiView.this.d(false);
                    return true;
                case 2:
                default:
                    return true;
            }
        }
    };
    private View.OnTouchListener aH = new View.OnTouchListener() {
        public boolean onTouch(View view, MotionEvent motionEvent) {
            switch (motionEvent.getAction()) {
                case 0:
                    view.setPressed(true);
                    break;
                case 1:
                case 3:
                    view.setPressed(false);
                    if (!MultiView.this.t()) {
                        if (gx.d != null) {
                            try {
                                if (!gx.d.b()) {
                                    gx.d.b(true);
                                    break;
                                } else {
                                    gx.d.c();
                                    break;
                                }
                            } catch (RemoteException e) {
                                e.printStackTrace();
                                break;
                            }
                        } else {
                            return true;
                        }
                    } else {
                        return true;
                    }
            }
            return true;
        }
    };
    private View.OnTouchListener aI = new View.OnTouchListener() {
        public boolean onTouch(View view, MotionEvent motionEvent) {
            switch (motionEvent.getAction()) {
                case 0:
                    view.setPressed(true);
                    break;
                case 1:
                case 3:
                    view.setPressed(false);
                    if (!MultiView.this.t()) {
                        if (gx.d != null) {
                            try {
                                gx.d.d();
                                break;
                            } catch (RemoteException e) {
                                e.printStackTrace();
                                break;
                            }
                        } else {
                            return true;
                        }
                    } else {
                        return true;
                    }
            }
            return true;
        }
    };
    private View.OnTouchListener aJ = new View.OnTouchListener() {
        public boolean onTouch(View view, MotionEvent motionEvent) {
            ad.b("playNext", "playNext begin:" + System.currentTimeMillis());
            switch (motionEvent.getAction()) {
                case 0:
                    view.setPressed(true);
                    break;
                case 1:
                case 3:
                    view.setPressed(false);
                    if (!MultiView.this.t()) {
                        if (gx.d != null) {
                            try {
                                gx.d.e();
                                break;
                            } catch (RemoteException e) {
                                e.printStackTrace();
                                break;
                            }
                        } else {
                            return true;
                        }
                    } else {
                        return true;
                    }
            }
            ad.b("playNext", "playNext end:" + System.currentTimeMillis());
            return true;
        }
    };
    private jo aK = new jo() {
        public void a() {
            MultiView.this.z.setVisibility(8);
            MultiView.this.B.setImageResource(R.drawable.handle_down);
            MultiView.this.D.setImageResource(R.drawable.songinfo);
            MultiView.this.E.setVisibility(4);
            MultiView.this.F.setVisibility(4);
            MultiView.ak = true;
            MultiView.this.an.sendEmptyMessage(4);
            if (!MultiView.this.at) {
                MultiView.this.D.setVisibility(4);
                MultiView.this.G.setVisibility(4);
            }
            if (as.a(MultiView.this.getContext()).o()) {
                MultiView.this.c((int) R.string.app_gallery_drawer_fisrt_tip);
                as.a(MultiView.this.getContext()).e(false);
            }
        }

        public void b() {
            if (MultiView.this.z.getVisibility() == 0) {
                MultiView.this.z.setVisibility(8);
            }
        }
    };
    private jn aL = new jn() {
        public void a() {
            MultiView.this.B.setImageResource(R.drawable.handle_up);
            MultiView.this.D.setImageResource(R.drawable.handle_menu);
            MultiView.this.E.setVisibility(0);
            MultiView.this.F.setVisibility(0);
            MultiView.ak = false;
            if (MultiView.this.D.getVisibility() == 4) {
                MultiView.this.D.setVisibility(0);
                MultiView.this.G.setVisibility(0);
            }
        }
    };
    private jp aM = new jp() {
        public void a() {
            if (MultiView.this.z.getVisibility() == 8 && MultiView.this.A.f()) {
                MultiView.this.z.setVisibility(0);
            }
        }

        public void b() {
        }
    };
    private hg aN = new hg() {
        /* JADX WARNING: Removed duplicated region for block: B:10:0x0021  */
        /* JADX WARNING: Removed duplicated region for block: B:6:0x000e  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void a() {
            /*
                r5 = this;
                r2 = 0
                com.duomi.android.app.media.IMusicService r0 = defpackage.gx.d
                if (r0 == 0) goto L_0x001f
                com.duomi.android.app.media.IMusicService r0 = defpackage.gx.d     // Catch:{ RemoteException -> 0x001b }
                bj r0 = r0.k()     // Catch:{ RemoteException -> 0x001b }
                r3 = r0
            L_0x000c:
                if (r3 != 0) goto L_0x0021
                com.duomi.app.ui.MultiView r0 = com.duomi.app.ui.MultiView.this
                android.content.Context r0 = r0.getContext()
                r1 = 2131296512(0x7f090100, float:1.8210943E38)
                defpackage.jh.a(r0, r1)
            L_0x001a:
                return
            L_0x001b:
                r0 = move-exception
                r0.printStackTrace()
            L_0x001f:
                r3 = r2
                goto L_0x000c
            L_0x0021:
                boolean r0 = defpackage.il.c
                if (r0 == 0) goto L_0x0031
                com.duomi.app.ui.MultiView r0 = com.duomi.app.ui.MultiView.this
                android.content.Context r0 = r0.getContext()
                boolean r0 = defpackage.il.c(r0)
                if (r0 != 0) goto L_0x003e
            L_0x0031:
                com.duomi.app.ui.MultiView r0 = com.duomi.app.ui.MultiView.this
                android.content.Context r0 = r0.getContext()
                r1 = 2131296589(0x7f09014d, float:1.8211099E38)
                defpackage.jh.a(r0, r1)
                goto L_0x001a
            L_0x003e:
                com.duomi.app.ui.MultiView r0 = com.duomi.app.ui.MultiView.this
                android.content.Context r0 = r0.getContext()
                java.lang.String r1 = "layout_inflater"
                java.lang.Object r0 = r0.getSystemService(r1)
                android.view.LayoutInflater r0 = (android.view.LayoutInflater) r0
                r1 = 2130903074(0x7f030022, float:1.7412956E38)
                android.view.View r0 = r0.inflate(r1, r2)
                android.widget.LinearLayout r0 = (android.widget.LinearLayout) r0
                r1 = 2131427529(0x7f0b00c9, float:1.8476677E38)
                android.view.View r1 = r0.findViewById(r1)
                android.widget.EditText r1 = (android.widget.EditText) r1
                r2 = 2131427530(0x7f0b00ca, float:1.8476679E38)
                android.view.View r2 = r0.findViewById(r2)
                android.widget.EditText r2 = (android.widget.EditText) r2
                if (r3 == 0) goto L_0x0077
                java.lang.String r4 = r3.h()
                r1.setText(r4)
                java.lang.String r4 = r3.j()
                r2.setText(r4)
            L_0x0077:
                java.lang.String r3 = r3.h()
                int r3 = r3.length()
                r1.setSelection(r3)
                android.app.AlertDialog$Builder r3 = new android.app.AlertDialog$Builder
                com.duomi.app.ui.MultiView r4 = com.duomi.app.ui.MultiView.this
                android.content.Context r4 = r4.getContext()
                r3.<init>(r4)
                r3.setView(r0)
                com.duomi.app.ui.MultiView r0 = com.duomi.app.ui.MultiView.this
                android.content.Context r0 = r0.getContext()
                r4 = 2131296595(0x7f090153, float:1.8211111E38)
                java.lang.String r0 = r0.getString(r4)
                com.duomi.app.ui.MultiView$19$1 r4 = new com.duomi.app.ui.MultiView$19$1
                r4.<init>(r1, r2)
                r3.setPositiveButton(r0, r4)
                com.duomi.app.ui.MultiView r0 = com.duomi.app.ui.MultiView.this
                android.content.Context r0 = r0.getContext()
                r4 = 2131296596(0x7f090154, float:1.8211113E38)
                java.lang.String r0 = r0.getString(r4)
                com.duomi.app.ui.MultiView$19$2 r4 = new com.duomi.app.ui.MultiView$19$2
                r4.<init>(r1, r2)
                r3.setNegativeButton(r0, r4)
                r3.show()
                goto L_0x001a
            */
            throw new UnsupportedOperationException("Method not decompiled: com.duomi.app.ui.MultiView.AnonymousClass19.a():void");
        }
    };
    private OnLikeReSetListener aO = new OnLikeReSetListener() {
        public void a(boolean z) {
            if (z) {
                MultiView.this.K.setImageResource(R.drawable.like_disable);
            } else {
                MultiView.this.K.setImageResource(R.drawable.like);
            }
        }
    };
    private OnLyricOffsetListener aP = new OnLyricOffsetListener() {
        public void a(float f) {
            MultiView.this.ag.a(f);
        }
    };
    /* access modifiers changed from: private */
    public DialogInterface.OnCancelListener aQ = new DialogInterface.OnCancelListener() {
        public void onCancel(DialogInterface dialogInterface) {
            MultiView.this.c((int) R.string.app_gallery_fisrt_tip);
            as.a(MultiView.this.getContext()).c(false);
        }
    };
    private boolean aR = false;
    /* access modifiers changed from: private */
    public AlertDialog aT;
    private int aU = 0;
    private boolean aV = true;
    private BroadcastReceiver aW = new BroadcastReceiver() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.duomi.app.ui.MultiView.b(com.duomi.app.ui.MultiView, boolean):void
         arg types: [com.duomi.app.ui.MultiView, int]
         candidates:
          com.duomi.app.ui.MultiView.b(com.duomi.app.ui.MultiView, int):void
          c.b(int, android.view.KeyEvent):boolean
          com.duomi.app.ui.MultiView.b(com.duomi.app.ui.MultiView, boolean):void */
        public void onReceive(Context context, Intent intent) {
            ad.b("MultiView", "BroadcastReceiver----begin" + System.currentTimeMillis());
            String action = intent.getAction();
            if (action.equals("com.duomi.android.playstatechanged")) {
                MultiView.this.a(true);
            } else if (action.equals("com.duomi.android.metachanged")) {
                MultiView.this.an.sendEmptyMessage(11);
                MultiView.this.ap.removeMessages(0);
                MultiView.this.ap.obtainMessage(0).sendToTarget();
                MultiView.this.an.removeMessages(0);
                MultiView.this.an.obtainMessage(0).sendToTarget();
            } else if (action.equals("com.duomi.android.queuechanged")) {
                if (intent.getBooleanExtra("refresh", true)) {
                    MultiView.this.an.sendEmptyMessage(11);
                    MultiView.this.ap.removeMessages(0);
                    MultiView.this.ap.obtainMessage(0).sendToTarget();
                }
                MultiView.this.an.removeMessages(0);
                MultiView.this.an.obtainMessage(0).sendToTarget();
            } else if (action.equals("com.duomi.android.playbackcomplete")) {
                Message message = new Message();
                message.what = 11;
                message.obj = "com.duomi.android.playbackcomplete";
                MultiView.this.an.sendMessage(message);
                MultiView.this.a(true);
            } else if (action.equals("com.duomi.android.playbackclear")) {
                MultiView.this.an.removeMessages(1);
                MultiView.this.u();
                MultiView.this.ap.removeMessages(0);
                MultiView.this.ap.obtainMessage(0).sendToTarget();
                MultiView.this.a(true);
            } else if (action.equals("com.duomi.android.playmode")) {
                MultiView.this.an.sendEmptyMessage(18);
            }
            ad.b("MultiView", action);
            ad.b("MultiView", "BroadcastReceiver----end" + System.currentTimeMillis());
        }
    };
    public boolean aa = false;
    public boolean ab = true;
    PlayListGroupView ad = null;
    PlayListGroupView ae = null;
    PlayerLayoutView ag = null;
    LyricLayoutView ah = null;
    PlayerCurrentListView ai = null;
    SearchGroupView aj = null;
    /* access modifiers changed from: private */
    public RefreshHandler an;
    private Worker ao;
    /* access modifiers changed from: private */
    public SongInfoHandler ap;
    /* access modifiers changed from: private */
    public SongInfoReader aq;
    /* access modifiers changed from: private */
    public Handler ar;
    private long as;
    /* access modifiers changed from: private */
    public boolean at = false;
    /* access modifiers changed from: private */
    public is au;
    private Animation av;
    private AlphaAnimation aw;
    private AlphaAnimation ax;
    private boolean ay = true;
    private long az = -1;
    public boolean x = false;
    public TipPointView y;
    public RelativeLayout z;

    interface ASync {
    }

    public interface MThr {
    }

    public interface OnLikeReSetListener {
        void a(boolean z);
    }

    public interface OnLyricOffsetListener {
        void a(float f);
    }

    class RefreshHandler extends Handler {
        public RefreshHandler(Looper looper) {
            super(looper);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.duomi.app.ui.MultiView.b(com.duomi.app.ui.MultiView, boolean):void
         arg types: [com.duomi.app.ui.MultiView, int]
         candidates:
          com.duomi.app.ui.MultiView.b(com.duomi.app.ui.MultiView, int):void
          c.b(int, android.view.KeyEvent):boolean
          com.duomi.app.ui.MultiView.b(com.duomi.app.ui.MultiView, boolean):void */
        public void handleMessage(Message message) {
            float f;
            float f2;
            switch (message.what) {
                case 0:
                    ad.b("MultiView", "MThr.REFRESH::" + System.currentTimeMillis());
                    MultiView.this.a(false);
                    MultiView.this.v();
                    MultiView.this.w();
                    MultiView.this.an.removeMessages(19);
                    MultiView.this.an.sendEmptyMessageDelayed(19, 500);
                    MultiView.this.b(true);
                    ad.b("MultiView", "MThr.REFRESH::" + System.currentTimeMillis());
                    return;
                case 1:
                    MultiView.this.v();
                    return;
                case 2:
                    MultiView.this.ag.a(true);
                    return;
                case 3:
                    ad.b("MultiView", "albumpic null with no anim in handler>>" + System.currentTimeMillis());
                    MultiView.this.ag.a(false);
                    return;
                case 4:
                    MultiView.this.w();
                    return;
                case 5:
                    Bitmap bitmap = (Bitmap) message.obj;
                    if (bitmap != null) {
                        String string = message.getData().getString("path");
                        if (en.b(MultiView.this.getContext()) <= 320 || en.c(MultiView.this.getContext()) <= 480) {
                            MultiView.this.ag.a(bitmap, (Bitmap) null);
                        } else {
                            MultiView.this.ag.a(bitmap, en.a(bitmap, (int) (((double) (112.0f * en.a(MultiView.this.getContext()))) / 1.5d), string));
                        }
                        DuomiAppWidgetHandler.a().a(MultiView.this.getContext(), string);
                        return;
                    }
                    MultiView.this.ag.a(true);
                    return;
                case 6:
                    ad.b("MultiView", "lyric_msg");
                    is unused = MultiView.this.au = (is) message.obj;
                    if (MultiView.this.au != null) {
                        MultiView.this.ag.a(MultiView.this.au);
                        MultiView.this.ah.a(MultiView.this.au);
                        String str = MultiView.this.au.d()[4];
                        ad.b("MultiView", "the offset before is >>" + str);
                        if (str == null || str.trim().length() <= 0) {
                            f = 0.0f;
                        } else {
                            try {
                                f2 = Float.valueOf(str).floatValue();
                            } catch (NumberFormatException e) {
                                f2 = 0.0f;
                            }
                            ad.b("MultiView", "the offset is >>" + (f2 / 1000.0f));
                            f = f2 / 1000.0f;
                        }
                        Bundle data = message.getData();
                        int i = data != null ? data.getInt("time") : 0;
                        float a2 = (float) (((double) MultiView.this.au.a()) / 1000.0d);
                        ad.b("MultiView", "duomiOffset>>" + a2);
                        if (i > 3000) {
                            i /= 1000;
                        }
                        MultiView.this.ag.a(a2, f, i);
                        MultiView.this.ah.a(a2, f, i);
                        return;
                    }
                    MultiView.this.an.sendEmptyMessage(7);
                    return;
                case 7:
                    ad.b("MultiView", "lyric is null in handler");
                    MultiView.this.ag.b(false);
                    MultiView.this.ah.b(false);
                    return;
                case 8:
                    Bundle data2 = message.getData();
                    String[] stringArray = data2.getStringArray("name");
                    final String[] stringArray2 = data2.getStringArray("sids");
                    if (MultiView.this.A.f() && stringArray != null && stringArray2 != null && stringArray.length > 0 && stringArray2.length > 0) {
                        if (MultiView.this.aT != null && MultiView.this.aT.isShowing()) {
                            MultiView.this.aT.dismiss();
                        }
                        AlertDialog unused2 = MultiView.this.aT = new AlertDialog.Builder(MultiView.this.getContext()).setTitle((int) R.string.player_lyric_search_alert).setItems(stringArray, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogInterface, int i) {
                                MultiView.this.ap.removeMessages(1);
                                Message obtainMessage = MultiView.this.ap.obtainMessage(1);
                                Bundle bundle = new Bundle();
                                bundle.putString("sid", stringArray2[i]);
                                obtainMessage.setData(bundle);
                                MultiView.this.ap.sendMessage(obtainMessage);
                            }
                        }).setOnCancelListener(new DialogInterface.OnCancelListener() {
                            public void onCancel(DialogInterface dialogInterface) {
                                MultiView.this.an.sendEmptyMessage(7);
                            }
                        }).create();
                        MultiView.this.aT.show();
                        return;
                    }
                    return;
                case 9:
                    if (message.getData() != null ? message.getData().getBoolean("flag") : false) {
                        boolean unused3 = MultiView.this.at = true;
                        ad.b("MultiView", "handlerightimage is shouled open");
                        if (MultiView.this.A.f()) {
                            MultiView.this.D.setVisibility(0);
                            MultiView.this.G.setVisibility(0);
                            return;
                        }
                        return;
                    }
                    boolean unused4 = MultiView.this.at = false;
                    ad.b("MultiView", "handlerightimage is shouled close");
                    if (MultiView.this.A.f()) {
                        MultiView.this.D.setVisibility(4);
                        MultiView.this.G.setVisibility(4);
                    }
                    MultiView.this.x();
                    return;
                case 10:
                    MultiView.this.K.setImageResource(message.getData().getInt("likeId"));
                    return;
                case 11:
                    if (message.obj == null || message.obj != "com.duomi.android.playbackcomplete") {
                        MultiView.this.K.setImageResource(R.drawable.like);
                    }
                    MultiView.this.ag.q();
                    MultiView.this.an.sendEmptyMessage(0);
                    return;
                case 12:
                    MultiView.this.d(-1);
                    return;
                case 13:
                    MultiView.this.ag.q();
                    return;
                case 14:
                    MultiView.this.ag.b(false);
                    MultiView.this.ah.b(false);
                    return;
                case 15:
                    ad.b("MultiView", "lyric search in handler>>" + System.currentTimeMillis());
                    MultiView.this.ag.b(true);
                    MultiView.this.ah.b(true);
                    return;
                case 16:
                    MultiView.this.S.show();
                    MultiView.this.an.sendEmptyMessage(17);
                    return;
                case 17:
                    MultiView.this.R.start();
                    return;
                case 18:
                    MultiView.this.b(true);
                    return;
                case 19:
                    MultiView.this.c(true);
                    return;
                case 20:
                    MultiView.this.q();
                    return;
                default:
                    return;
            }
        }
    }

    public class SongInfoHandler extends Handler {
        public SongInfoHandler(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message message) {
            SongInfoTrans songInfoTrans;
            ad.b("MultiView", "lyric search in songinfohandler>>" + System.currentTimeMillis());
            if (gx.d != null) {
                try {
                    songInfoTrans = SongInfoTrans.a(gx.d.k(), gx.d.j());
                } catch (RemoteException e) {
                    e.printStackTrace();
                    songInfoTrans = null;
                }
                if (songInfoTrans != null) {
                    switch (message.what) {
                        case 0:
                            MultiView.this.a(songInfoTrans);
                            return;
                        case 1:
                            MultiView.this.an.sendEmptyMessage(15);
                            Bundle data = message.getData();
                            String string = data.getString("sid");
                            String string2 = data.getString("song");
                            String string3 = data.getString("singer");
                            if (string != null) {
                                MultiView.this.aq.a(songInfoTrans, string, null, null);
                                return;
                            } else {
                                MultiView.this.aq.a(songInfoTrans, null, string2, string3);
                                return;
                            }
                        default:
                            return;
                    }
                }
            }
        }
    }

    class Worker implements Runnable {
        private final Object a = new Object();
        private Looper b;

        Worker(String str) {
            Thread thread = new Thread(null, this, str);
            thread.setPriority(10);
            thread.start();
            synchronized (this.a) {
                while (this.b == null) {
                    try {
                        this.a.wait();
                    } catch (InterruptedException e) {
                    }
                }
            }
        }

        public Looper a() {
            return this.b;
        }

        public void b() {
            if (this.b != null && this.b.getThread().isAlive()) {
                try {
                    this.b.quit();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        public void run() {
            synchronized (this.a) {
                Looper.prepare();
                this.b = Looper.myLooper();
                this.a.notifyAll();
            }
            Looper.loop();
        }
    }

    public MultiView(Activity activity) {
        super(activity);
    }

    /* access modifiers changed from: private */
    public void a(SongInfoTrans songInfoTrans) {
        this.aq.a(songInfoTrans);
        this.aq.a(false);
        ad.b("MultiView", "start processLyricAndAlbum >>" + System.currentTimeMillis());
        this.aq.b(songInfoTrans);
    }

    /* access modifiers changed from: private */
    public void a(boolean z2) {
        try {
            if (gx.d == null || !gx.d.b()) {
                ad.c("MultiView", "pausebutton service is not playing");
                this.M.setImageResource(R.drawable.player_play);
                this.E.setImageResource(R.drawable.handle_play);
            } else {
                ad.c("MultiView", "pausebutton service is playing");
                this.M.setImageResource(R.drawable.player_pause);
                this.E.setImageResource(R.drawable.handle_pause);
            }
            if (z2) {
                c(false);
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    private void a(boolean z2, ax axVar, bj bjVar) {
        if (z2) {
            this.V.setBackgroundResource(R.drawable.singerinfo_titlelayout_s);
            this.W.setBackgroundResource(R.drawable.none);
        } else {
            this.V.setBackgroundResource(R.drawable.none);
            this.W.setBackgroundResource(R.drawable.singerinfo_titlelayout_s);
        }
        ad.b("MultiView", "in songinfo singerpath>>" + axVar.h());
        ad.b("MultiView", "in songinfo path>>" + axVar.c());
        if (z2) {
            Bitmap decodeFile = BitmapFactory.decodeFile(axVar.h());
            if (decodeFile != null) {
                this.X.setImageBitmap(decodeFile);
            } else {
                this.X.setImageBitmap(BitmapFactory.decodeResource(getContext().getResources(), R.drawable.ablum_deflaut));
            }
            this.Y.setText(bjVar.j());
            this.Z.setText(axVar.f().replace("\r\n", "\n"));
            return;
        }
        Bitmap decodeFile2 = BitmapFactory.decodeFile(axVar.c());
        if (decodeFile2 != null) {
            this.X.setImageBitmap(decodeFile2);
        } else {
            this.X.setImageBitmap(BitmapFactory.decodeResource(getContext().getResources(), R.drawable.ablum_deflaut));
        }
        this.Y.setText(axVar.e());
        this.Z.setText(axVar.a().replace("\r\n", "\n"));
    }

    /* access modifiers changed from: private */
    public void b(boolean z2) {
        if (z2 && gx.d != null) {
            try {
                if (gx.d.j() != null) {
                    this.ag.c(gx.d.j().f());
                }
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: private */
    public void c(int i) {
        RelativeLayout relativeLayout = (RelativeLayout) ((LayoutInflater) getContext().getSystemService("layout_inflater")).inflate((int) R.layout.dialog_grallerytip, (ViewGroup) null, false);
        ImageView imageView = (ImageView) relativeLayout.findViewById(R.id.grallery_tip_bg);
        ((TextView) relativeLayout.findViewById(R.id.grallery_tip_title)).setText(i);
        imageView.setBackgroundResource(R.drawable.grallery_tip);
        this.R = (AnimationDrawable) imageView.getBackground();
        this.S = new Dialog(getContext());
        Window window = this.S.getWindow();
        WindowManager.LayoutParams attributes = window.getAttributes();
        window.requestFeature(1);
        window.setBackgroundDrawableResource(R.drawable.none);
        window.setAttributes(attributes);
        this.S.setCanceledOnTouchOutside(true);
        this.S.requestWindowFeature(1);
        this.S.setContentView(relativeLayout);
        relativeLayout.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == 3 || motionEvent.getAction() == 1) {
                    MultiView.this.S.cancel();
                }
                return true;
            }
        });
        this.an.sendEmptyMessageDelayed(16, 500);
    }

    /* access modifiers changed from: private */
    public void c(boolean z2) {
        int i;
        int i2;
        if (z2) {
            this.ai.a((Message) null);
        } else if (gx.d != null) {
            try {
                be j = gx.d.j();
                if (j != null) {
                    i2 = j.e();
                    i = j.c();
                } else {
                    i2 = -1;
                    i = 0;
                }
            } catch (RemoteException e) {
                e.printStackTrace();
                i = 0;
                i2 = -1;
            }
            if (i2 <= 0 || i2 != this.ai.q()) {
                this.ai.a((Message) null);
            } else {
                this.ai.c(i);
            }
        }
    }

    /* access modifiers changed from: private */
    public void d(int i) {
        switch (i) {
            case -1:
                this.H.setText((int) R.string.player_default_title);
                this.H.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                if (this.at) {
                    this.at = false;
                    if (this.A.f()) {
                        this.D.setVisibility(4);
                        this.G.setVisibility(4);
                    }
                }
                if (this.I.getVisibility() != 8) {
                    this.I.setVisibility(8);
                    return;
                }
                return;
            case 0:
                this.H.setText((int) R.string.player_network_onetime_error);
                this.H.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                if (this.I.getVisibility() != 0) {
                    this.I.setVisibility(0);
                    return;
                }
                return;
            case 1:
                this.H.setText((int) R.string.player_network_error);
                this.H.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                if (this.I.getVisibility() != 8) {
                    this.I.setVisibility(8);
                }
                this.an.sendEmptyMessage(0);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public void d(boolean z2) {
        bj bjVar;
        String str;
        ax a;
        String str2;
        if (this.aa) {
            if (this.ab != z2) {
                this.ab = z2;
            } else {
                return;
            }
        }
        if (gx.d != null) {
            try {
                bj k = gx.d.k();
                if (k != null) {
                    try {
                        str2 = k.g();
                    } catch (RemoteException e) {
                        RemoteException remoteException = e;
                        bjVar = k;
                        e = remoteException;
                    }
                } else {
                    str2 = null;
                }
                String str3 = str2;
                bjVar = k;
                str = str3;
            } catch (RemoteException e2) {
                e = e2;
                bjVar = null;
            }
            if (str != null && bjVar != null && (a = ai.a(getContext()).a(str)) != null) {
                a(this.ab, a, bjVar);
                this.T.setVisibility(0);
                af.setVisibility(8);
                this.aa = true;
                return;
            }
            return;
        }
        return;
        e.printStackTrace();
        str = null;
        if (str != null) {
        }
    }

    /* access modifiers changed from: private */
    public void s() {
        g().onKeyDown(82, new KeyEvent(0, 82));
    }

    /* access modifiers changed from: private */
    public boolean t() {
        if (this.as == 0) {
            this.as = System.currentTimeMillis();
        } else if (System.currentTimeMillis() - this.as < 500) {
            return true;
        }
        this.as = System.currentTimeMillis();
        return false;
    }

    /* access modifiers changed from: private */
    public void u() {
        this.an.sendEmptyMessage(3);
        this.an.sendEmptyMessage(7);
        this.an.sendEmptyMessage(12);
        this.an.sendEmptyMessage(13);
    }

    /* access modifiers changed from: private */
    public void v() {
        if (gx.d == null) {
            d(-1);
            return;
        }
        try {
            if (gx.d.j() != null) {
                int e = gx.d.j().e();
                String b = gx.d.j().b();
                if (e <= 0 || b.equals("-1")) {
                    d(-1);
                    return;
                }
            }
            if (gx.d.m() >= 0) {
                d(gx.d.m());
            } else {
                int i = gx.d.i();
                if (gx.d.l() || i >= 100) {
                    if (this.I.getVisibility() != 8) {
                        this.I.setVisibility(8);
                    }
                    switch (this.aU) {
                        case 0:
                            if (gx.d.k() != null) {
                                this.H.setText(gx.d.k().h());
                                if (gx.d.k().b() > 0) {
                                    this.H.setCompoundDrawablesWithIntrinsicBounds((int) R.drawable.play_local_icon, 0, 0, 0);
                                } else {
                                    this.H.setCompoundDrawablesWithIntrinsicBounds((int) R.drawable.play_online_icon, 0, 0, 0);
                                }
                                this.J.startAnimation(this.av);
                                this.aU++;
                                break;
                            }
                            break;
                        case 1:
                            if (gx.d.k() != null) {
                                this.H.setText(gx.d.k().j());
                                this.H.setCompoundDrawablesWithIntrinsicBounds((int) R.drawable.play_singername_icon, 0, 0, 0);
                                this.J.startAnimation(this.av);
                                this.aU++;
                                break;
                            }
                            break;
                        case 2:
                            if (gx.d.j() != null) {
                                int c = gx.d.j().c();
                                this.H.setText((c + 1) + "/" + gx.d.j().e());
                                this.H.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                                this.J.startAnimation(this.av);
                                this.aU = 0;
                                break;
                            }
                            break;
                    }
                } else {
                    ad.b("MultiView", "buffer>>>>>>" + i);
                    if (i > 0) {
                        this.H.setText(getContext().getResources().getString(R.string.player_buffering) + i + "%");
                        this.H.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                        if (this.I.getVisibility() != 8) {
                            this.I.setVisibility(8);
                        }
                    } else {
                        this.H.setText((int) R.string.player_buffering);
                        this.H.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                        this.I.setVisibility(0);
                    }
                    this.an.removeMessages(1);
                    this.an.sendEmptyMessageDelayed(1, 500);
                    return;
                }
            }
            this.an.removeMessages(1);
            this.an.sendEmptyMessageDelayed(1, 3000);
        } catch (RemoteException e2) {
            d(-1);
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void w() {
        int i;
        int i2 = 0;
        if (ak && gx.d != null) {
            try {
                i = gx.d.h();
                try {
                    if (this.aV && !gx.a() && i == 0 && gx.b() != null && gx.b().h().equals(as.a(getContext()).ae())) {
                        i = as.a(getContext()).ad();
                    }
                    if (gx.a()) {
                        this.aV = false;
                    }
                } catch (RemoteException e) {
                    RemoteException remoteException = e;
                    i2 = i;
                    e = remoteException;
                    e.printStackTrace();
                    i = i2;
                    this.ag.a(this.au, i);
                    this.ah.a(this.au, i);
                    this.an.removeMessages(4);
                    this.an.sendEmptyMessageDelayed(4, 300);
                }
            } catch (RemoteException e2) {
                e = e2;
            }
            this.ag.a(this.au, i);
            this.ah.a(this.au, i);
            this.an.removeMessages(4);
            this.an.sendEmptyMessageDelayed(4, 300);
        }
    }

    /* access modifiers changed from: private */
    public void x() {
        this.aa = false;
        this.T.setVisibility(8);
        af.setVisibility(0);
    }

    public void a() {
        this.av = AnimationUtils.loadAnimation(getContext(), R.anim.playertitle_push_up_in);
        this.aw = (AlphaAnimation) AnimationUtils.loadAnimation(getContext(), R.anim.fast_fade_in);
        this.ax = (AlphaAnimation) AnimationUtils.loadAnimation(getContext(), R.anim.fast_fade_out);
        super.a();
        this.ao = new Worker("album art worker");
        this.ap = new SongInfoHandler(this.ao.a());
        this.aq = SongInfoReader.a(getContext(), this.an);
        if (this.az < 0 || !gx.a(this.az)) {
            this.ap.removeMessages(0);
            this.ap.obtainMessage(0).sendToTarget();
        }
        this.an.obtainMessage(0).sendToTarget();
        if (!this.aR) {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("com.duomi.android.playstatechanged");
            intentFilter.addAction("com.duomi.android.metachanged");
            intentFilter.addAction("com.duomi.android.playbackcomplete");
            intentFilter.addAction("com.duomi.android.playbackclear");
            intentFilter.addAction("com.duomi.android.queuechanged");
            intentFilter.addAction("com.duomi.android.playmode");
            g().registerReceiver(this.aW, new IntentFilter(intentFilter));
            this.aR = true;
        }
        if (!as.a(getContext()).M()) {
            this.ah.a(false);
        } else if (!this.A.f() || af.a() != 2) {
            this.ah.a(false);
        } else {
            this.ah.a(true);
        }
        if (gx.b) {
            if (this.A != null) {
                this.A.c();
            }
            gx.b = false;
        }
    }

    public boolean a(int i, KeyEvent keyEvent) {
        this.ar.removeMessages(2);
        if (i != 82 || this.aA.booleanValue()) {
            ad.c("meun", "9");
            if (!this.A.f()) {
                switch (ac.a()) {
                    case 0:
                        if (this.ad.C.getVisibility() != 0 || ak) {
                            if (this.ad.D.getVisibility() != 0 || ak) {
                                if (this.ad.F.getVisibility() != 0 || ak) {
                                    if (this.ad.E.getVisibility() == 0 && !ak) {
                                        this.x = this.ad.E.a(i, keyEvent);
                                        break;
                                    }
                                } else {
                                    this.x = this.ad.F.a(i, keyEvent);
                                    break;
                                }
                            } else {
                                this.x = this.ad.D.a(i, keyEvent);
                                break;
                            }
                        } else {
                            this.x = this.ad.C.a(i, keyEvent);
                            break;
                        }
                        break;
                    case 1:
                        if (this.ae.z.getVisibility() != 0 || ak) {
                            if (this.ae.B.getVisibility() != 0 || ak) {
                                if (this.ae.A.getVisibility() == 0 && !ak) {
                                    this.x = this.ae.A.a(i, keyEvent);
                                    break;
                                }
                            } else {
                                this.x = this.ae.B.a(i, keyEvent);
                                break;
                            }
                        } else {
                            this.x = this.ae.z.a(i, keyEvent);
                            break;
                        }
                        break;
                    case 2:
                        if (this.aj.x.getVisibility() != 0 || ak) {
                            if (this.aj.y.getVisibility() != 0 || ak) {
                                if (this.aj.z.getVisibility() == 0 && !ak) {
                                    this.x = true;
                                    break;
                                }
                            } else {
                                this.x = this.aj.y.a(i, keyEvent);
                                break;
                            }
                        } else {
                            this.x = this.aj.x.a(i, keyEvent);
                            break;
                        }
                        break;
                }
            } else if (!this.aa) {
                switch (af.a()) {
                    case 0:
                        if (this.ai.getVisibility() == 0 && ak) {
                            this.x = this.ai.a(i, keyEvent);
                            break;
                        }
                    case 1:
                        if (this.ag.getVisibility() == 0 && ak) {
                            this.x = this.ag.a(i, keyEvent);
                            ad.b("MultiView", "keycode>>" + i + ">>" + this.x);
                            break;
                        }
                    case 2:
                        if (this.ag.getVisibility() == 0 && ak) {
                            this.x = this.ah.a(i, keyEvent);
                            ad.b("MultiView", "keycode>>" + i + ">>" + this.x);
                            break;
                        }
                }
                if (i == 4 && !this.x) {
                    this.A.d();
                    return true;
                }
            } else if (i != 4) {
                return false;
            } else {
                x();
                return true;
            }
            return this.x;
        }
        final Message obtainMessage = this.ar.obtainMessage(2);
        obtainMessage.arg1 = i;
        obtainMessage.obj = keyEvent;
        if (!am) {
            return false;
        }
        new Thread() {
            public void run() {
                MultiView.am = false;
                while (!MultiView.this.aA.booleanValue()) {
                    try {
                        Thread.sleep(20);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                MultiView.am = true;
                MultiView.this.ar.removeMessages(2);
                MultiView.this.ar.sendMessageDelayed(obtainMessage, 100);
            }
        }.start();
        return false;
    }

    public void b() {
        super.b();
        if (this.an != null) {
            this.an.removeMessages(1);
            this.an.removeMessages(4);
        }
        if (this.aR) {
            g().unregisterReceiver(this.aW);
            this.aR = false;
        }
        if (gx.d != null) {
            try {
                bj k = gx.d.k();
                if (k != null) {
                    this.az = k.f();
                }
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        this.ao.b();
    }

    public void c() {
        ad.b("MultiView", "mutilonResume");
    }

    public void d() {
        if (this.aR) {
            g().unregisterReceiver(this.aW);
            this.aR = false;
        }
        if (ac != null) {
            int childCount = ac.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View childAt = ac.getChildAt(i);
                if (childAt instanceof c) {
                    try {
                        ((c) childAt).d();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public void f() {
        inflate(g(), R.layout.multi_view, this);
        Message message = new Message();
        message.what = 2;
        p.a(g()).a(message);
        ac = (DMGallery) findViewById(R.id.gallery);
        ac.a(false);
        this.ad = new PlayListGroupView(g());
        this.ad.f();
        this.ad.setBackgroundResource(R.drawable.listlayout_bg);
        ac.addView(this.ad);
        this.ae = new PlayListGroupView(g(), true);
        this.ae.f();
        this.ae.setBackgroundResource(R.drawable.listlayout_bg);
        ac.addView(this.ae);
        this.aj = new SearchGroupView(g());
        this.aj.f();
        this.aj.setBackgroundResource(R.drawable.listlayout_bg);
        ac.addView(this.aj);
        af = (DMGallery) findViewById(R.id.contentlayout);
        this.y = (TipPointView) findViewById(R.id.tippoint);
        af.a(false);
        this.z = (RelativeLayout) findViewById(R.id.scrolllayout);
        this.A = (DrawerView) findViewById(R.id.slidingdrawer);
        this.D = (ImageView) findViewById(R.id.handle_right);
        this.E = (ImageView) findViewById(R.id.handle_left);
        this.F = (ImageView) findViewById(R.id.handle_left_line);
        this.G = (ImageView) findViewById(R.id.handle_right_line);
        this.H = (TextView) findViewById(R.id.handle_name);
        this.I = (ProgressBar) findViewById(R.id.handle_title_progress);
        this.J = (RelativeLayout) findViewById(R.id.handle_name_layout);
        this.H.setBackgroundResource(R.drawable.transparent_background);
        this.B = (ImageView) findViewById(R.id.handlebar);
        this.C = (RelativeLayout) findViewById(R.id.handlbar_layout);
        this.Q = (LinearLayout) findViewById(R.id.player_control_layout);
        this.K = (ImageView) findViewById(R.id.player_control_left);
        this.L = (ImageView) findViewById(R.id.player_control_prev);
        this.M = (ImageView) findViewById(R.id.player_control_play);
        this.N = (ImageView) findViewById(R.id.player_control_next);
        this.O = (ImageView) findViewById(R.id.player_control_right);
        this.P = (RelativeLayout) findViewById(R.id.content);
        this.T = (LinearLayout) findViewById(R.id.player_infolayout);
        this.U = (LinearLayout) findViewById(R.id.player_info_titlelayout);
        this.V = (TextView) findViewById(R.id.player_info_lefttitle);
        this.W = (TextView) findViewById(R.id.player_info_righttitle);
        this.X = (ImageView) findViewById(R.id.player_info_album);
        this.Y = (TextView) findViewById(R.id.player_info_name);
        this.Z = (TextView) findViewById(R.id.player_info_content);
        this.K.setImageResource(R.drawable.like);
        this.L.setImageResource(R.drawable.player_pre);
        this.M.setImageResource(R.drawable.player_play);
        this.N.setImageResource(R.drawable.player_next);
        this.O.setImageResource(R.drawable.menu);
        this.K.setBackgroundResource(R.drawable.control_bg);
        this.L.setBackgroundResource(R.drawable.control_bg);
        this.M.setBackgroundResource(R.drawable.control_bg);
        this.N.setBackgroundResource(R.drawable.control_bg);
        this.O.setBackgroundResource(R.drawable.control_bg);
        this.K.setOnTouchListener(this.aC);
        this.L.setOnTouchListener(this.aI);
        this.M.setOnTouchListener(this.aH);
        this.N.setOnTouchListener(this.aJ);
        this.O.setOnTouchListener(this.aE);
        this.C.setBackgroundResource(R.drawable.handlelayout_bg);
        this.B.setImageResource(R.drawable.handle_up);
        this.B.setBackgroundResource(R.drawable.handle_bar);
        this.D.setImageResource(R.drawable.handle_menu);
        this.E.setImageResource(R.drawable.handle_pause);
        this.F.setImageResource(R.drawable.handle_line);
        this.G.setImageResource(R.drawable.handle_line);
        this.P.setBackgroundResource(R.drawable.playlayout_bg);
        this.Q.setBackgroundResource(R.drawable.playcontrol_bg);
        this.U.setBackgroundResource(R.drawable.online_titlelayout_bg);
        this.ai = new PlayerCurrentListView(g());
        this.ai.f();
        af.addView(this.ai);
        this.ag = new PlayerLayoutView(g());
        this.ag.f();
        af.addView(this.ag);
        this.ah = new LyricLayoutView(g());
        this.ah.f();
        af.addView(this.ah);
        af.a(1);
        this.ag.a(this.aN);
        this.ah.a(this.aN);
        this.ag.a(this.aO);
        this.ah.a(this.aO);
        this.ah.a(this.aP);
        ac.a(new jk() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.duomi.app.ui.MultiView.a(com.duomi.app.ui.MultiView, java.lang.Boolean):java.lang.Boolean
             arg types: [com.duomi.app.ui.MultiView, int]
             candidates:
              com.duomi.app.ui.MultiView.a(com.duomi.app.ui.MultiView, android.app.AlertDialog):android.app.AlertDialog
              com.duomi.app.ui.MultiView.a(com.duomi.app.ui.MultiView, is):is
              com.duomi.app.ui.MultiView.a(com.duomi.app.ui.MultiView, int):void
              com.duomi.app.ui.MultiView.a(com.duomi.app.ui.MultiView, com.duomi.android.app.media.SongInfoTrans):void
              com.duomi.app.ui.MultiView.a(com.duomi.app.ui.MultiView, boolean):void
              com.duomi.app.ui.MultiView.a(int, android.view.KeyEvent):boolean
              c.a(java.lang.Class, android.os.Message):void
              c.a(boolean, android.os.Message):void
              c.a(int, android.view.KeyEvent):boolean
              c.a(int, boolean):boolean
              com.duomi.app.ui.MultiView.a(com.duomi.app.ui.MultiView, java.lang.Boolean):java.lang.Boolean */
            public void a() {
                Boolean unused = MultiView.this.aA = (Boolean) false;
            }
        });
        ac.a(new jj() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.duomi.app.ui.MultiView.a(com.duomi.app.ui.MultiView, java.lang.Boolean):java.lang.Boolean
             arg types: [com.duomi.app.ui.MultiView, int]
             candidates:
              com.duomi.app.ui.MultiView.a(com.duomi.app.ui.MultiView, android.app.AlertDialog):android.app.AlertDialog
              com.duomi.app.ui.MultiView.a(com.duomi.app.ui.MultiView, is):is
              com.duomi.app.ui.MultiView.a(com.duomi.app.ui.MultiView, int):void
              com.duomi.app.ui.MultiView.a(com.duomi.app.ui.MultiView, com.duomi.android.app.media.SongInfoTrans):void
              com.duomi.app.ui.MultiView.a(com.duomi.app.ui.MultiView, boolean):void
              com.duomi.app.ui.MultiView.a(int, android.view.KeyEvent):boolean
              c.a(java.lang.Class, android.os.Message):void
              c.a(boolean, android.os.Message):void
              c.a(int, android.view.KeyEvent):boolean
              c.a(int, boolean):boolean
              com.duomi.app.ui.MultiView.a(com.duomi.app.ui.MultiView, java.lang.Boolean):java.lang.Boolean */
            public void a(View view, int i) {
                MultiView.this.y.a(i);
                Boolean unused = MultiView.this.aA = (Boolean) true;
            }
        });
        af.a(new jk() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.duomi.app.ui.MultiView.a(com.duomi.app.ui.MultiView, java.lang.Boolean):java.lang.Boolean
             arg types: [com.duomi.app.ui.MultiView, int]
             candidates:
              com.duomi.app.ui.MultiView.a(com.duomi.app.ui.MultiView, android.app.AlertDialog):android.app.AlertDialog
              com.duomi.app.ui.MultiView.a(com.duomi.app.ui.MultiView, is):is
              com.duomi.app.ui.MultiView.a(com.duomi.app.ui.MultiView, int):void
              com.duomi.app.ui.MultiView.a(com.duomi.app.ui.MultiView, com.duomi.android.app.media.SongInfoTrans):void
              com.duomi.app.ui.MultiView.a(com.duomi.app.ui.MultiView, boolean):void
              com.duomi.app.ui.MultiView.a(int, android.view.KeyEvent):boolean
              c.a(java.lang.Class, android.os.Message):void
              c.a(boolean, android.os.Message):void
              c.a(int, android.view.KeyEvent):boolean
              c.a(int, boolean):boolean
              com.duomi.app.ui.MultiView.a(com.duomi.app.ui.MultiView, java.lang.Boolean):java.lang.Boolean */
            public void a() {
                Boolean unused = MultiView.this.aA = (Boolean) false;
            }
        });
        af.a(new jj() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.duomi.app.ui.MultiView.a(com.duomi.app.ui.MultiView, java.lang.Boolean):java.lang.Boolean
             arg types: [com.duomi.app.ui.MultiView, int]
             candidates:
              com.duomi.app.ui.MultiView.a(com.duomi.app.ui.MultiView, android.app.AlertDialog):android.app.AlertDialog
              com.duomi.app.ui.MultiView.a(com.duomi.app.ui.MultiView, is):is
              com.duomi.app.ui.MultiView.a(com.duomi.app.ui.MultiView, int):void
              com.duomi.app.ui.MultiView.a(com.duomi.app.ui.MultiView, com.duomi.android.app.media.SongInfoTrans):void
              com.duomi.app.ui.MultiView.a(com.duomi.app.ui.MultiView, boolean):void
              com.duomi.app.ui.MultiView.a(int, android.view.KeyEvent):boolean
              c.a(java.lang.Class, android.os.Message):void
              c.a(boolean, android.os.Message):void
              c.a(int, android.view.KeyEvent):boolean
              c.a(int, boolean):boolean
              com.duomi.app.ui.MultiView.a(com.duomi.app.ui.MultiView, java.lang.Boolean):java.lang.Boolean */
            public void a(View view, int i) {
                Boolean unused = MultiView.this.aA = (Boolean) true;
                if (i == 2) {
                    if (as.a(MultiView.this.getContext()).M()) {
                        MultiView.this.ah.a(true);
                    }
                } else if (as.a(MultiView.this.getContext()).M()) {
                    MultiView.this.ah.a(false);
                }
            }
        });
        this.A.a(this.aK);
        this.A.a(this.aL);
        this.A.a(this.aM);
        this.V.setOnTouchListener(this.aF);
        this.W.setOnTouchListener(this.aG);
        this.D.setOnTouchListener(this.aB);
        this.E.setOnTouchListener(this.aH);
        if (!(ac == null || this.y == null)) {
            this.y.a(ac.a());
        }
        ad.b("MultiView", "multi is open");
        this.ar = new Handler() {
            public void handleMessage(Message message) {
                switch (message.what) {
                    case 1:
                        MultiView.this.s();
                        return;
                    case 2:
                        MultiView.this.a(message.arg1, (KeyEvent) message.obj);
                        return;
                    case 3:
                        MultiView.this.g().onKeyDown(82, new KeyEvent(0, 82));
                        return;
                    case 4:
                    default:
                        return;
                    case 5:
                        MultiView.this.g().onKeyDown(82, new KeyEvent(0, 82));
                        return;
                }
            }
        };
        this.aD = new Handler() {
            public void handleMessage(Message message) {
            }
        };
        this.an = new RefreshHandler(g().getMainLooper());
        db.a(g(), this.an);
    }

    public void q() {
        if (as.a(getContext()).m()) {
            fd.a().a(getContext(), new Handler() {
                public void handleMessage(Message message) {
                    switch (message.what) {
                        case 0:
                            if (as.a(MultiView.this.getContext()).n() && MultiView.aS == null) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(MultiView.this.getContext());
                                builder.setCancelable(false);
                                AlertDialog unused = MultiView.aS = builder.setTitle((int) R.string.app_name).setMessage((int) R.string.app_addshortcut_tip).setPositiveButton((int) R.string.app_confirm, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        Intent intent = new Intent("com.android.launcher.action.INSTALL_SHORTCUT");
                                        Intent intent2 = new Intent("android.intent.action.MAIN");
                                        Intent.ShortcutIconResource fromContext = Intent.ShortcutIconResource.fromContext(MultiView.this.getContext(), R.drawable.icon1);
                                        intent2.setClassName("com.duomi.android", "com.duomi.android.DuomiMainActivity");
                                        intent.putExtra("android.intent.extra.shortcut.NAME", MultiView.this.getContext().getString(R.string.app_name));
                                        intent.putExtra("android.intent.extra.shortcut.ICON_RESOURCE", fromContext);
                                        intent.putExtra("android.intent.extra.shortcut.INTENT", intent2);
                                        MultiView.this.getContext().sendBroadcast(intent);
                                        MultiView.this.aQ.onCancel(dialogInterface);
                                    }
                                }).setNegativeButton((int) R.string.app_cancel, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        MultiView.this.aQ.onCancel(dialogInterface);
                                    }
                                }).create();
                                MultiView.aS.show();
                                return;
                            }
                            return;
                        default:
                            return;
                    }
                }
            });
            as.a(getContext()).d(false);
        }
    }
}
