package com.duomi.app.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.duomi.android.R;
import com.duomi.app.ui.MultiView;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class PlayListOnLineMusicView extends c implements View.OnClickListener {
    private static int E = 1;
    /* access modifiers changed from: private */
    public static int ae = 0;
    /* access modifiers changed from: private */
    public static int af = 30;
    /* access modifiers changed from: private */
    public static int[] ag = new int[2];
    public static int x = 0;
    private ImageView A;
    private ImageView B;
    private ImageView C;
    private RelativeLayout D;
    private FrameLayout F;
    /* access modifiers changed from: private */
    public Message G;
    private Button H;
    /* access modifiers changed from: private */
    public MyAdapter I;
    /* access modifiers changed from: private */
    public ListView J;
    /* access modifiers changed from: private */
    public int K = -1;
    private View L;
    private boolean M = false;
    private ImageView[] N = new ImageView[5];
    private Dialog O;
    /* access modifiers changed from: private */
    public Context P;
    /* access modifiers changed from: private */
    public List Q;
    /* access modifiers changed from: private */
    public List R;
    private TextView S;
    private ImageView T;
    private RelativeLayout U;
    private RelativeLayout V;
    private RelativeLayout W;
    private RelativeLayout X;
    private RelativeLayout Y;
    /* access modifiers changed from: private */
    public View Z;
    /* access modifiers changed from: private */
    public View aa;
    private Button ab;
    /* access modifiers changed from: private */
    public LinearLayout ac;
    /* access modifiers changed from: private */
    public RelativeLayout ad;
    /* access modifiers changed from: private */
    public ar ah;
    /* access modifiers changed from: private */
    public int ai = 0;
    /* access modifiers changed from: private */
    public hx aj;
    private AbsListView.OnScrollListener ak = new AbsListView.OnScrollListener() {
        public void onScroll(AbsListView absListView, int i, int i2, int i3) {
            if (i + i2 >= i3 && i3 - PlayListOnLineMusicView.this.ai < PlayListOnLineMusicView.ae && PlayListOnLineMusicView.x < i3 - PlayListOnLineMusicView.this.ai) {
                PlayListOnLineMusicView.x += PlayListOnLineMusicView.af;
                PlayListOnLineMusicView.this.ac.setVisibility(0);
                new Thread(PlayListOnLineMusicView.this.al).start();
            }
        }

        public void onScrollStateChanged(AbsListView absListView, int i) {
        }
    };
    /* access modifiers changed from: private */
    public Runnable al = new Runnable() {
        public void run() {
            if (PlayListOnLineMusicView.this.G != null && PlayListOnLineMusicView.this.G.obj != null) {
                try {
                    int c = cp.c(PlayListOnLineMusicView.this.P, cq.a(PlayListOnLineMusicView.this.P, ((cw) PlayListOnLineMusicView.this.G.obj).e(), PlayListOnLineMusicView.x, PlayListOnLineMusicView.af, PlayListOnLineMusicView.this.am));
                    if (c > 0) {
                        PlayListOnLineMusicView.this.am.obtainMessage(0, PlayListOnLineMusicView.this.G.obj).sendToTarget();
                    } else if (c == 0) {
                        PlayListOnLineMusicView.this.am.sendEmptyMessage(2);
                    } else {
                        PlayListOnLineMusicView.this.am.sendEmptyMessage(3);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    };
    /* access modifiers changed from: private */
    public Handler am = new Handler() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: gx.a(android.content.Context, bj, bl, com.duomi.app.ui.MultiView$OnLikeReSetListener, boolean):void
         arg types: [android.content.Context, bj, bl, ?[OBJECT, ARRAY], int]
         candidates:
          gx.a(android.content.Context, bj, int, android.os.Handler, com.duomi.app.ui.MultiView$OnLikeReSetListener):void
          gx.a(android.content.Context, bl, long, com.duomi.app.ui.MultiView$OnLikeReSetListener, int):void
          gx.a(android.content.Context, bj, bl, com.duomi.app.ui.MultiView$OnLikeReSetListener, boolean):void */
        public void handleMessage(Message message) {
            Class<PlayListOnLineView> cls = PlayListOnLineView.class;
            switch (message.what) {
                case 0:
                    if (message.obj != null) {
                        List unused = PlayListOnLineMusicView.this.Q = ((ct) PlayListOnLineMusicView.this.aj.a(en.k(((cw) message.obj).e())).b()).c();
                        for (int i = 0; i < PlayListOnLineMusicView.af; i++) {
                            PlayListOnLineMusicView.this.R.add(0);
                        }
                        if (PlayListOnLineMusicView.this.I == null) {
                            MyAdapter unused2 = PlayListOnLineMusicView.this.I = new MyAdapter(PlayListOnLineMusicView.this.P, PlayListOnLineMusicView.this.Q, PlayListOnLineMusicView.this.R);
                            PlayListOnLineMusicView.this.J.setAdapter((ListAdapter) PlayListOnLineMusicView.this.I);
                        } else {
                            PlayListOnLineMusicView.this.I.notifyDataSetChanged();
                        }
                        if (PlayListOnLineMusicView.x > 1) {
                            PlayListOnLineMusicView.this.ac.setVisibility(8);
                            return;
                        }
                        return;
                    }
                    return;
                case 1:
                    PlayListOnLineMusicView.this.ac.setVisibility(8);
                    PlayListOnLineMusicView.this.ad.setVisibility(0);
                    return;
                case 2:
                default:
                    return;
                case 252:
                    Class<PlayListOnLineView> cls2 = PlayListOnLineView.class;
                    PlayListOnLineMusicView.this.d.a(cls, (Message) null);
                    jh.a(PlayListOnLineMusicView.this.P, (int) R.string.app_service_unable);
                    return;
                case 253:
                    Class<PlayListOnLineView> cls3 = PlayListOnLineView.class;
                    PlayListOnLineMusicView.this.d.a(cls, (Message) null);
                    jh.a(PlayListOnLineMusicView.this.P, (int) R.string.app_net_error);
                    return;
                case 254:
                    PopDialogView.a(PlayListOnLineMusicView.this.getContext());
                    return;
                case 255:
                    try {
                        gx.a(PlayListOnLineMusicView.this.P, PlayListOnLineMusicView.a(PlayListOnLineMusicView.this.getContext(), (cx) PlayListOnLineMusicView.this.Q.get(PlayListOnLineMusicView.this.K)), ar.a(PlayListOnLineMusicView.this.getContext()).d(message.getData().getInt("listId")), (MultiView.OnLikeReSetListener) null, false);
                        Intent intent = new Intent("com.duomi.android.app.scanner.scancomplete");
                        intent.setAction("com.duomi.android.app.scanner.scancomplete");
                        PlayListOnLineMusicView.this.P.sendBroadcast(intent);
                        return;
                    } catch (Exception e) {
                        e.printStackTrace();
                        return;
                    }
            }
        }
    };
    private AdapterView.OnItemClickListener an = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView adapterView, View view, int i, long j) {
            try {
                if (PlayListOnLineMusicView.this.ai == 2) {
                    if (i == 0) {
                        PlayListOnLineMusicView.this.x();
                    } else if (i == 1) {
                        new PlayModelTask().execute(new Object[0]);
                    } else {
                        bj a2 = PlayListOnLineMusicView.a(PlayListOnLineMusicView.this.getContext(), (cx) PlayListOnLineMusicView.this.Q.get(i - PlayListOnLineMusicView.this.ai));
                        gx.a(PlayListOnLineMusicView.this.getContext(), a2);
                        ad.b("PlayListOnLineMusic", "song audition url>>>>>>" + a2.p());
                        ad.b("PlayListOnLineMusic", "song display url>>>>>>" + a2.x());
                        ad.b("PlayListOnLineMusic", "song low url>>>>>>>>>>" + a2.o());
                    }
                } else if (i == 0) {
                    new PlayModelTask().execute(new Object[0]);
                } else {
                    bj a3 = PlayListOnLineMusicView.a(PlayListOnLineMusicView.this.getContext(), (cx) PlayListOnLineMusicView.this.Q.get(i - PlayListOnLineMusicView.this.ai));
                    gx.a(PlayListOnLineMusicView.this.getContext(), a3);
                    ad.b("PlayListOnLineMusic", "song audition url>>>>>>" + a3.p());
                    ad.b("PlayListOnLineMusic", "song display url>>>>>>" + a3.x());
                    ad.b("PlayListOnLineMusic", "song low url>>>>>>>>>>" + a3.o());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
    private AdapterView.OnItemClickListener ao = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView adapterView, View view, int i, long j) {
            if (i == 3) {
                if (PlayListOnLineMusicView.this.g.getVisibility() == 0) {
                    PlayListOnLineMusicView.this.j();
                } else {
                    PlayListOnLineMusicView.this.i();
                }
            } else if (PlayListOnLineMusicView.this.a) {
                boolean unused = PlayListOnLineMusicView.this.a(i, PlayListOnLineMusicView.this.a);
                switch (i) {
                    case 0:
                        fd.a().a(PlayListOnLineMusicView.this.P, (Handler) null);
                        return;
                    case 1:
                    default:
                        return;
                    case 2:
                        en.d(PlayListOnLineMusicView.this.P);
                        return;
                }
            } else {
                boolean unused2 = PlayListOnLineMusicView.this.a(i);
                switch (i) {
                    case 1:
                        fd.a().a(PlayListOnLineMusicView.this.P, (Handler) null);
                        return;
                    case 2:
                        en.d(PlayListOnLineMusicView.this.P);
                        return;
                    default:
                        return;
                }
            }
        }
    };
    private AdapterView.OnItemClickListener ap = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView adapterView, View view, int i, long j) {
            boolean unused = PlayListOnLineMusicView.this.b(i);
        }
    };
    private View aq;
    private TextView y;
    private TextView z;

    class ItemStruct {
        TextView a;
        TextView b;
        TextView c;
        RatingBar d;
        TextView e;
        ImageView f;

        ItemStruct() {
        }
    }

    class MyAdapter extends BaseAdapter {
        ItemStruct a;
        LinearLayout b;
        boolean c;
        private LayoutInflater e;
        /* access modifiers changed from: private */
        public List f;

        private MyAdapter(Context context, List list, List list2) {
            this.c = false;
            this.e = LayoutInflater.from(context);
            this.f = list2;
        }

        private void a(final int i) {
            try {
                String d2 = ((cx) PlayListOnLineMusicView.this.Q.get(i)).d();
                String h = ((cx) PlayListOnLineMusicView.this.Q.get(i)).h();
                String i2 = ((cx) PlayListOnLineMusicView.this.Q.get(i)).i();
                if (!en.c(d2)) {
                    this.a.a.setText((i + 1) + "." + d2);
                    this.a.c.setVisibility(8);
                    this.a.b.setText(h);
                    this.a.d.setVisibility(8);
                    this.a.e.setVisibility(0);
                    this.a.e.setText(i2);
                } else {
                    this.a.c.setVisibility(0);
                    this.a.d.setVisibility(0);
                    this.a.e.setVisibility(8);
                    this.a.a.setText((i + 1) + "." + ((cx) PlayListOnLineMusicView.this.Q.get(i)).f());
                    this.a.b.setText(((cx) PlayListOnLineMusicView.this.Q.get(i)).a());
                    this.a.c.setText(en.d(Integer.valueOf(((cx) PlayListOnLineMusicView.this.Q.get(i)).b()).intValue() * 1000));
                }
                String c2 = ((cx) PlayListOnLineMusicView.this.Q.get(i)).c();
                this.a.d.setRating(Float.valueOf(c2.substring(c2.lastIndexOf("|") + 1, c2.length())).floatValue() / 2.0f);
                if (((Integer) this.f.get(i)).intValue() == 1) {
                    this.a.f.setImageResource(R.drawable.online_addicon_f);
                } else {
                    this.a.f.setImageResource(R.drawable.online_addicon_normal);
                }
                this.b.setOnClickListener(new View.OnClickListener() {
                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: gx.a(android.content.Context, bj, bl, com.duomi.app.ui.MultiView$OnLikeReSetListener, boolean):void
                     arg types: [android.content.Context, bj, bl, ?[OBJECT, ARRAY], int]
                     candidates:
                      gx.a(android.content.Context, bj, int, android.os.Handler, com.duomi.app.ui.MultiView$OnLikeReSetListener):void
                      gx.a(android.content.Context, bl, long, com.duomi.app.ui.MultiView$OnLikeReSetListener, int):void
                      gx.a(android.content.Context, bj, bl, com.duomi.app.ui.MultiView$OnLikeReSetListener, boolean):void */
                    public void onClick(View view) {
                        try {
                            if (((Integer) MyAdapter.this.f.get(i)).intValue() == 1) {
                                MyAdapter.this.f.set(i, 0);
                                gx.a(PlayListOnLineMusicView.this.P, PlayListOnLineMusicView.a(PlayListOnLineMusicView.this.getContext(), (cx) PlayListOnLineMusicView.this.Q.get(i)), PlayListOnLineMusicView.this.ah.b(bn.a().h(), 0));
                                PlayListOnLineMusicView.this.I.notifyDataSetChanged();
                                return;
                            }
                            MyAdapter.this.f.set(i, 1);
                            gx.a(PlayListOnLineMusicView.this.P, PlayListOnLineMusicView.a(PlayListOnLineMusicView.this.getContext(), (cx) PlayListOnLineMusicView.this.Q.get(i)), PlayListOnLineMusicView.this.ah.b(bn.a().h(), 0), (MultiView.OnLikeReSetListener) null, false);
                            PlayListOnLineMusicView.this.I.notifyDataSetChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }

        public int getCount() {
            return PlayListOnLineMusicView.this.Q == null ? PlayListOnLineMusicView.this.ai : PlayListOnLineMusicView.this.Q.size() + PlayListOnLineMusicView.this.ai;
        }

        public Object getItem(int i) {
            if (i < PlayListOnLineMusicView.this.ai) {
                return null;
            }
            if (PlayListOnLineMusicView.this.Q == null) {
                return null;
            }
            return PlayListOnLineMusicView.this.Q.get(i);
        }

        public long getItemId(int i) {
            if (i < PlayListOnLineMusicView.this.ai) {
                return -1;
            }
            return (long) i;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(int i, View view, ViewGroup viewGroup) {
            View view2;
            View view3;
            if (PlayListOnLineMusicView.this.ai == 2) {
                if (i == 0) {
                    View t = PlayListOnLineMusicView.this.Z;
                    this.c = false;
                    return t;
                } else if (i == 1) {
                    View u = PlayListOnLineMusicView.this.aa;
                    this.c = false;
                    return u;
                } else if (view == null || !this.c) {
                    this.a = new ItemStruct();
                    View inflate = this.e.inflate((int) R.layout.playlist_online_music_row, viewGroup, false);
                    this.a.a = (TextView) inflate.findViewById(R.id.playlist_online_song);
                    this.a.b = (TextView) inflate.findViewById(R.id.playlist_online_singer);
                    this.a.c = (TextView) inflate.findViewById(R.id.playlist_online_time);
                    this.a.d = (RatingBar) inflate.findViewById(R.id.pop_value);
                    this.a.f = (ImageView) inflate.findViewById(R.id.add_image);
                    this.a.e = (TextView) inflate.findViewById(R.id.playlist_online_sttime);
                    this.b = (LinearLayout) inflate.findViewById(R.id.add_layout);
                    inflate.setTag(this.a);
                    a(i - PlayListOnLineMusicView.this.ai);
                    this.c = true;
                    return inflate;
                } else {
                    this.a = (ItemStruct) view.getTag();
                    if (this.a == null) {
                        this.a = new ItemStruct();
                        View inflate2 = this.e.inflate((int) R.layout.playlist_online_music_row, viewGroup, false);
                        this.a.a = (TextView) inflate2.findViewById(R.id.playlist_online_song);
                        this.a.b = (TextView) inflate2.findViewById(R.id.playlist_online_singer);
                        this.a.c = (TextView) inflate2.findViewById(R.id.playlist_online_time);
                        this.a.d = (RatingBar) inflate2.findViewById(R.id.pop_value);
                        this.a.f = (ImageView) inflate2.findViewById(R.id.add_image);
                        this.a.e = (TextView) inflate2.findViewById(R.id.playlist_online_sttime);
                        this.b = (LinearLayout) inflate2.findViewById(R.id.add_layout);
                        inflate2.setTag(this.a);
                        view3 = inflate2;
                    } else {
                        this.a.a = (TextView) view.findViewById(R.id.playlist_online_song);
                        this.a.b = (TextView) view.findViewById(R.id.playlist_online_singer);
                        this.a.c = (TextView) view.findViewById(R.id.playlist_online_time);
                        this.a.d = (RatingBar) view.findViewById(R.id.pop_value);
                        this.a.f = (ImageView) view.findViewById(R.id.add_image);
                        this.a.e = (TextView) view.findViewById(R.id.playlist_online_sttime);
                        this.b = (LinearLayout) view.findViewById(R.id.add_layout);
                        view3 = view;
                    }
                    a(i - PlayListOnLineMusicView.this.ai);
                    return view3;
                }
            } else if (i == 0) {
                View u2 = PlayListOnLineMusicView.this.aa;
                this.c = false;
                return u2;
            } else if (view == null || !this.c) {
                this.a = new ItemStruct();
                View inflate3 = this.e.inflate((int) R.layout.playlist_online_music_row, viewGroup, false);
                this.a.a = (TextView) inflate3.findViewById(R.id.playlist_online_song);
                this.a.b = (TextView) inflate3.findViewById(R.id.playlist_online_singer);
                this.a.c = (TextView) inflate3.findViewById(R.id.playlist_online_time);
                this.a.d = (RatingBar) inflate3.findViewById(R.id.pop_value);
                this.a.f = (ImageView) inflate3.findViewById(R.id.add_image);
                this.a.e = (TextView) inflate3.findViewById(R.id.playlist_online_sttime);
                this.b = (LinearLayout) inflate3.findViewById(R.id.add_layout);
                inflate3.setTag(this.a);
                a(i - PlayListOnLineMusicView.this.ai);
                this.c = true;
                return inflate3;
            } else {
                this.a = (ItemStruct) view.getTag();
                if (this.a == null) {
                    this.a = new ItemStruct();
                    View inflate4 = this.e.inflate((int) R.layout.playlist_online_music_row, viewGroup, false);
                    this.a.a = (TextView) inflate4.findViewById(R.id.playlist_online_song);
                    this.a.b = (TextView) inflate4.findViewById(R.id.playlist_online_singer);
                    this.a.c = (TextView) inflate4.findViewById(R.id.playlist_online_time);
                    this.a.d = (RatingBar) inflate4.findViewById(R.id.pop_value);
                    this.a.f = (ImageView) inflate4.findViewById(R.id.add_image);
                    this.a.e = (TextView) inflate4.findViewById(R.id.playlist_online_sttime);
                    this.b = (LinearLayout) inflate4.findViewById(R.id.add_layout);
                    inflate4.setTag(this.a);
                    view2 = inflate4;
                } else {
                    this.a.a = (TextView) view.findViewById(R.id.playlist_online_song);
                    this.a.b = (TextView) view.findViewById(R.id.playlist_online_singer);
                    this.a.c = (TextView) view.findViewById(R.id.playlist_online_time);
                    this.a.d = (RatingBar) view.findViewById(R.id.pop_value);
                    this.a.f = (ImageView) view.findViewById(R.id.add_image);
                    this.a.e = (TextView) view.findViewById(R.id.playlist_online_sttime);
                    this.b = (LinearLayout) view.findViewById(R.id.add_layout);
                    view2 = view;
                }
                a(i - PlayListOnLineMusicView.this.ai);
                return view2;
            }
        }
    }

    public class PlayModelTask extends AsyncTask {
        public PlayModelTask() {
        }

        /* access modifiers changed from: protected */
        public Object doInBackground(Object... objArr) {
            PlayListOnLineMusicView.this.y();
            return null;
        }
    }

    public PlayListOnLineMusicView(Activity activity) {
        super(activity);
        this.P = activity;
    }

    public static bj a(Context context, cx cxVar) {
        String str;
        String str2;
        String str3;
        String str4;
        if (cxVar == null) {
            return null;
        }
        int size = cxVar.j() != null ? cxVar.j().size() : 0;
        String str5 = "";
        String str6 = "";
        String str7 = "";
        String str8 = "";
        int i = 0;
        while (i < size) {
            cy cyVar = (cy) cxVar.j().get(i);
            if (cyVar == null) {
                str = str5;
                str2 = str6;
                str3 = str7;
                str4 = str8;
            } else if (cyVar.c.equals("plh") && str8.equals("")) {
                str2 = str6;
                str3 = str7;
                str4 = cyVar.b;
                str = str5;
            } else if (cyVar.c.equals("pl") && str6.equals("")) {
                str2 = cyVar.b;
                str3 = str7;
                str4 = str8;
                str = str5;
            } else if (!cyVar.c.equals("dl") || !str7.equals("")) {
                str = str5;
                str2 = str6;
                str3 = str7;
                str4 = str8;
            } else {
                String str9 = cyVar.b;
                str = cyVar.e;
                str3 = str9;
                str4 = str8;
                str2 = str6;
            }
            i++;
            str5 = str;
            str6 = str2;
            str7 = str3;
            str8 = str4;
        }
        String c = cxVar.c();
        bj bjVar = new bj(cxVar.f(), "", cxVar.a(), Integer.valueOf(cxVar.b()).intValue() * 1000, 0, ae.p, cxVar.e(), false, str6, str8, str7, "", "", "", "", Integer.valueOf(c.substring(c.length() - 1, c.length())).intValue() / 2, "", "", str5);
        if ("r".equals(cxVar.k())) {
            bjVar.a = "r";
        } else {
            bjVar.a = "m";
        }
        return bjVar;
    }

    public static bj a(Context context, cx cxVar, int i) {
        String str;
        String str2;
        String str3;
        String str4;
        if (cxVar == null) {
            return null;
        }
        int size = cxVar.j() != null ? cxVar.j().size() : 0;
        String str5 = "";
        String str6 = "";
        String str7 = "";
        String str8 = "";
        int i2 = 0;
        while (i2 < size) {
            cy cyVar = (cy) cxVar.j().get(i2);
            if (cyVar == null) {
                str = str5;
                str2 = str6;
                str3 = str7;
                str4 = str8;
            } else if (cyVar.c.equals("plh") && str8.equals("")) {
                str2 = str6;
                str3 = str7;
                str4 = cyVar.b;
                str = str5;
            } else if (cyVar.c.equals("pl") && str6.equals("")) {
                str2 = cyVar.b;
                str3 = str7;
                str4 = str8;
                str = str5;
            } else if (!cyVar.c.equals("dl") || !str7.equals("")) {
                str = str5;
                str2 = str6;
                str3 = str7;
                str4 = str8;
            } else {
                String str9 = cyVar.b;
                str = cyVar.e;
                str3 = str9;
                str4 = str8;
                str2 = str6;
            }
            i2++;
            str5 = str;
            str6 = str2;
            str7 = str3;
            str8 = str4;
        }
        String c = cxVar.c();
        bj bjVar = new bj(cxVar.f(), "", cxVar.a(), Integer.valueOf(cxVar.b()).intValue() * 1000, 0, ae.p, cxVar.e(), false, str6, str8, str7, "", "", "", "", Integer.valueOf(c.substring(c.length() - 1, c.length())).intValue() / 2, "", "", str5);
        if ("r".equals(cxVar.k())) {
            bjVar.a = "r";
        } else {
            bjVar.a = "m";
        }
        return bjVar;
    }

    private void t() {
        if (!en.c(((cv) this.G.obj).c())) {
            this.ai = 2;
        } else {
            this.ai = 1;
        }
        this.ah = ar.a(this.P);
        this.B.setImageResource(R.drawable.list_return_icon);
        this.B.setVisibility(0);
        this.H.setVisibility(8);
        this.S.setText((int) R.string.playlist_play_all);
        this.T.setImageResource(R.drawable.list_playall_icon);
        this.y.setText(((cw) this.G.obj).f());
        this.z.setMaxLines(2);
        this.z.setText(((cv) this.G.obj).c());
        this.C.setImageResource(R.drawable.handle_down);
        this.aj = hx.a();
        this.aj.a("duomi", (String) null);
        x = 0;
        try {
            ae = ((ct) this.aj.a(en.k(((cw) this.G.obj).e())).b()).b();
            this.Q = ((ct) this.aj.a(en.k(((cw) this.G.obj).e())).b()).c();
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.R = new ArrayList();
        String str = ae.q + "/" + en.j(((cv) this.G.obj).b());
        if (new File(str).exists()) {
            this.A.setImageBitmap(BitmapFactory.decodeFile(str));
        } else {
            this.A.setImageResource(R.drawable.home_find);
        }
        if (this.Q != null) {
            for (int i = 0; i < this.Q.size(); i++) {
                this.R.add(0);
            }
        }
        this.I = new MyAdapter(g(), this.Q, this.R);
        this.J.setAdapter((ListAdapter) this.I);
        this.J.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView adapterView, View view, int i, long j) {
                if (i < PlayListOnLineMusicView.this.ai) {
                    return false;
                }
                view.getLocationInWindow(PlayListOnLineMusicView.ag);
                int unused = PlayListOnLineMusicView.this.K = i - PlayListOnLineMusicView.this.ai;
                PlayListOnLineMusicView.this.v();
                return false;
            }
        });
        this.D.setOnClickListener(this);
        this.J.setOnItemClickListener(this.an);
    }

    private void u() {
        this.L = LayoutInflater.from(this.P).inflate((int) R.layout.popwindow, (ViewGroup) null);
        this.O = new Dialog(g());
        Window window = this.O.getWindow();
        WindowManager.LayoutParams attributes = window.getAttributes();
        ad.b("type", attributes.type + "");
        window.setBackgroundDrawableResource(R.drawable.none);
        ad.b("flag", attributes.flags + "");
        window.clearFlags(2);
        this.O.setCanceledOnTouchOutside(true);
        this.O.setContentView(this.L);
        this.L.setFocusable(true);
        this.U = (RelativeLayout) this.L.findViewById(R.id.command1);
        this.V = (RelativeLayout) this.L.findViewById(R.id.command2);
        this.W = (RelativeLayout) this.L.findViewById(R.id.command3);
        this.X = (RelativeLayout) this.L.findViewById(R.id.command4);
        this.Y = (RelativeLayout) this.L.findViewById(R.id.command5);
        ImageView imageView = (ImageView) this.L.findViewById(R.id.image4);
        ImageView imageView2 = (ImageView) this.L.findViewById(R.id.image5);
        TextView textView = (TextView) this.L.findViewById(R.id.text1);
        TextView textView2 = (TextView) this.L.findViewById(R.id.text2);
        TextView textView3 = (TextView) this.L.findViewById(R.id.text4);
        TextView textView4 = (TextView) this.L.findViewById(R.id.text5);
        ((ImageView) this.L.findViewById(R.id.image1)).setImageResource(R.drawable.list_like_icon);
        ((ImageView) this.L.findViewById(R.id.image2)).setImageResource(R.drawable.list_popupicon_add);
        ((ImageView) this.L.findViewById(R.id.image3)).setImageResource(R.drawable.list_popupicon_play);
        if (kf.b) {
            imageView2.setImageResource(R.drawable.list_popupicon_share);
        } else {
            imageView2.setImageResource(R.drawable.list_popupicon_share1);
        }
        textView.setText((int) R.string.playlist_online_like);
        textView2.setText((int) R.string.playlist_popup_add_to);
        if (!as.a(this.P).ac()) {
            textView3.setText((int) R.string.playlist_popup_song_info);
            imageView.setImageResource(R.drawable.list_popupicon_detail);
        } else {
            textView3.setText((int) R.string.playlist_online_download);
            imageView.setImageResource(R.drawable.list_popupicon_download);
        }
        textView4.setText((int) R.string.playlist_popup_share);
        this.U.setOnClickListener(this);
        this.V.setOnClickListener(this);
        this.W.setOnClickListener(this);
        this.X.setOnClickListener(this);
        this.Y.setOnClickListener(this);
    }

    /* access modifiers changed from: private */
    public void v() {
        Window window = this.O.getWindow();
        WindowManager.LayoutParams attributes = window.getAttributes();
        attributes.y = en.b(this.P, ag[1]);
        window.setAttributes(attributes);
        this.L.setFocusable(true);
        this.L.requestFocus();
        this.O.show();
        this.M = true;
    }

    private void w() {
        this.M = false;
        this.O.dismiss();
    }

    /* access modifiers changed from: private */
    public void x() {
        if (E > 0) {
            this.z.setMaxLines(Integer.MAX_VALUE);
            E *= -1;
            this.C.setImageResource(R.drawable.handle_up);
            return;
        }
        this.z.setMaxLines(2);
        E *= -1;
        this.C.setImageResource(R.drawable.handle_down);
    }

    /* access modifiers changed from: private */
    public void y() {
        bj a;
        try {
            ArrayList arrayList = new ArrayList();
            int size = this.Q.size();
            for (int i = 0; i < size; i++) {
                cx cxVar = (cx) this.Q.get(i);
                if (!(cxVar == null || (a = a(getContext(), cxVar)) == null)) {
                    arrayList.add(a);
                }
            }
            if (arrayList.size() > 0) {
                gx.a(getContext(), arrayList, ((cw) this.Q.get(0)).g(), ((cv) this.G.obj).f());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void z() {
        try {
            ((PlayListGroupView) this.d).r();
            if (((PlayListGroupView) this.d).t()) {
                this.d.a(PlayListOnLineView.class, (Message) null);
            } else {
                this.d.a(PlayListOnLineSecondView.class, (Message) null);
            }
            co.a().b();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void a(Context context, bj bjVar) {
        String d = en.d(bjVar.k());
        ax a = ai.a(context).a(bjVar.g());
        StringBuffer append = new StringBuffer().append("");
        if (a == null) {
            append.append(bjVar.a());
        } else {
            append.append(a.e());
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(context.getString(R.string.playlist_song_name)).append(bjVar.h()).append("\n");
        stringBuffer.append(context.getString(R.string.playlist_song_singer)).append(en.a(context, bjVar.j())).append("\n");
        stringBuffer.append(context.getString(R.string.playlist_song_album)).append(en.a(context, append.toString())).append("\n");
        stringBuffer.append(context.getString(R.string.playlist_song_duration)).append(d).append("\n");
        if (bjVar.b() > 0) {
            stringBuffer.append(context.getString(R.string.playlist_song_path)).append(bjVar.e()).append("\n");
            stringBuffer.append(context.getString(R.string.playlist_song_size)).append(en.c(bjVar.l())).append("\n");
            String a2 = en.a(context, bjVar.c());
            if (!a2.equals(context.getString(R.string.unknown))) {
                a2 = a2 + "kbps";
            }
            stringBuffer.append(context.getString(R.string.playlist_song_kbps)).append(a2);
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        AlertDialog create = builder.create();
        builder.setNegativeButton((int) R.string.app_return, (DialogInterface.OnClickListener) null);
        create.setCanceledOnTouchOutside(true);
        builder.setTitle((int) R.string.playlist_songinfo);
        builder.setMessage(stringBuffer.toString());
        builder.show();
    }

    public void a(Message message) {
        this.G = message;
        t();
        super.a(message);
    }

    public boolean a(int i, KeyEvent keyEvent) {
        b(i, keyEvent);
        if (i == 4 && this.f.getVisibility() == 0 && !this.u) {
            if (this.g.getVisibility() == 0) {
                this.g.setVisibility(8);
                this.i.requestFocus();
            } else {
                this.f.setVisibility(8);
            }
            return true;
        } else if (i != 4 || this.f.getVisibility() != 8 || this.u) {
            return false;
        } else {
            z();
            return true;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public void f() {
        Window window = g().getWindow();
        if (window != null) {
            window.clearFlags(1024);
        }
        inflate(g(), R.layout.playlist_online_music, this);
        this.Z = LayoutInflater.from(this.P).inflate((int) R.layout.playlist_online_music_head, (ViewGroup) null, false);
        this.aa = LayoutInflater.from(this.P).inflate((int) R.layout.playlist_play_mode, (ViewGroup) null, false);
        this.y = (TextView) findViewById(R.id.list_title);
        this.z = (TextView) this.Z.findViewById(R.id.list_discription);
        this.J = (ListView) findViewById(R.id.music_list);
        this.S = (TextView) this.aa.findViewById(R.id.play_modle);
        this.T = (ImageView) this.aa.findViewById(R.id.play_modle_image);
        this.B = (ImageView) findViewById(R.id.go_back);
        this.D = (RelativeLayout) findViewById(R.id.title_panel);
        this.H = (Button) findViewById(R.id.command);
        this.F = (FrameLayout) this.Z.findViewById(R.id.discription_table);
        this.C = (ImageView) this.Z.findViewById(R.id.discription_more);
        this.A = (ImageView) this.Z.findViewById(R.id.list_image);
        this.ab = (Button) findViewById(R.id.playlist_retry);
        this.ac = (LinearLayout) findViewById(R.id.playlist_progress_layout);
        this.ad = (RelativeLayout) findViewById(R.id.playlist_failed_layout);
        this.J.setOnScrollListener(this.ak);
        this.aq = findViewById(R.id.scanning_tip);
        u();
        h();
    }

    public void n() {
        this.i.setOnItemClickListener(this.ao);
        this.j.setOnItemClickListener(this.ap);
    }

    public void o() {
        if (this.a) {
            this.q = getResources().getStringArray(R.array.PlayListOnLine_menu_without_download);
            this.s = new ArrayList();
            this.s.add(Integer.valueOf((int) R.drawable.meun_scan));
            this.s.add(Integer.valueOf((int) R.drawable.menu_bg));
        } else {
            this.q = getResources().getStringArray(R.array.PlayListOnLine_menu);
            this.s = new ArrayList();
            this.s.add(Integer.valueOf((int) R.drawable.meun_downloadsetup));
            this.s.add(Integer.valueOf((int) R.drawable.meun_scan));
        }
        this.s.add(Integer.valueOf((int) R.drawable.menu_recommend));
        this.s.add(Integer.valueOf((int) R.drawable.meun_more));
        this.s.add(Integer.valueOf((int) R.drawable.meun_setup));
        this.s.add(Integer.valueOf((int) R.drawable.meun_sleep));
        this.s.add(Integer.valueOf((int) R.drawable.meun_syc));
        this.s.add(Integer.valueOf((int) R.drawable.meun_exit));
        this.r = getResources().getStringArray(R.array.second_menu2);
        this.t = new ArrayList();
        this.t.add(Integer.valueOf((int) R.drawable.meun_help));
        this.t.add(Integer.valueOf((int) R.drawable.meun_advice));
        this.t.add(Integer.valueOf((int) R.drawable.meun_friends));
        this.t.add(Integer.valueOf((int) R.drawable.meun_about));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: bn.a(android.content.Context, boolean):void
     arg types: [android.content.Context, int]
     candidates:
      bn.a(int, android.content.Context):int
      bn.a(android.content.Context, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: gx.a(android.content.Context, bj, bl, com.duomi.app.ui.MultiView$OnLikeReSetListener, boolean):void
     arg types: [android.content.Context, bj, bl, com.duomi.app.ui.MultiView$OnLikeReSetListener, int]
     candidates:
      gx.a(android.content.Context, bj, int, android.os.Handler, com.duomi.app.ui.MultiView$OnLikeReSetListener):void
      gx.a(android.content.Context, bl, long, com.duomi.app.ui.MultiView$OnLikeReSetListener, int):void
      gx.a(android.content.Context, bj, bl, com.duomi.app.ui.MultiView$OnLikeReSetListener, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ev.a(android.content.Context, bj, boolean, java.lang.String):void
     arg types: [android.content.Context, bj, int, java.lang.String]
     candidates:
      ev.a(java.lang.String, java.io.File, android.os.Handler, android.content.Context):boolean
      ev.a(android.content.Context, bj, boolean, java.lang.String):void */
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.title_panel:
                z();
                break;
            case R.id.command1:
                final bj a = a(getContext(), (cx) this.Q.get(this.K));
                final bl b = this.ah.b(bn.a().h(), 2);
                if (!bn.a().b(getContext()) && kf.b && kf.a(this.P, 1) && !as.a(this.P).U()) {
                    AlertDialog create = new AlertDialog.Builder(getContext()).setPositiveButton(getContext().getString(R.string.app_confirm), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            as.a(PlayListOnLineMusicView.this.getContext()).u(true);
                        }
                    }).setNegativeButton(getContext().getString(R.string.app_cancel), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    }).create();
                    create.setTitle(getContext().getString(R.string.first_collection_title));
                    create.setMessage(getContext().getString(R.string.first_collection_message));
                    create.show();
                    create.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                         method: gx.a(android.content.Context, bj, bl, com.duomi.app.ui.MultiView$OnLikeReSetListener, boolean):void
                         arg types: [android.content.Context, bj, bl, com.duomi.app.ui.MultiView$OnLikeReSetListener, int]
                         candidates:
                          gx.a(android.content.Context, bj, int, android.os.Handler, com.duomi.app.ui.MultiView$OnLikeReSetListener):void
                          gx.a(android.content.Context, bl, long, com.duomi.app.ui.MultiView$OnLikeReSetListener, int):void
                          gx.a(android.content.Context, bj, bl, com.duomi.app.ui.MultiView$OnLikeReSetListener, boolean):void */
                        public void onDismiss(DialogInterface dialogInterface) {
                            try {
                                gx.a(PlayListOnLineMusicView.this.P, a, b, PlayerLayoutView.H, false);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    bn.a().a(getContext(), true);
                    break;
                } else {
                    try {
                        gx.a(this.P, a, b, PlayerLayoutView.H, false);
                        break;
                    } catch (Exception e) {
                        e.printStackTrace();
                        break;
                    }
                }
            case R.id.command2:
                try {
                    gx.a(this.P, a(getContext(), (cx) this.Q.get(this.K)), -1, this.am, PlayerLayoutView.H);
                    break;
                } catch (Exception e2) {
                    e2.printStackTrace();
                    break;
                }
            case R.id.command3:
                try {
                    gx.a(getContext(), a(getContext(), (cx) this.Q.get(this.K)));
                    break;
                } catch (Exception e3) {
                    e3.printStackTrace();
                    break;
                }
            case R.id.command4:
                if (!as.a(this.P).ac()) {
                    if (this.K >= 0 && this.Q.get(this.K) != null) {
                        a(this.P, a(getContext(), (cx) this.Q.get(this.K)));
                        break;
                    } else {
                        return;
                    }
                } else {
                    try {
                        if (this.K >= 0 && this.Q.get(this.K) != null) {
                            cx cxVar = (cx) this.Q.get(this.K);
                            ev.a(getContext(), a(getContext(), cxVar), false, cxVar.k());
                            break;
                        } else {
                            return;
                        }
                    } catch (Exception e4) {
                        e4.printStackTrace();
                        break;
                    }
                }
                break;
            case R.id.command5:
                if (il.b(this.P)) {
                    bj a2 = a(getContext(), (cx) this.Q.get(this.K));
                    if (!kf.b) {
                        en.a(getContext(), a2);
                        break;
                    } else {
                        new gd(g(), a2);
                        break;
                    }
                } else {
                    jh.a(this.P, (int) R.string.app_net_error);
                    break;
                }
        }
        w();
    }
}
