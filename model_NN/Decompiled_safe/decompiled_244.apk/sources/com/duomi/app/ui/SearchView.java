package com.duomi.app.ui;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.duomi.advertisement.AdvertisementList;
import com.duomi.android.R;
import com.duomi.core.view.CloundView;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class SearchView extends c {
    public static String A = "";
    public static boolean B = false;
    static String D = "";
    public static String y = "";
    SearchHotKeyTask C = null;
    public AdapterView.OnItemClickListener E = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView adapterView, View view, int i, long j) {
            if (i == 3) {
                if (SearchView.this.g.getVisibility() == 0) {
                    SearchView.this.j();
                } else {
                    SearchView.this.i();
                }
            } else if (SearchView.this.a) {
                boolean unused = SearchView.this.a(i, SearchView.this.a);
                switch (i) {
                    case 0:
                        new PopDialogView(SearchView.this.getContext()).c(SearchView.this.d);
                        return;
                    default:
                        return;
                }
            } else {
                boolean unused2 = SearchView.this.a(i);
                switch (i) {
                    case 1:
                        new PopDialogView(SearchView.this.getContext()).c(SearchView.this.d);
                        return;
                    default:
                        return;
                }
            }
        }
    };
    private CloundView F;
    /* access modifiers changed from: private */
    public String G = "SearchView";
    /* access modifiers changed from: private */
    public EditText H;
    /* access modifiers changed from: private */
    public ListView I;
    private Button J;
    /* access modifiers changed from: private */
    public Context K;
    /* access modifiers changed from: private */
    public am L;
    /* access modifiers changed from: private */
    public Handler M = new Handler() {
        public void handleMessage(Message message) {
            switch (message.what) {
                case 254:
                    PopDialogView.a(SearchView.this.getContext());
                    break;
            }
            super.handleMessage(message);
        }
    };
    private je N = new je() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
         arg types: [java.lang.String, int]
         candidates:
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
        public void a(String str) {
            ((InputMethodManager) SearchView.this.K.getSystemService("input_method")).hideSoftInputFromWindow(SearchView.this.H.getWindowToken(), 0);
            if (!il.b(SearchView.this.getContext())) {
                jh.a(SearchView.this.getContext(), (int) R.string.app_net_error);
                return;
            }
            SearchView.y = en.e(str);
            SearchView.this.z = "s";
            if (!SearchView.this.L.a(bn.a().h(), en.l(SearchView.y), "1")) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(AdvertisementList.EXTRA_UID, bn.a().h());
                contentValues.put("type", (Integer) 1);
                contentValues.put("words", en.l(SearchView.y));
                contentValues.put("date", Long.valueOf(System.currentTimeMillis()));
                SearchView.this.L.a(contentValues);
            }
            try {
                new SearchMusicTask().execute(new Object[0]);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
    private AdapterView.OnItemClickListener O = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView adapterView, View view, int i, long j) {
            SearchView.this.f.setVisibility(8);
            boolean unused = SearchView.this.b(i);
        }
    };
    ArrayAdapter x = null;
    String z = "u";

    class SearchHotKeyTask extends AsyncTask {
        SearchHotKeyTask() {
        }

        /* access modifiers changed from: protected */
        public Object doInBackground(Object... objArr) {
            String str;
            if (!il.b(SearchView.this.getContext())) {
                return null;
            }
            try {
                str = cq.a(SearchView.this.getContext(), bn.a().h(), URLEncoder.encode(SearchView.this.H.getText().toString().trim(), "utf-8"), 15, 10);
            } catch (Exception e) {
                e.printStackTrace();
                str = null;
            }
            ad.b(SearchView.this.G, "back>>>>" + str);
            return str;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Object obj) {
            List g;
            if (il.b(SearchView.this.getContext()) && obj != null && (g = cp.g(SearchView.this.getContext(), obj.toString())) != null && g.size() > 0) {
                g.add(0, SearchView.this.getContext().getResources().getString(R.string.search_hot_key));
                SearchView.this.I.setAdapter((ListAdapter) new ArrayAdapter(SearchView.this.K, (int) R.xml.auto_complete_text, g));
                SearchView.this.I.setVisibility(0);
            }
        }
    }

    class SearchListener implements View.OnClickListener {
        SearchListener() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
         arg types: [java.lang.String, int]
         candidates:
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
        public void onClick(View view) {
            ((InputMethodManager) SearchView.this.K.getSystemService("input_method")).hideSoftInputFromWindow(SearchView.this.H.getWindowToken(), 0);
            SearchView.y = en.e(SearchView.this.H.getText().toString());
            SearchView.this.z = "u";
            if (SearchView.y == null || SearchView.y.trim().equals("") || SearchView.this.H.getText().toString().trim().equals(SearchView.this.getContext().getResources().getString(R.string.search_init_text))) {
                jh.a(SearchView.this.getContext(), (int) R.string.search_in_keyword);
            } else if (!il.b(SearchView.this.getContext())) {
                jh.a(SearchView.this.getContext(), (int) R.string.app_net_error);
            } else {
                if (!SearchView.this.L.a(bn.a().h(), en.l(SearchView.y), "1")) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(AdvertisementList.EXTRA_UID, bn.a().h());
                    contentValues.put("type", (Integer) 1);
                    contentValues.put("words", en.l(SearchView.y));
                    contentValues.put("date", Long.valueOf(System.currentTimeMillis()));
                    SearchView.this.L.a(contentValues);
                }
                try {
                    new SearchMusicTask().execute(new Object[0]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    class SearchMusicTask extends AsyncTask {
        private SearchMusicTask() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public String doInBackground(Object... objArr) {
            String str;
            if (SearchView.A.equals(en.l(SearchView.y))) {
                return null;
            }
            String str2 = bh.a().c().size() <= 0 ? "1" : "0";
            try {
                str = cq.a(SearchView.this.K, SearchView.this.z, SearchView.y, str2, 0, 100, SearchView.this.M);
                try {
                    ad.b(SearchView.this.G, "back>>>>" + str);
                    cp.d(SearchView.this.K, str);
                    if (str2 == "1") {
                        StringBuffer stringBuffer = new StringBuffer();
                        int size = bh.a().c().size() > 12 ? 12 : bh.a().c().size();
                        for (int i = 0; i < size; i++) {
                            stringBuffer.append(((bc) bh.a().c().get(i)).a() + "|");
                        }
                        SearchView.this.L.a(stringBuffer.toString().substring(0, stringBuffer.toString().length() - 1));
                    }
                    return str;
                } catch (Exception e) {
                    e = e;
                    e.printStackTrace();
                    return str;
                }
            } catch (Exception e2) {
                e = e2;
                str = "";
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onPostExecute(String str) {
            as a2 = as.a(SearchView.this.K);
            if (!en.c(str) || SearchView.A.equals(en.l(SearchView.y))) {
                SearchView.this.d.a(SearchResultView.class, (Message) null);
                SearchView.A = en.l(SearchView.y);
                return;
            }
            if (il.a(SearchView.this.K) == 0) {
                jh.a(SearchView.this.getContext(), (int) R.string.search_no_result);
            } else if (a2.N()) {
                jh.a(SearchView.this.getContext(), (int) R.string.app_net_error);
            } else {
                jh.a(SearchView.this.getContext(), (int) R.string.search_no_result);
            }
            SearchView.this.d.a(SearchView.class, (Message) null);
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            if (!SearchView.A.equals(en.l(SearchView.y))) {
                SearchView.this.d.a(DialogView.class, (Message) null);
            }
        }
    }

    public SearchView(Activity activity) {
        super(activity);
        this.K = activity;
    }

    public void a(Message message) {
        if (message != null) {
            r();
        }
    }

    public boolean a(int i, KeyEvent keyEvent) {
        b(i, keyEvent);
        if (i == 4 && this.f.getVisibility() == 0 && !this.u) {
            if (this.g.getVisibility() == 0) {
                this.g.setVisibility(8);
                this.i.requestFocus();
            } else {
                this.f.setVisibility(8);
            }
            return true;
        } else if (i != 4 || this.I.getVisibility() != 0) {
            return false;
        } else {
            q();
            this.I.setVisibility(8);
            return true;
        }
    }

    public void f() {
        this.L = am.a(getContext());
        Window window = g().getWindow();
        if (window != null) {
            window.clearFlags(1024);
        }
        inflate(g(), R.layout.music_search, this);
        this.H = (EditText) findViewById(R.id.music_search_myEditText);
        this.H.setHint((int) R.string.search_init_text);
        this.H.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View view, boolean z) {
                if (!z) {
                    ((InputMethodManager) SearchView.this.K.getSystemService("input_method")).hideSoftInputFromWindow(SearchView.this.H.getWindowToken(), 0);
                } else if (!en.l(SearchView.y).equals(SearchView.this.getContext().getResources().getString(R.string.search_hot_key))) {
                    SearchView.this.H.setText(en.l(SearchView.y).trim());
                    try {
                        SearchView.this.H.setSelection(en.l(SearchView.y).length());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        this.J = (Button) findViewById(R.id.music_search_myButton);
        this.J.setOnClickListener(new SearchListener());
        this.F = (CloundView) findViewById(R.id.music_search_cloundview);
        this.F.a(this.N);
        this.H.setTextColor(-16777216);
        this.I = (ListView) findViewById(R.id.promptListView);
        if (this.L.b() == null) {
            this.F.a(getContext().getResources().getStringArray(R.array.search_hot_list));
        } else {
            this.F.a(this.L.b().split("\\|"));
        }
        h();
        r();
    }

    public void n() {
        this.i.setOnItemClickListener(this.E);
        this.j.setOnItemClickListener(this.O);
    }

    public void o() {
        if (this.a) {
            this.q = getResources().getStringArray(R.array.Search_menu_without_download);
            this.s = new ArrayList();
            this.s.add(Integer.valueOf((int) R.drawable.meun_searchhistory));
            this.s.add(Integer.valueOf((int) R.drawable.menu_bg));
        } else {
            this.q = getResources().getStringArray(R.array.Search_menu);
            this.s = new ArrayList();
            this.s.add(Integer.valueOf((int) R.drawable.meun_downloadsetup));
            this.s.add(Integer.valueOf((int) R.drawable.meun_searchhistory));
        }
        this.s.add(Integer.valueOf((int) R.drawable.menu_bg));
        this.s.add(Integer.valueOf((int) R.drawable.meun_more));
        this.s.add(Integer.valueOf((int) R.drawable.meun_setup));
        this.s.add(Integer.valueOf((int) R.drawable.meun_sleep));
        this.s.add(Integer.valueOf((int) R.drawable.meun_syc));
        this.s.add(Integer.valueOf((int) R.drawable.meun_exit));
        this.r = getResources().getStringArray(R.array.second_menu2);
        this.t = new ArrayList();
        this.t.add(Integer.valueOf((int) R.drawable.meun_help));
        this.t.add(Integer.valueOf((int) R.drawable.meun_advice));
        this.t.add(Integer.valueOf((int) R.drawable.meun_friends));
        this.t.add(Integer.valueOf((int) R.drawable.meun_about));
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        return motionEvent.getAction() == 0;
    }

    public void q() {
        if (this.C != null) {
            this.C.cancel(true);
            this.C = null;
        }
        if (this.I != null && this.I.isShown()) {
            this.I.setVisibility(8);
        }
    }

    public void r() {
        int i = 12;
        if (bh.a().c().size() > 0) {
            ArrayList arrayList = new ArrayList();
            if (bh.a().c().size() <= 12) {
                i = bh.a().c().size();
            }
            for (int i2 = 0; i2 < i; i2++) {
                String a = ((bc) bh.a().c().get(i2)).a();
                if (!en.c(a) && !arrayList.contains(a)) {
                    arrayList.add(a);
                }
            }
            this.F.a(arrayList);
        }
        this.H.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable editable) {
                ad.b(SearchView.this.G, "afterTextChanged");
            }

            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                ad.b(SearchView.this.G, "beforeTextChanged");
            }

            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                if (SearchView.this.H.getText().toString() == null || SearchView.this.H.getText().toString().trim().equals("") || SearchView.this.H.getText().toString().trim().equals(SearchView.this.K.getResources().getString(R.string.search_init_text))) {
                    SearchView.this.q();
                } else if (SearchView.this.H.getText().toString().trim().equals(SearchView.A)) {
                    SearchView.this.q();
                } else {
                    try {
                        SearchView.this.q();
                        SearchView.this.C = new SearchHotKeyTask();
                        SearchView.this.C.execute(new Object[0]);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        this.I.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView adapterView, View view, int i, long j) {
                SearchView.D = SearchView.this.H.getText().toString();
            }

            public void onNothingSelected(AdapterView adapterView) {
            }
        });
        this.I.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
             arg types: [java.lang.String, int]
             candidates:
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
            public void onItemClick(AdapterView adapterView, View view, int i, long j) {
                ((InputMethodManager) SearchView.this.K.getSystemService("input_method")).hideSoftInputFromWindow(SearchView.this.H.getWindowToken(), 0);
                if (!il.b(SearchView.this.getContext())) {
                    jh.a(SearchView.this.getContext(), (int) R.string.app_net_error);
                    return;
                }
                SearchView.this.q();
                SearchView.this.I.setVisibility(8);
                SearchView.y = en.e(((TextView) view).getText().toString()).trim();
                if (SearchView.this.getContext().getResources().getString(R.string.search_hot_key).trim().equals(((TextView) view).getText().toString().trim())) {
                    SearchView.this.H.setText(SearchView.D.trim());
                    try {
                        SearchView.this.H.setSelection(SearchView.D.length());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    SearchView.this.z = "c";
                    if (!SearchView.this.L.a(bn.a().h(), en.l(SearchView.y), "1")) {
                        ContentValues contentValues = new ContentValues();
                        contentValues.put(AdvertisementList.EXTRA_UID, bn.a().h());
                        contentValues.put("type", (Integer) 1);
                        contentValues.put("words", en.l(SearchView.y));
                        contentValues.put("date", Long.valueOf(System.currentTimeMillis()));
                        SearchView.this.L.a(contentValues);
                    }
                    new SearchMusicTask().execute(new Object[0]);
                }
            }
        });
    }
}
