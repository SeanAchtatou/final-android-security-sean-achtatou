package com.duomi.app.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class DMGallery extends ji {
    public DMGallery(Context context) {
        super(context);
    }

    public DMGallery(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, 16842864);
    }

    public DMGallery(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    /* access modifiers changed from: protected */
    public boolean a(MotionEvent motionEvent) {
        boolean z = false;
        int a = a();
        ad.b("DMGallery", "isIngoreScroll():currentViewId:" + a + " currentCount:" + getChildCount());
        View childAt = getChildAt(a);
        if (childAt instanceof jt) {
            ad.b("DMGallery", "isIngoreScroll()1:currentViewId:");
            z = ((jt) childAt).a(motionEvent);
        }
        ad.b("DMGallery", "is Igore>>" + z);
        return z;
    }
}
