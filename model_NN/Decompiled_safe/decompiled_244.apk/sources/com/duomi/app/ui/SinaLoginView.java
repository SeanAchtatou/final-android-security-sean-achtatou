package com.duomi.app.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.duomi.android.R;

public class SinaLoginView extends c {
    public static boolean x = false;
    /* access modifiers changed from: private */
    public EditText A;
    /* access modifiers changed from: private */
    public EditText B;
    private Button C;
    private Button D;
    private LinearLayout E;
    private RelativeLayout F;
    private RelativeLayout G;
    /* access modifiers changed from: private */
    public boolean H;
    /* access modifiers changed from: private */
    public Bundle I;
    public ProgressDialog y;
    Handler z;

    class SinaLoginTask extends AsyncTask {
        private SinaLoginTask() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.duomi.app.ui.SinaLoginView.a(com.duomi.app.ui.SinaLoginView, boolean):boolean
         arg types: [com.duomi.app.ui.SinaLoginView, int]
         candidates:
          c.a(java.lang.Class, android.os.Message):void
          c.a(boolean, android.os.Message):void
          c.a(int, android.view.KeyEvent):boolean
          c.a(int, boolean):boolean
          com.duomi.app.ui.SinaLoginView.a(com.duomi.app.ui.SinaLoginView, boolean):boolean */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kf.a(android.content.Context, java.lang.String, java.lang.String, boolean):int
         arg types: [android.app.Activity, java.lang.String, java.lang.String, int]
         candidates:
          kf.a(android.content.Context, java.lang.String, java.lang.String, java.lang.String):int
          kf.a(android.content.Context, java.lang.String, java.lang.String, boolean):int */
        /* access modifiers changed from: protected */
        /* renamed from: a */
        public Integer doInBackground(Object... objArr) {
            String trim = objArr[0].toString().trim();
            String trim2 = objArr[1].toString().trim();
            if (!il.b(SinaLoginView.this.getContext())) {
                Message obtainMessage = SinaLoginView.this.z.obtainMessage(-1);
                obtainMessage.arg2 = R.string.app_net_error;
                SinaLoginView.this.z.removeMessages(-1);
                SinaLoginView.this.z.sendMessageDelayed(obtainMessage, 500);
                return -100;
            }
            boolean unused = SinaLoginView.this.H = true;
            for (int i = 0; i < 1 && cq.a(SinaLoginView.this.getContext(), 7) == -1; i++) {
                boolean unused2 = SinaLoginView.this.H = false;
            }
            if (!SinaLoginView.this.H) {
                Message obtainMessage2 = SinaLoginView.this.z.obtainMessage(-1);
                obtainMessage2.arg2 = R.string.app_net_error;
                SinaLoginView.this.z.removeMessages(-1);
                SinaLoginView.this.z.sendMessageDelayed(obtainMessage2, 500);
                Log.i("SinaLoginView", "net 2");
                return -100;
            }
            Log.i("SinaLoginView", "REG START");
            try {
                Thread.sleep(20);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            int a2 = kf.a((Context) SinaLoginView.this.g(), trim, trim2, false);
            if (a2 == 0) {
                p a3 = p.a(SinaLoginView.this.g());
                a3.e();
                String string = SinaLoginView.this.I.getString("gotoWhere");
                if (en.c(string)) {
                    string = MultiView.class.getName();
                } else if ("charge".equals(string)) {
                    SinaLoginView.this.getContext().sendBroadcast(new Intent("com.duomi.charge_login_success"));
                }
                a3.a(string);
            }
            return Integer.valueOf(a2);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onPostExecute(Integer num) {
            if (num.intValue() == -100) {
                Log.v("SinaLoginView", "net  error ");
                return;
            }
            Message obtainMessage = SinaLoginView.this.z.obtainMessage(1);
            obtainMessage.arg1 = num.intValue();
            SinaLoginView.this.z.sendMessage(obtainMessage);
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            SinaLoginView.this.y = new ProgressDialog(SinaLoginView.this.getContext());
            SinaLoginView.this.y.setTitle((int) R.string.lg_title);
            SinaLoginView.this.y.setMessage("" + SinaLoginView.this.getContext().getString(R.string.lg_tips) + "    ");
            SinaLoginView.this.y.show();
        }

        /* access modifiers changed from: protected */
        public void onProgressUpdate(Object... objArr) {
        }
    }

    public SinaLoginView(Activity activity) {
        super(activity);
    }

    /* access modifiers changed from: private */
    public void r() {
        InputMethodManager inputMethodManager = (InputMethodManager) g().getSystemService("input_method");
        inputMethodManager.hideSoftInputFromWindow(this.A.getWindowToken(), 0);
        inputMethodManager.hideSoftInputFromWindow(this.B.getWindowToken(), 0);
    }

    public void a(Bundle bundle) {
        this.I = bundle;
    }

    /* access modifiers changed from: protected */
    public void f() {
        inflate(g(), R.layout.sina_login, this);
        this.A = (EditText) findViewById(R.id.sina_login_user_name);
        this.B = (EditText) findViewById(R.id.sina_login_user_passwd);
        this.C = (Button) findViewById(R.id.sina_login_btn);
        this.D = (Button) findViewById(R.id.sina_cancle_btn);
        this.E = (LinearLayout) findViewById(R.id.sina_login_name_layout);
        this.F = (RelativeLayout) findViewById(R.id.sina_login_edit_passwd);
        this.G = (RelativeLayout) findViewById(R.id.sina_login_edit_userName);
        this.z = new Handler() {
            public void handleMessage(Message message) {
                switch (message.what) {
                    case -1:
                        jh.a(SinaLoginView.this.g(), message.arg2);
                        break;
                    case 0:
                        jh.a(SinaLoginView.this.g(), (String) message.obj);
                        break;
                    case 1:
                        switch (message.arg1) {
                            case -3:
                                if (SinaLoginView.this.y != null) {
                                    SinaLoginView.this.y.dismiss();
                                }
                                Message obtainMessage = SinaLoginView.this.z.obtainMessage(-1);
                                obtainMessage.arg2 = R.string.sina_username_pwd_error_prompt;
                                SinaLoginView.this.z.removeMessages(-1);
                                SinaLoginView.this.z.sendMessageDelayed(obtainMessage, 500);
                                break;
                            case -2:
                            case -1:
                            default:
                                Message obtainMessage2 = SinaLoginView.this.z.obtainMessage(-1);
                                obtainMessage2.arg2 = R.string.weibo_common_error_prompt;
                                SinaLoginView.this.z.removeMessages(-1);
                                SinaLoginView.this.z.sendMessageDelayed(obtainMessage2, 500);
                                if (ae.g) {
                                }
                                break;
                            case 0:
                                SinaLoginView.this.getContext().sendBroadcast(new Intent("com.duomi.music_reloadlist"));
                                as.a(SinaLoginView.this.getContext()).l();
                                break;
                        }
                }
                if (SinaLoginView.this.y != null) {
                    SinaLoginView.this.y.dismiss();
                }
            }
        };
        q();
    }

    public void q() {
        String[] j = as.a(g()).j();
        if (!j[2].equals("1") || en.c(j[0])) {
            bo b = kf.b(1, g());
            if (b != null) {
                this.A.setText(b.c.trim());
                if (as.a(g()).i()) {
                    this.B.setText(b.d.trim());
                }
            }
        } else {
            this.A.setText(j[0].trim());
            if (as.a(g()).i()) {
                this.B.setText(j[1].trim());
            }
        }
        this.E.setBackgroundResource(R.drawable.sina_login_edit_layout_background);
        this.C.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SinaLoginView.this.r();
                if (en.c(SinaLoginView.this.A.getText().toString()) || en.c(SinaLoginView.this.B.getText().toString())) {
                    jh.a(SinaLoginView.this.getContext(), SinaLoginView.this.getContext().getString(R.string.lg_sina_null_err));
                    return;
                }
                new SinaLoginTask().execute(SinaLoginView.this.A.getText(), SinaLoginView.this.B.getText());
            }
        });
        this.D.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SinaLoginView.this.r();
                p.a(SinaLoginView.this.g()).e();
                if (SinaLoginView.this.I.getBoolean("isNotBackToLogin", true)) {
                    p.a(SinaLoginView.this.g()).a(LoginRegView.class.getName(), SinaLoginView.this.I);
                }
            }
        });
        this.F.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SinaLoginView.this.B.requestFocus();
            }
        });
        this.G.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SinaLoginView.this.A.requestFocus();
            }
        });
    }
}
