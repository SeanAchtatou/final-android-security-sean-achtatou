package com.duomi.app.ui;

import android.app.Activity;
import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Message;
import android.os.RemoteException;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.duomi.android.R;
import java.util.ArrayList;

public class PlayerCurrentListView extends c {
    /* access modifiers changed from: private */
    public static int H;
    private LinearLayout A;
    private ListQueryHandler B;
    /* access modifiers changed from: private */
    public MyAdapter C;
    private Message D;
    private int E = -1;
    private Context F;
    private String G = "";
    private AdapterView.OnItemClickListener I = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView adapterView, View view, int i, long j) {
            if (gx.d != null) {
                bj a2 = PlayerCurrentListView.this.C.a(i);
                be beVar = null;
                try {
                    beVar = gx.d.j();
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
                if (beVar != null) {
                    gx.a(PlayerCurrentListView.this.g(), i, a2);
                }
            }
        }
    };
    private AdapterView.OnItemClickListener J = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView adapterView, View view, int i, long j) {
            if (i == 3) {
                if (PlayerCurrentListView.this.g.getVisibility() == 0) {
                    PlayerCurrentListView.this.j();
                } else {
                    PlayerCurrentListView.this.i();
                }
            } else if (PlayerCurrentListView.this.a) {
                boolean unused = PlayerCurrentListView.this.a(i, PlayerCurrentListView.this.a);
                switch (i) {
                    case 0:
                        if (gx.d != null) {
                            try {
                                be j2 = gx.d.j();
                                if (j2 == null || j2.e() <= 0) {
                                    jh.a(PlayerCurrentListView.this.getContext(), (int) R.string.playlist_list_null);
                                } else {
                                    gx.d.a((bj) null);
                                    gx.d.a(new be("-1", 0, "", 0, 1, j2.f(), j2.g()), true);
                                }
                            } catch (RemoteException e) {
                                e.printStackTrace();
                            }
                        }
                        PlayerCurrentListView.this.u();
                        return;
                    default:
                        return;
                }
            } else {
                boolean unused2 = PlayerCurrentListView.this.a(i);
                switch (i) {
                    case 1:
                        if (gx.d != null) {
                            try {
                                be j3 = gx.d.j();
                                if (j3 == null || j3.e() <= 0) {
                                    jh.a(PlayerCurrentListView.this.getContext(), (int) R.string.playlist_list_null);
                                } else {
                                    gx.d.a((bj) null);
                                    gx.d.a(new be("-1", 0, "", 0, 1, j3.f(), j3.g()), true);
                                }
                            } catch (RemoteException e2) {
                                e2.printStackTrace();
                            }
                        }
                        PlayerCurrentListView.this.u();
                        return;
                    default:
                        return;
                }
            }
        }
    };
    private AdapterView.OnItemClickListener K = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView adapterView, View view, int i, long j) {
            boolean unused = PlayerCurrentListView.this.b(i);
        }
    };
    private TextView x;
    private ListView y;
    private TextView z;

    class ListQueryHandler extends AsyncQueryHandler {
        public ListQueryHandler(ContentResolver contentResolver) {
            super(contentResolver);
        }

        public void onQueryComplete(int i, Object obj, Cursor cursor) {
            PlayerCurrentListView.this.a(cursor);
        }
    }

    class MyAdapter extends CursorAdapter {
        ar a;
        private LayoutInflater b;

        class ItemStruct {
            TextView a;
            TextView b;
            TextView c;
            ImageView d;
            ImageView e;

            private ItemStruct() {
            }
        }

        public MyAdapter(Context context, Cursor cursor) {
            super(context, cursor);
            this.b = (LayoutInflater) context.getSystemService("layout_inflater");
            this.a = ar.a(context);
        }

        public int a() {
            if (getCursor() != null) {
                return getCount();
            }
            return 0;
        }

        public bj a(int i) {
            if (getCursor() != null) {
                return ar.a((Cursor) getItem(i));
            }
            return null;
        }

        public void bindView(View view, Context context, Cursor cursor) {
            ItemStruct itemStruct = (ItemStruct) view.getTag();
            bj a2 = ar.a(cursor);
            if (a2 != null) {
                itemStruct.b.setText(en.l(new StringBuffer().append(String.valueOf(cursor.getPosition() + 1)).append(".").append(a2.h()).toString()));
                itemStruct.c.setText(en.d(a2.k()));
                itemStruct.a.setText(a2.j());
                if (cursor.getPosition() == PlayerCurrentListView.H) {
                    if (itemStruct.d != null) {
                        if (gx.a()) {
                            itemStruct.d.setImageResource(R.drawable.list_play_icon);
                        } else {
                            itemStruct.d.setImageResource(R.drawable.list_pause_icon);
                        }
                        itemStruct.d.setVisibility(0);
                    }
                } else if (itemStruct.d != null) {
                    itemStruct.d.setVisibility(8);
                }
                if (a2.b() > 0) {
                    itemStruct.e.setVisibility(8);
                } else {
                    itemStruct.e.setVisibility(0);
                }
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
            View inflate = this.b.inflate((int) R.layout.playlist_music_row, viewGroup, false);
            ItemStruct itemStruct = new ItemStruct();
            itemStruct.b = (TextView) inflate.findViewById(R.id.playlist_local_song_name);
            itemStruct.c = (TextView) inflate.findViewById(R.id.playlist_local_duration);
            itemStruct.a = (TextView) inflate.findViewById(R.id.playlist_local_singer);
            itemStruct.d = (ImageView) inflate.findViewById(R.id.playlist_local_select);
            itemStruct.e = (ImageView) inflate.findViewById(R.id.playlist_online_image);
            inflate.setTag(itemStruct);
            return inflate;
        }
    }

    public PlayerCurrentListView(Activity activity) {
        super(activity);
        this.F = activity;
    }

    private void a(AsyncQueryHandler asyncQueryHandler) {
        if (asyncQueryHandler != null) {
            asyncQueryHandler.startQuery(0, null, Uri.parse(ci.b + "/" + this.E), null, null, null, null);
        }
    }

    /* access modifiers changed from: private */
    public void a(Cursor cursor) {
        if (cursor == null || cursor.getCount() <= 0) {
            u();
            return;
        }
        if (gx.d != null) {
            try {
                be j = gx.d.j();
                if (j != null) {
                    H = j.c();
                    if (j.e() != cursor.getCount()) {
                        j.c(cursor.getCount());
                        gx.d.a(j, false);
                    }
                }
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        t();
        if (this.C == null) {
            this.C = new MyAdapter(g(), cursor);
            this.y.setAdapter((ListAdapter) this.C);
            if (H > 0) {
                this.y.setSelection(H);
                return;
            }
            return;
        }
        this.C.changeCursor(cursor);
        if (H > 0) {
            this.y.setSelection(H);
        }
    }

    private void a(be beVar) {
        if (beVar.a() == 1) {
            this.E = en.d(beVar.b());
            if (this.E < 0) {
                u();
                return;
            }
            this.B = new ListQueryHandler(g().getContentResolver());
            a(this.B);
            return;
        }
        this.B = new ListQueryHandler(g().getContentResolver());
        b(this.B);
    }

    private void b(AsyncQueryHandler asyncQueryHandler) {
        if (asyncQueryHandler != null) {
            asyncQueryHandler.startQuery(0, null, bt.a, null, null, null, null);
        }
    }

    private void s() {
        if (gx.d != null) {
            try {
                if (gx.d.j() != null) {
                    be j = gx.d.j();
                    a(j);
                    this.G = j.d();
                    this.x.setText(getContext().getResources().getString(R.string.player_current_title) + this.G);
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
            } catch (RemoteException e2) {
                e2.printStackTrace();
            }
        } else {
            u();
        }
    }

    private void t() {
        if (this.A.getVisibility() == 8) {
            this.A.setVisibility(0);
        }
        if (this.z.getVisibility() == 0) {
            this.z.setVisibility(8);
        }
    }

    /* access modifiers changed from: private */
    public void u() {
        this.A.setVisibility(8);
        this.z.setVisibility(0);
    }

    public void a(Message message) {
        ad.b("PlayerCurrentListView", "setdata");
        this.D = message;
        s();
        super.a(message);
    }

    public boolean a(int i, KeyEvent keyEvent) {
        b(i, keyEvent);
        if (i != 4 || this.f.getVisibility() != 0 || this.u) {
            return false;
        }
        if (this.g.getVisibility() == 0) {
            this.g.setVisibility(8);
            this.i.requestFocus();
        } else {
            this.f.setVisibility(8);
        }
        return true;
    }

    public void c(int i) {
        if (this.C == null || this.C.a() <= 0) {
            u();
        } else if (gx.d != null) {
            ad.b("PlayerCurrentListView", "freshListState");
            try {
                be j = gx.d.j();
                t();
                if (this.C == null) {
                    a(j);
                    return;
                }
                this.C.notifyDataSetChanged();
                this.y.setSelection(i);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    public void f() {
        inflate(g(), R.layout.playlist_current, this);
        this.y = (ListView) findViewById(R.id.playlist_current_list);
        this.x = (TextView) findViewById(R.id.playlist_current_title);
        this.z = (TextView) findViewById(R.id.playlist_current_tip);
        this.A = (LinearLayout) findViewById(R.id.playlist_current_layout);
        this.z.setText((int) R.string.player_currentlist_null);
        this.y.setOnItemClickListener(this.I);
        h();
    }

    public void m() {
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.h.getLayoutParams();
        if (en.b(getContext()) > 239 && en.b(getContext()) < 241) {
            layoutParams.bottomMargin = 39;
        }
        if (en.b(getContext()) > 319 && en.b(getContext()) < 321) {
            layoutParams.bottomMargin = 49;
        }
        if (en.b(getContext()) > 479 && en.b(getContext()) < 481) {
            layoutParams.bottomMargin = 84;
        }
        this.h.setLayoutParams(layoutParams);
    }

    public void n() {
        this.i.setOnItemClickListener(this.J);
        this.j.setOnItemClickListener(this.K);
    }

    public void o() {
        if (this.a) {
            this.q = getResources().getStringArray(R.array.PlayerCurrent_menu_without_download);
            this.s = new ArrayList();
            this.s.add(Integer.valueOf((int) R.drawable.meun_clean));
            this.s.add(Integer.valueOf((int) R.drawable.menu_bg));
        } else {
            this.q = getResources().getStringArray(R.array.PlayerCurrent_menu);
            this.s = new ArrayList();
            this.s.add(Integer.valueOf((int) R.drawable.meun_downloadsetup));
            this.s.add(Integer.valueOf((int) R.drawable.meun_clean));
        }
        this.s.add(Integer.valueOf((int) R.drawable.menu_bg));
        this.s.add(Integer.valueOf((int) R.drawable.meun_more));
        this.s.add(Integer.valueOf((int) R.drawable.meun_setup));
        this.s.add(Integer.valueOf((int) R.drawable.meun_sleep));
        this.s.add(Integer.valueOf((int) R.drawable.meun_syc));
        this.s.add(Integer.valueOf((int) R.drawable.meun_exit));
        this.r = getResources().getStringArray(R.array.second_menu2);
        this.t = new ArrayList();
        this.t.add(Integer.valueOf((int) R.drawable.meun_help));
        this.t.add(Integer.valueOf((int) R.drawable.meun_advice));
        this.t.add(Integer.valueOf((int) R.drawable.meun_friends));
        this.t.add(Integer.valueOf((int) R.drawable.meun_about));
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        return motionEvent.getAction() == 0;
    }

    public int q() {
        if (this.C != null) {
            return this.C.a();
        }
        return 0;
    }
}
