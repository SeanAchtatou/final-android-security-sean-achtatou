package com.duomi.app.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.AsyncQueryHandler;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.provider.MediaStore;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.duomi.android.R;
import com.duomi.app.ui.MultiView;
import java.io.File;
import java.util.ArrayList;
import java.util.Random;

public class PlayListMusicView extends c implements View.OnClickListener {
    /* access modifiers changed from: private */
    public static int I;
    private static int[] O = new int[2];
    /* access modifiers changed from: private */
    public static int P = 0;
    /* access modifiers changed from: private */
    public static int Q = -1;
    /* access modifiers changed from: private */
    public static boolean V;
    private Button A;
    private ImageView B;
    private Message C;
    /* access modifiers changed from: private */
    public MyAdapter D;
    /* access modifiers changed from: private */
    public ListQueryHandler E;
    private LinearLayout F;
    private LinearLayout G;
    /* access modifiers changed from: private */
    public Context H;
    private RelativeLayout J;
    private TextView K;
    private TextView L;
    private ImageView M;
    private ImageView N;
    /* access modifiers changed from: private */
    public ar R;
    private View S;
    /* access modifiers changed from: private */
    public View T;
    /* access modifiers changed from: private */
    public RefreshHandler U;
    /* access modifiers changed from: private */
    public ProgressDialog W;
    /* access modifiers changed from: private */
    public int X = -1;
    /* access modifiers changed from: private */
    public Handler Y = new Handler() {
        public void handleMessage(Message message) {
            switch (message.what) {
                case 255:
                    gx.a(PlayListMusicView.this.H, PlayListMusicView.this.R.d(message.getData().getInt("listId")), PlayListMusicView.this.D.a(PlayListMusicView.P - 1).f(), (MultiView.OnLikeReSetListener) null, 0);
                    return;
                default:
                    return;
            }
        }
    };
    private AdapterView.OnItemClickListener Z = new AdapterView.OnItemClickListener() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.duomi.app.ui.PlayListMusicView.a(com.duomi.app.ui.PlayListMusicView, boolean):void
         arg types: [com.duomi.app.ui.PlayListMusicView, int]
         candidates:
          com.duomi.app.ui.PlayListMusicView.a(com.duomi.app.ui.PlayListMusicView, int):void
          com.duomi.app.ui.PlayListMusicView.a(com.duomi.app.ui.PlayListMusicView, android.content.AsyncQueryHandler):void
          com.duomi.app.ui.PlayListMusicView.a(com.duomi.app.ui.PlayListMusicView, android.database.Cursor):void
          com.duomi.app.ui.PlayListMusicView.a(com.duomi.app.ui.PlayListMusicView, bj):void
          com.duomi.app.ui.PlayListMusicView.a(android.content.Context, bj):void
          com.duomi.app.ui.PlayListMusicView.a(int, android.view.KeyEvent):boolean
          c.a(java.lang.Class, android.os.Message):void
          c.a(boolean, android.os.Message):void
          c.a(int, android.view.KeyEvent):boolean
          c.a(int, boolean):boolean
          com.duomi.app.ui.PlayListMusicView.a(com.duomi.app.ui.PlayListMusicView, boolean):void */
        public void onItemClick(AdapterView adapterView, View view, int i, long j) {
            if (i != 0) {
                int unused = PlayListMusicView.P = i;
                PlayListMusicView.this.b(false);
            } else if (PlayListMusicView.this.D == null || PlayListMusicView.this.D.a() < 1) {
                jh.a(PlayListMusicView.this.getContext(), (int) R.string.playlist_list_null);
            } else {
                int unused2 = PlayListMusicView.P = new Random().nextInt(PlayListMusicView.this.D.a()) + 1;
                PlayListMusicView.this.b(true);
            }
        }
    };
    private BroadcastReceiver aa = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals("action_addtolist")) {
                if (intent.getIntExtra("addlistid", -1) == PlayListMusicView.I) {
                    PlayListMusicView.this.U.sendEmptyMessage(1);
                }
            } else if (action.equals("com.duomi.android.app.scanner.scanstart")) {
                PlayListMusicView.this.U.sendEmptyMessage(7);
            } else if (action.equals("com.duomi.android.app.scanner.scancancel")) {
                PlayListMusicView.this.U.sendEmptyMessage(8);
            } else if (action.equals("com.duomi.android.app.scanner.scancomplete")) {
                PlayListMusicView.this.U.sendEmptyMessage(0);
            } else if (action.equals("com.duomi.android.playstatechanged") || action.equals("com.duomi.android.metachanged") || action.equals("com.duomi.android.playbackcomplete") || action.equals("com.duomi.android.queuechanged")) {
                ad.b("PlayListMusicView", "playlist state>>" + action);
                PlayListMusicView.this.U.removeMessages(2);
                PlayListMusicView.this.U.sendEmptyMessageDelayed(2, 1000);
            }
        }
    };
    private AdapterView.OnItemClickListener ab = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView adapterView, View view, int i, long j) {
            if (i == 3) {
                if (PlayListMusicView.this.g.getVisibility() == 0) {
                    PlayListMusicView.this.j();
                } else {
                    PlayListMusicView.this.i();
                }
            } else if (PlayListMusicView.this.a) {
                boolean unused = PlayListMusicView.this.a(i, PlayListMusicView.this.a);
                switch (i) {
                    case 0:
                        fd.a().a(PlayListMusicView.this.H, (Handler) null);
                        return;
                    default:
                        return;
                }
            } else {
                boolean unused2 = PlayListMusicView.this.a(i);
                switch (i) {
                    case 1:
                        fd.a().a(PlayListMusicView.this.H, (Handler) null);
                        return;
                    default:
                        return;
                }
            }
        }
    };
    private AdapterView.OnItemClickListener ac = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView adapterView, View view, int i, long j) {
            boolean unused = PlayListMusicView.this.b(i);
        }
    };
    boolean x = false;
    private ListView y;
    private TextView z;

    class DeleteTask extends AsyncTask {
        private DeleteTask() {
        }

        private void a(bj bjVar, int i) {
            PlayListMusicView.this.R.c(i, bjVar.f());
            bl b = PlayListMusicView.this.R.b(bn.a().h(), 2);
            if (b != null && b.g() == i && gx.a(bjVar.f())) {
                PlayListMusicView.this.g().runOnUiThread(new Runnable() {
                    public void run() {
                        PlayerLayoutView.H.a(false);
                    }
                });
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public Object doInBackground(Integer... numArr) {
            int intValue = numArr[0].intValue();
            int intValue2 = numArr[1].intValue();
            bj a2 = PlayListMusicView.this.D.a(numArr[2].intValue() - 1);
            if (intValue2 != 1) {
                a(a2, intValue);
            } else if (a2.p() == null || a2.p().trim().length() <= 0) {
                PlayListMusicView.this.R.a(a2.f());
                PlayListMusicView.this.R.b(a2.f());
                String e = a2.e();
                if (e == null || e.length() <= 0) {
                    return null;
                }
                File file = new File(e);
                if (file != null) {
                    file.delete();
                }
            } else {
                String e2 = a2.e();
                if (e2 == null || e2.length() <= 0) {
                    return null;
                }
                File file2 = new File(e2);
                if (file2 != null) {
                    file2.delete();
                }
                a(a2, intValue);
            }
            return null;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Object obj) {
            PlayListMusicView.this.a(PlayListMusicView.this.E);
            PlayListMusicView.this.W.dismiss();
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            PlayListMusicView.this.W.show();
            super.onPreExecute();
        }
    }

    class ListQueryHandler extends AsyncQueryHandler {
        public ListQueryHandler(ContentResolver contentResolver) {
            super(contentResolver);
        }

        public void onQueryComplete(int i, Object obj, Cursor cursor) {
            PlayListMusicView.this.a(cursor);
            int count = cursor.getCount();
            if (PlayListMusicView.this.X == 0 && gx.a(PlayListMusicView.I + "")) {
                try {
                    be j = gx.d.j();
                    if (j != null) {
                        if (count == 0) {
                            gx.d.a((bj) null);
                            gx.d.a(new be("-1", 0, "", 0, 1, j.f(), j.g()), true);
                            return;
                        }
                        bj k = gx.d.k();
                        if (k != null) {
                            j.b(PlayListMusicView.this.R.a(PlayListMusicView.I, k.f()));
                            gx.d.a(new be(j.b(), j.c(), j.d(), count, 1, j.f(), j.g()), true);
                        }
                    }
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            } else if (PlayListMusicView.this.X > 0) {
                try {
                    be j2 = gx.d.j();
                    if (j2 != null) {
                        if (j2.a() != 0) {
                            if (!gx.a(PlayListMusicView.I + "")) {
                                int a2 = PlayListMusicView.this.R.a(en.d(j2.b()));
                                if (a2 != j2.e()) {
                                    count = a2;
                                } else {
                                    return;
                                }
                            }
                            if (count == 0) {
                                gx.d.a((bj) null);
                                gx.d.a(new be("-1", 0, "", 0, 1, j2.f(), j2.g()), true);
                            } else {
                                bj k2 = gx.d.k();
                                if (k2 != null) {
                                    j2.b(PlayListMusicView.this.R.a(en.d(j2.b()), k2.f()));
                                }
                                gx.d.a(new be(j2.b(), j2.c(), j2.d(), count, 1, j2.f(), j2.g()), true);
                            }
                        } else {
                            return;
                        }
                    }
                } catch (RemoteException e2) {
                    e2.printStackTrace();
                }
            }
            int unused = PlayListMusicView.this.X = -1;
        }
    }

    class MyAdapter extends CursorAdapter {
        ar a;
        private LayoutInflater b;

        class ItemStruct {
            ImageView a;
            TextView b;
            TextView c;
            TextView d;
            ImageView e;

            private ItemStruct() {
            }
        }

        public MyAdapter(Context context, Cursor cursor) {
            super(context, cursor);
            this.b = (LayoutInflater) context.getSystemService("layout_inflater");
            this.a = ar.a(context);
        }

        public int a() {
            if (getCursor() != null) {
                return getCount();
            }
            return 0;
        }

        public bj a(int i) {
            if (getCursor() != null) {
                return ar.a((Cursor) getItem(i));
            }
            return null;
        }

        public void bindView(View view, Context context, Cursor cursor) {
            ItemStruct itemStruct = (ItemStruct) view.getTag();
            bj a2 = ar.a(cursor);
            ad.c("weibo_share", "   " + a2.g());
            if (a2 != null) {
                if (!PlayListMusicView.V) {
                    itemStruct.a.setVisibility(8);
                } else if (cursor.getPosition() == PlayListMusicView.Q) {
                    if (itemStruct.a != null) {
                        if (gx.a()) {
                            itemStruct.a.setImageResource(R.drawable.list_play_icon);
                        } else {
                            itemStruct.a.setImageResource(R.drawable.list_pause_icon);
                        }
                        itemStruct.a.setVisibility(0);
                    }
                } else if (itemStruct.a != null) {
                    itemStruct.a.setVisibility(8);
                }
                itemStruct.c.setText(en.l(new StringBuffer().append(String.valueOf(cursor.getPosition() + 1)).append(".").append(a2.h()).toString()));
                itemStruct.d.setText(en.d(a2.k()));
                if (a2.b() == 1) {
                    itemStruct.e.setVisibility(8);
                } else {
                    itemStruct.e.setVisibility(0);
                }
                itemStruct.b.setText(a2.j());
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
            View inflate = this.b.inflate((int) R.layout.playlist_music_row, viewGroup, false);
            ItemStruct itemStruct = new ItemStruct();
            itemStruct.a = (ImageView) inflate.findViewById(R.id.playlist_local_select);
            itemStruct.c = (TextView) inflate.findViewById(R.id.playlist_local_song_name);
            itemStruct.d = (TextView) inflate.findViewById(R.id.playlist_local_duration);
            itemStruct.b = (TextView) inflate.findViewById(R.id.playlist_local_singer);
            itemStruct.e = (ImageView) inflate.findViewById(R.id.playlist_online_image);
            inflate.setTag(itemStruct);
            return inflate;
        }
    }

    class RefreshHandler extends Handler {
        public RefreshHandler(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message message) {
            switch (message.what) {
                case 0:
                    PlayListMusicView.this.T.setVisibility(8);
                    PlayListMusicView.this.T.postInvalidate();
                    break;
                case 1:
                    PlayListMusicView.this.a(PlayListMusicView.this.E);
                    break;
                case 2:
                    if (PlayListMusicView.this.D == null || PlayListMusicView.this.D.a() <= 0 || !gx.a(PlayListMusicView.I + "")) {
                        if (PlayListMusicView.V) {
                            boolean unused = PlayListMusicView.V = false;
                            PlayListMusicView.this.D.notifyDataSetChanged();
                            break;
                        }
                    } else {
                        boolean unused2 = PlayListMusicView.V = true;
                        try {
                            int unused3 = PlayListMusicView.Q = gx.d.j().c();
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                        PlayListMusicView.this.D.notifyDataSetChanged();
                        break;
                    }
                    break;
                case 7:
                    PlayListMusicView.this.T.setVisibility(0);
                    PlayListMusicView.this.T.postInvalidate();
                    break;
                case 8:
                    PlayListMusicView.this.T.setVisibility(8);
                    PlayListMusicView.this.T.postInvalidate();
                    break;
            }
            super.handleMessage(message);
        }
    }

    class ShareHandler extends Handler {
        ShareHandler() {
        }

        public void handleMessage(Message message) {
            switch (message.what) {
                case 0:
                    Toast.makeText(PlayListMusicView.this.getContext(), " 已分享到新浪微博！", 1).show();
                    break;
            }
            super.handleMessage(message);
        }
    }

    public class ShareSinaWeibo extends AsyncTask {
        ProgressDialog a;
        final /* synthetic */ PlayListMusicView b;

        /* access modifiers changed from: protected */
        public Object doInBackground(Object... objArr) {
            return null;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Object obj) {
            if (this.a != null) {
                this.a.dismiss();
            }
            new ShareHandler().sendEmptyMessage(0);
            super.onPostExecute(obj);
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            this.a = new ProgressDialog(this.b.getContext());
            this.a.setMessage("正在分享，请稍后...");
            this.a.show();
            super.onPreExecute();
        }
    }

    class SongInfoSaveTask extends AsyncTask {
        private bj b;
        private ax c;
        private ProgressDialog d;
        private String e;
        private String f;
        private String g;

        public SongInfoSaveTask(bj bjVar, ax axVar, String str, String str2, String str3) {
            this.b = bjVar;
            this.c = axVar;
            this.e = str;
            this.f = str2;
            this.g = str3;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public Object doInBackground(Integer... numArr) {
            String a2;
            int intValue = numArr[0].intValue();
            int intValue2 = numArr[1].intValue();
            int intValue3 = numArr[2].intValue();
            ar a3 = ar.a(PlayListMusicView.this.getContext());
            if (intValue3 > 0) {
                if (this.c != null) {
                    this.c.a(this.g);
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("s_songid", this.c.b());
                    contentValues.put("album_name", this.g);
                    ai.a(PlayListMusicView.this.getContext()).a(contentValues);
                    a2 = this.c.e() != null ? this.c.e() : this.b.a();
                } else {
                    a2 = this.b.a();
                }
                this.b.a(this.g);
                if (this.g == null || this.g.length() <= 0) {
                    this.g = PlayListMusicView.this.getContext().getString(R.string.unknown);
                }
                bl a4 = a3.a(a2, 3);
                bl a5 = a3.a(this.g, 3);
                if (a4 != null) {
                    if (a5 == null) {
                        bl blVar = new bl(this.g, 1, 3, null, "", "", "", 0, 5);
                        blVar.a(a3.a(blVar));
                        if (a3.a(a4.g(), this.b.f(), blVar.g()) < 0) {
                            a3.a(this.b.f(), blVar.g());
                        }
                    } else if (a3.a(a4.g(), this.b.f(), a5.g()) < 0) {
                        a3.a(this.b.f(), a5.g());
                    }
                } else if (a5 != null) {
                    a3.a(this.b.f(), a5.g());
                } else {
                    bl blVar2 = new bl(this.g, 1, 3, null, "", "", "", 0, 5);
                    blVar2.a(a3.a(blVar2));
                    a3.a(this.b.f(), blVar2.g());
                }
            }
            if (intValue2 > 0) {
                String j = this.b.j();
                this.b.i(this.f);
                if (this.f == null || this.f.length() <= 0) {
                    this.f = PlayListMusicView.this.getContext().getString(R.string.unknown);
                }
                bl a6 = a3.a(j, 2);
                bl a7 = a3.a(this.f, 2);
                if (a6 != null) {
                    if (a7 == null) {
                        bl blVar3 = new bl(this.f, 1, 2, null, "", "", "", 0, 4);
                        blVar3.a(a3.a(blVar3));
                        if (a3.a(a6.g(), this.b.f(), blVar3.g()) < 0) {
                            a3.a(this.b.f(), blVar3.g());
                        }
                    } else if (a3.a(a6.g(), this.b.f(), a7.g()) < 0) {
                        a3.a(this.b.f(), a7.g());
                    }
                } else if (a7 != null) {
                    a3.a(this.b.f(), a7.g());
                } else {
                    bl blVar4 = new bl(this.f, 1, 2, null, "", "", "", 0, 4);
                    blVar4.a(a3.a(blVar4));
                    a3.a(this.b.f(), blVar4.g());
                }
            }
            if (intValue > 0) {
                this.b.g(this.e);
            }
            a3.a(ar.b(this.b), this.b.f());
            if (gx.a(this.b.f())) {
                try {
                    gx.d.a(this.b);
                } catch (RemoteException e2) {
                    e2.printStackTrace();
                }
            }
            int unused = PlayListMusicView.this.X = 0;
            return null;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Object obj) {
            PlayListMusicView.this.a(PlayListMusicView.this.E);
            this.d.dismiss();
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            this.d = new ProgressDialog(PlayListMusicView.this.getContext());
            this.d.setMessage(PlayListMusicView.this.getContext().getString(R.string.playlist_savesonginfo_tip));
            this.d.setCancelable(false);
            this.d.show();
            super.onPreExecute();
        }
    }

    public PlayListMusicView(Activity activity) {
        super(activity);
        this.H = activity;
    }

    /* access modifiers changed from: private */
    public void a(AsyncQueryHandler asyncQueryHandler) {
        if (asyncQueryHandler != null) {
            ad.b("PlayListMusicView", "querySongList:listid:" + I);
            asyncQueryHandler.startQuery(0, null, Uri.parse(ci.b + "/" + I), null, null, null, null);
        }
    }

    /* access modifiers changed from: private */
    public void a(Cursor cursor) {
        if (cursor != null) {
            ad.b("PlayListMusicView", "freshListViewNow()count:" + cursor.getCount());
            if (this.D == null) {
                ad.b("PlayListMusicView", "refresh---------------------1");
                this.D = new MyAdapter(g(), cursor);
                this.y.setAdapter((ListAdapter) this.D);
                return;
            }
            ad.b("PlayListMusicView", "refresh---------------------2");
            this.D.changeCursor(cursor);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void} */
    /* access modifiers changed from: private */
    public void a(bj bjVar) {
        Uri insert;
        ContentValues contentValues = new ContentValues();
        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        Cursor query = this.H.getContentResolver().query(uri, new String[]{"_id"}, "_data=?", new String[]{bjVar.e()}, null);
        if (query.moveToFirst()) {
            String string = query.getString(0);
            contentValues.put("is_ringtone", (Boolean) true);
            this.H.getContentResolver().update(uri, contentValues, "_data=?", new String[]{bjVar.e()});
            insert = ContentUris.withAppendedId(uri, Long.valueOf(string).longValue());
        } else {
            ContentValues contentValues2 = new ContentValues();
            contentValues2.put("_data", bjVar.e());
            contentValues2.put("title", bjVar.h());
            contentValues2.put("_size", Integer.valueOf(bjVar.l()));
            contentValues2.put("mime_type", "audio/mp3");
            contentValues2.put("duration", Integer.valueOf(bjVar.k()));
            contentValues2.put("is_ringtone", (Boolean) true);
            insert = this.H.getContentResolver().insert(MediaStore.Audio.Media.getContentUriForPath(bjVar.e()), contentValues2);
        }
        ad.b("PlayListMusicView", "uri>>>>>>" + Uri.parse(bjVar.e()));
        RingtoneManager.setActualDefaultRingtoneUri(this.H, 1, insert);
        jh.a(this.H, (int) R.string.playlist_ringtone_success);
        if (query != null) {
            query.close();
        }
    }

    /* access modifiers changed from: private */
    public void b(boolean z2) {
        gx.a(getContext(), I + "", P - 1, this.C != null ? ((bl) this.C.obj).h() : "", this.D.a(), this.D.a(P - 1), z2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: private */
    public void e(final int i) {
        final bj a = this.D.a(i - 1);
        if (a != null) {
            View inflate = LayoutInflater.from(this.H).inflate((int) R.layout.dialog_tips, (ViewGroup) null, false);
            TextView textView = (TextView) inflate.findViewById(R.id.tip1);
            final CheckBox checkBox = (CheckBox) inflate.findViewById(R.id.tiptext1);
            checkBox.setText((int) R.string.delete_check_title);
            checkBox.setChecked(false);
            if (a.b() == 0) {
                checkBox.setClickable(false);
            } else {
                checkBox.setClickable(true);
            }
            textView.setText(a.h());
            new AlertDialog.Builder(this.H).setTitle((int) R.string.delete_title).setView(inflate).setPositiveButton((int) R.string.button_ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    if (checkBox.isChecked()) {
                        int unused = PlayListMusicView.this.X = 1;
                        new DeleteTask().execute(Integer.valueOf(PlayListMusicView.I), 1, Integer.valueOf(i));
                        jh.a(PlayListMusicView.this.H, PlayListMusicView.this.H.getString(R.string.playlist_song_delete).replaceAll("%s", a.h()));
                        return;
                    }
                    int unused2 = PlayListMusicView.this.X = 0;
                    new DeleteTask().execute(Integer.valueOf(PlayListMusicView.I), 0, Integer.valueOf(i));
                    jh.a(PlayListMusicView.this.H, PlayListMusicView.this.H.getString(R.string.playlist_song_clear).replaceAll("%s", a.h()));
                }
            }).setNegativeButton((int) R.string.button_cancel, (DialogInterface.OnClickListener) null).show();
        }
    }

    private void u() {
        this.W = new ProgressDialog(this.H);
        this.W.setMessage(this.H.getString(R.string.app_scan_wait_moment));
        if (this.D != null) {
            this.D.changeCursor(null);
        }
        this.K.setText((int) R.string.playlist_random_play);
        this.M.setImageResource(R.drawable.list_random_icon);
        this.L.setText((int) R.string.downloading);
        this.N.setImageResource(R.drawable.list_download_icon);
        this.B.setImageResource(R.drawable.list_return_icon);
        this.z.setText(((bl) this.C.obj).h());
        if (!fd.i) {
            this.T.setVisibility(0);
        } else {
            this.T.setVisibility(8);
        }
        I = ((bl) this.C.obj).g();
        ad.b("PlayListMusicView", "parentId>>>>" + I);
        if (((bl) this.C.obj).a() == 3) {
            this.F.setVisibility(0);
        } else {
            this.F.setVisibility(8);
        }
        this.B.setVisibility(0);
        this.A.setText((int) R.string.playlist_edit);
        this.E = new ListQueryHandler(g().getContentResolver());
        a(this.E);
        w();
    }

    private void v() {
        try {
            ((PlayListGroupView) this.d).y.pop();
            if (!((PlayListGroupView) this.d).y.isEmpty()) {
                this.d.a(CategoryListView.class, (Message) null);
            } else {
                this.d.a(PlayListMainView.class, (Message) null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void w() {
        this.J.setOnClickListener(this);
        this.F.setOnClickListener(this);
        this.A.setOnClickListener(this);
        this.y.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView adapterView, View view, int i, long j) {
                if (i > 0) {
                    int unused = PlayListMusicView.P = i;
                    final bj a2 = PlayListMusicView.this.D.a(PlayListMusicView.P - 1);
                    if (a2 == null) {
                        return true;
                    }
                    int i2 = R.array.playlistmusic_contextmenu_local;
                    as a3 = as.a(PlayListMusicView.this.H);
                    if (a2.b() == 0 && a3.ac()) {
                        i2 = R.array.playlistmusic_contextmenu_online;
                    }
                    new AlertDialog.Builder(PlayListMusicView.this.getContext()).setTitle(a2.h() + "_" + a2.j()).setItems(i2, new DialogInterface.OnClickListener() {
                        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                         method: com.duomi.app.ui.PlayListMusicView.a(com.duomi.app.ui.PlayListMusicView, boolean):void
                         arg types: [com.duomi.app.ui.PlayListMusicView, int]
                         candidates:
                          com.duomi.app.ui.PlayListMusicView.a(com.duomi.app.ui.PlayListMusicView, int):void
                          com.duomi.app.ui.PlayListMusicView.a(com.duomi.app.ui.PlayListMusicView, android.content.AsyncQueryHandler):void
                          com.duomi.app.ui.PlayListMusicView.a(com.duomi.app.ui.PlayListMusicView, android.database.Cursor):void
                          com.duomi.app.ui.PlayListMusicView.a(com.duomi.app.ui.PlayListMusicView, bj):void
                          com.duomi.app.ui.PlayListMusicView.a(android.content.Context, bj):void
                          com.duomi.app.ui.PlayListMusicView.a(int, android.view.KeyEvent):boolean
                          c.a(java.lang.Class, android.os.Message):void
                          c.a(boolean, android.os.Message):void
                          c.a(int, android.view.KeyEvent):boolean
                          c.a(int, boolean):boolean
                          com.duomi.app.ui.PlayListMusicView.a(com.duomi.app.ui.PlayListMusicView, boolean):void */
                        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                         method: ev.a(android.content.Context, bj, boolean, java.lang.String):void
                         arg types: [android.content.Context, bj, int, java.lang.String]
                         candidates:
                          ev.a(java.lang.String, java.io.File, android.os.Handler, android.content.Context):boolean
                          ev.a(android.content.Context, bj, boolean, java.lang.String):void */
                        public void onClick(DialogInterface dialogInterface, int i) {
                            switch (i) {
                                case 0:
                                    PlayListMusicView.this.b(false);
                                    return;
                                case 1:
                                    if (!kf.b) {
                                        en.a(PlayListMusicView.this.getContext(), a2);
                                        return;
                                    } else if (!il.b(PlayListMusicView.this.H)) {
                                        jh.a(PlayListMusicView.this.H, (int) R.string.app_net_error);
                                        return;
                                    } else {
                                        new gd(PlayListMusicView.this.g(), a2);
                                        return;
                                    }
                                case 2:
                                    gx.a(PlayListMusicView.this.H, a2, PlayListMusicView.I, PlayListMusicView.this.Y, PlayerLayoutView.H);
                                    return;
                                case 3:
                                    if (a2.b() == 0) {
                                        String str = "m";
                                        if (ap.a(PlayListMusicView.this.H).a(String.valueOf(a2.f())) != null) {
                                            str = "r";
                                        }
                                        ev.a(PlayListMusicView.this.getContext(), a2, true, str);
                                        return;
                                    }
                                    PlayListMusicView.this.a(a2);
                                    return;
                                case 4:
                                    bj a2 = ar.a(PlayListMusicView.this.getContext()).a(PlayListMusicView.I, PlayListMusicView.P - 1);
                                    if (a2 != null) {
                                        PlayListMusicView.this.a(PlayListMusicView.this.getContext(), a2);
                                        return;
                                    } else {
                                        PlayListMusicView.this.a(PlayListMusicView.this.getContext(), a2);
                                        return;
                                    }
                                case 5:
                                    PlayListMusicView.this.e(PlayListMusicView.P);
                                    return;
                                default:
                                    return;
                            }
                        }
                    }).show();
                }
                return true;
            }
        });
    }

    public void a(final Context context, final bj bjVar) {
        String d = en.d(bjVar.k());
        final ax a = ai.a(context).a(bjVar.g());
        StringBuffer append = new StringBuffer().append("");
        if (a == null) {
            append.append(bjVar.a());
        } else {
            append.append(a.e());
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(context.getString(R.string.playlist_song_name)).append(bjVar.h()).append("\n");
        stringBuffer.append(context.getString(R.string.playlist_song_singer)).append(en.a(context, bjVar.j())).append("\n");
        stringBuffer.append(context.getString(R.string.playlist_song_album)).append(en.a(context, append.toString())).append("\n");
        stringBuffer.append(context.getString(R.string.playlist_song_duration)).append(d).append("\n");
        if (bjVar.b() > 0) {
            stringBuffer.append(context.getString(R.string.playlist_song_path)).append(bjVar.e()).append("\n");
            stringBuffer.append(context.getString(R.string.playlist_song_size)).append(en.c(bjVar.l())).append("\n");
            String a2 = en.a(context, bjVar.c());
            if (!a2.equals(context.getString(R.string.unknown))) {
                a2 = a2 + "kbps";
            }
            stringBuffer.append(context.getString(R.string.playlist_song_kbps)).append(a2);
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        AlertDialog create = builder.create();
        builder.setPositiveButton((int) R.string.app_modify, new DialogInterface.OnClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
             arg types: [?, ?[OBJECT, ARRAY], int]
             candidates:
              ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
              ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
            public void onClick(DialogInterface dialogInterface, int i) {
                LinearLayout linearLayout = (LinearLayout) ((LayoutInflater) context.getSystemService("layout_inflater")).inflate((int) R.layout.dialog_musicinfo_modify, (ViewGroup) null, false);
                final EditText editText = (EditText) linearLayout.findViewById(R.id.playlist_modify_song);
                final EditText editText2 = (EditText) linearLayout.findViewById(R.id.playlist_modify_singer);
                final EditText editText3 = (EditText) linearLayout.findViewById(R.id.playlist_modify_album);
                editText.setText(bjVar.h());
                editText2.setText(bjVar.j());
                String a2 = (a == null || a.e() == null) ? bjVar.a() : a.e();
                final String str = a2 == null ? "" : a2;
                editText3.setText(str);
                new AlertDialog.Builder(context).setTitle((int) R.string.playlist_modifysonginfo_tip).setView(linearLayout).setPositiveButton((int) R.string.app_confirm, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        int i2;
                        int i3;
                        int i4;
                        InputMethodManager inputMethodManager = (InputMethodManager) PlayListMusicView.this.H.getSystemService("input_method");
                        inputMethodManager.hideSoftInputFromWindow(editText3.getWindowToken(), 0);
                        inputMethodManager.hideSoftInputFromWindow(editText.getWindowToken(), 0);
                        inputMethodManager.hideSoftInputFromWindow(editText2.getWindowToken(), 0);
                        String trim = editText.getText().toString().trim();
                        String trim2 = editText2.getText().toString().trim();
                        String trim3 = editText3.getText().toString().trim();
                        if (trim == null || trim.length() <= 0) {
                            jh.a(PlayListMusicView.this.getContext(), (int) R.string.playlist_songinfo_save_namenull);
                            return;
                        }
                        if (!trim3.equals(str)) {
                            ad.b("PlayListMusicView", "ablumechanged");
                            i2 = 1;
                        } else {
                            i2 = 0;
                        }
                        if (!trim2.equals(bjVar.j())) {
                            ad.b("PlayListMusicView", "singerchanged");
                            i3 = 1;
                        } else {
                            i3 = 0;
                        }
                        if (!trim.equals(bjVar.h())) {
                            ad.b("PlayListMusicView", "namechanged");
                            i4 = 1;
                        } else {
                            i4 = 0;
                        }
                        if (i4 + i3 + i2 > 0) {
                            new SongInfoSaveTask(bjVar, a, trim, trim2, trim3).execute(Integer.valueOf(i4), Integer.valueOf(i3), Integer.valueOf(i2));
                        }
                    }
                }).setNegativeButton((int) R.string.app_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        InputMethodManager inputMethodManager = (InputMethodManager) PlayListMusicView.this.H.getSystemService("input_method");
                        inputMethodManager.hideSoftInputFromWindow(editText3.getWindowToken(), 0);
                        inputMethodManager.hideSoftInputFromWindow(editText.getWindowToken(), 0);
                        inputMethodManager.hideSoftInputFromWindow(editText2.getWindowToken(), 0);
                    }
                }).show();
            }
        });
        builder.setNegativeButton((int) R.string.app_return, (DialogInterface.OnClickListener) null);
        create.setCanceledOnTouchOutside(true);
        builder.setTitle((int) R.string.playlist_songinfo);
        builder.setMessage(stringBuffer.toString());
        builder.show();
    }

    public void a(Message message) {
        if (message != null) {
            this.C = message;
            u();
        }
        super.a(message);
    }

    public boolean a(int i, KeyEvent keyEvent) {
        b(i, keyEvent);
        if (i == 4 && this.f.getVisibility() == 0 && !this.u) {
            if (this.g.getVisibility() == 0) {
                this.g.setVisibility(8);
                this.i.requestFocus();
            } else {
                this.f.setVisibility(8);
            }
            return true;
        } else if (i != 4 || this.f.getVisibility() != 8 || this.u) {
            return false;
        } else {
            v();
            return true;
        }
    }

    public void c() {
        if (!this.x) {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("action_addtolist");
            intentFilter.addAction("com.duomi.android.playstatechanged");
            intentFilter.addAction("com.duomi.android.metachanged");
            intentFilter.addAction("com.duomi.android.playbackcomplete");
            intentFilter.addAction("com.duomi.android.queuechanged");
            intentFilter.addAction("com.duomi.android.app.scanner.scancomplete");
            intentFilter.addAction("com.duomi.android.app.scanner.scanstart");
            intentFilter.addAction("com.duomi.android.app.scanner.scancancel");
            g().registerReceiver(this.aa, new IntentFilter(intentFilter));
            this.x = true;
        }
        this.U = new RefreshHandler(g().getMainLooper());
        if (gx.a(I + "")) {
            V = true;
            try {
                Q = gx.d.j().c();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        } else {
            V = false;
        }
    }

    public void d() {
        ad.b("PlayListMusicView", "onPause>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        if (this.aa != null && this.x) {
            g().unregisterReceiver(this.aa);
            this.x = false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public void f() {
        Window window = g().getWindow();
        if (window != null) {
            window.clearFlags(1024);
        }
        inflate(g(), R.layout.playlist_music, this);
        this.y = (ListView) findViewById(R.id.music_list);
        if (en.b() == 4) {
            this.y.setFastScrollEnabled(false);
        }
        this.z = (TextView) findViewById(R.id.list_title);
        this.S = LayoutInflater.from(this.H).inflate((int) R.layout.playlist_play_mode, (ViewGroup) null, false);
        this.G = (LinearLayout) this.S.findViewById(R.id.play_mode_layout);
        this.F = (LinearLayout) findViewById(R.id.downloading_layout);
        this.K = (TextView) this.S.findViewById(R.id.play_modle);
        this.M = (ImageView) this.S.findViewById(R.id.play_modle_image);
        this.N = (ImageView) findViewById(R.id.downloading_image);
        this.L = (TextView) findViewById(R.id.downloading_text);
        this.A = (Button) findViewById(R.id.command);
        this.B = (ImageView) findViewById(R.id.go_back);
        this.J = (RelativeLayout) findViewById(R.id.title_panel);
        this.T = findViewById(R.id.scanning_tip);
        this.R = ar.a(this.H);
        this.y.addHeaderView(this.S);
        this.y.setOnItemClickListener(this.Z);
        h();
    }

    public void n() {
        this.i.setOnItemClickListener(this.ab);
        this.j.setOnItemClickListener(this.ac);
    }

    public void o() {
        if (this.a) {
            this.q = getResources().getStringArray(R.array.PlayListMusic_menu_without_download);
            this.s = new ArrayList();
            this.s.add(Integer.valueOf((int) R.drawable.meun_scan));
            this.s.add(Integer.valueOf((int) R.drawable.menu_bg));
        } else {
            this.q = getResources().getStringArray(R.array.PlayListMusic_menu);
            this.s = new ArrayList();
            this.s.add(Integer.valueOf((int) R.drawable.meun_downloadsetup));
            this.s.add(Integer.valueOf((int) R.drawable.meun_scan));
        }
        this.s.add(Integer.valueOf((int) R.drawable.menu_bg));
        this.s.add(Integer.valueOf((int) R.drawable.meun_more));
        this.s.add(Integer.valueOf((int) R.drawable.meun_setup));
        this.s.add(Integer.valueOf((int) R.drawable.meun_sleep));
        this.s.add(Integer.valueOf((int) R.drawable.meun_syc));
        this.s.add(Integer.valueOf((int) R.drawable.meun_exit));
        this.r = getResources().getStringArray(R.array.second_menu2);
        this.t = new ArrayList();
        this.t.add(Integer.valueOf((int) R.drawable.meun_help));
        this.t.add(Integer.valueOf((int) R.drawable.meun_advice));
        this.t.add(Integer.valueOf((int) R.drawable.meun_friends));
        this.t.add(Integer.valueOf((int) R.drawable.meun_about));
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.title_panel:
                v();
                return;
            case R.id.command:
                this.d.a(PlayListMusicEditView.class, this.C);
                return;
            case R.id.downloading_layout:
                p.a(g()).a(DownloadManagerView.class.getName());
                return;
            default:
                return;
        }
    }
}
