package com.duomi.app.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.InputFilter;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.duomi.android.R;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class DownloadPathView extends c {
    private static boolean H = false;
    private Button A;
    private Button B;
    private ListView C;
    /* access modifiers changed from: private */
    public as D;
    /* access modifiers changed from: private */
    public List E;
    private FolderAdapter F;
    /* access modifiers changed from: private */
    public String G = "";
    /* access modifiers changed from: private */
    public Activity x;
    private TextView y;
    private Button z;

    class FolderAdapter extends BaseAdapter {
        ItemStruct a;
        private Context c;
        private LayoutInflater d;

        public FolderAdapter(Context context) {
            this.c = context;
            this.d = LayoutInflater.from(context);
        }

        public int getCount() {
            return DownloadPathView.this.E.size();
        }

        public Object getItem(int i) {
            return DownloadPathView.this.E.get(i);
        }

        public long getItemId(int i) {
            return (long) i;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(int i, View view, ViewGroup viewGroup) {
            View view2;
            if (view == null) {
                this.a = new ItemStruct();
                view2 = this.d.inflate((int) R.layout.pathlist_row, viewGroup, false);
                this.a.a = (ImageView) view2.findViewById(R.id.back);
                this.a.b = (TextView) view2.findViewById(R.id.path);
                view2.setTag(this.a);
            } else {
                this.a = (ItemStruct) view.getTag();
                view2 = view;
            }
            if (i != 0 || !((String) DownloadPathView.this.E.get(i)).equals(DownloadPathView.this.getResources().getString(R.string.setting_folder_return))) {
                this.a.a.setVisibility(8);
            } else {
                this.a.a.setVisibility(0);
            }
            this.a.b.setText((CharSequence) DownloadPathView.this.E.get(i));
            return view2;
        }
    }

    class ItemStruct {
        ImageView a;
        TextView b;

        ItemStruct() {
        }
    }

    public DownloadPathView(Activity activity) {
        super(activity);
        this.x = activity;
    }

    /* access modifiers changed from: private */
    public String d(String str) {
        String stringBuffer = new StringBuffer(str).append("/").append(this.x.getResources().getString(R.string.setting_default_path)).toString();
        File file = new File(stringBuffer);
        String string = this.x.getResources().getString(R.string.setting_default_path);
        File file2 = file;
        int i = 0;
        String str2 = string;
        while (file2.exists()) {
            i++;
            file2 = new File(stringBuffer + "(" + i + ")");
            str2 = string + "(" + i + ")";
        }
        return str2;
    }

    public void a() {
        if (H) {
            q();
        }
        super.a();
    }

    public void a(String str) {
        try {
            File file = new File(this.G + "/" + str);
            if (file.exists()) {
                jh.a(this.x, (int) R.string.setting_folder_exist);
            } else if (file.mkdirs()) {
                jh.a(this.x, (int) R.string.setting_create_success);
                this.E.add(str);
                this.F.notifyDataSetChanged();
            } else {
                jh.a(this.x, (int) R.string.setting_create_fail);
            }
        } catch (Exception e) {
            jh.a(this.x, (int) R.string.setting_create_fail);
            e.printStackTrace();
        }
    }

    public boolean a(int i, KeyEvent keyEvent) {
        if (i != 4) {
            return false;
        }
        p.a(g()).a(SettingView.class.getName());
        return true;
    }

    public void b(String str) {
        this.G = str;
        this.y.setText(this.G);
        this.C.setAdapter((ListAdapter) c(str));
    }

    public FolderAdapter c(String str) {
        this.E.clear();
        if (!str.equals(ae.l)) {
            this.E.add(getResources().getString(R.string.setting_folder_return));
        }
        File file = new File(str);
        if (!(file == null || file.listFiles() == null)) {
            for (File file2 : file.listFiles()) {
                if (file2.isDirectory()) {
                    this.E.add(file2.getName());
                }
            }
        }
        this.F = new FolderAdapter(this.x);
        return this.F;
    }

    /* access modifiers changed from: protected */
    public void f() {
        inflate(g(), R.layout.download_path, this);
        this.y = (TextView) findViewById(R.id.download_path);
        this.z = (Button) findViewById(R.id.new_folder);
        this.A = (Button) findViewById(R.id.button_ok);
        this.B = (Button) findViewById(R.id.button_cancel);
        this.C = (ListView) findViewById(R.id.list);
        q();
        H = true;
    }

    public void q() {
        this.D = as.a(this.x);
        this.G = this.D.E();
        this.y.setText(this.G);
        this.E = new ArrayList();
        this.C.setAdapter((ListAdapter) c(this.G));
        this.C.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView adapterView, View view, int i, long j) {
                if (i != 0 || !((String) DownloadPathView.this.E.get(i)).equals(DownloadPathView.this.getResources().getString(R.string.setting_folder_return))) {
                    DownloadPathView.this.b(DownloadPathView.this.G + "/" + ((String) DownloadPathView.this.E.get(i)));
                } else {
                    DownloadPathView.this.b(new File(DownloadPathView.this.G).getParentFile().getAbsolutePath());
                }
            }
        });
        this.z.setOnClickListener(new View.OnClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
             arg types: [?, ?[OBJECT, ARRAY], int]
             candidates:
              ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
              ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
            public void onClick(View view) {
                View inflate = LayoutInflater.from(DownloadPathView.this.x).inflate((int) R.layout.create_directory, (ViewGroup) null, false);
                final EditText editText = (EditText) inflate.findViewById(R.id.name);
                editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(15)});
                editText.setText(DownloadPathView.this.d(DownloadPathView.this.G));
                editText.setSelectAllOnFocus(true);
                new AlertDialog.Builder(DownloadPathView.this.x).setTitle((int) R.string.setting_new_folder).setView(inflate).setPositiveButton((int) R.string.button_ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ((InputMethodManager) DownloadPathView.this.x.getSystemService("input_method")).hideSoftInputFromWindow(editText.getWindowToken(), 0);
                        String trim = editText.getText().toString().trim();
                        if (en.c(trim) || trim.contains("/") || trim.contains("\\") || trim.contains(":") || trim.contains("*") || trim.contains("?") || trim.contains("\"") || trim.contains("<") || trim.contains(">") || trim.contains("|") || trim.contains("con")) {
                            jh.a(DownloadPathView.this.x, (int) R.string.setting_create_fail);
                        } else {
                            DownloadPathView.this.a(trim);
                        }
                    }
                }).setNegativeButton((int) R.string.button_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ((InputMethodManager) DownloadPathView.this.x.getSystemService("input_method")).hideSoftInputFromWindow(editText.getWindowToken(), 0);
                    }
                }).show();
            }
        });
        this.A.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                DownloadPathView.this.D.i(DownloadPathView.this.G);
                p.a(DownloadPathView.this.g()).a(SettingView.class.getName());
            }
        });
        this.B.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                p.a(DownloadPathView.this.g()).a(SettingView.class.getName());
            }
        });
    }
}
