package com.duomi.app.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.duomi.android.PickBgActivity;
import com.duomi.android.R;

public class SinaAccountInfoView extends c {
    private TextView A;
    private TextView B;
    private TextView C;
    private Button D;
    private Button E;
    private Button F;
    private Button G;
    private RelativeLayout H;
    /* access modifiers changed from: private */
    public ProgressDialog I;
    private TextView x;
    private TextView y;
    private TextView z;

    public SinaAccountInfoView(Activity activity) {
        super(activity);
    }

    public void a(Message message) {
        super.a(message);
        q();
    }

    public boolean a(int i, KeyEvent keyEvent) {
        if (i != 4) {
            return false;
        }
        p.a(g()).a(SettingView.class.getName());
        return true;
    }

    /* access modifiers changed from: protected */
    public void f() {
        inflate(g(), R.layout.sina_account_info, this);
        this.B = (TextView) findViewById(R.id.account_type);
        this.C = (TextView) findViewById(R.id.account_name);
        this.C.setText(g().getString(R.string.account_name).replaceAll("#", "        "));
        this.x = (TextView) findViewById(R.id.account_type_text);
        this.y = (TextView) findViewById(R.id.account_name_text);
        this.z = (TextView) findViewById(R.id.account_duomi);
        this.z.setText(g().getString(R.string.account_duomi).replaceAll("#", "  "));
        this.A = (TextView) findViewById(R.id.account_duomi_text);
        this.E = (Button) findViewById(R.id.logout);
        this.H = (RelativeLayout) findViewById(R.id.go_back_layout);
        this.D = (Button) findViewById(R.id.reg_duomi);
        this.D.setVisibility(8);
        q();
    }

    public void q() {
        int intValue = Integer.valueOf(as.a(g()).j()[2]).intValue();
        this.x.setText(g().getResources().getStringArray(R.array.account_type)[intValue]);
        if (intValue == 0) {
            bo b = bn.a().b(0);
            String str = b.c;
            String str2 = b.b;
            this.y.setText(str);
            this.A.setText(str2);
        } else {
            String str3 = bn.a().b(1).c;
            this.z.setVisibility(8);
            this.A.setVisibility(8);
            this.y.setText(str3);
        }
        this.E.setOnClickListener(new View.OnClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: en.a(android.content.Context, boolean):void
             arg types: [android.content.Context, int]
             candidates:
              en.a(int, int):int
              en.a(android.content.Context, float):int
              en.a(android.content.Context, int):int
              en.a(android.graphics.Bitmap, int):android.graphics.Bitmap
              en.a(android.content.Context, java.lang.String):java.lang.String
              en.a(android.content.Context, bj):void
              en.a(java.io.File, java.io.File):void
              en.a(java.lang.String, java.lang.String):void
              en.a(android.app.Activity, boolean):boolean
              en.a(java.security.interfaces.RSAPublicKey, byte[]):byte[]
              en.a(byte[], java.lang.String):byte[]
              en.a(android.content.Context, boolean):void */
            public void onClick(View view) {
                en.a(SinaAccountInfoView.this.getContext(), true);
            }
        });
        this.D.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                p.a(SinaAccountInfoView.this.g()).a(DuomiAccountRegView.class.getName());
            }
        });
        this.H.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                p.a(SinaAccountInfoView.this.g()).a(SettingView.class.getName());
            }
        });
        final AnonymousClass4 r1 = new Handler() {
            public void handleMessage(Message message) {
                super.handleMessage(message);
                switch (message.what) {
                    case 0:
                        Intent intent = new Intent(SinaAccountInfoView.this.getContext(), PickBgActivity.class);
                        as.a(SinaAccountInfoView.this.g()).x(true);
                        SinaAccountInfoView.this.getContext().startActivity(intent);
                        return;
                    case 1:
                        ProgressDialog unused = SinaAccountInfoView.this.I = (ProgressDialog) null;
                        ProgressDialog unused2 = SinaAccountInfoView.this.I = new ProgressDialog(SinaAccountInfoView.this.g());
                        SinaAccountInfoView.this.I.setProgressStyle(0);
                        SinaAccountInfoView.this.I.setTitle("请稍候");
                        SinaAccountInfoView.this.I.setMessage("恢复中...");
                        SinaAccountInfoView.this.I.setCancelable(false);
                        SinaAccountInfoView.this.I.show();
                        sendEmptyMessageDelayed(2, 1000);
                        return;
                    case 2:
                        if (SinaAccountInfoView.this.I != null && SinaAccountInfoView.this.I.isShowing()) {
                            SinaAccountInfoView.this.I.dismiss();
                            as.a(SinaAccountInfoView.this.g()).w(false);
                            SinaAccountInfoView.this.g().getWindow().setBackgroundDrawableResource(R.drawable.background);
                            return;
                        }
                        return;
                    default:
                        return;
                }
            }
        };
        this.F = (Button) findViewById(R.id.pickBackground);
        this.F.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                r1.sendEmptyMessageDelayed(0, 500);
            }
        });
        this.G = (Button) findViewById(R.id.gotoDefault);
        this.G.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                r1.sendEmptyMessage(1);
            }
        });
    }
}
