package com.duomi.app.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.duomi.android.R;

public class BindSettingView extends c implements DialogInterface.OnClickListener, View.OnClickListener {
    public static boolean x = false;
    /* access modifiers changed from: private */
    public CheckBox A;
    /* access modifiers changed from: private */
    public EditText B;
    /* access modifiers changed from: private */
    public EditText C;
    private Button D;
    private Button E;
    /* access modifiers changed from: private */
    public Button F;
    /* access modifiers changed from: private */
    public TextView G;
    /* access modifiers changed from: private */
    public RelativeLayout H;
    /* access modifiers changed from: private */
    public LinearLayout I;
    /* access modifiers changed from: private */
    public TextView J;
    private ProgressDialog K;
    private View L;
    /* access modifiers changed from: private */
    public TextView M;
    /* access modifiers changed from: private */
    public TextView N;
    /* access modifiers changed from: private */
    public TextView O;
    private Button P;
    /* access modifiers changed from: private */
    public TextView Q;
    /* access modifiers changed from: private */
    public AlertDialog R;
    /* access modifiers changed from: private */
    public AlertDialog S;
    /* access modifiers changed from: private */
    public AlertDialog T;
    private AlertDialog U;
    /* access modifiers changed from: private */
    public Handler V;
    /* access modifiers changed from: private */
    public String W;
    private Runnable X = new Runnable() {
        public void run() {
            try {
                Thread.sleep(20);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            String unused = BindSettingView.this.W = bn.a().h();
            int a2 = kf.a(BindSettingView.this.g());
            Message obtainMessage = BindSettingView.this.V.obtainMessage(4);
            obtainMessage.arg1 = a2;
            BindSettingView.this.V.sendMessage(obtainMessage);
        }
    };
    private Runnable Y = new Runnable() {
        public void run() {
            try {
                Thread.sleep(20);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            int a2 = kf.a(BindSettingView.this.g(), "1");
            Message obtainMessage = BindSettingView.this.V.obtainMessage(6);
            obtainMessage.arg1 = a2;
            BindSettingView.this.V.sendMessage(obtainMessage);
        }
    };
    private Runnable Z = new Runnable() {
        public void run() {
            try {
                Thread.sleep(20);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            int a2 = kf.a(BindSettingView.this.g(), BindSettingView.this.B.getText().toString().trim(), BindSettingView.this.C.getText().toString().trim(), "0");
            Message obtainMessage = BindSettingView.this.V.obtainMessage(2);
            obtainMessage.arg1 = a2;
            BindSettingView.this.V.sendMessage(obtainMessage);
        }
    };
    private Runnable aa = new Runnable() {
        public void run() {
            try {
                Thread.sleep(20);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            int a2 = kf.a(BindSettingView.this.g(), BindSettingView.this.B.getText().toString().trim(), BindSettingView.this.C.getText().toString().trim(), "1");
            Message obtainMessage = BindSettingView.this.V.obtainMessage(2);
            obtainMessage.arg1 = a2;
            BindSettingView.this.V.sendMessage(obtainMessage);
        }
    };
    private RelativeLayout y;
    private RelativeLayout z;

    public BindSettingView(Activity activity) {
        super(activity);
    }

    private void r() {
        InputMethodManager inputMethodManager = (InputMethodManager) g().getSystemService("input_method");
        inputMethodManager.hideSoftInputFromWindow(this.B.getWindowToken(), 0);
        inputMethodManager.hideSoftInputFromWindow(this.C.getWindowToken(), 0);
    }

    public void a(Message message) {
        super.a(message);
        q();
    }

    public boolean a(int i, KeyEvent keyEvent) {
        if (i != 4) {
            return false;
        }
        p.a(g()).a(SettingView.class.getName());
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    public void f() {
        inflate(g(), R.layout.bind_setting, this);
        this.y = (RelativeLayout) findViewById(R.id.ly_bind);
        this.A = (CheckBox) findViewById(R.id.sina_check);
        this.A.setClickable(false);
        this.K = new ProgressDialog(g());
        this.K.setProgressStyle(0);
        this.z = (RelativeLayout) findViewById(R.id.go_back_layout);
        this.T = new AlertDialog.Builder(g()).setTitle((int) R.string.bind_sina_title).setMessage((int) R.string.sina_yes_bind).setPositiveButton((int) R.string.app_confirm, this).setNegativeButton((int) R.string.app_cancel, this).create();
        this.U = new AlertDialog.Builder(g()).setTitle((int) R.string.remove_bind_title).setMessage((int) R.string.remove_bind_sms).setPositiveButton((int) R.string.remove_bind, this).setNegativeButton((int) R.string.app_cancel, this).create();
        this.I = (LinearLayout) findViewById(R.id.dm_sina_detail_lable);
        this.H = (RelativeLayout) findViewById(R.id.dm_sina_waitting_lable);
        this.H.setVisibility(8);
        this.J = (TextView) findViewById(R.id.dm_sina_watting_text);
        this.Q = (TextView) findViewById(R.id.sina_account);
        this.V = new Handler() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: bn.a(android.content.Context, boolean):void
             arg types: [android.content.Context, int]
             candidates:
              bn.a(int, android.content.Context):int
              bn.a(android.content.Context, boolean):void */
            public void handleMessage(Message message) {
                switch (message.what) {
                    case 1:
                        BindSettingView.this.J.setText((int) R.string.bind_sina_watting_text);
                        BindSettingView.this.I.setVisibility(8);
                        BindSettingView.this.H.setVisibility(0);
                        return;
                    case 2:
                        BindSettingView.this.I.setVisibility(0);
                        BindSettingView.this.H.setVisibility(8);
                        switch (message.arg1) {
                            case -12:
                                BindSettingView.this.T.show();
                                return;
                            case -11:
                                BindSettingView.this.A.setChecked(true);
                                BindSettingView.this.T.show();
                                return;
                            case -3:
                                jh.a(BindSettingView.this.g(), (int) R.string.sina_username_pwd_error_prompt, 1);
                                BindSettingView.this.C.setText("");
                                BindSettingView.this.S.show();
                                return;
                            case 0:
                                BindSettingView.this.A.setChecked(true);
                                jh.a(BindSettingView.this.g(), (int) R.string.custom_bind_sinaaccount_success_prompt, 1);
                                bo a2 = kf.a(1, BindSettingView.this.g());
                                if (a2 != null) {
                                    if (!en.c(a2.c)) {
                                        BindSettingView.this.Q.setText(a2.c);
                                    }
                                    BindSettingView.this.Q.setVisibility(0);
                                    bn.a().a(BindSettingView.this.getContext(), false);
                                    return;
                                }
                                BindSettingView.this.Q.setVisibility(8);
                                return;
                            default:
                                BindSettingView.this.A.setChecked(false);
                                jh.a(BindSettingView.this.g(), (int) R.string.weibo_common_error_prompt, 1);
                                if (ae.g) {
                                    jh.a(BindSettingView.this.g(), "ERROR CODE = " + message.arg1, 1);
                                    return;
                                }
                                return;
                        }
                    case 3:
                        BindSettingView.this.J.setText((int) R.string.onekey_reg_sina_watting_text);
                        BindSettingView.this.I.setVisibility(8);
                        BindSettingView.this.H.setVisibility(0);
                        return;
                    case 4:
                        BindSettingView.this.I.setVisibility(0);
                        BindSettingView.this.H.setVisibility(8);
                        switch (message.arg1) {
                            case -10:
                                BindSettingView.this.A.setChecked(false);
                                BindSettingView.this.B.setText(bn.a().h() + "@duomi.com");
                                BindSettingView.this.B.setSelection(BindSettingView.this.B.getText().toString().length());
                                BindSettingView.this.F.setVisibility(8);
                                BindSettingView.this.G.setVisibility(8);
                                BindSettingView.this.C.setText("");
                                BindSettingView.this.S.show();
                                jh.a(BindSettingView.this.getContext(), (int) R.string.has_one_key_reg);
                                return;
                            case -7:
                                BindSettingView.this.A.setChecked(true);
                                jh.a(BindSettingView.this.g(), (int) R.string.havs_binded_sina_weibo_yet_error_prompt, 1);
                                return;
                            case 0:
                                BindSettingView.this.A.setChecked(true);
                                Log.e("test", "bindsetting one key reg [passwd]=" + BindSettingView.this.W + "\t[uid]" + bn.a().h());
                                BindSettingView.this.M.setText(BindSettingView.this.g().getResources().getString(R.string.sina_reg_finish_username).concat(" ").concat(kf.a(1, BindSettingView.this.g()).c));
                                BindSettingView.this.N.setText(BindSettingView.this.g().getResources().getString(R.string.sina_reg_finish_password).concat(" ").concat(en.c(BindSettingView.this.W) ? bn.a().h() : BindSettingView.this.W));
                                BindSettingView.this.O.setVisibility(0);
                                BindSettingView.this.R.show();
                                bo a3 = kf.a(1, BindSettingView.this.g());
                                if (a3 != null) {
                                    if (!en.c(a3.c)) {
                                        BindSettingView.this.Q.setText(a3.c);
                                    }
                                    BindSettingView.this.Q.setVisibility(0);
                                    bn.a().a(BindSettingView.this.getContext(), false);
                                    return;
                                }
                                BindSettingView.this.Q.setVisibility(8);
                                return;
                            default:
                                BindSettingView.this.A.setChecked(false);
                                jh.a(BindSettingView.this.g(), (int) R.string.weibo_common_error_prompt, 1);
                                if (ae.g) {
                                    jh.a(BindSettingView.this.g(), "ERROR CODE = " + message.arg1, 1);
                                    return;
                                }
                                return;
                        }
                    case 5:
                        BindSettingView.this.J.setText((int) R.string.unbind_sina_watting_text);
                        BindSettingView.this.I.setVisibility(8);
                        BindSettingView.this.H.setVisibility(0);
                        return;
                    case 6:
                        BindSettingView.this.I.setVisibility(0);
                        BindSettingView.this.H.setVisibility(8);
                        switch (message.arg1) {
                            case -8:
                                BindSettingView.this.A.setChecked(false);
                                BindSettingView.this.Q.setVisibility(8);
                                jh.a(BindSettingView.this.g(), (int) R.string.unbind_sina_account_prompt, 1);
                                return;
                            case 0:
                                BindSettingView.this.A.setChecked(false);
                                BindSettingView.this.Q.setVisibility(8);
                                bn.a().a(1, BindSettingView.this.getContext());
                                aw.a(BindSettingView.this.g()).a(1, bn.a().h());
                                jh.a(BindSettingView.this.g(), (int) R.string.unbind_sina_success, 1);
                                return;
                            default:
                                jh.a(BindSettingView.this.g(), (int) R.string.weibo_common_error_prompt, 1);
                                if (ae.g) {
                                    jh.a(BindSettingView.this.g(), "ERROR CODE = " + message.arg1, 1);
                                    return;
                                }
                                return;
                        }
                    default:
                        return;
                }
            }
        };
        this.L = LayoutInflater.from(g()).inflate((int) R.layout.register_weibo_finished, (ViewGroup) null, false);
        this.M = (TextView) this.L.findViewById(R.id.dm_reg_weibo_username);
        this.N = (TextView) this.L.findViewById(R.id.dm_reg_weibo_pwd);
        this.O = (TextView) this.L.findViewById(R.id.dm_reg_weibo_prompt);
        this.P = (Button) this.L.findViewById(R.id.dm_reg_weibo_entryduomi);
        this.R = new AlertDialog.Builder(g()).setTitle((int) R.string.sina_reg_success_title).create();
        this.R.setView(this.L);
        View inflate = LayoutInflater.from(g()).inflate((int) R.layout.bind_sina_weibo, (ViewGroup) null, false);
        this.B = (EditText) inflate.findViewById(R.id.account);
        this.C = (EditText) inflate.findViewById(R.id.password);
        this.D = (Button) inflate.findViewById(R.id.ok);
        this.E = (Button) inflate.findViewById(R.id.cancel);
        this.F = (Button) inflate.findViewById(R.id.sms_reg);
        this.G = (TextView) inflate.findViewById(R.id.sms_reg_text);
        this.S = new AlertDialog.Builder(g()).setTitle((int) R.string.bind_sina_title).setView(inflate).setIcon((int) R.drawable.sina_logo).create();
        q();
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        if (dialogInterface != this.S && dialogInterface != this.R) {
            if (dialogInterface == this.T) {
                switch (i) {
                    case -2:
                        bn.a().a(1, g());
                        dialogInterface.dismiss();
                        this.A.setChecked(false);
                        return;
                    case -1:
                        this.V.sendEmptyMessage(1);
                        new Thread(this.aa).start();
                        return;
                    default:
                        return;
                }
            } else if (dialogInterface == this.U) {
                switch (i) {
                    case -2:
                        dialogInterface.dismiss();
                        this.A.setChecked(true);
                        return;
                    case -1:
                        this.V.sendEmptyMessage(5);
                        new Thread(this.Y).start();
                        return;
                    default:
                        return;
                }
            }
        }
    }

    public void onClick(View view) {
        boolean z2;
        r();
        switch (view.getId()) {
            case R.id.go_back_layout:
                p.a(g()).a(SettingView.class.getName());
                return;
            case R.id.ly_bind:
                if (kf.b(g()) != 0) {
                    this.A.setChecked(false);
                    jh.a(g(), (int) R.string.weibo_account_operate_forbidden, 1);
                    return;
                } else if (this.A.isChecked()) {
                    this.U.show();
                    return;
                } else {
                    this.C.setText("");
                    this.S.show();
                    return;
                }
            case R.id.ok:
                String trim = this.B.getText().toString().trim();
                String trim2 = this.C.getText().toString().trim();
                if (en.c(trim)) {
                    jh.a(g(), (int) R.string.bind_sina_username_error, 1);
                    z2 = false;
                } else {
                    this.B.setError(null);
                    z2 = true;
                }
                if (en.c(trim2)) {
                    jh.a(g(), (int) R.string.bind_sina_password_error, 1);
                    z2 = false;
                } else {
                    this.C.setError(null);
                }
                if (z2) {
                    this.S.dismiss();
                    this.V.sendEmptyMessage(1);
                    new Thread(this.Z).start();
                    return;
                }
                return;
            case R.id.cancel:
                Log.e("text", "bind cancle  click");
                this.S.dismiss();
                this.A.setChecked(false);
                return;
            case R.id.sms_reg:
                this.S.dismiss();
                this.V.sendEmptyMessage(3);
                new Thread(this.X).start();
                return;
            case R.id.dm_reg_weibo_entryduomi:
                this.R.dismiss();
                return;
            default:
                return;
        }
    }

    public void q() {
        this.A.setChecked(x);
        if (x) {
            bo a = kf.a(1, g());
            if (a != null) {
                if (!en.c(a.c)) {
                    this.Q.setText(a.c);
                }
                this.Q.setVisibility(0);
            } else {
                this.Q.setVisibility(8);
            }
        } else {
            this.Q.setVisibility(8);
        }
        this.y.setClickable(kf.b(g()) != 1);
        this.P.setText((int) R.string.onekey_reg_sina_success_btn);
        this.O.setText((int) R.string.sina_reg_finish_prompt);
        this.P.setOnClickListener(this);
        this.D.setOnClickListener(this);
        this.E.setOnClickListener(this);
        this.F.setOnClickListener(this);
        this.y.setOnClickListener(this);
        this.z.setOnClickListener(this);
    }
}
