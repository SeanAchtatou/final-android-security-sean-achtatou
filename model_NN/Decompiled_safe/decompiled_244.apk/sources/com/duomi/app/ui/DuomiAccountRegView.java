package com.duomi.app.ui;

import android.app.Activity;
import android.view.KeyEvent;
import com.duomi.android.R;

public class DuomiAccountRegView extends c {
    public DuomiAccountRegView(Activity activity) {
        super(activity);
    }

    public boolean a(int i, KeyEvent keyEvent) {
        if (i != 4) {
            return false;
        }
        p.a(g()).a(SinaAccountInfoView.class.getName());
        return true;
    }

    /* access modifiers changed from: protected */
    public void f() {
        inflate(g(), R.layout.duomi_register, this);
        q();
    }

    public void q() {
    }
}
