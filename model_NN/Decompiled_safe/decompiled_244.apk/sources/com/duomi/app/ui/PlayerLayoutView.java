package com.duomi.app.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import com.duomi.android.R;
import com.duomi.app.ui.MultiView;
import com.duomi.core.view.DMAlertController;
import com.duomi.core.view.LyricView;
import com.duomi.core.view.VolumeView;
import java.util.ArrayList;

public class PlayerLayoutView extends c {
    public static boolean[] G;
    public static MultiView.OnLikeReSetListener H;
    public ImageView A;
    public ImageView B;
    public SeekBar C;
    public RelativeLayout D;
    public TextView E;
    public TextView F;
    private LyricView I;
    private TextView J;
    private int K = 18;
    /* access modifiers changed from: private */
    public VolumeView L;
    /* access modifiers changed from: private */
    public Dialog M;
    private ScaleAnimation N;
    /* access modifiers changed from: private */
    public boolean O = false;
    /* access modifiers changed from: private */
    public PlayLayoutHandler P;
    /* access modifiers changed from: private */
    public int Q = 1;
    private Animation R;
    /* access modifiers changed from: private */
    public hg S;
    private Handler T;
    /* access modifiers changed from: private */
    public Handler U;
    private View.OnTouchListener V = new View.OnTouchListener() {
        public boolean onTouch(View view, MotionEvent motionEvent) {
            switch (motionEvent.getAction()) {
                case 0:
                    view.setPressed(true);
                    break;
                case 1:
                case 3:
                    view.setPressed(false);
                    PlayerLayoutView.this.s();
                    break;
            }
            return true;
        }
    };
    private jw W = new jw() {
        public void a() {
            PlayerLayoutView.this.P.removeMessages(1);
            PlayerLayoutView.this.P.sendMessageDelayed(PlayerLayoutView.this.P.obtainMessage(1), 3000);
        }
    };
    private View.OnTouchListener X = new View.OnTouchListener() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.duomi.app.ui.PlayerLayoutView.a(com.duomi.app.ui.PlayerLayoutView, int, boolean):void
         arg types: [com.duomi.app.ui.PlayerLayoutView, int, int]
         candidates:
          com.duomi.app.ui.PlayerLayoutView.a(float, float, int):void
          com.duomi.app.ui.PlayerLayoutView.a(com.duomi.app.ui.PlayerLayoutView, int, boolean):void */
        public boolean onTouch(View view, MotionEvent motionEvent) {
            switch (motionEvent.getAction()) {
                case 0:
                    view.setPressed(true);
                    break;
                case 1:
                case 3:
                    view.setPressed(false);
                    PlayerLayoutView.c(PlayerLayoutView.this);
                    if (PlayerLayoutView.this.Q >= 4) {
                        int unused = PlayerLayoutView.this.Q = 0;
                    }
                    PlayerLayoutView.this.b(PlayerLayoutView.this.Q, true);
                    break;
            }
            return true;
        }
    };
    private final Object Y = new Object();
    /* access modifiers changed from: private */
    public boolean Z = false;
    private SeekBar.OnSeekBarChangeListener aa = new SeekBar.OnSeekBarChangeListener() {
        int a = 0;

        public void onProgressChanged(SeekBar seekBar, int i, boolean z) {
            if (!z || seekBar.getSecondaryProgress() >= 100) {
                if (gx.d != null) {
                    try {
                        bj k = gx.d.k();
                        if (k != null && k.b() == 1) {
                            seekBar.setSecondaryProgress(100);
                        }
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                }
                if (i > seekBar.getSecondaryProgress()) {
                    seekBar.setProgress(this.a);
                }
                PlayerLayoutView.this.U.sendEmptyMessage(0);
                return;
            }
            seekBar.setProgress(this.a);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.duomi.app.ui.PlayerLayoutView.a(com.duomi.app.ui.PlayerLayoutView, boolean):boolean
         arg types: [com.duomi.app.ui.PlayerLayoutView, int]
         candidates:
          com.duomi.app.ui.PlayerLayoutView.a(com.duomi.app.ui.PlayerLayoutView, int):int
          com.duomi.app.ui.PlayerLayoutView.a(android.graphics.Bitmap, android.graphics.Bitmap):void
          com.duomi.app.ui.PlayerLayoutView.a(is, int):void
          com.duomi.app.ui.PlayerLayoutView.a(int, android.view.KeyEvent):boolean
          c.a(java.lang.Class, android.os.Message):void
          c.a(boolean, android.os.Message):void
          c.a(int, android.view.KeyEvent):boolean
          c.a(int, boolean):boolean
          com.duomi.app.ui.PlayerLayoutView.a(com.duomi.app.ui.PlayerLayoutView, boolean):boolean */
        public void onStartTrackingTouch(SeekBar seekBar) {
            boolean unused = PlayerLayoutView.this.Z = true;
            this.a = seekBar.getProgress();
            PlayerLayoutView.this.U.sendEmptyMessage(0);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.duomi.app.ui.PlayerLayoutView.a(com.duomi.app.ui.PlayerLayoutView, boolean):boolean
         arg types: [com.duomi.app.ui.PlayerLayoutView, int]
         candidates:
          com.duomi.app.ui.PlayerLayoutView.a(com.duomi.app.ui.PlayerLayoutView, int):int
          com.duomi.app.ui.PlayerLayoutView.a(android.graphics.Bitmap, android.graphics.Bitmap):void
          com.duomi.app.ui.PlayerLayoutView.a(is, int):void
          com.duomi.app.ui.PlayerLayoutView.a(int, android.view.KeyEvent):boolean
          c.a(java.lang.Class, android.os.Message):void
          c.a(boolean, android.os.Message):void
          c.a(int, android.view.KeyEvent):boolean
          c.a(int, boolean):boolean
          com.duomi.app.ui.PlayerLayoutView.a(com.duomi.app.ui.PlayerLayoutView, boolean):boolean */
        public void onStopTrackingTouch(SeekBar seekBar) {
            try {
                if (gx.d != null) {
                    int progress = seekBar.getProgress();
                    int secondaryProgress = seekBar.getSecondaryProgress();
                    try {
                        if (progress != this.a) {
                            if (secondaryProgress >= 100) {
                                int k = gx.d.k().k();
                                if (k > 0) {
                                    if (progress >= k) {
                                        progress = k;
                                    }
                                    int a2 = en.a(progress, 100, k);
                                    gx.d.a(a2);
                                    seekBar.setProgress(en.a(a2, k, 100));
                                } else {
                                    seekBar.setProgress(this.a);
                                }
                            } else {
                                return;
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            boolean unused = PlayerLayoutView.this.Z = false;
            PlayerLayoutView.this.U.sendEmptyMessage(0);
        }
    };
    private DialogInterface.OnCancelListener ab = new DialogInterface.OnCancelListener() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.duomi.app.ui.PlayerLayoutView.b(com.duomi.app.ui.PlayerLayoutView, boolean):boolean
         arg types: [com.duomi.app.ui.PlayerLayoutView, int]
         candidates:
          com.duomi.app.ui.PlayerLayoutView.b(int, boolean):void
          com.duomi.app.ui.PlayerLayoutView.b(com.duomi.app.ui.PlayerLayoutView, int):boolean
          c.b(int, android.view.KeyEvent):boolean
          com.duomi.app.ui.PlayerLayoutView.b(com.duomi.app.ui.PlayerLayoutView, boolean):boolean */
        public void onCancel(DialogInterface dialogInterface) {
            boolean unused = PlayerLayoutView.this.O = false;
            PlayerLayoutView.this.B.setBackgroundResource(R.drawable.voicelayout_left_bg);
        }
    };
    /* access modifiers changed from: private */
    public Handler ac = new Handler() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: gx.a(android.content.Context, bj, bl, com.duomi.app.ui.MultiView$OnLikeReSetListener, boolean):void
         arg types: [android.content.Context, bj, bl, ?[OBJECT, ARRAY], int]
         candidates:
          gx.a(android.content.Context, bj, int, android.os.Handler, com.duomi.app.ui.MultiView$OnLikeReSetListener):void
          gx.a(android.content.Context, bl, long, com.duomi.app.ui.MultiView$OnLikeReSetListener, int):void
          gx.a(android.content.Context, bj, bl, com.duomi.app.ui.MultiView$OnLikeReSetListener, boolean):void */
        public void handleMessage(Message message) {
            switch (message.what) {
                case 255:
                    bl d = ar.a(PlayerLayoutView.this.getContext()).d(message.getData().getInt("listId"));
                    try {
                        bj k = gx.d.k();
                        if (k.b() == 1) {
                            gx.a(PlayerLayoutView.this.getContext(), d, k.f(), (MultiView.OnLikeReSetListener) null, 0);
                        } else {
                            gx.a(PlayerLayoutView.this.getContext(), k, d, (MultiView.OnLikeReSetListener) null, false);
                        }
                        Intent intent = new Intent("com.duomi.android.app.scanner.scancomplete");
                        intent.setAction("com.duomi.android.app.scanner.scancomplete");
                        PlayerLayoutView.this.getContext().sendBroadcast(intent);
                        return;
                    } catch (Exception e) {
                        e.printStackTrace();
                        return;
                    }
                default:
                    return;
            }
        }
    };
    private AdapterView.OnItemClickListener ad = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView adapterView, View view, int i, long j) {
            if (i != 3) {
                if (i != 6 && i != 0) {
                    boolean unused = PlayerLayoutView.this.a(i);
                } else if (PlayerLayoutView.this.f != null && PlayerLayoutView.this.f.getVisibility() == 0) {
                    PlayerLayoutView.this.f.setVisibility(8);
                }
                switch (i) {
                    case 0:
                        if (PlayerLayoutView.this.S != null) {
                            PlayerLayoutView.this.S.a();
                            return;
                        }
                        return;
                    case 1:
                        try {
                            if (gx.d == null || gx.d.k() == null || gx.d.j() == null) {
                                jh.a(PlayerLayoutView.this.getContext(), (int) R.string.player_song_null);
                                return;
                            }
                            gx.a(PlayerLayoutView.this.getContext(), gx.d.k(), en.d(gx.d.j().b()), PlayerLayoutView.this.ac, PlayerLayoutView.H);
                            return;
                        } catch (RemoteException e) {
                            e.printStackTrace();
                            return;
                        }
                    case 2:
                        if (!kf.b) {
                            return;
                        }
                        if (!il.b(PlayerLayoutView.this.g())) {
                            jh.a(PlayerLayoutView.this.g(), (int) R.string.app_net_error);
                            return;
                        }
                        try {
                            if (gx.d != null && gx.d.k() != null) {
                                bj k = gx.d.k();
                                if (kf.b) {
                                    new gd(PlayerLayoutView.this.g(), k);
                                    return;
                                } else {
                                    en.a(PlayerLayoutView.this.getContext(), k);
                                    return;
                                }
                            } else if (gx.d != null && gx.d.k() == null) {
                                jh.a(PlayerLayoutView.this.g(), (int) R.string.no_playing_song_to_share);
                                return;
                            } else {
                                return;
                            }
                        } catch (RemoteException e2) {
                            e2.printStackTrace();
                            return;
                        }
                    case 3:
                    case 4:
                    case 5:
                    default:
                        return;
                    case 6:
                        PlayerLayoutView.a(PlayerLayoutView.this.getContext());
                        return;
                }
            } else if (PlayerLayoutView.this.g.getVisibility() == 0) {
                PlayerLayoutView.this.j();
            } else {
                PlayerLayoutView.this.i();
            }
        }
    };
    private AdapterView.OnItemClickListener ae = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView adapterView, View view, int i, long j) {
            boolean unused = PlayerLayoutView.this.b(i);
        }
    };
    private DialogInterface.OnKeyListener af = new DialogInterface.OnKeyListener() {
        public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
            if (i == 24) {
                ad.b("PlayerLayoutView", "volume is up");
                if (!PlayerLayoutView.this.O) {
                    PlayerLayoutView.this.s();
                }
                PlayerLayoutView.this.L.b();
                return true;
            } else if (i != 25) {
                return false;
            } else {
                if (!PlayerLayoutView.this.O) {
                    PlayerLayoutView.this.s();
                }
                PlayerLayoutView.this.L.c();
                return true;
            }
        }
    };
    public ImageView x;
    public ImageView y;
    public ImageView z;

    class PlayLayoutHandler extends Handler {
        public PlayLayoutHandler(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message message) {
            switch (message.what) {
                case 1:
                    PlayerLayoutView.this.M.cancel();
                    break;
            }
            super.handleMessage(message);
        }
    }

    public PlayerLayoutView(Activity activity) {
        super(activity);
    }

    public static void a(final Context context) {
        if (gx.d != null) {
            try {
                if (gx.d.k() == null) {
                    jh.a(context, (int) R.string.player_song_null);
                    return;
                }
                final bj k = gx.d.k();
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle((int) R.string.player_report_error);
                builder.setPositiveButton((int) R.string.button_ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        StringBuffer stringBuffer = new StringBuffer();
                        if (PlayerLayoutView.G != null || PlayerLayoutView.G.length >= 3) {
                            if (PlayerLayoutView.G[0]) {
                                stringBuffer.append("song");
                            }
                            if (PlayerLayoutView.G[1]) {
                                if (PlayerLayoutView.G[0]) {
                                    stringBuffer.append(",");
                                }
                                stringBuffer.append("lyric");
                            }
                            if (PlayerLayoutView.G[2]) {
                                if ((PlayerLayoutView.G[0] && !PlayerLayoutView.G[1]) || PlayerLayoutView.G[1]) {
                                    stringBuffer.append(",");
                                }
                                stringBuffer.append("picture");
                            }
                            if (stringBuffer.length() < 2) {
                                jh.a(context, (int) R.string.player_report_error_null);
                                return;
                            }
                            final String stringBuffer2 = stringBuffer.toString();
                            if (!il.b(context) || !il.c(context)) {
                                jh.a(context, (int) R.string.app_net_error);
                                return;
                            }
                            new Thread() {
                                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                                 method: cq.a(android.content.Context, java.lang.String, boolean):java.lang.String
                                 arg types: [android.content.Context, java.lang.String, int]
                                 candidates:
                                  cq.a(android.content.Context, bj, java.lang.String):java.lang.String
                                  cq.a(android.content.Context, java.lang.String, java.lang.String):java.lang.String
                                  cq.a(java.lang.String, java.lang.String, android.content.Context):java.lang.String
                                  cq.a(android.content.Context, java.lang.String, boolean):java.lang.String */
                                public void run() {
                                    cq.a(context, cq.a(context, k, stringBuffer2), true);
                                }
                            }.start();
                            jh.a(context, (int) R.string.player_report_error_over);
                        }
                    }
                });
                builder.setNegativeButton((int) R.string.app_cancel, (DialogInterface.OnClickListener) null);
                G = new boolean[]{false, false, false};
                LinearLayout linearLayout = (LinearLayout) ((LayoutInflater) context.getSystemService("layout_inflater")).inflate((int) R.layout.select_dialog_plus, (ViewGroup) null);
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append(context.getString(R.string.player_report_currentsong));
                stringBuffer.append(k.h());
                stringBuffer.append("_");
                stringBuffer.append(k.j());
                ((TextView) linearLayout.findViewById(R.id.select_dialog_plus_name)).setText(stringBuffer.toString());
                final DMAlertController.RecycleListView recycleListView = (DMAlertController.RecycleListView) linearLayout.findViewById(R.id.select_dialog_listview);
                AnonymousClass13 r1 = new ArrayAdapter(context, R.layout.select_dialog_multichoice, R.id.text1, context.getResources().getStringArray(R.array.player_report)) {
                    public View getView(int i, View view, ViewGroup viewGroup) {
                        View view2 = super.getView(i, view, viewGroup);
                        if (PlayerLayoutView.G != null && PlayerLayoutView.G[i]) {
                            recycleListView.setItemChecked(i, true);
                        }
                        return view2;
                    }
                };
                recycleListView.setChoiceMode(2);
                recycleListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView adapterView, View view, int i, long j) {
                        if (PlayerLayoutView.G != null) {
                            PlayerLayoutView.G[i] = DMAlertController.RecycleListView.this.isItemChecked(i);
                        }
                    }
                });
                recycleListView.setAdapter((ListAdapter) r1);
                builder.setView(linearLayout);
                builder.show();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: private */
    public void b(int i, boolean z2) {
        switch (i) {
            case 0:
                this.A.setImageResource(R.drawable.mode_circleone);
                if (z2) {
                    this.T.removeMessages(0);
                    Message obtainMessage = this.T.obtainMessage(0);
                    obtainMessage.arg1 = 0;
                    obtainMessage.arg2 = R.string.player_mode_circleone;
                    this.T.sendMessageDelayed(obtainMessage, 50);
                    break;
                }
                break;
            case 1:
                this.A.setImageResource(R.drawable.mode_sequence);
                if (z2) {
                    this.T.removeMessages(0);
                    Message obtainMessage2 = this.T.obtainMessage(0);
                    obtainMessage2.arg1 = 1;
                    obtainMessage2.arg2 = R.string.player_mode_order;
                    this.T.sendMessageDelayed(obtainMessage2, 50);
                    break;
                }
                break;
            case 2:
                this.A.setImageResource(R.drawable.mode_circlelist);
                if (z2) {
                    this.T.removeMessages(0);
                    Message obtainMessage3 = this.T.obtainMessage(0);
                    obtainMessage3.arg1 = 2;
                    obtainMessage3.arg2 = R.string.player_mode_circle;
                    this.T.sendMessageDelayed(obtainMessage3, 50);
                    break;
                }
                break;
            case 3:
                this.A.setImageResource(R.drawable.mode_random);
                if (z2) {
                    this.T.removeMessages(0);
                    Message obtainMessage4 = this.T.obtainMessage(0);
                    obtainMessage4.arg1 = 3;
                    obtainMessage4.arg2 = R.string.player_mode_random;
                    this.T.sendMessageDelayed(obtainMessage4, 50);
                    break;
                }
                break;
        }
        if (z2) {
            ad.b("PlayerLayoutView", "repeateMode>>" + i);
            if (gx.d != null) {
                try {
                    be j = gx.d.j();
                    if (j != null) {
                        ad.b("PlayerLayoutView", "repeateMode>>" + i);
                        j.d(i);
                    }
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    static /* synthetic */ int c(PlayerLayoutView playerLayoutView) {
        int i = playerLayoutView.Q;
        playerLayoutView.Q = i + 1;
        return i;
    }

    private float d(int i) {
        try {
            return Float.parseFloat(en.a(i, 3, "."));
        } catch (Exception e) {
            ad.a("PlayerLayoutView", "lyric parse time error:", e);
            return 0.0f;
        }
    }

    private void r() {
        this.M = new Dialog(g());
        this.M.setOnKeyListener(this.af);
        Window window = this.M.getWindow();
        window.getAttributes();
        window.setBackgroundDrawableResource(R.drawable.none);
        window.clearFlags(2);
        this.M.setCanceledOnTouchOutside(true);
        this.M.requestWindowFeature(1);
        this.M.setContentView(this.L);
    }

    /* access modifiers changed from: private */
    public void s() {
        WindowManager.LayoutParams attributes = this.M.getWindow().getAttributes();
        attributes.x = (en.b(getContext()) - this.B.getWidth()) / 2;
        attributes.y = en.a(getContext(), this.B.getHeight());
        this.M.show();
        this.O = true;
        this.L.a();
        this.L.startAnimation(this.N);
        this.B.setBackgroundResource(R.drawable.voicelayout_up_bg);
        this.P.sendMessageDelayed(this.P.obtainMessage(1), 3000);
    }

    public void a() {
        super.a();
    }

    public void a(float f) {
        this.I.b(f);
    }

    public void a(float f, float f2, int i) {
        this.I.b(f);
        this.I.a(f2);
        if (i > 0) {
            this.I.a(i);
        }
    }

    public void a(Bitmap bitmap, Bitmap bitmap2) {
        this.x.setImageBitmap(bitmap);
        this.x.startAnimation(this.R);
        if (this.y != null && bitmap2 != null) {
            this.y.setImageBitmap(bitmap2);
            this.y.startAnimation(this.R);
        }
    }

    public void a(MultiView.OnLikeReSetListener onLikeReSetListener) {
        H = onLikeReSetListener;
    }

    public void a(hg hgVar) {
        this.S = hgVar;
    }

    public void a(is isVar) {
        this.I.setVisibility(0);
        this.J.setVisibility(8);
        this.I.b(isVar.b());
        this.I.a(isVar.c());
    }

    public void a(is isVar, int i) {
        if (gx.d != null) {
            try {
                this.E.setText(en.a(i, 1, ":"));
                int f = gx.d.f();
                bj b = gx.b();
                int g = (b == null || b.b() != 1) ? gx.d.g() : 100;
                if (gx.b() != null) {
                    f = gx.b().k();
                }
                if (f > 3000) {
                    this.F.setText(en.d(f));
                } else if (f > 0) {
                    this.F.setText(en.d(f));
                } else {
                    this.F.setText(en.d(0));
                }
                int a = en.a(i, f, 100);
                if (a >= g) {
                    a = g;
                }
                this.C.setSecondaryProgress(g);
                if (!this.Z) {
                    this.C.setProgress(a);
                    this.U.sendEmptyMessage(0);
                }
                if (isVar != null && isVar.b() != null && isVar.b().size() > 0) {
                    this.I.c(d(i));
                    this.I.invalidate();
                }
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    public void a(boolean z2) {
        Bitmap decodeResource = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.ablum_deflaut);
        this.x.setImageBitmap(decodeResource);
        if (en.b(getContext()) > 320 && en.c(getContext()) > 480) {
            int a = (int) (((double) (112.0f * en.a(getContext()))) / 1.5d);
            if (en.b(getContext()) < 800) {
                Bitmap a2 = en.a(decodeResource, a, ae.r + "/duomidefault");
                if (this.y != null) {
                    this.y.setImageBitmap(a2);
                }
            }
        }
        if (z2) {
            this.x.startAnimation(this.R);
            if (en.b(getContext()) > 320 && en.c(getContext()) > 480 && this.y != null) {
                this.y.startAnimation(this.R);
            }
        }
    }

    public boolean a(int i, KeyEvent keyEvent) {
        b(i, keyEvent);
        if (i == 4 && this.f.getVisibility() == 0 && !this.u) {
            if (this.g.getVisibility() == 0) {
                this.g.setVisibility(8);
                this.i.requestFocus();
            } else {
                this.f.setVisibility(8);
            }
            return true;
        } else if (i == 24) {
            if (!this.O) {
                s();
            }
            this.L.b();
            return true;
        } else if (i != 25) {
            return false;
        } else {
            if (!this.O) {
                s();
            }
            this.L.c();
            return true;
        }
    }

    public void b(boolean z2) {
        this.I.setVisibility(8);
        if (z2) {
            this.J.setVisibility(0);
            this.J.setText((int) R.string.player_lyric_tip_search);
        } else if (this.J.getVisibility() == 0) {
            this.J.setVisibility(8);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.duomi.app.ui.PlayerLayoutView.b(int, boolean):void
     arg types: [int, int]
     candidates:
      com.duomi.app.ui.PlayerLayoutView.b(com.duomi.app.ui.PlayerLayoutView, int):boolean
      com.duomi.app.ui.PlayerLayoutView.b(com.duomi.app.ui.PlayerLayoutView, boolean):boolean
      c.b(int, android.view.KeyEvent):boolean
      com.duomi.app.ui.PlayerLayoutView.b(int, boolean):void */
    public void c(int i) {
        if (this.Q != i) {
            this.Q = i;
            b(this.Q, false);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.duomi.app.ui.PlayerLayoutView.b(int, boolean):void
     arg types: [int, int]
     candidates:
      com.duomi.app.ui.PlayerLayoutView.b(com.duomi.app.ui.PlayerLayoutView, int):boolean
      com.duomi.app.ui.PlayerLayoutView.b(com.duomi.app.ui.PlayerLayoutView, boolean):boolean
      c.b(int, android.view.KeyEvent):boolean
      com.duomi.app.ui.PlayerLayoutView.b(int, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0245  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0259  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x02ad  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x02c3  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void f() {
        /*
            r13 = this;
            r11 = 10
            r1 = 1065353216(0x3f800000, float:1.0)
            r10 = 100
            r9 = 0
            r5 = 1
            android.app.Activity r0 = r13.g()
            r2 = 2130903090(0x7f030032, float:1.7412988E38)
            inflate(r0, r2, r13)
            r0 = 2131427596(0x7f0b010c, float:1.8476813E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.ImageView r0 = (android.widget.ImageView) r0
            r13.x = r0
            com.duomi.app.ui.PlayerLayoutView$1 r0 = new com.duomi.app.ui.PlayerLayoutView$1
            r0.<init>()
            r13.U = r0
            android.content.Context r0 = r13.getContext()
            int r0 = defpackage.en.b(r0)
            r2 = 320(0x140, float:4.48E-43)
            if (r0 <= r2) goto L_0x0066
            android.content.Context r0 = r13.getContext()
            int r0 = defpackage.en.c(r0)
            r2 = 480(0x1e0, float:6.73E-43)
            if (r0 <= r2) goto L_0x0066
            r0 = 2131427598(0x7f0b010e, float:1.8476817E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.ImageView r0 = (android.widget.ImageView) r0
            r13.y = r0
            r0 = 2131427597(0x7f0b010d, float:1.8476815E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.ImageView r0 = (android.widget.ImageView) r0
            r13.z = r0
            android.widget.ImageView r0 = r13.x
            r2 = 2130837505(0x7f020001, float:1.7279966E38)
            r0.setBackgroundResource(r2)
            android.widget.ImageView r0 = r13.z
            if (r0 == 0) goto L_0x0066
            android.widget.ImageView r0 = r13.z
            r2 = 2130837506(0x7f020002, float:1.7279968E38)
            r0.setImageResource(r2)
        L_0x0066:
            android.content.Context r0 = r13.getContext()
            int r0 = defpackage.en.c(r0)
            switch(r0) {
                case 800: goto L_0x0266;
                default: goto L_0x0071;
            }
        L_0x0071:
            r0 = 2131427594(0x7f0b010a, float:1.8476809E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.ImageView r0 = (android.widget.ImageView) r0
            r13.A = r0
            r0 = 2131427595(0x7f0b010b, float:1.847681E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.ImageView r0 = (android.widget.ImageView) r0
            r13.B = r0
            com.duomi.core.view.VolumeView r0 = new com.duomi.core.view.VolumeView
            android.app.Activity r2 = r13.g()
            r0.<init>(r2)
            r13.L = r0
            r0 = 2131427599(0x7f0b010f, float:1.8476819E38)
            android.view.View r0 = r13.findViewById(r0)
            com.duomi.core.view.LyricView r0 = (com.duomi.core.view.LyricView) r0
            r13.I = r0
            r0 = 2131427600(0x7f0b0110, float:1.847682E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            r13.J = r0
            android.widget.ImageView r0 = r13.B
            r2 = 2130837838(0x7f02014e, float:1.7280641E38)
            r0.setImageResource(r2)
            android.widget.ImageView r0 = r13.B
            r2 = 2130837840(0x7f020150, float:1.7280645E38)
            r0.setBackgroundResource(r2)
            android.widget.ImageView r0 = r13.A
            android.view.View$OnTouchListener r2 = r13.X
            r0.setOnTouchListener(r2)
            r0 = 2131427593(0x7f0b0109, float:1.8476807E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.SeekBar r0 = (android.widget.SeekBar) r0
            r13.C = r0
            r0 = 2131427590(0x7f0b0106, float:1.84768E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.RelativeLayout r0 = (android.widget.RelativeLayout) r0
            r13.D = r0
            r0 = 2131427591(0x7f0b0107, float:1.8476803E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            r13.E = r0
            r0 = 2131427592(0x7f0b0108, float:1.8476805E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            r13.F = r0
            android.widget.RelativeLayout r0 = r13.D
            r2 = 2130837769(0x7f020109, float:1.7280501E38)
            r0.setBackgroundResource(r2)
            android.widget.SeekBar r0 = r13.C
            r2 = 2130837763(0x7f020103, float:1.728049E38)
            r0.setBackgroundResource(r2)
            android.widget.SeekBar r0 = r13.C
            android.content.res.Resources r2 = r13.getResources()
            r3 = 2130837765(0x7f020105, float:1.7280493E38)
            android.graphics.drawable.Drawable r2 = r2.getDrawable(r3)
            r0.setProgressDrawable(r2)
            android.widget.SeekBar r0 = r13.C
            android.content.res.Resources r2 = r13.getResources()
            r3 = 2130837829(0x7f020145, float:1.7280623E38)
            android.graphics.drawable.Drawable r2 = r2.getDrawable(r3)
            r0.setThumb(r2)
            android.widget.SeekBar r0 = r13.C
            r0.setThumbOffset(r9)
            android.widget.SeekBar r0 = r13.C
            r0.setMax(r10)
            android.content.Context r0 = r13.getContext()
            int r0 = defpackage.en.b(r0)
            switch(r0) {
                case 240: goto L_0x0274;
                case 320: goto L_0x0283;
                case 480: goto L_0x0291;
                default: goto L_0x0130;
            }
        L_0x0130:
            r2 = 20
            r3 = 24
            r13.K = r3
            com.duomi.core.view.LyricView r3 = r13.I
            r4 = 700(0x2bc, float:9.81E-43)
            r3.c(r4)
        L_0x013d:
            com.duomi.core.view.LyricView r3 = r13.I
            r3.b(r0)
            com.duomi.core.view.LyricView r0 = r13.I
            r3 = 2
            r0.d(r3)
            com.duomi.core.view.LyricView r0 = r13.I
            int r3 = r13.K
            float r3 = (float) r3
            float r2 = (float) r2
            r0.a(r3, r2)
            r13.r()
            android.view.animation.ScaleAnimation r0 = new android.view.animation.ScaleAnimation
            r3 = 1045220557(0x3e4ccccd, float:0.2)
            r6 = 1056964608(0x3f000000, float:0.5)
            r8 = 0
            r2 = r1
            r4 = r1
            r7 = r5
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8)
            r13.N = r0
            android.view.animation.ScaleAnimation r0 = r13.N
            r1 = 500(0x1f4, double:2.47E-321)
            r0.setDuration(r1)
            android.view.animation.ScaleAnimation r0 = r13.N
            r0.setFillAfter(r5)
            android.widget.ImageView r0 = r13.B
            android.view.View$OnTouchListener r1 = r13.V
            r0.setOnTouchListener(r1)
            android.app.Dialog r0 = r13.M
            android.content.DialogInterface$OnCancelListener r1 = r13.ab
            r0.setOnCancelListener(r1)
            com.duomi.core.view.VolumeView r0 = r13.L
            jw r1 = r13.W
            r0.a(r1)
            com.duomi.app.ui.PlayerLayoutView$PlayLayoutHandler r0 = new com.duomi.app.ui.PlayerLayoutView$PlayLayoutHandler
            android.app.Activity r1 = r13.g()
            android.os.Looper r1 = r1.getMainLooper()
            r0.<init>(r1)
            r13.P = r0
            com.duomi.android.app.media.IMusicService r0 = defpackage.gx.d     // Catch:{ RemoteException -> 0x02a0 }
            if (r0 == 0) goto L_0x01c6
            com.duomi.android.app.media.IMusicService r0 = defpackage.gx.d     // Catch:{ RemoteException -> 0x02a0 }
            be r0 = r0.j()     // Catch:{ RemoteException -> 0x02a0 }
            if (r0 == 0) goto L_0x01c6
            com.duomi.android.app.media.IMusicService r0 = defpackage.gx.d     // Catch:{ RemoteException -> 0x02a0 }
            be r0 = r0.j()     // Catch:{ RemoteException -> 0x02a0 }
            int r0 = r0.f()     // Catch:{ RemoteException -> 0x02a0 }
            r13.Q = r0     // Catch:{ RemoteException -> 0x02a0 }
            java.lang.String r0 = "PlayerLayoutView"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ RemoteException -> 0x02a0 }
            r1.<init>()     // Catch:{ RemoteException -> 0x02a0 }
            java.lang.String r2 = "playmode>>"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ RemoteException -> 0x02a0 }
            int r2 = r13.Q     // Catch:{ RemoteException -> 0x02a0 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ RemoteException -> 0x02a0 }
            java.lang.String r1 = r1.toString()     // Catch:{ RemoteException -> 0x02a0 }
            defpackage.ad.b(r0, r1)     // Catch:{ RemoteException -> 0x02a0 }
        L_0x01c6:
            int r0 = r13.Q
            r13.b(r0, r9)
            android.widget.ImageView r0 = r13.A
            r1 = 2130837729(0x7f0200e1, float:1.728042E38)
            r0.setBackgroundResource(r1)
            r13.h()
            android.content.Context r0 = r13.getContext()
            r1 = 2130968578(0x7f040002, float:1.7545814E38)
            android.view.animation.Animation r0 = android.view.animation.AnimationUtils.loadAnimation(r0, r1)
            r13.R = r0
            com.duomi.app.ui.PlayerLayoutView$2 r0 = new com.duomi.app.ui.PlayerLayoutView$2
            r0.<init>()
            r13.T = r0
            android.widget.SeekBar r0 = r13.C
            android.widget.SeekBar$OnSeekBarChangeListener r1 = r13.aa
            r0.setOnSeekBarChangeListener(r1)
            bj r0 = defpackage.gx.b()
            if (r0 == 0) goto L_0x0265
            bj r0 = defpackage.gx.b()
            java.lang.String r0 = r0.h()
            android.content.Context r1 = r13.getContext()
            as r1 = defpackage.as.a(r1)
            java.lang.String r1 = r1.ae()
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x0265
            android.content.Context r0 = r13.getContext()
            as r0 = defpackage.as.a(r0)
            int r0 = r0.ad()
            java.lang.String r1 = ":"
            java.lang.String r1 = defpackage.en.a(r0, r5, r1)
            android.widget.TextView r2 = r13.E
            r2.setText(r1)
            bj r1 = defpackage.gx.b()     // Catch:{ Exception -> 0x02a6 }
            int r1 = r1.k()     // Catch:{ Exception -> 0x02a6 }
            bj r2 = defpackage.gx.b()     // Catch:{ Exception -> 0x02d5 }
            int r2 = r2.b()     // Catch:{ Exception -> 0x02d5 }
            if (r2 != r5) goto L_0x0241
            android.widget.SeekBar r2 = r13.C     // Catch:{ Exception -> 0x02d5 }
            r3 = 100
            r2.setSecondaryProgress(r3)     // Catch:{ Exception -> 0x02d5 }
        L_0x0241:
            r2 = 3000(0xbb8, float:4.204E-42)
            if (r1 <= r2) goto L_0x02ad
            android.widget.TextView r2 = r13.F
            java.lang.String r3 = defpackage.en.d(r1)
            r2.setText(r3)
        L_0x024e:
            int r0 = defpackage.en.a(r0, r1, r10)
            android.widget.SeekBar r1 = r13.C
            r1.setProgress(r0)
            if (r0 >= r11) goto L_0x02c3
            android.widget.SeekBar r1 = r13.C
            int r0 = r0 + 1
            r1.setThumbOffset(r0)
        L_0x0260:
            android.widget.SeekBar r0 = r13.C
            r0.invalidate()
        L_0x0265:
            return
        L_0x0266:
            android.widget.ImageView r0 = r13.x
            android.view.ViewGroup$LayoutParams r0 = r0.getLayoutParams()
            android.widget.RelativeLayout$LayoutParams r0 = (android.widget.RelativeLayout.LayoutParams) r0
            r2 = -16
            r0.topMargin = r2
            goto L_0x0071
        L_0x0274:
            r2 = 8
            com.duomi.core.view.LyricView r3 = r13.I
            r4 = 45
            r3.c(r4)
            r3 = 14
            r13.K = r3
            goto L_0x013d
        L_0x0283:
            com.duomi.core.view.LyricView r2 = r13.I
            r3 = 54
            r2.c(r3)
            r2 = 18
            r13.K = r2
            r2 = r11
            goto L_0x013d
        L_0x0291:
            r2 = 20
            r3 = 24
            r13.K = r3
            com.duomi.core.view.LyricView r3 = r13.I
            r4 = 86
            r3.c(r4)
            goto L_0x013d
        L_0x02a0:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x01c6
        L_0x02a6:
            r1 = move-exception
            r2 = r9
        L_0x02a8:
            r1.printStackTrace()
            r1 = r2
            goto L_0x0241
        L_0x02ad:
            if (r1 <= 0) goto L_0x02b9
            android.widget.TextView r2 = r13.F
            java.lang.String r3 = defpackage.en.d(r1)
            r2.setText(r3)
            goto L_0x024e
        L_0x02b9:
            android.widget.TextView r2 = r13.F
            java.lang.String r3 = defpackage.en.d(r9)
            r2.setText(r3)
            goto L_0x024e
        L_0x02c3:
            r1 = 90
            if (r0 <= r1) goto L_0x02cf
            android.widget.SeekBar r1 = r13.C
            int r0 = r10 - r0
            r1.setThumbOffset(r0)
            goto L_0x0260
        L_0x02cf:
            android.widget.SeekBar r0 = r13.C
            r0.setThumbOffset(r11)
            goto L_0x0260
        L_0x02d5:
            r2 = move-exception
            r12 = r2
            r2 = r1
            r1 = r12
            goto L_0x02a8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.duomi.app.ui.PlayerLayoutView.f():void");
    }

    public void m() {
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.h.getLayoutParams();
        if (en.b(getContext()) > 239 && en.b(getContext()) < 241) {
            layoutParams.bottomMargin = 39;
        }
        if (en.b(getContext()) > 319 && en.b(getContext()) < 321) {
            layoutParams.bottomMargin = 49;
        }
        if (en.b(getContext()) > 479 && en.b(getContext()) < 481) {
            layoutParams.bottomMargin = 84;
        }
        this.h.setLayoutParams(layoutParams);
    }

    public void n() {
        this.i.setOnItemClickListener(this.ad);
        this.j.setOnItemClickListener(this.ae);
    }

    public void o() {
        this.q = getResources().getStringArray(R.array.Player_menu);
        this.r = getResources().getStringArray(R.array.second_menu);
        this.s = new ArrayList();
        this.s.add(Integer.valueOf((int) R.drawable.meun_search));
        this.s.add(Integer.valueOf((int) R.drawable.meun_addto));
        if (kf.b) {
            this.s.add(Integer.valueOf((int) R.drawable.meun_sina));
        } else {
            this.s.add(Integer.valueOf((int) R.drawable.menu_bg));
        }
        this.s.add(Integer.valueOf((int) R.drawable.meun_more));
        this.s.add(Integer.valueOf((int) R.drawable.meun_setup));
        this.s.add(Integer.valueOf((int) R.drawable.meun_sleep));
        this.s.add(Integer.valueOf((int) R.drawable.meun_songwrong));
        this.s.add(Integer.valueOf((int) R.drawable.meun_exit));
        this.r = getResources().getStringArray(R.array.second_menu2);
        this.t = new ArrayList();
        this.t.add(Integer.valueOf((int) R.drawable.meun_help));
        this.t.add(Integer.valueOf((int) R.drawable.meun_advice));
        this.t.add(Integer.valueOf((int) R.drawable.meun_friends));
        this.t.add(Integer.valueOf((int) R.drawable.meun_about));
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        return motionEvent.getAction() == 0;
    }

    public void q() {
        this.C.setProgress(0);
        this.C.setSecondaryProgress(0);
        if (gx.d != null) {
            try {
                bj k = gx.d.k();
                if (k != null && k.b() == 1) {
                    this.C.setSecondaryProgress(100);
                }
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        this.C.setThumbOffset(1);
        this.E.setText("0:00");
        this.F.setText("0:00");
    }
}
