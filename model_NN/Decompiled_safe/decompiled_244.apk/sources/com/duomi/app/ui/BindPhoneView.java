package com.duomi.app.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import com.duomi.android.R;

public class BindPhoneView extends c {
    /* access modifiers changed from: private */
    public static String A;
    /* access modifiers changed from: private */
    public static long B;
    private RelativeLayout x;
    /* access modifiers changed from: private */
    public String[] y;
    private Handler z;

    class RequestSessionId extends AsyncTask {
        private String b;
        private ProgressDialog c;

        public RequestSessionId(String str) {
            this.b = str;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: cq.a(boolean, android.content.Context, int, java.lang.String, java.lang.String, bn):java.lang.String
         arg types: [int, android.app.Activity, int, ?[OBJECT, ARRAY], ?[OBJECT, ARRAY], ?[OBJECT, ARRAY]]
         candidates:
          cq.a(java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, android.content.Context):java.lang.String
          cq.a(boolean, android.content.Context, int, java.lang.String, java.lang.String, bn):java.lang.String */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: cq.a(boolean, android.content.Context, int, java.lang.String, java.lang.String, bn):java.lang.String
         arg types: [int, android.app.Activity, int, java.lang.String, java.lang.String, ?[OBJECT, ARRAY]]
         candidates:
          cq.a(java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, android.content.Context):java.lang.String
          cq.a(boolean, android.content.Context, int, java.lang.String, java.lang.String, bn):java.lang.String */
        /* access modifiers changed from: protected */
        /* renamed from: a */
        public Integer doInBackground(Object... objArr) {
            if (bn.a().e() != null && !bn.a().e().trim().equals("")) {
                return 1;
            }
            if (il.b(BindPhoneView.this.getContext())) {
                String f = bn.a().f();
                String g = bn.a().g();
                try {
                    if (!il.b(BindPhoneView.this.getContext())) {
                        jh.a(BindPhoneView.this.getContext(), (int) R.string.app_net_error);
                        return -1;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                String a2 = cq.a(true, (Context) BindPhoneView.this.g(), 3, (String) null, (String) null, (bn) null);
                Log.d("BindPhoneView", "xmlData>>>>>>" + a2);
                cp.a(BindPhoneView.this.g(), a2);
                String a3 = cq.a(true, (Context) BindPhoneView.this.g(), 4, f, g, (bn) null);
                Log.d("BindPhoneView", "xmlData>>>>>>" + a3);
                if (a3 == null) {
                    return -1;
                }
                return Integer.valueOf(cp.a(BindPhoneView.this.g(), a3, (Handler) null));
            }
            jh.a(BindPhoneView.this.getContext(), (int) R.string.app_net_error);
            return -1;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onPostExecute(Integer num) {
            if (this.c != null) {
                this.c.dismiss();
            }
            if (num.intValue() < 0) {
                jh.a(BindPhoneView.this.getContext(), (int) R.string.setting_getsid_err);
            } else {
                BindPhoneView.this.a(this.b);
            }
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            if (bn.a().e() != null && !bn.a().e().trim().equals("")) {
                this.c = new ProgressDialog(BindPhoneView.this.getContext());
                this.c.getWindow().requestFeature(1);
                this.c.setMessage(BindPhoneView.this.getContext().getResources().getString(R.string.setting_bind_wait));
                this.c.setCancelable(false);
                this.c.show();
            }
        }
    }

    public class SmsObserver extends ContentObserver {
        private static final String[] c = {"address", "person", "date", "type", "body"};
        private Context a;
        private Handler b;

        public SmsObserver(Context context, Handler handler) {
            super(handler);
            this.a = context;
            this.b = handler;
        }

        /* JADX WARNING: Removed duplicated region for block: B:22:0x007a  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onChange(boolean r9) {
            /*
                r8 = this;
                r6 = 0
                android.content.Context r0 = r8.a     // Catch:{ Exception -> 0x006b, all -> 0x0076 }
                android.content.ContentResolver r0 = r0.getContentResolver()     // Catch:{ Exception -> 0x006b, all -> 0x0076 }
                java.lang.String r1 = "content://sms/sent"
                android.net.Uri r1 = android.net.Uri.parse(r1)     // Catch:{ Exception -> 0x006b, all -> 0x0076 }
                java.lang.String[] r2 = com.duomi.app.ui.BindPhoneView.SmsObserver.c     // Catch:{ Exception -> 0x006b, all -> 0x0076 }
                java.lang.String r3 = "address=? "
                r4 = 1
                java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x006b, all -> 0x0076 }
                r5 = 0
                java.lang.String r7 = com.duomi.app.ui.BindPhoneView.A     // Catch:{ Exception -> 0x006b, all -> 0x0076 }
                r4[r5] = r7     // Catch:{ Exception -> 0x006b, all -> 0x0076 }
                java.lang.String r5 = "date desc"
                android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x006b, all -> 0x0076 }
            L_0x0021:
                boolean r0 = r1.moveToNext()     // Catch:{ Exception -> 0x0080 }
                if (r0 == 0) goto L_0x0060
                r0 = 2
                long r2 = r1.getLong(r0)     // Catch:{ Exception -> 0x0080 }
                long r4 = com.duomi.app.ui.BindPhoneView.B     // Catch:{ Exception -> 0x0080 }
                int r0 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                if (r0 <= 0) goto L_0x0021
                bn r2 = defpackage.bn.a()     // Catch:{ Exception -> 0x0080 }
                android.content.Context r0 = r8.a     // Catch:{ Exception -> 0x0080 }
                java.util.Map r0 = defpackage.en.k(r0)     // Catch:{ Exception -> 0x0080 }
                java.lang.String[] r3 = defpackage.eu.a     // Catch:{ Exception -> 0x0080 }
                r4 = 1
                r3 = r3[r4]     // Catch:{ Exception -> 0x0080 }
                java.lang.Object r0 = r0.get(r3)     // Catch:{ Exception -> 0x0080 }
                java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x0080 }
                r2.k(r0)     // Catch:{ Exception -> 0x0080 }
                android.content.Context r0 = r8.a     // Catch:{ Exception -> 0x0080 }
                aw r0 = defpackage.aw.a(r0)     // Catch:{ Exception -> 0x0080 }
                r0.a(r2)     // Catch:{ Exception -> 0x0080 }
                android.os.Handler r0 = r8.b     // Catch:{ Exception -> 0x0080 }
                r2 = 34
                android.os.Message r0 = r0.obtainMessage(r2)     // Catch:{ Exception -> 0x0080 }
                r0.sendToTarget()     // Catch:{ Exception -> 0x0080 }
            L_0x0060:
                if (r1 == 0) goto L_0x0065
                r1.close()     // Catch:{ Exception -> 0x0080 }
            L_0x0065:
                if (r1 == 0) goto L_0x006a
                r1.close()
            L_0x006a:
                return
            L_0x006b:
                r0 = move-exception
                r1 = r6
            L_0x006d:
                r0.printStackTrace()     // Catch:{ all -> 0x007e }
                if (r1 == 0) goto L_0x006a
                r1.close()
                goto L_0x006a
            L_0x0076:
                r0 = move-exception
                r1 = r6
            L_0x0078:
                if (r1 == 0) goto L_0x007d
                r1.close()
            L_0x007d:
                throw r0
            L_0x007e:
                r0 = move-exception
                goto L_0x0078
            L_0x0080:
                r0 = move-exception
                goto L_0x006d
            */
            throw new UnsupportedOperationException("Method not decompiled: com.duomi.app.ui.BindPhoneView.SmsObserver.onChange(boolean):void");
        }
    }

    class mServiceReceiver extends BroadcastReceiver {
        private mServiceReceiver() {
        }

        /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
        public void onReceive(Context context, Intent intent) {
            Log.i("sendsms", "recevie");
            try {
                switch (getResultCode()) {
                    case -1:
                        Log.d("sendsms", "ok");
                        bn a = bn.a();
                        a.k((String) en.k(context).get(eu.a[1]));
                        aw.a(context).a(a);
                        return;
                    case 0:
                    case 2:
                    default:
                        return;
                    case 1:
                        Log.d("sendsms", "fail");
                        return;
                }
            } catch (Exception e) {
                e.getStackTrace();
            }
            e.getStackTrace();
        }
    }

    public BindPhoneView(Activity activity) {
        super(activity);
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        Uri parse = Uri.parse("smsto:" + str);
        A = str;
        Intent intent = new Intent("android.intent.action.SENDTO", parse);
        intent.putExtra("sms_body", getContext().getString(R.string.setting_sms_tips).replace("%sId", bn.a().e()).replace("%tgid", as.a(getContext()).C()).replace("%ver", as.a(getContext()).B()));
        B = System.currentTimeMillis();
        g().startActivityForResult(intent, 1);
    }

    public void a(Message message) {
        super.a(message);
        q();
    }

    public boolean a(int i, KeyEvent keyEvent) {
        if (i != 4) {
            return false;
        }
        p.a(g()).a(SettingView.class.getName());
        return true;
    }

    /* access modifiers changed from: protected */
    public void f() {
        inflate(g(), R.layout.bind_phone_layout, this);
        this.x = (RelativeLayout) findViewById(R.id.go_back_layout);
        q();
    }

    public void q() {
        Button button = (Button) findViewById(R.id.mobile_btn);
        Button button2 = (Button) findViewById(R.id.liantong_btn);
        Button button3 = (Button) findViewById(R.id.dianxin_btn);
        if (cp.a != null) {
            this.y = cp.a.split(",");
        }
        if (this.y != null) {
            switch (this.y.length) {
                case 1:
                    button.setVisibility(0);
                    break;
                case 2:
                    button.setVisibility(0);
                    button2.setVisibility(0);
                    break;
                case 3:
                    button.setVisibility(0);
                    button2.setVisibility(0);
                    button3.setVisibility(0);
                    break;
            }
        }
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                new RequestSessionId(BindPhoneView.this.y[0]).execute(new Object[0]);
            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                new RequestSessionId(BindPhoneView.this.y[1]).execute(new Object[0]);
            }
        });
        button3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                new RequestSessionId(BindPhoneView.this.y[2]).execute(new Object[0]);
            }
        });
        this.z = new Handler() {
            public void handleMessage(Message message) {
                switch (message.what) {
                    case 0:
                        jh.a(BindPhoneView.this.g(), (String) message.obj);
                        return;
                    default:
                        return;
                }
            }
        };
        this.x.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                p.a(BindPhoneView.this.g()).a(SettingView.class.getName());
            }
        });
    }
}
