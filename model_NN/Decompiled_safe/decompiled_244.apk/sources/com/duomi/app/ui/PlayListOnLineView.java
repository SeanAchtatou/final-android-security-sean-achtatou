package com.duomi.app.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.duomi.android.R;
import com.duomi.app.ui.PlayListGroupView;
import java.io.File;
import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PlayListOnLineView extends c implements jt {
    /* access modifiers changed from: private */
    public static int K;
    /* access modifiers changed from: private */
    public static int ac = 0;
    /* access modifiers changed from: private */
    public static int ad = 0;
    /* access modifiers changed from: private */
    public static boolean ae = false;
    /* access modifiers changed from: private */
    public static boolean af = false;
    /* access modifiers changed from: private */
    public static int ag = 0;
    /* access modifiers changed from: private */
    public static boolean ah = false;
    hx A;
    cu B;
    as C;
    public Runnable D = new Runnable() {
        public void run() {
            Bitmap a2;
            int b = en.b(PlayListOnLineView.this.S);
            for (int i = 0; i < PlayListOnLineView.this.x.size(); i++) {
                try {
                    String str = PlayListOnLineView.this.x.get(i) instanceof cr ? ae.q + "/" + en.j(((cr) PlayListOnLineView.this.x.get(i)).c()) : ae.q + "/" + en.j(((cv) PlayListOnLineView.this.x.get(i)).b());
                    ad.b("PlayListOnLineView", "ticker pic " + i + " >>>" + str);
                    if (new File(str).exists()) {
                        a2 = BitmapFactory.decodeFile(str);
                    } else {
                        String c = PlayListOnLineView.this.x.get(i) instanceof cr ? ((cr) PlayListOnLineView.this.x.get(i)).c() : ((cv) PlayListOnLineView.this.x.get(i)).b();
                        a2 = il.c(PlayListOnLineView.this.S) ? il.a(en.c(PlayListOnLineView.this.S, c, b), PlayListOnLineView.this.S, false, c) : null;
                    }
                    if (a2 != null) {
                        Bundle bundle = new Bundle();
                        bundle.putInt("position", i);
                        Message message = new Message();
                        message.what = 4;
                        message.obj = a2;
                        message.setData(bundle);
                        PlayListOnLineView.this.ai.sendMessage(message);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    };
    HashMap E = new HashMap();
    Bitmap F = BitmapFactory.decodeResource(getResources(), R.drawable.list_arrow_icon);
    /* access modifiers changed from: private */
    public TextView G;
    /* access modifiers changed from: private */
    public TextView H;
    /* access modifiers changed from: private */
    public TextView I;
    /* access modifiers changed from: private */
    public TextView J;
    private LinearLayout L;
    /* access modifiers changed from: private */
    public LinearLayout M;
    /* access modifiers changed from: private */
    public LinearLayout N;
    private LinearLayout O;
    /* access modifiers changed from: private */
    public ListView P;
    /* access modifiers changed from: private */
    public ScrollGallery Q;
    /* access modifiers changed from: private */
    public ImageView[] R;
    /* access modifiers changed from: private */
    public Context S;
    /* access modifiers changed from: private */
    public int T;
    /* access modifiers changed from: private */
    public int U;
    /* access modifiers changed from: private */
    public Handler V;
    /* access modifiers changed from: private */
    public EventHandler W;
    /* access modifiers changed from: private */
    public ScrollThread X;
    /* access modifiers changed from: private */
    public int Y;
    /* access modifiers changed from: private */
    public float Z;
    /* access modifiers changed from: private */
    public float aa;
    /* access modifiers changed from: private */
    public MyAdapter ab;
    /* access modifiers changed from: private */
    public Handler ai = new Handler() {
        public void handleMessage(Message message) {
            String str;
            String str2;
            Class<PlayListOnLineView> cls = PlayListOnLineView.class;
            switch (message.what) {
                case 0:
                    if (PlayListOnLineView.this.ab == null) {
                        MyAdapter unused = PlayListOnLineView.this.ab = new MyAdapter(PlayListOnLineView.this.S, PlayListOnLineView.this.y);
                        PlayListOnLineView.this.P.setAdapter((ListAdapter) PlayListOnLineView.this.ab);
                        return;
                    }
                    PlayListOnLineView.this.ab.notifyDataSetChanged();
                    return;
                case 1:
                    PlayListOnLineView.this.w();
                    ((PlayListGroupView) PlayListOnLineView.this.d).q();
                    return;
                case 2:
                    if (!(PlayListOnLineView.this.y.get(PlayListOnLineView.ac) instanceof cr)) {
                        Message message2 = new Message();
                        message2.obj = PlayListOnLineView.this.y.get(PlayListOnLineView.ac);
                        try {
                            str2 = ((ct) PlayListOnLineView.this.A.a(en.k(((cw) PlayListOnLineView.this.y.get(PlayListOnLineView.ac)).e())).b()).d();
                        } catch (Exception e) {
                            e.printStackTrace();
                            str2 = "";
                        }
                        co.a().a(str2);
                        if (gx.b(((cw) PlayListOnLineView.this.y.get(PlayListOnLineView.ac)).e())) {
                            ((PlayListGroupView) PlayListOnLineView.this.d).a(new PlayListGroupView.OnLineInfo((cw) PlayListOnLineView.this.y.get(PlayListOnLineView.ac), PlayListOnLineView.this.P.getFirstVisiblePosition()));
                            PlayListOnLineView.this.d.a(PlayListOnLineMusicView.class, message2);
                            return;
                        }
                        ((PlayListGroupView) PlayListOnLineView.this.d).a(new PlayListGroupView.OnLineInfo((cw) PlayListOnLineView.this.y.get(PlayListOnLineView.ac), PlayListOnLineView.this.P.getFirstVisiblePosition()));
                        PlayListOnLineView.this.d.a(PlayListOnLineSecondView.class, message2);
                        return;
                    }
                    return;
                case 3:
                    if (!(PlayListOnLineView.this.x.get(PlayListOnLineView.ad) instanceof cr)) {
                        Message message3 = new Message();
                        message3.obj = PlayListOnLineView.this.x.get(PlayListOnLineView.ad);
                        try {
                            str = ((ct) PlayListOnLineView.this.A.a(en.k(((cw) PlayListOnLineView.this.x.get(PlayListOnLineView.ac)).e())).b()).d();
                        } catch (Exception e2) {
                            e2.printStackTrace();
                            str = "";
                        }
                        co.a().a(str);
                        if (gx.b(((cw) PlayListOnLineView.this.x.get(PlayListOnLineView.ad)).e())) {
                            ((PlayListGroupView) PlayListOnLineView.this.d).a(new PlayListGroupView.OnLineInfo((cw) PlayListOnLineView.this.x.get(PlayListOnLineView.ad), PlayListOnLineView.this.P.getFirstVisiblePosition()));
                            PlayListOnLineView.this.d.a(PlayListOnLineMusicView.class, message3);
                            return;
                        }
                        ((PlayListGroupView) PlayListOnLineView.this.d).a(new PlayListGroupView.OnLineInfo((cw) PlayListOnLineView.this.x.get(PlayListOnLineView.ad), PlayListOnLineView.this.P.getFirstVisiblePosition()));
                        PlayListOnLineView.this.d.a(PlayListOnLineSecondView.class, message3);
                        return;
                    }
                    return;
                case 4:
                    PlayListOnLineView.this.R[message.getData().getInt("position")].setImageBitmap((Bitmap) message.obj);
                    return;
                case 5:
                    if (PlayListOnLineView.this.Q != null) {
                        PlayListOnLineView.this.Q.b();
                        ImageView[] unused2 = PlayListOnLineView.this.R = new ImageView[PlayListOnLineView.this.x.size()];
                        int b = en.b(PlayListOnLineView.this.S);
                        for (final int i = 0; i < PlayListOnLineView.this.x.size(); i++) {
                            PlayListOnLineView.this.R[i] = new ImageView(PlayListOnLineView.this.S);
                            PlayListOnLineView.this.R[i].setImageResource(R.drawable.defaul_tiker);
                            switch (b) {
                                case 240:
                                    PlayListOnLineView.this.Q.a(2, 2, 2, 2);
                                    break;
                                case 320:
                                    PlayListOnLineView.this.Q.a(2, 2, 2, 2);
                                    break;
                                case 480:
                                    PlayListOnLineView.this.Q.a(3, 3, 3, 3);
                                    break;
                                default:
                                    PlayListOnLineView.this.Q.a(3, 3, 3, 3);
                                    break;
                            }
                            PlayListOnLineView.this.Q.a(PlayListOnLineView.this.R[i]);
                            PlayListOnLineView.this.R[i].setOnClickListener(new View.OnClickListener() {
                                public void onClick(View view) {
                                    PlayListOnLineView.this.Q.a(view, i);
                                }
                            });
                        }
                        new Thread(PlayListOnLineView.this.D).start();
                        return;
                    }
                    return;
                case 6:
                    PlayListOnLineView.this.y();
                    return;
                case 7:
                    MyAdapter unused3 = PlayListOnLineView.this.ab = new MyAdapter(PlayListOnLineView.this.S, PlayListOnLineView.this.y);
                    PlayListOnLineView.this.P.setAdapter((ListAdapter) PlayListOnLineView.this.ab);
                    if (PlayListOnLineView.this.Q == null) {
                        return;
                    }
                    if (PlayListOnLineView.K == 0) {
                        PlayListOnLineView.this.Q.setVisibility(0);
                        return;
                    } else {
                        PlayListOnLineView.this.Q.setVisibility(8);
                        return;
                    }
                case 8:
                    Class<PlayListOnLineView> cls2 = PlayListOnLineView.class;
                    PlayListOnLineView.this.d.a(cls, (Message) null);
                    jh.a(PlayListOnLineView.this.S, (int) R.string.app_net_error);
                    return;
                case 9:
                    ad.b("PlayListOnLineView", "show dialog>>>>>>>>>>>>>>>>>>.");
                    PlayListOnLineView.this.d.a(DialogView.class, (Message) null);
                    return;
                case 10:
                    ad.b("PlayListOnLineView", "dismiss dialog>>>>>>>>>>>>>>>.");
                    Class<PlayListOnLineView> cls3 = PlayListOnLineView.class;
                    PlayListOnLineView.this.d.a(cls, (Message) null);
                    return;
                case 11:
                default:
                    return;
                case 12:
                    PlayListOnLineView.this.z();
                    Class<PlayListOnLineView> cls4 = PlayListOnLineView.class;
                    PlayListOnLineView.this.d.a(cls, (Message) null);
                    jh.a(PlayListOnLineView.this.S, (int) R.string.app_data_error);
                    return;
                case 13:
                    PlayListOnLineView.this.z();
                    Class<PlayListOnLineView> cls5 = PlayListOnLineView.class;
                    PlayListOnLineView.this.d.a(cls, (Message) null);
                    jh.a(PlayListOnLineView.this.S, (int) R.string.app_service_unable);
                    return;
                case 14:
                    if (PlayListOnLineView.this.M.getVisibility() == 8) {
                        PlayListOnLineView.this.M.setVisibility(0);
                        PlayListOnLineView.this.N.setVisibility(8);
                        return;
                    }
                    return;
                case 252:
                    Class<PlayListOnLineView> cls6 = PlayListOnLineView.class;
                    PlayListOnLineView.this.d.a(cls, (Message) null);
                    jh.a(PlayListOnLineView.this.S, (int) R.string.app_service_unable);
                    return;
                case 253:
                    PlayListOnLineView.this.z();
                    Class<PlayListOnLineView> cls7 = PlayListOnLineView.class;
                    PlayListOnLineView.this.d.a(cls, (Message) null);
                    jh.a(PlayListOnLineView.this.S, (int) R.string.app_net_error);
                    return;
                case 254:
                    if (!as.a(PlayListOnLineView.this.getContext()).n()) {
                        PopDialogView.a(PlayListOnLineView.this.getContext());
                        return;
                    }
                    return;
            }
        }
    };
    private AdapterView.OnItemClickListener aj = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView adapterView, View view, int i, long j) {
            if (i == 3) {
                if (PlayListOnLineView.this.g.getVisibility() == 0) {
                    PlayListOnLineView.this.j();
                } else {
                    PlayListOnLineView.this.i();
                }
            } else if (PlayListOnLineView.this.a) {
                boolean unused = PlayListOnLineView.this.a(i, PlayListOnLineView.this.a);
                switch (i) {
                    case 0:
                        fd.a().a(PlayListOnLineView.this.S, (Handler) null);
                        return;
                    case 1:
                    default:
                        return;
                    case 2:
                        en.d(PlayListOnLineView.this.S);
                        return;
                }
            } else {
                boolean unused2 = PlayListOnLineView.this.a(i);
                switch (i) {
                    case 1:
                        fd.a().a(PlayListOnLineView.this.S, (Handler) null);
                        return;
                    case 2:
                        en.d(PlayListOnLineView.this.S);
                        return;
                    default:
                        return;
                }
            }
        }
    };
    private AdapterView.OnItemClickListener ak = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView adapterView, View view, int i, long j) {
            boolean unused = PlayListOnLineView.this.b(i);
        }
    };
    List x = null;
    List y = null;
    List z = null;

    /* renamed from: com.duomi.app.ui.PlayListOnLineView$8  reason: invalid class name */
    class AnonymousClass8 implements View.OnTouchListener {
        final /* synthetic */ ScrollGallery a;
        final /* synthetic */ PlayListOnLineView b;

        public boolean onTouch(View view, MotionEvent motionEvent) {
            switch (motionEvent.getAction()) {
                case 1:
                    ad.b("PlayListOnLineView", ">>>sGallery onTouchUp>>>last_x:" + 0.0f + " action_x:" + this.b.aa);
                    float x = motionEvent.getX();
                    if ((x == this.b.aa || Math.abs(this.b.aa - x) < 20.0f) && this.a.c != null) {
                        this.a.a(this.a.c, this.a.b);
                    }
                    if (this.b.U == 1) {
                        int unused = this.b.T = this.b.T - ((int) (x - this.b.Z));
                    } else {
                        int unused2 = this.b.T = ((int) (x - this.b.Z)) + this.b.T;
                    }
                    this.a.b(null, -1);
                    EventHandler unused3 = this.b.W = new EventHandler(Looper.getMainLooper());
                    this.b.W.removeMessages(0);
                    this.b.W.sendMessage(this.b.W.obtainMessage(3, 1, 1, "Start"));
                    return false;
                case 0:
                    float x2 = motionEvent.getX();
                    EventHandler unused4 = this.b.W = new EventHandler(Looper.getMainLooper());
                    this.b.W.removeMessages(0);
                    this.b.W.sendMessage(this.b.W.obtainMessage(2, 1, 1, "Stop"));
                    float unused5 = this.b.Z = x2;
                    float unused6 = this.b.aa = x2;
                    break;
            }
            return false;
        }
    }

    interface ASync {
    }

    class EventHandler extends Handler {
        public EventHandler(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message message) {
            switch (message.what) {
                case 1:
                    PlayListOnLineView.this.q();
                    return;
                case 2:
                    PlayListOnLineView.this.V.removeMessages(0);
                    return;
                case 3:
                    PlayListOnLineView.this.V.post(PlayListOnLineView.this.X);
                    if (PlayListOnLineView.this.Q.getScrollX() == 0) {
                        int unused = PlayListOnLineView.this.T = 0;
                        int unused2 = PlayListOnLineView.this.U = 1;
                    } else if (PlayListOnLineView.this.Q.getScrollX() == PlayListOnLineView.this.Y) {
                        int unused3 = PlayListOnLineView.this.T = 0;
                        int unused4 = PlayListOnLineView.this.U = -1;
                    }
                    if (PlayListOnLineView.this.U == 1) {
                        int unused5 = PlayListOnLineView.this.T = PlayListOnLineView.this.Q.getScrollX();
                        return;
                    } else if (PlayListOnLineView.this.U == -1) {
                        int unused6 = PlayListOnLineView.this.T = PlayListOnLineView.this.Y - PlayListOnLineView.this.Q.getScrollX();
                        return;
                    } else {
                        return;
                    }
                default:
                    return;
            }
        }
    }

    class FreshDataTask extends AsyncTask {
        private FreshDataTask() {
        }

        /* access modifiers changed from: protected */
        public Object doInBackground(Object... objArr) {
            PlayListOnLineView.this.a(0, "");
            return null;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Object obj) {
            String str;
            try {
                str = ((ct) PlayListOnLineView.this.B.a().get(cw.b)).d();
            } catch (Exception e) {
                e.printStackTrace();
                str = "";
            }
            co.a().a(str);
            super.onPostExecute(obj);
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            PlayListOnLineView.this.ai.sendEmptyMessage(9);
            super.onPreExecute();
        }
    }

    public class ListThread extends Thread {
        int a;
        String b;

        public ListThread(int i, String str) {
            this.a = i;
            this.b = str;
        }

        public void run() {
            if (!PlayListOnLineView.this.C.b(PlayListOnLineView.this.S)) {
                en.i(PlayListOnLineView.this.S);
            }
            if (PlayListOnLineView.ag == 0) {
                if (cp.b(PlayListOnLineView.this.S, cq.a(PlayListOnLineView.this.S, "homepage", 0, 30, PlayListOnLineView.this.ai)) > 0) {
                    int unused = PlayListOnLineView.ag = 2;
                    boolean unused2 = PlayListOnLineView.ah = true;
                }
            }
            int c2 = cp.c(PlayListOnLineView.this.S, cq.a(PlayListOnLineView.this.S, this.b, 0, 30, PlayListOnLineView.this.ai));
            if (c2 > 0) {
                if (this.a == 1) {
                    PlayListOnLineView.this.ai.sendEmptyMessage(3);
                } else {
                    PlayListOnLineView.this.ai.sendEmptyMessage(2);
                }
            } else if (c2 == 0) {
                PlayListOnLineView.this.ai.sendEmptyMessage(12);
            } else {
                PlayListOnLineView.this.ai.sendEmptyMessage(13);
            }
        }
    }

    class MyAdapter extends BaseAdapter {
        Bitmap a = null;
        private LayoutInflater c;
        private List d;

        class ItemStruct {
            ImageView a;
            TextView b;
            TextView c;
            ImageView d;

            private ItemStruct() {
            }
        }

        public MyAdapter(Context context, List list) {
            this.d = list;
            this.c = LayoutInflater.from(context);
        }

        public int getCount() {
            return this.d.size();
        }

        public Object getItem(int i) {
            return null;
        }

        public long getItemId(int i) {
            return (long) i;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(int i, View view, ViewGroup viewGroup) {
            ItemStruct itemStruct;
            View view2;
            if (view == null) {
                View inflate = this.c.inflate((int) R.layout.hall_list_row, viewGroup, false);
                ItemStruct itemStruct2 = new ItemStruct();
                itemStruct2.a = (ImageView) inflate.findViewById(R.id.hall_list_image);
                itemStruct2.b = (TextView) inflate.findViewById(R.id.hall_main_title);
                itemStruct2.c = (TextView) inflate.findViewById(R.id.hall_vice_title);
                itemStruct2.d = (ImageView) inflate.findViewById(R.id.hall_list_point);
                inflate.setTag(itemStruct2);
                ItemStruct itemStruct3 = itemStruct2;
                view2 = inflate;
                itemStruct = itemStruct3;
            } else {
                itemStruct = (ItemStruct) view.getTag();
                view2 = view;
            }
            String j = en.j(((cv) this.d.get(i)).e());
            if (PlayListOnLineView.this.E.get(j) == null || ((SoftReference) PlayListOnLineView.this.E.get(j)).get() == null) {
                String str = ae.q + "/" + en.j(((cv) this.d.get(i)).b());
                if (new File(str).exists()) {
                    PlayListOnLineView.this.E.put(j, new SoftReference(BitmapFactory.decodeFile(str)));
                    itemStruct.a.setImageBitmap((Bitmap) ((SoftReference) PlayListOnLineView.this.E.get(j)).get());
                } else {
                    itemStruct.a.setImageResource(R.drawable.home_find);
                }
            } else {
                itemStruct.a.setImageBitmap((Bitmap) ((SoftReference) PlayListOnLineView.this.E.get(j)).get());
            }
            itemStruct.d.setImageBitmap(PlayListOnLineView.this.F);
            if (this.d.get(i) instanceof cv) {
                itemStruct.b.setText(((cv) this.d.get(i)).f());
                itemStruct.c.setText(((cv) this.d.get(i)).a());
            } else if (this.d.get(i) instanceof cr) {
                itemStruct.b.setText(((cr) this.d.get(i)).b());
                itemStruct.c.setVisibility(8);
            }
            return view2;
        }
    }

    public class PicHandler extends Handler {
        List a;

        public PicHandler(Looper looper, List list) {
            super(looper);
            this.a = list;
        }

        public void handleMessage(Message message) {
            switch (message.what) {
                case 0:
                    int b2 = en.b(PlayListOnLineView.this.S);
                    for (int i = 0; i < this.a.size(); i++) {
                        try {
                            if (!new File(this.a.get(i) instanceof cr ? ae.q + "/" + en.j(((cr) this.a.get(i)).c()) : ae.q + "/" + en.j(((cv) this.a.get(i)).b())).exists() && il.c(PlayListOnLineView.this.S)) {
                                String c = this.a.get(i) instanceof cr ? ((cr) this.a.get(i)).c() : ((cv) this.a.get(i)).b();
                                il.a(en.b(PlayListOnLineView.this.S, c, b2), PlayListOnLineView.this.S, false, c);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    PlayListOnLineView.this.ai.sendEmptyMessage(0);
                    return;
                default:
                    return;
            }
        }
    }

    public class PicThread implements Runnable {
        private final Object b = new Object();
        private Looper c;

        public PicThread(String str) {
            Thread thread = new Thread(null, this, str);
            thread.setPriority(10);
            thread.start();
            synchronized (this.b) {
                while (this.c == null) {
                    try {
                        this.b.wait();
                    } catch (InterruptedException e) {
                    }
                }
            }
        }

        public Looper a() {
            return this.c;
        }

        public void run() {
            synchronized (this.b) {
                Looper.prepare();
                this.c = Looper.myLooper();
                this.b.notifyAll();
            }
            Looper.loop();
        }
    }

    class ScrollGallery extends jr {
        public ScrollGallery(Context context, int i, int i2) {
            super(context, i, i2);
        }

        public void a(View view, int i) {
            int unused = PlayListOnLineView.ad = i;
            boolean unused2 = PlayListOnLineView.ae = true;
            if (PlayListOnLineView.this.x.size() >= 2) {
                if (PlayListOnLineView.this.A.a(en.k(((cw) PlayListOnLineView.this.x.get(i)).e())) == null) {
                    if (PlayListOnLineView.this.x.get(i) instanceof cr) {
                        Intent intent = new Intent("android.intent.action.VIEW");
                        intent.setData(Uri.parse(((cr) PlayListOnLineView.this.x.get(PlayListOnLineView.ad)).a()));
                        PlayListOnLineView.this.S.startActivity(intent);
                    } else if (!il.b(PlayListOnLineView.this.S) || !il.c(PlayListOnLineView.this.S)) {
                        PlayListOnLineView.this.ai.sendEmptyMessage(253);
                    } else {
                        PlayListOnLineView.this.ai.sendEmptyMessage(9);
                        new ListThread(1, ((cw) PlayListOnLineView.this.x.get(i)).e()).start();
                    }
                } else if (((cv) PlayListOnLineView.this.x.get(i)).d()) {
                    if (!il.b(PlayListOnLineView.this.S) || !il.c(PlayListOnLineView.this.S)) {
                        PlayListOnLineView.this.ai.sendEmptyMessage(253);
                        return;
                    }
                    PlayListOnLineView.this.ai.sendEmptyMessage(9);
                    PlayListOnLineView.this.A.b(en.k(((cw) PlayListOnLineView.this.x.get(i)).e()));
                    new ListThread(1, ((cw) PlayListOnLineView.this.x.get(i)).e()).start();
                } else if (PlayListOnLineView.this.C.c(PlayListOnLineView.this.S).equals(PlayListOnLineView.this.C.b(1))) {
                    PlayListOnLineView.this.ai.sendEmptyMessage(3);
                } else if (!il.b(PlayListOnLineView.this.S) || !il.c(PlayListOnLineView.this.S)) {
                    PlayListOnLineView.this.ai.sendEmptyMessage(253);
                } else {
                    PlayListOnLineView.this.ai.sendEmptyMessage(9);
                    PlayListOnLineView.this.A.b(en.k(((cw) PlayListOnLineView.this.x.get(i)).e()));
                    new ListThread(1, ((cw) PlayListOnLineView.this.x.get(i)).e()).start();
                }
            }
        }
    }

    class ScrollThread extends Thread {
        final /* synthetic */ PlayListOnLineView a;
        private int b;

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.duomi.app.ui.PlayListOnLineView.a(com.duomi.app.ui.PlayListOnLineView, android.os.Handler):android.os.Handler
         arg types: [com.duomi.app.ui.PlayListOnLineView, com.duomi.app.ui.PlayListOnLineView$EventHandler]
         candidates:
          com.duomi.app.ui.PlayListOnLineView.a(com.duomi.app.ui.PlayListOnLineView, float):float
          com.duomi.app.ui.PlayListOnLineView.a(com.duomi.app.ui.PlayListOnLineView, com.duomi.app.ui.PlayListOnLineView$EventHandler):com.duomi.app.ui.PlayListOnLineView$EventHandler
          com.duomi.app.ui.PlayListOnLineView.a(com.duomi.app.ui.PlayListOnLineView, com.duomi.app.ui.PlayListOnLineView$MyAdapter):com.duomi.app.ui.PlayListOnLineView$MyAdapter
          com.duomi.app.ui.PlayListOnLineView.a(com.duomi.app.ui.PlayListOnLineView, int):void
          com.duomi.app.ui.PlayListOnLineView.a(com.duomi.app.ui.PlayListOnLineView, android.widget.ImageView[]):android.widget.ImageView[]
          com.duomi.app.ui.PlayListOnLineView.a(int, java.lang.String):void
          com.duomi.app.ui.PlayListOnLineView.a(int, android.view.KeyEvent):boolean
          c.a(java.lang.Class, android.os.Message):void
          c.a(boolean, android.os.Message):void
          c.a(int, android.view.KeyEvent):boolean
          c.a(int, boolean):boolean
          com.duomi.app.ui.PlayListOnLineView.a(com.duomi.app.ui.PlayListOnLineView, android.os.Handler):android.os.Handler */
        public void run() {
            Handler unused = this.a.V = (Handler) new EventHandler(Looper.getMainLooper());
            if (this.b == 1) {
                this.a.V.removeMessages(0);
                this.a.V.sendMessage(this.a.V.obtainMessage(1, 1, 1, "current thread"));
                this.a.V.postDelayed(this.a.X, 50);
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    class SetDataTask extends AsyncTask {
        private SetDataTask() {
        }

        /* access modifiers changed from: protected */
        public Object doInBackground(Object... objArr) {
            PlayListOnLineView.this.A = hx.a();
            PlayListOnLineView.this.A.a("duomi", (String) null);
            PlayListOnLineView.this.C = as.a(PlayListOnLineView.this.S);
            if (!(PlayListOnLineView.this.A.a("homepage") == null || PlayListOnLineView.this.A.a("homepage").b() == null)) {
                PlayListOnLineView.this.B = (cu) PlayListOnLineView.this.A.a("homepage").b();
                PlayListOnLineView.this.w();
            }
            if (PlayListOnLineView.this.y != null && PlayListOnLineView.this.y.size() > 0) {
                boolean unused = PlayListOnLineView.af = true;
                if (!PlayListOnLineView.ae) {
                    PlayListOnLineView.this.ai.sendEmptyMessage(10);
                }
            }
            if (!il.b(PlayListOnLineView.this.S) || !il.c(PlayListOnLineView.this.S)) {
                PlayListOnLineView.this.ai.sendEmptyMessage(253);
            } else {
                PlayListOnLineView.this.a(0, "");
            }
            return null;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Object obj) {
            String str;
            try {
                str = ((ct) PlayListOnLineView.this.B.a().get(cw.b)).d();
            } catch (Exception e) {
                e.printStackTrace();
                str = "";
            }
            co.a().a(str);
            super.onPostExecute(obj);
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            PlayListOnLineView.this.ai.sendEmptyMessage(9);
            super.onPreExecute();
        }
    }

    public PlayListOnLineView(Activity activity) {
        super(activity);
        this.S = activity;
    }

    private void a(List list) {
        this.y = new ArrayList();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < list.size()) {
                if (((cw) list.get(i2)).g().equals("homepage")) {
                    this.y.add(list.get(i2));
                }
                i = i2 + 1;
            } else {
                this.ai.sendEmptyMessage(7);
                return;
            }
        }
    }

    /* access modifiers changed from: private */
    public void g(int i) {
        this.z = new ArrayList();
        switch (i) {
            case 0:
                if (this.B != null && this.B.a().containsKey(cw.b)) {
                    this.z = ((ct) this.B.a().get(cw.b)).c();
                    break;
                }
            case 1:
                if (this.B != null && this.B.a().containsKey(cw.c)) {
                    this.z = ((ct) this.B.a().get(cw.c)).c();
                    break;
                }
            case 2:
                if (this.B != null && this.B.a().containsKey(cw.d)) {
                    this.z = ((ct) this.B.a().get(cw.d)).c();
                    break;
                }
            case 3:
                if (this.B != null && this.B.a().containsKey(cw.e)) {
                    this.z = ((ct) this.B.a().get(cw.e)).c();
                    break;
                }
        }
        a(this.z);
        PicHandler picHandler = new PicHandler(new PicThread("get pic").a(), this.y);
        picHandler.removeMessages(0);
        picHandler.obtainMessage(0).sendToTarget();
    }

    /* access modifiers changed from: private */
    public void w() {
        this.x = new ArrayList();
        if (this.B != null && this.B.a().containsKey(cw.a)) {
            this.x = ((ct) this.B.a().get(cw.a)).c();
        }
        this.ai.sendEmptyMessage(5);
        this.ai.sendEmptyMessage(6);
        g(K);
        x();
    }

    private void x() {
        this.G.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (PlayListOnLineView.K != 0) {
                    PlayListOnLineView.this.H.setBackgroundDrawable(null);
                    PlayListOnLineView.this.I.setBackgroundDrawable(null);
                    PlayListOnLineView.this.J.setBackgroundDrawable(null);
                    PlayListOnLineView.this.G.setBackgroundResource(R.drawable.online_titlelayout_s);
                    int unused = PlayListOnLineView.K = 0;
                    PlayListOnLineView.this.g(PlayListOnLineView.K);
                }
            }
        });
        this.H.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (PlayListOnLineView.K != 1) {
                    PlayListOnLineView.this.G.setBackgroundDrawable(null);
                    PlayListOnLineView.this.I.setBackgroundDrawable(null);
                    PlayListOnLineView.this.J.setBackgroundDrawable(null);
                    PlayListOnLineView.this.H.setBackgroundResource(R.drawable.online_titlelayout_s);
                    int unused = PlayListOnLineView.K = 1;
                    PlayListOnLineView.this.g(PlayListOnLineView.K);
                }
            }
        });
        this.I.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (PlayListOnLineView.K != 2) {
                    PlayListOnLineView.this.G.setBackgroundDrawable(null);
                    PlayListOnLineView.this.H.setBackgroundDrawable(null);
                    PlayListOnLineView.this.J.setBackgroundDrawable(null);
                    PlayListOnLineView.this.I.setBackgroundResource(R.drawable.online_titlelayout_s);
                    int unused = PlayListOnLineView.K = 2;
                    PlayListOnLineView.this.g(PlayListOnLineView.K);
                }
            }
        });
        this.J.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (PlayListOnLineView.K != 3) {
                    PlayListOnLineView.this.G.setBackgroundDrawable(null);
                    PlayListOnLineView.this.H.setBackgroundDrawable(null);
                    PlayListOnLineView.this.I.setBackgroundDrawable(null);
                    PlayListOnLineView.this.J.setBackgroundResource(R.drawable.online_titlelayout_s);
                    int unused = PlayListOnLineView.K = 3;
                    PlayListOnLineView.this.g(PlayListOnLineView.K);
                }
            }
        });
        this.P.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView adapterView, View view, int i, long j) {
                int unused = PlayListOnLineView.ac = i;
                boolean unused2 = PlayListOnLineView.ae = true;
                if (PlayListOnLineView.this.A.a(en.k(((cw) PlayListOnLineView.this.y.get(i)).e())) == null) {
                    if (PlayListOnLineView.this.y.get(i) instanceof cr) {
                        Intent intent = new Intent("android.intent.action.VIEW");
                        intent.setData(Uri.parse(((cr) PlayListOnLineView.this.y.get(PlayListOnLineView.ac)).a()));
                        PlayListOnLineView.this.S.startActivity(intent);
                    } else if (!il.b(PlayListOnLineView.this.S) || !il.c(PlayListOnLineView.this.S)) {
                        PlayListOnLineView.this.ai.sendEmptyMessage(253);
                    } else {
                        PlayListOnLineView.this.ai.sendEmptyMessage(9);
                        new ListThread(2, ((cw) PlayListOnLineView.this.y.get(i)).e()).start();
                    }
                } else if (((cv) PlayListOnLineView.this.y.get(i)).d()) {
                    if (!il.b(PlayListOnLineView.this.S) || !il.c(PlayListOnLineView.this.S)) {
                        PlayListOnLineView.this.ai.sendEmptyMessage(253);
                        return;
                    }
                    PlayListOnLineView.this.ai.sendEmptyMessage(9);
                    PlayListOnLineView.this.A.b(en.k(((cw) PlayListOnLineView.this.y.get(i)).e()));
                    new ListThread(2, ((cw) PlayListOnLineView.this.y.get(i)).e()).start();
                } else if (PlayListOnLineView.this.C.c(PlayListOnLineView.this.S).equals(PlayListOnLineView.this.C.b(1))) {
                    PlayListOnLineView.this.ai.sendEmptyMessage(2);
                } else if (!il.b(PlayListOnLineView.this.S) || !il.c(PlayListOnLineView.this.S)) {
                    PlayListOnLineView.this.ai.sendEmptyMessage(253);
                } else {
                    PlayListOnLineView.this.ai.sendEmptyMessage(9);
                    new ListThread(2, ((cw) PlayListOnLineView.this.y.get(i)).e()).start();
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void y() {
        if (this.B != null) {
            if (this.G != null && this.B.a().containsKey(cw.b)) {
                this.G.setText(((ct) this.B.a().get(cw.b)).a());
            }
            if (this.H != null && this.B.a().containsKey(cw.c)) {
                this.H.setText(((ct) this.B.a().get(cw.c)).a());
            }
            if (this.I != null && this.B.a().containsKey(cw.d)) {
                this.I.setText(((ct) this.B.a().get(cw.d)).a());
            }
            if (this.J != null && this.B.a().containsKey(cw.e)) {
                this.J.setText(((ct) this.B.a().get(cw.e)).a());
            }
        }
    }

    /* access modifiers changed from: private */
    public void z() {
        if (af) {
            this.M.setVisibility(0);
            this.N.setVisibility(8);
            return;
        }
        this.M.setVisibility(8);
        this.N.setVisibility(0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: cq.a(boolean, android.content.Context, int, java.lang.String, java.lang.String, bn):java.lang.String
     arg types: [int, android.content.Context, int, ?[OBJECT, ARRAY], ?[OBJECT, ARRAY], ?[OBJECT, ARRAY]]
     candidates:
      cq.a(java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, android.content.Context):java.lang.String
      cq.a(boolean, android.content.Context, int, java.lang.String, java.lang.String, bn):java.lang.String */
    public void a(int i, String str) {
        if (i == 0) {
            if (!this.C.b(this.S)) {
                cp.l(this.S, cq.a(true, this.S, 7, (String) null, (String) null, (bn) null));
            }
            if (this.A.a(cw.a) == null || this.A.a(cw.b) == null || this.A.a(cw.c) == null || this.A.a(cw.d) == null || this.A.a(cw.e) == null || !this.C.c(this.S).equals(this.C.b(1))) {
                int b = cp.b(this.S, cq.a(this.S, "homepage", 0, 30, this.ai));
                if (b > 0) {
                    ag = 2;
                } else if (b == 0) {
                    this.ai.sendEmptyMessage(12);
                    return;
                } else {
                    this.ai.sendEmptyMessage(13);
                    return;
                }
            } else {
                ag = 1;
            }
            this.ai.sendEmptyMessage(14);
            if (!(this.A.a("homepage") == null || this.A.a("homepage").b() == null)) {
                this.B = (cu) this.A.a("homepage").b();
            }
            w();
            if (!ae) {
                this.ai.sendEmptyMessage(10);
            }
        }
    }

    public void a(Message message) {
        if (ah) {
            if (!(this.A.a("homepage") == null || this.A.a("homepage").b() == null)) {
                this.B = (cu) this.A.a("homepage").b();
                w();
            }
            ah = false;
        }
        super.a(message);
    }

    public boolean a(int i, KeyEvent keyEvent) {
        b(i, keyEvent);
        if (i != 4 || this.f.getVisibility() != 0 || this.u) {
            return false;
        }
        if (this.g.getVisibility() == 0) {
            this.g.setVisibility(8);
            this.i.requestFocus();
        } else {
            this.f.setVisibility(8);
        }
        return true;
    }

    public boolean a(MotionEvent motionEvent) {
        ad.b("PlayListOnLineView", "beForceToMotion---enter-------");
        Rect rect = new Rect();
        if (this.Q == null) {
            return false;
        }
        this.Q.getGlobalVisibleRect(rect);
        int x2 = (int) motionEvent.getX();
        int y2 = ((int) motionEvent.getY()) + en.a(en.b(getContext()));
        ad.b("PlayListOnLineView", "beForceToMotion:left:" + rect.left + "right:" + rect.right + "bottom:" + rect.bottom + "top:" + rect.top + " gX:" + x2 + " gY:" + y2 + " sGallery.getTop()" + this.Q.getTop());
        if (!rect.contains(x2, y2)) {
            return false;
        }
        ad.b("PlayListOnLineView", "beForceToMotion---igore-------" + true);
        return true;
    }

    public void c() {
    }

    /* access modifiers changed from: protected */
    public void f() {
        Window window = g().getWindow();
        if (window != null) {
            window.clearFlags(1024);
        }
        inflate(g(), R.layout.playlist_online, this);
        this.G = (TextView) findViewById(R.id.recommend);
        this.H = (TextView) findViewById(R.id.rank);
        this.I = (TextView) findViewById(R.id.topics);
        this.J = (TextView) findViewById(R.id.read);
        this.L = (LinearLayout) findViewById(R.id.main_panel);
        this.M = (LinearLayout) findViewById(R.id.home_layout);
        this.N = (LinearLayout) findViewById(R.id.refresh_layout);
        this.O = (LinearLayout) findViewById(R.id.refresh);
        this.P = (ListView) findViewById(R.id.main_listview);
        this.O.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (!il.b(PlayListOnLineView.this.S) || !il.c(PlayListOnLineView.this.S)) {
                    PlayListOnLineView.this.ai.sendEmptyMessage(253);
                } else {
                    new FreshDataTask().execute(new Object[0]);
                }
            }
        });
        this.Q = new ScrollGallery(this.S, en.b(getContext()), (int) (80.0f * en.a(getContext())));
        this.Q.setVisibility(0);
        this.L.addView(this.Q, 0);
        h();
        K = 0;
        if (this.G != null) {
            this.G.setBackgroundResource(R.drawable.online_titlelayout_s);
        }
        new SetDataTask().execute(new Object[0]);
    }

    public void n() {
        this.i.setOnItemClickListener(this.aj);
        this.j.setOnItemClickListener(this.ak);
    }

    public void o() {
        if (this.a) {
            this.q = getResources().getStringArray(R.array.PlayListOnLine_menu_without_download);
            this.s = new ArrayList();
            this.s.add(Integer.valueOf((int) R.drawable.meun_scan));
            this.s.add(Integer.valueOf((int) R.drawable.menu_bg));
        } else {
            this.q = getResources().getStringArray(R.array.PlayListOnLine_menu);
            this.s = new ArrayList();
            this.s.add(Integer.valueOf((int) R.drawable.meun_downloadsetup));
            this.s.add(Integer.valueOf((int) R.drawable.meun_scan));
        }
        this.s.add(Integer.valueOf((int) R.drawable.menu_recommend));
        this.s.add(Integer.valueOf((int) R.drawable.meun_more));
        this.s.add(Integer.valueOf((int) R.drawable.meun_setup));
        this.s.add(Integer.valueOf((int) R.drawable.meun_sleep));
        this.s.add(Integer.valueOf((int) R.drawable.meun_syc));
        this.s.add(Integer.valueOf((int) R.drawable.meun_exit));
        this.r = getResources().getStringArray(R.array.second_menu2);
        this.t = new ArrayList();
        this.t.add(Integer.valueOf((int) R.drawable.meun_help));
        this.t.add(Integer.valueOf((int) R.drawable.meun_advice));
        this.t.add(Integer.valueOf((int) R.drawable.meun_friends));
        this.t.add(Integer.valueOf((int) R.drawable.meun_about));
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        return true;
    }

    public void q() {
        if (this.T < this.Y) {
            this.Q.scrollBy(this.U * 1, 0);
            if (this.U == 1) {
                this.T = this.Q.getScrollX();
            } else if (this.U == -1) {
                this.T = this.Y - this.Q.getScrollX();
            }
        } else if (this.T >= this.Y) {
            this.T = 0;
            this.U *= -1;
        }
    }
}
