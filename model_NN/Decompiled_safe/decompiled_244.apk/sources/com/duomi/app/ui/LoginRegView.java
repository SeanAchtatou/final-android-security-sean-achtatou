package com.duomi.app.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.duomi.android.R;
import java.util.ArrayList;
import java.util.regex.Pattern;

public class LoginRegView extends c implements View.OnClickListener {
    public static boolean A = true;
    public static boolean B = false;
    public static boolean C = false;
    public static boolean D = false;
    public static boolean E = false;
    /* access modifiers changed from: private */
    public static boolean R = false;
    public static boolean z = true;
    public boolean F = true;
    Handler G = new Handler() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: cq.a(boolean, android.content.Context, int, java.lang.String, java.lang.String, bn):java.lang.String
         arg types: [int, android.content.Context, int, ?[OBJECT, ARRAY], ?[OBJECT, ARRAY], ?[OBJECT, ARRAY]]
         candidates:
          cq.a(java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, android.content.Context):java.lang.String
          cq.a(boolean, android.content.Context, int, java.lang.String, java.lang.String, bn):java.lang.String */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: cq.a(boolean, android.content.Context, int, java.lang.String, java.lang.String, bn):java.lang.String
         arg types: [int, android.content.Context, int, java.lang.String, java.lang.String, ?[OBJECT, ARRAY]]
         candidates:
          cq.a(java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, android.content.Context):java.lang.String
          cq.a(boolean, android.content.Context, int, java.lang.String, java.lang.String, bn):java.lang.String */
        public void handleMessage(Message message) {
            LoginRegView.this.t();
            switch (message.what) {
                case 0:
                    if (message.obj != null) {
                        jh.a(LoginRegView.this.getContext(), message.obj.toString());
                        break;
                    }
                    break;
                case 1:
                    if (LoginRegView.this.aF != null) {
                        LoginRegView.this.aF.dismiss();
                    }
                    LoginRegView.this.U.setClickable(true);
                    break;
                case 2:
                    if (LoginRegView.this.H != null && !LoginRegView.this.H.isShowing()) {
                        LoginRegView.this.H.setCancelable(false);
                        LoginRegView.this.H.show();
                        break;
                    }
                case 3:
                    if (LoginRegView.this.H != null) {
                        LoginRegView.this.H.dismiss();
                        break;
                    }
                    break;
                case 4:
                    if (LoginRegView.this.H != null) {
                        LoginRegView.this.H.dismiss();
                        break;
                    }
                    break;
                case 5:
                    if (LoginRegView.this.H != null) {
                        LoginRegView.this.H.dismiss();
                    }
                    if (LoginRegView.this.au != null) {
                        LoginRegView.this.au.dismiss();
                    }
                    if (kf.b) {
                        LoginRegView.this.ar.setCancelable(false);
                        LoginRegView.this.ar.show();
                        break;
                    }
                    break;
                case 6:
                    if (LoginRegView.this.H != null) {
                        LoginRegView.this.H.dismiss();
                    }
                    if (LoginRegView.this.ar != null) {
                        LoginRegView.this.ar.dismiss();
                    }
                    LoginRegView.this.ar.setCancelable(false);
                    LoginRegView.this.au.show();
                    break;
                case 7:
                    if (LoginRegView.this.ar != null) {
                        LoginRegView.this.ar.dismiss();
                    }
                    if (LoginRegView.this.au != null) {
                        LoginRegView.this.au.dismiss();
                    }
                    if (LoginRegView.this.H != null && !LoginRegView.this.H.isShowing()) {
                        LoginRegView.this.H = null;
                        LoginRegView.this.H = new ProgressDialog(LoginRegView.this.ac);
                        LoginRegView.this.H.setProgressStyle(0);
                        LoginRegView.this.H.setTitle((int) R.string.sina_reg_registing);
                        LoginRegView.this.H.setMessage("" + LoginRegView.this.getContext().getString(R.string.reg_tips) + "    ");
                        LoginRegView.this.H.setCancelable(false);
                        LoginRegView.this.H.show();
                        break;
                    }
                case 8:
                    switch (message.arg1) {
                        case -10:
                            LoginRegView.this.aG.setText(bn.a().h() + "@duomi.com");
                            Message obtainMessage = LoginRegView.this.G.obtainMessage(9);
                            obtainMessage.arg1 = R.string.has_one_key_reg;
                            LoginRegView.this.G.sendMessageDelayed(obtainMessage, 200);
                            break;
                        case -7:
                            LoginRegView.this.aG.setVisibility(8);
                            LoginRegView.this.aI.setVisibility(8);
                            LoginRegView.this.aH.setVisibility(0);
                            LoginRegView.this.aJ.setVisibility(8);
                            LoginRegView.this.aH.setText(LoginRegView.this.g().getResources().getString(R.string.havs_binded_sina_weibo_yet_error_prompt));
                            LoginRegView.this.G.sendEmptyMessageDelayed(6, 1000);
                            break;
                        case -4:
                            int i = 0;
                            while (true) {
                                int i2 = i + 1;
                                if (i >= 3) {
                                    Message obtainMessage2 = LoginRegView.this.G.obtainMessage(9);
                                    obtainMessage2.arg1 = R.string.lg_servererrlogin_tips;
                                    LoginRegView.this.G.sendMessageDelayed(obtainMessage2, 200);
                                    break;
                                } else {
                                    String a2 = cq.a(false, LoginRegView.this.ac, 3, (String) null, (String) null, (bn) null);
                                    ad.b("LoginRegView", "xmlData>>>>>>" + a2);
                                    LoginRegView.D = cp.a(LoginRegView.this.ac, a2);
                                    if (LoginRegView.D) {
                                        bn a3 = bn.a();
                                        if (cp.a(LoginRegView.this.ac, cq.a(true, LoginRegView.this.ac, 4, a3.f(), a3.g(), (bn) null), LoginRegView.this.G) == 1 && kf.a(LoginRegView.this.g()) != -4) {
                                            LoginRegView.this.G.sendEmptyMessage(8);
                                            return;
                                        }
                                    }
                                    i = i2;
                                }
                            }
                        case 0:
                            ad.e("test", "one key reg [passwd]=" + LoginRegView.this.ao + "\t[uid]" + bn.a().h());
                            LoginRegView.this.aG.setText(LoginRegView.this.g().getResources().getString(R.string.sina_reg_finish_username).concat(" ").concat(kf.a(1, LoginRegView.this.g()).c));
                            LoginRegView.this.aI.setText(LoginRegView.this.g().getResources().getString(R.string.sina_reg_finish_password).concat(" ").concat(en.c(LoginRegView.this.ao) ? bn.a().h() : LoginRegView.this.ao));
                            LoginRegView.this.aH.setText(LoginRegView.this.g().getResources().getString(R.string.sina_reg_finish_prompt));
                            LoginRegView.this.aJ.setVisibility(0);
                            LoginRegView.this.aH.setVisibility(0);
                            LoginRegView.this.au.setTitle((int) R.string.sina_reg_success_title);
                            LoginRegView.this.G.sendEmptyMessageDelayed(6, 1000);
                            break;
                        default:
                            if (LoginRegView.this.H != null) {
                                LoginRegView.this.H.dismiss();
                            }
                            if (LoginRegView.this.ar != null) {
                                LoginRegView.this.ar.dismiss();
                            }
                            if (LoginRegView.this.au != null) {
                                LoginRegView.this.au.dismiss();
                            }
                            LoginRegView.this.au.setTitle((int) R.string.sina_reg_failure_title);
                            LoginRegView.this.aJ.setVisibility(8);
                            LoginRegView.this.aG.setVisibility(8);
                            LoginRegView.this.aI.setVisibility(8);
                            LoginRegView.this.aH.setVisibility(0);
                            LoginRegView.this.aH.setText(LoginRegView.this.g().getResources().getString(R.string.weibo_onekeybind_error_prompt));
                            LoginRegView.this.aH.setGravity(1);
                            LoginRegView.this.au.show();
                            if (ae.g) {
                                jh.a(LoginRegView.this.g(), "ERROR CODE = " + message.arg1, 1);
                                break;
                            }
                            break;
                    }
                    break;
                case 9:
                    if (LoginRegView.this.H != null) {
                        LoginRegView.this.H.dismiss();
                    }
                    if (LoginRegView.this.ar != null) {
                        LoginRegView.this.ar.dismiss();
                    }
                    if (LoginRegView.this.au != null) {
                        LoginRegView.this.au.dismiss();
                    }
                    String string = LoginRegView.this.g().getString(R.string.weibo_common_error_prompt);
                    String string2 = LoginRegView.this.g().getString(message.arg1);
                    Context c = LoginRegView.this.ac;
                    if (!en.c(string2)) {
                        string = string2;
                    }
                    jh.a(c, string, 1);
                    p a4 = p.a(LoginRegView.this.g());
                    a4.e();
                    String string3 = LoginRegView.this.ah.getString("gotoWhere");
                    if (en.c(string3)) {
                        string3 = MultiView.class.getName();
                    } else if ("charge".equals(string3)) {
                        LoginRegView.this.getContext().sendBroadcast(new Intent("com.duomi.charge_login_success"));
                    }
                    a4.a(string3);
                    break;
                case 10:
                    if (LoginRegView.this.H != null) {
                        LoginRegView.this.H.dismiss();
                    }
                    String str = (String) message.obj;
                    if (en.c(str)) {
                        LoginRegView.this.g().getString(R.string.reg_regist_tips);
                    }
                    jh.a(LoginRegView.this.g(), str, 1);
                    LoginRegView.this.V.setClickable(true);
                    break;
                case 11:
                    if (LoginRegView.this.H != null) {
                        LoginRegView.this.H.dismiss();
                    }
                    LoginRegView.this.V.setClickable(true);
                    break;
                case 12:
                    LoginRegView.this.u();
                    break;
                case 13:
                    jh.a(LoginRegView.this.g(), message.arg1, 1);
                    break;
                case 21:
                    LoginRegView.this.H = new ProgressDialog(LoginRegView.this.getContext());
                    LoginRegView.this.H.setTitle((int) R.string.reg_registertitle);
                    LoginRegView.this.H.setMessage("" + LoginRegView.this.getContext().getString(R.string.reg_tips) + "    ");
                    LoginRegView.this.H.setCancelable(false);
                    LoginRegView.this.H.show();
                    LoginRegView.this.V.setClickable(false);
                    break;
                case 22:
                    if (LoginRegView.this.H != null && LoginRegView.this.H.isShowing()) {
                        LoginRegView.this.H.dismiss();
                    }
                    if (!en.c((String) message.obj)) {
                        jh.a(LoginRegView.this.g(), (String) message.obj);
                    }
                    LoginRegView.this.V.setClickable(true);
                    break;
                case 23:
                    Selection.setSelection(LoginRegView.this.aa.getEditableText(), LoginRegView.this.aa.getEditableText().length());
                    Selection.setSelection(LoginRegView.this.ab.getEditableText(), LoginRegView.this.ab.getEditableText().length());
                    if (LoginRegView.this.ae != null) {
                        LoginRegView.this.ae.clear();
                    }
                    ArrayList unused = LoginRegView.this.ae = (ArrayList) message.obj;
                    if (LoginRegView.this.ae.size() > 3) {
                        ArrayList arrayList = new ArrayList();
                        for (int i3 = 0; i3 < 3; i3++) {
                            arrayList.add(LoginRegView.this.ae.get(i3));
                        }
                        LoginRegView.this.ae.clear();
                        LoginRegView.this.ae.addAll(arrayList);
                    }
                    if (en.a(LoginRegView.this.aa.getText().toString().trim())) {
                        LoginRegView.this.V.setClickable(true);
                    } else {
                        LoginRegView.this.G.sendEmptyMessage(31);
                    }
                    if (LoginRegView.this.H != null && LoginRegView.this.H.isShowing()) {
                        LoginRegView.this.H.dismiss();
                        break;
                    }
                case 24:
                    if (kf.b && LoginRegView.this.H != null && LoginRegView.this.H.isShowing()) {
                        LoginRegView.this.H.dismiss();
                    }
                    String string4 = LoginRegView.this.P ? LoginRegView.this.ac.getString(R.string.sina_reg_success_message_exist) : LoginRegView.this.ac.getString(R.string.sina_reg_success_message_noexist);
                    LoginRegView.this.J.setText(LoginRegView.this.P ? R.string.sina_reg_bind : R.string.sina_reg_onkey_register);
                    LoginRegView.this.ar.setMessage(string4);
                    LoginRegView.this.ar.setCancelable(false);
                    LoginRegView.this.ar.setView(LoginRegView.this.as);
                    LoginRegView.this.ay.setText(LoginRegView.this.ab.getText().toString().trim());
                    if (!LoginRegView.this.P) {
                        LoginRegView.this.ay.setVisibility(8);
                        LoginRegView.this.az.setVisibility(8);
                    }
                    LoginRegView.this.G.sendEmptyMessage(5);
                    jh.a(LoginRegView.this.g(), LoginRegView.this.ac.getResources().getString(R.string.reg_success));
                    LoginRegView.this.V.setClickable(true);
                    if (!kf.b) {
                        aw.a(LoginRegView.this.g()).b(bn.a().b(0));
                        as.a(LoginRegView.this.g()).l();
                        LoginRegView.this.u();
                        new Thread() {
                            public void run() {
                                try {
                                    Thread.sleep(100);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                        }.start();
                        break;
                    }
                    break;
                case 30:
                    if (message.obj != null) {
                        jh.a(LoginRegView.this.getContext(), message.obj.toString());
                        LoginRegView.this.a(LoginRegView.this.Y);
                    }
                    LoginRegView.this.U.setClickable(true);
                    break;
                case 31:
                    LoginRegView.this.D();
                    break;
                case 50:
                    LoginRegView.this.aK.setVisibility(8);
                    bo boVar = (bo) message.obj;
                    LoginRegView.this.Y.setText(boVar.c);
                    LoginRegView.this.Z.setText(boVar.d);
                    break;
            }
            super.handleMessage(message);
        }
    };
    public ProgressDialog H;
    private Runnable I = new Runnable() {
        public void run() {
            ad.d("testcase", "bind account run");
        }
    };
    /* access modifiers changed from: private */
    public Button J;
    private Button K;
    private Button L;
    private Button M;
    /* access modifiers changed from: private */
    public String N;
    private Button O;
    /* access modifiers changed from: private */
    public boolean P = false;
    /* access modifiers changed from: private */
    public boolean Q = true;
    private LinearLayout S;
    private LinearLayout T;
    /* access modifiers changed from: private */
    public Button U;
    /* access modifiers changed from: private */
    public Button V;
    private RelativeLayout W;
    private RelativeLayout X;
    /* access modifiers changed from: private */
    public EditText Y;
    /* access modifiers changed from: private */
    public EditText Z;
    private Button aA;
    private Button aB;
    private Button aC;
    private RelativeLayout aD;
    private RelativeLayout aE;
    /* access modifiers changed from: private */
    public ProgressDialog aF;
    /* access modifiers changed from: private */
    public TextView aG;
    /* access modifiers changed from: private */
    public TextView aH;
    /* access modifiers changed from: private */
    public TextView aI;
    /* access modifiers changed from: private */
    public TextView aJ;
    /* access modifiers changed from: private */
    public ListView aK;
    /* access modifiers changed from: private */
    public ArrayList aL = new ArrayList();
    /* access modifiers changed from: private */
    public EditText aa;
    /* access modifiers changed from: private */
    public EditText ab;
    /* access modifiers changed from: private */
    public Context ac;
    private ImageView ad;
    /* access modifiers changed from: private */
    public ArrayList ae;
    private ListView af;
    private TextWatcher ag = new TextWatcher() {
        public void afterTextChanged(Editable editable) {
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            if (LoginRegView.this.aa.getText().toString().length() > 40 || LoginRegView.this.aa.getText().toString().length() < 4) {
                LoginRegView.this.aa.setError(LoginRegView.this.g().getString(R.string.reg_name_limit_prompt));
            } else {
                LoginRegView.this.aa.setError(null);
            }
        }
    };
    /* access modifiers changed from: private */
    public Bundle ah;
    private TextWatcher ai = new TextWatcher() {
        public void afterTextChanged(Editable editable) {
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            if (LoginRegView.this.ab.getText().toString().length() >= 20) {
                Message obtainMessage = LoginRegView.this.G.obtainMessage(13);
                obtainMessage.arg1 = R.string.reg_passwd_limit_prompt;
                LoginRegView.this.G.removeMessages(13);
                LoginRegView.this.G.sendMessageDelayed(obtainMessage, 300);
            }
        }
    };
    private Runnable aj = new Runnable() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: cq.a(boolean, android.content.Context, int, java.lang.String, java.lang.String, bn):java.lang.String
         arg types: [int, android.content.Context, int, ?[OBJECT, ARRAY], ?[OBJECT, ARRAY], ?[OBJECT, ARRAY]]
         candidates:
          cq.a(java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, android.content.Context):java.lang.String
          cq.a(boolean, android.content.Context, int, java.lang.String, java.lang.String, bn):java.lang.String */
        public void run() {
            String str;
            try {
                str = cq.a(false, LoginRegView.this.ac, 1, (String) null, (String) null, (bn) null);
            } catch (Exception e) {
                e.printStackTrace();
                str = null;
            }
            ad.b("LoginRegView", "xmlData>>>>>>" + str);
            LoginRegView.E = cp.a(LoginRegView.this.ac, str);
            boolean unused = LoginRegView.R = true;
        }
    };
    private RelativeLayout ak;
    private RelativeLayout al;
    private RelativeLayout am;
    private RelativeLayout an;
    /* access modifiers changed from: private */
    public String ao;
    private Runnable ap = new Runnable() {
        public void run() {
            LoginRegView.this.G.sendEmptyMessage(7);
            try {
                Thread.sleep(20);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            String unused = LoginRegView.this.ao = bn.a().h();
            int a2 = kf.a(LoginRegView.this.g());
            Message obtainMessage = LoginRegView.this.G.obtainMessage(8);
            obtainMessage.arg1 = a2;
            LoginRegView.this.G.sendMessage(obtainMessage);
        }
    };
    private View aq;
    /* access modifiers changed from: private */
    public AlertDialog ar;
    /* access modifiers changed from: private */
    public View as;
    private Runnable at = new Runnable() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: cq.a(boolean, android.content.Context, int, java.lang.String, java.lang.String, bn):java.lang.String
         arg types: [int, android.content.Context, int, ?[OBJECT, ARRAY], ?[OBJECT, ARRAY], ?[OBJECT, ARRAY]]
         candidates:
          cq.a(java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, android.content.Context):java.lang.String
          cq.a(boolean, android.content.Context, int, java.lang.String, java.lang.String, bn):java.lang.String */
        public void run() {
            kc a2;
            Message obtainMessage;
            if (!il.b(LoginRegView.this.g())) {
                LoginRegView.this.r();
                return;
            }
            LoginRegView.this.G.sendEmptyMessage(21);
            LoginRegView.this.F = true;
            for (int i = 0; i < 1 && cq.a(LoginRegView.this.ac, 7) == -1; i++) {
                LoginRegView.this.F = false;
            }
            if (!LoginRegView.this.F) {
                LoginRegView.this.r();
                return;
            }
            long currentTimeMillis = System.currentTimeMillis();
            while (!LoginRegView.R && System.currentTimeMillis() - currentTimeMillis < 10000) {
                try {
                    Thread.sleep(20);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            try {
                Thread.sleep(20);
            } catch (InterruptedException e2) {
                e2.printStackTrace();
            }
            try {
                if (LoginRegView.E) {
                    if (System.currentTimeMillis() - LoginRegView.this.x > 20000 || en.c(as.a(LoginRegView.this.getContext()).v()) || en.c(as.a(LoginRegView.this.getContext()).u())) {
                        String a3 = cq.a(false, LoginRegView.this.getContext(), 1, (String) null, (String) null, (bn) null);
                        LoginRegView.E = cp.a(LoginRegView.this.getContext(), a3);
                        if (!en.c(a3)) {
                            boolean unused = LoginRegView.R = true;
                        } else {
                            LoginRegView.this.r();
                            boolean unused2 = LoginRegView.R = false;
                            return;
                        }
                    }
                    a2 = jz.a(LoginRegView.this.g(), LoginRegView.this.N.toLowerCase().trim(), LoginRegView.this.ab.getText().toString().trim(), LoginRegView.this.G);
                } else {
                    String a4 = cq.a(false, LoginRegView.this.getContext(), 1, (String) null, (String) null, (bn) null);
                    LoginRegView.E = cp.a(LoginRegView.this.getContext(), a4);
                    if (!en.c(a4)) {
                        boolean unused3 = LoginRegView.R = true;
                        a2 = jz.a(LoginRegView.this.g(), LoginRegView.this.N.toLowerCase().trim(), LoginRegView.this.ab.getText().toString().trim(), LoginRegView.this.G);
                    } else {
                        LoginRegView.this.r();
                        boolean unused4 = LoginRegView.R = false;
                        new kc().a = -3;
                        return;
                    }
                }
                if (a2.a == 0) {
                    bo b = bn.a().b(0);
                    if (b == null) {
                        LoginRegView.this.G.obtainMessage(10).obj = LoginRegView.this.g().getString(R.string.reg_regist_tips);
                        return;
                    }
                    aw.a(LoginRegView.this.g()).b(b);
                    obtainMessage = LoginRegView.this.G.obtainMessage(24);
                } else if (a2.a == -1) {
                    obtainMessage = LoginRegView.this.G.obtainMessage(22);
                } else if (a2.a == -2) {
                    Message obtainMessage2 = LoginRegView.this.G.obtainMessage(23);
                    obtainMessage2.obj = a2.b;
                    obtainMessage2.sendToTarget();
                    return;
                } else {
                    obtainMessage = LoginRegView.this.G.obtainMessage(10);
                    obtainMessage.obj = LoginRegView.this.g().getString(R.string.reg_regist_tips);
                }
                if (obtainMessage != null) {
                    obtainMessage.sendToTarget();
                }
            } catch (Exception e3) {
                Message obtainMessage3 = LoginRegView.this.G.obtainMessage(10);
                obtainMessage3.obj = LoginRegView.this.g().getString(R.string.reg_regist_tips);
                obtainMessage3.sendToTarget();
            }
        }
    };
    /* access modifiers changed from: private */
    public AlertDialog au;
    private View av;
    private Dialog aw;
    /* access modifiers changed from: private */
    public int ax = 0;
    /* access modifiers changed from: private */
    public EditText ay;
    /* access modifiers changed from: private */
    public EditText az;
    long x = 0;
    long y = 0;

    class AccountWraper {
        bo a;
        TextView b;

        AccountWraper() {
        }
    }

    class LoginTask extends AsyncTask {
        private LoginTask() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: cq.a(boolean, android.content.Context, int, java.lang.String, java.lang.String, bn):java.lang.String
         arg types: [int, android.content.Context, int, ?[OBJECT, ARRAY], ?[OBJECT, ARRAY], ?[OBJECT, ARRAY]]
         candidates:
          cq.a(java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, android.content.Context):java.lang.String
          cq.a(boolean, android.content.Context, int, java.lang.String, java.lang.String, bn):java.lang.String */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: cq.a(boolean, android.content.Context, int, java.lang.String, java.lang.String, bn):java.lang.String
         arg types: [int, android.content.Context, int, java.lang.String, java.lang.String, ?[OBJECT, ARRAY]]
         candidates:
          cq.a(java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, android.content.Context):java.lang.String
          cq.a(boolean, android.content.Context, int, java.lang.String, java.lang.String, bn):java.lang.String */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kf.a(android.content.Context, java.lang.String, java.lang.String, boolean):int
         arg types: [android.app.Activity, java.lang.String, java.lang.String, int]
         candidates:
          kf.a(android.content.Context, java.lang.String, java.lang.String, java.lang.String):int
          kf.a(android.content.Context, java.lang.String, java.lang.String, boolean):int */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.duomi.app.ui.LoginRegView.a(com.duomi.app.ui.LoginRegView, boolean):boolean
         arg types: [com.duomi.app.ui.LoginRegView, int]
         candidates:
          com.duomi.app.ui.LoginRegView.a(com.duomi.app.ui.LoginRegView, int):int
          com.duomi.app.ui.LoginRegView.a(com.duomi.app.ui.LoginRegView, android.app.ProgressDialog):android.app.ProgressDialog
          com.duomi.app.ui.LoginRegView.a(com.duomi.app.ui.LoginRegView, java.lang.String):java.lang.String
          com.duomi.app.ui.LoginRegView.a(com.duomi.app.ui.LoginRegView, java.util.ArrayList):java.util.ArrayList
          com.duomi.app.ui.LoginRegView.a(int, android.view.KeyEvent):boolean
          c.a(java.lang.Class, android.os.Message):void
          c.a(boolean, android.os.Message):void
          c.a(int, android.view.KeyEvent):boolean
          c.a(int, boolean):boolean
          com.duomi.app.ui.LoginRegView.a(com.duomi.app.ui.LoginRegView, boolean):boolean */
        /* access modifiers changed from: protected */
        /* renamed from: a */
        public Integer doInBackground(String... strArr) {
            String a2;
            String str = strArr[0];
            String str2 = strArr[1];
            try {
                if (!il.b(LoginRegView.this.getContext())) {
                    if (LoginRegView.this.aF != null) {
                        LoginRegView.this.aF.dismiss();
                    }
                    LoginRegView.this.U.setClickable(true);
                    return -1;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                LoginRegView.A = true;
                for (int i = 0; i < 1 && cq.a(LoginRegView.this.ac, 7) == -1; i++) {
                    LoginRegView.A = false;
                }
                if (!LoginRegView.A) {
                    LoginRegView.this.q();
                    return -1;
                }
                long currentTimeMillis = System.currentTimeMillis();
                while (!LoginRegView.C && System.currentTimeMillis() - currentTimeMillis < 10000) {
                    try {
                        Thread.sleep(20);
                    } catch (InterruptedException e2) {
                        e2.printStackTrace();
                    }
                }
                if (LoginRegView.D) {
                    if (System.currentTimeMillis() - LoginRegView.this.x > 20000 || en.c(as.a(LoginRegView.this.getContext()).v()) || en.c(as.a(LoginRegView.this.getContext()).u())) {
                        String a3 = cq.a(false, LoginRegView.this.ac, 3, (String) null, (String) null, (bn) null);
                        cp.a(LoginRegView.this.ac, a3);
                        if (en.c(a3) || a3.contains("ERR")) {
                            LoginRegView.this.q();
                            LoginRegView.D = false;
                            return -1;
                        }
                        LoginRegView.D = true;
                    }
                    a2 = cq.a(false, LoginRegView.this.ac, 4, str.toLowerCase(), str2, (bn) null);
                } else {
                    String a4 = cq.a(false, LoginRegView.this.ac, 3, (String) null, (String) null, (bn) null);
                    cp.a(LoginRegView.this.ac, a4);
                    if (en.c(a4) || a4.contains("ERR")) {
                        LoginRegView.this.q();
                        LoginRegView.D = false;
                        return -1;
                    }
                    LoginRegView.D = true;
                    a2 = cq.a(false, LoginRegView.this.ac, 4, str.toLowerCase(), str2, (bn) null);
                }
                ad.b("LoginRegView", "xmlData>>>>>>" + a2);
                bn.a().d(str2);
                if (a2 == null) {
                    if (LoginRegView.this.G != null) {
                        LoginRegView.this.G.sendMessage(LoginRegView.this.G.obtainMessage(30, LoginRegView.this.ac.getString(R.string.lg_errlogin_tips)));
                    }
                    if (LoginRegView.this.aF != null) {
                        LoginRegView.this.aF.dismiss();
                    }
                    LoginRegView.this.U.setClickable(true);
                    return -1;
                }
                as.a(LoginRegView.this.getContext()).a(true, true, bn.a().f().toLowerCase(), str2, 0);
                int a5 = cp.a(LoginRegView.this.ac, a2, LoginRegView.this.G);
                if (a5 != 1) {
                    LoginRegView.this.G.sendEmptyMessage(14);
                } else if (LoginRegView.this.Q) {
                    LoginRegView.this.a(true, null);
                    bo b = bn.a().b(0);
                    if (b == null) {
                        b = aw.a(LoginRegView.this.ac).a(bn.a().h(), 0);
                    }
                    aw.a(LoginRegView.this.g()).b(b);
                    as.a(LoginRegView.this.getContext()).l();
                    p.a(LoginRegView.this.g()).e();
                    while (gx.d == null && ev.a != null) {
                        try {
                            Thread.sleep(200);
                        } catch (InterruptedException e3) {
                            e3.printStackTrace();
                        }
                    }
                    LoginRegView.this.getContext().sendBroadcast(new Intent("com.duomi.music_reloadlist"));
                    String h = cp.h(LoginRegView.this.getContext(), cq.d(LoginRegView.this.getContext()));
                    as.a(LoginRegView.this.getContext()).a(h);
                    if (en.c(bn.a().n())) {
                        bn.a().k(h);
                        aw.a(LoginRegView.this.ac).a(bn.a());
                    }
                    bo boVar = null;
                    if (kf.b) {
                        try {
                            boVar = kf.a(LoginRegView.this.g(), 1, LoginRegView.this.G);
                        } catch (Exception e4) {
                            LoginRegView.this.G.sendEmptyMessageDelayed(250, 250);
                        }
                        if (boVar == null || en.c(boVar.c)) {
                            bn.a().a(1, LoginRegView.this.getContext());
                        } else {
                            bo a6 = kf.a(1, LoginRegView.this.g());
                            if (a6 != null && a6.f.equals(boVar.f) && a6.c.equals(boVar.c)) {
                                int a7 = kf.a((Context) LoginRegView.this.g(), a6.c, a6.d, true);
                                int i2 = 0;
                                while (a7 != 0) {
                                    int i3 = i2 + 1;
                                    if (i2 >= 3) {
                                        break;
                                    }
                                    int i4 = i3;
                                    a7 = kf.a((Context) LoginRegView.this.g(), a6.c, a6.d, true);
                                    i2 = i4;
                                }
                            }
                        }
                    }
                    String string = LoginRegView.this.ah.getString("gotoWhere");
                    if (en.c(string)) {
                        string = MultiView.class.getName();
                    } else if ("charge".equals(string)) {
                        LoginRegView.this.getContext().sendBroadcast(new Intent("com.duomi.charge_login_success"));
                    }
                    p.a(LoginRegView.this.g()).a(string);
                } else {
                    boolean unused = LoginRegView.this.Q = true;
                }
                return Integer.valueOf(a5);
            } catch (Exception e5) {
                e5.printStackTrace();
                LoginRegView.this.q();
                return -1;
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onPostExecute(Integer num) {
            LoginRegView.this.G.sendEmptyMessageDelayed(1, 1000);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onProgressUpdate(Integer... numArr) {
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            try {
                if (!il.b(LoginRegView.this.getContext())) {
                    LoginRegView.this.G.sendMessage(LoginRegView.this.G.obtainMessage(30, LoginRegView.this.ac.getString(R.string.app_net_error)));
                    LoginRegView.this.U.setClickable(true);
                    return;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            ProgressDialog unused = LoginRegView.this.aF = new ProgressDialog(LoginRegView.this.getContext());
            LoginRegView.this.aF.setTitle((int) R.string.lg_checkin);
            LoginRegView.this.aF.setMessage("" + LoginRegView.this.getContext().getString(R.string.lg_tips) + "    ");
            LoginRegView.this.aF.setCancelable(true);
            LoginRegView.this.aF.show();
        }
    }

    class NameAdapter extends BaseAdapter {
        AccountWraper a;
        private LayoutInflater c = LayoutInflater.from(this.d);
        private Context d;

        public NameAdapter(Context context) {
            this.d = context;
        }

        /* renamed from: a */
        public bo getItem(int i) {
            if (LoginRegView.this.aL.size() == 0) {
                return null;
            }
            return (bo) LoginRegView.this.aL.get(i);
        }

        public int getCount() {
            return LoginRegView.this.aL.size();
        }

        public long getItemId(int i) {
            return 0;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(final int i, View view, ViewGroup viewGroup) {
            View view2;
            if (view == null) {
                View inflate = this.c.inflate((int) R.layout.login_reg_listview_layout, viewGroup, false);
                this.a = new AccountWraper();
                this.a.b = (TextView) inflate.findViewById(R.id.name_list_text);
                this.a.a = getItem(i);
                inflate.setTag(this.a);
                view2 = inflate;
            } else {
                this.a = (AccountWraper) view.getTag();
                view2 = view;
            }
            this.a.b.setText(getItem(i).c);
            view2.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    LoginRegView.this.G.obtainMessage(50, NameAdapter.this.getItem(i)).sendToTarget();
                }
            });
            return view2;
        }
    }

    class NameSelectAdapter extends BaseAdapter {
        SelectNameStrut a;
        private LayoutInflater c = LayoutInflater.from(this.d);
        private Context d;

        public NameSelectAdapter(Context context) {
            this.d = context;
        }

        /* renamed from: a */
        public String getItem(int i) {
            return (String) LoginRegView.this.ae.get(i);
        }

        public int getCount() {
            return LoginRegView.this.ae.size();
        }

        public long getItemId(int i) {
            return (long) i;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(final int i, View view, ViewGroup viewGroup) {
            View view2;
            if (view == null) {
                this.a = new SelectNameStrut();
                view2 = this.c.inflate((int) R.layout.reg_username_list_view_layout, viewGroup, false);
                this.a.b = (TextView) view2.findViewById(R.id.reg_name_text);
                this.a.a = (RadioButton) view2.findViewById(R.id.reg_name_radio);
                view2.setTag(this.a);
            } else {
                this.a = (SelectNameStrut) view.getTag();
                view2 = view;
            }
            this.a.a.setClickable(false);
            view2.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    int unused = LoginRegView.this.ax = i;
                    NameSelectAdapter.this.notifyDataSetChanged();
                }
            });
            this.a.b.setText((CharSequence) LoginRegView.this.ae.get(i));
            if (LoginRegView.this.ax == i) {
                this.a.a.setChecked(true);
            } else {
                this.a.a.setChecked(false);
            }
            return view2;
        }
    }

    class SelectNameStrut {
        RadioButton a;
        TextView b;

        SelectNameStrut() {
        }
    }

    public LoginRegView(Activity activity) {
        super(activity);
        this.ac = activity;
    }

    private void A() {
        this.aL = jz.a(0, g());
        this.aK.setAdapter((ListAdapter) new NameAdapter(g()));
        this.ad.setOnClickListener(this);
        this.ak.setOnClickListener(this);
        this.al.setOnClickListener(this);
        this.am.setOnClickListener(this);
        this.an.setOnClickListener(this);
        this.S.setBackgroundResource(R.drawable.login_edit_layout_background);
        this.T.setBackgroundResource(R.drawable.login_edit_layout_background);
        this.aA.setOnClickListener(this);
        this.aB.setOnClickListener(this);
        this.aC.setOnClickListener(this);
        this.U.setOnClickListener(this);
        this.V.setOnClickListener(this);
        this.O.setOnClickListener(this);
        this.K.setOnClickListener(this);
        this.J.setOnClickListener(this);
        String[] j = as.a(this.ac).j();
        if (j[2].equals("0")) {
            this.Y.setText(j[0].toLowerCase());
            if (as.a(this.ac).i()) {
                this.Z.setText(j[1]);
            }
        } else {
            bo b = kf.b(0, g());
            if (b != null) {
                this.Y.setText(b.c.toLowerCase());
                if (as.a(this.ac).i()) {
                    this.Z.setText(b.d);
                }
            }
        }
        this.aa.addTextChangedListener(this.ag);
        this.ab.addTextChangedListener(this.ai);
        this.Y.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable editable) {
            }

            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                LoginRegView.this.Y.setError(null);
            }
        });
        this.Z.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable editable) {
            }

            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                LoginRegView.this.Z.setError(null);
            }
        });
        this.aa.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable editable) {
            }

            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                LoginRegView.this.aa.setError(null);
            }
        });
        this.ab.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable editable) {
            }

            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                LoginRegView.this.ab.setError(null);
            }
        });
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: cq.a(boolean, android.content.Context, int, java.lang.String, java.lang.String, bn):java.lang.String
     arg types: [int, android.content.Context, int, ?[OBJECT, ARRAY], ?[OBJECT, ARRAY], ?[OBJECT, ARRAY]]
     candidates:
      cq.a(java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, android.content.Context):java.lang.String
      cq.a(boolean, android.content.Context, int, java.lang.String, java.lang.String, bn):java.lang.String */
    /* access modifiers changed from: private */
    public void B() {
        if (!as.a(getContext()).b(getContext())) {
            en.i(getContext());
        }
        String a = cq.a(false, getContext(), 1, (String) null, (String) null, (bn) null);
        E = cp.a(getContext(), a);
        if (en.c(a) || a.contains("ERR")) {
            R = false;
        } else {
            R = true;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: cq.a(boolean, android.content.Context, int, java.lang.String, java.lang.String, bn):java.lang.String
     arg types: [int, android.content.Context, int, ?[OBJECT, ARRAY], ?[OBJECT, ARRAY], ?[OBJECT, ARRAY]]
     candidates:
      cq.a(java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, android.content.Context):java.lang.String
      cq.a(boolean, android.content.Context, int, java.lang.String, java.lang.String, bn):java.lang.String */
    /* access modifiers changed from: private */
    public void C() {
        if (!as.a(getContext()).b(getContext())) {
            en.i(getContext());
        }
        String a = cq.a(false, this.ac, 3, (String) null, (String) null, (bn) null);
        cp.a(this.ac, a);
        if (en.c(a) || a.contains("ERR")) {
            D = false;
        } else {
            D = true;
        }
    }

    /* access modifiers changed from: private */
    public void D() {
        this.af.setAdapter((ListAdapter) new NameSelectAdapter(this.ac));
        if (this.aw == null) {
            this.aw = new Dialog(this.ac);
            Window window = this.aw.getWindow();
            WindowManager.LayoutParams attributes = window.getAttributes();
            window.requestFeature(1);
            window.setBackgroundDrawableResource(R.drawable.none);
            window.setAttributes(attributes);
            this.aw.setContentView(this.aq);
        }
        ad.a("LoginRegView", toString());
        this.aw.setCancelable(false);
        this.aw.show();
    }

    private void E() {
        p a = p.a(g());
        a.e();
        Bundle bundle = new Bundle();
        bundle.putBoolean("isQuitOnCancle", this.ah.getBoolean("isQuitOnCancle", true));
        String string = this.ah.getString("gotoWhere");
        if (string == null) {
            string = MultiView.class.getName();
        }
        bundle.putBoolean("isNotBackToLogin", true);
        bundle.putString("gotoWhere", string);
        bundle.putBoolean("isReplaceLocalUserData", this.ah.getBoolean("isReplaceLocalUserData", false));
        a.a(SinaLoginView.class.getName(), bundle);
    }

    /* access modifiers changed from: private */
    public void t() {
        if (this.Y != null) {
            this.Y.setSelection(this.Y.getText().toString().length());
        }
        if (this.Z != null) {
            this.Z.setSelection(this.Z.getText().toString().length());
        }
        if (this.aa != null) {
            this.aa.setSelection(this.aa.getText().toString().length());
        }
        if (this.ab != null) {
            this.ab.setSelection(this.ab.getText().toString().length());
        }
    }

    /* access modifiers changed from: private */
    public void u() {
        p a = p.a(g());
        a.e();
        if ("charge".equals(this.ah.getString("gotoWhere"))) {
            getContext().sendBroadcast(new Intent("com.duomi.charge_login_success"));
        }
        a.a(MultiView.class.getName());
        as.a(this.ac).l();
        as.a(this.ac).a(true, true, this.aa.getText().toString().trim(), this.ab.getText().toString().trim(), 0);
        getContext().sendBroadcast(new Intent("com.duomi.music_reloadlist"));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void v() {
        this.aq = LayoutInflater.from(this.ac).inflate((int) R.layout.reg_username_list_layout, (ViewGroup) null, false);
        this.af = (ListView) this.aq.findViewById(R.id.reg_uesrname_list);
        this.L = (Button) this.aq.findViewById(R.id.reg_uesrnam_confrim);
        this.M = (Button) this.aq.findViewById(R.id.reg_uesrnam_cancle);
        this.L.setOnClickListener(this);
        this.M.setOnClickListener(this);
        this.aw = new Dialog(this.ac);
        this.aw.setCancelable(false);
        Window window = this.aw.getWindow();
        WindowManager.LayoutParams attributes = window.getAttributes();
        window.requestFeature(1);
        window.setBackgroundDrawableResource(R.drawable.none);
        window.setAttributes(attributes);
        this.aw.setContentView(this.aq);
    }

    private void w() {
        InputMethodManager inputMethodManager = (InputMethodManager) this.ac.getSystemService("input_method");
        inputMethodManager.hideSoftInputFromWindow(this.Y.getWindowToken(), 0);
        inputMethodManager.hideSoftInputFromWindow(this.Z.getWindowToken(), 0);
        String replaceAll = this.Y.getText().toString().trim().replaceAll("＠", "@");
        this.Y.setText(replaceAll.toLowerCase());
        String obj = this.Z.getText().toString();
        if (this.Y.getText() == null || this.Y.getText().toString().equals("") || this.Y.getText().toString().equalsIgnoreCase(this.ac.getResources().getString(R.string.lg_username_default))) {
            this.Y.setError(g().getString(R.string.lg_input_username));
            this.Y.requestFocus();
        } else if (this.Y.getText().toString().length() < 4 || this.Y.getText().toString().length() > 44) {
            this.Y.setError(g().getString(R.string.lg_errlogin_fail2));
            this.Y.requestFocus();
            a(this.Y);
        } else if (this.Y.getText().toString().contains("%") || this.Y.getText().toString().contains("&") || this.Y.getText().toString().contains("~") || this.Y.getText().toString().contains("!") || this.Y.getText().toString().contains("$")) {
            this.Y.setError(g().getString(R.string.lg_errlogin_fail3));
            this.Y.requestFocus();
            a(this.Y);
        } else if (this.Y.getText().toString().matches("[.@\\w]*[一-龥]+[.@\\w]*")) {
            this.Y.setError(g().getString(R.string.lg_errlogin_fail3));
            this.Y.requestFocus();
            a(this.Y);
        } else if (this.Z.getText() == null || this.Z.getText().toString().equals("")) {
            this.Z.setError(g().getString(R.string.lg_input_passwd));
            this.Z.requestFocus();
            a(this.Z);
        } else if (this.Z.getText().toString().length() < 4 || this.Z.getText().toString().length() > 20) {
            this.Z.setError(g().getString(R.string.prompt_passwrod_length_err));
            this.Z.requestFocus();
            a(this.Z);
        } else if (this.Z.getText().toString().contains("%") || this.Z.getText().toString().contains("&") || this.Z.getText().toString().contains("~") || this.Z.getText().toString().contains("!") || this.Z.getText().toString().contains("$")) {
            this.Z.setError(g().getString(R.string.lg_errlogin_fail4));
            this.Z.requestFocus();
            a(this.Z);
        } else if (this.Z.getText().toString().matches("[.@\\w]*[一-龥]+[.@\\w]*")) {
            this.Z.setError(g().getString(R.string.lg_errlogin_fail4));
            this.Z.requestFocus();
            a(this.Z);
        } else {
            this.Z.setError(null);
            this.Y.setError(null);
            bn.a().c(replaceAll.toLowerCase());
            bn.a().d(obj);
            this.U.setClickable(false);
            new LoginTask().execute(replaceAll, obj);
        }
    }

    private void x() {
        InputMethodManager inputMethodManager = (InputMethodManager) this.ac.getSystemService("input_method");
        inputMethodManager.hideSoftInputFromWindow(this.aa.getWindowToken(), 0);
        inputMethodManager.hideSoftInputFromWindow(this.ab.getWindowToken(), 0);
        String replaceAll = this.aa.getText().toString().trim().replaceAll("＠", "@");
        this.aa.setText(replaceAll.toLowerCase());
        String trim = this.ab.getText().toString().trim();
        if (this.aa.getText() == null || en.c(replaceAll)) {
            this.aa.setError(g().getString(R.string.prompt_username_err));
            this.aa.requestFocus();
            Selection.setSelection(this.aa.getEditableText(), this.aa.getEditableText().length());
        } else if (replaceAll.length() < 4 || replaceAll.length() > 40) {
            this.aa.setError(g().getString(R.string.lg_errlogin_fail2));
            this.aa.requestFocus();
            Selection.setSelection(this.aa.getEditableText(), this.aa.getEditableText().length());
        } else if (replaceAll.startsWith(".") || replaceAll.endsWith(".")) {
            this.aa.requestFocus();
            this.aa.setError(g().getString(R.string.reg_name_dot_start_or_end));
            Selection.setSelection(this.aa.getEditableText(), this.aa.getEditableText().length());
        } else if (en.b(replaceAll)) {
            this.aa.setError(null);
            if (this.ab.getText() == null || en.c(trim)) {
                this.ab.requestFocus();
                this.ab.setError(g().getString(R.string.prompt_input_password_err));
                Selection.setSelection(this.ab.getEditableText(), this.ab.getEditableText().length());
            } else if (Pattern.compile("[a-zA-Z0-9]{4,20}").matcher(trim).matches()) {
                this.ab.setError(null);
                if (1 != 0) {
                    this.ab.setError(null);
                    this.aa.setError(null);
                    this.N = this.aa.getText().toString().trim();
                    this.V.setClickable(false);
                    new Thread(this.at).start();
                }
            } else {
                this.ab.requestFocus();
                this.ab.setError(g().getString(R.string.prompt_passwrod_format_err));
                Selection.setSelection(this.ab.getEditableText(), this.ab.getEditableText().length());
            }
        } else {
            this.aa.requestFocus();
            this.aa.setError(g().getString(R.string.prompt_email_format_err));
            Selection.setSelection(this.aa.getEditableText(), this.aa.getEditableText().length());
        }
    }

    private void y() {
        final long currentTimeMillis = System.currentTimeMillis();
        if (currentTimeMillis - this.x > 20000 || en.c(as.a(getContext()).v()) || en.c(as.a(getContext()).u())) {
            new Thread() {
                public void run() {
                    LoginRegView.C = false;
                    LoginRegView.this.C();
                    LoginRegView.this.x = currentTimeMillis;
                    LoginRegView.C = true;
                }
            }.start();
        } else {
            D = true;
        }
        InputMethodManager inputMethodManager = (InputMethodManager) this.ac.getSystemService("input_method");
        inputMethodManager.hideSoftInputFromWindow(this.Y.getWindowToken(), 0);
        inputMethodManager.hideSoftInputFromWindow(this.Z.getWindowToken(), 0);
    }

    private void z() {
        final long currentTimeMillis = System.currentTimeMillis();
        if (currentTimeMillis - this.y > 20000 || en.c(as.a(getContext()).v()) || en.c(as.a(getContext()).u())) {
            new Thread() {
                public void run() {
                    boolean unused = LoginRegView.R = false;
                    LoginRegView.this.B();
                    LoginRegView.this.y = currentTimeMillis;
                    boolean unused2 = LoginRegView.R = true;
                }
            }.start();
        } else {
            E = true;
        }
        InputMethodManager inputMethodManager = (InputMethodManager) this.ac.getSystemService("input_method");
        inputMethodManager.hideSoftInputFromWindow(this.Y.getWindowToken(), 0);
        inputMethodManager.hideSoftInputFromWindow(this.Z.getWindowToken(), 0);
    }

    public void a(Bundle bundle) {
        this.ah = bundle;
    }

    public void a(EditText editText) {
        if (editText.getText().toString() != null) {
            editText.setSelection(editText.getText().toString().length());
        }
    }

    public boolean a(int i, KeyEvent keyEvent) {
        return super.a(i, keyEvent);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public void f() {
        inflate(g(), R.layout.login_reg_layout, this);
        this.ad = (ImageView) findViewById(R.id.show_list_image);
        this.ak = (RelativeLayout) findViewById(R.id.reg_edit_passwd);
        this.al = (RelativeLayout) findViewById(R.id.reg_edit_userName);
        this.am = (RelativeLayout) findViewById(R.id.login_edit_passwd);
        this.an = (RelativeLayout) findViewById(R.id.login_edit_userName);
        this.S = (LinearLayout) findViewById(R.id.login_name_layout);
        this.T = (LinearLayout) findViewById(R.id.reg_name_layout);
        this.aA = (Button) findViewById(R.id.r_login_btn);
        this.aB = (Button) findViewById(R.id.l_reg_btn);
        this.aC = (Button) findViewById(R.id.sina_login_btn);
        this.aD = (RelativeLayout) findViewById(R.id.login_btn_lable);
        this.aE = (RelativeLayout) findViewById(R.id.reg_btn_lable);
        this.W = (RelativeLayout) findViewById(R.id.login_layout);
        this.X = (RelativeLayout) findViewById(R.id.reg_layout);
        this.Y = (EditText) findViewById(R.id.login_user_name);
        this.Z = (EditText) findViewById(R.id.login_user_passwd);
        this.aa = (EditText) findViewById(R.id.reg_user_name);
        this.ab = (EditText) findViewById(R.id.reg_user_passwd);
        this.U = (Button) findViewById(R.id.login_btn);
        this.V = (Button) findViewById(R.id.reg_btn);
        this.as = LayoutInflater.from(this.ac).inflate((int) R.layout.register_success, (ViewGroup) null, false);
        this.ay = (EditText) this.as.findViewById(R.id.dm_reg_email);
        this.az = (EditText) this.as.findViewById(R.id.dm_reg_pwd);
        this.J = (Button) this.as.findViewById(R.id.dm_reg_btn_confirm);
        this.K = (Button) this.as.findViewById(R.id.dm_reg_btn_cancle);
        this.ar = new AlertDialog.Builder(this.ac).setTitle((int) R.string.sina_reg_success_title).create();
        this.av = LayoutInflater.from(this.ac).inflate((int) R.layout.register_weibo_finished, (ViewGroup) null, false);
        this.aG = (TextView) this.av.findViewById(R.id.dm_reg_weibo_username);
        this.aI = (TextView) this.av.findViewById(R.id.dm_reg_weibo_pwd);
        this.aH = (TextView) this.av.findViewById(R.id.dm_reg_weibo_prompt);
        this.O = (Button) this.av.findViewById(R.id.dm_reg_weibo_entryduomi);
        this.aJ = (TextView) this.av.findViewById(R.id.dm_reg_weibo_msg);
        this.au = new AlertDialog.Builder(this.ac).setTitle((int) R.string.sina_reg_success_title).create();
        this.au.setCancelable(false);
        this.au.setView(this.av);
        if (z) {
            new Thread() {
                public void run() {
                    LoginRegView.this.C();
                    LoginRegView.C = true;
                    LoginRegView.this.x = System.currentTimeMillis();
                }
            }.start();
            this.aD.setVisibility(0);
            this.W.setVisibility(0);
            this.aE.setVisibility(8);
            this.X.setVisibility(8);
        } else {
            new Thread() {
                public void run() {
                    LoginRegView.this.B();
                    boolean unused = LoginRegView.R = true;
                    LoginRegView.this.y = System.currentTimeMillis();
                }
            }.start();
            this.aD.setVisibility(8);
            this.W.setVisibility(8);
            this.aE.setVisibility(0);
            this.X.setVisibility(0);
        }
        v();
        this.V.setClickable(true);
        this.aC.setClickable(true);
        this.U.setClickable(true);
        this.aA.setClickable(true);
        this.aB.setClickable(true);
        this.aK = (ListView) findViewById(R.id.login_usernames_list);
        this.aK.setVisibility(8);
        A();
        if (!kf.b) {
            findViewById(R.id.sina_login_layout).setVisibility(8);
        }
        t();
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.l_reg_btn:
                this.aD.setVisibility(8);
                this.W.setVisibility(8);
                this.aE.setVisibility(0);
                this.X.setVisibility(0);
                z = false;
                z();
                return;
            case R.id.r_login_btn:
                this.aD.setVisibility(0);
                this.W.setVisibility(0);
                this.aE.setVisibility(8);
                this.X.setVisibility(8);
                z = true;
                y();
                return;
            case R.id.login_edit_userName:
                this.Y.requestFocus();
                return;
            case R.id.login_edit_passwd:
                this.Z.requestFocus();
                return;
            case R.id.show_list_image:
                Animation loadAnimation = AnimationUtils.loadAnimation(g(), R.anim.common_fade_in);
                Animation loadAnimation2 = AnimationUtils.loadAnimation(g(), R.anim.common_fade_out);
                if (this.aK.getVisibility() == 8) {
                    this.aK.startAnimation(loadAnimation);
                    this.aK.setVisibility(0);
                    return;
                }
                this.aK.startAnimation(loadAnimation2);
                this.aK.setVisibility(8);
                return;
            case R.id.login_btn:
                w();
                return;
            case R.id.reg_edit_userName:
                this.aa.requestFocus();
                return;
            case R.id.reg_edit_passwd:
                this.ab.requestFocus();
                return;
            case R.id.reg_btn:
                x();
                return;
            case R.id.sina_login_btn:
                E();
                return;
            case R.id.reg_uesrnam_confrim:
                if (this.aw != null && this.aw.isShowing()) {
                    this.aw.dismiss();
                }
                this.N = ((String) this.ae.get(this.ax)).toLowerCase();
                this.aa.setText(this.N.toLowerCase());
                new Thread(this.at).start();
                return;
            case R.id.reg_uesrnam_cancle:
                if (this.aw != null && this.aw.isShowing()) {
                    this.aw.dismiss();
                }
                this.V.setClickable(true);
                return;
            case R.id.dm_reg_btn_confirm:
                if (this.P) {
                    new Thread(this.I).start();
                    return;
                } else {
                    new Thread(this.ap).start();
                    return;
                }
            case R.id.dm_reg_btn_cancle:
                if (this.ar != null) {
                    this.ar.dismiss();
                }
                u();
                return;
            case R.id.dm_reg_weibo_entryduomi:
                if (this.au != null) {
                    this.au.dismiss();
                }
                this.V.setClickable(false);
                this.aC.setClickable(false);
                this.aA.setClickable(false);
                this.G.sendEmptyMessageDelayed(12, 300);
                return;
            default:
                return;
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        switch (i) {
            case 84:
                return true;
            default:
                return super.onKeyDown(i, keyEvent);
        }
    }

    public void q() {
        if (this.G != null) {
            this.G.sendMessage(this.G.obtainMessage(30, this.ac.getString(R.string.app_net_error)));
        }
        if (this.aF != null) {
            this.aF.dismiss();
        }
        if (this.H != null) {
            this.H.dismiss();
        }
        this.U.setClickable(true);
        this.V.setClickable(true);
    }

    public void r() {
        if (this.G != null) {
            this.G.sendMessage(this.G.obtainMessage(10, this.ac.getString(R.string.app_net_error)));
        }
        if (this.aF != null) {
            this.aF.dismiss();
        }
        if (this.H != null) {
            this.H.dismiss();
        }
        this.V.setClickable(true);
        this.U.setClickable(true);
    }
}
