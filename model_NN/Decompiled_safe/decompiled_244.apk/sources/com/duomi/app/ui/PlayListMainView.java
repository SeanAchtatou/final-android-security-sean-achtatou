package com.duomi.app.ui;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.duomi.android.R;
import java.util.ArrayList;

public class PlayListMainView extends c {
    /* access modifiers changed from: private */
    public static boolean y = false;
    /* access modifiers changed from: private */
    public ListView A;
    /* access modifiers changed from: private */
    public MyAdapter B;
    private TextView C;
    /* access modifiers changed from: private */
    public ArrayList D;
    private Button E;
    /* access modifiers changed from: private */
    public ar F;
    /* access modifiers changed from: private */
    public Activity G;
    /* access modifiers changed from: private */
    public ArrayList H;
    /* access modifiers changed from: private */
    public ArrayList I;
    private BroadcastReceiver J = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (!action.equals("com.duomi.android.app.scanner.scancomplete") || !fd.i) {
                if (action.equals("com.duomi.android.app.scanner.scanstart")) {
                    PlayListMainView.this.x.sendEmptyMessage(7);
                } else if (action.equals("com.duomi.android.app.scanner.scancancel")) {
                    PlayListMainView.this.x.sendEmptyMessage(8);
                } else if (action.equals("action_addtolist")) {
                    PlayListMainView.this.x.sendEmptyMessage(4);
                }
            } else if (PlayListMainView.this.A != null && PlayListMainView.this.B != null) {
                ad.b("PlayListMainView", "BroadcastReceiver:onReceive()>>>");
                PlayListMainView.this.x.sendEmptyMessage(0);
                if (PlayListMainView.this.d != null) {
                    ((PlayListGroupView) PlayListMainView.this.d).y.clear();
                    PlayListMainView.this.d.a(PlayListMainView.class);
                }
            }
        }
    };
    private AdapterView.OnItemClickListener K = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView adapterView, View view, int i, long j) {
            if (i != 3) {
                boolean unused = PlayListMainView.this.a(i, PlayListMainView.this.a);
                if (PlayListMainView.this.a) {
                    switch (i) {
                        case 0:
                            fd.a().a(PlayListMainView.this.G, (Handler) null);
                            return;
                        case 1:
                        default:
                            return;
                        case 2:
                            PopDialogView.a(PlayListMainView.this.g(), (Handler) null);
                            return;
                    }
                } else {
                    switch (i) {
                        case 1:
                            fd.a().a(PlayListMainView.this.G, (Handler) null);
                            return;
                        case 2:
                            PopDialogView.a(PlayListMainView.this.g(), (Handler) null);
                            return;
                        default:
                            return;
                    }
                }
            } else if (PlayListMainView.this.g.getVisibility() == 0) {
                PlayListMainView.this.j();
            } else {
                PlayListMainView.this.i();
            }
        }
    };
    private AdapterView.OnItemClickListener L = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView adapterView, View view, int i, long j) {
            boolean unused = PlayListMainView.this.b(i);
        }
    };
    private View.OnClickListener M = new View.OnClickListener() {
        public void onClick(View view) {
            hk.a(PlayListMainView.this.G);
        }
    };
    Handler x = new Handler() {
        public void handleMessage(Message message) {
            switch (message.what) {
                case 0:
                    ad.b("PlayListMainView", "getListData()userid>>>" + bn.a().h());
                    if (!as.a(PlayListMainView.this.G).ac()) {
                        ArrayList unused = PlayListMainView.this.H = PlayListMainView.this.F.f(bn.a().h());
                    } else {
                        ArrayList unused2 = PlayListMainView.this.H = PlayListMainView.this.F.e(bn.a().h());
                    }
                    int size = PlayListMainView.this.H.size();
                    for (int i = 0; i < size; i++) {
                        PlayListMainView.this.D.add(0);
                    }
                    new GetDataTask().execute(new Object[0]);
                    PlayListMainView.this.z.setVisibility(8);
                    PlayListMainView.this.z.postInvalidate();
                    ad.b("PlayListMainView", "------------REFRESH_SCAN_END_TIP-------------");
                    break;
                case 3:
                    new GetDataTask().execute(new Object[0]);
                    break;
                case 4:
                    new GetDataTask().execute(new Object[0]);
                    break;
                case 5:
                    if (PlayListMainView.this.B != null) {
                        PlayListMainView.this.B.notifyDataSetChanged();
                        break;
                    } else {
                        MyAdapter unused3 = PlayListMainView.this.B = new MyAdapter(PlayListMainView.this.g(), PlayListMainView.this.H, PlayListMainView.this.D);
                        PlayListMainView.this.A.setAdapter((ListAdapter) PlayListMainView.this.B);
                        break;
                    }
                case 6:
                    boolean unused4 = PlayListMainView.y = false;
                    break;
                case 7:
                    PlayListMainView.this.z.setVisibility(0);
                    PlayListMainView.this.z.postInvalidate();
                    ad.b("PlayListMainView", "------------REFRESH_SCAN_START_TIP-------------");
                    break;
                case 8:
                    PlayListMainView.this.z.setVisibility(8);
                    PlayListMainView.this.z.postInvalidate();
                    ad.b("PlayListMainView", "------------REFRESH_SCAN_CANCEL_TIP-------------");
                    break;
            }
            super.handleMessage(message);
        }
    };
    /* access modifiers changed from: private */
    public View z;

    class GetDataTask extends AsyncTask {
        private GetDataTask() {
        }

        /* access modifiers changed from: protected */
        public Object doInBackground(Object... objArr) {
            PlayListMainView.this.r();
            return null;
        }
    }

    class ItemStruct {
        ImageView a;
        TextView b;
        TextView c;
        ImageView d;

        ItemStruct() {
        }
    }

    class MyAdapter extends BaseAdapter {
        ItemStruct a;
        private LayoutInflater c;

        public MyAdapter(Activity activity, ArrayList arrayList, ArrayList arrayList2) {
            this.c = LayoutInflater.from(activity);
        }

        public int getCount() {
            return PlayListMainView.this.H.size();
        }

        public Object getItem(int i) {
            return PlayListMainView.this.H.get(i);
        }

        public long getItemId(int i) {
            return (long) i;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(int i, View view, ViewGroup viewGroup) {
            View view2;
            if (view == null) {
                this.a = new ItemStruct();
                view2 = this.c.inflate((int) R.layout.playlist_main_row, viewGroup, false);
                this.a.a = (ImageView) view2.findViewById(R.id.list_image);
                this.a.b = (TextView) view2.findViewById(R.id.list_title);
                this.a.c = (TextView) view2.findViewById(R.id.sub_count);
                this.a.d = (ImageView) view2.findViewById(R.id.list_arrow);
                view2.setTag(this.a);
            } else {
                this.a = (ItemStruct) view.getTag();
                view2 = view;
            }
            int i2 = 7;
            if (PlayListMainView.this.a) {
                i2 = 6;
            }
            if (i < i2) {
                this.a.a.setImageResource(((Integer) PlayListMainView.this.I.get(i)).intValue());
            } else {
                this.a.a.setImageResource(R.drawable.list_user_defined_icon);
            }
            this.a.b.setText(((bl) PlayListMainView.this.H.get(i)).h());
            this.a.c.setText(PlayListMainView.this.D.get(i) == null ? "" : String.valueOf(PlayListMainView.this.D.get(i)));
            this.a.d.setImageResource(R.drawable.list_arrow_icon);
            return view2;
        }
    }

    public PlayListMainView(Activity activity) {
        super(activity);
        this.G = activity;
    }

    /* access modifiers changed from: private */
    public void r() {
        int size = this.H.size();
        ad.b("PlayListMainView", "log>>>>>>>>" + this.H.size());
        for (int i = 0; i < size; i++) {
            ad.b("PlayListMainView", "log>>>>>>>>" + ((bl) this.H.get(i)).g() + ">>>" + ((bl) this.H.get(i)).h());
            if (((bl) this.H.get(i)).i() == 1) {
                this.D.set(i, Integer.valueOf(this.F.a(((bl) this.H.get(i)).g())));
            } else {
                try {
                    this.D.set(i, null);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        this.x.sendEmptyMessage(5);
    }

    private void s() {
        this.a = !as.a(this.G).ac();
        this.C.setText((int) R.string.playlist_main_title);
        this.E.setText((int) R.string.app_playlist_sync);
        this.I = new ArrayList();
        this.I.add(Integer.valueOf((int) R.drawable.list_playlist_icon));
        this.I.add(Integer.valueOf((int) R.drawable.list_music_icon));
        this.I.add(Integer.valueOf((int) R.drawable.list_like_icon));
        if (!this.a) {
            this.I.add(Integer.valueOf((int) R.drawable.list_download_icon));
        }
        this.I.add(Integer.valueOf((int) R.drawable.list_singer_icon));
        this.I.add(Integer.valueOf((int) R.drawable.list_cd_icon));
        this.I.add(Integer.valueOf((int) R.drawable.list_folder_icon));
        this.D = new ArrayList();
        ad.b("PlayListMainView", "getListData()userid>>>" + bn.a().h());
        if (this.a) {
            this.H = this.F.f(bn.a().h());
        } else {
            this.H = this.F.e(bn.a().h());
        }
        int size = this.H.size();
        for (int i = 0; i < size; i++) {
            this.D.add(0);
        }
        new GetDataTask().execute(new Object[0]);
        this.A.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView adapterView, View view, int i, long j) {
                if (!PlayListMainView.y) {
                    Message message = new Message();
                    message.obj = PlayListMainView.this.H.get(i);
                    int i2 = 7;
                    if (PlayListMainView.this.a) {
                        i2 = 6;
                    }
                    if (i < i2 - 3 || i >= i2) {
                        ((PlayListGroupView) PlayListMainView.this.d).y.push(1);
                        PlayListMainView.this.d.a(PlayListMusicView.class, message);
                        return;
                    }
                    ((PlayListGroupView) PlayListMainView.this.d).y.push(0);
                    PlayListMainView.this.d.a(CategoryListView.class, message);
                }
            }
        });
        this.A.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView adapterView, View view, int i, long j) {
                boolean unused = PlayListMainView.y = true;
                if (i > (PlayListMainView.this.a ? 5 : 6)) {
                    int[] iArr = new int[2];
                    view.getLocationInWindow(iArr);
                    new PopDialogView(PlayListMainView.this.G).a((bl) PlayListMainView.this.H.get(i), PlayListMainView.this.d, iArr[1]);
                }
                PlayListMainView.this.x.sendEmptyMessageDelayed(6, 1000);
                return false;
            }
        });
    }

    public void a(Message message) {
        super.a(message);
        y = false;
        s();
        h();
    }

    public boolean a(int i, KeyEvent keyEvent) {
        b(i, keyEvent);
        if (i != 4 || this.f.getVisibility() != 0 || this.u) {
            return false;
        }
        if (this.g.getVisibility() == 0) {
            this.g.setVisibility(8);
            this.i.requestFocus();
        } else {
            this.f.setVisibility(8);
        }
        return true;
    }

    public void c() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.duomi.android.app.scanner.scancomplete");
        intentFilter.addAction("com.duomi.android.app.scanner.scanstart");
        intentFilter.addAction("com.duomi.android.app.scanner.scancancel");
        intentFilter.addAction("action_addtolist");
        g().registerReceiver(this.J, new IntentFilter(intentFilter));
    }

    public void d() {
        ad.b("PlayListMainView", "onPause>>>>>>>>>>>>>unregisterReceiver");
        if (this.J != null) {
            g().unregisterReceiver(this.J);
        }
    }

    public void f() {
        Window window = g().getWindow();
        if (window != null) {
            window.clearFlags(1024);
        }
        inflate(g(), R.layout.playlist_main, this);
        this.C = (TextView) findViewById(R.id.list_title);
        this.A = (ListView) findViewById(R.id.playlist_main);
        this.z = findViewById(R.id.scanning_tip);
        this.E = (Button) findViewById(R.id.command);
        this.E.setOnClickListener(this.M);
        this.F = ar.a(this.G);
        s();
        h();
    }

    public void n() {
        this.i.setOnItemClickListener(this.K);
        this.j.setOnItemClickListener(this.L);
    }

    public void o() {
        this.s = new ArrayList();
        if (this.a) {
            this.s.add(Integer.valueOf((int) R.drawable.meun_scan));
            this.s.add(Integer.valueOf((int) R.drawable.menu_bg));
            this.q = getResources().getStringArray(R.array.PlayListMain_menu_without_download);
        } else {
            this.s.add(Integer.valueOf((int) R.drawable.meun_downloadsetup));
            this.s.add(Integer.valueOf((int) R.drawable.meun_scan));
            this.q = getResources().getStringArray(R.array.PlayListMain_menu);
        }
        this.s.add(Integer.valueOf((int) R.drawable.meun_createlist));
        this.s.add(Integer.valueOf((int) R.drawable.meun_more));
        this.s.add(Integer.valueOf((int) R.drawable.meun_setup));
        this.s.add(Integer.valueOf((int) R.drawable.meun_sleep));
        this.s.add(Integer.valueOf((int) R.drawable.meun_syc));
        this.s.add(Integer.valueOf((int) R.drawable.meun_exit));
        this.r = getResources().getStringArray(R.array.second_menu2);
        this.t = new ArrayList();
        this.t.add(Integer.valueOf((int) R.drawable.meun_help));
        this.t.add(Integer.valueOf((int) R.drawable.meun_advice));
        this.t.add(Integer.valueOf((int) R.drawable.meun_friends));
        this.t.add(Integer.valueOf((int) R.drawable.meun_about));
    }

    /* access modifiers changed from: protected */
    public void p() {
        s();
        super.p();
    }
}
