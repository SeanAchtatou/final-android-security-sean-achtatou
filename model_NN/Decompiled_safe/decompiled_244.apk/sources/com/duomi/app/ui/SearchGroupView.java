package com.duomi.app.ui;

import android.app.Activity;

public class SearchGroupView extends UIGroup {
    SearchView x;
    SearchResultView y;
    DialogView z;

    public SearchGroupView(Activity activity) {
        super(activity);
    }

    /* access modifiers changed from: protected */
    public void f() {
        this.x = new SearchView(g());
        this.x.setVisibility(8);
        this.x.a(this);
        addView(this.x);
        this.y = new SearchResultView(g());
        this.y.setVisibility(8);
        this.y.a(this);
        addView(this.y);
        this.z = new DialogView(g());
        this.z.setVisibility(8);
        this.z.a(this);
        addView(this.z);
        a(SearchView.class);
    }
}
