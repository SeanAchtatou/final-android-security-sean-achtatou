package com.duomi.app.ui;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.duomi.android.R;
import java.util.ArrayList;

public class DownloadManagerView extends c {
    public static boolean x = false;
    private ImageView A;
    private RelativeLayout B;
    private ListView C;
    /* access modifiers changed from: private */
    public ArrayList D;
    private MyAdapter E;
    /* access modifiers changed from: private */
    public RefreshHandler F;
    private int G = 0;
    private String H;
    private boolean I = false;
    private View.OnClickListener J = new View.OnClickListener() {
        public void onClick(View view) {
            p.a(DownloadManagerView.this.g()).a(MultiView.class.getName());
        }
    };
    private AdapterView.OnItemClickListener K = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView adapterView, View view, int i, long j) {
            boolean z;
            if (ev.a != null && DownloadManagerView.this.D != null && i < DownloadManagerView.this.D.size() && DownloadManagerView.this.D.get(i) != null) {
                int j2 = ((az) DownloadManagerView.this.D.get(i)).j();
                boolean z2 = j2 == 2;
                if (j2 == 1) {
                    ad.b("DownloadManagerView", "percent is >>" + ((az) DownloadManagerView.this.D.get(i)).n() + ">>" + j2);
                    z = true;
                } else if (j2 != 2) {
                    ad.b("DownloadManagerView", "percent is >>" + ((az) DownloadManagerView.this.D.get(i)).n() + ">>" + j2);
                    z = false;
                } else {
                    z = false;
                }
                if (z) {
                    try {
                        ad.b("DownloadManagerView", "prepare pause >>" + i);
                        ev.a.b(i);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                } else {
                    ad.b("DownloadManagerView", "prepare start >>" + i);
                    az azVar = (az) DownloadManagerView.this.D.get(i);
                    if (z2) {
                        dl a2 = dl.a(DownloadManagerView.this.getContext());
                        az b = al.a(DownloadManagerView.this.getContext()).b(bn.a().h(), azVar.d());
                        if (b != null) {
                            azVar.a(b.a());
                        }
                        a2.a(true);
                        a2.a(azVar);
                        dl.a = a2.b(1);
                        bj g = ar.a(DownloadManagerView.this.getContext()).g(azVar.d());
                        if (g == null) {
                            g = new bj();
                            g.f(azVar.d());
                            g.g(azVar.h());
                        }
                        a2.a(azVar.a(), g);
                        a2.a(i);
                        a2.b();
                        return;
                    }
                    ev.a.c(i);
                }
            }
        }
    };
    private BroadcastReceiver L = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals("start")) {
                DownloadManagerView.this.F.removeMessages(1);
                DownloadManagerView.this.F.obtainMessage(1).sendToTarget();
            } else if (action.equals("stop")) {
                DownloadManagerView.this.F.removeMessages(1);
            }
        }
    };
    private final String y = "DownloadManagerView";
    private TextView z;

    /* renamed from: com.duomi.app.ui.DownloadManagerView$4  reason: invalid class name */
    class AnonymousClass4 implements DialogInterface.OnClickListener {
        public void onClick(DialogInterface dialogInterface, int i) {
            if (ev.a != null) {
                try {
                    ev.a.d();
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    class MyAdapter extends BaseAdapter {
        private LayoutInflater b;

        class ItemStruct {
            ImageView a;
            TextView b;
            TextView c;
            TextView d;
            ProgressBar e;
            ImageView f;

            private ItemStruct() {
            }
        }

        public MyAdapter(Context context, ArrayList arrayList) {
            this.b = LayoutInflater.from(context);
        }

        public int getCount() {
            if (DownloadManagerView.this.D != null) {
                return DownloadManagerView.this.D.size();
            }
            return 0;
        }

        public Object getItem(int i) {
            return DownloadManagerView.this.D.get(i);
        }

        public long getItemId(int i) {
            return (long) i;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(final int i, View view, ViewGroup viewGroup) {
            ItemStruct itemStruct;
            View view2;
            if (view == null) {
                View inflate = this.b.inflate((int) R.layout.downloadmanager_row, viewGroup, false);
                ItemStruct itemStruct2 = new ItemStruct();
                itemStruct2.a = (ImageView) inflate.findViewById(R.id.downloadmanager_state);
                itemStruct2.b = (TextView) inflate.findViewById(R.id.downloadmanager_percent);
                itemStruct2.d = (TextView) inflate.findViewById(R.id.downloadmanager_info);
                itemStruct2.c = (TextView) inflate.findViewById(R.id.downloadmanager_name);
                itemStruct2.e = (ProgressBar) inflate.findViewById(R.id.downloadmanager_progress);
                itemStruct2.f = (ImageView) inflate.findViewById(R.id.downloadmanager_cancel);
                itemStruct2.e.setMax(100);
                itemStruct2.e.setBackgroundResource(R.drawable.download_progress_bg);
                inflate.setTag(itemStruct2);
                ItemStruct itemStruct3 = itemStruct2;
                view2 = inflate;
                itemStruct = itemStruct3;
            } else {
                itemStruct = (ItemStruct) view.getTag();
                view2 = view;
            }
            itemStruct.f.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    try {
                        if (ev.a != null) {
                            ev.a.a(i);
                        }
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                }
            });
            int b2 = en.b(((az) DownloadManagerView.this.D.get(i)).l(), ((az) DownloadManagerView.this.D.get(i)).m());
            int i2 = b2 > 100 ? 0 : b2;
            switch (((az) DownloadManagerView.this.D.get(i)).j()) {
                case 1:
                    itemStruct.a.setImageResource(R.drawable.download_loading);
                    itemStruct.d.setText(((az) DownloadManagerView.this.D.get(i)).v());
                    break;
                case 2:
                    itemStruct.a.setImageResource(R.drawable.download_pause);
                    itemStruct.d.setText(new StringBuffer().append(en.c(((az) DownloadManagerView.this.D.get(i)).l())).append("/").append(en.c(((az) DownloadManagerView.this.D.get(i)).m())).append("  ").append(DownloadManagerView.this.getContext().getString(R.string.download_pause_tip)).toString());
                    break;
                case 3:
                    itemStruct.a.setImageResource(R.drawable.download_waiting);
                    itemStruct.d.setText(new StringBuffer(DownloadManagerView.this.getContext().getString(R.string.download_waiting_tip)));
                    break;
                case 4:
                    itemStruct.a.setImageResource(R.drawable.download_wrong);
                    itemStruct.d.setText(new StringBuffer(DownloadManagerView.this.getContext().getString(R.string.download_wrong_tip)));
                    break;
            }
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(((az) DownloadManagerView.this.D.get(i)).h()).append("_").append(((az) DownloadManagerView.this.D.get(i)).i());
            itemStruct.c.setText(en.l(stringBuffer.toString()));
            itemStruct.b.setText(i2 + "%");
            itemStruct.e.setProgress(i2);
            itemStruct.f.setImageResource(R.drawable.download_delete);
            return view2;
        }
    }

    class RefreshHandler extends Handler {
        public RefreshHandler(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message message) {
            switch (message.what) {
                case 1:
                    DownloadManagerView.this.q();
                    break;
            }
            super.handleMessage(message);
        }
    }

    public DownloadManagerView(Activity activity) {
        super(activity);
    }

    /* access modifiers changed from: private */
    public void q() {
        if (ev.a != null) {
            try {
                this.D = (ArrayList) ev.a.a();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            if (this.D == null || this.D.size() <= 0) {
                if (this.G != 0) {
                    this.G = 0;
                    this.z.setText(this.H.replace("n", this.G + ""));
                }
                ad.b("DownloadManagerView", "mdownloadlist sizeis null");
                if (this.E != null) {
                    this.E.notifyDataSetChanged();
                } else {
                    this.E = new MyAdapter(getContext(), this.D);
                    this.C.setAdapter((ListAdapter) this.E);
                }
            } else {
                ad.b("DownloadManagerView", "mdownloadlist size >>" + this.D.size());
                if (this.G != this.D.size()) {
                    this.G = this.D.size();
                    this.z.setText(this.H.replace("n", this.G + ""));
                }
                if (this.E == null) {
                    this.E = new MyAdapter(getContext(), this.D);
                    this.C.setAdapter((ListAdapter) this.E);
                } else {
                    this.E.notifyDataSetChanged();
                }
            }
            this.F.removeMessages(1);
            this.F.sendEmptyMessageDelayed(1, 1000);
        }
    }

    public void a() {
        super.a();
        if (!this.I) {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("start");
            intentFilter.addAction("stop");
            g().registerReceiver(this.L, new IntentFilter(intentFilter));
            this.I = true;
        }
        x = false;
        ad.b("DownloadManagerView", "downloadmanager is show");
        this.F = new RefreshHandler(g().getMainLooper());
        this.F.sendEmptyMessage(1);
    }

    public boolean a(int i, KeyEvent keyEvent) {
        if (i != 4) {
            return false;
        }
        p.a(g()).a(MultiView.class.getName());
        return true;
    }

    public void b() {
        super.b();
        if (this.c) {
            if (this.F != null) {
                this.F.removeMessages(1);
            }
            if (this.L != null && this.I) {
                g().unregisterReceiver(this.L);
                this.I = false;
            }
        }
        ad.b("DownloadManagerView", "gone is passed");
    }

    public void c() {
    }

    public void d() {
        if (this.L != null && this.I) {
            g().unregisterReceiver(this.L);
            this.I = false;
        }
    }

    public void f() {
        inflate(g(), R.layout.downloadmanager, this);
        this.z = (TextView) findViewById(R.id.downloadmanager_list_title);
        this.A = (ImageView) findViewById(R.id.downloadmanager_back);
        this.B = (RelativeLayout) findViewById(R.id.downloadmanager_titlelayout);
        this.C = (ListView) findViewById(R.id.downloadmanager_list);
        this.H = getContext().getString(R.string.downloadmanager_title);
        this.z.setText(this.H.replace("n", this.G + ""));
        this.A.setImageResource(R.drawable.list_return_icon);
        this.B.setOnClickListener(this.J);
        this.C.setOnItemClickListener(this.K);
    }
}
