package com.duomi.app.ui;

import android.app.Activity;
import android.os.Message;
import android.view.MotionEvent;
import android.view.View;

public abstract class UIGroup extends c implements jt {
    public UIGroup(Activity activity) {
        super(activity);
    }

    private void q() {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            if (childAt.getVisibility() != 8) {
                childAt.setVisibility(8);
                return;
            }
        }
    }

    public void a(Class cls) {
        q();
        String name = cls.getName();
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            String name2 = childAt.getClass().getName();
            ad.b("UIGroup", "showChild:cName:" + name + " tName:" + name2);
            if (name2.equals(name)) {
                ((b) childAt).a();
                ((b) childAt).c();
                return;
            }
        }
    }

    public void a(Class cls, Message message) {
        q();
        String name = cls.getName();
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            String name2 = childAt.getClass().getName();
            ad.b("UIGroup", "showChild:cName:" + name + " tName:" + name2);
            if (name2.equals(name)) {
                ((b) childAt).a();
                ((b) childAt).a(message);
                ((b) childAt).c();
                return;
            }
        }
    }

    public boolean a(MotionEvent motionEvent) {
        int childCount = getChildCount();
        int i = 0;
        while (true) {
            if (i >= childCount) {
                break;
            }
            View childAt = getChildAt(i);
            if (childAt.getVisibility() == 8) {
                i++;
            } else if (childAt instanceof jt) {
                return ((jt) childAt).a(motionEvent);
            }
        }
        return false;
    }

    public void d() {
        super.d();
    }
}
