package com.duomi.app.ui;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.duomi.android.R;

public class FirstTipView extends View {
    boolean a = true;
    AlphaAnimation b = null;
    ImageView[] c = new ImageView[5];
    ImageView[] d = new ImageView[5];
    TextView[] e = new TextView[5];
    RelativeLayout[] f = new RelativeLayout[5];
    Button g;
    int h = -1;
    WindowManager i;
    final WindowManager.LayoutParams j = new WindowManager.LayoutParams();
    RelativeLayout k = null;
    Handler l;
    /* access modifiers changed from: private */
    public Handler m = new Handler() {
        public void handleMessage(Message message) {
            switch (message.what) {
                case 2:
                    for (ImageView background : FirstTipView.this.c) {
                        ((AnimationDrawable) background.getBackground()).start();
                    }
                    for (int i = 0; i < FirstTipView.this.d.length; i++) {
                        if (true == FirstTipView.this.d[i].isShown() && FirstTipView.this.b != null) {
                            FirstTipView.this.d[i].startAnimation(FirstTipView.this.b);
                        }
                    }
                    return;
                case 3:
                    for (RelativeLayout clickable : FirstTipView.this.f) {
                        clickable.setClickable(false);
                    }
                    FirstTipView.this.m.sendEmptyMessageDelayed(4, 200);
                    return;
                case 4:
                    for (RelativeLayout clickable2 : FirstTipView.this.f) {
                        clickable2.setClickable(true);
                    }
                    return;
                default:
                    return;
            }
        }
    };

    public FirstTipView(Activity activity, Handler handler) {
        super(activity);
        a(activity);
        this.l = handler;
        this.b = new AlphaAnimation(0.1f, 1.0f);
        this.b.setDuration(200);
        this.m.sendEmptyMessage(2);
    }

    public void a() {
        this.g.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (FirstTipView.this.k.getParent() != null) {
                    FirstTipView.this.i.removeView(FirstTipView.this.k);
                }
                FirstTipView.this.l.sendEmptyMessage(2);
            }
        });
        for (RelativeLayout onClickListener : this.f) {
            onClickListener.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    FirstTipView.this.m.sendEmptyMessage(3);
                    for (int i = 0; i < FirstTipView.this.f.length; i++) {
                        if (view.getId() == FirstTipView.this.f[i].getId()) {
                            Log.d("FirstTipView", "<<<<<<<<" + i);
                            if (i != FirstTipView.this.h || FirstTipView.this.h == -1) {
                                FirstTipView.this.d[i].setVisibility(0);
                                FirstTipView.this.h = i;
                                if (FirstTipView.this.b != null) {
                                    FirstTipView.this.d[i].startAnimation(FirstTipView.this.b);
                                }
                            } else {
                                return;
                            }
                        } else {
                            FirstTipView.this.d[i].setVisibility(4);
                        }
                    }
                }
            });
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public void a(Context context) {
        this.k = (RelativeLayout) ((LayoutInflater) context.getSystemService("layout_inflater")).inflate((int) R.layout.floatview, (ViewGroup) null, false);
        this.k.setBackgroundResource(R.drawable.setting_layout_bg3);
        this.c[0] = (ImageView) this.k.findViewById(R.id.circle1);
        this.c[1] = (ImageView) this.k.findViewById(R.id.circle2);
        this.c[2] = (ImageView) this.k.findViewById(R.id.circle3);
        this.c[3] = (ImageView) this.k.findViewById(R.id.circle4);
        this.c[4] = (ImageView) this.k.findViewById(R.id.circle5);
        this.e[0] = (TextView) this.k.findViewById(R.id.title1);
        this.e[1] = (TextView) this.k.findViewById(R.id.title2);
        this.e[2] = (TextView) this.k.findViewById(R.id.title3);
        this.e[3] = (TextView) this.k.findViewById(R.id.title4);
        this.e[4] = (TextView) this.k.findViewById(R.id.title5);
        this.d[0] = (ImageView) this.k.findViewById(R.id.funDesc1);
        this.d[1] = (ImageView) this.k.findViewById(R.id.funDesc2);
        this.d[2] = (ImageView) this.k.findViewById(R.id.funDesc3);
        this.d[3] = (ImageView) this.k.findViewById(R.id.funDesc4);
        this.d[4] = (ImageView) this.k.findViewById(R.id.funDesc5);
        this.f[0] = (RelativeLayout) this.k.findViewById(R.id.touch_area1);
        this.f[1] = (RelativeLayout) this.k.findViewById(R.id.touch_area2);
        this.f[2] = (RelativeLayout) this.k.findViewById(R.id.touch_area3);
        this.f[3] = (RelativeLayout) this.k.findViewById(R.id.touch_area4);
        this.f[4] = (RelativeLayout) this.k.findViewById(R.id.touch_area5);
        this.g = (Button) this.k.findViewById(R.id.button);
        switch (en.b(getContext())) {
            case 640:
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
                layoutParams.setMargins(325, 850, 0, 0);
                this.g.setLayoutParams(layoutParams);
                break;
        }
        this.i = (WindowManager) context.getSystemService("window");
        this.j.height = -1;
        this.j.width = -1;
        this.j.x = 0;
        this.j.y = 0;
        this.j.flags = 1224;
        this.j.format = -3;
        a();
        b();
    }

    public void b() {
        for (ImageView backgroundResource : this.c) {
            backgroundResource.setBackgroundResource(R.anim.framecircle);
        }
        if (!(this.k == null || this.k.getParent() == null)) {
            this.i.removeView(this.k);
        }
        this.i.addView(this.k, this.j);
    }
}
