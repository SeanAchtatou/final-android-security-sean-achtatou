package com.duomi.app.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.duomi.android.R;
import com.duomi.app.ui.MultiView;
import java.util.ArrayList;
import java.util.List;

public class SearchResultView extends c implements View.OnClickListener {
    /* access modifiers changed from: private */
    public static int[] G = new int[2];
    private ImageView A;
    private RelativeLayout B;
    private Message C;
    private Button D;
    /* access modifiers changed from: private */
    public MyAdapter E;
    private ListView F;
    /* access modifiers changed from: private */
    public int H = -1;
    private View I;
    private boolean J = false;
    private Dialog K;
    /* access modifiers changed from: private */
    public Context L;
    /* access modifiers changed from: private */
    public List M;
    private List N;
    private RelativeLayout O;
    private RelativeLayout P;
    private RelativeLayout Q;
    private RelativeLayout R;
    private RelativeLayout S;
    private jg T;
    /* access modifiers changed from: private */
    public ar U;
    private AdapterView.OnItemClickListener V = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView adapterView, View view, int i, long j) {
            gx.a(SearchResultView.this.getContext(), PlayListOnLineMusicView.a(SearchResultView.this.getContext(), (cx) SearchResultView.this.M.get(i), i));
        }
    };
    private Handler W = new Handler() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: gx.a(android.content.Context, bj, bl, com.duomi.app.ui.MultiView$OnLikeReSetListener, boolean):void
         arg types: [android.content.Context, bj, bl, ?[OBJECT, ARRAY], int]
         candidates:
          gx.a(android.content.Context, bj, int, android.os.Handler, com.duomi.app.ui.MultiView$OnLikeReSetListener):void
          gx.a(android.content.Context, bl, long, com.duomi.app.ui.MultiView$OnLikeReSetListener, int):void
          gx.a(android.content.Context, bj, bl, com.duomi.app.ui.MultiView$OnLikeReSetListener, boolean):void */
        public void handleMessage(Message message) {
            switch (message.what) {
                case 255:
                    bl d = ar.a(SearchResultView.this.getContext()).d(message.getData().getInt("listId"));
                    gx.a(SearchResultView.this.L, PlayListOnLineMusicView.a(SearchResultView.this.getContext(), (cx) SearchResultView.this.M.get(SearchResultView.this.H)), d, (MultiView.OnLikeReSetListener) null, false);
                    Intent intent = new Intent("com.duomi.android.app.scanner.scancomplete");
                    intent.setAction("com.duomi.android.app.scanner.scancomplete");
                    SearchResultView.this.L.sendBroadcast(intent);
                    return;
                default:
                    return;
            }
        }
    };
    private AdapterView.OnItemClickListener X = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView adapterView, View view, int i, long j) {
            SearchResultView.this.f.setVisibility(8);
            boolean unused = SearchResultView.this.b(i);
        }
    };
    bh x;
    public AdapterView.OnItemClickListener y = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView adapterView, View view, int i, long j) {
            if (i == 3) {
                if (SearchResultView.this.g.getVisibility() == 0) {
                    SearchResultView.this.j();
                } else {
                    SearchResultView.this.i();
                }
            } else if (SearchResultView.this.a) {
                boolean unused = SearchResultView.this.a(i, SearchResultView.this.a);
                switch (i) {
                    case 0:
                        new PopDialogView(SearchResultView.this.getContext()).c(SearchResultView.this.d);
                        return;
                    default:
                        return;
                }
            } else {
                boolean unused2 = SearchResultView.this.a(i);
                switch (i) {
                    case 1:
                        new PopDialogView(SearchResultView.this.getContext()).c(SearchResultView.this.d);
                        return;
                    default:
                        return;
                }
            }
        }
    };
    private TextView z;

    class ItemStruct {
        TextView a;
        TextView b;
        TextView c;
        RatingBar d;
        TextView e;
        ImageView f;

        ItemStruct() {
        }
    }

    class MyAdapter extends BaseAdapter {
        ItemStruct a;
        private LayoutInflater c;
        /* access modifiers changed from: private */
        public List d;
        /* access modifiers changed from: private */
        public List e;

        private MyAdapter(Context context, List list, List list2) {
            this.c = LayoutInflater.from(context);
            this.d = list;
            this.e = list2;
        }

        public int getCount() {
            return this.d.size();
        }

        public Object getItem(int i) {
            return this.d.get(i);
        }

        public long getItemId(int i) {
            return (long) i;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(final int i, View view, ViewGroup viewGroup) {
            View view2;
            if (view == null) {
                this.a = new ItemStruct();
                View inflate = this.c.inflate((int) R.layout.playlist_online_music_row, viewGroup, false);
                this.a.a = (TextView) inflate.findViewById(R.id.playlist_online_song);
                this.a.b = (TextView) inflate.findViewById(R.id.playlist_online_singer);
                this.a.c = (TextView) inflate.findViewById(R.id.playlist_online_time);
                this.a.d = (RatingBar) inflate.findViewById(R.id.pop_value);
                this.a.e = (TextView) inflate.findViewById(R.id.playlist_online_sttime);
                this.a.f = (ImageView) inflate.findViewById(R.id.add_image);
                inflate.setTag(this.a);
                view2 = inflate;
            } else {
                this.a = (ItemStruct) view.getTag();
                view2 = view;
            }
            LinearLayout linearLayout = (LinearLayout) view2.findViewById(R.id.add_layout);
            String d2 = ((cx) this.d.get(i)).d();
            String h = ((cx) this.d.get(i)).h();
            String i2 = ((cx) this.d.get(i)).i();
            if (!en.c(d2)) {
                this.a.a.setText((i + 1) + "." + d2);
                this.a.c.setVisibility(8);
                this.a.b.setText(h);
                this.a.d.setVisibility(8);
                this.a.e.setVisibility(0);
                this.a.e.setText(i2);
            } else {
                this.a.c.setVisibility(0);
                this.a.d.setVisibility(0);
                this.a.e.setVisibility(8);
                this.a.a.setText((i + 1) + "." + ((cx) this.d.get(i)).f());
                this.a.b.setText(((cx) this.d.get(i)).a());
                this.a.c.setText(en.d(Integer.valueOf(((cx) this.d.get(i)).b()).intValue() * 1000));
            }
            String c2 = ((cx) this.d.get(i)).c();
            this.a.d.setRating(Float.valueOf(c2.substring(c2.lastIndexOf("|") + 1, c2.length())).floatValue() / 2.0f);
            if (((Integer) this.e.get(i)).intValue() == 1) {
                this.a.f.setImageResource(R.drawable.online_addicon_f);
            } else {
                this.a.f.setImageResource(R.drawable.online_addicon_normal);
            }
            linearLayout.setOnClickListener(new View.OnClickListener() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: gx.a(android.content.Context, bj, bl, com.duomi.app.ui.MultiView$OnLikeReSetListener, boolean):void
                 arg types: [android.content.Context, bj, bl, ?[OBJECT, ARRAY], int]
                 candidates:
                  gx.a(android.content.Context, bj, int, android.os.Handler, com.duomi.app.ui.MultiView$OnLikeReSetListener):void
                  gx.a(android.content.Context, bl, long, com.duomi.app.ui.MultiView$OnLikeReSetListener, int):void
                  gx.a(android.content.Context, bj, bl, com.duomi.app.ui.MultiView$OnLikeReSetListener, boolean):void */
                public void onClick(View view) {
                    try {
                        if (((Integer) MyAdapter.this.e.get(i)).intValue() == 1) {
                            MyAdapter.this.e.set(i, 0);
                            gx.a(SearchResultView.this.L, PlayListOnLineMusicView.a(SearchResultView.this.getContext(), (cx) MyAdapter.this.d.get(i)), SearchResultView.this.U.b(bn.a().h(), 0));
                            SearchResultView.this.E.notifyDataSetChanged();
                            Intent intent = new Intent("com.duomi.android.app.scanner.scancomplete");
                            intent.setAction("com.duomi.android.app.scanner.scancomplete");
                            SearchResultView.this.L.sendBroadcast(intent);
                            return;
                        }
                        MyAdapter.this.e.set(i, 1);
                        gx.a(SearchResultView.this.L, PlayListOnLineMusicView.a(SearchResultView.this.getContext(), (cx) MyAdapter.this.d.get(i)), SearchResultView.this.U.b(bn.a().h(), 0), (MultiView.OnLikeReSetListener) null, false);
                        SearchResultView.this.E.notifyDataSetChanged();
                        Intent intent2 = new Intent("com.duomi.android.app.scanner.scancomplete");
                        intent2.setAction("com.duomi.android.app.scanner.scancomplete");
                        SearchResultView.this.L.sendBroadcast(intent2);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            return view2;
        }
    }

    public SearchResultView(Activity activity) {
        super(activity);
        this.L = activity;
    }

    private void r() {
        this.U = ar.a(this.L);
        this.T = new jg(this.L);
        this.A.setImageResource(R.drawable.list_return_icon);
        this.A.setVisibility(0);
        this.D.setVisibility(8);
        this.M = this.x.b();
        this.N = new ArrayList();
        if (this.M == null || this.M.size() <= 0) {
            jh.a(getContext(), (int) R.string.search_no_result);
        } else {
            for (int i = 0; i < this.M.size(); i++) {
                this.N.add(0);
            }
        }
        this.E = new MyAdapter(g(), this.M, this.N);
        this.F.setAdapter((ListAdapter) this.E);
        this.F.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView adapterView, View view, int i, long j) {
                view.getLocationInWindow(SearchResultView.G);
                int unused = SearchResultView.this.H = i;
                SearchResultView.this.t();
                return false;
            }
        });
        this.B.setOnClickListener(this);
        this.F.setOnItemClickListener(this.V);
        this.F.setFastScrollEnabled(true);
    }

    private void s() {
        this.I = LayoutInflater.from(this.L).inflate((int) R.layout.popwindow, (ViewGroup) null);
        this.K = new Dialog(g());
        Window window = this.K.getWindow();
        WindowManager.LayoutParams attributes = window.getAttributes();
        ad.b("type", attributes.type + "");
        window.setBackgroundDrawableResource(R.drawable.none);
        ad.b("flag", attributes.flags + "");
        window.clearFlags(2);
        this.K.setCanceledOnTouchOutside(true);
        this.K.setContentView(this.I);
        this.I.setFocusable(true);
        this.O = (RelativeLayout) this.I.findViewById(R.id.command1);
        this.P = (RelativeLayout) this.I.findViewById(R.id.command2);
        this.Q = (RelativeLayout) this.I.findViewById(R.id.command3);
        this.R = (RelativeLayout) this.I.findViewById(R.id.command4);
        this.S = (RelativeLayout) this.I.findViewById(R.id.command5);
        ImageView imageView = (ImageView) this.I.findViewById(R.id.image4);
        ImageView imageView2 = (ImageView) this.I.findViewById(R.id.image5);
        TextView textView = (TextView) this.I.findViewById(R.id.text1);
        TextView textView2 = (TextView) this.I.findViewById(R.id.text2);
        TextView textView3 = (TextView) this.I.findViewById(R.id.text4);
        TextView textView4 = (TextView) this.I.findViewById(R.id.text5);
        ((ImageView) this.I.findViewById(R.id.image1)).setImageResource(R.drawable.list_like_icon);
        ((ImageView) this.I.findViewById(R.id.image2)).setImageResource(R.drawable.list_popupicon_add);
        ((ImageView) this.I.findViewById(R.id.image3)).setImageResource(R.drawable.list_popupicon_play);
        if (kf.b) {
            imageView2.setImageResource(R.drawable.list_popupicon_share);
        } else {
            imageView2.setImageResource(R.drawable.list_popupicon_share1);
        }
        textView.setText((int) R.string.playlist_online_like);
        textView2.setText((int) R.string.playlist_popup_add_to);
        if (!as.a(this.L).ac()) {
            textView3.setText((int) R.string.playlist_popup_song_info);
            imageView.setImageResource(R.drawable.list_popupicon_detail);
        } else {
            textView3.setText((int) R.string.playlist_online_download);
            imageView.setImageResource(R.drawable.list_popupicon_download);
        }
        textView4.setText((int) R.string.playlist_popup_share);
        this.O.setOnClickListener(this);
        this.P.setOnClickListener(this);
        this.Q.setOnClickListener(this);
        this.R.setOnClickListener(this);
        this.S.setOnClickListener(this);
    }

    /* access modifiers changed from: private */
    public void t() {
        Window window = this.K.getWindow();
        WindowManager.LayoutParams attributes = window.getAttributes();
        attributes.y = en.b(this.L, G[1]);
        window.setAttributes(attributes);
        this.I.setFocusable(true);
        this.I.requestFocus();
        this.K.show();
        this.J = true;
    }

    private void u() {
        this.J = false;
        this.K.dismiss();
    }

    public void a(Context context, bj bjVar) {
        String d = en.d(bjVar.k());
        ax a = ai.a(context).a(bjVar.g());
        StringBuffer append = new StringBuffer().append("");
        if (a == null) {
            append.append(bjVar.a());
        } else {
            append.append(a.e());
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(context.getString(R.string.playlist_song_name)).append(bjVar.h()).append("\n");
        stringBuffer.append(context.getString(R.string.playlist_song_singer)).append(en.a(context, bjVar.j())).append("\n");
        stringBuffer.append(context.getString(R.string.playlist_song_album)).append(en.a(context, append.toString())).append("\n");
        stringBuffer.append(context.getString(R.string.playlist_song_duration)).append(d).append("\n");
        if (bjVar.b() > 0) {
            stringBuffer.append(context.getString(R.string.playlist_song_path)).append(bjVar.e()).append("\n");
            stringBuffer.append(context.getString(R.string.playlist_song_size)).append(en.c(bjVar.l())).append("\n");
            String a2 = en.a(context, bjVar.c());
            if (!a2.equals(context.getString(R.string.unknown))) {
                a2 = a2 + "kbps";
            }
            stringBuffer.append(context.getString(R.string.playlist_song_kbps)).append(a2);
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        AlertDialog create = builder.create();
        builder.setNegativeButton((int) R.string.app_return, (DialogInterface.OnClickListener) null);
        create.setCanceledOnTouchOutside(true);
        builder.setTitle((int) R.string.playlist_songinfo);
        builder.setMessage(stringBuffer.toString());
        builder.show();
    }

    public void a(Message message) {
        this.C = message;
        r();
        super.a(message);
    }

    public boolean a(int i, KeyEvent keyEvent) {
        b(i, keyEvent);
        if (i == 4 && this.f.getVisibility() == 0 && !this.u) {
            if (this.g.getVisibility() == 0) {
                this.g.setVisibility(8);
                this.i.requestFocus();
            } else {
                this.f.setVisibility(8);
            }
            return true;
        } else if (i != 4 || getVisibility() != 0 || this.u) {
            return false;
        } else {
            this.d.a(SearchView.class, (Message) null);
            return true;
        }
    }

    public void f() {
        this.x = bh.a();
        Window window = g().getWindow();
        if (window != null) {
            window.clearFlags(1024);
        }
        inflate(g(), R.layout.playlist_online_music, this);
        this.z = (TextView) findViewById(R.id.list_title);
        this.z.setText((int) R.string.search_result_title);
        this.F = (ListView) findViewById(R.id.music_list);
        this.A = (ImageView) findViewById(R.id.go_back);
        this.B = (RelativeLayout) findViewById(R.id.title_panel);
        this.D = (Button) findViewById(R.id.command);
        r();
        s();
        h();
    }

    public void n() {
        this.i.setOnItemClickListener(this.y);
        this.j.setOnItemClickListener(this.X);
    }

    public void o() {
        if (this.a) {
            this.q = getResources().getStringArray(R.array.Search_menu_without_download);
            this.s = new ArrayList();
            this.s.add(Integer.valueOf((int) R.drawable.meun_searchhistory));
            this.s.add(Integer.valueOf((int) R.drawable.menu_bg));
        } else {
            this.q = getResources().getStringArray(R.array.Search_menu);
            this.s = new ArrayList();
            this.s.add(Integer.valueOf((int) R.drawable.meun_downloadsetup));
            this.s.add(Integer.valueOf((int) R.drawable.meun_searchhistory));
        }
        this.s.add(Integer.valueOf((int) R.drawable.menu_bg));
        this.s.add(Integer.valueOf((int) R.drawable.meun_more));
        this.s.add(Integer.valueOf((int) R.drawable.meun_setup));
        this.s.add(Integer.valueOf((int) R.drawable.meun_sleep));
        this.s.add(Integer.valueOf((int) R.drawable.meun_syc));
        this.s.add(Integer.valueOf((int) R.drawable.meun_exit));
        this.r = getResources().getStringArray(R.array.second_menu2);
        this.t = new ArrayList();
        this.t.add(Integer.valueOf((int) R.drawable.meun_help));
        this.t.add(Integer.valueOf((int) R.drawable.meun_advice));
        this.t.add(Integer.valueOf((int) R.drawable.meun_friends));
        this.t.add(Integer.valueOf((int) R.drawable.meun_about));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: bn.a(android.content.Context, boolean):void
     arg types: [android.content.Context, int]
     candidates:
      bn.a(int, android.content.Context):int
      bn.a(android.content.Context, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: gx.a(android.content.Context, bj, bl, com.duomi.app.ui.MultiView$OnLikeReSetListener, boolean):void
     arg types: [android.content.Context, bj, bl, com.duomi.app.ui.MultiView$OnLikeReSetListener, int]
     candidates:
      gx.a(android.content.Context, bj, int, android.os.Handler, com.duomi.app.ui.MultiView$OnLikeReSetListener):void
      gx.a(android.content.Context, bl, long, com.duomi.app.ui.MultiView$OnLikeReSetListener, int):void
      gx.a(android.content.Context, bj, bl, com.duomi.app.ui.MultiView$OnLikeReSetListener, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ev.a(android.content.Context, bj, boolean, java.lang.String):void
     arg types: [android.content.Context, bj, int, java.lang.String]
     candidates:
      ev.a(java.lang.String, java.io.File, android.os.Handler, android.content.Context):boolean
      ev.a(android.content.Context, bj, boolean, java.lang.String):void */
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.title_panel:
                try {
                    this.d.a(SearchView.class, new Message());
                    break;
                } catch (Exception e) {
                    e.printStackTrace();
                    break;
                }
            case R.id.command1:
                final bj a = PlayListOnLineMusicView.a(getContext(), (cx) this.M.get(this.H));
                final bl b = this.U.b(bn.a().h(), 2);
                if (!bn.a().b(getContext()) && kf.b && kf.a(this.L, 1) && !as.a(this.L).U()) {
                    AlertDialog create = new AlertDialog.Builder(getContext()).setPositiveButton(getContext().getString(R.string.app_confirm), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            as.a(SearchResultView.this.getContext()).u(true);
                        }
                    }).setNegativeButton(getContext().getString(R.string.app_cancel), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    }).create();
                    create.setTitle(getContext().getString(R.string.first_collection_title));
                    create.setMessage(getContext().getString(R.string.first_collection_message));
                    create.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                         method: gx.a(android.content.Context, bj, bl, com.duomi.app.ui.MultiView$OnLikeReSetListener, boolean):void
                         arg types: [android.content.Context, bj, bl, com.duomi.app.ui.MultiView$OnLikeReSetListener, int]
                         candidates:
                          gx.a(android.content.Context, bj, int, android.os.Handler, com.duomi.app.ui.MultiView$OnLikeReSetListener):void
                          gx.a(android.content.Context, bl, long, com.duomi.app.ui.MultiView$OnLikeReSetListener, int):void
                          gx.a(android.content.Context, bj, bl, com.duomi.app.ui.MultiView$OnLikeReSetListener, boolean):void */
                        public void onDismiss(DialogInterface dialogInterface) {
                            gx.a(SearchResultView.this.L, a, b, PlayerLayoutView.H, false);
                        }
                    });
                    create.show();
                    bn.a().a(getContext(), true);
                    break;
                } else {
                    gx.a(this.L, a, b, PlayerLayoutView.H, false);
                    break;
                }
                break;
            case R.id.command2:
                gx.a(this.L, PlayListOnLineMusicView.a(getContext(), (cx) this.M.get(this.H)), -1, this.W, PlayerLayoutView.H);
                break;
            case R.id.command3:
                gx.a(getContext(), PlayListOnLineMusicView.a(getContext(), (cx) this.M.get(this.H), this.H));
                break;
            case R.id.command4:
                as a2 = as.a(this.L);
                cx cxVar = (cx) this.M.get(this.H);
                bj a3 = PlayListOnLineMusicView.a(getContext(), cxVar);
                if (a2.ac()) {
                    ev.a(getContext(), a3, false, cxVar.k());
                    break;
                } else {
                    a(this.L, a3);
                    break;
                }
            case R.id.command5:
                if (il.b(this.L)) {
                    bj a4 = PlayListOnLineMusicView.a(getContext(), (cx) this.M.get(this.H));
                    if (!kf.b) {
                        en.a(getContext(), a4);
                        break;
                    } else {
                        new gd(g(), a4);
                        break;
                    }
                } else {
                    jh.a(this.L, (int) R.string.app_net_error);
                    break;
                }
        }
        u();
    }
}
