package com.duomi.app.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import android.text.InputFilter;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.duomi.advertisement.AdvertisementList;
import com.duomi.android.R;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class PopDialogView extends c {
    static Timer A;
    static int B = 0;
    static Activity C;
    static Handler D = new Handler() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: en.a(android.app.Activity, boolean):boolean
         arg types: [android.app.Activity, int]
         candidates:
          en.a(int, int):int
          en.a(android.content.Context, float):int
          en.a(android.content.Context, int):int
          en.a(android.graphics.Bitmap, int):android.graphics.Bitmap
          en.a(android.content.Context, java.lang.String):java.lang.String
          en.a(android.content.Context, bj):void
          en.a(android.content.Context, boolean):void
          en.a(java.io.File, java.io.File):void
          en.a(java.lang.String, java.lang.String):void
          en.a(java.security.interfaces.RSAPublicKey, byte[]):byte[]
          en.a(byte[], java.lang.String):byte[]
          en.a(android.app.Activity, boolean):boolean */
        public void handleMessage(Message message) {
            long time = (PopDialogView.y - (new Date().getTime() - PopDialogView.x)) / 1000;
            StringBuffer stringBuffer = new StringBuffer();
            if (time % 60 == 0) {
                stringBuffer.append(String.valueOf(time / 60)).append("分钟");
            } else if (time / 60 <= 0) {
                stringBuffer.append(String.valueOf(time % 60)).append("秒");
            } else {
                stringBuffer.append(String.valueOf(time / 60)).append("分钟").append(String.valueOf(time % 60)).append("秒");
            }
            switch (message.what) {
                case 0:
                    try {
                        PopDialogView.y = 0;
                        gx.d.c();
                        return;
                    } catch (RemoteException e) {
                        e.printStackTrace();
                        return;
                    }
                case 1:
                    ad.b("sleepMode", "quit");
                    PopDialogView.y = 0;
                    en.a(PopDialogView.C, false);
                    ((NotificationManager) PopDialogView.C.getSystemService("notification")).cancel(1);
                    if (PopDialogView.C != null) {
                        PopDialogView.C.finish();
                        return;
                    }
                    return;
                case 2:
                    if (time > 0) {
                        PopDialogView.z.setText(((Object) stringBuffer) + "后将停止播放音乐");
                        return;
                    } else {
                        PopDialogView.z.setText("");
                        return;
                    }
                case 3:
                    if (time > 0) {
                        PopDialogView.z.setText(((Object) stringBuffer) + "后将退出多米音乐");
                        return;
                    } else {
                        PopDialogView.z.setText("");
                        return;
                    }
                default:
                    return;
            }
        }
    };
    /* access modifiers changed from: private */
    public static Activity O;
    /* access modifiers changed from: private */
    public static Dialog Q;
    static long x = 0;
    static long y = 0;
    static TextView z;
    String E = "";
    c F = null;
    ArrayList G;
    Dialog H;
    p I = null;
    int J = 0;
    as K = as.a(O);
    String L = "";
    bb M = null;
    String[] N = {"s", "m"};
    private View P;
    private RelativeLayout R;
    private RelativeLayout S;
    private RelativeLayout T;
    /* access modifiers changed from: private */
    public Handler U = new Handler() {
        public void handleMessage(Message message) {
            switch (message.what) {
                case 254:
                    PopDialogView.a(PopDialogView.this.getContext());
                    break;
            }
            super.handleMessage(message);
        }
    };
    /* access modifiers changed from: private */
    public Map V;
    private AlertDialog W;

    /* renamed from: com.duomi.app.ui.PopDialogView$17  reason: invalid class name */
    class AnonymousClass17 implements DialogInterface.OnKeyListener {
        final /* synthetic */ PopDialogView a;

        public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
            if (i != 4) {
                return false;
            }
            dialogInterface.dismiss();
            p.a(this.a.g()).a(LoginRegView.class.getName(), new a() {
                int a = 0;

                public void a(boolean z, Message message) {
                }

                public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                    if (this.a <= 0 || i != 4) {
                        this.a++;
                        return false;
                    }
                    PopDialogView.a(AnonymousClass17.this.a.g(), null, null, true);
                    return true;
                }
            });
            return true;
        }
    }

    /* renamed from: com.duomi.app.ui.PopDialogView$18  reason: invalid class name */
    class AnonymousClass18 implements AdapterView.OnItemClickListener {
        final /* synthetic */ PopDialogView a;

        public void onItemClick(AdapterView adapterView, View view, int i, long j) {
            this.a.H.dismiss();
            if (!il.b(this.a.getContext())) {
                jh.a(this.a.getContext(), (int) R.string.app_net_error);
                return;
            }
            new FindPassWordTask().execute(Integer.valueOf(i), this.a.N[i]);
            ad.b("PopDialogView", this.a.L);
        }
    }

    class FindPassWordTask extends AsyncTask {
        private FindPassWordTask() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: cq.a(boolean, android.content.Context, int, java.lang.String, java.lang.String, bn):java.lang.String
         arg types: [int, android.app.Activity, int, java.lang.String, java.lang.String, ?[OBJECT, ARRAY]]
         candidates:
          cq.a(java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, android.content.Context):java.lang.String
          cq.a(boolean, android.content.Context, int, java.lang.String, java.lang.String, bn):java.lang.String */
        /* access modifiers changed from: protected */
        public Object doInBackground(Object... objArr) {
            PopDialogView.this.K.d();
            ad.b("PopDialogView", "username>>>>" + bn.a().f());
            PopDialogView.this.L = cq.a(true, (Context) PopDialogView.O, 9, bn.a().f(), objArr[1].toString(), (bn) null);
            PopDialogView.this.M = cp.e(PopDialogView.O, PopDialogView.this.L);
            return objArr[0];
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(final Object obj) {
            super.onPostExecute(obj);
            PopDialogView.this.r();
            switch (Integer.parseInt(obj.toString())) {
                case 0:
                    if (PopDialogView.this.M.d() != null) {
                        if (PopDialogView.this.M.d().trim().contains("ERR:04")) {
                            jh.a(PopDialogView.this.getContext(), (int) R.string.find_pwd_fail2);
                        } else {
                            jh.a(PopDialogView.this.getContext(), (int) R.string.find_pwd_fail);
                        }
                        PopDialogView.this.s();
                        return;
                    } else if (PopDialogView.this.M.a().trim().equals("0")) {
                        final jg jgVar = new jg(PopDialogView.O);
                        jgVar.a(R.string.lg_findpasswd);
                        TextView a2 = jgVar.a();
                        a2.setText(PopDialogView.O.getResources().getString(R.string.find_pwd_success));
                        a2.setVisibility(0);
                        Button b = jgVar.b();
                        b.setText(PopDialogView.O.getResources().getString(R.string.hall_user_ok));
                        b.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View view) {
                                jgVar.dismiss();
                                PopDialogView.this.s();
                            }
                        });
                        b.setVisibility(0);
                        jgVar.b(0);
                        jgVar.setCancelable(false);
                        jgVar.show();
                        return;
                    } else if (PopDialogView.this.M.a().trim().equals("1")) {
                        final jg jgVar2 = new jg(PopDialogView.O);
                        jgVar2.a(R.string.lg_findpasswd);
                        TextView a3 = jgVar2.a();
                        a3.setText(PopDialogView.O.getResources().getString(R.string.find_pwd_bdphone_fail));
                        a3.setVisibility(0);
                        Button b2 = jgVar2.b();
                        b2.setText(PopDialogView.O.getResources().getString(R.string.hall_user_ok));
                        b2.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View view) {
                                jgVar2.dismiss();
                                PopDialogView.this.s();
                            }
                        });
                        b2.setVisibility(0);
                        jgVar2.b(0);
                        jgVar2.setCancelable(false);
                        jgVar2.show();
                        return;
                    } else {
                        return;
                    }
                case 1:
                    if (PopDialogView.this.M.d() != null) {
                        if (PopDialogView.this.M.d().trim().contains("ERR:04")) {
                            jh.a(PopDialogView.this.getContext(), (int) R.string.find_pwd_fail3);
                        } else {
                            jh.a(PopDialogView.this.getContext(), (int) R.string.find_pwd_fail);
                        }
                        PopDialogView.this.s();
                        return;
                    } else if (PopDialogView.this.M.a().trim().equals("0")) {
                        final jg jgVar3 = new jg(PopDialogView.O);
                        jgVar3.a(R.string.lg_findpasswd);
                        TextView a4 = jgVar3.a();
                        a4.setText((CharSequence) PopDialogView.this.M.c().get("ds"));
                        a4.setVisibility(0);
                        Button b3 = jgVar3.b();
                        b3.setText((int) R.string.find_pwd_resend);
                        b3.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View view) {
                                jgVar3.dismiss();
                                if (!il.b(PopDialogView.this.getContext())) {
                                    jh.a(PopDialogView.this.getContext(), (int) R.string.app_net_error);
                                    return;
                                }
                                new FindPassWordTask().execute(obj.toString(), PopDialogView.this.N[Integer.parseInt(obj.toString())]);
                            }
                        });
                        b3.setVisibility(0);
                        Button c = jgVar3.c();
                        c.setText(PopDialogView.O.getResources().getString(R.string.hall_user_ok));
                        c.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View view) {
                                jgVar3.dismiss();
                                PopDialogView.this.s();
                            }
                        });
                        c.setVisibility(0);
                        jgVar3.b(0);
                        jgVar3.setCancelable(false);
                        jgVar3.show();
                        return;
                    } else if (PopDialogView.this.M.a().trim().equals("1")) {
                        final jg jgVar4 = new jg(PopDialogView.O);
                        jgVar4.a(R.string.lg_findpasswd);
                        TextView a5 = jgVar4.a();
                        a5.setText((CharSequence) PopDialogView.this.M.c().get("ds"));
                        a5.setVisibility(0);
                        Button b4 = jgVar4.b();
                        b4.setText(PopDialogView.O.getResources().getString(R.string.hall_user_ok));
                        b4.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View view) {
                                jgVar4.dismiss();
                                PopDialogView.this.s();
                            }
                        });
                        b4.setVisibility(0);
                        jgVar4.b(0);
                        jgVar4.setCancelable(false);
                        jgVar4.show();
                        return;
                    } else {
                        return;
                    }
                default:
                    return;
            }
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            PopDialogView.this.q();
        }
    }

    class MyAdapter extends BaseAdapter {
        TextView a;
        TextView b;
        final /* synthetic */ PopDialogView c;
        private LayoutInflater d;

        public int getCount() {
            return this.c.V.size();
        }

        public Object getItem(int i) {
            return this.c.V.get(i + "");
        }

        public long getItemId(int i) {
            return (long) i;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(int i, View view, ViewGroup viewGroup) {
            View inflate = view == null ? this.d.inflate((int) R.layout.find_passwd_row, viewGroup, false) : view;
            this.a = (TextView) inflate.findViewById(R.id.findPasswdBig);
            this.b = (TextView) inflate.findViewById(R.id.findPasswdSmall);
            this.a.setText(((String[]) this.c.V.get(i + ""))[0]);
            this.b.setText(((String[]) this.c.V.get(i + ""))[1]);
            return inflate;
        }
    }

    public class SearchAdapter extends BaseAdapter {
        private LayoutInflater b;
        private TextView c;
        private TextView d;

        public SearchAdapter() {
        }

        public int getCount() {
            return PopDialogView.this.G.size();
        }

        public Object getItem(int i) {
            return PopDialogView.this.G.get(i);
        }

        public long getItemId(int i) {
            return (long) i;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(int i, View view, ViewGroup viewGroup) {
            this.b = (LayoutInflater) PopDialogView.this.getContext().getSystemService("layout_inflater");
            View inflate = this.b.inflate((int) R.layout.search_history_row, viewGroup, false);
            this.c = (TextView) inflate.findViewById(R.id.searchHistoryList);
            this.c.setText(en.l(((bd) PopDialogView.this.G.get(i)).a()));
            this.d = (TextView) inflate.findViewById(R.id.searchHistoryTime);
            this.d.setText(new SimpleDateFormat("yyyy-MM-dd").format(new Date(((bd) PopDialogView.this.G.get(i)).b())));
            return inflate;
        }
    }

    class SearchMusicTask extends AsyncTask {
        ProgressDialog a;
        boolean b;

        private SearchMusicTask() {
            this.a = null;
            this.b = true;
        }

        public void a() {
            try {
                this.a = new ProgressDialog(PopDialogView.this.getContext());
                this.a.setTitle("搜索");
                this.a.setMessage("正在搜索...");
                this.a.setCanceledOnTouchOutside(false);
                this.a.setButton(PopDialogView.this.getContext().getString(R.string.hall_user_cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        SearchMusicTask.this.b = false;
                    }
                });
                this.a.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        /* access modifiers changed from: protected */
        public Object doInBackground(Object... objArr) {
            if (SearchView.A.equalsIgnoreCase(en.l(SearchView.y))) {
                return null;
            }
            String a2 = cq.a(PopDialogView.this.getContext(), PopDialogView.this.E, SearchView.y, "1", 0, 50, PopDialogView.this.U);
            ad.b("PopDialogView", "back>>>>" + a2);
            if (this.b) {
                cp.d(PopDialogView.this.getContext(), a2);
            }
            return null;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Object obj) {
            Class<SearchView> cls = SearchView.class;
            if (this.b && (bh.a().b() == null || bh.a().b().size() == 0)) {
                jh.a(PopDialogView.this.getContext(), (int) R.string.search_no_result);
                Class<SearchView> cls2 = SearchView.class;
                PopDialogView.this.F.a(cls, (Message) null);
            }
            if (this.a != null) {
                this.a.dismiss();
            }
            if (!this.b || !il.b(PopDialogView.this.getContext())) {
                Class<SearchView> cls3 = SearchView.class;
                PopDialogView.this.F.a(cls, (Message) null);
                return;
            }
            PopDialogView.this.F.a(SearchResultView.class, (Message) null);
            SearchView.A = en.l(SearchView.y);
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            if (!SearchView.A.equalsIgnoreCase(en.l(SearchView.y))) {
                a();
            }
        }
    }

    public PopDialogView(Context context) {
        super((Activity) context);
        O = (Activity) context;
    }

    public static void a(final Activity activity) {
        C = activity;
        A = new Timer();
        A.schedule(new TimerTask() {
            public void run() {
                if (PopDialogView.B == 0) {
                    PopDialogView.D.sendEmptyMessage(2);
                } else if (PopDialogView.B == 1) {
                    PopDialogView.D.sendEmptyMessage(3);
                }
            }
        }, 1000, 1000);
        LinearLayout linearLayout = (LinearLayout) ((LayoutInflater) activity.getSystemService("layout_inflater")).inflate((int) R.layout.sleep_mode, (ViewGroup) null);
        z = (TextView) linearLayout.findViewById(R.id.prompt_time);
        final EditText editText = (EditText) linearLayout.findViewById(R.id.wait_time);
        final RadioButton radioButton = (RadioButton) linearLayout.findViewById(R.id.radio_stop);
        final RadioButton radioButton2 = (RadioButton) linearLayout.findViewById(R.id.radio_quit);
        if (B == 0) {
            radioButton.setChecked(true);
        } else {
            radioButton2.setChecked(true);
        }
        AlertDialog create = new AlertDialog.Builder(activity).create();
        create.setTitle((int) R.string.sleep_mode_title);
        if (y - (System.currentTimeMillis() - x) < 0) {
            editText.setText("30");
        } else {
            editText.setText(((y - (System.currentTimeMillis() - x)) / 60000) + "");
        }
        create.setButton(activity.getResources().getString(R.string.app_confirm), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                ((InputMethodManager) activity.getSystemService("input_method")).hideSoftInputFromWindow(editText.getWindowToken(), 0);
                dialogInterface.dismiss();
                if (en.c(editText.getText().toString())) {
                    jh.a(activity, (int) R.string.sleep_text_error);
                    return;
                }
                PopDialogView.y = (long) (Integer.parseInt(editText.getText().toString()) * 60 * 1000);
                PopDialogView.x = System.currentTimeMillis();
                if (PopDialogView.D.hasMessages(0)) {
                    PopDialogView.D.removeMessages(0);
                }
                if (PopDialogView.D.hasMessages(1)) {
                    PopDialogView.D.removeMessages(1);
                }
                if (radioButton.isChecked()) {
                    PopDialogView.B = 0;
                    PopDialogView.D.sendEmptyMessageDelayed(0, PopDialogView.y);
                    jh.a(activity, editText.getText().toString() + "分钟后将停止播放音乐");
                } else if (radioButton2.isChecked()) {
                    PopDialogView.B = 1;
                    PopDialogView.D.sendEmptyMessageDelayed(1, PopDialogView.y);
                    jh.a(activity, editText.getText().toString() + "分钟后将退出多米音乐");
                }
            }
        });
        create.setButton3(activity.getResources().getString(R.string.sleep_mode_cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                ((InputMethodManager) activity.getSystemService("input_method")).hideSoftInputFromWindow(editText.getWindowToken(), 0);
                dialogInterface.dismiss();
                if (PopDialogView.D.hasMessages(0)) {
                    PopDialogView.D.removeMessages(0);
                }
                if (PopDialogView.D.hasMessages(1)) {
                    PopDialogView.D.removeMessages(1);
                }
                PopDialogView.y = 0;
                jh.a(activity, "取消所有定时");
            }
        });
        create.setButton2("返回", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                ((InputMethodManager) activity.getSystemService("input_method")).hideSoftInputFromWindow(editText.getWindowToken(), 0);
                dialogInterface.dismiss();
            }
        });
        create.setView(linearLayout);
        create.show();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: en.a(android.app.Activity, boolean):boolean
     arg types: [android.app.Activity, int]
     candidates:
      en.a(int, int):int
      en.a(android.content.Context, float):int
      en.a(android.content.Context, int):int
      en.a(android.graphics.Bitmap, int):android.graphics.Bitmap
      en.a(android.content.Context, java.lang.String):java.lang.String
      en.a(android.content.Context, bj):void
      en.a(android.content.Context, boolean):void
      en.a(java.io.File, java.io.File):void
      en.a(java.lang.String, java.lang.String):void
      en.a(java.security.interfaces.RSAPublicKey, byte[]):byte[]
      en.a(byte[], java.lang.String):byte[]
      en.a(android.app.Activity, boolean):boolean */
    public static void a(final Activity activity, DialogInterface.OnClickListener onClickListener, DialogInterface.OnClickListener onClickListener2, boolean z2) {
        if (as.a(activity).P()) {
            String str = "";
            if (!fd.i) {
                str = "扫描任务还未完成\n";
            }
            new AlertDialog.Builder(activity).setTitle((int) R.string.hall_quit).setIcon((int) R.drawable.icon1).setCancelable(z2).setMessage(str + activity.getString(R.string.hall_quit_tipcontent) + "    ").setPositiveButton(activity.getString(R.string.hall_user_ok), new DialogInterface.OnClickListener() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: en.a(android.app.Activity, boolean):boolean
                 arg types: [android.app.Activity, int]
                 candidates:
                  en.a(int, int):int
                  en.a(android.content.Context, float):int
                  en.a(android.content.Context, int):int
                  en.a(android.graphics.Bitmap, int):android.graphics.Bitmap
                  en.a(android.content.Context, java.lang.String):java.lang.String
                  en.a(android.content.Context, bj):void
                  en.a(android.content.Context, boolean):void
                  en.a(java.io.File, java.io.File):void
                  en.a(java.lang.String, java.lang.String):void
                  en.a(java.security.interfaces.RSAPublicKey, byte[]):byte[]
                  en.a(byte[], java.lang.String):byte[]
                  en.a(android.app.Activity, boolean):boolean */
                public void onClick(DialogInterface dialogInterface, int i) {
                    en.a(activity, false);
                }
            }).setNeutralButton(activity.getString(R.string.hall_user_hide), onClickListener == null ? new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    Intent intent = new Intent();
                    intent.setAction("android.intent.action.MAIN");
                    intent.addCategory("android.intent.category.HOME");
                    activity.startActivity(intent);
                }
            } : onClickListener).setNegativeButton(activity.getString(R.string.hall_user_cancel), onClickListener2 == null ? null : onClickListener2).show();
            return;
        }
        en.a(activity, false);
    }

    public static void a(final Activity activity, final Handler handler) {
        final ar a = ar.a(activity);
        LinearLayout linearLayout = (LinearLayout) activity.getLayoutInflater().inflate((int) R.layout.create_list_dialog, (ViewGroup) null);
        final EditText editText = (EditText) linearLayout.findViewById(R.id.list_name);
        new AlertDialog.Builder(activity).setIcon((Drawable) null).setTitle((int) R.string.dialog_create_list).setView(linearLayout).setPositiveButton((int) R.string.app_confirm, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                ((InputMethodManager) activity.getSystemService("input_method")).hideSoftInputFromWindow(editText.getWindowToken(), 0);
                String obj = editText.getText().toString();
                if (obj == null || obj.trim().equals("")) {
                    jh.a(activity, (int) R.string.dialog_create_fail);
                } else if (obj.length() > 20) {
                    jh.a(activity, (int) R.string.dialog_create_fail2);
                } else if (obj.contains("/") || obj.contains("\\") || obj.contains("<") || obj.contains(">") || obj.contains("*") || obj.contains(":") || obj.contains("#") || obj.contains("@") || obj.contains("￥") || obj.contains("?") || obj.contains("？") || obj.contains("\"") || obj.contains("|") || obj.contains("&")) {
                    jh.a(activity, (int) R.string.dialog_create_fail3);
                } else {
                    String trim = obj.trim();
                    if (!a.h(trim)) {
                        int c2 = a.c(trim);
                        if (handler != null) {
                            Message message = new Message();
                            message.what = 255;
                            message.getData().putInt("listId", c2);
                            handler.sendMessage(message);
                            return;
                        }
                        Intent intent = new Intent("com.duomi.android.app.scanner.scancomplete");
                        intent.setAction("com.duomi.android.app.scanner.scancomplete");
                        activity.sendBroadcast(intent);
                        jh.a(activity, (int) R.string.dialog_create_success);
                        return;
                    }
                    jh.a(activity, (int) R.string.dialog_create_fail4);
                }
            }
        }).setNegativeButton((int) R.string.app_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                ((InputMethodManager) activity.getSystemService("input_method")).hideSoftInputFromWindow(editText.getWindowToken(), 0);
            }
        }).show();
    }

    public static void a(final Context context) {
        if (il.a(context) != 0 && as.a(context).O()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            View inflate = ((LayoutInflater) context.getSystemService("layout_inflater")).inflate((int) R.layout.dialog_tips, (ViewGroup) null);
            final CheckBox checkBox = (CheckBox) inflate.findViewById(R.id.tiptext1);
            checkBox.setChecked(true);
            checkBox.setText((int) R.string.app_no_alert_tip);
            ((TextView) inflate.findViewById(R.id.tip1)).setText((int) R.string.app_net_tips);
            builder.setPositiveButton((int) R.string.hall_user_ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    as.a(context).q(!checkBox.isChecked());
                }
            });
            builder.setView(inflate);
            builder.show();
        }
    }

    public static void b(Activity activity) {
        activity.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://wap.duomi.com/android_help.php")));
    }

    private void c(int i) {
        this.P = LayoutInflater.from(getContext()).inflate((int) R.layout.popwindow2, (ViewGroup) null);
        Q = new Dialog(g());
        Window window = Q.getWindow();
        WindowManager.LayoutParams attributes = window.getAttributes();
        attributes.y = en.b(g(), i);
        window.setAttributes(attributes);
        ad.b("type", attributes.type + "");
        window.setBackgroundDrawableResource(R.drawable.none);
        ad.b("flag", attributes.flags + "");
        window.clearFlags(2);
        Q.setCanceledOnTouchOutside(true);
        Q.setContentView(this.P);
        this.P.setFocusable(true);
        this.R = (RelativeLayout) this.P.findViewById(R.id.command1);
        this.S = (RelativeLayout) this.P.findViewById(R.id.command2);
        this.T = (RelativeLayout) this.P.findViewById(R.id.command3);
        ((ImageView) this.P.findViewById(R.id.image1)).setImageResource(R.drawable.list_popupicon_open);
        ((ImageView) this.P.findViewById(R.id.image2)).setImageResource(R.drawable.list_popupicon_rename);
        ((ImageView) this.P.findViewById(R.id.image3)).setImageResource(R.drawable.list_popupicon_delete);
        ((TextView) this.P.findViewById(R.id.text1)).setText(getContext().getResources().getStringArray(R.array.songlist_dialog)[0]);
        ((TextView) this.P.findViewById(R.id.text2)).setText(getContext().getResources().getStringArray(R.array.songlist_dialog)[1]);
        ((TextView) this.P.findViewById(R.id.text3)).setText(getContext().getResources().getStringArray(R.array.songlist_dialog)[2]);
    }

    public void a(final bl blVar, final c cVar, int i) {
        c(i);
        this.P.setFocusable(true);
        this.P.requestFocus();
        Q.show();
        this.R.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (PopDialogView.Q != null) {
                    PopDialogView.Q.dismiss();
                }
                Message message = new Message();
                message.obj = blVar;
                ((PlayListGroupView) cVar).y.push(1);
                cVar.a(PlayListMusicView.class, message);
            }
        });
        this.S.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (PopDialogView.Q != null) {
                    PopDialogView.Q.dismiss();
                }
                final EditText editText = new EditText(PopDialogView.this.getContext());
                editText.setSingleLine(true);
                editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(8)});
                new AlertDialog.Builder(PopDialogView.O).setTitle((int) R.string.dialog_rename).setView(editText).setPositiveButton((int) R.string.button_ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ((InputMethodManager) PopDialogView.this.getContext().getSystemService("input_method")).hideSoftInputFromWindow(editText.getWindowToken(), 0);
                        dialogInterface.dismiss();
                        if (editText.getText() == null || editText.getText().toString().trim().equals("")) {
                            jh.a(PopDialogView.this.getContext(), (int) R.string.dialog_create_fail);
                            return;
                        }
                        String trim = editText.getText().toString().trim();
                        if (trim == null || trim.trim().equals("")) {
                            jh.a(PopDialogView.O, (int) R.string.dialog_create_fail);
                        } else if (trim.length() > 20) {
                            jh.a(PopDialogView.O, (int) R.string.dialog_create_fail2);
                        } else if (trim.contains("/") || trim.contains("\\") || trim.contains("<") || trim.contains(">") || trim.contains("*") || trim.contains(":") || trim.contains("#") || trim.contains("@") || trim.contains("￥") || trim.contains("?") || trim.contains("？") || trim.contains("\"") || trim.contains("|") || trim.contains("&")) {
                            jh.a(PopDialogView.O, (int) R.string.dialog_create_fail3);
                        } else {
                            ar a2 = ar.a(PopDialogView.O);
                            if (!a2.h(trim)) {
                                a2.a(blVar.g() + "", editText.getText().toString());
                                Intent intent = new Intent("com.duomi.android.app.scanner.scancomplete");
                                intent.setAction("com.duomi.android.app.scanner.scancomplete");
                                PopDialogView.O.sendBroadcast(intent);
                                return;
                            }
                            jh.a(PopDialogView.O, (int) R.string.dialog_create_fail4);
                        }
                    }
                }).setNegativeButton((int) R.string.button_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ((InputMethodManager) PopDialogView.this.getContext().getSystemService("input_method")).hideSoftInputFromWindow(editText.getWindowToken(), 0);
                    }
                }).show();
            }
        });
        this.T.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (PopDialogView.Q != null) {
                    PopDialogView.Q.dismiss();
                }
                new AlertDialog.Builder(PopDialogView.O).setTitle(PopDialogView.this.getContext().getResources().getString(R.string.dialog_delete_list) + blVar.h() + "?").setMessage(PopDialogView.this.getContext().getResources().getString(R.string.dialog_delete_prompt) + blVar.h() + "?").setPositiveButton((int) R.string.button_ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ar.a(PopDialogView.O).d(blVar.h());
                        Intent intent = new Intent("com.duomi.android.app.scanner.scancomplete");
                        intent.setAction("com.duomi.android.app.scanner.scancomplete");
                        PopDialogView.O.sendBroadcast(intent);
                    }
                }).setNegativeButton((int) R.string.button_cancel, (DialogInterface.OnClickListener) null).show();
            }
        });
    }

    public void c(c cVar) {
        this.F = cVar;
        final am a = am.a(getContext());
        this.G = a.a(bn.a().h(), "1");
        boolean z2 = this.G.isEmpty();
        ListView listView = new ListView(getContext());
        listView.setAdapter((ListAdapter) new SearchAdapter());
        listView.setCacheColorHint(0);
        if (z2) {
            jh.a(O, "没有搜索历史记录");
            return;
        }
        final AlertDialog create = new AlertDialog.Builder(getContext()).setTitle((int) R.string.search_history).setView(listView).setPositiveButton((int) R.string.search_clear, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                a.b("1");
                jh.a(PopDialogView.this.getContext(), (int) R.string.search_clear_success);
                dialogInterface.dismiss();
            }
        }).setNegativeButton((int) R.string.search_close, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).create();
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
             arg types: [java.lang.String, int]
             candidates:
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
            public void onItemClick(AdapterView adapterView, View view, int i, long j) {
                am a2 = am.a(PopDialogView.this.getContext());
                SearchView.y = en.e(((TextView) view.findViewById(R.id.searchHistoryList)).getText().toString());
                if (!a2.a(bn.a().h(), en.l(SearchView.y), "1")) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(AdvertisementList.EXTRA_UID, bn.a().h());
                    contentValues.put("type", (Integer) 1);
                    contentValues.put("words", SearchView.y);
                    contentValues.put("date", Long.valueOf(System.currentTimeMillis()));
                    a2.a(contentValues);
                }
                PopDialogView.this.E = "u";
                if (!il.b(PopDialogView.O) || !il.c(PopDialogView.O)) {
                    jh.a(PopDialogView.this.getContext(), (int) R.string.app_net_error);
                } else {
                    new SearchMusicTask().execute(new Object[0]);
                }
                create.dismiss();
            }
        });
        create.show();
    }

    public void f() {
    }

    public void q() {
        this.W = new ProgressDialog(getContext());
        this.W.setTitle((int) R.string.lg_findpasswd);
        this.W.setIcon((int) R.drawable.icon1);
        this.W.setMessage("" + getContext().getString(R.string.find_pwd_tips) + bn.a().f() + getContext().getString(R.string.find_pwd_tips2));
        this.W.setCancelable(false);
        this.W.show();
    }

    public void r() {
        if (this.W != null) {
            this.W.dismiss();
        }
    }

    public void s() {
        LoginRegView.z = true;
        this.I.a(LoginRegView.class.getName(), new a() {
            public void a(boolean z, Message message) {
            }

            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                if (i != 4) {
                    return false;
                }
                PopDialogView.a(PopDialogView.this.g(), null, null, true);
                return false;
            }
        });
    }
}
