package com.duomi.app.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import com.duomi.android.DuomiMainActivity;
import com.duomi.android.PickBgActivity;
import com.duomi.android.R;
import com.duomi.app.ui.BindPhoneView;

public class SettingView extends c implements View.OnClickListener, kb {
    private static AlertDialog au;
    private static boolean az = false;
    private p A;
    private Button B;
    private Button C;
    private Button D;
    private RelativeLayout E;
    private RelativeLayout F;
    private RelativeLayout G;
    private RelativeLayout H;
    private RelativeLayout I;
    private RelativeLayout J;
    private RelativeLayout K;
    private RelativeLayout L;
    private RelativeLayout M;
    private RelativeLayout N;
    private RelativeLayout O;
    private RelativeLayout P;
    private RelativeLayout Q;
    /* access modifiers changed from: private */
    public RelativeLayout R;
    private RelativeLayout S;
    private RelativeLayout T;
    private RelativeLayout U;
    private RelativeLayout V;
    private RelativeLayout W;
    private RelativeLayout X;
    private CheckBox Y;
    private CheckBox Z;
    private Runnable aA = new Runnable() {
        public void run() {
            try {
                jz.a(SettingView.this.g(), SettingView.this, SettingView.this.ay);
            } catch (Exception e) {
                Log.e("99", "11");
                e.printStackTrace();
                Looper.prepare();
                if (SettingView.this.av != null && SettingView.this.av.isShowing()) {
                    SettingView.this.av.dismiss();
                }
                Looper.loop();
            }
        }
    };
    /* access modifiers changed from: private */
    public Dialog aB;
    private View aC;
    /* access modifiers changed from: private */
    public EditText aD;
    /* access modifiers changed from: private */
    public EditText aE;
    private Button aF;
    private Button aG;
    private View aH;
    /* access modifiers changed from: private */
    public ProgressDialog aI;
    private Runnable aJ = new Runnable() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kf.a(android.content.Context, java.lang.String, java.lang.String, boolean):int
         arg types: [android.app.Activity, java.lang.String, java.lang.String, int]
         candidates:
          kf.a(android.content.Context, java.lang.String, java.lang.String, java.lang.String):int
          kf.a(android.content.Context, java.lang.String, java.lang.String, boolean):int */
        public void run() {
            String trim = SettingView.this.aD.getText().toString().trim();
            String trim2 = SettingView.this.aE.getText().toString().trim();
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            int a2 = kf.a((Context) SettingView.this.g(), trim, trim2, true);
            int i = -1;
            while (a2 != 0 && a2 != 3) {
                int i2 = i + 1;
                if (i >= 3) {
                    break;
                }
                a2 = kf.a((Context) SettingView.this.g(), trim, trim2, true);
                i = i2;
            }
            Message obtainMessage = SettingView.this.ay.obtainMessage(-1);
            obtainMessage.arg1 = a2;
            SettingView.this.ay.sendMessage(obtainMessage);
        }
    };
    private CheckBox aa;
    private CheckBox ab;
    private CheckBox ac;
    private CheckBox ad;
    private CheckBox ae;
    private CheckBox af;
    private CheckBox ag;
    private CheckBox ah;
    private CheckBox ai;
    private CheckBox aj;
    /* access modifiers changed from: private */
    public TextView ak;
    /* access modifiers changed from: private */
    public TextView al;
    private TextView am;
    private TextView an;
    /* access modifiers changed from: private */
    public ImageView ao;
    private ImageView ap;
    private SeekBar aq;
    private as ar;
    private RelativeLayout as;
    /* access modifiers changed from: private */
    public as at;
    /* access modifiers changed from: private */
    public ProgressDialog av;
    /* access modifiers changed from: private */
    public ProgressDialog aw;
    private String[] ax = null;
    /* access modifiers changed from: private */
    public Handler ay = null;
    String x = "";
    /* access modifiers changed from: private */
    public Context y;
    /* access modifiers changed from: private */
    public String z = "SettingView";

    public class RequestBindPhoneNum extends AsyncTask {
        final /* synthetic */ SettingView a;

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: cq.a(boolean, android.content.Context, int, java.lang.String, java.lang.String, bn):java.lang.String
         arg types: [int, android.content.Context, int, ?[OBJECT, ARRAY], ?[OBJECT, ARRAY], ?[OBJECT, ARRAY]]
         candidates:
          cq.a(java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, android.content.Context):java.lang.String
          cq.a(boolean, android.content.Context, int, java.lang.String, java.lang.String, bn):java.lang.String */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: cq.a(boolean, android.content.Context, int, java.lang.String, java.lang.String, bn):java.lang.String
         arg types: [int, android.content.Context, int, java.lang.String, java.lang.String, ?[OBJECT, ARRAY]]
         candidates:
          cq.a(java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, android.content.Context):java.lang.String
          cq.a(boolean, android.content.Context, int, java.lang.String, java.lang.String, bn):java.lang.String */
        /* access modifiers changed from: protected */
        public Object doInBackground(Object... objArr) {
            try {
                String a2 = cq.a(true, this.a.getContext(), 3, (String) null, (String) null, (bn) null);
                Log.d(this.a.z, "begin prelogin xmlData>>>>>>" + a2);
                cp.a(this.a.getContext(), a2);
                String a3 = cq.a(true, this.a.getContext(), 4, bn.a().f(), bn.a().g(), (bn) null);
                Log.d(this.a.z, "begin login xmlData>>>>>>" + a3);
                cp.a(this.a.getContext(), a3, (Handler) null);
                String d = cq.d(this.a.getContext());
                Log.d(this.a.z, "Response BindPhone >>\t" + d);
                bn a4 = bn.a();
                this.a.x = cp.h(this.a.getContext(), d);
                a4.k(this.a.x);
                aw.a(this.a.g()).a(a4);
                Log.d(this.a.z, "RequestBindPhoneNum>>>>>>>>" + this.a.x);
                this.a.at.a(this.a.x);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Object obj) {
            this.a.ak.setText(this.a.x);
            super.onPostExecute(obj);
        }
    }

    class RequestSessionId extends AsyncTask {
        final /* synthetic */ SettingView a;
        private String b;
        private ProgressDialog c;

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: cq.a(boolean, android.content.Context, int, java.lang.String, java.lang.String, bn):java.lang.String
         arg types: [int, android.content.Context, int, ?[OBJECT, ARRAY], ?[OBJECT, ARRAY], ?[OBJECT, ARRAY]]
         candidates:
          cq.a(java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, android.content.Context):java.lang.String
          cq.a(boolean, android.content.Context, int, java.lang.String, java.lang.String, bn):java.lang.String */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: cq.a(boolean, android.content.Context, int, java.lang.String, java.lang.String, bn):java.lang.String
         arg types: [int, android.content.Context, int, java.lang.String, java.lang.String, ?[OBJECT, ARRAY]]
         candidates:
          cq.a(java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, android.content.Context):java.lang.String
          cq.a(boolean, android.content.Context, int, java.lang.String, java.lang.String, bn):java.lang.String */
        /* access modifiers changed from: protected */
        /* renamed from: a */
        public Integer doInBackground(Object... objArr) {
            if (bn.a().e() != null && !bn.a().e().trim().equals("")) {
                return 1;
            }
            if (il.b(this.a.getContext())) {
                String f = bn.a().f();
                String g = bn.a().g();
                try {
                    if (!il.b(this.a.getContext())) {
                        jh.a(this.a.getContext(), (int) R.string.app_net_error);
                        return -1;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                String a2 = cq.a(true, this.a.y, 3, (String) null, (String) null, (bn) null);
                Log.d(this.a.z, "xmlData>>>>>>" + a2);
                cp.a(this.a.y, a2);
                String a3 = cq.a(true, this.a.y, 4, f, g, (bn) null);
                Log.d(this.a.z, "xmlData>>>>>>" + a3);
                if (a3 == null) {
                    return -1;
                }
                return Integer.valueOf(cp.a(this.a.y, a3, this.a.ay));
            }
            jh.a(this.a.getContext(), (int) R.string.app_net_error);
            return -1;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onPostExecute(Integer num) {
            if (this.c != null) {
                this.c.dismiss();
            }
            if (num.intValue() < 0) {
                jh.a(this.a.getContext(), (int) R.string.setting_getsid_err);
            } else {
                this.a.a(this.b);
            }
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            if (bn.a().e() != null && !bn.a().e().trim().equals("")) {
                this.c = new ProgressDialog(this.a.getContext());
                this.c.getWindow().requestFeature(1);
                this.c.setMessage(this.a.getContext().getResources().getString(R.string.setting_bind_wait));
                this.c.setCancelable(false);
                this.c.show();
            }
        }
    }

    public SettingView(Activity activity) {
        super(activity);
        this.y = activity;
    }

    /* access modifiers changed from: private */
    public void a(EditText editText) {
        if (editText != null) {
            ((InputMethodManager) g().getSystemService("input_method")).hideSoftInputFromWindow(editText.getWindowToken(), 0);
        }
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        Intent intent = new Intent("android.intent.action.SENDTO", Uri.parse("smsto:" + str));
        intent.putExtra("sms_body", getContext().getString(R.string.setting_sms_tips).replace("%sId", bn.a().e()).replace("%tgid", as.a(getContext()).C()).replace("%ver", as.a(getContext()).B()));
        g().startActivity(intent);
    }

    private void b(String str) {
        this.aD.setText(str);
        this.aD.setFilters(new InputFilter[]{new InputFilter() {
            public CharSequence filter(CharSequence charSequence, int i, int i2, Spanned spanned, int i3, int i4) {
                return charSequence.length() < 1 ? spanned.subSequence(i3, i4) : "";
            }
        }});
    }

    private void s() {
        t();
        this.ar = as.a(this.y);
        this.ar.e();
        this.at = as.a(this.y);
        this.Y.setChecked(this.at.H());
        View findViewById = findViewById(R.id.autoCacheLine);
        if (!this.ar.ab()) {
            this.E.setVisibility(0);
            findViewById.setVisibility(0);
        } else {
            this.E.setVisibility(8);
            findViewById.setVisibility(8);
        }
        this.Z.setChecked(this.at.I());
        this.aa.setChecked(this.at.J());
        if (!this.at.I()) {
            this.J.setVisibility(8);
            this.ap.setVisibility(8);
        }
        this.ab.setChecked(this.at.K());
        this.ac.setChecked(this.at.L());
        this.ad.setChecked(this.at.M());
        this.ae.setChecked(this.at.N());
        this.af.setChecked(this.at.O());
        this.ag.setChecked(this.at.P());
        this.an.setText(this.at.E());
        this.ai.setChecked(this.at.F());
        this.aj.setChecked(this.at.G());
        this.am.setEnabled(this.at.G());
        this.aq.setEnabled(this.at.G());
        this.ak.setText(bn.a().n());
        switch (this.at.x()) {
            case 0:
                this.al.setText(getContext().getResources().getTextArray(R.array.music_quality)[0]);
                break;
            case 1:
                this.al.setText(getContext().getResources().getTextArray(R.array.music_quality)[1]);
                break;
            case 2:
                this.al.setText(getContext().getResources().getTextArray(R.array.music_quality)[2]);
                break;
        }
        if (this.ae.isChecked()) {
            this.R.setVisibility(8);
            this.ao.setVisibility(8);
        } else {
            this.R.setVisibility(0);
            this.ao.setVisibility(0);
        }
        this.ah.setChecked(this.at.Q());
        this.ae.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                if (z) {
                    SettingView.this.R.setVisibility(8);
                    SettingView.this.ao.setVisibility(8);
                    return;
                }
                SettingView.this.R.setVisibility(0);
                SettingView.this.ao.setVisibility(0);
            }
        });
        if (this.a) {
            this.U.setVisibility(8);
        } else {
            this.U.setVisibility(0);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void t() {
        this.aB = new Dialog(g());
        Window window = this.aB.getWindow();
        WindowManager.LayoutParams attributes = window.getAttributes();
        window.requestFeature(1);
        window.setBackgroundDrawableResource(R.drawable.none);
        window.setAttributes(attributes);
        this.aC = LayoutInflater.from(g()).inflate((int) R.layout.sina_login, (ViewGroup) null, false);
        this.aB.setContentView(this.aC);
        this.aH = this.aC.findViewById(R.id.sina_login_name_layout);
        this.aD = (EditText) this.aC.findViewById(R.id.sina_login_user_name);
        this.aE = (EditText) this.aC.findViewById(R.id.sina_login_user_passwd);
        this.aF = (Button) this.aC.findViewById(R.id.sina_login_btn);
        this.aG = (Button) this.aC.findViewById(R.id.sina_cancle_btn);
        this.aF.setOnClickListener(this);
        this.aG.setOnClickListener(this);
        this.aH.setBackgroundResource(R.drawable.sina_login_edit_layout_background);
    }

    private void u() {
        p a = p.a(g());
        LoginRegView.z = true;
        Bundle bundle = new Bundle();
        bundle.putBoolean("isQuitOnCancle", false);
        bundle.putString("gotoWhere", BindSettingView.class.getName());
        bundle.putBoolean("isReplaceLocalUserData", true);
        bundle.putBoolean("isNotBackToLogin", true);
        a.a(LoginRegView.class.getName(), bundle);
        this.ay.sendEmptyMessage(4);
    }

    private void v() {
        p a = p.a(g());
        Bundle bundle = new Bundle();
        bundle.putBoolean("isQuitOnCancle", false);
        bundle.putString("gotoWhere", BindSettingView.class.getName());
        bundle.putBoolean("isReplaceLocalUserData", true);
        bundle.putBoolean("isNotBackToLogin", false);
        a.a(SinaLoginView.class.getName(), bundle);
        this.ay.sendEmptyMessage(4);
    }

    public void a() {
        if (az) {
            s();
        }
        super.a();
    }

    public void a(int i, int i2) {
        Looper.prepare();
        jh.a(g(), (int) R.string.weibo_bind_validate_failure);
        if (this.av != null && this.av.isShowing()) {
            this.av.dismiss();
        }
        Looper.loop();
        if (i == 0) {
            u();
        } else if (i == 1) {
            switch (i2) {
                case -3:
                    this.ay.obtainMessage(5, 1, 1).sendToTarget();
                    v();
                    return;
                default:
                    this.ay.obtainMessage(5, 1, 2).sendToTarget();
                    v();
                    return;
            }
        }
    }

    public void a(int i, String str) {
        Log.d("test", "" + i);
        switch (i) {
            case -5:
                this.ay.removeMessages(-103);
                this.ay.sendEmptyMessageDelayed(-103, 500);
                BindSettingView.x = true;
                b(str);
                this.ay.sendEmptyMessage(-101);
                return;
            case -4:
                this.ay.removeMessages(-102);
                this.ay.sendEmptyMessageDelayed(-102, 500);
                BindSettingView.x = true;
                b(str);
                this.ay.sendEmptyMessage(-101);
                return;
            case -3:
                this.ay.removeMessages(-102);
                this.ay.sendEmptyMessageDelayed(-102, 500);
                BindSettingView.x = true;
                b(str);
                this.ay.sendEmptyMessage(-101);
                return;
            case -2:
                this.ay.removeMessages(-102);
                this.ay.sendEmptyMessageDelayed(-102, 500);
                BindSettingView.x = true;
                b(str);
                this.ay.sendEmptyMessage(-101);
                return;
            case -1:
                BindSettingView.x = false;
                bn.a().a(1, getContext());
                return;
            default:
                return;
        }
    }

    public void a(Message message) {
        super.a(message);
        as a = as.a(g());
        ImageView imageView = (ImageView) findViewById(R.id.sociality_net_line);
        ImageView imageView2 = (ImageView) findViewById(R.id.bindPhone_line);
        if (!a.j()[2].equals(String.valueOf(0))) {
            this.O.setVisibility(8);
            imageView.setVisibility(8);
            this.as.setVisibility(8);
            imageView2.setVisibility(8);
        } else {
            this.O.setVisibility(0);
            imageView.setVisibility(0);
            this.as.setVisibility(0);
            imageView2.setVisibility(0);
        }
        if (!kf.b) {
            this.O.setVisibility(8);
            this.P.setVisibility(8);
            ((ImageView) findViewById(R.id.sociality_net_line)).setVisibility(8);
            imageView2.setVisibility(8);
        }
        if (this.a) {
            this.U.setVisibility(8);
        } else {
            this.U.setVisibility(0);
        }
    }

    public boolean a(int i, KeyEvent keyEvent) {
        if (i != 4) {
            return false;
        }
        this.A.a(MultiView.class.getName());
        return true;
    }

    public void f() {
        inflate(g(), R.layout.setting, this);
        this.A = p.a(g());
        this.B = (Button) findViewById(R.id.complete_button);
        this.E = (RelativeLayout) findViewById(R.id.playAutoChache);
        this.F = (RelativeLayout) findViewById(R.id.tone);
        this.G = (RelativeLayout) findViewById(R.id.cableSwitch);
        this.H = (RelativeLayout) findViewById(R.id.get_lyric);
        this.J = (RelativeLayout) findViewById(R.id.match_lyric);
        this.K = (RelativeLayout) findViewById(R.id.get_album);
        this.L = (RelativeLayout) findViewById(R.id.kloklyric);
        this.M = (RelativeLayout) findViewById(R.id.backLight);
        this.N = (RelativeLayout) findViewById(R.id.sinablog);
        this.O = (RelativeLayout) findViewById(R.id.sociality_net);
        this.P = (RelativeLayout) findViewById(R.id.share);
        this.V = (RelativeLayout) findViewById(R.id.autoplay);
        this.W = (RelativeLayout) findViewById(R.id.sensor);
        this.as = (RelativeLayout) findViewById(R.id.bindPhone);
        this.Q = (RelativeLayout) findViewById(R.id.useWifi);
        this.R = (RelativeLayout) findViewById(R.id.internetPrompt);
        this.S = (RelativeLayout) findViewById(R.id.quitPrompt);
        this.T = (RelativeLayout) findViewById(R.id.account_info);
        this.Y = (CheckBox) findViewById(R.id.playAutoChache_check);
        this.Z = (CheckBox) findViewById(R.id.get_lyric_check);
        this.aa = (CheckBox) findViewById(R.id.match_lyric_check);
        this.ab = (CheckBox) findViewById(R.id.get_album_check);
        this.ac = (CheckBox) findViewById(R.id.kloklyric_check);
        this.ad = (CheckBox) findViewById(R.id.backLight_check);
        this.ae = (CheckBox) findViewById(R.id.useWifi_check);
        this.ai = (CheckBox) findViewById(R.id.autoplay_check);
        this.aj = (CheckBox) findViewById(R.id.sensor_check);
        this.af = (CheckBox) findViewById(R.id.internetPrompt_check);
        this.ag = (CheckBox) findViewById(R.id.quitPrompt_check);
        this.ah = (CheckBox) findViewById(R.id.cableSwitch_check);
        this.ao = (ImageView) findViewById(R.id.line);
        this.ap = (ImageView) findViewById(R.id.line2);
        this.ak = (TextView) findViewById(R.id.bindPhone_number);
        this.al = (TextView) findViewById(R.id.play_tone);
        this.U = (RelativeLayout) findViewById(R.id.download_path);
        this.an = (TextView) findViewById(R.id.download_path_value);
        this.C = (Button) findViewById(R.id.back_default);
        this.C.setOnClickListener(this);
        this.B.setOnClickListener(this);
        this.am = (TextView) findViewById(R.id.sensor_set_text);
        this.aq = (SeekBar) findViewById(R.id.sensor_set_seek);
        this.aq.setProgress(as.a(this.y).S());
        this.I = (RelativeLayout) findViewById(R.id.sensor_set);
        this.aq.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int i, boolean z) {
                as.a(SettingView.this.getContext()).f(i);
                DuomiMainActivity.a(SettingView.this.getContext());
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
        this.X = (RelativeLayout) findViewById(R.id.change_background);
        this.X.setOnClickListener(this);
        this.D = (Button) findViewById(R.id.restore_default);
        this.D.setOnClickListener(this);
        this.E.setOnClickListener(this);
        this.F.setOnClickListener(this);
        this.G.setOnClickListener(this);
        this.H.setOnClickListener(this);
        this.J.setOnClickListener(this);
        this.K.setOnClickListener(this);
        this.L.setOnClickListener(this);
        this.M.setOnClickListener(this);
        this.N.setOnClickListener(this);
        this.O.setOnClickListener(this);
        this.P.setOnClickListener(this);
        this.as.setOnClickListener(this);
        this.Q.setOnClickListener(this);
        this.R.setOnClickListener(this);
        this.S.setOnClickListener(this);
        this.T.setOnClickListener(this);
        this.U.setOnClickListener(this);
        this.V.setOnClickListener(this);
        this.W.setOnClickListener(this);
        au = new AlertDialog.Builder(this.y).create();
        az = true;
        this.ay = new Handler() {
            public void handleMessage(Message message) {
                switch (message.what) {
                    case -103:
                        jh.a(SettingView.this.g(), (int) R.string.weibo_passwd_doesnt_match);
                        break;
                    case -102:
                        jh.a(SettingView.this.g(), (int) R.string.weibo_doesnt_match);
                        break;
                    case -101:
                        SettingView.this.aB.show();
                        break;
                    case -1:
                        if (SettingView.this.aI != null && SettingView.this.aI.isShowing()) {
                            SettingView.this.aI.dismiss();
                        }
                        switch (message.arg1) {
                            case -3:
                                jh.a(SettingView.this.g(), (int) R.string.sina_username_pwd_error_prompt, 1);
                                break;
                            case -2:
                            case -1:
                            default:
                                jh.a(SettingView.this.g(), (int) R.string.system_error, 1);
                                break;
                            case 0:
                                if (SettingView.this.aB != null && SettingView.this.aB.isShowing()) {
                                    SettingView.this.aB.dismiss();
                                }
                                aw.a(SettingView.this.g()).a(bn.a());
                                SettingView.this.a(SettingView.this.aD);
                                SettingView.this.a(SettingView.this.aE);
                                jh.a(SettingView.this.g(), (int) R.string.weibo_doesnt_match_ok, 1);
                                p.a(SettingView.this.g()).a(BindSettingView.class.getName());
                                SettingView.this.ay.sendEmptyMessage(4);
                                return;
                        }
                    case 3:
                        ProgressDialog unused = SettingView.this.av = new ProgressDialog(SettingView.this.y);
                        SettingView.this.av.setProgressStyle(0);
                        SettingView.this.av.setTitle((int) R.string.check_account_title);
                        SettingView.this.av.setMessage(SettingView.this.y.getResources().getString(R.string.check_account_message));
                        SettingView.this.av.setCancelable(true);
                        SettingView.this.av.show();
                        break;
                    case 4:
                        if (SettingView.this.av != null && SettingView.this.av.isShowing()) {
                            SettingView.this.av.dismiss();
                            ProgressDialog unused2 = SettingView.this.av = (ProgressDialog) null;
                            break;
                        }
                    case 5:
                        switch (message.arg1) {
                            case 1:
                                switch (message.arg2) {
                                    case 1:
                                        jh.a(SettingView.this.y, (int) R.string.sina_username_pwd_error_prompt, 1);
                                        break;
                                    case 2:
                                        jh.a(SettingView.this.y, (int) R.string.weibo_common_error_prompt, 1);
                                        break;
                                }
                        }
                    case 31:
                        Intent intent = new Intent(SettingView.this.getContext(), PickBgActivity.class);
                        as.a(SettingView.this.g()).x(true);
                        SettingView.this.getContext().startActivity(intent);
                        break;
                    case 32:
                        ProgressDialog unused3 = SettingView.this.aw = (ProgressDialog) null;
                        ProgressDialog unused4 = SettingView.this.aw = new ProgressDialog(SettingView.this.g());
                        SettingView.this.aw.setProgressStyle(0);
                        SettingView.this.aw.setTitle("请稍候");
                        SettingView.this.aw.setMessage("恢复中...");
                        SettingView.this.aw.setCancelable(false);
                        SettingView.this.aw.show();
                        sendEmptyMessageDelayed(33, 1000);
                        break;
                    case 33:
                        if (SettingView.this.aw != null && SettingView.this.aw.isShowing()) {
                            SettingView.this.aw.dismiss();
                            as.a(SettingView.this.g()).w(false);
                            SettingView.this.g().getWindow().setBackgroundDrawableResource(R.drawable.background);
                            break;
                        }
                    case 34:
                        SettingView.this.ak.setText(bn.a().n());
                        break;
                    case 250:
                        jh.a(SettingView.this.g(), (int) R.string.conntect_timeout);
                        if (SettingView.this.av != null && SettingView.this.av.isShowing()) {
                            SettingView.this.av.dismiss();
                            break;
                        }
                }
                super.handleMessage(message);
            }
        };
        g().getContentResolver().registerContentObserver(Uri.parse("content://sms"), true, new BindPhoneView.SmsObserver(g(), this.ay));
        if (!kf.b) {
            this.O.setVisibility(8);
            this.P.setVisibility(8);
            ((ImageView) findViewById(R.id.sociality_net_line)).setVisibility(8);
            ((ImageView) findViewById(R.id.bindPhone_line)).setVisibility(8);
        }
        s();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: en.a(android.content.Context, boolean):void
     arg types: [android.content.Context, int]
     candidates:
      en.a(int, int):int
      en.a(android.content.Context, float):int
      en.a(android.content.Context, int):int
      en.a(android.graphics.Bitmap, int):android.graphics.Bitmap
      en.a(android.content.Context, java.lang.String):java.lang.String
      en.a(android.content.Context, bj):void
      en.a(java.io.File, java.io.File):void
      en.a(java.lang.String, java.lang.String):void
      en.a(android.app.Activity, boolean):boolean
      en.a(java.security.interfaces.RSAPublicKey, byte[]):byte[]
      en.a(byte[], java.lang.String):byte[]
      en.a(android.content.Context, boolean):void */
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.account_info:
                p.a(g()).a(SinaAccountInfoView.class.getName());
                return;
            case R.id.download_path:
                p.a(g()).a(DownloadPathView.class.getName());
                return;
            case R.id.sina_login_btn:
                a(this.aD);
                a(this.aE);
                this.aI = new ProgressDialog(g());
                Window window = this.aI.getWindow();
                WindowManager.LayoutParams attributes = window.getAttributes();
                window.requestFeature(1);
                window.setBackgroundDrawableResource(R.drawable.none);
                window.setAttributes(attributes);
                this.aI.setMessage(g().getString(R.string.lg_tips));
                this.aI.setProgressStyle(0);
                this.aI.show();
                if (this.av != null && this.av.isShowing()) {
                    this.av.dismiss();
                }
                new Thread(this.aJ).start();
                return;
            case R.id.complete_button:
                this.A.a(MultiView.class.getName());
                return;
            case R.id.useWifi:
                if (this.ae.isChecked()) {
                    this.ae.setChecked(false);
                    this.at.p(false);
                    return;
                }
                this.ae.setChecked(true);
                this.at.p(true);
                return;
            case R.id.internetPrompt:
                if (this.af.isChecked()) {
                    this.af.setChecked(false);
                    this.at.q(false);
                    return;
                }
                this.af.setChecked(true);
                this.at.q(true);
                return;
            case R.id.quitPrompt:
                if (this.ag.isChecked()) {
                    this.ag.setChecked(false);
                    this.at.r(false);
                    return;
                }
                this.ag.setChecked(true);
                this.at.r(true);
                return;
            case R.id.playAutoChache:
                if (this.Y.isChecked()) {
                    this.Y.setChecked(false);
                    this.at.j(false);
                    return;
                }
                this.Y.setChecked(true);
                this.at.j(true);
                return;
            case R.id.tone:
                new AlertDialog.Builder(this.y).setTitle((int) R.string.setting_music_quality).setSingleChoiceItems((int) R.array.music_quality, this.at.x(), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        switch (i) {
                            case 0:
                                SettingView.this.al.setText(SettingView.this.getContext().getResources().getTextArray(R.array.music_quality)[0]);
                                SettingView.this.at.e(0);
                                break;
                            case 1:
                                SettingView.this.al.setText(SettingView.this.getContext().getResources().getTextArray(R.array.music_quality)[1]);
                                SettingView.this.at.e(1);
                                break;
                            case 2:
                                SettingView.this.al.setText(SettingView.this.getContext().getResources().getTextArray(R.array.music_quality)[2]);
                                SettingView.this.at.e(2);
                                break;
                        }
                        dialogInterface.dismiss();
                    }
                }).show();
                return;
            case R.id.back_default:
                this.at.i(ae.o);
                this.an.setText(this.at.E());
                return;
            case R.id.cableSwitch:
                if (this.ah.isChecked()) {
                    this.ah.setChecked(false);
                    this.at.s(false);
                    return;
                }
                this.ah.setChecked(true);
                this.at.s(true);
                return;
            case R.id.get_lyric:
                if (this.Z.isChecked()) {
                    this.Z.setChecked(false);
                    this.at.l(false);
                    this.J.setVisibility(8);
                    this.ap.setVisibility(8);
                    return;
                }
                this.Z.setChecked(true);
                this.at.l(true);
                this.J.setVisibility(0);
                this.ap.setVisibility(0);
                return;
            case R.id.match_lyric:
                if (this.aa.isChecked()) {
                    this.aa.setChecked(false);
                    this.at.m(false);
                    return;
                }
                this.aa.setChecked(true);
                this.at.m(true);
                return;
            case R.id.get_album:
                if (this.ab.isChecked()) {
                    this.ab.setChecked(false);
                    this.at.k(false);
                    return;
                }
                this.ab.setChecked(true);
                this.at.k(true);
                return;
            case R.id.kloklyric:
                if (this.ac.isChecked()) {
                    this.ac.setChecked(false);
                    this.at.n(false);
                    return;
                }
                this.ac.setChecked(true);
                this.at.n(true);
                return;
            case R.id.backLight:
                if (this.ad.isChecked()) {
                    this.ad.setChecked(false);
                    this.at.o(false);
                    return;
                }
                this.ad.setChecked(true);
                this.at.o(true);
                return;
            case R.id.autoplay:
                if (this.ai.isChecked()) {
                    this.ai.setChecked(false);
                    this.at.h(false);
                    return;
                }
                this.ai.setChecked(true);
                this.at.h(true);
                return;
            case R.id.sensor:
                if (this.aj.isChecked()) {
                    this.aj.setChecked(false);
                    this.at.i(false);
                    this.am.setEnabled(false);
                    this.aq.setEnabled(false);
                    DuomiMainActivity.a();
                    return;
                }
                this.aj.setChecked(true);
                this.at.i(true);
                this.am.setEnabled(true);
                this.aq.setEnabled(true);
                DuomiMainActivity.a(getContext());
                return;
            case R.id.change_background:
                this.ay.sendEmptyMessageDelayed(31, 300);
                return;
            case R.id.restore_default:
                this.ay.sendEmptyMessage(32);
                return;
            case R.id.bindPhone:
                p.a(g()).a(BindPhoneView.class.getName());
                return;
            case R.id.sinablog:
            default:
                return;
            case R.id.sociality_net:
                this.ay.sendEmptyMessage(3);
                if (il.b(g())) {
                    new Thread(this.aA).start();
                    return;
                }
                this.ay.sendEmptyMessage(4);
                jh.a(getContext(), (int) R.string.weibo_connect_unable);
                return;
            case R.id.share:
                p.a(g()).a(ShareSettingView.class.getName());
                return;
            case R.id.sina_cancle_btn:
                a(this.aD);
                a(this.aE);
                if (this.aB != null && this.aB.isShowing()) {
                    this.aB.dismiss();
                }
                if (this.aI != null && this.aI.isShowing()) {
                    this.aI.dismiss();
                }
                if (this.av != null && this.av.isShowing()) {
                    this.av.dismiss();
                    return;
                }
                return;
            case R.id.logout_button:
                en.a(getContext(), true);
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void p() {
        super.p();
        if (this.U == null) {
            return;
        }
        if (this.a) {
            this.U.setVisibility(8);
        } else {
            this.U.setVisibility(0);
        }
    }

    public void q() {
        Log.d("test", "success");
        p.a(g()).a(BindSettingView.class.getName());
        this.ay.sendEmptyMessage(4);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: bn.a(android.content.Context, boolean):void
     arg types: [android.content.Context, int]
     candidates:
      bn.a(int, android.content.Context):int
      bn.a(android.content.Context, boolean):void */
    public void r() {
        BindSettingView.x = true;
        bn.a().a(this.y, false);
    }
}
