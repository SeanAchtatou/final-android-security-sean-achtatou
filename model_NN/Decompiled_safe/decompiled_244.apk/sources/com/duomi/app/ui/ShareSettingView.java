package com.duomi.app.ui;

import android.app.Activity;
import android.os.Message;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.duomi.android.R;

public class ShareSettingView extends c implements View.OnClickListener, View.OnTouchListener {
    private RelativeLayout A;
    private CheckBox B;
    private CheckBox C;
    private CheckBox D;
    private TextView E;
    private RelativeLayout F;
    as x = as.a(g());
    private RelativeLayout y;
    private RelativeLayout z;

    public ShareSettingView(Activity activity) {
        super(activity);
    }

    private void r() {
        if (this.C.isChecked()) {
            this.C.setChecked(false);
            this.x.u(false);
            this.D.setChecked(false);
            this.x.v(false);
            this.D.setEnabled(false);
            this.D.setClickable(false);
            this.A.setEnabled(false);
            this.A.setClickable(false);
            this.E.setTextColor(-7829368);
            return;
        }
        this.C.setChecked(true);
        this.x.u(true);
        this.D.setClickable(false);
        this.D.setEnabled(true);
        this.A.setEnabled(true);
        this.A.setClickable(true);
        this.E.setTextColor(-1);
    }

    private void s() {
        if (this.D.isChecked()) {
            this.D.setChecked(false);
            this.x.v(false);
            return;
        }
        this.D.setChecked(true);
        this.x.v(true);
    }

    public void a(Message message) {
        super.a(message);
        q();
    }

    public boolean a(int i, KeyEvent keyEvent) {
        if (i != 4) {
            return false;
        }
        p.a(g()).a(SettingView.class.getName());
        return true;
    }

    /* access modifiers changed from: protected */
    public void f() {
        inflate(g(), R.layout.share_setting, this);
        this.y = (RelativeLayout) findViewById(R.id.ly_share_download);
        this.z = (RelativeLayout) findViewById(R.id.ly_share_store);
        this.A = (RelativeLayout) findViewById(R.id.ly_share_lyric);
        this.B = (CheckBox) findViewById(R.id.auto_share_downloading);
        this.C = (CheckBox) findViewById(R.id.auto_share_store);
        this.D = (CheckBox) findViewById(R.id.auto_share_lyric);
        this.B.setClickable(false);
        this.C.setClickable(false);
        this.D.setClickable(false);
        this.F = (RelativeLayout) findViewById(R.id.go_back_layout);
        this.E = (TextView) findViewById(R.id.ly_share_lyric_text);
        q();
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.go_back_layout:
                p.a(g()).a(SettingView.class.getName());
                return;
            case R.id.ly_share_download:
                if (this.B.isChecked()) {
                    this.B.setChecked(false);
                    this.x.t(false);
                    return;
                }
                this.B.setChecked(true);
                this.x.t(true);
                return;
            case R.id.ly_share_store:
                r();
                return;
            case R.id.ly_share_lyric:
                s();
                return;
            default:
                return;
        }
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        motionEvent.getAction();
        return false;
    }

    public void q() {
        this.B.setChecked(this.x.T());
        this.C.setChecked(this.x.U());
        this.D.setChecked(this.x.V());
        this.C.setOnTouchListener(this);
        this.D.setOnTouchListener(this);
        this.y.setOnClickListener(this);
        this.z.setOnClickListener(this);
        this.A.setOnClickListener(this);
        this.F.setOnClickListener(this);
        this.D.setClickable(false);
        if (this.x.U()) {
            this.D.setEnabled(true);
            this.A.setEnabled(true);
            this.E.setTextColor(-1);
            this.A.setClickable(true);
            return;
        }
        this.D.setEnabled(false);
        this.A.setEnabled(false);
        this.A.setClickable(false);
        this.E.setTextColor(-7829368);
    }
}
