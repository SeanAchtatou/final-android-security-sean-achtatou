package com.duomi.app.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.Window;
import com.duomi.android.R;

public class WelcomeView extends c {
    private static ProgressDialog A;
    private View x;
    /* access modifiers changed from: private */
    public Context y;
    /* access modifiers changed from: private */
    public Handler z;

    class RedirectThread extends Thread {
        RedirectThread() {
        }

        private void a(p pVar) {
            LoginRegView.z = true;
            pVar.b(LoginRegView.class.getName());
        }

        private boolean a(String[] strArr) {
            return aw.a(WelcomeView.this.getContext()).a(strArr[0], strArr[1], Integer.parseInt(strArr[2])) != null;
        }

        public void run() {
            p a2 = p.a(WelcomeView.this.g());
            if (!Environment.getExternalStorageState().equals("mounted")) {
                WelcomeView.this.z.sendEmptyMessage(1);
            }
            while (true) {
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (a2.a() && gx.d != null && ev.a != null) {
                    break;
                }
            }
            as a3 = as.a(WelcomeView.this.getContext());
            boolean g = a3.g();
            boolean h = a3.h();
            String[] j = a3.j();
            if (g) {
                en.i(WelcomeView.this.y);
                hx.a().c();
                WelcomeView.this.z.sendEmptyMessage(4);
            } else if (j == null || j.length < 2) {
                en.i(WelcomeView.this.y);
                a(a2);
            } else if (h) {
                if (!en.c(j[0]) && !en.c(j[1]) && a(j)) {
                    WelcomeView.this.getContext().sendBroadcast(new Intent("com.duomi.music_reloadlist"));
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e2) {
                        e2.printStackTrace();
                    }
                    a2.a(MultiView.class.getName());
                } else if (j[2].equals("0")) {
                    a(a2);
                } else if (j[2].equals("1")) {
                    WelcomeView.this.b(a2);
                }
                en.i(WelcomeView.this.y);
            } else {
                en.i(WelcomeView.this.y);
                if (j[2].equals("0")) {
                    a(a2);
                } else if (j[2].equals("1")) {
                    WelcomeView.this.b(a2);
                    SinaLoginView.x = true;
                }
            }
            if (ae.k && !a3.b()) {
                new Thread() {
                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: cq.a(android.content.Context, java.lang.String, boolean):java.lang.String
                     arg types: [android.content.Context, java.lang.String, int]
                     candidates:
                      cq.a(android.content.Context, bj, java.lang.String):java.lang.String
                      cq.a(android.content.Context, java.lang.String, java.lang.String):java.lang.String
                      cq.a(java.lang.String, java.lang.String, android.content.Context):java.lang.String
                      cq.a(android.content.Context, java.lang.String, boolean):java.lang.String */
                    public void run() {
                        cq.a(WelcomeView.this.getContext(), cq.c(WelcomeView.this.getContext()), false);
                    }
                }.start();
            }
            a3.a(false);
        }
    }

    public WelcomeView(Activity activity) {
        super(activity);
        this.y = activity;
    }

    public void a() {
        super.a();
        new RedirectThread().start();
    }

    public void a(p pVar) {
        LoginRegView.z = false;
        pVar.b(LoginRegView.class.getName());
    }

    public void b(p pVar) {
        pVar.b(SinaLoginView.class.getName());
    }

    public void f() {
        this.z = new Handler() {
            public void handleMessage(Message message) {
                switch (message.what) {
                    case 1:
                        jh.a(WelcomeView.this.getContext(), (int) R.string.prompt_no_sd);
                        break;
                    case 2:
                        WelcomeView.this.a(p.a(WelcomeView.this.g()));
                        break;
                    case 4:
                        FirstTipView firstTipView = new FirstTipView(WelcomeView.this.g(), WelcomeView.this.z);
                        firstTipView.setFocusable(true);
                        WelcomeView.this.addView(firstTipView);
                        break;
                }
                super.handleMessage(message);
            }
        };
        Window window = g().getWindow();
        if (window != null) {
            window.setFlags(1024, 1024);
        }
        this.x = inflate(g(), R.layout.welcome, this);
        ad.b("DomiStart", "3----" + System.currentTimeMillis());
        A = new ProgressDialog(this.y);
        A.setTitle("下载更新");
        A.setMax(100);
        A.setProgressStyle(1);
    }
}
