package com.duomi.app.ui;

import android.app.Activity;
import android.content.Context;
import android.os.Message;
import android.view.MotionEvent;
import android.view.Window;
import com.duomi.android.R;

public class DialogView extends c {
    private Context x;
    private Message y = null;

    public DialogView(Activity activity) {
        super(activity);
        this.x = activity;
    }

    private void q() {
    }

    public void f() {
        Window window = g().getWindow();
        if (window != null) {
            window.clearFlags(1024);
        }
        inflate(g(), R.layout.waiting_dialog, this);
        q();
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        return true;
    }
}
