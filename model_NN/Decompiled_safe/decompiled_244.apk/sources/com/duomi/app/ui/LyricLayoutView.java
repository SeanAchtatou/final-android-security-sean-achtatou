package com.duomi.app.ui;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.duomi.android.R;
import com.duomi.app.ui.MultiView;
import com.duomi.core.view.LyricAdjustView;
import com.duomi.core.view.LyricView;
import java.util.ArrayList;

public class LyricLayoutView extends c {
    private TextView A;
    /* access modifiers changed from: private */
    public LyricView B;
    private RelativeLayout C;
    /* access modifiers changed from: private */
    public TextView D;
    private int E = 16;
    /* access modifiers changed from: private */
    public hg F;
    private MultiView.OnLikeReSetListener G;
    /* access modifiers changed from: private */
    public WindowManager H;
    private WindowManager.LayoutParams I = new WindowManager.LayoutParams();
    private LyricAdjustView J;
    private Dialog K;
    /* access modifiers changed from: private */
    public String L = "";
    private MultiView.OnLyricOffsetListener M;
    private Button N;
    private View O;
    /* access modifiers changed from: private */
    public Handler P = new Handler() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: gx.a(android.content.Context, bj, bl, com.duomi.app.ui.MultiView$OnLikeReSetListener, boolean):void
         arg types: [android.content.Context, bj, bl, ?[OBJECT, ARRAY], int]
         candidates:
          gx.a(android.content.Context, bj, int, android.os.Handler, com.duomi.app.ui.MultiView$OnLikeReSetListener):void
          gx.a(android.content.Context, bl, long, com.duomi.app.ui.MultiView$OnLikeReSetListener, int):void
          gx.a(android.content.Context, bj, bl, com.duomi.app.ui.MultiView$OnLikeReSetListener, boolean):void */
        public void handleMessage(Message message) {
            switch (message.what) {
                case 255:
                    bl d = ar.a(LyricLayoutView.this.getContext()).d(message.getData().getInt("listId"));
                    try {
                        bj k = gx.d.k();
                        if (k.b() == 1) {
                            gx.a(LyricLayoutView.this.getContext(), d, k.f(), (MultiView.OnLikeReSetListener) null, 0);
                            return;
                        } else {
                            gx.a(LyricLayoutView.this.getContext(), k, d, (MultiView.OnLikeReSetListener) null, false);
                            return;
                        }
                    } catch (RemoteException e) {
                        e.printStackTrace();
                        return;
                    }
                default:
                    return;
            }
        }
    };
    private AdapterView.OnItemClickListener Q = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView adapterView, View view, int i, long j) {
            if (i != 3) {
                if (i != 0 && i != 6) {
                    boolean unused = LyricLayoutView.this.a(i);
                } else if (LyricLayoutView.this.f != null && LyricLayoutView.this.f.getVisibility() == 0) {
                    LyricLayoutView.this.f.setVisibility(8);
                }
                switch (i) {
                    case 0:
                        if (LyricLayoutView.this.F != null) {
                            LyricLayoutView.this.F.a();
                            return;
                        }
                        return;
                    case 1:
                        LyricLayoutView.this.r();
                        return;
                    case 2:
                        if (!kf.b) {
                            return;
                        }
                        if (!il.b(LyricLayoutView.this.g())) {
                            jh.a(LyricLayoutView.this.g(), (int) R.string.app_net_error);
                            return;
                        }
                        try {
                            if (gx.d != null && gx.d.k() != null) {
                                bj k = gx.d.k();
                                if (kf.b) {
                                    new gd(LyricLayoutView.this.g(), k);
                                    return;
                                } else {
                                    en.a(LyricLayoutView.this.getContext(), k);
                                    return;
                                }
                            } else if (gx.d != null && gx.d.k() == null) {
                                jh.a(LyricLayoutView.this.g(), (int) R.string.no_playing_song_to_share);
                                return;
                            } else {
                                return;
                            }
                        } catch (RemoteException e) {
                            e.printStackTrace();
                            return;
                        }
                    case 3:
                    case 4:
                    case 5:
                    default:
                        return;
                    case 6:
                        PlayerLayoutView.a(LyricLayoutView.this.getContext());
                        return;
                }
            } else if (LyricLayoutView.this.g.getVisibility() == 0) {
                LyricLayoutView.this.j();
            } else {
                LyricLayoutView.this.i();
            }
        }
    };
    private AdapterView.OnItemClickListener R = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView adapterView, View view, int i, long j) {
            boolean unused = LyricLayoutView.this.b(i);
        }
    };
    private String S = "";
    private Runnable T = new Runnable() {
        public void run() {
            if (LyricLayoutView.this.D.getVisibility() == 0 && LyricLayoutView.this.D.getParent() != null) {
                LyricLayoutView.this.H.removeView(LyricLayoutView.this.D);
            }
        }
    };
    /* access modifiers changed from: private */
    public Runnable U = new Runnable() {
        public void run() {
            LyricLayoutView.this.w();
        }
    };
    private ju V = new ju() {
        public void a(boolean z) {
            if (z) {
                LyricLayoutView.this.B.b(LyricLayoutView.this.B.a() - 0.5f);
            } else {
                LyricLayoutView.this.B.b(LyricLayoutView.this.B.a() + 0.5f);
            }
            LyricLayoutView.this.u();
            LyricLayoutView.this.P.removeCallbacks(LyricLayoutView.this.U);
            LyricLayoutView.this.P.postDelayed(LyricLayoutView.this.U, 3000);
        }
    };
    /* access modifiers changed from: private */
    public boolean W = false;
    private DialogInterface.OnCancelListener X = new DialogInterface.OnCancelListener() {
        public void onCancel(DialogInterface dialogInterface) {
            if (!LyricLayoutView.this.W) {
                LyricLayoutView.this.t();
            }
            new Thread() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.duomi.app.ui.LyricLayoutView.a(com.duomi.app.ui.LyricLayoutView, boolean):boolean
                 arg types: [com.duomi.app.ui.LyricLayoutView, int]
                 candidates:
                  com.duomi.app.ui.LyricLayoutView.a(com.duomi.app.ui.LyricLayoutView, int):boolean
                  com.duomi.app.ui.LyricLayoutView.a(is, int):void
                  com.duomi.app.ui.LyricLayoutView.a(int, android.view.KeyEvent):boolean
                  c.a(java.lang.Class, android.os.Message):void
                  c.a(boolean, android.os.Message):void
                  c.a(int, android.view.KeyEvent):boolean
                  c.a(int, boolean):boolean
                  com.duomi.app.ui.LyricLayoutView.a(com.duomi.app.ui.LyricLayoutView, boolean):boolean */
                public void run() {
                    StringBuffer stringBuffer = new StringBuffer();
                    float a2 = LyricLayoutView.this.B.a();
                    if (!LyricLayoutView.this.W || a2 != 0.0f) {
                        if (((double) a2) > -0.001d) {
                            stringBuffer.append("0");
                            stringBuffer.append(en.b(a2));
                        } else {
                            stringBuffer.append("1");
                            stringBuffer.append(en.b(-a2));
                        }
                        boolean unused = LyricLayoutView.this.W = false;
                        en.a(LyricLayoutView.this.L, stringBuffer.toString());
                        return;
                    }
                    stringBuffer.append("00000000");
                    en.a(LyricLayoutView.this.L, stringBuffer.toString());
                    boolean unused2 = LyricLayoutView.this.W = false;
                    Log.i(LyricLayoutView.this.x, "restore save");
                }
            }.start();
        }
    };
    /* access modifiers changed from: private */
    public String x = "LyricLayoutView";
    private LinearLayout y;
    private LinearLayout z;

    public LyricLayoutView(Activity activity) {
        super(activity);
    }

    private float c(int i) {
        try {
            return Float.parseFloat(en.a(i, 3, "."));
        } catch (Exception e) {
            return 0.0f;
        }
    }

    /* access modifiers changed from: private */
    public void r() {
        if (this.B == null || this.B.getVisibility() != 8) {
            try {
                if (gx.d == null || gx.d.k() != null) {
                    bj k = gx.d.k();
                    if (k != null) {
                        this.L = k.w();
                        if (this.L == null || this.L.length() <= 0) {
                            String h = k.h();
                            String j = k.j();
                            if (h != null && !"".equals(h)) {
                                StringBuffer stringBuffer = new StringBuffer(ae.s);
                                stringBuffer.append("/").append(h);
                                if (j != null && !".".equals(j)) {
                                    stringBuffer.append("_").append(j);
                                }
                                stringBuffer.append(".lrc");
                                this.L = stringBuffer.toString();
                            }
                        }
                    } else {
                        return;
                    }
                }
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            WindowManager.LayoutParams attributes = this.K.getWindow().getAttributes();
            int b = en.b(getContext());
            attributes.x = ((b - (b / 10)) - 12) / 2;
            attributes.y = 0;
            this.K.show();
            this.P.removeCallbacks(this.U);
            this.P.postDelayed(this.U, 3000);
        }
    }

    private void s() {
        this.K = new Dialog(g());
        Window window = this.K.getWindow();
        window.setBackgroundDrawableResource(R.drawable.none);
        window.clearFlags(2);
        this.K.setCanceledOnTouchOutside(true);
        this.K.setOnCancelListener(this.X);
        this.K.requestWindowFeature(1);
        this.N.setOnClickListener(new View.OnClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.duomi.app.ui.LyricLayoutView.a(com.duomi.app.ui.LyricLayoutView, boolean):boolean
             arg types: [com.duomi.app.ui.LyricLayoutView, int]
             candidates:
              com.duomi.app.ui.LyricLayoutView.a(com.duomi.app.ui.LyricLayoutView, int):boolean
              com.duomi.app.ui.LyricLayoutView.a(is, int):void
              com.duomi.app.ui.LyricLayoutView.a(int, android.view.KeyEvent):boolean
              c.a(java.lang.Class, android.os.Message):void
              c.a(boolean, android.os.Message):void
              c.a(int, android.view.KeyEvent):boolean
              c.a(int, boolean):boolean
              com.duomi.app.ui.LyricLayoutView.a(com.duomi.app.ui.LyricLayoutView, boolean):boolean */
            public void onClick(View view) {
                LyricLayoutView.this.B.b(0.0f);
                LyricLayoutView.this.t();
                LyricLayoutView.this.u();
                boolean unused = LyricLayoutView.this.W = true;
                LyricLayoutView.this.P.removeCallbacks(LyricLayoutView.this.U);
                LyricLayoutView.this.P.postDelayed(LyricLayoutView.this.U, 3000);
            }
        });
        this.K.setContentView(this.O);
    }

    /* access modifiers changed from: private */
    public void t() {
        try {
            if (gx.d.k() != null) {
                StringBuffer stringBuffer = new StringBuffer();
                if (((double) this.B.a()) > -1.0E-4d) {
                    stringBuffer.append("歌词提前了").append(this.B.a()).append("s");
                } else {
                    stringBuffer.append("歌词延后了").append(-this.B.a()).append("s");
                }
                if (this.D.getParent() != null) {
                    this.H.removeView(this.D);
                }
                this.H.addView(this.D, this.I);
                this.D.setText(stringBuffer.toString());
                this.P.removeCallbacks(this.T);
                this.P.postDelayed(this.T, 1500);
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void u() {
        this.M.a(this.B.a());
    }

    private void v() {
        this.I.height = -2;
        this.I.width = -2;
        this.I.flags = 152;
        this.I.format = -3;
        this.I.windowAnimations = 16973828;
        this.I.type = 2005;
    }

    /* access modifiers changed from: private */
    public void w() {
        if (this.K != null && this.K.isShowing()) {
            this.K.cancel();
        }
    }

    public void a(float f, float f2, int i) {
        this.B.b(f);
        this.B.a(f2);
        if (i > 0) {
            this.B.a(i);
        }
    }

    public void a(MultiView.OnLikeReSetListener onLikeReSetListener) {
        this.G = onLikeReSetListener;
    }

    public void a(MultiView.OnLyricOffsetListener onLyricOffsetListener) {
        this.M = onLyricOffsetListener;
    }

    public void a(hg hgVar) {
        this.F = hgVar;
    }

    public void a(is isVar) {
        w();
        this.B.setVisibility(0);
        this.y.setVisibility(8);
        this.B.b(isVar.b());
        this.B.a(isVar.c());
    }

    public void a(is isVar, int i) {
        if (isVar != null) {
            float c = c(i);
            if (isVar != null && isVar.b() != null && isVar.b().size() > 0) {
                this.B.c(c);
                this.B.invalidate();
            }
        }
    }

    public void a(boolean z2) {
        this.C.setKeepScreenOn(z2);
    }

    public boolean a(int i, KeyEvent keyEvent) {
        b(i, keyEvent);
        if (i != 4 || this.f.getVisibility() != 0 || this.u) {
            return false;
        }
        if (this.g.getVisibility() == 0) {
            this.g.setVisibility(8);
            this.i.requestFocus();
        } else {
            this.f.setVisibility(8);
        }
        return true;
    }

    public void b(boolean z2) {
        w();
        this.B.setVisibility(8);
        if (z2) {
            this.A.setText((int) R.string.player_lyric_tip_search);
        } else {
            this.A.setText((int) R.string.player_lyric_tip_notfound);
        }
        this.A.setVisibility(0);
        this.y.setVisibility(0);
    }

    public void f() {
        int i;
        inflate(g(), R.layout.lyriclayout, this);
        this.C = (RelativeLayout) findViewById(R.id.lyric_linearlayout);
        this.A = (TextView) findViewById(R.id.lyric_tip);
        this.y = (LinearLayout) findViewById(R.id.lyric_layout);
        this.z = (LinearLayout) findViewById(R.id.lyric_text);
        this.B = (LyricView) findViewById(R.id.lyric_view);
        this.H = (WindowManager) getContext().getSystemService("window");
        this.D = new TextView(getContext());
        this.D.setBackgroundResource(17301654);
        v();
        int b = en.b(getContext());
        switch (b) {
            case 240:
                i = 8;
                this.B.c(250);
                this.E = 14;
                break;
            case 320:
                i = 10;
                this.B.c(350);
                this.E = 18;
                break;
            case 480:
                this.E = 24;
                this.B.c(700);
                i = 20;
                break;
            default:
                this.E = 24;
                this.B.c(700);
                i = 20;
                break;
        }
        this.B.b(b);
        this.B.d(1);
        this.B.a((float) this.E, (float) i);
        this.B.setLongClickable(true);
        this.O = ((LayoutInflater) getContext().getSystemService("layout_inflater")).inflate((int) R.layout.adjust_lyric, (ViewGroup) null);
        this.J = (LyricAdjustView) this.O.findViewById(R.id.lyric_adjust);
        this.N = (Button) this.O.findViewById(R.id.restore_lyric);
        this.J.a(this.V);
        s();
        q();
        h();
    }

    public void m() {
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.h.getLayoutParams();
        if (en.b(getContext()) > 239 && en.b(getContext()) < 241) {
            layoutParams.bottomMargin = 39;
        }
        if (en.b(getContext()) > 319 && en.b(getContext()) < 321) {
            layoutParams.bottomMargin = 49;
        }
        if (en.b(getContext()) > 479 && en.b(getContext()) < 481) {
            layoutParams.bottomMargin = 84;
        }
        this.h.setLayoutParams(layoutParams);
    }

    public void n() {
        this.i.setOnItemClickListener(this.Q);
        this.j.setOnItemClickListener(this.R);
    }

    public void o() {
        if (kf.b) {
            this.q = getResources().getStringArray(R.array.lyric_menu);
        } else {
            this.q = getResources().getStringArray(R.array.lyric_menu_noWeibo);
        }
        this.s = new ArrayList();
        this.s.add(Integer.valueOf((int) R.drawable.meun_search));
        this.s.add(Integer.valueOf((int) R.drawable.meun_lyricadjust));
        if (kf.b) {
            this.s.add(Integer.valueOf((int) R.drawable.meun_sina));
        } else {
            this.s.add(Integer.valueOf((int) R.drawable.menu_bg));
        }
        this.s.add(Integer.valueOf((int) R.drawable.meun_more));
        this.s.add(Integer.valueOf((int) R.drawable.meun_setup));
        this.s.add(Integer.valueOf((int) R.drawable.meun_sleep));
        this.s.add(Integer.valueOf((int) R.drawable.meun_songwrong));
        this.s.add(Integer.valueOf((int) R.drawable.meun_exit));
        this.r = getResources().getStringArray(R.array.second_menu2);
        this.t = new ArrayList();
        this.t.add(Integer.valueOf((int) R.drawable.meun_help));
        this.t.add(Integer.valueOf((int) R.drawable.meun_advice));
        this.t.add(Integer.valueOf((int) R.drawable.meun_friends));
        this.t.add(Integer.valueOf((int) R.drawable.meun_about));
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        return motionEvent.getAction() == 0;
    }

    public void q() {
        this.y.setVisibility(8);
        this.z.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (LyricLayoutView.this.F != null) {
                    LyricLayoutView.this.F.a();
                }
            }
        });
    }
}
