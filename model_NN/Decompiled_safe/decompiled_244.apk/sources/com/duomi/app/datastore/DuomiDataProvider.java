package com.duomi.app.datastore;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;
import com.duomi.advertisement.AdvertisementList;
import com.duomi.android.R;

public class DuomiDataProvider extends ContentProvider {
    private static final UriMatcher a = new UriMatcher(-1);
    private ag b;
    private SQLiteDatabase c;

    static {
        a.addURI("com.duomi.android.datastore", "badorder", 67);
        a.addURI("com.duomi.android.datastore", "ring", 68);
        a.addURI("com.duomi.android.datastore", "serviceinfomodle", 69);
        a.addURI("com.duomi.android.datastore", "musicsearch", 1);
        a.addURI("com.duomi.android.datastore", "musicsearch/#", 2);
        a.addURI("com.duomi.android.datastore", "playerhistory", 3);
        a.addURI("com.duomi.android.datastore", "playerhistory/#", 4);
        a.addURI("com.duomi.android.datastore", "randomhistory", 5);
        a.addURI("com.duomi.android.datastore", "randomhistory/#/#", 6);
        a.addURI("com.duomi.android.datastore", "randomhistory/#", 58);
        a.addURI("com.duomi.android.datastore", "downloads", 7);
        a.addURI("com.duomi.android.datastore", "downloads/#", 8);
        a.addURI("com.duomi.android.datastore", "addition", 9);
        a.addURI("com.duomi.android.datastore", "addition/#", 10);
        a.addURI("com.duomi.android.datastore", "duomiusers", 11);
        a.addURI("com.duomi.android.datastore", "duomiusers/#", 12);
        a.addURI("com.duomi.android.datastore", "cachepic", 13);
        a.addURI("com.duomi.android.datastore", "cachepic/#", 14);
        a.addURI("com.duomi.android.datastore", "category_online", 15);
        a.addURI("com.duomi.android.datastore", "category_online/#", 16);
        a.addURI("com.duomi.android.datastore", "playlog", 19);
        a.addURI("com.duomi.android.datastore", "playlog/#", 20);
        a.addURI("com.duomi.android.datastore", "playlog/max_startcost", 25);
        a.addURI("com.duomi.android.datastore", "playlog/max_speed", 26);
        a.addURI("com.duomi.android.datastore", "playlog/min_speed", 27);
        a.addURI("com.duomi.android.datastore", "playlog/first_line", 28);
        a.addURI("com.duomi.android.datastore", "playlog/end_line", 29);
        a.addURI("com.duomi.android.datastore", "playblocklog", 21);
        a.addURI("com.duomi.android.datastore", "playblocklog/#", 22);
        a.addURI("com.duomi.android.datastore", "playcommand", 23);
        a.addURI("com.duomi.android.datastore", "playcommand/#", 24);
        a.addURI("com.duomi.android.datastore", "playlog/prefix", 30);
        a.addURI("com.duomi.android.datastore", "playlog/groupsongid", 31);
        a.addURI("com.duomi.android.datastore", "halldata/#/#", 32);
        a.addURI("com.duomi.android.datastore", "halldata", 33);
        a.addURI("com.duomi.android.datastore", "song/#", 35);
        a.addURI("com.duomi.android.datastore", "song", 34);
        a.addURI("com.duomi.android.datastore", "songlist/#", 37);
        a.addURI("com.duomi.android.datastore", "songlist", 36);
        a.addURI("com.duomi.android.datastore", "songreflist/songref/#/#", 41);
        a.addURI("com.duomi.android.datastore", "songreflist/refpos/#/#", 42);
        a.addURI("com.duomi.android.datastore", "songreflist/songref/#", 40);
        a.addURI("com.duomi.android.datastore", "songreflist/#", 39);
        a.addURI("com.duomi.android.datastore", "songreflist", 38);
        a.addURI("com.duomi.android.datastore", "song a  join songreflist b on a._id=b.songid left join addition c on a.duomisongid=c.s_songid", 43);
        a.addURI("com.duomi.android.datastore", "userreflist/#", 44);
        a.addURI("com.duomi.android.datastore", "userreflist/ref/#", 45);
        a.addURI("com.duomi.android.datastore", "songtemp/#", 47);
        a.addURI("com.duomi.android.datastore", "songtemp", 46);
        a.addURI("com.duomi.android.datastore", "song/bulk/start", 62);
        a.addURI("com.duomi.android.datastore", "song/bulk/end", 63);
        a.addURI("com.duomi.android.datastore", "song/bulk", 48);
        a.addURI("com.duomi.android.datastore", "inc_singer", 49);
        a.addURI("com.duomi.android.datastore", "songlist/user_ref_list", 50);
        a.addURI("com.duomi.android.datastore", "currentsongs", 52);
        a.addURI("com.duomi.android.datastore", "currentsongs/#", 53);
        a.addURI("com.duomi.android.datastore", "songlist/insert_songlist_user_ref_list", 51);
        a.addURI("com.duomi.android.datastore", "syncsong", 54);
        a.addURI("com.duomi.android.datastore", "syncsong/merge/#", 55);
        a.addURI("com.duomi.android.datastore", "syncsong/clear/#", 56);
        a.addURI("com.duomi.android.datastore", "syncsong/unchange/#", 57);
        a.addURI("com.duomi.android.datastore", "syncsong/#", 54);
        a.addURI("com.duomi.android.datastore", "songreflist/songrefid/#/#", 59);
        a.addURI("com.duomi.android.datastore", "songlist/songlist_user_ref_list", 60);
        a.addURI("com.duomi.android.datastore", "songlisttemp", 61);
        a.addURI("com.duomi.android.datastore", "tempdeletelistpos/#/#", 64);
        a.addURI("com.duomi.android.datastore", "tempdeletelistpos/add/#/#", 65);
        a.addURI("com.duomi.android.datastore", "login_account", 66);
    }

    private int a(Uri uri, ContentValues contentValues, String str, String[] strArr, String str2, String str3) {
        return this.c.update(str2, contentValues, str3 + "=" + uri.getPathSegments().get(1) + (!TextUtils.isEmpty(str) ? " AND (" + str + ')' : ""), strArr);
    }

    private int a(Uri uri, String str, String[] strArr, String str2, String str3) {
        return this.c.delete(str2, str3 + "=" + uri.getPathSegments().get(1) + (!TextUtils.isEmpty(str) ? " AND (" + str + ')' : ""), strArr);
    }

    private Uri a(ContentValues contentValues, String str, Uri uri) {
        long insert = contentValues != null ? this.c.insert(str, null, contentValues) : 0;
        if (insert <= 0) {
            return null;
        }
        Uri withAppendedId = ContentUris.withAppendedId(uri, insert);
        getContext().getContentResolver().notifyChange(withAppendedId, null);
        return withAppendedId;
    }

    private void a(StringBuffer stringBuffer, int i, ContentValues contentValues) {
        StringBuffer stringBuffer2;
        this.c.execSQL("update songlist set picurl=?,version=?,desc=?,lid=?,name=?,ismodify=0  where _id=" + i, new String[]{en.a(contentValues.get("limg")), en.a(contentValues.get("lver")), en.a(contentValues.get("ldesc")), en.a(contentValues.get("lid")), en.a(contentValues.get("lnm"))});
        if (stringBuffer == null) {
            stringBuffer2 = new StringBuffer();
        } else {
            stringBuffer.delete(0, stringBuffer.length());
            stringBuffer2 = stringBuffer;
        }
        stringBuffer2.append("create table if not exists ").append("syncsong").append(" (").append("_id").append("  INTEGER primary key autoincrement,").append("ldesc").append(" TEXT,").append("lid").append(" TEXT,").append("limg").append(" TEXT,").append("lnm").append(" TEXT,").append("lver").append(" TEXT,").append("album").append(" TEXT,").append("snum").append(" TEXT,").append("hurl").append(" TEXT,").append("singer").append(" TEXT,").append("style").append(" TEXT,").append("url").append(" TEXT,").append("sid").append(" TEXT,").append("sdesc").append(" TEXT,").append("sname").append(" TEXT,").append("sver").append(" TEXT,").append("durl").append(" TEXT)");
        this.c.execSQL(stringBuffer2.toString());
        if (stringBuffer2 == null) {
            stringBuffer2 = new StringBuffer();
        } else {
            stringBuffer2.delete(0, stringBuffer2.length());
        }
        stringBuffer2.append("insert into syncsong(ldesc,lid,limg,lnm,lver,album,snum,hurl,singer,style,url,sid,sdesc,sname,sver,durl) ");
        stringBuffer2.append("select c.desc,c.lid,c.picurl,c.name,c.version,b.albumname,'0',b.auditionurl,b.singer,b.musicstyle,b.lurl,b.duomisongid,b.durationtime,b.name,'" + bn.a().d() + "',b.downloadurl from songreflist a join song b on a.songid=b._id " + "join songlist c on a.listid=c._id where a.listid=" + i);
        this.c.execSQL(stringBuffer2.toString());
    }

    public int a(String str) {
        ad.b("DuomiDataProvider", ">>begin bulk insert>>>>>>");
        this.c.beginTransaction();
        try {
            this.c.execSQL(str);
            ad.b("DuomiDataProvider", ">>sql:" + str);
            this.c.setTransactionSuccessful();
            return 0;
        } finally {
            this.c.endTransaction();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* JADX WARNING: Removed duplicated region for block: B:101:0x0b17 A[Catch:{ all -> 0x0c76, all -> 0x0c7e }] */
    /* JADX WARNING: Removed duplicated region for block: B:175:0x105b  */
    /* JADX WARNING: Removed duplicated region for block: B:201:0x1255  */
    /* JADX WARNING: Removed duplicated region for block: B:237:0x1379  */
    /* JADX WARNING: Removed duplicated region for block: B:248:0x13e4  */
    /* JADX WARNING: Removed duplicated region for block: B:298:0x150e  */
    /* JADX WARNING: Removed duplicated region for block: B:349:0x0f29 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:353:0x1258 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:97:0x0b0e A[SYNTHETIC, Splitter:B:97:0x0b0e] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int bulkInsert(android.net.Uri r14, android.content.ContentValues[] r15) {
        /*
            r13 = this;
            java.lang.String r0 = "DuomiDataProvider"
            java.lang.String r1 = ">>begin bulk insert>>>>>>"
            defpackage.ad.b(r0, r1)
            r1 = 0
            java.lang.StringBuffer r2 = new java.lang.StringBuffer
            r2.<init>()
            android.content.UriMatcher r0 = com.duomi.app.datastore.DuomiDataProvider.a
            int r0 = r0.match(r14)
            switch(r0) {
                case 48: goto L_0x004a;
                case 49: goto L_0x0016;
                case 50: goto L_0x0016;
                case 51: goto L_0x0016;
                case 52: goto L_0x0016;
                case 53: goto L_0x0016;
                case 54: goto L_0x034f;
                case 55: goto L_0x04aa;
                case 56: goto L_0x0ead;
                case 57: goto L_0x11d5;
                case 58: goto L_0x0016;
                case 59: goto L_0x0016;
                case 60: goto L_0x0016;
                case 61: goto L_0x0016;
                case 62: goto L_0x002c;
                case 63: goto L_0x00c8;
                case 64: goto L_0x12aa;
                case 65: goto L_0x1457;
                default: goto L_0x0016;
            }
        L_0x0016:
            android.database.sqlite.SQLiteDatabase r0 = r13.c
            r0.beginTransaction()
            int r0 = r15.length     // Catch:{ all -> 0x15d2 }
            r2 = 0
            r12 = r2
            r2 = r1
            r1 = r12
        L_0x0020:
            if (r1 >= r0) goto L_0x15d9
            r3 = r15[r1]     // Catch:{ all -> 0x15d2 }
            r13.insert(r14, r3)     // Catch:{ Exception -> 0x15cc }
            int r2 = r2 + 1
        L_0x0029:
            int r1 = r1 + 1
            goto L_0x0020
        L_0x002c:
            android.database.sqlite.SQLiteDatabase r0 = r13.c
            java.lang.String r2 = "DROP TABLE IF EXISTS songtemp"
            r0.execSQL(r2)
            android.database.sqlite.SQLiteDatabase r0 = r13.c
            java.lang.String r2 = "CREATE TABLE songtemp AS SELECT * FROM song WHERE 1=0"
            r0.execSQL(r2)
            android.database.sqlite.SQLiteDatabase r0 = r13.c
            java.lang.String r2 = "create index idx_path_songtemp on songtemp(path)"
            r0.execSQL(r2)
            android.database.sqlite.SQLiteDatabase r0 = r13.c
            java.lang.String r2 = "create index idx_disp_songtemp on songtemp(disp)"
            r0.execSQL(r2)
            r0 = r1
        L_0x0049:
            return r0
        L_0x004a:
            java.lang.String r0 = "DuomiDataProvider"
            java.lang.String r2 = ">>scan localsong bulk insert>>>>>>"
            defpackage.ad.b(r0, r2)
            if (r15 == 0) goto L_0x0057
            int r0 = r15.length
            r2 = 1
            if (r0 >= r2) goto L_0x006e
        L_0x0057:
            android.database.sqlite.SQLiteDatabase r0 = r13.c
            java.lang.String r2 = "delete from songreflist where listid in(select _id from songlist where type in(1,4,5,6) or parent in(1,2,3,4))"
            r0.execSQL(r2)
            android.database.sqlite.SQLiteDatabase r0 = r13.c
            java.lang.String r2 = "delete from songlist where parent in (1,2,3,4)"
            r0.execSQL(r2)
            android.database.sqlite.SQLiteDatabase r0 = r13.c
            java.lang.String r2 = "delete from song where islocal=1 and (auditionurl is null or auditionurl='')"
            r0.execSQL(r2)
            r0 = r1
            goto L_0x0049
        L_0x006e:
            android.database.sqlite.SQLiteDatabase r0 = r13.c
            r0.beginTransaction()
            int r0 = r15.length     // Catch:{ all -> 0x008f }
            r2 = 0
            r12 = r2
            r2 = r1
            r1 = r12
        L_0x0078:
            if (r1 >= r0) goto L_0x0096
            r3 = r15[r1]     // Catch:{ all -> 0x008f }
            android.database.sqlite.SQLiteDatabase r4 = r13.c     // Catch:{ Exception -> 0x008a }
            java.lang.String r5 = "songtemp"
            java.lang.String r6 = ""
            r4.insert(r5, r6, r3)     // Catch:{ Exception -> 0x008a }
            int r2 = r2 + 1
        L_0x0087:
            int r1 = r1 + 1
            goto L_0x0078
        L_0x008a:
            r3 = move-exception
            r3.printStackTrace()     // Catch:{ all -> 0x008f }
            goto L_0x0087
        L_0x008f:
            r0 = move-exception
            android.database.sqlite.SQLiteDatabase r1 = r13.c
            r1.endTransaction()
            throw r0
        L_0x0096:
            java.lang.String r0 = "DuomiDataProvider"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x008f }
            r1.<init>()     // Catch:{ all -> 0x008f }
            java.lang.String r3 = ">>bulk insert record ("
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ all -> 0x008f }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x008f }
            java.lang.String r3 = ") rows:"
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ all -> 0x008f }
            java.lang.String r3 = r14.toString()     // Catch:{ all -> 0x008f }
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ all -> 0x008f }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x008f }
            defpackage.ad.b(r0, r1)     // Catch:{ all -> 0x008f }
            android.database.sqlite.SQLiteDatabase r0 = r13.c     // Catch:{ all -> 0x008f }
            r0.setTransactionSuccessful()     // Catch:{ all -> 0x008f }
            android.database.sqlite.SQLiteDatabase r0 = r13.c
            r0.endTransaction()
            r0 = r2
            goto L_0x0049
        L_0x00c8:
            if (r2 == 0) goto L_0x00d2
            r0 = 0
            int r3 = r2.length()
            r2.delete(r0, r3)
        L_0x00d2:
            java.lang.String r0 = "insert into song"
            java.lang.StringBuffer r0 = r2.append(r0)
            java.lang.String r3 = " ("
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = "albumid"
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = ","
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = "albumname"
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = ","
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = "auditionurl"
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = ","
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = "downloadurl"
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = ","
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = "durationtime"
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = ","
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = "disp"
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = ","
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = "islocal"
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = ","
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = "likemusicflag"
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = ","
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = "lyricpath"
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = ","
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = "mood"
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = ","
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = "name"
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = ","
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = "path"
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = ","
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = "popindex"
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = ","
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = "lurl"
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = ","
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = "duomisongid"
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = ","
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = "singer"
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = ","
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = "size"
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = ","
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = "musicstyle"
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = ","
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = "type"
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = " )"
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = " select "
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = "albumid"
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = ","
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = "albumname"
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = ","
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = "auditionurl"
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = ","
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = "downloadurl"
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = ","
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = "durationtime"
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = ","
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = "disp"
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = ","
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = "islocal"
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = ","
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = "likemusicflag"
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = ","
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = "lyricpath"
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = ","
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = "mood"
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = ","
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = "name"
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = ","
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = "path"
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = ","
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = "popindex"
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = ","
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = "lurl"
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = ","
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = "duomisongid"
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = ","
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = "singer"
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = ","
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = "size"
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = ","
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = "musicstyle"
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = ","
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = "type"
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = " from "
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = "songtemp"
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = " where "
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = "disp"
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = " not in("
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = "select "
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = "disp"
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = " from "
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = "song"
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = ")"
            r0.append(r3)
            java.lang.String r0 = "DuomiDataProvider"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "execute insert sql:"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r4 = r2.toString()
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            defpackage.ad.b(r0, r3)
            android.database.sqlite.SQLiteDatabase r0 = r13.c
            java.lang.String r2 = r2.toString()
            r0.execSQL(r2)
            android.database.sqlite.SQLiteDatabase r0 = r13.c
            java.lang.String r2 = "delete  from song where _id in (select _id from song where not exists(select disp from songtemp where songtemp.disp=song.disp) and (duomisongid='' or duomisongid is null))"
            r0.execSQL(r2)
            android.database.sqlite.SQLiteDatabase r0 = r13.c
            java.lang.String r2 = "delete from songreflist where  listid in(select _id from songlist where type in(1,4,5,6) or parent in(1,2,3,4))"
            r0.execSQL(r2)
            android.database.sqlite.SQLiteDatabase r0 = r13.c
            java.lang.String r2 = "delete from songlist where _id not in(1,2,3,4) and parent in (select _id from songlist where type in(1,4,5,6))"
            r0.execSQL(r2)
            android.database.sqlite.SQLiteDatabase r0 = r13.c
            java.lang.String r2 = "insert into songreflist(songid,listid) select _id,1 from song where disp in(select disp from songtemp)"
            r0.execSQL(r2)
            android.database.sqlite.SQLiteDatabase r0 = r13.c
            java.lang.String r2 = "insert into songlist(name,state,picurl,parent,lid,version,desc,ismodify,type) select distinct(singer),1,'',2,'','0','',0,4 from song where singer<>'' and disp in(select disp from songtemp)"
            r0.execSQL(r2)
            android.database.sqlite.SQLiteDatabase r0 = r13.c
            java.lang.String r2 = "insert into songreflist(songid,listid) select song._id,songlist._id from song join songlist on song.singer=songlist.name where songlist.type=4 and parent=2 and song.disp in(select disp from songtemp)"
            r0.execSQL(r2)
            android.database.sqlite.SQLiteDatabase r0 = r13.c
            java.lang.String r2 = "insert into songlist(name,state,picurl,parent,lid,version,desc,ismodify,type) select distinct(albumname),1,'',3,'','0','',0,5 from song where albumname<>'' and albumname is not null and disp in(select disp from songtemp)"
            r0.execSQL(r2)
            android.database.sqlite.SQLiteDatabase r0 = r13.c
            java.lang.String r2 = "insert into songreflist(songid,listid) select song._id,songlist._id from song join songlist on song.albumname=songlist.name where songlist.type=5 and parent=3 and song.disp in(select disp from songtemp)"
            r0.execSQL(r2)
            android.database.sqlite.SQLiteDatabase r0 = r13.c
            java.lang.String r2 = "insert into songlist(name,state,picurl,parent,lid,version,desc,ismodify,type) select distinct(path),1,'',4,'','0','',0,6 from song where path<>'' and disp in(select disp from songtemp)"
            r0.execSQL(r2)
            android.database.sqlite.SQLiteDatabase r0 = r13.c
            java.lang.String r2 = "insert into songreflist(songid,listid) select song._id,songlist._id from song join songlist on song.path=songlist.name where songlist.type=6 and parent=4 and song.disp in(select disp from songtemp)"
            r0.execSQL(r2)
            r0 = r1
            goto L_0x0049
        L_0x034f:
            java.lang.String r0 = "DuomiDataProvider"
            java.lang.String r1 = "begin sync db----------------"
            defpackage.ad.b(r0, r1)
            if (r2 != 0) goto L_0x0460
            java.lang.StringBuffer r0 = new java.lang.StringBuffer
            r0.<init>()
        L_0x035d:
            java.lang.String r1 = "create table if not exists "
            java.lang.StringBuffer r1 = r0.append(r1)
            java.lang.String r2 = "syncsong"
            java.lang.StringBuffer r1 = r1.append(r2)
            java.lang.String r2 = " ("
            java.lang.StringBuffer r1 = r1.append(r2)
            java.lang.String r2 = "_id"
            java.lang.StringBuffer r1 = r1.append(r2)
            java.lang.String r2 = "  INTEGER primary key autoincrement,"
            java.lang.StringBuffer r1 = r1.append(r2)
            java.lang.String r2 = "ldesc"
            java.lang.StringBuffer r1 = r1.append(r2)
            java.lang.String r2 = " TEXT,"
            java.lang.StringBuffer r1 = r1.append(r2)
            java.lang.String r2 = "lid"
            java.lang.StringBuffer r1 = r1.append(r2)
            java.lang.String r2 = " TEXT,"
            java.lang.StringBuffer r1 = r1.append(r2)
            java.lang.String r2 = "limg"
            java.lang.StringBuffer r1 = r1.append(r2)
            java.lang.String r2 = " TEXT,"
            java.lang.StringBuffer r1 = r1.append(r2)
            java.lang.String r2 = "lnm"
            java.lang.StringBuffer r1 = r1.append(r2)
            java.lang.String r2 = " TEXT,"
            java.lang.StringBuffer r1 = r1.append(r2)
            java.lang.String r2 = "lver"
            java.lang.StringBuffer r1 = r1.append(r2)
            java.lang.String r2 = " TEXT,"
            java.lang.StringBuffer r1 = r1.append(r2)
            java.lang.String r2 = "album"
            java.lang.StringBuffer r1 = r1.append(r2)
            java.lang.String r2 = " TEXT,"
            java.lang.StringBuffer r1 = r1.append(r2)
            java.lang.String r2 = "snum"
            java.lang.StringBuffer r1 = r1.append(r2)
            java.lang.String r2 = " TEXT,"
            java.lang.StringBuffer r1 = r1.append(r2)
            java.lang.String r2 = "hurl"
            java.lang.StringBuffer r1 = r1.append(r2)
            java.lang.String r2 = " TEXT,"
            java.lang.StringBuffer r1 = r1.append(r2)
            java.lang.String r2 = "singer"
            java.lang.StringBuffer r1 = r1.append(r2)
            java.lang.String r2 = " TEXT,"
            java.lang.StringBuffer r1 = r1.append(r2)
            java.lang.String r2 = "style"
            java.lang.StringBuffer r1 = r1.append(r2)
            java.lang.String r2 = " TEXT,"
            java.lang.StringBuffer r1 = r1.append(r2)
            java.lang.String r2 = "url"
            java.lang.StringBuffer r1 = r1.append(r2)
            java.lang.String r2 = " TEXT,"
            java.lang.StringBuffer r1 = r1.append(r2)
            java.lang.String r2 = "sid"
            java.lang.StringBuffer r1 = r1.append(r2)
            java.lang.String r2 = " TEXT,"
            java.lang.StringBuffer r1 = r1.append(r2)
            java.lang.String r2 = "sdesc"
            java.lang.StringBuffer r1 = r1.append(r2)
            java.lang.String r2 = " INTEGER,"
            java.lang.StringBuffer r1 = r1.append(r2)
            java.lang.String r2 = "sname"
            java.lang.StringBuffer r1 = r1.append(r2)
            java.lang.String r2 = " TEXT,"
            java.lang.StringBuffer r1 = r1.append(r2)
            java.lang.String r2 = "sver"
            java.lang.StringBuffer r1 = r1.append(r2)
            java.lang.String r2 = " TEXT,"
            java.lang.StringBuffer r1 = r1.append(r2)
            java.lang.String r2 = "durl"
            java.lang.StringBuffer r1 = r1.append(r2)
            java.lang.String r2 = " TEXT)"
            r1.append(r2)
            android.database.sqlite.SQLiteDatabase r1 = r13.c
            java.lang.String r0 = r0.toString()
            r1.execSQL(r0)
            android.database.sqlite.SQLiteDatabase r0 = r13.c
            r0.beginTransaction()
            r0 = 0
            int r1 = r15.length     // Catch:{ all -> 0x0470 }
            r2 = 0
            r12 = r2
            r2 = r0
            r0 = r12
        L_0x044e:
            if (r0 >= r1) goto L_0x0477
            r3 = r15[r0]     // Catch:{ all -> 0x0470 }
            android.database.sqlite.SQLiteDatabase r4 = r13.c     // Catch:{ Exception -> 0x046b }
            java.lang.String r5 = "syncsong"
            java.lang.String r6 = ""
            r4.insert(r5, r6, r3)     // Catch:{ Exception -> 0x046b }
            int r2 = r2 + 1
        L_0x045d:
            int r0 = r0 + 1
            goto L_0x044e
        L_0x0460:
            r0 = 0
            int r1 = r2.length()
            r2.delete(r0, r1)
            r0 = r2
            goto L_0x035d
        L_0x046b:
            r3 = move-exception
            r3.printStackTrace()     // Catch:{ all -> 0x0470 }
            goto L_0x045d
        L_0x0470:
            r0 = move-exception
            android.database.sqlite.SQLiteDatabase r1 = r13.c
            r1.endTransaction()
            throw r0
        L_0x0477:
            java.lang.String r0 = "DuomiDataProvider"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0470 }
            r1.<init>()     // Catch:{ all -> 0x0470 }
            java.lang.String r3 = ">>bulk insert record ("
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ all -> 0x0470 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0470 }
            java.lang.String r3 = ") rows:"
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ all -> 0x0470 }
            java.lang.String r3 = r14.toString()     // Catch:{ all -> 0x0470 }
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ all -> 0x0470 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0470 }
            defpackage.ad.b(r0, r1)     // Catch:{ all -> 0x0470 }
            android.database.sqlite.SQLiteDatabase r0 = r13.c     // Catch:{ all -> 0x0470 }
            r0.setTransactionSuccessful()     // Catch:{ all -> 0x0470 }
            android.database.sqlite.SQLiteDatabase r0 = r13.c
            r0.endTransaction()
            r0 = r2
            goto L_0x0049
        L_0x04aa:
            java.lang.String r0 = r14.getLastPathSegment()
            java.lang.String r3 = "1"
            boolean r3 = r3.equals(r0)
            if (r3 != 0) goto L_0x04b8
            java.lang.String r0 = "0"
        L_0x04b8:
            android.database.sqlite.SQLiteDatabase r3 = r13.c
            java.lang.String r4 = "drop table if exists syncsongtmp1"
            r3.execSQL(r4)
            android.database.sqlite.SQLiteDatabase r3 = r13.c
            java.lang.String r4 = "drop table if exists syncsongtmp2"
            r3.execSQL(r4)
            android.database.sqlite.SQLiteDatabase r3 = r13.c
            java.lang.String r4 = "create table syncsongtmp1 as select syncsong.* from syncsong group by sid having count(sid)>1"
            r3.execSQL(r4)
            android.database.sqlite.SQLiteDatabase r3 = r13.c
            java.lang.String r4 = "create table syncsongtmp2 as select * from syncsong where not exists(select sid from syncsongtmp1 where syncsongtmp1.sid=syncsong.sid)"
            r3.execSQL(r4)
            if (r2 == 0) goto L_0x04de
            r3 = 0
            int r4 = r2.length()
            r2.delete(r3, r4)
        L_0x04de:
            java.lang.String r3 = "insert into song"
            java.lang.StringBuffer r3 = r2.append(r3)
            java.lang.String r4 = " ("
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "albumid"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "albumname"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "auditionurl"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "downloadurl"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "durationtime"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "disp"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "islocal"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "likemusicflag"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "lyricpath"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "mood"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "name"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "path"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "popindex"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "lurl"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "duomisongid"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "singer"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "size"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "musicstyle"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "type"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = " )"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = " select "
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "''"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "album"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "hurl"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "durl"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "sdesc"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "''"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "'0'"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "0"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "''"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "''"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "sname"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "''"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "0"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "url"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "sid"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "singer"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "0"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "''"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "''"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = " from "
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = " syncsongtmp1 "
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = " where not exists("
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "select "
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "_id"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = " from "
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "song"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = " where "
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "song"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "."
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "duomisongid"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "="
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = " syncsongtmp1 "
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ".sid)"
            r3.append(r4)
            android.database.sqlite.SQLiteDatabase r3 = r13.c
            java.lang.String r4 = r2.toString()
            r3.execSQL(r4)
            if (r2 == 0) goto L_0x0718
            r3 = 0
            int r4 = r2.length()
            r2.delete(r3, r4)
        L_0x0718:
            java.lang.String r3 = "insert into song"
            java.lang.StringBuffer r3 = r2.append(r3)
            java.lang.String r4 = " ("
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "albumid"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "albumname"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "auditionurl"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "downloadurl"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "durationtime"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "disp"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "islocal"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "likemusicflag"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "lyricpath"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "mood"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "name"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "path"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "popindex"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "lurl"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "duomisongid"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "singer"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "size"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "musicstyle"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "type"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = " )"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = " select "
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "''"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "album"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "hurl"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "durl"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "sdesc"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "''"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "'0'"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "0"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "''"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "''"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "sname"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "''"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "0"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "url"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "sid"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "singer"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "0"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "''"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ","
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "''"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = " from "
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = " syncsongtmp2 "
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = " where not exists("
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "select "
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "_id"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = " from "
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "song"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = " where "
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "song"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "."
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "duomisongid"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "="
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = " syncsongtmp2 "
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ".sid)"
            r3.append(r4)
            android.database.sqlite.SQLiteDatabase r3 = r13.c
            java.lang.String r4 = r2.toString()
            r3.execSQL(r4)
            java.lang.String r3 = "0"
            boolean r3 = r3.equals(r0)
            if (r3 == 0) goto L_0x0c85
            java.lang.String r3 = "DuomiDataProvider"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "this is all sync type:>>>"
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r0 = r4.append(r0)
            java.lang.String r0 = r0.toString()
            defpackage.ad.b(r3, r0)
            if (r2 == 0) goto L_0x0972
            r0 = 0
            int r3 = r2.length()
            r2.delete(r0, r3)
        L_0x0972:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r3 = "delete from songreflist where listid in(select a.listid from userreflist a join songlist b on a.listid=b._id join songreflist c on c.listid=b._id where a.uid='"
            java.lang.StringBuilder r0 = r0.append(r3)
            bn r3 = defpackage.bn.a()
            java.lang.String r3 = r3.h()
            java.lang.StringBuilder r0 = r0.append(r3)
            java.lang.String r3 = "' and b.type in(2,7)) and songid in(select x._id from song x join songreflist y on x._id=y.songid where x.duomisongid is not null and x.duomisongid<>'' )"
            java.lang.StringBuilder r0 = r0.append(r3)
            java.lang.String r0 = r0.toString()
            r2.append(r0)
            java.lang.String r0 = "DuomiDataProvider"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "songreflist execute:"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r4 = r2.toString()
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            defpackage.ad.b(r0, r3)
            android.database.sqlite.SQLiteDatabase r0 = r13.c
            java.lang.String r2 = r2.toString()
            r0.execSQL(r2)
            android.database.sqlite.SQLiteDatabase r0 = r13.c
            java.lang.String r2 = "select distinct(lnm),lid,lver,ldesc,limg from syncsong where lid='1'"
            r3 = 0
            android.database.Cursor r0 = r0.rawQuery(r2, r3)
            java.lang.String r2 = ""
            java.lang.String r2 = ""
            java.lang.String r2 = ""
            java.lang.String r2 = ""
            java.lang.String r2 = ""
            if (r0 == 0) goto L_0x0a49
            boolean r2 = r0.moveToFirst()     // Catch:{ all -> 0x0b4d }
            if (r2 == 0) goto L_0x0a49
            r2 = 0
            r0.getString(r2)     // Catch:{ all -> 0x0b4d }
            r2 = 1
            java.lang.String r2 = r0.getString(r2)     // Catch:{ all -> 0x0b4d }
            r3 = 2
            java.lang.String r3 = r0.getString(r3)     // Catch:{ all -> 0x0b4d }
            r4 = 3
            java.lang.String r4 = r0.getString(r4)     // Catch:{ all -> 0x0b4d }
            r5 = 4
            java.lang.String r5 = r0.getString(r5)     // Catch:{ all -> 0x0b4d }
            android.database.sqlite.SQLiteDatabase r6 = r13.c     // Catch:{ all -> 0x0b4d }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x0b4d }
            r7.<init>()     // Catch:{ all -> 0x0b4d }
            java.lang.String r8 = "update songlist set picurl=?,version=?,desc=?,lid=?,ismodify=0 where type=2 and exists(select _id from userreflist where uid='"
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ all -> 0x0b4d }
            bn r8 = defpackage.bn.a()     // Catch:{ all -> 0x0b4d }
            java.lang.String r8 = r8.h()     // Catch:{ all -> 0x0b4d }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ all -> 0x0b4d }
            java.lang.String r8 = "' and userreflist.listid=songlist._id)"
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ all -> 0x0b4d }
            java.lang.String r7 = r7.toString()     // Catch:{ all -> 0x0b4d }
            r8 = 4
            java.lang.String[] r8 = new java.lang.String[r8]     // Catch:{ all -> 0x0b4d }
            r9 = 0
            r8[r9] = r5     // Catch:{ all -> 0x0b4d }
            r5 = 1
            r8[r5] = r3     // Catch:{ all -> 0x0b4d }
            r3 = 2
            r8[r3] = r4     // Catch:{ all -> 0x0b4d }
            r3 = 3
            r8[r3] = r2     // Catch:{ all -> 0x0b4d }
            r6.execSQL(r7, r8)     // Catch:{ all -> 0x0b4d }
            android.database.sqlite.SQLiteDatabase r2 = r13.c     // Catch:{ all -> 0x0b4d }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0b4d }
            r3.<init>()     // Catch:{ all -> 0x0b4d }
            java.lang.String r4 = "insert into songreflist(songid,listid) select distinct(song._id),(select listid from userreflist join songlist on userreflist.listid=songlist._id  where userreflist.uid='"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0b4d }
            bn r4 = defpackage.bn.a()     // Catch:{ all -> 0x0b4d }
            java.lang.String r4 = r4.h()     // Catch:{ all -> 0x0b4d }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0b4d }
            java.lang.String r4 = "' and songlist.type=2 limit 0,1) from syncsong join song on syncsong.sid=song.duomisongid where syncsong.lid=1"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0b4d }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0b4d }
            r2.execSQL(r3)     // Catch:{ all -> 0x0b4d }
        L_0x0a49:
            if (r0 == 0) goto L_0x0a4e
            r0.close()
        L_0x0a4e:
            android.database.sqlite.SQLiteDatabase r0 = r13.c
            java.lang.String r2 = "select distinct(lnm),lid,lver,ldesc,limg from syncsong where lid<>'1';"
            r3 = 0
            android.database.Cursor r0 = r0.rawQuery(r2, r3)
            if (r0 == 0) goto L_0x0b45
            boolean r2 = r0.moveToFirst()     // Catch:{ all -> 0x0c7e }
            if (r2 == 0) goto L_0x0b45
        L_0x0a5f:
            r2 = 0
            java.lang.String r2 = r0.getString(r2)     // Catch:{ all -> 0x0c7e }
            r3 = 1
            java.lang.String r3 = r0.getString(r3)     // Catch:{ all -> 0x0c7e }
            r4 = 2
            java.lang.String r4 = r0.getString(r4)     // Catch:{ all -> 0x0c7e }
            r5 = 3
            java.lang.String r5 = r0.getString(r5)     // Catch:{ all -> 0x0c7e }
            r6 = 4
            java.lang.String r6 = r0.getString(r6)     // Catch:{ all -> 0x0c7e }
            android.database.sqlite.SQLiteDatabase r7 = r13.c     // Catch:{ all -> 0x0c7e }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ all -> 0x0c7e }
            r8.<init>()     // Catch:{ all -> 0x0c7e }
            java.lang.String r9 = "select a._id from songlist a join userreflist b on a._id=b.listid where a.type=7 and b.uid='"
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ all -> 0x0c7e }
            bn r9 = defpackage.bn.a()     // Catch:{ all -> 0x0c7e }
            java.lang.String r9 = r9.h()     // Catch:{ all -> 0x0c7e }
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ all -> 0x0c7e }
            java.lang.String r9 = "' and a.lid='"
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ all -> 0x0c7e }
            java.lang.StringBuilder r8 = r8.append(r3)     // Catch:{ all -> 0x0c7e }
            java.lang.String r9 = "'"
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ all -> 0x0c7e }
            java.lang.String r8 = r8.toString()     // Catch:{ all -> 0x0c7e }
            r9 = 0
            android.database.Cursor r7 = r7.rawQuery(r8, r9)     // Catch:{ all -> 0x0c7e }
            if (r7 == 0) goto L_0x0b54
            boolean r8 = r7.moveToNext()     // Catch:{ all -> 0x0c76 }
            if (r8 == 0) goto L_0x0b54
            r8 = 0
            java.lang.String r8 = r7.getString(r8)     // Catch:{ all -> 0x0c76 }
            android.content.ContentValues r9 = new android.content.ContentValues     // Catch:{ all -> 0x0c76 }
            r9.<init>()     // Catch:{ all -> 0x0c76 }
            java.lang.String r10 = "name"
            r9.put(r10, r2)     // Catch:{ all -> 0x0c76 }
            java.lang.String r2 = "state"
            r10 = 1
            java.lang.Integer r10 = java.lang.Integer.valueOf(r10)     // Catch:{ all -> 0x0c76 }
            r9.put(r2, r10)     // Catch:{ all -> 0x0c76 }
            java.lang.String r2 = "picurl"
            r9.put(r2, r6)     // Catch:{ all -> 0x0c76 }
            java.lang.String r2 = "parent"
            r6 = 0
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ all -> 0x0c76 }
            r9.put(r2, r6)     // Catch:{ all -> 0x0c76 }
            java.lang.String r2 = "version"
            r9.put(r2, r4)     // Catch:{ all -> 0x0c76 }
            java.lang.String r2 = "desc"
            r9.put(r2, r5)     // Catch:{ all -> 0x0c76 }
            java.lang.String r2 = "ismodify"
            r4 = 0
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ all -> 0x0c76 }
            r9.put(r2, r4)     // Catch:{ all -> 0x0c76 }
            java.lang.String r2 = "type"
            r4 = 7
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ all -> 0x0c76 }
            r9.put(r2, r4)     // Catch:{ all -> 0x0c76 }
            android.database.sqlite.SQLiteDatabase r2 = r13.c     // Catch:{ all -> 0x0c76 }
            java.lang.String r4 = "songlist"
            java.lang.String r5 = " _id=? "
            r6 = 1
            java.lang.String[] r6 = new java.lang.String[r6]     // Catch:{ all -> 0x0c76 }
            r10 = 0
            r6[r10] = r8     // Catch:{ all -> 0x0c76 }
            r2.update(r4, r9, r5, r6)     // Catch:{ all -> 0x0c76 }
            long r4 = java.lang.Long.parseLong(r8)     // Catch:{ all -> 0x0c76 }
            r2 = r7
        L_0x0b0c:
            if (r2 == 0) goto L_0x0b11
            r2.close()     // Catch:{ all -> 0x0c7e }
        L_0x0b11:
            r6 = 0
            int r2 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r2 <= 0) goto L_0x0b3f
            android.database.sqlite.SQLiteDatabase r2 = r13.c     // Catch:{ all -> 0x0c7e }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x0c7e }
            r6.<init>()     // Catch:{ all -> 0x0c7e }
            java.lang.String r7 = "insert into songreflist(songid,listid) select distinct(song._id),"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ all -> 0x0c7e }
            java.lang.StringBuilder r4 = r6.append(r4)     // Catch:{ all -> 0x0c7e }
            java.lang.String r5 = " from song join syncsong on song.duomisongid=syncsong.sid where syncsong.lid='"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0c7e }
            java.lang.StringBuilder r3 = r4.append(r3)     // Catch:{ all -> 0x0c7e }
            java.lang.String r4 = "'"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0c7e }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0c7e }
            r2.execSQL(r3)     // Catch:{ all -> 0x0c7e }
        L_0x0b3f:
            boolean r2 = r0.moveToNext()     // Catch:{ all -> 0x0c7e }
            if (r2 != 0) goto L_0x0a5f
        L_0x0b45:
            if (r0 == 0) goto L_0x0b4a
            r0.close()
        L_0x0b4a:
            r0 = r1
            goto L_0x0049
        L_0x0b4d:
            r1 = move-exception
            if (r0 == 0) goto L_0x0b53
            r0.close()
        L_0x0b53:
            throw r1
        L_0x0b54:
            r7.close()     // Catch:{ all -> 0x0c76 }
            android.database.sqlite.SQLiteDatabase r8 = r13.c     // Catch:{ all -> 0x0c76 }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ all -> 0x0c76 }
            r9.<init>()     // Catch:{ all -> 0x0c76 }
            java.lang.String r10 = "select a._id from songlist a join userreflist b on a._id=b.listid where a.type=7 and b.uid='"
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ all -> 0x0c76 }
            bn r10 = defpackage.bn.a()     // Catch:{ all -> 0x0c76 }
            java.lang.String r10 = r10.h()     // Catch:{ all -> 0x0c76 }
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ all -> 0x0c76 }
            java.lang.String r10 = "' and a.name='"
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ all -> 0x0c76 }
            java.lang.StringBuilder r9 = r9.append(r2)     // Catch:{ all -> 0x0c76 }
            java.lang.String r10 = "'"
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ all -> 0x0c76 }
            java.lang.String r9 = r9.toString()     // Catch:{ all -> 0x0c76 }
            r10 = 0
            android.database.Cursor r7 = r8.rawQuery(r9, r10)     // Catch:{ all -> 0x0c76 }
            if (r7 == 0) goto L_0x0bf2
            boolean r8 = r7.moveToNext()     // Catch:{ all -> 0x0c76 }
            if (r8 == 0) goto L_0x0bf2
            r8 = 0
            java.lang.String r8 = r7.getString(r8)     // Catch:{ all -> 0x0c76 }
            android.content.ContentValues r9 = new android.content.ContentValues     // Catch:{ all -> 0x0c76 }
            r9.<init>()     // Catch:{ all -> 0x0c76 }
            java.lang.String r10 = "name"
            r9.put(r10, r2)     // Catch:{ all -> 0x0c76 }
            java.lang.String r2 = "state"
            r10 = 1
            java.lang.Integer r10 = java.lang.Integer.valueOf(r10)     // Catch:{ all -> 0x0c76 }
            r9.put(r2, r10)     // Catch:{ all -> 0x0c76 }
            java.lang.String r2 = "picurl"
            r9.put(r2, r6)     // Catch:{ all -> 0x0c76 }
            java.lang.String r2 = "parent"
            r6 = 0
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ all -> 0x0c76 }
            r9.put(r2, r6)     // Catch:{ all -> 0x0c76 }
            java.lang.String r2 = "version"
            r9.put(r2, r4)     // Catch:{ all -> 0x0c76 }
            java.lang.String r2 = "desc"
            r9.put(r2, r5)     // Catch:{ all -> 0x0c76 }
            java.lang.String r2 = "ismodify"
            r4 = 0
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ all -> 0x0c76 }
            r9.put(r2, r4)     // Catch:{ all -> 0x0c76 }
            java.lang.String r2 = "lid"
            r9.put(r2, r3)     // Catch:{ all -> 0x0c76 }
            java.lang.String r2 = "type"
            r4 = 7
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ all -> 0x0c76 }
            r9.put(r2, r4)     // Catch:{ all -> 0x0c76 }
            android.database.sqlite.SQLiteDatabase r2 = r13.c     // Catch:{ all -> 0x0c76 }
            java.lang.String r4 = "songlist"
            java.lang.String r5 = " _id=? "
            r6 = 1
            java.lang.String[] r6 = new java.lang.String[r6]     // Catch:{ all -> 0x0c76 }
            r10 = 0
            r6[r10] = r8     // Catch:{ all -> 0x0c76 }
            r2.update(r4, r9, r5, r6)     // Catch:{ all -> 0x0c76 }
            long r4 = java.lang.Long.parseLong(r8)     // Catch:{ all -> 0x0c76 }
            r2 = r7
            goto L_0x0b0c
        L_0x0bf2:
            android.content.ContentValues r8 = new android.content.ContentValues     // Catch:{ all -> 0x0c76 }
            r8.<init>()     // Catch:{ all -> 0x0c76 }
            java.lang.String r9 = "name"
            r8.put(r9, r2)     // Catch:{ all -> 0x0c76 }
            java.lang.String r2 = "lid"
            r8.put(r2, r3)     // Catch:{ all -> 0x0c76 }
            java.lang.String r2 = "state"
            r9 = 1
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ all -> 0x0c76 }
            r8.put(r2, r9)     // Catch:{ all -> 0x0c76 }
            java.lang.String r2 = "picurl"
            r8.put(r2, r6)     // Catch:{ all -> 0x0c76 }
            java.lang.String r2 = "parent"
            r6 = 0
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ all -> 0x0c76 }
            r8.put(r2, r6)     // Catch:{ all -> 0x0c76 }
            java.lang.String r2 = "version"
            r8.put(r2, r4)     // Catch:{ all -> 0x0c76 }
            java.lang.String r2 = "desc"
            r8.put(r2, r5)     // Catch:{ all -> 0x0c76 }
            java.lang.String r2 = "ismodify"
            r4 = 0
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ all -> 0x0c76 }
            r8.put(r2, r4)     // Catch:{ all -> 0x0c76 }
            java.lang.String r2 = "type"
            r4 = 7
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ all -> 0x0c76 }
            r8.put(r2, r4)     // Catch:{ all -> 0x0c76 }
            android.database.sqlite.SQLiteDatabase r2 = r13.c     // Catch:{ all -> 0x0c76 }
            java.lang.String r4 = "songlist"
            java.lang.String r5 = ""
            long r4 = r2.insert(r4, r5, r8)     // Catch:{ all -> 0x0c76 }
            android.database.sqlite.SQLiteDatabase r2 = r13.c     // Catch:{ all -> 0x0c76 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x0c76 }
            r6.<init>()     // Catch:{ all -> 0x0c76 }
            java.lang.String r8 = "insert into userreflist(uid,listid) values('"
            java.lang.StringBuilder r6 = r6.append(r8)     // Catch:{ all -> 0x0c76 }
            bn r8 = defpackage.bn.a()     // Catch:{ all -> 0x0c76 }
            java.lang.String r8 = r8.h()     // Catch:{ all -> 0x0c76 }
            java.lang.StringBuilder r6 = r6.append(r8)     // Catch:{ all -> 0x0c76 }
            java.lang.String r8 = "',?)"
            java.lang.StringBuilder r6 = r6.append(r8)     // Catch:{ all -> 0x0c76 }
            java.lang.String r6 = r6.toString()     // Catch:{ all -> 0x0c76 }
            r8 = 1
            java.lang.Long[] r8 = new java.lang.Long[r8]     // Catch:{ all -> 0x0c76 }
            r9 = 0
            java.lang.Long r10 = new java.lang.Long     // Catch:{ all -> 0x0c76 }
            r10.<init>(r4)     // Catch:{ all -> 0x0c76 }
            r8[r9] = r10     // Catch:{ all -> 0x0c76 }
            r2.execSQL(r6, r8)     // Catch:{ all -> 0x0c76 }
            r2 = r7
            goto L_0x0b0c
        L_0x0c76:
            r1 = move-exception
            r2 = r7
            if (r2 == 0) goto L_0x0c7d
            r2.close()     // Catch:{ all -> 0x0c7e }
        L_0x0c7d:
            throw r1     // Catch:{ all -> 0x0c7e }
        L_0x0c7e:
            r1 = move-exception
            if (r0 == 0) goto L_0x0c84
            r0.close()
        L_0x0c84:
            throw r1
        L_0x0c85:
            java.lang.String r2 = "DuomiDataProvider"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "this is cover sync type:>>>"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r0 = r3.append(r0)
            java.lang.String r0 = r0.toString()
            defpackage.ad.b(r2, r0)
            android.database.sqlite.SQLiteDatabase r0 = r13.c
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "delete from songreflist where listid in(select a.listid from userreflist a join songlist b on a.listid=b._id join songreflist c on c.listid=b._id where a.uid='"
            java.lang.StringBuilder r2 = r2.append(r3)
            bn r3 = defpackage.bn.a()
            java.lang.String r3 = r3.h()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = "' and b.type in(2,7))"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r0.execSQL(r2)
            android.database.sqlite.SQLiteDatabase r0 = r13.c
            java.lang.String r2 = "drop table  if exists userreflisttmp"
            r0.execSQL(r2)
            android.database.sqlite.SQLiteDatabase r0 = r13.c
            java.lang.String r2 = "create table userreflisttmp as select * from userreflist"
            r0.execSQL(r2)
            android.database.sqlite.SQLiteDatabase r0 = r13.c
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "delete from userreflist where uid='"
            java.lang.StringBuilder r2 = r2.append(r3)
            bn r3 = defpackage.bn.a()
            java.lang.String r3 = r3.h()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = "' and exists(select _id from songlist where songlist._id=userreflist.listid and songlist.type=7);"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r0.execSQL(r2)
            android.database.sqlite.SQLiteDatabase r0 = r13.c
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "delete from songlist where exists (select _id from  userreflisttmp where uid='"
            java.lang.StringBuilder r2 = r2.append(r3)
            bn r3 = defpackage.bn.a()
            java.lang.String r3 = r3.h()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = "' and songlist.type=7 and listid=songlist._id)"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r0.execSQL(r2)
            android.database.sqlite.SQLiteDatabase r0 = r13.c
            java.lang.String r2 = "drop table  if exists userreflisttmp"
            r0.execSQL(r2)
            android.database.sqlite.SQLiteDatabase r0 = r13.c
            java.lang.String r2 = "select distinct(lnm),lid,lver,ldesc,limg from syncsong where lid='1'"
            r3 = 0
            android.database.Cursor r0 = r0.rawQuery(r2, r3)
            java.lang.String r2 = ""
            java.lang.String r2 = ""
            java.lang.String r2 = ""
            java.lang.String r2 = ""
            java.lang.String r2 = ""
            if (r0 == 0) goto L_0x0db2
            boolean r2 = r0.moveToFirst()     // Catch:{ all -> 0x0e9f }
            if (r2 == 0) goto L_0x0db2
            r2 = 0
            r0.getString(r2)     // Catch:{ all -> 0x0e9f }
            r2 = 1
            java.lang.String r2 = r0.getString(r2)     // Catch:{ all -> 0x0e9f }
            r3 = 2
            java.lang.String r3 = r0.getString(r3)     // Catch:{ all -> 0x0e9f }
            r4 = 3
            java.lang.String r4 = r0.getString(r4)     // Catch:{ all -> 0x0e9f }
            r5 = 4
            java.lang.String r5 = r0.getString(r5)     // Catch:{ all -> 0x0e9f }
            android.database.sqlite.SQLiteDatabase r6 = r13.c     // Catch:{ all -> 0x0e9f }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x0e9f }
            r7.<init>()     // Catch:{ all -> 0x0e9f }
            java.lang.String r8 = "update songlist set  picurl=?,version=?,desc=?,lid=?,ismodify=0 where type=2 and exists(select _id from userreflist where uid='"
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ all -> 0x0e9f }
            bn r8 = defpackage.bn.a()     // Catch:{ all -> 0x0e9f }
            java.lang.String r8 = r8.h()     // Catch:{ all -> 0x0e9f }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ all -> 0x0e9f }
            java.lang.String r8 = "' and userreflist.listid=songlist._id)"
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ all -> 0x0e9f }
            java.lang.String r7 = r7.toString()     // Catch:{ all -> 0x0e9f }
            r8 = 4
            java.lang.String[] r8 = new java.lang.String[r8]     // Catch:{ all -> 0x0e9f }
            r9 = 0
            r8[r9] = r5     // Catch:{ all -> 0x0e9f }
            r5 = 1
            r8[r5] = r3     // Catch:{ all -> 0x0e9f }
            r3 = 2
            r8[r3] = r4     // Catch:{ all -> 0x0e9f }
            r3 = 3
            r8[r3] = r2     // Catch:{ all -> 0x0e9f }
            r6.execSQL(r7, r8)     // Catch:{ all -> 0x0e9f }
            android.database.sqlite.SQLiteDatabase r2 = r13.c     // Catch:{ all -> 0x0e9f }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0e9f }
            r3.<init>()     // Catch:{ all -> 0x0e9f }
            java.lang.String r4 = "insert into songreflist(songid,listid) select song._id,(select listid from userreflist join songlist  on userreflist.listid=songlist._id where userreflist.uid='"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0e9f }
            bn r4 = defpackage.bn.a()     // Catch:{ all -> 0x0e9f }
            java.lang.String r4 = r4.h()     // Catch:{ all -> 0x0e9f }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0e9f }
            java.lang.String r4 = "' and songlist.type=2 limit 0,1) from syncsong join song on syncsong.sid=song.duomisongid where syncsong.lid='1'"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0e9f }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0e9f }
            r2.execSQL(r3)     // Catch:{ all -> 0x0e9f }
        L_0x0db2:
            if (r0 == 0) goto L_0x0db7
            r0.close()
        L_0x0db7:
            android.database.sqlite.SQLiteDatabase r0 = r13.c
            java.lang.String r2 = "select lnm,lid,lver,ldesc,limg from (select distinct(lnm),lid,lver,ldesc,limg from syncsong where lid<>'1' union select name,lid,version,desc,picurl from songlisttemp) group by lnm"
            r3 = 0
            android.database.Cursor r0 = r0.rawQuery(r2, r3)
            if (r0 == 0) goto L_0x0e97
            boolean r2 = r0.moveToFirst()     // Catch:{ all -> 0x0ea6 }
            if (r2 == 0) goto L_0x0e97
        L_0x0dc8:
            r2 = 0
            java.lang.String r2 = r0.getString(r2)     // Catch:{ all -> 0x0ea6 }
            r3 = 1
            java.lang.String r3 = r0.getString(r3)     // Catch:{ all -> 0x0ea6 }
            r4 = 2
            java.lang.String r4 = r0.getString(r4)     // Catch:{ all -> 0x0ea6 }
            r5 = 3
            java.lang.String r5 = r0.getString(r5)     // Catch:{ all -> 0x0ea6 }
            r6 = 4
            java.lang.String r6 = r0.getString(r6)     // Catch:{ all -> 0x0ea6 }
            android.content.ContentValues r7 = new android.content.ContentValues     // Catch:{ all -> 0x0ea6 }
            r7.<init>()     // Catch:{ all -> 0x0ea6 }
            java.lang.String r8 = "name"
            r7.put(r8, r2)     // Catch:{ all -> 0x0ea6 }
            java.lang.String r2 = "state"
            r8 = 1
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ all -> 0x0ea6 }
            r7.put(r2, r8)     // Catch:{ all -> 0x0ea6 }
            java.lang.String r2 = "picurl"
            r7.put(r2, r6)     // Catch:{ all -> 0x0ea6 }
            java.lang.String r2 = "lid"
            r7.put(r2, r3)     // Catch:{ all -> 0x0ea6 }
            java.lang.String r2 = "parent"
            r6 = 0
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ all -> 0x0ea6 }
            r7.put(r2, r6)     // Catch:{ all -> 0x0ea6 }
            java.lang.String r2 = "version"
            r7.put(r2, r4)     // Catch:{ all -> 0x0ea6 }
            java.lang.String r2 = "desc"
            r7.put(r2, r5)     // Catch:{ all -> 0x0ea6 }
            java.lang.String r2 = "ismodify"
            r4 = 0
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ all -> 0x0ea6 }
            r7.put(r2, r4)     // Catch:{ all -> 0x0ea6 }
            java.lang.String r2 = "type"
            r4 = 7
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ all -> 0x0ea6 }
            r7.put(r2, r4)     // Catch:{ all -> 0x0ea6 }
            android.database.sqlite.SQLiteDatabase r2 = r13.c     // Catch:{ all -> 0x0ea6 }
            java.lang.String r4 = "songlist"
            java.lang.String r5 = ""
            long r4 = r2.insert(r4, r5, r7)     // Catch:{ all -> 0x0ea6 }
            android.database.sqlite.SQLiteDatabase r2 = r13.c     // Catch:{ all -> 0x0ea6 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x0ea6 }
            r6.<init>()     // Catch:{ all -> 0x0ea6 }
            java.lang.String r7 = "insert into userreflist(uid,listid) values('"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ all -> 0x0ea6 }
            bn r7 = defpackage.bn.a()     // Catch:{ all -> 0x0ea6 }
            java.lang.String r7 = r7.h()     // Catch:{ all -> 0x0ea6 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ all -> 0x0ea6 }
            java.lang.String r7 = "',?)"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ all -> 0x0ea6 }
            java.lang.String r6 = r6.toString()     // Catch:{ all -> 0x0ea6 }
            r7 = 1
            java.lang.Long[] r7 = new java.lang.Long[r7]     // Catch:{ all -> 0x0ea6 }
            r8 = 0
            java.lang.Long r9 = new java.lang.Long     // Catch:{ all -> 0x0ea6 }
            r9.<init>(r4)     // Catch:{ all -> 0x0ea6 }
            r7[r8] = r9     // Catch:{ all -> 0x0ea6 }
            r2.execSQL(r6, r7)     // Catch:{ all -> 0x0ea6 }
            android.database.sqlite.SQLiteDatabase r2 = r13.c     // Catch:{ all -> 0x0ea6 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x0ea6 }
            r6.<init>()     // Catch:{ all -> 0x0ea6 }
            java.lang.String r7 = "insert into songreflist(songid,listid) select song._id,"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ all -> 0x0ea6 }
            java.lang.StringBuilder r4 = r6.append(r4)     // Catch:{ all -> 0x0ea6 }
            java.lang.String r5 = " from song join syncsong on song.duomisongid=syncsong.sid where syncsong.lid='"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0ea6 }
            java.lang.StringBuilder r3 = r4.append(r3)     // Catch:{ all -> 0x0ea6 }
            java.lang.String r4 = "'"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0ea6 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0ea6 }
            r2.execSQL(r3)     // Catch:{ all -> 0x0ea6 }
            boolean r2 = r0.moveToNext()     // Catch:{ all -> 0x0ea6 }
            if (r2 != 0) goto L_0x0dc8
            android.database.sqlite.SQLiteDatabase r2 = r13.c     // Catch:{ all -> 0x0ea6 }
            java.lang.String r3 = "delete from songlisttemp"
            r2.execSQL(r3)     // Catch:{ all -> 0x0ea6 }
        L_0x0e97:
            if (r0 == 0) goto L_0x0e9c
            r0.close()
        L_0x0e9c:
            r0 = r1
            goto L_0x0049
        L_0x0e9f:
            r1 = move-exception
            if (r0 == 0) goto L_0x0ea5
            r0.close()
        L_0x0ea5:
            throw r1
        L_0x0ea6:
            r1 = move-exception
            if (r0 == 0) goto L_0x0eac
            r0.close()
        L_0x0eac:
            throw r1
        L_0x0ead:
            java.lang.String r0 = "DuomiDataProvider"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "begin SYNCSONGSTRU_URI_CLEAR...."
            java.lang.StringBuilder r2 = r2.append(r3)
            bn r3 = defpackage.bn.a()
            java.lang.String r3 = r3.h()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            defpackage.ad.b(r0, r2)
            java.lang.String r0 = r14.getLastPathSegment()
            java.lang.String r2 = "1"
            boolean r2 = r2.equals(r0)
            if (r2 != 0) goto L_0x1623
            java.lang.String r0 = "0"
            r2 = r0
        L_0x0edc:
            java.lang.String r0 = "DuomiDataProvider"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "SYNCSONGSTRU_URI_CLEAR...."
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r3 = r3.append(r2)
            java.lang.String r3 = r3.toString()
            defpackage.ad.b(r0, r3)
            java.lang.String r0 = ""
            java.lang.String r0 = ""
            int r3 = r15.length
            r0 = 0
            r4 = r0
        L_0x0efb:
            if (r4 >= r3) goto L_0x11d2
            r5 = r15[r4]
            java.lang.String r0 = "lid"
            java.lang.Object r14 = r5.get(r0)
            java.lang.String r14 = (java.lang.String) r14
            java.lang.String r0 = "lnm"
            java.lang.Object r0 = r5.get(r0)
            java.lang.String r0 = (java.lang.String) r0
            java.lang.String r6 = "1"
            boolean r6 = r6.equals(r2)
            if (r6 == 0) goto L_0x0fa5
            java.lang.String r0 = "lid"
            java.lang.Object r0 = r5.get(r0)
            java.lang.String r0 = defpackage.en.a(r0)
            java.lang.String r6 = "1"
            boolean r6 = r6.equals(r0)
            if (r6 == 0) goto L_0x0f2d
        L_0x0f29:
            int r0 = r4 + 1
            r4 = r0
            goto L_0x0efb
        L_0x0f2d:
            android.content.ContentValues r6 = new android.content.ContentValues
            r6.<init>()
            java.lang.String r7 = "name"
            java.lang.String r8 = "lnm"
            java.lang.Object r8 = r5.get(r8)
            java.lang.String r8 = defpackage.en.a(r8)
            r6.put(r7, r8)
            java.lang.String r7 = "state"
            r8 = 1
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)
            r6.put(r7, r8)
            java.lang.String r7 = "picurl"
            java.lang.String r8 = "limg"
            java.lang.Object r8 = r5.get(r8)
            java.lang.String r8 = defpackage.en.a(r8)
            r6.put(r7, r8)
            java.lang.String r7 = "parent"
            r8 = 0
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)
            r6.put(r7, r8)
            java.lang.String r7 = "version"
            java.lang.String r8 = "lver"
            java.lang.Object r8 = r5.get(r8)
            java.lang.String r8 = defpackage.en.a(r8)
            r6.put(r7, r8)
            java.lang.String r7 = "desc"
            java.lang.String r8 = "ldesc"
            java.lang.Object r5 = r5.get(r8)
            java.lang.String r5 = defpackage.en.a(r5)
            r6.put(r7, r5)
            java.lang.String r5 = "ismodify"
            r7 = 0
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)
            r6.put(r5, r7)
            java.lang.String r5 = "type"
            r7 = 7
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)
            r6.put(r5, r7)
            java.lang.String r5 = "lid"
            r6.put(r5, r0)
            android.database.sqlite.SQLiteDatabase r0 = r13.c
            java.lang.String r5 = "songlisttemp"
            java.lang.String r7 = ""
            r0.insert(r5, r7, r6)
            goto L_0x0f29
        L_0x0fa5:
            android.database.sqlite.SQLiteDatabase r6 = r13.c
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = "select a._id from songlist a join userreflist b on a._id=b.listid where a.type in(2,7) and  b.uid='"
            java.lang.StringBuilder r7 = r7.append(r8)
            bn r8 = defpackage.bn.a()
            java.lang.String r8 = r8.h()
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.String r8 = "' and a.lid='"
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.StringBuilder r7 = r7.append(r14)
            java.lang.String r8 = "'"
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.String r7 = r7.toString()
            r8 = 0
            android.database.Cursor r6 = r6.rawQuery(r7, r8)
            if (r6 == 0) goto L_0x1060
            boolean r7 = r6.moveToFirst()     // Catch:{ all -> 0x161c }
            if (r7 == 0) goto L_0x1060
            r0 = 0
            int r0 = r6.getInt(r0)     // Catch:{ all -> 0x161c }
            android.database.sqlite.SQLiteDatabase r7 = r13.c     // Catch:{ all -> 0x161c }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ all -> 0x161c }
            r8.<init>()     // Catch:{ all -> 0x161c }
            java.lang.String r9 = "update songlist set picurl=?,version=?,desc=?,lid=?,name=?,ismodify=0  where _id="
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ all -> 0x161c }
            java.lang.StringBuilder r8 = r8.append(r0)     // Catch:{ all -> 0x161c }
            java.lang.String r8 = r8.toString()     // Catch:{ all -> 0x161c }
            r9 = 5
            java.lang.String[] r9 = new java.lang.String[r9]     // Catch:{ all -> 0x161c }
            r10 = 0
            java.lang.String r11 = "limg"
            java.lang.Object r11 = r5.get(r11)     // Catch:{ all -> 0x161c }
            java.lang.String r11 = defpackage.en.a(r11)     // Catch:{ all -> 0x161c }
            r9[r10] = r11     // Catch:{ all -> 0x161c }
            r10 = 1
            java.lang.String r11 = "lver"
            java.lang.Object r11 = r5.get(r11)     // Catch:{ all -> 0x161c }
            java.lang.String r11 = defpackage.en.a(r11)     // Catch:{ all -> 0x161c }
            r9[r10] = r11     // Catch:{ all -> 0x161c }
            r10 = 2
            java.lang.String r11 = "ldesc"
            java.lang.Object r11 = r5.get(r11)     // Catch:{ all -> 0x161c }
            java.lang.String r11 = defpackage.en.a(r11)     // Catch:{ all -> 0x161c }
            r9[r10] = r11     // Catch:{ all -> 0x161c }
            r10 = 3
            java.lang.String r11 = "lid"
            java.lang.Object r11 = r5.get(r11)     // Catch:{ all -> 0x161c }
            java.lang.String r11 = defpackage.en.a(r11)     // Catch:{ all -> 0x161c }
            r9[r10] = r11     // Catch:{ all -> 0x161c }
            r10 = 4
            java.lang.String r11 = "lnm"
            java.lang.Object r5 = r5.get(r11)     // Catch:{ all -> 0x161c }
            java.lang.String r5 = defpackage.en.a(r5)     // Catch:{ all -> 0x161c }
            r9[r10] = r5     // Catch:{ all -> 0x161c }
            r7.execSQL(r8, r9)     // Catch:{ all -> 0x161c }
            android.database.sqlite.SQLiteDatabase r5 = r13.c     // Catch:{ all -> 0x161c }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x161c }
            r7.<init>()     // Catch:{ all -> 0x161c }
            java.lang.String r8 = "delete from songreflist where listid="
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ all -> 0x161c }
            java.lang.StringBuilder r0 = r7.append(r0)     // Catch:{ all -> 0x161c }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x161c }
            r5.execSQL(r0)     // Catch:{ all -> 0x161c }
            r0 = r6
        L_0x1059:
            if (r0 == 0) goto L_0x0f29
            r0.close()
            goto L_0x0f29
        L_0x1060:
            r6.close()     // Catch:{ all -> 0x161c }
            android.database.sqlite.SQLiteDatabase r7 = r13.c     // Catch:{ all -> 0x161c }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ all -> 0x161c }
            r8.<init>()     // Catch:{ all -> 0x161c }
            java.lang.String r9 = "select a._id from songlist a join userreflist b on a._id=b.listid where a.type in(2,7) and b.uid='"
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ all -> 0x161c }
            bn r9 = defpackage.bn.a()     // Catch:{ all -> 0x161c }
            java.lang.String r9 = r9.h()     // Catch:{ all -> 0x161c }
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ all -> 0x161c }
            java.lang.String r9 = "' and a.name=? "
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ all -> 0x161c }
            java.lang.String r8 = r8.toString()     // Catch:{ all -> 0x161c }
            r9 = 1
            java.lang.String[] r9 = new java.lang.String[r9]     // Catch:{ all -> 0x161c }
            r10 = 0
            r9[r10] = r0     // Catch:{ all -> 0x161c }
            android.database.Cursor r0 = r7.rawQuery(r8, r9)     // Catch:{ all -> 0x161c }
            if (r0 == 0) goto L_0x111d
            boolean r6 = r0.moveToNext()     // Catch:{ all -> 0x1113 }
            if (r6 == 0) goto L_0x111d
            r6 = 0
            int r6 = r0.getInt(r6)     // Catch:{ all -> 0x1113 }
            android.database.sqlite.SQLiteDatabase r7 = r13.c     // Catch:{ all -> 0x1113 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ all -> 0x1113 }
            r8.<init>()     // Catch:{ all -> 0x1113 }
            java.lang.String r9 = "update songlist set picurl=?,version=?,desc=?,lid=?,name=?,ismodify=0  where _id="
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ all -> 0x1113 }
            java.lang.StringBuilder r8 = r8.append(r6)     // Catch:{ all -> 0x1113 }
            java.lang.String r8 = r8.toString()     // Catch:{ all -> 0x1113 }
            r9 = 5
            java.lang.String[] r9 = new java.lang.String[r9]     // Catch:{ all -> 0x1113 }
            r10 = 0
            java.lang.String r11 = "limg"
            java.lang.Object r11 = r5.get(r11)     // Catch:{ all -> 0x1113 }
            java.lang.String r11 = defpackage.en.a(r11)     // Catch:{ all -> 0x1113 }
            r9[r10] = r11     // Catch:{ all -> 0x1113 }
            r10 = 1
            java.lang.String r11 = "lver"
            java.lang.Object r11 = r5.get(r11)     // Catch:{ all -> 0x1113 }
            java.lang.String r11 = defpackage.en.a(r11)     // Catch:{ all -> 0x1113 }
            r9[r10] = r11     // Catch:{ all -> 0x1113 }
            r10 = 2
            java.lang.String r11 = "ldesc"
            java.lang.Object r11 = r5.get(r11)     // Catch:{ all -> 0x1113 }
            java.lang.String r11 = defpackage.en.a(r11)     // Catch:{ all -> 0x1113 }
            r9[r10] = r11     // Catch:{ all -> 0x1113 }
            r10 = 3
            java.lang.String r11 = "lid"
            java.lang.Object r11 = r5.get(r11)     // Catch:{ all -> 0x1113 }
            java.lang.String r11 = defpackage.en.a(r11)     // Catch:{ all -> 0x1113 }
            r9[r10] = r11     // Catch:{ all -> 0x1113 }
            r10 = 4
            java.lang.String r11 = "lnm"
            java.lang.Object r5 = r5.get(r11)     // Catch:{ all -> 0x1113 }
            java.lang.String r5 = defpackage.en.a(r5)     // Catch:{ all -> 0x1113 }
            r9[r10] = r5     // Catch:{ all -> 0x1113 }
            r7.execSQL(r8, r9)     // Catch:{ all -> 0x1113 }
            android.database.sqlite.SQLiteDatabase r5 = r13.c     // Catch:{ all -> 0x1113 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x1113 }
            r7.<init>()     // Catch:{ all -> 0x1113 }
            java.lang.String r8 = "delete from songreflist where listid="
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ all -> 0x1113 }
            java.lang.StringBuilder r6 = r7.append(r6)     // Catch:{ all -> 0x1113 }
            java.lang.String r6 = r6.toString()     // Catch:{ all -> 0x1113 }
            r5.execSQL(r6)     // Catch:{ all -> 0x1113 }
            goto L_0x1059
        L_0x1113:
            r1 = move-exception
            r12 = r1
            r1 = r0
            r0 = r12
        L_0x1117:
            if (r1 == 0) goto L_0x111c
            r1.close()
        L_0x111c:
            throw r0
        L_0x111d:
            android.content.ContentValues r6 = new android.content.ContentValues     // Catch:{ all -> 0x1113 }
            r6.<init>()     // Catch:{ all -> 0x1113 }
            java.lang.String r7 = "name"
            java.lang.String r8 = "lnm"
            java.lang.Object r8 = r5.get(r8)     // Catch:{ all -> 0x1113 }
            java.lang.String r8 = defpackage.en.a(r8)     // Catch:{ all -> 0x1113 }
            r6.put(r7, r8)     // Catch:{ all -> 0x1113 }
            java.lang.String r7 = "state"
            r8 = 1
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ all -> 0x1113 }
            r6.put(r7, r8)     // Catch:{ all -> 0x1113 }
            java.lang.String r7 = "picurl"
            java.lang.String r8 = "limg"
            java.lang.Object r8 = r5.get(r8)     // Catch:{ all -> 0x1113 }
            java.lang.String r8 = defpackage.en.a(r8)     // Catch:{ all -> 0x1113 }
            r6.put(r7, r8)     // Catch:{ all -> 0x1113 }
            java.lang.String r7 = "parent"
            r8 = 0
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ all -> 0x1113 }
            r6.put(r7, r8)     // Catch:{ all -> 0x1113 }
            java.lang.String r7 = "version"
            java.lang.String r8 = "lver"
            java.lang.Object r8 = r5.get(r8)     // Catch:{ all -> 0x1113 }
            java.lang.String r8 = defpackage.en.a(r8)     // Catch:{ all -> 0x1113 }
            r6.put(r7, r8)     // Catch:{ all -> 0x1113 }
            java.lang.String r7 = "desc"
            java.lang.String r8 = "ldesc"
            java.lang.Object r8 = r5.get(r8)     // Catch:{ all -> 0x1113 }
            java.lang.String r8 = defpackage.en.a(r8)     // Catch:{ all -> 0x1113 }
            r6.put(r7, r8)     // Catch:{ all -> 0x1113 }
            java.lang.String r7 = "ismodify"
            r8 = 0
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ all -> 0x1113 }
            r6.put(r7, r8)     // Catch:{ all -> 0x1113 }
            java.lang.String r7 = "type"
            r8 = 7
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ all -> 0x1113 }
            r6.put(r7, r8)     // Catch:{ all -> 0x1113 }
            java.lang.String r7 = "lid"
            java.lang.String r8 = "lid"
            java.lang.Object r5 = r5.get(r8)     // Catch:{ all -> 0x1113 }
            java.lang.String r5 = defpackage.en.a(r5)     // Catch:{ all -> 0x1113 }
            r6.put(r7, r5)     // Catch:{ all -> 0x1113 }
            android.database.sqlite.SQLiteDatabase r5 = r13.c     // Catch:{ all -> 0x1113 }
            java.lang.String r7 = "songlist"
            java.lang.String r8 = ""
            long r5 = r5.insert(r7, r8, r6)     // Catch:{ all -> 0x1113 }
            android.database.sqlite.SQLiteDatabase r7 = r13.c     // Catch:{ all -> 0x1113 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ all -> 0x1113 }
            r8.<init>()     // Catch:{ all -> 0x1113 }
            java.lang.String r9 = "insert into userreflist(uid,listid) values('"
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ all -> 0x1113 }
            bn r9 = defpackage.bn.a()     // Catch:{ all -> 0x1113 }
            java.lang.String r9 = r9.h()     // Catch:{ all -> 0x1113 }
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ all -> 0x1113 }
            java.lang.String r9 = "',?)"
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ all -> 0x1113 }
            java.lang.String r8 = r8.toString()     // Catch:{ all -> 0x1113 }
            r9 = 1
            java.lang.Long[] r9 = new java.lang.Long[r9]     // Catch:{ all -> 0x1113 }
            r10 = 0
            java.lang.Long r11 = new java.lang.Long     // Catch:{ all -> 0x1113 }
            r11.<init>(r5)     // Catch:{ all -> 0x1113 }
            r9[r10] = r11     // Catch:{ all -> 0x1113 }
            r7.execSQL(r8, r9)     // Catch:{ all -> 0x1113 }
            goto L_0x1059
        L_0x11d2:
            r0 = r1
            goto L_0x0049
        L_0x11d5:
            java.lang.String r0 = "DuomiDataProvider"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "begin SYNCSONGSTRU_URI_UNCHANGE...."
            java.lang.StringBuilder r3 = r3.append(r4)
            bn r4 = defpackage.bn.a()
            java.lang.String r4 = r4.h()
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            defpackage.ad.b(r0, r3)
            java.lang.String r0 = ""
            java.lang.String r0 = ""
            int r3 = r15.length
            r0 = 0
            r4 = r0
        L_0x11fc:
            if (r4 >= r3) goto L_0x12a7
            r5 = r15[r4]
            java.lang.String r0 = "lid"
            java.lang.Object r14 = r5.get(r0)
            java.lang.String r14 = (java.lang.String) r14
            java.lang.String r0 = "lnm"
            java.lang.Object r0 = r5.get(r0)
            java.lang.String r0 = (java.lang.String) r0
            android.database.sqlite.SQLiteDatabase r6 = r13.c
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = "select a._id from songlist a join userreflist b on a._id=b.listid where a.type in(2,7) and  b.uid='"
            java.lang.StringBuilder r7 = r7.append(r8)
            bn r8 = defpackage.bn.a()
            java.lang.String r8 = r8.h()
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.String r8 = "' and a.lid='"
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.StringBuilder r7 = r7.append(r14)
            java.lang.String r8 = "'"
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.String r7 = r7.toString()
            r8 = 0
            android.database.Cursor r6 = r6.rawQuery(r7, r8)
            if (r6 == 0) goto L_0x125c
            boolean r7 = r6.moveToFirst()     // Catch:{ all -> 0x1618 }
            if (r7 == 0) goto L_0x125c
            r0 = 0
            int r0 = r6.getInt(r0)     // Catch:{ all -> 0x1618 }
            r13.a(r2, r0, r5)     // Catch:{ all -> 0x1618 }
            r0 = r6
        L_0x1253:
            if (r0 == 0) goto L_0x1258
            r0.close()
        L_0x1258:
            int r0 = r4 + 1
            r4 = r0
            goto L_0x11fc
        L_0x125c:
            r6.close()     // Catch:{ all -> 0x1618 }
            android.database.sqlite.SQLiteDatabase r7 = r13.c     // Catch:{ all -> 0x1618 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ all -> 0x1618 }
            r8.<init>()     // Catch:{ all -> 0x1618 }
            java.lang.String r9 = "select a._id from songlist a join userreflist b on a._id=b.listid where a.type in(2,7) and  b.uid='"
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ all -> 0x1618 }
            bn r9 = defpackage.bn.a()     // Catch:{ all -> 0x1618 }
            java.lang.String r9 = r9.h()     // Catch:{ all -> 0x1618 }
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ all -> 0x1618 }
            java.lang.String r9 = "' and a.name=?"
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ all -> 0x1618 }
            java.lang.String r8 = r8.toString()     // Catch:{ all -> 0x1618 }
            r9 = 1
            java.lang.String[] r9 = new java.lang.String[r9]     // Catch:{ all -> 0x1618 }
            r10 = 0
            r9[r10] = r0     // Catch:{ all -> 0x1618 }
            android.database.Cursor r0 = r7.rawQuery(r8, r9)     // Catch:{ all -> 0x1618 }
            if (r0 == 0) goto L_0x1253
            boolean r6 = r0.moveToFirst()     // Catch:{ all -> 0x129d }
            if (r6 == 0) goto L_0x1253
            r6 = 0
            int r6 = r0.getInt(r6)     // Catch:{ all -> 0x129d }
            r13.a(r2, r6, r5)     // Catch:{ all -> 0x129d }
            goto L_0x1253
        L_0x129d:
            r1 = move-exception
            r12 = r1
            r1 = r0
            r0 = r12
        L_0x12a1:
            if (r1 == 0) goto L_0x12a6
            r1.close()
        L_0x12a6:
            throw r0
        L_0x12a7:
            r0 = r1
            goto L_0x0049
        L_0x12aa:
            java.util.List r0 = r14.getPathSegments()
            r1 = 1
            java.lang.Object r0 = r0.get(r1)
            java.lang.String r0 = (java.lang.String) r0
            java.util.List r1 = r14.getPathSegments()
            r2 = 2
            java.lang.Object r1 = r1.get(r2)
            java.lang.String r1 = (java.lang.String) r1
            android.database.sqlite.SQLiteDatabase r2 = r13.c
            java.lang.String r3 = "drop table if exists tempdeletelistpos "
            r2.execSQL(r3)
            android.database.sqlite.SQLiteDatabase r2 = r13.c
            java.lang.String r3 = "create table if not exists tempdeletelistpos (position Integer)"
            r2.execSQL(r3)
            android.database.sqlite.SQLiteDatabase r2 = r13.c
            r2.beginTransaction()
            r2 = 0
            int r3 = r15.length     // Catch:{ Exception -> 0x1612 }
            r4 = 0
            r12 = r4
            r4 = r2
            r2 = r12
        L_0x12d9:
            if (r2 >= r3) goto L_0x13da
            r5 = r15[r2]     // Catch:{ Exception -> 0x130e }
            android.database.sqlite.SQLiteDatabase r6 = r13.c     // Catch:{ Exception -> 0x1309 }
            java.lang.String r7 = "tempdeletelistpos"
            java.lang.String r8 = ""
            r6.insert(r7, r8, r5)     // Catch:{ Exception -> 0x1309 }
            java.lang.String r6 = "DuomiDataProvider"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x1309 }
            r7.<init>()     // Catch:{ Exception -> 0x1309 }
            java.lang.String r8 = "position>>>>>"
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Exception -> 0x1309 }
            java.lang.String r8 = "position"
            java.lang.Object r5 = r5.get(r8)     // Catch:{ Exception -> 0x1309 }
            java.lang.StringBuilder r5 = r7.append(r5)     // Catch:{ Exception -> 0x1309 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x1309 }
            defpackage.ad.b(r6, r5)     // Catch:{ Exception -> 0x1309 }
            int r4 = r4 + 1
        L_0x1306:
            int r2 = r2 + 1
            goto L_0x12d9
        L_0x1309:
            r5 = move-exception
            r5.printStackTrace()     // Catch:{ Exception -> 0x130e }
            goto L_0x1306
        L_0x130e:
            r2 = move-exception
            r3 = r4
        L_0x1310:
            r2.printStackTrace()     // Catch:{ all -> 0x13dd }
            r2 = r3
        L_0x1314:
            java.lang.String r3 = "DuomiDataProvider"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x13dd }
            r4.<init>()     // Catch:{ all -> 0x13dd }
            java.lang.String r5 = ">>bulk insert record ("
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x13dd }
            java.lang.StringBuilder r4 = r4.append(r2)     // Catch:{ all -> 0x13dd }
            java.lang.String r5 = ") rows:"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x13dd }
            java.lang.String r5 = r14.toString()     // Catch:{ all -> 0x13dd }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x13dd }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x13dd }
            defpackage.ad.b(r3, r4)     // Catch:{ all -> 0x13dd }
            android.database.sqlite.SQLiteDatabase r3 = r13.c     // Catch:{ all -> 0x13dd }
            r3.setTransactionSuccessful()     // Catch:{ all -> 0x13dd }
            android.database.sqlite.SQLiteDatabase r3 = r13.c
            r3.endTransaction()
            android.database.sqlite.SQLiteDatabase r3 = r13.c
            java.lang.String r4 = "drop table if exists tempdeletereflist "
            r3.execSQL(r4)
            android.database.sqlite.SQLiteDatabase r3 = r13.c
            java.lang.String r4 = "create table if not exists tempdeletereflist(_id INTEGER primary key autoincrement,songid INTEGER,path TEXT,filepath TEXT)"
            r3.execSQL(r4)
            android.database.sqlite.SQLiteDatabase r3 = r13.c
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "insert into tempdeletereflist(songid,path,filepath) select a._id,a.path,a.disp from song a join songreflist b on a._id=b.songid where b.listid="
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r4 = r4.append(r0)
            java.lang.String r4 = r4.toString()
            r3.execSQL(r4)
            android.database.sqlite.SQLiteDatabase r3 = r13.c
            java.lang.String r4 = "delete from tempdeletereflist where _id not in(select position from tempdeletelistpos)"
            r3.execSQL(r4)
            java.lang.String r3 = "0"
            boolean r1 = r3.equals(r1)
            if (r1 == 0) goto L_0x13e4
            android.database.sqlite.SQLiteDatabase r1 = r13.c
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "delete from songreflist where listid in("
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r3 = r3.append(r0)
            java.lang.String r4 = ") and songid in(select songid from tempdeletereflist)"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            r1.execSQL(r3)
        L_0x1397:
            boolean r1 = defpackage.en.c(r0)
            if (r1 != 0) goto L_0x1620
            if (r15 == 0) goto L_0x1620
            int r1 = r15.length
            if (r1 <= 0) goto L_0x1620
            java.lang.String r1 = "update "
            java.lang.String r3 = "songlist"
            java.lang.String r1 = r1.concat(r3)
            java.lang.String r3 = " set "
            java.lang.String r1 = r1.concat(r3)
            java.lang.String r3 = "ismodify"
            java.lang.String r1 = r1.concat(r3)
            java.lang.String r3 = "=1 "
            java.lang.String r1 = r1.concat(r3)
            java.lang.String r3 = " where "
            java.lang.String r1 = r1.concat(r3)
            java.lang.String r3 = "_id"
            java.lang.String r1 = r1.concat(r3)
            java.lang.String r3 = "="
            java.lang.String r1 = r1.concat(r3)
            java.lang.String r0 = r1.concat(r0)
            android.database.sqlite.SQLiteDatabase r1 = r13.c
            r1.execSQL(r0)
            r0 = r2
            goto L_0x0049
        L_0x13da:
            r2 = r4
            goto L_0x1314
        L_0x13dd:
            r0 = move-exception
            android.database.sqlite.SQLiteDatabase r1 = r13.c
            r1.endTransaction()
            throw r0
        L_0x13e4:
            android.database.sqlite.SQLiteDatabase r1 = r13.c
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "delete from songreflist where  listid in(select _id from songlist where parent in(1,2,3,4) or _id in(1,"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r3 = r3.append(r0)
            java.lang.String r4 = ") ) and songid in(select songid from tempdeletereflist) "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            r1.execSQL(r3)
            android.database.sqlite.SQLiteDatabase r1 = r13.c
            java.lang.String r3 = "delete from songreflist where exists(select _id from song where song._id=songreflist.songid and (auditionurl is null or auditionurl='') and songreflist.songid in(select songid from tempdeletereflist))"
            r1.execSQL(r3)
            android.database.sqlite.SQLiteDatabase r1 = r13.c
            java.lang.String r3 = "select filepath from tempdeletereflist "
            r4 = 0
            android.database.Cursor r1 = r1.rawQuery(r3, r4)
            if (r1 == 0) goto L_0x1439
            boolean r3 = r1.moveToFirst()     // Catch:{ Exception -> 0x1445 }
            if (r3 == 0) goto L_0x1439
        L_0x141a:
            r3 = 0
            java.lang.String r3 = r1.getString(r3)     // Catch:{ Exception -> 0x1445 }
            boolean r4 = defpackage.en.c(r3)     // Catch:{ Exception -> 0x1440 }
            if (r4 != 0) goto L_0x1433
            java.io.File r4 = new java.io.File     // Catch:{ Exception -> 0x1440 }
            r4.<init>(r3)     // Catch:{ Exception -> 0x1440 }
            boolean r3 = r4.exists()     // Catch:{ Exception -> 0x1440 }
            if (r3 == 0) goto L_0x1433
            r4.delete()     // Catch:{ Exception -> 0x1440 }
        L_0x1433:
            boolean r3 = r1.moveToNext()     // Catch:{ Exception -> 0x1445 }
            if (r3 != 0) goto L_0x141a
        L_0x1439:
            if (r1 == 0) goto L_0x1397
            r1.close()
            goto L_0x1397
        L_0x1440:
            r3 = move-exception
            r3.printStackTrace()     // Catch:{ Exception -> 0x1445 }
            goto L_0x1433
        L_0x1445:
            r3 = move-exception
            r3.printStackTrace()     // Catch:{ all -> 0x1450 }
            if (r1 == 0) goto L_0x1397
            r1.close()
            goto L_0x1397
        L_0x1450:
            r0 = move-exception
            if (r1 == 0) goto L_0x1456
            r1.close()
        L_0x1456:
            throw r0
        L_0x1457:
            java.util.List r0 = r14.getPathSegments()
            r1 = 2
            java.lang.Object r0 = r0.get(r1)
            java.lang.String r0 = (java.lang.String) r0
            java.util.List r1 = r14.getPathSegments()
            r2 = 3
            java.lang.Object r1 = r1.get(r2)
            java.lang.String r1 = (java.lang.String) r1
            android.database.sqlite.SQLiteDatabase r2 = r13.c
            java.lang.String r3 = "drop table if exists tempdeletelistpos "
            r2.execSQL(r3)
            android.database.sqlite.SQLiteDatabase r2 = r13.c
            java.lang.String r3 = "create table if not exists tempdeletelistpos (position Integer)"
            r2.execSQL(r3)
            android.database.sqlite.SQLiteDatabase r2 = r13.c
            r2.beginTransaction()
            r2 = 0
            int r3 = r15.length     // Catch:{ Exception -> 0x160c }
            r4 = 0
            r12 = r4
            r4 = r2
            r2 = r12
        L_0x1486:
            if (r2 >= r3) goto L_0x1541
            r5 = r15[r2]     // Catch:{ Exception -> 0x149d }
            android.database.sqlite.SQLiteDatabase r6 = r13.c     // Catch:{ Exception -> 0x1498 }
            java.lang.String r7 = "tempdeletelistpos"
            java.lang.String r8 = ""
            r6.insert(r7, r8, r5)     // Catch:{ Exception -> 0x1498 }
            int r4 = r4 + 1
        L_0x1495:
            int r2 = r2 + 1
            goto L_0x1486
        L_0x1498:
            r5 = move-exception
            r5.printStackTrace()     // Catch:{ Exception -> 0x149d }
            goto L_0x1495
        L_0x149d:
            r2 = move-exception
            r3 = r4
        L_0x149f:
            r2.printStackTrace()     // Catch:{ all -> 0x1544 }
            r2 = r3
        L_0x14a3:
            java.lang.String r3 = "DuomiDataProvider"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x1544 }
            r4.<init>()     // Catch:{ all -> 0x1544 }
            java.lang.String r5 = ">>>bulk insert record ("
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x1544 }
            java.lang.StringBuilder r4 = r4.append(r2)     // Catch:{ all -> 0x1544 }
            java.lang.String r5 = ") rows:"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x1544 }
            java.lang.String r5 = r14.toString()     // Catch:{ all -> 0x1544 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x1544 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x1544 }
            defpackage.ad.b(r3, r4)     // Catch:{ all -> 0x1544 }
            android.database.sqlite.SQLiteDatabase r3 = r13.c     // Catch:{ all -> 0x1544 }
            r3.setTransactionSuccessful()     // Catch:{ all -> 0x1544 }
            android.database.sqlite.SQLiteDatabase r3 = r13.c
            r3.endTransaction()
            android.database.sqlite.SQLiteDatabase r3 = r13.c
            java.lang.String r4 = "drop table if exists tempdeletereflist "
            r3.execSQL(r4)
            android.database.sqlite.SQLiteDatabase r3 = r13.c
            java.lang.String r4 = "create table if not exists tempdeletereflist(_id INTEGER primary key autoincrement,songid INTEGER,path TEXT,filepath TEXT)"
            r3.execSQL(r4)
            android.database.sqlite.SQLiteDatabase r3 = r13.c
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "insert into tempdeletereflist(songid,path,filepath) select a._id,a.path,a.disp from song a join songreflist b on a._id=b.songid where b.listid="
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r0 = r4.append(r0)
            java.lang.String r0 = r0.toString()
            r3.execSQL(r0)
            android.database.sqlite.SQLiteDatabase r0 = r13.c
            java.lang.String r3 = "delete from tempdeletereflist where _id not in(select position from tempdeletelistpos)"
            r0.execSQL(r3)
            android.content.Context r0 = r13.getContext()
            as r0 = defpackage.as.a(r0)
            boolean r0 = r0.U()
            if (r0 == 0) goto L_0x1561
            android.database.sqlite.SQLiteDatabase r0 = r13.c
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "select s.* from song s  where s._id in (select songid from tempdeletereflist where songid not in (select songid from songreflist where listid="
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r3 = r3.append(r1)
            java.lang.String r4 = "))"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            r4 = 0
            android.database.Cursor r0 = r0.rawQuery(r3, r4)
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>()
        L_0x1533:
            boolean r4 = r0.moveToNext()
            if (r4 == 0) goto L_0x154b
            bj r4 = defpackage.ar.a(r0)
            r3.add(r4)
            goto L_0x1533
        L_0x1541:
            r2 = r4
            goto L_0x14a3
        L_0x1544:
            r0 = move-exception
            android.database.sqlite.SQLiteDatabase r1 = r13.c
            r1.endTransaction()
            throw r0
        L_0x154b:
            r0.close()
            int r0 = r3.size()
            if (r0 <= 0) goto L_0x1561
            java.lang.Thread r0 = new java.lang.Thread
            af r4 = new af
            r4.<init>(r13, r3)
            r0.<init>(r4)
            r0.start()
        L_0x1561:
            android.database.sqlite.SQLiteDatabase r0 = r13.c
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "insert into songreflist(songid,listid) select songid,"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r3 = r3.append(r1)
            java.lang.String r4 = " from tempdeletereflist where songid not in(select songid from songreflist where listid="
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r3 = r3.append(r1)
            java.lang.String r4 = ")"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            r0.execSQL(r3)
            boolean r0 = defpackage.en.c(r1)
            if (r0 != 0) goto L_0x1620
            if (r15 == 0) goto L_0x1620
            int r0 = r15.length
            if (r0 <= 0) goto L_0x1620
            java.lang.String r0 = "update "
            java.lang.String r3 = "songlist"
            java.lang.String r0 = r0.concat(r3)
            java.lang.String r3 = " set "
            java.lang.String r0 = r0.concat(r3)
            java.lang.String r3 = "ismodify"
            java.lang.String r0 = r0.concat(r3)
            java.lang.String r3 = "=1 "
            java.lang.String r0 = r0.concat(r3)
            java.lang.String r3 = " where "
            java.lang.String r0 = r0.concat(r3)
            java.lang.String r3 = "_id"
            java.lang.String r0 = r0.concat(r3)
            java.lang.String r3 = "="
            java.lang.String r0 = r0.concat(r3)
            java.lang.String r0 = r0.concat(r1)
            android.database.sqlite.SQLiteDatabase r1 = r13.c
            r1.execSQL(r0)
            r0 = r2
            goto L_0x0049
        L_0x15cc:
            r3 = move-exception
            r3.printStackTrace()     // Catch:{ all -> 0x15d2 }
            goto L_0x0029
        L_0x15d2:
            r0 = move-exception
            android.database.sqlite.SQLiteDatabase r1 = r13.c
            r1.endTransaction()
            throw r0
        L_0x15d9:
            java.lang.String r0 = "DuomiDataProvider"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x15d2 }
            r1.<init>()     // Catch:{ all -> 0x15d2 }
            java.lang.String r3 = ">>bulk insert record ("
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ all -> 0x15d2 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x15d2 }
            java.lang.String r3 = ") rows:"
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ all -> 0x15d2 }
            java.lang.String r3 = r14.toString()     // Catch:{ all -> 0x15d2 }
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ all -> 0x15d2 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x15d2 }
            defpackage.ad.b(r0, r1)     // Catch:{ all -> 0x15d2 }
            android.database.sqlite.SQLiteDatabase r0 = r13.c     // Catch:{ all -> 0x15d2 }
            r0.setTransactionSuccessful()     // Catch:{ all -> 0x15d2 }
            android.database.sqlite.SQLiteDatabase r0 = r13.c
            r0.endTransaction()
            r0 = r2
            goto L_0x0049
        L_0x160c:
            r3 = move-exception
            r12 = r3
            r3 = r2
            r2 = r12
            goto L_0x149f
        L_0x1612:
            r3 = move-exception
            r12 = r3
            r3 = r2
            r2 = r12
            goto L_0x1310
        L_0x1618:
            r0 = move-exception
            r1 = r6
            goto L_0x12a1
        L_0x161c:
            r0 = move-exception
            r1 = r6
            goto L_0x1117
        L_0x1620:
            r0 = r2
            goto L_0x0049
        L_0x1623:
            r2 = r0
            goto L_0x0edc
        */
        throw new UnsupportedOperationException("Method not decompiled: com.duomi.app.datastore.DuomiDataProvider.bulkInsert(android.net.Uri, android.content.ContentValues[]):int");
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x01ab  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x01ec  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int delete(android.net.Uri r10, java.lang.String r11, java.lang.String[] r12) {
        /*
            r9 = this;
            r8 = 1
            r7 = 0
            r6 = 0
            java.lang.String r4 = "songreflist"
            java.lang.String r5 = "_id"
            android.content.UriMatcher r0 = com.duomi.app.datastore.DuomiDataProvider.a
            int r0 = r0.match(r10)
            switch(r0) {
                case 1: goto L_0x003d;
                case 2: goto L_0x0046;
                case 3: goto L_0x0053;
                case 4: goto L_0x005c;
                case 5: goto L_0x0069;
                case 6: goto L_0x0072;
                case 7: goto L_0x007f;
                case 8: goto L_0x0088;
                case 9: goto L_0x0095;
                case 10: goto L_0x009e;
                case 11: goto L_0x00ab;
                case 12: goto L_0x00b5;
                case 13: goto L_0x00c3;
                case 14: goto L_0x00cd;
                case 15: goto L_0x00db;
                case 16: goto L_0x00e5;
                case 17: goto L_0x0010;
                case 18: goto L_0x0010;
                case 19: goto L_0x00f0;
                case 20: goto L_0x00fa;
                case 21: goto L_0x0108;
                case 22: goto L_0x0112;
                case 23: goto L_0x0120;
                case 24: goto L_0x012a;
                case 25: goto L_0x0010;
                case 26: goto L_0x0010;
                case 27: goto L_0x0010;
                case 28: goto L_0x0010;
                case 29: goto L_0x0010;
                case 30: goto L_0x0010;
                case 31: goto L_0x0010;
                case 32: goto L_0x0010;
                case 33: goto L_0x0138;
                case 34: goto L_0x0142;
                case 35: goto L_0x014c;
                case 36: goto L_0x015a;
                case 37: goto L_0x0164;
                case 38: goto L_0x0172;
                case 39: goto L_0x017c;
                case 40: goto L_0x0187;
                case 41: goto L_0x0010;
                case 42: goto L_0x0010;
                case 43: goto L_0x0010;
                case 44: goto L_0x0010;
                case 45: goto L_0x0010;
                case 46: goto L_0x01ef;
                case 47: goto L_0x01f9;
                case 48: goto L_0x0010;
                case 49: goto L_0x0010;
                case 50: goto L_0x0010;
                case 51: goto L_0x0010;
                case 52: goto L_0x0207;
                case 53: goto L_0x0211;
                case 54: goto L_0x025e;
                case 55: goto L_0x0010;
                case 56: goto L_0x0010;
                case 57: goto L_0x0010;
                case 58: goto L_0x0010;
                case 59: goto L_0x0010;
                case 60: goto L_0x0268;
                case 61: goto L_0x0029;
                case 62: goto L_0x0010;
                case 63: goto L_0x0010;
                case 64: goto L_0x0010;
                case 65: goto L_0x0010;
                case 66: goto L_0x0344;
                case 67: goto L_0x034e;
                case 68: goto L_0x0358;
                case 69: goto L_0x0362;
                default: goto L_0x0010;
            }
        L_0x0010:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Unsupported URI: "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r10)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0029:
            android.database.sqlite.SQLiteDatabase r0 = r9.c
            java.lang.String r1 = "songlisttemp"
            int r0 = r0.delete(r1, r11, r12)
        L_0x0031:
            android.content.Context r1 = r9.getContext()
            android.content.ContentResolver r1 = r1.getContentResolver()
            r1.notifyChange(r10, r7)
        L_0x003c:
            return r0
        L_0x003d:
            android.database.sqlite.SQLiteDatabase r0 = r9.c
            java.lang.String r1 = "musicsearch"
            int r0 = r0.delete(r1, r11, r12)
            goto L_0x0031
        L_0x0046:
            java.lang.String r4 = "musicsearch"
            java.lang.String r0 = "_id"
            r0 = r9
            r1 = r10
            r2 = r11
            r3 = r12
            int r0 = r0.a(r1, r2, r3, r4, r5)
            goto L_0x0031
        L_0x0053:
            android.database.sqlite.SQLiteDatabase r0 = r9.c
            java.lang.String r1 = "playerhistory"
            int r0 = r0.delete(r1, r11, r12)
            goto L_0x0031
        L_0x005c:
            java.lang.String r4 = "playerhistory"
            java.lang.String r0 = "_id"
            r0 = r9
            r1 = r10
            r2 = r11
            r3 = r12
            int r0 = r0.a(r1, r2, r3, r4, r5)
            goto L_0x0031
        L_0x0069:
            android.database.sqlite.SQLiteDatabase r0 = r9.c
            java.lang.String r1 = "randomhistory"
            int r0 = r0.delete(r1, r7, r7)
            goto L_0x0031
        L_0x0072:
            java.lang.String r4 = "randomhistory"
            java.lang.String r0 = "_id"
            r0 = r9
            r1 = r10
            r2 = r11
            r3 = r12
            int r0 = r0.a(r1, r2, r3, r4, r5)
            goto L_0x0031
        L_0x007f:
            android.database.sqlite.SQLiteDatabase r0 = r9.c
            java.lang.String r1 = "downloads"
            int r0 = r0.delete(r1, r11, r12)
            goto L_0x0031
        L_0x0088:
            java.lang.String r4 = "downloads"
            java.lang.String r0 = "_id"
            r0 = r9
            r1 = r10
            r2 = r11
            r3 = r12
            int r0 = r0.a(r1, r2, r3, r4, r5)
            goto L_0x0031
        L_0x0095:
            android.database.sqlite.SQLiteDatabase r0 = r9.c
            java.lang.String r1 = "addition"
            int r0 = r0.delete(r1, r11, r12)
            goto L_0x0031
        L_0x009e:
            java.lang.String r4 = "addition"
            java.lang.String r0 = "_id"
            r0 = r9
            r1 = r10
            r2 = r11
            r3 = r12
            int r0 = r0.a(r1, r2, r3, r4, r5)
            goto L_0x0031
        L_0x00ab:
            android.database.sqlite.SQLiteDatabase r0 = r9.c
            java.lang.String r1 = "duomiusers"
            int r0 = r0.delete(r1, r11, r12)
            goto L_0x0031
        L_0x00b5:
            java.lang.String r4 = "duomiusers"
            java.lang.String r0 = "_id"
            r0 = r9
            r1 = r10
            r2 = r11
            r3 = r12
            int r0 = r0.a(r1, r2, r3, r4, r5)
            goto L_0x0031
        L_0x00c3:
            android.database.sqlite.SQLiteDatabase r0 = r9.c
            java.lang.String r1 = "cachepic"
            int r0 = r0.delete(r1, r11, r12)
            goto L_0x0031
        L_0x00cd:
            java.lang.String r4 = "cachepic"
            java.lang.String r0 = "_id"
            r0 = r9
            r1 = r10
            r2 = r11
            r3 = r12
            int r0 = r0.a(r1, r2, r3, r4, r5)
            goto L_0x0031
        L_0x00db:
            android.database.sqlite.SQLiteDatabase r0 = r9.c
            java.lang.String r1 = "category_online"
            int r0 = r0.delete(r1, r11, r12)
            goto L_0x0031
        L_0x00e5:
            java.lang.String r4 = "category_online"
            java.lang.String r0 = "_id"
            r0 = r9
            r1 = r10
            r2 = r11
            r3 = r12
            r0.a(r1, r2, r3, r4, r5)
        L_0x00f0:
            android.database.sqlite.SQLiteDatabase r0 = r9.c
            java.lang.String r1 = "playlog"
            int r0 = r0.delete(r1, r11, r12)
            goto L_0x0031
        L_0x00fa:
            java.lang.String r4 = "playlog"
            java.lang.String r0 = "_id"
            r0 = r9
            r1 = r10
            r2 = r11
            r3 = r12
            int r0 = r0.a(r1, r2, r3, r4, r5)
            goto L_0x0031
        L_0x0108:
            android.database.sqlite.SQLiteDatabase r0 = r9.c
            java.lang.String r1 = "playblocklog"
            int r0 = r0.delete(r1, r11, r12)
            goto L_0x0031
        L_0x0112:
            java.lang.String r4 = "playblocklog"
            java.lang.String r0 = "_id"
            r0 = r9
            r1 = r10
            r2 = r11
            r3 = r12
            int r0 = r0.a(r1, r2, r3, r4, r5)
            goto L_0x0031
        L_0x0120:
            android.database.sqlite.SQLiteDatabase r0 = r9.c
            java.lang.String r1 = "playcommand"
            int r0 = r0.delete(r1, r11, r12)
            goto L_0x0031
        L_0x012a:
            java.lang.String r4 = "playcommand"
            java.lang.String r0 = "_id"
            r0 = r9
            r1 = r10
            r2 = r11
            r3 = r12
            int r0 = r0.a(r1, r2, r3, r4, r5)
            goto L_0x0031
        L_0x0138:
            android.database.sqlite.SQLiteDatabase r0 = r9.c
            java.lang.String r1 = "halldata"
            int r0 = r0.delete(r1, r7, r7)
            goto L_0x0031
        L_0x0142:
            android.database.sqlite.SQLiteDatabase r0 = r9.c
            java.lang.String r1 = "song"
            int r0 = r0.delete(r1, r11, r12)
            goto L_0x0031
        L_0x014c:
            java.lang.String r4 = "song"
            java.lang.String r0 = "_id"
            r0 = r9
            r1 = r10
            r2 = r11
            r3 = r12
            int r0 = r0.a(r1, r2, r3, r4, r5)
            goto L_0x0031
        L_0x015a:
            android.database.sqlite.SQLiteDatabase r0 = r9.c
            java.lang.String r1 = "songlist"
            int r0 = r0.delete(r1, r11, r12)
            goto L_0x0031
        L_0x0164:
            java.lang.String r4 = "songlist"
            java.lang.String r0 = "_id"
            r0 = r9
            r1 = r10
            r2 = r11
            r3 = r12
            int r0 = r0.a(r1, r2, r3, r4, r5)
            goto L_0x0031
        L_0x0172:
            android.database.sqlite.SQLiteDatabase r0 = r9.c
            java.lang.String r1 = "songreflist"
            int r0 = r0.delete(r4, r11, r12)
            goto L_0x0031
        L_0x017c:
            java.lang.String r0 = "songreflist"
            java.lang.String r0 = "_id"
            r0 = r9
            r1 = r10
            r2 = r11
            r3 = r12
            r0.a(r1, r2, r3, r4, r5)
        L_0x0187:
            java.util.List r0 = r10.getPathSegments()
            r1 = 2
            java.lang.Object r0 = r0.get(r1)
            java.lang.String r0 = (java.lang.String) r0
            android.database.sqlite.SQLiteDatabase r1 = r9.c
            java.lang.String r2 = "songreflist"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "listid="
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r0)
            boolean r3 = android.text.TextUtils.isEmpty(r11)
            if (r3 != 0) goto L_0x01ec
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r5 = " AND ("
            java.lang.StringBuilder r3 = r3.append(r5)
            java.lang.StringBuilder r3 = r3.append(r11)
            r5 = 41
            java.lang.StringBuilder r3 = r3.append(r5)
            java.lang.String r3 = r3.toString()
        L_0x01c4:
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            int r1 = r1.delete(r4, r2, r12)
            android.content.ContentValues r2 = new android.content.ContentValues
            r2.<init>()
            java.lang.String r3 = "ismodify"
            java.lang.Integer r4 = java.lang.Integer.valueOf(r8)
            r2.put(r3, r4)
            android.net.Uri r3 = defpackage.cf.a
            java.lang.String r4 = "_id=?"
            java.lang.String[] r5 = new java.lang.String[r8]
            r5[r6] = r0
            r9.update(r3, r2, r4, r5)
            r0 = r1
            goto L_0x0031
        L_0x01ec:
            java.lang.String r3 = ""
            goto L_0x01c4
        L_0x01ef:
            android.database.sqlite.SQLiteDatabase r0 = r9.c
            java.lang.String r1 = "songtemp"
            int r0 = r0.delete(r1, r11, r12)
            goto L_0x0031
        L_0x01f9:
            java.lang.String r4 = "songtemp"
            java.lang.String r0 = "_id"
            r0 = r9
            r1 = r10
            r2 = r11
            r3 = r12
            int r0 = r0.a(r1, r2, r3, r4, r5)
            goto L_0x0031
        L_0x0207:
            android.database.sqlite.SQLiteDatabase r0 = r9.c
            java.lang.String r1 = "currentsongs"
            int r0 = r0.delete(r1, r11, r12)
            goto L_0x0031
        L_0x0211:
            java.util.List r0 = r10.getPathSegments()
            java.lang.Object r0 = r0.get(r8)
            java.lang.String r0 = (java.lang.String) r0
            android.database.sqlite.SQLiteDatabase r1 = r9.c
            java.lang.String r2 = "currentsongs"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "_id="
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r0 = r3.append(r0)
            boolean r3 = android.text.TextUtils.isEmpty(r11)
            if (r3 != 0) goto L_0x025b
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = " AND ("
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r3 = r3.append(r11)
            r4 = 41
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
        L_0x024d:
            java.lang.StringBuilder r0 = r0.append(r3)
            java.lang.String r0 = r0.toString()
            int r0 = r1.delete(r2, r0, r12)
            goto L_0x0031
        L_0x025b:
            java.lang.String r3 = ""
            goto L_0x024d
        L_0x025e:
            android.database.sqlite.SQLiteDatabase r0 = r9.c
            java.lang.String r1 = "drop table if exists syncsong"
            r0.execSQL(r1)
            r0 = r6
            goto L_0x003c
        L_0x0268:
            java.lang.String r0 = "DuomiDataProvider"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.StringBuilder r1 = r1.append(r11)
            java.lang.String r2 = ">>>>>"
            java.lang.StringBuilder r1 = r1.append(r2)
            r2 = r12[r6]
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            defpackage.ad.b(r0, r1)
            android.database.sqlite.SQLiteDatabase r0 = r9.c
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "select a._id,a.version from songlist a join userreflist b on a._id = b.listid where b.uid='"
            java.lang.StringBuilder r1 = r1.append(r2)
            bn r2 = defpackage.bn.a()
            java.lang.String r2 = r2.h()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "' and a.name=?"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            android.database.Cursor r0 = r0.rawQuery(r1, r12)
            if (r0 == 0) goto L_0x0335
            boolean r1 = r0.moveToFirst()     // Catch:{ all -> 0x033d }
            if (r1 == 0) goto L_0x0335
            r1 = 0
            int r1 = r0.getInt(r1)     // Catch:{ all -> 0x033d }
            r2 = 1
            java.lang.String r2 = r0.getString(r2)     // Catch:{ all -> 0x033d }
            java.lang.String r3 = "0"
            boolean r2 = r3.equals(r2)     // Catch:{ all -> 0x033d }
            if (r2 != 0) goto L_0x02ed
            android.database.sqlite.SQLiteDatabase r2 = r9.c     // Catch:{ all -> 0x033d }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x033d }
            r3.<init>()     // Catch:{ all -> 0x033d }
            java.lang.String r4 = "insert into songlisttemp select a.* from songlist a join userreflist b on a._id = b.listid where b.uid='"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x033d }
            bn r4 = defpackage.bn.a()     // Catch:{ all -> 0x033d }
            java.lang.String r4 = r4.h()     // Catch:{ all -> 0x033d }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x033d }
            java.lang.String r4 = "' and a.name=?"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x033d }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x033d }
            r2.execSQL(r3, r12)     // Catch:{ all -> 0x033d }
        L_0x02ed:
            android.database.sqlite.SQLiteDatabase r2 = r9.c     // Catch:{ all -> 0x033d }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x033d }
            r3.<init>()     // Catch:{ all -> 0x033d }
            java.lang.String r4 = "delete from userreflist where listid="
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x033d }
            java.lang.StringBuilder r3 = r3.append(r1)     // Catch:{ all -> 0x033d }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x033d }
            r2.execSQL(r3)     // Catch:{ all -> 0x033d }
            android.database.sqlite.SQLiteDatabase r2 = r9.c     // Catch:{ all -> 0x033d }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x033d }
            r3.<init>()     // Catch:{ all -> 0x033d }
            java.lang.String r4 = "delete from songreflist where listid="
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x033d }
            java.lang.StringBuilder r3 = r3.append(r1)     // Catch:{ all -> 0x033d }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x033d }
            r2.execSQL(r3)     // Catch:{ all -> 0x033d }
            android.database.sqlite.SQLiteDatabase r2 = r9.c     // Catch:{ all -> 0x033d }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x033d }
            r3.<init>()     // Catch:{ all -> 0x033d }
            java.lang.String r4 = "delete from songlist where _id="
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x033d }
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ all -> 0x033d }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x033d }
            r2.execSQL(r1)     // Catch:{ all -> 0x033d }
        L_0x0335:
            if (r0 == 0) goto L_0x033a
            r0.close()
        L_0x033a:
            r0 = r6
            goto L_0x0031
        L_0x033d:
            r1 = move-exception
            if (r0 == 0) goto L_0x0343
            r0.close()
        L_0x0343:
            throw r1
        L_0x0344:
            android.database.sqlite.SQLiteDatabase r0 = r9.c
            java.lang.String r1 = "login_account"
            int r0 = r0.delete(r1, r11, r12)
            goto L_0x0031
        L_0x034e:
            android.database.sqlite.SQLiteDatabase r0 = r9.c
            java.lang.String r1 = "badorder"
            int r0 = r0.delete(r1, r11, r12)
            goto L_0x0031
        L_0x0358:
            android.database.sqlite.SQLiteDatabase r0 = r9.c
            java.lang.String r1 = "ring"
            int r0 = r0.delete(r1, r11, r12)
            goto L_0x0031
        L_0x0362:
            android.database.sqlite.SQLiteDatabase r0 = r9.c
            java.lang.String r1 = "serviceinfomodle"
            int r0 = r0.delete(r1, r11, r12)
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: com.duomi.app.datastore.DuomiDataProvider.delete(android.net.Uri, java.lang.String, java.lang.String[]):int");
    }

    public String getType(Uri uri) {
        switch (a.match(uri)) {
            case 1:
                return "vnd.android.cursor.dir/vnd.duomi.musicsearch";
            case 2:
                return "vnd.android.cursor.item/vnd.duomi.musicsearch";
            case 3:
                return "vnd.android.cursor.dir/vnd.duomi.playerhistory";
            case 4:
                return "vnd.android.cursor.item/vnd.duomi.playerhistory";
            case 5:
                return "vnd.android.cursor.dir/vnd.duomi.randomhistory";
            case 6:
                return "vnd.android.cursor.item/vnd.duomi.randomhistory";
            case 7:
                return "vnd.android.cursor.dir/vnd.duomi.download";
            case 8:
                return "vnd.android.cursor.item/vnd.duomi.download";
            case 9:
                return "vnd.android.cursor.dir/vnd.duomi.addition";
            case 10:
                return "vnd.android.cursor.item/vnd.duomi.addition";
            case 11:
                return "vnd.android.cursor.dir/vnd.duomi.duomiusers";
            case 12:
                return "vnd.android.cursor.item/vnd.duomi.duomiusers";
            case 13:
                return "vnd.android.cursor.dir/vnd.duomi.cachepic";
            case 14:
                return "vnd.android.cursor.item/vnd.duomi.cachepic";
            case 15:
                return "vnd.android.cursor.dir/vnd.duomi.category_online";
            case 16:
                return "vnd.android.cursor.item/vnd.duomi.category_online";
            case 17:
            case 18:
            case 25:
            case 26:
            case 27:
            case 28:
            case 29:
            case 30:
            case 31:
            case 44:
            case 45:
            case 46:
            case 47:
            case 48:
            case 49:
            case 50:
            case 51:
            case 55:
            case 56:
            case 57:
            case 59:
            case 60:
            case 61:
            case 62:
            case 63:
            case 64:
            case 65:
            default:
                return null;
            case 19:
                return "vnd.android.cursor.dir/vnd.duomi.playlog";
            case 20:
                return "vnd.android.cursor.item/vnd.duomi.playlog";
            case 21:
                return "vnd.android.cursor.dir/vnd.duomi.playblocklog";
            case 22:
                return "vnd.android.cursor.item/vnd.duomi.playblocklog";
            case 23:
                return "vnd.android.cursor.dir/vnd.duomi.playcommand";
            case 24:
                return "vnd.android.cursor.item/vnd.duomi.playcommand";
            case 32:
                return "vnd.android.cursor.item/vnd.duomi.halldata";
            case 33:
                return "vnd.android.cursor.dir/vnd.duomi.halldata";
            case 34:
                return "vnd.android.cursor.dir/vnd.duomi.song";
            case 35:
                return "vnd.android.cursor.item/vnd.duomi.song";
            case 36:
                return "vnd.android.cursor.dir/vnd.duomi.songlist";
            case 37:
                return "vnd.android.cursor.item/vnd.duomi.songlist";
            case 38:
                return "vnd.android.cursor.dir/vnd.duomi.songreflist";
            case 39:
                return "vnd.android.cursor.item/vnd.duomi.songreflist";
            case 40:
                return "vnd.android.cursor.dir/vnd.duomi.song";
            case 41:
                return "vnd.android.cursor.item/vnd.duomi.song";
            case 42:
                return "vnd.android.cursor.item/vnd.duomi.song";
            case 43:
                return "vnd.android.cursor.item/vnd.duomi.song";
            case 52:
                return "vnd.android.cursor.dir/vnd.duomi.currentsongs";
            case 53:
                return "vnd.android.cursor.item/vnd.duomi.currentsongs";
            case 54:
                return "vnd.android.cursor.item/vnd.duomi.syncsong";
            case 58:
                return "vnd.android.cursor.item/vnd.duomi.randomhistory";
            case 66:
                return "vnd.android.cursor.item/vnd.duomi.login_account";
            case 67:
                return "vnd.android.cursor.item/vnd.duomi.badorder";
            case 68:
                return "vnd.android.cursor.item/vnd.duomi.ring";
            case 69:
                return "vnd.android.cursor.item/vnd.duomi.serviceinfomodle";
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public Uri insert(Uri uri, ContentValues contentValues) {
        Uri a2;
        switch (a.match(uri)) {
            case 1:
            case 2:
                a2 = a(contentValues, "musicsearch", bx.b);
                break;
            case 3:
            case 4:
                a2 = a(contentValues, "playerhistory", cb.a);
                break;
            case 5:
            case 6:
                a2 = a(contentValues, "randomhistory", cc.a);
                break;
            case 7:
            case 8:
                a2 = a(contentValues, "downloads", bu.a);
                break;
            case 9:
            case 10:
                a2 = a(contentValues, "addition", bp.a);
                break;
            case 11:
            case 12:
                a2 = a(contentValues, "duomiusers", cn.b);
                break;
            case 13:
            case 14:
                a2 = a(contentValues, "cachepic", br.a);
                break;
            case 15:
            case 16:
                a2 = a(contentValues, "category_online", bs.a);
                break;
            case 17:
            case 19:
            case 20:
                a2 = a(contentValues, "playlog", ca.a);
                break;
            case 18:
            case 25:
            case 26:
            case 27:
            case 28:
            case 29:
            case 30:
            case 31:
            case 32:
            case 40:
            case 41:
            case 42:
            case 43:
            case 45:
            case 50:
            case 55:
            case 56:
            case 57:
            case 58:
            case 59:
            case 60:
            case 61:
            case 62:
            case 63:
            case 64:
            case 65:
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
            case 21:
            case 22:
                a2 = a(contentValues, "playblocklog", by.a);
                break;
            case 23:
            case 24:
                a2 = a(contentValues, "playcommand", bz.a);
                break;
            case 33:
                a2 = a(contentValues, "halldata", bv.a);
                break;
            case 34:
            case 35:
                a2 = a(contentValues, "song", cj.b);
                break;
            case 36:
            case 37:
                a2 = a(contentValues, "songlist", cf.a);
                break;
            case 38:
            case 39:
                a2 = a(contentValues, "songreflist", ci.a);
                break;
            case 44:
                String lastPathSegment = uri.getLastPathSegment();
                String[] stringArray = getContext().getResources().getStringArray(R.array.songlist_name);
                ContentValues contentValues2 = new ContentValues();
                ContentValues contentValues3 = new ContentValues();
                contentValues2.put("desc", "");
                contentValues2.put("ismodify", (Integer) 1);
                contentValues2.put("lid", "-1");
                contentValues2.put("version", "0");
                contentValues2.put("name", stringArray[0]);
                contentValues2.put("parent", (Integer) 0);
                contentValues2.put("picurl", "");
                contentValues2.put("state", (Integer) 1);
                contentValues2.put("type", (Integer) 0);
                this.c.beginTransaction();
                long insert = this.c.insert("songlist", null, contentValues2);
                if (insert != -1) {
                    contentValues3.put("listid", Long.valueOf(insert));
                    contentValues3.put(AdvertisementList.EXTRA_UID, lastPathSegment);
                    this.c.insert("userreflist", null, contentValues3);
                }
                contentValues3.put("listid", (Integer) 1);
                contentValues3.put(AdvertisementList.EXTRA_UID, lastPathSegment);
                this.c.insert("userreflist", null, contentValues3);
                contentValues2.put("name", stringArray[2]);
                contentValues2.put("type", (Integer) 2);
                contentValues2.put("lid", "1");
                long insert2 = this.c.insert("songlist", null, contentValues2);
                contentValues2.put("lid", "-1");
                if (insert2 != -1) {
                    contentValues3.put("listid", Long.valueOf(insert2));
                    contentValues3.put(AdvertisementList.EXTRA_UID, lastPathSegment);
                    this.c.insert("userreflist", null, contentValues3);
                }
                contentValues2.put("name", stringArray[3]);
                contentValues2.put("type", (Integer) 3);
                long insert3 = this.c.insert("songlist", null, contentValues2);
                if (insert3 != -1) {
                    contentValues3.put("listid", Long.valueOf(insert3));
                    contentValues3.put(AdvertisementList.EXTRA_UID, lastPathSegment);
                    this.c.insert("userreflist", null, contentValues3);
                }
                contentValues3.put("listid", (Integer) 2);
                contentValues3.put(AdvertisementList.EXTRA_UID, lastPathSegment);
                this.c.insert("userreflist", null, contentValues3);
                contentValues3.put("listid", (Integer) 3);
                contentValues3.put(AdvertisementList.EXTRA_UID, lastPathSegment);
                this.c.insert("userreflist", null, contentValues3);
                contentValues3.put("listid", (Integer) 4);
                contentValues3.put(AdvertisementList.EXTRA_UID, lastPathSegment);
                this.c.insert("userreflist", null, contentValues3);
                this.c.setTransactionSuccessful();
                this.c.endTransaction();
                return null;
            case 46:
            case 47:
                a2 = a(contentValues, "songtemp", ck.a);
                break;
            case 48:
                return null;
            case 49:
                ad.b("DuomiDataProvider", "SONGLIST_INC");
                a("insert into songlist(name,state,parent,lid,version,ismodify,type) select distinct singer,0,4,-1,'0',1,'4' from songtemp where not exists (select name from songlist where songlist.name=songtemp.singer)");
                return null;
            case 51:
                ContentValues contentValues4 = new ContentValues();
                ContentValues contentValues5 = new ContentValues();
                contentValues4.put("desc", "");
                contentValues4.put("ismodify", (Integer) 1);
                contentValues4.put("lid", "-1");
                contentValues4.put("version", "0");
                contentValues4.put("name", contentValues.getAsString("name"));
                contentValues4.put("parent", (Integer) 0);
                contentValues4.put("picurl", "");
                contentValues4.put("state", (Integer) 1);
                contentValues4.put("type", (Integer) 7);
                long insert4 = this.c.insert("songlist", null, contentValues4);
                if (insert4 != -1) {
                    contentValues5.put("listid", Long.valueOf(insert4));
                    contentValues5.put(AdvertisementList.EXTRA_UID, bn.a().h());
                    this.c.insert("userreflist", null, contentValues5);
                }
                return ContentUris.withAppendedId(cm.c, insert4);
            case 52:
            case 53:
                a2 = a(contentValues, "currentsongs", bt.a);
                break;
            case 54:
                this.c.insert("syncsong", null, contentValues);
                return null;
            case 66:
                a2 = a(contentValues, "login_account", bw.a);
                break;
            case 67:
                a2 = a(contentValues, "badorder", bq.a);
                break;
            case 68:
                a2 = a(contentValues, "ring", cd.a);
                break;
            case 69:
                a2 = a(contentValues, "serviceinfomodle", ce.a);
                break;
        }
        if (a2 != null) {
            return a2;
        }
        throw new SQLException("Failed to insert row into " + uri);
    }

    public boolean onCreate() {
        this.b = new ag(getContext(), "duomiandroid.db", null, 7);
        try {
            this.c = this.b.getWritableDatabase();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this.c != null;
    }

    public Cursor query(Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        String str3;
        SQLiteQueryBuilder sQLiteQueryBuilder = new SQLiteQueryBuilder();
        StringBuffer stringBuffer = new StringBuffer();
        switch (a.match(uri)) {
            case 1:
                sQLiteQueryBuilder.setTables("musicsearch");
                if (!TextUtils.isEmpty(str2)) {
                    str3 = str2;
                    break;
                } else {
                    str3 = "_id";
                    break;
                }
            case 2:
                sQLiteQueryBuilder.setTables("musicsearch");
                sQLiteQueryBuilder.appendWhere("_id=" + uri.getPathSegments().get(1));
                if (!TextUtils.isEmpty(str2)) {
                    str3 = str2;
                    break;
                } else {
                    str3 = "_id";
                    break;
                }
            case 3:
                sQLiteQueryBuilder.setTables("playerhistory");
                if (!TextUtils.isEmpty(str2)) {
                    str3 = str2;
                    break;
                } else {
                    str3 = "_id";
                    break;
                }
            case 4:
                sQLiteQueryBuilder.setTables("playerhistory");
                sQLiteQueryBuilder.appendWhere("_id=" + uri.getPathSegments().get(1));
                if (!TextUtils.isEmpty(str2)) {
                    str3 = str2;
                    break;
                } else {
                    str3 = "_id";
                    break;
                }
            case 5:
                return this.c.rawQuery("select count(*) from  randomhistory", null);
            case 6:
                String str4 = uri.getPathSegments().get(1);
                String str5 = "select * from songreflist where listid=" + str4 + " and not exists (select * from " + "randomhistory" + " where " + cc.b + " = " + "songreflist" + "." + "songid" + " and " + "listid" + " = " + str4 + ")" + " limit " + uri.getPathSegments().get(2) + ",1";
                ad.b("DuomiDataProvider", "sql>>" + str5);
                return this.c.rawQuery(str5, null);
            case 7:
                sQLiteQueryBuilder.setTables("downloads");
                if (!TextUtils.isEmpty(str2)) {
                    str3 = str2;
                    break;
                } else {
                    str3 = "date";
                    break;
                }
            case 8:
                sQLiteQueryBuilder.setTables("downloads");
                sQLiteQueryBuilder.appendWhere("_id=" + uri.getPathSegments().get(1));
                if (!TextUtils.isEmpty(str2)) {
                    str3 = str2;
                    break;
                } else {
                    str3 = "date";
                    break;
                }
            case 9:
                sQLiteQueryBuilder.setTables("addition");
                if (!TextUtils.isEmpty(str2)) {
                    str3 = str2;
                    break;
                } else {
                    str3 = "_id";
                    break;
                }
            case 10:
                sQLiteQueryBuilder.setTables("addition");
                sQLiteQueryBuilder.appendWhere("_id=" + uri.getPathSegments().get(1));
                if (!TextUtils.isEmpty(str2)) {
                    str3 = str2;
                    break;
                } else {
                    str3 = "_id";
                    break;
                }
            case 11:
                sQLiteQueryBuilder.setTables("duomiusers");
                if (!TextUtils.isEmpty(str2)) {
                    str3 = str2;
                    break;
                } else {
                    str3 = "_id";
                    break;
                }
            case 12:
                sQLiteQueryBuilder.setTables("duomiusers");
                sQLiteQueryBuilder.appendWhere("_id=" + uri.getPathSegments().get(1));
                if (!TextUtils.isEmpty(str2)) {
                    str3 = str2;
                    break;
                } else {
                    str3 = "_id";
                    break;
                }
            case 13:
                sQLiteQueryBuilder.setTables("cachepic");
                if (!TextUtils.isEmpty(str2)) {
                    str3 = str2;
                    break;
                } else {
                    str3 = "_id";
                    break;
                }
            case 14:
                sQLiteQueryBuilder.setTables("cachepic");
                sQLiteQueryBuilder.appendWhere("_id=" + uri.getPathSegments().get(1));
                if (!TextUtils.isEmpty(str2)) {
                    str3 = str2;
                    break;
                } else {
                    str3 = "_id";
                    break;
                }
            case 15:
                sQLiteQueryBuilder.setTables("category_online");
                if (!TextUtils.isEmpty(str2)) {
                    str3 = str2;
                    break;
                } else {
                    str3 = "_id";
                    break;
                }
            case 16:
                sQLiteQueryBuilder.setTables("category_online");
                sQLiteQueryBuilder.appendWhere("_id=" + uri.getPathSegments().get(1));
                str3 = null;
                break;
            case 17:
            case 18:
            case 44:
            case 46:
            case 47:
            case 48:
            case 49:
            case 51:
            case 54:
            case 55:
            case 56:
            case 57:
            case 60:
            case 62:
            case 63:
            case 64:
            case 65:
            default:
                str3 = null;
                break;
            case 19:
                sQLiteQueryBuilder.setTables("playlog");
                if (!TextUtils.isEmpty(str2)) {
                    str3 = str2;
                    break;
                } else {
                    str3 = "_id";
                    break;
                }
            case 20:
                sQLiteQueryBuilder.setTables("playlog");
                sQLiteQueryBuilder.appendWhere("_id=" + uri.getPathSegments().get(1));
                str3 = null;
                break;
            case 21:
                sQLiteQueryBuilder.setTables("playblocklog");
                if (!TextUtils.isEmpty(str2)) {
                    str3 = str2;
                    break;
                } else {
                    str3 = "_id";
                    break;
                }
            case 22:
                sQLiteQueryBuilder.setTables("playblocklog");
                sQLiteQueryBuilder.appendWhere("_id=" + uri.getPathSegments().get(1));
                str3 = null;
                break;
            case 23:
                sQLiteQueryBuilder.setTables("playcommand");
                if (!TextUtils.isEmpty(str2)) {
                    str3 = str2;
                    break;
                } else {
                    str3 = "_id";
                    break;
                }
            case 24:
                sQLiteQueryBuilder.setTables("playcommand");
                sQLiteQueryBuilder.appendWhere("_id=" + uri.getPathSegments().get(1));
                str3 = null;
                break;
            case 25:
                String str6 = "select startcost,rectime from playlog where " + (str == null ? "" : str + " and ") + "startcost" + "=(select max(" + "startcost" + ") from " + "playlog" + (str == null ? "" : " where " + str + " ") + ") limit 0,1";
                Cursor rawQuery = this.c.rawQuery(str6, null);
                ad.b("DuomiDataProvider", str6);
                rawQuery.setNotificationUri(getContext().getContentResolver(), uri);
                return rawQuery;
            case 26:
                Cursor rawQuery2 = this.c.rawQuery("select speed,rectime from playlog where " + (str == null ? "" : str + " and ") + " " + "speed" + "=(select max(" + "speed" + ") from " + "playlog" + " " + (str == null ? "" : " where " + str + " ") + ") limit 0,1", null);
                rawQuery2.setNotificationUri(getContext().getContentResolver(), uri);
                return rawQuery2;
            case 27:
                return this.c.rawQuery("select speed,rectime from playlog where " + (str == null ? "" : str + " and ") + " speed=(select min(speed) from playlog " + (str == null ? "" : " where " + str + " ") + ") limit 0,1", null);
            case 28:
                String concat = "".concat(" select * from ").concat("playlog").concat(" ");
                if (str != null) {
                    concat = concat.concat(" where ").concat(str);
                }
                Cursor rawQuery3 = this.c.rawQuery(concat.concat(" limit 1"), null);
                rawQuery3.setNotificationUri(getContext().getContentResolver(), uri);
                return rawQuery3;
            case 29:
                Cursor rawQuery4 = this.c.rawQuery("select * from playlog" + (str == null ? "" : " where " + str) + " limit (select count(*) from " + "playlog" + ")-1,1", null);
                rawQuery4.setNotificationUri(getContext().getContentResolver(), uri);
                return rawQuery4;
            case 30:
                Cursor rawQuery5 = this.c.rawQuery("select prefix from playlog" + (str == null ? "" : " where " + str) + " limit 1 ", null);
                rawQuery5.setNotificationUri(getContext().getContentResolver(), uri);
                return rawQuery5;
            case 31:
                Cursor rawQuery6 = this.c.rawQuery("select count(*) from (select " + ca.i + ",count(" + ca.i + ") from " + "playlog" + (str == null ? "" : " where " + str) + " group by " + ca.i + ")", null);
                rawQuery6.setNotificationUri(getContext().getContentResolver(), uri);
                return rawQuery6;
            case 32:
                sQLiteQueryBuilder.setTables("halldata");
                sQLiteQueryBuilder.appendWhere("_id=" + uri.getPathSegments().get(1));
                str3 = null;
                break;
            case 33:
                sQLiteQueryBuilder.setTables("halldata");
                if (!TextUtils.isEmpty(str2)) {
                    str3 = str2;
                    break;
                } else {
                    str3 = "_id";
                    break;
                }
            case 34:
                sQLiteQueryBuilder.setTables("song");
                if (!TextUtils.isEmpty(str2)) {
                    str3 = str2;
                    break;
                } else {
                    str3 = "_id";
                    break;
                }
            case 35:
                sQLiteQueryBuilder.setTables("song");
                sQLiteQueryBuilder.appendWhere("_id=" + uri.getPathSegments().get(1));
                if (!TextUtils.isEmpty(str2)) {
                    str3 = str2;
                    break;
                } else {
                    str3 = "_id";
                    break;
                }
            case 36:
                sQLiteQueryBuilder.setTables("songlist");
                if (!TextUtils.isEmpty(str2)) {
                    str3 = str2;
                    break;
                } else {
                    str3 = "_id";
                    break;
                }
            case 37:
                sQLiteQueryBuilder.setTables("songlist");
                sQLiteQueryBuilder.appendWhere("_id=" + uri.getPathSegments().get(1));
                if (!TextUtils.isEmpty(str2)) {
                    str3 = str2;
                    break;
                } else {
                    str3 = "_id";
                    break;
                }
            case 38:
                sQLiteQueryBuilder.setTables("songreflist");
                if (!TextUtils.isEmpty(str2)) {
                    str3 = str2;
                    break;
                } else {
                    str3 = "_id";
                    break;
                }
            case 39:
                sQLiteQueryBuilder.setTables("songreflist");
                sQLiteQueryBuilder.appendWhere("_id=" + uri.getPathSegments().get(1));
                if (!TextUtils.isEmpty(str2)) {
                    str3 = str2;
                    break;
                } else {
                    str3 = "_id";
                    break;
                }
            case 40:
                String str7 = uri.getPathSegments().get(2);
                String str8 = "select s.* from song s join songreflist srl on s._id=srl.songid where srl.listid=" + str7;
                if (!en.c(str)) {
                    str8 = str8 + " and " + str;
                }
                if (strArr != null && strArr.length > 0 && "count(*)".equals(strArr[0].trim())) {
                    str8 = "select count(s._id) from song s join songreflist srl on s._id=srl.songid where srl.listid=" + str7;
                }
                ad.b("DuomiDataProvider", "REFLISTSONGS :sqlString >>>" + str8);
                Cursor rawQuery7 = this.c.rawQuery(str8, null);
                rawQuery7.setNotificationUri(getContext().getContentResolver(), uri);
                return rawQuery7;
            case 41:
                Cursor rawQuery8 = this.c.rawQuery("select s.* from song s join songreflist srl on s._id=srl.songid where srl.listid=" + uri.getPathSegments().get(2) + " limit " + uri.getPathSegments().get(3) + ",1", null);
                rawQuery8.setNotificationUri(getContext().getContentResolver(), uri);
                return rawQuery8;
            case 42:
                String str9 = uri.getPathSegments().get(2);
                String str10 = "select count(*) from songreflist where listid=" + str9 + " and " + "_id" + "<=" + "(select " + "_id" + " from " + "songreflist" + " where " + "listid" + "=" + str9 + " and " + "songid" + "=" + uri.getPathSegments().get(3) + " limit 0,1)" + " order by " + "_id" + " asc";
                ad.b("DuomiDataProvider", str10);
                return this.c.rawQuery(str10, null);
            case 43:
                ad.b("SongDaoTest", ">>entering songreflist>>>>>>");
                sQLiteQueryBuilder.setTables("song a  join songreflist b on a._id=b.songid left join addition c on a.duomisongid=c.s_songid");
                String buildQuery = sQLiteQueryBuilder.buildQuery(strArr, str, strArr2, null, null, str2, null);
                ad.b("SongDaoTest", ">>execute sql>>>>>>" + buildQuery);
                Cursor rawQuery9 = this.c.rawQuery(buildQuery, strArr2);
                rawQuery9.setNotificationUri(getContext().getContentResolver(), uri);
                return rawQuery9;
            case 45:
                String lastPathSegment = uri.getLastPathSegment();
                stringBuffer.delete(0, stringBuffer.length());
                stringBuffer.append("select b.* from ").append("userreflist").append(" a join ").append("songlist").append(" b on a.").append("listid").append("=b.").append("_id").append(" where a.").append(AdvertisementList.EXTRA_UID).append("=").append("'").append(lastPathSegment).append("'");
                ad.b("DuomiDataProvider", "USERREFLIST_QUERY_TOP>>>" + stringBuffer.toString());
                return this.c.rawQuery(stringBuffer.toString(), null);
            case 50:
                stringBuffer.delete(0, stringBuffer.length());
                stringBuffer.append("select b.* from userreflist a join songlist b on a.listid=b._id ");
                if (!en.c(str)) {
                    stringBuffer.append(" where ").append(str);
                }
                ad.b("DuomiDataProvider", "sqlbuffer>>>>>" + stringBuffer.toString());
                return this.c.rawQuery(stringBuffer.toString(), strArr2);
            case 52:
                sQLiteQueryBuilder.setTables("currentsongs");
                if (!TextUtils.isEmpty(str2)) {
                    str3 = str2;
                    break;
                } else {
                    str3 = "_id";
                    break;
                }
            case 53:
                return this.c.rawQuery("select s.* from currentsongs s limit " + uri.getPathSegments().get(1) + ",1", null);
            case 58:
                String str11 = "select * from currentsongs where not exists (select * from randomhistory where " + cc.b + " = " + "currentsongs" + "." + "_id" + ")" + " limit " + uri.getPathSegments().get(1) + ",1";
                ad.b("DuomiDataProvider", "sql>>" + str11);
                return this.c.rawQuery(str11, null);
            case 59:
                Cursor rawQuery10 = this.c.rawQuery("select s.* from song s join songreflist srl on s._id=srl.songid where srl.listid=" + uri.getPathSegments().get(2) + " and srl." + "songid" + "=" + uri.getPathSegments().get(3), null);
                rawQuery10.setNotificationUri(getContext().getContentResolver(), uri);
                return rawQuery10;
            case 61:
                sQLiteQueryBuilder.setTables("songlisttemp");
                str3 = null;
                break;
            case 66:
                sQLiteQueryBuilder.setTables("login_account");
                if (!TextUtils.isEmpty(str2)) {
                    str3 = str2;
                    break;
                } else {
                    str3 = "_id";
                    break;
                }
            case 67:
                sQLiteQueryBuilder.setTables("badorder");
                str3 = null;
                break;
            case 68:
                sQLiteQueryBuilder.setTables("ring");
                str3 = null;
                break;
            case 69:
                sQLiteQueryBuilder.setTables("serviceinfomodle");
                str3 = null;
                break;
        }
        Cursor query = sQLiteQueryBuilder.query(this.c, strArr, str, strArr2, null, null, str3, null);
        query.setNotificationUri(getContext().getContentResolver(), uri);
        return query;
    }

    public int update(Uri uri, ContentValues contentValues, String str, String[] strArr) {
        int update;
        switch (a.match(uri)) {
            case 1:
                update = this.c.update("musicsearch", contentValues, str, strArr);
                break;
            case 2:
                update = a(uri, contentValues, str, strArr, "musicsearch", "_id");
                break;
            case 3:
                update = this.c.update("playerhistory", contentValues, str, strArr);
                break;
            case 4:
                update = a(uri, contentValues, str, strArr, "playerhistory", "_id");
                break;
            case 5:
                update = this.c.update("randomhistory", contentValues, str, strArr);
                break;
            case 6:
                update = a(uri, contentValues, str, strArr, "randomhistory", "_id");
                break;
            case 7:
                update = this.c.update("downloads", contentValues, str, strArr);
                break;
            case 8:
                update = a(uri, contentValues, str, strArr, "downloads", "_id");
                break;
            case 9:
                update = this.c.update("addition", contentValues, str, strArr);
                break;
            case 10:
                update = a(uri, contentValues, str, strArr, "addition", "_id");
                break;
            case 11:
                update = this.c.update("duomiusers", contentValues, str, strArr);
                break;
            case 12:
                update = a(uri, contentValues, str, strArr, "duomiusers", "_id");
                break;
            case 13:
                update = this.c.update("cachepic", contentValues, str, strArr);
                break;
            case 14:
                update = a(uri, contentValues, str, strArr, "cachepic", "_id");
                break;
            case 15:
                update = this.c.update("category_online", contentValues, str, strArr);
                break;
            case 16:
                update = a(uri, contentValues, str, strArr, "category_online", "_id");
                break;
            case 17:
            case 18:
            case 25:
            case 26:
            case 27:
            case 28:
            case 29:
            case 30:
            case 31:
            case 32:
            case 33:
            case 40:
            case 41:
            case 42:
            case 43:
            case 44:
            case 45:
            case 46:
            case 47:
            case 48:
            case 49:
            case 50:
            case 51:
            case 54:
            case 55:
            case 56:
            case 57:
            case 58:
            case 59:
            case 60:
            case 61:
            case 62:
            case 63:
            case 64:
            case 65:
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
            case 19:
                update = this.c.update("playlog", contentValues, str, strArr);
                break;
            case 20:
                update = a(uri, contentValues, str, strArr, "playlog", "_id");
                break;
            case 21:
                update = this.c.update("playblocklog", contentValues, str, strArr);
                break;
            case 22:
                update = a(uri, contentValues, str, strArr, "playblocklog", "_id");
                break;
            case 23:
                update = this.c.update("playcommand", contentValues, str, strArr);
                break;
            case 24:
                update = a(uri, contentValues, str, strArr, "playcommand", "_id");
                break;
            case 34:
                update = this.c.update("song", contentValues, str, strArr);
                break;
            case 35:
                update = a(uri, contentValues, str, strArr, "song", "_id");
                break;
            case 36:
                update = this.c.update("songlist", contentValues, str, strArr);
                break;
            case 37:
                update = a(uri, contentValues, str, strArr, "songlist", "_id");
                break;
            case 38:
                update = this.c.update("songreflist", contentValues, str, strArr);
                break;
            case 39:
                update = a(uri, contentValues, str, strArr, "songreflist", "_id");
                break;
            case 52:
                update = this.c.update("currentsongs", contentValues, str, strArr);
                break;
            case 53:
                update = a(uri, contentValues, str, strArr, "currentsongs", "_id");
                break;
            case 66:
                update = this.c.update("login_account", contentValues, str, strArr);
                break;
            case 67:
                update = this.c.update("badorder", contentValues, str, strArr);
                break;
            case 68:
                update = this.c.update("ring", contentValues, str, strArr);
                break;
            case 69:
                update = this.c.update("serviceinfomodle", contentValues, str, strArr);
                break;
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return update;
    }
}
