package com.duomi.app.online;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;

public class SynchronizeService extends Service {
    int a;
    public Handler b = new cz(this);
    /* access modifiers changed from: private */
    public boolean c = false;
    /* access modifiers changed from: private */
    public int d;

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
        ad.b("SynchronizeService", "onCreate>>>");
    }

    public void onStart(Intent intent, int i) {
        super.onStart(intent, i);
        ad.b("SynchronizeService", "onStart>>>user id>>>" + bn.a().h());
        if (!this.c) {
            Bundle extras = intent.getExtras();
            if (extras != null) {
                this.d = extras.getInt("type");
            }
            ad.b("SynchronizeService", "onStart()>>>type>>>" + this.d);
            da daVar = new da(this, null);
            int i2 = this.a;
            this.a = i2 + 1;
            daVar.execute(Integer.valueOf(i2));
        }
    }
}
