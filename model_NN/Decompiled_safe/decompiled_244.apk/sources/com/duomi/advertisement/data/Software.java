package com.duomi.advertisement.data;

import java.io.Serializable;

public class Software implements Serializable {
    private static final long serialVersionUID = 1;
    private String describe;
    private String icon;
    private String name;
    private String url;

    public Software(String str, String str2, String str3, String str4) {
        this.name = str;
        this.icon = str2;
        this.describe = str3;
        this.url = str4;
    }

    public String getDescribe() {
        return this.describe;
    }

    public String getIcon() {
        return this.icon;
    }

    public String getName() {
        return this.name;
    }

    public String getUrl() {
        return this.url;
    }

    public void setDescribe(String str) {
        this.describe = str;
    }

    public void setIcon(String str) {
        this.icon = str;
    }

    public void setName(String str) {
        this.name = str;
    }

    public void setUrl(String str) {
        this.url = str;
    }
}
