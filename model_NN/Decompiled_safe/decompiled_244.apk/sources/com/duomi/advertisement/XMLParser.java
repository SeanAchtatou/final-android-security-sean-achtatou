package com.duomi.advertisement;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

public class XMLParser {
    static String outstr = "";

    public static byte[] outPutXML(XML xml) {
        outstr = "<?xml version=\"1.0\" encoding=\"UTF-8\"?> ";
        printXML(xml);
        byte[] bArr = new byte[0];
        try {
            return outstr.getBytes("UTF-8");
        } catch (Exception e) {
            return bArr;
        }
    }

    public static XML parser(InputStream inputStream) {
        XML xml;
        Exception e;
        XML xml2;
        boolean z;
        XML xml3;
        XML xml4;
        XML xml5 = new XML();
        try {
            KXmlParser kXmlParser = new KXmlParser();
            kXmlParser.setInput(inputStream, "UTF-8");
            XML xml6 = xml5;
            int eventType = kXmlParser.getEventType();
            boolean z2 = true;
            while (eventType != 1) {
                if (eventType == 0) {
                    try {
                        xml4 = new XML();
                    } catch (Exception e2) {
                        e = e2;
                        xml = xml6;
                        System.out.println("KXML parser error : " + e);
                        xml2 = xml;
                        return xml2.getChild(0);
                    }
                    try {
                        xml4.isRoot = true;
                        boolean z3 = z2;
                        xml3 = xml4;
                        z = z3;
                    } catch (Exception e3) {
                        Exception exc = e3;
                        xml = xml4;
                        e = exc;
                        System.out.println("KXML parser error : " + e);
                        xml2 = xml;
                        return xml2.getChild(0);
                    }
                } else if (eventType == 1) {
                    z = z2;
                    xml3 = xml6;
                } else if (eventType == 2) {
                    XML xml7 = new XML(xml6);
                    try {
                        xml7.setName(kXmlParser.getName());
                        xml3 = xml7;
                        z = true;
                    } catch (Exception e4) {
                        Exception exc2 = e4;
                        xml = xml7;
                        e = exc2;
                        System.out.println("KXML parser error : " + e);
                        xml2 = xml;
                        return xml2.getChild(0);
                    }
                } else if (eventType == 3) {
                    xml3 = xml6.getParent();
                    z = false;
                } else {
                    if (eventType == 4 && z2) {
                        xml6.setText(kXmlParser.getText());
                    }
                    z = z2;
                    xml3 = xml6;
                }
                try {
                    xml6 = xml3;
                    z2 = z;
                    eventType = kXmlParser.next();
                } catch (Exception e5) {
                    e = e5;
                    xml = xml3;
                    System.out.println("KXML parser error : " + e);
                    xml2 = xml;
                    return xml2.getChild(0);
                }
            }
            xml2 = xml6;
        } catch (Exception e6) {
            Exception exc3 = e6;
            xml = xml5;
            e = exc3;
            System.out.println("KXML parser error : " + e);
            xml2 = xml;
            return xml2.getChild(0);
        }
        return xml2.getChild(0);
    }

    public static XML parser(byte[] bArr) {
        byte[] bArr2;
        byte[] bArr3;
        if (bArr == null || bArr.length == 0) {
            return new XML();
        }
        try {
            if ((bArr[0] & 255) == 239 && (bArr[1] & 255) == 187 && (bArr[2] & 255) == 191) {
                byte[] bArr4 = new byte[bArr.length];
                System.arraycopy(bArr, 0, bArr4, 0, bArr.length);
                bArr2 = new byte[(bArr.length - 3)];
                try {
                    System.arraycopy(bArr4, 3, bArr2, 0, bArr2.length);
                    bArr3 = bArr2;
                } catch (Exception e) {
                    e = e;
                    System.out.println("xml parser error : " + e);
                    bArr3 = bArr2;
                    return parser(new ByteArrayInputStream(bArr3));
                }
                return parser(new ByteArrayInputStream(bArr3));
            }
            bArr3 = bArr;
            return parser(new ByteArrayInputStream(bArr3));
        } catch (Exception e2) {
            e = e2;
            bArr2 = bArr;
            System.out.println("xml parser error : " + e);
            bArr3 = bArr2;
            return parser(new ByteArrayInputStream(bArr3));
        }
    }

    private static void printXML(XML xml) {
        outstr += "<" + xml.getName() + ">";
        if (xml.isNode()) {
            for (int i = 0; i < xml.getChildrenLength(); i++) {
                printXML(xml.getChild(i));
            }
        } else {
            outstr += xml.getText();
        }
        outstr += "</" + xml.getName() + ">";
    }
}
