package com.duomi.advertisement.networker;

import android.content.Context;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.ParseException;
import android.net.Proxy;
import android.net.Uri;
import android.util.Log;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.impl.client.DefaultHttpClient;

public class NetWorker {
    public static final int ConnectionTimeout = 15000;
    public static int SoTimeout = ConnectionTimeout;
    public static final int SocketBufferSize = 10240;
    private static final String TAG = "Networker";
    private static ConnectivityManager connect;

    public static HttpURLConnection getConnection(String str, Context context) {
        HttpURLConnection httpURLConnection = null;
        if (Proxy.getDefaultHost() != null) {
            try {
                if (!isFromWifi(context)) {
                    httpURLConnection = (HttpURLConnection) new URL(str).openConnection(new java.net.Proxy(Proxy.Type.HTTP, new InetSocketAddress(android.net.Proxy.getDefaultHost(), android.net.Proxy.getDefaultPort())));
                    httpURLConnection.setDoInput(true);
                    httpURLConnection.setConnectTimeout(ConnectionTimeout);
                    httpURLConnection.setReadTimeout(SoTimeout);
                    return httpURLConnection;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
        httpURLConnection.setDoInput(true);
        httpURLConnection.setConnectTimeout(ConnectionTimeout);
        httpURLConnection.setReadTimeout(SoTimeout);
        return httpURLConnection;
    }

    private static String getContentCharSet(HttpEntity httpEntity) throws ParseException {
        if (httpEntity.getContentEncoding() == null) {
            return null;
        }
        String value = httpEntity.getContentEncoding().getValue();
        if ("gzip".equals(value.toLowerCase().trim())) {
            return value;
        }
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0091, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x009c, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00a4, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00a5, code lost:
        r5 = r1;
        r1 = r0;
        r0 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00ae, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00af, code lost:
        r5 = r2;
        r2 = r1;
        r1 = r0;
        r0 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00b4, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00b5, code lost:
        r5 = r1;
        r1 = r0;
        r0 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:?, code lost:
        return r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:?, code lost:
        return r2;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0091  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x009c  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00a4 A[ExcHandler: all (r1v10 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:16:0x0043] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x00b4 A[ExcHandler: IOException (r1v6 'e' java.io.IOException A[CUSTOM_DECLARE]), Splitter:B:16:0x0043] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x00b9  */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:38:0x0097=Splitter:B:38:0x0097, B:30:0x008d=Splitter:B:30:0x008d} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.graphics.Bitmap getRemoteBitMap(java.lang.String r6, android.content.Context r7, java.lang.String r8, java.lang.String r9) throws java.io.IOException {
        /*
            r2 = 0
            if (r6 == 0) goto L_0x0068
            int r0 = r6.length()
            if (r0 <= 0) goto L_0x0068
            if (r8 == 0) goto L_0x00c3
            int r0 = r8.length()
            if (r0 <= 0) goto L_0x00c3
            java.lang.String r0 = "?"
            int r0 = r6.lastIndexOf(r0)
            if (r0 >= 0) goto L_0x0050
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.StringBuilder r0 = r0.append(r6)
            java.lang.String r1 = "?uid="
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r8)
            java.lang.String r0 = r0.toString()
        L_0x0030:
            java.lang.String r0 = java.net.URLDecoder.decode(r0)
            if (r7 == 0) goto L_0x00c0
            java.net.HttpURLConnection r0 = getConnection(r0, r7)     // Catch:{ IOException -> 0x008b, Exception -> 0x0095, all -> 0x00a1 }
        L_0x003a:
            if (r0 == 0) goto L_0x00bd
            r0.connect()     // Catch:{ IOException -> 0x008b, Exception -> 0x0095, all -> 0x00a1 }
            java.io.InputStream r0 = r0.getInputStream()     // Catch:{ IOException -> 0x008b, Exception -> 0x0095, all -> 0x00a1 }
            android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeStream(r0)     // Catch:{ IOException -> 0x00b4, Exception -> 0x00a9, all -> 0x00a4 }
            if (r1 != 0) goto L_0x006a
            if (r0 == 0) goto L_0x004e
            r0.close()
        L_0x004e:
            r0 = r2
        L_0x004f:
            return r0
        L_0x0050:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.StringBuilder r0 = r0.append(r6)
            java.lang.String r1 = "&uid="
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r8)
            java.lang.String r0 = r0.toString()
            goto L_0x0030
        L_0x0068:
            r0 = r2
            goto L_0x004f
        L_0x006a:
            java.lang.String r2 = com.duomi.advertisement.Util.getMd5(r6)     // Catch:{ IOException -> 0x00b4, Exception -> 0x00ae, all -> 0x00a4 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00b4, Exception -> 0x00ae, all -> 0x00a4 }
            r3.<init>()     // Catch:{ IOException -> 0x00b4, Exception -> 0x00ae, all -> 0x00a4 }
            java.lang.StringBuilder r3 = r3.append(r9)     // Catch:{ IOException -> 0x00b4, Exception -> 0x00ae, all -> 0x00a4 }
            java.lang.String r4 = "/advcache"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ IOException -> 0x00b4, Exception -> 0x00ae, all -> 0x00a4 }
            java.lang.String r3 = r3.toString()     // Catch:{ IOException -> 0x00b4, Exception -> 0x00ae, all -> 0x00a4 }
            saveToLocal(r1, r3, r2)     // Catch:{ IOException -> 0x00b4, Exception -> 0x00ae, all -> 0x00a4 }
        L_0x0084:
            if (r0 == 0) goto L_0x00bb
            r0.close()
            r0 = r1
            goto L_0x004f
        L_0x008b:
            r0 = move-exception
            r1 = r2
        L_0x008d:
            throw r0     // Catch:{ all -> 0x008e }
        L_0x008e:
            r0 = move-exception
        L_0x008f:
            if (r1 == 0) goto L_0x0094
            r1.close()
        L_0x0094:
            throw r0
        L_0x0095:
            r0 = move-exception
            r1 = r2
        L_0x0097:
            r0.printStackTrace()     // Catch:{ all -> 0x008e }
            if (r1 == 0) goto L_0x00b9
            r1.close()
            r0 = r2
            goto L_0x004f
        L_0x00a1:
            r0 = move-exception
            r1 = r2
            goto L_0x008f
        L_0x00a4:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x008f
        L_0x00a9:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0097
        L_0x00ae:
            r2 = move-exception
            r5 = r2
            r2 = r1
            r1 = r0
            r0 = r5
            goto L_0x0097
        L_0x00b4:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x008d
        L_0x00b9:
            r0 = r2
            goto L_0x004f
        L_0x00bb:
            r0 = r1
            goto L_0x004f
        L_0x00bd:
            r0 = r2
            r1 = r2
            goto L_0x0084
        L_0x00c0:
            r0 = r2
            goto L_0x003a
        L_0x00c3:
            r0 = r6
            goto L_0x0030
        */
        throw new UnsupportedOperationException("Method not decompiled: com.duomi.advertisement.networker.NetWorker.getRemoteBitMap(java.lang.String, android.content.Context, java.lang.String, java.lang.String):android.graphics.Bitmap");
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x002a A[SYNTHETIC, Splitter:B:16:0x002a] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x002f A[SYNTHETIC, Splitter:B:19:0x002f] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0034 A[SYNTHETIC, Splitter:B:22:0x0034] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String getString(java.io.InputStream r8, java.lang.String r9, java.lang.String r10) throws java.io.IOException {
        /*
            r7 = -1
            r5 = 0
            java.lang.StringBuffer r0 = new java.lang.StringBuffer
            r0.<init>()
            java.lang.String r1 = "UTF-8"
            java.io.BufferedInputStream r2 = new java.io.BufferedInputStream     // Catch:{ all -> 0x008a }
            r2.<init>(r8)     // Catch:{ all -> 0x008a }
            if (r10 != 0) goto L_0x0038
            java.io.InputStreamReader r3 = new java.io.InputStreamReader     // Catch:{ all -> 0x008f }
            r3.<init>(r2, r1)     // Catch:{ all -> 0x008f }
            r1 = r5
        L_0x0016:
            r4 = 1024(0x400, float:1.435E-42)
            char[] r4 = new char[r4]     // Catch:{ all -> 0x0027 }
            if (r3 == 0) goto L_0x0058
        L_0x001c:
            int r5 = r3.read(r4)     // Catch:{ all -> 0x0027 }
            if (r5 == r7) goto L_0x0058
            r6 = 0
            r0.append(r4, r6, r5)     // Catch:{ all -> 0x0027 }
            goto L_0x001c
        L_0x0027:
            r0 = move-exception
        L_0x0028:
            if (r3 == 0) goto L_0x002d
            r3.close()     // Catch:{ Exception -> 0x007b }
        L_0x002d:
            if (r1 == 0) goto L_0x0032
            r1.close()     // Catch:{ Exception -> 0x0080 }
        L_0x0032:
            if (r2 == 0) goto L_0x0037
            r2.close()     // Catch:{ Exception -> 0x0085 }
        L_0x0037:
            throw r0
        L_0x0038:
            java.lang.String r3 = r10.toLowerCase()     // Catch:{ all -> 0x008f }
            java.lang.String r4 = "gzip"
            int r3 = r3.indexOf(r4)     // Catch:{ all -> 0x008f }
            if (r3 <= r7) goto L_0x0051
            java.util.zip.GZIPInputStream r3 = new java.util.zip.GZIPInputStream     // Catch:{ all -> 0x008f }
            r3.<init>(r2)     // Catch:{ all -> 0x008f }
            java.io.InputStreamReader r4 = new java.io.InputStreamReader     // Catch:{ all -> 0x0093 }
            r4.<init>(r3, r1)     // Catch:{ all -> 0x0093 }
            r1 = r3
            r3 = r4
            goto L_0x0016
        L_0x0051:
            java.io.InputStreamReader r3 = new java.io.InputStreamReader     // Catch:{ all -> 0x008f }
            r3.<init>(r2, r1)     // Catch:{ all -> 0x008f }
            r1 = r5
            goto L_0x0016
        L_0x0058:
            if (r3 == 0) goto L_0x005d
            r3.close()     // Catch:{ Exception -> 0x006c }
        L_0x005d:
            if (r1 == 0) goto L_0x0062
            r1.close()     // Catch:{ Exception -> 0x0071 }
        L_0x0062:
            if (r2 == 0) goto L_0x0067
            r2.close()     // Catch:{ Exception -> 0x0076 }
        L_0x0067:
            java.lang.String r0 = r0.toString()
            return r0
        L_0x006c:
            r3 = move-exception
            r3.printStackTrace()
            goto L_0x005d
        L_0x0071:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0062
        L_0x0076:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0067
        L_0x007b:
            r3 = move-exception
            r3.printStackTrace()
            goto L_0x002d
        L_0x0080:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0032
        L_0x0085:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0037
        L_0x008a:
            r0 = move-exception
            r1 = r5
            r2 = r5
            r3 = r5
            goto L_0x0028
        L_0x008f:
            r0 = move-exception
            r1 = r5
            r3 = r5
            goto L_0x0028
        L_0x0093:
            r0 = move-exception
            r1 = r3
            r3 = r5
            goto L_0x0028
        */
        throw new UnsupportedOperationException("Method not decompiled: com.duomi.advertisement.networker.NetWorker.getString(java.io.InputStream, java.lang.String, java.lang.String):java.lang.String");
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x010e  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x00b8  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String httpPost(android.content.Context r9, java.lang.String r10, java.lang.String r11, java.lang.String r12) throws java.net.URISyntaxException, java.io.IOException {
        /*
            r7 = 0
            r5 = 1
            java.lang.String r8 = ">>executing connect network...error:"
            java.lang.String r6 = "Networker"
            org.apache.http.protocol.BasicHttpContext r0 = new org.apache.http.protocol.BasicHttpContext
            r0.<init>()
            java.net.URL r1 = new java.net.URL
            r1.<init>(r10)
            org.apache.http.client.methods.HttpPost r2 = new org.apache.http.client.methods.HttpPost
            java.net.URI r1 = r1.toURI()
            r2.<init>(r1)
            org.apache.http.params.BasicHttpParams r1 = new org.apache.http.params.BasicHttpParams
            r1.<init>()
            org.apache.http.HttpVersion r3 = org.apache.http.HttpVersion.HTTP_1_1
            org.apache.http.params.HttpProtocolParams.setVersion(r1, r3)
            java.lang.String r3 = "utf-8"
            org.apache.http.params.HttpProtocolParams.setContentCharset(r1, r3)
            org.apache.http.params.HttpProtocolParams.setHttpElementCharset(r1, r11)
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = android.os.Build.MANUFACTURER
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r4 = android.os.Build.MODEL
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            org.apache.http.params.HttpProtocolParams.setUserAgent(r1, r3)
            org.apache.http.params.HttpProtocolParams.setUseExpectContinue(r1, r5)
            org.apache.http.client.params.HttpClientParams.setRedirecting(r1, r5)
            r3 = 15000(0x3a98, float:2.102E-41)
            org.apache.http.params.HttpConnectionParams.setConnectionTimeout(r1, r3)
            int r3 = com.duomi.advertisement.networker.NetWorker.SoTimeout
            org.apache.http.params.HttpConnectionParams.setSoTimeout(r1, r3)
            r3 = 10240(0x2800, float:1.4349E-41)
            org.apache.http.params.HttpConnectionParams.setSocketBufferSize(r1, r3)
            r2.setParams(r1)
            org.apache.http.entity.StringEntity r1 = new org.apache.http.entity.StringEntity
            r1.<init>(r12, r11)
            r2.setEntity(r1)
            java.lang.String r1 = android.net.Proxy.getDefaultHost()     // Catch:{ IOException -> 0x00d1, Exception -> 0x00ef }
            if (r1 == 0) goto L_0x00c0
            boolean r3 = isFromWifi(r9)     // Catch:{ IOException -> 0x00d1, Exception -> 0x00ef }
            if (r3 != 0) goto L_0x00c0
            java.lang.String r3 = "Networker"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00d1, Exception -> 0x00ef }
            r4.<init>()     // Catch:{ IOException -> 0x00d1, Exception -> 0x00ef }
            java.lang.String r5 = "proxyHost is >>"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ IOException -> 0x00d1, Exception -> 0x00ef }
            java.lang.String r5 = r1.toString()     // Catch:{ IOException -> 0x00d1, Exception -> 0x00ef }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ IOException -> 0x00d1, Exception -> 0x00ef }
            java.lang.String r4 = r4.toString()     // Catch:{ IOException -> 0x00d1, Exception -> 0x00ef }
            android.util.Log.d(r3, r4)     // Catch:{ IOException -> 0x00d1, Exception -> 0x00ef }
            org.apache.http.impl.client.DefaultHttpClient r3 = new org.apache.http.impl.client.DefaultHttpClient     // Catch:{ IOException -> 0x00d1, Exception -> 0x00ef }
            r3.<init>()     // Catch:{ IOException -> 0x00d1, Exception -> 0x00ef }
            org.apache.http.HttpHost r4 = new org.apache.http.HttpHost     // Catch:{ IOException -> 0x00d1, Exception -> 0x00ef }
            r5 = 80
            r4.<init>(r1, r5)     // Catch:{ IOException -> 0x00d1, Exception -> 0x00ef }
            org.apache.http.params.HttpParams r1 = r3.getParams()     // Catch:{ IOException -> 0x00d1, Exception -> 0x00ef }
            java.lang.String r5 = "http.route.default-proxy"
            r1.setParameter(r5, r4)     // Catch:{ IOException -> 0x00d1, Exception -> 0x00ef }
            org.apache.http.params.HttpParams r1 = r3.getParams()     // Catch:{ IOException -> 0x00d1, Exception -> 0x00ef }
            java.lang.String r4 = "Connection"
            java.lang.String r5 = "Keep-Alive"
            r1.setParameter(r4, r5)     // Catch:{ IOException -> 0x00d1, Exception -> 0x00ef }
            org.apache.http.HttpResponse r0 = r3.execute(r2, r0)     // Catch:{ IOException -> 0x00d1, Exception -> 0x00ef }
            java.lang.String r1 = "Networker"
            java.lang.String r2 = ">>connect network by gprs successful."
            android.util.Log.d(r1, r2)     // Catch:{ IOException -> 0x00d1, Exception -> 0x00ef }
        L_0x00b6:
            if (r0 != 0) goto L_0x010e
            java.io.IOException r0 = new java.io.IOException
            java.lang.String r1 = " http response null "
            r0.<init>(r1)
            throw r0
        L_0x00c0:
            java.lang.String r1 = "Networker"
            java.lang.String r3 = "proxyHost is >>null"
            android.util.Log.d(r1, r3)     // Catch:{ IOException -> 0x00d1, Exception -> 0x00ef }
            org.apache.http.impl.client.DefaultHttpClient r1 = new org.apache.http.impl.client.DefaultHttpClient     // Catch:{ IOException -> 0x00d1, Exception -> 0x00ef }
            r1.<init>()     // Catch:{ IOException -> 0x00d1, Exception -> 0x00ef }
            org.apache.http.HttpResponse r0 = r1.execute(r2, r0)     // Catch:{ IOException -> 0x00d1, Exception -> 0x00ef }
            goto L_0x00b6
        L_0x00d1:
            r0 = move-exception
            java.lang.String r1 = "Networker"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = ">>executing connect network...error:"
            java.lang.StringBuilder r1 = r1.append(r8)
            java.lang.String r2 = r0.toString()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            android.util.Log.d(r6, r1, r0)
            throw r0
        L_0x00ef:
            r0 = move-exception
            java.lang.String r1 = "Networker"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = ">>executing connect network...error:"
            java.lang.StringBuilder r1 = r1.append(r8)
            java.lang.String r2 = r0.toString()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            android.util.Log.d(r6, r1, r0)
            r0 = r7
            goto L_0x00b6
        L_0x010e:
            org.apache.http.StatusLine r1 = r0.getStatusLine()
            int r1 = r1.getStatusCode()
            r2 = 200(0xc8, float:2.8E-43)
            if (r1 == r2) goto L_0x0122
            org.apache.http.client.ClientProtocolException r0 = new org.apache.http.client.ClientProtocolException
            java.lang.String r1 = " http response protocol error "
            r0.<init>(r1)
            throw r0
        L_0x0122:
            org.apache.http.HttpEntity r0 = r0.getEntity()
            if (r0 == 0) goto L_0x0155
            java.io.InputStream r1 = r0.getContent()
            java.lang.String r2 = getContentCharSet(r0)
            java.lang.String r1 = getString(r1, r11, r2)
            java.lang.String r2 = "Networker"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Response content length: "
            java.lang.StringBuilder r2 = r2.append(r3)
            long r3 = r0.getContentLength()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            android.util.Log.d(r6, r2)
            r0.consumeContent()
            r0 = r1
        L_0x0154:
            return r0
        L_0x0155:
            r0 = r7
            goto L_0x0154
        */
        throw new UnsupportedOperationException("Method not decompiled: com.duomi.advertisement.networker.NetWorker.httpPost(android.content.Context, java.lang.String, java.lang.String, java.lang.String):java.lang.String");
    }

    private static boolean isFromWifi(Context context) {
        if (connect == null) {
            connect = (ConnectivityManager) context.getSystemService("connectivity");
        }
        if (connect == null) {
            return false;
        }
        NetworkInfo activeNetworkInfo = connect.getActiveNetworkInfo();
        if (activeNetworkInfo == null) {
            return false;
        }
        Log.d(TAG, "type>>" + activeNetworkInfo.getType() + ">>wifi>>" + 1);
        return activeNetworkInfo.getType() == 1;
    }

    /* JADX WARNING: Removed duplicated region for block: B:38:0x0084 A[SYNTHETIC, Splitter:B:38:0x0084] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0096 A[SYNTHETIC, Splitter:B:47:0x0096] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x00a8 A[SYNTHETIC, Splitter:B:56:0x00a8] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x00b7 A[SYNTHETIC, Splitter:B:63:0x00b7] */
    /* JADX WARNING: Removed duplicated region for block: B:81:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:83:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:85:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:44:0x0091=Splitter:B:44:0x0091, B:53:0x00a3=Splitter:B:53:0x00a3, B:35:0x007f=Splitter:B:35:0x007f} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void saveToLocal(android.graphics.Bitmap r6, java.lang.String r7, java.lang.String r8) {
        /*
            if (r8 == 0) goto L_0x0041
            java.lang.String r0 = r8.trim()
            java.lang.String r1 = ""
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x0041
            r0 = 0
            java.io.File r1 = new java.io.File     // Catch:{ FileNotFoundException -> 0x007b, IOException -> 0x008d, Exception -> 0x009f, all -> 0x00b1 }
            r1.<init>(r7)     // Catch:{ FileNotFoundException -> 0x007b, IOException -> 0x008d, Exception -> 0x009f, all -> 0x00b1 }
            java.io.File r2 = new java.io.File     // Catch:{ FileNotFoundException -> 0x007b, IOException -> 0x008d, Exception -> 0x009f, all -> 0x00b1 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x007b, IOException -> 0x008d, Exception -> 0x009f, all -> 0x00b1 }
            r3.<init>()     // Catch:{ FileNotFoundException -> 0x007b, IOException -> 0x008d, Exception -> 0x009f, all -> 0x00b1 }
            java.lang.StringBuilder r3 = r3.append(r7)     // Catch:{ FileNotFoundException -> 0x007b, IOException -> 0x008d, Exception -> 0x009f, all -> 0x00b1 }
            java.lang.String r4 = "/"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ FileNotFoundException -> 0x007b, IOException -> 0x008d, Exception -> 0x009f, all -> 0x00b1 }
            java.lang.StringBuilder r3 = r3.append(r8)     // Catch:{ FileNotFoundException -> 0x007b, IOException -> 0x008d, Exception -> 0x009f, all -> 0x00b1 }
            java.lang.String r3 = r3.toString()     // Catch:{ FileNotFoundException -> 0x007b, IOException -> 0x008d, Exception -> 0x009f, all -> 0x00b1 }
            r2.<init>(r3)     // Catch:{ FileNotFoundException -> 0x007b, IOException -> 0x008d, Exception -> 0x009f, all -> 0x00b1 }
            boolean r3 = r1.exists()     // Catch:{ FileNotFoundException -> 0x007b, IOException -> 0x008d, Exception -> 0x009f, all -> 0x00b1 }
            if (r3 != 0) goto L_0x0047
            boolean r1 = r1.mkdirs()     // Catch:{ FileNotFoundException -> 0x007b, IOException -> 0x008d, Exception -> 0x009f, all -> 0x00b1 }
            if (r1 != 0) goto L_0x0047
            if (r0 == 0) goto L_0x0041
            r0.close()     // Catch:{ IOException -> 0x0042 }
        L_0x0041:
            return
        L_0x0042:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0041
        L_0x0047:
            boolean r1 = r2.exists()     // Catch:{ FileNotFoundException -> 0x007b, IOException -> 0x008d, Exception -> 0x009f, all -> 0x00b1 }
            if (r1 != 0) goto L_0x005e
            boolean r1 = r2.createNewFile()     // Catch:{ FileNotFoundException -> 0x007b, IOException -> 0x008d, Exception -> 0x009f, all -> 0x00b1 }
            if (r1 != 0) goto L_0x005e
            if (r0 == 0) goto L_0x0041
            r0.close()     // Catch:{ IOException -> 0x0059 }
            goto L_0x0041
        L_0x0059:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0041
        L_0x005e:
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x007b, IOException -> 0x008d, Exception -> 0x009f, all -> 0x00b1 }
            r1.<init>(r2)     // Catch:{ FileNotFoundException -> 0x007b, IOException -> 0x008d, Exception -> 0x009f, all -> 0x00b1 }
            android.graphics.Bitmap$CompressFormat r0 = android.graphics.Bitmap.CompressFormat.PNG     // Catch:{ FileNotFoundException -> 0x00c6, IOException -> 0x00c4, Exception -> 0x00c2 }
            r2 = 100
            r6.compress(r0, r2, r1)     // Catch:{ FileNotFoundException -> 0x00c6, IOException -> 0x00c4, Exception -> 0x00c2 }
            r1.flush()     // Catch:{ FileNotFoundException -> 0x00c6, IOException -> 0x00c4, Exception -> 0x00c2 }
            r1.close()     // Catch:{ FileNotFoundException -> 0x00c6, IOException -> 0x00c4, Exception -> 0x00c2 }
            if (r1 == 0) goto L_0x0041
            r1.close()     // Catch:{ IOException -> 0x0076 }
            goto L_0x0041
        L_0x0076:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0041
        L_0x007b:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x007f:
            r0.printStackTrace()     // Catch:{ all -> 0x00c0 }
            if (r1 == 0) goto L_0x0041
            r1.close()     // Catch:{ IOException -> 0x0088 }
            goto L_0x0041
        L_0x0088:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0041
        L_0x008d:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x0091:
            r0.printStackTrace()     // Catch:{ all -> 0x00c0 }
            if (r1 == 0) goto L_0x0041
            r1.close()     // Catch:{ IOException -> 0x009a }
            goto L_0x0041
        L_0x009a:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0041
        L_0x009f:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x00a3:
            r0.printStackTrace()     // Catch:{ all -> 0x00c0 }
            if (r1 == 0) goto L_0x0041
            r1.close()     // Catch:{ IOException -> 0x00ac }
            goto L_0x0041
        L_0x00ac:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0041
        L_0x00b1:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x00b5:
            if (r1 == 0) goto L_0x00ba
            r1.close()     // Catch:{ IOException -> 0x00bb }
        L_0x00ba:
            throw r0
        L_0x00bb:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00ba
        L_0x00c0:
            r0 = move-exception
            goto L_0x00b5
        L_0x00c2:
            r0 = move-exception
            goto L_0x00a3
        L_0x00c4:
            r0 = move-exception
            goto L_0x0091
        L_0x00c6:
            r0 = move-exception
            goto L_0x007f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.duomi.advertisement.networker.NetWorker.saveToLocal(android.graphics.Bitmap, java.lang.String, java.lang.String):void");
    }

    public InputStream getConnection(Context context) {
        Cursor query;
        if (connect == null) {
            connect = (ConnectivityManager) context.getSystemService("connectivity");
        }
        NetworkInfo activeNetworkInfo = connect.getActiveNetworkInfo();
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        if (activeNetworkInfo == null) {
            return null;
        }
        if (!(activeNetworkInfo.getType() == 1 || (query = context.getContentResolver().query(Uri.parse("content://telephony/carriers/preferapn"), null, null, null, null)) == null)) {
            query.moveToNext();
            String string = query.getString(query.getColumnIndex("proxy"));
            if (string != null && string.trim().length() > 0) {
                defaultHttpClient.getParams().setParameter("http.route.default-proxy", new HttpHost(string, 80));
            }
        }
        return null;
    }
}
