package com.duomi.advertisement;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

public class BackButtonView extends FrameLayout {
    ImageView image;
    TextView text;

    public BackButtonView(Context context) {
        super(context);
        setFocusable(true);
        setLongClickable(true);
        initView();
    }

    private void initView() {
        this.text = new TextView(getContext());
        this.image = new ImageView(getContext());
        this.image.setScaleType(ImageView.ScaleType.FIT_XY);
        this.text.setFocusable(true);
        this.text.setGravity(17);
        this.text.setTextColor(Color.rgb(116, 121, 125));
        this.text.setTextSize(16.0f);
        this.text.setText("返回");
        this.text.setBackgroundDrawable(null);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, -1);
        addView(this.image, layoutParams);
        addView(this.text, layoutParams);
    }

    public void setBackground(Drawable drawable) {
        this.image.setBackgroundDrawable(drawable);
    }

    public void setText(String str) {
        this.text.setText(str);
    }
}
