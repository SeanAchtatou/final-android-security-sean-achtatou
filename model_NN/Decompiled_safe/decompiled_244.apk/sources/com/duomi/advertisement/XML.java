package com.duomi.advertisement;

import java.util.Vector;

public class XML {
    private static int maxLevel = 0;
    private String[][] attribute;
    private Vector<XML> children = new Vector<>();
    private boolean isNode = false;
    public boolean isRoot = false;
    private int level = 0;
    private String name;
    private XML parent = null;
    private String text;

    public XML() {
    }

    public XML(XML xml) {
        this.parent = xml;
        xml.addChild(this);
    }

    public static int getmaxLevel() {
        return maxLevel;
    }

    public void addChild(XML xml) {
        this.isNode = true;
        xml.level = this.level + 1;
        if (xml.level > maxLevel) {
            maxLevel = xml.level;
        }
        this.children.addElement(xml);
    }

    public void deleteChild(int i) {
        this.children.removeElementAt(i);
        if (this.children.size() == 0) {
            this.isNode = false;
        }
    }

    public XML find1stByName(String str) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.children.size()) {
                return null;
            }
            if (this.children.elementAt(i2).getName().equals(str)) {
                return this.children.elementAt(i2);
            }
            i = i2 + 1;
        }
    }

    public Vector<XML> findByName(String str) {
        Vector<XML> vector = new Vector<>();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.children.size()) {
                return vector;
            }
            if (this.children.elementAt(i2).getName().equals(str)) {
                vector.addElement(this.children.elementAt(i2));
            }
            i = i2 + 1;
        }
    }

    public String[][] getAttribute() {
        return this.attribute;
    }

    public XML getChild(int i) {
        if (this.children == null || this.children.size() <= i) {
            return null;
        }
        return this.children.elementAt(i);
    }

    public int getChildrenLength() {
        return this.children.size();
    }

    public int getLevel() {
        return this.level;
    }

    public String getName() {
        return this.name;
    }

    public XML getParent() {
        return this.parent;
    }

    public String getText() {
        if (this.text == null) {
            this.text = "";
        }
        return this.text;
    }

    public boolean isNode() {
        return this.isNode;
    }

    public void setAttribute(String[][] strArr) {
        this.attribute = strArr;
    }

    public void setName(String str) {
        this.name = str;
    }

    public void setText(String str) {
        this.text = str;
    }
}
