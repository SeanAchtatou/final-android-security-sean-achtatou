package com.duomi.advertisement;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

public class TabTextView extends FrameLayout {
    ImageView image;
    TextView text;

    public TabTextView(Context context) {
        super(context);
        initView();
    }

    private void initView() {
        this.text = new TextView(getContext());
        this.image = new ImageView(getContext());
        this.image.setScaleType(ImageView.ScaleType.FIT_XY);
        this.text.setGravity(17);
        this.text.setTextColor(-1);
        this.text.setTextSize(18.0f);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, -1);
        addView(this.image, layoutParams);
        addView(this.text, layoutParams);
    }

    public void setBackground(Drawable drawable) {
        if (drawable == null) {
            this.image.setVisibility(8);
            return;
        }
        this.image.setVisibility(0);
        this.image.setImageDrawable(drawable);
    }

    public void setText(String str) {
        this.text.setText(str);
    }
}
