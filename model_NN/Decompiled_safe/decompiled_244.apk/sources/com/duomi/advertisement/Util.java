package com.duomi.advertisement;

import android.content.Context;
import android.os.Build;
import android.telephony.TelephonyManager;
import com.duomi.advertisement.data.Software;
import com.duomi.advertisement.networker.NetWorker;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Vector;

public class Util {
    private static final String FILENAME = "adcache";
    public static final String ICONCACHE = "/advcache";
    private static final String REQUESTURL = "http://ads.duomi.com/crossmarketfe/";
    private static final String TAG = "Util";
    private static StringBuffer header = null;
    private static final Object lockObj = new Object();
    private static String zipStr = "gzip";

    public interface OnPostLoadDataListener {
        void onPostLoadData(String str, ArrayList<String> arrayList, ArrayList<Software> arrayList2, ArrayList<Software> arrayList3, ArrayList<Software> arrayList4, boolean z);
    }

    public static int formatInt(String str) {
        if (str != null && str.length() > 0) {
            try {
                return Integer.valueOf(str).intValue();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

    public static String getIMEI(Context context) {
        try {
            return ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:107:?, code lost:
        return r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:108:?, code lost:
        return r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:109:?, code lost:
        return r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x00f0, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x00f1, code lost:
        r0.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x00f5, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x00f6, code lost:
        r0.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x010a, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x010b, code lost:
        r2.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x010f, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x0110, code lost:
        r1.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x0118, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x0119, code lost:
        r1 = r8;
        r2 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x0123, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x0124, code lost:
        r1 = r8;
        r2 = r7;
        r3 = null;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x00e5 A[SYNTHETIC, Splitter:B:62:0x00e5] */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x00ea A[SYNTHETIC, Splitter:B:65:0x00ea] */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x0101 A[SYNTHETIC, Splitter:B:75:0x0101] */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x0106 A[SYNTHETIC, Splitter:B:78:0x0106] */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x0118 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:16:0x005b] */
    /* JADX WARNING: Removed duplicated region for block: B:96:0x012d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String getLocalVersion(java.lang.String r12, com.duomi.advertisement.Util.OnPostLoadDataListener r13) {
        /*
            r11 = 0
            r10 = 1
            r9 = 0
            java.lang.String r0 = android.os.Environment.getExternalStorageState()
            java.lang.String r1 = "mounted"
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x0011
            r0 = r11
        L_0x0010:
            return r0
        L_0x0011:
            java.io.File r0 = new java.io.File
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.StringBuilder r1 = r1.append(r12)
            java.lang.String r2 = "/"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "adcache"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            if (r0 == 0) goto L_0x0037
            boolean r1 = r0.exists()
            if (r1 != 0) goto L_0x0039
        L_0x0037:
            r0 = r11
            goto L_0x0010
        L_0x0039:
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>()
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>()
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>()
            java.io.FileInputStream r7 = new java.io.FileInputStream     // Catch:{ Exception -> 0x00dc, all -> 0x00fc }
            r7.<init>(r0)     // Catch:{ Exception -> 0x00dc, all -> 0x00fc }
            java.io.ObjectInputStream r8 = new java.io.ObjectInputStream     // Catch:{ Exception -> 0x011e, all -> 0x0114 }
            r8.<init>(r7)     // Catch:{ Exception -> 0x011e, all -> 0x0114 }
            r0 = r9
        L_0x0058:
            r1 = 3
            if (r0 >= r1) goto L_0x009b
            java.lang.Object r12 = r8.readObject()     // Catch:{ Exception -> 0x0123, all -> 0x0118 }
            java.lang.String r12 = (java.lang.String) r12     // Catch:{ Exception -> 0x0123, all -> 0x0118 }
            r2.add(r12)     // Catch:{ Exception -> 0x0123, all -> 0x0118 }
            int r1 = r8.readInt()     // Catch:{ Exception -> 0x0123, all -> 0x0118 }
            switch(r0) {
                case 0: goto L_0x006e;
                case 1: goto L_0x007d;
                case 2: goto L_0x008c;
                default: goto L_0x006b;
            }     // Catch:{ Exception -> 0x0123, all -> 0x0118 }
        L_0x006b:
            int r0 = r0 + 1
            goto L_0x0058
        L_0x006e:
            r6 = r9
        L_0x006f:
            if (r6 >= r1) goto L_0x006b
            java.lang.Object r12 = r8.readObject()     // Catch:{ Exception -> 0x0123, all -> 0x0118 }
            com.duomi.advertisement.data.Software r12 = (com.duomi.advertisement.data.Software) r12     // Catch:{ Exception -> 0x0123, all -> 0x0118 }
            r3.add(r12)     // Catch:{ Exception -> 0x0123, all -> 0x0118 }
            int r6 = r6 + 1
            goto L_0x006f
        L_0x007d:
            r6 = r9
        L_0x007e:
            if (r6 >= r1) goto L_0x006b
            java.lang.Object r12 = r8.readObject()     // Catch:{ Exception -> 0x0123, all -> 0x0118 }
            com.duomi.advertisement.data.Software r12 = (com.duomi.advertisement.data.Software) r12     // Catch:{ Exception -> 0x0123, all -> 0x0118 }
            r4.add(r12)     // Catch:{ Exception -> 0x0123, all -> 0x0118 }
            int r6 = r6 + 1
            goto L_0x007e
        L_0x008c:
            r6 = r9
        L_0x008d:
            if (r6 >= r1) goto L_0x006b
            java.lang.Object r12 = r8.readObject()     // Catch:{ Exception -> 0x0123, all -> 0x0118 }
            com.duomi.advertisement.data.Software r12 = (com.duomi.advertisement.data.Software) r12     // Catch:{ Exception -> 0x0123, all -> 0x0118 }
            r5.add(r12)     // Catch:{ Exception -> 0x0123, all -> 0x0118 }
            int r6 = r6 + 1
            goto L_0x008d
        L_0x009b:
            java.lang.Object r1 = r8.readObject()     // Catch:{ Exception -> 0x0123, all -> 0x0118 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ Exception -> 0x0123, all -> 0x0118 }
            if (r13 == 0) goto L_0x00cc
            r0 = r10
        L_0x00a4:
            if (r1 == 0) goto L_0x00ce
            r6 = r10
        L_0x00a7:
            r0 = r0 & r6
            if (r0 == 0) goto L_0x00bf
            if (r2 == 0) goto L_0x00bf
            int r0 = r2.size()     // Catch:{ Exception -> 0x0128, all -> 0x0118 }
            if (r0 <= 0) goto L_0x00bf
            if (r3 == 0) goto L_0x00bf
            int r0 = r3.size()     // Catch:{ Exception -> 0x0128, all -> 0x0118 }
            if (r0 <= 0) goto L_0x00bf
            r6 = 1
            r0 = r13
            r0.onPostLoadData(r1, r2, r3, r4, r5, r6)     // Catch:{ Exception -> 0x0128, all -> 0x0118 }
        L_0x00bf:
            if (r7 == 0) goto L_0x00c4
            r7.close()     // Catch:{ IOException -> 0x00d0 }
        L_0x00c4:
            if (r8 == 0) goto L_0x0130
            r8.close()     // Catch:{ IOException -> 0x00d5 }
            r0 = r1
            goto L_0x0010
        L_0x00cc:
            r0 = r9
            goto L_0x00a4
        L_0x00ce:
            r6 = r9
            goto L_0x00a7
        L_0x00d0:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00c4
        L_0x00d5:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r1
            goto L_0x0010
        L_0x00dc:
            r0 = move-exception
            r1 = r11
            r2 = r11
            r3 = r11
        L_0x00e0:
            r0.printStackTrace()     // Catch:{ all -> 0x011c }
            if (r2 == 0) goto L_0x00e8
            r2.close()     // Catch:{ IOException -> 0x00f0 }
        L_0x00e8:
            if (r1 == 0) goto L_0x012d
            r1.close()     // Catch:{ IOException -> 0x00f5 }
            r0 = r3
            goto L_0x0010
        L_0x00f0:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00e8
        L_0x00f5:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r3
            goto L_0x0010
        L_0x00fc:
            r0 = move-exception
            r1 = r11
            r2 = r11
        L_0x00ff:
            if (r2 == 0) goto L_0x0104
            r2.close()     // Catch:{ IOException -> 0x010a }
        L_0x0104:
            if (r1 == 0) goto L_0x0109
            r1.close()     // Catch:{ IOException -> 0x010f }
        L_0x0109:
            throw r0
        L_0x010a:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0104
        L_0x010f:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0109
        L_0x0114:
            r0 = move-exception
            r1 = r11
            r2 = r7
            goto L_0x00ff
        L_0x0118:
            r0 = move-exception
            r1 = r8
            r2 = r7
            goto L_0x00ff
        L_0x011c:
            r0 = move-exception
            goto L_0x00ff
        L_0x011e:
            r0 = move-exception
            r1 = r11
            r2 = r7
            r3 = r11
            goto L_0x00e0
        L_0x0123:
            r0 = move-exception
            r1 = r8
            r2 = r7
            r3 = r11
            goto L_0x00e0
        L_0x0128:
            r0 = move-exception
            r2 = r7
            r3 = r1
            r1 = r8
            goto L_0x00e0
        L_0x012d:
            r0 = r3
            goto L_0x0010
        L_0x0130:
            r0 = r1
            goto L_0x0010
        */
        throw new UnsupportedOperationException("Method not decompiled: com.duomi.advertisement.Util.getLocalVersion(java.lang.String, com.duomi.advertisement.Util$OnPostLoadDataListener):java.lang.String");
    }

    public static String getMd5(String str) {
        return str == null ? "" : new MD5().getMD5ofStr(str).substring(0, 8);
    }

    public static String getPhoneNumber(Context context) {
        try {
            return ((TelephonyManager) context.getSystemService("phone")).getLine1Number();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    private static boolean getRequestState(XML xml) {
        new XML();
        return xml.find1stByName("srsh").find1stByName("sc").getText().substring(0, 2).equalsIgnoreCase("OK");
    }

    public static String getRequstStr(Context context, String str, String str2, String str3, String str4, String str5) {
        String stringBuffer;
        synchronized (lockObj) {
            if (header == null) {
                header = new StringBuffer();
                header.append("<c><srqh><cv>");
                header.append(str3);
                header.append("</cv><ua>");
                header.append(getUserAgent(context));
                header.append("</ua><cn>");
                header.append(getPhoneNumber(context));
                header.append("</cn><uid>");
                header.append(str);
                header.append("</uid><sid>");
                header.append(str2);
                header.append("</sid><cc>");
                header.append(str4);
                header.append("</cc><pc>");
                header.append(str5);
                header.append("</pc>");
                header.append("<zt>").append(zipStr).append("</zt>");
                header.append("<devi>").append(getIMEI(context)).append("</devi>");
                header.append("</srqh>");
            }
            StringBuffer stringBuffer2 = new StringBuffer();
            stringBuffer2.append(header);
            stringBuffer = stringBuffer2.toString();
        }
        return stringBuffer;
    }

    public static String getUserAgent(Context context) {
        try {
            return Build.MANUFACTURER + Build.MODEL;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    private static Software parseItems(XML xml) {
        String str = "";
        if (xml == null) {
            return null;
        }
        String str2 = str;
        String str3 = str;
        String str4 = str;
        for (int i = 0; i < xml.getChildrenLength(); i++) {
            if (xml.getChild(i).getName().equals("title")) {
                str4 = xml.getChild(i).getText();
            } else if (xml.getChild(i).getName().equals("caption")) {
                str3 = xml.getChild(i).getText();
            } else if (xml.getChild(i).getName().equals("icon")) {
                str2 = xml.getChild(i).getText();
            } else if (xml.getChild(i).getName().equals("url")) {
                str = xml.getChild(i).getText();
            }
        }
        return new Software(str4, str2, str3, str);
    }

    public static void parseSoftList(String str, OnPostLoadDataListener onPostLoadDataListener, Context context) {
        if (str != null && str.length() > 0) {
            try {
                parserData(str, onPostLoadDataListener);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void parserData(String str, OnPostLoadDataListener onPostLoadDataListener) {
        String str2 = "0";
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList();
        ArrayList arrayList4 = new ArrayList();
        if (str != null && str.length() > 0) {
            XML parser = XMLParser.parser(str.getBytes());
            if (parser.getName().equals("c")) {
                if (getRequestState(parser)) {
                    XML find1stByName = parser.find1stByName("mver");
                    if (find1stByName != null) {
                        str2 = find1stByName.getText();
                    }
                    Vector<XML> findByName = parser.findByName("tab");
                    for (int i = 0; i < findByName.size(); i++) {
                        XML find1stByName2 = findByName.elementAt(i).find1stByName("title");
                        if (find1stByName2 != null) {
                            arrayList.add(find1stByName2.getText());
                        } else {
                            arrayList.add("");
                        }
                        Vector<XML> findByName2 = findByName.elementAt(i).findByName("item");
                        for (int i2 = 0; i2 < findByName2.size(); i2++) {
                            Software parseItems = parseItems(findByName2.elementAt(i2));
                            switch (i) {
                                case 0:
                                    arrayList2.add(parseItems);
                                    break;
                                case 1:
                                    arrayList3.add(parseItems);
                                    break;
                                case 2:
                                    arrayList4.add(parseItems);
                                    break;
                            }
                        }
                    }
                }
            } else {
                return;
            }
        }
        String str3 = str2;
        if (onPostLoadDataListener != null && arrayList != null && arrayList.size() > 0 && arrayList2 != null && arrayList2.size() > 0) {
            onPostLoadDataListener.onPostLoadData(str3, arrayList, arrayList2, arrayList3, arrayList4, false);
        }
    }

    public static String requestNetData(Context context, String str, String str2) {
        StringBuffer stringBuffer = new StringBuffer(str);
        stringBuffer.append("<k>market</k><mver>").append(str2).append("</mver></c>");
        try {
            return NetWorker.httpPost(context, REQUESTURL, "utf-8", stringBuffer.toString());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        return null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:102:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0082 A[SYNTHETIC, Splitter:B:37:0x0082] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0087 A[SYNTHETIC, Splitter:B:40:0x0087] */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x00f0 A[SYNTHETIC, Splitter:B:70:0x00f0] */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x00f5 A[SYNTHETIC, Splitter:B:73:0x00f5] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void writeToLocal(java.lang.String r9, java.lang.String r10, java.util.ArrayList<java.lang.String> r11, java.util.ArrayList<com.duomi.advertisement.data.Software> r12, java.util.ArrayList<com.duomi.advertisement.data.Software> r13, java.util.ArrayList<com.duomi.advertisement.data.Software> r14) {
        /*
            r4 = 0
            r7 = 3
            r6 = 0
            if (r10 == 0) goto L_0x0019
            int r0 = r10.length()
            if (r0 <= 0) goto L_0x0019
            if (r11 == 0) goto L_0x0019
            int r0 = r11.size()
            if (r0 < r7) goto L_0x0019
            if (r12 == 0) goto L_0x0019
            if (r13 == 0) goto L_0x0019
            if (r14 != 0) goto L_0x001a
        L_0x0019:
            return
        L_0x001a:
            java.lang.String r0 = android.os.Environment.getExternalStorageState()
            java.lang.String r1 = "mounted"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x0019
            java.io.File r0 = new java.io.File     // Catch:{ Exception -> 0x007a, all -> 0x00eb }
            r0.<init>(r9)     // Catch:{ Exception -> 0x007a, all -> 0x00eb }
            java.io.File r1 = new java.io.File     // Catch:{ Exception -> 0x007a, all -> 0x00eb }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x007a, all -> 0x00eb }
            r2.<init>()     // Catch:{ Exception -> 0x007a, all -> 0x00eb }
            java.lang.StringBuilder r2 = r2.append(r9)     // Catch:{ Exception -> 0x007a, all -> 0x00eb }
            java.lang.String r3 = "/"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x007a, all -> 0x00eb }
            java.lang.String r3 = "adcache"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x007a, all -> 0x00eb }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x007a, all -> 0x00eb }
            r1.<init>(r2)     // Catch:{ Exception -> 0x007a, all -> 0x00eb }
            if (r0 == 0) goto L_0x0054
            boolean r2 = r0.exists()     // Catch:{ Exception -> 0x007a, all -> 0x00eb }
            if (r2 != 0) goto L_0x0054
            r0.mkdirs()     // Catch:{ Exception -> 0x007a, all -> 0x00eb }
        L_0x0054:
            if (r1 == 0) goto L_0x005c
            boolean r0 = r1.exists()     // Catch:{ Exception -> 0x007a, all -> 0x00eb }
            if (r0 == 0) goto L_0x0076
        L_0x005c:
            java.io.FileOutputStream r0 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x007a, all -> 0x00eb }
            r0.<init>(r1)     // Catch:{ Exception -> 0x007a, all -> 0x00eb }
            java.io.ObjectOutputStream r1 = new java.io.ObjectOutputStream     // Catch:{ Exception -> 0x010f, all -> 0x0103 }
            r1.<init>(r0)     // Catch:{ Exception -> 0x010f, all -> 0x0103 }
            r2 = r6
        L_0x0067:
            if (r2 >= r7) goto L_0x00cc
            java.lang.Object r3 = r11.get(r2)     // Catch:{ Exception -> 0x0115, all -> 0x0108 }
            r1.writeObject(r3)     // Catch:{ Exception -> 0x0115, all -> 0x0108 }
            switch(r2) {
                case 0: goto L_0x0090;
                case 1: goto L_0x00a4;
                case 2: goto L_0x00b8;
                default: goto L_0x0073;
            }
        L_0x0073:
            int r2 = r2 + 1
            goto L_0x0067
        L_0x0076:
            r1.createNewFile()     // Catch:{ Exception -> 0x007a, all -> 0x00eb }
            goto L_0x005c
        L_0x007a:
            r0 = move-exception
            r1 = r4
            r2 = r4
        L_0x007d:
            r0.printStackTrace()     // Catch:{ all -> 0x010d }
            if (r2 == 0) goto L_0x0085
            r2.close()     // Catch:{ IOException -> 0x00e6 }
        L_0x0085:
            if (r1 == 0) goto L_0x0019
            r1.close()     // Catch:{ IOException -> 0x008b }
            goto L_0x0019
        L_0x008b:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0019
        L_0x0090:
            int r3 = r12.size()     // Catch:{ Exception -> 0x0115, all -> 0x0108 }
            r1.writeInt(r3)     // Catch:{ Exception -> 0x0115, all -> 0x0108 }
            r4 = r6
        L_0x0098:
            if (r4 >= r3) goto L_0x0073
            java.lang.Object r5 = r12.get(r4)     // Catch:{ Exception -> 0x0115, all -> 0x0108 }
            r1.writeObject(r5)     // Catch:{ Exception -> 0x0115, all -> 0x0108 }
            int r4 = r4 + 1
            goto L_0x0098
        L_0x00a4:
            int r3 = r13.size()     // Catch:{ Exception -> 0x0115, all -> 0x0108 }
            r1.writeInt(r3)     // Catch:{ Exception -> 0x0115, all -> 0x0108 }
            r4 = r6
        L_0x00ac:
            if (r4 >= r3) goto L_0x0073
            java.lang.Object r5 = r13.get(r4)     // Catch:{ Exception -> 0x0115, all -> 0x0108 }
            r1.writeObject(r5)     // Catch:{ Exception -> 0x0115, all -> 0x0108 }
            int r4 = r4 + 1
            goto L_0x00ac
        L_0x00b8:
            int r3 = r14.size()     // Catch:{ Exception -> 0x0115, all -> 0x0108 }
            r1.writeInt(r3)     // Catch:{ Exception -> 0x0115, all -> 0x0108 }
            r4 = r6
        L_0x00c0:
            if (r4 >= r3) goto L_0x0073
            java.lang.Object r5 = r14.get(r4)     // Catch:{ Exception -> 0x0115, all -> 0x0108 }
            r1.writeObject(r5)     // Catch:{ Exception -> 0x0115, all -> 0x0108 }
            int r4 = r4 + 1
            goto L_0x00c0
        L_0x00cc:
            r1.writeObject(r10)     // Catch:{ Exception -> 0x0115, all -> 0x0108 }
            if (r0 == 0) goto L_0x00d4
            r0.close()     // Catch:{ IOException -> 0x00e1 }
        L_0x00d4:
            if (r1 == 0) goto L_0x0019
            r1.close()     // Catch:{ IOException -> 0x00db }
            goto L_0x0019
        L_0x00db:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0019
        L_0x00e1:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00d4
        L_0x00e6:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0085
        L_0x00eb:
            r0 = move-exception
            r1 = r4
            r2 = r4
        L_0x00ee:
            if (r2 == 0) goto L_0x00f3
            r2.close()     // Catch:{ IOException -> 0x00f9 }
        L_0x00f3:
            if (r1 == 0) goto L_0x00f8
            r1.close()     // Catch:{ IOException -> 0x00fe }
        L_0x00f8:
            throw r0
        L_0x00f9:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x00f3
        L_0x00fe:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00f8
        L_0x0103:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r4
            goto L_0x00ee
        L_0x0108:
            r2 = move-exception
            r8 = r2
            r2 = r0
            r0 = r8
            goto L_0x00ee
        L_0x010d:
            r0 = move-exception
            goto L_0x00ee
        L_0x010f:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r4
            goto L_0x007d
        L_0x0115:
            r2 = move-exception
            r8 = r2
            r2 = r0
            r0 = r8
            goto L_0x007d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.duomi.advertisement.Util.writeToLocal(java.lang.String, java.lang.String, java.util.ArrayList, java.util.ArrayList, java.util.ArrayList, java.util.ArrayList):void");
    }
}
